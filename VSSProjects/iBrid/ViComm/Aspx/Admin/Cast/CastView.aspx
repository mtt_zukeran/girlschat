<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastView.aspx.cs" Inherits="Cast_CastView" Title="出演者詳細" %>

<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="iBridCommLib" %>
<%@ Import Namespace="System" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="出演者詳細"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HolderContent" runat="Server">
	<asp:Panel runat="server" ID="pnlSeek">
		<asp:Panel runat="server" ID="pnlKey">
			<fieldset class="fieldset">
				<legend>[検索条件]</legend>
				<table border="0" style="width:650px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							ログインID・電話番号
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtLoginId" runat="server" Width="80px" MaxLength="11"></asp:TextBox>
							<asp:Label ID="lblUserSeq" runat="server" Text="" Visible="false"></asp:Label>
						</td>
						<td class="tdHeaderStyle">
							ﾒｰﾙｱﾄﾞﾚｽ
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtEmailAddr" runat="server" Width="200px" MaxLength="64"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							ユーザーSEQ
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtUserSeq" runat="server" Width="80px" MaxLength="15"></asp:TextBox>
						</td>
						<td class="tdHeaderStyle2">
							履歴日
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtReportDayFrom" runat="server" MaxLength="10" Width="66px"></asp:TextBox>&nbsp;〜
							<asp:TextBox ID="txtReportDayTo" runat="server" MaxLength="10" Width="66px"></asp:TextBox>
							<asp:CheckBox ID="chkReportDayEnable" runat="server" Text="有効" Checked="true" />
							<asp:RangeValidator ID="vdrReportDayFrom" runat="server" ErrorMessage="履歴日Fromを正しく入力して下さい。" ControlToValidate="txtReportDayFrom" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:RangeValidator ID="vdrReportDayTo" runat="server" ErrorMessage="履歴日Toを正しく入力して下さい。" ControlToValidate="txtReportDayTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
								Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:CompareValidator ID="vdcReportDayFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtReportDayFrom" ControlToValidate="txtReportDayTo"
								Operator="GreaterThanEqual" ValidationGroup="Key">*</asp:CompareValidator>
						</td>
					</tr>
				</table>
				<asp:CustomValidator ID="vrdLoginId" runat="server" ErrorMessage="CustomValidator" OnServerValidate="vrdLoginId_ServerValidate" ValidationGroup="Key" Display="Dynamic">検索条件を入力して下さい。</asp:CustomValidator>
				<asp:Label ID="lblNotFound" runat="server" Text="指定された出演者は存在しません。" ForeColor="Red" Font-Bold="true"></asp:Label>
				<asp:Label ID="lblManyUser" runat="server" Text="該当の出演者は複数存在します。検索内容を変更して下さい。" ForeColor="Red" Font-Bold="true"></asp:Label>
				<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
				<asp:Button ID="btnBack" runat="server" Text="戻る" CssClass="seekbutton" OnClick="btnBack_Click" />
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlWithdrawal" HorizontalAlign="Left" Visible="true" Width="400px">
			<table border="0">
				<tr>
					<td>
						<fieldset class="fieldset-inner">
							<legend>[退会申請]</legend>
							<table border="0" class="tableStyleAutoSize">
								<tr>
									<td class="tdHeaderStyle">
										申請状態
									</td>
									<td class="tdDataStyle">
										<asp:Label ID="lblWithdrawalStatus" runat="server" Text="申請"></asp:Label><asp:Label ID="lblWithdrawalSeq" runat="server" Text="" Visible="false"></asp:Label>
										<asp:Label ID="lblErrorMessageWithdrawal" runat="server" ForeColor="red" Visible="false"></asp:Label>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										申請日時
									</td>
									<td class="tdDataStyle">
										<asp:Label ID="lblWithdrawalDate" runat="server" Text="yyyy/mm/dd hh:ss"></asp:Label>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										理由区分
									</td>
									<td class="tdDataStyle">
										<asp:Label ID="lblWithdrawalReasonCd" runat="server" Text="申請"></asp:Label>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										詳細理由
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtWithdrawalReasonDoc" runat="server" Width="250px" TextMode="MultiLine" Height="100px" ReadOnly="true"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle2">
										備考
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtWithdrawalRemarks" runat="server" Width="250px" TextMode="MultiLine" Height="100px"></asp:TextBox>
									</td>
								</tr>
							</table>
							<asp:Button runat="server" ID="btnReturnWithdrawalList" Text="申請履歴に戻る" CssClass="seekbutton" ValidationGroup="Withdrawal" OnClick="btnReturnWithdrawalList_Click" />
							<asp:Button runat="server" ID="btnUpdateWithdrawal" Text="備考更新" CssClass="seekbutton" ValidationGroup="Withdrawal" OnClick="btnUpdateWithdrawal_Click" />
							<asp:Button runat="server" ID="btnDelWithdrawal" Text="申請削除" CssClass="seekbutton" ValidationGroup="Withdrawal" OnClick="btnDelWithdrawal_Click" />
							<asp:Button runat="server" ID="btnSendDetainMail" Text="引き留めメール送信" CssClass="seekbutton" ValidationGroup="Withdrawal" OnClick="btnSendDetainMail_Click" />
							<asp:Button runat="server" ID="btnAcceptWithdrawal" Text="退会受理" CssClass="seekrightbutton" ValidationGroup="Withdrawal" OnClick="btnAcceptWithdrawal_Click" />
						</fieldset>
					</td>
				</tr>
			</table>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlWithdrawalHistoryDetail" Visible="false">
			<fieldset class="fieldset-inner">
				<legend>[退会履歴]</legend>
				<asp:Button ID="btnCloseWithdrawalHistoryDetail" runat="server" Text="閉じる" OnClick="btnCloseWithdrawalHistoryDetail_Click" />
				<table border="0" class="tableStyleAutoSize"">
					<tr>
						<td class="tdHeaderStyle">
							退会処理日
						</td>
						<td class="tdDataStyle">
                            <asp:DropDownList ID="lstWithdrawalYYYY" runat="server" Width="60px">
                            </asp:DropDownList>年
                            <asp:DropDownList ID="lstWithdrawalMM" runat="server" Width="40px">
                            </asp:DropDownList>月
                            <asp:DropDownList ID="lstWithdrawalDD" runat="server" Width="40px">
                            </asp:DropDownList>日
                            <asp:DropDownList ID="lstWithdrawalHH" runat="server" Width="40px">
                            </asp:DropDownList>時
                            <asp:DropDownList ID="lstWithdrawalMI" runat="server" Width="40px">
                            </asp:DropDownList>分
							<asp:Label ID="lblWithdrawalSS" runat="server" Visible="false"></asp:Label>
							<asp:Label ID="lblWithdrawalHistorySeq" runat="server" Visible="false"></asp:Label>
							<asp:Label ID="lblWithdrawalHistorySiteCd" runat="server" Visible="false"></asp:Label>
							<asp:CustomValidator ID="vrdWithdrawalHistoryDate" runat="server" ControlToValidate="lstWithdrawalMI" ErrorMessage="退会処理日を正しく入力して下さい。" 
								OnServerValidate="vrdWithdrawalHistoryDate_ServerValidate" ValidationGroup="WithdrawalHistory" Display="Dynamic" />
							<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender22" TargetControlID="vrdWithdrawalHistoryDate" HighlightCssClass="validatorCallout" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							備考
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtWithdrawalHistoryRemarks" runat="server" Width="500px" Height="48px" TextMode="MultiLine"></asp:TextBox>
							<asp:RequiredFieldValidator ID="vdrWithdrawalHistoryRemarks" runat="server" ErrorMessage="備考を入力してください。" ControlToValidate="txtWithdrawalHistoryRemarks" ValidationGroup="WithdrawalHistory"
								Display="Dynamic">*</asp:RequiredFieldValidator>
							<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender20" TargetControlID="vdrWithdrawalHistoryRemarks" HighlightCssClass="validatorCallout" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							退会時ポイント付与回数
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtWithdrawalHistoryAddPointCount" runat="server" Width="70px" MaxLength="4"></asp:TextBox>
							<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtWithdrawalHistoryAddPointCount" />
							<asp:RegularExpressionValidator ID="vdeWithdrawalHistoryAddPointCount" runat="server" ErrorMessage="退会時ポイント付与回数は数値で入力してください。" ControlToValidate="txtWithdrawalHistoryAddPointCount" ValidationExpression="^\d+$"
								ValidationGroup="WithdrawalHistory" Display="Dynamic">*</asp:RegularExpressionValidator>
							<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender19" TargetControlID="vdeWithdrawalHistoryAddPointCount" HighlightCssClass="validatorCallout" />
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnRegistWithdrawalHistory" Text="登録" CssClass="seekbutton" OnClick="btnRegWithdrawalHistory_Click" Visible="false" ValidationGroup="WithdrawalHistory" />
				<asp:Button runat="server" ID="btnUpdateWithdrawalHistory" Text="更新" CssClass="seekbutton" OnClick="btnUpWithdrawalHistory_Click" Visible="false" ValidationGroup="WithdrawalHistory" />
				<asp:Button runat="server" ID="btnDeleteWithdrawalHistory" Text="削除" CssClass="seekbutton" OnClick="btnDelWithdrawalHistory_Click" Visible="false" ValidationGroup="WithdrawalHistory" />
				<br />
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlWithdrawalHistoryList" Visible="false">
			<fieldset class="fieldset-inner">
				<legend>[退会履歴一覧]</legend>
				<asp:Button ID="btnNewWithdrawalHistory" runat="server" Text="新規登録" OnClick="btnNewWithdrawalHistory_Click" />
				<asp:Button ID="btnCloseWithdrawalHistoryList" runat="server" Text="閉じる" OnClick="btnCloseWithdrawalHistoryList_Click" />
				<asp:GridView ID="grdWithdrawalHistoryList" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsWithdrawalHistoryList" AllowSorting="True" SkinID="GridViewColor"
					PageSize="30">
					<Columns>
						<asp:TemplateField HeaderText="退会処理日時">
							<ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Wrap="false" />
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="lblCreateDate" runat="server" Text='<%# DateTime.Parse(Eval("CREATE_DATE").ToString()).ToString("yyyy/MM/dd HH:mm:ss") %>'></asp:Label><br />
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="備考">
							<ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="lblRemarks" runat="server" Text='<%# GetWithdrawalHistoryRemarks(Eval("REMARKS"),Eval("WITHDRAWAL_ADD_POINT_CNT"))%>'></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="">
							<ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Wrap="false" />
							<ItemTemplate>
								<asp:LinkButton ID="lnkEditWithdrawalHistory" runat="server" CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("WITHDRAWAL_HISTORY_SEQ")) %>' Text="編集" OnCommand="lnkEditWithdrawalHistory_Command"></asp:LinkButton>
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
					<PagerSettings Mode="NumericFirstLast" />
				</asp:GridView>
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlBingoEntry" HorizontalAlign="Left" Visible="false" Width="400px">
			<table border="0">
				<tr>
					<td>
						<fieldset class="fieldset-inner">
							<legend>[ﾒｰﾙdeﾋﾞﾝｺﾞ]</legend>
							<table border="0" class="tableStyleAutoSize">
								<tr>
									<td class="tdHeaderStyle">
										ﾋﾞﾝｺﾞｶｰﾄﾞ
									</td>
									<td class="tdDataStyle">
										<asp:Label ID="lblBingoCardNo" runat="server"></asp:Label>&nbsp;枚目
										<asp:Label ID="lblBingoEntrySeq" runat="server" Visible="false"></asp:Label>
										<asp:Label ID="lblBingoTermSeq" runat="server" Visible="false"></asp:Label>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										挑戦回数
									</td>
									<td class="tdDataStyle">
										<asp:Label ID="lblBingoCharangeCount" runat="server"></asp:Label>&nbsp;回
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										付与前のｼﾞｬｯｸﾎﾟｯﾄ金額
									</td>
									<td class="tdDataStyle">
										<asp:Label ID="lblBingoCompliateJackpotBefore" runat="server"></asp:Label>&nbsp;円
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										付与PT
									</td>
									<td class="tdDataStyle">
										<asp:Label ID="lblBingoElectionPoint" runat="server"></asp:Label>&nbsp;PT
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										付与金額
									</td>
									<td class="tdDataStyle">
										<asp:Label ID="lblBingoElectionAmt" runat="server"></asp:Label>&nbsp;円
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										付与後のｼﾞｬｯｸﾎﾟｯﾄ金額
									</td>
									<td class="tdDataStyle">
										<asp:Label ID="lblBingoCompliateJackpotAfter" runat="server"></asp:Label>&nbsp;円
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										ｴﾝﾄﾘｰ日時
									</td>
									<td class="tdDataStyle">
										<asp:Label ID="lblBingoEntryDate" runat="server" Text=""></asp:Label>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										ﾋﾞﾝｺﾞ日時
									</td>
									<td class="tdDataStyle">
										<asp:Label ID="lblBingoApplicationDate" runat="server" Text=""></asp:Label>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle2">
										pt付与
									</td>
									<td class="tdDataStyle">
										<asp:RadioButton ID="rdoBingoNotPointPayment" runat="server" Text="未" GroupName="AdGrouping" ForeColor="red" Checked="true" />
										<asp:RadioButton ID="rdoBingoPointPayment" runat="server" Text="済" GroupName="AdGrouping" />
									</td>
								</tr>
							</table>
							<asp:Button runat="server" ID="btnBingoEntryMainte" Text="更新" CssClass="seekrightbutton" OnClick="btnBingoEntryMainte_Click" />
							<asp:Button runat="server" ID="btnReturnBingoEntryList" Text="一覧に戻る" CssClass="seekbutton" OnClick="btnReturnBingoEntryList_Click" />
						</fieldset>
					</td>
				</tr>
			</table>
		</asp:Panel>
		<table border="0" style="margin-bottom: 0px">
			<asp:Panel runat="server" ID="pnlCharacter">
				<tr>
					<td>
						<fieldset class="fieldset-inner">
							<legend>[キャラクター設定]</legend>
							<asp:GridView ID="grdCharacter" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCharacter" AllowSorting="True" SkinID="GridView"
								OnRowDataBound="grdCharacter_RowDataBound" Font-Size="X-Small" DataKeyNames="SITE_CD,USER_SEQ,USER_CHAR_NO,LOGIN_ID">
								<Columns>
									<asp:TemplateField HeaderText="状態">
										<ItemTemplate>
											<asp:Image ID="Image2" runat="server" ImageUrl='<%# Eval("SYS_ONLINE_IMG_PATH","../{0}") %>' ImageAlign="Top" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="ｻｲﾄ">
										<ItemTemplate>
											<asp:HyperLink ID="lnkSubCharacter" runat="server" NavigateUrl='<%# string.Format("CastView.aspx?loginid={0}&sitecd={1}&return=CastView.aspx",Eval("LOGIN_ID"),Eval("SITE_CD")) %>'
												Text='<%# Eval("SITE_NM")%>' Enabled='<%# GetMultiCharFlag(Eval("SITE_CD")) %>'></asp:HyperLink><br />
											<asp:Label ID="lblConnectTypeNm1" runat="server" Text='<%# Eval("CONNECT_TYPE_NM") %>'></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="ｻｲﾄ/ｷｬﾗｸﾀｰNo">
										<ItemTemplate>
											<asp:Label ID="lblSiteNm" runat="server" Text='<%# Eval("SITE_NM") %>'></asp:Label>
											<asp:Label ID="lblUserCharNo" runat="server" Text='<%# string.Format("- {0}",Eval("USER_CHAR_NO")) %>' Visible='<%# GetMultiCharFlag(Eval("SITE_CD")) %>'></asp:Label><br />
											<asp:Label ID="lblConnectTypeNm2" runat="server" Text='<%# Eval("CONNECT_TYPE_NM") %>'></asp:Label><br />
											<asp:LinkButton ID="lnkSiteUseStatusMainte" runat="server" CausesValidation="false" CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
												CommandName="" OnCommand="lnkSiteUseStatusMainte_Command" Text="ﾗｲﾌﾞﾁｬｯﾄ認証" Visible='<%# Eval("SITE_USE_STATUS").ToString().Equals(ViCommConst.SiteUseStatus.GAME_ONLY) %>'></asp:LinkButton>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="ﾊﾝﾄﾞﾙ/ｶﾃｺﾞﾘ/ｺﾒﾝﾄ">
										<HeaderStyle HorizontalAlign="center" />
										<ItemTemplate>
											<asp:HyperLink ID="lnkCharacterMainte" runat="server" NavigateUrl='<%# string.Format("CastCharacterMainte.aspx?userseq={0}&sitecd={1}&usercharno={2}&return={3}",Eval("USER_SEQ"),Eval("SITE_CD"),Eval("USER_CHAR_NO"),ViewState["RETURN"].ToString()) %>'
												Text='<%# GetHandleNameLinkText(ViCommPrograms.DefHandleName(Eval("HANDLE_NM"))) %>'></asp:HyperLink><br />
											<asp:Label ID="Label4" runat="server" Text='<%# Eval("ACT_CATEGORY_NM") %>'></asp:Label><br />
											<asp:Label ID="Label5" runat="server" Text='<%# Eval("COMMENT_LIST") %>'></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="写真">
										<ItemTemplate>
											<asp:HyperLink ID="lnkPicMiante" runat="server" ImageUrl='<%# Eval("SMALL_PHOTO_IMG_PATH","../{0}") %>' NavigateUrl='<%# string.Format("CastPicMainte.aspx?userseq={0}&usercharno={1}&sitecd={2}&return={3}",Eval("USER_SEQ"),Eval("USER_CHAR_NO"),Eval("SITE_CD"),ViewState["RETURN"].ToString()) %>'
												BorderColor="#00C0C0" BorderStyle="Solid" BorderWidth="2px">[lnkPicMiante]</asp:HyperLink>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="履歴">
										<ItemTemplate>
											<asp:LinkButton ID="lnkUsedLog" runat="server" CausesValidation="false" CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
												CommandName="" OnCommand="lnkUsed_Command" Text="利用履歴"></asp:LinkButton>
											<br />
											<asp:LinkButton ID="lnkUsedLogTalk" runat="server" CausesValidation="false" CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
												CommandName="" OnCommand="lnkUsedTalk_Command" Text="会話履歴"></asp:LinkButton>
											<br />
											<asp:LinkButton ID="lnkPaymentHis" runat="server" CausesValidation="false" CommandName="" OnCommand="lnkPaymentHis_Command" Text="支払履歴"></asp:LinkButton>
											<br />
											<asp:LinkButton ID="lnkBonusPointHis" runat="server" CausesValidation="false" CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("USER_SEQ")) %>'
												CommandName="" OnCommand="lnkBonusPointHis_Command" Text="ﾎﾞｰﾅｽPT履歴"></asp:LinkButton>
											<br />
											<asp:LinkButton ID="lnkWithdrawalHis" runat="server" CausesValidation="false" CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("USER_SEQ")) %>'
												CommandName="" OnCommand="lnkWithdrawalHis_Command" Text="退会履歴"></asp:LinkButton>
										</ItemTemplate>
										<ItemStyle HorizontalAlign="Center" />
									</asp:TemplateField>
									<asp:TemplateField HeaderText="動画">
										<ItemTemplate>
											<asp:LinkButton ID="lnkProfileMovie" runat="server" CausesValidation="false" CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
												CommandName="" OnCommand="lnkProfileMovie_Command" Text="ﾌﾟﾛﾌ動画"></asp:LinkButton><br />
											<asp:LinkButton ID="lnkBbsMovie" runat="server" CausesValidation="false" CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
												CommandName="" OnCommand="lnkBbsMovie_Command" Text="掲示板動画"></asp:LinkButton><br />
											<asp:LinkButton ID="lnkTalkMovie" runat="server" CausesValidation="false" CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
												CommandName="" OnCommand="lnkTalkMovie_Command" Text="会話動画" Visible="<%# IsAdmin() %>"></asp:LinkButton>
										</ItemTemplate>
										<ItemStyle HorizontalAlign="Center" />
									</asp:TemplateField>
									<asp:TemplateField HeaderText="動画/画像">
										<HeaderStyle HorizontalAlign="center" />
										<ItemTemplate>
											<asp:LinkButton ID="lnkPublicMovie" runat="server" CausesValidation="false" CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
												CommandName="" OnCommand="lnkPublicMovie_Command" Text="販売動画" Visible="<%# EnableSaleMovieMainte() %>"></asp:LinkButton><br />
											<asp:LinkButton ID="lnkSaleVoice" runat="server" CausesValidation="false" CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
												CommandName="" OnCommand="lnkSaleVoice_Command" Text="販売着ボイス" Visible="<%# EnableSaleVoiceMainte() %>"></asp:LinkButton><br />
											<asp:LinkButton ID="lnkBbsPic" runat="server" CausesValidation="false" CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
												CommandName="" OnCommand="lnkBbsPic_Command" Text="掲示板画像"></asp:LinkButton><br />
										</ItemTemplate>
										<ItemStyle HorizontalAlign="Center" />
									</asp:TemplateField>
									<asp:TemplateField HeaderText="お気に入り">
										<ItemTemplate>
											<asp:LinkButton ID="lnkFavorit" runat="server" CausesValidation="false" CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
												CommandName="" Text="お気に入り" OnCommand="lnkFavorit_Command" Visible='<%#IsNewSite(Eval("USE_OTHER_SYS_INFO_FLAG")) %>'></asp:LinkButton><br />
											<asp:LinkButton ID="lnkLikeMe" runat="server" CausesValidation="false" CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
												CommandName="" Text="気に入られ" Visible='<%#IsNewSite(Eval("USE_OTHER_SYS_INFO_FLAG")) %>' OnCommand="lnkLikeMe_Command"></asp:LinkButton><br />
											<asp:LinkButton ID="lnkRefuse" runat="server" CausesValidation="false" CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
												CommandName="" Text="拒否している" Visible='<%#IsNewSite(Eval("USE_OTHER_SYS_INFO_FLAG")) %>' OnCommand="lnkRefuse_Command"></asp:LinkButton><br />
											<asp:LinkButton ID="lnkRefuseMe" runat="server" CausesValidation="false" CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
												CommandName="" Text="拒否されている" Visible='<%#IsNewSite(Eval("USE_OTHER_SYS_INFO_FLAG")) %>' OnCommand="lnkRefuseMe_Command"></asp:LinkButton><br />
											<asp:LinkButton ID="lnkMarking" runat="server" CausesValidation="false" CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
												CommandName="" OnCommand="lnkMarking_Command" Text="足あと" Visible='<%#IsNewSite(Eval("USE_OTHER_SYS_INFO_FLAG")) %>'></asp:LinkButton>
										</ItemTemplate>
										<ItemStyle HorizontalAlign="Center" />
									</asp:TemplateField>
									<asp:TemplateField HeaderText="メール">
										<ItemTemplate>
											<asp:LinkButton ID="lnkMailBox" runat="server" CausesValidation="false" CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
												CommandName="" Text="メールBOX" OnCommand="lnkMailBox_Command" Visible='<%#IsNewSite(Eval("USE_OTHER_SYS_INFO_FLAG")) %>'></asp:LinkButton><br />
											<asp:LinkButton ID="lnkMailTemplate" runat="server" CausesValidation="false" CommandArgument='<%#string.Format("{0}:{1}:{2}",Eval("SITE_CD"),Eval("USER_SEQ"),Eval("USER_CHAR_NO")) %>'
												CommandName="" OnCommand="lnkMailTemplate_Command" Text="ﾃﾝﾌﾟﾚｰﾄ" Visible='<%#IsNewSite(Eval("USE_OTHER_SYS_INFO_FLAG")) %>'></asp:LinkButton><br />
											<asp:LinkButton ID="lnkCreateMail" runat="server" CausesValidation="false" CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
												CommandName="" Text="メール作成" OnCommand="lnkCreateMail_Command"></asp:LinkButton><br />
											<asp:LinkButton ID="lnkEmail" runat="server" CausesValidation="false" CommandArgument='<%# string.Format("{0}",Eval("USER_SEQ")) %>' CommandName="" Text="変更履歴/送信NG"
												OnCommand="lnkModifyMailHis_Command" />
										</ItemTemplate>
										<ItemStyle HorizontalAlign="Center" />
									</asp:TemplateField>
									<asp:TemplateField HeaderText="書込み">
										<ItemTemplate>
											<asp:LinkButton ID="lnkDiary" runat="server" CausesValidation="false" CommandArgument='<%#string.Format("{0}:{1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
												CommandName="" Text="日記" Visible='<%#IsNewSite(Eval("USE_OTHER_SYS_INFO_FLAG")) %>' OnCommand="lnkDiary_Command"></asp:LinkButton>
											<br />
											<asp:LinkButton ID="lnkBbs" runat="server" CausesValidation="false" CommandArgument='<%#string.Format("{0}:{1}",Eval("SITE_CD"),Eval("USER_CHAR_NO")) %>'
												CommandName="" Text="掲示板" Visible='<%#IsNewSite(Eval("USE_OTHER_SYS_INFO_FLAG")) %>' OnCommand="lnkBbs_Command"></asp:LinkButton>
										</ItemTemplate>
										<ItemStyle HorizontalAlign="Center" />
									</asp:TemplateField>
									<asp:TemplateField HeaderText="利用" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
										<ItemTemplate>
											<asp:Label runat="server" Text='<%# Eval("NA_MARK") %>' ID="lblNaMark" />
											<br />
											<asp:LinkButton  ID="lnkFriends" runat="server" CausesValidation="false" CommandArgument='<%# string.Format("{0}:{1}:{2}",Eval("SITE_CD"),Eval("USER_CHAR_NO"),Eval("FRIEND_INTRO_CD")) %>'
												CommandName="" Text="お友達紹介" OnCommand="lnkFriends_Command" />
											<br />
											<asp:LinkButton ID="lnkBlogArticle" runat="server" CausesValidation="false" CommandArgument='<%# Container.DataItemIndex %>' Visible="true"
												CommandName="" Text='<%# GetBlogArticleMark(Container.DataItemIndex) %>' OnCommand="lnkBlogArticle_Command" />
											<br />
											<asp:LinkButton ID="lnkGameCharacter" runat="server" CausesValidation="false" CommandArgument='<%# Container.DataItemIndex %>' Visible="true"
												CommandName="" Text='<%# GetGameCharacterMark(Container.DataItemIndex) %>' OnCommand="lnkGameCharacter_Command" />
											<br />
											<asp:LinkButton ID="lnkFanClubStatus" runat="server" CausesValidation="false" CommandArgument='<%# Container.DataItemIndex %>'
												CommandName="" Text='<%# GetFanClubStatusEnableCount(Container.DataItemIndex) %>' OnCommand="lnkFanClubStatus_Command" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="男性/お知らせ">
										<ItemTemplate>
											<asp:Label ID="lblManMailRxTypeNm" runat="server" Text='<%# Eval("MAN_MAIL_RX_TYPE_NM") %>'></asp:Label><br />
											<asp:Label ID="lblInfoMailRxTypeNm" runat="server" Text='<%# Eval("INFO_MAIL_RX_TYPE_NM") %>'></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="ﾕｰｻﾞｰﾌﾗｸﾞ">
										<ItemTemplate>
											<asp:Label ID="lblUserDefineMask" runat="server" Text='<%# GetEnabledUserDefineFlagMark(Eval("USER_DEFINE_MASK")) %>'></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
								</Columns>
								<PagerSettings Mode="NumericFirstLast" />
							</asp:GridView>
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
							</asp:DropDownList>
							<asp:Button ID="btnRegist" runat="server" Text="キャラクター追加" OnClick="btnRegist_Click" />
							<asp:Button ID="btnRetrun" runat="server" Text="戻る" OnClick="btnReturn_Click" />
						</fieldset>
					</td>
				</tr>
			</asp:Panel>
		</table>
		<asp:Panel runat="server" ID="pnlDtl" HorizontalAlign="Left">
			<table border="0" style="margin-bottom: 0px">
				<asp:Panel runat="server" ID="pnlProfileMovie">
					<tr style="margin-top: 0px">
						<td valign="top">
							<div id="anchorProfileMovie">
							</div>
							<fieldset class="fieldset-inner">
								<legend>[プロフ動画一覧]</legend>
								<asp:Button ID="btnProfileCloseMovie" runat="server" Text="閉じる" OnClick="btnCloseProfileMovie_Click" />
								<asp:Button ID="btnProfileRegistMovie" runat="server" Text="アップロード" OnClick="btnRegistProfileMovie_Click" />
								<asp:Label ID="lblProfileMovieSite" runat="server" Font-Bold="True" Font-Underline="True" ForeColor="#FF8000"></asp:Label>
								<asp:Label ID="lblProfileMovieCharNo" runat="server"></asp:Label><br />
								<asp:GridView ID="grdProfileMovie" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCastMovie" AllowSorting="True" SkinID="GridViewColor">
									<Columns>
										<asp:BoundField DataField="MOVIE_TITLE" HeaderText="動画タイトル">
											<ItemStyle Width="250px" />
										</asp:BoundField>
										<asp:BoundField DataField="CHARGE_POINT" HeaderText="消費Pt">
											<ItemStyle HorizontalAlign="Right" />
										</asp:BoundField>
										<asp:BoundField DataField="UPLOAD_DATE" HeaderText="アップロード日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<%--<asp:BoundField DataField="NOT_APPROVE_MARK" HeaderText="認証">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>--%>
                                        <asp:TemplateField HeaderText="認証">
                                            <ItemTemplate>
                                                <asp:Label ID="lblApproveFlag" runat="server" Text='<%# Eval("NOT_APPROVE_MARK") %>'
                                                    Visible='<%# !ViComm.ViCommConst.FLAG_ON_STR.Equals(Eval("OBJ_NOT_APPROVE_FLAG","{0}")) %>'></asp:Label>
                                                <asp:Button ID="btnBbsMovieUpdate" runat="server" CommandArgument='<%# string.Format("{0},{1},{2},{3},1",Eval("SITE_CD"),Eval("USER_SEQ"),Eval("USER_CHAR_NO"),Eval("MOVIE_SEQ")) %>'
                                                    OnCommand="btnBbsMovieUpdate_OnCommand" Text="認証" OnClientClick="return confirm('設定を行いますか？');"
                                                    Visible='<%# ViComm.ViCommConst.FLAG_ON_STR.Equals(Eval("OBJ_NOT_APPROVE_FLAG","{0}")) %>'>
                                                </asp:Button>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center"/>
                                        </asp:TemplateField>
										<asp:TemplateField HeaderText="動画確認">
											<ItemStyle HorizontalAlign="Center" />
											<ItemTemplate>
												<a href="<%# PickupHelper.GeneratePickupObjsViewerUrl(this, Eval("SITE_CD").ToString(),Eval("MOVIE_SEQ").ToString(), ViComm.ViCommConst.PicupTypes.PROFILE_MOVIE) %>">
													確認 </a>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:HyperLinkField HeaderText="動画設定" Text="設定" DataNavigateUrlFields="SITE_CD,USER_SEQ,USER_CHAR_NO,MOVIE_SEQ" DataNavigateUrlFormatString="ProfileMovieMainte.aspx?sitecd={0}&amp;userseq={1}&amp;usercharno={2}&amp;movieseq={3}">
											<ItemStyle HorizontalAlign="Center" />
										</asp:HyperLinkField>
									</Columns>
									<PagerSettings Mode="NumericFirstLast" />
								</asp:GridView>
							</fieldset>
						</td>
					</tr>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlPublicMovie">
					<tr style="margin-top: 0px">
						<td valign="top">
							<div id="anchorPublicMovie">
							</div>
							<fieldset class="fieldset-inner">
								<legend>[販売動画一覧]</legend>
								<asp:Button ID="btnClosePublicMovie" runat="server" Text="閉じる" OnClick="btnClosePublicMovie_Click" />
								<asp:Button ID="btnRegistPublicMovie" runat="server" Text="販売動画追加" OnClick="btnRegistPublicMovie_Click" />
								<asp:Label ID="lblPublicMovieSite" runat="server" Font-Bold="True" Font-Underline="True" ForeColor="#FF8000"></asp:Label>
								<asp:Label ID="lblPublicMovieCharNo" runat="server"></asp:Label><br />
								<asp:GridView ID="grdPublicMovie" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsPublicMovie" AllowSorting="True" SkinID="GridViewColor">
									<Columns>
										<asp:TemplateField HeaderText="サムネイル">
											<ItemStyle HorizontalAlign="Center" />
											<ItemTemplate>
												<asp:Image ID="imgThumbnailPic" runat="server" Width="100px" ImageUrl='<%# GetThumbnailPicUrl(Eval("THUMBNAIL_PIC_SEQ"),Eval("SITE_CD"),Eval("LOGIN_ID")) %>'
													Visible='<%# !Eval("THUMBNAIL_PIC_SEQ").Equals(DBNull.Value)%>'>
												</asp:Image>
												<asp:Label ID="lblThumbnailPicNothingMsg" runat="server" Visible='<%# Eval("THUMBNAIL_PIC_SEQ").Equals(DBNull.Value) %>' Text="[未登録]"></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:BoundField DataField="MOVIE_TITLE" HeaderText="動画タイトル">
											<ItemStyle Width="250px" />
										</asp:BoundField>
										<asp:BoundField DataField="CHARGE_POINT" HeaderText="消費Pt">
											<ItemStyle HorizontalAlign="Right" />
										</asp:BoundField>
										<asp:BoundField DataField="UPLOAD_DATE" HeaderText="登録日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:BoundField DataField="NOT_APPROVE_MARK" HeaderText="認証">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:TemplateField HeaderText="サンプル">
											<ItemStyle HorizontalAlign="Center" />
											<ItemTemplate>
												<%# Eval("SAMPLE_MOVIE_SEQ").Equals(DBNull.Value)?"なし":"あり" %>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="動画確認">
											<ItemStyle HorizontalAlign="Center" />
											<ItemTemplate>
												<asp:LinkButton ID="lnkProfileQuickTime" runat="server" CommandArgument='<%# Eval("MOVIE_SEQ") %>' OnCommand="lnkProfileQuickTime_Command" Text="確認"></asp:LinkButton>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:HyperLinkField HeaderText="動画設定" Text="設定" DataNavigateUrlFields="SITE_CD,USER_SEQ,USER_CHAR_NO,MOVIE_SEQ,MOVIE_TYPE" DataNavigateUrlFormatString="SaleMovieMainte.aspx?sitecd={0}&amp;userseq={1}&amp;usercharno={2}&amp;movieseq={3}&amp;movietype={4}">
											<ItemStyle HorizontalAlign="Center" />
										</asp:HyperLinkField>
									</Columns>
									<PagerSettings Mode="NumericFirstLast" />
								</asp:GridView>
							</fieldset>
						</td>
					</tr>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlSaleVoice">
					<tr style="margin-top: 0px">
						<td valign="top">
							<div id="anchorSaleVoice">
							</div>
							<fieldset class="fieldset-inner">
								<legend>[販売着ボイス一覧]</legend>
								<asp:Button ID="btnCloseSaleVoice" runat="server" Text="閉じる" OnClick="btnCloseSaleVoice_Click" />
								<asp:Button ID="btnRegistSaleVoice" runat="server" Text="販売着ボイス追加" OnClick="btnRegistSaleVoice_Click" />
								<asp:Label ID="lblSaleVoiceSite" runat="server" Font-Bold="True" Font-Underline="True" ForeColor="#FF8000"></asp:Label>
								<asp:Label ID="lblSaleVoiceCharNo" runat="server"></asp:Label><br />
								<asp:GridView ID="grdSaleVoice" runat="server" DataSourceID="dsCastVoice" AllowPaging="True" AutoGenerateColumns="False" AllowSorting="True" SkinID="GridViewColor">
									<Columns>
										<asp:BoundField DataField="VOICE_TITLE" HeaderText="着ボイスタイトル">
											<ItemStyle Width="250px" />
										</asp:BoundField>
										<asp:BoundField DataField="CHARGE_POINT" HeaderText="消費Pt">
											<ItemStyle HorizontalAlign="Right" />
										</asp:BoundField>
										<asp:BoundField DataField="UPLOAD_DATE" HeaderText="登録日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:BoundField DataField="NOT_APPROVE_MARK" HeaderText="認証">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:TemplateField HeaderText="ボイス設定">
											<ItemStyle Width="60px" HorizontalAlign="Center" />
											<ItemTemplate>
												<a href='<%# GetVoiceMainteUrl(Eval("VOICE_SEQ"),Eval("VOICE_TYPE")) %>'>設定</a>
											</ItemTemplate>
										</asp:TemplateField>
									</Columns>
									<PagerSettings Mode="NumericFirstLast" />
								</asp:GridView>
							</fieldset>
						</td>
					</tr>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlBbsMovie">
					<tr style="margin-top: 0px">
						<td>
							<div id="anchorBbsMovie">
							</div>
							<fieldset class="fieldset-inner">
								<legend>[掲示板動画一覧]</legend>
								<asp:Button ID="btnBbsCloseMovie" runat="server" Text="閉じる" OnClick="btnCloseBbsMovie_Click" />
								<asp:Button ID="btnBbsRegistMovie" runat="server" Text="アップロード" OnClick="btnRegistBbsMovie_Click" />
								<asp:Label ID="lblBbsMovieSite" runat="server" Font-Bold="True" Font-Underline="True" ForeColor="#FF8000"></asp:Label>
								<asp:Label ID="lblBbsMovieCharNo" runat="server"></asp:Label><br />
								<asp:GridView ID="grdBbsMovie" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCastBbsMovie" AllowSorting="True" SkinID="GridViewColor">
									<Columns>
										<asp:BoundField DataField="MOVIE_TITLE" HeaderText="動画タイトル">
											<ItemStyle Width="250px" />
										</asp:BoundField>
										<asp:BoundField DataField="MOVIE_DOC" HeaderText="動画本文">
											<ItemStyle Width="250px" />
										</asp:BoundField>
										<asp:BoundField DataField="CHARGE_POINT" HeaderText="消費Pt">
											<ItemStyle HorizontalAlign="Right" />
										</asp:BoundField>
										<asp:BoundField DataField="UPLOAD_DATE" HeaderText="アップロード日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:BoundField DataField="CAST_MOVIE_ATTR_TYPE_NM" HeaderText="属性" />
										<asp:BoundField DataField="CAST_MOVIE_ATTR_NM" HeaderText="属性値" />
                                        <asp:TemplateField HeaderText="認証">
                                            <ItemTemplate>
                                                <asp:Label ID="lblApproveFlag" runat="server" Text='<%# Eval("NOT_APPROVE_MARK") %>'
                                                    Visible='<%# !ViComm.ViCommConst.FLAG_ON_STR.Equals(Eval("OBJ_NOT_APPROVE_FLAG","{0}")) %>'></asp:Label>
                                                <asp:Button ID="btnBbsMovieUpdate" runat="server" CommandArgument='<%# string.Format("{0},{1},{2},{3},2",Eval("SITE_CD"),Eval("USER_SEQ"),Eval("USER_CHAR_NO"),Eval("MOVIE_SEQ")) %>'
                                                    OnCommand="btnBbsMovieUpdate_OnCommand" Text="認証" OnClientClick="return confirm('設定を行いますか？');"
                                                    Visible='<%# ViComm.ViCommConst.FLAG_ON_STR.Equals(Eval("OBJ_NOT_APPROVE_FLAG","{0}")) %>'>
                                                </asp:Button>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center"/>
                                        </asp:TemplateField>
										<%--<asp:BoundField DataField="NOT_APPROVE_MARK" HeaderText="認証">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>--%>
										<asp:TemplateField HeaderText="動画確認">
											<ItemStyle HorizontalAlign="Center" />
											<ItemTemplate>
												<a href="<%# PickupHelper.GeneratePickupObjsViewerUrl(this, Eval("SITE_CD").ToString(),Eval("MOVIE_SEQ").ToString(), ViComm.ViCommConst.PicupTypes.BBS_MOVIE) %>">
													確認 </a>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:HyperLinkField HeaderText="動画設定" Text="設定" DataNavigateUrlFields="SITE_CD,USER_SEQ,USER_CHAR_NO,MOVIE_SEQ" DataNavigateUrlFormatString="../CastAdmin/BbsMovieMainte.aspx?sitecd={0}&userseq={1}&usercharno={2}&movieseq={3}">
											<ItemStyle HorizontalAlign="Center" />
										</asp:HyperLinkField>
									</Columns>
									<PagerSettings Mode="NumericFirstLast" />
								</asp:GridView>
							</fieldset>
						</td>
					</tr>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlBbs">
					<tr style="margin-top: 0px">
						<td>
							<div id="anchorBbs">
							</div>
							<fieldset class="fieldset-inner">
								<legend>[掲示板一覧]</legend>
								<asp:Button ID="btnBbsClose" runat="server" Text="閉じる" OnClick="btnCloseBbs_Click" />
								<asp:Label ID="lblBbsSite" runat="server" Font-Bold="True" Font-Underline="True" ForeColor="#FF8000"></asp:Label>
								<asp:Label ID="lblBbsCharNo" runat="server"></asp:Label><br />
								<asp:GridView ID="grdBbs" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsBbsLog" AllowSorting="True" SkinID="GridViewColor"
									OnRowDataBound="grdBbs_RowDataBound">
									<Columns>
										<asp:TemplateField HeaderText="書込日時">
											<ItemTemplate>
											    $NO_TRANS_START;
												<asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("CREATE_DATE")%>'></asp:Label><br />
												<asp:LinkButton ID="lnkDel" runat="server" CommandArgument='<%#string.Format("{0},{1},{2}",Eval("BBS_SEQ"),Eval("SEX_CD"),1) %>' Text="削除" Visible='<%#!iBridCommLib.iBridUtil.GetStringValue(Eval("DEL_FLAG")).Equals(ViComm.ViCommConst.FLAG_ON_STR)%>'
													OnCommand="lnkDelBbs_Command" OnClientClick="return confirm('削除を実行しますか？');"></asp:LinkButton>
												<asp:LinkButton ID="lnkRetrieve" runat="server" CommandArgument='<%#string.Format("{0},{1},{2}",Eval("BBS_SEQ"),Eval("SEX_CD"),0) %>' Text="復活" Visible='<%#!iBridCommLib.iBridUtil.GetStringValue(Eval("DEL_FLAG")).Equals(ViComm.ViCommConst.FLAG_OFF_STR)%>'
													OnCommand="lnkDelBbs_Command"></asp:LinkButton><br />
												$NO_TRANS_END;
											</ItemTemplate>
											<ItemStyle VerticalAlign="Top" />
										</asp:TemplateField>
										<asp:TemplateField HeaderText="内容">
											<ItemTemplate>
												<asp:Label ID="lblMailTitle" runat="server" Text='<%# Eval("BBS_TITLE") %>'></asp:Label><br />
												<asp:Label ID="lblDoc" runat="server" Text='<%# Eval("BBS_DOC") %>' Width="500px"></asp:Label>
											</ItemTemplate>
											<ItemStyle VerticalAlign="Top" />
										</asp:TemplateField>
										<asp:TemplateField HeaderText="削除">
											<ItemTemplate>
												<%# GetDelFlag(Eval("DEL_FLAG"),Eval("ADMIN_DEL_FLAG")) %>
											</ItemTemplate>
											<ItemStyle VerticalAlign="Top" HorizontalAlign="Center" />
										</asp:TemplateField>
									</Columns>
									<PagerSettings Mode="NumericFirstLast" />
								</asp:GridView>
							</fieldset>
						</td>
					</tr>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlTalkMovie">
					<tr style="margin-top: 0px">
						<td>
							<div id="anchorTalkMovie">
							</div>
							<fieldset class="fieldset-inner">
								<legend>[会話動画一覧]</legend>
								<asp:Button ID="btnTalkCloseMovie" runat="server" Text="閉じる" OnClick="btnCloseTalkMovie_Click" />
								<asp:Label ID="lblTalkMovieSite" runat="server" Font-Bold="True" Font-Underline="True" ForeColor="#FF8000"></asp:Label>
								<asp:Label ID="lblTalkMovieCharNo" runat="server"></asp:Label><br />
								<asp:GridView ID="grdTalkMovie" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCharacter" AllowSorting="True" SkinID="GridViewColor">
									<Columns>
										<asp:BoundField DataField="MOVIE_TITLE" HeaderText="動画タイトル">
											<ItemStyle Width="250px" />
										</asp:BoundField>
										<asp:BoundField DataField="CHARGE_POINT" HeaderText="消費Pt">
											<ItemStyle HorizontalAlign="Right" />
										</asp:BoundField>
										<asp:BoundField DataField="UPLOAD_DATE" HeaderText="アップロード日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:BoundField DataField="RECORDING_SEC" HeaderText="再生秒数">
											<ItemStyle HorizontalAlign="Right" />
										</asp:BoundField>
                                        <asp:TemplateField HeaderText="認証">
                                            <ItemTemplate>
                                                <asp:Label ID="lblApproveFlag" runat="server" Text='<%# Eval("NOT_APPROVE_MARK") %>'
                                                    Visible='<%# !ViComm.ViCommConst.FLAG_ON_STR.Equals(Eval("OBJ_NOT_APPROVE_FLAG","{0}")) %>'></asp:Label>
                                                <asp:Button ID="btnBbsMovieUpdate" runat="server" CommandArgument='<%# string.Format("{0},{1},{2},{3},0",Eval("SITE_CD"),Eval("USER_SEQ"),Eval("USER_CHAR_NO"),Eval("MOVIE_SEQ")) %>'
                                                    OnCommand="btnBbsMovieUpdate_OnCommand" Text="認証" OnClientClick="return confirm('設定を行いますか？');"
                                                    Visible='<%# ViComm.ViCommConst.FLAG_ON_STR.Equals(Eval("OBJ_NOT_APPROVE_FLAG","{0}")) %>'>
                                                </asp:Button>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center"/>
                                        </asp:TemplateField>
										<%--<asp:BoundField DataField="NOT_APPROVE_MARK" HeaderText="認証">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>--%>
										<asp:TemplateField HeaderText="動画確認">
											<ItemStyle HorizontalAlign="Center" />
											<ItemTemplate>
												<asp:LinkButton ID="lnkTalkQuickTime" runat="server" CommandArgument='<%# Eval("PLAY_FILE_NM") %>' OnCommand="lnkTalkQuickTime_Command" Text="確認"></asp:LinkButton>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:HyperLinkField HeaderText="動画設定" Text="設定" DataNavigateUrlFields="SITE_CD,USER_SEQ,USER_CHAR_NO,MOVIE_SEQ" DataNavigateUrlFormatString="../CastAdmin/TalkMovieMainte.aspx?sitecd={0}&amp;userseq={1}&amp;usercharno={2}&amp;movieseq={3}">
											<ItemStyle HorizontalAlign="Center" />
										</asp:HyperLinkField>
									</Columns>
									<PagerSettings Mode="NumericFirstLast" />
								</asp:GridView>
							</fieldset>
						</td>
					</tr>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlBbsPic">
					<tr style="margin-top: 0px">
						<td>
							<div id="anchorBbsPic">
							</div>
							<fieldset class="fieldset-inner">
								<legend>[掲示板画像一覧]</legend>
								<asp:Button ID="btnBbsClosePic" runat="server" Text="閉じる" OnClick="btnCloseBbsPic_Click" />
								<asp:Button ID="btnBbsRegistPic" runat="server" Text="アップロード" OnClick="btnRegistBbsPic_Click" />
								<asp:Label ID="lblBbsPicSite" runat="server" Font-Bold="True" Font-Underline="True" ForeColor="#FF8000"></asp:Label>
								<asp:Label ID="lblBbsPicCharNo" runat="server"></asp:Label><br />
								<asp:GridView ID="grdBbsPic" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCastBbsPic" AllowSorting="True" SkinID="GridViewColor">
									<Columns>
										<asp:TemplateField HeaderText="写真">
											<ItemStyle HorizontalAlign="Center" />
											<ItemTemplate>
												<a href="<%# PickupHelper.GeneratePickupObjsViewerUrl(this, Eval("SITE_CD").ToString(),Eval("PIC_SEQ").ToString(), ViComm.ViCommConst.PicupTypes.BBS_PIC) %>">
													<asp:Image ID="imgPicSmall" runat="server" ImageUrl='<%# Eval("OBJ_SMALL_PHOTO_IMG_PATH","../{0}") %>' BorderStyle="none" BorderWidth="0px">
													</asp:Image>
												</a>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:BoundField DataField="PIC_TITLE" HeaderText="画像タイトル" HtmlEncode="False">
											<ItemStyle Width="250px" />
										</asp:BoundField>
										<asp:BoundField DataField="PIC_DOC" HeaderText="画像本文">
											<ItemStyle Width="250px" />
										</asp:BoundField>
										<asp:BoundField DataField="UPLOAD_DATE" HeaderText="アップロード日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:BoundField DataField="CAST_PIC_ATTR_TYPE_NM" HeaderText="属性" />
										<asp:BoundField DataField="CAST_PIC_ATTR_NM" HeaderText="属性値" />
                                        <asp:TemplateField HeaderText="認証">
                                            <ItemTemplate>
                                                <asp:Label ID="lblApproveFlag" runat="server" Text='<%# Eval("NOT_APPROVE_MARK") %>'
                                                    Visible='<%# !ViComm.ViCommConst.FLAG_ON_STR.Equals(Eval("OBJ_NOT_APPROVE_FLAG","{0}")) %>'></asp:Label>
                                                <asp:Button ID="btnBbsPicUpdate" runat="server" CommandArgument='<%# string.Format("{0},{1},{2},{3},2",Eval("SITE_CD"),Eval("USER_SEQ"),Eval("USER_CHAR_NO"),Eval("PIC_SEQ")) %>'
                                                    OnCommand="btnBbsPicUpdate_OnCommand" Text="認証" OnClientClick="return confirm('設定を行いますか？');"
                                                    Visible='<%# ViComm.ViCommConst.FLAG_ON_STR.Equals(Eval("OBJ_NOT_APPROVE_FLAG","{0}")) %>'>
                                                </asp:Button>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
										<%--<asp:BoundField DataField="NOT_APPROVE_MARK" HeaderText="認証">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>--%>
										<asp:HyperLinkField HeaderText="画像設定" Text="設定" DataNavigateUrlFields="SITE_CD,USER_SEQ,USER_CHAR_NO,PIC_SEQ" DataNavigateUrlFormatString="../CastAdmin/BbsPicMainte.aspx?sitecd={0}&userseq={1}&usercharno={2}&picseq={3}&return=CastView">
											<ItemStyle HorizontalAlign="Center" />
										</asp:HyperLinkField>
									</Columns>
									<PagerSettings Mode="NumericFirstLast" />
								</asp:GridView>
							</fieldset>
						</td>
					</tr>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlBonusLog" Visible="false">
					<tr style="margin-top: 0px">
						<td>
							<div id="anchorBonusLog">
							</div>
							<fieldset class="fieldset">
								<legend>[ボーナス報酬履歴]</legend>
								<asp:GridView ID="grdBonusLog" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsBonusLog" AllowSorting="True" SkinID="GridViewColor">
									<Columns>
										<asp:BoundField DataField="CREATE_DATE" HeaderText="ボーナス報酬追加日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:BoundField DataField="BONUS_POINT" HeaderText="追加Pt">
											<ItemStyle HorizontalAlign="Right" />
										</asp:BoundField>
										<asp:BoundField DataField="REMARKS" HeaderText="備考"></asp:BoundField>
										<asp:BoundField DataField="PAYMENT_FLAG" HeaderText="精算"></asp:BoundField>
										<asp:TemplateField HeaderText="削除">
											<ItemTemplate>
												<asp:LinkButton ID="lnkCancelBonusLog" runat="server" CausesValidation="False" CommandArgument='<%#Eval("BONUS_SEQ")%>' OnCommand="lnkCancelBonusLog_Command"
													Text="削除" OnClientClick="return confirm('ボーナス履歴を削除しますか？');" Visible="false"></asp:LinkButton>
											</ItemTemplate>
										</asp:TemplateField>
									</Columns>
									<PagerSettings Mode="NumericFirstLast" />
								</asp:GridView>
								<asp:Button runat="server" ID="btnCloseExt1" Text="閉じる" CssClass="seekbutton" OnClick="btnClose_Ext_Click" />
							</fieldset>
						</td>
					</tr>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlPaymentHistory" Visible="false">
					<tr style="margin-top: 0px">
						<td valign="top">
							<div id="anchorPaymentHis">
							</div>
							<fieldset class="fieldset-inner">
								<legend>[支払履歴]</legend>
								<asp:GridView ID="grdPaymentHistory" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsPaymentHistory" AllowSorting="True"
									SkinID="GridViewColor">
									<Columns>
										<asp:BoundField DataField="PAYMENT_DATE" HeaderText="支払日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:BoundField DataField="PAYMENT_AMT" HeaderText="支払金額">
											<ItemStyle HorizontalAlign="Right" />
										</asp:BoundField>
									</Columns>
									<PagerSettings Mode="NumericFirstLast" />
								</asp:GridView>
								<asp:Button runat="server" ID="btnClosePaymentHistory" Text="閉じる" CssClass="seekbutton" OnClick="btnClosePaymentHistory_Click" />
							</fieldset>
						</td>
					</tr>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlUserDefPointHistory" Visible="false">
					<tr style="margin-top: 0px">
						<td valign="top">
							<div id="anchorUserDefPointHis">
							</div>
							<fieldset class="fieldset-inner">
								<legend>[定義ﾎﾟｲﾝﾄ履歴]</legend>
								<asp:GridView ID="grdUserDefPointHistory" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsUserDefPointHistory" AllowSorting="True"
									SkinID="GridViewColor">
									<Columns>
										<asp:BoundField DataField="ADD_POINT_ID" HeaderText="ﾎﾟｲﾝﾄ追加識別">
											<ItemStyle HorizontalAlign="Left" />
										</asp:BoundField>
										<asp:BoundField DataField="ADD_POINT" HeaderText="増減ﾎﾟｲﾝﾄ">
											<ItemStyle HorizontalAlign="Right" />
										</asp:BoundField>
										<asp:TemplateField HeaderText="追加日時">
											<ItemTemplate>
												<asp:Label runat="server" Text='<%# Eval("ADD_POINT_DATE", "{0:yy/MM/dd HH:mm:ss}") %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
									</Columns>
									<PagerSettings Mode="NumericFirstLast" />
								</asp:GridView>
								<asp:Button runat="server" ID="btnCloseUserDefPointHistory" Text="閉じる" CssClass="seekbutton" OnClick="btnCloseUserDefPointHistory_Click" />
							</fieldset>
						</td>
					</tr>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlUsedLogParent">
					<tr style="margin-top: 0px">
						<td valign="top" colspan="2">
							<fieldset class="fieldset-inner">
								<legend>[利用履歴]</legend>
								<table border="0" class="SmallTableStyle">
									<tr>
										<td class="tdHeaderSmallStyle">
											相手ＩＤ
										</td>
										<td class="tdDataStyle" align="center">
											<asp:TextBox ID="txtManLoginId" runat="server" Width="70px" MaxLength="8"></asp:TextBox>
										</td>
									</tr>
								</table>
								<table>
									<tr valign="top">
										<td>
											<asp:GridView ID="grdWebUsedLogSummary" runat="server" AutoGenerateColumns="False" DataSourceID="dsWebUsedLogSummary" SkinID="GridViewColor">
												<Columns>
													<asp:BoundField DataField="CHARGE_TYPE_NM" HeaderText="種別" HtmlEncode="False">
														<ItemStyle HorizontalAlign="Left" />
													</asp:BoundField>
													<asp:BoundField DataField="SUMMARY" DataFormatString="{0:#,##0}" HeaderText="報酬ポイント計(WEB)" HtmlEncode="False">
														<ItemStyle HorizontalAlign="Right" />
													</asp:BoundField>
												</Columns>
											</asp:GridView>
										</td>
									</tr>
								</table>
								<asp:Button runat="server" ID="btnSeekUsedLog" Text="検索" CssClass="seekbutton" OnClick="btnSeekUsedLog_Click" />
								<asp:Button runat="server" ID="btnCloseUsedLogParent" Text="閉じる" CssClass="seekbutton" OnClick="btnCloseUsedLogParent_Click" />
							</fieldset>
						</td>
					</tr>
					<tr>
						<td valign="top">
							<asp:Panel runat="server" ID="pnlWebUsedLog">
								<div id="anchorWebUsedLog">
								</div>
								<fieldset class="fieldset-inner">
									<legend>[利用明細]</legend>
									<asp:Label ID="lblWebUsedLogSite" runat="server" Font-Bold="True" Font-Underline="True" ForeColor="#FF8000"></asp:Label>
									<asp:Label ID="lblWebUsedLogCharNo" runat="server"></asp:Label>
									<asp:Label ID="lblWebFromDay" runat="server" />&nbsp;〜
									<asp:Label ID="lblWebToDay" runat="server" />
									<asp:GridView ID="grdWebUsedLog" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsWebUsedLog" AllowSorting="True" Font-Size="x-Small"
										SkinID="GridViewColor">
										<Columns>
											<asp:TemplateField HeaderText="利用日時">
												<ItemTemplate>
													<asp:Label ID="lblChargeStartDate" runat="server" Text='<%# Eval("CHARGE_START_DATE", "{0:yy/MM/dd HH:mm:ss}") %>'></asp:Label>&nbsp;
												</ItemTemplate>
												<ItemStyle HorizontalAlign="Center" />
											</asp:TemplateField>
											<asp:BoundField DataField="CHARGE_TYPE_NM" HeaderText="種別">
												<ItemStyle HorizontalAlign="Left" />
											</asp:BoundField>
											<asp:TemplateField HeaderText="ハンドル名">
												<ItemStyle HorizontalAlign="Left" />
												<ItemTemplate>
													<asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%# string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",Eval("MAN_USER_SITE_CD"),Eval("MAN_LOGIN_ID")) %>'
														Text='<%# ViCommPrograms.DefHandleName(Eval("MAN_HANDLE_NM")) %>' Enabled='<% #IsAdmin() %>'></asp:HyperLink>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="会員ID">
												<ItemTemplate>
													<asp:Label ID="lblManLoginId" runat="server" Text='<%# Eval("MAN_LOGIN_ID") %>'></asp:Label>
												</ItemTemplate>
												<ItemStyle HorizontalAlign="Center" />
											</asp:TemplateField>
											<asp:BoundField DataField="PAYMENT_POINT" HeaderText="報酬Pt">
												<ItemStyle HorizontalAlign="Right" />
											</asp:BoundField>
										</Columns>
										<PagerSettings Mode="NumericFirstLast" />
									</asp:GridView>
								</fieldset>
							</asp:Panel>
						</td>
					</tr>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlUsedLogTalkParent">
					<tr style="margin-top: 0px">
						<td valign="top" colspan="2">
							<fieldset class="fieldset-inner">
								<legend>[会話履歴]</legend>
								<table border="0" class="SmallTableStyle">
									<tr>
										<td class="tdHeaderSmallStyle">
											相手ＩＤ
										</td>
										<td class="tdDataStyle" align="center">
											<asp:TextBox ID="txtManLoginIdTalk" runat="server" Width="70px" MaxLength="8"></asp:TextBox>
										</td>
									</tr>
								</table>
								<table>
									<tr valign="top">
										<td>
											<asp:GridView ID="grdTalkHistorySummary" runat="server" AutoGenerateColumns="False" DataSourceID="dsUsedLogTalkSummary" SkinID="GridViewColor">
												<Columns>
													<asp:BoundField DataField="CHARGE_TYPE_NM" HeaderText="会話種別" HtmlEncode="False">
														<ItemStyle HorizontalAlign="Left" />
													</asp:BoundField>
													<asp:BoundField DataField="SUMMARY_PAYMENT" DataFormatString="{0:#,##0}" HeaderText="報酬ポイント計(会話)" HtmlEncode="False">
														<ItemStyle HorizontalAlign="Right" />
													</asp:BoundField>
												</Columns>
											</asp:GridView>
										</td>
										<td>
											<asp:GridView ID="grdUsedLogMonitorSummary" runat="server" AutoGenerateColumns="False" DataSourceID="dsUsedLogMonitorSummary" SkinID="GridViewColor">
												<Columns>
													<asp:BoundField DataField="CHARGE_TYPE_NM" HeaderText="モニタ種別" HtmlEncode="False">
														<ItemStyle HorizontalAlign="Left" />
													</asp:BoundField>
													<asp:BoundField DataField="SUMMARY_PAYMENT" DataFormatString="{0:#,##0}" HeaderText="モニタ報酬計" HtmlEncode="False">
														<ItemStyle HorizontalAlign="Right" />
													</asp:BoundField>
												</Columns>
											</asp:GridView>
										</td>
									</tr>
								</table>
								<asp:Button runat="server" ID="btnSeekUsedLogTalk" Text="検索" CssClass="seekbutton" OnClick="btnSeekUsedLogTalk_Click" />
								<asp:Button runat="server" ID="btnCloseUsedLogTalkParent" Text="閉じる" CssClass="seekbutton" OnClick="btnCloseUsedLogTalkParent_Click" />
							</fieldset>
						</td>
					</tr>
					<tr>
						<td valign="top">
							<asp:Panel runat="server" ID="pnlUsedLog">
								<div id="anchorUsedLog">
								</div>
								<fieldset class="fieldset-inner">
									<legend>[会話明細]</legend>
									<asp:Label ID="lblUsedLogSite" runat="server" Font-Bold="True" Font-Underline="True" ForeColor="#FF8000"></asp:Label>
									<asp:Label ID="lblUsedLogCharNo" runat="server"></asp:Label>
									<asp:Label ID="lblFromDay" runat="server" />&nbsp;〜
									<asp:Label ID="lblToDay" runat="server" />
									<asp:GridView ID="grdTalkHistory" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsUsedLogTalk" AllowSorting="True" Font-Size="x-Small"
										SkinID="GridViewColor">
										<Columns>
											<asp:TemplateField HeaderText="会話日時">
												<ItemTemplate>
													<asp:Label ID="Label3" runat="server" Text='<%# Eval("CHARGE_START_DATE", "{0:yy/MM/dd HH:mm:ss}") %>'></asp:Label>&nbsp;
													<asp:Label ID="Label2" runat="server" Text='<%# Eval("CHARGE_END_DATE", "{0:HH:mm:ss}") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:BoundField DataField="CHARGE_TYPE_NM" HeaderText="会話種別">
												<ItemStyle HorizontalAlign="Left" />
											</asp:BoundField>
											<asp:TemplateField HeaderText="ハンドル名">
												<ItemStyle HorizontalAlign="Left" />
												<ItemTemplate>
													<asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%# string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",Eval("MAN_USER_SITE_CD"),Eval("MAN_LOGIN_ID")) %>'
														Text='<%# ViCommPrograms.DefHandleName(Eval("MAN_HANDLE_NM")) %>' Enabled='<% #IsAdmin() %>'></asp:HyperLink>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="会員ID">
												<ItemTemplate>
													<asp:Label ID="lblManLoginId" runat="server" Text='<%# Eval("MAN_LOGIN_ID") %>'></asp:Label>
												</ItemTemplate>
												<ItemStyle HorizontalAlign="Center" />
											</asp:TemplateField>
											<asp:BoundField DataField="CHARGE_SEC" HeaderText="秒数">
												<ItemStyle HorizontalAlign="Right" />
											</asp:BoundField>
											<asp:BoundField DataField="PAYMENT_POINT" HeaderText="報酬Pt" DataFormatString="{0:#,##0}">
												<ItemStyle HorizontalAlign="Right" />
											</asp:BoundField>
											<asp:BoundField DataField="INVITE_TALK_SERVICE_MIN" HeaderText="招待分">
												<ItemStyle HorizontalAlign="Right" />
											</asp:BoundField>
											<asp:TemplateField HeaderText="Free">
												<ItemTemplate>
													<asp:Literal ID="lblUsedFree" runat="server" Text='<%# (Eval("USE_FREE_DIAL_FLAG").ToString().Equals("1") &&  Eval("CHARGE_TYPE").ToString().CompareTo(ViCommConst.CHARGE_CAST_ACTION)>=0) ?"○":""  %>'></asp:Literal>
												</ItemTemplate>
												<ItemStyle HorizontalAlign="Center" />
											</asp:TemplateField>
											<asp:BoundField DataField="CALL_RESULT_NM" HeaderText="通話終了理由" HtmlEncode="False" SortExpression="CALL_RESULT_NM">
												<ItemStyle HorizontalAlign="Left" />
												<FooterStyle HorizontalAlign="Center" />
											</asp:BoundField>
										</Columns>
										<PagerSettings Mode="NumericFirstLast" />
									</asp:GridView>
									<br />
									<asp:GridView ID="grdUsedLogMonitor" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="dsUsedLogMonitor"
										Font-Size="x-Small" PageSize="10" SkinID="GridViewColor">
										<Columns>
											<asp:TemplateField HeaderText="モニタ日時">
												<ItemTemplate>
													<asp:Label ID="lblTalkStartDate" runat="server" Text='<%# Eval("CHARGE_START_DATE", "{0:yy/MM/dd HH:mm:ss}") %>'></asp:Label>&nbsp;
													<asp:Label ID="lblTalkEndDate" runat="server" Text='<%# Eval("CHARGE_END_DATE", "{0:HH:mm:ss}") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:BoundField DataField="CHARGE_TYPE_NM" HeaderText="モニタ種別">
												<ItemStyle HorizontalAlign="Left" />
											</asp:BoundField>
											<asp:TemplateField HeaderText="ハンドル名">
												<ItemStyle HorizontalAlign="Left" />
												<ItemTemplate>
													<asp:HyperLink ID="lnkHandleNm" runat="server" NavigateUrl='<%# string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",Eval("MAN_USER_SITE_CD"),Eval("MAN_LOGIN_ID")) %>'
														Text='<%# ViCommPrograms.DefHandleName(Eval("MAN_HANDLE_NM")) %>' Enabled='<% #IsAdmin() %>'></asp:HyperLink>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="会員ID">
												<ItemTemplate>
													<asp:Label ID="lblManLoginId" runat="server" Text='<%# Eval("MAN_LOGIN_ID") %>'></asp:Label>
												</ItemTemplate>
												<ItemStyle HorizontalAlign="Center" />
											</asp:TemplateField>
											<asp:BoundField DataField="CHARGE_SEC" HeaderText="秒数">
												<ItemStyle HorizontalAlign="Right" />
											</asp:BoundField>
											<asp:BoundField DataField="PAYMENT_POINT" HeaderText="報酬Pt">
												<ItemStyle HorizontalAlign="Right" />
											</asp:BoundField>
										</Columns>
										<PagerSettings Mode="NumericFirstLast" />
									</asp:GridView>
								</fieldset>
							</asp:Panel>
						</td>
					</tr>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlFavorit">
					<tr style="margin-top: 0px">
						<td>
							<div id="anchorFavorit">
							</div>
							<fieldset class="fieldset-inner">
								<legend>[お気に入り一覧]</legend>
								<asp:Button ID="btnFavoritCloseMovie" runat="server" Text="閉じる" OnClick="btnCloseFavorit_Click" />
								<asp:Label ID="lblFavoritSite" runat="server" Font-Bold="True" Font-Underline="True" ForeColor="#FF8000"></asp:Label>
								<asp:Label ID="lblFavoritCharNo" runat="server"></asp:Label><br />
								<asp:GridView ID="grdFavorit" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCastFavorit" AllowSorting="True" SkinID="GridViewColor">
									<Columns>
										<asp:BoundField DataField="FAVORIT_REGIST_DATE" HeaderText="お気に入り登録日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:TemplateField HeaderText="ハンドル名">
											<ItemStyle HorizontalAlign="Left" />
											<ItemTemplate>
												<asp:HyperLink ID="lnkManView" runat="server" NavigateUrl='<%# string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>'
													Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>' Enabled='<% #IsAdmin() %>'></asp:HyperLink>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:BoundField DataField="LAST_TALK_DATE" HeaderText="最終会話日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:BoundField DataField="TALK_COUNT" HeaderText="会話数">
											<ItemStyle HorizontalAlign="Right" />
										</asp:BoundField>
										<asp:BoundField DataField="LAST_LOGIN_DATE" HeaderText="最終ﾛｸﾞｲﾝ日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:TemplateField HeaderText="状態">
											<ItemTemplate>
												<asp:Image ID="Image2" runat="server" ImageUrl='<%# GetSysImage(Eval("CHARACTER_ONLINE_STATUS") )%>' />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="お気に入られ">
											<ItemTemplate>
												<asp:Label ID="lblLikeMe" runat="server" Text='<%# GetLikeMeMark(Eval("USER_SEQ"), Eval("USER_CHAR_NO"), Eval("PARTNER_USER_SEQ"), Eval("PARTNER_USER_CHAR_NO")) %>' />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="拒否されている">
											<ItemTemplate>
												<asp:Label ID="lblRefuseMe" runat="server" Text='<%# GetRefuseMeMark(Eval("USER_SEQ"), Eval("USER_CHAR_NO"), Eval("PARTNER_USER_SEQ"), Eval("PARTNER_USER_CHAR_NO")) %>' />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="削除">
											<ItemTemplate>
												<asp:LinkButton ID="lnkFavoritDelete" runat="server" CausesValidation="False" CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("PARTNER_USER_SEQ"))%>'
													OnCommand="lnkFavolitDelete_Command" Text="削除" OnClientClick="return confirm('削除してよろしいですか？');"></asp:LinkButton>
											</ItemTemplate>
										</asp:TemplateField>
									</Columns>
									<PagerSettings Mode="NumericFirstLast" />
								</asp:GridView>
							</fieldset>
						</td>
					</tr>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlLikeMe">
					<tr style="margin-top: 0px">
						<td>
							<div id="anchorLikeMe">
							</div>
							<fieldset class="fieldset-inner">
								<legend>[気に入られ一覧]</legend>
								<asp:Button ID="btnLikeMeCloseMovie" runat="server" Text="閉じる" OnClick="btnCloseLikeMe_Click" />
								<asp:Label ID="lblLikeMeSite" runat="server" Font-Bold="True" Font-Underline="True" ForeColor="#FF8000"></asp:Label>
								<asp:Label ID="lblLikeMeCharNo" runat="server"></asp:Label><br />
								<asp:GridView ID="grdLikeMe" runat="server" AllowPaging="True" AutoGenerateColumns="False" AllowSorting="True" SkinID="GridViewColor">
									<Columns>
										<asp:BoundField DataField="FAVORIT_REGIST_DATE" HeaderText="気に入られ登録日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:TemplateField HeaderText="ハンドル名">
											<ItemStyle HorizontalAlign="Left" />
											<ItemTemplate>
												<asp:HyperLink ID="lnkManView" runat="server" NavigateUrl='<%# string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>'
													Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>' Enabled='<% #IsAdmin() %>'></asp:HyperLink>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:BoundField DataField="LAST_TALK_DATE" HeaderText="最終会話日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:BoundField DataField="TALK_COUNT" HeaderText="会話数">
											<ItemStyle HorizontalAlign="Right" />
										</asp:BoundField>
										<asp:BoundField DataField="LAST_LOGIN_DATE" HeaderText="最終ﾛｸﾞｲﾝ日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:TemplateField HeaderText="状態">
											<ItemTemplate>
												<asp:Image ID="Image2" runat="server" ImageUrl='<%# GetSysImage(Eval("CHARACTER_ONLINE_STATUS") )%>' />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="削除">
											<ItemTemplate>
												<asp:LinkButton ID="lnkLikeMeDelete" runat="server" CausesValidation="False" CommandArgument='<%#string.Format("{0}:{1}",Eval("SITE_CD"),Eval("USER_SEQ"))%>'
													OnCommand="lnkLikeMeDelete_Command" Text="削除" OnClientClick="return confirm('削除してよろしいですか？');"></asp:LinkButton>
											</ItemTemplate>
										</asp:TemplateField>
									</Columns>
									<PagerSettings Mode="NumericFirstLast" />
								</asp:GridView>
							</fieldset>
						</td>
					</tr>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlRefuse">
					<tr style="margin-top: 0px">
						<td>
							<div id="anchorRefuse">
							</div>
							<fieldset class="fieldset-inner">
								<legend>[拒否している一覧]</legend>
								<asp:Button ID="btnRefuseCloseMovie" runat="server" Text="閉じる" OnClick="btnCloseRefuse_Click" />
								<asp:Label ID="lblRefuseSite" runat="server" Font-Bold="True" Font-Underline="True" ForeColor="#FF8000"></asp:Label>
								<asp:Label ID="lblRefuseCharNo" runat="server"></asp:Label><br />
								<asp:GridView ID="grdRefuse" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsRefuse" AllowSorting="True" SkinID="GridViewColor">
									<Columns>
										<asp:BoundField DataField="REFUSE_DATE" HeaderText="拒否登録日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:TemplateField HeaderText="ハンドル名">
											<ItemStyle HorizontalAlign="Left" />
											<ItemTemplate>
												<asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%# string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>'
													Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>' Enabled='<% #IsAdmin() %>'></asp:HyperLink>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:BoundField DataField="LAST_TALK_DATE" HeaderText="最終会話日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:BoundField DataField="TALK_COUNT" HeaderText="会話数">
											<ItemStyle HorizontalAlign="Right" />
										</asp:BoundField>
										<asp:BoundField DataField="LAST_LOGIN_DATE" HeaderText="最終ﾛｸﾞｲﾝ日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:TemplateField HeaderText="状態">
											<ItemTemplate>
												<asp:Image ID="Image2" runat="server" ImageUrl='<%# GetSysImage(Eval("CHARACTER_ONLINE_STATUS") )%>' />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="お気に入られ">
											<ItemTemplate>
												<asp:Label ID="lblLikeMe" runat="server" Text='<%# GetLikeMeMark(Eval("USER_SEQ"), Eval("USER_CHAR_NO"), Eval("PARTNER_USER_SEQ"), Eval("PARTNER_USER_CHAR_NO")) %>' />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="拒否されている">
											<ItemTemplate>
												<asp:Label ID="lblRefuseMe" runat="server" Text='<%# GetRefuseMeMark(Eval("USER_SEQ"), Eval("USER_CHAR_NO"), Eval("PARTNER_USER_SEQ"), Eval("PARTNER_USER_CHAR_NO")) %>' />
											</ItemTemplate>
										</asp:TemplateField>
									</Columns>
									<PagerSettings Mode="NumericFirstLast" />
								</asp:GridView>
							</fieldset>
						</td>
					</tr>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlRefuseMe">
					<tr style="margin-top: 0px">
						<td>
							<div id="anchorRefuseMe">
							</div>
							<fieldset class="fieldset-inner">
								<legend>[拒否されている一覧]</legend>
								<asp:Button ID="btnRefuseMeCloseMovie" runat="server" Text="閉じる" OnClick="btnCloseRefuseMe_Click" />
								<asp:Label ID="lblRefuseMeSite" runat="server" Font-Bold="True" Font-Underline="True" ForeColor="#FF8000"></asp:Label>
								<asp:Label ID="lblRefuseMeCharNo" runat="server"></asp:Label><br />
								<asp:GridView ID="grdRefuseMe" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsRefuseMe" AllowSorting="True" SkinID="GridViewColor">
									<Columns>
										<asp:BoundField DataField="REFUSE_DATE" HeaderText="拒否登録日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:TemplateField HeaderText="ハンドル名">
											<ItemStyle HorizontalAlign="Left" />
											<ItemTemplate>
												<asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%# string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>'
													Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>' Enabled='<% #IsAdmin() %>'></asp:HyperLink>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:BoundField DataField="LAST_TALK_DATE" HeaderText="最終会話日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:BoundField DataField="TALK_COUNT" HeaderText="会話数">
											<ItemStyle HorizontalAlign="Right" />
										</asp:BoundField>
										<asp:BoundField DataField="LAST_LOGIN_DATE" HeaderText="最終ﾛｸﾞｲﾝ日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:TemplateField HeaderText="状態">
											<ItemTemplate>
												<asp:Image ID="Image2" runat="server" ImageUrl='<%# GetSysImage(Eval("CHARACTER_ONLINE_STATUS") )%>' />
											</ItemTemplate>
										</asp:TemplateField>
									</Columns>
									<PagerSettings Mode="NumericFirstLast" />
								</asp:GridView>
							</fieldset>
						</td>
					</tr>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlMarking" Visible="false">
					<tr style="margin-top: 0px">
						<td>
							<div id="Div1">
							</div>
							<fieldset class="fieldset-inner">
								<legend>[足あと]</legend>
								<asp:Button ID="btnCloseMarking" runat="server" Text="閉じる" OnClick="btnCloseMarking_Click" />
								<asp:Label ID="lblMarkingSite" runat="server" Font-Bold="True" Font-Underline="True" ForeColor="#FF8000"></asp:Label>
								<asp:Label ID="lblMarkingCharNo" runat="server"></asp:Label><br />
								<asp:GridView ID="grdMarking" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsMarking" AllowSorting="True" SkinID="GridViewColor"
									PageSize="30">
									<Columns>
										<asp:BoundField DataField="MARKING_DATE" HeaderText="足あと日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:TemplateField HeaderText="ハンドル名">
											<ItemStyle HorizontalAlign="Left" />
											<ItemTemplate>
												<asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%# string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>'
													Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>' Enabled='<% #IsAdmin() %>'></asp:HyperLink>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:BoundField DataField="LAST_TALK_DATE" HeaderText="最終会話日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:BoundField DataField="TALK_COUNT" HeaderText="会話数">
											<ItemStyle HorizontalAlign="Right" />
										</asp:BoundField>
										<asp:TemplateField HeaderText="状態">
											<ItemTemplate>
												<asp:Image ID="Image2" runat="server" ImageUrl='<%# GetSysImage(Eval("CHARACTER_ONLINE_STATUS"))%>' />
											</ItemTemplate>
										</asp:TemplateField>
									</Columns>
									<PagerSettings Mode="NumericFirstLast" />
								</asp:GridView>
							</fieldset>
						</td>
					</tr>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlFriends" Visible="false">
					<asp:Panel runat="server" ID="pnlCastFriends" Visible="false">
						<tr style="margin-top: 0px">
							<td>
								<div id="anchorCastFriends">
								</div>
								<fieldset class="fieldset-inner">
									<legend>[出演者紹介]</legend>
									<asp:Button runat="server" ID="btnCloseCastFriends" Text="閉じる" OnClick="btnCloseCastFriends_Click" />
									<asp:Label ID="lblCastFriendsSite" runat="server" Font-Bold="True" Font-Underline="True" ForeColor="#FF8000" />
									<asp:Label ID="lblCastFriendsCharNo" runat="server" /><br />
									<asp:GridView ID="grdCastFriends" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCastFriends" AllowSorting="True" SkinID="GridViewColor"
										HeaderStyle-HorizontalAlign="center">
										<Columns>
											<asp:TemplateField HeaderText="ﾛｸﾞｲﾝID">
												<ItemStyle HorizontalAlign="Left" Width="80px" />
												<ItemTemplate>
													<asp:HyperLink ID="lblLoginId" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}",Eval("LOGIN_ID")) %>' Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="ﾊﾝﾄﾞﾙ名">
												<ItemStyle HorizontalAlign="Left" Width="120px" />
												<ItemTemplate>
													<asp:Label runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>' ID="lblHandleNm" />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:BoundField DataField="REGIST_DATE" HeaderText="登録日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
												<ItemStyle HorizontalAlign="Center" Width="110px" />
											</asp:BoundField>
											<asp:BoundField DataField="LAST_LOGIN_DATE" DataFormatString="{0:yy/MM/dd HH:mm}" HeaderText="最終ﾛｸﾞｲﾝ日" HtmlEncode="False">
												<ItemStyle HorizontalAlign="Center" Width="110px" />
											</asp:BoundField>
											<asp:TemplateField HeaderText="ﾌﾟﾛﾌ入力済">
												<ItemTemplate>
													<asp:Label runat="server" Text='<%# GetInValidMark(Eval("NA_FLAG")) %>' ID="lblProfileOk" />
												</ItemTemplate>
												<ItemStyle HorizontalAlign="Center" Width="70px" />
											</asp:TemplateField>
											<asp:BoundField DataField="TOTAL_PAYMENT_AMT" HeaderText="累計報酬金額" DataFormatString="{0:#,##0}" HtmlEncode="False">
												<ItemStyle HorizontalAlign="Right" Width="100px" />
											</asp:BoundField>
										</Columns>
										<PagerSettings Mode="NumericFirstLast" />
									</asp:GridView>
								</fieldset>
							</td>
						</tr>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlManFriends" Visible="false">
						<tr style="margin-top: 0px">
							<td>
								<div id="anchorManFriends">
								</div>
								<fieldset class="fieldset-inner">
									<legend>[男性会員紹介]</legend>
									<asp:Button runat="server" ID="btnCloseManFriends" Text="閉じる" OnClick="btnCloseManFriends_Click" />
									<asp:Label ID="lblManFriendsSite" runat="server" Font-Bold="True" Font-Underline="True" ForeColor="#FF8000"></asp:Label>
									<asp:Label ID="lblManFriendsCharNo" runat="server"></asp:Label><br />
									<asp:GridView ID="grdManFriends" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsManFriends" AllowSorting="True" SkinID="GridViewColor"
										PageSize="30" OnDataBound="grdManFriends_DataBound" HeaderStyle-HorizontalAlign="center">
										<Columns>
											<asp:TemplateField HeaderText="ﾛｸﾞｲﾝID">
												<ItemStyle HorizontalAlign="Left" Width="80px" />
												<ItemTemplate>
													<asp:HyperLink ID="lblLoginId" runat="server" NavigateUrl='<%# string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>'
														Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="ﾊﾝﾄﾞﾙ名">
												<ItemStyle HorizontalAlign="Left" Width="120px" />
												<ItemTemplate>
													<asp:Label runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>' ID="lblHandleNm" />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:BoundField DataField="REGIST_DATE" HeaderText="登録日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
												<ItemStyle HorizontalAlign="Center" Width="110px" />
											</asp:BoundField>
											<asp:BoundField DataField="LAST_LOGIN_DATE" DataFormatString="{0:yy/MM/dd HH:mm}" HeaderText="最終ﾛｸﾞｲﾝ日" HtmlEncode="False">
												<ItemStyle HorizontalAlign="Center" Width="110px" />
											</asp:BoundField>
											<asp:TemplateField HeaderText="ﾌﾟﾛﾌ入力済">
												<ItemTemplate>
													<asp:Label runat="server" Text='<%# GetValidMark(Eval("PROFILE_OK_FLAG")) %>' ID="lblProfileOk" />
												</ItemTemplate>
												<ItemStyle HorizontalAlign="Center" Width="70px" />
											</asp:TemplateField>
											<asp:BoundField DataField="LIMIT_POINT" HeaderText="残ﾎﾟｲﾝﾄ" DataFormatString="{0:#,##0}" HtmlEncode="False">
												<ItemStyle HorizontalAlign="Right" Width="100px" />
											</asp:BoundField>
										</Columns>
										<PagerSettings Mode="NumericFirstLast" />
									</asp:GridView>
								</fieldset>
							</td>
						</tr>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlManFriendLogs" Visible="false">
						<tr style="margin-top: 0px">
							<td>
								<div id="anchorManFriendLogs">
								</div>
								<fieldset class="fieldset-inner">
									<legend>[男性会員紹介履歴]</legend>
									<asp:Button runat="server" ID="btnCloseManFriendLogs" Text="閉じる" OnClick="btnCloseManFriendLogs_Click" />
									<asp:Label ID="lblManFriendLogsSite" runat="server" Font-Bold="True" Font-Underline="True" ForeColor="#FF8000"></asp:Label>
									<asp:Label ID="lblManFriendLogsCharNo" runat="server"></asp:Label><br />
									<asp:GridView ID="grdManFriendLogs" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsManFriendLogs" AllowSorting="True" SkinID="GridViewColor"
										PageSize="30" OnDataBound="grdManFriendLogs_DataBound" HeaderStyle-HorizontalAlign="center">
										<Columns>
											<asp:TemplateField HeaderText="ﾛｸﾞｲﾝID">
												<ItemStyle HorizontalAlign="Left" Width="80px" />
												<ItemTemplate>
													<asp:HyperLink ID="lblLoginId" runat="server" NavigateUrl='<%# string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>'
														Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="ﾊﾝﾄﾞﾙ名">
												<ItemStyle HorizontalAlign="Left" Width="120px" />
												<ItemTemplate>
													<asp:Label runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>' ID="lblHandleNm" />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:BoundField DataField="REGIST_DATE" HeaderText="登録日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
												<ItemStyle HorizontalAlign="Center" Width="110px" />
											</asp:BoundField>
											<asp:BoundField DataField="LAST_LOGIN_DATE" DataFormatString="{0:yy/MM/dd HH:mm}" HeaderText="最終ﾛｸﾞｲﾝ日" HtmlEncode="False">
												<ItemStyle HorizontalAlign="Center" Width="110px" />
											</asp:BoundField>
											<asp:TemplateField HeaderText="ﾌﾟﾛﾌ入力済">
												<ItemTemplate>
													<asp:Label runat="server" Text='<%# GetValidMark(Eval("PROFILE_OK_FLAG")) %>' ID="lblProfileOk" />
												</ItemTemplate>
												<ItemStyle HorizontalAlign="Center" Width="70px" />
											</asp:TemplateField>
											<asp:BoundField DataField="LIMIT_POINT" HeaderText="残ﾎﾟｲﾝﾄ" DataFormatString="{0:#,##0}" HtmlEncode="False">
												<ItemStyle HorizontalAlign="Right" Width="100px" />
											</asp:BoundField>
										</Columns>
										<PagerSettings Mode="NumericFirstLast" />
									</asp:GridView>
								</fieldset>
							</td>
						</tr>
					</asp:Panel>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlMailBox">
					<tr style="margin-top: 0px">
						<td>
							<div id="anchorMailBox">
							</div>
							<fieldset class="fieldset-inner">
								<legend>[メールBOX]</legend>
								<table border="0" class="tableStyle"">
									<tr>
										<td class="tdHeaderStyle">
											履歴開始日
										</td>
										<td class="tdDataStyle">
											<asp:DropDownList ID="lstMailFromYYYY" runat="server" Width="60px">
											</asp:DropDownList>年
											<asp:DropDownList ID="lstMailFromMM" runat="server" Width="40px">
											</asp:DropDownList>月
											<asp:DropDownList ID="lstMailFromDD" runat="server" Width="40px">
											</asp:DropDownList>日
											<asp:DropDownList ID="lstMailFromHH" runat="server" Width="40px">
											</asp:DropDownList>時
										</td>
										<td class="tdHeaderStyle">
											履歴終了日
										</td>
										<td class="tdDataStyle">
											<asp:DropDownList ID="lstMailToYYYY" runat="server" Width="60px">
											</asp:DropDownList>年
											<asp:DropDownList ID="lstMailToMM" runat="server" Width="40px">
											</asp:DropDownList>月
											<asp:DropDownList ID="lstMailToDD" runat="server" Width="40px">
											</asp:DropDownList>日
											<asp:DropDownList ID="lstMailToHH" runat="server" Width="40px">
											</asp:DropDownList>時
										</td>
									</tr>
									<tr>
									<asp:PlaceHolder ID="plcNotLimitedInputByAdminType" runat="server" Visible="true">
										<td class="tdHeaderStyle">
											男性会員ID
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtMailPartnerLoginId" runat="server" Width="70px" MaxLength="8"></asp:TextBox>
										</td>
										<td class="tdHeaderStyle">
											送受信
										</td>
										<td class="tdDataStyle">
									</asp:PlaceHolder>
									<asp:PlaceHolder ID="plcIsLimitedInputByAdminType" runat="server" Visible="false">
										<td class="tdHeaderStyle">
											送受信
										</td>
										<td class="tdDataStyle" colspan="3">
									</asp:PlaceHolder>
											<asp:RadioButton ID="rdoTxRxType" runat="server" Text="送受信" GroupName="TxRxTypeGrouping" Checked="true" />
											<asp:RadioButton ID="rdoTxType" runat="server" Text="送信" GroupName="TxRxTypeGrouping" />
											<asp:RadioButton ID="rdoRxType" runat="server" Text="受信" GroupName="TxRxTypeGrouping" />
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle2">
											添付種別
										</td>
										<td class="tdDataStyle">
											<asp:CheckBoxList ID="chkAttachedType" runat="server" RepeatDirection="horizontal">
												<asp:ListItem Text="画像添付" Value="1"></asp:ListItem>
												<asp:ListItem Text="動画添付" Value="2"></asp:ListItem>
											</asp:CheckBoxList></td>
										<td class="tdHeaderStyle2">
											一括送信
										</td>
										<td class="tdDataStyle">
											<asp:RadioButton ID="rdoWithBatchMailNon" runat="server" Text="会員間" GroupName="WithBatchMailGrouping" Checked="true" />
											<asp:RadioButton ID="rdoWithBatchMailOnly" runat="server" Text="一括" GroupName="WithBatchMailGrouping" />
											<asp:RadioButton ID="rdoWithBatchMailAll" runat="server" Text="全て" GroupName="WithBatchMailGrouping" />
										</td>
									</tr>
								</table>
								<asp:Button ID="btnSeekMailBox" runat="server" Text="検索" CssClass="seekbutton" OnClick="btnSeekMailBox_Click" ValidationGroup="MailBox" />
								<asp:Button ID="btnCloseMailBox" runat="server" Text="閉じる" CssClass="seekbutton" OnClick="btnCloseMailBox_Click" />
								<asp:Label ID="lblMailSite" runat="server" Font-Bold="True" Font-Underline="True" ForeColor="#FF8000"></asp:Label>
								<asp:Label ID="lblMailCharNo" runat="server"></asp:Label>
								<asp:Label ID="lblMailBoxSiteCd" runat="server" Visible="false"></asp:Label>
								<asp:Label ID="lblMailBoxCharNo" runat="server" Visible="false"></asp:Label>
								<asp:CustomValidator ID="vdcMailBox" runat="server" ErrorMessage="日時を正しく入力してください" OnServerValidate="vdcMailBox_ServerValidate" ValidationGroup="MailBox"></asp:CustomValidator>
								<br />
								<br />
								<asp:GridView ID="grdMailBox" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsMailBox" AllowSorting="True" SkinID="GridViewColor"
									PageSize="20" OnRowDataBound="grdMailBox_RowDataBound">
									<Columns>
										<asp:TemplateField HeaderText="送受信">
											<ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Wrap="false" />
											<HeaderStyle Wrap="false" />
											<ItemTemplate>
												<asp:Label ID="lblTxRxTypeNm" runat="server" Text='<%# GetMailTxRxNm(Eval("TXRX_TYPE"))%>'></asp:Label><br />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="作成日時">
											<ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Wrap="false" />
											<ItemTemplate>
												<asp:Label ID="lblCreateDate" runat="server" Text='<%# DateTime.Parse(Eval("CREATE_DATE").ToString()).ToString("yy/MM/dd HH:mm:ss") %>'></asp:Label><br />
												<asp:LinkButton ID="lnkRxDel" runat="server" CommandArgument='<%# Eval("MAIL_SEQ") %>' Text="受信削除" OnCommand="lnkDelRxMail_Command"></asp:LinkButton>&nbsp;
												<asp:LinkButton ID="lnkTxDel" runat="server" CommandArgument='<%# Eval("MAIL_SEQ") %>' Text="送信削除" OnCommand="lnkDelTxMail_Command"></asp:LinkButton>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="男性会員情報">
											<ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Wrap="false" />
											<HeaderStyle Wrap="false" />
											<ItemTemplate>
												<asp:HyperLink ID="lblHandelNm" runat="server" NavigateUrl='<%# string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("PARTNER_LOGIN_ID")) %>'
													Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:HyperLink>
													<asp:Label ID="selfText" Text='<%# Eval("STR_NEW") %>' runat="server" CssClass="new_label"></asp:Label>
													<br />
												<asp:Label ID="lblMailPartnerLoginId" runat="server" Text='<%# Eval("PARTNER_LOGIN_ID") %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="内容">
											<ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
											<ItemTemplate>
												<asp:Label ID="lblMailTitle" runat="server" Text='<%# Eval("ORIGINAL_TITLE") %>' Font-Bold="true"></asp:Label><br />
												<asp:Label ID="lblMailDoc" runat="server" Text='<%# string.Format("{0}{1}{2}{3}{4}",Eval("ORIGINAL_DOC1"),Eval("ORIGINAL_DOC2"),Eval("ORIGINAL_DOC3"),Eval("ORIGINAL_DOC4"),Eval("ORIGINAL_DOC5")).Replace("\r\n","<br />") %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="種別">
											<ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Wrap="false" />
											<ItemTemplate>
												<asp:Panel ID="pnlOpenPicViewer" runat="server" Visible='<%# iBridUtil.GetStringValue(Eval("ATTACHED_OBJ_TYPE")).Equals(ViCommConst.ATTACH_PIC_INDEX.ToString()) %>'>
													<asp:HyperLink ID="lnkOpenPicViewer" runat="server" NavigateUrl="<%# GenerateOpenPicScript(Container.DataItem) %>" Text="画像添付"></asp:HyperLink>
												</asp:Panel>
												<asp:Panel ID="pnlOpenMovieViewer" runat="server" Visible='<%# iBridUtil.GetStringValue(Eval("ATTACHED_OBJ_TYPE")).Equals(ViCommConst.ATTACH_MOVIE_INDEX.ToString()) %>'>
													<asp:HyperLink ID="lnkOpenMovieViewer" runat="server" NavigateUrl="<%# GenerateOpenMovieScript(Container.DataItem) %>" Text="動画添付"></asp:HyperLink>
												</asp:Panel>
												<asp:Label ID="lblBatchMailMark" runat="server" Text='<%# GetBatchMark(Eval("BATCH_MAIL_FLAG")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="状態">
											<ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Wrap="false" />
											<ItemTemplate>
												<asp:Label ID="lblDelMark" runat="server" Text='<%# GetDelMark(Eval("TX_DEL_FLAG"),Eval("RX_DEL_FLAG")) %>'></asp:Label><br />
												<asp:Label ID="lblReadFlag" runat="server" Text='<%# GetReadNm(Eval("READ_FLAG")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
									</Columns>
									<PagerSettings Mode="NumericFirstLast" />
								</asp:GridView>
							</fieldset>
						</td>
					</tr>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlTalkHis">
					<tr style="margin-top: 0px">
						<td>
							<div id="anchorTalkHis">
							</div>
							<fieldset class="fieldset-inner">
								<legend>[会話履歴]</legend>
								<asp:Button ID="btnTalkHisClose" runat="server" Text="閉じる" OnClick="btnCloseTalkHis_Click" />
								<asp:Label ID="lblTalkHisSite" runat="server" Font-Bold="True" Font-Underline="True" ForeColor="#FF8000"></asp:Label>
								<asp:Label ID="lblTalkHisCharNo" runat="server"></asp:Label><br />
								<asp:GridView ID="grdTalkHis" runat="server" AllowPaging="True" AutoGenerateColumns="False" AllowSorting="True" SkinID="GridViewColor">
									<Columns>
										<asp:BoundField DataField="TALK_START_DATE" HeaderText="会話開始日時" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:BoundField DataField="TALK_END_DATE" HeaderText="終了時刻" DataFormatString="{0:HH:mm}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:TemplateField HeaderText="ハンドル名">
											<ItemStyle HorizontalAlign="Left" />
											<ItemTemplate>
												<asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%# string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>'
													Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>' Enabled='<% #IsAdmin() %>'></asp:HyperLink>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:BoundField DataField="BAL_POINT" HeaderText="残Ｐ">
											<ItemStyle HorizontalAlign="Right" />
										</asp:BoundField>
										<asp:BoundField DataField="LAST_LOGIN_DATE" HeaderText="最終ﾛｸﾞｲﾝ日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:TemplateField HeaderText="状態">
											<ItemTemplate>
												<asp:Image ID="Image2" runat="server" ImageUrl='<%# GetSysImage(Eval("CHARACTER_ONLINE_STATUS") )%>' />
											</ItemTemplate>
										</asp:TemplateField>
									</Columns>
									<PagerSettings Mode="NumericFirstLast" />
								</asp:GridView>
							</fieldset>
						</td>
					</tr>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlRequestHis">
					<tr style="margin-top: 0px">
						<td>
							<div id="anchorRequestHis">
							</div>
							<fieldset class="fieldset-inner">
								<legend>[会話履歴]</legend>
								<asp:Button ID="btnRequestHisClose" runat="server" Text="閉じる" OnClick="btnCloseRequestHis_Click" />
								<asp:Label ID="lblRequestHisSite" runat="server" Font-Bold="True" Font-Underline="True" ForeColor="#FF8000"></asp:Label>
								<asp:Label ID="lblRequestHisCharNo" runat="server"></asp:Label><br />
								<asp:GridView ID="grdRequestHis" runat="server" AllowPaging="True" AutoGenerateColumns="False" AllowSorting="True" SkinID="GridViewColor">
									<Columns>
										<asp:BoundField DataField="REQUEST_DATE" HeaderText="会話要求日時" DataFormatString="{0:yy/MM/dd HH:mm:ss}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:BoundField DataField="TALK_START_DATE" HeaderText="会話開始日時" DataFormatString="{0:HH:mm:ss}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:BoundField DataField="TALK_END_DATE" HeaderText="終了時刻" DataFormatString="{0:HH:mm:ss}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:TemplateField HeaderText="ハンドル名">
											<ItemStyle HorizontalAlign="Left" />
											<ItemTemplate>
												<asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%# string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>'
													Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>' Enabled='<% #IsAdmin() %>'></asp:HyperLink>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:BoundField DataField="REQUEST_CONNECT_TYPE_NM" HeaderText="待機種別">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:TemplateField HeaderText="状態">
											<ItemTemplate>
												<asp:Image ID="Image2" runat="server" ImageUrl='<%# GetSysImage(Eval("CHARACTER_ONLINE_STATUS") )%>' />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:BoundField DataField="CHARGE_SEC" HeaderText="秒数">
											<ItemStyle HorizontalAlign="Right" />
										</asp:BoundField>
										<asp:BoundField DataField="CHARGE_TYPE_NM" HeaderText="会話種別">
											<ItemStyle HorizontalAlign="Left" />
										</asp:BoundField>
										<asp:BoundField DataField="CALL_RESULT_NM" HeaderText="通話終了理由" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Left" />
										</asp:BoundField>
									</Columns>
									<PagerSettings Mode="NumericFirstLast" />
								</asp:GridView>
							</fieldset>
						</td>
					</tr>
				</asp:Panel>
				<asp:Panel ID="pnlMail" runat="server">
					<tr style="margin-top: 0px">
						<td>
							<div id="anchorMailCreate">
							</div>
							<fieldset class="fieldset-inner">
								<legend>[メール作成]</legend>
								<table border="0" style="width: 500px" class="tableStyle">
									<tr>
										<td class="tdHeaderStyle">
											ﾒｰﾙｻｰﾊﾞｰ
										</td>
										<td class="tdDataStyle">
											<asp:RadioButtonList ID="rdoMailServer" runat="server" RepeatDirection="Horizontal">
												<asp:ListItem Text="BEAM" Value="1" Selected="True"></asp:ListItem>
												<asp:ListItem Text="MULLER3" Value="2"></asp:ListItem>
											</asp:RadioButtonList>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											送信先情報
										</td>
										<td class="tdDataStyle">
											<asp:Label ID="lblCreateMailSiteNm" runat="server" Font-Bold="True" Font-Underline="True" ForeColor="#FF8000"></asp:Label>
											<asp:Label ID="lblCreateMailCharNo" runat="server"></asp:Label>
											<asp:Label ID="lblCreateMailVisibleSiteCd" runat="server" Visible="false"></asp:Label>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											ﾒｰﾙ種別
										</td>
										<td class="tdDataStyle">
											<asp:DropDownList ID="lstMailTemplate" runat="server" DataSourceID="dsDMMail" DataTextField="TEMPLATE_NM" DataValueField="MAIL_TEMPLATE_NO" Width="309px"
												AutoPostBack="True" OnSelectedIndexChanged="lstMailTemplate_SelectedIndexChanged">
											</asp:DropDownList>
											<asp:RequiredFieldValidator ID="vdrMailTemplate" runat="server" ErrorMessage="ﾒｰﾙ種別を選択して下さい。" ControlToValidate="lstMailTemplate" ValidationGroup="TxMail">*</asp:RequiredFieldValidator>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle2">
											ﾒｰﾙﾀｲﾄﾙ
										</td>
										<td class="tdDataStyle">
											$NO_TRANS_START;
											<asp:TextBox ID="txtMailTitle" runat="server" MaxLength="60" Width="180px"></asp:TextBox>
											$NO_TRANS_END;
											<asp:RequiredFieldValidator ID="vdrMailTitle" runat="server" ErrorMessage="メールタイトルを選択して下さい。" ControlToValidate="txtMailTitle" ValidationGroup="TxMail">*</asp:RequiredFieldValidator>
										</td>
									</tr>
									<tr>
										<td class="tdDataStyle" valign="top" colspan="2">
											$NO_TRANS_START;
											<pin:pinEdit ID="txtMailDocHtml" runat="server" UserMode="HTML" DisplayStyle="Style5" DocumentView="True" HtmlMode="Body" Ruler="False" ReturnMode="BR"
												SpellMode="Inline" ToolbarConfiguration="ToolbarDesigner" Toolbar="T09SE10116012136267;T22232427282930313233343536;B53SE54SE55" FormatMode="Classic"
												BorderWidth="1px" Height="500px" Width="500px" DocumentWidth="230" Font-Bold="False" FontSizeType="px" RelativeBaseImageUrl="/" RelativeBaseLinkUrl="/"
												RelativeImageRoot="/">
											</pin:pinEdit>
											<asp:TextBox ID="txtMailDocText" runat="server" TextMode="MultiLine" Width="500px" Height="500px">
											</asp:TextBox>
											$NO_TRANS_END;
										</td>
									</tr>
								</table>
								<asp:Label ID="lblAttention" runat="server" Text="" Font-Size="Small" ForeColor="Red"></asp:Label><br />
								<asp:Button runat="server" ID="btnCloseMail" Text="閉じる" CssClass="seekbutton" OnClick="btnCloseMail_Click" />
								<asp:Button runat="server" ID="btnTransMail" Text="メール送信" CssClass="seekbutton" OnClientClick='return confirm("メールを送信しますか？");' OnClick="btnTransMail_Click"
									ValidationGroup="TxMail" />
							</fieldset>
						</td>
					</tr>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlModifyMailHis" Visible="false">
					<tr style="margin-top: 0px">
						<td>
							<div id="anchorModifyMailHis">
							</div>
							<fieldset class="fieldset-inner">
								<legend>[ﾒｱﾄﾞ変更履歴/送信NG履歴/TEL変更履歴/ﾊﾟｽﾜｰﾄﾞ変更履歴]</legend>
								<table>
									<tr valign="top">
										<td>
											<asp:GridView ID="grdModifyMailHis" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsModifyMailHis" AllowSorting="True" SkinID="GridViewColor"
												PageSize="20" HeaderStyle-HorizontalAlign="center">
												<Columns>
													<asp:TemplateField HeaderText="変更日時">
														<ItemTemplate>
															<asp:Label ID="Label3" runat="server" Text='<%# Eval("CREATE_DATE", "{0:yy/MM/dd HH:mm:ss}") %>'></asp:Label>
														</ItemTemplate>
													</asp:TemplateField>
													<asp:BoundField DataField="EMAIL_ADDR" HeaderText="ﾒｰﾙｱﾄﾞﾚｽ">
														<ItemStyle HorizontalAlign="Left" />
													</asp:BoundField>
													<asp:TemplateField HeaderText="変更者">
														<ItemTemplate>
															<asp:Label ID="lblModifyUserNm" runat="server" Text='<%# GetModifyUserNm(Eval("MODIFY_USER_CD")) %>'></asp:Label>
														</ItemTemplate>
														<ItemStyle HorizontalAlign="Center" />
													</asp:TemplateField>
													<asp:TemplateField HeaderText="変更ﾀｲﾐﾝｸﾞ">
														<ItemTemplate>
															<asp:Label ID="lblModifyBeforeAfterNm" runat="server" Text='<%# string.Format("変更{0}",Eval("MODIFY_BEFORE_AFTER_NM")) %>'></asp:Label>
														</ItemTemplate>
														<ItemStyle HorizontalAlign="Center" />
													</asp:TemplateField>
												</Columns>
												<PagerSettings Mode="NumericFirstLast" />
											</asp:GridView>
										</td>
										<td>
											<asp:GridView ID="grdNgMailHistory" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsNgMailHistory" AllowSorting="True" SkinID="GridViewColor"
												PageSize="20" HeaderStyle-HorizontalAlign="center">
												<Columns>
													<asp:TemplateField HeaderText="NGﾒｰﾙ受信日">
														<ItemTemplate>
															<asp:Label ID="Label3" runat="server" Text='<%# Eval("NG_MAIL_DATE", "{0:yy/MM/dd HH:mm:ss}") %>'></asp:Label>
														</ItemTemplate>
													</asp:TemplateField>
													<asp:BoundField DataField="EMAIL_ADDR" HeaderText="ﾒｰﾙｱﾄﾞﾚｽ">
														<ItemStyle HorizontalAlign="Left" />
													</asp:BoundField>
													<asp:TemplateField HeaderText="ｴﾗｰ種別">
														<ItemTemplate>
															<asp:Label ID="lblErrorType" runat="server" Text='<%# GetErrorTypeText(Eval("ERROR_TYPE")) %>'></asp:Label>
														</ItemTemplate>
													</asp:TemplateField>
												</Columns>
												<PagerSettings Mode="NumericFirstLast" />
											</asp:GridView>
										</td>
									</tr>
									<tr>
									</tr>
									<tr valign="top">
										<td>
											<asp:GridView ID="grdModifyTelHis" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsModifyTelHis" AllowSorting="True" SkinID="GridViewColor"
												PageSize="20" HeaderStyle-HorizontalAlign="center">
												<Columns>
													<asp:TemplateField HeaderText="変更日時">
														<ItemTemplate>
															<asp:Label ID="Label3" runat="server" Text='<%# Eval("CREATE_DATE", "{0:yy/MM/dd HH:mm:ss}") %>'></asp:Label>
														</ItemTemplate>
													</asp:TemplateField>
													<asp:BoundField DataField="TEL" HeaderText="電話番号">
														<ItemStyle HorizontalAlign="Left" />
													</asp:BoundField>
													<asp:TemplateField HeaderText="変更者">
														<ItemTemplate>
															<asp:Label ID="lblModifyUserNm" runat="server" Text='<%# GetModifyUserNm(Eval("MODIFY_USER_CD")) %>'></asp:Label>
														</ItemTemplate>
														<ItemStyle HorizontalAlign="Center" />
													</asp:TemplateField>
													<asp:TemplateField HeaderText="変更ﾀｲﾐﾝｸﾞ">
														<ItemTemplate>
															<asp:Label ID="lblModifyBeforeAfterNm" runat="server" Text='<%# string.Format("変更{0}",Eval("MODIFY_BEFORE_AFTER_NM")) %>'></asp:Label>
														</ItemTemplate>
														<ItemStyle HorizontalAlign="Center" />
													</asp:TemplateField>
												</Columns>
												<PagerSettings Mode="NumericFirstLast" />
											</asp:GridView>
										</td>
										<td>
											<asp:GridView ID="grdModifyLoginPassword" runat="server" AllowPaging="false" AutoGenerateColumns="false" DataSourceID="dsModifyLoginPassword" AllowSorting="false" SkinID="GridViewColor" HeaderStyle-HorizontalAlign="center">
												<Columns>
													<asp:TemplateField HeaderText="変更日時">
														<ItemTemplate>
															<asp:Label ID="lblMLPCreateDate" runat="server" Text='<%# Eval("CREATE_DATE", "{0:yy/MM/dd HH:mm:ss}") %>'></asp:Label>
														</ItemTemplate>
													</asp:TemplateField>
													<asp:BoundField DataField="LOGIN_PASSWORD" HeaderText="ﾊﾟｽﾜｰﾄﾞ">
														<ItemStyle HorizontalAlign="Left" />
													</asp:BoundField>
													<asp:TemplateField HeaderText="変更者">
														<ItemTemplate>
															<asp:Label ID="lblMLPModifyUserNm" runat="server" Text='<%# GetModifyUserNm(Eval("MODIFY_USER_CD")) %>'></asp:Label>
														</ItemTemplate>
														<ItemStyle HorizontalAlign="Center" />
													</asp:TemplateField>
													<asp:TemplateField HeaderText="変更ﾀｲﾐﾝｸﾞ">
														<ItemTemplate>
															<asp:Label ID="lblMLPModifyBeforeAfterNm" runat="server" Text='<%# GetModifyBeforeAfterNm(Eval("MODIFY_BEFORE_AFTER_CD")) %>'></asp:Label>
														</ItemTemplate>
														<ItemStyle HorizontalAlign="Center" />
													</asp:TemplateField>
												</Columns>
											</asp:GridView>
										</td>
									</tr>
								</table>
								<asp:Button runat="server" ID="btnCloseModifyMailHis" Text="閉じる" CssClass="seekbutton" OnClick="btnCloseModifyMailHis_Click" />
							</fieldset>
						</td>
					</tr>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlGameCharacter" Visible="false">
					<tr style="margin-top: 0px" valign="top">
						<td>
							<div id="anchorGameCharacter">
							</div>
							<fieldset class="fieldset-inner">
								<legend>[ｹﾞｰﾑｷｬﾗｸﾀｰ情報]</legend>
								<asp:Button ID="btnCloseGameCharacter" runat="server" Text="閉じる" OnClick="btnCloseGameCharacter_Click" />
								<asp:PlaceHolder ID="plcGameHolder" runat="server">
									<asp:Table ID="Table2" runat="server" CssClass="tableStyle" Width="368px">
										<asp:TableRow ID="rowGameCharacterHandleNm" runat="server">
											<asp:TableCell ID="celGameCharacterHandleNmHeader" runat="server" CssClass="tdHeaderStyle">
												<asp:Label ID="lblGameCharacterHandleNmHeader" runat="server" Text="ｹﾞｰﾑﾊﾝﾄﾞﾙﾈｰﾑ"></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celGameCharacterHandleNm" runat="server" CssClass="tdDataStyle">
												<asp:Label ID="lblGameCharacterHandleNm" runat="server"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowGameCharacterTypeNm" runat="server">
											<asp:TableCell ID="celGameCharacterTypeNmHeader" runat="server" CssClass="tdHeaderStyle">
												<asp:Label ID="lblGameCharacterTypeNmHeader" runat="server" Text="ｹﾞｰﾑｷｬﾗｸﾀｰ種別"></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celGameCharacterTypeNm" runat="server" CssClass="tdDataStyle">
												<asp:Label ID="lblGameCharacterTypeNm" runat="server"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowGameCharacterPoint" runat="server">
											<asp:TableCell ID="celGameCharacterPointHeader" runat="server" CssClass="tdHeaderStyle">
												<asp:Label ID="lblGameCharacterPointHeader" runat="server" Text="ｹﾞｰﾑ内ﾎﾟｲﾝﾄ"></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celGameCharacterPoint" runat="server" CssClass="tdDataStyle">
												<asp:Label ID="lblGameCharacterPoint" runat="server"></asp:Label>円
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowGameCharacterCastPoint" runat="server">
											<asp:TableCell ID="celGameCharacterCastPointHeader" runat="server" CssClass="tdHeaderStyle">
												<asp:Label ID="lblGameCharacterCastPointHeader" runat="server" Text="ﾌﾟﾚﾐｱﾑﾎﾟｲﾝﾄ"></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celGameCharacterCastPoint" runat="server" CssClass="tdDataStyle">
												<asp:Label ID="lblGameCharacterCastPoint" runat="server"></asp:Label>pt
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowGameCharacterLevel" runat="server">
											<asp:TableCell ID="celGameCharacterLevelHeader" runat="server" CssClass="tdHeaderStyle">
												<asp:Label ID="lblGameCharacterLevelHeader" runat="server" Text="ﾚﾍﾞﾙ"></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celGameCharacterLevel" runat="server" CssClass="tdDataStyle">
												<asp:Label ID="lblGameCharacterLevel" runat="server"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowGameCharacterExp" runat="server">
											<asp:TableCell ID="celGameCharacterExpHeader" runat="server" CssClass="tdHeaderStyle">
												<asp:Label ID="lblGameCharacterExpHeader" runat="server" Text="現在の経験値"></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celGameCharacterExp" runat="server" CssClass="tdDataStyle">
												<asp:Label ID="lblGameCharacterExp" runat="server"></asp:Label>
												次のﾚﾍﾞﾙまであと<asp:Label ID="lblGameCharacterRestExp" runat="server"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowGameCharacterTotalExp" runat="server">
											<asp:TableCell ID="celGameCharacterTotalExpHeader" runat="server" CssClass="tdHeaderStyle">
												<asp:Label ID="lblGameCharacterTotalExpHeader" runat="server" Text="累計経験値"></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celGameCharacterTotalExp" runat="server" CssClass="tdDataStyle">
												<asp:Label ID="lblGameCharacterTotalExp" runat="server"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowGameCharacterUnassignedFroce" runat="server">
											<asp:TableCell ID="celGameCharacterUnassignedFroceHeader" runat="server" CssClass="tdHeaderStyle">
												<asp:Label ID="lblGameCharacterUnassignedFroceHeader" runat="server" Text="未割当部隊数"></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celGameCharacterUnassignedFroce" runat="server" CssClass="tdDataStyle">
												<asp:Label ID="lblGameCharacterUnassignedFroce" runat="server"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowGameCharacterMissionMaxForce" runat="server">
											<asp:TableCell ID="celGameCharacterMissionMaxForceHeader" runat="server" CssClass="tdHeaderStyle">
												<asp:Label ID="lblGameCharacterMissionMaxForceHeader" runat="server" Text="ﾐｯｼｮﾝ部隊数"></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celGameCharacterMissionMaxForce" runat="server" CssClass="tdDataStyle">
												<asp:Label ID="lblGameCharacterMissionMaxForce" runat="server"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowGameCharacterAttackMaxForce" runat="server">
											<asp:TableCell ID="celGameCharacterAttackMaxForceHeader" runat="server" CssClass="tdHeaderStyle">
												<asp:Label ID="lblGameCharacterAttackMaxForceHeader" runat="server" Text="攻撃部隊数"></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celGameCharacterAttackMaxForce" runat="server" CssClass="tdDataStyle">
												<asp:Label ID="lblGameCharacterAttackMaxForce" runat="server"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowGameCharacterDefenceMaxForce" runat="server">
											<asp:TableCell ID="celGameCharacterDefenceMaxForceHeader" runat="server" CssClass="tdHeaderStyle">
												<asp:Label ID="lblGameCharacterDefenceMaxForcepHeader" runat="server" Text="防御部隊数"></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celGameCharacterDefenceMaxForce" runat="server" CssClass="tdDataStyle">
												<asp:Label ID="lblGameCharacterDefenceMaxForce" runat="server"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowGameCharacterCooperation" runat="server">
											<asp:TableCell ID="celGameCharacterCooperationHeader" runat="server" CssClass="tdHeaderStyle">
												<asp:Label ID="lblGameCharacterCooperationHeader" runat="server" Text="連携ﾎﾟｲﾝﾄ"></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celGameCharacterCooperation" runat="server" CssClass="tdDataStyle">
												<asp:Label ID="lblGameCharacterCooperation" runat="server"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowGameCharacterStageNm" runat="server">
											<asp:TableCell ID="celGameCharacterStageNmHeader" runat="server" CssClass="tdHeaderStyle">
												<asp:Label ID="lblGameCharacterStageNmHeader" runat="server" Text="現在のｽﾃｰｼﾞ"></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celGameCharacterStageNm" runat="server" CssClass="tdDataStyle">
												<asp:Label ID="lblGameCharacterStageNm" runat="server"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowGameCharacterRegistDate" runat="server">
											<asp:TableCell ID="celGameCharacterRegistDateHeader" runat="server" CssClass="tdHeaderStyle">
												<asp:Label ID="lblGameCharacterRegistDateHeader" runat="server" Text="登録日"></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celGameCharacterRegistDate" runat="server" CssClass="tdDataStyle">
												<asp:Label ID="lblGameCharacterRegistDate" runat="server"></asp:Label>
											</asp:TableCell>
										</asp:TableRow>
										<asp:TableRow ID="rowGameCharacterMainteLink" runat="server">
											<asp:TableCell ID="celGameCharacterMainteLinkHeader" runat="server" CssClass="tdHeaderStyle2">
												<asp:Label ID="lblGameCharacterMainteLinkHeader" runat="server" Text="ｹﾞｰﾑｷｬﾗｸﾀｰ情報更新"></asp:Label>
											</asp:TableCell>
											<asp:TableCell ID="celGameCharacterMainteLink" runat="server" CssClass="tdDataStyle">
												<asp:HyperLink ID="lnkGameCharacterMainteLink" runat="server" NavigateUrl='' Text="更新実行"></asp:HyperLink>
											</asp:TableCell>
										</asp:TableRow>
									</asp:Table>
								</asp:PlaceHolder>
							</fieldset>
							<asp:Panel runat="server" ID="pnlPossessionGameItem" Visible="false">
								<fieldset class="fieldset-inner">
									<legend>[所持ゲームアイテム一覧]</legend>
									<asp:GridView ID="grdPossessionGameItem" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsPossessionGameItem" AllowSorting="True"
										SkinID="GridViewColor" PageSize="15">
										<Columns>
											<asp:BoundField DataField="GAME_ITEM_CATEGORY_NM" HeaderText="ｱｲﾃﾑｶﾃｺﾞﾘ名">
												<ItemStyle HorizontalAlign="Left" />
											</asp:BoundField>
											<asp:BoundField DataField="GAME_ITEM_NM" HeaderText="ｱｲﾃﾑ名">
												<ItemStyle HorizontalAlign="Left" />
											</asp:BoundField>
											<asp:BoundField DataField="POSSESSION_COUNT" DataFormatString="{0:N0}" HeaderText="所持数">
												<ItemStyle HorizontalAlign="Right" />
											</asp:BoundField>
											<asp:TemplateField HeaderText="現在耐久力">
												<ItemTemplate>
													<asp:Label ID="lblEndurance" runat="server" Text='<%# Eval("NOW_ENDURANCE") + "/" + Eval("ENDURANCE") %>'></asp:Label>
												</ItemTemplate>
												<ItemStyle HorizontalAlign="Right" />
											</asp:TemplateField>
										</Columns>
										<PagerSettings Mode="NumericFirstLast" />
									</asp:GridView>
									<asp:Button runat="server" ID="btnClosePossessionGameItem" Text="閉じる" CssClass="seekbutton" OnClick="btnClosePossessionGameItem_Click" />
									<asp:Button runat="server" ID="btnSeekPossessionGameItem" Text="検索" CssClass="seekbutton" OnClick="btnSeekPossessionGameItem_Click" />
								</fieldset>
							</asp:Panel>
						</td>
						<td>
							<asp:Panel runat="server" ID="pnlAddGamePoint">
								<fieldset class="fieldset-inner">
									<legend>[ゲーム用ポイント追加]</legend>
									<table border="0" style="width: 520px" class="tableStyle">
										<tr>
											<td class="tdHeaderStyle2">
												追加ポイント
											</td>
											<td class="tdDataStyle">
												<asp:TextBox ID="txtAddGamePoint" runat="server" MaxLength="6" Width="46px"></asp:TextBox>円
												<asp:RequiredFieldValidator ID="vdrAddGamePoint" runat="server" ErrorMessage="追加ポイントを入力してください。" ControlToValidate="txtAddGamePoint" ValidationGroup="AddGamePoint"
													Display="Dynamic">*</asp:RequiredFieldValidator>
												<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender15" TargetControlID="vdrAddGamePoint" HighlightCssClass="validatorCallout" />
												<asp:RegularExpressionValidator ID="vdeAddGamePoint" runat="server" ErrorMessage="追加ポイントは数値で入力してください。" ControlToValidate="txtAddGamePoint" ValidationExpression="^[-]?\d+$"
													ValidationGroup="AddGamePoint" Display="Dynamic">*</asp:RegularExpressionValidator>
												<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9" TargetControlID="vdeAddGamePoint" HighlightCssClass="validatorCallout" />
												<asp:CustomValidator ID="vdcAddGamePoint" runat="server" ErrorMessage="ポイントの合計が0以上になるように入力してください" ControlToValidate="txtAddGamePoint" OnServerValidate="vdcAddGamePoint_ServerValidate"
													ValidationGroup="AddGamePoint" Display="Dynamic">*</asp:CustomValidator>
												<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdcAddGamePoint" HighlightCssClass="validatorCallout" />
											</td>
										</tr>
									</table>
									<asp:Button runat="server" ID="btnAddGamePoint" Text="ポイント追加" CssClass="seekbutton" ValidationGroup="AddGamePoint" OnClick="btnAddGamePoint_Click" />
								</fieldset>
							</asp:Panel>
							<asp:Panel runat="server" ID="pnlAddCastGamePoint">
								<fieldset class="fieldset-inner">
									<legend>[プレミアムポイント追加]</legend>
									<table border="0" style="width: 520px" class="tableStyle">
										<tr>
											<td class="tdHeaderStyle2">
												追加ポイント
											</td>
											<td class="tdDataStyle">
												<asp:TextBox ID="txtAddCastGamePoint" runat="server" MaxLength="6" Width="46px"></asp:TextBox>pt
												<asp:RequiredFieldValidator ID="vdrAddCastGamePoint" runat="server" ErrorMessage="追加ポイントを入力してください。" ControlToValidate="txtAddCastGamePoint" ValidationGroup="AddCastGamePoint"
													Display="Dynamic">*</asp:RequiredFieldValidator>
												<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender14" TargetControlID="vdrAddCastGamePoint" HighlightCssClass="validatorCallout" />
												<asp:RegularExpressionValidator ID="vdeAddCastGamePoint" runat="server" ErrorMessage="追加ポイントは数値で入力してください。" ControlToValidate="txtAddCastGamePoint" ValidationExpression="^[-]?\d+$"
													ValidationGroup="AddCastGamePoint" Display="Dynamic">*</asp:RegularExpressionValidator>
												<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10" TargetControlID="vdeAddCastGamePoint" HighlightCssClass="validatorCallout" />
												<asp:CustomValidator ID="vdcAddCastGamePoint" runat="server" ErrorMessage="現在のポイントとの合計が0以上になるように入力してください。" ControlToValidate="txtAddCastGamePoint" OnServerValidate="vdcAddCastGamePoint_ServerValidate"
													ValidationGroup="AddCastGamePoint" Display="Dynamic">*</asp:CustomValidator>
												<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8" TargetControlID="vdcAddCastGamePoint" HighlightCssClass="validatorCallout" />
											</td>
										</tr>
									</table>
									<asp:Button runat="server" ID="btnAddCastGamePoint" Text="ポイント追加" CssClass="seekbutton" ValidationGroup="AddCastGamePoint" OnClick="btnAddCastGamePoint_Click" />
								</fieldset>
							</asp:Panel>
							<asp:Panel runat="server" ID="pnlAddExp">
								<fieldset class="fieldset-inner">
									<legend>[経験値追加]</legend>
									<table border="0" style="width: 520px" class="tableStyle">
										<tr>
											<td class="tdHeaderStyle2">
												追加経験値
											</td>
											<td class="tdDataStyle">
												<asp:TextBox ID="txtAddExp" runat="server" MaxLength="6" Width="46px"></asp:TextBox>
												<asp:RequiredFieldValidator ID="vdrAddExp" runat="server" ErrorMessage="追加経験値を入力してください。" ControlToValidate="txtAddExp" ValidationGroup="AddExp" Display="Dynamic">*</asp:RequiredFieldValidator>
												<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender17" TargetControlID="vdrAddExp" HighlightCssClass="validatorCallout" />
												<asp:RegularExpressionValidator ID="vdeAddExp" runat="server" ErrorMessage="追加経験値は1以上の数値で入力してください。" ControlToValidate="txtAddExp" ValidationExpression="[1-9]\d*"
													ValidationGroup="AddExp" Display="Dynamic">*</asp:RegularExpressionValidator>
												<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender18" TargetControlID="vdeAddExp" HighlightCssClass="validatorCallout" />
											</td>
										</tr>
									</table>
									<asp:Button runat="server" ID="btnAddExp" Text="追加する" CssClass="seekbutton" ValidationGroup="AddExp" OnClick="btnAddExp_Click" />
								</fieldset>
							</asp:Panel>
							<asp:Panel runat="server" ID="pnlAddGameItem">
								<fieldset class="fieldset-inner">
									<legend>[ゲーム用アイテム追加]</legend>
									<table border="0" style="width: 650px" class="tableStyle">
										<tr>
											<td class="tdHeaderStyle">
												アイテム
											</td>
											<td class="tdDataStyle">
												<asp:DropDownList ID="lstGameItemCategory" runat="server" DataSourceID="dsGameItemCategory" OnDataBound="lst_DataBound" OnSelectedIndexChanged="lstGameItemCategory_IndexChanged"
													DataTextField="GAME_ITEM_CATEGORY_NM" DataValueField="GAME_ITEM_CATEGORY_TYPE" Width="170px" AutoPostBack="True">
												</asp:DropDownList>
												<asp:DropDownList ID="lstItemGetCd" runat="server" DataSourceID="dsItemGet" DataTextField="CODE_NM"
													DataValueField="CODE" OnDataBound="lstItemGetCd_DataBound" OnSelectedIndexChanged="lstItemGetCd_IndexChanged" AutoPostBack="True">
												</asp:DropDownList>
												<asp:DropDownList ID="lstItemPresent" runat="server" OnSelectedIndexChanged="lstItemPresent_IndexChanged" AutoPostBack="True">
													<asp:ListItem Value=""></asp:ListItem>
													<asp:ListItem Value="0">通常</asp:ListItem>
													<asp:ListItem Value="1">ﾌﾟﾚｾﾞﾝﾄ</asp:ListItem>
												</asp:DropDownList>
												<asp:DropDownList ID="lstGameItem" runat="server" DataSourceID="dsGameItem" DataTextField="GAME_ITEM_NM" OnDataBound="lst_DataBound" DataValueField="GAME_ITEM_SEQ"
													Width="170px">
												</asp:DropDownList>
												<asp:RequiredFieldValidator ID="vdrGameItem" runat="server" ErrorMessage="付与するアイテムを選んでください。" ControlToValidate="lstGameItem" ValidationGroup="AddGameItem"
													Display="Dynamic">*</asp:RequiredFieldValidator>
												<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11" TargetControlID="vdrGameItem" HighlightCssClass="validatorCallout" />
											</td>
										</tr>
										<tr>
											<td class="tdHeaderStyle2">
												個数
											</td>
											<td class="tdDataStyle">
												<asp:TextBox ID="txtGameItemGiveCount" runat="server" MaxLength="10" Width="60px"></asp:TextBox>
												<asp:RequiredFieldValidator ID="vdrGameItemGiveCount" runat="server" ErrorMessage="個数を入力してください" ControlToValidate="txtGameItemGiveCount" ValidationGroup="AddGameItem"
													Display="Dynamic">*</asp:RequiredFieldValidator>
												<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender12" TargetControlID="vdrGameItemGiveCount" HighlightCssClass="validatorCallout" />
												<asp:RegularExpressionValidator ID="vdeGameItemGiveCount" runat="server" ErrorMessage="個数は数値で入力してください" ControlToValidate="txtGameItemGiveCount" ValidationExpression="^[-]?\d+$"
													ValidationGroup="AddGameItem" Display="Dynamic">*</asp:RegularExpressionValidator>
												<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender13" TargetControlID="vdeGameItemGiveCount" HighlightCssClass="validatorCallout" />
												<asp:RangeValidator ID="vdaGameItemGivCount" runat="server" ErrorMessage="数値は1以上で入力してください" ControlToValidate="txtGameItemGiveCount" MinimumValue="1" MaximumValue="9999999999"
													ValidationGroup="AddGameItem" Display="Dynamic">*</asp:RangeValidator>
												<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender16" TargetControlID="vdaGameItemGivCount" HighlightCssClass="validatorCallout" />
											</td>
										</tr>
									</table>
									<asp:Button runat="server" ID="btnAddGameItem" Text="追加する" CssClass="seekbutton" ValidationGroup="AddGameItem" OnClick="btnAddGameItem_Click" />
									<asp:LinkButton runat="server" ID="lnkPossessionGameItem" Text="所持アイテム一覧" CssClass="seekbutton" OnClick="lnkPossessionGameItem_Click"></asp:LinkButton>
								</fieldset>
							</asp:Panel>
						</td>
					</tr>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlFanClubStatus">
					<tr style="margin-top: 0px">
						<td>
							<div id="anchorFanClubStatus">
							</div>
							<fieldset class="fieldset-inner">
								<legend>[ﾌｧﾝｸﾗﾌﾞ会員一覧]</legend>
								<asp:Button runat="server" ID="btnCloseFanClubStatus" Text="閉じる" OnClick="btnCloseFanClubStatus_Click" />
								<asp:Label ID="lblFanClubStatusSite" runat="server" Font-Bold="True" Font-Underline="True" ForeColor="#FF8000"></asp:Label>
								<asp:GridView ID="grdFanClubStatus" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsFanClubStatus" AllowSorting="True" SkinID="GridViewColor">
									<Columns>
										<asp:BoundField DataField="ADMISSION_DATE" HeaderText="入会日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:TemplateField HeaderText="ハンドル名">
											<ItemStyle HorizontalAlign="Left" />
											<ItemTemplate>
												<asp:HyperLink ID="lnkManView" runat="server" NavigateUrl='<%# string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>'
													Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>' Enabled='<% #IsAdmin() %>'></asp:HyperLink>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:BoundField DataField="EXPIRE_DATE" HeaderText="FC有効期限" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:TemplateField HeaderText="会員ﾗﾝｸ">
											<ItemTemplate>
												<asp:Label ID="lblMemberRank" runat="server" Text='<%# GetFanClubMemberRankNm(Eval("MEMBER_RANK")) %>' />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:BoundField DataField="MEMBER_RANK_EXPIRE_DATE" HeaderText="会員ﾗﾝｸ有効期限" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:BoundField DataField="COIN_COUNT" HeaderText="ｸﾗﾌﾞｺｲﾝ数">
											<ItemStyle HorizontalAlign="Right" />
										</asp:BoundField>
									</Columns>
									<PagerSettings Mode="NumericFirstLast" />
								</asp:GridView>
							</fieldset>
						</td>
					</tr>
				</asp:Panel>
			</table>
			<table border="0">
				<tr style="margin-bottom: 0px">
					<td valign="top">
						<asp:Panel runat="server" ID="pnlInfo">
							<fieldset class="fieldset-inner">
								<legend>
									<%= DisplayWordUtil.Replace("[出演者詳細]") %>
								</legend>
								<asp:DetailsView ID="dvwCast" runat="server" DataSourceID="dsCast" AutoGenerateRows="False" SkinID="DetailsView" Width="440px" OnDataBound="dvwCast_DataBound">
									<Fields>
										<asp:TemplateField HeaderText="IＤ">
											<ItemTemplate>
												<asp:Label ID="Label1" runat="server" Text='<%# Eval("LOGIN_ID") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>(<asp:Label ID="Label6" runat="server" Text='<%# Eval("USER_SEQ") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>)
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="パスワード">
											<ItemTemplate><asp:Label ID="Label9" runat="server" Text='<%# Eval("LOGIN_PASSWORD") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label></ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="名前">
											<ItemTemplate><asp:Label ID="Label10" runat="server" Text='<%# Eval("CAST_NM") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label></ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="名前(カナ)">
											<ItemTemplate><asp:Label ID="Label11" runat="server" Text='<%# Eval("CAST_KANA_NM") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label></ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="認証状態">
											<ItemTemplate>
												<asp:Label ID="Label12" runat="server" Text='<%# Eval("USER_STATUS_NM") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="事業者コード">
											<ItemTemplate>
												<asp:Label ID="Label13" runat="server" Text='<%# Eval("STAFF_ID") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="利用端末">
											<ItemTemplate>
												<asp:Label ID="lblUseTerminalTypeNm" runat="server" Text='<%# Eval("USE_TERMINAL_TYPE_NM") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
												<br />
												<asp:Label ID="lblTerminalNm" runat="server" Text='<%# Eval("MOBILE_TERMINAL_NM")%>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>' CssClass="Warp"
													Width="240px"></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="利用機種">
											<ItemTemplate><asp:Label ID="Label14" runat="server" Text='<%# GetModelName(Eval("MOBILE_CARRIER_CD").ToString(),Eval("MOBILE_TERMINAL_NM").ToString())%>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label></ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="TEL">
											<ItemTemplate><asp:Label ID="Label15" runat="server" Text='<%# Eval("TEL") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label></ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="固体識別番号">
											<ItemTemplate><asp:Label ID="Label16" runat="server" Text='<%# Eval("IMODE_ID") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label></ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="固体識別">
											<ItemTemplate>
												<asp:Label ID="Label17" runat="server" Text='<%# Eval("TERMINAL_UNIQUE_ID") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="URI">
											<ItemTemplate>
												<asp:Label ID="Label18" runat="server" Text='<%# Eval("URI") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="ﾒｰﾙｱﾄﾞﾚｽ">
											<ItemTemplate>
												<asp:Label ID="lblEmail" runat="server" ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>' Text='<%# CheckAdminLevel(Eval("EMAIL_ADDR")) %>' CssClass="Warp"
													Width="300px"></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="ﾒｰﾙｱﾄﾞﾚｽ状態">
											<ItemTemplate>
												<asp:Label ID="lblEmailNg" runat="server" Text='<%# CheckAddrNg(Eval("NON_EXIST_MAIL_ADDR_FLAG"))%>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="MULLER3送信">
											<ItemTemplate>
												<asp:Label ID="lblMuller3Ng" runat="server" Text='<%# CheckAddrNgM3(Eval("TX_MULLER3_NG_MAIL_ADDR_FLAG"))%>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="広告コード">
											<ItemTemplate>
												<asp:Label ID="lblAdCd" runat="server" Text='<%# GetAdCd(Eval("USER_SEQ")) %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="ユーザーランク">
											<ItemTemplate>
												<asp:Label ID="lblUserRank" runat="server" Text='<%# Eval("USER_RANK") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="プロダクション">
											<ItemTemplate>
												<asp:Label ID="Label20" runat="server" Text='<%# Eval("PRODUCTION_NM") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="担当">
											<ItemTemplate>
												<asp:Label ID="Label21" runat="server" Text='<%# Eval("MANAGER_NM") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="累計報酬金額">
											<ItemTemplate>
												<asp:Label runat="server" Text='<%# string.Format("{0} ({1})",Eval("TOTAL_PAYMENT_AMT"),Eval("TOTAL_PAYMENT_AMT_NOT_WORK")) %>' ID="lblTotalPaymentAmt"
													ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>' />
												<asp:Label runat="server" Text="()はﾎﾞｰﾅｽ、友達紹介" ID="Label19" ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>' />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="未支払報酬金額">
											<ItemTemplate>
												<asp:Label ID="Label23" runat="server" Text='<%# Eval("NOT_PAYMENT_AMT") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="未支払報酬Pt">
											<ItemTemplate>
												<asp:Label ID="lblNotPaymentPoint" runat="server" Text='<%# Eval("NOT_PAYMENT_POINT") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="精算申請日">
											<ItemTemplate>
												<asp:Label ID="lblReqPaymentDate" runat="server" Text='<%# Eval("REQ_PAYMENT_DATE") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="登録日">
											<ItemTemplate>
												<asp:Label ID="Label24" runat="server" Text='<%# Eval("REGIST_DATE") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="更新日">
											<ItemTemplate>
												<asp:Label ID="Label25" runat="server" Text='<%# Eval("UPDATE_DATE") %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="最終ﾛｸﾞｲﾝ日">
											<ItemTemplate>
												<asp:Label ID="Label27" runat="server" Text='<%# GetLastLoginDate(Eval("USER_SEQ")) %>' ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
                                        <asp:TemplateField HeaderText="基本情報更新">
											<ItemTemplate>
												<asp:HyperLink ID="lnkUpdate" runat="server" NavigateUrl='<%# string.Format("CastBasicMainte.aspx?userseq={0}&loginid={1}&return={2}",Eval("USER_SEQ"),Eval("LOGIN_ID"),ViewState["RETURN"].ToString()) %>'
													Text="更新実行" Enabled="true"></asp:HyperLink>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="直ﾛｸﾞｲﾝ">
											<ItemTemplate>
												<asp:LinkButton ID="lnkLogin" runat="server" CommandArgument='<%# string.Format("{0}:{1}",Eval("LOGIN_ID"),Eval("LOGIN_PASSWORD")) %>' OnCommand="lnkLogin_Command"
													Text="直ﾛｸﾞｲﾝ"></asp:LinkButton>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="紹介者情報">
											<ItemTemplate>
												<asp:HyperLink ID="lnkIntroducerData" runat="server" Text='<%# Eval("INTRODUCER_FRIEND_CD") %>' NavigateUrl='<%# GetIntroducerLink(Eval("INTRODUCER_FRIEND_CD")) %>'></asp:HyperLink>
											</ItemTemplate>
										</asp:TemplateField>
                                        <asp:TemplateField HeaderText="CrosmileVer">
                                            <ItemTemplate>
                                                <asp:Label ID="Label28" runat="server" ForeColor='<%# GetUserStatusColor(Eval("USER_STATUS")) %>'
                                                    Text='<%# Eval("CROSMILE_LAST_USED_VERSION") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="GCｱﾌﾟﾘVer">
                                            <ItemTemplate>
												<asp:Label ID="lblGcappLastLoginVersion" runat="server" Text='<%# Eval("GCAPP_LAST_LOGIN_VERSION") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="GCｱﾌﾟﾘﾛｸﾞｲﾝ日">
                                            <ItemTemplate>
												<asp:Label ID="lblGcappLastLoginDate" runat="server" Text='<%# Eval("GCAPP_LAST_LOGIN_DATE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
										<asp:TemplateField HeaderText="旧ID">
											<ItemTemplate>
												<asp:Label ID="lblBeforeSystemId" runat="server" Text='<%# Eval("BEFORE_SYSTEM_ID") %>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="ﾃﾞﾊﾞｲｽﾄｰｸﾝ">
											<ItemTemplate>
												<asp:Label ID="lblDeviceToken" runat="server" Text='<%# Eval("DEVICE_TOKEN") %>' CssClass="Warp" Width="240px"></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
									</Fields>
									<HeaderStyle Wrap="False" />
								</asp:DetailsView>
							</fieldset>
						</asp:Panel>
					</td>
					<td valign="top">
						<asp:Panel runat="server" ID="pnlRemarks">
							<fieldset class="fieldset-inner">
								<legend>[備考]</legend>
								<table border="0" style="width: 540px" class="tableStyle">
									<tr>
										<td class="tdHeaderStyle2">
											備考
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtRemarks1" runat="server" MaxLength="1000" Width="380px" Height="280px" TextMode="MultiLine"></asp:TextBox><br />
											<asp:TextBox ID="txtRemarks2" runat="server" MaxLength="256" Width="380px"></asp:TextBox><br />
											<asp:TextBox ID="txtRemarks3" runat="server" MaxLength="256" Width="380px"></asp:TextBox>
										</td>
									</tr>
								</table>
								<asp:Button runat="server" ID="btnRemarks" Text="備考更新" CssClass="seekbutton" OnClick="btnRemarks_Click" />
							</fieldset>
						</asp:Panel>
						<asp:Panel runat="server" ID="pnlBonusRemark" HorizontalAlign="Left">
							<asp:Panel runat="server" ID="pnlBonusPoint">
								<fieldset class="fieldset-inner">
									<legend>[ボーナス報酬]</legend>
									<table border="0" style="width: 540px" class="tableStyle">
										<tr>
											<td class="tdHeaderStyle">
												ボーナスポイント
											</td>
											<td class="tdDataStyle">
												<asp:TextBox ID="txtBonusPointAmt" runat="server" MaxLength="6" Width="46px"></asp:TextBox>
												<asp:RegularExpressionValidator ID="vdeBonusPointAmt" runat="server" ErrorMessage="ボーナス報酬は数値で入力してください。" ControlToValidate="txtBonusPointAmt" ValidationExpression="^[+-]?\d+$"
													ValidationGroup="BonusPoint" Display="Dynamic">*</asp:RegularExpressionValidator>
												<asp:CustomValidator ID="vdcBonusPointAmt" runat="server" ErrorMessage="0以外の数値を入力してください" OnServerValidate="vdcBonusPointAmt_ServerValidate" ValidationGroup="BonusPoint"></asp:CustomValidator>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderStyle">
												備考
											</td>
											<td class="tdDataStyle">
												<asp:TextBox ID="txtRemarks" runat="server" MaxLength="80" Width="320"></asp:TextBox>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderStyle2">
												累計報酬金額
											</td>
											<td class="tdDataStyle">
												<asp:RadioButton ID="rdoTotalPayAmtNg" runat="server" GroupName="TotalPayAmt" Text="増減しない">
												</asp:RadioButton>
												<asp:RadioButton ID="rdoTotalPayAmtOk" runat="server" Checked="True" GroupName="TotalPayAmt" Text="増減する">
												</asp:RadioButton>
											</td>
										</tr>
									</table>
									<asp:Button runat="server" ID="btnBonusPoint" Text="ボーナス報酬更新" CssClass="seekbutton" OnClick="btnBonusPoint_Click" ValidationGroup="BonusPoint" />
								</fieldset>
							</asp:Panel>
						</asp:Panel>
					</td>
				</tr>
			</table>
			<table>
				<tr>
					<td valign="top">
						<fieldset class="fieldset-inner">
							<legend>[待機履歴]</legend>
							<asp:Panel runat="server" ID="pnlLogin">
								<asp:GridView ID="grdLogin" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsLoginCharacter" AllowSorting="True" SkinID="GridViewColor">
									<Columns>
										<asp:BoundField DataField="START_DATE" HeaderText="開始" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" Width="100px" />
										</asp:BoundField>
										<asp:BoundField DataField="LOGOFF_DATE" HeaderText="終了" DataFormatString="{0:HH:mm}" HtmlEncode="False">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:BoundField DataField="SITE_CD" HeaderText="ｻｲﾄ">
											<ItemStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:TemplateField HeaderText="TV<br />分/回">
											<ItemTemplate>
												<asp:Label ID="Label7" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("PRV_TV_TALK_MIN"),Eval("PRV_TV_TALK_COUNT")) %>' Width="50px"></asp:Label>
											</ItemTemplate>
											<ItemStyle HorizontalAlign="Right" />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:TemplateField>
										<asp:TemplateField HeaderText="TVﾁｬｯﾄ<br />分/回">
											<ItemTemplate>
												<asp:Label ID="Label8" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("PUB_TV_TALK_MIN"),Eval("PUB_TV_TALK_COUNT")) %>' Width="50px"></asp:Label>
											</ItemTemplate>
											<ItemStyle HorizontalAlign="Right" />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:TemplateField>
										<asp:TemplateField HeaderText="音声<br />分/回">
											<ItemTemplate>
												<asp:Label ID="Label8" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("PRV_VOICE_TALK_MIN"),Eval("PRV_VOICE_TALK_COUNT")) %>' Width="50px"></asp:Label>
											</ItemTemplate>
											<ItemStyle HorizontalAlign="Right" />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:TemplateField>
										<asp:TemplateField HeaderText="音声ﾁｬｯﾄ<br />分/回">
											<ItemTemplate>
												<asp:Label ID="Label8" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("PUB_VOICE_TALK_MIN"),Eval("PUB_VOICE_TALK_COUNT")) %>' Width="50px"></asp:Label>
											</ItemTemplate>
											<ItemStyle HorizontalAlign="Right" />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:TemplateField>
										<asp:TemplateField HeaderText="会話ﾓﾆﾀ<br />分/回">
											<ItemTemplate>
												<asp:Label ID="Label8" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("VIEW_TALK_MIN"),Eval("VIEW_TALK_COUNT")) %>' Width="50px"></asp:Label>
											</ItemTemplate>
											<ItemStyle HorizontalAlign="Right" />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:TemplateField>
										<asp:TemplateField HeaderText="音声ﾓﾆﾀ<br />分/回">
											<ItemTemplate>
												<asp:Label ID="Label8" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("WIRETAP_MIN"),Eval("WIRETAP_COUNT")) %>' Width="50px"></asp:Label>
											</ItemTemplate>
											<ItemStyle HorizontalAlign="Right" />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:TemplateField>
										<asp:TemplateField HeaderText="部屋ﾓﾆﾀ<br />分/回">
											<ItemTemplate>
												<asp:Label ID="Label8" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("VIEW_BROADCAST_MIN"),Eval("VIEW_BROADCAST_COUNT")) %>' Width="50px"></asp:Label>
											</ItemTemplate>
											<ItemStyle HorizontalAlign="Right" />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:TemplateField>
										<asp:TemplateField HeaderText="待機TV<br />分/回">
											<ItemTemplate>
												<asp:Label ID="Label7" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("CAST_PRV_TV_TALK_MIN"),Eval("CAST_PRV_TV_TALK_COUNT")) %>' Width="50px"></asp:Label>
											</ItemTemplate>
											<ItemStyle HorizontalAlign="Right" />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:TemplateField>
										<asp:TemplateField HeaderText="待機TV<br />ﾁｬｯﾄ分/回">
											<ItemTemplate>
												<asp:Label ID="Label8" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("CAST_PUB_TV_TALK_MIN"),Eval("CAST_PUB_TV_TALK_COUNT")) %>' Width="50px"></asp:Label>
											</ItemTemplate>
											<ItemStyle HorizontalAlign="Right" />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:TemplateField>
										<asp:TemplateField HeaderText="待機音声<br />分/回">
											<ItemTemplate>
												<asp:Label ID="Label8" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("CAST_PRV_VOICE_TALK_MIN"),Eval("CAST_PRV_VOICE_TALK_COUNT")) %>' Width="50px"></asp:Label>
											</ItemTemplate>
											<ItemStyle HorizontalAlign="Right" />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:TemplateField>
										<asp:TemplateField HeaderText="待機音声<br />ﾁｬｯﾄ分/回">
											<ItemTemplate>
												<asp:Label ID="Label8" runat="server" Text='<%#string.Format("{0}({1,2})",Eval("CAST_PUB_VOICE_TALK_MIN"),Eval("CAST_PUB_VOICE_TALK_COUNT")) %>' Width="50px"></asp:Label>
											</ItemTemplate>
											<ItemStyle HorizontalAlign="Right" />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:TemplateField>
									</Columns>
									<PagerSettings Mode="NumericFirstLast" />
								</asp:GridView>
							</asp:Panel>
						</fieldset>
					</td>
				</tr>
			</table>
		</asp:Panel>
	</asp:Panel>
	<asp:ObjectDataSource ID="dsCast" runat="server" SelectMethod="GetOne" TypeName="Cast" OnSelected="dsCast_Selected" OnSelecting="dsCast_Selecting">
		<SelectParameters>
			<asp:ControlParameter ControlID="txtLoginId" Name="pLoginID" PropertyName="Text" Type="String" />
			<asp:ControlParameter ControlID="txtUserSeq" Name="pUserSeq" PropertyName="Text" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCharacter" runat="server" SelectMethod="GetCharacterList" TypeName="CastCharacter" OnSelecting="dsCharacter_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pMainCharOnlyFlag" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsWebUsedLog" runat="server" SelectMethod="GetCastPageCollection" TypeName="WebUsedLog" SelectCountMethod="GetCastPageCount"
		EnablePaging="True" OnSelecting="dsWebUsedLog_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
			<asp:Parameter Name="pManLoginId" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsWebUsedLogSummary" runat="server" OnSelecting="dsWebUsedLog_Selecting" SelectMethod="GetCastPointSummaryList" TypeName="WebUsedLog">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
			<asp:Parameter Name="pManLoginId" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUsedLogTalk" runat="server" EnablePaging="True" OnSelecting="dsUsedLogTalk_Selecting" SelectCountMethod="GetPageCount" SelectMethod="GetPageCollection"
		TypeName="UsedLog">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pSexCd" Type="String" />
			<asp:Parameter Name="pLoginId" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pPartnerLoginId" Type="String" />
			<asp:Parameter Name="pPartnerUserCharNo" Type="String" />
			<asp:Parameter Name="pTel" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
			<asp:Parameter Name="pChargeType" Type="String" />
			<asp:Parameter Name="pReportTimeFrom" Type="String" />
			<asp:Parameter Name="pReportTimeTo" Type="String" />
			<asp:Parameter Name="pCallResult" Type="String" />
			<asp:Parameter Name="pTalkSubType" Type="int32" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUsedLogTalkSummary" runat="server" OnSelecting="dsUsedLogTalk_Selecting" SelectMethod="GetTalkPointSummaryList" TypeName="UsedLog">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pSexCd" Type="String" />
			<asp:Parameter Name="pLoginId" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pPartnerLoginId" Type="String" />
			<asp:Parameter Name="pPartnerUserCharNo" Type="String" />
			<asp:Parameter Name="pTel" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
			<asp:Parameter Name="pChargeType" Type="String" />
			<asp:Parameter Name="pReportTimeFrom" Type="String" />
			<asp:Parameter Name="pReportTimeTo" Type="String" />
			<asp:Parameter Name="pCallResult" Type="String" />
			<asp:Parameter Name="pTalkSubType" Type="int32" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsLoginCharacter" runat="server" SelectMethod="GetPageCollection" TypeName="LoginCharacter" SelectCountMethod="GetPageCount"
		EnablePaging="True" OnSelecting="dsLoginCharacter_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastMovie" runat="server" SelectMethod="GetPageCollection" TypeName="CastMovie" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsCastMovie_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pMovieType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsPublicMovie" runat="server" SelectMethod="GetPageCollection" TypeName="CastMovie" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsPublicMovie_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pMovieType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastVoice" runat="server" SelectMethod="GetPageCollection" TypeName="CastVoice" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsCastVoice_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pVoiceType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastBbsMovie" runat="server" SelectMethod="GetPageCollection" TypeName="CastMovie" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsCastBbsMovie_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pMovieType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsBbsLog" runat="server" SelectMethod="GetPageCollection" TypeName="BbsLog" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsBbsLog_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pSexCd" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportTimeFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
			<asp:Parameter Name="pReportTimeTo" Type="String" />
			<asp:Parameter Name="pTxLoginId" Type="String" />
			<asp:Parameter Name="pTxUserCharNo" Type="String" />
			<asp:Parameter Name="pKeyword" Type="String" DefaultValue="" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsTalkMovie" runat="server" SelectMethod="GetPageCollection" TypeName="TalkMovie" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsTalkMovie_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastBbsPic" runat="server" SelectMethod="GetPageCollection" TypeName="CastPic" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsCastBbsPic_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pPicType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastFavorit" runat="server" SelectMethod="GetPageCollection" TypeName="CastFavorit" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsCastFavorit_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter DefaultValue="0" Name="pLikeMeFlag" Type="Int16" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastLikeMe" runat="server" SelectMethod="GetPageCollection" TypeName="CastFavorit" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsCastLikeMe_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter DefaultValue="1" Name="pLikeMeFlag" Type="Int16" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsRefuse" runat="server" SelectMethod="GetPageCollection" TypeName="Refuse" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsRefuse_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter DefaultValue="0" Name="pRefuseMeFlag" Type="Int16" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsRefuseMe" runat="server" SelectMethod="GetPageCollection" TypeName="Refuse" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsRefuse_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter DefaultValue="1" Name="pRefuseMeFlag" Type="Int16" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsMarking" runat="server" SelectMethod="GetCastPageCollection" TypeName="Marking" SelectCountMethod="GetCastPageCount" EnablePaging="True"
		OnSelecting="dsMarking_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="string" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsMailBox" runat="server" SelectMethod="GetPageCollection" TypeName="MailBox" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsMailBox_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pPartnerLoginId" Type="String" />
			<asp:Parameter Name="pPartnerUserCharNo" Type="String" />
			<asp:Parameter Name="pSexCd" Type="String" />
			<asp:Parameter Name="pReportDayTimeFrom" Type="String" />
			<asp:Parameter Name="pReportDayTimeTo" Type="String" />
			<asp:Parameter Name="pAttachedType" Type="String" />
			<asp:Parameter Name="pWithBatchMailFlag" Type="String" />
			<asp:Parameter Name="pTxRxType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsBonusLog" runat="server" SelectMethod="GetPageCollection" TypeName="BonusLog" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsBonusLog_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsPaymentHistory" runat="server" SelectMethod="GetPageCollection" TypeName="PaymentHistory" SelectCountMethod="GetPageCount"
		EnablePaging="True" OnSelecting="dsPaymentHistory_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUserDefPointHistory" runat="server" SelectMethod="GetPageCollection" TypeName="UserDefPointHistory" SelectCountMethod="GetPageCount"
		EnablePaging="True" OnSelecting="dsUserDefPointHistory_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsDMMail" runat="server" SelectMethod="GetListByTemplateType" TypeName="MailTemplate">
		<SelectParameters>
			<asp:ControlParameter ControlID="lblCreateMailVisibleSiteCd" Name="pSiteCd" PropertyName="Text" Type="String" />
			<asp:Parameter DefaultValue="19" Name="pMailTemplateType" Type="String" />
			<asp:Parameter DefaultValue="2" Name="pSexCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsManFriendLogs" runat="server" EnablePaging="true" OnSelecting="dsManFriendLogs_Selecting" SelectCountMethod="GetManLogPageCount" SelectMethod="GetManLogPageCollection"
		TypeName="Friends">
		<SelectParameters>
			<asp:Parameter Name="pFriendIntroCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsManFriends" runat="server" EnablePaging="true" OnSelecting="dsManFriends_Selecting" SelectCountMethod="GetManPageCount" SelectMethod="GetManPageCollection"
		TypeName="Friends">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pFriendIntroCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastFriends" runat="server" EnablePaging="true" OnSelecting="dsCastFriends_Selecting" SelectCountMethod="GetCastPageCount"
		SelectMethod="GetCastPageCollection" TypeName="Friends">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pFriendIntroCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsModifyMailHis" runat="server" EnablePaging="true" OnSelecting="dsModifyMailHis_Selecting" SelectCountMethod="GetModifyMailHistoryCount"
		SelectMethod="GetModifyMailHistory" TypeName="UserHistory">
		<SelectParameters>
			<asp:Parameter Name="pUserSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsNgMailHistory" runat="server" EnablePaging="true" OnSelecting="dsNgMailHistory_Selecting" SelectCountMethod="GetPageCount"
		SelectMethod="GetPageCollection" TypeName="NgMailHistory">
		<SelectParameters>
			<asp:Parameter Name="pUserSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsModifyTelHis" runat="server" EnablePaging="true" OnSelecting="dsModifyTelHis_Selecting" SelectCountMethod="GetModifyTelHistoryCount"
		SelectMethod="GetModifyTelHistory" TypeName="UserHistory">
		<SelectParameters>
			<asp:Parameter Name="pUserSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsModifyLoginPassword" runat="server" EnablePaging="false" OnSelecting="dsModifyLoginPassword_Selecting" SelectMethod="GetCastModifyLoginPassword" TypeName="UserHistory">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUsedLogMonitor" runat="server" EnablePaging="True" OnSelecting="dsUsedLogMonitor_Selecting" SelectCountMethod="GetMonitorPageCount"
		SelectMethod="GetMonitorPageCollection" TypeName="UsedLog">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pSexCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUsedLogMonitorSummary" runat="server" OnSelecting="dsUsedLogMonitor_Selecting" SelectMethod="GetMonitorPointSummaryList" TypeName="UsedLog">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pSexCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsGameItemCategory" runat="server" SelectMethod="GetList" TypeName="GameItemCategory" OnSelecting="dsGameItemCategory_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsGameItem" runat="server" SelectMethod="GetListByCategoryType" TypeName="GameItem" OnSelecting="dsGameItem_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pSexCd" Type="String" DefaultValue="" />
			<asp:Parameter Name="pGameItemCategoryType" Type="String" DefaultValue="" />
			<asp:Parameter Name="pGameItemGetCd" Type="String" DefaultValue="" />
            <asp:Parameter Name="pPresentFlag" Type="String" DefaultValue="" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsItemGet" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="A2" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsPossessionGameItem" runat="server" EnablePaging="true" OnSelecting="dsPossessionGameItem_Selecting" SelectCountMethod="GetPageCount"
		SelectMethod="GetPageCollection" TypeName="PossessionGameItem">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsFanClubStatus" runat="server" SelectMethod="GetPageCollection" TypeName="FanClubStatus" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsFanClubStatus_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsWithdrawalHistoryList" runat="server" SelectMethod="GetPageCollection" TypeName="WithdrawalHistory" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelecting="dsWithdrawalHistoryList_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
            <asp:Parameter Name="pUserSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeBonusPointAmt" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrReportDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdrReportDayTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdcReportDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdrMailTitle" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vdrMailTemplate" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender6" runat="server" TargetControlID="btnDelWithdrawal" ConfirmText="退会申請の削除を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender7" runat="server" TargetControlID="btnUpdateWithdrawal" ConfirmText="退会申請の備考を更新しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender8" runat="server" TargetControlID="btnAcceptWithdrawal" ConfirmText="退会申請を受理しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender10" runat="server" TargetControlID="btnSendDetainMail" ConfirmText="引き留めメールを送信して継続ステータスに更新しても宜しいですか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender14" runat="server" TargetControlID="btnRegistWithdrawalHistory" ConfirmText="退会履歴を登録しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender11" runat="server" TargetControlID="btnUpdateWithdrawalHistory" ConfirmText="退会履歴を更新しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender15" runat="server" TargetControlID="btnDeleteWithdrawalHistory" ConfirmText="退会履歴を削除しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Custom, Numbers" TargetControlID="txtBonusPointAmt"
		ValidChars="-+" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnBonusPoint" ConfirmText="ボーナス報酬更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnRemarks" ConfirmText="備考の更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender3" runat="server" TargetControlID="btnAddGamePoint" ConfirmText="ゲーム用ポイントを追加しても宜しいですか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender4" runat="server" TargetControlID="btnAddCastGamePoint" ConfirmText="プレミアムポイントを追加しても宜しいですか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender5" runat="server" TargetControlID="btnAddExp" ConfirmText="経験値を追加しても宜しいですか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender9" runat="server" TargetControlID="btnAddGameItem" ConfirmText="アイテムを追加しても宜しいですか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:MaskedEditExtender ID="mskReportDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtReportDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="makReportDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtReportDayTo">
	</ajaxToolkit:MaskedEditExtender>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtUserSeq" />
</asp:Content>
