﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者詳細
--	Progaram ID		: CastView
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Drawing;
using System.Web.Security;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using MobileLib;
using ViComm;
using ViComm.Extension.Pwild;
using System.Text;

public partial class Cast_CastView:System.Web.UI.Page {
	private string siteCd = "";
	private string profileMovieRegistSite = "";
	private string publicMovieRegistSite = "";
	private string saleVoiceRegistSite = "";
	private string bbsMovieRegistSite = "";
	private string talkMovieRegistSite = "";
	private string userCharNo = "";
	private string userSeq = "";

	private bool DisablePinEdit {
		get {
			return (bool)this.ViewState["DisablePinEdit"];
		}
		set {
			this.ViewState["DisablePinEdit"] = value;
		}
	}
	private string GameItemGetCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemGetCd"]);
		}
		set {
			this.ViewState["GameItemGetCd"] = value;
		}
	}

	private string GameItemPresent {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemPresent"]);
		}
		set {
			this.ViewState["GameItemPresent"] = value;
		}
	}

	private int CompanyMask {
		get { return int.Parse(Session["CompanyMask"].ToString()); }
	}

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		txtMailDocHtml.EditorDirectory = string.Format("http://{0}:{1}/pinEdit/editor/",Request.UrlReferrer.Host,Request.UrlReferrer.Port);

		if (!IsPostBack) {
			using (ManageCompany oManageCompany = new ManageCompany()) {
				this.DisablePinEdit = oManageCompany.IsAvailableService(ViCommConst.RELEASE_DISABLE_PINEDIT,2);
			}
			ViewState["CAST_LOGIN_ID"] = iBridUtil.GetStringValue(Request.QueryString["loginid"]);
			siteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			userSeq = iBridUtil.GetStringValue(Request.QueryString["userseq"]);
			profileMovieRegistSite = iBridUtil.GetStringValue(Request.QueryString["moviesite"]);
			publicMovieRegistSite = iBridUtil.GetStringValue(Request.QueryString["pmoviesite"]);
			saleVoiceRegistSite = iBridUtil.GetStringValue(Request.QueryString["voicesite"]);
			bbsMovieRegistSite = iBridUtil.GetStringValue(Request.QueryString["bbsmoviesite"]);
			talkMovieRegistSite = iBridUtil.GetStringValue(Request.QueryString["talkmoviesite"]);
			userCharNo = iBridUtil.GetStringValue(Request.QueryString["usercharno"]);
			ViewState["SITE_CD"] = siteCd;
			ViewState["RETURN"] = iBridUtil.GetStringValue(Request.QueryString["return"]);
			ViewState["MAN_LOGIN_ID"] = iBridUtil.GetStringValue(Request.QueryString["manloginid"]);
			ViewState["MAN_SITE_CD"] = iBridUtil.GetStringValue(Request.QueryString["mansite"]);
			InitPage();
			GetData();
		}
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		lblNotFound.Visible = false;
		lblManyUser.Visible = false;
		if (IsValid) {
			pnlDtl.Visible = true;
			pnlCharacter.Visible = true;
			dvwCast.DataSourceID = "dsCast";
			CloseExtInfo();
			txtBonusPointAmt.Text = "";
			txtRemarks.Text = "";
			txtLoginId.Text = TrimEnd(txtLoginId.Text);
			txtUserSeq.Text = TrimEnd(txtUserSeq.Text);
			rdoTotalPayAmtNg.Checked = false;
			rdoTotalPayAmtOk.Checked = true;
			txtAddCastGamePoint.Text = string.Empty;
			txtAddExp.Text = string.Empty;
			txtAddGamePoint.Text = string.Empty;
			txtGameItemGiveCount.Text = string.Empty;
		}
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		CloseExtInfo();
		Response.Redirect("CastView.aspx");
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		if (GetMultiCharFlag(lstSiteCd.SelectedValue)) {
			Server.Transfer(string.Format("CastCharacterMainte.aspx?userseq={0}&sitecd={1}&return={2}",
						iBridUtil.GetStringValue(ViewState["UserSeq"]),lstSiteCd.SelectedValue,ViewState["RETURN"].ToString()));
		} else {
			Server.Transfer(string.Format("CastCharacterMainte.aspx?userseq={0}&sitecd={1}&usercharno={2}&return={2}",
						iBridUtil.GetStringValue(ViewState["UserSeq"]),lstSiteCd.SelectedValue,ViCommConst.MAIN_CHAR_NO,ViewState["RETURN"].ToString()));
		}
	}

	protected void lnkProfileMovie_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		string sSiteCd = sKeys[0];
		string sUserCharNo = sKeys[1];
		DisplayProfileMovieInfo(sSiteCd,sUserCharNo);
	}

	protected void lnkPublicMovie_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		string sSiteCd = sKeys[0];
		string sUserCharNo = sKeys[1];
		DisplayPublicMovieInfo(sSiteCd,sUserCharNo);
	}
	protected void lnkSaleVoice_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		string sSiteCd = sKeys[0];
		string sUserCharNo = sKeys[1];
		DisplaySaleVoiceInfo(sSiteCd,sUserCharNo);
	}

	protected void lnkBbsMovie_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		string sSiteCd = sKeys[0];
		string sUserCharNo = sKeys[1];
		DisplayBbsMovieInfo(sSiteCd,sUserCharNo);
	}

	protected void lnkTalkMovie_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		string sSiteCd = sKeys[0];
		string sUserCharNo = sKeys[1];
		DisplayTalkMovieInfo(sSiteCd,sUserCharNo);
	}

	protected void lnkBbsPic_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		string sSiteCd = sKeys[0];
		string sUserCharNo = sKeys[1];
		DisplayBbsPicInfo(sSiteCd,sUserCharNo);
	}

	protected void lnkFavorit_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		string sSiteCd = sKeys[0];
		string sUserCharNo = sKeys[1];
		DisplayFavoritInfo(sSiteCd,sUserCharNo);
	}

	protected void lnkLikeMe_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		string sSiteCd = sKeys[0];
		string sUserCharNo = sKeys[1];
		DisplayLikeMeInfo(sSiteCd,sUserCharNo);
	}

	protected void lnkRefuse_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		string sSiteCd = sKeys[0];
		string sUserCharNo = sKeys[1];
		DisplayRefuseInfo(sSiteCd,sUserCharNo);
	}

	protected void lnkRefuseMe_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		string sSiteCd = sKeys[0];
		string sUserCharNo = sKeys[1];
		DisplayRefuseMeInfo(sSiteCd,sUserCharNo);
	}

	protected void lnkMarking_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		string sSiteCd = sKeys[0];
		string sUserCharNo = sKeys[1];
		DisplayMarkingInfo(sSiteCd,sUserCharNo);
	}

	protected void lnkFriends_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		string sSiteCd = sKeys[0];
		string sUserCharNo = sKeys[1];
		string sFriendIntroCd = sKeys[2];
		DisplayFriendsInfo(sSiteCd,sUserCharNo,sFriendIntroCd);
	}
	protected void lnkModifyMailHis_Command(object sender,CommandEventArgs e) {
		string sUserSeq = e.CommandArgument.ToString();
		DisplayModifyMailHisInfo(sUserSeq);
	}

	protected void lnkMailBox_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		string sSiteCd = sKeys[0];
		string sUserCharNo = sKeys[1];
		DisplayMailBoxInfo(sSiteCd,sUserCharNo);
	}

	protected void lnkBonusPointHis_Command(object sender,CommandEventArgs e) {
		CloseExtInfo();
		SetViewStateReportDay();
		pnlBonusLog.Visible = true;
		grdBonusLog.DataSourceID = "dsBonusLog";
		grdBonusLog.DataBind();
	}

	protected void lnkBlogArticle_Command(object sender,CommandEventArgs e) {
		IDictionary oDataKeys = this.grdCharacter.DataKeys[int.Parse(e.CommandArgument.ToString())].Values;
		string sSiteCd = iBridUtil.GetStringValue(oDataKeys["SITE_CD"]);
		string sUserSeq = iBridUtil.GetStringValue(oDataKeys["USER_SEQ"]);
		string sUserCharNo = iBridUtil.GetStringValue(oDataKeys["USER_CHAR_NO"]);

		using (CastCharacterEx oCastCharacterEx = new CastCharacterEx()) {
			using (DataSet oDataSet = oCastCharacterEx.GetOne(sSiteCd,sUserSeq,sUserCharNo)) {
				if (oDataSet.Tables[0].Rows.Count > 0) {
					DataRow oDataRow = oDataSet.Tables[0].Rows[0];
					if (ViCommConst.FLAG_OFF_STR.Equals(oDataRow["ENABLED_BLOG_FLAG"].ToString())) {
						return;
					}
				}
			}
		}

		this.Response.Redirect(string.Format("~/Extension/BlogArticleList.aspx?sitecd={0}&loginid={1}",sSiteCd,oDataKeys["LOGIN_ID"]));
	}

    protected void lnkGameCharacter_Command(object sender, CommandEventArgs e) {

        IDictionary oDataKeys = this.grdCharacter.DataKeys[int.Parse(e.CommandArgument.ToString())].Values;
        string sSiteCd = iBridUtil.GetStringValue(oDataKeys["SITE_CD"]);
        string sUserSeq = iBridUtil.GetStringValue(oDataKeys["USER_SEQ"]);
        string sUserCharNo = iBridUtil.GetStringValue(oDataKeys["USER_CHAR_NO"]);
        if (IsGamecharacter(sSiteCd, sUserSeq, sUserCharNo)) {
            DisplayGameCharacterInfo(sSiteCd,sUserSeq,sUserCharNo);
        }
    }

	protected void lnkFanClubStatus_Command(object sender,CommandEventArgs e) {
		IDictionary oDataKeys = this.grdCharacter.DataKeys[int.Parse(e.CommandArgument.ToString())].Values;
		string sSiteCd = iBridUtil.GetStringValue(oDataKeys["SITE_CD"]);
		string sUserSeq = iBridUtil.GetStringValue(oDataKeys["USER_SEQ"]);
		string sUserCharNo = iBridUtil.GetStringValue(oDataKeys["USER_CHAR_NO"]);
		DisplayFanClubStatusInfo(sSiteCd,sUserSeq,sUserCharNo);
	}

	protected void btnSeekMailBox_Click(object sender,EventArgs e) {
		if (IsValid) {
			grdMailBox.PageIndex = 0;
			grdMailBox.DataBind();
		}
	}

	protected void btnSeekUsedLog_Click(object sender,EventArgs e) {
		SetViewStateReportDay();

		string sSiteNm = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(iBridUtil.GetStringValue(ViewState["SiteCd"]),"SITE_NM",ref  sSiteNm);
		}
		lblWebFromDay.Text = iBridUtil.GetStringValue(ViewState["ReportDayFrom"]);
		lblWebToDay.Text = iBridUtil.GetStringValue(ViewState["ReportDayTo"]);
		lblWebUsedLogSite.Text = sSiteNm;
		lblWebUsedLogCharNo.Text = " - " + iBridUtil.GetStringValue(ViewState["UserCharNo"]);
		if (GetMultiCharFlag(ViewState["SiteCd"])) {
			lblWebUsedLogCharNo.Visible = true;
		} else {
			lblWebUsedLogCharNo.Visible = false;
		}
		//CloseExtInfo();
		grdWebUsedLog.DataSourceID = "dsWebUsedLog";
		grdWebUsedLogSummary.DataSourceID = "dsWebUsedLogSummary";
		pnlUsedLogParent.Visible = true;
		pnlWebUsedLog.Visible = true;

		grdWebUsedLog.PageIndex = 0;

		grdWebUsedLog.DataBind();
		grdWebUsedLogSummary.DataBind();
	}

	protected void btnSeekUsedLogTalk_Click(object sender,EventArgs e) {
		SetViewStateReportDay();

		string sSiteNm = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(iBridUtil.GetStringValue(ViewState["SiteCd"]),"SITE_NM",ref  sSiteNm);
		}
		lblUsedLogSite.Text = sSiteNm;
		lblUsedLogCharNo.Text = " - " + iBridUtil.GetStringValue(ViewState["UserCharNo"]);
		if (GetMultiCharFlag(ViewState["SiteCd"])) {
			lblUsedLogCharNo.Visible = true;
		} else {
			lblUsedLogCharNo.Visible = false;
		}
		lblFromDay.Text = iBridUtil.GetStringValue(ViewState["ReportDayFrom"]);
		lblToDay.Text = iBridUtil.GetStringValue(ViewState["ReportDayTo"]);

		grdTalkHistorySummary.DataSourceID = "dsUsedLogTalkSummary";
		grdUsedLogMonitorSummary.DataSourceID = "dsUsedLogMonitorSummary";
		grdTalkHistory.DataSourceID = "dsUsedLogTalk";
		grdUsedLogMonitor.DataSourceID = "dsUsedLogMonitor";

		pnlUsedLogTalkParent.Visible = true;
		pnlUsedLog.Visible = true;

		grdTalkHistory.PageIndex = 0;
		grdUsedLogMonitor.PageIndex = 0;

		grdTalkHistorySummary.DataBind();
		grdUsedLogMonitorSummary.DataBind();
		grdTalkHistory.DataBind();
		grdUsedLogMonitor.DataBind();
	}

	protected void lnkUsed_Command(object sender,CommandEventArgs e) {
		SetViewStateReportDay();
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		ViewState["SiteCd"] = sKeys[0];
		ViewState["UserCharNo"] = sKeys[1];
		string sSiteNm = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(iBridUtil.GetStringValue(ViewState["SiteCd"]),"SITE_NM",ref  sSiteNm);
		}
		lblWebFromDay.Text = iBridUtil.GetStringValue(ViewState["ReportDayFrom"]);
		lblWebToDay.Text = iBridUtil.GetStringValue(ViewState["ReportDayTo"]);
		lblWebUsedLogSite.Text = sSiteNm;
		lblWebUsedLogCharNo.Text = " - " + iBridUtil.GetStringValue(ViewState["UserCharNo"]);
		if (GetMultiCharFlag(ViewState["SiteCd"])) {
			lblWebUsedLogCharNo.Visible = true;
		} else {
			lblWebUsedLogCharNo.Visible = false;
		}
		CloseExtInfo();
		grdWebUsedLog.DataSourceID = "dsWebUsedLog";
		grdWebUsedLogSummary.DataSourceID = "dsWebUsedLogSummary";
		pnlUsedLogParent.Visible = true;
		pnlWebUsedLog.Visible = true;

		grdWebUsedLog.PageIndex = 0;

		grdWebUsedLog.DataBind();
		grdWebUsedLogSummary.DataBind();
		//		this.RegisterAutoScroll("anchorUsedLog");
	}

	protected void lnkUsedTalk_Command(object sender,CommandEventArgs e) {
		SetViewStateReportDay();
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		ViewState["SiteCd"] = sKeys[0];
		ViewState["UserCharNo"] = sKeys[1];

		string sSiteNm = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(iBridUtil.GetStringValue(ViewState["SiteCd"]),"SITE_NM",ref  sSiteNm);
		}
		lblUsedLogSite.Text = sSiteNm;
		lblUsedLogCharNo.Text = " - " + iBridUtil.GetStringValue(ViewState["UserCharNo"]);
		if (GetMultiCharFlag(ViewState["SiteCd"])) {
			lblUsedLogCharNo.Visible = true;
		} else {
			lblUsedLogCharNo.Visible = false;
		}
		lblFromDay.Text = iBridUtil.GetStringValue(ViewState["ReportDayFrom"]);
		lblToDay.Text = iBridUtil.GetStringValue(ViewState["ReportDayTo"]);

		CloseExtInfo();

		grdTalkHistorySummary.DataSourceID = "dsUsedLogTalkSummary";
		grdUsedLogMonitorSummary.DataSourceID = "dsUsedLogMonitorSummary";
		grdTalkHistory.DataSourceID = "dsUsedLogTalk";
		grdUsedLogMonitor.DataSourceID = "dsUsedLogMonitor";

		pnlUsedLogTalkParent.Visible = true;
		pnlUsedLog.Visible = true;

		grdTalkHistory.PageIndex = 0;
		grdUsedLogMonitor.PageIndex = 0;

		grdTalkHistorySummary.DataBind();
		grdUsedLogMonitorSummary.DataBind();
		grdTalkHistory.DataBind();
		grdUsedLogMonitor.DataBind();
	}

	protected void lnkDiary_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		string sSiteCd = sKeys[0];
		string sUserCharNo = sKeys[1];

        Response.Redirect(string.Format("../Cast/CastDiaryInquiry.aspx?sitecd={0}&usercharno={1}&loginid={2}&redirect=1", sKeys[0], sKeys[1], ViewState["LoginId"].ToString()));
	}

	protected void lnkBbs_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		string sSiteCd = sKeys[0];
		string sUserCharNo = sKeys[1];
		DisplayBbsInfo(sSiteCd,sUserCharNo);
	}

	protected void btnCloseProfileMovie_Click(object sender,EventArgs e) {
		grdProfileMovie.DataSourceID = "";
		pnlProfileMovie.Visible = false;
	}
	protected void btnClosePublicMovie_Click(object sender,EventArgs e) {
		grdPublicMovie.DataSourceID = "";
		pnlPublicMovie.Visible = false;
	}
	protected void btnCloseSaleVoice_Click(object sender,EventArgs e) {
		grdSaleVoice.DataSourceID = "";
		pnlSaleVoice.Visible = false;
	}

	protected void btnCloseBbsMovie_Click(object sender,EventArgs e) {
		grdBbsMovie.DataSourceID = "";
		pnlBbsMovie.Visible = false;
	}

	protected void btnCloseTalkMovie_Click(object sender,EventArgs e) {
		grdTalkMovie.DataSourceID = "";
		pnlTalkMovie.Visible = false;
	}

	protected void btnCloseBbsPic_Click(object sender,EventArgs e) {
		grdBbsPic.DataSourceID = "";
		pnlBbsPic.Visible = false;
	}

	protected void btnCloseFavorit_Click(object sender,EventArgs e) {
		grdFavorit.DataSourceID = "";
		pnlFavorit.Visible = false;
	}

	protected void btnCloseLikeMe_Click(object sender,EventArgs e) {
		grdLikeMe.DataSourceID = "";
		pnlLikeMe.Visible = false;
	}

	protected void btnCloseRefuse_Click(object sender,EventArgs e) {
		grdRefuse.DataSourceID = "";
		pnlRefuse.Visible = false;
	}

	protected void btnCloseRefuseMe_Click(object sender,EventArgs e) {
		grdRefuseMe.DataSourceID = "";
		pnlRefuseMe.Visible = false;
	}

	protected void btnCloseMarking_Click(object sender,EventArgs e) {
		grdMarking.DataSourceID = "";
		pnlMarking.Visible = false;
	}

	protected void btnCloseCastFriends_Click(object sender,EventArgs e) {
		this.grdCastFriends.DataSourceID = string.Empty;
		this.pnlCastFriends.Visible = false;
	}

	protected void btnCloseManFriends_Click(object sender,EventArgs e) {
		this.grdManFriends.DataSourceID = string.Empty;
		this.pnlManFriends.Visible = false;
	}

	/// <summary>
	/// 男性会員紹介履歴の閉じるボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnCloseManFriendLogs_Click(object sender,EventArgs e) {
		this.grdManFriendLogs.DataSourceID = string.Empty;
		this.pnlManFriendLogs.Visible = false;
	}

	protected void btnCloseModifyMailHis_Click(object sender,EventArgs e) {
		this.grdModifyMailHis.DataSourceID = string.Empty;
		this.grdNgMailHistory.DataSourceID = string.Empty;
		this.grdModifyTelHis.DataSourceID = string.Empty;
		this.grdModifyLoginPassword.DataSourceID = string.Empty;
		this.pnlModifyMailHis.Visible = false;
	}
	protected void btnCloseMailBox_Click(object sender,EventArgs e) {
		grdMailBox.DataSourceID = "";
		pnlMailBox.Visible = false;
	}

	protected void btnCloseTalkHis_Click(object sender,EventArgs e) {
		grdTalkHis.DataSourceID = "";
		pnlTalkHis.Visible = false;
	}

	protected void btnCloseRequestHis_Click(object sender,EventArgs e) {
		grdRequestHis.DataSourceID = "";
		pnlRequestHis.Visible = false;
	}

	protected void btnCloseBbs_Click(object sender,EventArgs e) {
		grdBbs.DataSourceID = "";
		pnlBbs.Visible = false;
	}

	protected void btnCloseMail_Click(object sender,EventArgs e) {
		pnlMail.Visible = false;
	}

    protected void btnCloseGameCharacter_Click(object sender, EventArgs e) {
        this.pnlGameCharacter.Visible = false;
    }

	protected void btnCloseFanClubStatus_Click(object sender, EventArgs e) {
		grdFanClubStatus.DataSourceID = "";
		this.pnlFanClubStatus.Visible = false;
	}

	protected void lnkMailTemplate_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		Server.Transfer(string.Format("CastMailTemplateList.aspx?sitecd={0}&userseq={1}&usercharno={2}&return={3}",sKeys[0],sKeys[1],sKeys[2],ViewState["RETURN"].ToString()));
	}

	protected void lnkPaymentHis_Command(object sender,CommandEventArgs e) {
		DisplayPaymentHisInfo();
	}
	protected void lnkUserDefPointHis_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		string sSiteCd = sKeys[0];
		string sUserSeq = sKeys[1];
		DisplayUserDefPointHisInfo(sSiteCd,sUserSeq);
	}

	protected void btnCloseUserDefPointHistory_Click(object sender,EventArgs e) {
		grdUserDefPointHistory.DataSourceID = "";
		pnlUserDefPointHistory.Visible = false;
	}
	protected void btnClosePaymentHistory_Click(object sender,EventArgs e) {
		grdPaymentHistory.DataSourceID = "";
		pnlPaymentHistory.Visible = false;
	}


	protected void lnkCreateMail_Command(object sender,CommandEventArgs e) {
		CloseExtInfo();
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		string sSiteNm = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(sKeys[0],"SITE_NM",ref sSiteNm);
		}
		lblCreateMailSiteNm.Text = sSiteNm;
		lblCreateMailCharNo.Text = " - " + sKeys[1];
		lblCreateMailVisibleSiteCd.Text = sKeys[0];
		if (GetMultiCharFlag(sKeys[0])) {
			lblCreateMailCharNo.Visible = true;
		} else {
			lblCreateMailCharNo.Visible = false;
		}
		pnlMail.Visible = true;
		btnTransMail.Enabled = true;
		lblAttention.Visible = false;
		lstMailTemplate.DataSourceID = "dsDMMail";
		lstMailTemplate.DataBind();
		lstMailTemplate.Items.Insert(0,new ListItem("",""));
		lstMailTemplate.DataSourceID = "";
		lstMailTemplate.SelectedIndex = 0;
		txtMailDocText.Visible = false;
		txtMailDocHtml.Visible = false;
		txtMailTitle.Text = "";

		//		RegisterAutoScroll("anchorMailCreate");
	}

	protected void btnCloseUsedLogParent_Click(object sender,EventArgs e) {
		grdWebUsedLogSummary.DataSourceID = string.Empty;
		grdWebUsedLog.DataSourceID = string.Empty;
		pnlUsedLogParent.Visible = false;
	}

	protected void btnCloseUsedLogTalkParent_Click(object sender,EventArgs e) {
		grdTalkHistorySummary.DataSourceID = string.Empty;
		grdUsedLogMonitorSummary.DataSourceID = string.Empty;
		grdTalkHistory.DataSourceID = string.Empty;
		grdUsedLogMonitor.DataSourceID = string.Empty;
		pnlUsedLogTalkParent.Visible = false;
	}

	protected void lnkProfileQuickTime_Command(object sender,CommandEventArgs e) {
		using (CastMovie oMovie = new CastMovie()) {
			if (oMovie.GetOne(decimal.Parse(e.CommandArgument.ToString()))) {
				string sRoot = ConfigurationManager.AppSettings["Root"];
				string sURL = "http://" + Request.Url.Authority + sRoot + ViCommConst.WEB_MOVIE_PATH + "/" + oMovie.siteCd + string.Format("/operator/{0}/{1}{2}",oMovie.loginId,iBridUtil.addZero(oMovie.movieSeq.ToString(),ViCommConst.OBJECT_NM_LENGTH),ViCommConst.MOVIE_FOODER);
				string sScripts = string.Format("<script language=\"javascript\">window.open('{0}','','width=300,height=300,resizable=no,directories=no,scrollbars=no' , false);</script>",sURL);
				ClientScript.RegisterStartupScript(Page.GetType(),"OpenNewWindow",sScripts);
			}
		}
	}

	protected void lnkLogin_Command(object sender,CommandEventArgs e) {
		using (Site oSite = new Site()) {
			string[] sKeys = e.CommandArgument.ToString().Split(':');
			if (oSite.GetOne("Z001")) {
				string sURL = string.Format("{0}/User/Start.aspx?loginid={1}&password={2}&adminflg=1&nopc=1&urlsex=3",oSite.url,sKeys[0],sKeys[1]);
				string sScripts = string.Format("<script language=\"javascript\">window.open('{0}','','width=300,height=800,resizable=no,directories=no,scrollbars=yes' , false);</script>",sURL);
				ClientScript.RegisterStartupScript(Page.GetType(),"OpenNewWindow",sScripts);
			}
		}
	}

	protected void lnkTalkQuickTime_Command(object sender,CommandEventArgs e) {
		string sURL = string.Format("file:///M:/{0}",e.CommandArgument.ToString());
		string sScripts = string.Format("<script language=\"javascript\">window.open('{0}','','width=300,height=300,resizable=no,directories=no,scrollbars=no' , false);</script>",sURL);
		ClientScript.RegisterStartupScript(Page.GetType(),"OpenNewWindow",sScripts);
	}

	protected void btnRegistProfileMovie_Click(object sender,EventArgs e) {
		Server.Transfer(string.Format("MovieUpload.aspx?userseq={0}&usercharno={1}&sitecd={2}&return={3}&movietype={4}",
					iBridUtil.GetStringValue(ViewState["UserSeq"]),lblProfileMovieCharNo.Text.Substring(3),btnProfileRegistMovie.CommandArgument.ToString(),
					ViewState["RETURN"].ToString(),ViCommConst.ATTACHED_PROFILE.ToString()));
	}
	protected void btnRegistPublicMovie_Click(object sender,EventArgs e) {
		Server.Transfer(string.Format("MovieConvert.aspx?userseq={0}&usercharno={1}&sitecd={2}&return={3}&movietype={4}",
					iBridUtil.GetStringValue(ViewState["UserSeq"]),lblPublicMovieCharNo.Text.Substring(3),btnRegistPublicMovie.CommandArgument.ToString(),
					ViewState["RETURN"].ToString(),ViCommConst.ATTACHED_MOVIE.ToString()));
	}
	protected void btnRegistSaleVoice_Click(object sender,EventArgs e) {
		ViComm.UrlBuilder oUrlBuilder = new ViComm.UrlBuilder("VoiceUpload.aspx");

		oUrlBuilder.Parameters.Add("userseq",iBridUtil.GetStringValue(ViewState["UserSeq"]));
		oUrlBuilder.Parameters.Add("usercharno",lblSaleVoiceCharNo.Text.Substring(3));
		oUrlBuilder.Parameters.Add("sitecd",btnRegistSaleVoice.CommandArgument.ToString());
		oUrlBuilder.Parameters.Add("return",ViewState["RETURN"].ToString());
		oUrlBuilder.Parameters.Add("voicetype",ViCommConst.ATTACHED_VOICE.ToString());

		Server.Transfer(oUrlBuilder.ToString());
	}


	protected void btnRegistBbsMovie_Click(object sender,EventArgs e) {
		Server.Transfer(string.Format("MovieUpload.aspx?userseq={0}&usercharno={1}&sitecd={2}&return={3}&movietype={4}",
					iBridUtil.GetStringValue(ViewState["UserSeq"]),lblBbsMovieCharNo.Text.Substring(3),btnBbsRegistMovie.CommandArgument.ToString(),
					ViewState["RETURN"].ToString(),ViCommConst.ATTACHED_BBS.ToString()));
	}
	protected void btnRegistBbsPic_Click(object sender,EventArgs e) {
		Server.Transfer(string.Format("CastPicMainte.aspx?userseq={0}&usercharno={1}&sitecd={2}&return=CastView&pictype={3}",
					iBridUtil.GetStringValue(ViewState["UserSeq"]),lblBbsPicCharNo.Text.Substring(3),btnBbsRegistPic.CommandArgument.ToString(),ViCommConst.ATTACHED_BBS.ToString()));
	}


	protected void lnkDelRxMail_Command(object sender,CommandEventArgs e) {
		string sMailSeq = e.CommandArgument.ToString();
		using (MailBox oMailBox = new MailBox()) {
			oMailBox.UpdateMailDel(sMailSeq,ViCommConst.RX);
		}
		DataBind();
	}

	protected void lnkDelTxMail_Command(object sender,CommandEventArgs e) {
		string sMailSeq = e.CommandArgument.ToString();
		using (MailBox oMailBox = new MailBox()) {
			oMailBox.UpdateMailDel(sMailSeq,ViCommConst.TX);
		}
		DataBind();
	}

	protected void btnBack_Click(object sender,EventArgs e) {
		string sCallFrom = iBridUtil.GetStringValue(ViewState["RETURN"]);
		switch (sCallFrom) {
			case "CastInquiry.aspx":
			case "CastMovieCheckList.aspx":
			case "SameTelInquiry.aspx":
			case "ProfileMovieCheckList.aspx":
			case "ProfileMovieOpenList.aspx":
			case "MultiPickupMainte.aspx":
				Server.Transfer("../CastAdmin/" + sCallFrom);
				break;

			case "CastRankingMainte.aspx":
				Server.Transfer("../Site/" + sCallFrom);
				break;

			case "LiveBroadCastList.aspx":
			case "ShareLiveTalkList.aspx":
				Server.Transfer("../Live/" + sCallFrom);
				break;

			case "PointLogByDetail.aspx":
			case "CastTalkMovieDownload.aspx":
			case "CastProfileMovieDownload.aspx":
			case "CastLoginHistory.aspx":
			case "CastOperation.aspx":
			case "BbsInquiry.aspx":
				Server.Transfer("../Status/" + sCallFrom);
				break;

			case "CastPerformance.aspx":
				Server.Transfer("../StatusManager/" + sCallFrom);
				break;

			case "MailInquiry.aspx":
			case "MailLogInquiry.aspx":
				Server.Transfer("../Mail/" + sCallFrom);
				break;

			case "ManView.aspx":
				Response.Redirect(string.Format("../Man/{0}?site={1}&manloginid={2}",sCallFrom,iBridUtil.GetStringValue(ViewState["MAN_SITE_CD"]),iBridUtil.GetStringValue(ViewState["MAN_LOGIN_ID"])));
				break;

			case "CastView.aspx":
				Response.Redirect(string.Format("../Cast/{0}?loginid={1}&return=CastCharacterInquiry.aspx",sCallFrom,iBridUtil.GetStringValue(ViewState["CAST_LOGIN_ID"])));
				break;

			default:
				Server.Transfer(string.Format("CastCharacterInquiry.aspx"));
				break;
		}
	}

	protected void dsCast_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		pnlProfileMovie.Visible = false;
		pnlPublicMovie.Visible = false;

		ViewState["SiteCd"] = "";
		ViewState["UserSeq"] = "";
		ViewState["LoginId"] = "";
		ViewState["ReportDayFrom"] = "";
		ViewState["ReportDayTo"] = "";
		lblUserSeq.Text = "";

		if (e.ReturnValue == null) {
			return;
		}

		DataSet ds = (DataSet)e.ReturnValue;
		if (ds.Tables[0].Rows.Count == 0) {
			NoData();
		} else {
			lblNotFound.Visible = false;
			if (ds.Tables[0].Rows.Count > 1) {
				lblManyUser.Visible = true;
			}
			DataRow dr = ds.Tables[0].Rows[0];

			if (Session["AdminType"].ToString().CompareTo(ViCommConst.RIGHT_LOCAL_STAFF) <= 0) {
				if (!dr["MANAGER_SEQ"].ToString().Equals(iBridUtil.GetStringValue(Session["MANAGER_SEQ"]))) {
					NoData();
					return;
				}
			}

			ViewState["UserSeq"] = dr["USER_SEQ"].ToString();
			ViewState["LoginId"] = dr["LOGIN_ID"].ToString();

			SetViewStateReportDay();

			lblUserSeq.Text = ViewState["UserSeq"].ToString();

			grdLogin.DataSourceID = "dsLoginCharacter";
			grdCharacter.DataSourceID = "dsCharacter";
			grdLogin.DataBind();
			grdCharacter.DataBind();
			pnlCharacter.Visible = true;
			GetData();

			if (profileMovieRegistSite.Equals("") == false && userCharNo.Equals("") == false) {
				DisplayProfileMovieInfo(profileMovieRegistSite,userCharNo);
			}
			if (publicMovieRegistSite.Equals("") == false && userCharNo.Equals("") == false) {
				DisplayPublicMovieInfo(publicMovieRegistSite,userCharNo);
			}
			if (bbsMovieRegistSite.Equals("") == false && userCharNo.Equals("") == false) {
				DisplayBbsMovieInfo(bbsMovieRegistSite,userCharNo);
			}
			if (talkMovieRegistSite.Equals("") == false && userCharNo.Equals("") == false) {
				DisplayTalkMovieInfo(talkMovieRegistSite,userCharNo);
			}
			if (saleVoiceRegistSite.Equals("") == false && userCharNo.Equals("") == false) {
				DisplaySaleVoiceInfo(saleVoiceRegistSite,userCharNo);
			}


			string sWithdrwalseq = iBridUtil.GetStringValue(Request.QueryString["withdrawalseq"]);

			// 退会申請履歴からの遷移
			if (!sWithdrwalseq.Equals(string.Empty)) {
				using (Withdrawal oWithdrawal = new Withdrawal()) {
					if (oWithdrawal.GetOne(sWithdrwalseq,ViCommConst.OPERATOR)) {
						pnlWithdrawal.Visible = true;
						lblWithdrawalSeq.Text = sWithdrwalseq;
						lblWithdrawalStatus.Text = oWithdrawal.withdrawalStatusNm;
						lblWithdrawalDate.Text = oWithdrawal.updateDate;
						lblWithdrawalReasonCd.Text = oWithdrawal.withdrawalReasonNm;
						txtWithdrawalReasonDoc.Text = oWithdrawal.withdrawalReasonDoc;
						txtWithdrawalRemarks.Text = oWithdrawal.remarks;
						if (oWithdrawal.withdrawalStatus.Equals(ViCommConst.WITHDRAWAL_STATUS_COMPLIATE) || dr["USER_STATUS"].ToString().Equals(ViCommConst.USER_WOMAN_RESIGNED)) {
							btnAcceptWithdrawal.Visible = false;
						}
					} else {
						pnlWithdrawal.Visible = false;
					}
				}

				// 退会履歴を表示
				pnlWithdrawalHistoryDetail.Visible = false;
				pnlWithdrawalHistoryList.Visible = true;
				grdWithdrawalHistoryList.DataSourceID = "dsWithdrawalHistoryList";
				grdWithdrawalHistoryList.DataBind();
			} else {
				pnlWithdrawal.Visible = false;

				// 退会履歴を非表示
				pnlWithdrawalHistoryDetail.Visible = false;
				pnlWithdrawalHistoryList.Visible = false;
				grdWithdrawalHistoryList.DataSourceID = string.Empty;
			}

			string sBingoEntrySeq = iBridUtil.GetStringValue(Request.QueryString["bingoentryseq"]);
			if (!sBingoEntrySeq.Equals(string.Empty)) {
				using (BingoEntry oBingoEntry = new BingoEntry()) {
					if (oBingoEntry.GetOne(sBingoEntrySeq)) {
						pnlBingoEntry.Visible = true;
						lblBingoTermSeq.Text = oBingoEntry.bingoTermSeq.ToString();
						lblBingoEntrySeq.Text = sBingoEntrySeq;
						lblBingoCardNo.Text = oBingoEntry.bingoCardNo.ToString();
						lblBingoCharangeCount.Text = oBingoEntry.bingoCharangeCount.ToString();
						lblBingoCompliateJackpotBefore.Text = oBingoEntry.bingoCompliateJackpot.ToString();
						lblBingoCompliateJackpotAfter.Text = (oBingoEntry.bingoCompliateJackpot - oBingoEntry.bingoElectionAmt).ToString();
						lblBingoElectionPoint.Text = oBingoEntry.bingoElectionPoint.ToString();
						lblBingoElectionAmt.Text = oBingoEntry.bingoElectionAmt.ToString();
						lblBingoEntryDate.Text = oBingoEntry.bingoEntryDate;
						lblBingoApplicationDate.Text = oBingoEntry.bingoApplicationDate;
						if (oBingoEntry.bingoPointPaymentFlag == ViCommConst.FLAG_ON) {
							rdoBingoPointPayment.Checked = true;
							rdoBingoNotPointPayment.Checked = false;
						} else {
							rdoBingoPointPayment.Checked = false;
							rdoBingoNotPointPayment.Checked = true;
						}

					} else {
						pnlBingoEntry.Visible = false;
					}
				}
			} else {
				pnlBingoEntry.Visible = false;
			}
		}
	}

	private void SetViewStateReportDay() {

		if ((txtReportDayFrom.Text.Equals("")) && (txtReportDayTo.Text.Equals(""))) {
			txtReportDayFrom.Text = DateTime.Now.AddMonths(-1).ToString("yyyy/MM/dd");
			txtReportDayTo.Text = DateTime.Now.AddDays(1).ToString("yyyy/MM/dd");
		}
		if ((!txtReportDayFrom.Text.Equals("")) || (!txtReportDayTo.Text.Equals(""))) {
			if (txtReportDayFrom.Text.Equals("")) {
				txtReportDayFrom.Text = txtReportDayTo.Text;
			} else if (txtReportDayTo.Text.Equals("")) {
				txtReportDayTo.Text = txtReportDayFrom.Text;
			}
		}
		if (chkReportDayEnable.Checked) {
			ViewState["ReportDayFrom"] = txtReportDayFrom.Text.ToString();
			ViewState["ReportDayTo"] = txtReportDayTo.Text.ToString();
		} else {
			ViewState["ReportDayFrom"] = "1900/01/01";
			ViewState["ReportDayTo"] = "2100/01/01";
		}
	}

	private void NoData() {
		lblNotFound.Visible = true;
		pnlDtl.Visible = false;
		pnlCharacter.Visible = false;
		grdTalkHistory.DataSourceID = string.Empty;
		grdTalkHistorySummary.DataSourceID = string.Empty;
		grdWebUsedLog.DataSourceID = string.Empty;
		grdWebUsedLogSummary.DataSourceID = string.Empty;
		grdUsedLogMonitor.DataSourceID = string.Empty;
		grdUsedLogMonitorSummary.DataSourceID = string.Empty;
		grdBonusLog.DataSourceID = "";
		grdLogin.DataSourceID = "";
		grdCharacter.DataSourceID = "";
	}

	private string TransToAgeCheck(string pUrl,out string pLoginId,out string pPassword,out string pName,out string pKanaName) {
		pLoginId = "";
		pPassword = "";
		pName = "";
		pKanaName = "";
		try {
			ViCommInterface.WriteIFLog("TransToParent",pUrl);
			WebRequest req = WebRequest.Create(pUrl);
			req.Timeout = 30000;
			WebResponse res = req.GetResponse();
			System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

			Stream st = res.GetResponseStream();
			using (StreamReader sr = new StreamReader(st,encoding)) {
				string sResponse = sr.ReadToEnd();
				NameValueCollection objCol = HttpUtility.ParseQueryString(sResponse);
				sr.Close();
				st.Close();
				ViCommInterface.WriteIFLog("TransToParent",pUrl + " Response=" + sResponse);
				pLoginId = iBridUtil.GetStringValue(objCol["userid"]);
				pPassword = iBridUtil.GetStringValue(objCol["password"]);
				pName = iBridUtil.GetStringValue(objCol["p06"]);
				pKanaName = iBridUtil.GetStringValue(objCol["p05"]);

				return iBridUtil.GetStringValue(objCol["result"]);
			}
		} catch (Exception e) {
			ViCommInterface.WriteIFError(e,"TransToParent",pUrl);
			return "-1";
		}
	}

	private void InitPage() {
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);
		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblNotFound.Text = DisplayWordUtil.Replace(this.lblNotFound.Text);
		this.lblManyUser.Text = DisplayWordUtil.Replace(this.lblManyUser.Text);

		grdLogin.PageSize = 100;

		grdBonusLog.PageSize = 100;
		grdBonusLog.DataSourceID = "";

		grdTalkHistorySummary.DataSourceID = string.Empty;
		grdWebUsedLogSummary.DataSourceID = string.Empty;
		grdUsedLogMonitorSummary.DataSourceID = string.Empty;

		grdTalkHistory.PageSize = 100;
		grdTalkHistory.DataSourceID = "";
		grdUsedLogMonitor.DataSourceID = string.Empty;
		pnlUsedLog.Visible = false;

		grdWebUsedLog.PageSize = 100;
		grdWebUsedLog.DataSourceID = string.Empty;
		pnlWebUsedLog.Visible = false;
		pnlUsedLogParent.Visible = false;
		pnlUsedLogTalkParent.Visible = false;

		grdProfileMovie.PageSize = 100;
		grdProfileMovie.DataSourceID = "";
		pnlProfileMovie.Visible = false;

		grdPublicMovie.PageSize = 100;
		grdPublicMovie.DataSourceID = "";
		pnlPublicMovie.Visible = false;

		grdSaleVoice.PageSize = 100;
		grdSaleVoice.DataSourceID = "";
		pnlSaleVoice.Visible = false;

		grdBbsMovie.PageSize = 100;
		grdBbsMovie.DataSourceID = "";
		pnlBbsMovie.Visible = false;

		grdBbs.PageSize = 100;
		grdBbs.DataSourceID = "";
		pnlBbs.Visible = false;

		grdTalkMovie.PageSize = 100;
		grdTalkMovie.DataSourceID = "";
		pnlTalkMovie.Visible = false;

		grdBbsPic.PageSize = 100;
		grdBbsPic.DataSourceID = "";
		pnlBbsPic.Visible = false;

		grdFavorit.PageSize = 100;
		grdFavorit.DataSourceID = "";
		pnlFavorit.Visible = false;

		grdLikeMe.PageSize = 100;
		grdLikeMe.DataSourceID = "";
		pnlLikeMe.Visible = false;

		grdRefuse.PageSize = 100;
		grdRefuse.DataSourceID = "";
		pnlRefuse.Visible = false;

		grdRefuseMe.PageSize = 100;
		grdRefuseMe.DataSourceID = "";
		pnlRefuseMe.Visible = false;

		grdMarking.PageSize = 100;
		grdMarking.DataSourceID = "";
		pnlMarking.Visible = false;

		grdMailBox.PageSize = 100;
		grdMailBox.DataSourceID = "";
		pnlMailBox.Visible = false;

		grdTalkHis.PageSize = 100;
		grdTalkHis.DataSourceID = "";
		pnlTalkHis.Visible = false;

		grdRequestHis.PageSize = 100;
		grdRequestHis.DataSourceID = "";
		pnlRequestHis.Visible = false;

		grdPaymentHistory.PageSize = 100;
		grdPaymentHistory.DataSourceID = "";
		pnlPaymentHistory.Visible = false;

		grdUserDefPointHistory.PageSize = 100;
		grdUserDefPointHistory.DataSourceID = string.Empty;
		pnlUserDefPointHistory.Visible = false;

		grdFanClubStatus.PageSize = 100;
		grdFanClubStatus.DataSourceID = string.Empty;
		pnlFanClubStatus.Visible = false;

		int iCompanyMask = int.Parse(Session["CompanyMask"].ToString());
	
		pnlMail.Visible = false;
		pnlWithdrawal.Visible = false;

		txtReportDayFrom.Text = "";
		txtReportDayTo.Text = "";

		// 退会履歴を初期化
		ClearWithdrawalHistory();

		if (!iBridUtil.GetStringValue(ViewState["CAST_LOGIN_ID"]).Equals("")) {
			dvwCast.DataSourceID = "dsCast";
			txtLoginId.Text = iBridUtil.GetStringValue(ViewState["CAST_LOGIN_ID"]);
			txtUserSeq.Text = "";
			pnlDtl.Visible = true;
			pnlCharacter.Visible = true;
		} else if (!string.IsNullOrEmpty(userSeq)) {
			dvwCast.DataSourceID = "dsCast";
			txtLoginId.Text = "";
			txtUserSeq.Text = userSeq;
			pnlDtl.Visible = true;
			pnlCharacter.Visible = true;
		} else {
			txtLoginId.Text = "";
			txtUserSeq.Text = "";
			dvwCast.DataSourceID = "";
			grdTalkHistory.DataSourceID = "";
			grdLogin.DataSourceID = "";
			grdBonusLog.DataSourceID = "";
			pnlDtl.Visible = false;
			pnlCharacter.Visible = false;
		}
		lblNotFound.Visible = false;
		lblManyUser.Visible = false;

		if (Session["AdminType"].ToString().CompareTo(ViCommConst.RIGHT_PRODUCTION) <= 0) {
			btnRegist.Visible = false;
			lstSiteCd.Visible = false;
			txtLoginId.Enabled = false;
			txtUserSeq.Enabled = false;
		} else {
			btnRegist.Visible = true;
			lstSiteCd.Visible = true;
			txtLoginId.Enabled = true;
			txtUserSeq.Enabled = true;
		}

		pnlCharacter.Visible = false;

		if (!siteCd.Equals("")) {
			btnRetrun.Visible = true;
			pnlKey.Visible = false;
			pnlInfo.Visible = false;
			pnlLogin.Visible = false;
			pnlBonusRemark.Visible = false;
			pnlRemarks.Visible = false;
			grdCharacter.Columns[1].Visible = false;
			grdCharacter.Columns[2].Visible = true;
			ViewState["FLAG_MAIN_CHAR"] = "";
		} else {
			btnRetrun.Visible = false;
			pnlKey.Visible = true;
			pnlInfo.Visible = true;
			pnlLogin.Visible = true;
			pnlBonusRemark.Visible = true;
			pnlRemarks.Visible = true;
			grdCharacter.Columns[1].Visible = true;
			grdCharacter.Columns[2].Visible = false;
			ViewState["FLAG_MAIN_CHAR"] = "1";
		}

		// Available Serive?
		using (ManageCompany oCompany = new ManageCompany()) {
			dvwCast.Fields[5].Visible = oCompany.IsAvailableService(ViCommConst.RELEASE_SUPPORT_STAFF_ID);
		}
		DataBind();

	}


	protected bool IsAdmin() {
		return (Session["AdminType"].ToString().CompareTo(ViCommConst.RIGHT_LOCAL_STAFF) > 0);
	}

	protected string GetSysImage(object pOnlienStatus) {
		return this.GetSysImage(pOnlienStatus,0);
	}

	protected string GetSysImage(object pOnlienStatus,object pDummyTalkingFlag) {
		int iOnlineStatus = int.Parse(pOnlienStatus.ToString());

		switch (iOnlineStatus) {
			case ViCommConst.USER_LOGINED:
				return "../image/sys_logined.jpg";
			case ViCommConst.USER_WAITING:
				return "../image/sys_online.jpg";
			case ViCommConst.USER_TALKING:
				int iDummyTakingFlag = int.Parse(pDummyTalkingFlag.ToString());
				if (iDummyTakingFlag == ViCommConst.FLAG_ON) {
					return "../image/sys_dummy_talking.jpg";
				}
				return "../image/sys_talking.jpg";
			case ViCommConst.USER_OFFLINE:
				return "../image/sys_offline.jpg";
		}
		return "";
	}

	protected bool IsNewSite(object pUseOtherInfoFlag) {
		return (pUseOtherInfoFlag.ToString().Equals("1") == false);
	}

	protected string CheckAdminLevel(object pText) {
		int iCompare = string.Compare(Session["AdminType"].ToString(),ViCommConst.RIGHT_SITE_MANAGER);
		if (iCompare >= 0) {
			return pText.ToString();
		} else {
			return "***********";
		}
	}

	protected bool GetMultiCharFlag(object pSiteCd) {
		bool bMultiCharFlag = false;
		using (Site oSite = new Site()) {
			bMultiCharFlag = oSite.GetMultiCharFlag(pSiteCd.ToString());
		}
		return bMultiCharFlag;
	}

	private void DisplayProfileMovieInfo(string pSiteCd,string pUserCharNo) {
		CloseExtInfo();
		ViewState["SiteCd"] = pSiteCd;
		btnProfileRegistMovie.CommandArgument = pSiteCd;
		ViewState["UserCharNo"] = pUserCharNo;
		string sSiteNm = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(pSiteCd,"SITE_NM",ref sSiteNm);
		}
		lblProfileMovieSite.Text = sSiteNm;
		lblProfileMovieCharNo.Text = " - " + pUserCharNo;
		if (GetMultiCharFlag(pSiteCd)) {
			lblProfileMovieCharNo.Visible = true;
		} else {
			lblProfileMovieCharNo.Visible = false;
		}
		grdProfileMovie.DataSourceID = "dsCastMovie";
		pnlProfileMovie.Visible = true;
		grdProfileMovie.DataBind();

		//		RegisterAutoScroll("anchorProfileMovie");
	}

	private void DisplaySaleVoiceInfo(string pSiteCd,string pUserCharNo) {
		CloseExtInfo();
		ViewState["SiteCd"] = pSiteCd;
		btnRegistSaleVoice.CommandArgument = pSiteCd;
		ViewState["UserCharNo"] = pUserCharNo;
		string sSiteNm = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(pSiteCd,"SITE_NM",ref sSiteNm);
		}
		lblSaleVoiceSite.Text = sSiteNm;
		lblSaleVoiceCharNo.Text = " - " + pUserCharNo;
		lblSaleVoiceCharNo.Visible = GetMultiCharFlag(pSiteCd);

		grdSaleVoice.DataSourceID = "dsCastVoice";
		grdSaleVoice.DataBind();
		pnlSaleVoice.Visible = true;

		//		RegisterAutoScroll("anchorSaleVoice");
	}

	private void DisplayPublicMovieInfo(string pSiteCd,string pUserCharNo) {
		CloseExtInfo();
		ViewState["SiteCd"] = pSiteCd;
		btnRegistPublicMovie.CommandArgument = pSiteCd;
		ViewState["UserCharNo"] = pUserCharNo;
		string sSiteNm = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(pSiteCd,"SITE_NM",ref sSiteNm);
		}
		lblPublicMovieSite.Text = sSiteNm;
		lblPublicMovieCharNo.Text = " - " + pUserCharNo;
		if (GetMultiCharFlag(pSiteCd)) {
			lblPublicMovieCharNo.Visible = true;
		} else {
			lblPublicMovieCharNo.Visible = false;
		}
		grdPublicMovie.DataSourceID = "dsPublicMovie";
		pnlPublicMovie.Visible = true;
		grdPublicMovie.DataBind();

		//		RegisterAutoScroll("anchorPublicMovie");
	}


	private void DisplayBbsMovieInfo(string pSiteCd,string pUserCharNo) {
		CloseExtInfo();
		ViewState["SiteCd"] = pSiteCd;
		btnBbsRegistMovie.CommandArgument = pSiteCd;
		ViewState["UserCharNo"] = pUserCharNo;
		string sSiteNm = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(pSiteCd,"SITE_NM",ref sSiteNm);
		}
		lblBbsMovieSite.Text = sSiteNm;
		lblBbsMovieCharNo.Text = " - " + pUserCharNo;
		if (GetMultiCharFlag(pSiteCd)) {
			lblBbsMovieCharNo.Visible = true;
		} else {
			lblBbsMovieCharNo.Visible = false;
		}
		grdBbsMovie.DataSourceID = "dsCastBbsMovie";
		pnlBbsMovie.Visible = true;
		grdBbsMovie.DataBind();

		//		RegisterAutoScroll("anchorBbsMovie");
	}

	private void DisplayTalkMovieInfo(string pSiteCd,string pUserCharNo) {
		CloseExtInfo();
		ViewState["SiteCd"] = pSiteCd;
		ViewState["UserCharNo"] = pUserCharNo;
		string sSiteNm = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(pSiteCd,"SITE_NM",ref sSiteNm);
		}
		lblTalkMovieSite.Text = sSiteNm;
		lblTalkMovieCharNo.Text = " - " + pUserCharNo;
		if (GetMultiCharFlag(pSiteCd)) {
			lblTalkMovieCharNo.Visible = true;
		} else {
			lblTalkMovieCharNo.Visible = false;
		}
		grdTalkMovie.DataSourceID = "dsTalkMovie";
		pnlTalkMovie.Visible = true;
		grdTalkMovie.DataBind();

		//		RegisterAutoScroll("anchorTalkMovie");
	}

	private void DisplayBbsPicInfo(string pSiteCd,string pUserCharNo) {
		CloseExtInfo();
		ViewState["SiteCd"] = pSiteCd;
		btnBbsRegistPic.CommandArgument = pSiteCd;
		ViewState["UserCharNo"] = pUserCharNo;
		string sSiteNm = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(pSiteCd,"SITE_NM",ref sSiteNm);
		}
		lblBbsPicSite.Text = sSiteNm;
		lblBbsPicCharNo.Text = " - " + pUserCharNo;
		if (GetMultiCharFlag(pSiteCd)) {
			lblBbsPicCharNo.Visible = true;
		} else {
			lblBbsPicCharNo.Visible = false;
		}
		pnlBbsPic.Visible = true;
		grdBbsPic.DataSourceID = "dsCastBbsPic";
		grdBbsPic.DataBind();

		//		RegisterAutoScroll("anchorBbsPic");
	}

	private void DisplayFavoritInfo(string pSiteCd,string pUserCharNo) {
		CloseExtInfo();
		ViewState["SiteCd"] = pSiteCd;
		ViewState["UserCharNo"] = pUserCharNo;
		string sSiteNm = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(pSiteCd,"SITE_NM",ref sSiteNm);
		}
		lblFavoritSite.Text = sSiteNm;
		lblFavoritCharNo.Text = " - " + pUserCharNo;
		if (GetMultiCharFlag(pSiteCd)) {
			lblFavoritCharNo.Visible = true;
		} else {
			lblFavoritCharNo.Visible = false;
		}
		grdFavorit.DataSourceID = "dsCastFavorit";
		pnlFavorit.Visible = true;
		grdFavorit.DataBind();

		//		RegisterAutoScroll("anchorFavorit");
	}

	private void DisplayLikeMeInfo(string pSiteCd,string pUserCharNo) {
		CloseExtInfo();
		ViewState["SiteCd"] = pSiteCd;
		ViewState["UserCharNo"] = pUserCharNo;
		string sSiteNm = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(pSiteCd,"SITE_NM",ref sSiteNm);
		}
		lblLikeMeSite.Text = sSiteNm;
		lblLikeMeCharNo.Text = " - " + pUserCharNo;
		if (GetMultiCharFlag(pSiteCd)) {
			lblLikeMeCharNo.Visible = true;
		} else {
			lblLikeMeCharNo.Visible = false;
		}
		grdLikeMe.DataSourceID = "dsCastLikeMe";
		pnlLikeMe.Visible = true;
		grdLikeMe.DataBind();

		//		RegisterAutoScroll("anchorLikeMe");
	}

	private void DisplayRefuseInfo(string pSiteCd,string pUserCharNo) {
		CloseExtInfo();
		ViewState["SiteCd"] = pSiteCd;
		ViewState["UserCharNo"] = pUserCharNo;
		string sSiteNm = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(pSiteCd,"SITE_NM",ref sSiteNm);
		}
		lblRefuseSite.Text = sSiteNm;
		lblRefuseCharNo.Text = " - " + pUserCharNo;
		if (GetMultiCharFlag(pSiteCd)) {
			lblRefuseCharNo.Visible = true;
		} else {
			lblRefuseCharNo.Visible = false;
		}
		grdRefuse.DataSourceID = "dsRefuse";
		pnlRefuse.Visible = true;
		grdRefuse.DataBind();

		//		RegisterAutoScroll("anchorRefuse");
	}

	private void DisplayRefuseMeInfo(string pSiteCd,string pUserCharNo) {
		CloseExtInfo();
		ViewState["SiteCd"] = pSiteCd;
		ViewState["UserCharNo"] = pUserCharNo;
		string sSiteNm = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(pSiteCd,"SITE_NM",ref sSiteNm);
		}
		lblRefuseMeSite.Text = sSiteNm;
		lblRefuseMeCharNo.Text = " - " + pUserCharNo;
		if (GetMultiCharFlag(pSiteCd)) {
			lblRefuseMeCharNo.Visible = true;
		} else {
			lblRefuseMeCharNo.Visible = false;
		}
		grdRefuseMe.DataSourceID = "dsRefuseMe";
		pnlRefuseMe.Visible = true;
		grdRefuseMe.DataBind();

		//		RegisterAutoScroll("anchorRefuseMe");
	}

	private void DisplayMarkingInfo(string pSiteCd,string pUserCharNo) {
		CloseExtInfo();
		ViewState["SiteCd"] = pSiteCd;
		ViewState["UserCharNo"] = pUserCharNo;
		string sSiteNm = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(pSiteCd,"SITE_NM",ref sSiteNm);
		}
		lblMarkingSite.Text = sSiteNm;
		lblMarkingCharNo.Text = " - " + pUserCharNo;
		if (GetMultiCharFlag(pSiteCd)) {
			lblMarkingCharNo.Visible = true;
		} else {
			lblMarkingCharNo.Visible = false;
		}
		grdMarking.DataSourceID = "dsMarking";
		pnlMarking.Visible = true;
		grdMarking.DataBind();

		//		RegisterAutoScroll("anchorMarking");
	}

	private void DisplayFriendsInfo(string pSiteCd,string pUserCharNo,string pFriendIntroCd) {
		CloseExtInfo();
		ViewState["SiteCd"] = pSiteCd;
		ViewState["UserCharNo"] = pUserCharNo;
		ViewState["FiendIntroCd"] = pFriendIntroCd;
		string sSiteNm = string.Empty;

		using (Site oSite = new Site()) {
			oSite.GetValue(pSiteCd,"SITE_NM",ref sSiteNm);
		}
		lblCastFriendsSite.Text = sSiteNm;
		lblCastFriendsCharNo.Text = " - " + pUserCharNo;
		lblManFriendsSite.Text = sSiteNm;
		lblManFriendsCharNo.Text = " - " + pUserCharNo;
		lblManFriendLogsSite.Text = sSiteNm;
		lblManFriendLogsCharNo.Text = " - " + pUserCharNo;

		if (GetMultiCharFlag(pSiteCd)) {
			lblCastFriendsCharNo.Visible = true;
			lblManFriendsCharNo.Visible = true;
			lblManFriendLogsCharNo.Visible = true;
		} else {
			lblCastFriendsCharNo.Visible = false;
			lblManFriendsCharNo.Visible = false;
			lblManFriendLogsCharNo.Visible = false;
		}

		pnlFriends.Visible = true;
		pnlCastFriends.Visible = true;
		pnlManFriends.Visible = true;
		pnlManFriendLogs.Visible = true;

		grdCastFriends.DataSourceID = "dsCastFriends";
		grdCastFriends.DataBind();
		grdManFriends.DataSourceID = "dsManFriends";
		grdManFriends.DataBind();
		grdManFriendLogs.DataSourceID = "dsManFriendLogs";
		grdManFriendLogs.DataBind();
		//		RegisterAutoScroll("anchorCastFriends");
	}

	private void DisplayModifyMailHisInfo(string pUserSeq) {
		CloseExtInfo();
		ViewState["UserSeq"] = pUserSeq;

		pnlModifyMailHis.Visible = true;

		grdModifyMailHis.DataSourceID = "dsModifyMailHis";
		grdNgMailHistory.DataSourceID = "dsNgMailHistory";
		grdModifyTelHis.DataSourceID = "dsModifyTelHis";
		grdModifyLoginPassword.DataSourceID = "dsModifyLoginPassword";
		grdMailBox.PageIndex = 0;
		grdModifyMailHis.DataBind();
		grdNgMailHistory.DataBind();
		grdModifyTelHis.DataBind();
		grdModifyLoginPassword.DataBind();

		//		RegisterAutoScroll("anchorCastFriends");
	}

	private void DisplayMailBoxInfo(string pSiteCd,string pUserCharNo) {
		CloseExtInfo();
		ViewState["SiteCd"] = pSiteCd;
		ViewState["UserCharNo"] = pUserCharNo;
		//検索用 
		lblMailBoxSiteCd.Text = pSiteCd;
		lblMailBoxCharNo.Text = pUserCharNo;

		string sSiteNm = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(pSiteCd,"SITE_NM",ref sSiteNm);
		}
		lblMailSite.Text = sSiteNm;
		lblMailCharNo.Text = " - " + pUserCharNo;
		if (GetMultiCharFlag(pSiteCd)) {
			lblMailCharNo.Visible = true;
		} else {
			lblMailCharNo.Visible = false;
		}

		SysPrograms.SetupFromToDayTime(lstMailFromYYYY,lstMailFromMM,lstMailFromDD,lstMailFromHH,lstMailToYYYY,lstMailToMM,lstMailToDD,lstMailToHH,false);
		DateTime dtFrom = DateTime.Now.AddDays(-7);
		DateTime dtTo = DateTime.Now;
		lstMailFromYYYY.SelectedValue = dtFrom.ToString("yyyy");
		lstMailToYYYY.SelectedValue = dtTo.ToString("yyyy");
		lstMailFromMM.SelectedValue = dtFrom.ToString("MM");
		lstMailToMM.SelectedValue = dtTo.ToString("MM");
		lstMailFromDD.SelectedValue = dtFrom.ToString("dd");
		lstMailToDD.SelectedValue = dtTo.ToString("dd");
		lstMailFromHH.SelectedValue = dtFrom.ToString("HH");
		lstMailToHH.SelectedValue = dtTo.ToString("HH");

		rdoWithBatchMailNon.Checked = true;

		grdMailBox.DataSourceID = "dsMailBox";
		pnlMailBox.Visible = true;
		grdMailBox.PageIndex = 0;
		grdMailBox.DataBind();

		//		RegisterAutoScroll("anchorMailBox");
	}

	private void DisplayBbsInfo(string pSiteCd,string pUserCharNo) {
		CloseExtInfo();
		ViewState["SiteCd"] = pSiteCd;
		ViewState["UserCharNo"] = pUserCharNo;
		string sSiteNm = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(pSiteCd,"SITE_NM",ref sSiteNm);
		}
		lblBbsSite.Text = sSiteNm;
		lblBbsCharNo.Text = " - " + pUserCharNo;
		if (GetMultiCharFlag(pSiteCd)) {
			lblBbsCharNo.Visible = true;
		} else {
			lblBbsCharNo.Visible = false;
		}
		grdBbs.DataSourceID = "dsBbsLog";
		pnlBbs.Visible = true;
		grdBbs.DataBind();

		//		RegisterAutoScroll("anchorBbs");
	}

	private void DisplayPaymentHisInfo() {
		CloseExtInfo();
		grdPaymentHistory.DataSourceID = "dsPaymentHistory";
		grdPaymentHistory.DataBind();
		pnlPaymentHistory.Visible = true;
		//		RegisterAutoScroll("anchorPaymentHis");
	}
	private void DisplayUserDefPointHisInfo(string pSiteCd,string pUserSeq) {
		ViewState["SiteCd"] = pSiteCd;
		ViewState["UserSeq"] = pUserSeq;
		CloseExtInfo();
		grdUserDefPointHistory.DataSourceID = "dsUserDefPointHistory";
		grdUserDefPointHistory.DataBind();
		pnlUserDefPointHistory.Visible = true;
	}

    private void DisplayGameCharacterInfo(string pSiteCd, string pUserSeq, string pUserCharNo) {
        CloseExtInfo();
        pnlGameCharacter.Visible = true;
		SetGameCharacter(pSiteCd, pUserSeq, pUserCharNo);
		ViewState["SiteCd"] = pSiteCd;
		ViewState["UserSeq"] = pUserSeq;
		ViewState["UserCharNo"] = pUserCharNo;
		this.lstGameItemCategory.DataBind();
    }

	private void DisplayFanClubStatusInfo(string pSiteCd,string pUserSeq,string pUserCharNo) {
		CloseExtInfo();
		ViewState["SiteCd"] = pSiteCd;
		ViewState["UserSeq"] = pUserSeq;
		ViewState["UserCharNo"] = pUserCharNo;
		string sSiteNm = string.Empty;
		using (Site oSite = new Site()) {
			oSite.GetValue(pSiteCd,"SITE_NM",ref sSiteNm);
		}
		lblFanClubStatusSite.Text = sSiteNm;
		pnlFanClubStatus.Visible = true;
		grdFanClubStatus.DataSourceID = "dsFanClubStatus";
		grdFanClubStatus.DataBind();
		//		RegisterAutoScroll("anchorFanClubStatus");
	}

	protected void vrdLoginId_ServerValidate(object source,ServerValidateEventArgs args) {
		if (IsValid) {
			args.IsValid = true;
			if (txtEmailAddr.Text.Equals(string.Empty) && txtLoginId.Text.Equals(string.Empty) && txtUserSeq.Text.Equals(string.Empty)) {
				args.IsValid = false;
			}
		}
	}

	protected void dsCast_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = (!txtLoginId.Text.Equals(string.Empty)) ? txtLoginId.Text.TrimEnd() : txtEmailAddr.Text.TrimEnd();
		e.InputParameters[1] = txtUserSeq.Text.TrimEnd();
	}

	protected void dsUsedLogMonitor_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = iBridUtil.GetStringValue(ViewState["SiteCd"]);
		e.InputParameters["pSexCd"] = ViCommConst.OPERATOR;
		e.InputParameters["pUserSeq"] = iBridUtil.GetStringValue(ViewState["UserSeq"]);
		e.InputParameters["pUserCharNo"] = iBridUtil.GetStringValue(ViewState["UserCharNo"]);
		e.InputParameters["pReportDayFrom"] = iBridUtil.GetStringValue(ViewState["ReportDayFrom"]);
		e.InputParameters["pReportDayTo"] = iBridUtil.GetStringValue(ViewState["ReportDayTo"]);
		e.InputParameters["pPartnerLoginId"] = this.txtManLoginIdTalk.Text.Trim();
	}

	protected void dsUsedLogTalk_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = iBridUtil.GetStringValue(ViewState["SiteCd"]);
		e.InputParameters["pSexCd"] = ViCommConst.OPERATOR;
		e.InputParameters["pLoginId"] = iBridUtil.GetStringValue(ViewState["LoginId"]);
		e.InputParameters["pUserCharNo"] = iBridUtil.GetStringValue(ViewState["UserCharNo"]);
		e.InputParameters["pPartnerLoginId"] = txtManLoginIdTalk.Text.Trim();
		e.InputParameters["pPartnerUserCharNo"] = ViCommConst.MAIN_CHAR_NO;
		e.InputParameters["pTel"] = string.Empty;
		e.InputParameters["pReportDayFrom"] = iBridUtil.GetStringValue(ViewState["ReportDayFrom"]);
		e.InputParameters["pReportDayTo"] = iBridUtil.GetStringValue(ViewState["ReportDayTo"]);
		e.InputParameters["pChargeType"] = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8}",
															ViCommConst.CHARGE_TALK_WSHOT,
															ViCommConst.CHARGE_TALK_PUBLIC,
															ViCommConst.CHARGE_TALK_VOICE_WSHOT,
															ViCommConst.CHARGE_TALK_VOICE_PUBLIC,
															ViCommConst.CHARGE_GPF_TALK_VOICE,
															ViCommConst.CHARGE_CAST_TALK_PUBLIC,
															ViCommConst.CHARGE_CAST_TALK_WSHOT,
															ViCommConst.CHARGE_CAST_TALK_VOICE_WSHOT,
															ViCommConst.CHARGE_CAST_TALK_VOICE_PUBLIC);
		e.InputParameters["pReportTimeFrom"] = string.Empty;
		e.InputParameters["pReportTimeTo"] = string.Empty;
		e.InputParameters["pCallResult"] = string.Empty;
		e.InputParameters["pTalkSubType"] = ViCommConst.TALK_SUB_TYPE_ALL;
	}

	protected void dsWebUsedLog_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = iBridUtil.GetStringValue(ViewState["SiteCd"]);
		e.InputParameters["pUserSeq"] = iBridUtil.GetStringValue(ViewState["UserSeq"]);
		e.InputParameters["pUserCharNo"] = iBridUtil.GetStringValue(ViewState["UserCharNo"]);
		e.InputParameters["pReportDayFrom"] = iBridUtil.GetStringValue(ViewState["ReportDayFrom"]);
		e.InputParameters["pReportDayTo"] = iBridUtil.GetStringValue(ViewState["ReportDayTo"]);
		e.InputParameters["pManLoginId"] = this.txtManLoginId.Text.Trim();
	}

	protected void dsLoginCharacter_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["UserSeq"]);
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["ReportDayFrom"]);
		e.InputParameters[2] = iBridUtil.GetStringValue(ViewState["ReportDayTo"]);
	}

	protected void dsCastMovie_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["SiteCd"]);
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["UserSeq"]);
		e.InputParameters[2] = iBridUtil.GetStringValue(ViewState["UserCharNo"]);
		e.InputParameters[3] = ViCommConst.ATTACHED_PROFILE.ToString();
	}

	protected void dsPublicMovie_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["SiteCd"]);
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["UserSeq"]);
		e.InputParameters[2] = iBridUtil.GetStringValue(ViewState["UserCharNo"]);
		e.InputParameters[3] = ViCommConst.ATTACHED_MOVIE.ToString();
	}
	protected void dsCastVoice_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["SiteCd"]);
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["UserSeq"]);
		e.InputParameters[2] = iBridUtil.GetStringValue(ViewState["UserCharNo"]);
		e.InputParameters[3] = ViCommConst.ATTACHED_VOICE.ToString();
	}

	protected void dsCastBbsMovie_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["SiteCd"]);
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["UserSeq"]);
		e.InputParameters[2] = iBridUtil.GetStringValue(ViewState["UserCharNo"]);
		e.InputParameters[3] = ViCommConst.ATTACHED_BBS.ToString();
	}

	protected void dsBbsLog_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["SiteCd"]);
		e.InputParameters[1] = ViCommConst.OPERATOR;
		e.InputParameters[2] = string.Empty;
		e.InputParameters[3] = string.Empty;
		e.InputParameters[4] = string.Empty;
		e.InputParameters[5] = string.Empty;
		e.InputParameters[6] = iBridUtil.GetStringValue(ViewState["LoginId"]);
		e.InputParameters[7] = iBridUtil.GetStringValue(ViewState["UserCharNo"]);
	}

	protected void dsTalkMovie_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["SiteCd"]);
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["UserSeq"]);
		e.InputParameters[2] = iBridUtil.GetStringValue(ViewState["UserCharNo"]);
	}

	protected void dsCastBbsPic_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["SiteCd"]);
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["UserSeq"]);
		e.InputParameters[2] = iBridUtil.GetStringValue(ViewState["UserCharNo"]);
		e.InputParameters[3] = ViCommConst.ATTACHED_BBS.ToString();
	}

	protected void dsCastFavorit_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["SiteCd"]);
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["UserSeq"]);
		e.InputParameters[2] = iBridUtil.GetStringValue(ViewState["UserCharNo"]);
	}

	protected void dsCastLikeMe_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["SiteCd"]);
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["UserSeq"]);
		e.InputParameters[2] = iBridUtil.GetStringValue(ViewState["UserCharNo"]);
	}

	protected void dsRefuse_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["SiteCd"]);
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["UserSeq"]);
		e.InputParameters[2] = iBridUtil.GetStringValue(ViewState["UserCharNo"]);
	}

	protected void dsMarking_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["SiteCd"]);
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["UserSeq"]);
		e.InputParameters[2] = iBridUtil.GetStringValue(ViewState["UserCharNo"]);
	}

	protected void dsPaymentHistory_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["UserSeq"]);
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["ReportDayFrom"]);
		e.InputParameters[2] = iBridUtil.GetStringValue(ViewState["ReportDayTo"]);
	}

	protected void dsMailBox_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		string sAttachedType = string.Empty;
		string sTxRxType = string.Empty;
		string sWithBatchMail = string.Empty;

		foreach (ListItem oItem in this.chkAttachedType.Items) {
			if (!oItem.Selected)
				continue;

			if (!sAttachedType.Equals(string.Empty))
				sAttachedType += ",";
			sAttachedType += oItem.Value;
		}

		if (rdoRxType.Checked) {
			sTxRxType = ViCommConst.RX;
		} else if (rdoTxType.Checked) {
			sTxRxType = ViCommConst.TX;
		} else {
			sTxRxType = ViCommConst.TXRX;
		}

		if (rdoWithBatchMailNon.Checked) {
			sWithBatchMail = ViCommConst.FLAG_OFF_STR;
		} else if (rdoWithBatchMailOnly.Checked) {
			sWithBatchMail = ViCommConst.FLAG_ON_STR;
		} else {
			sWithBatchMail = string.Empty;
		}

		e.InputParameters[0] = lblMailBoxSiteCd.Text;
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["UserSeq"]);
		e.InputParameters[2] = lblMailBoxCharNo.Text;
		e.InputParameters[3] = TrimEnd(txtMailPartnerLoginId.Text);
		e.InputParameters[4] = ViCommConst.MAIN_CHAR_NO;
		e.InputParameters[5] = ViCommConst.OPERATOR;
		e.InputParameters[6] = string.Format("{0}/{1}/{2} {3}",lstMailFromYYYY.SelectedValue,lstMailFromMM.SelectedValue,lstMailFromDD.SelectedValue,lstMailFromHH.SelectedValue);
		e.InputParameters[7] = string.Format("{0}/{1}/{2} {3}",lstMailToYYYY.SelectedValue,lstMailToMM.SelectedValue,lstMailToDD.SelectedValue,lstMailToHH.SelectedValue);
		e.InputParameters[8] = sAttachedType;
		e.InputParameters[9] = sWithBatchMail;
		e.InputParameters[10] = sTxRxType;
	}

	protected void dsTalkHistory_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["SiteCd"]);
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["UserSeq"]);
		e.InputParameters[2] = iBridUtil.GetStringValue(ViewState["UserCharNo"]);
	}

	protected void dsRequestHistory_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["SiteCd"]);
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["UserSeq"]);
		e.InputParameters[2] = iBridUtil.GetStringValue(ViewState["UserCharNo"]);
	}

	protected void dsUserDefPointHistory_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["SiteCd"]);
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["UserSeq"]);
	}

	protected void dsFanClubStatus_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		FanClubStatus.SearchCondition oSearchCondition = new FanClubStatus.SearchCondition();
		oSearchCondition.SiteCd = iBridUtil.GetStringValue(ViewState["SiteCd"]);
		oSearchCondition.CastUserSeq = iBridUtil.GetStringValue(ViewState["UserSeq"]);
		oSearchCondition.CastCharNo = iBridUtil.GetStringValue(ViewState["UserCharNo"]);
		oSearchCondition.EnableFlag = ViCommConst.FLAG_ON_STR;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void grdCharacter_RowDataBound(object sender,GridViewRowEventArgs e) {
		if ((e.Row.RowType == DataControlRowType.DataRow) || (e.Row.RowType == DataControlRowType.Header)) {
			if (Session["AdminType"].ToString().CompareTo(ViCommConst.RIGHT_LOCAL_STAFF) <= 0) {
				//e.Row.Cells[6].Visible = false;
			}
		}
	}

	private void DeletePicture(string pFileSeq) {
		string sWebPhisicalDir = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(iBridUtil.GetStringValue(ViewState["SiteCd"]),"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
		}

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
			string sFullPath = sWebPhisicalDir +
								ViCommConst.PIC_DIRECTRY + "\\" + iBridUtil.GetStringValue(ViewState["SiteCd"]) + "\\Operator\\" + txtLoginId.Text + "\\" +
								iBridUtil.addZero(pFileSeq,ViCommConst.OBJECT_NM_LENGTH) + ViCommConst.PIC_FOODER;

			if (System.IO.File.Exists(sFullPath)) {
				System.IO.File.Delete(sFullPath);
			}
			sFullPath = sWebPhisicalDir +
								ViCommConst.PIC_DIRECTRY + "\\" + iBridUtil.GetStringValue(ViewState["SiteCd"]) + "\\Operator\\" + txtLoginId.Text + "\\" +
								iBridUtil.addZero(pFileSeq,ViCommConst.OBJECT_NM_LENGTH) + ViCommConst.PIC_FOODER_SMALL;

			if (System.IO.File.Exists(sFullPath)) {
				System.IO.File.Delete(sFullPath);
			}
		}
	}

	protected void btnBonusPoint_Click(object sender,EventArgs e) {
		int iAmt = 0;
		int iTotalPaymentAmtFlag = 0;

		int.TryParse(txtBonusPointAmt.Text,out iAmt);

		if (rdoTotalPayAmtOk.Checked) {
			iTotalPaymentAmtFlag = 1;
		} else {
			iTotalPaymentAmtFlag = 0;
		}
		if ((IsValid) && (txtBonusPointAmt.Text != "")) {
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("ADD_BONUS_POINT_ADMIN");
				db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,
									iBridUtil.GetStringValue(ViewState["UserSeq"]));
				db.ProcedureInParm("PBONUS_POINT",DbSession.DbType.NUMBER,iAmt);
				db.ProcedureInParm("PBONUS_TYPE",DbSession.DbType.VARCHAR2,ViCommConst.BONUS_TYPE_ADMIN);
				db.ProcedureInParm("PREMARKS",DbSession.DbType.VARCHAR2,txtRemarks.Text);
				db.ProcedureInParm("PTOTAL_PAYMENT_AMT_FLAG",DbSession.DbType.NUMBER,iTotalPaymentAmtFlag);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
			}
			DataBind();
			txtBonusPointAmt.Text = "";
			txtRemarks.Text = "";
		}
	}

	protected void btnClose_Ext_Click(object sender,EventArgs e) {
		pnlBonusLog.Visible = false;
	}

	protected void dsBonusLog_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["UserSeq"]);
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["ReportDayFrom"]);
		e.InputParameters[2] = iBridUtil.GetStringValue(ViewState["ReportDayTo"]);
	}


	protected void lnkCancelBonusLog_Command(object sender,CommandEventArgs e) {
		string sBonusSeq = e.CommandArgument.ToString();
		using (BonusLog oBonusLog = new BonusLog()) {
			oBonusLog.CancelBonusLog(sBonusSeq);
		}
		DataBind();
	}

	protected void vdcBonusPointAmt_ServerValidate(object source,ServerValidateEventArgs args) {
		int iAmt = 0;

		int.TryParse(txtBonusPointAmt.Text,out iAmt);
		if (iAmt == 0) {
			args.IsValid = false;
		} else {
			args.IsValid = true;
		}
	}
	protected void btnRemarks_Click(object sender,EventArgs e) {
		UpdateData(0);
	}

	private void CloseExtInfo() {
		grdProfileMovie.DataSourceID = "";
		grdPublicMovie.DataSourceID = "";
		grdSaleVoice.DataSourceID = "";
		grdBbsMovie.DataSourceID = "";
		grdTalkMovie.DataSourceID = "";
		grdBbsPic.DataSourceID = "";
		grdBonusLog.DataSourceID = "";
		grdTalkHistory.DataSourceID = "";
		grdWebUsedLog.DataSourceID = string.Empty;
		grdTalkHistorySummary.DataSourceID = string.Empty;
		grdWebUsedLogSummary.DataSourceID = string.Empty;
		grdUsedLogMonitor.DataSourceID = string.Empty;
		grdUsedLogMonitorSummary.DataSourceID = string.Empty;
		grdFavorit.DataSourceID = "";
		grdLikeMe.DataSourceID = "";
		grdRefuse.DataSourceID = "";
		grdRefuseMe.DataSourceID = "";
		grdMarking.DataSourceID = string.Empty;
		grdMailBox.DataSourceID = "";
		grdTalkHis.DataSourceID = "";
		grdRequestHis.DataSourceID = "";
		grdPaymentHistory.DataSourceID = "";
		grdBbs.DataSourceID = "";
		grdManFriendLogs.DataSourceID = string.Empty;
		grdManFriends.DataSourceID = string.Empty;
		grdCastFriends.DataSourceID = string.Empty;
		grdModifyMailHis.DataSourceID = string.Empty;
		grdNgMailHistory.DataSourceID = string.Empty;
		grdModifyTelHis.DataSourceID = string.Empty;
		grdModifyLoginPassword.DataSourceID = string.Empty;
		grdUserDefPointHistory.DataSourceID = string.Empty;
		grdPossessionGameItem.DataSourceID = string.Empty;

		// 退会履歴を初期化
		ClearWithdrawalHistory();

		this.txtManLoginId.Text = string.Empty;
		this.txtManLoginIdTalk.Text = string.Empty;

		pnlProfileMovie.Visible = false;
		pnlPublicMovie.Visible = false;
		pnlSaleVoice.Visible = false;
		pnlBbsMovie.Visible = false;
		pnlTalkMovie.Visible = false;
		pnlBbsPic.Visible = false;
		pnlBonusLog.Visible = false;
		pnlPaymentHistory.Visible = false;
		pnlUsedLogParent.Visible = false;
		pnlUsedLogTalkParent.Visible = false;
		pnlWebUsedLog.Visible = false;
		pnlUsedLog.Visible = false;
		pnlFavorit.Visible = false;
		pnlLikeMe.Visible = false;
		pnlRefuse.Visible = false;
		pnlRefuseMe.Visible = false;
		pnlMarking.Visible = false;
		pnlMailBox.Visible = false;
		pnlTalkHis.Visible = false;
		pnlRequestHis.Visible = false;
		pnlMail.Visible = false;
		pnlPaymentHistory.Visible = false;
		pnlBbs.Visible = false;
		pnlFriends.Visible = false;
		pnlCastFriends.Visible = false;
		pnlManFriends.Visible = false;
		pnlManFriendLogs.Visible = false;
		pnlModifyMailHis.Visible = false;
		pnlUserDefPointHistory.Visible = false;
        pnlGameCharacter.Visible = false;
		pnlPossessionGameItem.Visible = false;
		pnlFanClubStatus.Visible = false;
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REMARKS_GET");
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["UserSeq"]));
			db.ProcedureOutParm("PREMARKS1",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREMARKS2",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREMARKS3",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtRemarks1.Text = db.GetStringValue("PREMARKS1");
				txtRemarks2.Text = db.GetStringValue("PREMARKS2");
				txtRemarks3.Text = db.GetStringValue("PREMARKS3");
			} else {
				ClearField();
			}
		}
	}

	private void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("REMARKS_MAINTE");
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["UserSeq"]));
			db.ProcedureInParm("PREMARKS1",DbSession.DbType.VARCHAR2,txtRemarks1.Text);
			db.ProcedureInParm("PREMARKS2",DbSession.DbType.VARCHAR2,txtRemarks2.Text);
			db.ProcedureInParm("PREMARKS3",DbSession.DbType.VARCHAR2,txtRemarks3.Text);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["ROWID"]));
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

		}
		DataBind();
		GetData();
	}

	private void ClearField() {
		txtRemarks1.Text = "";
		txtRemarks2.Text = "";
		txtRemarks3.Text = "";
	}

	protected void lnkFavolitDelete_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		string sSiteCd = sKeys[0];
		string sPartnerUserSeq = sKeys[1];
		DeleteFavorit(sSiteCd,iBridUtil.GetStringValue(ViewState["UserSeq"]),lblFavoritCharNo.Text.Substring(3),sPartnerUserSeq,ViCommConst.MAIN_CHAR_NO);
		grdFavorit.DataBind();
	}

	protected void lnkLikeMeDelete_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		string sSiteCd = sKeys[0];
		string sPartnerUserSeq = sKeys[1];
		DeleteFavorit(sSiteCd,sPartnerUserSeq,ViCommConst.MAIN_CHAR_NO,iBridUtil.GetStringValue(ViewState["UserSeq"]),lblLikeMeCharNo.Text.Substring(3));
		grdLikeMe.DataBind();
	}

	private void DeleteFavorit(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("FAVORIT_DELETE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PPARTNER_USER_SEQ",DbSession.DbType.VARCHAR2,pPartnerUserSeq);
			db.ProcedureInParm("PPARTNER_USER_CHAR_NO",DbSession.DbType.VARCHAR2,pPartnerUserCharNo);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	protected void dsCharacter_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		e.InputParameters[1] = iBridUtil.GetStringValue(lblUserSeq.Text);
		e.InputParameters[2] = iBridUtil.GetStringValue(ViewState["FLAG_MAIN_CHAR"]);
	}
	protected void btnReturn_Click(object sender,EventArgs e) {
		Response.Redirect(string.Format("CastView.aspx?loginid={0}&return=CastCharacterInquiry.aspx",iBridUtil.GetStringValue(ViewState["CAST_LOGIN_ID"])));
	}


	private void RegisterAutoScroll(string pTargetClientId) {
		string sScript = string.Format("document.getElementById('{0}').scrollIntoView(true);",pTargetClientId);
		this.ClientScript.RegisterStartupScript(this.GetType(),"pageScroll",sScript,true);
	}

	protected string GetThumbnailPicUrl(object pThumbnailPicSeq,object pSiteCd,object pLoginId) {
		long? dThumbnailPicSeq = pThumbnailPicSeq as long?;
		if (dThumbnailPicSeq == null)
			return string.Empty;
		return this.GenerateWebImageDirectoryPath(pSiteCd.ToString(),pLoginId.ToString()) + ViCommPrograms.GeneratePicFileNm(decimal.Parse(dThumbnailPicSeq.Value.ToString())) + ViCommConst.PIC_FOODER;
	}
	private string GenerateWebImageDirectoryPath(string pSiteCd,string pLoginId) {
		return ConfigurationManager.AppSettings["Root"] + ViCommConst.WEB_PIC_PATH + "/" + pSiteCd + "/Operator/" + pLoginId + "/";
	}

	protected bool EnableSaleMovieMainte() {
		return IsAvailableService(ViCommConst.RELEASE_SALE_MOVIE);
	}
	protected bool EnableSaleVoiceMainte() {
		return IsAvailableService(ViCommConst.RELEASE_SALE_VOICE);
	}
	protected bool EnableRequestHistory() {
		return IsAvailableService(ViCommConst.RELEASE_ADMIN_VIEW_REQEST_HISTORY);
	}
	protected bool EnableTalkHistory() {
		return !IsAvailableService(ViCommConst.RELEASE_ADMIN_VIEW_REQEST_HISTORY);
	}
	public static bool IsAvailableService(ulong pService) {
		using (ManageCompany oInstance = new ManageCompany()) {
			return oInstance.IsAvailableService(pService);
		}
	}

	public string GetVoiceMainteUrl(object pVoiceSeq,object pVoiceType) {
		ViComm.UrlBuilder oUrlBuilder = new ViComm.UrlBuilder("VoiceMainte.aspx");

		oUrlBuilder.Parameters.Add("userseq",iBridUtil.GetStringValue(ViewState["UserSeq"]));
		oUrlBuilder.Parameters.Add("usercharno",lblSaleVoiceCharNo.Text.Substring(3));
		oUrlBuilder.Parameters.Add("sitecd",btnRegistSaleVoice.CommandArgument.ToString());
		oUrlBuilder.Parameters.Add("return",ViewState["RETURN"].ToString());
		oUrlBuilder.Parameters.Add("voiceseq",pVoiceSeq.ToString());
		oUrlBuilder.Parameters.Add("voicetype",pVoiceType.ToString());

		return oUrlBuilder.ToString();

	}

	protected string GetLikeMeMark(object pUserSeq,object pUserCharNo,object pPartnerUserSeq,object pPartnerUserCharNo) {
		string sValue = string.Empty;
		using (Favorit oFavorit = new Favorit()) {
			if (oFavorit.GetOne(iBridUtil.GetStringValue(ViewState["SiteCd"]),iBridUtil.GetStringValue(pPartnerUserSeq),iBridUtil.GetStringValue(pPartnerUserCharNo),iBridUtil.GetStringValue(pUserSeq),iBridUtil.GetStringValue(pUserCharNo))) {
				sValue = "お気に入られ";
			}
		}
		return sValue;
	}

	protected string GetRefuseMeMark(object pUserSeq,object pUserCharNo,object pPartnerUserSeq,object pPartnerUserCharNo) {
		string sValue = string.Empty;
		using (Refuse oRefuse = new Refuse()) {
			if (oRefuse.GetOne(iBridUtil.GetStringValue(ViewState["SiteCd"]),iBridUtil.GetStringValue(pPartnerUserSeq),iBridUtil.GetStringValue(pPartnerUserCharNo),iBridUtil.GetStringValue(pUserSeq),iBridUtil.GetStringValue(pUserCharNo))) {
				sValue = "拒否されている";
			}
		}
		return sValue;
	}

	protected string GetModelName(string pCarrierCd,string pUserAgent) {
		string sModelName = string.Empty;
		using (ModelDiscriminant oModelDiscriminant = new ModelDiscriminant()) {
			sModelName = oModelDiscriminant.GetModelName(pCarrierCd,pUserAgent) ?? string.Empty;
		}
		return sModelName;
	}

	protected string GetIntroducerLink(object sIntroducerFriendCd) {
		DataSet ds;

		using (CastCharacter oCastCharacter = new CastCharacter()) {
			ds = oCastCharacter.GetOneByFriendIntroCd(sIntroducerFriendCd.ToString());
		}
		if (ds.Tables[0].Rows.Count != 0) {
			return string.Format("../Cast/CastView.aspx?loginid={0}&return=ManView.aspx",ds.Tables[0].Rows[0]["LOGIN_ID"].ToString());
		} else {
			using (UserManCharacter oUserManCharacter = new UserManCharacter()) {
				ds = oUserManCharacter.GetOneByFriendIntroCd(sIntroducerFriendCd.ToString());
			}
			if (ds.Tables[0].Rows.Count != 0) {
				return string.Format("../Man/ManView.aspx?site={0}&manloginid={1}",ds.Tables[0].Rows[0]["SITE_CD"].ToString(),ds.Tables[0].Rows[0]["LOGIN_ID"].ToString());
			}
		}
		return "";
	}


	protected string GetMailTxRxNm(object pTxRxType) {
		string sTxRxType = pTxRxType.ToString();
		string sTxRxTypeNm = string.Empty;

		if (sTxRxType.Equals(ViCommConst.TX)) {
			sTxRxTypeNm = "送信";
		} else {
			sTxRxTypeNm = "受信";
		}
		return sTxRxTypeNm;
	}

	protected string GetDelMark(object pTxDelFlag,object pRxDelFlag) {
		string sTx = pTxDelFlag.ToString();
		string sRx = pRxDelFlag.ToString();

		if ((sTx.Equals(ViCommConst.FLAG_OFF_STR)) && (sRx.Equals(ViCommConst.FLAG_OFF_STR))) {
			return string.Empty;
		} else if ((sTx.Equals(ViCommConst.FLAG_ON_STR)) && (sRx.Equals(ViCommConst.FLAG_ON_STR))) {
			return "削除済";
		} else if ((sTx.Equals(ViCommConst.FLAG_OFF_STR)) && (sRx.Equals(ViCommConst.FLAG_ON_STR))) {
			return "受信側削除";
		} else {
			return "送信側削除";
		}
	}

	protected string GetReadNm(object pReadFlag) {
		if (pReadFlag.ToString().Equals(ViCommConst.FLAG_ON_STR)) {
			return "既読";
		} else {
			return "未読";
		}
	}

	protected string GetBatchMark(object pBatchMailFlag) {
		string sBatch = pBatchMailFlag.ToString();

		if (sBatch.Equals(ViCommConst.FLAG_ON_STR)) {
			return "一括送信ﾒｰﾙ";
		} else {
			return string.Empty;
		}
	}

	protected string GetFanClubMemberRankNm(object pMemberRank) {
		string sValue = string.Empty;

		switch (pMemberRank.ToString()) {
			case "1":
				sValue = "ﾌｧﾝｸﾗﾌﾞ";
				break;
			case "2":
				sValue = "ｺﾞｰﾙﾄﾞ";
				break;
			case "3":
				sValue = "ﾌﾟﾗﾁﾅ";
				break;
			case "4":
				sValue = "ﾌﾟﾚﾐｱﾑ";
				break;
		}

		return sValue;
	}

	protected string GenerateOpenPicScript(object pDataItem) {
		DataRowView oDataRowView = (DataRowView)pDataItem;

		string sSiteCd = this.lstSiteCd.SelectedValue;
		string sTxLoginId = iBridUtil.GetStringValue(oDataRowView["TX_LOGIN_ID"]);
		string sPicSeq = iBridUtil.GetStringValue(oDataRowView["PIC_SEQ"]);
		string sTxSexCd = iBridUtil.GetStringValue(oDataRowView["TX_SEX_CD"]);

		string sImgPath = string.Empty;

		if (sTxSexCd.Equals(ViCommConst.MAN)) {
			sImgPath = string.Format("data/{0}/Man/{1}.jpg",sSiteCd,iBridUtil.addZero(sPicSeq,ViCommConst.OBJECT_NM_LENGTH));
		} else {
			sImgPath = string.Format("data/{0}/Operator/{1}/{2}.jpg",sSiteCd,sTxLoginId,iBridUtil.addZero(sPicSeq,ViCommConst.OBJECT_NM_LENGTH));
		}

		return string.Format("javascript:openPicViewer('{0}');",sImgPath);
	}

	protected string GenerateOpenMovieScript(object pDataItem) {
		DataRowView oDataRowView = (DataRowView)pDataItem;

		string sSiteCd = this.lstSiteCd.SelectedValue;
		string sTxLoginId = iBridUtil.GetStringValue(oDataRowView["TX_LOGIN_ID"]);
		string sMovieSeq = iBridUtil.GetStringValue(oDataRowView["MOVIE_SEQ"]);
		string sTxSexCd = iBridUtil.GetStringValue(oDataRowView["TX_SEX_CD"]);

		string sImgPath = string.Empty;

		if (sTxSexCd.Equals(ViCommConst.MAN)) {
			sImgPath = string.Format("~/movie/{0}/Man/{1}.3gp",sSiteCd,iBridUtil.addZero(sMovieSeq,ViCommConst.OBJECT_NM_LENGTH));
		} else {
			sImgPath = string.Format("~/movie/{0}/Operator/{1}/{2}.3gp",sSiteCd,sTxLoginId,iBridUtil.addZero(sMovieSeq,ViCommConst.OBJECT_NM_LENGTH));
		}

		return string.Format("javascript:openMovieViewer('{0}');",this.ResolveUrl(sImgPath));
	}

	protected void vdcMailBox_ServerValidate(object source,ServerValidateEventArgs args) {
		DateTime dtFrom;
		DateTime dtTo;
		try {
			dtFrom = DateTime.Parse(string.Format("{0}/{1}/{2} {3}:00:00",lstMailFromYYYY.SelectedValue,lstMailFromMM.SelectedValue,lstMailFromDD.SelectedValue,lstMailFromHH.SelectedValue));
			dtTo = DateTime.Parse(string.Format("{0}/{1}/{2} {3}:00:00",lstMailToYYYY.SelectedValue,lstMailToMM.SelectedValue,lstMailToDD.SelectedValue,lstMailToHH.SelectedValue));
		} catch {
			vdcMailBox.ErrorMessage = "正しい日時を入力してください";
			args.IsValid = false;
			return;
		}
		if (dtFrom > dtTo) {
			vdcMailBox.ErrorMessage = "日時の大小関係を正しく入力してください";
			args.IsValid = false;
			return;
		}

		args.IsValid = true;
	}

	protected void grdMailBox_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			if (DataBinder.Eval(e.Row.DataItem,"WAIT_TX_FLAG").ToString().Equals(ViCommConst.FLAG_OFF_STR)) {
				if (DataBinder.Eval(e.Row.DataItem,"TXRX_TYPE").ToString().Equals(ViCommConst.TX)) {
					e.Row.BackColor = Color.Azure;
				} else {
					e.Row.BackColor = Color.LavenderBlush;
				}
			} else {
				e.Row.BackColor = Color.LightGray;
			}
		}
	}

	protected string ConvertDate(object pDate) {
		return DateTime.Parse(pDate.ToString()).ToString("yy/MM/dd HH:mm:ss");
	}

	protected void btnTransMail_Click(object sender,EventArgs e) {
		if (IsValid == false) {
			return;
		}

		string[] sDoc;
		int iDocCount;

		string[] sUserSeq = new string[1];
		sUserSeq[0] = lblUserSeq.Text;

		if ((bool)ViewState["TEXT_MAIL_FLAG"]) {
			SysPrograms.SeparateHtml(HttpUtility.HtmlDecode(txtMailDocText.Text.Replace(Environment.NewLine,"<br/>")),ViCommConst.MAX_ORG_MAIL_BLOCKS,out sDoc,out iDocCount);
		} else if (this.DisablePinEdit) {
			SysPrograms.SeparateHtml(HttpUtility.HtmlDecode(txtMailDocText.Text),ViCommConst.MAX_ORG_MAIL_BLOCKS,out sDoc,out iDocCount);
		} else {
			SysPrograms.SeparateHtml(HttpUtility.HtmlDecode(txtMailDocHtml.Text),ViCommConst.MAX_ORG_MAIL_BLOCKS,out sDoc,out iDocCount);
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("TX_ADMIN_TO_CAST_MAIL");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lblCreateMailVisibleSiteCd.Text);
			db.ProcedureInParm("PMAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,lstMailTemplate.SelectedValue);
			db.ProcedureInArrayParm("PCAST_USER_SEQ",DbSession.DbType.VARCHAR2,1,sUserSeq);
			db.ProcedureInParm("PMAN_USER_COUNT",DbSession.DbType.NUMBER,1);
			db.ProcedureInParm("PORIGINAL_TITLE",DbSession.DbType.VARCHAR2,txtMailTitle.Text);
			db.ProcedureInArrayParm("PORIGINAL_DOC",DbSession.DbType.VARCHAR2,iDocCount,sDoc);
			db.ProcedureInParm("PORIGINAL_DOC_COUNT",DbSession.DbType.NUMBER,iDocCount);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ProcedureInParm("pTEST_SEND_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			db.ProcedureInParm("pMAIL_SEND_TYPE",DbSession.DbType.VARCHAR2,string.Empty);
			db.ProcedureInParm("pMAIL_SERVER",DbSession.DbType.VARCHAR2,rdoMailServer.SelectedValue);
			db.ExecuteProcedure();
		}
		btnTransMail.Enabled = false;
		lblAttention.Text = "メール送信が完了しました。";
	}

	protected void lstMailTemplate_SelectedIndexChanged(object sender,EventArgs e) {
		using (MailTemplate oTemplate = new MailTemplate()) {
			if (oTemplate.GetOne(lblCreateMailVisibleSiteCd.Text,lstMailTemplate.SelectedValue)) {
				txtMailTitle.Text = oTemplate.mailTitle;
				if (oTemplate.IsTextMail) {
					txtMailDocText.Visible = true;
					txtMailDocText.Text = oTemplate.textDoc;
					txtMailDocHtml.Visible = false;
					txtMailDocHtml.Text = string.Empty;
				} else if (this.DisablePinEdit) {
					txtMailDocText.Visible = true;
					txtMailDocText.Text = oTemplate.htmlDoc;
					txtMailDocHtml.Visible = false;
					txtMailDocHtml.Text = string.Empty;
				} else {
					txtMailDocText.Visible = false;
					txtMailDocText.Text = string.Empty;
					txtMailDocHtml.Visible = true;
					txtMailDocHtml.Text = oTemplate.htmlDoc;
				}
				ViewState["TEXT_MAIL_FLAG"] = oTemplate.IsTextMail;
			} else {
				txtMailDocText.Visible = false;
				txtMailDocText.Text = string.Empty;
				txtMailDocHtml.Visible = false;
				txtMailDocHtml.Text = string.Empty;
			}
		}
	}

	protected string CheckAddrNg(object pNonExistMailAddrFlag) {
		string sText = string.Empty;

		switch (pNonExistMailAddrFlag.ToString()) {
			case "0":
				sText = "正常";
				break;
			case "1":
				sText = "フィルタリングエラー";
				break;
			case "2":
				sText = "不通エラー";
				break;
		}

		return sText;
	}

	protected string CheckAddrNgM3(object pTxMuller3NgMailAddrFlag) {
		string sText = string.Empty;

		switch (pTxMuller3NgMailAddrFlag.ToString()) {
			case "0":
				sText = "正常";
				break;
			case "1":
				sText = "フィルタリングエラー";
				break;
		}

		return sText;
	}

	protected string TrimEnd(string pTargetStr) {
		char[] trims = { ' ' };
		string sRetStr = pTargetStr.TrimEnd(trims);

		return sRetStr;
	}

	protected Color GetUserStatusColor(object pUserStatus) {

		Color oRetColor = System.Drawing.Color.Black;
		switch (pUserStatus.ToString()) {

			case ViCommConst.USER_WOMAN_AUTH_WAIT:
				oRetColor = System.Drawing.Color.SeaGreen;
				break;
			case ViCommConst.USER_WOMAN_NORMAL:
				oRetColor = System.Drawing.Color.Black;
				break;
			case ViCommConst.USER_WOMAN_STOP:
				oRetColor = System.Drawing.Color.Red;
				break;
			case ViCommConst.USER_WOMAN_HOLD:
				oRetColor = System.Drawing.Color.Blue;
				break;
			case ViCommConst.USER_WOMAN_RESIGNED:
				oRetColor = System.Drawing.Color.Maroon;
				break;
			case ViCommConst.USER_WOMAN_BAN:
				oRetColor = System.Drawing.Color.Red;
				break;
			default:
				break;
		}

		return oRetColor;
	}
	protected void grdBbs_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			if (DataBinder.Eval(e.Row.DataItem,"DEL_FLAG").ToString().Equals(ViCommConst.FLAG_ON_STR)) {
				e.Row.ForeColor = Color.Gray;
			}
		}
	}
	protected string GetDelFlag(object pDelFlag,object pAdminDelFlag) {
		if (pAdminDelFlag.ToString().Equals(ViCommConst.FLAG_ON_STR)) {
			return "管理者削除";
		} else if (pDelFlag.ToString().Equals(ViCommConst.FLAG_ON_STR)) {
			return "ﾕｰｻﾞｰ削除";
		} else {
			return string.Empty;
		}
	}
	protected void lnkDelBbs_Command(object sender,CommandEventArgs e) {
		string[] arguments = e.CommandArgument.ToString().Split(',');

		string sBbsSeq = arguments[0];
		string sexCd = arguments[1];
		int iDelFlag;
		int.TryParse(arguments[2],out iDelFlag);
		using (BbsLog oBbsLog = new BbsLog()) {
			oBbsLog.DeleteBbs(sBbsSeq,sexCd,iDelFlag);
		}
		DataBind();
	}

	protected void lnkSiteUseStatusMainte_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SITE_USE_STATUS_MAINTE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,sKeys[0]);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,ViewState["UserSeq"].ToString());
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,sKeys[1]);
			db.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,ViCommConst.OPERATOR);
			db.ProcedureInParm("pUSE_LIVE_CHAT_FLAG",DbSession.DbType.VARCHAR2,ViCommConst.FLAG_ON);
			db.ProcedureInParm("pUSE_GAME_FLAG",DbSession.DbType.VARCHAR2,ViCommConst.FLAG_OFF);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			DataBind();
		}

	}

	protected void grdManFriends_DataBound(object sender,EventArgs e) {
		GridView oGridView = sender as GridView;
		if (oGridView == null) {
			return;
		}
		if (oGridView.Rows.Count == 0) {
			oGridView.Parent.Visible = false;
		}
	}

	protected void grdManFriendLogs_DataBound(object sender,EventArgs e) {
		GridView oGridView = sender as GridView;
		if (oGridView == null) {
			return;
		}
		// データがない場合、リストを表示しない
		if (oGridView.Rows.Count == 0) {
			oGridView.Parent.Visible = false;
		}
	}

	protected string GetValidMark(object pValue) {
		if (ViCommConst.FLAG_ON_STR.Equals(iBridUtil.GetStringValue(pValue))) {
			return "○";
		} else {
			return "×";
		}
	}

	protected string GetInValidMark(object pValue) {
		if (ViCommConst.NA_CHAR_PF_NOT_APPROVED.ToString().Equals(iBridUtil.GetStringValue(pValue)) ||
			ViCommConst.NA_CHAR_IMPORT.ToString().Equals(iBridUtil.GetStringValue(pValue))) {
			return "×";
		} else {
			return "○";
		}
	}

	protected string GetBlogArticleMark(object pRowIndex) {
		IDictionary oDataKeys = this.grdCharacter.DataKeys[int.Parse(pRowIndex.ToString())].Values;
		string sSiteCd = iBridUtil.GetStringValue(oDataKeys["SITE_CD"]);
		string sUserSeq = iBridUtil.GetStringValue(oDataKeys["USER_SEQ"]);
		string sUserCharNo = iBridUtil.GetStringValue(oDataKeys["USER_CHAR_NO"]);

		using (CastCharacterEx oCastCharacterEx = new CastCharacterEx()) {
			using (DataSet oDataSet = oCastCharacterEx.GetOne(sSiteCd,sUserSeq,sUserCharNo)) {
				if (oDataSet.Tables[0].Rows.Count > 0) {
					DataRow oDataRow = oDataSet.Tables[0].Rows[0];
					if (ViCommConst.FLAG_OFF_STR.Equals(oDataRow["ENABLED_BLOG_FLAG"].ToString())) {
						return "ブログなし";
					}
				}
			}
		}

		return "ブログあり";
	}

	protected string GetFanClubStatusEnableCount(object pRowIndex) {
		int iRowCount = 0;
		IDictionary oDataKeys = this.grdCharacter.DataKeys[int.Parse(pRowIndex.ToString())].Values;
		FanClubStatus.SearchCondition oSearchCondition = new FanClubStatus.SearchCondition();
		oSearchCondition.SiteCd = iBridUtil.GetStringValue(oDataKeys["SITE_CD"]);
		oSearchCondition.CastUserSeq = iBridUtil.GetStringValue(oDataKeys["USER_SEQ"]);
		oSearchCondition.CastCharNo = iBridUtil.GetStringValue(oDataKeys["USER_CHAR_NO"]);
		oSearchCondition.EnableFlag = ViCommConst.FLAG_ON_STR;
		using (FanClubStatus oFanClubStatus = new FanClubStatus()) {
			iRowCount = oFanClubStatus.GetPageCount(oSearchCondition);
		}
		return string.Format("FC会員:{0}人",iRowCount.ToString());
	}

    protected bool IsGamecharacter(string pSiteCd, string pUserSeq, string pUserCharNo) {
        using (CastCharacterEx oCastCharacterEx = new CastCharacterEx()) {
            using (DataSet oDataSet = oCastCharacterEx.GetOne(pSiteCd, pUserSeq, pUserCharNo)) {
                if (oDataSet.Tables[0].Rows.Count > 0) {
                    DataRow oDataRow = oDataSet.Tables[0].Rows[0];
                    string sSiteUseStatus = oDataRow["SITE_USE_STATUS"].ToString();
                    if (sSiteUseStatus.Equals(ViCommConst.SiteUseStatus.GAME_ONLY) ||
                    sSiteUseStatus.Equals(ViCommConst.SiteUseStatus.LIVE_CHAT_AND_GAME)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
    protected string GetGameCharacterMark(object pRowIndex) {
        IDictionary oDataKeys = this.grdCharacter.DataKeys[int.Parse(pRowIndex.ToString())].Values;
        string sSiteCd = iBridUtil.GetStringValue(oDataKeys["SITE_CD"]);
        string sUserSeq = iBridUtil.GetStringValue(oDataKeys["USER_SEQ"]);
        string sUserCharNo = iBridUtil.GetStringValue(oDataKeys["USER_CHAR_NO"]);

        if (IsGamecharacter(sSiteCd, sUserSeq, sUserCharNo)) {
            return "ｹﾞｰﾑｷｬﾗｸﾀｰあり";
        }
        return "ｹﾞｰﾑｷｬﾗｸﾀｰなし";
    }

	protected void dsCastFriends_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = iBridUtil.GetStringValue(ViewState["SiteCd"]);
		e.InputParameters["pFriendIntroCd"] = iBridUtil.GetStringValue(ViewState["FiendIntroCd"]);
	}

	protected void dsManFriends_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = iBridUtil.GetStringValue(ViewState["SiteCd"]);
		e.InputParameters["pFriendIntroCd"] = iBridUtil.GetStringValue(ViewState["FiendIntroCd"]);
	}

	protected void dsManFriendLogs_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pFriendIntroCd"] = iBridUtil.GetStringValue(ViewState["FiendIntroCd"]);
	}

	protected void dsModifyMailHis_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pUserSeq"] = iBridUtil.GetStringValue(ViewState["UserSeq"]);
	}

	protected void dsNgMailHistory_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pUserSeq"] = iBridUtil.GetStringValue(ViewState["UserSeq"]);
	}

	protected void dsModifyTelHis_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pUserSeq"] = iBridUtil.GetStringValue(ViewState["UserSeq"]);
	}

	protected void dsModifyLoginPassword_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = ViCommConst.CAST_SITE_CD;
		e.InputParameters["pUserSeq"] = iBridUtil.GetStringValue(ViewState["UserSeq"]);
		e.InputParameters["pUserCharNo"] = ViCommConst.MAIN_CHAR_NO;
	}

	protected bool IsPointVisible(object pUserRank) {
		using (CastRank oCastRank = new CastRank()) {
			int? oPointPrice = oCastRank.GetPointPrice(iBridUtil.GetStringValue(pUserRank));
			if (oPointPrice.HasValue) {
				if (oPointPrice.Value == 1) {
					return false;
				}
			}
		}

		return true;
	}

	protected void dvwCast_DataBound(object sender,EventArgs e) {
		DetailsView oDetailsView = sender as DetailsView;
		if (oDetailsView != null) {
			Label oLabel = oDetailsView.FindControl("lblUserRank") as Label;
			if (oLabel != null) {
				oDetailsView.Fields[20].Visible = this.IsPointVisible(oLabel.Text);
			}
		}
	}

	protected string GetHandleNameLinkText(object pHandleNm) {
		string sHandleName = iBridUtil.GetStringValue(pHandleNm);
		if (sHandleName.Trim().Length > 0) {
			return sHandleName;
		}

		return "(未設定)";
	}

	protected string GetAdCd(object pUserSeq) {
		string sUserSeq = iBridUtil.GetStringValue(pUserSeq);
		string sAdCd = string.Empty;
		DataSet ds = new DataSet();
		using (SiteUser oSiteUser = new SiteUser()) {
			ds = oSiteUser.GetAdCdByUserSeq(sUserSeq);
		}

		if (ds.Tables[0].Rows.Count > 0) {
			sAdCd = string.Format("{0} {1}",ds.Tables[0].Rows[0]["AD_CD"],ds.Tables[0].Rows[0]["AD_NM"]);
		}
		return sAdCd;
	}

	protected string GetLastLoginDate(object pUserSeq) {
		string sUserSeq = iBridUtil.GetStringValue(pUserSeq);
		string sLastLoginDate = string.Empty;
		DataSet ds = new DataSet();
		using (CastCharacter oCastCharacter = new CastCharacter()) {
			sLastLoginDate = oCastCharacter.GetLastLoginDate(sUserSeq);
		}

		return sLastLoginDate;
	}

	protected void btnDelWithdrawal_Click(object sender,EventArgs e) {
		UpdateWithdrawal(string.Empty,ViCommConst.FLAG_ON);
	}

	protected void btnUpdateWithdrawal_Click(object sender,EventArgs e) {
		UpdateWithdrawal(string.Empty,ViCommConst.FLAG_OFF);
	}

	protected void btnAcceptWithdrawal_Click(object sender,EventArgs e) {
		lblErrorMessageWithdrawal.Text = string.Empty;
		lblErrorMessageWithdrawal.Visible = false;
		
		if (this.IsExistInvestigateTarget(iBridUtil.GetStringValue(ViewState["UserSeq"]))) {
			lblErrorMessageWithdrawal.Text = "この娘を探せターゲットです。";
			lblErrorMessageWithdrawal.Visible = true;
		} else {
			UpdateWithdrawal(ViCommConst.WITHDRAWAL_STATUS_COMPLIATE,ViCommConst.FLAG_OFF);
		}
	}

	protected void btnReturnWithdrawalList_Click(object sender,EventArgs e) {
		Server.Transfer(string.Format("../Extension/AcceptWithdrawalList.aspx?sexcd={0}&reload=1",ViCommConst.OPERATOR));
	}

	private void UpdateWithdrawal(string pStatus,int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("WITHDRAWAL_MAINTE");
			db.ProcedureBothParm("PWITHDRAWAL_SEQ",DbSession.DbType.VARCHAR2,lblWithdrawalSeq.Text);
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,string.Empty);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,string.Empty);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,string.Empty);
			db.ProcedureInParm("PWITHDRAWAL_STATUS",DbSession.DbType.VARCHAR2,pStatus);
			db.ProcedureInParm("PWITHDRAWAL_REASON_CD",DbSession.DbType.VARCHAR2,string.Empty);
			db.ProcedureInParm("PWITHDRAWAL_REASON_DOC",DbSession.DbType.VARCHAR2,txtWithdrawalReasonDoc.Text);
			db.ProcedureInParm("PREMARKS",DbSession.DbType.VARCHAR2,txtWithdrawalRemarks.Text);
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.VARCHAR2,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			DataBind();
		}
	}
	protected void btnBingoEntryMainte_Click(object sender,EventArgs e) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("BINGO_ENTRY_MAINTE");
			db.ProcedureInParm("PBINGO_ENTRY_SEQ",DbSession.DbType.VARCHAR2,lblBingoEntrySeq.Text);
			db.ProcedureInParm("PBINGO_POINT_PAYMENT_FLAG",DbSession.DbType.VARCHAR2,rdoBingoPointPayment.Checked ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			DataBind();
		}
	}

	protected void btnReturnBingoEntryList_Click(object sender,EventArgs e) {
		Server.Transfer(string.Format("../Extension/MailDeBingoPrizeGetList.aspx?sexcd={0}&seq={1}",ViCommConst.OPERATOR,lblBingoTermSeq.Text));
	}

    protected void SetGameCharacter(string pSiteCd, string pUserSeq, string pUserCharNo) {
        this.pnlGameCharacter.Visible = false;

        using (CastCharacterEx oUserEx = new CastCharacterEx()) {
            using (DataSet oDataSet = oUserEx.GetOne(pSiteCd, pUserSeq, pUserCharNo)) {
                if (oDataSet.Tables[0].Rows.Count > 0) {
                    string sSiteUseStatus = oDataSet.Tables[0].Rows[0]["SITE_USE_STATUS"].ToString();
                    if (sSiteUseStatus.Equals(ViCommConst.SiteUseStatus.GAME_ONLY) ||
                        sSiteUseStatus.Equals(ViCommConst.SiteUseStatus.LIVE_CHAT_AND_GAME)) {
                        this.pnlGameCharacter.Visible = true;

                        using (GameCharacter oGameCharacter = new GameCharacter()) {
                            using (DataSet oGameCharData = oGameCharacter.GetOne(pSiteCd, pUserSeq, pUserCharNo, ViCommConst.OPERATOR)) {
                                foreach (DataRow oGameCharDr in oGameCharData.Tables[0].Rows) {

                                    Label oTmpControl = this.plcGameHolder.FindControl("lblGameCharacterHandleNm") as Label;
                                    oTmpControl.Text = oGameCharDr["GAME_HANDLE_NM"].ToString();

                                    oTmpControl = this.plcGameHolder.FindControl("lblGameCharacterTypeNm") as Label;
                                    oTmpControl.Text = oGameCharDr["GAME_CHAR_TYPE_NM"].ToString();

                                    oTmpControl = this.plcGameHolder.FindControl("lblGameCharacterPoint") as Label;
                                    oTmpControl.Text = oGameCharDr["GAME_POINT"].ToString();

									oTmpControl = this.plcGameHolder.FindControl("lblGameCharacterCastPoint") as Label;
									oTmpControl.Text = oGameCharDr["CAST_GAME_POINT"].ToString();

                                    oTmpControl = this.plcGameHolder.FindControl("lblGameCharacterLevel") as Label;
                                    oTmpControl.Text = oGameCharDr["GAME_CHARACTER_LEVEL"].ToString();

                                    oTmpControl = this.plcGameHolder.FindControl("lblGameCharacterExp") as Label;
                                    oTmpControl.Text = oGameCharDr["EXP"].ToString();

                                    oTmpControl = this.plcGameHolder.FindControl("lblGameCharacterRestExp") as Label;
                                    oTmpControl.Text = oGameCharDr["REST_EXP"].ToString();

                                    oTmpControl = this.plcGameHolder.FindControl("lblGameCharacterTotalExp") as Label;
                                    oTmpControl.Text = oGameCharDr["TOTAL_EXP"].ToString();

									oTmpControl = this.plcGameHolder.FindControl("lblGameCharacterUnassignedFroce") as Label;
									oTmpControl.Text = oGameCharDr["UNASSIGNED_FORCE_COUNT"].ToString();

									oTmpControl = this.plcGameHolder.FindControl("lblGameCharacterMissionMaxForce") as Label;
									oTmpControl.Text = oGameCharDr["MISSION_MAX_FORCE_COUNT"].ToString();

									oTmpControl = this.plcGameHolder.FindControl("lblGameCharacterAttackMaxForce") as Label;
									oTmpControl.Text = oGameCharDr["ATTACK_MAX_FORCE_COUNT"].ToString();

									oTmpControl = this.plcGameHolder.FindControl("lblGameCharacterDefenceMaxForce") as Label;
									oTmpControl.Text = oGameCharDr["DEFENCE_MAX_FORCE_COUNT"].ToString();

                                    oTmpControl = this.plcGameHolder.FindControl("lblGameCharacterCooperation") as Label;
                                    oTmpControl.Text = oGameCharDr["COOPERATION_POINT"].ToString();

                                    oTmpControl = this.plcGameHolder.FindControl("lblGameCharacterStageNm") as Label;
                                    oTmpControl.Text = oGameCharDr["STAGE_NM"].ToString();

									oTmpControl = this.plcGameHolder.FindControl("lblGameCharacterRegistDate") as Label;
									oTmpControl.Text = oGameCharDr["GAME_REGIST_DATE"].ToString();

                                    HyperLink oTmpLink = this.plcGameHolder.FindControl("lnkGameCharacterMainteLink") as HyperLink;
                                    oTmpLink.NavigateUrl = GetGameCharacterMainteLink(pSiteCd,pUserSeq,pUserCharNo);

									ViewState["RevisionNoGame"] = oGameCharDr["REVISION_NO"].ToString();
									ViewState["RevisionNoCastPoint"] = oGameCharDr["CAST_POINT_REVISION_NO"].ToString();
                                }
                            }
                        }
                    }
                }
            }
        }
    }

	protected void btnAddGamePoint_Click(object sender, EventArgs e) {

		if (IsValid) {
			using (DbSession oDbSession = new DbSession()) {
				oDbSession.PrepareProcedure("GAME_POINT_UPDATE");
				oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, iBridUtil.GetStringValue(ViewState["SiteCd"]));
				oDbSession.ProcedureInParm("pUSER_SEQ", DbSession.DbType.VARCHAR2, iBridUtil.GetStringValue(ViewState["UserSeq"]));
				oDbSession.ProcedureInParm("pUSER_CHAR_NO", DbSession.DbType.VARCHAR2, iBridUtil.GetStringValue(ViewState["UserCharNo"]));
				oDbSession.ProcedureInParm("pADD_POINT", DbSession.DbType.VARCHAR2, txtAddGamePoint.Text);
				oDbSession.ProcedureInParm("pREVISION_NO", DbSession.DbType.NUMBER, iBridUtil.GetStringValue(ViewState["RevisionNoGame"]));
				oDbSession.ProcedureOutParm("pRESULT", DbSession.DbType.VARCHAR2);
				oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
				oDbSession.ExecuteProcedure();
			}
			DisplayGameCharacterInfo(iBridUtil.GetStringValue(ViewState["SiteCd"]), iBridUtil.GetStringValue(ViewState["UserSeq"]), iBridUtil.GetStringValue(ViewState["UserCharNo"]));
			txtAddGamePoint.Text = string.Empty;
		}
	}
	protected void vdcAddGamePoint_ServerValidate(object sender, ServerValidateEventArgs args) {
		if (IsValid) {
			args.IsValid = true;
			Label lblGameCharacterPoint = this.plcGameHolder.FindControl("lblGameCharacterPoint") as Label;
			if (lblGameCharacterPoint.Text.Equals(string.Empty) ||
				txtAddGamePoint.Text.Equals(string.Empty) ||
				(int.Parse(lblGameCharacterPoint.Text) + int.Parse(txtAddGamePoint.Text)) < 0) {
				args.IsValid = false;
			}
		}

	}

	protected void btnAddCastGamePoint_Click(object sender, EventArgs e) {

		if (IsValid) {
			using (DbSession oDbSession = new DbSession()) {
				oDbSession.PrepareProcedure("CAST_GAME_POINT_UPDATE");
				oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, iBridUtil.GetStringValue(ViewState["SiteCd"]));
				oDbSession.ProcedureInParm("pUSER_SEQ", DbSession.DbType.VARCHAR2, iBridUtil.GetStringValue(ViewState["UserSeq"]));
				oDbSession.ProcedureInParm("pUSER_CHAR_NO", DbSession.DbType.VARCHAR2, iBridUtil.GetStringValue(ViewState["UserCharNo"]));
				oDbSession.ProcedureInParm("pADD_POINT", DbSession.DbType.VARCHAR2, txtAddCastGamePoint.Text);
				oDbSession.ProcedureInParm("pREVISION_NO", DbSession.DbType.NUMBER, iBridUtil.GetStringValue(ViewState["RevisionNoCastPoint"]));
				oDbSession.ProcedureOutParm("pRESULT", DbSession.DbType.VARCHAR2);
				oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
				oDbSession.ExecuteProcedure();
			}
			DisplayGameCharacterInfo(iBridUtil.GetStringValue(ViewState["SiteCd"]), iBridUtil.GetStringValue(ViewState["UserSeq"]), iBridUtil.GetStringValue(ViewState["UserCharNo"]));
			txtAddCastGamePoint.Text = string.Empty;
		}
	}
	protected void vdcAddCastGamePoint_ServerValidate(object sender, ServerValidateEventArgs args) {
		if (IsValid) {
			args.IsValid = true;
			Label lblGameCharacterCastPoint = this.plcGameHolder.FindControl("lblGameCharacterCastPoint") as Label;
			if (lblGameCharacterCastPoint.Text.Equals(string.Empty) ||
				txtAddCastGamePoint.Text.Equals(string.Empty) ||
				(int.Parse(lblGameCharacterCastPoint.Text) + int.Parse(txtAddCastGamePoint.Text)) < 0) {
				args.IsValid = false;
			}
		}

	}

	protected void lst_DataBound(object sender, EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0, new ListItem(string.Empty, string.Empty));
	}

	protected void lstGameItemCategory_IndexChanged(object sender, EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		this.ViewState["GameItemCategoryType"] = oDropDownList.SelectedValue;
		this.lstGameItem.DataBind();
	}

	protected void dsGameItemCategory_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = this.ViewState["SiteCd"];
	}
	protected void dsGameItem_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = this.ViewState["SiteCd"];
		e.InputParameters["pSexCd"] = ViCommConst.OPERATOR;
		e.InputParameters["pGameItemCategoryType"] = this.ViewState["GameItemCategoryType"];
		e.InputParameters["pGameItemGetCd"] = this.GameItemGetCd;
		e.InputParameters["pPresentFlag"] = this.GameItemPresent;
	}
	protected void btnAddGameItem_Click(object sender, EventArgs e) {

		if (IsValid) {
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("GAME_POSSESSION_ITEM_MAINTE");
				db.ProcedureInParm("PSITE_CD", DbSession.DbType.VARCHAR2, iBridUtil.GetStringValue(ViewState["SiteCd"]));
				db.ProcedureInParm("PUSER_SEQ", DbSession.DbType.VARCHAR2, iBridUtil.GetStringValue(ViewState["UserSeq"]));
				db.ProcedureInParm("PUSER_CHAR_NO", DbSession.DbType.VARCHAR2, iBridUtil.GetStringValue(ViewState["UserCharNo"]));
				db.ProcedureInParm("pGAME_ITEM_SEQ", DbSession.DbType.VARCHAR2, this.lstGameItem.SelectedValue);
				db.ProcedureInParm("pADD_COUNT", DbSession.DbType.NUMBER, int.Parse(txtGameItemGiveCount.Text));
				db.ProcedureInParm("pWITHOUT_COMMIT", DbSession.DbType.NUMBER, 1);
				db.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
			}
			DisplayGameCharacterInfo(iBridUtil.GetStringValue(ViewState["SiteCd"]), iBridUtil.GetStringValue(ViewState["UserSeq"]), iBridUtil.GetStringValue(ViewState["UserCharNo"]));
			this.txtGameItemGiveCount.Text = string.Empty;
			lstItemGetCd.SelectedValue = null;
			lstItemPresent.SelectedValue = null;
			lstGameItem.SelectedValue = null;
		}
	}

	protected void btnSeekPossessionGameItem_Click(object sender,EventArgs e) {
		grdPossessionGameItem.DataBind();
	}

	protected void dsPossessionGameItem_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = iBridUtil.GetStringValue(ViewState["SiteCd"]);
		e.InputParameters["pUserSeq"] = iBridUtil.GetStringValue(ViewState["UserSeq"]);
		e.InputParameters["pUserCharNo"] = iBridUtil.GetStringValue(ViewState["UserCharNo"]);
	}

	protected void btnAddExp_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		string sSiteCd = iBridUtil.GetStringValue(ViewState["SiteCd"]);
		string sUserSeq = iBridUtil.GetStringValue(ViewState["UserSeq"]);
		string sUserCharNo = iBridUtil.GetStringValue(ViewState["UserCharNo"]);

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("ADD_EXP");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,sSiteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,sUserSeq);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,sUserCharNo);
			oDbSession.ProcedureInParm("pADD_EXP",DbSession.DbType.NUMBER,this.txtAddExp.Text);
			oDbSession.ProcedureOutParm("pADD_FORCE_COUNT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pLEVEL_UP_FLAG",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.txtAddExp.Text = string.Empty;
		this.SetGameCharacter(sSiteCd,sUserSeq,sUserCharNo);
	}

	protected void lnkPossessionGameItem_Click(object sender,EventArgs e) {
		this.pnlPossessionGameItem.Visible = true;
		this.grdPossessionGameItem.DataSourceID = "dsPossessionGameItem";
		this.grdPossessionGameItem.DataBind();
	}

	protected void btnClosePossessionGameItem_Click(object sender,EventArgs e) {
		this.pnlPossessionGameItem.Visible = false;
		this.grdPossessionGameItem.DataSourceID = string.Empty;
	}

    protected string GetGameCharacterMainteLink(string pSiteCd,string pUserSeq,string pUserCharNo) {
        return string.Format("../Extension/GameCharacterMainte.aspx?site_cd={0}&user_seq={1}&user_char_no={2}&sex_cd={3}&login_id={4}&return={5}",
            pSiteCd,
            pUserSeq,
            pUserCharNo,
            ViCommConst.OPERATOR,
            txtLoginId.Text,
            iBridUtil.GetStringValue(ViewState["RETURN"]));
    }

	protected string GetModifyUserNm(object pModifyUserCd) {
		return (pModifyUserCd.ToString().Equals(ViCommConst.ModifyUserCd.Admin)) ? "管理者変更" : "ユーザー変更";
	}

	protected string GetModifyBeforeAfterNm(object pModifyBeforeAfterCd) {
		string sModifyBeforeAfterNm = string.Empty;

		switch (pModifyBeforeAfterCd.ToString()) {
			case "1":
				sModifyBeforeAfterNm = "変更前";
				break;
			case "2":
				sModifyBeforeAfterNm = "変更後";
				break;
		}

		return sModifyBeforeAfterNm;
	}

	protected string GetEnabledUserDefineFlagMark(object pUserDefineMask) {
		int iUserDefineMask = 0;
		if (!int.TryParse(iBridUtil.GetStringValue(pUserDefineMask),out iUserDefineMask)) {
			return string.Empty;
		}
		StringBuilder oMark = new StringBuilder();
		using (CodeDtl oCodeDtl = new CodeDtl()) {
			foreach (DataRow oRow in oCodeDtl.GetList(ViCommConst.CODE_TYPE_USER_DEFINE_FLAG_WOMAN).Tables[0].Rows) {
				if ((iUserDefineMask & int.Parse(oRow["CODE"].ToString())) > 0) {
					oMark.AppendFormat("{0}<br />",oRow["CODE_NM"]);
				}
			}
		}
		return oMark.ToString();
	}

	protected void btnBbsMovieUpdate_OnCommand(object sender,CommandEventArgs e) {
		string[] sArgs = e.CommandArgument.ToString().Split(',');
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_CAST_MOVIE_STATUS");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,sArgs[0]);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,sArgs[1]);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,sArgs[2]);
			db.ProcedureInParm("pMOVIE_SEQ",DbSession.DbType.VARCHAR2,sArgs[3]);
			db.ProcedureInParm("pMOIVE_TYPE",DbSession.DbType.NUMBER,sArgs[4]);
			db.ProcedureInParm("pOBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,0);
			db.ProcedureInParm("pOBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER,0);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		this.grdBbsMovie.DataBind();
	}

	protected void btnBbsPicUpdate_OnCommand(object sender,CommandEventArgs e) {
		string[] sArgs = e.CommandArgument.ToString().Split(',');
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_CAST_PIC_STATUS");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,sArgs[0]);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,sArgs[1]);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,sArgs[2]);
			db.ProcedureInParm("pPIC_SEQ",DbSession.DbType.VARCHAR2,sArgs[3]);
			db.ProcedureInParm("pPIC_TYPE",DbSession.DbType.NUMBER,sArgs[4]);
			db.ProcedureInParm("pOBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,0);
			db.ProcedureInParm("pOBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER,0);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		this.grdBbsPic.DataBind();
	}

	protected void lstItemGetCd_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		this.GameItemGetCd = oDropDownList.SelectedValue;
		this.lstGameItem.DataBind();
	}

	protected void lstItemPresent_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		this.GameItemPresent = oDropDownList.SelectedValue;
		this.lstGameItem.DataBind();
	}

	protected void lstItemGetCd_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		oDropDownList.Items.Insert(3,new ListItem("戦利品","3"));
	}

	protected string GetErrorTypeText(object pErrorType) {
		string sText = string.Empty;

		switch (pErrorType.ToString()) {
			case "1":
				sText = "ﾌｨﾙﾀﾘﾝｸﾞ";
				break;
			case "2":
				sText = "不通";
				break;
		}

		return sText;
	}
	
	private bool IsExistInvestigateTarget(string pUserSeq) {
		bool bOk;
		using (InvestigateTarget oInvestigateTarget = new InvestigateTarget()) {
			bOk = oInvestigateTarget.IsExistTarget(pUserSeq);
		}
		
		return bOk;
	}

	/// <summary>
	/// 退会履歴リンク押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void lnkWithdrawalHis_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		string sSiteCd = sKeys[0];
		string sUserSeq = sKeys[1];

		// 初期表示状態に戻す
		CloseExtInfo();

		Session["WithdrawalSiteCd"] = sSiteCd;
		Session["WithdrawalUserSeq"] = sUserSeq;

		// 退会履歴一覧を表示する
		pnlWithdrawalHistoryList.Visible = true;
		grdWithdrawalHistoryList.DataSourceID = "dsWithdrawalHistoryList";
		grdWithdrawalHistoryList.DataBind();
	}

	/// <summary>
	/// 退会履歴のバリデーション実行
	/// </summary>
	/// <returns></returns>
	private bool validateWithdrawalHistory() {
		vrdWithdrawalHistoryDate.Validate();
		vdrWithdrawalHistoryRemarks.Validate();
		vdeWithdrawalHistoryAddPointCount.Validate();
		if (!vrdWithdrawalHistoryDate.IsValid) {
			return false;
		}
		if (!vdrWithdrawalHistoryRemarks.IsValid) {
			return false;
		}
		if (!vdeWithdrawalHistoryAddPointCount.IsValid) {
			return false;
		}
		return true;
	}

	/// <summary>
	/// (退会履歴)登録ボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnRegWithdrawalHistory_Click(object sender,EventArgs e) {
		string sSiteCd = Session["WithdrawalSiteCd"].ToString();
		string sUserSeq = Session["WithdrawalUserSeq"].ToString();
		string sStatus = string.Empty;
		string sCreateDate = string.Format(
			"{0}/{1}/{2} {3}:{4}:{5}",
			lstWithdrawalYYYY.SelectedValue,
			lstWithdrawalMM.SelectedValue,
			lstWithdrawalDD.SelectedValue,
			lstWithdrawalHH.SelectedValue,
			lstWithdrawalMI.SelectedValue,
			lblWithdrawalSS.Text
		);
		string sWithdrawalHistorySeq = string.Empty;

		// バリデーション実行
		if (!validateWithdrawalHistory()) {
			return;
		}

		// 退会履歴の追加・更新
		WithdrawalHistory oWithdrawalHistory = new WithdrawalHistory();
		oWithdrawalHistory.Mainte(
			sSiteCd,
			sUserSeq,
			null,
			iBridUtil.GetStringValue(Request.QueryString["withdrawalseq"]),
			ref sWithdrawalHistorySeq,
			txtWithdrawalHistoryAddPointCount.Text,
			txtWithdrawalHistoryRemarks.Text,
			sCreateDate,
			ViCommConst.FLAG_OFF_STR,
			out sStatus
		);
		// 入力欄を非表示
		pnlWithdrawalHistoryDetail.Visible = false;
		// 退会履歴一覧を再取得
		grdWithdrawalHistoryList.DataBind();
	}

	/// <summary>
	/// (退会履歴)更新ボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnUpWithdrawalHistory_Click(object sender,EventArgs e) {
		string sSiteCd = Session["WithdrawalSiteCd"].ToString();
		string sUserSeq = Session["WithdrawalUserSeq"].ToString();
		string sStatus = string.Empty;
		string sCreateDate = string.Format(
			"{0}/{1}/{2} {3}:{4}:{5}",
			lstWithdrawalYYYY.SelectedValue,
			lstWithdrawalMM.SelectedValue,
			lstWithdrawalDD.SelectedValue,
			lstWithdrawalHH.SelectedValue,
			lstWithdrawalMI.SelectedValue,
			lblWithdrawalSS.Text
		);
		string sWithdrawalHistorySeq = lblWithdrawalHistorySeq.Text;

		// バリデーション実行
		if (!validateWithdrawalHistory()) {
			return;
		}

		// 退会履歴の追加・更新
		WithdrawalHistory oWithdrawalHistory = new WithdrawalHistory();
		oWithdrawalHistory.Mainte(
			sSiteCd,
			sUserSeq,
			null,
			iBridUtil.GetStringValue(Request.QueryString["withdrawalseq"]),
			ref sWithdrawalHistorySeq,
			txtWithdrawalHistoryAddPointCount.Text,
			txtWithdrawalHistoryRemarks.Text,
			sCreateDate,
			ViCommConst.FLAG_OFF_STR,
			out sStatus
		);
		// 入力欄を非表示
		pnlWithdrawalHistoryDetail.Visible = false;
		// 退会履歴一覧を再取得
		grdWithdrawalHistoryList.DataBind();
	}

	/// <summary>
	/// (退会履歴)削除ボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnDelWithdrawalHistory_Click(object sender,EventArgs e) {
		string sSiteCd = Session["WithdrawalSiteCd"].ToString();
		string sUserSeq = Session["WithdrawalUserSeq"].ToString();
		string sStatus = string.Empty;
		string sWithdrawalHistorySeq = lblWithdrawalHistorySeq.Text;

		// 退会履歴の削除
		WithdrawalHistory oWithdrawalHistory = new WithdrawalHistory();
		oWithdrawalHistory.Mainte(
			sSiteCd,
			sUserSeq,
			null,
			iBridUtil.GetStringValue(Request.QueryString["withdrawalseq"]),
			ref sWithdrawalHistorySeq,
			null,
			null,
			null,
			ViCommConst.FLAG_ON_STR,
			out sStatus
		);
		// 入力欄を非表示
		pnlWithdrawalHistoryDetail.Visible = false;
		// 退会履歴一覧を再取得
		grdWithdrawalHistoryList.DataBind();
	}

	/// <summary>
	/// (退会履歴一覧)新規登録ボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnNewWithdrawalHistory_Click(object sender,EventArgs e) {
		pnlWithdrawalHistoryDetail.Visible = true;

		lblWithdrawalHistorySeq.Text = string.Empty;
		txtWithdrawalHistoryRemarks.Text = string.Empty;
		txtWithdrawalHistoryAddPointCount.Text = string.Empty;

		lstWithdrawalYYYY.SelectedIndex = 0;
		lstWithdrawalMM.SelectedValue = DateTime.Now.ToString("MM");
		lstWithdrawalDD.SelectedValue = DateTime.Now.ToString("dd");
		lstWithdrawalHH.SelectedValue = DateTime.Now.ToString("HH");
		lstWithdrawalMI.SelectedValue = DateTime.Now.ToString("mm");
		lblWithdrawalSS.Text = DateTime.Now.ToString("ss");

		btnRegistWithdrawalHistory.Visible = true;
		btnUpdateWithdrawalHistory.Visible = false;
		btnDeleteWithdrawalHistory.Visible = false;
	}

	/// <summary>
	/// (退会履歴)閉じるボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnCloseWithdrawalHistoryDetail_Click(object sender,EventArgs e) {
		pnlWithdrawalHistoryDetail.Visible = false;
	}

	/// <summary>
	/// (退会履歴一覧)閉じるボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnCloseWithdrawalHistoryList_Click(object sender,EventArgs e) {
		grdWithdrawalHistoryList.DataSourceID = string.Empty;
		pnlWithdrawalHistoryDetail.Visible = false;
		pnlWithdrawalHistoryList.Visible = false;
	}

	/// <summary>
	/// 退会履歴取得用パラメータを設定
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void dsWithdrawalHistoryList_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		string sSiteCd = lblWithdrawalHistorySiteCd.Text;
		if (string.IsNullOrEmpty(sSiteCd)) {
			if (Session["WithdrawalSiteCd"] == null || string.IsNullOrEmpty(Session["WithdrawalSiteCd"].ToString())) {
				sSiteCd = PwViCommConst.MAIN_SITE_CD;
			} else {
				sSiteCd = Session["WithdrawalSiteCd"].ToString();
			}
		}
		Session["WithdrawalSiteCd"] = sSiteCd;

		string sUserSeq = iBridUtil.GetStringValue(ViewState["UserSeq"]);
		Session["WithdrawalUserSeq"] = sUserSeq;

		e.InputParameters[0] = sSiteCd;
		e.InputParameters[1] = sUserSeq;
	}

	/// <summary>
	/// 退会履歴内容の取得（整形して返す）
	/// </summary>
	/// <param name="pRemarks"></param>
	/// <param name="pAddPointCnt"></param>
	/// <returns></returns>
	protected string GetWithdrawalHistoryRemarks(object pRemarks,object pAddPointCnt) {
		string sRemarks = iBridUtil.GetStringValue(pRemarks);
		string sAddPointCnt = iBridUtil.GetStringValue(pAddPointCnt);
		if (!string.IsNullOrEmpty(sAddPointCnt)) {
			return string.Format("{0}/{1}回目",sRemarks,sAddPointCnt);
		}
		return sRemarks;
	}

	/// <summary>
	/// (退会履歴一覧)編集リンク押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void lnkEditWithdrawalHistory_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		string sSiteCd = sKeys[0];
		string sWithdrawalHistorySeq = sKeys[1];

		using (WithdrawalHistory oWithdrawalHistory = new WithdrawalHistory()) {
			DataSet ds = oWithdrawalHistory.GetOne(sSiteCd,iBridUtil.GetStringValue(ViewState["USER_SEQ"]),sWithdrawalHistorySeq);
			if (ds.Tables[0].Rows.Count > 0) {
				// 退会履歴の入力欄を表示
				pnlWithdrawalHistoryDetail.Visible = true;

				// 登録ボタンを非表示、更新・削除ボタンを表示
				btnRegistWithdrawalHistory.Visible = false;
				btnUpdateWithdrawalHistory.Visible = true;
				btnDeleteWithdrawalHistory.Visible = true;

				// 退会履歴の内容を設定
				DataRow dr = ds.Tables[0].Rows[0];
				lblWithdrawalHistorySiteCd.Text = dr["SITE_CD"].ToString();
				lblWithdrawalHistorySeq.Text = dr["WITHDRAWAL_HISTORY_SEQ"].ToString();
				txtWithdrawalHistoryRemarks.Text = dr["REMARKS"].ToString();
				txtWithdrawalHistoryAddPointCount.Text = dr["WITHDRAWAL_ADD_POINT_CNT"].ToString();

				DateTime dt = DateTime.Parse(dr["CREATE_DATE"].ToString());
				lstWithdrawalYYYY.SelectedValue = dt.ToString("yyyy");
				lstWithdrawalMM.SelectedValue = dt.ToString("MM");
				lstWithdrawalDD.SelectedValue = dt.ToString("dd");
				lstWithdrawalHH.SelectedValue = dt.ToString("HH");
				lstWithdrawalMI.SelectedValue = dt.ToString("mm");
				lblWithdrawalSS.Text = dt.ToString("ss");
			}
		}
	}

	/// <summary>
	/// 退会履歴関連を初期化
	/// </summary>
	private void ClearWithdrawalHistory() {
		grdWithdrawalHistoryList.DataSourceID = string.Empty;
		pnlWithdrawalHistoryDetail.Visible = false;
		pnlWithdrawalHistoryList.Visible = false;

		lblWithdrawalHistorySeq.Text = string.Empty;
		lblWithdrawalHistorySiteCd.Text = string.Empty;
		txtWithdrawalHistoryRemarks.Text = string.Empty;
		txtWithdrawalHistoryAddPointCount.Text = string.Empty;

		// 退会履歴の退会処理日の選択肢を設定
		SysPrograms.SetupDay(lstWithdrawalYYYY,lstWithdrawalMM,lstWithdrawalDD,false);
		SysPrograms.SetHourList(lstWithdrawalHH);
		SysPrograms.SetMinuteList(lstWithdrawalMI);
		lstWithdrawalYYYY.SelectedIndex = 0;
		lstWithdrawalMM.SelectedValue = DateTime.Now.ToString("MM");
		lstWithdrawalDD.SelectedValue = DateTime.Now.ToString("dd");
		lstWithdrawalHH.SelectedValue = DateTime.Now.ToString("HH");
		lstWithdrawalMI.SelectedValue = DateTime.Now.ToString("mm");
		lblWithdrawalSS.Text = DateTime.Now.ToString("ss");
	}

	/// <summary>
	/// 引きとめメール送信
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnSendDetainMail_Click(object sender,EventArgs e) {
		string sSiteCd = Session["WithdrawalSiteCd"].ToString();
		string sUserSeq = Session["WithdrawalUserSeq"].ToString();
		string sWithdrwalseq = iBridUtil.GetStringValue(Request.QueryString["withdrawalseq"]);
		string sStatus = string.Empty;

		Withdrawal oWithdrawal = new Withdrawal();
		oWithdrawal.CastSendDetainMail(
			sSiteCd,
			sUserSeq,
			ref sWithdrwalseq,
			out sStatus
		);
		DataBind();
	}

	/// <summary>
	/// 退会処理日のバリデーション
	/// </summary>
	/// <param name="source"></param>
	/// <param name="args"></param>
	protected void vrdWithdrawalHistoryDate_ServerValidate(object source,ServerValidateEventArgs args) {
		DateTime dt;
		string sWithdrawalDate = string.Format(
			"{0}/{1}/{2} {3}:{4}:{5}",
			lstWithdrawalYYYY.SelectedValue,
			lstWithdrawalMM.SelectedValue,
			lstWithdrawalDD.SelectedValue,
			lstWithdrawalHH.SelectedValue,
			lstWithdrawalMI.SelectedValue,
			lblWithdrawalSS.Text
		);

		args.IsValid = true;
		if (!DateTime.TryParse(sWithdrawalDate,out dt)) {
			args.IsValid = false;
		}
	}
}
