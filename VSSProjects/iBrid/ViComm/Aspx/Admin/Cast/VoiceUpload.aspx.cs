﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 着ボイスアップロード
--	Progaram ID		: VoiceUpload
--
--  Creation Date	: 2010.07.27
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain

-------------------------------------------------------------------------*/
using System;
using System.IO;
using System.Text;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

/// <summary>
/// 着ボイスを登録する機能を提供する画面
/// </summary>
public partial class Cast_VoiceUpload : System.Web.UI.Page {


	#region □■□ プロパティ □■□ ==============================================================
	protected string SiteCd {
		get { return this.ViewState["sitecd"] as string; }
		set { this.ViewState["sitecd"] = value; }
	}
	protected string UserSeq {
		get { return this.ViewState["userseq"] as string; }
		set { this.ViewState["userseq"] = value; }
	}
	protected string UserCharNo {
		get { return this.ViewState["usercharno"] as string; }
		set { this.ViewState["usercharno"] = value; }
	}
	protected int VoiceType {
		get { return (int)this.ViewState["voicetype"]; }
		set { this.ViewState["voicetype"] = value; }
	}
	protected string Return{
		get { return this.ViewState["return"] as string; }
		set { this.ViewState["return"] = value; }
	}
	#endregion ====================================================================================


	#region □■□ イベントハンドラ □■□ =========================================================

	protected void Page_Load(object sender, EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			InitPage();
		}
	}
	
	protected void btnUpload_Click(object sender,EventArgs e){
		if(!this.IsValid)return;
		
		this.UploadVoiceAction();
		this.TransferBack();
	}
	
	protected void btnCancel_Click(object sender,EventArgs e){
		this.TransferBack();
	}

	#endregion ====================================================================================


	/// <summary>
	/// 画面を初期化する
	/// </summary>
	private void InitPage() {
	
		this.ParseFormParameters();

		this.lblSiteCd.Text = this.SiteCd;
		this.lblUserSeq.Text = this.UserSeq;
		this.lblUserCharNo.Text = this.UserCharNo;

		using (Cast oCast = new Cast()) {
			string sCastName = string.Empty;
			string sLoginId = string.Empty;

			oCast.GetValue(this.UserSeq, "CAST_NM", ref sCastName);	// ｷｬｽﾄ名
			oCast.GetValue(this.UserSeq, "LOGIN_ID", ref sLoginId);	// ﾛｸﾞｲﾝID

			this.lblCastNm.Text = sCastName;
			this.lblLoginId.Text = sLoginId;
		}

		this.DataBind();
	}

	/// <summary>
	/// 着ボイスファイルをサーバー上にｱｯﾌﾟﾛｰﾄﾞする
	/// </summary>
	private void UploadVoiceAction(){
	
		decimal dVoiceSeq = CastVoice.GetVoiceSeq();
		string sFileNm = iBridUtil.addZero(dVoiceSeq.ToString(),ViCommConst.OBJECT_NM_LENGTH);
		string sLoginId = this.lblLoginId.Text.Trim();

		string sWebPhisicalDir = string.Empty;
		using (Site oSite = new Site()) {
			oSite.GetValue(lblSiteCd.Text, "WEB_PHISICAL_DIR", ref sWebPhisicalDir);
		}
		
		string sCastVoiceDir = ViCommPrograms.GetCastVoiceDir(sWebPhisicalDir,this.SiteCd,sLoginId);
		
		using (Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME,ViCommConst.FILE_UPLOAD_PASSWORD)) {
			this.UploadVoiceFile(this.uldDocomo,this.rfvDocomo,sCastVoiceDir,sFileNm,ViCommConst.FILE_SUFFIX_DOCOMO,ViCommConst.FILE_EXTENSION_DOCOMO);
			this.UploadVoiceFile(this.uldAu, this.rfvAu, sCastVoiceDir, sFileNm, ViCommConst.FILE_SUFFIX_AU, ViCommConst.FILE_EXTENSION_AU);
			this.UploadVoiceFile(this.uldSoftbank, this.rfvSoftbank, sCastVoiceDir, sFileNm, ViCommConst.FILE_SUFFIX_SOFTBANK, ViCommConst.FILE_EXTENSION_SOFTBANK);		
		}
		this.SaveData(dVoiceSeq,sFileNm);	
	}
	private void UploadVoiceFile(FileUpload pFileUpload, BaseValidator pValidator, string pCastVoiceDir, string pFileNm, string pFileSuffix, string pFileExtension) {
		if( !pFileUpload.HasFile ){
			pValidator.IsValid = false;
			pValidator.ErrorMessage = "指定されたファイルが存在しません。";
		} else {
			pFileNm = string.Format("{0}_{1}.{2}",pFileNm,pFileSuffix,pFileExtension);
			pFileUpload.SaveAs(Path.Combine(pCastVoiceDir,pFileNm));
		}
	}
	
	/// <summary>
	/// 画面の入力値をDBにシリアライズする。
	/// </summary>
	private void SaveData(decimal pVoiceSeq,string pVoiceFileNm) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_VOICE_UPLOAD");
			// IN
			db.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.SiteCd);
			db.ProcedureInParm("pUSER_SEQ", DbSession.DbType.VARCHAR2, this.UserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO", DbSession.DbType.VARCHAR2, this.UserCharNo);
			db.ProcedureInParm("pVOICE_SEQ", DbSession.DbType.NUMBER, pVoiceSeq);
			db.ProcedureInParm("pVOICE_FILE_NM", DbSession.DbType.VARCHAR2, pVoiceFileNm);
			db.ProcedureInParm("pOBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,1);
			db.ProcedureInParm("pOBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER,0);
			db.ProcedureInParm("pVOICE_TYPE", DbSession.DbType.NUMBER, ViCommConst.ATTACHED_VOICE);
			// OUT
			db.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);
						
			db.ExecuteProcedure();
		}
	}

	/// <summary>
	/// 画面引数を解析する
	/// </summary>
	private void ParseFormParameters() {
		if (string.IsNullOrEmpty(Request.QueryString["sitecd"])) throw new InvalidOperationException("sitecdが指定されていません。");
		if (string.IsNullOrEmpty(Request.QueryString["userseq"])) throw new InvalidOperationException("userseqが指定されていません。");
		if (string.IsNullOrEmpty(Request.QueryString["usercharno"])) throw new InvalidOperationException("usercharnoが指定されていません。");
		if (string.IsNullOrEmpty(Request.QueryString["voicetype"])) throw new InvalidOperationException("voicetypeが指定されていません。");
		//if (string.IsNullOrEmpty(Request.QueryString["return"])) throw new InvalidOperationException("returnが指定されていません。");
		
		this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		this.UserSeq = iBridUtil.GetStringValue(Request.QueryString["userseq"]);
		this.UserCharNo = iBridUtil.GetStringValue(Request.QueryString["usercharno"]);
		this.VoiceType = int.Parse(Request.QueryString["voicetype"]);
		this.Return = iBridUtil.GetStringValue(Request.QueryString["return"]);
	}
	
	/// <summary>
	/// ｻｲﾄがﾏﾙﾁｷｬｽﾄに対応しているかどうかを示す値を取得する
	/// </summary>
	/// <param name="pSiteCd">ｻｲﾄｺｰﾄﾞ</param>
	/// <returns>ｻｲﾄがﾏﾙﾁｷｬｽﾄに対応している場合はtrue。それ以外はfalse。</returns>
	public bool GetMultiCharFlag(string pSiteCd) {
		using (Site oSite = new Site()) {
			return (oSite.GetMultiCharFlag(pSiteCd));
		}
	}
	
	/// <summary>
	/// 元の画面へ遷移する
	/// </summary>
	private void TransferBack(){

		ViComm.UrlBuilder oUrlBuilder = new ViComm.UrlBuilder("../Cast/CastView.aspx");

		oUrlBuilder.Parameters.Add("loginid", lblLoginId.Text.Trim());
		oUrlBuilder.Parameters.Add("voicesite", this.SiteCd);
		oUrlBuilder.Parameters.Add("usercharno", this.UserCharNo);
		oUrlBuilder.Parameters.Add("return", this.Return);
		if (this.GetMultiCharFlag(this.SiteCd)) {
			oUrlBuilder.Parameters.Add("sitecd", this.SiteCd);
		}

		Server.Transfer(oUrlBuilder.ToString());
	}
}
