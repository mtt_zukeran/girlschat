﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: プロフィール動画認証
--	Progaram ID		: ProfileMovieMainte
--
--  Creation Date	: 2009.06.01
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater		Update Explain
  2010/07/16	Koyanagi	動画ｽﾃｰﾀｽ「非公開」に対応

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Cast_ProfileMovieMainte:System.Web.UI.Page {

	protected string SeekSiteCd {
		get {
			return this.ViewState["SeekSiteCd"] as string;
		}
		set {
			this.ViewState["SeekSiteCd"] = value;
		}
	}

	protected string PrevNonPublicFlag {
		get {
			return this.ViewState["PrevNonPublicFlag"] as string;
		}
		set {
			this.ViewState["PrevNonPublicFlag"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		
		using (ManageCompany oManageCompany = new ManageCompany()) {
			this.txtMovieDoc.Visible = oManageCompany.IsAvailableService(ViCommConst.RELEASE_PROFILE_MOVIE_INPUT_DOC);
		}

		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		lblSiteCd.Text = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		lblUserSeq.Text = iBridUtil.GetStringValue(Request.QueryString["userseq"]);
		lblUserCharNo.Text = iBridUtil.GetStringValue(Request.QueryString["usercharno"]);
		ViewState["SITE_CD"] = lblSiteCd.Text;
		ViewState["USER_SEQ"] = lblUserSeq.Text;
		ViewState["USER_CHAR_NO"] = lblUserCharNo.Text;
		ViewState["MOVIE_SEQ"] = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);
		ViewState["RETURN"] = iBridUtil.GetStringValue(Request.QueryString["return"]);
		ViewState["OBJ_NOT_PUBLISH_FLAG"] = iBridUtil.GetStringValue(Request.QueryString["closed"]);
		this.SeekSiteCd = iBridUtil.GetStringValue(Request.QueryString["seeksitecd"]);
		string sMovieType = iBridUtil.GetStringValue(Request.QueryString["movietype"]);
		if(string.IsNullOrEmpty(sMovieType))sMovieType = ViCommConst.ATTACHED_PROFILE.ToString();
		ViewState["MOVIE_TYPE"]=sMovieType;
		

		using (Cast oCast = new Cast()) {
			string sName = "",sId = "";
			oCast.GetValue(lblUserSeq.Text,"CAST_NM",ref sName);
			oCast.GetValue(lblUserSeq.Text,"LOGIN_ID",ref sId);
			lblUserNm.Text = sName;
			lblLoginId.Text = sId;
		}
		ClearField();
		DataBind();
		GetData();

		if (ViCommConst.FLAG_ON_STR.Equals(iBridUtil.GetStringValue(ConfigurationManager.AppSettings["enabledNoTitleObj"]))) {
			this.vdrMovieTitle.Enabled = false;
		}
	}

	private void ClearField() {
		txtChargePoint.Text = "";
		txtMovieTitle.Text = "";
		lstAuthType.SelectedIndex = 0;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		ReturnToCall();
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {

			db.PrepareProcedure("CAST_MOVIE_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,ViewState["SITE_CD"]);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,ViewState["USER_SEQ"].ToString());
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViewState["USER_CHAR_NO"].ToString());
			db.ProcedureInParm("PMOVIE_SEQ",DbSession.DbType.VARCHAR2,ViewState["MOVIE_SEQ"].ToString());
			db.ProcedureOutParm("PMOVIE_TITLE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMOVIE_DOC",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCHARGE_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PUPLOAD_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMOVIE_TYPE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_MOVIE_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_MOVIE_ATTR_SEQ", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPLAY_TIME", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMOVIE_SERIES", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSAMPLE_MOVIE_SEQ", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PTHUMBNAIL_PIC_SEQ", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtMovieTitle.Text = db.GetStringValue("PMOVIE_TITLE");
				txtChargePoint.Text = db.GetStringValue("PCHARGE_POINT");
				txtMovieDoc.Text = db.GetStringValue("PMOVIE_DOC");
				string sUnAtuthFlag = db.GetStringValue("POBJ_NOT_APPROVE_FLAG");
				string sNotPublishFlag = db.GetStringValue("POBJ_NOT_PUBLISH_FLAG");
				if (sUnAtuthFlag.Equals("0")) {
					if (sNotPublishFlag.Equals("0")) {
						lstAuthType.SelectedValue = ViCommConst.MOVIE_APPLY_OK;
					} else if (sNotPublishFlag.Equals("1")) {
						lstAuthType.SelectedValue = ViCommConst.MOVIE_APPLY_NG;
					}
				} else if (sUnAtuthFlag.Equals("1")) {
					lstAuthType.SelectedValue = ViCommConst.MOVIE_APPLY_WAIT;
				}
			} else {
				ClearField();
			}
		}
		pnlDtl.Visible = true;
		this.PrevNonPublicFlag = lstAuthType.SelectedValue;
	}

	private void UpdateData(int pDelFlag) {
		int iNotApproveFlag = 0;
		int iDelFlag = 0;
		int iNotPublishFlag = 0;

		if (pDelFlag == 0) {
			switch (lstAuthType.SelectedValue) {
				case ViCommConst.MOVIE_APPLY_WAIT:	// 認証待ち
					iDelFlag = 0;
					iNotApproveFlag = 1;
					iNotPublishFlag = 0;
					break;

				case ViCommConst.MOVIE_APPLY_OK:	// 公開					iDelFlag = 0;
					iNotApproveFlag = 0;
					iNotPublishFlag = 0;
					break;

				case ViCommConst.MOVIE_APPLY_REMOVE: // 削除
					iNotApproveFlag = 0;
					iDelFlag = 1;
					iNotPublishFlag = 0;
					break;

				case ViCommConst.MOVIE_APPLY_NG:	// 非公開
					iNotApproveFlag = 0;
					iDelFlag = 0;
					iNotPublishFlag = 1;
					break;
			}
		} else {
			iDelFlag = 1;
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_MOVIE_MAINTE");
			db.ProcedureInParm("PMOVIE_SEQ",DbSession.DbType.VARCHAR2,ViewState["MOVIE_SEQ"].ToString());
			db.ProcedureInParm("PMOVIE_TITLE",DbSession.DbType.VARCHAR2,txtMovieTitle.Text);
			db.ProcedureInParm("PMOVIE_DOC",DbSession.DbType.VARCHAR2,txtMovieDoc.Text);
			db.ProcedureInParm("PCHARGE_POINT",DbSession.DbType.NUMBER,decimal.Parse(txtChargePoint.Text));
			db.ProcedureInParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,iNotApproveFlag);
			db.ProcedureInParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER,iNotPublishFlag);
			db.ProcedureInParm("PMOVIE_TYPE", DbSession.DbType.NUMBER, int.Parse((string)ViewState["MOVIE_TYPE"]));
			db.ProcedureInParm("PCAST_MOVIE_ATTR_TYPE_SEQ",DbSession.DbType.NUMBER,ViCommConst.DEFAULT_CAST_MOVIE_ATTR_TYPE_SEQ.ToString());
			db.ProcedureInParm("PCAST_MOVIE_ATTR_SEQ",DbSession.DbType.NUMBER,ViCommConst.DEFAULT_CAST_MOVIE_ATTR_SEQ.ToString());
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,iDelFlag);
			db.ProcedureInParm("PPLAY_TIME", DbSession.DbType.VARCHAR2,null);
			db.ProcedureInParm("PMOVIE_SERIES_SEQ", DbSession.DbType.NUMBER,null);
			db.ProcedureInParm("PTHUMBNAIL_PIC_SEQ", DbSession.DbType.NUMBER, null);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		if (iDelFlag == 1) {
			string sWebPhisicalDir = "";
			using (Site oSite = new Site()) {
				oSite.GetValue(ViewState["SITE_CD"].ToString(),"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
			}

			using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
				string sMovie = sWebPhisicalDir +
									ViCommConst.MOVIE_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\Operator\\" + lblLoginId.Text + "\\" +
									ViCommConst.MOVIE_HEADER + iBridUtil.addZero(ViewState["MOVIE_SEQ"].ToString(),ViCommConst.OBJECT_NM_LENGTH) +
									ViCommConst.MOVIE_FOODER;

				if (System.IO.File.Exists(sMovie)) {
					System.IO.File.Delete(sMovie);
				}

				sMovie = sWebPhisicalDir +
									ViCommConst.MOVIE_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\Operator\\" + lblLoginId.Text + "\\" +
									ViCommConst.MOVIE_HEADER + iBridUtil.addZero(ViewState["MOVIE_SEQ"].ToString(),ViCommConst.OBJECT_NM_LENGTH) + "s" +
									ViCommConst.MOVIE_FOODER;

				if (System.IO.File.Exists(sMovie)) {
					System.IO.File.Delete(sMovie);
				}

				sMovie = sWebPhisicalDir +
									ViCommConst.MOVIE_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\Operator\\" + lblLoginId.Text + "\\" +
									ViCommConst.MOVIE_HEADER + iBridUtil.addZero(ViewState["MOVIE_SEQ"].ToString(),ViCommConst.OBJECT_NM_LENGTH) +
									ViCommConst.MOVIE_FOODER2;

				if (System.IO.File.Exists(sMovie)) {
					System.IO.File.Delete(sMovie);
				}

				sMovie = sWebPhisicalDir +
									ViCommConst.MOVIE_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\Operator\\" + lblLoginId.Text + "\\" +
									ViCommConst.MOVIE_HEADER + iBridUtil.addZero(ViewState["MOVIE_SEQ"].ToString(),ViCommConst.OBJECT_NM_LENGTH) +
									ViCommConst.MOVIE_FOODER3;

				if (System.IO.File.Exists(sMovie)) {
					System.IO.File.Delete(sMovie);
				}
			}
		}

		bool bTxNotPublicNotice;
		using (ManageCompany oManageCompany = new ManageCompany()) {
			bTxNotPublicNotice = oManageCompany.IsAvailableService(ViCommConst.RELEASE_CAST_OBJ_NON_PUBLIC_INFO,2);
		}
		
		if (bTxNotPublicNotice && !this.PrevNonPublicFlag.Equals(ViCommConst.MOVIE_APPLY_NG) && this.lstAuthType.SelectedValue.Equals(ViCommConst.MOVIE_APPLY_NG)) {
			Server.Transfer(string.Format("../CastAdmin/TxCastObjNonPublicMail.aspx?site={0}&seeksitecd={1}&userseq={2}&usercharNo={3}&mailtype={4}&return={5}",ViewState["SITE_CD"].ToString(),SeekSiteCd,ViewState["USER_SEQ"].ToString(),ViewState["USER_CHAR_NO"].ToString(),ViCommConst.MAIL_TP_CAST_PROF_MOVIE_NP,ViewState["RETURN"].ToString()));
		} else {
			ReturnToCall();
		}
	}

	private void ReturnToCall() {
		if (ViewState["RETURN"].ToString().Equals("ProfileMovieCheckList")) {
			Server.Transfer(string.Format("../CastAdmin/ProfileMovieCheckList.aspx?moviesite={0}&closed={1}&seeksitecd={2}",ViewState["SITE_CD"].ToString(),ViewState["OBJ_NOT_PUBLISH_FLAG"].ToString(),this.SeekSiteCd));

		} else if (ViewState["RETURN"].ToString().Equals("ProfileMovieOpenList")) {
			Server.Transfer(string.Format("../CastAdmin/ProfileMovieOpenList.aspx?moviesite={0}",ViewState["SITE_CD"].ToString()));

		//} else if (ViewState["RETURN"].ToString().Equals("ProfileMovieClosedList")) {
		//    Server.Transfer(string.Format("../CastAdmin/ProfileMovieClosedList.aspx?moviesite={0}",ViewState["SITE_CD"].ToString()));

		} else {
			Server.Transfer(string.Format("../Cast/CastView.aspx?loginid={0}&moviesite={1}",lblLoginId.Text,ViewState["SITE_CD"].ToString()));
		}
	}
}
