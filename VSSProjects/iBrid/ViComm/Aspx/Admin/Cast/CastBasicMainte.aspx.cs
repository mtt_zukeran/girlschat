﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者基本情報設定

--	Progaram ID		: CastBasicMainte
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public partial class Cast_CastBasicMainte:System.Web.UI.Page {
	private bool bCheckTmgt;

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			InitPage();
		}
		using (ManageCompany oInstance = new ManageCompany()) {
			bCheckTmgt = oInstance.IsAvailableService(ViCommConst.RELEASE_CHECK_TMGT_DUPLI);
		}
	}

	private void InitPage() {

		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);
		this.vdrCastNm.ErrorMessage = DisplayWordUtil.Replace(this.vdrCastNm.ErrorMessage);
		this.vdrTel.ErrorMessage = DisplayWordUtil.Replace(this.vdrTel.ErrorMessage);

		if (Session["AdminType"].ToString().CompareTo(ViCommConst.RIGHT_LOCAL_STAFF) <= 0) {
			txtEmailAddr.Visible = false;

			lstManagerSeq.Enabled = false;
			txtCastNm.Enabled = false;
			txtCastKanaNm.Enabled = false;
			lstUseTerminalType.Enabled = false;
			txtLoginId.Enabled = false;
			txtLoginPassword.Enabled = false;
			lstUserStatus.Enabled = false;
			chkMonitorEnableFlag.Enabled = false;
			chkOnlineLiveEnableFlag.Enabled = false;
			chkUseFreeDialFlag.Enabled = false;
			txtCrosmileUri.Enabled = false;
			rdoNonExistMailAddrFlag.Enabled = false;
			rdoTxMuller3NgMailAddrFlag.Enabled = false;
			chkWShotNaFlag.Enabled = false;
			chkMailNaFlag.Enabled = false;
			txtIModeId.Enabled = false;
			txtTerminalUniqueId.Enabled = false;
			txtBankNm.Enabled = false;
			txtBankCd.Enabled = false;
			txtBankOfficeNm.Enabled = false;
			txtBankOfficeKanaNm.Enabled = false;
			txtBankOfficeCd.Enabled = false;
			txtBankAccountType.Enabled = false;
			txtBankAccountNo.Enabled = false;
			rdoNormalAccount.Enabled = false;
			rdoCheckingAccount.Enabled = false;
			rdoSavingsAccounts.Enabled = false;
			txtBankAccountHolderNm.Enabled = false;
			lstUserRank.Enabled = false;
			txtStaffId.Enabled = false;
			btnDelete.Visible = false;
		} else {
			txtEmailAddr.Visible = true;
			lstManagerSeq.Enabled = true;
			txtCastNm.Enabled = true;
			txtCastKanaNm.Enabled = true;
			lstUseTerminalType.Enabled = true;
			txtLoginId.Enabled = true;
			txtLoginPassword.Enabled = true;
			lstUserStatus.Enabled = true;
			chkMonitorEnableFlag.Enabled = true;
			chkOnlineLiveEnableFlag.Enabled = true;
			chkUseFreeDialFlag.Enabled = true;
			txtCrosmileUri.Enabled = true;
			rdoNonExistMailAddrFlag.Enabled = true;
			rdoTxMuller3NgMailAddrFlag.Enabled = true;
			chkWShotNaFlag.Enabled = true;
			chkMailNaFlag.Enabled = true;
			txtIModeId.Enabled = true;
			txtTerminalUniqueId.Enabled = true;
			txtBankNm.Enabled = true;
			txtBankCd.Enabled = true;
			txtBankOfficeNm.Enabled = true;
			txtBankOfficeKanaNm.Enabled = true;
			txtBankOfficeCd.Enabled = true;
			txtBankAccountType.Enabled = true;
			txtBankAccountNo.Enabled = true;
			rdoNormalAccount.Enabled = true;
			rdoCheckingAccount.Enabled = true;
			rdoSavingsAccounts.Enabled = true;
			txtBankAccountHolderNm.Enabled = true;
			lstUserRank.Enabled = true;
			txtStaffId.Enabled = true;
			btnDelete.Visible = true;
			if (!Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_SITE_OWNER)) {
				pnlPic.Visible = false;
			} else {
				pnlPic.Visible = true;
			}
		}
		if (Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_PRODUCTION)) {
			txtBirthDay.Enabled = false;
			txtEmailAddr.Visible = true;
			rdoNonExistMailAddrFlag.Enabled = true;
			rdoTxMuller3NgMailAddrFlag.Enabled = true;
			chkWaitPaymentFlag.Enabled = false;
			txtStaffId.Enabled = false;
			txtZipCode.Enabled = false;
			lstPrefectureCd.Enabled = false;
			txtAddress1.Enabled = false;
			txtAddress2.Enabled = false;
			chkBankAccountInvalidFlag.Enabled = false;
			chkCautionFlag.Enabled = false;
			chkAdminFlag.Enabled = false;
			txtRemarks1.Enabled = false;
			txtRemarks2.Enabled = false;
			txtRemarks3.Enabled = false;
			txtRemarks4.Enabled = false;
		}

		// Available Serive?
		using (ManageCompany oCompany = new ManageCompany()) {
			plcStaffIdSupport.Visible = oCompany.IsAvailableService(ViCommConst.RELEASE_SUPPORT_STAFF_ID);
			plcInvalidBankAccount.Visible = oCompany.IsAvailableService(ViCommConst.RELEASE_VALIDATE_ACCOUNT_INFO);
			plcBankAccountType.Visible = !oCompany.IsAvailableService(ViCommConst.RELEASE_BANK_TYPE_RADIO);
			plcBankAccountType2.Visible = oCompany.IsAvailableService(ViCommConst.RELEASE_BANK_TYPE_RADIO);
			plcBankCd.Visible = oCompany.IsAvailableService(ViCommConst.RELEASE_CAST_INPUT_BANK_CD,2);
			plcBankOfficeCd.Visible = oCompany.IsAvailableService(ViCommConst.RELEASE_CAST_INPUT_BANK_CD,2);

			oCompany.GetOne();
			plcPaymentTimingType.Visible = oCompany.execMonthlyAutoPaymentFlag.Equals(ViCommConst.FLAG_ON);
		}

		plcBank.Visible = true;


		ViewState["RETURN"] = iBridUtil.GetStringValue(Request.QueryString["return"]);
		lblUserSeq.Text = iBridUtil.GetStringValue(Request.QueryString["userseq"]);
		ViewState["LOGIN_ID"] = iBridUtil.GetStringValue(Request.QueryString["loginid"]);
		ClearField();
		DataBind();
		lstPrefectureCd.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		GetData();
	}

	private void ClearField() {
		lblRegist.Visible = false;
		txtCastNm.Text = string.Empty;
		txtCastKanaNm.Text = string.Empty;
		txtBirthDay.Text = string.Empty;
		txtLoginId.Text = string.Empty;
		txtLoginPassword.Text = string.Empty;
		txtEmailAddr.Text = string.Empty;
		txtTel.Text = string.Empty;
		txtIModeId.Text = string.Empty;
		txtTerminalUniqueId.Text = string.Empty;
		lblUri.Text = string.Empty;
		lblTerminalPassword.Text = string.Empty;
		lstManagerSeq.SelectedIndex = 0;
		lstUseTerminalType.SelectedIndex = 0;
		lstUserStatus.SelectedIndex = 0;
		lstPrefectureCd.SelectedIndex = 0;
		lblIntroducerFriendCd.Text = string.Empty;
		chkWaitPaymentFlag.Checked = false;
		chkMonitorEnableFlag.Checked = false;
		rdoNonExistMailAddrFlag.SelectedValue = "0";
		rdoTxMuller3NgMailAddrFlag.SelectedValue = "0";
		chkWShotNaFlag.Checked = false;
		chkMailNaFlag.Checked = false;
		chkAdminFlag.Checked = false;
		txtZipCode.Text = string.Empty;
		txtAddress1.Text = string.Empty;
		txtAddress2.Text = string.Empty;
		txtBankNm.Text = string.Empty;
		txtBankCd.Text = string.Empty;
		txtBankOfficeNm.Text = string.Empty;
		txtBankOfficeKanaNm.Text = string.Empty;
		txtBankOfficeCd.Text = string.Empty;
		txtBankAccountType.Text = string.Empty;
		txtBankAccountNo.Text = string.Empty;
		txtBankAccountHolderNm.Text = string.Empty;
		lstUserRank.SelectedIndex = 0;
		txtStaffId.Text = string.Empty;
		rptUpload.DataSource = null;
		rdoAutoMonthAndRequest.Checked = true;
		rdoAutoMonthOnly.Checked = false;
		rdoRequestOnly.Checked = false;
		txtRemarks1.Text = string.Empty;
		txtRemarks2.Text = string.Empty;
		txtRemarks3.Text = string.Empty;
		txtRemarks4.Text = string.Empty;
		txtCrosmileUri.Text = string.Empty;
		txtGuardianNm.Text = string.Empty;
		txtGuardianTel.Text = string.Empty;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		Server.Transfer(string.Format("CastView.aspx?loginid={0}&return={1}",iBridUtil.GetStringValue(ViewState["LOGIN_ID"]),ViewState["RETURN"].ToString()));
	}

	protected void btnUpload_Command(object sender,CommandEventArgs e) {
		if (!e.CommandName.Equals("UPLOAD")) {
			return;
		}

		int iItemIndex;
		if (int.TryParse(e.CommandArgument.ToString(),out iItemIndex)) {
			FileUpload oFileUpload = this.rptUpload.Items[iItemIndex].FindControl("uldIdPic") as FileUpload;
			Image oImage = this.rptUpload.Items[iItemIndex].FindControl("imgIdPic") as Image;
			if (oFileUpload != null && oImage != null) {
				this.UploadPicture(oFileUpload,oImage,iItemIndex + 1);
				this.GetPictureData();
			}
		}
	}

	protected void btnDelete_Command(object sender,CommandEventArgs e) {
		if (!e.CommandName.Equals("DELETE")) {
			return;
		}

		int iItemIndex;
		if (int.TryParse(e.CommandArgument.ToString(),out iItemIndex)) {
			this.DeletePicture(iItemIndex + 1);
			this.GetPictureData();
		}
	}

	protected void vdcLoginId_ServerValidate(object source,ServerValidateEventArgs args) {
		using (Cast objCast = new Cast()) {
			args.IsValid = objCast.IsUniqueID(txtLoginId.Text,lblUserSeq.Text);
			if (args.IsValid) {
				ViewState["LOGIN_ID"] = txtLoginId.Text;
			}
		}
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_BASIC_GET");
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.NUMBER,lblUserSeq.Text);
			db.ProcedureOutParm("PLOGIN_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPRODUCTION_SEQ",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMANAGER_SEQ",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_KANA_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PBIRTHDAY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PTEL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PEMAIL_ADDR",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PURI",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PTERMINAL_PASSWORD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PUSE_TERMINAL_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PTERMINAL_UNIQUE_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PIMODE_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PNON_EXIST_MAIL_ADDR_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PLOGIN_PASSWORD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREGIST_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PUSER_STATUS",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PINTRODUCER_FRIEND_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PADMIN_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMONITOR_ENABLE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PONLINE_LIVE_ENABLE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PWSHOT_NA_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMAIL_NA_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PZIP_CODE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPREFECTURE_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PADDRESS1",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PADDRESS2",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PBANK_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PBANK_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PBANK_OFFICE_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PBANK_OFFICE_KANA_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PBANK_OFFICE_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PBANK_ACCOUNT_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PBANK_ACCOUNT_TYPE2",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PBANK_ACCOUNT_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PBANK_ACCOUNT_HOLDER_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PBANK_ACCOUNT_INVALID_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PUSER_RANK",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTAFF_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PWAIT_PAYMENT_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAUTION_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID_USER",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO_USER",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID_CAST",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO_CAST",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PUSE_FREE_DIAL_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PUSE_CROSMILE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pCROSMILE_URI",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPAYMENT_TIMING_TYPE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PGUARDIAN_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PGUARDIAN_TEL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pUSE_VOICEAPP_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pREMARKS1",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pREMARKS2",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pREMARKS3",DbSession.DbType.VARCHAR2);
			db.ProcedureOutArrayParm("PREMARKS4",DbSession.DbType.VARCHAR2,7);
			db.ProcedureOutParm("pTX_MULLER3_NG_MAIL_ADDR_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO_USER"] = db.GetStringValue("PREVISION_NO_USER");
			ViewState["ROWID_USER"] = db.GetStringValue("PROWID_USER");

			ViewState["REVISION_NO_CAST"] = db.GetStringValue("PREVISION_NO_CAST");
			ViewState["ROWID_CAST"] = db.GetStringValue("PROWID_CAST");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtCastNm.Text = db.GetStringValue("PCAST_NM");
				txtCastKanaNm.Text = db.GetStringValue("PCAST_KANA_NM");
				txtBirthDay.Text = db.GetStringValue("PBIRTHDAY");
				txtTel.Text = db.GetStringValue("PTEL");
				lblUri.Text = db.GetStringValue("PURI");
				lblTerminalPassword.Text = db.GetStringValue("PTERMINAL_PASSWORD");
				txtEmailAddr.Text = db.GetStringValue("PEMAIL_ADDR");
				txtLoginId.Text = db.GetStringValue("PLOGIN_ID");
				if (!txtLoginId.Text.Equals(string.Empty)) {
					txtLoginId.Enabled = false;
				}
				lblLoginId.Text = db.GetStringValue("PLOGIN_ID");
				txtLoginPassword.Text = db.GetStringValue("PLOGIN_PASSWORD");
				lstUserStatus.SelectedValue = db.GetStringValue("PUSER_STATUS");
				lstManagerSeq.SelectedValue = db.GetStringValue("PMANAGER_SEQ");
				lblIntroducerFriendCd.Text = db.GetStringValue("PINTRODUCER_FRIEND_CD");
				lstUseTerminalType.SelectedValue = db.GetStringValue("PUSE_TERMINAL_TYPE");
				txtIModeId.Text = db.GetStringValue("PIMODE_ID");
				txtTerminalUniqueId.Text = db.GetStringValue("PTERMINAL_UNIQUE_ID");
				chkWaitPaymentFlag.Checked = db.GetStringValue("PWAIT_PAYMENT_FLAG").ToString().Equals("1");
				chkMonitorEnableFlag.Checked = db.GetStringValue("PMONITOR_ENABLE_FLAG").ToString().Equals("1");
				chkOnlineLiveEnableFlag.Checked = db.GetStringValue("PONLINE_LIVE_ENABLE_FLAG").ToString().Equals("1");
				chkUseFreeDialFlag.Checked = db.GetStringValue("PUSE_FREE_DIAL_FLAG").ToString().Equals("1");
				chkUseCrosmileFlag.Checked = db.GetStringValue("PUSE_CROSMILE_FLAG").ToString().Equals("1");
				txtCrosmileUri.Text = db.GetStringValue("pCROSMILE_URI");
				rdoNonExistMailAddrFlag.SelectedValue = db.GetStringValue("PNON_EXIST_MAIL_ADDR_FLAG");
				rdoTxMuller3NgMailAddrFlag.SelectedValue = db.GetStringValue("PTX_MULLER3_NG_MAIL_ADDR_FLAG");
				chkWShotNaFlag.Checked = db.GetStringValue("PWSHOT_NA_FLAG").ToString().Equals("1");
				chkMailNaFlag.Checked = db.GetStringValue("PMAIL_NA_FLAG").ToString().Equals("1");
				chkCautionFlag.Checked = db.GetStringValue("PCAUTION_FLAG").ToString().Equals(ViCommConst.FLAG_ON_STR);
				chkAdminFlag.Checked = db.GetStringValue("PADMIN_FLAG").ToString().Equals(ViCommConst.FLAG_ON_STR);
				chkUseVoiceappFlag.Checked = (db.GetIntValue("pUSE_VOICEAPP_FLAG") != 0);
				lstPrefectureCd.SelectedValue = db.GetStringValue("PPREFECTURE_CD");
				txtZipCode.Text = db.GetStringValue("PZIP_CODE");
				txtAddress1.Text = db.GetStringValue("PADDRESS1");
				txtAddress2.Text = db.GetStringValue("PADDRESS2");
				txtBankNm.Text = db.GetStringValue("PBANK_NM");
				txtBankCd.Text = db.GetStringValue("PBANK_CD");
				txtBankOfficeNm.Text = db.GetStringValue("PBANK_OFFICE_NM");
				txtBankOfficeKanaNm.Text = db.GetStringValue("PBANK_OFFICE_KANA_NM");
				txtBankOfficeCd.Text = db.GetStringValue("PBANK_OFFICE_CD");
				txtBankAccountType.Text = db.GetStringValue("PBANK_ACCOUNT_TYPE");
				if (db.GetStringValue("PBANK_ACCOUNT_TYPE2").Equals(ViCommConst.BankAccountType.NORMAL_ACCOUNT)) {
					rdoNormalAccount.Checked = true;
					rdoCheckingAccount.Checked = false;
					rdoSavingsAccounts.Checked = false;
				} else if (db.GetStringValue("PBANK_ACCOUNT_TYPE2").Equals(ViCommConst.BankAccountType.CHECKING_ACCOUNT)) {
					rdoNormalAccount.Checked = false;
					rdoCheckingAccount.Checked = true;
					rdoSavingsAccounts.Checked = false;
				} else if (db.GetStringValue("PBANK_ACCOUNT_TYPE2").Equals(ViCommConst.BankAccountType.SAVINGS_ACCOUNTS)) {
					rdoNormalAccount.Checked = false;
					rdoCheckingAccount.Checked = false;
					rdoSavingsAccounts.Checked = true;
				} else {
					rdoNormalAccount.Checked = false;
					rdoCheckingAccount.Checked = false;
					rdoSavingsAccounts.Checked = false;
				}
				if (db.GetIntValue("PPAYMENT_TIMING_TYPE").Equals(ViCommConst.PaymentTimingType.AutoMonthAndRequest)) {
					rdoAutoMonthAndRequest.Checked = true;
					rdoAutoMonthOnly.Checked = false;
					rdoRequestOnly.Checked = false;
				} else if (db.GetIntValue("PPAYMENT_TIMING_TYPE").Equals(ViCommConst.PaymentTimingType.AutoMonthOnly)) {
					rdoAutoMonthAndRequest.Checked = false;
					rdoAutoMonthOnly.Checked = true;
					rdoRequestOnly.Checked = false;
				} else {
					rdoAutoMonthAndRequest.Checked = false;
					rdoAutoMonthOnly.Checked = false;
					rdoRequestOnly.Checked = true;
				}
				txtBankAccountNo.Text = db.GetStringValue("PBANK_ACCOUNT_NO");
				txtBankAccountHolderNm.Text = db.GetStringValue("PBANK_ACCOUNT_HOLDER_NM");
				chkBankAccountInvalidFlag.Checked = db.GetStringValue("PBANK_ACCOUNT_INVALID_FLAG").ToString().Equals("1");
				lstUserRank.SelectedValue = db.GetStringValue("PUSER_RANK");
				txtStaffId.Text = db.GetStringValue("PSTAFF_ID");
				txtGuardianNm.Text = db.GetStringValue("PGUARDIAN_NM");
				txtGuardianTel.Text = db.GetStringValue("PGUARDIAN_TEL");
				lblRegist.Visible = false;
				this.txtRemarks1.Text = db.GetStringValue("pREMARKS1");
				this.txtRemarks2.Text = db.GetStringValue("pREMARKS2");
				this.txtRemarks3.Text = db.GetStringValue("pREMARKS3");
				for (int i = 0;i < 7;i++) {
					txtRemarks4.Text = txtRemarks4.Text + db.GetArryStringValue("PREMARKS4",i);
				}

				this.GetPictureData();

			} else {
				ClearField();
				lblRegist.Visible = true;
				txtLoginPassword.Text = db.GetStringValue("PLOGIN_PASSWORD");
			}
		}
		pnlDtl.Visible = true;
	}

	private void GetPictureData() {
		using (Cast oCast = new Cast()) {
			DataSet oDataSet = oCast.GetOneIdPicSeq(lblUserSeq.Text);
			if (oDataSet.Tables[0].Rows.Count > 0) {
				this.rptUpload.DataSource = oDataSet.Tables[0].Rows[0].ItemArray;
				this.rptUpload.DataBind();
			}
		}
	}

	private void UpdateData(int pDelFlag) {
		string sBankAccountType = string.Empty;
		int iPaymentTimingType = 0;

		if (rdoNormalAccount.Checked) {
			sBankAccountType = ViCommConst.BankAccountType.NORMAL_ACCOUNT;
		} else if (rdoCheckingAccount.Checked) {
			sBankAccountType = ViCommConst.BankAccountType.CHECKING_ACCOUNT;
		} else if (rdoSavingsAccounts.Checked) {
			sBankAccountType = ViCommConst.BankAccountType.SAVINGS_ACCOUNTS;
		}

		if (rdoAutoMonthAndRequest.Checked) {
			iPaymentTimingType = ViCommConst.PaymentTimingType.AutoMonthAndRequest;
		} else if (rdoAutoMonthOnly.Checked) {
			iPaymentTimingType = ViCommConst.PaymentTimingType.AutoMonthOnly;
		} else if (rdoRequestOnly.Checked) {
			iPaymentTimingType = ViCommConst.PaymentTimingType.RequestOnly;
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_BASIC_MAINTE");
			db.ProcedureBothParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,lblUserSeq.Text);
			db.ProcedureInParm("PMANAGER_SEQ",DbSession.DbType.VARCHAR2,lstManagerSeq.SelectedValue);
			db.ProcedureInParm("PCAST_NM",DbSession.DbType.VARCHAR2,txtCastNm.Text);
			db.ProcedureInParm("PCAST_KANA_NM",DbSession.DbType.VARCHAR2,txtCastKanaNm.Text);
			db.ProcedureInParm("PBIRTHDAY",DbSession.DbType.VARCHAR2,txtBirthDay.Text);
			db.ProcedureInParm("PTEL",DbSession.DbType.VARCHAR2,txtTel.Text);
			db.ProcedureInParm("PEMAIL_ADDR",DbSession.DbType.VARCHAR2,txtEmailAddr.Text.ToLower());
			db.ProcedureInParm("PURI",DbSession.DbType.VARCHAR2,lblUri.Text);
			db.ProcedureInParm("PTERMINAL_PASSWORD",DbSession.DbType.VARCHAR2,lblTerminalPassword.Text);
			db.ProcedureInParm("PUSE_TERMINAL_TYPE",DbSession.DbType.VARCHAR2,lstUseTerminalType.SelectedValue);
			db.ProcedureInParm("PTERMINAL_UNIQUE_ID",DbSession.DbType.VARCHAR2,txtTerminalUniqueId.Text);
			db.ProcedureInParm("PIMODE_ID",DbSession.DbType.VARCHAR2,txtIModeId.Text);
			db.ProcedureInParm("PLOGIN_ID",DbSession.DbType.VARCHAR2,txtLoginId.Text);
			db.ProcedureInParm("PLOGIN_PASSWORD",DbSession.DbType.VARCHAR2,txtLoginPassword.Text);
			db.ProcedureInParm("PUSER_STATUS",DbSession.DbType.VARCHAR2,lstUserStatus.SelectedValue);
			db.ProcedureInParm("PINTRODUCER_FRIEND_CD",DbSession.DbType.VARCHAR2,lblIntroducerFriendCd.Text);
			db.ProcedureInParm("PADMIN_FLAG",DbSession.DbType.NUMBER,chkAdminFlag.Checked);
			db.ProcedureInParm("PMONITOR_ENABLE_FLAG",DbSession.DbType.NUMBER,chkMonitorEnableFlag.Checked);
			db.ProcedureInParm("PNON_EXIST_MAIL_ADDR_FLAG",DbSession.DbType.NUMBER,int.Parse(rdoNonExistMailAddrFlag.SelectedValue));
			db.ProcedureInParm("PONLINE_LIVE_ENABLE_FLAG",DbSession.DbType.NUMBER,chkOnlineLiveEnableFlag.Checked);
			db.ProcedureInParm("PWSHOT_NA_FLAG",DbSession.DbType.NUMBER,chkWShotNaFlag.Checked);
			db.ProcedureInParm("PMAIL_NA_FLAG",DbSession.DbType.NUMBER,chkMailNaFlag.Checked);
			db.ProcedureInParm("PZIP_CODE",DbSession.DbType.VARCHAR2,txtZipCode.Text);
			db.ProcedureInParm("PPREFECTURE_CD",DbSession.DbType.VARCHAR2,lstPrefectureCd.SelectedValue);
			db.ProcedureInParm("PADDRESS1",DbSession.DbType.VARCHAR2,txtAddress1.Text);
			db.ProcedureInParm("PADDRESS2",DbSession.DbType.VARCHAR2,txtAddress2.Text);
			db.ProcedureInParm("PBANK_NM",DbSession.DbType.VARCHAR2,txtBankNm.Text);
			db.ProcedureInParm("PBANK_CD",DbSession.DbType.VARCHAR2,txtBankCd.Text);
			db.ProcedureInParm("PBANK_OFFICE_NM",DbSession.DbType.VARCHAR2,txtBankOfficeNm.Text);
			db.ProcedureInParm("PBANK_OFFICE_KANA_NM",DbSession.DbType.VARCHAR2,txtBankOfficeKanaNm.Text);
			db.ProcedureInParm("PBANK_OFFICE_CD",DbSession.DbType.VARCHAR2,txtBankOfficeCd.Text);
			db.ProcedureInParm("PBANK_ACCOUNT_TYPE",DbSession.DbType.VARCHAR2,txtBankAccountType.Text);
			db.ProcedureInParm("PBANK_ACCOUNT_TYPE2",DbSession.DbType.VARCHAR2,sBankAccountType);
			db.ProcedureInParm("PBANK_ACCOUNT_NO",DbSession.DbType.VARCHAR2,txtBankAccountNo.Text);
			db.ProcedureInParm("PBANK_ACCOUNT_HOLDER_NM",DbSession.DbType.VARCHAR2,txtBankAccountHolderNm.Text);
			db.ProcedureInParm("PBANK_ACCOUNT_INVALID_FLAG",DbSession.DbType.VARCHAR2,chkBankAccountInvalidFlag.Checked);
			db.ProcedureInParm("PUSER_RANK",DbSession.DbType.VARCHAR2,lstUserRank.SelectedValue);
			db.ProcedureInParm("PSTAFF_ID",DbSession.DbType.VARCHAR2,txtStaffId.Text);
			db.ProcedureInParm("PWAIT_PAYMENT_FLAG",DbSession.DbType.NUMBER,chkWaitPaymentFlag.Checked);
			db.ProcedureInParm("PCAUTION_FLAG",DbSession.DbType.NUMBER,chkCautionFlag.Checked);
			db.ProcedureInParm("PROWID_USER",DbSession.DbType.VARCHAR2,ViewState["ROWID_USER"].ToString());
			db.ProcedureInParm("PREVISION_NO_USER",DbSession.DbType.VARCHAR2,ViewState["REVISION_NO_USER"].ToString());
			db.ProcedureInParm("PROWID_CAST",DbSession.DbType.VARCHAR2,ViewState["ROWID_CAST"].ToString());
			db.ProcedureInParm("PREVISION_NO_CAST",DbSession.DbType.VARCHAR2,ViewState["REVISION_NO_CAST"].ToString());
			db.ProcedureInParm("PUSE_FREE_DIAL_FLAG",DbSession.DbType.NUMBER,chkUseFreeDialFlag.Checked);
			db.ProcedureInParm("PUSE_CROSMILE_FLAG",DbSession.DbType.NUMBER,chkUseCrosmileFlag.Checked);
			db.ProcedureInParm("pCROSMILE_URI",DbSession.DbType.VARCHAR2,txtCrosmileUri.Text.Trim());
			db.ProcedureInParm("PPAYMENT_TIMING_TYPE",DbSession.DbType.NUMBER,iPaymentTimingType);
			db.ProcedureInParm("PGUARDIAN_NM",DbSession.DbType.VARCHAR2,txtGuardianNm.Text);
			db.ProcedureInParm("PGUARDIAN_TEL",DbSession.DbType.VARCHAR2,txtGuardianTel.Text);
			db.ProcedureInParm("pUSE_VOICEAPP_FLAG",DbSession.DbType.NUMBER,chkUseVoiceappFlag.Checked);
			db.ProcedureInParm("pREMARKS1",DbSession.DbType.VARCHAR2,this.txtRemarks1.Text.TrimEnd());
			db.ProcedureInParm("pREMARKS2",DbSession.DbType.VARCHAR2,this.txtRemarks2.Text.TrimEnd());
			db.ProcedureInParm("pREMARKS3",DbSession.DbType.VARCHAR2,this.txtRemarks3.Text.TrimEnd());
			string[] sRemarks = SysPrograms.SplitBytes(Encoding.GetEncoding("UTF-8"),txtRemarks4.Text,3000);
			db.ProcedureInArrayParm("PREMARKS4",DbSession.DbType.VARCHAR2,sRemarks);
			db.ProcedureInParm("pTX_MULLER3_NG_MAIL_ADDR_FLAG",DbSession.DbType.NUMBER,int.Parse(rdoTxMuller3NgMailAddrFlag.SelectedValue));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		//画像・動画フォルダ名も変更
		UpdateFolder(pDelFlag);

		//ARCHE対応　別システムにて電話番号、メールアドレスの2重管理用
		string sCastMainteDataPostUrl = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["castMainteDataPostUrl"]);
		if (!sCastMainteDataPostUrl.Equals(string.Empty)) {
			System.Text.Encoding enc = System.Text.Encoding.GetEncoding("UTF-8");
			string sPostData = "";
			string sBasePost = "loginid={0}&passwd={1}&mail={2}&tel={3}";

			sPostData = string.Format(sBasePost,txtLoginId.Text,txtLoginPassword.Text,txtEmailAddr.Text,txtTel.Text);
			byte[] oPostDataBytes = enc.GetBytes(sPostData);

			System.Net.WebClient wc = new System.Net.WebClient();
			wc.Headers.Add("Content-Type","application/x-www-form-urlencoded");
			byte[] resData = wc.UploadData(sCastMainteDataPostUrl,oPostDataBytes);

			wc.Dispose();
		}
		if (pDelFlag != 1) {
			Server.Transfer(string.Format("CastView.aspx?loginid={0}&return={1}",iBridUtil.GetStringValue(ViewState["LOGIN_ID"]),ViewState["RETURN"].ToString()));
		} else {
			Server.Transfer(string.Format("CastView.aspx?return={0}",ViewState["RETURN"].ToString()));
		}

	}

	private void UpdateFolder(int pDelFlag) {
		if ((lblLoginId.Text == "") || (lblLoginId.Text == txtLoginId.Text)) {
			return;
		}

		DataSet ds = new DataSet();
		using (Site oSite = new Site()) {
			ds = oSite.GetList();
		}

		foreach (DataRow dr in ds.Tables[0].Rows) {
			string sWebPhisicalDir = "";
			using (Site oSite = new Site()) {
				oSite.GetValue(dr["SITE_CD"].ToString(),"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
			}

			if (pDelFlag == 0) {
				string sDirFrom = "";
				string sDirTo = "";

				sDirFrom = sWebPhisicalDir + ViCommConst.PIC_DIRECTRY + "\\" + dr["SITE_CD"].ToString() + string.Format("\\Operator\\{0}",lblLoginId.Text);
				sDirTo = sWebPhisicalDir + ViCommConst.PIC_DIRECTRY + "\\" + dr["SITE_CD"].ToString() + string.Format("\\Operator\\{0}",txtLoginId.Text);
				if (Directory.Exists(sDirFrom)) {
					Directory.Move(sDirFrom,sDirTo);
				}

				sDirFrom = sWebPhisicalDir + ViCommConst.MOVIE_DIRECTRY + "\\" + dr["SITE_CD"].ToString() + string.Format("\\Operator\\{0}",lblLoginId.Text);
				sDirTo = sWebPhisicalDir + ViCommConst.MOVIE_DIRECTRY + "\\" + dr["SITE_CD"].ToString() + string.Format("\\Operator\\{0}",txtLoginId.Text);
				if (Directory.Exists(sDirFrom)) {
					Directory.Move(sDirFrom,sDirTo);
				}
			}
		}
	}

	private void UploadPicture(System.Web.UI.WebControls.FileUpload pUpload,System.Web.UI.WebControls.Image pImage,int pPicNo) {
		string sWebPhisicalDir = "";
		string sDomain = "";
		decimal dNo;

		using (Site oSite = new Site()) {
			oSite.GetValue(ViCommConst.CAST_SITE_CD,"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
			oSite.GetValue(ViCommConst.CAST_SITE_CD,"HOST_NM",ref sDomain);
		}

		using (CastPic objPic = new CastPic()) {
			dNo = objPic.GetPicNo();
		}

		string sFileNm = "",sPath = "",sFullPath = "";

		if (pUpload.HasFile) {

			sFileNm = ViCommConst.PIC_HEADER + iBridUtil.addZero(dNo.ToString(),ViCommConst.OBJECT_NM_LENGTH);
			sPath = sWebPhisicalDir +
							ViCommConst.PIC_DIRECTRY + "\\" + ViCommConst.CAST_SITE_CD + "\\Operator\\ID-PIC";

			sFullPath = sPath + "\\" + sFileNm + ViCommConst.PIC_FOODER;

			using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
				if (!System.IO.File.Exists(sFullPath)) {
					pUpload.SaveAs(sFullPath);
				}

				using (DbSession db = new DbSession()) {
					db.PrepareProcedure("CAST_ID_PIC_UPLOAD");
					db.ProcedureInParm("PTEMP_REGIST_ID",DbSession.DbType.VARCHAR2,lblUserSeq.Text);
					db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.VARCHAR2,dNo);
					db.ProcedureInParm("PID_PIC_NO",DbSession.DbType.NUMBER,pPicNo);
					db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
					db.ExecuteProcedure();
				}
				pImage.ImageUrl = ConfigurationManager.AppSettings["Root"] + "/data/" + ViCommConst.CAST_SITE_CD + "/operator/ID-PIC/" + iBridUtil.addZero(dNo.ToString(),ViCommConst.OBJECT_NM_LENGTH) + ".jpg";

			}
		}
	}

	private void DeletePicture(int pPicNo) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_ID_PIC_DELETE");
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,lblUserSeq.Text);
			db.ProcedureInParm("pID_PIC_NO",DbSession.DbType.NUMBER,pPicNo);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	protected void vdcBirthday_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid) {
			int iOkAge;
			int.TryParse(iBridUtil.GetStringValue(ConfigurationManager.AppSettings["RefuseAge"]),out iOkAge);
			if (iOkAge == 0) {
				iOkAge = 10;
			}
			args.IsValid = (ViCommPrograms.Age(txtBirthDay.Text) >= iOkAge);
		}
	}

	protected void btnRotate_Command(object sender,CommandEventArgs e) {
		if (!e.CommandName.Equals("ROTATE")) {
			return;
		}

		string[] sArguments = e.CommandArgument.ToString().Split(',');
		int iIndex;
		if (!int.TryParse(sArguments[0],out iIndex)) {
			return;
		}

		this.RotatePic(iBridUtil.addZero(sArguments[1],ViCommConst.OBJECT_NM_LENGTH));

		this.GetPictureData();
	}

	private void RotatePic(string pIdNo) {
		string sWebPhisicalDir = "";
		string sPath;
		try {
			using (Site oSite = new Site()) {
				oSite.GetValue(ViCommConst.CAST_SITE_CD,"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
			}
			sPath = sWebPhisicalDir +
							ViCommConst.PIC_DIRECTRY + "\\" + ViCommConst.CAST_SITE_CD + "\\Operator\\ID-PIC\\" + ViCommConst.PIC_HEADER + pIdNo + ViCommConst.PIC_FOODER;

			using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
				System.Drawing.Image oImg = System.Drawing.Image.FromFile(sPath);
				oImg.RotateFlip(System.Drawing.RotateFlipType.Rotate90FlipNone);
				oImg.Save(sPath,ImageFormat.Jpeg);
			}
		} catch {
		}
	}


	protected void vdcTerminalUniqueId_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid) {
			if (bCheckTmgt && !lstUserStatus.SelectedValue.Equals(ViCommConst.USER_WOMAN_HOLD)) {
				if (string.IsNullOrEmpty(this.txtTerminalUniqueId.Text)) {
					return;
				}

				using (CastCharacter oCharacter = new CastCharacter()) {
					args.IsValid = oCharacter.IsTerminalUniqueIdDupli(this.lblUserSeq.Text,this.txtTerminalUniqueId.Text);
				}
			}
		}
	}
	protected void vdcIModeId_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid) {
			if (bCheckTmgt && !lstUserStatus.SelectedValue.Equals(ViCommConst.USER_WOMAN_HOLD)) {
				if (string.IsNullOrEmpty(this.txtIModeId.Text)) {
					return;
				}

				using (CastCharacter oCharacter = new CastCharacter()) {
					args.IsValid = oCharacter.IsIModeIdDupli(this.lblUserSeq.Text,this.txtIModeId.Text);
				}
			}
		}
	}
	protected void vdcEmailAddr_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid) {
			if (bCheckTmgt && !lstUserStatus.SelectedValue.Equals(ViCommConst.USER_WOMAN_HOLD)) {
				if (string.IsNullOrEmpty(this.txtEmailAddr.Text)) {
					return;
				}

				using (CastCharacter oCharacter = new CastCharacter()) {
					args.IsValid = oCharacter.IsEmailDupli(this.lblUserSeq.Text,this.txtEmailAddr.Text);
				}
			}
		}
	}
	protected void vdcTel_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid) {
			if (bCheckTmgt && !lstUserStatus.SelectedValue.Equals(ViCommConst.USER_WOMAN_HOLD)) {
				if (string.IsNullOrEmpty(this.txtTel.Text)) {
					return;
				}

				using (CastCharacter oCharacter = new CastCharacter()) {
					args.IsValid = oCharacter.IsTelDupli(this.lblUserSeq.Text,this.txtTel.Text);
				}
			}
		}
	}


	protected bool GetImgVisible(object pIdPicSeq) {
		string sIdPicSeq = iBridUtil.GetStringValue(pIdPicSeq);
		return !string.IsNullOrEmpty(sIdPicSeq);
	}

	protected string GetImgUrl(object pIdPicSeq) {
		if (!this.GetImgVisible(pIdPicSeq)) {
			return string.Empty;
		}
		return ConfigurationManager.AppSettings["Root"] + "/data/" + ViCommConst.CAST_SITE_CD + "/operator/ID-PIC/" + iBridUtil.addZero(iBridUtil.GetStringValue(pIdPicSeq),ViCommConst.OBJECT_NM_LENGTH) + ".jpg" + string.Format("?dummy={0}",DateTime.Now.Ticks);
	}

	protected bool IsOverRightLocalStaff() {
		if (Session["AdminType"].ToString().CompareTo(ViCommConst.RIGHT_LOCAL_STAFF) <= 0) {
			return false;
		} else {
			return true;
		}
	}
}
