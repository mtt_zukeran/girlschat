﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ボイス管理
--	Progaram ID		: VoiceMainte
--
--  Creation Date	: 2010.07.27
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater		Update Explain
  


-------------------------------------------------------------------------*/
using System;
using System.IO;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

/// <summary>
/// 着ボイスの登録、編集、削除機能を提供する画面
/// </summary>
/// <remarks>
/// 画面パラメータ
/// 
/// </remarks>
public partial class Cast_VoiceMainte : System.Web.UI.Page {

	#region □■□ プロパティ □■□ ==============================================================
	protected string SiteCd {
		get { return this.ViewState["sitecd"] as string; }
		set { this.ViewState["sitecd"] = value; }
	}
	protected string UserSeq {
		get { return this.ViewState["userseq"] as string; }
		set { this.ViewState["userseq"] = value; }
	}
	protected string UserCharNo {
		get { return this.ViewState["usercharno"] as string; }
		set { this.ViewState["usercharno"] = value; }
	}
	protected int VoiceType {
		get { return (int)this.ViewState["voicetype"]; }
		set { this.ViewState["voicetype"] = value; }
	}
	protected string VoiceSeq {
		get { return this.ViewState["voiceseq"] as string; }
		set { this.ViewState["voiceseq"] = value; }
	}
	protected decimal RevisionNo {
		get { return (decimal)this.ViewState["REVISION_NO"]; }
		set { this.ViewState["REVISION_NO"] = value; }
	}
	protected string RowId {
		get { return this.ViewState["ROWID"] as string; }
		set { this.ViewState["ROWID"] = value; }
	}
	protected string VoiceFileNm {
		get { return this.ViewState["VOICE_FILE_NM"] as string; }
		set { this.ViewState["VOICE_FILE_NM"] = value; }
	}
	protected string Return {
		get { return this.ViewState["return"] as string; }
		set { this.ViewState["return"] = value; }
	}


	#endregion ====================================================================================


	protected void Page_Load(object sender, EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			this.InitPage();
		}
	}


	protected void btnUpdate_Click(object sender, EventArgs e) {
		if(!this.IsValid)return;
		this.SaveData(false);
		this.TransferBack();
	}

	protected void btnDelete_Click(object sender, EventArgs e) {
		if (!this.IsValid) return;
		this.SaveData(true);
		this.TransferBack();
	}

	protected void btnCancel_Click(object sender, EventArgs e) {
		this.TransferBack();
	}


	/// <summary>
	///  画面の初期化処理を行う
	/// </summary>
	private void InitPage(){

		this.ParseFormParameters();

		this.lblSiteCd.Text = this.SiteCd;
		this.lblUserSeq.Text = this.UserSeq;
		this.lblUserCharNo.Text = this.UserCharNo;
		using (Cast oCast = new Cast()) {
			string sCastName = string.Empty;
			string sLoginId = string.Empty;

			oCast.GetValue(this.UserSeq, "CAST_NM", ref sCastName);	// ｷｬｽﾄ名
			oCast.GetValue(this.UserSeq, "LOGIN_ID", ref sLoginId);	// ﾛｸﾞｲﾝID

			this.lblUserNm.Text = sCastName;
			this.lblLoginId.Text = sLoginId;
		}
		
		this.DataBind();
		this.GetData();

	}
	
	/// <summary>
	/// 画面に表示するデータ取得を行う
	/// </summary>
	private void GetData(){
		using (DbSession db = new DbSession()) {
			// ===========================
			//  データ取得
			// ===========================
			db.PrepareProcedure("CAST_VOICE_GET");
			// in
			db.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.SiteCd);
			db.ProcedureInParm("pUSER_SEQ", DbSession.DbType.VARCHAR2, this.UserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO", DbSession.DbType.VARCHAR2, this.UserCharNo);
			db.ProcedureInParm("pVOICE_SEQ", DbSession.DbType.VARCHAR2, this.VoiceSeq);
			// out
			db.ProcedureOutParm("pVOICE_FILE_NM", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pVOICE_TITLE", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pVOICE_DOC", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pCHARGE_POINT", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pOBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pOBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pUPLOAD_DATE", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pVOICE_TYPE", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pROWID", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pREVISION_NO", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pRECORD_COUNT", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();

			// ===========================
			//  画面出力
			// ===========================
			if(db.GetIntValue("pRECORD_COUNT")==0){
				string sErrorMessage = string.Format("該当するｷｬｽﾄ着ﾎﾞｲｽ情報が取得できませんでした。sitecd={0},userseq={1},usercharno={2},voiceseq{3}",this.SiteCd,this.UserSeq,this.UserCharNo,this.VoiceSeq);
				throw new ApplicationException(sErrorMessage);
			}

			this.txtVoiceTitle.Text = db.GetStringValue("pVOICE_TITLE").Trim();
			this.txtChargePoint.Text = db.GetStringValue("pCHARGE_POINT").Trim();
			string sUnAtuthFlag = db.GetStringValue("pOBJ_NOT_APPROVE_FLAG").Trim();
			string sNotPublishFlag = db.GetStringValue("pOBJ_NOT_PUBLISH_FLAG").Trim();
			
			if (sUnAtuthFlag.Equals("0")) {
				if (sNotPublishFlag.Equals("0")) {
					lstAuthType.SelectedValue = ViCommConst.MOVIE_APPLY_OK;
				} else if (sNotPublishFlag.Equals("1")) {
					lstAuthType.SelectedValue = ViCommConst.MOVIE_APPLY_NG;
				}else{
					throw new InvalidOperationException();
				}
			} else if (sUnAtuthFlag.Equals("1")) {
				lstAuthType.SelectedValue = ViCommConst.MOVIE_APPLY_WAIT;
			} else {
				throw new InvalidOperationException();
			}

			this.RevisionNo = decimal.Parse(db.GetStringValue("PREVISION_NO").Trim());
			this.RowId = db.GetStringValue("PROWID").Trim();
			this.VoiceFileNm = db.GetStringValue("pVOICE_FILE_NM").Trim();
		}

	}
	
	/// <summary>
	/// 画面の入力値をDBにシリアライズする。
	/// </summary>
	private void SaveData(bool pIsDelete){
		int iNotApproveFlag = 0;
		int iDelFlag = 0;
		int iNotPublishFlag = 0;

		if (!pIsDelete) {
			switch (lstAuthType.SelectedValue) {
				case ViCommConst.MOVIE_APPLY_WAIT:	// 認証待ち
					iDelFlag = 0;
					iNotApproveFlag = 1;
					iNotPublishFlag = 0;
					break;

				case ViCommConst.MOVIE_APPLY_OK:	// 公開
					iDelFlag = 0;
					iNotApproveFlag = 0;
					iNotPublishFlag = 0;
					break;

				case ViCommConst.MOVIE_APPLY_REMOVE: // 削除
					iNotApproveFlag = 0;
					iDelFlag = 1;
					iNotPublishFlag = 0;
					break;

				case ViCommConst.MOVIE_APPLY_NG:	// 非公開

					iNotApproveFlag = 0;
					iDelFlag = 0;
					iNotPublishFlag = 1;
					break;
			}
		} else {
			iDelFlag = 1;
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_VOICE_MAINTE");
			// in
			db.ProcedureInParm("pVOICE_SEQ", DbSession.DbType.VARCHAR2, this.VoiceSeq);
			db.ProcedureInParm("pVOICE_TITLE", DbSession.DbType.VARCHAR2, txtVoiceTitle.Text.Trim());
			db.ProcedureInParm("pVOICE_DOC", DbSession.DbType.VARCHAR2, string.Empty);
			db.ProcedureInParm("pOBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,iNotApproveFlag);
			db.ProcedureInParm("pOBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER,iNotPublishFlag);
			db.ProcedureInParm("pCHARGE_POINT", DbSession.DbType.NUMBER, decimal.Parse(txtChargePoint.Text));
			db.ProcedureInParm("pVOICE_TYPE", DbSession.DbType.NUMBER, this.VoiceType);
			db.ProcedureInParm("pROWID", DbSession.DbType.VARCHAR2, this.RowId);
			db.ProcedureInParm("pREVISION_NO", DbSession.DbType.NUMBER, this.RevisionNo);
			db.ProcedureInParm("pDEL_FLAG", DbSession.DbType.NUMBER, iDelFlag);
			// out
			db.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		if (iDelFlag == 1) {
			string sWebPhisicalDir = string.Empty;
			using (Site oSite = new Site()) {
				oSite.GetValue(lblSiteCd.Text, "WEB_PHISICAL_DIR", ref sWebPhisicalDir);
			}

			string sCastVoiceDir = ViCommPrograms.GetCastVoiceDir(sWebPhisicalDir, this.SiteCd,this.lblLoginId.Text.Trim());

			using (Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME,ViCommConst.FILE_UPLOAD_PASSWORD)) {
				foreach(string sFilePath in Directory.GetFiles(sCastVoiceDir,string.Format("{0}*",this.VoiceFileNm))){
					if(File.Exists(sFilePath))File.Delete(sFilePath);
				}
			}
		}
	}

	/// <summary>
	/// 元の画面へ遷移する
	/// </summary>
	private void TransferBack() {

		ViComm.UrlBuilder oUrlBuilder = new ViComm.UrlBuilder("../Cast/CastView.aspx");

		oUrlBuilder.Parameters.Add("loginid", lblLoginId.Text.Trim());
		oUrlBuilder.Parameters.Add("voicesite", this.SiteCd);
		oUrlBuilder.Parameters.Add("usercharno", this.UserCharNo);
		oUrlBuilder.Parameters.Add("return", this.Return);
		if (this.GetMultiCharFlag(this.SiteCd)) {
			oUrlBuilder.Parameters.Add("sitecd", this.SiteCd);
		}

		Server.Transfer(oUrlBuilder.ToString());
	}
	
	/// <summary>
	/// 画面引数を解析する
	/// </summary>
	private void ParseFormParameters(){
		if (string.IsNullOrEmpty(Request.QueryString["sitecd"])) throw new InvalidOperationException("sitecdが指定されていません。");
		if (string.IsNullOrEmpty(Request.QueryString["userseq"])) throw new InvalidOperationException("userseqが指定されていません。");
		if (string.IsNullOrEmpty(Request.QueryString["usercharno"])) throw new InvalidOperationException("usercharnoが指定されていません。");
		if (string.IsNullOrEmpty(Request.QueryString["voicetype"])) throw new InvalidOperationException("voicetypeが指定されていません。");
		if (string.IsNullOrEmpty(Request.QueryString["voiceseq"])) throw new InvalidOperationException("voiceseqが指定されていません。");
		//if (string.IsNullOrEmpty(Request.QueryString["return"])) throw new InvalidOperationException("returnが指定されていません。");

		this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		this.UserSeq = iBridUtil.GetStringValue(Request.QueryString["userseq"]);
		this.UserCharNo = iBridUtil.GetStringValue(Request.QueryString["usercharno"]);
		this.VoiceType = int.Parse(Request.QueryString["voicetype"]);
		this.VoiceSeq = iBridUtil.GetStringValue(Request.QueryString["voiceseq"]);
		this.Return = iBridUtil.GetStringValue(Request.QueryString["return"]);
	}
	/// <summary>
	/// ｻｲﾄがﾏﾙﾁｷｬｽﾄに対応しているかどうかを示す値を取得する
	/// </summary>
	/// <param name="pSiteCd">ｻｲﾄｺｰﾄﾞ</param>
	/// <returns>ｻｲﾄがﾏﾙﾁｷｬｽﾄに対応している場合はtrue。それ以外はfalse。</returns>
	public bool GetMultiCharFlag(string pSiteCd) {
		using (Site oSite = new Site()) {
			return (oSite.GetMultiCharFlag(pSiteCd));
		}
	}

}
