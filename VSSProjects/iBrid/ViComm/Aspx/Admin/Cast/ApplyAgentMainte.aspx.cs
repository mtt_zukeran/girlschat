﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: エージェントID登録

--	Progaram ID		: ApplyAgentMainte
--
--  Creation Date	: 2012.01.19
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public partial class Cast_ApplyAgentMainte : System.Web.UI.Page {
	private static readonly int REQUIRED_PIC_COUNT = 2;

	private bool bInitial = false;

	private string TempRegistId {
		get { return iBridUtil.GetStringValue(this.ViewState["TempRegistId"]); }
		set { this.ViewState["TempRegistId"] = value; }
	}

	private string SiteCd {
		get { return iBridUtil.GetStringValue(this.ViewState["SiteCd"]); }
		set { this.ViewState["SiteCd"] = value; }
	}

	private string RevisionNo {
		get { return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]); }
		set { this.ViewState["RevisionNo"] = value; }
	}

	private bool Certifiable {
		get { return Session["AdminType"].ToString().CompareTo(ViCommConst.RIGHT_LOCAL_STAFF) > 0; }
	}

	private string[] IdPicSeqArray {
		get { return (string[])ViewState["IdPicSeqArray"]; }
		set { this.ViewState["IdPicSeqArray"] = value; }
	}

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			this.IdPicSeqArray = new string[10];
			this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			this.TempRegistId = iBridUtil.GetStringValue(Request.QueryString["tempregistid"]);
			this.bInitial = string.IsNullOrEmpty(this.TempRegistId);

			this.pnlRegist.Visible = !this.Certifiable;
			this.pnlCertify.Visible = this.Certifiable;

			this.lstProductionSeq.DataBind();
			this.lstManagerSeq.DataBind();
			this.ClearField();
			if (string.IsNullOrEmpty(this.TempRegistId)) {
				this.lstProductionSeq.SelectedValue = iBridUtil.GetStringValue(Session["PRODUCTION_SEQ"]);
				this.lstManagerSeq.SelectedValue = iBridUtil.GetStringValue(Session["MANAGER_SEQ"]);
			}
			this.GetData();
		}
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		this.GetData();
	}

	protected void btnCertify_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("APPLY_AGENT");
				db.ProcedureInParm("pTEMP_REGIST_ID",DbSession.DbType.VARCHAR2,this.TempRegistId);
				db.ProcedureOutParm("pLOGIN_ID",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("pUSER_SEQ",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("pREGIST_STATUS",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
			}

			this.Response.Redirect("../Cast/ApplyAgentList.aspx");
		}
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			this.UpdateData(ViCommConst.FLAG_OFF);
			this.Response.Redirect("../Cast/ApplyAgentList.aspx");
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			this.UpdateData(ViCommConst.FLAG_ON);
			this.Response.Redirect("../Cast/ApplyAgentList.aspx");
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.Response.Redirect("../Cast/ApplyAgentList.aspx");
	}


	protected void btnUpload_Command(object sender,CommandEventArgs e) {
		if (!e.CommandName.Equals("UPLOAD")) {
			return;
		}

		int iItemIndex;
		if (int.TryParse(e.CommandArgument.ToString(),out iItemIndex)) {
			FileUpload oFileUpload = this.rptUpload.Items[iItemIndex].FindControl("uldIdPic") as FileUpload;
			if (oFileUpload != null) {
				this.UploadPicture(oFileUpload,iItemIndex);
				this.GetPictureData();
			}
		}
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	private void ClearField() {
		this.txtCastNm.Text = string.Empty;
		this.txtCastKanaNm.Text = string.Empty;
		this.txtBirthDay.Text = string.Empty;
		this.txtLoginPassword.Text = string.Empty;
		this.txtEmailAddr.Text = string.Empty;
		this.txtTel.Text = string.Empty;
		this.lstProductionSeq.SelectedValue = string.Empty;
		this.lstManagerSeq.SelectedValue = string.Empty;
		this.txtRemarks.Text = string.Empty;
		this.RevisionNo = string.Empty;
		this.rptUpload.DataSource = null;
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("AGENT_REGIST_GET");
			db.ProcedureBothParm("pTEMP_REGIST_ID",DbSession.DbType.VARCHAR2,this.TempRegistId);
			db.ProcedureOutParm("pSITE_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pCAST_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pCAST_KANA_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pBIRTHDAY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pLOGIN_PASSWORD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pEMAIL_ADDR",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pTEL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pPRODUCTION_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pMANAGER_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutArrayParm("pID_PIC_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pREMARKS",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			if (db.GetIntValue("pRECORD_COUNT") > 0) {
				this.SiteCd = db.GetStringValue("pSITE_CD");
				this.txtCastNm.Text = db.GetStringValue("pCAST_NM");
				this.txtCastKanaNm.Text = db.GetStringValue("pCAST_KANA_NM");
				this.txtBirthDay.Text = db.GetStringValue("pBIRTHDAY");
				this.txtLoginPassword.Text = db.GetStringValue("pLOGIN_PASSWORD");
				this.txtEmailAddr.Text = db.GetStringValue("pEMAIL_ADDR");
				this.txtTel.Text = db.GetStringValue("pTEL");
				this.lstProductionSeq.SelectedValue = db.GetStringValue("pPRODUCTION_SEQ");
				this.lstManagerSeq.SelectedValue = db.GetStringValue("pMANAGER_SEQ");
				this.IdPicSeqArray = db.GetArrayString("pID_PIC_SEQ");
				this.txtRemarks.Text = db.GetStringValue("pREMARKS");
				this.RevisionNo = db.GetStringValue("pREVISION_NO");
			} else {
				this.TempRegistId = db.GetStringValue("pTEMP_REGIST_ID");
			}
		}
		this.GetPictureData();
	}

	private void GetPictureData() {
		this.rptUpload.DataSource = this.IdPicSeqArray;
		this.rptUpload.DataBind();
	}

	private void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("AGENT_REGIST_MAINTE");
			db.ProcedureInParm("pTEMP_REGIST_ID",DbSession.DbType.VARCHAR2,this.TempRegistId);
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			db.ProcedureInParm("pCAST_NM",DbSession.DbType.VARCHAR2,this.txtCastNm.Text.Trim());
			db.ProcedureInParm("pCAST_KANA_NM",DbSession.DbType.VARCHAR2,this.txtCastKanaNm.Text.Trim());
			db.ProcedureInParm("pBIRTHDAY",DbSession.DbType.VARCHAR2,this.txtBirthDay.Text.Trim());
			db.ProcedureInParm("pLOGIN_PASSWORD",DbSession.DbType.VARCHAR2,this.txtLoginPassword.Text.Trim());
			db.ProcedureInParm("pEMAIL_ADDR",DbSession.DbType.VARCHAR2,this.txtEmailAddr.Text.Trim());
			db.ProcedureInParm("pTEL",DbSession.DbType.VARCHAR2,this.txtTel.Text.Trim());
			db.ProcedureInParm("pPRODUCTION_SEQ",DbSession.DbType.VARCHAR2,this.lstProductionSeq.SelectedValue);
			db.ProcedureInParm("pMANAGER_SEQ",DbSession.DbType.VARCHAR2,this.lstManagerSeq.SelectedValue);
			db.ProcedureInArrayParm("pID_PIC_SEQ",DbSession.DbType.NUMBER,this.IdPicSeqArray);
			db.ProcedureInParm("pREMARKS",DbSession.DbType.VARCHAR2,this.txtRemarks.Text.Trim());
			db.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			db.ProcedureInParm("pDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			this.TempRegistId = db.GetStringValue("pTEMP_REGIST_ID");
		}

		if (!this.Certifiable) {
			using (MailTemplate oMailTemplate = new MailTemplate()) {
				using (DataSet ds = oMailTemplate.GetListByTemplateType(this.SiteCd,ViCommConst.MAIL_TP_AGENT_REGIST,string.Empty)) {
					if (ds.Tables[0].Rows.Count > 0) {
						string sMailTemplateNo = iBridUtil.GetStringValue(ds.Tables[0].Rows[0]["MAIL_TEMPLATE_NO"]);
						if (oMailTemplate.GetOne(this.SiteCd,sMailTemplateNo)) {
							string[] sDoc;
							int iDocCount;

							if (oMailTemplate.IsTextMail) {
								SysPrograms.SeparateHtml(HttpUtility.HtmlDecode(oMailTemplate.textDoc.Replace(Environment.NewLine,"<br/>")),ViCommConst.MAX_ORG_MAIL_BLOCKS,out sDoc,out iDocCount);
							} else {
								SysPrograms.SeparateHtml(HttpUtility.HtmlDecode(oMailTemplate.htmlDoc),ViCommConst.MAX_ORG_MAIL_BLOCKS,out sDoc,out iDocCount);
							}

							using (DbSession db = new DbSession()) {
								db.PrepareProcedure("TX_AGENT_REGIST_REPORT");
								db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
								db.ProcedureInParm("pTEMP_REGIST_ID",DbSession.DbType.VARCHAR2,this.TempRegistId);
								db.ProcedureInParm("pMAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,sMailTemplateNo);
								db.ProcedureInParm("pORIGINAL_TITLE",DbSession.DbType.VARCHAR2,oMailTemplate.mailTitle);
								db.ProcedureInArrayParm("pORIGINAL_DOC",DbSession.DbType.ARRAY_VARCHAR2,sDoc);
								db.ProcedureInParm("pORIGINAL_DOC_COUNT",DbSession.DbType.NUMBER,sDoc.Length);
								db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
								db.ExecuteProcedure();
							}
						}
					}
				}
			}
		}

		this.GetData();
	}

	private void UploadPicture(System.Web.UI.WebControls.FileUpload pUpload,int pPicNo) {
		string sWebPhisicalDir = "";
		string sDomain = "";
		decimal dNo;

		using (Site oSite = new Site()) {
			oSite.GetValue(ViCommConst.CAST_SITE_CD,"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
			oSite.GetValue(ViCommConst.CAST_SITE_CD,"HOST_NM",ref sDomain);
		}

		using (CastPic objPic = new CastPic()) {
			dNo = objPic.GetPicNo();
		}

		string sFileNm = "",sPath = "",sFullPath = "";

		if (pUpload.HasFile) {

			sFileNm = ViCommConst.PIC_HEADER + iBridUtil.addZero(dNo.ToString(),ViCommConst.OBJECT_NM_LENGTH);
			sPath = sWebPhisicalDir +
							ViCommConst.PIC_DIRECTRY + "\\" + ViCommConst.CAST_SITE_CD + "\\Operator\\ID-PIC";

			sFullPath = sPath + "\\" + sFileNm + ViCommConst.PIC_FOODER;

			using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
				if (!System.IO.File.Exists(sFullPath)) {
					pUpload.SaveAs(sFullPath);
				}

				using (DbSession db = new DbSession()) {
					db.PrepareProcedure("AGENT_REGIST_ID_PIC_UPLOAD");
					db.ProcedureInParm("PTEMP_REGIST_ID",DbSession.DbType.VARCHAR2,this.TempRegistId);
					db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.VARCHAR2,dNo);
					db.ProcedureInParm("PID_PIC_NO",DbSession.DbType.NUMBER,pPicNo + 1);
					db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
					db.ExecuteProcedure();
				}

				this.IdPicSeqArray[pPicNo] = iBridUtil.GetStringValue(dNo);
			}
		}
	}

	protected void vdcBirthday_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid) {
			int iOkAge;
			int.TryParse(iBridUtil.GetStringValue(ConfigurationManager.AppSettings["RefuseAge"]),out iOkAge);
			if (iOkAge == 0) {
				iOkAge = 18;
			}
			args.IsValid = (ViCommPrograms.Age(txtBirthDay.Text) >= iOkAge);
		}
	}

	protected void vdcIdPic_ServerValidate(object source,ServerValidateEventArgs args) {
		if (this.rptUpload.Items.Count == 0) {
			return;
		}

		this.vdcIdPic.ErrorMessage = string.Empty;
		if (args.IsValid) {
			for (int i = 0; i < REQUIRED_PIC_COUNT; i++) {
				Image imgIdPic = this.rptUpload.Items[i].FindControl("imgIdPic") as Image;
				if (imgIdPic != null) {
					if (!imgIdPic.Visible) {
						if (vdcIdPic.ErrorMessage.Length == 0) {
							this.vdcIdPic.ErrorMessage = string.Format("身分証画像{0}が登録されていません。",i + 1);
						} else {
							this.vdcIdPic.ErrorMessage += string.Format("<br />身分証画像{0}が登録されていません。",i + 1);
						}
						args.IsValid = false;
					}
				}
			}
		}
	}

	protected bool GetImgVisible(object pIdPicSeq) {
		string sIdPicSeq = iBridUtil.GetStringValue(pIdPicSeq);
		return !string.IsNullOrEmpty(sIdPicSeq);
	}

	protected string GetImgUrl(object pIdPicSeq) {
		if (!this.GetImgVisible(pIdPicSeq)) {
			return string.Empty;
		}
		return ConfigurationManager.AppSettings["Root"] + "/data/" + ViCommConst.CAST_SITE_CD + "/operator/ID-PIC/" + iBridUtil.addZero(iBridUtil.GetStringValue(pIdPicSeq),ViCommConst.OBJECT_NM_LENGTH) + ".jpg" + string.Format("?dummy={0}",DateTime.Now.Ticks);
	}
}
