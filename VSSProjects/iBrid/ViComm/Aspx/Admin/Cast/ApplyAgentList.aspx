﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ApplyAgentList.aspx.cs" Inherits="Cast_ApplyAgentList" Title="エージェントID認証"
    ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="エージェントID認証"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <fieldset class="fieldset">
            <legend>[検索条件]</legend>
            <asp:Panel runat="server" ID="pnlKey">
                <table border="0" style="width: 800px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            サイト
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="240px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            プロダクション
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstProductionSeq" runat="server" DataSourceID="dsProduction"
                                DataTextField="PRODUCTION_NM" DataValueField="PRODUCTION_SEQ" Width="240px" OnDataBound="lst_DataBound">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
                <asp:Button runat="server" ID="btnRegist" Text="新規登録" CssClass="seekbutton" OnClick="btnRegist_Click" />
            </asp:Panel>
        </fieldset>
        <fieldset>
            <legend>
                <%= DisplayWordUtil.Replace("[申請中出演者一覧]") %>
            </legend>
            <asp:Panel runat="server" ID="pnlCount">
                <a class="reccount">Record Count
                    <%#GetRecCount() %>
                </a>
                <br />
                <a class="reccount">Current viewing page
                    <%=grdAgentRegist.PageIndex + 1%>
                    of
                    <%=grdAgentRegist.PageCount%>
                </a>
            </asp:Panel>
            &nbsp;
            <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="430px">
                <asp:GridView ID="grdAgentRegist" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    DataSourceID="dsAgentRegist" SkinID="GridViewColor" AllowSorting="True">
                    <Columns>
                        <asp:BoundField DataField="PRODUCTION_NM" HeaderText="プロダクション" />
                        <asp:BoundField DataField="MANAGER_NM" HeaderText="担当" />
                        <asp:BoundField DataField="CAST_NM" HeaderText="氏名" SortExpression="CAST_NM" />
                        <asp:BoundField DataField="CAST_KANA_NM" HeaderText="氏名(カナ)" SortExpression="CAST_KANA_NM" />
                        <asp:TemplateField HeaderText="年齢">
                            <ItemTemplate>
                                <asp:Label ID="lblAge" runat="server" Text='<%# GetAgeByBirthDay(Eval("BIRTHDAY")) %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="REMARKS" HeaderText="備考" />
                        <asp:BoundField DataField="REGIST_DATE" HeaderText="登録申請日時" DataFormatString="{0:yyyy/MM/dd HH:mm}"
                            HtmlEncode="False" SortExpression="REGIST_DATE">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:HyperLinkField HeaderText="詳細" Text="詳細" DataNavigateUrlFields="TEMP_REGIST_ID"
                            DataNavigateUrlFormatString="../Cast/ApplyAgentMainte.aspx?tempregistid={0}">
                            <ItemStyle Width="60px" HorizontalAlign="Center" />
                        </asp:HyperLinkField>
                    </Columns>
                    <PagerSettings Mode="NumericFirstLast" />
                </asp:GridView>
            </asp:Panel>
        </fieldset>
        <br />
    </div>
    <asp:ObjectDataSource ID="dsAgentRegist" runat="server" SelectMethod="GetPageCollection"
        TypeName="AgentRegist" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelected="dsAgentRegist_Selected"
        OnSelecting="dsAgentRegist_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:ControlParameter ControlID="lstProductionSeq" Name="pProductionSeq" PropertyName="SelectedValue"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsProduction" runat="server" SelectMethod="GetList" TypeName="Production">
    </asp:ObjectDataSource>
</asp:Content>
