﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="VoiceMainte.aspx.cs" Inherits="Cast_VoiceMainte" Title="Untitled Page" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="着ボイス設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" Runat="Server">
	<asp:Panel runat="server" ID="pnlMainte">
		<fieldset class="fieldset">
			<legend>[着ボイス設定]</legend>
			<asp:Panel runat="server" ID="pnlDtl">
					<table border="0" style="width: 640px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle">
								ｻｲﾄ／SEQ／ｷｬﾗｸﾀｰNo.
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblSiteCd" runat="server" Text="Label"></asp:Label>-
								<asp:Label ID="lblLoginId" runat="server" Text="Label"></asp:Label>-
								<asp:Label ID="lblUserSeq" runat="server" Text="Label" Visible="false"></asp:Label>
								<asp:Label ID="lblUserCharNo" runat="server" Text="Label"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								<%= DisplayWordUtil.Replace("出演者名.") %>
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblUserNm" runat="server" Text="Label"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								着ボイスタイトル
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtVoiceTitle" runat="server" MaxLength="50" Width="313px"></asp:TextBox>
								<asp:RequiredFieldValidator ID="vdrVoiceTitle" runat="server" ErrorMessage="着ボイスタイトルを入力して下さい。" ControlToValidate="txtVoiceTitle" ValidationGroup="Detail" Display="Dynamic">*</asp:RequiredFieldValidator>
								
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								消費ポイント
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtChargePoint" runat="server" MaxLength="3" Width="40px"></asp:TextBox>
								<asp:RequiredFieldValidator ID="vdrChargePoint" runat="server" ErrorMessage="消費ポイントを入力して下さい。" ControlToValidate="txtChargePoint" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle2">
								着ボイス&nbsp;ステータス
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstAuthType" runat="server" DataSourceID="dsAuthType" DataTextField="CODE_NM" DataValueField="CODE" Width="118px">
								</asp:DropDownList>
							</td>
						</tr>						
					</table>
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
					<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
					<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />					
			</asp:Panel>
		</fieldset>
	</asp:Panel>
	<asp:ObjectDataSource ID="dsAuthType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="54" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtChargePoint" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrVoiceTitle" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrChargePoint" HighlightCssClass="validatorCallout" />

</asp:Content>

