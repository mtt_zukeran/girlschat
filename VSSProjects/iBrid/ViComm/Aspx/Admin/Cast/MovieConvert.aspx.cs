﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 動画変換登録

--	Progaram ID		: MovieConvert
--
--  Creation Date	: 2010.07.01
--  Creater			: Kazuaki.Itoh
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------
  Date			Updater			Update Explain

-------------------------------------------------------------------------*/
using System;
using System.IO;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Cast_MovieConvert : System.Web.UI.Page {

	#region □ プロパティ(画面パラメータ) □ ==================================
	protected string ParamReturn {
		get { return (string)this.ViewState["return"]; }
		set { this.ViewState["return"] = value; }
	}
	protected string ParamSiteCd {
		get { return (string)this.ViewState["sitecd"]; }
		set { this.ViewState["sitecd"] = value; }
	}
	protected string ParamUserSeq {
		get { return (string)this.ViewState["userseq"]; }
		set { this.ViewState["userseq"] = value; }
	}
	protected string ParamUserCharNo {
		get { return (string)this.ViewState["usercharno"]; }
		set { this.ViewState["usercharno"] = value; }
	}
	protected string ParamMovieType {
		get { return (string)this.ViewState["movietype"]; }
		set { this.ViewState["movietype"] = value; }
	}
	#endregion ================================================================
	
	protected void Page_Load(object sender, EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			// 画面パラメータのパース
			this.ParsePageParameter();
			// 画面初期化
			this.InitPage();
		}
	}
	
	/// <summary>
	/// クリアﾎﾞﾀﾝ.Click
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnCancel_Click(object sender, EventArgs e) {
			if (GetMultiCharFlag(this.ParamSiteCd)) {
				Server.Transfer(string.Format("../Cast/CastView.aspx?loginid={0}&sitecd={1}&pmoviesite={1}&usercharno={2}&return={3}", lblLoginId.Text, this.ParamSiteCd, this.ParamUserCharNo, this.ParamReturn));
			} else {
				Server.Transfer(string.Format("../Cast/CastView.aspx?loginid={0}&pmoviesite={1}&usercharno={2}&return={3}", lblLoginId.Text, this.ParamSiteCd, this.ParamUserCharNo, this.ParamReturn));
			}
	}
	
	/// <summary>
	/// 最新の情報に更新ﾎﾞﾀﾝ.Click
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnRefresh_Click(object sender, EventArgs e) {
		Response.Redirect(Request.Url.PathAndQuery);
	}
	
	/// <summary>
	/// 登録ﾎﾞﾀﾝ.Command
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnConvert_Command(object sender, CommandEventArgs e) {

		string sFilePath = e.CommandArgument as string;
		
		string sAspectType = ViCommConst.MovieAspectType.NONE;
		if (this.rdoAspectNormal.Checked) {
			sAspectType = ViCommConst.MovieAspectType.NORMAL;
		} else if (this.rdoAspectWide.Checked) {
			sAspectType = ViCommConst.MovieAspectType.WIDE;
		}

		string sWebPhisicalDir = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(this.ParamSiteCd, "WEB_PHISICAL_DIR", ref sWebPhisicalDir);
		}

		string sDataDirPath = ViCommPrograms.GetCastPicDir(sWebPhisicalDir, this.ParamSiteCd, this.lblLoginId.Text);
		
		using (Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME, ViCommConst.FILE_UPLOAD_PASSWORD)) {
			if (!string.IsNullOrEmpty(sFilePath) && File.Exists(sFilePath)) {
				FileInfo oOriginalFile = new FileInfo(sFilePath);
				FileInfo oSampleMovieFile = this.GetSampleMovieFileInfo(oOriginalFile);
				
				decimal dMovieSeq = this.CreateMovieSeq();
				string sUploadFile = MovieConvertHelper.GetUploadFilePath(dMovieSeq);
				using(Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME,ViCommConst.FILE_UPLOAD_PASSWORD)){
					oOriginalFile.CopyTo(sUploadFile);
				}

				decimal? dSampleMovieSeq = null;
				if (oSampleMovieFile.Exists) {
					dSampleMovieSeq = this.CreateMovieSeq();
					string sSampleUploadFile = MovieConvertHelper.GetUploadFilePath(dSampleMovieSeq.Value);
					using(Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME,ViCommConst.FILE_UPLOAD_PASSWORD)){
						oSampleMovieFile.CopyTo(sSampleUploadFile);		
					}
				}

				decimal? dThumbnailPicSeq = null;
				if (chkCreateThumbnail.Checked) {
					dThumbnailPicSeq = dMovieSeq;
				}

				// ★ 変換処理				MovieConvertHelper.Convert(this.ParamSiteCd,this.ParamUserSeq,this.ParamUserCharNo,dMovieSeq,this.ParamMovieType,this.lblLoginId.Text,-1,-1,dSampleMovieSeq,sAspectType,dThumbnailPicSeq);

				// 成功した場合はオリジナルを削除する
				using(Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME,ViCommConst.FILE_UPLOAD_PASSWORD)){
					oOriginalFile.Delete();
					if (oSampleMovieFile.Exists) {
						oSampleMovieFile.Delete();
					}
				}

				// サムネイルははmovieseq+".jpg"でMOVIEディレクトリに作られるのでDATAディレクトリに移動(&加工)
				string sMovieDirPath = ViCommPrograms.GetCastMovieDir(sWebPhisicalDir,this.ParamSiteCd,this.lblLoginId.Text);
				string sThumbnailPicFileName = iBridUtil.addZero(dMovieSeq.ToString(),ViCommConst.OBJECT_NM_LENGTH)+ViCommConst.PIC_FOODER;

				using (Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME, ViCommConst.FILE_UPLOAD_PASSWORD)) {
					FileInfo oTmpThumbnailPic = new FileInfo(Path.Combine(sMovieDirPath, sThumbnailPicFileName));
					if(oTmpThumbnailPic.Exists){

						string sThumbnailPicFilePath = Path.Combine(sDataDirPath,sThumbnailPicFileName);
						using(Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME,ViCommConst.FILE_UPLOAD_PASSWORD)){
							oTmpThumbnailPic.CopyTo(sThumbnailPicFilePath);
						}
						ImageHelper.SetCopyRight(this.ParamSiteCd, sDataDirPath, sThumbnailPicFileName);
					}
				}
				
			}
		}

		Response.Redirect(Request.Url.PathAndQuery);
	}
	
	protected decimal CreateMovieSeq(){
		using (CastMovie objMovie = new CastMovie()) {
			// MOVIE_SEQを採番
			return objMovie.GetMovieNo();
		}
	}

	/// <summary>
	/// 画面引数の解析処理を行う
	/// </summary>
	private void ParsePageParameter() {
		this.ParamReturn = iBridUtil.GetStringValue(Request.QueryString["return"]);
		this.ParamSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		this.ParamUserSeq = iBridUtil.GetStringValue(Request.QueryString["userseq"]);
		this.ParamUserCharNo = iBridUtil.GetStringValue(Request.QueryString["usercharno"]);
		this.ParamMovieType = iBridUtil.GetStringValue(Request.QueryString["movietype"]);
	}

	/// <summary>
	/// ページを初期化する。
	/// </summary>
	private void InitPage() {
	
		this.lblSiteCd.Text = this.ParamSiteCd;
		this.lblUserSeq.Text = this.ParamUserSeq;
		this.lblUserCharNo.Text = this.ParamUserCharNo;

		this.lblHeader.Text = "ｻｲﾄ／SEQ.";
		this.lblUserCharNo.Visible = false;
		if (this.GetMultiCharFlag(lblSiteCd.Text)) {
			this.lblHeader.Text += "／ｷｬﾗｸﾀｰNo.";
			this.lblUserCharNo.Text = string.Format(" - {0}",lblUserCharNo.Text);
			this.lblUserCharNo.Visible = true;
		}

		using (Cast oCast = new Cast()) {
			string sName = string.Empty;
			string sId = string.Empty;

			oCast.GetValue(this.ParamUserSeq, "CAST_NM", ref sName);
			oCast.GetValue(this.ParamUserSeq, "LOGIN_ID", ref sId);

			this.lblCastNm.Text = sName;
			this.lblLoginId.Text = sId;
		}
		
		this.DisplayFileList();
	}

	
	private bool GetMultiCharFlag(string pSiteCd) {
		bool bMultiCharFlag = false;
		using (Site oSite = new Site()) {
			bMultiCharFlag = oSite.GetMultiCharFlag(pSiteCd);
		}
		return bMultiCharFlag;
	}
	
	private void DisplayFileList() {
		string sDirPath = this.GetMovieConvertTargetDir();
		
		using(Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME,ViCommConst.FILE_UPLOAD_PASSWORD)){
			DirectoryInfo oDirInfo = new DirectoryInfo(sDirPath);
			
			IList<FileInfo> oTargetFileInfoList = new List<FileInfo>();
			foreach(FileInfo oFileInfo in oDirInfo.GetFiles()){
				if(!oFileInfo.Name.ToLower().StartsWith(ViCommConst.PREFIX_SAMPLE_MOVIE)){
					oTargetFileInfoList.Add(oFileInfo);
				}
			}
			this.grdTargetFile.DataSource = oTargetFileInfoList;
			this.grdTargetFile.DataBind();	
		}	
	}
	
	/// <summary>
	/// 変換対象ファイルを格納するディレクトリの設定値を取得する
	/// </summary>
	/// <returns>変換対象ファイルを格納するディレクトリの設定値</returns>
	private string GetMovieConvertTargetDir(){
		string sDirPath = AdminConfigs.MovieConvertTargetDir;
		using(Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME,ViCommConst.FILE_UPLOAD_PASSWORD)){
			if (!Directory.Exists(sDirPath)) {
				string sErrorMessage = string.Format("「MovieConvertTargetDir」が設定されていないか、設定値に誤りがあります。(設定値:{0})", sDirPath);
				throw new InvalidOperationException(sErrorMessage);
			}
			return sDirPath;
		}
	}
	
	/// <summary>
	/// BをKBに変換する
	/// </summary>
	/// <param name="pByteLength"></param>
	/// <returns></returns>
	protected long Convert2KB(long pByteLength)	{
		long lResult = 0;
		long lOvermeasure;
		lResult =Math.DivRem(pByteLength,1024,out lOvermeasure);
		if(lOvermeasure>0)lResult+=1;
		return lResult;
	}
	
	protected bool HasSampleMovie(FileInfo pFileInfo){
		return GetSampleMovieFileInfo(pFileInfo).Exists;
	}
	protected FileInfo GetSampleMovieFileInfo(FileInfo pFileInfo){
		return new FileInfo(Path.Combine(pFileInfo.Directory.FullName,ViCommConst.PREFIX_SAMPLE_MOVIE+pFileInfo.Name));	
	}
}
