<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastCharacterMainte.aspx.cs" Inherits="Cast_CastCharacterMainte"
	Title="出演者キャラクター設定" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="出演者キャラクター設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>$NO_TRANS_START;
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>
							<%= DisplayWordUtil.Replace("[出演者キャラクター設定]") %>
						</legend>
						<table border="0" style="width: 700px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									ｻｲﾄ／SEQ
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblSiteCd" runat="server" Text="Label"></asp:Label>-
									<asp:Label ID="lblLoginId" runat="server" Text="Label"></asp:Label>
									<asp:Label ID="lblUserSeq" runat="server" Text="Label" Visible="false"></asp:Label>
									<asp:Label ID="lblRegist" runat="server" Text="新規登録"></asp:Label>
								</td>
							</tr>
							<tr runat="server" id="trUserCharNo">
								<td class="tdHeaderStyle">
									ｷｬﾗｸﾀｰ番号
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUserCharNo" runat="server" MaxLength="2" Width="35px"></asp:TextBox>
									<asp:CustomValidator ID="vdcUserCharNo" runat="server" ControlToValidate="txtUserCharNo" Display="Dynamic" ErrorMessage="入力したｷｬﾗｸﾀｰ番号は登録済みです。" ValidationGroup="Detail"
										OnServerValidate="vdcUserCharNo_ServerValidate">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrUserCharNo" runat="server" ErrorMessage="ｷｬﾗｸﾀｰ番号を入力してください。" ControlToValidate="txtUserCharNo" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeUserCharNo" runat="server" ErrorMessage="ｷｬﾀｸﾀｰ番号は00〜99の間で入力してください" ControlToValidate="txtUserCharNo" ValidationGroup="Detail"
										ValidationExpression="(\d\d)">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("出演者名.") %>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblCastNm" runat="server" Text="Label"></asp:Label>
								</td>
							</tr>
							<asp:PlaceHolder ID="plcActCategorySeq" runat="server" Visible="true">
							<tr>
								<td class="tdHeaderStyle">
									カテゴリ
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstActCategorySeq" runat="server" Width="206px" DataSourceID="dsActCategory" DataTextField="ACT_CATEGORY_NM" DataValueField="ACT_CATEGORY_SEQ">
									</asp:DropDownList>
									<asp:RequiredFieldValidator ID="vdrActCategorySeq" runat="server" ErrorMessage="カテゴリを選択して下さい。" ControlToValidate="lstActCategorySeq" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							</asp:PlaceHolder>
							<tr>
								<td class="tdHeaderStyle">
									ハンドルネーム
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtHandleNm" runat="server" MaxLength="20" Width="200px"></asp:TextBox>
									<asp:HiddenField ID="hdnPrevHandleNm" runat="server">
									</asp:HiddenField>
									<asp:RequiredFieldValidator ID="vdrHandleNm" runat="server" ErrorMessage="ハンドルネームを入力して下さい。" ControlToValidate="txtHandleNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:CustomValidator ID="vdcHandleNmDupli" runat="server" ControlToValidate="txtHandleNm" ErrorMessage="既に利用されているﾊﾝﾄﾞﾙ名です。" OnServerValidate="vdcHandleNm_ServerValidate"
										ValidationGroup="Detail" Display="Dynamic">*</asp:CustomValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ハンドルネーム(カナ)
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtHandleKanaNm" runat="server" MaxLength="20" Width="200px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									対応通話形態
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstConnectType" runat="server" Width="206px" DataSourceID="dsConnectType" DataTextField="CODE_NM" DataValueField="CODE">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									オンラインステータス
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstOnLineStatus" runat="server" Width="206px" DataSourceID="dsOnlineStatus" DataTextField="CODE_NM" DataValueField="CODE">
									</asp:DropDownList>
									最終動作日時:
									<asp:Label ID="lblLastActionDate" runat="server"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.ReplaceWordCommentList("一覧用コメント") %>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCommentList" runat="server" MaxLength="20" Width="497px" TextMode="MultiLine" Rows="5" Columns="60"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									特殊コメント
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCommentDetail" runat="server" Columns="60" MaxLength="20" Rows="5" TextMode="MultiLine" Width="497px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									待機コメント
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtWaitingComment" runat="server" Columns="60" MaxLength="20" Rows="5" TextMode="MultiLine" Width="497px"></asp:TextBox>
								</td>
							</tr>
							<tr runat="server" id="trCommentAdmin">
								<td class="tdHeaderStyle">
									運営者からのｺﾒﾝﾄ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCommentAdmin" runat="server" Columns="60" MaxLength="20" Rows="2" TextMode="MultiLine" Width="497px"></asp:TextBox>
								</td>
							</tr>
							<asp:PlaceHolder ID="plcPriority" runat="server" Visible="true">
							<tr>
								<td class="tdHeaderStyle">
									優先順位
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtPriority" runat="server" MaxLength="3" Width="35px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrPriority" runat="server" ErrorMessage="優先順位を入力して下さい。" ControlToValidate="txtPriority" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							</asp:PlaceHolder>
							<asp:PlaceHolder ID="plcStartPerformDay" runat="server" Visible="true">
							<tr>
								<td class="tdHeaderStyle">
									出演開始日
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtStartPerformDay" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrStartPerformDay" runat="server" ErrorMessage="出演開始日を入力して下さい。" ControlToValidate="txtStartPerformDay" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RangeValidator ID="vddStartPerformDay" runat="server" ErrorMessage="出演開始日を正しく入力して下さい。" ControlToValidate="txtStartPerformDay" MaximumValue="2099/12/31"
										MinimumValue="1990/01/01" Type="Date" ValidationGroup="Detail">*</asp:RangeValidator>
								</td>
							</tr>
							</asp:PlaceHolder>
							<asp:PlaceHolder ID="plcBirthDay" runat="server">
								<tr>
									<td class="tdHeaderStyle">
										生年月日
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtBirthDay" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
										<asp:RangeValidator ID="vddBirthDay" runat="server" ErrorMessage="生年月日を正しく入力して下さい。" ControlToValidate="txtBirthDay" MaximumValue="2099/12/31" MinimumValue="1940/01/01"
											Type="Date" ValidationGroup="Detail" Display="Dynamic">*</asp:RangeValidator>
										<asp:RegularExpressionValidator ID="vdeBirthDay" runat="server" ErrorMessage="生年月日を正しく入力して下さい。" ControlToValidate="txtBirthDay" ValidationExpression="\d{4}/\d{2}/\d{2}"
											ValidationGroup="Detail" Display="Dynamic">*</asp:RegularExpressionValidator>
										<asp:CustomValidator ID="vdcBirthDay" runat="server" ControlToValidate="txtBirthDay" ErrorMessage="10歳未満です。" OnServerValidate="vdcBirthday_ServerValidate"
											ValidationGroup="Detail" Display="Dynamic">*</asp:CustomValidator>
									</td>
								</tr>
							</asp:PlaceHolder>
							<asp:PlaceHolder ID="plcOfflineDisplayFlag" runat="server" Visible="true">
							<tr>
								<td class="tdHeaderStyle">
									ｵﾌﾗｲﾝﾋﾟｯｸｱｯﾌﾟ対象者
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkOfflineDisplayFlag" runat="server" />
								</td>
							</tr>
							</asp:PlaceHolder>
							<tr>
								<td class="tdHeaderStyle">
									ログイン通知メール<br />
									(気に入られている)
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstLoginMailTemplateNo" runat="server" Width="309px" DataSourceID="dsLoginMail" DataTextField="TEMPLATE_NM" DataValueField="MAIL_TEMPLATE_NO"
										OnDataBound="lst_DataBound">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ログイン通知メール<br />
									(気にいっている)
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstLoginMailTemplateNo2" runat="server" Width="309px" DataSourceID="dsLoginMail" DataTextField="TEMPLATE_NM" DataValueField="MAIL_TEMPLATE_NO"
										OnDataBound="lst_DataBound">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ログアウト通知メール<br />
									標準テンプレート
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstLogoutMailTemplateNo" runat="server" Width="309px" DataSourceID="dsLogoutMail" DataTextField="TEMPLATE_NM" DataValueField="MAIL_TEMPLATE_NO"
										OnDataBound="lst_DataBound">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("男性メール受信区分") %>
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstManMailRxType" runat="server" Width="206px" DataSourceID="dsMailRxType" DataTextField="CODE_NM" DataValueField="CODE">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									お知らせメール受信区分
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstInfoMailRxType" runat="server" Width="206px" DataSourceID="dsInfoMailRxType" DataTextField="CODE_NM" DataValueField="CODE">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									携帯ﾒｰﾙ受信時間帯
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstMobileMailRxStartTime" runat="server" Width="60px" DataSourceID="dsTime" DataTextField="CODE_NM" DataValueField="CODE_NM">
									</asp:DropDownList>〜
									<asp:DropDownList ID="lstMobileMailRxEndTime" runat="server" Width="60px" DataSourceID="dsTime" DataTextField="CODE_NM" DataValueField="CODE_NM">
									</asp:DropDownList>
								</td>
							</tr>
							<asp:PlaceHolder ID="plcAdCd" runat="server" Visible="true">
							<tr>
								<td class="tdHeaderStyle">
									広告コード
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtAdCd" runat="server" MaxLength="32" Width="200px"></asp:TextBox>
									<asp:RegularExpressionValidator ID="vdeAdCd" runat="server" ErrorMessage="広告コードは32桁以下の英数字で入力して下さい。" ValidationExpression="[a-zA-Z0-9-\/_]{1,32}" ControlToValidate="txtAdCd"
										ValidationGroup="Detail" Display="Dynamic">*</asp:RegularExpressionValidator>
									<asp:CustomValidator ID="vdcAdCd" runat="server" ControlToValidate="txtAdCd" ErrorMessage="広告コードが未登録です。" OnServerValidate="vdcAdCd_ServerValidate" ValidationGroup="Detail"
										Display="Dynamic">*</asp:CustomValidator>
									<asp:Label ID="lblAdNm" runat="server"></asp:Label>
								</td>
							</tr>
							</asp:PlaceHolder>
							<tr>
								<td class="tdHeaderStyle">
									ｷｬﾗｸﾀｰ状態
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstNaFlag" runat="server" Width="206px" DataSourceID="dsNaFlag" DataTextField="CODE_NM" DataValueField="CODE">
									</asp:DropDownList>
								</td>
							</tr>
							<asp:PlaceHolder ID="plcFriendIntroCd" runat="server" Visible="true">
							<tr>
								<td class="tdHeaderStyle">
									紹介者コード
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblFriendIntroCd" runat="server" Text=""></asp:Label>
								</td>
							</tr>
							</asp:PlaceHolder>
							<asp:PlaceHolder ID="plcIntroducerFriendCd" runat="server" Visible="true">
							<tr>
								<td class="tdHeaderStyle">
									紹介元コード
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblIntroducerFriendCd" runat="server"></asp:Label>
								</td>
							</tr>
							</asp:PlaceHolder>
							<tr>
								<td class="tdHeaderStyle">
									ﾒｰﾙ一括送信利用不可
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkCastToManBatchMailNaFlag" runat="server">
									</asp:CheckBox>メール一括送信機能を利用させない
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("ｷｬｽﾄ") %>
									から<%= DisplayWordUtil.Replace("男性") %>への<br />
									ﾒｰﾙ一括送信上限数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCastToManBatchMailLimit" runat="server" Width="50px" MaxLength="6"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrCastToManBatchMailLimit" runat="server" ErrorMessage="ｷｬｽﾄから男性へのﾒｰﾙ一括送信上限数を入力して下さい。" ControlToValidate="txtCastToManBatchMailLimit"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									一括ﾒｰﾙ送信済み数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtBatchMailCount" runat="server" Width="50px" MaxLength="6"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrBatchMailCount" runat="server" ErrorMessage="一括ﾒｰﾙの送信済み数を入力して下さい。" ControlToValidate="txtBatchMailCount" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RangeValidator ID="vdeBatchMailCount" runat="server" ErrorMessage="1" ControlToValidate="txtBatchMailCount" MaximumValue="1" MinimumValue="0" Type="Integer"
										ValidationGroup="Detail" Display="Dynamic">*</asp:RangeValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("ｷｬｽﾄ") %>
									から<%= DisplayWordUtil.Replace("男性") %>への<br />
									招待ﾒｰﾙ送信上限数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtInviteMailLimit" runat="server" Width="50px" MaxLength="6"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrInviteMailLimit" runat="server" ErrorMessage="ｷｬｽﾄから男性への会話招待ﾒｰﾙ送信上限数を入力して下さい。" ControlToValidate="txtInviteMailLimit"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									招待ﾒｰﾙ送信済み数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtInviteMailCount" runat="server" Width="50px" MaxLength="6"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrInviteMailCount" runat="server" ErrorMessage="会話招待ﾒｰﾙの送信済み数を入力して下さい。" ControlToValidate="txtInviteMailCount" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RangeValidator ID="vdeInviteMailCount" runat="server" ErrorMessage="1" ControlToValidate="txtInviteMailCount" MaximumValue="1" MinimumValue="0" Type="Integer"
										ValidationGroup="Detail" Display="Dynamic">*</asp:RangeValidator>
								</td>
							</tr>
							<asp:PlaceHolder ID="plcUserDefineFlag" runat="server" Visible="true">
							<tr>
								<td class="tdHeaderStyle2">
									ﾕｰｻﾞｰ定義ﾌﾗｸﾞ
								</td>
								<td class="tdDataStyle">
									<asp:CheckBoxList ID="chkUserDefineFlag" runat="server" DataSourceID="dsUserDefineFlag" DataTextField="CODE_NM" DataValueField="CODE" RepeatDirection="Horizontal"
										RepeatLayout="Flow" RepeatColumns="5">
									</asp:CheckBoxList>
								</td>
							</tr>
							</asp:PlaceHolder>
						</table>
						<asp:PlaceHolder ID="plcOkList" runat="server">「<%= DisplayWordUtil.ReplaceOkList("ＯＫリスト")%>」
							<table border="0" style="width: 640px" class="tableStyle">
								<tr>
									<td>
										<asp:CheckBox ID="chkOKFlag0" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag1" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag2" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag3" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag4" runat="server" />
									</td>
								</tr>
								<tr>
									<td>
										<asp:CheckBox ID="chkOKFlag5" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag6" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag7" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag8" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag9" runat="server" />
									</td>
								</tr>
								<tr>
									<td>
										<asp:CheckBox ID="chkOKFlag10" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag11" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag12" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag13" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag14" runat="server" />
									</td>
								</tr>
								<tr>
									<td>
										<asp:CheckBox ID="chkOKFlag15" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag16" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag17" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag18" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag19" runat="server" />
									</td>
								</tr>
								<tr>
									<td>
										<asp:CheckBox ID="chkOKFlag20" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag21" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag22" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag23" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag24" runat="server" />
									</td>
								</tr>
								<tr>
									<td>
										<asp:CheckBox ID="chkOKFlag25" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag26" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag27" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag28" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag29" runat="server" />
									</td>
								</tr>
								<tr>
									<td>
										<asp:CheckBox ID="chkOKFlag30" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag31" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag32" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag33" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag34" runat="server" />
									</td>
								</tr>
								<tr>
									<td>
										<asp:CheckBox ID="chkOKFlag35" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag36" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag37" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag38" runat="server" />
									</td>
									<td>
										<asp:CheckBox ID="chkOKFlag39" runat="server" />
									</td>
								</tr>
							</table>
						</asp:PlaceHolder>
						｢プロフィール｣<br />
						<asp:PlaceHolder ID="plcHolder" runat="server">
							<asp:Table ID="Table1" runat="server" CssClass="tableStyle" Width="640px">
								<asp:TableRow ID="rowAttr0" runat="server">
									<asp:TableCell ID="celCastAttrNm0" runat="server" CssClass="tdHeaderStyle">
										<asp:Label ID="lblCastAttrNm0" runat="server" Text="Label"></asp:Label>
									</asp:TableCell>
									<asp:TableCell ID="celCastAttrSeq0" runat="server" CssClass="tdDataStyle">
										<asp:DropDownList ID="lstCastAttrSeq0" runat="server" Width="206px">
										</asp:DropDownList>
										<asp:TextBox ID="txtAttrInputValue0" runat="server" Width="200px" MaxLength="256"></asp:TextBox>
									</asp:TableCell>
								</asp:TableRow>
								<asp:TableRow ID="rowAttr1" runat="server">
									<asp:TableCell ID="celCastAttrNm1" runat="server" CssClass="tdHeaderStyle">
										<asp:Label ID="lblCastAttrNm1" runat="server" Text="Label"></asp:Label>
									</asp:TableCell>
									<asp:TableCell ID="celCastAttrSeq1" runat="server" CssClass="tdDataStyle">
										<asp:DropDownList ID="lstCastAttrSeq1" runat="server" Width="206px">
										</asp:DropDownList>
										<asp:TextBox ID="txtAttrInputValue1" runat="server" Width="200px" MaxLength="256"></asp:TextBox>
									</asp:TableCell>
								</asp:TableRow>
								<asp:TableRow ID="rowAttr2" runat="server">
									<asp:TableCell ID="celCastAttrNm2" runat="server" CssClass="tdHeaderStyle">
										<asp:Label ID="lblCastAttrNm2" runat="server" Text="Label"></asp:Label>
									</asp:TableCell>
									<asp:TableCell ID="celCastAttrSeq2" runat="server" CssClass="tdDataStyle">
										<asp:DropDownList ID="lstCastAttrSeq2" runat="server" Width="206px">
										</asp:DropDownList>
										<asp:TextBox ID="txtAttrInputValue2" runat="server" Width="200px" MaxLength="256"></asp:TextBox>
									</asp:TableCell>
								</asp:TableRow>
								<asp:TableRow ID="rowAttr3" runat="server">
									<asp:TableCell ID="celCastAttrNm3" runat="server" CssClass="tdHeaderStyle">
										<asp:Label ID="lblCastAttrNm3" runat="server" Text="Label"></asp:Label>
									</asp:TableCell>
									<asp:TableCell ID="celCastAttrSeq3" runat="server" CssClass="tdDataStyle">
										<asp:DropDownList ID="lstCastAttrSeq3" runat="server" Width="206px">
										</asp:DropDownList>
										<asp:TextBox ID="txtAttrInputValue3" runat="server" Width="200px" MaxLength="256"></asp:TextBox>
									</asp:TableCell>
								</asp:TableRow>
								<asp:TableRow ID="rowAttr4" runat="server">
									<asp:TableCell ID="celCastAttrNm4" runat="server" CssClass="tdHeaderStyle">
										<asp:Label ID="lblCastAttrNm4" runat="server" Text="Label"></asp:Label>
									</asp:TableCell>
									<asp:TableCell ID="celCastAttrSeq4" runat="server" CssClass="tdDataStyle">
										<asp:DropDownList ID="lstCastAttrSeq4" runat="server" Width="206px">
										</asp:DropDownList>
										<asp:TextBox ID="txtAttrInputValue4" runat="server" Width="200px" MaxLength="256"></asp:TextBox>
									</asp:TableCell>
								</asp:TableRow>
								<asp:TableRow ID="rowAttr5" runat="server">
									<asp:TableCell ID="celCastAttrNm5" runat="server" CssClass="tdHeaderStyle">
										<asp:Label ID="lblCastAttrNm5" runat="server" Text="Label"></asp:Label>
									</asp:TableCell>
									<asp:TableCell ID="celCastAttrSeq5" runat="server" CssClass="tdDataStyle">
										<asp:DropDownList ID="lstCastAttrSeq5" runat="server" Width="206px">
										</asp:DropDownList>
										<asp:TextBox ID="txtAttrInputValue5" runat="server" Width="200px" MaxLength="256"></asp:TextBox>
									</asp:TableCell>
								</asp:TableRow>
								<asp:TableRow ID="rowAttr6" runat="server">
									<asp:TableCell ID="celCastAttrNm6" runat="server" CssClass="tdHeaderStyle">
										<asp:Label ID="lblCastAttrNm6" runat="server" Text="Label"></asp:Label>
									</asp:TableCell>
									<asp:TableCell ID="celCastAttrSeq6" runat="server" CssClass="tdDataStyle">
										<asp:DropDownList ID="lstCastAttrSeq6" runat="server" Width="206px">
										</asp:DropDownList>
										<asp:TextBox ID="txtAttrInputValue6" runat="server" Width="200px" MaxLength="256"></asp:TextBox>
									</asp:TableCell>
								</asp:TableRow>
								<asp:TableRow ID="rowAttr7" runat="server">
									<asp:TableCell ID="celCastAttrNm7" runat="server" CssClass="tdHeaderStyle">
										<asp:Label ID="lblCastAttrNm7" runat="server" Text="Label"></asp:Label>
									</asp:TableCell>
									<asp:TableCell ID="celCastAttrSeq7" runat="server" CssClass="tdDataStyle">
										<asp:DropDownList ID="lstCastAttrSeq7" runat="server" Width="206px">
										</asp:DropDownList>
										<asp:TextBox ID="txtAttrInputValue7" runat="server" Width="200px" MaxLength="256"></asp:TextBox>
									</asp:TableCell>
								</asp:TableRow>
								<asp:TableRow ID="rowAttr8" runat="server">
									<asp:TableCell ID="celCastAttrNm8" runat="server" CssClass="tdHeaderStyle">
										<asp:Label ID="lblCastAttrNm8" runat="server" Text="Label"></asp:Label>
									</asp:TableCell>
									<asp:TableCell ID="celCastAttrSeq8" runat="server" CssClass="tdDataStyle">
										<asp:DropDownList ID="lstCastAttrSeq8" runat="server" Width="206px">
										</asp:DropDownList>
										<asp:TextBox ID="txtAttrInputValue8" runat="server" Width="200px" MaxLength="256"></asp:TextBox>
									</asp:TableCell>
								</asp:TableRow>
								<asp:TableRow ID="rowAttr9" runat="server">
									<asp:TableCell ID="celCastAttrNm9" runat="server" CssClass="tdHeaderStyle">
										<asp:Label ID="lblCastAttrNm9" runat="server" Text="Label"></asp:Label>
									</asp:TableCell>
									<asp:TableCell ID="celCastAttrSeq9" runat="server" CssClass="tdDataStyle">
										<asp:DropDownList ID="lstCastAttrSeq9" runat="server" Width="206px">
										</asp:DropDownList>
										<asp:TextBox ID="txtAttrInputValue9" runat="server" Width="200px" MaxLength="256"></asp:TextBox>
									</asp:TableCell>
								</asp:TableRow>
								<asp:TableRow ID="rowAttr10" runat="server">
									<asp:TableCell ID="celCastAttrNm10" runat="server" CssClass="tdHeaderStyle">
										<asp:Label ID="lblCastAttrNm10" runat="server" Text="Label"></asp:Label>
									</asp:TableCell>
									<asp:TableCell ID="celCastAttrSeq10" runat="server" CssClass="tdDataStyle">
										<asp:DropDownList ID="lstCastAttrSeq10" runat="server" Width="206px">
										</asp:DropDownList>
										<asp:TextBox ID="txtAttrInputValue10" runat="server" Width="200px" MaxLength="256"></asp:TextBox>
									</asp:TableCell>
								</asp:TableRow>
								<asp:TableRow ID="rowAttr11" runat="server">
									<asp:TableCell ID="celCastAttrNm11" runat="server" CssClass="tdHeaderStyle">
										<asp:Label ID="lblCastAttrNm11" runat="server" Text="Label"></asp:Label>
									</asp:TableCell>
									<asp:TableCell ID="celCastAttrSeq11" runat="server" CssClass="tdDataStyle">
										<asp:DropDownList ID="lstCastAttrSeq11" runat="server" Width="206px">
										</asp:DropDownList>
										<asp:TextBox ID="txtAttrInputValue11" runat="server" Width="200px" MaxLength="256"></asp:TextBox>
									</asp:TableCell>
								</asp:TableRow>
								<asp:TableRow ID="rowAttr12" runat="server">
									<asp:TableCell ID="celCastAttrNm12" runat="server" CssClass="tdHeaderStyle">
										<asp:Label ID="lblCastAttrNm12" runat="server" Text="Label"></asp:Label>
									</asp:TableCell>
									<asp:TableCell ID="celCastAttrSeq12" runat="server" CssClass="tdDataStyle">
										<asp:DropDownList ID="lstCastAttrSeq12" runat="server" Width="206px">
										</asp:DropDownList>
										<asp:TextBox ID="txtAttrInputValue12" runat="server" Width="200px" MaxLength="256"></asp:TextBox>
									</asp:TableCell>
								</asp:TableRow>
								<asp:TableRow ID="rowAttr13" runat="server">
									<asp:TableCell ID="celCastAttrNm13" runat="server" CssClass="tdHeaderStyle">
										<asp:Label ID="lblCastAttrNm13" runat="server" Text="Label"></asp:Label>
									</asp:TableCell>
									<asp:TableCell ID="celCastAttrSeq13" runat="server" CssClass="tdDataStyle">
										<asp:DropDownList ID="lstCastAttrSeq13" runat="server" Width="206px">
										</asp:DropDownList>
										<asp:TextBox ID="txtAttrInputValue13" runat="server" Width="200px" MaxLength="256"></asp:TextBox>
									</asp:TableCell>
								</asp:TableRow>
								<asp:TableRow ID="rowAttr14" runat="server">
									<asp:TableCell ID="celCastAttrNm14" runat="server" CssClass="tdHeaderStyle">
										<asp:Label ID="lblCastAttrNm14" runat="server" Text="Label"></asp:Label>
									</asp:TableCell>
									<asp:TableCell ID="celCastAttrSeq14" runat="server" CssClass="tdDataStyle">
										<asp:DropDownList ID="lstCastAttrSeq14" runat="server" Width="206px">
										</asp:DropDownList>
										<asp:TextBox ID="txtAttrInputValue14" runat="server" Width="200px" MaxLength="256"></asp:TextBox>
									</asp:TableCell>
								</asp:TableRow>
								<asp:TableRow ID="rowAttr15" runat="server">
									<asp:TableCell ID="celCastAttrNm15" runat="server" CssClass="tdHeaderStyle">
										<asp:Label ID="lblCastAttrNm15" runat="server" Text="Label"></asp:Label>
									</asp:TableCell>
									<asp:TableCell ID="celCastAttrSeq15" runat="server" CssClass="tdDataStyle">
										<asp:DropDownList ID="lstCastAttrSeq15" runat="server" Width="206px">
										</asp:DropDownList>
										<asp:TextBox ID="txtAttrInputValue15" runat="server" Width="200px" MaxLength="256"></asp:TextBox>
									</asp:TableCell>
								</asp:TableRow>
								<asp:TableRow ID="rowAttr16" runat="server">
									<asp:TableCell ID="celCastAttrNm16" runat="server" CssClass="tdHeaderStyle">
										<asp:Label ID="lblCastAttrNm16" runat="server" Text="Label"></asp:Label>
									</asp:TableCell>
									<asp:TableCell ID="celCastAttrSeq16" runat="server" CssClass="tdDataStyle">
										<asp:DropDownList ID="lstCastAttrSeq16" runat="server" Width="206px">
										</asp:DropDownList>
										<asp:TextBox ID="txtAttrInputValue16" runat="server" Width="200px" MaxLength="256"></asp:TextBox>
									</asp:TableCell>
								</asp:TableRow>
								<asp:TableRow ID="rowAttr17" runat="server">
									<asp:TableCell ID="celCastAttrNm17" runat="server" CssClass="tdHeaderStyle">
										<asp:Label ID="lblCastAttrNm17" runat="server" Text="Label"></asp:Label>
									</asp:TableCell>
									<asp:TableCell ID="celCastAttrSeq17" runat="server" CssClass="tdDataStyle">
										<asp:DropDownList ID="lstCastAttrSeq17" runat="server" Width="206px">
										</asp:DropDownList>
										<asp:TextBox ID="txtAttrInputValue17" runat="server" Width="200px" MaxLength="256"></asp:TextBox>
									</asp:TableCell>
								</asp:TableRow>
								<asp:TableRow ID="rowAttr18" runat="server">
									<asp:TableCell ID="celCastAttrNm18" runat="server" CssClass="tdHeaderStyle">
										<asp:Label ID="lblCastAttrNm18" runat="server" Text="Label"></asp:Label>
									</asp:TableCell>
									<asp:TableCell ID="celCastAttrSeq18" runat="server" CssClass="tdDataStyle">
										<asp:DropDownList ID="lstCastAttrSeq18" runat="server" Width="206px">
										</asp:DropDownList>
										<asp:TextBox ID="txtAttrInputValue18" runat="server" Width="200px" MaxLength="256"></asp:TextBox>
									</asp:TableCell>
								</asp:TableRow>
								<asp:TableRow ID="rowAttr19" runat="server">
									<asp:TableCell ID="celCastAttrNm19" runat="server" CssClass="tdHeaderStyle">
										<asp:Label ID="lblCastAttrNm19" runat="server" Text="Label"></asp:Label>
									</asp:TableCell>
									<asp:TableCell ID="celCastAttrSeq19" runat="server" CssClass="tdDataStyle">
										<asp:DropDownList ID="lstCastAttrSeq19" runat="server" Width="206px">
										</asp:DropDownList>
										<asp:TextBox ID="txtAttrInputValue19" runat="server" Width="200px" MaxLength="256"></asp:TextBox>
									</asp:TableCell>
								</asp:TableRow>
							</asp:Table>
						</asp:PlaceHolder>
						<asp:Panel ID="pnlCharacterEx" runat="server">
							「キャラクター拡張」
							<table border="0" style="width: 640px" class="tableStyle">
								<tr>
									<td class="tdHeaderStyle">
										ﾌﾞﾛｸﾞ開設許可
									</td>
									<td class="tdDataStyle">
										<asp:CheckBox ID="chkEnabledBlog" runat="server" />
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										ﾘｯﾁｰﾉ閲覧許可
									</td>
									<td class="tdDataStyle">
										<asp:CheckBox ID="chkEnabledRichino" runat="server" />
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle2">
										ﾘｯﾁｰﾉ閲覧自動更新不可
									</td>
									<td class="tdDataStyle">
										<asp:CheckBox ID="chkNoUpdateRichiNoViewFlag" runat="server" Text="報酬条件によるﾘｯﾁｰﾉ閲覧許可の自動更新を実行しない" />
									</td>
								</tr>
							</table>
						</asp:Panel>
						<table border="0" style="width: 640px">
							<tr>
								<td>
									<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
									<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
								</td>
								<td>
									<!--<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekrightbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />-->
								</td>
							</tr>
						</table>
					</fieldset>
				</asp:Panel>
				$NO_TRANS_END;
			</fieldset>
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsActCategory" runat="server" SelectMethod="GetList" TypeName="ActCategory">
		<SelectParameters>
			<asp:QueryStringParameter Name="pSiteCd" QueryStringField="sitecd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsLoginMail" runat="server" SelectMethod="GetListByTemplateType" TypeName="MailTemplate">
		<SelectParameters>
			<asp:QueryStringParameter Name="pSiteCd" QueryStringField="sitecd" Type="String" />
			<asp:Parameter DefaultValue="08" Name="pMailTemplateType" Type="String" />
			<asp:Parameter DefaultValue="1" Name="pSexCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsLogoutMail" runat="server" SelectMethod="GetListByTemplateType" TypeName="MailTemplate">
		<SelectParameters>
			<asp:QueryStringParameter Name="pSiteCd" QueryStringField="sitecd" Type="String" />
			<asp:Parameter DefaultValue="16" Name="pMailTemplateType" Type="String" />
			<asp:Parameter DefaultValue="1" Name="pSexCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsTime" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="68" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsPrefecture" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="67" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsConnectType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="61" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsNaFlag" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="98" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsMailRxType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="66" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsInfoMailRxType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="15" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsOnlineStatus" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="52" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUserDefineFlag" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="A1" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPriority" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtUserCharNo" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdcUserCharNo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vdrUserCharNo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8" TargetControlID="vdeUserCharNo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdrActCategorySeq" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrHandleNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrPriority" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrStartPerformDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vddStartPerformDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10" TargetControlID="vddBirthDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11" TargetControlID="vdeBirthDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender12" TargetControlID="vdcBirthDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender13" TargetControlID="vdeAdcd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender15" TargetControlID="vdrCastToManBatchMailLimit" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender17" TargetControlID="vdrBatchMailCount" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender18" TargetControlID="vdeBatchMailCount" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender19" TargetControlID="vdrInviteMailLimit" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender21" TargetControlID="vdrInviteMailCount" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender22" TargetControlID="vdeInviteMailCount" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender14" TargetControlID="vdcHandleNmDupli" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:MaskedEditExtender ID="mskStartPerformDay" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtStartPerformDay">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskBirthDay" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtBirthDay">
	</ajaxToolkit:MaskedEditExtender>
</asp:Content>
