﻿<%@ Page AutoEventWireup="true" CodeFile="ApplyAgentMainte.aspx.cs" Inherits="Cast_ApplyAgentMainte"
    Language="C#" MasterPageFile="~/MasterPage.master" Title="エージェントID登録" %>

<asp:Content ID="ContentTitle" runat="Server" ContentPlaceHolderID="HolderContentTitle">
    <asp:Label ID="lblPgmTitle" runat="server" Text="エージェントID登録"></asp:Label>
</asp:Content>
<asp:Content ID="Content" runat="Server" ContentPlaceHolderID="HolderContent">
    <div class="admincontent">
        <asp:Panel ID="pnlMainte" runat="server">
            <fieldset class="fieldset">
                <legend>
                    <%= DisplayWordUtil.Replace("[出演者情報]") %>
                </legend>
                <asp:Panel ID="pnlDtl" runat="server">
                    <table border="0">
                        <tr style="margin-bottom: 0px">
                            <td valign="top">
                                <table border="0" class="tableStyle" style="width: 540px">
                                    <tr>
                                        <td class="tdHeaderStyle">
                                            <%= DisplayWordUtil.Replace("出演者名(全角)") %>
                                        </td>
                                        <td class="tdDataStyle">
                                            <asp:TextBox ID="txtCastNm" runat="server" MaxLength="50" Width="200px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="vdrCastNm" runat="server" ControlToValidate="txtCastNm"
                                                ErrorMessage="出演者名(全角)を入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="vdeCastNm" runat="server" ControlToValidate="txtCastNm"
                                                Display="Dynamic" ErrorMessage="出演者名(全角)を正しく入力して下さい。 (姓(全角)+全角スペース+名(全角))" ValidationExpression="^[^ -~｡-ﾟ]+　[^ -~｡-ﾟ]+$"
                                                ValidationGroup="Detail">*</asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tdHeaderStyle">
                                            <%= DisplayWordUtil.Replace("出演者名(カナ)") %>
                                        </td>
                                        <td class="tdDataStyle">
                                            <asp:TextBox ID="txtCastKanaNm" runat="server" MaxLength="50" Width="200px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="vdrCastKanaNm" runat="server" ControlToValidate="txtCastKanaNm"
                                                ErrorMessage="出演者名(カナ)を入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="vdeCastKanaNm" runat="server" ControlToValidate="txtCastKanaNm"
                                                Display="Dynamic" ErrorMessage="出演者名(カナ)を正しく入力して下さい。 (姓(全角カナ)+全角スペース+名(全角カナ))" ValidationExpression="^[ァ-ヴー]+　[ァ-ヴー]+$"
                                                ValidationGroup="Detail">*</asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tdHeaderStyle">
                                            生年月日
                                        </td>
                                        <td class="tdDataStyle">
                                            <asp:TextBox ID="txtBirthDay" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="vdrBirthDay" runat="server" ControlToValidate="txtBirthDay"
                                                ErrorMessage="生年月日を入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
                                            <asp:RangeValidator ID="vddBirthDay" runat="server" ControlToValidate="txtBirthDay"
                                                Display="Dynamic" ErrorMessage="生年月日を正しく入力して下さい。" MaximumValue="2099/12/31" MinimumValue="1940/01/01"
                                                Type="Date" ValidationGroup="Detail">*</asp:RangeValidator>
                                            <asp:RegularExpressionValidator ID="vdeBirthDay" runat="server" ControlToValidate="txtBirthDay"
                                                Display="Dynamic" ErrorMessage="生年月日を正しく入力して下さい。" ValidationExpression="\d{4}/\d{2}/\d{2}"
                                                ValidationGroup="Detail">*</asp:RegularExpressionValidator>
                                            <asp:CustomValidator ID="vdcBirthDay" runat="server" ControlToValidate="txtBirthDay"
                                                Display="Dynamic" ErrorMessage="18歳未満です。" OnServerValidate="vdcBirthday_ServerValidate"
                                                ValidationGroup="Detail">*</asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tdHeaderStyle">
                                            ログインパスワード
                                        </td>
                                        <td class="tdDataStyle">
                                            <asp:TextBox ID="txtLoginPassword" runat="server" MaxLength="4" Width="80px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="vdrLoginPassword" runat="server" ControlToValidate="txtLoginPassword"
                                                ErrorMessage="ログインパスワードを入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="vdeLoginPassword" runat="server" ControlToValidate="txtLoginPassword"
                                                ErrorMessage="ログインパスワードは4桁の数字で入力して下さい。" ValidationExpression="\d{4}" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tdHeaderStyle">
                                            メールアドレス
                                        </td>
                                        <td class="tdDataStyle">
                                            <asp:TextBox ID="txtEmailAddr" runat="server" MaxLength="60" Width="200px"></asp:TextBox>
                                            <%--<asp:CustomValidator ID="vdcEmailAddr" runat="server" ControlToValidate="txtEmailAddr"
                                                Display="Dynamic" ErrorMessage="既に利用されているﾒｰﾙｱﾄﾞﾚｽです。" OnServerValidate="vdcEmailAddr_ServerValidate"
                                                ValidationGroup="Detail">*</asp:CustomValidator>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tdHeaderStyle">
                                            <%= DisplayWordUtil.Replace("出演者電話番号") %>
                                        </td>
                                        <td class="tdDataStyle">
                                            <asp:TextBox ID="txtTel" runat="server" MaxLength="11" Width="80px"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="vdeTel" runat="server" ControlToValidate="txtTel"
                                                ErrorMessage="電話番号を正しく入力して下さい。" ValidationExpression="(0\d{9,10})" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
                                            <%--<asp:CustomValidator ID="vdcTel" runat="server" ControlToValidate="txtTel" Display="Dynamic"
                                                ErrorMessage="既に利用されている電話番号です。" OnServerValidate="vdcTel_ServerValidate" ValidationGroup="Detail">*</asp:CustomValidator>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tdHeaderStyle">
                                            プロダクション
                                        </td>
                                        <td class="tdDataStyle">
                                            <asp:DropDownList ID="lstProductionSeq" runat="server" DataSourceID="dsProduction"
                                                Enabled="false" OnDataBound="lst_DataBound" DataTextField="PRODUCTION_NM" DataValueField="PRODUCTION_SEQ"
                                                Width="206px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tdHeaderStyle">
                                            担当
                                        </td>
                                        <td class="tdDataStyle">
                                            <asp:DropDownList ID="lstManagerSeq" runat="server" DataSourceID="dsManager" DataTextField="MANAGER_NM"
                                                Enabled="false" OnDataBound="lst_DataBound" DataValueField="MANAGER_SEQ" Width="206px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tdHeaderStyle2">
                                            備考
                                        </td>
                                        <td class="tdDataStyle">
                                            <asp:TextBox ID="txtRemarks" runat="server" Height="48px" MaxLength="1000" TextMode="MultiLine"
                                                Width="360px "></asp:TextBox><br />
                                        </td>
                                    </tr>
                                </table>
                                <asp:Panel ID="pnlRegist" runat="server">
                                    <asp:Button ID="btnUpdate" runat="server" CssClass="seekbutton" OnClick="btnUpdate_Click"
                                        Text="申請" ValidationGroup="Detail" />
                                    <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="seekrightbutton"
                                        OnClick="btnCancel_Click" Text="キャンセル" /><br />
                                </asp:Panel>
                                <asp:Panel ID="pnlCertify" runat="server">
                                    <asp:Button ID="btnCertify" runat="server" CssClass="seekbutton" OnClick="btnCertify_Click"
                                        Text="認証" ValidationGroup="Detail" />
                                    <asp:Button ID="btnCertifyUpdate" runat="server" CssClass="seekbutton" OnClick="btnUpdate_Click"
                                        Text="更新" ValidationGroup="Detail" />
                                    <asp:Button ID="btnCertifyDelete" runat="server" CssClass="seekrightbutton" OnClick="btnDelete_Click"
                                        Text="削除" />
                                    <asp:Button ID="btnCertifyCancel" runat="server" CausesValidation="False" CssClass="seekrightbutton"
                                        OnClick="btnCancel_Click" Text="キャンセル" /><br />
                                </asp:Panel>
                                <br />
                                <asp:CustomValidator ID="vdcIdPic" runat="server" ErrorMessage="" OnServerValidate="vdcIdPic_ServerValidate"
                                    ValidationGroup="Detail"></asp:CustomValidator>
                            </td>
                            <td valign="top">
                                <asp:Panel ID="pnlPic" runat="server" Height="600px" ScrollBars="auto">
                                    <table border="0" class="tableStyle" style="width: 500px">
                                        <asp:Repeater ID="rptUpload" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td class="tdHeaderSmallStyle2">
                                                        <%# string.Concat("身分証画像<br />", Container.ItemIndex + 1) %>
                                                    </td>
                                                    <td class="tdDataStyle">
                                                        <asp:FileUpload ID="uldIdPic" runat="server" Width="400px" />
                                                        <asp:Button ID="btnUpload" runat="server" CommandArgument='<%# Container.ItemIndex %>'
                                                            CommandName="UPLOAD" CssClass="seekbutton" OnCommand="btnUpload_Command" Text='<%# string.Concat("ｱﾌﾟﾛｰﾄﾞ", Container.ItemIndex + 1) %>'
                                                            ValidationGroup='<%# string.Concat("Upload", Container.ItemIndex + 1) %>' /><br />
                                                        <asp:RequiredFieldValidator ID="rfvUplod" runat="server" ControlToValidate="uldIdPic"
                                                            ErrorMessage='<%# string.Format("身分証ファイル{0}が入力されていません。", Container.ItemIndex + 1) %>'
                                                            SetFocusOnError="True" ValidationGroup='<%# string.Concat("Upload", Container.ItemIndex+1) %>'></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdDataStyle" colspan="3">
                                                        <asp:Image ID="imgIdPic" runat="server" Height="300px" ImageUrl='<%# GetImgUrl(Container.DataItem) %>'
                                                            Visible='<%# GetImgVisible(Container.DataItem) %>' Width="400px" />
                                                        <asp:Label ID="lblNoImage" runat="server" ForeColor="Red" Text="未登録" Visible="<%# !GetImgVisible(Container.DataItem) %>"></asp:Label>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsProduction" runat="server" SelectMethod="GetList" TypeName="Production">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsManager" runat="server" SelectMethod="GetList" TypeName="Manager">
    </asp:ObjectDataSource>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtLoginPassword">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
        HighlightCssClass="validatorCallout" TargetControlID="vdrCastNm">
    </ajaxToolkit:ValidatorCalloutExtender>
    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
        HighlightCssClass="validatorCallout" TargetControlID="vdeCastNm">
    </ajaxToolkit:ValidatorCalloutExtender>
    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
        HighlightCssClass="validatorCallout" TargetControlID="vdrCastKanaNm">
    </ajaxToolkit:ValidatorCalloutExtender>
    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server"
        HighlightCssClass="validatorCallout" TargetControlID="vdeCastKanaNm">
    </ajaxToolkit:ValidatorCalloutExtender>
    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
        HighlightCssClass="validatorCallout" TargetControlID="vdrBirthDay">
    </ajaxToolkit:ValidatorCalloutExtender>
    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server"
        HighlightCssClass="validatorCallout" TargetControlID="vdcBirthDay">
    </ajaxToolkit:ValidatorCalloutExtender>
    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server"
        HighlightCssClass="validatorCallout" TargetControlID="vddBirthDay">
    </ajaxToolkit:ValidatorCalloutExtender>
    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server"
        HighlightCssClass="validatorCallout" TargetControlID="vdeBirthDay">
    </ajaxToolkit:ValidatorCalloutExtender>
    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server"
        HighlightCssClass="validatorCallout" TargetControlID="vdrLoginPassword">
    </ajaxToolkit:ValidatorCalloutExtender>
    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server"
        HighlightCssClass="validatorCallout" TargetControlID="vdeLoginPassword">
    </ajaxToolkit:ValidatorCalloutExtender>
    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server"
        HighlightCssClass="validatorCallout" TargetControlID="vdeTel">
    </ajaxToolkit:ValidatorCalloutExtender>
    <%--<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server"
        HighlightCssClass="validatorCallout" TargetControlID="vdcEmailAddr">
    </ajaxToolkit:ValidatorCalloutExtender>--%>
    <%--<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server"
        HighlightCssClass="validatorCallout" TargetControlID="vdcTel">
    </ajaxToolkit:ValidatorCalloutExtender>--%>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmOnFormSubmit="true"
        ConfirmText="申請を行ないますか？" TargetControlID="btnUpdate">
    </ajaxToolkit:ConfirmButtonExtender>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" ConfirmOnFormSubmit="true"
        ConfirmText="削除を行ないますか？" TargetControlID="btnCertifyDelete">
    </ajaxToolkit:ConfirmButtonExtender>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender3" runat="server" ConfirmOnFormSubmit="true"
        ConfirmText="更新を行ないますか？" TargetControlID="btnCertifyUpdate">
    </ajaxToolkit:ConfirmButtonExtender>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender4" runat="server" ConfirmOnFormSubmit="true"
        ConfirmText="認証を行ないますか？" TargetControlID="btnCertify">
    </ajaxToolkit:ConfirmButtonExtender>
    <ajaxToolkit:MaskedEditExtender ID="mskBirthDay" runat="server" ClearMaskOnLostFocus="true"
        Mask="9999/99/99" MaskType="Date" TargetControlID="txtBirthDay" UserDateFormat="YearMonthDay">
    </ajaxToolkit:MaskedEditExtender>
</asp:Content>
