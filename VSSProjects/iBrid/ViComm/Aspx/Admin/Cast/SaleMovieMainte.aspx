﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SaleMovieMainte.aspx.cs" Inherits="Cast_SaleMovieMainte" Title="販売動画設定" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="販売動画動画設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[販売動画設定]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					<table border="0" style="width: 640px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle">
								ｻｲﾄ／SEQ／ｷｬﾗｸﾀｰNo.
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblSiteCd" runat="server" Text="Label"></asp:Label>-
								<asp:Label ID="lblLoginId" runat="server" Text="Label"></asp:Label>-
								<asp:Label ID="lblUserSeq" runat="server" Text="Label" Visible="false"></asp:Label>
								<asp:Label ID="lblUserCharNo" runat="server" Text="Label"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								<%= DisplayWordUtil.Replace("出演者名.") %>
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblUserNm" runat="server" Text="Label"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								動画タイトル
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtMovieTitle" runat="server" MaxLength="30" Width="313px"></asp:TextBox>
								<asp:RequiredFieldValidator ID="vdrMovieTitle" runat="server" ErrorMessage="動画タイトルを入力して下さい。" ControlToValidate="txtMovieTitle" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								消費ポイント
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtChargePoint" runat="server" MaxLength="3" Width="40px"></asp:TextBox>
								<asp:RequiredFieldValidator ID="vdrChargePoint" runat="server" ErrorMessage="消費ポイントを入力して下さい。" ControlToValidate="txtChargePoint" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								再生時間
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtPlayTime" runat="server" MaxLength="5" Width="50px"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								サンプルムービー
							</td>
							<td class="tdDataStyle">
								<asp:LinkButton ID="lnkSampleMovie" runat="server" Enabled="false" OnCommand="lnkSampleMovie_Command"></asp:LinkButton>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								サムネイル画像
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblThumbnailPicNothingMessage" runat="server" Text="[未登録]" Visible="true"></asp:Label>
								<asp:Image ID="imgThumbnailPic" runat="server" Visible="false"/><br />
								<asp:FileUpload ID="uldThumbnailPic" runat="server" />&nbsp;<asp:Button ID="btnThumbnailPicPic" runat="server" Text="サムネイル画像アップロード" Width="250px" OnClick="btnThumbnailPic_Click"
									ValidationGroup="ThumbnailPic" />
								<asp:RequiredFieldValidator ID="vdrThumbnailPic" runat="server" ControlToValidate="uldThumbnailPic" ErrorMessage="アップロードファイルが入力されていません。" ValidationGroup="ThumbnailPic">*</asp:RequiredFieldValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								シリーズ
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstMovieSeries" runat="server" 
									DataSourceID="dsMovieAttrType" 
									DataTextField="CAST_MOVIE_ATTR_TYPE_NM" 
									DataValueField="CAST_MOVIE_ATTR_TYPE_SEQ">
								</asp:DropDownList>							
							</td>
						</tr>	
						<tr>
							<td class="tdHeaderStyle" style="height: 71px">
								動画説明
							</td>
							<td class="tdDataStyle" style="height: 71px">
								<asp:TextBox ID="txtMovieDoc" runat="server" MaxLength="1000" Width="313px" Height="74px" TextMode="MultiLine"></asp:TextBox>
							</td>
						</tr>												
						<tr>
							<td class="tdHeaderStyle2">
								動画ステータス
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstAuthType" runat="server" DataSourceID="dsAuthType" DataTextField="CODE_NM" DataValueField="CODE" Width="118px">
								</asp:DropDownList>
							</td>
						</tr>						
					</table>
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
					<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
					<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsAuthType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="54" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsMovieAttrType" runat="server" SelectMethod="GetListIncDefault" TypeName="CastMovieAttrType" OnSelecting="dsMovieAttrType_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtChargePoint" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrMovieTitle" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrChargePoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskPlayTime" runat="server" MaskType="Time" Mask="99:99" ClearMaskOnLostFocus="true" TargetControlID="txtPlayTime" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender18" TargetControlID="vdrThumbnailPic" HighlightCssClass="validatorCallout" />
	
</asp:Content>

