<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastMailTemplateList.aspx.cs" Inherits="Cast_CastMailTemplateList"
	Title="出演者別メールテンプレート設定" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="出演者別メールテンプレート設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<asp:TextBox ID="txtHtmlDocTitle" runat="server" Visible="false"></asp:TextBox>
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<asp:Label ID="lblHtmlDocSeq" runat="server" Text="" Visible="false"></asp:Label>
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 740px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									<asp:Label ID="lblHeader" runat="server" Text="Label"></asp:Label>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblSiteCd" runat="server" Text="Label"></asp:Label>-
									<asp:Label ID="lblLoginId" runat="server" Text="Label"></asp:Label>
									<asp:Label ID="lblUserSeq" runat="server" Text="Label" Visible="false"></asp:Label>
									<asp:Label ID="lblUserCharNo" runat="server" Text="Label"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("出演者名.") %>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblCastNm" runat="server" Text="Label"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									テンプレート種別
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstMailTemplateType" runat="server" DataSourceID="dsOrgTemplate" DataTextField="TEMPLATE_NM" DataValueField="MAIL_TEMPLATE_NO" Width="309px">
									</asp:DropDownList>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnSeekCancel" Text="戻る" CssClass="seekbutton" OnClick="btnSeekCancel_Click" CausesValidation="False" />
					</fieldset>
					<fieldset class="fieldset-inner">
						<asp:Panel ID="Panel1" runat="server">
							<table border="0" style="width: 500px">
								<tr>
									<td>
										�@<%= DisplayWordUtil.Replace("男性") %>ﾒｰﾙｱﾄﾞﾚｽ
									</td>
									<td>
										�A<%= DisplayWordUtil.Replace("男性") %>ﾛｸﾞｲﾝID
									</td>
									<td>
										�B<%= DisplayWordUtil.Replace("男性") %>ﾊﾟｽﾜｰﾄﾞ
									</td>
									<td>
										�C<%= DisplayWordUtil.Replace("男性") %>ﾊﾝﾄﾞﾙ名
									</td>
								</tr>
								<tr>
									<td>
										�D<%= DisplayWordUtil.Replace("出演者") %>ﾊﾝﾄﾞﾙ名
									</td>
									<td>
										�E<%= DisplayWordUtil.Replace("出演者") %>ﾛｸﾞｲﾝID
									</td>
									<td>
										�F<%= DisplayWordUtil.Replace("出演者") %>画像ﾊﾟｽ
									</td>
									<td>
										�G<%= DisplayWordUtil.Replace("出演者") %>登録地域
									</td>
									<td>
										�H<%= DisplayWordUtil.Replace("出演者") %>年齢
									</td>
								</tr>
							</table>
						</asp:Panel>
					</fieldset>
				</asp:Panel>
				<table border="0">
					<tr>
						<td valign="top">
							<asp:Panel runat="server" ID="pnlSelect" Height="580px" ScrollBars="Auto">
								<fieldset class="fieldset-inner">
									<legend>[アウトルック]</legend>
									<asp:GridView ID="grdSiteHtmlDoc" runat="server" AllowPaging="False" AutoGenerateColumns="False" DataSourceID="dsSiteHtmlDoc" SkinID="GridView" Width="230px"
										ShowHeader="False">
										<Columns>
											<asp:TemplateField>
												<ItemTemplate>
													<asp:LinkButton ID="lnkDoc" runat="server" CommandArgument='' Text='<%# string.Format("{0}",Eval("HTML_DOC_TITLE")) %>' OnCommand="lnkDoc_Command" Font-Bold="True"
														Font-Underline="True"></asp:LinkButton>
													<br />
													<asp:Literal ID="lblHtmlDoc1" runat="server" Text='<%# string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}",Eval("HTML_DOC1"), Eval("HTML_DOC2"), Eval("HTML_DOC3"), Eval("HTML_DOC4"), Eval("HTML_DOC5"), Eval("HTML_DOC6"),Eval("HTML_DOC7"), Eval("HTML_DOC8") ,Eval("HTML_DOC9"), Eval("HTML_DOC10"))%>'></asp:Literal>
												</ItemTemplate>
												<ItemStyle HorizontalAlign="Left" />
											</asp:TemplateField>
										</Columns>
									</asp:GridView>
								</fieldset>
							</asp:Panel>
						</td>
						<td valign="top">
							<asp:Panel runat="server" ID="pnlDtl">
								<fieldset class="fieldset-inner">
									<legend>[メールテンプレート内容]</legend>
									<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seektopbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
									<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seektopbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
									<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seektopbutton" OnClick="btnCancel_Click" CausesValidation="False" />
									<br />&nbsp;
									<table border="0" style="width: 610px" class="tableStyle">
										<tr>
											<td class="tdHeaderSmallStyle">
												ﾒｰﾙﾀｲﾄﾙ
											</td>
											<td class="tdDataStyle">
												<asp:TextBox ID="txtMailTitle" runat="server" MaxLength="60" Width="180px"></asp:TextBox>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderSmallStyle2">
												HTML文章
											</td>
											<td class="tdDataStyle">
												$NO_TRANS_START;
												<pin:pinEdit ID="txtHtmlDoc" runat="server" UserMode="HTML" DisplayStyle="Style5" DocumentView="True" HtmlMode="Body" Ruler="False" ReturnMode="BR" SpellMode="Inline"
													ToolbarConfiguration="ToolbarDesigner" Toolbar="T09SE10116012136267;T22232427282930313233343536;B53SE54SE55" FormatMode="Classic" BorderWidth="1px"
													Height="500px" Width="500px" DocumentWidth="230" Font-Bold="False" FontSizeType="px" RelativeBaseImageUrl="/" RelativeBaseLinkUrl="/" RelativeImageRoot="/">
												</pin:pinEdit>
												<asp:TextBox ID="txtHtmlDocText" runat="server" TextMode="MultiLine" Height="500px" Width="500px" Visible="false"></asp:TextBox>
												$NO_TRANS_END;
											</td>
										</tr>
									</table>
								</fieldset>
							</asp:Panel>
						</td>
					</tr>
				</table>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[メールテンプレート一覧]</legend>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<br />
			<br />
			<asp:GridView ID="grdTemplate" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsMailTemplate" AllowSorting="True" SkinID="GridViewColor">
				<Columns>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkTemplate" runat="server" Text='<%# string.Format("{0} {1}",Eval("MAIL_TEMPLATE_NO"),Eval("MAIL_TITLE")) %>' CommandArgument='<%# string.Format("{0}:{1}",Eval("MAIL_TEMPLATE_NO"),Eval("HTML_DOC_SEQ"))  %>'
								OnCommand="lnkTemplate_Command" CausesValidation="False">
							</asp:LinkButton>
						</ItemTemplate>
						<HeaderTemplate>
							メールタイトル
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:BoundField DataField="MAIL_TEMPLATE_TYPE_NM" HeaderText="メールテンプレート種別">
						<ItemStyle HorizontalAlign="Left" />
					</asp:BoundField>
					<asp:BoundField DataField="DOC" HeaderText="HTML文章">
						<ItemStyle HorizontalAlign="Left" Wrap="True" />
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br>
				<a class="reccount">Current viewing page
					<%=grdTemplate.PageIndex + 1%>
					of
					<%=grdTemplate.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsOrgTemplate" runat="server" SelectMethod="GetCastCreateOriginal" TypeName="MailTemplate" OnSelecting="dsOrgTemplate_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSiteHtmlDoc" runat="server" SelectMethod="GetListByDocSeq" TypeName="SiteHtmlDoc" OnSelecting="dsSiteHtmlDoc_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pHtmlDocSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsMailTemplate" runat="server" SelectMethod="GetPageCollection" TypeName="CastMailTemplate" SelectCountMethod="GetPageCount"
		EnablePaging="True" OnSelected="dsMailTemplate_Selected" OnSelecting="dsMailTemplate_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
