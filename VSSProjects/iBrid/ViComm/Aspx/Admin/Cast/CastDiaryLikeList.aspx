﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CastDiaryLikeList.aspx.cs" Inherits="Cast_CastDiaryLikeList" Title="いいね履歴"
    ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="いいね履歴"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <fieldset class="fieldset">
            <legend>[検索条件]</legend>
            <fieldset class="fieldset-inner">
                <table border="0" style="width: 800px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle2">
                            サイトコード
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="180px" Enabled="false">
                            </asp:DropDownList>
                        </td>
                        <td class="tdHeaderStyle2">
                            男性ログインID
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtLoginId" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    CausesValidation="False" ValidationGroup="Search" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" ValidationGroup="Search" />
            </fieldset>
            <asp:Panel runat="server" ID="pnlGrid" Visible="false">
                <fieldset class="fieldset-inner">
                    <legend>[履歴一覧]</legend>
                    <asp:Panel runat="server" ID="pnlCount">
                        <a class="reccount">Record Count
                            <%#GetRecCount() %>
                        </a>
                        <br />
                        <a class="reccount">Current viewing page
                            <%=grdCastDiaryLikeList.PageIndex + 1%>
                            of
                            <%=grdCastDiaryLikeList.PageCount%>
                        </a>
                    </asp:Panel>
                    &nbsp;
                    <asp:GridView ID="grdCastDiaryLikeList" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        DataSourceID="" AllowSorting="True" SkinID="GridViewColor">
                        <Columns>
                            <asp:TemplateField HeaderText="男性ログインID">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkLoginId" runat="server" Text='<%# Eval("LOGIN_ID") %>' CausesValidation="False"
                                        CommandArgument='<%# string.Format("{0},{1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>'
                                        OnCommand="lnkLoginId_Command"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="日時" DataField="UPDATE_DATE">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </fieldset>
            </asp:Panel>
        </fieldset>
        <br />
    </div>
    <asp:ObjectDataSource ID="dsCastDiaryLikeList" runat="server" EnablePaging="True"
        OnSelected="dsCastDiaryLikeList_Selected" OnSelecting="dsCastDiaryLikeList_Selecting"
        SelectCountMethod="GetPageCount" SelectMethod="GetPageCollection" TypeName="CastDiaryLike">
        <SelectParameters>
            <asp:QueryStringParameter Name="pSiteCd" Type="String" QueryStringField="sitecd" />
            <asp:QueryStringParameter Name="pUserSeq" Type="String" QueryStringField="userseq" />
            <asp:QueryStringParameter Name="pUserCharNo" Type="String" QueryStringField="usercharno" />
            <asp:QueryStringParameter Name="pReportDay" Type="String" QueryStringField="reportday" />
            <asp:QueryStringParameter Name="pSubSeq" Type="String" QueryStringField="subseq" />
            <asp:Parameter Name="pLoginId" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
</asp:Content>
