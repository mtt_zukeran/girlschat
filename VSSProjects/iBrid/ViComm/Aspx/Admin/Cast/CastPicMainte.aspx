<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastPicMainte.aspx.cs" Inherits="Cast_CastPicMainte" Title="出演者写真設定" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="出演者写真設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>
							<%= DisplayWordUtil.Replace("[出演者写真設定]") %>
						</legend>
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									<asp:Label ID="lblHeader" runat="server" Text="Label"></asp:Label>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblSiteCd" runat="server" Text="Label"></asp:Label>-
									<asp:Label ID="lblLoginId" runat="server" Text="Label"></asp:Label>
									<asp:Label ID="lblUserSeq" runat="server" Text="Label" Visible="false"></asp:Label>
									<asp:Label ID="lblUserCharNo" runat="server" Text="Label"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("出演者名.") %>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblCastNm" runat="server" Text="Label"></asp:Label>
								</td>
							</tr>
							<tr id="trSaveType" runat="server">
								<td class="tdHeaderStyle">
									画像保存形式
								</td>
								<td class="tdDataStyle">
									<asp:RadioButton ID="rdoSaveTypeProfile" runat="server" GroupName="SaveType" Checked="true" Text="プロフィール">
									</asp:RadioButton>
									<asp:RadioButton ID="rdoSaveTypeMail" runat="server" GroupName="SaveType" Checked="false" Text="メール添付形式">
									</asp:RadioButton>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									ファイルアップロード
								</td>
								<td class="tdDataStyle">
									<asp:FileUpload ID="uldCastPic" runat="server" Width="400px" />
									<asp:RequiredFieldValidator ID="rfvUplod" runat="server" ErrorMessage="アップロードファイルが入力されていません。" SetFocusOnError="True" ValidationGroup="Upload" ControlToValidate="uldCastPic">*</asp:RequiredFieldValidator>
								</td>
							</tr>
						</table>
						<asp:Button ID="btnUpload" runat="server" ValidationGroup="Upload" Text="実行" CssClass="seekbutton" OnClick="btnUpload_Click" />
						<asp:Button runat="server" ID="btnCancel" Text="戻る" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<asp:Panel ID="pnlList" runat="server">
			<fieldset>
				<legend>[写真一覧]</legend>
				<asp:Label ID="lblStatus3" runat="server" Text="青枠" ForeColor="Blue"></asp:Label><asp:Label ID="lblStatus4" runat="server" Text="で囲まれた画像が"></asp:Label>
				<asp:Label ID="lblStatus5" runat="server" Text="現在プロフィールに指定し<br/>てある画像です。"></asp:Label>
				<asp:DataList ID="lstPic" runat="server" DataSourceID="dsCastPic" Width="300px">
					<ItemTemplate>
						<a href="<%# PickupHelper.GeneratePickupObjsViewerUrl(this, lblSiteCd.Text,Eval("PIC_SEQ").ToString(), ViComm.ViCommConst.PicupTypes.PROFILE_PIC) %>">
							<asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("OBJ_PHOTO_IMG_PATH","../{0}") %>' BorderColor='<%# GetBorderColor(Eval("PROFILE_PIC_FLAG")) %>'
								BorderStyle="Solid" BorderWidth="2" />
						</a>
						<br />
						<asp:Label ID="lblClosed" runat="server" Text='<%# GetClosedMark(Eval("OBJ_NOT_APPROVE_FLAG"),Eval("OBJ_NOT_PUBLISH_FLAG"),Eval("PIC_TYPE")) %>' ForeColor='<%# GetClosedMarkColor(Eval("OBJ_NOT_APPROVE_FLAG"),Eval("OBJ_NOT_PUBLISH_FLAG"),Eval("PIC_TYPE")) %>'/>&nbsp;&nbsp;
						<asp:LinkButton ID="lnkUnClosed" runat="server" Text="公開にする" OnCommand="lnkClosed_Command" CommandArgument='<%# string.Format("{0},{1}",Eval("PIC_SEQ"),ViComm.ViCommConst.FLAG_OFF_STR) %>' CommandName="CLOSE" Visible='<%# GetClosedVisible(Eval("OBJ_NOT_APPROVE_FLAG"),Eval("OBJ_NOT_PUBLISH_FLAG")) %>'></asp:LinkButton>
						<asp:LinkButton ID="lnkClosed" runat="server" Text="非公開にする" OnCommand="lnkClosed_Command" CommandArgument='<%# string.Format("{0},{1}",Eval("PIC_SEQ"),ViComm.ViCommConst.FLAG_ON_STR) %>' CommandName="CLOSE" Visible='<%# !GetClosedVisible(Eval("OBJ_NOT_APPROVE_FLAG"),Eval("OBJ_NOT_PUBLISH_FLAG")) %>'></asp:LinkButton><br />
                        <asp:LinkButton ID="lnkImgDownload" runat="server" CommandArgument='<%# Eval("PIC_SEQ") %>' CommandName="DOWNLOAD" OnCommand="lnkImgDownload_Command" Text="画像ダウンロード"></asp:LinkButton><br />
                        <asp:Label ID="lblUploadDay" runat="server" Text='<%# string.Format("アップロード日時：{0}",Eval("UPLOAD_DATE")) %>' /><br />
						<asp:Label ID="lblPicTitle" runat="server" Text='<%# string.Format("タイトル：{0}",Eval("PIC_TITLE")) %>' /><br />
						<asp:Label ID="lblPicDoc" runat="server" Text='<%# string.Format("本文：{0}",Eval("PIC_DOC")) %>' /><br />						
						<asp:ImageButton ID="btnUp" runat="server" ImageUrl="../image/up.gif" OnClientClick="return confirm('表示を上にしますか？');" OnCommand="btnUp_Command" CommandArgument='<%# Eval("PIC_SEQ") %>'
							Visible='<%# Eval("UP_BTN_FLAG") %>' />&nbsp;
						<asp:ImageButton ID="btnDown" runat="server" ImageUrl="../image/down.gif" OnClientClick="return confirm('表示を下にしますか？');" OnCommand="btnDown_Command" CommandArgument='<%# Eval("PIC_SEQ") %>'
							Visible='<%# Eval("DOWN_BTN_FLAG") %>' />&nbsp;
						<asp:ImageButton ID="btnDelete" runat="server" ImageUrl="../image/del.gif" CommandArgument='<%# Eval("PIC_SEQ") %>' OnClientClick="return confirm('削除を行いますか？');"
							OnCommand="btnDelete_Command" Visible='<%# Session["AdminId"].Equals("itomichiko") %>' /><br />
						<asp:ImageButton ID="btnProfile" runat="server" ImageUrl="../image/prof.gif" CommandArgument='<%# Eval("PIC_SEQ") %>' OnClientClick="return confirm('設定を行いますか？');"
							OnCommand="btnProfile_Command" /><br />
						<br />
					</ItemTemplate>
				</asp:DataList>
			</fieldset>
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsCastPic" runat="server" SelectMethod="GetList" TypeName="CastPic" OnSelecting="dsCastPic_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pUserCharNo" Type="String" />
			<asp:Parameter Name="pPicType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="rfvUplod" HighlightCssClass="validatorCallout" />
</asp:Content>
