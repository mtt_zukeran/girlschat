﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者キャラクター設定 
--	Progaram ID		: CastCharacterMainte
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class Cast_CastCharacterMainte:System.Web.UI.Page {
	#region キャストキャラクター拡張
	protected string EnabledBlogFlag {
		get {
			return iBridUtil.GetStringValue(this.ViewState["EnabledBlogFlag"]);
		}
		private set {
			this.ViewState["EnabledBlogFlag"] = value;
		}
	}
	protected string BuyPointMailRxType {
		get {
			return iBridUtil.GetStringValue(this.ViewState["BuyPointMailRxType"]);
		}
		private set {
			this.ViewState["BuyPointMailRxType"] = value;
		}
	}
	protected string WaitingPointLastAddDay {
		get {
			return iBridUtil.GetStringValue(this.ViewState["WaitingPointLastAddDay"]);
		}
		private set {
			this.ViewState["WaitingPointLastAddDay"] = value;
		}
	}
	protected string WaitingPointLastReleaseDay {
		get {
			return iBridUtil.GetStringValue(this.ViewState["WaitingPointLastReleaseDay"]);
		}
		private set {
			this.ViewState["WaitingPointLastReleaseDay"] = value;
		}
	}
	protected string RevisionNoEX {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNoEX"]);
		}
		private set {
			this.ViewState["RevisionNoEX"] = value;
		}
	}
	#endregion

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {

		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);
		this.vdrCastToManBatchMailLimit.ErrorMessage = DisplayWordUtil.Replace(this.vdrCastToManBatchMailLimit.ErrorMessage);

		if (Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_LOCAL_STAFF)) {
			lstActCategorySeq.Visible = false;
			lstConnectType.Visible = false;
			lstOnLineStatus.Enabled = false;
			txtPriority.Visible = false;
			txtStartPerformDay.Visible = false;
			chkOfflineDisplayFlag.Visible = false;
			lstNaFlag.Visible = false;
			btnDelete.Visible = false;
			txtAdCd.Visible = false;
		} else {
			lstActCategorySeq.Visible = true;
			lstOnLineStatus.Enabled = true;
			lstConnectType.Visible = true;
			txtPriority.Visible = true;
			txtStartPerformDay.Visible = true;
			chkOfflineDisplayFlag.Visible = true;
			lstNaFlag.Visible = true;
			btnDelete.Visible = true;
			txtAdCd.Visible = true;
		}

		txtCommentAdmin.Visible = false;
		trCommentAdmin.Visible = false;

		//キャストが営業用誕生日を使用する場合、生年月日欄を表示する
		using (ManageCompany oCompany = new ManageCompany()) {
			plcBirthDay.Visible = oCompany.IsAvailableService(ViCommConst.RELEASE_CAST_USE_DUMMY_BIRTHDAY);
		}

		ViewState["RETURN"] = iBridUtil.GetStringValue(Request.QueryString["return"]);
		lblSiteCd.Text = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		lblUserSeq.Text = iBridUtil.GetStringValue(Request.QueryString["userseq"]);
		txtUserCharNo.Text = iBridUtil.GetStringValue(Request.QueryString["usercharno"]);
		lblLoginId.Text = "";
		ClearField();
		DataBind();
		GetData();

		DisplayWordUtil.ReplaceValidatorErrorMessage(this.Page.Validators);
	}

	private void ClearField() {
		lblRegist.Visible = false;
		txtUserCharNo.Enabled = true;
		lstActCategorySeq.SelectedIndex = 0;
		lstConnectType.SelectedIndex = 0;
		lblCastNm.Text = string.Empty;
		txtHandleNm.Text = string.Empty;
		hdnPrevHandleNm.Value = string.Empty;
		txtHandleKanaNm.Text = string.Empty;
		txtCommentList.Text = string.Empty;
		txtCommentDetail.Text = string.Empty;
		txtCommentAdmin.Text = string.Empty;
		txtWaitingComment.Text = string.Empty;
		txtPriority.Text = string.Empty;
		lblLastActionDate.Text = string.Empty;
		txtStartPerformDay.Text = string.Empty;
		txtBirthDay.Text = string.Empty;
		txtAdCd.Text = string.Empty;
		lstNaFlag.SelectedIndex = 0;
		lstManMailRxType.SelectedIndex = 1;
		lstInfoMailRxType.SelectedIndex = 1;
		lstOnLineStatus.SelectedIndex = 0;
		chkOfflineDisplayFlag.Checked = false;
		txtCastToManBatchMailLimit.Text = string.Empty;
		txtBatchMailCount.Text = "0";
		txtInviteMailLimit.Text = string.Empty;
		txtInviteMailCount.Text = "0";
		if (lstLoginMailTemplateNo.Items.Count > 0) {
			lstLoginMailTemplateNo.SelectedIndex = 0;
		}
		if (lstLoginMailTemplateNo2.Items.Count > 0) {
			lstLoginMailTemplateNo2.SelectedIndex = 0;
		}
		if (lstLogoutMailTemplateNo.Items.Count > 0) {
			lstLogoutMailTemplateNo.SelectedIndex = 0;
		}
		chkEnabledBlog.Checked = false;
		chkEnabledRichino.Checked = false;
		chkNoUpdateRichiNoViewFlag.Checked = false;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		Server.Transfer(string.Format("CastView.aspx?loginid={0}&return={1}",lblLoginId.Text,ViewState["RETURN"].ToString()));
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_CHARACTER_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lblSiteCd.Text);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,lblUserSeq.Text);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,txtUserCharNo.Text);
			db.ProcedureOutParm("PLOGIN_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PACT_CATEGORY_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCONNECT_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCHARACTER_ONLINE_STATUS",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHANDLE_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHANDLE_KANA_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCOMMENT_LIST",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCOMMENT_DETAIL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCOMMENT_ADMIN",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PWAITING_COMMENT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPRIORITY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PLAST_ACTION_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTART_PERFORM_DAY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PBIRTHDAY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("POK_PLAY_MASK",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("POFFLINE_DISPLAY_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PLOGIN_MAIL_TEMPALTE_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PLOGIN_MAIL_TEMPALTE_NO2",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PLOGOUT_MAIL_TEMPALTE_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMAN_MAIL_RX_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PINFO_MAIL_RX_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMOBILE_MAIL_RX_START_TIME",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMOBILE_MAIL_RX_END_TIME",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PNA_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PFRIEND_INTRO_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PINTRODUCER_FRIEND_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_TO_MAN_BATCH_MAIL_NA_FLG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_TO_MAN_BATCH_MAIL_LIMIT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_TO_MAN_BATCH_MAIL_LIMITS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PBATCH_MAIL_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PINVITE_MAIL_LIMIT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PINVITE_MAIL_LIMITS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PINVITE_MAIL_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PAD_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PUSER_DEFINE_MASK",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);

			db.ProcedureOutArrayParm("POK_PLAY_NM",DbSession.DbType.VARCHAR2,ViCommConst.MAX_PLAY_COUNT);
			db.ProcedureOutParm("POK_PLAY_RECORD_COUNT",DbSession.DbType.NUMBER);

			db.ProcedureOutArrayParm("PCAST_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,ViCommConst.MAX_ATTR_COUNT);
			db.ProcedureOutArrayParm("PCAST_ATTR_TYPE_NM",DbSession.DbType.VARCHAR2,ViCommConst.MAX_ATTR_COUNT);
			db.ProcedureOutArrayParm("PATTR_INPUT_TYPE",DbSession.DbType.VARCHAR2,ViCommConst.MAX_ATTR_COUNT);
			db.ProcedureOutArrayParm("PCAST_ATTR_SEQ",DbSession.DbType.VARCHAR2,ViCommConst.MAX_ATTR_COUNT);
			db.ProcedureOutArrayParm("PATTR_INPUT_VALUE",DbSession.DbType.VARCHAR2,ViCommConst.MAX_ATTR_COUNT);
			db.ProcedureOutArrayParm("PATTR_ROWID",DbSession.DbType.VARCHAR2,ViCommConst.MAX_ATTR_COUNT);

			db.ProcedureOutParm("PATTR_RECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");
			ViewState["ATTR_RECORD_COUNT"] = db.GetStringValue("PATTR_RECORD_COUNT");
			ViewState["OK_PLAY_RECORD_COUNT"] = db.GetStringValue("POK_PLAY_RECORD_COUNT");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				lblLoginId.Text = db.GetStringValue("PLOGIN_ID");
				lblCastNm.Text = db.GetStringValue("PCAST_NM");
				txtUserCharNo.Text = db.GetStringValue("PUSER_CHAR_NO");
				txtHandleNm.Text = db.GetStringValue("PHANDLE_NM");
				hdnPrevHandleNm.Value = txtHandleNm.Text;
				txtHandleKanaNm.Text = db.GetStringValue("PHANDLE_KANA_NM");
				txtCommentList.Text = db.GetStringValue("PCOMMENT_LIST");
				txtCommentDetail.Text = db.GetStringValue("PCOMMENT_DETAIL");
				txtCommentAdmin.Text = db.GetStringValue("PCOMMENT_ADMIN");
				txtWaitingComment.Text = db.GetStringValue("PWAITING_COMMENT");
				txtPriority.Text = db.GetStringValue("PPRIORITY");
				lblLastActionDate.Text = db.GetStringValue("PLAST_ACTION_DATE");
				txtStartPerformDay.Text = db.GetStringValue("PSTART_PERFORM_DAY");
				txtBirthDay.Text = db.GetStringValue("PBIRTHDAY");
				lstActCategorySeq.SelectedValue = db.GetStringValue("PACT_CATEGORY_SEQ");
				lstConnectType.SelectedValue = db.GetStringValue("PCONNECT_TYPE");
				lstOnLineStatus.SelectedValue = db.GetStringValue("PCHARACTER_ONLINE_STATUS");
				chkOfflineDisplayFlag.Checked = (db.GetStringValue("POFFLINE_DISPLAY_FLAG").Equals("1"));
				lstNaFlag.SelectedValue = db.GetStringValue("PNA_FLAG");
				lblFriendIntroCd.Text = db.GetStringValue("PFRIEND_INTRO_CD");
				lblIntroducerFriendCd.Text = db.GetStringValue("PINTRODUCER_FRIEND_CD");
				chkCastToManBatchMailNaFlag.Checked = (db.GetStringValue("PCAST_TO_MAN_BATCH_MAIL_NA_FLG").Equals(ViCommConst.FLAG_ON_STR));
				txtCastToManBatchMailLimit.Text = db.GetStringValue("PCAST_TO_MAN_BATCH_MAIL_LIMIT");
				txtBatchMailCount.Text = db.GetStringValue("PBATCH_MAIL_COUNT");
				txtInviteMailLimit.Text = db.GetStringValue("PINVITE_MAIL_LIMIT");
				txtInviteMailCount.Text = db.GetStringValue("PINVITE_MAIL_COUNT");
				txtAdCd.Text = db.GetStringValue("PAD_CD");

				if (!db.GetStringValue("PMAN_MAIL_RX_TYPE").Equals("")) {
					lstManMailRxType.SelectedValue = db.GetStringValue("PMAN_MAIL_RX_TYPE");
				}
				if (!db.GetStringValue("PINFO_MAIL_RX_TYPE").Equals("")) {
					lstInfoMailRxType.SelectedValue = db.GetStringValue("PINFO_MAIL_RX_TYPE");
				}
				if (!db.GetStringValue("PLOGIN_MAIL_TEMPALTE_NO").Equals("")) {
					lstLoginMailTemplateNo.SelectedValue = db.GetStringValue("PLOGIN_MAIL_TEMPALTE_NO");
				}
				if (!db.GetStringValue("PLOGIN_MAIL_TEMPALTE_NO2").Equals("")) {
					lstLoginMailTemplateNo2.SelectedValue = db.GetStringValue("PLOGIN_MAIL_TEMPALTE_NO2");
				}
				if (!db.GetStringValue("PLOGOUT_MAIL_TEMPALTE_NO").Equals("")) {
					lstLogoutMailTemplateNo.SelectedValue = db.GetStringValue("PLOGOUT_MAIL_TEMPALTE_NO");
				}
				if (!db.GetStringValue("PMOBILE_MAIL_RX_START_TIME").Equals("")) {
					lstMobileMailRxStartTime.SelectedValue = db.GetStringValue("PMOBILE_MAIL_RX_START_TIME");
				}
				if (!db.GetStringValue("PMOBILE_MAIL_RX_END_TIME").Equals("")) {
					lstMobileMailRxEndTime.SelectedValue = db.GetStringValue("PMOBILE_MAIL_RX_END_TIME");
				}
				txtUserCharNo.Enabled = false;
				int iUserDefineFlag = db.GetIntValue("PUSER_DEFINE_MASK");

				foreach (ListItem oListItem in chkUserDefineFlag.Items) {
					if ((iUserDefineFlag & int.Parse(oListItem.Value)) > 0) {
						oListItem.Selected = true;
					} else {
						oListItem.Selected = false;
					}
				}

				using (Ad oAd = new Ad()) {
					string sAdNm = "";
					oAd.IsExist(txtAdCd.Text,ref sAdNm);
					lblAdNm.Text = sAdNm;
				}

			} else {
				ClearField();
				lblRegist.Visible = true;
				lblLoginId.Text = db.GetStringValue("PLOGIN_ID");
				lblCastNm.Text = db.GetStringValue("PCAST_NM");
				txtPriority.Text = db.GetStringValue("PPRIORITY");
				txtCastToManBatchMailLimit.Text = db.GetStringValue("PCAST_TO_MAN_BATCH_MAIL_LIMITS");
				txtInviteMailLimit.Text = db.GetStringValue("PINVITE_MAIL_LIMITS");

				//ｷｬｽﾄ登録時のみﾛｰｶﾙｽﾀｯﾌの権限をなくす
				if (Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_LOCAL_STAFF)) {
					lstActCategorySeq.Visible = true;
					lstConnectType.Visible = true;
					txtPriority.Visible = true;
					txtStartPerformDay.Visible = true;
					chkOfflineDisplayFlag.Visible = true;
					//chkNaFlag.Visible = true;
					txtAdCd.Visible = true;
				}
			}
			vdeBatchMailCount.MaximumValue = "999999";
			vdeBatchMailCount.ErrorMessage = "0～999999の値で設定してください";

			vdeInviteMailCount.MaximumValue = db.GetStringValue("PINVITE_MAIL_LIMITS");
			vdeInviteMailCount.ErrorMessage = string.Format("0～{0}の値で設定してください",db.GetStringValue("PINVITE_MAIL_LIMITS"));

			db.conn.Close();

			if (db.GetIntValue("POK_PLAY_RECORD_COUNT") != 0) {
				Int64 iOkMask = Int64.Parse(db.GetStringValue("POK_PLAY_MASK"));
				Int64 iMaskValue = 1;
				for (int i = 0;i < db.GetIntValue("POK_PLAY_RECORD_COUNT");i++) {
					CheckBox chkOk = (CheckBox)plcOkList.FindControl(string.Format("chkOKFlag{0}",i)) as CheckBox;
					chkOk.Text = db.GetArryStringValue("POK_PLAY_NM",i);
					if ((iOkMask & iMaskValue) != 0) {
						chkOk.Checked = true;
					}
					iMaskValue = iMaskValue << 1;
				}
				for (int i = db.GetIntValue("POK_PLAY_RECORD_COUNT");i < ViCommConst.MAX_PLAY_COUNT;i++) {
					CheckBox chkOk = (CheckBox)plcOkList.FindControl(string.Format("chkOKFlag{0}",i)) as CheckBox;
					chkOk.Visible = false;
				}
			} else {
				plcOkList.Visible = false;
			}

			using (CastAttrTypeValue oCastAttrTypeValue = new CastAttrTypeValue())
			using (CastAttrType oCastAttrType = new CastAttrType()) {
				for (int i = 0;i < db.GetIntValue("PATTR_RECORD_COUNT");i++) {
					Label lblCastAttrNm = (Label)plcHolder.FindControl(string.Format("lblCastAttrNm{0}",i)) as Label;
					lblCastAttrNm.Text = db.GetArryStringValue("PCAST_ATTR_TYPE_NM",i);

					DropDownList lstCastAttrSeq = (DropDownList)plcHolder.FindControl(string.Format("lstCastAttrSeq{0}",i)) as DropDownList;
					TextBox txtInputValue = (TextBox)plcHolder.FindControl(string.Format("txtAttrInputValue{0}",i)) as TextBox;
					DataSet ds = oCastAttrTypeValue.GetList(lblSiteCd.Text,db.GetArryStringValue("PCAST_ATTR_TYPE_SEQ",i));
					foreach (DataRow dr in ds.Tables[0].Rows) {
						lstCastAttrSeq.Items.Add(new ListItem(dr["CAST_ATTR_NM"].ToString(),dr["CAST_ATTR_SEQ"].ToString()));
					}

					lstCastAttrSeq.Items.Insert(0,new ListItem(string.Empty,string.Empty));

					if (db.GetArryStringValue("PATTR_INPUT_TYPE",i).Equals(ViCommConst.INPUT_TYPE_TEXT)) {
						txtInputValue.Text = db.GetArryStringValue("PATTR_INPUT_VALUE",i);
						lstCastAttrSeq.Visible = false;
					} else {
						if (db.GetArryStringValue("PCAST_ATTR_SEQ",i) != "") {
							lstCastAttrSeq.SelectedValue = db.GetArryStringValue("PCAST_ATTR_SEQ",i);
						}
						txtInputValue.Visible = false;
					}

					ViewState["ATTR_ROWID" + i.ToString()] = db.GetArryStringValue("PATTR_ROWID",i);
					ViewState["CAST_ATTR_TYPE_SEQ" + i.ToString()] = db.GetArryStringValue("PCAST_ATTR_TYPE_SEQ",i);

					if (oCastAttrType.GetOne(lblSiteCd.Text,db.GetArryStringValue("PCAST_ATTR_TYPE_SEQ",i))) {
						TableRow rowAttr = (TableRow)plcHolder.FindControl(string.Format("rowAttr{0}",i)) as TableRow;
						if (oCastAttrType.naFlag) {
							rowAttr.Visible = false;
						} else {
							rowAttr.Visible = true;
						}
					}
				}

				for (int i = db.GetIntValue("PATTR_RECORD_COUNT");i < ViCommConst.MAX_ATTR_COUNT;i++) {
					TableRow rowAttr = (TableRow)plcHolder.FindControl(string.Format("rowAttr{0}",i)) as TableRow;
					rowAttr.Visible = false;
				}
			}

		}
		using (Site oSite = new Site()) {
			if (oSite.GetMultiCharFlag(lblSiteCd.Text)) {
				trUserCharNo.Visible = true;
			} else {
				trUserCharNo.Visible = false;
			}
		}
		pnlDtl.Visible = true;
		this.pnlCharacterEx.Visible = true;

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("CAST_CHARACTER_EX_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.lblSiteCd.Text);
			oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,this.lblUserSeq.Text);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,this.txtUserCharNo.Text);
			oDbSession.ProcedureOutParm("pENABLED_BLOG_FLAG",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pENABLED_RICHINO_FLAG",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pNO_UPDATE_RICHINO_VIEW_FLAG",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pBUY_POINT_MAIL_RX_TYPE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pWAITING_PT_LAST_ADD_DAY",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pWAITING_PT_LAST_RELEASE_DAY",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pROWID",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pROW_COUNT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			if (int.Parse(oDbSession.GetStringValue("pROW_COUNT")) > 0) {
				this.EnabledBlogFlag = oDbSession.GetStringValue("pENABLED_BLOG_FLAG");
				this.BuyPointMailRxType = oDbSession.GetStringValue("pBUY_POINT_MAIL_RX_TYPE");
				this.WaitingPointLastAddDay = oDbSession.GetStringValue("pWAITING_PT_LAST_ADD_DAY");
				this.WaitingPointLastReleaseDay = oDbSession.GetStringValue("pWAITING_PT_LAST_RELEASE_DAY");
				this.RevisionNoEX = oDbSession.GetStringValue("pREVISION_NO");

				if (ViCommConst.FLAG_ON_STR.Equals(oDbSession.GetStringValue("pENABLED_BLOG_FLAG"))) {
					this.chkEnabledBlog.Checked = true;
				} else {
					this.chkEnabledBlog.Checked = false;
				}

				if (ViCommConst.FLAG_ON_STR.Equals(oDbSession.GetStringValue("pENABLED_RICHINO_FLAG"))) {
					this.chkEnabledRichino.Checked = true;
				} else {
					this.chkEnabledRichino.Checked = false;
				}
				if (ViCommConst.FLAG_ON_STR.Equals(oDbSession.GetStringValue("pNO_UPDATE_RICHINO_VIEW_FLAG"))) {
					this.chkNoUpdateRichiNoViewFlag.Checked = true;
				} else {
					this.chkNoUpdateRichiNoViewFlag.Checked = false;
				}
			}
		}
	}

	private void UpdateData(int pDelFlag) {
		string sHandelNm = Mobile.EmojiToCommTag(ViCommConst.CARRIER_OTHERS,txtHandleNm.Text);
		string sHandelNmKana = Mobile.EmojiToCommTag(ViCommConst.CARRIER_OTHERS,txtHandleKanaNm.Text);
		string sList = Mobile.EmojiToCommTag(ViCommConst.CARRIER_OTHERS,txtCommentList.Text);
		string sDetail = Mobile.EmojiToCommTag(ViCommConst.CARRIER_OTHERS,txtCommentDetail.Text);
		string sAdmin = Mobile.EmojiToCommTag(ViCommConst.CARRIER_OTHERS,txtCommentAdmin.Text);
		string sWaiting = Mobile.EmojiToCommTag(ViCommConst.CARRIER_OTHERS,txtWaitingComment.Text);

		Int64 iOkMask = 0;
		Int64 iMaskValue = 1;
		int iMaskCount = int.Parse(ViewState["OK_PLAY_RECORD_COUNT"].ToString());
		for (int i = 0;i < iMaskCount;i++) {
			CheckBox chkOk = (CheckBox)plcOkList.FindControl(string.Format("chkOKFlag{0}",i)) as CheckBox;
			if (chkOk.Checked) {
				iOkMask = iOkMask + iMaskValue;
			}
			iMaskValue = iMaskValue << 1;
		}

		int iUserDefineFlag = 0;

		foreach (ListItem oListItem in chkUserDefineFlag.Items) {
			if (!oListItem.Selected)
				continue;
			iUserDefineFlag += int.Parse(oListItem.Value);
		}

		//ｷｬｽﾄｷｬﾗｸﾀｰ新規登録時にﾛｰｶﾙｽﾀｯﾌだった場合NA_FLAGを未認証に
		if (Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_LOCAL_STAFF) && (lblRegist.Visible == true)) {
			lstNaFlag.SelectedValue = ViCommConst.NaFlag.NO_CERTIFIED.ToString();
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_CHARACTER_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lblSiteCd.Text);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,lblUserSeq.Text);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,txtUserCharNo.Text);
			db.ProcedureInParm("PACT_CATEGORY_SEQ",DbSession.DbType.VARCHAR2,lstActCategorySeq.SelectedValue);
			db.ProcedureInParm("PCONNECT_TYPE",DbSession.DbType.VARCHAR2,lstConnectType.SelectedValue);
			db.ProcedureInParm("PCHARACTER_ONLINE_STATUS",DbSession.DbType.VARCHAR2,lstOnLineStatus.SelectedValue);
			db.ProcedureInParm("PHANDLE_NM",DbSession.DbType.VARCHAR2,sHandelNm);
			db.ProcedureInParm("PHANDLE_KANA_NM",DbSession.DbType.VARCHAR2,sHandelNmKana);
			db.ProcedureInParm("PCOMMENT_LIST",DbSession.DbType.VARCHAR2,sList);
			db.ProcedureInParm("PCOMMENT_DETAIL",DbSession.DbType.VARCHAR2,sDetail);
			db.ProcedureInParm("PCOMMENT_ADMIN",DbSession.DbType.VARCHAR2,sAdmin);
			db.ProcedureInParm("PWAITING_COMMENT",DbSession.DbType.VARCHAR2,sWaiting);
			db.ProcedureInParm("PPRIORITY",DbSession.DbType.NUMBER,decimal.Parse(txtPriority.Text));
			db.ProcedureInParm("PSTART_PERFORM_DAY",DbSession.DbType.VARCHAR2,txtStartPerformDay.Text);
			db.ProcedureInParm("PBIRTHDAY",DbSession.DbType.VARCHAR2,txtBirthDay.Text);
			db.ProcedureInParm("POK_PLAY_MASK",DbSession.DbType.NUMBER,iOkMask);
			db.ProcedureInParm("POFFLINE_DISPLAY_FLAG",DbSession.DbType.NUMBER,chkOfflineDisplayFlag.Checked);
			db.ProcedureInParm("PLOGIN_MAIL_TEMPALTE_NO",DbSession.DbType.VARCHAR2,lstLoginMailTemplateNo.SelectedValue);
			db.ProcedureInParm("PLOGIN_MAIL_TEMPALTE_NO2",DbSession.DbType.VARCHAR2,lstLoginMailTemplateNo2.SelectedValue);
			db.ProcedureInParm("PLOGOUT_MAIL_TEMPALTE_NO",DbSession.DbType.VARCHAR2,lstLogoutMailTemplateNo.SelectedValue);
			db.ProcedureInParm("PMAN_MAIL_RX_TYPE",DbSession.DbType.VARCHAR2,lstManMailRxType.SelectedValue);
			db.ProcedureInParm("PINFO_MAIL_RX_TYPE",DbSession.DbType.VARCHAR2,lstInfoMailRxType.SelectedValue);
			db.ProcedureInParm("PMOBILE_MAIL_RX_START_TIME",DbSession.DbType.VARCHAR2,lstMobileMailRxStartTime.SelectedValue);
			db.ProcedureInParm("PMOBILE_MAIL_RX_END_TIME",DbSession.DbType.VARCHAR2,lstMobileMailRxEndTime.SelectedValue);
			db.ProcedureInParm("PNA_FLAG",DbSession.DbType.NUMBER,int.Parse(lstNaFlag.SelectedValue));
			db.ProcedureInParm("PFRIEND_INTRO_CD",DbSession.DbType.VARCHAR2,lblFriendIntroCd.Text);
			db.ProcedureInParm("PCAST_TO_MAN_BATCH_MAIL_NA_FLG",DbSession.DbType.NUMBER,chkCastToManBatchMailNaFlag.Checked);
			db.ProcedureInParm("PCAST_TO_MAN_BATCH_MAIL_LIMIT",DbSession.DbType.NUMBER,int.Parse(txtCastToManBatchMailLimit.Text));
			db.ProcedureInParm("PBATCH_MAIL_COUNT",DbSession.DbType.NUMBER,int.Parse(txtBatchMailCount.Text));
			db.ProcedureInParm("PINVITE_MAIL_LIMIT",DbSession.DbType.NUMBER,int.Parse(txtInviteMailLimit.Text));
			db.ProcedureInParm("PINVITE_MAIL_COUNT",DbSession.DbType.NUMBER,int.Parse(txtInviteMailCount.Text));
			db.ProcedureInParm("PAD_CD",DbSession.DbType.VARCHAR2,txtAdCd.Text);
			db.ProcedureInParm("PUSER_DEFINE_MASK",DbSession.DbType.NUMBER,iUserDefineFlag);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);

			int iRecCount = int.Parse(ViewState["ATTR_RECORD_COUNT"].ToString());

			string[] sCastAttrTypeSeq = new string[ViCommConst.MAX_ATTR_COUNT];
			string[] sCastAttrSeq = new string[ViCommConst.MAX_ATTR_COUNT];
			string[] sAttrInputValue = new string[ViCommConst.MAX_ATTR_COUNT];
			string[] sAttrRowID = new string[ViCommConst.MAX_ATTR_COUNT];

			for (int i = 0;i < iRecCount;i++) {
				DropDownList lstCastAttrSeq = (DropDownList)plcHolder.FindControl(string.Format("lstCastAttrSeq{0}",i)) as DropDownList;
				sCastAttrSeq[i] = lstCastAttrSeq.SelectedValue;

				TextBox txtInputValue = (TextBox)plcHolder.FindControl(string.Format("txtAttrInputValue{0}",i)) as TextBox;
				sAttrInputValue[i] = txtInputValue.Text;

				sCastAttrTypeSeq[i] = (string)ViewState["CAST_ATTR_TYPE_SEQ" + i.ToString()];
				sAttrRowID[i] = (string)ViewState["ATTR_ROWID" + i.ToString()];
			}

			db.ProcedureInArrayParm("PCAST_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,iRecCount,sCastAttrTypeSeq);
			db.ProcedureInArrayParm("PCAST_ATTR_SEQ",DbSession.DbType.VARCHAR2,iRecCount,sCastAttrSeq);
			db.ProcedureInArrayParm("PATTR_INPUT_VALUE",DbSession.DbType.VARCHAR2,iRecCount,sAttrInputValue);
			db.ProcedureInArrayParm("PATTR_ROWID",DbSession.DbType.VARCHAR2,iRecCount,sAttrRowID);

			db.ProcedureInParm("PATTR_RECORD_COUNT",DbSession.DbType.NUMBER,iRecCount);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("CAST_CHARACTER_EX_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.lblSiteCd.Text);
			oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,this.lblUserSeq.Text);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,this.txtUserCharNo.Text);
			oDbSession.ProcedureInParm("pENABLED_BLOG_FLAG",DbSession.DbType.VARCHAR2,this.chkEnabledBlog.Checked ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR);
			oDbSession.ProcedureInParm("pENABLED_RICHINO_FLAG",DbSession.DbType.NUMBER,this.chkEnabledRichino.Checked);
			oDbSession.ProcedureInParm("pNO_UPDATE_RICHINO_VIEW_FLAG",DbSession.DbType.NUMBER,this.chkNoUpdateRichiNoViewFlag.Checked);
			oDbSession.ProcedureInParm("pBUY_POINT_MAIL_RX_TYPE",DbSession.DbType.VARCHAR2,this.BuyPointMailRxType);
			oDbSession.ProcedureInParm("pWAITING_POINT_LAST_ADD_DAY",DbSession.DbType.VARCHAR2,this.WaitingPointLastAddDay);
			oDbSession.ProcedureInParm("pWAITING_POINT_LAST_RELEASE_DAY",DbSession.DbType.VARCHAR2,this.WaitingPointLastReleaseDay);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.VARCHAR2,this.RevisionNoEX);
			oDbSession.ProcedureInParm("pDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		string sWebPhisicalDir = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(lblSiteCd.Text,"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
		}

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {

			if (pDelFlag == 0) {
				string sDir = "";

				sDir = sWebPhisicalDir + ViCommConst.PIC_DIRECTRY + "\\" + lblSiteCd.Text + string.Format("\\Operator\\{0}",lblLoginId.Text);
				Directory.CreateDirectory(sDir);

				sDir = sWebPhisicalDir + ViCommConst.MOVIE_DIRECTRY + "\\" + lblSiteCd.Text + string.Format("\\Operator\\{0}",lblLoginId.Text);
				Directory.CreateDirectory(sDir);
			}
		}
		Server.Transfer(string.Format("CastView.aspx?loginid={0}&return={1}",lblLoginId.Text,ViewState["RETURN"].ToString()));
	}
	protected void vdcUserCharNo_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid && lblRegist.Visible == true) {
			using (CastCharacter oCastCharacter = new CastCharacter()) {
				args.IsValid = !oCastCharacter.IsExistUserCharNo(lblSiteCd.Text,lblUserSeq.Text,txtUserCharNo.Text);
			}
		}
	}
	protected void vdcBirthday_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid) {
			int iOkAge;
			int.TryParse(iBridUtil.GetStringValue(ConfigurationManager.AppSettings["RefuseAge"]),out iOkAge);
			if (iOkAge == 0) {
				iOkAge = 10;
			}
			args.IsValid = (ViCommPrograms.Age(txtBirthDay.Text) >= iOkAge);
		}
	}

	protected void vdcAdCd_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid) {
			using (Ad oAd = new Ad()) {
				string sAdNm = "";
				args.IsValid = oAd.IsExist(txtAdCd.Text,ref sAdNm);
				lblAdNm.Text = sAdNm;
			}
		}
	}

	protected void vdcHandleNm_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid) {
			if (string.IsNullOrEmpty(this.txtHandleNm.Text))
				return;

			using (CastCharacter oCastCharacter = new CastCharacter()) {
				args.IsValid = oCastCharacter.CheckHandleNmDupli(lblSiteCd.Text,lblUserSeq.Text,txtUserCharNo.Text,this.txtHandleNm.Text);
			}
		}
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oList = sender as DropDownList;
		if (oList == null) {
			return;
		}

		oList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}
}