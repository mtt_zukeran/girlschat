<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastCharacterInquiry.aspx.cs" Inherits="Cast_CastCharacterInquiry"
	Title="サイト別出演者検索" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="サイト別出演者検索"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlKey">
			<fieldset class="fieldset">
				<legend>[検索条件]</legend>
				<asp:Panel ID="pnlSeekCondition" runat="server">
					<table border="0" class="tableStyle" style="width: 1200px">
						<tr>
							<td class="tdHeaderStyle">
								サイト
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px" AutoPostBack="True"
									OnSelectedIndexChanged="lstSiteCd_SelectedIndexChanged">
								</asp:DropDownList>
							</td>
							<td class="tdHeaderStyle">
								オンライン状態
							</td>
							<td class="tdDataStyle">
								<asp:CheckBoxList ID="chkOnlineStatus" runat="server" DataSourceID="dsOnline" DataTextField="CODE_NM" DataValueField="CODE" RepeatDirection="Horizontal">
								</asp:CheckBoxList>
							</td>
							<td class="tdHeaderStyle">
								メールアドレス状態
							</td>
							<td class="tdDataStyle">
								<asp:CheckBox ID="chkMailAddrOk" runat="server" Text="正常" />
								<asp:CheckBox ID="chkMailAddrFilteringError" runat="server" Text="ﾌｨﾙﾀﾘﾝｸﾞｴﾗｰ" />
								<asp:CheckBox ID="chkMailAddrNg" runat="server" Text="不通ｴﾗｰ" />
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								電話番号・GUID
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtTel" runat="server" MaxLength="11" Width="80px"></asp:TextBox>&nbsp;<asp:TextBox ID="txtGuid" runat="server" MaxLength="32" Width="70px"></asp:TextBox>
							</td>
							<td class="tdHeaderStyle">
								メールアドレス
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtEmailAddr" runat="server" Width="174px"></asp:TextBox>
								<asp:CheckBox ID="chkExcludeEmailAddr" runat="server" Text="除外" />
							</td>
							<td class="tdHeaderStyle">
								お知らせﾒｰﾙ受信区分
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstInfoMailRxType" runat="server" DataSourceID="dsInfoMailRxType" DataTextField="CODE_NM" DataValueField="CODE" Width="200px" />
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								ログインID・SEQ<asp:CheckBox ID="chkLoginIdNegativeFlag" runat="server" /><br />
								<asp:LinkButton ID="lnkDisplayLoginIdList" runat="server" Text="複数入力" OnClick="lnkDisplayLoginIdList_Click"></asp:LinkButton>
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtLoginId" runat="server"></asp:TextBox>
								<asp:TextBox ID="txtUserSeq" runat="server" MaxLength="15" Width="64px"></asp:TextBox>
							</td>
							<td class="tdHeaderStyle">
								ハンドル名
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtHandelNm" runat="server" MaxLength="120"></asp:TextBox>
							    <asp:CheckBox ID="chkHandleNmExactFlag" runat="server" Checked="false" Text="完全一致" />
							</td>
							<td class="tdHeaderStyle" id="tdHeaderCastNm" runat="server">
								<%= DisplayWordUtil.Replace("出演者名") %>
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtCastNm" runat="server"></asp:TextBox>
							</td>
						</tr>
						<asp:Panel ID="pnlLoginIdList" runat="server" Visible="false">
							<tr>
								<td class="tdHeaderStyle">
									ログインID<asp:CheckBox ID="chkLoginIdListNegativeFlag" runat="server" /><br />
									（複数入力）
									<asp:LinkButton ID="lnkHideLoginIdList" runat="server" Text="閉じる" OnClick="lnkHideLoginIdList_Click"></asp:LinkButton>
									<asp:CheckBox ID="chkLoginIdListVisible" runat="server" Visible="false" />
								</td>
								<td class="tdDataStyle" colspan="7">
									<asp:TextBox ID="txtLoginIdList" runat="server" Width="500px" TextMode="multiline" Rows="3"></asp:TextBox>
								</td>
							</tr>
						</asp:Panel>						
						<tr>
							<td class="tdHeaderStyle">
								会員状態
							</td>
							<td class="tdDataStyle">
								<asp:CheckBox ID="chkNormal" runat="server" Text="通常" />
								<asp:CheckBox ID="chkAuthWait" runat="server" Text="認証待ち" ForeColor="SeaGreen" Visible="false" />
								<asp:CheckBox ID="chkHold" runat="server" Text="保留" ForeColor="Blue" />
								<asp:CheckBox ID="chkResigned" runat="server" Text="退会" ForeColor="Maroon" />
								<asp:CheckBox ID="chkStop" runat="server" Text="禁止" ForeColor="Red" />
								<asp:CheckBox ID="chkBan" runat="server" Text="停止" ForeColor="Red" />
							</td>
							<td class="tdHeaderStyle">
								ユーザーランク
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstUserRankSeek" runat="server" Width="180px" DataSourceID="dsCastRank" DataTextField="USER_RANK_NM" DataValueField="USER_RANK">
								</asp:DropDownList>
							</td>
							<td class="tdHeaderStyle">
								カテゴリ
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstActCategorySeq" runat="server" DataSourceID="dsActCategory" DataTextField="ACT_CATEGORY_NM" DataValueField="ACT_CATEGORY_SEQ"
									Width="180px">
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								登録日<br />
								最終ログイン日<br />
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtRegistDayFrom" runat="server" MaxLength="10" Width="62px" style="margin-bottom:3px;"></asp:TextBox>
								&nbsp;〜
								<asp:TextBox ID="txtRegistDayTo" runat="server" MaxLength="10" Width="62px" style="margin-bottom:3px;"></asp:TextBox>
								

								<asp:RangeValidator ID="vdrRegistDayFrom" runat="server" ErrorMessage="登録日Fromを正しく入力して下さい。" ControlToValidate="txtRegistDayFrom" MaximumValue="2099/12/31"
									MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:RangeValidator ID="vdrRegistDayTo" runat="server" ErrorMessage="登録日Toを正しく入力して下さい。" ControlToValidate="txtRegistDayTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
									Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:CompareValidator ID="vdcRegistDayFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtRegistDayFrom" ControlToValidate="txtRegistDayTo"
									Operator="GreaterThanEqual" ValidationGroup="Key" Display="Dynamic">*</asp:CompareValidator>
								
								<br />
								<asp:TextBox ID="txtFinalLoginDayFrom" runat="server" MaxLength="10" Width="62px" style="margin-bottom:3px;"></asp:TextBox>
								&nbsp;〜
								<asp:TextBox ID="txtFinalLoginDayTo" runat="server" MaxLength="10" Width="62px" style="margin-bottom:3px;"></asp:TextBox>
								
								<asp:RangeValidator ID="vdrFinalLoginDayFrom" runat="server" ErrorMessage="最終ログイン日Fromを正しく入力して下さい。" ControlToValidate="txtFinalLoginDayFrom" MaximumValue="2099/12/31"
									MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:RangeValidator ID="vdrFinalLoginDayTo" runat="server" ErrorMessage="最終ログイン日Toを正しく入力して下さい。" ControlToValidate="txtFinalLoginDayTo" MaximumValue="2099/12/31"
									MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:CompareValidator ID="vdcFinalLoginDayFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtFinalLoginDayFrom" ControlToValidate="txtFinalLoginDayTo"
									Operator="GreaterThanEqual" ValidationGroup="Key" Display="Dynamic">*</asp:CompareValidator>
								<br />
								
							</td>
							<td class="tdHeaderStyle">
								最終ｱｸｼｮﾝ日<asp:CheckBox ID="chkLastActionDayNegativeFlag" runat="server" />
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtLastLoginDayFrom" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
								&nbsp;〜
								<asp:TextBox ID="txtLastLoginDayTo" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
								<asp:RangeValidator ID="vdrLastLoginDayFrom" runat="server" ErrorMessage="最終アクション日Fromを正しく入力して下さい。" ControlToValidate="txtLastLoginDayFrom" MaximumValue="2099/12/31"
									MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:RangeValidator ID="vdrLastLoginDayTo" runat="server" ErrorMessage="最終アクション日Toを正しく入力して下さい。" ControlToValidate="txtLastLoginDayTo" MaximumValue="2099/12/31"
									MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:CompareValidator ID="vdcLastLoginDayFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtLastLoginDayFrom" ControlToValidate="txtLastLoginDayTo"
									Operator="GreaterThanEqual" ValidationGroup="Key" Display="Dynamic">*</asp:CompareValidator>
							</td>
							<td class="tdHeaderStyle">
								ｱﾌｨﾘｴｲﾄ認証日
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtReportAffiliateDateFrom" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
								&nbsp;〜
								<asp:TextBox ID="txtReportAffiliateDateTo" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
								<asp:RangeValidator ID="vdrReportAffiliateDateFrom" runat="server" ErrorMessage="ｱﾌｨﾘｴｲﾄ認証日Fromを正しく入力して下さい。" ControlToValidate="txtReportAffiliateDateFrom"
									MaximumValue="2099/12/31" MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:RangeValidator ID="vdrReportAffiliateDateTo" runat="server" ErrorMessage="ｱﾌｨﾘｴｲﾄ認証日Toを正しく入力して下さい。" ControlToValidate="txtReportAffiliateDateTo" MaximumValue="2099/12/31"
									MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:CompareValidator ID="vdcReportAffiliateDateFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtReportAffiliateDateFrom" ControlToValidate="txtReportAffiliateDateTo"
									Operator="GreaterThanEqual" ValidationGroup="Key" Display="Dynamic">*</asp:CompareValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								広告コード
								<asp:LinkButton ID="lnkDisplayAdCdList" runat="server" Text="複数入力" OnClick="lnkDisplayAdCdList_Click"></asp:LinkButton>
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtAdCd" runat="server" MaxLength="32" Width="100px"></asp:TextBox>
								<asp:RegularExpressionValidator ID="vdeAdCd" runat="server" ErrorMessage="広告コードは32桁以下の英数字で入力して下さい。" ValidationExpression="[a-zA-Z0-9-\/_]{1,32}" ControlToValidate="txtAdCd"
									ValidationGroup="Key" Display="Dynamic">*</asp:RegularExpressionValidator>
								<asp:CustomValidator ID="vdcAdCd" runat="server" ControlToValidate="txtAdCd" ErrorMessage="広告コードが未登録です。" OnServerValidate="vdcAdCd_ServerValidate" ValidationGroup="Key"
									Display="Dynamic">*</asp:CustomValidator>
								<asp:Literal ID="lblAdNmNotVisibleStartFont" runat="server"></asp:Literal>
								<asp:Literal ID="lblAdNm" runat="server"></asp:Literal>
								<asp:Literal ID="lblAdNmNotVisibleEndFont" runat="server"></asp:Literal>
							</td>
							<td class="tdHeaderStyle">
								キャリア
							</td>
							<td class="tdDataStyle">
								<asp:CheckBox ID="chkDocomo" runat="server" Text="D" />
								<asp:CheckBox ID="chkAu" runat="server" Text="A" />
								<asp:CheckBox ID="chkSoftbank" runat="server" Text="S" />
								<asp:CheckBox ID="chkAndroid" runat="server" Text="Android" />
								<asp:CheckBox ID="chkIphone" runat="server" Text="iPhone" />
							</td>
							<td class="tdHeaderStyle">
								ｷｬﾗｸﾀ状態
							</td>
							<td class="tdDataStyle">
								<asp:CheckBoxList ID="chkNaFlag" runat="server" DataSourceID="dsNaFlag" DataTextField="CODE_NM" DataValueField="CODE" RepeatDirection="Horizontal">
								</asp:CheckBoxList>
							</td>
						</tr>
						<asp:Panel ID="pnlAdCdList" runat="server" Visible="false">
							<tr>
								<td class="tdHeaderStyle">
									広告コード<br />
									（複数入力）
									<asp:LinkButton ID="lnkHideAdCdList" runat="server" Text="閉じる" OnClick="lnkHideAdCdList_Click"></asp:LinkButton>
									<asp:CheckBox ID="chkAdCdListVisible" runat="server" Visible="false" />
								</td>
								<td class="tdDataStyle" colspan="7">
									<asp:TextBox ID="txtAdCdList" runat="server" Width="500px" TextMode="multiline" Rows="3"></asp:TextBox>
                                    <asp:CustomValidator ID="vdcAdCdList" runat="server" ControlToValidate="txtAdCdList" ErrorMessage="" OnServerValidate="vdcAdCdList_ServerValidate" ValidationGroup="Key"
									    Display="Dynamic">*</asp:CustomValidator>
									<asp:Literal ID="lblAdNmList" runat="server"></asp:Literal>
								</td>
							</tr>
						</asp:Panel>
						<tr>
							<td class="tdHeaderStyle">
								プロダクション
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstProductionSeq" runat="server" DataSourceID="dsProduction" DataTextField="PRODUCTION_NM" DataValueField="PRODUCTION_SEQ" Width="170px"
									OnSelectedIndexChanged="lstProductionSeq_SelectedIndexChanged" AutoPostBack="True">
								</asp:DropDownList>
							</td>
							<td class="tdHeaderStyle">
								担当
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstManager" runat="server" DataSourceID="dsManager" DataTextField="MANAGER_NM" DataValueField="MANAGER_SEQ" Width="180px">
								</asp:DropDownList>
							</td>
							<td class="tdHeaderStyle">
							    ﾔｷﾓﾁ防止設定利用
							</td>
							<td class="tdDataStyle">
								<asp:RadioButtonList ID="rdoUseJealousyFlag" runat="server" RepeatDirection="horizontal">
									<asp:ListItem Selected="true" Text="全て" Value=""></asp:ListItem>
									<asp:ListItem Text="利用" Value="1"></asp:ListItem>
									<asp:ListItem Text="非利用" Value="0"></asp:ListItem>
								</asp:RadioButtonList>
							</td>
						</tr>
						<tr>
							<asp:PlaceHolder ID="plsAge" runat="server" Visible="true">
								<td class="tdHeaderStyle">
									年齢
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtAgeFrom" runat="server" MaxLength="2" Width="20px"></asp:TextBox>
									&nbsp;〜
									<asp:TextBox ID="txtAgeTo" runat="server" MaxLength="2" Width="20px"></asp:TextBox>
									<asp:RangeValidator ID="vdrAgeFrom" runat="server" ErrorMessage="年齢Fromを正しく入力して下さい。" ControlToValidate="txtAgeFrom" MaximumValue="99" MinimumValue="0"
										Type="Integer" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
									<asp:RangeValidator ID="vdrAgeTo" runat="server" ErrorMessage="年齢Toを正しく入力して下さい。" ControlToValidate="txtAgeTo" MaximumValue="99" MinimumValue="0" Type="Integer"
										ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
									<asp:CompareValidator ID="vdcAgeFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtAgeFrom" ControlToValidate="txtAgeTo" Operator="GreaterThanEqual"
										ValidationGroup="Key" Type="Integer" Display="Dynamic">*</asp:CompareValidator>
								</td>
							</asp:PlaceHolder>
							<asp:PlaceHolder ID="plsFavoritMeCount" runat="server" Visible="true">
								<td class="tdHeaderStyle">
									お気に入られ数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtFavoritMeCountFrom" runat="server" MaxLength="6" Width="45px"></asp:TextBox>
									&nbsp;〜
									<asp:TextBox ID="txtFavoritMeCountTo" runat="server" MaxLength="6" Width="45px"></asp:TextBox>
									<asp:RangeValidator ID="vdrFavoritMeCountFrom" runat="server" ErrorMessage="お気に入られ数Fromを正しく入力して下さい。" ControlToValidate="txtFavoritMeCountFrom" MaximumValue="999999"
										MinimumValue="0" Type="Integer" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
									<asp:RangeValidator ID="vdrFavoritMeCountTo" runat="server" ErrorMessage="お気に入られ数Toを正しく入力して下さい。" ControlToValidate="txtFavoritMeCountTo" MaximumValue="999999"
										MinimumValue="0" Type="Integer" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
									<asp:CompareValidator ID="vdcFavoritMeCountFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtFavoritMeCountFrom" ControlToValidate="txtFavoritMeCountTo"
										Operator="GreaterThanEqual" ValidationGroup="Key" Type="Integer" Display="Dynamic">*</asp:CompareValidator>
								</td>
							</asp:PlaceHolder>
							<td class="tdHeaderStyle">
								最終ﾒｰﾙ送信日
							</td>
							<td class="tdDataStyle" >
								<asp:TextBox ID="txtLastTxMailDateFrom" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
								&nbsp;〜
								<asp:TextBox ID="txtLastTxMailDateTo" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
							    <asp:RangeValidator ID="vdrLastTxMailDateFrom" runat="server" ErrorMessage="最終ﾒｰﾙ送信日Fromを正しく入力して下さい。" ControlToValidate="txtLastTxMailDateFrom" MaximumValue="2099/12/31"
								    MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							    <asp:RangeValidator ID="vdrLastTxMailDateTo" runat="server" ErrorMessage="最終ﾒｰﾙ送信日Toを正しく入力して下さい。" ControlToValidate="txtLastTxMailDateTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
								    Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							    <asp:CompareValidator ID="vdcLastTxMailDateFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtLastTxMailDateFrom" ControlToValidate="txtLastTxMailDateTo"
								    Operator="GreaterThanEqual" ValidationGroup="Key" Display="Dynamic">*</asp:CompareValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								備考
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtRemarks1" runat="server" MaxLength="1024" Width="160px"></asp:TextBox>
							</td>
							<td class="tdHeaderStyle">
								ピックアップ種別
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstPickupId" runat="server" DataSourceID="dsPickup" DataTextField="PICKUP_TITLE" DataValueField="PICKUP_ID" Width="180px">
								</asp:DropDownList>
							</td>
							<td class="tdHeaderStyle">
								報酬累計額
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtTotalPaymentAmtFrom" runat="server" MaxLength="9" Width="70px"></asp:TextBox>&nbsp;〜
								<asp:TextBox ID="txtTotalPaymentAmtTo" runat="server" MaxLength="9" Width="70px"></asp:TextBox>
								<asp:CompareValidator ID="vdcTotalPaymentAmtFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtTotalPaymentAmtFrom" ControlToValidate="txtTotalPaymentAmtTo"
									Operator="GreaterThanEqual" ValidationGroup="Key" Type="Integer">*</asp:CompareValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								待機種別
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstConnectType" runat="server" DataSourceID="dsConnectType" DataTextField="CODE_NM" DataValueField="CODE" Width="170px">
								</asp:DropDownList>
							</td>
							<td class="tdHeaderStyle">
							    Crosmile利用
							</td>
							<td class="tdDataStyle">
								<asp:RadioButtonList ID="rdoUseCrosmileFlag" runat="server" RepeatDirection="horizontal">
									<asp:ListItem Selected="true" Text="全て" Value=""></asp:ListItem>
									<asp:ListItem Text="利用" Value="1"></asp:ListItem>
									<asp:ListItem Text="非利用" Value="0"></asp:ListItem>
								</asp:RadioButtonList>
							</td>
							<td class="tdHeaderStyle">
							    音声通話ｱﾌﾟﾘ利用
							</td>
							<td class="tdDataStyle" colspan="3">
								<asp:RadioButtonList ID="rdoUseVoiceappFlag" runat="server" RepeatDirection="horizontal">
									<asp:ListItem Selected="true" Text="全て" Value=""></asp:ListItem>
									<asp:ListItem Text="利用" Value="1"></asp:ListItem>
									<asp:ListItem Text="非利用" Value="0"></asp:ListItem>
								</asp:RadioButtonList>
							</td>
						</tr>
                        <tr>
							<td class="tdHeaderStyle">
								Crosmile最終利用Ver.
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtCrosmileLastUsedVersionFrom" runat="server" MaxLength="12" Width="60px"></asp:TextBox>&nbsp;〜
								<asp:TextBox ID="txtCrosmileLastUsedVersionTo" runat="server" MaxLength="12" Width="60px"></asp:TextBox>
								<asp:CompareValidator ID="vdcCrosmileLastUsedVersion" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtCrosmileLastUsedVersionFrom" ControlToValidate="txtCrosmileLastUsedVersionTo"
									Operator="GreaterThanEqual" ValidationGroup="Key" Type="String">*</asp:CompareValidator>
                            	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender35" TargetControlID="vdcCrosmileLastUsedVersion" HighlightCssClass="validatorCallout" />
							</td>
							<td class="tdHeaderStyle2">
								GCｱﾌﾟﾘVer
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtGcappLastLoginVersionFrom" runat="server" MaxLength="12" Width="60px"></asp:TextBox>&nbsp;〜
								<asp:TextBox ID="txtGcappLastLoginVersionTo" runat="server" MaxLength="12" Width="60px"></asp:TextBox>
								<asp:CompareValidator ID="vdcGcappLastLoginVersion" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtGcappLastLoginVersionFrom" ControlToValidate="txtGcappLastLoginVersionTo" Operator="GreaterThanEqual" ValidationGroup="Key" Type="String">*</asp:CompareValidator>
                            	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender36" TargetControlID="vdcGcappLastLoginVersion" HighlightCssClass="validatorCallout" />
							</td>
							<td class="tdHeaderStyle">
								MULLER3送信状態
							</td>
							<td class="tdDataStyle">
								<asp:RadioButtonList ID="rdoTxMuller3NgMailAddrFlag" runat="server" RepeatDirection="horizontal">
									<asp:ListItem Selected="true" Text="全て" Value=""></asp:ListItem>
									<asp:ListItem Text="正常" Value="0"></asp:ListItem>
									<asp:ListItem Text="ﾌｨﾙﾀﾘﾝｸﾞｴﾗｰ" Value="1"></asp:ListItem>
								</asp:RadioButtonList>
							</td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle" id="tdHeaderKeyword" runat="server">
                                キーワード(空白区切り)
                            </td>
                            <td class="tdDataStyle" colspan="3">
                                <asp:TextBox ID="txtKeyword" runat="server" Width="500px"></asp:TextBox>&nbsp;ｺﾒﾝﾄを検索
                            </td>
							<td class="tdHeaderStyle2">
								未支払報酬額
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtNotPaymentAmtFrom" runat="server" MaxLength="9" Width="70px"></asp:TextBox>&nbsp;〜
								<asp:TextBox ID="txtNotPaymentAmtTo" runat="server" MaxLength="9" Width="70px"></asp:TextBox>
								<asp:CompareValidator ID="vdcNotPaymentAmtFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtNotPaymentAmtFrom" ControlToValidate="txtNotPaymentAmtTo"
									Operator="GreaterThanEqual" ValidationGroup="Key" Type="Integer">*</asp:CompareValidator>
							</td>
                        </tr>
                        <asp:PlaceHolder ID="plcUserDefineFlag" runat="server">
                            <tr>
                                <td class="tdHeaderStyle" id="tdHeaderUserDefineMask" runat="server">
                                    ユーザー定義フラグ<asp:CheckBox ID="chkUserDefineMaskNegatieFlag" runat="server" />
                                </td>
                                <td class="tdDataStyle" colspan="5">
                                    <asp:CheckBoxList ID="chkUserDefineMask" runat="server" DataSourceID="dsUserDefineMask" RepeatDirection="horizontal" RepeatLayout="Flow" RepeatColumns="10"
                                        DataTextField="CODE_NM" DataValueField="CODE">
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                        </asp:PlaceHolder>
						<asp:PlaceHolder ID="plcBeforeSystemId" runat="server" Visible="false">
                        <tr>
                            <td class="tdHeaderStyle2">
                                旧ID
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtBeforeSystemId" runat="server" ></asp:TextBox>
                            </td>
                            <td class="tdHeaderStyle2">
                                紹介された出演者
                            </td>
                            <td class="tdDataStyle">
                                <asp:CheckBox ID="chkIntroducerFriendAllFlag" runat="server" Text="" Height="18px" />
                            </td>
                        </tr>
                        </asp:PlaceHolder>
					</table>
					<asp:PlaceHolder ID="plcAttrType" runat="server" Visible="false">
						<div style="padding-top: 5px; padding-bottom: 3px">
							<table border="0" class="tableStyle">
								<tr>
									<td class="tdHeaderSmallStyle2" id="celCastAttrNm0" runat="server" style="width: 115px">
										<asp:Label ID="lblCastAttrNm0" runat="server" Text="Label"></asp:Label>
									</td>
									<td class="tdDataStyle" id="celCastAttrSeq0" runat="server">
										<asp:DropDownList ID="lstCastAttrSeq0" runat="server" Width="100px">
										</asp:DropDownList>
									</td>
									<td class="tdHeaderSmallStyle2" id="celCastAttrNm1" runat="server" style="width: 115px">
										<asp:Label ID="lblCastAttrNm1" runat="server" Text="Label"></asp:Label>
									</td>
									<td class="tdDataStyle" id="celCastAttrSeq1" runat="server">
										<asp:DropDownList ID="lstCastAttrSeq1" runat="server" Width="100px">
										</asp:DropDownList>
									</td>
									<td class="tdHeaderSmallStyle2" id="celCastAttrNm2" runat="server" style="width: 115px">
										<asp:Label ID="lblCastAttrNm2" runat="server" Text="Label"></asp:Label>
									</td>
									<td class="tdDataStyle" id="celCastAttrSeq2" runat="server">
										<asp:DropDownList ID="lstCastAttrSeq2" runat="server" Width="100px">
										</asp:DropDownList>
									</td>
									<td class="tdHeaderSmallStyle2" id="celCastAttrNm3" runat="server" style="width: 115px">
										<asp:Label ID="lblCastAttrNm3" runat="server" Text="Label"></asp:Label>
									</td>
									<td class="tdDataStyle" id="celCastAttrSeq3" runat="server">
										<asp:DropDownList ID="lstCastAttrSeq3" runat="server" Width="100px">
										</asp:DropDownList>
									</td>
								</tr>
							</table>
						</div>
					</asp:PlaceHolder>
					<asp:PlaceHolder ID="plcExtension" runat="server">
						<div style="padding-top: 5px; padding-bottom: 3px">
							<table border="0" class="tableStyle" style="width: 1140px">
								<tr>
									<td class="tdHeaderStyle">
										ﾌﾞﾛｸﾞ開設許可
									</td>
									<td class="tdDataStyle">
										<asp:RadioButtonList ID="rdoEnabledBlog" runat="server" RepeatDirection="horizontal">
											<asp:ListItem Selected="true" Text="全て" Value=""></asp:ListItem>
											<asp:ListItem Text="有効" Value="1"></asp:ListItem>
											<asp:ListItem Text="無効" Value="0"></asp:ListItem>
										</asp:RadioButtonList>
									</td>
									<td class="tdHeaderStyle">
										ﾘｯﾁｰﾉ閲覧許可
									</td>
									<td class="tdDataStyle">
										<asp:RadioButtonList ID="rdoEnaledRichino" runat="server" RepeatDirection="horizontal">
											<asp:ListItem Selected="true" Text="全て" Value=""></asp:ListItem>
											<asp:ListItem Text="有効" Value="1"></asp:ListItem>
											<asp:ListItem Text="無効" Value="0"></asp:ListItem>
										</asp:RadioButtonList>
									</td>
									<td class="tdHeaderStyle">
										ｻｲﾄ利用状況
									</td>
									<td class="tdDataStyle">
										<asp:CheckBox ID="chkChat" runat="server" Text="ﾗｲﾌﾞﾁｬｯﾄのみ" />
										<asp:CheckBox ID="chkGame" runat="server" Text="ｹﾞｰﾑのみ" />
										<asp:CheckBox ID="chkChatGamet" runat="server" Text="両方" />
									</td>
								</tr>
                                <tr>
                                    <td class="tdHeaderStyle">
                                        ゲームハンドル名
                                    </td>
                                    <td class="tdDataStyle">
                                        <asp:TextBox ID="txtGameHandleNm" runat="server" MaxLength="120"></asp:TextBox>
                                    </td>
                                    <td class="tdHeaderStyle">
                                        ｹﾞｰﾑｷｬﾗｸﾀｰﾀｲﾌﾟ
                                    </td>
                                    <td class="tdDataStyle">
                                        <asp:CheckBoxList ID="chkGameCharacterType" runat="server" DataSourceID="dsGameCharacterType"
                                            RepeatDirection="horizontal" DataTextField="CODE_NM" DataValueField="CODE">
                                        </asp:CheckBoxList>
                                    </td>
                                    <td class="tdHeaderStyle">
                                        ゲーム登録日
                                    </td>
                                    <td class="tdDataStyle">
                                        <asp:TextBox ID="txtGameRegistDateFrom" runat="server" MaxLength="10" Width="62px"></asp:TextBox>〜
                                        <asp:TextBox ID="txtGameRegistDateTo" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
                                        <asp:RangeValidator ID="vdrGameRegistDateFrom" runat="server" ErrorMessage="ゲーム登録日Fromを正しく入力して下さい。"
                                            ControlToValidate="txtGameRegistDateFrom" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
                                            Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                                        <asp:RangeValidator ID="vdrGameRegistDateTo" runat="server" ErrorMessage="ゲーム登録日Toを正しく入力して下さい。"
                                            ControlToValidate="txtGameRegistDateTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
                                            Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                                        <asp:CompareValidator ID="vdcGameRegistDateFrom" runat="server" ErrorMessage="大小関係が正しくありません。"
                                            ControlToCompare="txtGameRegistDateFrom" ControlToValidate="txtGameRegistDateTo"
                                            Operator="GreaterThanEqual" ValidationGroup="Key" Display="Dynamic">*</asp:CompareValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdHeaderStyle">
                                        ｹﾞｰﾑｷｬﾗｸﾀｰﾚﾍﾞﾙ
                                    </td>
                                    <td class="tdDataStyle">
                                        <asp:TextBox ID="txtGameCharacterLevelFrom" runat="server" MaxLength="4" Width="35px"></asp:TextBox>〜
                                        <asp:TextBox ID="txtGameCharacterLevelTo" runat="server" MaxLength="4" Width="35px"></asp:TextBox>
                                        <asp:RangeValidator ID="vdrGameCharacterLevelFrom" runat="server" ErrorMessage="ｹﾞｰﾑｷｬﾗｸﾀｰﾚﾍﾞﾙFromを正しく入力して下さい。"
                                            ControlToValidate="txtGameCharacterLevelFrom" MaximumValue="9999" MinimumValue="0"
                                            Type="Integer" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                                        <asp:RangeValidator ID="vdrGameCharacterLevelTo" runat="server" ErrorMessage="ｹﾞｰﾑｷｬﾗｸﾀｰﾚﾍﾞﾙToを正しく入力して下さい。"
                                            ControlToValidate="txtGameCharacterLevelTo" MaximumValue="999" MinimumValue="0"
                                            Type="Integer" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                                        <asp:CompareValidator ID="vdcGameCharacterLevelFrom" runat="server" ErrorMessage="大小関係が正しくありません。"
                                            ControlToCompare="txtGameCharacterLevelFrom" ControlToValidate="txtGameCharacterLevelTo"
                                            Operator="GreaterThanEqual" ValidationGroup="Key" Type="Integer" Display="Dynamic">*</asp:CompareValidator>
                                    </td>
                                    <td class="tdHeaderStyle2">
                                        お金
                                    </td>
                                    <td class="tdDataStyle">
                                        <asp:TextBox ID="txtGamePointFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>〜
                                        <asp:TextBox ID="txtGamePointTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
                                        <asp:RangeValidator ID="vdrGamePointFrom" runat="server" ErrorMessage="お金の開始値を正しく入力して下さい。"
                                            ControlToValidate="txtGamePointFrom" MaximumValue="9999999999" MinimumValue="0" Type="Double"
                                            ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                                        <asp:RangeValidator ID="vdrGamePointTo" runat="server" ErrorMessage="お金の終了値を正しく入力して下さい。"
                                            ControlToValidate="txtGamePointTo" MaximumValue="9999999999" MinimumValue="0" Type="Double"
                                            ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                                        <asp:CompareValidator ID="vdcGamePointFrom" runat="server" ErrorMessage="大小関係が正しくありません。"
                                            ControlToCompare="txtGamePointFrom" ControlToValidate="txtGamePointTo" Operator="GreaterThanEqual"
                                            ValidationGroup="Key" Type="Double" Display="Dynamic">*</asp:CompareValidator>
                                    </td>
                                    <td class="tdHeaderStyle2">
                                        ゴールド
                                    </td>
                                    <td class="tdDataStyle">
                                        <asp:TextBox ID="txtCastGamePointFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>〜
                                        <asp:TextBox ID="txtCastGamePointTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
                                        <asp:RangeValidator ID="vdrCastGamePointFrom" runat="server" ErrorMessage="ゴールドの開始値を正しく入力して下さい。"
                                            ControlToValidate="txtCastGamePointFrom" MaximumValue="9999999999" MinimumValue="0" Type="Double"
                                            ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                                        <asp:RangeValidator ID="vdrCastGamePointTo" runat="server" ErrorMessage="ゴールドの終了値を正しく入力して下さい。"
                                            ControlToValidate="txtCastGamePointTo" MaximumValue="9999999999" MinimumValue="0" Type="Double"
                                            ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                                        <asp:CompareValidator ID="vdcCastGamePointFrom" runat="server" ErrorMessage="大小関係が正しくありません。"
                                            ControlToCompare="txtCastGamePointFrom" ControlToValidate="txtCastGamePointTo"
                                            Operator="GreaterThanEqual" ValidationGroup="Key" Type="Double" Display="Dynamic">*</asp:CompareValidator>
                                    </td>
                                </tr>
                                <tr>
									<td class="tdHeaderStyle2">
										保護者の電話番号
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtGuardianTel" runat="server" MaxLength="11" Width="80px"></asp:TextBox>
									</td>
								</tr>
                            </table>
						</div>
					</asp:PlaceHolder>
                    <asp:PlaceHolder ID="plcSelectColumns" runat="server" Visible="false">
                        <div style="padding-top: 5px; padding-bottom: 3px">
                            <table border="0" class="tableStyle">
                                <tr>
                                    <td class="tdHeaderStyle2">
                                        表示項目
                                    </td>
                                    <td class="tdDataStyle">
                                        <asp:CheckBoxList ID="chkSelectColumns" runat="server" DataSource='<%# ColumnsArray %>' />
                                        <asp:CheckBox ID="chkSelectColumnsVisible" runat="server" Visible="false" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:PlaceHolder>
				</asp:Panel>
				<table border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td>
							<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="True" ValidationGroup="Key" />
							<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
							ﾌｨﾙﾀｰ：
							<asp:DropDownList runat="server" ID="lstSeekCondition" AutoPostBack="true" OnSelectedIndexChanged="lstSeekCondition_SelectedIndexChanged">
							</asp:DropDownList>
							<asp:LinkButton ID="lnkEditSeekCondition" runat="server" Text="編集" OnClick="lnkEditSeekCondition_Click" CausesValidation="True" ValidationGroup="Key"></asp:LinkButton>
							<asp:LinkButton ID="lnkAppendSeekCondition" runat="server" Text="追加" OnClick="lnkAppendSeekCondition_Click" CausesValidation="True" ValidationGroup="Key"></asp:LinkButton>
							&nbsp;
						</td>
						<td>
							<asp:Button runat="server" ID="btnPickup" Visible="False" Text="ピックアップ作成" CssClass="seekbutton" OnClick="btnPickup_Click" />
							<asp:Button runat="server" ID="btnSetupMail" Text="お知らせメール送信設定" CssClass="seekbutton" OnClick="btnSetupMail_Click" CausesValidation="False" />
							<asp:Button runat="server" ID="btnApproachSetupMail" Text="アプローチメール送信設定" CssClass="seekbutton" OnClick="btnApproachSetupMail_Click" CausesValidation="False" />
							<asp:Button runat="server" ID="btnCleaningMail" Text="クリーニングメール送信設定" CssClass="seekbutton" OnClick="btnCleaningMail_Click" CausesValidation="False" />
							<asp:Button runat="server" ID="btnToResignedMail" Text="退会者メール送信設定" CssClass="seekbutton" OnClick="btnToResignedMail_Click" CausesValidation="False" />
							<asp:Button runat="server" ID="btnSetupRank" Text="ランク設定" CssClass="seekbutton" OnClick="btnSetupRank_Click" CausesValidation="False" />
							<asp:Button runat="server" ID="btnSetupBulkUpdate" Text="一括更新" CssClass="seekbutton" OnClick="btnSetupBulkUpdate_Click" CausesValidation="False" />
							<asp:LinkButton ID="lnkSelectColumns" runat="server" Text="[一覧項目選択表示]" CssClass="seekbutton" OnClick="lnkSelectColumns_Click" CausesValidation ="false" />
						</td>
					</tr>
					<tr>
						<td>
							<asp:Label ID="lblCsvPassword" runat="server" Text="ﾊﾟｽﾜｰﾄﾞ:"></asp:Label>
							<asp:TextBox ID="txtCsvPassword" runat="server" MaxLength="20" Width="140px" TextMode="Password"></asp:TextBox>
							<asp:Button ID="btnCSV" runat="server" Text="CSV出力" OnClick="btnCSV_Click" CausesValidation="False" />
						</td>
						<td>
						</td>
					</tr>
				</table>
			</fieldset>
		</asp:Panel>
		<asp:Panel ID="pnlSeekConditionMainte" runat="server">
			<fieldset>
				<legend>[検索条件保存]</legend>
				<table border="0" class="tableStyle">
					<tr>
						<td class="tdHeaderSmallStyle">
							ﾌｨﾙﾀｰ名
						</td>
						<td class="tdDataStyle">
							<asp:HiddenField ID="hdnSeekConditionSeq" runat="server">
							</asp:HiddenField>
							<asp:HiddenField ID="hdnSeekConditionRevisionNo" runat="server">
							</asp:HiddenField>
							<asp:TextBox ID="txtSeekConditionNm" runat="server" Width="300px" MaxLength="32"></asp:TextBox>
							<asp:RequiredFieldValidator ID="vdrSeekConditionNm" runat="server" ErrorMessage="検索条件名を入力して下さい。" ControlToValidate="txtSeekConditionNm" ValidationGroup="SaveSeekCondition">*</asp:RequiredFieldValidator>
							<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender16" TargetControlID="vdrSeekConditionNm" HighlightCssClass="validatorCallout" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderSmallStyle2">
							公開
						</td>
						<td class="tdDataStyle">
							<asp:CheckBox ID="chkSeekConditionPublish" runat="server" Text="他の管理者と共有する">
							</asp:CheckBox>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnSaveSeekCondition" Text="更新" CssClass="seekbutton" OnClick="btnSaveSeekCondition_Click" ValidationGroup="SaveSeekCondition" />
				<asp:Button runat="server" ID="btnDeleteSeekCondition" Text="削除" CssClass="seekbutton" CausesValidation="False" OnClick="btnDeleteSeekCondition_Click" />
				<asp:Button runat="server" ID="btnCancelSeekCondition" Text="中止" CssClass="seekbutton" CausesValidation="False" OnClick="btnCancelSeekCondition_Click" />
				<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDeleteSeekCondition" ConfirmText="検索条件設定の削除を実行しますか？"
					ConfirmOnFormSubmit="true" />
				<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender3" runat="server" TargetControlID="btnSaveSeekCondition" ConfirmText="検索条件設定の更新を実行しますか？" ConfirmOnFormSubmit="true" />
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlTxMail">
			<fieldset>
				<legend>[メール送信設定]</legend>
				<table border="0" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle" id="tdHeaderMailType" runat="server">
							ﾒｰﾙ種別
						</td>
						<td class="tdDataStyle" colspan="3">
							<asp:DropDownList ID="lstMailTemplateNo" runat="server" Width="309px" DataSourceID="dsCastMailMagazine" DataTextField="TEMPLATE_NM" DataValueField="MAIL_TEMPLATE_NO"
								AutoPostBack="true" OnSelectedIndexChanged="lstMailTemplateNo_SelectedIndexChanged">
							</asp:DropDownList>
						</td>
					</tr>
					<asp:PlaceHolder ID="plcMailSendType" runat="server" Visible="true">
						<tr>
							<td class="tdHeaderStyle" ID="tdHeaderMailSendType" runat="server">
								ﾒｰﾙ送信種別
							</td>
							<td class="tdDataStyle">
								<asp:RadioButtonList ID="rdoMailSendType" runat="server" DataSourceID="dsMailSendType" DataTextField="CODE_NM" DataValueField="CODE" RepeatDirection="Horizontal">
								</asp:RadioButtonList>
							</td>
							<td class="tdHeaderStyle2" runat="server">
								ﾒｰﾙｻｰﾊﾞｰ
							</td>
							<td class="tdDataStyle">
								<asp:RadioButtonList ID="rdoMailServer" runat="server" RepeatDirection="Horizontal">
									<asp:ListItem Text="BEAM" Value="1" Selected="True"></asp:ListItem>
									<asp:ListItem Text="MULLER3" Value="2"></asp:ListItem>
								</asp:RadioButtonList>
							</td>
						</tr>
					</asp:PlaceHolder>
					<tr runat="server" id="trManLoginId">
						<td class="tdHeaderStyle2">
							会員ID
						</td>
						<td class="tdDataStyle" colspan="3">
							<asp:TextBox ID="txtManLoginId" runat="server" MaxLength="8" Width="60px"></asp:TextBox>
							<asp:CustomValidator ID="vdcManLoginId" runat="server" ControlToValidate="txtManLoginId" ErrorMessage="会員ログインＩＤが未登録です。" OnServerValidate="vdcManLoginId_ServerValidate"
								ValidationGroup="TxMail" Display="Dynamic" ValidateEmptyText="True"></asp:CustomValidator>
							<asp:Label ID="lblManHandleNm" runat="server" Text=""></asp:Label>
						</td>
					</tr>
					<asp:PlaceHolder ID="plcReservationMail" runat="server" Visible="false">
						<tr>
							<td class="tdHeaderStyle2">
								送信予定日時
							</td>
							<td class="tdDataStyle" colspan="3">
								<asp:TextBox ID="txtReservationSendDay" runat="server" MaxLength="10" Width="62px"></asp:TextBox>&nbsp;
								<asp:TextBox ID="txtReservationSendHour" runat="server" Width="15px" MaxLength="2"></asp:TextBox>時
								<asp:TextBox ID="txtReservationSendMinute" runat="server" Width="15px" MaxLength="2"></asp:TextBox>分
								<asp:RangeValidator ID="vdrReservationSendHour" runat="server" ErrorMessage="送信予定時間を正しく入力して下さい。" ControlToValidate="txtReservationSendHour" MaximumValue="23" MinimumValue="00"
									Type="Integer" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:RangeValidator ID="vdrReservationSendMinute" runat="server" ErrorMessage="送信予定分を正しく入力して下さい。" ControlToValidate="txtReservationSendMinute" MaximumValue="59" MinimumValue="00"
									Type="Integer" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<asp:RangeValidator ID="vdrReservationSendDay" runat="server" ErrorMessage="送信予定日を正しく入力して下さい。" ControlToValidate="txtReservationSendDay" MaximumValue="2099/12/31"
									MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
								<ajaxToolkit:MaskedEditExtender ID="mskReservationSendDay" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
									TargetControlID="txtReservationSendDay"></ajaxToolkit:MaskedEditExtender>
								<ajaxToolkit:FilteredTextBoxExtender ID="fltReservationSendHour" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtReservationSendHour" />
								<ajaxToolkit:FilteredTextBoxExtender ID="fltReservationSendMinute" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtReservationSendMinute" />
							</td>
						</tr>
					</asp:PlaceHolder>
				</table>
				<asp:Label ID="lblAttention" runat="server" Text="" Font-Size="Small" ForeColor="Red"></asp:Label><br />
				<asp:Label ID="lblAttentionCleaning" runat="server" Text="クリーニングメールはフィルタリングエラーの出演者にのみ送信されます。" Font-Size="Small" ForeColor="Red" Visible="false"></asp:Label><br />
				<asp:Label ID="lblAttentionToResigned" runat="server" Text="検索結果に退会者以外が含まれる場合、退会者以外にも送信されます。" Font-Size="Small" ForeColor="Red" Visible="false"></asp:Label><br />
				<asp:Button runat="server" ID="btnTxMail" Text="メール送信" CssClass="seekbutton" OnClick="btnTxMail_Click" ValidationGroup="TxMail" />
				<asp:Button runat="server" ID="btnTxReservationMail" Text="予約送信" CssClass="seekbutton" OnClick="btnTxReservationMail_Click" ValidationGroup="TxMail" Visible="false" />
				<asp:Button runat="server" ID="btnCancelTx" Text="中止" CssClass="seekbutton" CausesValidation="False" OnClick="btnCancelTx_Click" />&nbsp;
				<asp:LinkButton ID="lnkMailTemplate" runat="server" CssClass="seekbutton" OnClick="lnkMailTemplate_Click" Text="[プレビュー表示]" />
				&nbsp;
				<asp:Panel ID="pnlMailTemplate" runat="server">
					<br />
					<fieldset class="fieldset-inner">
						<legend>[メールプレビュー]</legend>
						<asp:GridView ID="grdMailTemplate" runat="server" AllowPaging="False" AutoGenerateColumns="False" DataSourceID="" SkinID="GridView" Width="230px" ShowHeader="False">
							<Columns>
								<asp:TemplateField>
									<ItemTemplate>
										<asp:Literal ID="lblHtmlDoc1" runat="server" Text='<%# ReplaceForm(string.Format("<table class=\"userColor\" ><tr><td width=\"220px\">{0}</tr></td></table>", ViComm.ViCommConst.FLAG_OFF_STR.Equals(Eval("TEXT_MAIL_FLAG").ToString()) ? string.Concat(Eval("HTML_DOC1"), Eval("HTML_DOC2"), Eval("HTML_DOC3"), Eval("HTML_DOC4"), Eval("HTML_DOC5"), Eval("HTML_DOC6"),Eval("HTML_DOC7"), Eval("HTML_DOC8") ,Eval("HTML_DOC9"), Eval("HTML_DOC10")) : string.Concat(Eval("HTML_DOC11"), Eval("HTML_DOC12"), Eval("HTML_DOC13"), Eval("HTML_DOC14"), Eval("HTML_DOC15"), Eval("HTML_DOC16"),Eval("HTML_DOC17"), Eval("HTML_DOC18") ,Eval("HTML_DOC19"), Eval("HTML_DOC20")))) %>'></asp:Literal>
									</ItemTemplate>
									<ItemStyle HorizontalAlign="Left" />
								</asp:TemplateField>
							</Columns>
						</asp:GridView>
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlBulkUpdate">
			<fieldset>
				<legend>[一括更新]</legend>
				<ajaxToolkit:TabContainer ID="tabBulkUpdate" runat="server" Width="650px">
					<ajaxToolkit:TabPanel ID="tpgBulkUpdateUserStatus" runat="server" HeaderText="会員状態">
						<ContentTemplate>
							<fieldset>
								<table border="0" style="width: 95%" class="tableStyle">
									<tr>
										<td class="tdHeaderStyle2">
											会員状態
										</td>
										<td class="tdDataStyle">
											<asp:DropDownList ID="lstUserStatus" runat="server" Width="206px" DataSourceID="dsUserStatus" DataTextField="CODE_NM" DataValueField="CODE">
											</asp:DropDownList>
										</td>
									</tr>
								</table>
								<asp:Button runat="server" ID="btnBulkUpdUserStatus" Text="更新" CssClass="seekbutton" ValidationGroup="BULK_UPD_USER_STATUS" OnClick="btnExecuteBulkUpdate_Click" />
								<asp:Button runat="server" ID="btnCancelBulkUpdUserStatus" Text="中止" CssClass="seekbutton" OnClick="btnCancelBulkUpdate_Click" CausesValidation="False" />
								<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender4" runat="server" TargetControlID="btnBulkUpdUserStatus" ConfirmText="会員状態の一括更新を行います。よろしいですか？"
									ConfirmOnFormSubmit="true" />
							</fieldset>
						</ContentTemplate>
					</ajaxToolkit:TabPanel>
					<ajaxToolkit:TabPanel ID="tpgBulkUpdateAdCd" runat="server" HeaderText="広告コード">
						<ContentTemplate>
							<fieldset>
								<table border="0" style="width: 95%" class="tableStyle">
									<tr>
										<td class="tdHeaderStyle2">
											広告コード
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtAdCd4BulkUpd" runat="server" MaxLength="32" Width="200px"></asp:TextBox>
											<asp:RegularExpressionValidator ID="vdeAdCd4BulkUpd" runat="server" ErrorMessage="広告コードは32桁以下の英数字で入力して下さい。" ValidationExpression="[a-zA-Z0-9-\/_]{1,32}"
												ControlToValidate="txtAdCd4BulkUpd" ValidationGroup="BULK_UPD_AD_CD" Display="Dynamic">*</asp:RegularExpressionValidator>
											<asp:CustomValidator ID="vdcAdCd4BulkUpd" runat="server" ControlToValidate="txtAdCd4BulkUpd" ErrorMessage="広告コードが未登録です。" OnServerValidate="vdcBulkUpdAdCd_ServerValidate"
												ValidationGroup="BULK_UPD_AD_CD" Display="Dynamic">*</asp:CustomValidator>
											<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender20" TargetControlID="vdeAdCd4BulkUpd" HighlightCssClass="validatorCallout" />
											<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender21" TargetControlID="vdcAdCd4BulkUpd" HighlightCssClass="validatorCallout" />
										</td>
									</tr>
								</table>
								<asp:Button runat="server" ID="btnBulkUpdAdCd" Text="更新" CssClass="seekbutton" ValidationGroup="BULK_UPD_AD_CD" OnClick="btnExecuteBulkUpdate_Click" />
								<asp:Button runat="server" ID="btnCancelBulkUpdAdCd" Text="中止" CssClass="seekbutton" OnClick="btnCancelBulkUpdate_Click" CausesValidation="False" />
								<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender5" runat="server" TargetControlID="btnBulkUpdAdCd" ConfirmText="広告コードの一括更新を行います。よろしいですか？" ConfirmOnFormSubmit="true" />
							</fieldset>
						</ContentTemplate>
					</ajaxToolkit:TabPanel>
					<ajaxToolkit:TabPanel ID="tpgBulkUpdateMailFlag" runat="server" HeaderText="メールアドレスＮＧ解除">
						<ContentTemplate>
							<fieldset>
								<font color="red">※メールアドレスNGを解除します。</font><br />
								<asp:Button runat="server" ID="btnBulkUpdMailFlag" Text="解除する" CssClass="seekbutton" ValidationGroup="BULK_UPD_MAIL_FLAG" OnClick="btnExecuteBulkUpdate_Click" />
								<asp:Button runat="server" ID="btnCancelBulkUpdMailFlag" Text="中止" CssClass="seekbutton" OnClick="btnCancelBulkUpdate_Click" CausesValidation="False" />
								<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender6" runat="server" TargetControlID="btnBulkUpdMailFlag" ConfirmText="ﾒｰﾙｱﾄﾞﾚｽNGの一括解除を行います。よろしいですか？"
									ConfirmOnFormSubmit="true" />
							</fieldset>
						</ContentTemplate>
					</ajaxToolkit:TabPanel>
					<ajaxToolkit:TabPanel ID="TabPanel1" runat="server">
						<HeaderTemplate>
							<font color="red">削除</font>
						</HeaderTemplate>
						<ContentTemplate>
							<fieldset>
								<font color="red"><b>注意）一括削除を行います。削除したデータは元に戻すことが出来ません。</b></font><br />
								<br />
								<asp:CheckBox runat="server" ID="chkBulkDeleteCharacter" Text="キャラクターのみ削除(出演者基本情報は削除しない)" Checked="true">
								</asp:CheckBox><br />
								<asp:Button runat="server" ID="btnBulkDelete" Text="削除する" CssClass="seekbutton" ValidationGroup="BULK_DELETE" OnClick="btnExecuteBulkUpdate_Click" BackColor="red" />
								<asp:Button runat="server" ID="btnCancelBulkDelete" Text="中止" CssClass="seekbutton" OnClick="btnCancelBulkUpdate_Click" CausesValidation="False" />
								<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender7" runat="server" TargetControlID="btnBulkDelete" ConfirmText="条件に該当するデータの一括削除を行います。よろしいですか？"
									ConfirmOnFormSubmit="true" />
							</fieldset>
						</ContentTemplate>
					</ajaxToolkit:TabPanel>
				</ajaxToolkit:TabContainer>
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlRank">
			<fieldset>
				<legend>[ランク更新]</legend>
				<table border="0" class="tableStyle">
					<tr>
						<td class="tdHeaderSmallStyle2">
							ﾕｰｻﾞｰﾗﾝｸ
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstUserRank" runat="server" Width="100px" DataSourceID="dsCastRank" DataTextField="USER_RANK_NM" DataValueField="USER_RANK">
							</asp:DropDownList>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnUpdRank" Text="ランク更新" CssClass="seekbutton" OnClick="btnUpdRank_Click" ValidationGroup="UpdRank" />
				<asp:Button runat="server" ID="btnCancelRank" Text="中止" CssClass="seekbutton" CausesValidation="False" OnClick="btnCancelRank_Click" />
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlPickup">
			<fieldset>
				<legend>[ピックアップ設定]</legend>
				<table border="0" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							ピックアップ種別
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstPickup" runat="server" Width="240px" DataSourceID="dsPickup" DataTextField="PICKUP_TITLE" DataValueField="PICKUP_ID">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							ピックアップ追加・更新
						</td>
						<td class="tdDataStyle">
							<asp:RadioButton ID="rdoAppend" runat="server" Text="既存ﾋﾟｯｸｱｯﾌﾟに追加する" GroupName="List" />
							<asp:RadioButton ID="rdoReplace" runat="server" Text="既存ﾋﾟｯｸｱｯﾌﾟを更新する" GroupName="List" />
						</td>
					</tr>
				</table>
				<div align="center">
					<asp:Label ID="lblNoCheckError" runat="server" Text="ピックアップ出演者が一人も選択されていません。" Font-Bold="True" Font-Size="Small" ForeColor="Red" Visible="False"></asp:Label>
				</div>
				<asp:Button runat="server" ID="btnPickupDetail" Text="ピックアップ更新" CssClass="seekbutton" OnClick="btnPickupDetail_Click" ValidationGroup="Pickup" />
				<asp:Button runat="server" ID="Button1" Text="中止" CssClass="seekbutton" CausesValidation="False" OnClick="btnCancelPickup_Click" />
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>
					<%= DisplayWordUtil.Replace("[出演者一覧]") %>
				</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<asp:Label ID="lblRecCount" runat="server" Text="<%# GetRecCount() %>" Visible="false" />
					<a class="reccount">Record Count
						<%#GetRecCount() %>
					</a>
					<br />
					<a class="reccount">Current viewing page
						<%=grdCast.PageIndex + 1%>
						of
						<%=grdCast.PageCount%>
					</a>
				</asp:Panel>
				&nbsp;
				<asp:LinkButton ID="lnkCondition" runat="server" OnClick="lnkCondition_Click" Text="[条件非表示]" />
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="290px">
					<asp:GridView ID="grdCast" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCastCharacter" AllowSorting="True" SkinID="GridView"
						OnDataBound="grdCast_DataBound" OnRowDataBound="grdCast_RowDataBound" OnSorting="grdCast_Sorting" HeaderStyle-Wrap="false" RowStyle-Wrap="false">
						<Columns>
							<asp:TemplateField HeaderText="選択">
								<ItemTemplate>
									<asp:CheckBox ID="chkPickup" runat="server" />
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ID/ｷｬﾗｸﾀｰNo" SortExpression="LOGIN_ID">
								<ItemTemplate>
									<asp:Label ID="lblCastLoginId" runat="server" Text='<%# Eval("LOGIN_ID") %>'></asp:Label>
									<asp:Label ID="lblUserSeq" runat="server" Text='<%# Eval("USER_SEQ") %>' Visible="false"></asp:Label>
									<asp:Label ID="lblUserCharNo" runat="server" Text='<%# string.Format("- {0}",Eval("USER_CHAR_NO")) %>'></asp:Label><br />
									<asp:Label ID="lblCallCount" runat="server" Text='<%# GetCallCount(Eval("SITE_CD"),Eval("USER_SEQ"),Eval("USER_CHAR_NO"),Eval("LOGIN_SEQ")) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="94px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="写真" SortExpression="PROFILE_PIC_SEQ">
								<ItemTemplate>
									<a href="<%# PickupHelper.GeneratePickupObjsViewerUrl(this, Eval("SITE_CD").ToString(),Eval("USER_SEQ").ToString(), ViComm.ViCommConst.PicupTypes.CAST_CHARACTER) %>">
										<asp:Image ID="imgProfilePic" runat="server" ImageUrl='<%# string.Format("../{0}",Eval("SMALL_PHOTO_IMG_PATH").ToString()) %>'>
										</asp:Image>
									</a>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="カテゴリ" SortExpression="ACT_CATEGORY_SEQ">
								<ItemTemplate>
									<asp:Label ID="lblActCategory" runat="server" Text='<%# Bind("ACT_CATEGORY_NM") %>'></asp:Label>
									<span style="color: Teal">
										<asp:Label ID="lblPickup" runat="server" Text='<%# GetPickupMask(Eval("PICKUP_MASK")) %>'></asp:Label>
									</span>
									<br />
									<asp:Label ID="lblTalkType" runat="server" Text='<%# Eval("CONNECT_TYPE_NM") %>'></asp:Label>
									<asp:Label ID="lblTerminalType" runat="server" Text='<%# Eval("USE_TERMINAL_TYPE_NM") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle Width="138px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="プロダクション" SortExpression="PRODUCTION_SEQ">
								<ItemTemplate>
									<asp:Label ID="lblProductionNm" runat="server" Text='<%# Eval("PRODUCTION_NM") %>'>
									</asp:Label><br />
									<asp:Label ID="lblManagerNm" runat="server" Text='<%# Eval("MANAGER_NM") %>'></asp:Label><br />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="氏名/ハンドル名" SortExpression="CAST_NM">
								<ItemTemplate>
									<asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%# string.Format("CastView.aspx?loginid={0}&return={1}",Eval("LOGIN_ID"),"CastCharacterInquiry.aspx") %>'
										Text='<%# Eval("CAST_NM") %>' Enabled='<%#IsAreaManager(Eval("MANAGER_SEQ")) %>'></asp:HyperLink>
									<br />
									<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# string.Format("CastView.aspx?loginid={0}&return={1}",Eval("LOGIN_ID"),"CastCharacterInquiry.aspx") %>'
										Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>' Enabled='<%#IsAreaManager(Eval("MANAGER_SEQ")) %>'></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle Width="120px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="会員状態" SortExpression="USER_STATUS">
								<ItemTemplate>
									<asp:Label ID="lblUserStatusNm" runat="server" Text='<%# Eval("USER_STATUS_NM") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle Width="60px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="オンライン" SortExpression="CHARACTER_ONLINE_STATUS">
								<ItemTemplate>
									<asp:Image ID="imgSysOnlineImg" runat="server" ImageUrl='<%# Eval("SYS_ONLINE_IMG_PATH","../{0}") %>'>
									</asp:Image><br />
									<asp:Label ID="lblOnlineSite" runat="server" Text='<%# GetOnlineSite(Eval("USER_SEQ")) %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="会話端末">
								<ItemTemplate>
									<asp:Label ID="lblTalkTerm" runat="server" Text='<%# Eval("TALK_TERM") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Left" Width="50px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="お気に入られ数" SortExpression="FAVORIT_ME_COUNT">
								<ItemTemplate>
									<asp:Label ID="lblFavoriteMeCount" runat="server" Text='<%# Eval("FAVORIT_ME_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" Width="50px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="お気に入り数">
								<ItemTemplate>
									<asp:Label ID="lblFavoriteCount" runat="server" Text='<%# Eval("FAVORITE_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" Width="50px" />
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
			<br />
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsCastCharacter" runat="server" SelectMethod="GetPageCollection" TypeName="CastCharacter" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsCastCharacter_Selected" OnSelecting="dsCastCharacter_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pOnline" Type="String" />
			<asp:Parameter Name="pLoginId" Type="String" />
			<asp:Parameter Name="pProductionSeq" Type="String" />
			<asp:Parameter Name="pCastNm" Type="String" />
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pTalkType" Type="String" />
			<asp:Parameter Name="pTerminalType" Type="String" />
			<asp:Parameter Name="pHandleNm" Type="String" />
			<asp:Parameter Name="pActCategorySeq" Type="String" />
			<asp:Parameter Name="pEmailAddr" Type="String" />
			<asp:Parameter Name="pPicType" Type="String" />
			<asp:Parameter Name="pNonExistMailAddrFlagMask" Type="Int16" />
			<asp:Parameter Name="pMainCharOnlyFlag" Type="String" DefaultValue="1" />
			<asp:Parameter Name="pManagerSeq" Type="String" />
			<asp:Parameter Name="pRegistDayFrom" Type="String" />
			<asp:Parameter Name="pRegistDayTo" Type="String" />
			<asp:Parameter Name="pLastLoginDayFrom" Type="String" />
			<asp:Parameter Name="pLastLoginDayTo" Type="String" />
			<asp:Parameter Name="pTotalPaymentAmtFrom" Type="String" />
			<asp:Parameter Name="pTotalPaymentAmtTo" Type="String" />
			<asp:Parameter Name="pNaFlag" Type="String" />
			<asp:Parameter Name="pUserRank" Type="String" />
			<asp:Parameter Name="pAdCd" Type="String" />
			<asp:Parameter Name="pTel" Type="String" />
			<asp:Parameter Name="pCastAttrValue1" Type="String" />
			<asp:Parameter Name="pCastAttrValue2" Type="String" />
			<asp:Parameter Name="pCastAttrValue3" Type="String" />
			<asp:Parameter Name="pCastAttrValue4" Type="String" />
			<asp:Parameter Name="pAgeFrom" />
			<asp:Parameter Name="pAgeTo" />
			<asp:Parameter Name="pFavoritMeCountFrom" />
			<asp:Parameter Name="pFavoritMeCountTo" />
			<asp:Parameter Name="pUserStatusMask" />
			<asp:Parameter Name="pPickupId" />
			<asp:Parameter Name="pGuid" Type="String" />
			<asp:Parameter Name="pRemarks1" Type="String" />
			<asp:Parameter Name="pEnabledBlogFlag" Type="String" />
			<asp:Parameter Name="pEnabledRichinoFlag" Type="String" />
			<asp:Parameter Name="pSiteUseStatus" Type="String" />
			<asp:Parameter Name="pInfoMailRxType" Type="String" />
			<asp:Parameter Name="pUserMailRxType" Type="String" />
			<asp:Parameter Name="pReportAffiliateDateFrom" Type="String" />
			<asp:Parameter Name="pReportAffiliateDateTo" Type="String" />
			<asp:Parameter Name="pUserDefineMask" Type="String" />
            <asp:Parameter Name="pExceptRefusedByManUserSeq" Type="String" />
			<asp:Parameter Name="pLastTxMailDateFrom" Type="String" />
			<asp:Parameter Name="pLastTxMailDateTo" Type="String" />
            <asp:Parameter Name="pGameHandleNm" Type="String" />
            <asp:Parameter Name="pGameCharacterType" Type="String" />
            <asp:Parameter Name="pGameRegistDateFrom" Type="String" />
            <asp:Parameter Name="pGameRegistDateTo" Type="String" />
            <asp:Parameter Name="pGameCharacterLevelFrom" Type="String" />
            <asp:Parameter Name="pGameCharacterLevelTo" Type="String" />
            <asp:Parameter Name="pGamePointFrom" Type="String" />
            <asp:Parameter Name="pGamePointTo" Type="String" />
            <asp:Parameter Name="pCastGamePointFrom" Type="String" />
            <asp:Parameter Name="pCastGamePointTo" Type="String" />
            <asp:Parameter Name="pHandleNmExactFlag" Type="Boolean" />
            <asp:Parameter Name="pBeforeSystemId" Type="String" />
            <asp:Parameter Name="pKeyword" Type="String" DefaultValue="" />
            <asp:Parameter Name="pCarrier" Type="Int32" />
            <asp:Parameter Name="pLoginIdNegativeFlag" Type="Boolean" />
            <asp:Parameter Name="pLastActionDayNegativeFlag" Type="Boolean" />
            <asp:Parameter Name="pUserDefineMaskNegatieFlag" Type="Boolean" />
            <asp:Parameter Name="pIntroducerFriendAllFlag" Type="Boolean" />
            <asp:Parameter Name="pCrosmileLastUsedVersionFrom" Type="String" />
            <asp:Parameter Name="pCrosmileLastUsedVersionTo" Type="String" />
            <asp:Parameter Name="pUseCrosmileFlag" Type="String" />
            <asp:Parameter Name="pSortExpression" Type="String" />
            <asp:Parameter Name="pSortDirection" Type="String" />
            <asp:Parameter Name="pGuardianTel" Type="String" />
            <asp:Parameter Name="pUserSeq" Type="String" />
            <asp:Parameter Name="pUseVoiceappFlag" Type="String" />
            <asp:Parameter Name="pGcappLastLoginVersionFrom" Type="String" />
            <asp:Parameter Name="pGcappLastLoginVersionTo" Type="String" />
            <asp:Parameter Name="pUseJealousyFlag" Type="string" />
            <asp:Parameter Name="pFinalLoginDayFrom" Type="String" />
			<asp:Parameter Name="pFinalLoginDayTo" Type="String" />
			<asp:Parameter Name="pExcludeEmailAddr" Type="Boolean" />
			<asp:Parameter Name="pTxMuller3NgMailAddrFlag" Type="string" />
			<asp:Parameter Name="pNotPaymentAmtFrom" Type="String" />
			<asp:Parameter Name="pNotPaymentAmtTo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsProduction" runat="server" SelectMethod="GetList" TypeName="Production"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsManager" runat="server" SelectMethod="GetProductionList" TypeName="Manager" OnSelecting="dsManager_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pProductionSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsOnLine" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="52" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsConnectType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="61" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsActCategory" runat="server" SelectMethod="GetList" TypeName="ActCategory" OnSelecting="dsActCategory_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastMailMagazine" runat="server" SelectMethod="GetListByTemplateType" TypeName="MailTemplate" OnSelecting="dsCastMailMagazine_Selecting">
		<SelectParameters>
			<asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue" Type="String" />
			<asp:Parameter Name="pMailTemplateType" Type="String" />
			<asp:Parameter DefaultValue="2" Name="pSexCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsPickup" runat="server" SelectMethod="GetListLite" TypeName="Pickup">
		<SelectParameters>
			<asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue" Type="String" />
			<asp:Parameter Name="pPickupType" Type="String" DefaultValue="" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastRank" runat="server" SelectMethod="GetList" TypeName="CastRank"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsMailSendType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="28" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsNaFlag" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="98" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsMailTemplate" runat="server" SelectMethod="GetListByTemplateNo" TypeName="MailTemplate">
		<SelectParameters>
			<asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstMailTemplateNo" Name="pMailTemplateNo" PropertyName="SelectedValue" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsInfoMailRxType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="15" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUserStatus" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="51" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsUserDefineMask" runat="server" SelectMethod="GetList"
        TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="A1" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameCharacterType" runat="server" SelectMethod="GetList"
        TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="A4" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilterNum1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtTotalPaymentAmtFrom" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum2" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtTotalPaymentAmtTo" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtGameCharacterLevelFrom" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtGameCharacterLevelTo" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtGamePointFrom" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtGamePointTo" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtCastGamePointFrom" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtCastGamePointTo" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtUserSeq" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" Enabled="true" FilterType="Numbers,Custom" ValidChars="." TargetControlID="txtGcappLastLoginVersionFrom" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" Enabled="true" FilterType="Numbers,Custom" ValidChars="." TargetControlID="txtGcappLastLoginVersionTo" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" Enabled="true" FilterType="Numbers,Custom" ValidChars="-" TargetControlID="txtNotPaymentAmtFrom" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" Enabled="true" FilterType="Numbers,Custom" ValidChars="-" TargetControlID="txtNotPaymentAmtTo" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrRegistDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrRegistDayTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdcRegistDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdrLastLoginDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdrLastLoginDayTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdcLastLoginDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vdcTotalPaymentAmtFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8" TargetControlID="vdeAdCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9" TargetControlID="vdcAdCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10" TargetControlID="vdrAgeFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11" TargetControlID="vdrAgeTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender12" TargetControlID="vdcAgeFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender13" TargetControlID="vdrFavoritMeCountFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender14" TargetControlID="vdrFavoritMeCountTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender15" TargetControlID="vdcFavoritMeCountFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender17" TargetControlID="vdrReportAffiliateDateFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender18" TargetControlID="vdrReportAffiliateDateTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender19" TargetControlID="vdcReportAffiliateDateFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender22" TargetControlID="vdrGameRegistDateFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender23" TargetControlID="vdrGameRegistDateTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender24" TargetControlID="vdcGameRegistDateFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender25" TargetControlID="vdrGamePointFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender26" TargetControlID="vdrGamePointTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender27" TargetControlID="vdcGamePointFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender28" TargetControlID="vdrCastGamePointFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender29" TargetControlID="vdrCastGamePointTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender30" TargetControlID="vdcCastGamePointFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender31" TargetControlID="vdrLastTxMailDateFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender32" TargetControlID="vdrLastTxMailDateTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender33" TargetControlID="vdcLastTxMailDateFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender34" TargetControlID="vdcAdCdList" HighlightCssClass="validatorCallout" />
	
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender37" TargetControlID="vdrFinalLoginDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender38" TargetControlID="vdrFinalLoginDayTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender39" TargetControlID="vdcFinalLoginDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender40" TargetControlID="vdcNotPaymentAmtFrom" HighlightCssClass="validatorCallout" />
	
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnTxMail" ConfirmText="メールの配信を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:MaskedEditExtender ID="mskRegistDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtRegistDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskRegistDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtRegistDayTo">
	</ajaxToolkit:MaskedEditExtender>
	
	<ajaxToolkit:MaskedEditExtender ID="mskFinalLoginDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtFinalLoginDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskFinalLoginDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtFinalLoginDayTo">
	</ajaxToolkit:MaskedEditExtender>
	
	<ajaxToolkit:MaskedEditExtender ID="mskLastLoginDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtLastLoginDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskLastLoginDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtLastLoginDayTo">
	</ajaxToolkit:MaskedEditExtender>
	
	<ajaxToolkit:MaskedEditExtender ID="mskReportAffiliateDateFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtReportAffiliateDateFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskReportAffiliateDateTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtReportAffiliateDateTo">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskGameRegistDateFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtGameRegistDateFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskGameRegistDateTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtGameRegistDateTo">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskLastTxMailDateFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtLastTxMailDateFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskLastTxMailDateTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtLastTxMailDateTo">
	</ajaxToolkit:MaskedEditExtender>
</asp:Content>
