﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 動画アップロード設定
--	Progaram ID		: MovieUpload
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  2010/06/08  Kazuaki.Itoh 動画をキャリアにあわせて分割

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Cast_MovieUpload:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		ViewState["RETURN"] = iBridUtil.GetStringValue(Request.QueryString["return"]);
		lblSiteCd.Text = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		lblUserSeq.Text = iBridUtil.GetStringValue(Request.QueryString["userseq"]);
		lblUserCharNo.Text = iBridUtil.GetStringValue(Request.QueryString["usercharno"]);
		ViewState["SITE_CD"] = lblSiteCd.Text;
		ViewState["USER_SEQ"] = lblUserSeq.Text;
		ViewState["USER_CHAR_NO"] = lblUserCharNo.Text;
		ViewState["MOVIE_TYPE"] = iBridUtil.GetStringValue(Request.QueryString["movietype"]);
		if (GetMultiCharFlag(lblSiteCd.Text)) {
			lblHeader.Text = "ｻｲﾄ／SEQ／ｷｬﾗｸﾀｰNo.";
			lblUserCharNo.Text = " - " + lblUserCharNo.Text;
			lblUserCharNo.Visible = true;
		} else {
			lblHeader.Text = "ｻｲﾄ／SEQ.";
			lblUserCharNo.Visible = false;
		}

		using (Cast oCast = new Cast()) {
			string sName = "",sId = "";
			oCast.GetValue(lblUserSeq.Text,"CAST_NM",ref sName);
			oCast.GetValue(lblUserSeq.Text,"LOGIN_ID",ref sId);
			lblCastNm.Text = sName;
			lblLoginId.Text = sId;
		}

		lstCastMovieAttrType.DataSourceID = "dsCastMovieAttrType";
		lstCastMovieAttrType.DataBind();
		lstCastMovieAttrType.DataSourceID = "";
		lstCastMovieAttrTypeValue.DataSourceID = "dsCastMovieAttrTypeValue";
		lstCastMovieAttrTypeValue.DataBind();
		lstCastMovieAttrTypeValue.DataSourceID = "";

		if (ViewState["MOVIE_TYPE"].ToString().Equals(ViCommConst.ATTACHED_BBS.ToString())) {
			plcHolder.Visible = true;
		} else {
			plcHolder.Visible = false;
		}

		ClearField();
		DataBind();
	}

	private void ClearField() {
	}


	protected void btnUpload_Click(object sender,EventArgs e) {
		UploadMovie();
		if (ViewState["MOVIE_TYPE"].ToString().Equals(ViCommConst.ATTACHED_PROFILE.ToString())) {
			if (GetMultiCharFlag(ViewState["SITE_CD"].ToString())) {
				Server.Transfer(string.Format("../Cast/CastView.aspx?loginid={0}&sitecd={1}&moviesite={1}&usercharno={2}&return={3}",lblLoginId.Text,ViewState["SITE_CD"].ToString(),ViewState["USER_CHAR_NO"].ToString(),ViewState["RETURN"].ToString()));
			} else {
				Server.Transfer(string.Format("../Cast/CastView.aspx?loginid={0}&moviesite={1}&usercharno={2}&return={3}",lblLoginId.Text,ViewState["SITE_CD"].ToString(),ViewState["USER_CHAR_NO"].ToString(),ViewState["RETURN"].ToString()));
			}
		} else {
			if (GetMultiCharFlag(ViewState["SITE_CD"].ToString())) {
				Server.Transfer(string.Format("../Cast/CastView.aspx?loginid={0}&sitecd={1}&bbsmoviesite={1}&usercharno={2}&return={3}",lblLoginId.Text,ViewState["SITE_CD"].ToString(),ViewState["USER_CHAR_NO"].ToString(),ViewState["RETURN"].ToString()));
			} else {
				Server.Transfer(string.Format("../Cast/CastView.aspx?loginid={0}&bbsmoviesite={1}&usercharno={2}&return={3}",lblLoginId.Text,ViewState["SITE_CD"].ToString(),ViewState["USER_CHAR_NO"].ToString(),ViewState["RETURN"].ToString()));
			}
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		if (ViewState["MOVIE_TYPE"].ToString().Equals(ViCommConst.ATTACHED_PROFILE.ToString())) {
			if (GetMultiCharFlag(ViewState["SITE_CD"].ToString())) {
				Server.Transfer(string.Format("../Cast/CastView.aspx?loginid={0}&sitecd={1}&moviesite={1}&usercharno={2}&return={3}",lblLoginId.Text,ViewState["SITE_CD"].ToString(),ViewState["USER_CHAR_NO"].ToString(),ViewState["RETURN"].ToString()));
			} else {
				Server.Transfer(string.Format("../Cast/CastView.aspx?loginid={0}&moviesite={1}&usercharno={2}&return={3}",lblLoginId.Text,ViewState["SITE_CD"].ToString(),ViewState["USER_CHAR_NO"].ToString(),ViewState["RETURN"].ToString()));

			}
		} else {
			if (GetMultiCharFlag(ViewState["SITE_CD"].ToString())) {
				Server.Transfer(string.Format("../Cast/CastView.aspx?loginid={0}&sitecd={1}&bbsmoviesite={1}&usercharno={2}&return={3}",lblLoginId.Text,ViewState["SITE_CD"].ToString(),ViewState["USER_CHAR_NO"].ToString(),ViewState["RETURN"].ToString()));
			} else {
				Server.Transfer(string.Format("../Cast/CastView.aspx?loginid={0}&bbsmoviesite={1}&usercharno={2}&return={3}",lblLoginId.Text,ViewState["SITE_CD"].ToString(),ViewState["USER_CHAR_NO"].ToString(),ViewState["RETURN"].ToString()));
			}
		}
	}

	private bool GetMultiCharFlag(string pSiteCd) {
		bool bMultiCharFlag = false;
		using (Site oSite = new Site()) {
			if (oSite.GetMultiCharFlag(pSiteCd)) {
				bMultiCharFlag = true;
			}
		}
		return bMultiCharFlag;
	}

	private void UploadMovie() {

		string sWebPhisicalDir = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(ViewState["SITE_CD"].ToString(),"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
		}
		
		string sAspectType = ViCommConst.MovieAspectType.NONE;
		if(this.rdoAspectNormal.Checked){
			sAspectType = ViCommConst.MovieAspectType.NORMAL;
		}else if( this.rdoAspectWide.Checked){
			sAspectType = ViCommConst.MovieAspectType.WIDE;
		}

		using (CastMovie objMovie = new CastMovie()) {

			decimal dNo = objMovie.GetMovieNo();

			string sFileNm = "",sFullPath = "";

			if (uldCastMovie.HasFile) {

				sFileNm = ViCommConst.MOVIE_HEADER + iBridUtil.addZero(dNo.ToString(),ViCommConst.OBJECT_NM_LENGTH);

				sFullPath = sWebPhisicalDir +
								ViCommConst.MOVIE_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\Operator\\" + lblLoginId.Text + "\\" +
								sFileNm +
								ViCommConst.MOVIE_FOODER;

				using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
					if (!System.IO.File.Exists(sFullPath)) {
						string sUploadFile = ConfigurationManager.AppSettings["MailParseDir"] + "\\" + sFileNm + ViCommConst.MOVIE_FOODER;
						uldCastMovie.SaveAs(sUploadFile);

						using (localhost.Service objWebSrv = new localhost.Service()) {
							string sInDir = ConfigurationManager.AppSettings["MailParseDir"];
							string sInFileNm = sFileNm + ViCommConst.MOVIE_FOODER;
							string sOutDir = sWebPhisicalDir + ViCommConst.MOVIE_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\Operator\\" + lblLoginId.Text;
							string sOutThumbnailDir = sWebPhisicalDir + ViCommConst.PIC_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\Operator\\" + lblLoginId.Text;
							string sOutFileNm = sFileNm;
							string sSiteCd = ViewState["SITE_CD"].ToString();

							localhost.ConvertMobileMovieResult[] resultArry
								= objWebSrv.ConvertMobileMovieWithAspectRate(iBridUtil.GetStringValue(ConfigurationManager.AppSettings["MailParseDir"]), sSiteCd, sInDir, sInFileNm, sOutDir, sOutFileNm, sAspectType, sOutThumbnailDir);

							List<string> movileFileTypeList = new List<string>();
							List<string> fileNumList = new List<string>();
							
							foreach(localhost.ConvertMobileMovieResult result in resultArry) {
								movileFileTypeList.Add(result.MobileFileType);
								fileNumList.Add(result.FileNum.ToString());	
							}
			
							using (DbSession db = new DbSession()) {
								db.PrepareProcedure("CAST_MOVIE_UPLOAD");
								db.ProcedureInParm("PSITE_CD", DbSession.DbType.VARCHAR2, sSiteCd);
								db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.NUMBER,ViewState["USER_SEQ"].ToString());
								db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViewState["USER_CHAR_NO"].ToString());
								db.ProcedureInParm("PMOVIE_SEQ",DbSession.DbType.NUMBER,dNo);
								db.ProcedureInParm("PMOVIE_TYPE",DbSession.DbType.NUMBER,int.Parse(ViewState["MOVIE_TYPE"].ToString()));
								db.ProcedureInParm("PCAST_MOVIE_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,lstCastMovieAttrType.SelectedValue);
								db.ProcedureInParm("PCAST_MOVIE_ATTR_SEQ", DbSession.DbType.VARCHAR2, lstCastMovieAttrTypeValue.SelectedValue);
								db.ProcedureInArrayParm("PMOVIE_FILE_TYPE_ARRAY", DbSession.DbType.VARCHAR2, movileFileTypeList.Count, movileFileTypeList.ToArray());
								db.ProcedureInArrayParm("PFILE_COUNT_ARRAY", DbSession.DbType.VARCHAR2, fileNumList.Count, fileNumList.ToArray());
								db.ProcedureInParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,1); 
								db.ProcedureInParm("PSAMPLE_MOVIE_SEQ", DbSession.DbType.NUMBER, null);
								db.ProcedureInParm("PTHUMBNAIL_PIC_SEQ", DbSession.DbType.NUMBER, dNo); 								
								db.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);
								db.ExecuteProcedure();
							}
						}
					}
				}
			}
		}
	}

	protected void lstCastMovieAttrType_SelectedIndexChanged(object sender,EventArgs e) {
		lstCastMovieAttrTypeValue.DataSourceID = "dsCastMovieAttrTypeValue";
		lstCastMovieAttrTypeValue.DataBind();
		lstCastMovieAttrTypeValue.DataSourceID = "";
	}

	protected void dsCastMovieAttrType_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = ViewState["SITE_CD"].ToString();
	}

	protected void dsCastMovieAttrTypeValue_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = ViewState["SITE_CD"].ToString();
		e.InputParameters[1] = lstCastMovieAttrType.SelectedValue;
	}
}
