﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: サイト別出演者検索
--	Progaram ID		: CastCharacterInquiry
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater		Update Explain
  2009/12/03				CSV機能コメントアウト化
  2010/07/15	Koyanagi	CSV機能再作成

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using System.IO;
using System.Text;
using iBridCommLib;
using ViComm;
using System.Text.RegularExpressions;
using System.Reflection;

public partial class Cast_CastCharacterInquiry:System.Web.UI.Page {
	// 一覧表示項目選択チェックボックス　並び順はGridViewと合わせること。
	protected static readonly string[] ColumnsArray = new string[] { "ID/ｷｬﾗｸﾀｰNo","写真","カテゴリ","プロダクション","氏名/ハンドル名","会員状態","TEL/URI","オンラインステータス","会話端末","お気に入られ数","お気に入り数","メールアドレス" };
	private static readonly string[] ColumnsSelectedArray = new string[] { "ID/ｷｬﾗｸﾀｰNo","写真","カテゴリ","プロダクション","氏名/ハンドル名","会員状態","TEL/URI","オンラインステータス","会話端末","お気に入られ数","お気に入り数" };

#region Sorting

	private string SortExpression {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortExpression"]);
		}
		set {
			this.ViewState["SortExpression"] = value;
		}
	}

	private string SortDirect {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortDirect"]);
		}
		set {
			this.ViewState["SortDirect"] = value;
		}
	}
	#endregion

	private string TxMailManUserSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["TxMailManUserSeq"]);
		}
		set {
			this.ViewState["TxMailManUserSeq"] = value;
		}
	}

	private const int MODIFY_USER_STATUS = 1;
	private const int MODIFY_AD_CD = 2;
	private const int MODIFY_MAIL_ADDR_FLAG = 4;
	private const int MODIFY_DELETE = 8;
	private const int MODIFY_DELETE_CHAR = 16;

	private string MailTemplateType {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MailTemplateType"]);
		}
		set {
			this.ViewState["MailTemplateType"] = value;
		}
	}

	private string recCount = "";
	private Stream filter;

	// お知らせメール予約送信編集用
	private PageCollectionParameters oReservationParamlist;
	private TxInfoMailHistory.MainteData oReservationMailParamlist;

	protected void Page_Load(object sender,EventArgs e) {
		filter = Response.Filter;
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			// 予約送信条件の初期化
			this.oReservationParamlist = null;
			this.oReservationMailParamlist = null;

			InitPage();
			this.SetupSelectColumns();
			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["adcd"]))) {
				txtAdCd.Text = iBridUtil.GetStringValue(Request.QueryString["adcd"]);
			}
			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["adnm"]))) {
				lblAdNm.Text = iBridUtil.GetStringValue(Request.QueryString["adnm"]);
			}
			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["sitecd"]))) {
				lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			}
			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["registdayfrom"]))) {
				txtRegistDayFrom.Text = iBridUtil.GetStringValue(Request.QueryString["registdayfrom"]);
			}
			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["registdayto"]))) {
				txtRegistDayTo.Text = iBridUtil.GetStringValue(Request.QueryString["registdayto"]);
			}

			SetupInqAdminCastType();

			if (!Request.QueryString.ToString().Equals("")) {
				GetList();
			}
		}
	}

	protected void dsCastCharacter_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);
		this.Title = DisplayWordUtil.Replace(this.Title);

		int iCompare = string.Compare(Session["AdminType"].ToString(),ViCommConst.RIGHT_SITE_MANAGER);

		btnSetupMail.Visible = false;
		btnApproachSetupMail.Visible = false;
		btnCleaningMail.Visible = false;
		btnSetupRank.Visible = false;
		btnToResignedMail.Visible = false;
		lblCsvPassword.Visible = false;
		txtCsvPassword.Visible = false;
		btnCSV.Visible = false;
		btnPickup.Visible = false;
		pnlTxMail.Visible = false;
		pnlRank.Visible = false;
		pnlPickup.Visible = false;
		btnSetupBulkUpdate.Visible = false;
		pnlBulkUpdate.Visible = false;
		grdCast.PageSize = 1;
		pnlInfo.Visible = false;
		this.pnlSeekConditionMainte.Visible = false;
		this.InitSeekConditionList();
		ClearField();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);

		}
		if (!IsPostBack) {
			chkOnlineStatus.DataBind();
			lstSiteCd.DataBind();
			lstProductionSeq.DataBind();
			lstManager.DataBind();
			lstConnectType.DataBind();
			lstActCategorySeq.DataBind();
			lstPickupId.DataBind();
			lstUserRankSeek.DataBind();
			lstInfoMailRxType.DataBind();

			chkOnlineStatus.DataSourceID = "";

			if (!(Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_PRODUCTION) ||
				(Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_AD_MANAGE)))) {
				chkOnlineStatus.Items.Add(new ListItem("ﾀﾞﾐｰ",ViCommConst.USER_DUMMY_TALKING.ToString()));
			}

			if (!iBridUtil.GetStringValue(Session["PRODUCTION_SEQ"]).Equals("")) {
				lstProductionSeq.SelectedValue = Session["PRODUCTION_SEQ"].ToString();
				lstProductionSeq.Enabled = false;
			} else {
				lstProductionSeq.Items.Insert(0,new ListItem("",""));
				lstProductionSeq.DataSourceID = "";
				lstProductionSeq.SelectedIndex = 0;
			}

			lstConnectType.Items.Insert(0,new ListItem("",""));
			lstConnectType.DataSourceID = "";
			lstConnectType.SelectedIndex = 0;

			lstActCategorySeq.Items.Insert(0,new ListItem("",""));
			lstActCategorySeq.DataSourceID = "";
			lstActCategorySeq.SelectedIndex = 0;

			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MANAGER_SEQ"]))) {
				lstManager.SelectedValue = iBridUtil.GetStringValue(Session["MANAGER_SEQ"]);
				lstManager.Enabled = false;
			} else {
				lstManager.Items.Insert(0,new ListItem("",""));
				lstManager.DataSourceID = "";
				lstManager.SelectedIndex = 0;
			}

			lstUserRankSeek.Items.Insert(0,new ListItem("",""));
			lstUserRankSeek.DataSourceID = "";
			lstUserRankSeek.SelectedIndex = 0;

			lstPickupId.Items.Insert(0,new ListItem("",""));
			lstPickupId.DataSourceID = "";
			lstPickupId.SelectedIndex = 0;

			lstInfoMailRxType.Items.Insert(0,new ListItem("",""));
			lstInfoMailRxType.DataSourceID = "";
			lstInfoMailRxType.SelectedIndex = 0;

		}

		using (ManageCompany oCompany = new ManageCompany()) {
			bool bCastUseDummyBirthday = oCompany.IsAvailableService(ViCommConst.RELEASE_CAST_USE_DUMMY_BIRTHDAY);
			bool bFavoritMeCountCastInquiry = oCompany.IsAvailableService(ViCommConst.RELEASE_FAVORIT_ME_SEARCH);
			plsAge.Visible = bCastUseDummyBirthday;
			plsFavoritMeCount.Visible = bFavoritMeCountCastInquiry;
			if (oCompany.IsAvailableService(ViCommConst.RELEASE_SELECT_HTML_TEXT_VISIBLE)) {
				plcMailSendType.Visible = false;
			}
			this.lnkSelectColumns.Visible = oCompany.IsAvailableService(ViCommConst.RELEASE_INQUIRY_SELECT_COLUMNS,2);

			bool isEnableNegative = oCompany.IsAvailableService(ViCommConst.RELEASE_SEEK_CONDITION_NEGATIVE,2);
			this.chkLoginIdListNegativeFlag.Visible = isEnableNegative;
			this.chkLoginIdNegativeFlag.Visible = isEnableNegative;
			this.chkLastActionDayNegativeFlag.Visible = isEnableNegative;
			this.chkUserDefineMaskNegatieFlag.Visible = isEnableNegative;
		}

		this.plcExtension.Visible = true;
		tdHeaderUserDefineMask.Attributes["class"] = "tdHeaderStyle2";
		plcBeforeSystemId.Visible = false;

		if (Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_PRODUCTION) ||
			(Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_AD_MANAGE))) {
			plcUserDefineFlag.Visible = false;
			tdHeaderKeyword.Attributes["class"] = "tdHeaderStyle2";
		} else {
			plcUserDefineFlag.Visible = true;
			tdHeaderKeyword.Attributes["class"] = "tdHeaderStyle";
		}

		this.chkNaFlag.DataBind();
		this.chkUserDefineMask.DataBind();
		this.chkGameCharacterType.DataBind();
		this.chkSelectColumns.DataBind();
		this.SortExpression = string.Empty;
		this.SortDirect = string.Empty;

		this.SetupConditionLoginIdList(false);
		this.SetupConditionAdCdList(false);

		// お知らせメール送信履歴からの遷移
		// (予約送信条件の設定)
		this.GetReservationParamList();
		if (this.oReservationParamlist != null) {
			PageCollectionParameters oParamlist = this.oReservationParamlist;

			// 会員状態
			if ((oParamlist.iUserStatusMask & ViCommConst.MASK_WOMAN_AUTH_WAIT) != 0) {
				chkAuthWait.Checked = true;
			}
			if ((oParamlist.iUserStatusMask & ViCommConst.MASK_WOMAN_NORMAL) != 0) {
				chkNormal.Checked = true;
			}
			if ((oParamlist.iUserStatusMask & ViCommConst.MASK_WOMAN_STOP) != 0) {
				chkStop.Checked = true;
			}
			if ((oParamlist.iUserStatusMask & ViCommConst.MASK_WOMAN_HOLD) != 0) {
				chkHold.Checked = true;
			}
			if ((oParamlist.iUserStatusMask & ViCommConst.MASK_WOMAN_RESIGNED) != 0) {
				chkResigned.Checked = true;
			}
			if ((oParamlist.iUserStatusMask & ViCommConst.MASK_WOMAN_BAN) != 0) {
				chkBan.Checked = true;
			}

			// 利用キャリア
			if ((oParamlist.iCarrier & ViCommConst.MASK_DOCOMO) != 0) {
				chkDocomo.Checked = true;
			}
			if ((oParamlist.iCarrier & ViCommConst.MASK_KDDI) != 0) {
				chkAu.Checked = true;
			}
			if ((oParamlist.iCarrier & ViCommConst.MASK_SOFTBANK) != 0) {
				chkSoftbank.Checked = true;
			}
			if ((oParamlist.iCarrier & ViCommConst.MASK_ANDROID) != 0) {
				chkAndroid.Checked = true;
			}
			if ((oParamlist.iCarrier & ViCommConst.MASK_IPHONE) != 0) {
				chkIphone.Checked = true;
			}

			// メールアドレス状態
			if ((oParamlist.iNonExistMailAddrFlagMask & ViCommConst.MSK_MAIL_ADDR_OK) != 0) {
				chkMailAddrOk.Checked = true;
			}
			if ((oParamlist.iNonExistMailAddrFlagMask & ViCommConst.MSK_MAIL_ADDR_FILTERING_ERROR) != 0) {
				chkMailAddrFilteringError.Checked = true;
			}
			if ((oParamlist.iNonExistMailAddrFlagMask & ViCommConst.MSK_MAIL_ADDR_NG) != 0) {
				chkMailAddrNg.Checked = true;
			}

			// サイト利用状況
			if (string.IsNullOrEmpty(oParamlist.sSiteUseStatus)) {
				// チェックなし
			} else {
				string[] aUseStatus = oParamlist.sSiteUseStatus.Split(',');
				foreach (string sUseStatus in aUseStatus) {
					if (sUseStatus.Equals("0")) {
						chkChat.Checked = true;
					} else if (sUseStatus.Equals("1")) {
						chkGame.Checked = true;
					} else if (sUseStatus.Equals("2")) {
						chkChatGamet.Checked = true;
					}
				}
			}

			// キャラクタ状態
			if (oParamlist.sNaFlag.Equals(ViCommConst.WITHOUT)) {
				// チェックなし
			} else {
				string[] aNaFlag = oParamlist.sNaFlag.Split(',');
				foreach (ListItem oListItem in chkNaFlag.Items) {
					foreach (string sNaFlag in aNaFlag) {
						if (sNaFlag.Equals(oListItem.Value)) {
							oListItem.Selected = true;
							break;
						}
					}
				}
			}

			// オンライン状態
			if (string.IsNullOrEmpty(oParamlist.sOnline)) {
				// チェックなし
			} else {
				string[] aOnline = oParamlist.sOnline.Split(',');
				foreach (ListItem oListItem in chkOnlineStatus.Items) {
					foreach (string sOnline in aOnline) {
						if (sOnline.Equals(oListItem.Value)) {
							oListItem.Selected = true;
							break;
						}
					}
				}
			}

			// ユーザー定義フラグ
			if (string.IsNullOrEmpty(oParamlist.sUserDefineMask)) {
				// チェックなし
			} else {
				int iUserDefineMask = 0;
				if (int.TryParse(oParamlist.sUserDefineMask,out iUserDefineMask)) {
					foreach (ListItem oListItem in chkUserDefineMask.Items) {
						if ((iUserDefineMask & int.Parse(oListItem.Value)) != 0) {
							oListItem.Selected = true;
						}
					}
				}
			}

			// ゲームキャラクタータイプ
			if (string.IsNullOrEmpty(oParamlist.sGameCharacterType)) {
				// チェックなし
			} else {
				string[] aGameCharacterType = oParamlist.sGameCharacterType.Split(',');
				foreach (ListItem oListItem in chkGameCharacterType.Items) {
					foreach (string sGameCharaType in aGameCharacterType) {
						if (sGameCharaType.Equals(string.Format("'{0}'",oListItem.Value))) {
							oListItem.Selected = true;
							break;
						}
					}
				}
			}

			// ログインID
			if (Regex.IsMatch(oParamlist.sLoginId,",|\r?\n")) {
				// 複数入力
				txtLoginIdList.Text = oParamlist.sLoginId;
				this.SetupConditionLoginIdList(true);
			} else {
				txtLoginId.Text = oParamlist.sLoginId;
			}

			// 広告コード
			if (Regex.IsMatch(oParamlist.sAdCd,",|\r?\n")) {
				// 複数入力
				txtAdCdList.Text = oParamlist.sAdCd;
				this.SetupConditionAdCdList(true);
			} else {
				txtAdCd.Text = oParamlist.sAdCd;
			}

			this.TxMailManUserSeq = oParamlist.sExceptRefusedByManUserSeq;
			this.SortExpression = oParamlist.sSortExpression;
			this.SortDirect = oParamlist.sSortDirection;

			lstSiteCd.SelectedValue = oParamlist.sSiteCd;

			this.DropDownList_SetSelectedIndexByValue(lstProductionSeq,oParamlist.sProductionSeq);
			this.DropDownList_SetSelectedIndexByValue(lstConnectType,oParamlist.sTalkType);
			this.DropDownList_SetSelectedIndexByValue(lstActCategorySeq,oParamlist.sActCategorySeq);
			this.DropDownList_SetSelectedIndexByValue(lstManager,oParamlist.sManagerSeq);
			this.DropDownList_SetSelectedIndexByValue(lstUserRankSeek,oParamlist.sUserRank);
			this.DropDownList_SetSelectedIndexByValue(lstCastAttrSeq0,oParamlist.sCastAttrValue1);
			this.DropDownList_SetSelectedIndexByValue(lstCastAttrSeq1,oParamlist.sCastAttrValue2);
			this.DropDownList_SetSelectedIndexByValue(lstCastAttrSeq2,oParamlist.sCastAttrValue3);
			this.DropDownList_SetSelectedIndexByValue(lstCastAttrSeq3,oParamlist.sCastAttrValue4);
			this.DropDownList_SetSelectedIndexByValue(lstPickupId,oParamlist.sPickupId);
			this.DropDownList_SetSelectedIndexByValue(lstInfoMailRxType,oParamlist.sInfoMailRxType);

			rdoEnabledBlog.SelectedValue = oParamlist.sEnabledBlogFlag;
			rdoEnaledRichino.SelectedValue = oParamlist.sEnabledRichinoFlag;
			rdoTxMuller3NgMailAddrFlag.SelectedValue = oParamlist.sTxMuller3NgMailAddrFlag;

			this.RadioButtonList_SetSelectedIndexByValue(rdoUseCrosmileFlag,oParamlist.sUseCrosmileFlag);
			this.RadioButtonList_SetSelectedIndexByValue(rdoUseVoiceappFlag,oParamlist.sUseVoiceappFlag);
			this.RadioButtonList_SetSelectedIndexByValue(rdoUseJealousyFlag,oParamlist.sUseJealousyFlag);

			txtUserSeq.Text = oParamlist.sUserSeq;
			txtCastNm.Text = oParamlist.sCastNm;
			txtHandelNm.Text = oParamlist.sHandleNm;
			txtEmailAddr.Text = oParamlist.sEmailAddr;
			txtRegistDayFrom.Text = oParamlist.sRegistDayFrom;
			txtRegistDayTo.Text = oParamlist.sRegistDayTo;
			txtLastLoginDayFrom.Text = oParamlist.sLastLoginDayFrom;
			txtLastLoginDayTo.Text = oParamlist.sLastLoginDayTo;
			txtFinalLoginDayFrom.Text = oParamlist.sFinalLoginDayFrom;
			txtFinalLoginDayTo.Text = oParamlist.sFinalLoginDayTo;
			txtReportAffiliateDateFrom.Text = oParamlist.sReportAffiliateDateFrom;
			txtReportAffiliateDateTo.Text = oParamlist.sReportAffiliateDateTo;
			txtTotalPaymentAmtFrom.Text = oParamlist.sTotalPaymentAmtFrom;
			txtTotalPaymentAmtTo.Text = oParamlist.sTotalPaymentAmtTo;
			txtTel.Text = oParamlist.sTel;
			txtAgeFrom.Text = oParamlist.sAgeFrom;
			txtAgeTo.Text = oParamlist.sAgeTo;
			txtFavoritMeCountFrom.Text = oParamlist.sFavoritMeCountFrom;
			txtFavoritMeCountTo.Text = oParamlist.sFavoritMeCountTo;
			txtGuid.Text = oParamlist.sGuid;
			txtRemarks1.Text = oParamlist.sRemarks1;
			txtLastTxMailDateFrom.Text = oParamlist.sLastTxMailDateFrom;
			txtLastTxMailDateTo.Text = oParamlist.sLastTxMailDateTo;
			txtGameHandleNm.Text = oParamlist.sGameHandleNm;
			txtGameRegistDateFrom.Text = oParamlist.sGameRegistDateFrom;
			txtGameRegistDateTo.Text = oParamlist.sGameRegistDateTo;
			txtGameCharacterLevelFrom.Text = oParamlist.sGameCharacterLevelFrom;
			txtGameCharacterLevelTo.Text = oParamlist.sGameCharacterLevelTo;
			txtGamePointFrom.Text = oParamlist.sGamePointFrom;
			txtGamePointTo.Text = oParamlist.sGamePointTo;
			txtCastGamePointFrom.Text = oParamlist.sCastGamePointFrom;
			txtCastGamePointTo.Text = oParamlist.sCastGamePointTo;
			txtGuardianTel.Text = oParamlist.sGuardianTel;
			chkHandleNmExactFlag.Checked = oParamlist.bHandleNmExactFlag;
			if (chkLoginIdListVisible.Checked) {
				chkLoginIdListNegativeFlag.Checked = oParamlist.bLoginIdNegativeFlag;
			} else {
				chkLoginIdNegativeFlag.Checked = oParamlist.bLoginIdNegativeFlag;
			}
			txtBeforeSystemId.Text = oParamlist.sBeforeSystemId;
			txtKeyword.Text = oParamlist.sKeyword;
			chkLastActionDayNegativeFlag.Checked = oParamlist.bLastActionDayNegativeFlag;
			chkUserDefineMaskNegatieFlag.Checked = oParamlist.bUserDefineMaskNegatieFlag;
			chkIntroducerFriendAllFlag.Checked = oParamlist.bIntroducerFriendAllFlag;
			txtCrosmileLastUsedVersionFrom.Text = oParamlist.sCrosmileLastUsedVersionFrom;
			txtCrosmileLastUsedVersionTo.Text = oParamlist.sCrosmileLastUsedVersionTo;
			txtGcappLastLoginVersionFrom.Text = oParamlist.sGcappLastLoginVersionFrom;
			txtGcappLastLoginVersionTo.Text = oParamlist.sGcappLastLoginVersionTo;
			chkExcludeEmailAddr.Checked = oParamlist.bExcludeEmailAddr;
			txtNotPaymentAmtFrom.Text = oParamlist.sNotPaymentAmtFrom;
			txtNotPaymentAmtTo.Text = oParamlist.sNotPaymentAmtTo;
		}// (予約送信条件の設定終了)
	}

	private void ClearField() {
		chkOnlineStatus.SelectedIndex = -1;
		if (lstProductionSeq.Enabled) {
			lstProductionSeq.SelectedIndex = 0;
		}
		lstManager.SelectedIndex = 0;
		txtLoginId.Text = "";
		txtUserSeq.Text = "";
		txtCastNm.Text = "";
		txtHandelNm.Text = "";
		lstConnectType.SelectedIndex = 0;
		lstActCategorySeq.SelectedIndex = 0;
		lstPickupId.SelectedIndex = 0;
		lstInfoMailRxType.SelectedIndex = 0;
		chkMailAddrOk.Checked = false;
		chkMailAddrFilteringError.Checked = false;
		chkMailAddrNg.Checked = false;
		txtTel.Text = "";
		txtEmailAddr.Text = "";
		lstUserRankSeek.SelectedIndex = 0;
		chkAuthWait.Checked = false;
		chkNormal.Checked = false;
		chkStop.Checked = false;
		chkHold.Checked = false;
		chkResigned.Checked = false;
		chkBan.Checked = false;
		txtRegistDayFrom.Text = "";
		txtRegistDayTo.Text = "";
		txtLastLoginDayFrom.Text = "";
		txtLastLoginDayTo.Text = "";

		txtFinalLoginDayFrom.Text = "";
		txtFinalLoginDayTo.Text = "";
		
		txtReportAffiliateDateFrom.Text = "";
		txtReportAffiliateDateTo.Text = "";
		txtTotalPaymentAmtFrom.Text = "";
		txtTotalPaymentAmtTo.Text = "";
		txtAdCd.Text = "";
		txtAgeFrom.Text = "";
		txtAgeTo.Text = "";
		txtFavoritMeCountFrom.Text = "";
		txtFavoritMeCountTo.Text = "";
		chkNaFlag.SelectedIndex = -1;
		recCount = "0";
		this.txtGuid.Text = string.Empty;
		txtRemarks1.Text = string.Empty;
		for (int i = 0;i < ViCommConst.MAX_ADMIN_CAST_INQUIRY_ITEMS;i++) {
			DropDownList lstCastAttrSeq = (DropDownList)plcAttrType.FindControl(string.Format("lstCastAttrSeq{0}",i)) as DropDownList;
			if (lstCastAttrSeq.Visible && lstCastAttrSeq.Items.Count > 0) {
				lstCastAttrSeq.SelectedIndex = 0;
			}
		}
		this.txtLastTxMailDateFrom.Text = "";
		this.txtLastTxMailDateTo.Text = "";

		this.txtGameHandleNm.Text = string.Empty;
		this.txtGameRegistDateFrom.Text = string.Empty;
		this.txtGameRegistDateTo.Text = string.Empty;
		this.txtGameCharacterLevelFrom.Text = string.Empty;
		this.txtGameCharacterLevelTo.Text = string.Empty;
		this.txtGamePointFrom.Text = string.Empty;
		this.txtGamePointTo.Text = string.Empty;
		this.txtCastGamePointFrom.Text = string.Empty;
		this.txtCastGamePointTo.Text = string.Empty;
		this.txtGuardianTel.Text = string.Empty;

		this.chkHandleNmExactFlag.Checked = false;

		this.txtBeforeSystemId.Text = string.Empty;
		this.txtKeyword.Text = string.Empty;
		this.chkLoginIdListNegativeFlag.Checked = false;
		this.chkLoginIdNegativeFlag.Checked = false;
		this.chkLastActionDayNegativeFlag.Checked = false;
		this.chkUserDefineMaskNegatieFlag.Checked = false;
		this.chkIntroducerFriendAllFlag.Checked = false;
		this.txtCrosmileLastUsedVersionFrom.Text = string.Empty;
		this.txtCrosmileLastUsedVersionTo.Text = string.Empty;
		this.rdoUseCrosmileFlag.SelectedIndex = 0;
		this.rdoUseVoiceappFlag.SelectedIndex = 0;
		this.txtGcappLastLoginVersionFrom.Text = string.Empty;
		this.txtGcappLastLoginVersionTo.Text = string.Empty;
		this.rdoUseJealousyFlag.SelectedIndex = 0;
		this.chkExcludeEmailAddr.Checked = false;
		this.rdoTxMuller3NgMailAddrFlag.SelectedValue = string.Empty;
		this.txtNotPaymentAmtFrom.Text = string.Empty;
		this.txtNotPaymentAmtTo.Text = string.Empty;

		this.SortExpression = string.Empty;
		this.SortDirect = string.Empty;
	}

	protected string GetRecCount() {
		return recCount;
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (IsValid) {
			ViewState["PICKUP"] = "0";
			this.SortExpression = string.Empty;
			this.SortDirect = string.Empty;
			GetList();
		}
	}

	protected void btnPickup_Click(object sender,EventArgs e) {
		if (IsValid) {
			pnlKey.Enabled = false;
			pnlPickup.Visible = true;
			rdoAppend.Checked = true;
			lstPickup.DataBind();

			if (!iBridUtil.GetStringValue(ViewState["PICKUP"]).Equals("1")) {
				ViewState["PICKUP"] = "1";
				GetList();
			}
		}
	}

	protected void btnCancelPickup_Click(object sender,EventArgs e) {
		pnlKey.Enabled = true;
		pnlPickup.Visible = false;
	}

	protected void btnPickupDetail_Click(object sender,EventArgs e) {
		string sCheckedSeq = "",sUserSeq;
		int iAppend = 0;

		for (int i = 0;i < this.grdCast.Rows.Count;i++) {
			GridViewRow oRow = this.grdCast.Rows[i];

			if (oRow.RowType == DataControlRowType.DataRow) {
				if (((CheckBox)oRow.FindControl("chkPickup")).Checked) {
					sUserSeq = ((Label)(oRow.FindControl("lblUserSeq"))).Text;
					sCheckedSeq = sCheckedSeq + "," + sUserSeq;
				}
			}
		}
		if (sCheckedSeq.Length > 0) {
			sCheckedSeq = sCheckedSeq.Substring(1);
			lblNoCheckError.Visible = false;
			if (rdoAppend.Checked) {
				iAppend = 1;
			}
			Server.Transfer(
				string.Format("../CastAdmin/MultiPickupMainte.aspx?sitecd={0}&sitenm={1}&pickupid={2}&userseq={3}&append={4}",lstSiteCd.SelectedValue,lstSiteCd.Items[lstSiteCd.SelectedIndex].Text,lstPickup.SelectedValue,sCheckedSeq,iAppend)
			);
		} else {
			lblNoCheckError.Visible = true;
		}
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
		this.SetupSelectColumns();
		this.SetupSelectColumnsVisible(true);
	}

	private void GetList() {
		if (txtAdCd.Text.Equals(string.Empty)) {
			lblAdNm.Text = string.Empty;
		} else {
			lblAdNm.Text = "<br>" + lblAdNm.Text;
		}

		grdCast.PageIndex = 0;
		grdCast.PageSize = 50;

		using (Site oSite = new Site()) {
			oSite.GetOne(lstSiteCd.SelectedValue);
			Session["VIEW_CALL_COUNT_FLAG"] = oSite.viewCallCountFlag.ToString();
		}

		pnlInfo.Visible = true;
		grdCast.DataSourceID = "dsCastCharacter";
		grdCast.DataBind();
		pnlCount.DataBind();
		int iCompare = string.Compare(Session["AdminType"].ToString(),ViCommConst.RIGHT_SITE_OWNER);
		btnSetupMail.Visible = (iCompare >= 0);

		string sAvaCheckExistAddrFlag = ViCommConst.FLAG_OFF_STR;

		using (Sys oSys = new Sys()) {
			oSys.GetValue("AVA_CHECK_EXIST_ADDR_FLAG",out sAvaCheckExistAddrFlag);
		}

		if (sAvaCheckExistAddrFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			btnCleaningMail.Visible = (iCompare >= 0);
		}

		btnApproachSetupMail.Visible = false;
		btnSetupRank.Visible = (iCompare >= 0);
		btnToResignedMail.Visible = (iCompare >= 0);

		lblCsvPassword.Visible = (iCompare >= 0);
		txtCsvPassword.Visible = (iCompare >= 0);
		btnCSV.Visible = (iCompare >= 0);

		btnSetupBulkUpdate.Visible = (iCompare >= 0);
		if (btnSetupBulkUpdate.Visible) {
			using (ManageCompany oCompany = new ManageCompany()) {
				bool bEnableBulkUpdate = oCompany.IsAvailableService(ViCommConst.RELEASE_BULK_UPDATE_CAST);
				this.btnSetupBulkUpdate.Visible = bEnableBulkUpdate;
			}
		}

		// お知らせメール送信履歴からの遷移
		if (!IsPostBack) {
			if (this.oReservationParamlist != null) {
				// お知らせメール送信欄の表示
				SetupMail(ViCommConst.MAIL_TP_CAST_MAIL_MAGAZINE);

				// お知らせメール送信欄の初期値設定
				lstMailTemplateNo.SelectedValue = this.oReservationMailParamlist.MailTemplateNo;
				rdoMailServer.SelectedValue = this.oReservationMailParamlist.MailServer;
				rdoMailSendType.SelectedValue = this.oReservationMailParamlist.MailSendType;
				if (!string.IsNullOrEmpty(this.oReservationMailParamlist.ReservationSendDate)) {
					DateTime oDT = Convert.ToDateTime(this.oReservationMailParamlist.ReservationSendDate);
					txtReservationSendDay.Text = oDT.ToString("yyyy/MM/dd");
					txtReservationSendHour.Text = oDT.ToString("HH");
					txtReservationSendMinute.Text = oDT.ToString("mm");
				}
			}
		}
	}

	private DataSet GetListDs(bool pExceptRefused) {
		DataSet ds = new DataSet();

		PageCollectionParameters oParamlist = GetSearchParamList();

		using (CastCharacter oCharacter = new CastCharacter()) {
			ds = oCharacter.GetPageCollection(
							oParamlist.sOnline,
							oParamlist.sLoginId,
							oParamlist.sProductionSeq,
							oParamlist.sCastNm,
							oParamlist.sSiteCd,
							oParamlist.sTalkType,
							oParamlist.sTerminalType,
							oParamlist.sHandleNm,
							oParamlist.sActCategorySeq,
							oParamlist.sEmailAddr,
							oParamlist.sPicType,
							oParamlist.iNonExistMailAddrFlagMask,
							oParamlist.sMainCharOnlyFlag,
							oParamlist.sManagerSeq,
							oParamlist.sRegistDayFrom,
							oParamlist.sRegistDayTo,
							oParamlist.sLastLoginDayFrom,
							oParamlist.sLastLoginDayTo,
							
							oParamlist.sFinalLoginDayFrom,
							oParamlist.sFinalLoginDayTo,
							
							oParamlist.sTotalPaymentAmtFrom,
							oParamlist.sTotalPaymentAmtTo,
							oParamlist.sNaFlag,
							oParamlist.sUserRank,
							oParamlist.sAdCd,
							oParamlist.sTel,
							oParamlist.sCastAttrValue1,
							oParamlist.sCastAttrValue2,
							oParamlist.sCastAttrValue3,
							oParamlist.sCastAttrValue4,
							oParamlist.sAgeFrom,
							oParamlist.sAgeTo,
							oParamlist.sFavoritMeCountFrom,
							oParamlist.sFavoritMeCountTo,
							oParamlist.iUserStatusMask,
							oParamlist.sPickupId,
							oParamlist.sGuid,
							oParamlist.sRemarks1,
							oParamlist.sEnabledBlogFlag,
							oParamlist.sEnabledRichinoFlag,
							oParamlist.sSiteUseStatus,
							oParamlist.sInfoMailRxType,
							oParamlist.sUserMailRxType,
							oParamlist.sReportAffiliateDateFrom,
							oParamlist.sReportAffiliateDateTo,
							oParamlist.sUserDefineMask,
							pExceptRefused ? oParamlist.sExceptRefusedByManUserSeq : string.Empty,
							oParamlist.sLastTxMailDateFrom,
							oParamlist.sLastTxMailDateTo,
							oParamlist.sGameHandleNm,
							oParamlist.sGameCharacterType,
							oParamlist.sGameRegistDateFrom,
							oParamlist.sGameRegistDateTo,
							oParamlist.sGameCharacterLevelFrom,
							oParamlist.sGameCharacterLevelTo,
							oParamlist.sGamePointFrom,
							oParamlist.sGamePointTo,
							oParamlist.sCastGamePointFrom,
							oParamlist.sCastGamePointTo,
							oParamlist.bHandleNmExactFlag,
							oParamlist.sBeforeSystemId,
							oParamlist.sKeyword,
							oParamlist.iCarrier,
							oParamlist.bLoginIdNegativeFlag,
							oParamlist.bLastActionDayNegativeFlag,
							oParamlist.bUserDefineMaskNegatieFlag,
							oParamlist.bIntroducerFriendAllFlag,
							oParamlist.sCrosmileLastUsedVersionFrom,
							oParamlist.sCrosmileLastUsedVersionTo,
							oParamlist.sUseCrosmileFlag,
							oParamlist.sSortExpression,
							oParamlist.sSortDirection,
							oParamlist.sGuardianTel,
							oParamlist.sUserSeq,
							oParamlist.sUseVoiceappFlag,
							oParamlist.sGcappLastLoginVersionFrom,
							oParamlist.sGcappLastLoginVersionTo,
							oParamlist.sUseJealousyFlag,
							oParamlist.bExcludeEmailAddr,
							oParamlist.sTxMuller3NgMailAddrFlag,
							oParamlist.sNotPaymentAmtFrom,
							oParamlist.sNotPaymentAmtTo,
							0,
							SysConst.DB_MAX_ROWS);
		}
		return ds;
	}

	protected void dsCastCharacter_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {

		PageCollectionParameters oParamlist = GetSearchParamList();

		e.InputParameters[0] = oParamlist.sOnline;
		e.InputParameters[1] = oParamlist.sLoginId;
		e.InputParameters[2] = oParamlist.sProductionSeq;
		e.InputParameters[3] = oParamlist.sCastNm;
		e.InputParameters[4] = oParamlist.sSiteCd;
		e.InputParameters[5] = oParamlist.sTalkType;
		e.InputParameters[6] = oParamlist.sTerminalType;
		e.InputParameters[7] = oParamlist.sHandleNm;
		e.InputParameters[8] = oParamlist.sActCategorySeq;
		e.InputParameters[9] = oParamlist.sEmailAddr;
		e.InputParameters[10] = oParamlist.sPicType;
		e.InputParameters[11] = oParamlist.iNonExistMailAddrFlagMask;
		e.InputParameters[12] = oParamlist.sMainCharOnlyFlag;
		e.InputParameters[13] = oParamlist.sManagerSeq;
		e.InputParameters[14] = oParamlist.sRegistDayFrom;
		e.InputParameters[15] = oParamlist.sRegistDayTo;
		e.InputParameters[16] = oParamlist.sLastLoginDayFrom;
		e.InputParameters[17] = oParamlist.sLastLoginDayTo;
		e.InputParameters[18] = oParamlist.sTotalPaymentAmtFrom;
		e.InputParameters[19] = oParamlist.sTotalPaymentAmtTo;
		e.InputParameters[20] = oParamlist.sNaFlag;
		e.InputParameters[21] = oParamlist.sUserRank;
		e.InputParameters[22] = oParamlist.sAdCd;
		e.InputParameters[23] = oParamlist.sTel;
		e.InputParameters[24] = oParamlist.sCastAttrValue1;
		e.InputParameters[25] = oParamlist.sCastAttrValue2;
		e.InputParameters[26] = oParamlist.sCastAttrValue3;
		e.InputParameters[27] = oParamlist.sCastAttrValue4;
		e.InputParameters[28] = oParamlist.sAgeFrom;
		e.InputParameters[29] = oParamlist.sAgeTo;
		e.InputParameters[30] = oParamlist.sFavoritMeCountFrom;
		e.InputParameters[31] = oParamlist.sFavoritMeCountTo;
		e.InputParameters[32] = oParamlist.iUserStatusMask;
		e.InputParameters[33] = oParamlist.sPickupId;
		e.InputParameters[34] = oParamlist.sGuid;
		e.InputParameters[35] = oParamlist.sRemarks1;
		e.InputParameters[36] = oParamlist.sEnabledBlogFlag;
		e.InputParameters[37] = oParamlist.sEnabledRichinoFlag;
		e.InputParameters[38] = oParamlist.sSiteUseStatus;
		e.InputParameters[39] = oParamlist.sInfoMailRxType;
		e.InputParameters[40] = oParamlist.sUserMailRxType;
		e.InputParameters[41] = oParamlist.sReportAffiliateDateFrom;
		e.InputParameters[42] = oParamlist.sReportAffiliateDateTo;
		e.InputParameters[43] = oParamlist.sUserDefineMask;
		e.InputParameters[44] = oParamlist.sExceptRefusedByManUserSeq;
		e.InputParameters[45] = oParamlist.sLastTxMailDateFrom;
		e.InputParameters[46] = oParamlist.sLastTxMailDateTo;
		e.InputParameters[47] = oParamlist.sGameHandleNm;
		e.InputParameters[48] = oParamlist.sGameCharacterType;
		e.InputParameters[49] = oParamlist.sGameRegistDateFrom;
		e.InputParameters[50] = oParamlist.sGameRegistDateTo;
		e.InputParameters[51] = oParamlist.sGameCharacterLevelFrom;
		e.InputParameters[52] = oParamlist.sGameCharacterLevelTo;
		e.InputParameters[53] = oParamlist.sGamePointFrom;
		e.InputParameters[54] = oParamlist.sGamePointTo;
		e.InputParameters[55] = oParamlist.sCastGamePointFrom;
		e.InputParameters[56] = oParamlist.sCastGamePointTo;
		e.InputParameters[57] = oParamlist.bHandleNmExactFlag;
		e.InputParameters[58] = oParamlist.sBeforeSystemId;
		e.InputParameters[59] = oParamlist.sKeyword;
		e.InputParameters[60] = oParamlist.iCarrier;
		e.InputParameters[61] = oParamlist.bLoginIdNegativeFlag;
		e.InputParameters[62] = oParamlist.bLastActionDayNegativeFlag;
		e.InputParameters[63] = oParamlist.bUserDefineMaskNegatieFlag;
		e.InputParameters[64] = oParamlist.bIntroducerFriendAllFlag;
		e.InputParameters[65] = oParamlist.sCrosmileLastUsedVersionFrom;
		e.InputParameters[66] = oParamlist.sCrosmileLastUsedVersionTo;
		e.InputParameters[67] = oParamlist.sUseCrosmileFlag;
		e.InputParameters[68] = oParamlist.sSortExpression;
		e.InputParameters[69] = oParamlist.sSortDirection;
		e.InputParameters[70] = oParamlist.sGuardianTel;
		e.InputParameters[71] = oParamlist.sUserSeq;
		e.InputParameters[72] = oParamlist.sUseVoiceappFlag;
		e.InputParameters[73] = oParamlist.sGcappLastLoginVersionFrom;
		e.InputParameters[74] = oParamlist.sGcappLastLoginVersionTo;
		e.InputParameters[75] = oParamlist.sUseJealousyFlag;

		e.InputParameters[76] = oParamlist.sFinalLoginDayFrom;
		e.InputParameters[77] = oParamlist.sFinalLoginDayTo;
		e.InputParameters[78] = oParamlist.bExcludeEmailAddr;
		e.InputParameters[79] = oParamlist.sTxMuller3NgMailAddrFlag;
		e.InputParameters[80] = oParamlist.sNotPaymentAmtFrom;
		e.InputParameters[81] = oParamlist.sNotPaymentAmtTo;
	}

	protected void dsActCategory_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
	}

	protected void dsCastMailMagazine_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pMailTemplateType"] = this.MailTemplateType;
	}

	protected void lstSiteCd_SelectedIndexChanged(object sender,EventArgs e) {
		lstActCategorySeq.DataSourceID = "dsActCategory";
		lstActCategorySeq.DataBind();
		lstActCategorySeq.Items.Insert(0,new ListItem("",""));
		lstActCategorySeq.DataSourceID = "";
		SetupInqAdminCastType();
	}

	protected void lstMailTemplateNo_SelectedIndexChanged(object sender,EventArgs e) {
		if (this.pnlMailTemplate.Visible) {
			this.grdMailTemplate.DataSourceID = "dsMailTemplate";
			this.grdMailTemplate.DataBind();
		}
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		bool bLimitFlag = false;
		
		if (grdCast.Rows.Count == 0) {
			return;
		}

		string sCsvPassword = string.Empty;

		using (SysEx oSysEx = new SysEx()) {
			oSysEx.GetValue("CSV_PASSWORD",out sCsvPassword);
		}

		if (!sCsvPassword.Equals(txtCsvPassword.Text)) {
			bLimitFlag = true;
		}

		//データ取得 
		DataSet dsList = GetListDs(false);
		DataTable dtList = dsList.Tables[0];

		//ヘッダ作成
		string sHeader =
				"サイト," +
				"キャラクターNo," +
				DisplayWordUtil.Replace("キャスト名,") +
				DisplayWordUtil.Replace("キャスト名カナ,") +
				"ログインID," +
				"ログインパスワード," +
				"メールアドレス," +
				"メールアドレスNG," +
				"プロダクション," +
				"担当," +
				"利用端末種別," +
				"電話番号," +
				DisplayWordUtil.Replace("キャストステータス,") +
				"ランク," +
				"ハンドル名," +
				"ハンドル名カナ," +
				"対応通話形態," +
				DisplayWordUtil.ReplaceWordCommentList("一覧用コメント") + "," +
				"特殊コメント," +
				"運営者からのコメント," +
				"出演開始日," +
				"ユーザーランク," +
				"登録日," +
				"最終ログイン日," +
				"最終精算日," +
				"累計報酬額," +
				"未支払報酬額," +
				"銀行名," +
				"支店名," +
				"支店名カナ," +
				"口座種類," +
				"口座番号," +
				"口座名義," +
				"広告コード," +
				"広告名," +
				"前システムID"
				;

		bool bAttrOutPut = false;
		using (ManageCompany oCompany = new ManageCompany()) {
			bAttrOutPut = oCompany.IsAvailableService(ViCommConst.RELEASE_CAST_CSV_ATTR_OUTPUT);
		}

		string[] sAttrColumnNm = new string[0];
		if (bAttrOutPut) {
			sHeader += "," + DisplayWordUtil.ReplaceOkList("OKリスト");

			//OKリストのカラム追加
			dtList.Columns.Add(new DataColumn("OK_PLAY_NM",typeof(string)));

			//属性値のカラムとヘッダー文字列追加
			using (CastAttrType oCastAttrType = new CastAttrType()) {
				DataSet ds = oCastAttrType.GetPageCollection(lstSiteCd.SelectedValue,0,ViCommConst.MAX_ATTR_COUNT);
				DataTable dt = ds.Tables[0];

				if (dt.Rows.Count != 0) {
					sAttrColumnNm = new string[dt.Rows.Count];
					sHeader = sHeader + ",";
					for (int i = 0;i < dt.Rows.Count;i++) {
						//CSVヘッダ文字列
						sHeader = sHeader + dt.Rows[i]["CAST_ATTR_TYPE_NM"].ToString();
						if (i == dt.Rows.Count - 1) {
							sHeader = sHeader + "\r\n";
						} else {
							sHeader = sHeader + ",";
						}
						//データセットにカラムを追加
						dtList.Columns.Add(new DataColumn(dt.Rows[i]["CAST_ATTR_TYPE_SEQ"].ToString(),typeof(string)));
						sAttrColumnNm[i] = dt.Rows[i]["CAST_ATTR_TYPE_SEQ"].ToString();
					}
				} else {
					sHeader = sHeader + "\r\n";
				}
			}

			//OKリストデータ追加
			using (OkPlayList oOkPlayList = new OkPlayList()) {
				DataSet ds = oOkPlayList.GetList(lstSiteCd.SelectedValue);

				foreach (DataRow drList in dtList.Rows) {
					string sOkPlay = "";
					if (ds.Tables[0].Rows.Count != 0) {
						Int64 iOkMask = Int64.Parse(drList["OK_PLAY_MASK"].ToString());
						Int64 iMaskValue = 1;
						for (int i = 0;i < ds.Tables[0].Rows.Count;i++) {
							if ((iOkMask & iMaskValue) != 0) {
								sOkPlay += ds.Tables[0].Rows[i]["OK_PLAY_NM"].ToString() + " ";
							}
							iMaskValue = iMaskValue << 1;
						}
						drList["OK_PLAY_NM"] = sOkPlay;
					} else {
						drList["OK_PLAY_NM"] = "";
					}
				}
			}

			//属性データ追加
			Object[] SearchKey = new object[3];
			dtList.PrimaryKey = new DataColumn[] { dtList.Columns["SITE_CD"],dtList.Columns["USER_SEQ"],dtList.Columns["USER_CHAR_NO"] };	//主キー設定 

			using (CastAttrValue oCastAttrValue = new CastAttrValue()) {
				DataSet ds = oCastAttrValue.GetAllList(lstSiteCd.SelectedValue);
				DataTable dt = ds.Tables[0];
				foreach (DataRow dr in dt.Rows) {
					SearchKey[0] = dr["SITE_CD"].ToString();
					SearchKey[1] = dr["USER_SEQ"].ToString();
					SearchKey[2] = dr["USER_CHAR_NO"].ToString();
					DataRow EditRow = dsList.Tables[0].Rows.Find(SearchKey);
					if (EditRow != null) {
						EditRow.BeginEdit();
						if (dr["INPUT_TYPE"].ToString().Equals(ViCommConst.INPUT_TYPE_TEXT)) {
							EditRow[dr["CAST_ATTR_TYPE_SEQ"].ToString()] = dr["CAST_ATTR_INPUT_VALUE"].ToString();
						} else {
							EditRow[dr["CAST_ATTR_TYPE_SEQ"].ToString()] = dr["CAST_ATTR_NM"].ToString();
						}
						EditRow.EndEdit();
					}
				}
			}
		} else {
			sHeader = sHeader + "\r\n";
		}

		//CSV出力 
		Response.ContentType = "application/download";
		Response.AppendHeader("Content-Disposition","attachment;filename=CastList.csv");
		Response.ContentEncoding = System.Text.Encoding.GetEncoding("Shift_JIS");

		Response.Write(sHeader);
		Response.Write("$NO_TRANS_START;");
		foreach (DataRow drList in dtList.Rows) {
			string sData = null;
			if (bLimitFlag == false) {
			    sData =
				setCsvString(drList["SITE_CD"].ToString()) + "," +
				setCsvString(drList["USER_CHAR_NO"].ToString()) + "," +
				setCsvString(drList["CAST_NM"].ToString()) + "," +
				setCsvString(drList["CAST_KANA_NM"].ToString()) + "," +
				setCsvString(drList["LOGIN_ID"].ToString()) + "," +
				setCsvString(drList["LOGIN_PASSWORD"].ToString()) + "," +
				setCsvString(drList["EMAIL_ADDR"].ToString()) + "," +
				setCsvString(drList["NON_EXIST_MAIL_ADDR_FLAG"].ToString()) + "," +
				setCsvString(drList["PRODUCTION_NM"].ToString()) + "," +
				setCsvString(drList["MANAGER_NM"].ToString()) + "," +
				setCsvString(drList["USE_TERMINAL_TYPE_NM"].ToString()) + "," +
				setCsvString(drList["TEL"].ToString()) + "," +
				setCsvString(drList["USER_STATUS_NM"].ToString()) + "," +
				setCsvString(drList["USER_RANK"].ToString()) + "," +
				setCsvString(drList["HANDLE_NM"].ToString()) + "," +
				setCsvString(drList["HANDLE_KANA_NM"].ToString()) + "," +
				setCsvString(drList["CONNECT_TYPE_NM"].ToString()) + "," +
				setCsvString(drList["COMMENT_LIST"].ToString()) + "," +
				setCsvString(drList["COMMENT_DETAIL"].ToString()) + "," +
				setCsvString(drList["COMMENT_ADMIN"].ToString()) + "," +
				setCsvString(drList["START_PERFORM_DAY"].ToString()) + "," +
				setCsvString(drList["USER_RANK"].ToString()) + "," +
				setCsvString(drList["REGIST_DATE"].ToString()) + "," +
				setCsvString(drList["LAST_ACTION_DATE"].ToString()) + "," +
				setCsvString(drList["LAST_PAYMENT_DATE"].ToString()) + "," +
				setCsvString(drList["TOTAL_PAYMENT_AMT"].ToString()) + "," +
				setCsvString(drList["NOT_PAYMENT_AMT"].ToString()) + "," +
				setCsvString(drList["BANK_NM"].ToString()) + "," +
				setCsvString(drList["BANK_OFFICE_NM"].ToString()) + "," +
				setCsvString(drList["BANK_OFFICE_KANA_NM"].ToString()) + "," +
				setCsvString(drList["BANK_ACCOUNT_TYPE"].ToString()) + "," +
				setCsvString(drList["BANK_ACCOUNT_NO"].ToString()) + "," +
				setCsvString(drList["BANK_ACCOUNT_HOLDER_NM"].ToString()) + "," +
				setCsvString(drList["AD_CD"].ToString()) + "," +
				setCsvString(drList["AD_NM"].ToString()) + "," +
				setCsvString(drList["BEFORE_SYSTEM_ID"].ToString())
				;
			} else {
			    sData =
				"," +
				"," +
				"," +
				"," +
				setCsvString(drList["LOGIN_ID"].ToString()) + "," +
				"," +
				"," +
				"," +
				"," +
				"," +
				"," +
				"," +
				"," +
				"," +
				setCsvString(drList["HANDLE_NM"].ToString()) + "," +
				"," +
				"," +
				"," +
				"," +
				"," +
				"," +
				"," +
				setCsvString(drList["REGIST_DATE"].ToString()) + "," +
				"," +
				"," +
				"," +
				"," +
				"," +
				"," +
				"," +
				"," +
				"," +
				"," +
				setCsvString(drList["AD_CD"].ToString()) + "," +
				","
				;
			}

			if (bAttrOutPut) {
				//OKリスト 
				if (bLimitFlag == false) {
					sData += "," + setCsvString(drList["OK_PLAY_NM"].ToString());
				} else {
					sData += ",";
				}
				//属性
				if (sAttrColumnNm.Length != 0) {
					sData = sData + ",";
					for (int i = 0;i < sAttrColumnNm.Length;i++) {
						if (bLimitFlag == false) {	
							sData = sData + setCsvString(drList[sAttrColumnNm[i]].ToString());
						}
						if (i == sAttrColumnNm.Length - 1) {
							sData = sData + "\r\n";
						} else {
							sData = sData + ",";
						}
					}
				} else {
					sData = sData + "\r\n";
				}
			} else {
				sData = sData + "\r\n";
			}
			Response.Write(sData);
		}
		Response.Write("$NO_TRANS_END;");
		Response.End();
	}

	private string setCsvString(string pData) {
		if (pData.IndexOf('"') > -1 ||
			pData.IndexOf(',') > -1 ||
			pData.IndexOf('\r') > -1 ||
			pData.IndexOf('\n') > -1 ||
			pData.StartsWith(" ") || pData.StartsWith("\t") ||
			pData.EndsWith(" ") || pData.EndsWith("\t")) {
			if (pData.IndexOf('"') > -1) {
				//"を""とする
				pData = pData.Replace("\"","\"\"");
			}
			if (pData.IndexOf('\r') > -1 || pData.IndexOf('\n') > -1) {
				//改行しない 
				pData = pData.Replace("\r","");
				pData = pData.Replace("\n","");
			}
			pData = "\"" + pData + "\"";
		}
		return pData;
	}

	protected bool IsAreaManager(object pManagerSeq) {
		if (!Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_LOCAL_STAFF)) {
			return true;
		} else {
			return iBridUtil.GetStringValue(Session["MANAGER_SEQ"]).Equals(pManagerSeq.ToString());
		}
	}

	protected void lnkMailTemplate_Click(object sender,EventArgs e) {
		this.pnlMailTemplate.Visible = !this.pnlMailTemplate.Visible;
		if (this.pnlMailTemplate.Visible) {
			this.lnkMailTemplate.Text = "[プレビュー非表示]";
			this.grdMailTemplate.DataSourceID = "dsMailTemplate";
			this.grdMailTemplate.DataBind();
		} else {
			this.lnkMailTemplate.Text = "[プレビュー表示]";
			this.grdMailTemplate.DataSourceID = string.Empty;
		}
		this.SetSearchCondition(this.pnlMailTemplate.Visible);
	}

	protected void btnSetupMail_Click(object sender,EventArgs e) {
		SetupMail(ViCommConst.MAIL_TP_CAST_MAIL_MAGAZINE);
	}

	protected void btnApproachSetupMail_Click(object sender,EventArgs e) {
		SetupMail(ViCommConst.MAIL_TP_APPROACH_ADMIN_TO_CAST);
	}


	protected void btnCleaningMail_Click(object sender,EventArgs e) {
		SetupMail(ViCommConst.MAIL_TP_CLEANING_ADDR);
	}

	protected void btnToResignedMail_Click(object sender,EventArgs e) {
		SetupMail(ViCommConst.MAIL_TP_ADMIN_TO_RESIGNED);
	}

	private void SetupMail(string pMailTemplateType) {
		pnlKey.Enabled = false;
		pnlTxMail.Visible = true;
		btnTxMail.Enabled = true;
		lblAttention.Text = string.Empty;
		this.MailTemplateType = pMailTemplateType;
		lstMailTemplateNo.DataBind();
		rdoMailSendType.DataBind();
		rdoMailSendType.SelectedIndex = 0;
		lblAttentionCleaning.Visible = false;
		lblAttentionToResigned.Visible = false;
		if (pMailTemplateType.Equals(ViCommConst.MAIL_TP_CLEANING_ADDR)) {
			lblAttentionCleaning.Visible = true;
		}
		if (pMailTemplateType.Equals(ViCommConst.MAIL_TP_ADMIN_TO_RESIGNED)) {
			lblAttentionToResigned.Visible = true;
		}

		// 予約送信設定
		plcReservationMail.Visible = false;
		btnTxReservationMail.Visible = false;

		if (pMailTemplateType.Equals(ViCommConst.MAIL_TP_APPROACH_ADMIN_TO_CAST)) {
			trManLoginId.Visible = true;
			this.tdHeaderMailType.Attributes["class"] = "tdHeaderStyle";
			this.tdHeaderMailSendType.Attributes["class"] = "tdHeaderStyle";
		} else {
			trManLoginId.Visible = false;
			if (pMailTemplateType.Equals(ViCommConst.MAIL_TP_CAST_MAIL_MAGAZINE)) {
				plcReservationMail.Visible = true;
				btnTxReservationMail.Visible = true;
				this.tdHeaderMailType.Attributes["class"] = "tdHeaderStyle";
				this.tdHeaderMailSendType.Attributes["class"] = "tdHeaderStyle";
			} else if (this.plcMailSendType.Visible) {
				this.tdHeaderMailType.Attributes["class"] = "tdHeaderStyle";
				this.tdHeaderMailSendType.Attributes["class"] = "tdHeaderStyle2";
			} else {
				this.tdHeaderMailType.Attributes["class"] = "tdHeaderStyle2";
				this.tdHeaderMailSendType.Attributes["class"] = "tdHeaderStyle";
			}
		}

		this.lnkMailTemplate.Text = "[プレビュー表示]";
		this.pnlMailTemplate.Visible = false;
	}

	protected void btnCancelTx_Click(object sender,EventArgs e) {
		pnlKey.Enabled = true;
		pnlTxMail.Visible = false;
	}

	protected void btnTxMail_Click(object sender,EventArgs e) {
		if (IsValid == false) {
			return;
		}

		Dictionary<string,List<string>> oUserStatusDictionary = this.GetUserStatus(true,"USER_SEQ","USER_CHAR_NO");

		string[] sDoc;
		int iDocCount;
		string sMailSendType = this.rdoMailSendType.SelectedValue;
		if (plcMailSendType.Visible == false) {
			sMailSendType = string.Empty;
		}

		SysPrograms.SeparateHtml("",ViCommConst.MAX_ORG_MAIL_BLOCKS,out sDoc,out iDocCount);

		AdminMailSender oAdminMailSender;

		if (ViCommConst.MAIL_TP_CAST_MAIL_MAGAZINE.Equals(this.MailTemplateType) || ViCommConst.MAIL_TP_CLEANING_ADDR.Equals(this.MailTemplateType) || ViCommConst.MAIL_TP_ADMIN_TO_RESIGNED.Equals(this.MailTemplateType)) {
			oAdminMailSender = new AdminToCastMailSender(
												this.lstSiteCd.SelectedValue,
												this.lstMailTemplateNo.SelectedValue,
												sMailSendType,
												SessionObjs.AdminId,
												oUserStatusDictionary["USER_SEQ"].ToArray(),
												string.Empty,
												sDoc,
												iDocCount,
												ViCommConst.FLAG_OFF,
												this.rdoMailServer.SelectedValue);

		} else if (ViCommConst.MAIL_TP_APPROACH_ADMIN_TO_CAST.Equals(this.MailTemplateType)) {
			oAdminMailSender = new ApproachAdminToCastMailSender(
				this.lstSiteCd.SelectedValue,
				this.lstMailTemplateNo.SelectedValue,
				sMailSendType,
				this.txtManLoginId.Text,
				SessionObjs.AdminId,
				oUserStatusDictionary["USER_SEQ"].ToArray(),
				oUserStatusDictionary["USER_CHAR_NO"].ToArray(),
				string.Empty,
				sDoc,
				ViCommConst.FLAG_OFF);

		} else {
			btnTxMail.Enabled = false;
			this.lblAttention.Text = "ﾒｰﾙ種別の指定に誤りがあります。再度、メール送信設定を行って下さい。";
			return;
		}

		if (!oAdminMailSender.Send()) {
			btnTxMail.Enabled = false;
			this.lblAttention.Text = "実行中のﾒｰﾙ送信処理が存在するため送信できません。";
			return;
		}

		// メール送信情報を保存
		if (ViCommConst.MAIL_TP_CAST_MAIL_MAGAZINE.Equals(this.MailTemplateType)) {
			TxInfoMailHistory.MainteData oMailHisData = new TxInfoMailHistory.MainteData();
			SetTxInfoMailHistory(ref oMailHisData,ViCommConst.FLAG_OFF_STR);
		}

		btnTxMail.Enabled = false;
		lblAttention.Text = "メール送信が完了しました。";
	}

	protected void btnSetupRank_Click(object sender,EventArgs e) {
		pnlKey.Enabled = false;
		pnlRank.Visible = true;
		btnUpdRank.Enabled = true;

		lstUserRank.DataBind();
	}
	protected void btnCancelRank_Click(object sender,EventArgs e) {
		pnlKey.Enabled = true;
		pnlRank.Visible = false;
	}
	protected void btnUpdRank_Click(object sender,EventArgs e) {
		if (IsValid == false) {
			return;
		}

		string[] sUserSeq;
		string[] sRevisionNo;
		GetUserSeqArr(out sUserSeq,out sRevisionNo);

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_RANK_ALL_UPDATE");
			db.ProcedureInArrayParm("PCAST_USER_SEQ",DbSession.DbType.VARCHAR2,sUserSeq.Length,sUserSeq);
			db.ProcedureInArrayParm("PCAST_REVISION_NO",DbSession.DbType.VARCHAR2,sRevisionNo.Length,sRevisionNo);
			db.ProcedureInParm("PCAST_USER_COUNT",DbSession.DbType.NUMBER,sUserSeq.Length);
			db.ProcedureInParm("PUSER_RANK",DbSession.DbType.VARCHAR2,lstUserRank.SelectedValue);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		btnUpdRank.Enabled = false;
	}

	private void GetUserSeqArr(out string[] pUserRank,out string[] pRevisionNo) {
		DataSet ds = GetListDs(false);

		DataRow dr = null;
		pUserRank = new string[ds.Tables[0].Rows.Count];
		pRevisionNo = new string[ds.Tables[0].Rows.Count];

		for (int i = 0;i < ds.Tables[0].Rows.Count;i++) {
			dr = ds.Tables[0].Rows[i];
			pUserRank[i] = dr["USER_SEQ"].ToString();
			pRevisionNo[i] = dr["CAST_REVISION_NO"].ToString();
		}
	}

	private Dictionary<string,List<string>> GetUserStatus(bool pExceptRefused, params string[] pColumnNames) {
		DataSet oDataSet = GetListDs(pExceptRefused);
		Dictionary<string,List<string>> oDictionary = new Dictionary<string,List<string>>();
		foreach (string sColumnName in pColumnNames) {
			oDictionary[sColumnName] = new List<string>();
		}

		foreach (DataRow oDataRow in oDataSet.Tables[0].Rows) {
			foreach (string sColumnName in oDictionary.Keys) {
				if (oDataRow.Table.Columns.Contains(sColumnName)) {
					oDictionary[sColumnName].Add(oDataRow[sColumnName].ToString());
				}
			}
		}

		return oDictionary;
	}

	protected void dsManager_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {

		if (lstProductionSeq.Items.Count == 0) {
			e.InputParameters[0] = "";
		} else if (lstProductionSeq.Items.Count > 0 && lstProductionSeq.SelectedIndex == 0 && !string.IsNullOrEmpty(lstProductionSeq.Items[0].Value)) {
			e.InputParameters[0] = "";
		} else {
			e.InputParameters[0] = lstProductionSeq.SelectedValue;
		}
	}
	protected void lstProductionSeq_SelectedIndexChanged(object sender,EventArgs e) {
		lstManager.DataSourceID = "dsManager";
		lstManager.DataBind();
		lstManager.Items.Insert(0,new ListItem("",""));
		lstManager.DataSourceID = "";

		Page.Validate();
		if (IsValid) {
			ViewState["PICKUP"] = "0";
			GetList();
		}
	}

	private void SetupCondition(out int pStatus,out int pCarrier,out int pNonExistMailAddrFlag) {

		pStatus = 0;
		pCarrier = 0;
		pNonExistMailAddrFlag = 0;

		if (chkAuthWait.Checked) {
			pStatus += ViCommConst.MASK_WOMAN_AUTH_WAIT;
		}
		if (chkNormal.Checked) {
			pStatus += ViCommConst.MASK_WOMAN_NORMAL;
		}
		if (chkStop.Checked) {
			pStatus += ViCommConst.MASK_WOMAN_STOP;
		}
		if (chkHold.Checked) {
			pStatus += ViCommConst.MASK_WOMAN_HOLD;
		}
		if (chkResigned.Checked) {
			pStatus += ViCommConst.MASK_WOMAN_RESIGNED;
		}
		if (chkBan.Checked) {
			pStatus += ViCommConst.MASK_WOMAN_BAN;
		}

		if (chkDocomo.Checked) {
			pCarrier += ViCommConst.MASK_DOCOMO;
		}
		if (chkAu.Checked) {
			pCarrier += ViCommConst.MASK_KDDI;
		}
		if (chkSoftbank.Checked) {
			pCarrier += ViCommConst.MASK_SOFTBANK;
		}
		if (chkAndroid.Checked) {
			pCarrier += ViCommConst.MASK_ANDROID;
		}
		if (chkIphone.Checked) {
			pCarrier += ViCommConst.MASK_IPHONE;
		}
		
		if (chkMailAddrOk.Checked) {
			pNonExistMailAddrFlag += ViCommConst.MSK_MAIL_ADDR_OK;
		}
		if (chkMailAddrFilteringError.Checked) {
			pNonExistMailAddrFlag += ViCommConst.MSK_MAIL_ADDR_FILTERING_ERROR;
		}
		if (chkMailAddrNg.Checked) {
			pNonExistMailAddrFlag += ViCommConst.MSK_MAIL_ADDR_NG;
		}

		if ((!txtRegistDayFrom.Text.Equals("")) || (!txtRegistDayTo.Text.Equals(""))) {
			if (txtRegistDayFrom.Text.Equals("")) {
				txtRegistDayFrom.Text = txtRegistDayTo.Text;
			} else if (txtRegistDayTo.Text.Equals("")) {
				txtRegistDayTo.Text = txtRegistDayFrom.Text;
			}
		}

		if ((!txtLastLoginDayFrom.Text.Equals("")) || (!txtLastLoginDayTo.Text.Equals(""))) {
			if (txtLastLoginDayFrom.Text.Equals("")) {
				txtLastLoginDayFrom.Text = txtLastLoginDayTo.Text;
			} else if (txtLastLoginDayTo.Text.Equals("")) {
				txtLastLoginDayTo.Text = txtLastLoginDayFrom.Text;
			}
		}
		
		//最終ログイン
		if ((!txtFinalLoginDayFrom.Text.Equals("")) || (!txtFinalLoginDayTo.Text.Equals(""))) {
			if (txtFinalLoginDayFrom.Text.Equals("")) {
				txtFinalLoginDayFrom.Text = txtFinalLoginDayTo.Text;
			} else if (txtFinalLoginDayTo.Text.Equals("")) {
				txtFinalLoginDayTo.Text = txtFinalLoginDayFrom.Text;
			}
		}

		if ((!txtTotalPaymentAmtFrom.Text.Equals("")) || (!txtTotalPaymentAmtTo.Text.Equals(""))) {
			if (txtTotalPaymentAmtFrom.Text.Equals("")) {
				txtTotalPaymentAmtFrom.Text = txtTotalPaymentAmtTo.Text;
			} else if (txtTotalPaymentAmtTo.Text.Equals("")) {
				txtTotalPaymentAmtTo.Text = txtTotalPaymentAmtFrom.Text;
			}
		}

		if ((!txtReportAffiliateDateFrom.Text.Equals("")) || (!txtReportAffiliateDateTo.Text.Equals(""))) {
			if (txtReportAffiliateDateFrom.Text.Equals("")) {
				txtReportAffiliateDateFrom.Text = txtReportAffiliateDateTo.Text;
			} else if (txtReportAffiliateDateTo.Text.Equals("")) {
				txtReportAffiliateDateTo.Text = txtReportAffiliateDateFrom.Text;
			}
		}

		if ((!txtLastTxMailDateFrom.Text.Equals("")) || (!txtLastTxMailDateTo.Text.Equals(""))) {
			if (txtLastTxMailDateFrom.Text.Equals("")) {
				txtLastTxMailDateFrom.Text = txtLastTxMailDateTo.Text;
			} else if (txtLastTxMailDateTo.Text.Equals("")) {
				txtLastTxMailDateTo.Text = txtLastTxMailDateFrom.Text;
			}
		}

		if ((!txtCrosmileLastUsedVersionFrom.Text.Equals("")) || (!txtCrosmileLastUsedVersionTo.Text.Equals(""))) {
			if (txtCrosmileLastUsedVersionFrom.Text.Equals("")) {
				txtCrosmileLastUsedVersionFrom.Text = txtCrosmileLastUsedVersionTo.Text;
			} else if (txtCrosmileLastUsedVersionTo.Text.Equals("")) {
				txtCrosmileLastUsedVersionTo.Text = txtCrosmileLastUsedVersionFrom.Text;
			}
		}

		if ((!txtGcappLastLoginVersionFrom.Text.Equals("")) || (!txtGcappLastLoginVersionTo.Text.Equals(""))) {
			if (txtGcappLastLoginVersionFrom.Text.Equals("")) {
				txtGcappLastLoginVersionFrom.Text = txtGcappLastLoginVersionTo.Text;
			} else if (txtGcappLastLoginVersionTo.Text.Equals("")) {
				txtGcappLastLoginVersionTo.Text = txtGcappLastLoginVersionFrom.Text;
			}
		}
	}

	private void SetupInqAdminCastType() {

		// Available Serive?
		using (ManageCompany oCompany = new ManageCompany()) {
			if (!oCompany.IsAvailableService(ViCommConst.RELEASE_ATTR_INQUIRY)) {
				plcAttrType.Visible = false;
				return;
			}
		}

		using (CastAttrTypeValue oCastAttrTypeValue = new CastAttrTypeValue())
		using (CastAttrType oCastAttrType = new CastAttrType()) {
			DataSet dsCastAttrType = oCastAttrType.GetInqAdminCastAttrType(lstSiteCd.SelectedValue);
			bool bDisp = false;
			for (int i = 0;i < ViCommConst.MAX_ADMIN_CAST_INQUIRY_ITEMS;i++) {
				Label lblCastAttrNm = (Label)plcAttrType.FindControl(string.Format("lblCastAttrNm{0}",i)) as Label;
				lblCastAttrNm.Text = dsCastAttrType.Tables[0].Rows[0][string.Format("CAST_ATTR_TYPE_NM{0}",i + 1)].ToString();

				DropDownList lstCastAttrSeq = (DropDownList)plcAttrType.FindControl(string.Format("lstCastAttrSeq{0}",i)) as DropDownList;
				lstCastAttrSeq.Items.Clear();

				HtmlTableCell CellAttrNm = (HtmlTableCell)plcAttrType.FindControl(string.Format("celCastAttrNm{0}",i)) as HtmlTableCell;
				HtmlTableCell CellAttrSeq = (HtmlTableCell)plcAttrType.FindControl(string.Format("celCastAttrSeq{0}",i)) as HtmlTableCell;

				if (lblCastAttrNm.Text == "") {
					CellAttrNm.Visible = false;
					CellAttrSeq.Visible = false;
				} else {
					bDisp = true;
					DataSet dsCastAttrTypeValue = oCastAttrTypeValue.GetList(lstSiteCd.SelectedValue,dsCastAttrType.Tables[0].Rows[0][string.Format("CAST_ATTR_TYPE_SEQ{0}",i + 1)].ToString());

					lstCastAttrSeq.Items.Add(new ListItem("指定なし",""));
					if (string.IsNullOrEmpty(dsCastAttrType.Tables[0].Rows[0][string.Format("GROUPING_CATEGORY_CD{0}",i + 1)].ToString())) {
						foreach (DataRow dr in dsCastAttrTypeValue.Tables[0].Rows) {
							lstCastAttrSeq.Items.Add(new ListItem(dr["CAST_ATTR_NM"].ToString(),dr["CAST_ATTR_SEQ"].ToString()));
						}
					} else {
						using (CodeDtl oCodeDtl = new CodeDtl()) {
							using (Code oCode = new Code()) {
								if (oCode.GetOne(dsCastAttrType.Tables[0].Rows[0][string.Format("GROUPING_CATEGORY_CD{0}",i + 1)].ToString())) {
									lblCastAttrNm.Text = oCode.codeTypeNm;
								}
							}
							DataSet dsGroup = oCodeDtl.GetList(dsCastAttrType.Tables[0].Rows[0][string.Format("GROUPING_CATEGORY_CD{0}",i + 1)].ToString());
							foreach (DataRow drGroup in dsGroup.Tables[0].Rows) {
								lstCastAttrSeq.Items.Add(new ListItem(drGroup["CODE_NM"].ToString(),drGroup["CODE"].ToString()));
							}
						}
					}
					CellAttrNm.Visible = true;
					CellAttrSeq.Visible = true;
				}
			}
			plcAttrType.Visible = bDisp;
		}

		// お知らせメール送信履歴からの遷移
		// (予約送信条件の設定)
		this.GetReservationParamList();
		if (this.oReservationParamlist != null) {
			PageCollectionParameters oParamlist = this.oReservationParamlist;
			this.DropDownList_SetSelectedIndexByValue(lstCastAttrSeq0,oParamlist.sCastAttrValue1);
			this.DropDownList_SetSelectedIndexByValue(lstCastAttrSeq1,oParamlist.sCastAttrValue2);
			this.DropDownList_SetSelectedIndexByValue(lstCastAttrSeq2,oParamlist.sCastAttrValue3);
			this.DropDownList_SetSelectedIndexByValue(lstCastAttrSeq3,oParamlist.sCastAttrValue4);
		}
	}

	protected string GetOnlineSite(object pUserSeq) {
		string sOnlineSite = "";

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_ONLINE_SITE");
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq.ToString());
			db.ProcedureOutParm("PONLINE_SITE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sOnlineSite = db.GetStringValue("PONLINE_SITE");
		}
		return sOnlineSite;
	}

	protected string GetCallCount(object pSiteCd,object pUserSeq,object pUserCharNo,object pLoginSeq) {
		string sCallCount = "";

		if (!iBridUtil.GetStringValue(Session["VIEW_CALL_COUNT_FLAG"]).Equals(ViCommConst.FLAG_ON.ToString())) {
			return "";
		}
		if (pLoginSeq.ToString().Equals("")) {
			return "";
		}
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_CALL_COUNT");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd.ToString());
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq.ToString());
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo.ToString());
			db.ProcedureInParm("PLOGIN_SEQ",DbSession.DbType.VARCHAR2,pLoginSeq.ToString());
			db.ProcedureOutParm("PCALL_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sCallCount = string.Format("Call   {0,6}",db.GetStringValue("PCALL_COUNT")).Replace(" ","&nbsp;");
		}
		return sCallCount;
	}

	protected void vdcAdCd_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid) {
			using (Ad oAd = new Ad()) {
				string sAdNm = "";
				args.IsValid = oAd.IsExist(txtAdCd.Text,ref sAdNm);
				lblAdNm.Text = sAdNm;
			}
		}
	}

	protected void vdcAdCdList_ServerValidate(object source, ServerValidateEventArgs args) {
		if (args.IsValid) {

			string[] sAdCdArray = Regex.Split(txtAdCdList.Text, ",|\r?\n");
			lblAdNmList.Text = string.Empty;
			for (int i = 0; i < sAdCdArray.Length; i++) {
				using (Ad oAd = new Ad()) {
					string sAdNm = "";
					args.IsValid = oAd.IsExist(sAdCdArray[i], ref sAdNm);
					lblAdNmList.Text = lblAdNmList.Text + "<br />"+sAdNm;
					if (!args.IsValid) {
						this.vdcAdCdList.ErrorMessage = string.Format("広告コード[{0}]が未登録です。",sAdCdArray[i]);
						break; 
					}
				}
			}
		}
	}
	protected void vdcBulkUpdAdCd_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid) {
			using (Ad oAd = new Ad()) {
				string sAdNm = "";
				args.IsValid = oAd.IsExist(txtAdCd4BulkUpd.Text,ref sAdNm);
				lblAdNm.Text = sAdNm;
			}
		}
	}

	protected void lnkCondition_Click(object sender,EventArgs e) {
		SetSearchCondition(pnlKey.Visible);

		//GridViewHelper.RegiserCreateGridHeader(1, this.grdCast, this.pnlGrid);
	}

	private void SetSearchCondition(bool pIsPanelVisible) {
		if (pIsPanelVisible) {
			pnlKey.Visible = false;
			pnlGrid.Height = 550;
			lnkCondition.Text = "[検索条件表示]";
		} else {
			pnlKey.Visible = true;
			pnlGrid.Height = 290;
			lnkCondition.Text = "[条件非表示]";
		}
	}

	protected void grdCast_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.Pager) {
			return;
		}

		int iLoginIdCol = 0;

		if (!iBridUtil.GetStringValue(ViewState["PICKUP"]).Equals("1")) {
			e.Row.Cells[0].Visible = false;
			iLoginIdCol = 1;
		} else {
			e.Row.Cells[0].Visible = true;
			iLoginIdCol = 1;
		}

		if (e.Row.RowType == DataControlRowType.DataRow && lstPickupId.SelectedValue.Equals(string.Empty)) {
			if (!Convert.ToString(DataBinder.Eval(e.Row.DataItem,"PICKUP_MASK")).Equals("0")) {
				e.Row.Cells[iLoginIdCol].BackColor = Color.Teal;
				e.Row.Cells[iLoginIdCol].ForeColor = Color.White;
			}
		}
	}

	protected string GetPickupMask(object pPickupMask) {
		long lMask = Convert.ToInt64(pPickupMask);
		if (lMask == 0) {
			return "";
		}

		string sLabel = "";
		long lValue = 1;
		for (int i = 1;i < 60;i++) {
			if ((lMask & lValue) > 0) {
				sLabel = sLabel + i.ToString("d2") + "&nbsp;";
			}
			lValue = lValue << 1;
		}
		if (!sLabel.Equals("")) {
			sLabel = "<BR>Pickup " + sLabel;
		}
		return sLabel;
	}

	public class PageCollectionParameters {
		public string sOnline;
		public string sLoginId;
		public string sUserSeq;
		public string sProductionSeq;
		public string sCastNm;
		public string sSiteCd;
		public string sTalkType;
		public string sTerminalType;
		public string sHandleNm;
		public string sActCategorySeq;
		public string sEmailAddr;
		public string sPicType;
		public int iNonExistMailAddrFlagMask;
		public string sMainCharOnlyFlag;
		public string sManagerSeq;
		public string sRegistDayFrom;
		public string sRegistDayTo;
		public string sLastLoginDayFrom;
		public string sLastLoginDayTo;

		public string sFinalLoginDayFrom;
		public string sFinalLoginDayTo;
		
		public string sReportAffiliateDateFrom;
		public string sReportAffiliateDateTo;
		public string sTotalPaymentAmtFrom;
		public string sTotalPaymentAmtTo;
		public string sNaFlag;
		public string sUserRank;
		public string sAdCd;
		public string sTel;
		public string sCastAttrValue1;
		public string sCastAttrValue2;
		public string sCastAttrValue3;
		public string sCastAttrValue4;
		public string sAgeFrom;
		public string sAgeTo;
		public string sFavoritMeCountFrom;
		public string sFavoritMeCountTo;
		public int iUserStatusMask;
		public string sPickupId;
		public string sGuid;
		public string sRemarks1;
		public string sEnabledBlogFlag;
		public string sEnabledRichinoFlag;
		public string sSiteUseStatus;
		public string sInfoMailRxType;
		public string sUserMailRxType;
		public string sUserDefineMask;
		public string sExceptRefusedByManUserSeq;
		public string sLastTxMailDateFrom;
		public string sLastTxMailDateTo;
		public string sGameHandleNm;
		public string sGameCharacterType;
		public string sGameRegistDateFrom;
		public string sGameRegistDateTo;
		public string sGameCharacterLevelFrom;
		public string sGameCharacterLevelTo;
		public string sGamePointFrom;
		public string sGamePointTo;
		public string sCastGamePointFrom;
		public string sCastGamePointTo;
		public string sGuardianTel;
		public bool bHandleNmExactFlag;
		public string sBeforeSystemId;
		public string sKeyword;
		public int iCarrier;
		public bool bLoginIdNegativeFlag;
		public bool bLastActionDayNegativeFlag;
		public bool bUserDefineMaskNegatieFlag;
		public bool bIntroducerFriendAllFlag;
		public string sCrosmileLastUsedVersionFrom;
		public string sCrosmileLastUsedVersionTo;
		public string sUseCrosmileFlag;
		public string sUseVoiceappFlag;
		public string sGcappLastLoginVersionFrom;
		public string sGcappLastLoginVersionTo;
		public string sUseJealousyFlag;
		public bool bExcludeEmailAddr;
		public string sTxMuller3NgMailAddrFlag;
		public string sSortExpression;
		public string sSortDirection;
		public string sNotPaymentAmtFrom;
		public string sNotPaymentAmtTo;

		public PageCollectionParameters() {
			this.sOnline = "";
			this.sLoginId = "";
			this.sUserSeq = "";
			this.sProductionSeq = "";
			this.sCastNm = "";
			this.sSiteCd = "";
			this.sTalkType = "";
			this.sTerminalType = "";
			this.sHandleNm = "";
			this.sActCategorySeq = "";
			this.sEmailAddr = "";
			this.sPicType = "";
			this.iNonExistMailAddrFlagMask = 0;
			this.sMainCharOnlyFlag = "";
			this.sManagerSeq = "";
			this.sRegistDayFrom = "";
			this.sRegistDayTo = "";
			this.sLastLoginDayFrom = "";
			this.sLastLoginDayTo = "";

			this.sFinalLoginDayFrom = "";
			this.sFinalLoginDayTo = "";
			
			this.sReportAffiliateDateFrom = "";
			this.sReportAffiliateDateTo = "";
			this.sTotalPaymentAmtFrom = "";
			this.sTotalPaymentAmtTo = "";
			this.sNaFlag = "";
			this.sUserRank = "";
			this.sAdCd = "";
			this.sTel = "";
			this.sCastAttrValue1 = "";
			this.sCastAttrValue2 = "";
			this.sCastAttrValue3 = "";
			this.sCastAttrValue4 = "";
			this.sAgeFrom = "";
			this.sAgeTo = "";
			this.sFavoritMeCountFrom = "";
			this.sFavoritMeCountTo = "";
			this.iUserStatusMask = 0;
			this.sPickupId = "";
			this.sGuid = "";
			this.sRemarks1 = string.Empty;
			this.sEnabledBlogFlag = string.Empty;
			this.sEnabledRichinoFlag = string.Empty;
			this.sSiteUseStatus = string.Empty;
			this.sInfoMailRxType = string.Empty;
			this.sUserMailRxType = string.Empty;
			this.sUserDefineMask = string.Empty;
			this.sExceptRefusedByManUserSeq = string.Empty;
			this.sLastTxMailDateFrom = string.Empty;
			this.sLastTxMailDateTo = string.Empty;
			this.sGameHandleNm = string.Empty;
			this.sGameCharacterType = string.Empty;
			this.sGameRegistDateFrom = string.Empty;
			this.sGameRegistDateTo = string.Empty;
			this.sGameCharacterLevelFrom = string.Empty;
			this.sGameCharacterLevelTo = string.Empty;
			this.sGamePointFrom = string.Empty;
			this.sGamePointTo = string.Empty;
			this.sCastGamePointFrom = string.Empty;
			this.sCastGamePointTo = string.Empty;
			this.bHandleNmExactFlag = false;
			this.sBeforeSystemId = string.Empty;
			this.sKeyword = string.Empty;
			this.iCarrier = 0;
			this.bLoginIdNegativeFlag = false;
			this.bLastActionDayNegativeFlag = false;
			this.bUserDefineMaskNegatieFlag = false;
			this.bIntroducerFriendAllFlag = false;
			this.sCrosmileLastUsedVersionFrom = string.Empty;
			this.sCrosmileLastUsedVersionTo = string.Empty;
			this.sUseCrosmileFlag = string.Empty;
			this.sUseVoiceappFlag = string.Empty;
			this.sGcappLastLoginVersionFrom = string.Empty;
			this.sGcappLastLoginVersionTo = string.Empty;
			this.sUseJealousyFlag = string.Empty;
			this.bExcludeEmailAddr = false;
			this.sTxMuller3NgMailAddrFlag = string.Empty;
			this.sSortExpression = string.Empty;
			this.sSortDirection = string.Empty;
			this.sNotPaymentAmtFrom = string.Empty;
			this.sNotPaymentAmtTo = string.Empty;
		}
	}
	protected PageCollectionParameters GetSearchParamList() {
		PageCollectionParameters oRetDatas = new PageCollectionParameters();

		int iUserStatus;
		int iCarrier;
		int iNonExistMailAddrFlag;
		string sSiteUseStatus = null;
		SetupCondition(out iUserStatus, out iCarrier, out iNonExistMailAddrFlag);

		string sMainCharOnlyFlag = "1";

		string sOnlineStatus = string.Empty;
		foreach (ListItem oListItem in chkOnlineStatus.Items) {
			if (!oListItem.Selected)
				continue;
			if (!string.IsNullOrEmpty(sOnlineStatus)) {
				sOnlineStatus += ",";
			}
			sOnlineStatus += oListItem.Value;
		}
		string sNaFlags = string.Empty;
		foreach (ListItem oListItem in chkNaFlag.Items) {
			if (!oListItem.Selected)
				continue;

			if (!sNaFlags.Equals(string.Empty)) {
				sNaFlags += ",";
			}
			sNaFlags += oListItem.Value;
		}
		if (sNaFlags.Equals(string.Empty)) {
			sNaFlags = ViCommConst.WITHOUT;
		}

		if (this.chkChat.Checked == true) {
			sSiteUseStatus = "0";
		}
		if (string.IsNullOrEmpty(sSiteUseStatus)) {
			if (this.chkGame.Checked == true) {
				sSiteUseStatus = "1";
			}
		} else {
			if (this.chkGame.Checked == true) {
				sSiteUseStatus = sSiteUseStatus + ",1";
			}
		}
		if (string.IsNullOrEmpty(sSiteUseStatus)) {
			if (this.chkChatGamet.Checked == true) {
				sSiteUseStatus = "2";
			}
		} else {
			if (this.chkChatGamet.Checked == true) {
				sSiteUseStatus = sSiteUseStatus + ",2";
			}
		}

		oRetDatas.sOnline = sOnlineStatus;
		oRetDatas.sLoginId = this.chkLoginIdListVisible.Checked ? TrimEnd(txtLoginIdList.Text) : TrimEnd(txtLoginId.Text);
		oRetDatas.sUserSeq = this.txtUserSeq.Text;
		oRetDatas.sProductionSeq = lstProductionSeq.SelectedValue;
		oRetDatas.sCastNm = txtCastNm.Text.Replace("　","").Replace(" ","");
		oRetDatas.sSiteCd = lstSiteCd.SelectedValue;
		oRetDatas.sTalkType = lstConnectType.SelectedValue;
		oRetDatas.sTerminalType = "";
		oRetDatas.sHandleNm = TrimEnd(txtHandelNm.Text);
		oRetDatas.sActCategorySeq = lstActCategorySeq.SelectedValue;
		oRetDatas.sEmailAddr = TrimEnd(txtEmailAddr.Text);
		oRetDatas.sPicType = "";
		oRetDatas.iNonExistMailAddrFlagMask = iNonExistMailAddrFlag;
		oRetDatas.sMainCharOnlyFlag = sMainCharOnlyFlag;
		oRetDatas.sManagerSeq = lstManager.SelectedValue;
		oRetDatas.sRegistDayFrom = txtRegistDayFrom.Text;
		oRetDatas.sRegistDayTo = txtRegistDayTo.Text;
		oRetDatas.sLastLoginDayFrom = txtLastLoginDayFrom.Text;
		oRetDatas.sLastLoginDayTo = txtLastLoginDayTo.Text;

		oRetDatas.sFinalLoginDayFrom = txtFinalLoginDayFrom.Text;
		oRetDatas.sFinalLoginDayTo = txtFinalLoginDayTo.Text;
		
		oRetDatas.sReportAffiliateDateFrom = txtReportAffiliateDateFrom.Text;
		oRetDatas.sReportAffiliateDateTo = txtReportAffiliateDateTo.Text;
		oRetDatas.sTotalPaymentAmtFrom = txtTotalPaymentAmtFrom.Text;
		oRetDatas.sTotalPaymentAmtTo = txtTotalPaymentAmtTo.Text;
		oRetDatas.sNaFlag = sNaFlags;
		oRetDatas.sUserRank = lstUserRankSeek.SelectedValue;
		oRetDatas.sAdCd = this.chkAdCdListVisible.Checked ? TrimEnd(txtAdCdList.Text) : TrimEnd(txtAdCd.Text);
		oRetDatas.sTel = TrimEnd(txtTel.Text);
		oRetDatas.sCastAttrValue1 = lstCastAttrSeq0.SelectedValue;
		oRetDatas.sCastAttrValue2 = lstCastAttrSeq1.SelectedValue;
		oRetDatas.sCastAttrValue3 = lstCastAttrSeq2.SelectedValue;
		oRetDatas.sCastAttrValue4 = lstCastAttrSeq3.SelectedValue;
		oRetDatas.sAgeFrom = TrimEnd(txtAgeFrom.Text);
		oRetDatas.sAgeTo = TrimEnd(txtAgeTo.Text);
		oRetDatas.sFavoritMeCountFrom = TrimEnd(txtFavoritMeCountFrom.Text);
		oRetDatas.sFavoritMeCountTo = TrimEnd(txtFavoritMeCountTo.Text);
		oRetDatas.iUserStatusMask = iUserStatus;
		oRetDatas.sPickupId = lstPickupId.SelectedValue;
		oRetDatas.sGuid = TrimEnd(txtGuid.Text);
		oRetDatas.sRemarks1 = txtRemarks1.Text.TrimEnd();
		oRetDatas.sEnabledBlogFlag = this.rdoEnabledBlog.SelectedValue;
		oRetDatas.sEnabledRichinoFlag = this.rdoEnaledRichino.SelectedValue;
		oRetDatas.sSiteUseStatus = sSiteUseStatus;
		oRetDatas.sInfoMailRxType = this.lstInfoMailRxType.SelectedValue;
		oRetDatas.sUserMailRxType = string.Empty;
		int iUserDefineMask = 0;
		foreach (ListItem oListItem in this.chkUserDefineMask.Items) {
			if (oListItem.Selected) {
				iUserDefineMask += int.Parse(oListItem.Value);
			}
		}
		oRetDatas.sUserDefineMask = iUserDefineMask > 0 ? iUserDefineMask.ToString() : string.Empty;
		oRetDatas.sExceptRefusedByManUserSeq = this.TxMailManUserSeq;
		oRetDatas.sLastTxMailDateFrom = this.txtLastTxMailDateFrom.Text;
		oRetDatas.sLastTxMailDateTo = this.txtLastTxMailDateTo.Text;
		oRetDatas.sGameHandleNm = this.txtGameHandleNm.Text;
		List<string> oCharType = new List<string>();
		foreach (ListItem oListItem in this.chkGameCharacterType.Items) {
			if (oListItem.Selected) {
				oCharType.Add(string.Format("'{0}'",oListItem.Value));
			}
		}
		oRetDatas.sGameCharacterType = string.Join(",",oCharType.ToArray());
		oRetDatas.sGameRegistDateFrom = this.txtGameRegistDateFrom.Text;
		oRetDatas.sGameRegistDateTo = this.txtGameRegistDateTo.Text;
		oRetDatas.sGameCharacterLevelFrom = this.txtGameCharacterLevelFrom.Text;
		oRetDatas.sGameCharacterLevelTo = this.txtGameCharacterLevelTo.Text;
		oRetDatas.sGamePointFrom = this.txtGamePointFrom.Text;
		oRetDatas.sGamePointTo = this.txtGamePointTo.Text;
		oRetDatas.sCastGamePointFrom = this.txtCastGamePointFrom.Text;
		oRetDatas.sCastGamePointTo = this.txtCastGamePointTo.Text;
		oRetDatas.sGuardianTel = TrimEnd(txtGuardianTel.Text);

		oRetDatas.bHandleNmExactFlag = this.chkHandleNmExactFlag.Checked;

		oRetDatas.sBeforeSystemId = this.txtBeforeSystemId.Text;
		oRetDatas.sKeyword = this.txtKeyword.Text;
		oRetDatas.iCarrier = iCarrier;
		oRetDatas.bLoginIdNegativeFlag = this.chkLoginIdListVisible.Checked ? this.chkLoginIdListNegativeFlag.Checked : this.chkLoginIdNegativeFlag.Checked;
		oRetDatas.bLastActionDayNegativeFlag = this.chkLastActionDayNegativeFlag.Checked;
		oRetDatas.bUserDefineMaskNegatieFlag = this.chkUserDefineMaskNegatieFlag.Checked;
		oRetDatas.bIntroducerFriendAllFlag = this.chkIntroducerFriendAllFlag.Checked;
		oRetDatas.sCrosmileLastUsedVersionFrom = this.txtCrosmileLastUsedVersionFrom.Text.Trim();
		oRetDatas.sCrosmileLastUsedVersionTo = this.txtCrosmileLastUsedVersionTo.Text.Trim();
		oRetDatas.sUseCrosmileFlag = this.rdoUseCrosmileFlag.SelectedValue;
		oRetDatas.sUseVoiceappFlag = this.rdoUseVoiceappFlag.SelectedValue;
		oRetDatas.sGcappLastLoginVersionFrom = this.txtGcappLastLoginVersionFrom.Text.Trim();
		oRetDatas.sGcappLastLoginVersionTo = this.txtGcappLastLoginVersionTo.Text.Trim();
		oRetDatas.sUseJealousyFlag = this.rdoUseJealousyFlag.SelectedValue;
		oRetDatas.bExcludeEmailAddr = this.chkExcludeEmailAddr.Checked;
		oRetDatas.sTxMuller3NgMailAddrFlag = this.rdoTxMuller3NgMailAddrFlag.SelectedValue;
		oRetDatas.sSortExpression = this.SortExpression;
		oRetDatas.sSortDirection = this.SortDirect;
		oRetDatas.sNotPaymentAmtFrom = txtNotPaymentAmtFrom.Text;
		oRetDatas.sNotPaymentAmtTo = txtNotPaymentAmtTo.Text;

		return oRetDatas;
	}
	protected string TrimEnd(string pTargetStr) {
		char[] trims = { ' ' };
		string sRetStr = pTargetStr.TrimEnd(trims);

		return sRetStr;
	}


	#region === 検索条件保存 ================================================================================

	protected void lstSeekCondition_SelectedIndexChanged(object sender,EventArgs e) {
		this.ReSearchBySeekCondition();
	}

	protected void lnkEditSeekCondition_Click(object sender,EventArgs e) {
		if (!this.IsValid)
			return;
		this.InitSeekConditionPanel();

		using (SeekCondition oSeekCondition = new SeekCondition()) {
			using (DataSet oSeekConditionDataSet = oSeekCondition.GetOne(this.lstSeekCondition.SelectedValue)) {
				if (oSeekConditionDataSet.Tables[0].Rows.Count == 0) {
					return;
				}

				DataRow oSeekConditionDataRow = oSeekConditionDataSet.Tables[0].Rows[0];

				this.hdnSeekConditionSeq.Value = iBridUtil.GetStringValue(oSeekConditionDataRow["SEEK_CONDITION_SEQ"]);
				this.hdnSeekConditionRevisionNo.Value = iBridUtil.GetStringValue(oSeekConditionDataRow["REVISION_NO"]);
				this.txtSeekConditionNm.Text = iBridUtil.GetStringValue(oSeekConditionDataRow["SEEK_CONDITION_NM"]);
				this.chkSeekConditionPublish.Checked = iBridUtil.GetStringValue(oSeekConditionDataRow["PUBLISH_FLAG"]).Equals(ViCommConst.FLAG_ON_STR);
				this.btnDeleteSeekCondition.Visible = true;
			}
		}
	}
	protected void lnkAppendSeekCondition_Click(object sender,EventArgs e) {
		if (!this.IsValid)
			return;
		this.InitSeekConditionPanel();
	}

	protected void btnSaveSeekCondition_Click(object sender,EventArgs e) {
		if (!this.IsValid)
			return;
		UpdateSeekCondition(false);
	}
	protected void btnDeleteSeekCondition_Click(object sender,EventArgs e) {
		UpdateSeekCondition(true);
	}
	protected void btnCancelSeekCondition_Click(object sender,EventArgs e) {
		this.pnlKey.Enabled = true;
		this.pnlSeekConditionMainte.Visible = false;
	}

	private void InitSeekConditionPanel() {
		this.pnlKey.Enabled = false;
		this.pnlSeekConditionMainte.Visible = true;

		this.hdnSeekConditionSeq.Value = string.Empty;
		this.hdnSeekConditionRevisionNo.Value = string.Empty;
		this.txtSeekConditionNm.Text = string.Empty;
		this.chkSeekConditionPublish.Checked = false;
		this.btnDeleteSeekCondition.Visible = false;
	}

	private void InitSeekConditionList() {
		SeekConditionHelper.SetupSeekConditionDropDownList(SessionObjs.AdminId,ViCommConst.SeekConditionType.CastCharacter,this.lstSeekCondition);
		this.lnkEditSeekCondition.Enabled = false;
	}

	private void UpdateSeekCondition(bool pIsDelete) {
		string sSeekConditionSeq = this.hdnSeekConditionSeq.Value;
		int iPublishFlag = this.chkSeekConditionPublish.Checked ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF;
		string sRevisionNo = this.hdnSeekConditionRevisionNo.Value;
		string sSeekConditionNm = this.txtSeekConditionNm.Text.Trim();

		if (pIsDelete) {
			SeekConditionHelper.Delete(SessionObjs.AdminId,sSeekConditionSeq,sRevisionNo);
		} else {
			sSeekConditionSeq = SeekConditionHelper.Save(SessionObjs.AdminId,sSeekConditionSeq,ViCommConst.SeekConditionType.CastCharacter,sSeekConditionNm,iPublishFlag,sRevisionNo,this.pnlSeekCondition);
		}

		this.InitSeekConditionList();
		if (!pIsDelete) {
			this.lstSeekCondition.SelectedValue = sSeekConditionSeq;
		}
		this.ReSearchBySeekCondition();

		if (pIsDelete) {
			this.SetupSelectColumns();
		}
	}

	private void ReSearchBySeekCondition() {
		this.pnlKey.Enabled = true;
		this.pnlSeekConditionMainte.Visible = false;
		this.lnkEditSeekCondition.Enabled = false;

		string sSeekConditionSeq = this.lstSeekCondition.SelectedValue;

		this.InitPage();
		this.lstSeekCondition.SelectedValue = sSeekConditionSeq;

		if (!string.IsNullOrEmpty(sSeekConditionSeq) && !sSeekConditionSeq.StartsWith("*")) {

			DataSet oSeekConditionDataSet = SeekConditionHelper.Load(sSeekConditionSeq,this.pnlSeekCondition);
			if (oSeekConditionDataSet.Tables[0].Rows.Count > 0) {
				DataRow oSeekConditionDataRow = oSeekConditionDataSet.Tables[0].Rows[0];
				this.lnkEditSeekCondition.Enabled = iBridUtil.GetStringValue(oSeekConditionDataRow["ADMIN_ID"]).Equals(SessionObjs.AdminId);
			}
			this.SetupConditionLoginIdList(this.chkLoginIdListVisible.Checked);
			this.SetupConditionAdCdList(this.chkAdCdListVisible.Checked);
			this.SetupSelectColumnsVisible(!this.chkSelectColumnsVisible.Checked);
			this.SetupSelectColumns();
			ViewState["PICKUP"] = "0";
			GetList();
		}
	}
	#endregion ========================================================================================================

	protected void grdCast_Sorting(object sender,GridViewSortEventArgs e) {
		if (e.SortExpression.Equals("FAVORITE_COUNT")) {
			e.Cancel = false;
			return;
		}

		if (this.SortExpression.Equals(e.SortExpression)) {
			if (this.SortDirect.Equals("ASC")) {
				this.SortDirect = "DESC";
			} else if (this.SortDirect.Equals("DESC")) {
				this.SortDirect = "ASC";
			}
		} else {
			this.SortDirect = "ASC";
		}

		this.SortExpression = e.SortExpression;

		e.Cancel = true;

		ViewState["PICKUP"] = "0";
		this.GetList();
	}

	protected string ReplaceForm(object pHtmlDoc) {
		string sUrl = "";
		using (Site oSite = new Site()) {
			if (oSite.GetOne(lstSiteCd.SelectedValue)) {
				sUrl = oSite.url;
			}
		}

		string sDoc = Regex.Replace(pHtmlDoc.ToString(),"%%12%%",sUrl);
		sDoc = Regex.Replace(sDoc,"\r\n","<br />");
		return Regex.Replace(sDoc,@"<\/?[fF][oO][rR][mM].*?>",string.Empty,RegexOptions.Compiled);
	}

	protected void vdcManLoginId_ServerValidate(object source,ServerValidateEventArgs e) {
		if (e.IsValid) {
			using (MailTemplate oTemplate = new MailTemplate()) {
				if (oTemplate.GetOne(lstSiteCd.SelectedValue,lstMailTemplateNo.SelectedValue)) {
					if (!ViCommConst.MAIL_TP_APPROACH_ADMIN_TO_CAST.Equals(oTemplate.mailTemplateType)) {
						txtManLoginId.Text = string.Empty;
						return;
					}
				}
			}

			using (UserManCharacter oUserManCharacter = new UserManCharacter()) {
				e.IsValid = oUserManCharacter.IsExistLoginId(this.lstSiteCd.SelectedValue,txtManLoginId.Text,ViCommConst.MAIN_CHAR_NO);
				if (e.IsValid) {
					lblManHandleNm.Text = oUserManCharacter.handleNm;
					this.TxMailManUserSeq = oUserManCharacter.userSeq;
				} else {
					lblManHandleNm.Text = "";
					this.TxMailManUserSeq = string.Empty;
				}
			}
		}
	}

	protected void grdCast_DataBound(object sender,EventArgs e) {
		for (int i = 1; i < this.grdCast.Columns.Count; i++) {
			this.grdCast.Columns[i].Visible = this.chkSelectColumns.Items[i - 1].Selected;
		}
		//GridViewHelper.RegiserCreateGridHeader(1, this.grdCast, this.pnlGrid);
	}

	protected void lnkSelectColumns_Click(object sender,EventArgs e) {
		this.SetupSelectColumnsVisible(this.plcSelectColumns.Visible);
	}

	private void SetupSelectColumnsVisible(bool pIsVisible) {
		if (pIsVisible) {
			this.plcSelectColumns.Visible = false;
			this.lnkSelectColumns.Text = "[一覧項目選択表示]";
		} else {
			this.plcSelectColumns.Visible = true;
			this.lnkSelectColumns.Text = "[一覧項目選択非表示]";
		}
		this.chkSelectColumnsVisible.Checked = this.plcSelectColumns.Visible;
	}

	private void SetupSelectColumns() {
		foreach (string sColumnName in ColumnsSelectedArray) {
			ListItem oListItem = this.chkSelectColumns.Items.FindByText(sColumnName);
			if (oListItem != null) {
				oListItem.Selected = true;
			}
		}
	}

	protected void lnkDisplayLoginIdList_Click(object sender,EventArgs e) {
		this.SetupConditionLoginIdList(true);
	}

	protected void lnkHideLoginIdList_Click(object sender,EventArgs e) {
		this.SetupConditionLoginIdList(false);
	}

	private void SetupConditionLoginIdList(bool pIsVisible) {
		this.lnkDisplayLoginIdList.Visible = !pIsVisible;
		this.chkLoginIdNegativeFlag.Enabled = !pIsVisible;
		this.txtLoginId.Enabled = !pIsVisible;
		this.txtLoginId.BackColor = pIsVisible ? System.Drawing.SystemColors.Control : System.Drawing.Color.White;
		this.pnlLoginIdList.Visible = pIsVisible;
		this.chkLoginIdListVisible.Checked = pIsVisible;
		this.tdHeaderCastNm.Attributes["class"] = pIsVisible ? "tdHeaderStyle2" : "tdHeaderStyle";

	}

	protected void lnkDisplayAdCdList_Click(object sender, EventArgs e) {
		this.SetupConditionAdCdList(true);
	}

	protected void lnkHideAdCdList_Click(object sender, EventArgs e) {
		this.SetupConditionAdCdList(false);
	}

	private void SetupConditionAdCdList(bool pIsVisible) {
		using (ManageCompany oCompany = new ManageCompany()) {
			bool bIsCompanyVisible = oCompany.IsAvailableService(ViCommConst.RELEASE_INQUIRY_MULTI_AD_CD, 2);
			if (bIsCompanyVisible) {
				this.lnkDisplayAdCdList.Visible = !pIsVisible;
				this.txtAdCd.Enabled = !pIsVisible;
				this.txtAdCd.BackColor = pIsVisible ? System.Drawing.SystemColors.Control : System.Drawing.Color.White;
				this.lblAdNmNotVisibleStartFont.Text = pIsVisible ? "<font color=\"#999999\">" : string.Empty;
				this.lblAdNmNotVisibleEndFont.Text = pIsVisible ? "</font>" : string.Empty;
				this.pnlAdCdList.Visible = pIsVisible;
				this.chkAdCdListVisible.Checked = pIsVisible;
				this.lblAdNmList.Text = string.Empty;
			} else {
				this.lnkDisplayAdCdList.Visible = false;
				this.txtAdCd.Enabled = true;
				this.txtAdCd.BackColor = System.Drawing.Color.White;
				this.lblAdNmNotVisibleStartFont.Text = string.Empty;
				this.lblAdNmNotVisibleEndFont.Text = string.Empty;
				this.pnlAdCdList.Visible = false;
				this.chkAdCdListVisible.Checked = false;
				this.lblAdNmList.Text = string.Empty;
			}
		}

	}

	/// <summary>
	/// 予約送信ボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnTxReservationMail_Click(object sender,EventArgs e) {
		if (IsValid == false) {
			return;
		}

		// メール送信情報を保存
		TxInfoMailHistory.MainteData oMailHisData = new TxInfoMailHistory.MainteData();
		SetTxInfoMailHistory(ref oMailHisData,ViCommConst.FLAG_ON_STR);

		// メール送信履歴に移動して登録・更新したデータを表示
		Response.Redirect("/ADMIN/Mail/TxInfoMailHistory.aspx?sdate=" + oMailHisData.ReservationSendDate + "&sexcd=" + ViCommConst.OPERATOR);
	}

	/// <summary>
	/// メール送信情報を保存
	/// </summary>
	/// <param name="oMailHisData"></param>
	/// <param name="sReservationFlag"></param>
	private void SetTxInfoMailHistory(ref TxInfoMailHistory.MainteData oMailHisData,string sReservationFlag) {
		PageCollectionParameters oParamlist = GetSearchParamList();

		// 送信予定(検索結果)件数の取得
		int iRecCount = 0;
		int.TryParse(lblRecCount.Text,out iRecCount);

		// オリジナル本文
		string[] sDoc;
		int iDocCount;
		string sMailSendType = this.rdoMailSendType.SelectedValue;
		if (plcMailSendType.Visible == false) {
			sMailSendType = string.Empty;
		}
		SysPrograms.SeparateHtml("",ViCommConst.MAX_ORG_MAIL_BLOCKS,out sDoc,out iDocCount);

		// Json文字列へ変換
		//oParamStr.Remove(0,oParamStr.Length);
		//new JavaScriptSerializer().Serialize(sDoc,oParamStr);
		//string sDocJson = oParamStr.ToString();

		// 検索条件を配列に格納する
		PropertyInfo[] infoArray = oParamlist.GetType().GetProperties();
		FieldInfo[] fields = oParamlist.GetType().GetFields();
		int num = fields.Length;
		List<string> keys = new List<string>();
		List<string> vals = new List<string>();
		foreach (FieldInfo info in fields) {
			keys.Add(info.Name);
			if (info.GetValue(oParamlist) == null) {
				vals.Add(string.Empty);
			} else {
				vals.Add(info.GetValue(oParamlist).ToString());
			}
		}

		// 送信予定日時
		DateTime oSendDate;
		if (sReservationFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
			// 手動送信の場合、現在日時を設定する
			oSendDate = DateTime.Now;
		} else if (string.IsNullOrEmpty(txtReservationSendDay.Text)) {
			// 送信予定日が未入力の場合、現在日付を設定する
			oSendDate = DateTime.Today;
		} else if (!string.IsNullOrEmpty(txtReservationSendHour.Text)
			&& !string.IsNullOrEmpty(txtReservationSendMinute.Text)
		) {
			// 送信予定日時分が全て入力されてる場合、日時分が正しいかチェックして設定する
			string sDT = string.Format("{0} {1}:{2}:00",
				txtReservationSendDay.Text,
				txtReservationSendHour.Text,
				txtReservationSendMinute.Text
			);
			if (!DateTime.TryParse(sDT,out oSendDate)) {
				oSendDate = DateTime.Today;
			}
		} else if (!string.IsNullOrEmpty(txtReservationSendHour.Text)) {
			// 送信予定日時が入力されてる場合、日時が正しいかチェックして設定する
			string sDT = string.Format("{0} {1}:00:00",txtReservationSendDay.Text,txtReservationSendHour.Text);
			if (!DateTime.TryParse(sDT,out oSendDate)) {
				oSendDate = DateTime.Today;
			}
		} else {
			// 送信予定日のみ入力されてる場合、日付が正しいかチェックして設定する
			string sDT = string.Format("{0} 00:00:00",txtReservationSendDay.Text);
			if (!DateTime.TryParse(sDT,out oSendDate)) {
				oSendDate = DateTime.Today;
			}
		}

		oMailHisData.SiteCd = lstSiteCd.SelectedValue;
		oMailHisData.MailTemplateNo = lstMailTemplateNo.SelectedValue;
		oMailHisData.MailServer = rdoMailServer.SelectedValue;
		oMailHisData.MailSendType = rdoMailSendType.SelectedValue;
		oMailHisData.MailType = this.MailTemplateType;
		oMailHisData.RxSexCd = ViCommConst.OPERATOR;
		//oMailHisData.Doc = sDocJson;
		oMailHisData.DocCount = sDoc.Length;
		oMailHisData.aConditionNames = keys.ToArray();
		oMailHisData.aConditionVals = vals.ToArray();
		oMailHisData.ConditionCount = oMailHisData.aConditionNames.Length;
		oMailHisData.ReservationSendDate = oSendDate.ToString("yyyy/MM/dd HH:mm:ss");
		oMailHisData.ReservationSendNum = iRecCount;
		oMailHisData.ReservationFlag = sReservationFlag;
		oMailHisData.AdminId = Session["AdminId"].ToString();

		string sSeq = null;
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["rmhseq"]))) {
			sSeq = iBridUtil.GetStringValue(Request.QueryString["rmhseq"]);
		}
		string sRNo = null;
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["revno"]))) {
			sRNo = iBridUtil.GetStringValue(Request.QueryString["revno"]);
		}

		// 登録
		TxInfoMailHistory oTxInfoMailHistory = new TxInfoMailHistory();
		oTxInfoMailHistory.Mainte(oMailHisData,sRNo,sSeq,ViCommConst.FLAG_OFF_STR);
	}

	/// <summary>
	/// 予約送信設定された条件を取得
	/// </summary>
	/// <returns></returns>
	private void GetReservationParamList() {
		// post通信時
		if (IsPostBack) {
			this.oReservationParamlist = null;
			this.oReservationMailParamlist = null;
			return;
		}

		// 取得済みの場合
		if (this.oReservationParamlist != null) {
			return;
		}

		// お知らせメール送信履歴からの遷移
		string sReservationSeq = iBridUtil.GetStringValue(Request.QueryString["rmhseq"]);
		if (string.IsNullOrEmpty(sReservationSeq)) {
			return;
		}

		this.oReservationMailParamlist = new TxInfoMailHistory.MainteData();
		this.oReservationParamlist = new PageCollectionParameters();

		// 予約送信条件の取得
		TxInfoMailHistory oTxInfoMailHistory = new TxInfoMailHistory();
		DataSet oDS = oTxInfoMailHistory.GetOne(sReservationSeq);
		if (oDS.Tables[0].Rows.Count > 0) {
			DataRow oDR = oDS.Tables[0].Rows[0];

			// メール送信設定欄の内容取得
			this.oReservationMailParamlist.MailTemplateNo = oDR["MAIL_TEMPLATE_NO"].ToString();
			this.oReservationMailParamlist.MailServer = oDR["MAIL_SERVER"].ToString();
			this.oReservationMailParamlist.MailSendType = oDR["MAIL_SEND_TYPE"].ToString();
			this.oReservationMailParamlist.MailType = oDR["MAIL_TYPE"].ToString();
			if (oDR["RESERVATION_SEND_DATE"] != DBNull.Value) {
				DateTime oDT = Convert.ToDateTime(oDR["RESERVATION_SEND_DATE"]);
				this.oReservationMailParamlist.ReservationSendDate = oDT.ToString("yyyy/MM/dd HH:mm:00");
			}
		}

		// 検索条件欄の内容取得
		DataSet oDSDtl = oTxInfoMailHistory.GetDetail(sReservationSeq);
		if (oDSDtl.Tables[0].Rows.Count > 0) {
			foreach (DataRow oDRDtl in oDSDtl.Tables[0].Rows) {
				FieldInfo oFInfo = this.oReservationParamlist.GetType().GetField(oDRDtl["NAME"].ToString());
				// それぞれプロパティの型にあわせて変換した後に設定する
				if (oDRDtl["VALUE"] == DBNull.Value) {
					// Nullの場合は何もしない
				} else if (oFInfo.FieldType == typeof(int) || oFInfo.FieldType == typeof(Int32)) {
					oFInfo.SetValue(this.oReservationParamlist,int.Parse(oDRDtl["VALUE"].ToString()));
				} else if (oFInfo.FieldType == typeof(bool)) {
					oFInfo.SetValue(this.oReservationParamlist,bool.Parse(oDRDtl["VALUE"].ToString()));
				} else {
					oFInfo.SetValue(this.oReservationParamlist,oDRDtl["VALUE"]);
				}
			}
		}
	}

	/// <summary>
	/// RadioButtonListのSelectedIndexを値から検索して設定する
	/// (SelectedIndexとSelectedValueにより同時に設定するとエラーが発生するため)
	/// </summary>
	/// <param name="oList"></param>
	/// <param name="sVal"></param>
	private void RadioButtonList_SetSelectedIndexByValue(RadioButtonList oList,string sVal) {
		oList.SelectedIndex = oList.Items.IndexOf(oList.Items.FindByValue(sVal));
	}

	/// <summary>
	/// DropDownListのSelectedIndexを値から検索して設定する
	/// (SelectedIndexとSelectedValueにより同時に設定するとエラーが発生するため)
	/// </summary>
	/// <param name="oList"></param>
	/// <param name="sVal"></param>
	private void DropDownList_SetSelectedIndexByValue(DropDownList oList,string sVal) {
		oList.SelectedIndex = oList.Items.IndexOf(oList.Items.FindByValue(sVal));
	}

	#region □■□ 一括更新関連 □■□ ================================================================================
	protected void btnSetupBulkUpdate_Click(object sender,EventArgs e) {
		this.pnlKey.Enabled = false;
		this.pnlBulkUpdate.Visible = true;
		this.btnBulkUpdUserStatus.Enabled = true;
		this.btnBulkUpdAdCd.Enabled = true;
		this.btnBulkUpdMailFlag.Enabled = true;
		this.btnBulkDelete.Enabled = true;

		this.lstUserStatus.DataBind();
	}

	protected void btnCancelBulkUpdate_Click(object sender,EventArgs e) {
		pnlKey.Enabled = true;
		pnlBulkUpdate.Visible = false;
	}

	protected void btnExecuteBulkUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid)
			return;

		List<string> oUserSeqList = new List<string>();
		List<string> oUserCharNoList = new List<string>();
		using (DataSet oDs = this.GetListDs(false)) {
			if (oDs.Tables.Count > 0) {
				foreach (DataRow oDr in oDs.Tables[0].Rows) {
					string sUserSeq = iBridUtil.GetStringValue(oDr["USER_SEQ"]);
					string sUserCharNo = iBridUtil.GetStringValue(oDr["USER_CHAR_NO"]);
					if (!string.IsNullOrEmpty(sUserSeq) && !string.IsNullOrEmpty(sUserCharNo)) {
						oUserSeqList.Add(sUserSeq);
						oUserCharNoList.Add(sUserCharNo);
					}

				}
			}
		}
		if (oUserSeqList.Count > 0) {

			int sModifyType = -1;
			string sUserStatus = null;
			string sAdCd = null;
			string pMAIL_ADDR_FLAG = null;

			if (this.tabBulkUpdate.ActiveTabIndex == 0) {
				sModifyType = ViCommConst.BulkUpdateCastType.USER_STATUS;
				sUserStatus = this.lstUserStatus.SelectedValue;
			} else if (this.tabBulkUpdate.ActiveTabIndex == 1) {
				sModifyType = ViCommConst.BulkUpdateCastType.AD_CD;
				sAdCd = this.txtAdCd4BulkUpd.Text;
			} else if (this.tabBulkUpdate.ActiveTabIndex == 2) {
				sModifyType = ViCommConst.BulkUpdateCastType.MAIL_ADDR_FLAG;
				pMAIL_ADDR_FLAG = ViCommConst.FLAG_OFF_STR;
			} else if (this.tabBulkUpdate.ActiveTabIndex == 3) {
				if (chkBulkDeleteCharacter.Checked) {
					sModifyType = ViCommConst.BulkUpdateCastType.DELETE_CHAR;
				} else {
					sModifyType = ViCommConst.BulkUpdateCastType.DELETE;
				}
			} else {
				throw new InvalidOperationException();
			}

			switch (sModifyType) {
				case ViCommConst.BulkUpdateCastType.AD_CD:
				case ViCommConst.BulkUpdateCastType.DELETE_CHAR:
					using (DbSession oDbSession = new DbSession()) {
						oDbSession.PrepareProcedure("CAST_CHARACTER_BULK_UPDATE");
						oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
						oDbSession.ProcedureInArrayParm("pUSER_SEQ_ARR",DbSession.DbType.VARCHAR2,oUserSeqList.Count,oUserSeqList.ToArray());
						oDbSession.ProcedureInArrayParm("pUSER_CHAR_NO_ARR",DbSession.DbType.VARCHAR2,oUserSeqList.Count,oUserCharNoList.ToArray());
						oDbSession.ProcedureInParm("pMODIFY_TYPE",DbSession.DbType.NUMBER,sModifyType);
						oDbSession.ProcedureInParm("pAD_CD",DbSession.DbType.VARCHAR2,sAdCd);
						oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
						oDbSession.cmd.BindByName = true;
						oDbSession.ExecuteProcedure();
					}
					break;
				case ViCommConst.BulkUpdateCastType.USER_STATUS:
				case ViCommConst.BulkUpdateCastType.MAIL_ADDR_FLAG:
				case ViCommConst.BulkUpdateCastType.DELETE:
					using (DbSession oDbSession = new DbSession()) {
						oDbSession.PrepareProcedure("CAST_BASIC_BULK_UPDATE");
						oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
						oDbSession.ProcedureInArrayParm("pUSER_SEQ_ARR",DbSession.DbType.VARCHAR2,oUserSeqList.Count,oUserSeqList.ToArray());
						oDbSession.ProcedureInParm("pMODIFY_TYPE",DbSession.DbType.NUMBER,sModifyType);
						oDbSession.ProcedureInParm("pUSER_STATUS",DbSession.DbType.VARCHAR2,sUserStatus);
						oDbSession.ProcedureInParm("pMAIL_ADDR_FLAG",DbSession.DbType.NUMBER,pMAIL_ADDR_FLAG);
						oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
						oDbSession.cmd.BindByName = true;
						oDbSession.ExecuteProcedure();
					}
					break;
			}
		}
		this.btnBulkUpdUserStatus.Enabled = false;
		this.btnBulkUpdAdCd.Enabled = false;
		this.btnBulkUpdMailFlag.Enabled = false;
		this.btnBulkDelete.Enabled = false;

		this.GetList();
	}
	#endregion ========================================================================================================
}

