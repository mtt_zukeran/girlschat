<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MovieUpload.aspx.cs" Inherits="Cast_MovieUpload" Title="動画アップロード" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="動画アップロード"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend><%= DisplayWordUtil.Replace("[出演者動画アップロード]") %></legend>
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									<asp:Label ID="lblHeader" runat="server" Text="Label"></asp:Label>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblSiteCd" runat="server" Text="Label"></asp:Label>-
									<asp:Label ID="lblLoginId" runat="server" Text="Label"></asp:Label>
									<asp:Label ID="lblUserSeq" runat="server" Text="Label" Visible="false"></asp:Label>
									<asp:Label ID="lblUserCharNo" runat="server" Text="Label"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("出演者名.") %>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblCastNm" runat="server" Text="Label"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									アスペクト比
								</td>
								<td class="tdDataStyle">
									<asp:RadioButton ID="rdoAspectNone"   runat="server" GroupName="aspect" Checked="true"  Text="指定しない"></asp:RadioButton>
									<asp:RadioButton ID="rdoAspectNormal" runat="server" GroupName="aspect" Checked="false" Text="1.33:1（4:3）"></asp:RadioButton>
									<asp:RadioButton ID="rdoAspectWide"   runat="server" GroupName="aspect" Checked="false" Text="1.78:1（16:9）"></asp:RadioButton>
								</td>
							</tr>
							<asp:PlaceHolder ID="plcHolder" runat="server">
								<tr>
									<td class="tdHeaderStyle">
										動画属性
									</td>
									<td class="tdDataStyle">
										<asp:DropDownList ID="lstCastMovieAttrType" runat="server" DataSourceID="dsCastMovieAttrType" DataTextField="CAST_MOVIE_ATTR_TYPE_NM" DataValueField="CAST_MOVIE_ATTR_TYPE_SEQ"
											AutoPostBack="True" OnSelectedIndexChanged="lstCastMovieAttrType_SelectedIndexChanged">
										</asp:DropDownList>
										<asp:RequiredFieldValidator ID="vdrCastMovieAttrType" runat="server" ErrorMessage="動画属性を選択して下さい。" ControlToValidate="lstCastMovieAttrType" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										動画属性値
									</td>
									<td class="tdDataStyle">
										<asp:DropDownList ID="lstCastMovieAttrTypeValue" runat="server" DataSourceID="dsCastMovieAttrTypeValue" DataTextField="CAST_MOVIE_ATTR_NM" DataValueField="CAST_MOVIE_ATTR_SEQ">
										</asp:DropDownList>
									</td>
									<asp:RequiredFieldValidator ID="vdrCastMovieAttrTypeValue" runat="server" ErrorMessage="動画属性値を選択して下さい。" ControlToValidate="lstCastMovieAttrTypeValue" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</tr>
							</asp:PlaceHolder>
							<tr>
								<td class="tdHeaderStyle2">
									ファイルアップロード
								</td>
								<td class="tdDataStyle">
									<asp:FileUpload ID="uldCastMovie" runat="server" Width="400px" />
									<asp:RequiredFieldValidator ID="rfvUplod" runat="server" ErrorMessage="アップロードファイルが入力されていません。" SetFocusOnError="True" ValidationGroup="Upload" ControlToValidate="uldCastMovie">*</asp:RequiredFieldValidator>
								</td>
							</tr>
						</table>
						<asp:Button ID="btnUpload" runat="server" ValidationGroup="Upload" Text="実行" CssClass="seekbutton" OnClick="btnUpload_Click" />
						<asp:Button runat="server" ID="btnCancel" Text="戻る" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsCastMovieAttrType" runat="server" SelectMethod="GetListIncDefault" TypeName="CastMovieAttrType" OnSelecting="dsCastMovieAttrType_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastMovieAttrTypeValue" runat="server" SelectMethod="GetListIncDefault" TypeName="CastMovieAttrTypeValue" OnSelecting="dsCastMovieAttrTypeValue_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pCastMovieAttrTypeSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="rfvUplod" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrCastMovieAttrType" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrCastMovieAttrTypeValue" HighlightCssClass="validatorCallout" />
</asp:Content>
