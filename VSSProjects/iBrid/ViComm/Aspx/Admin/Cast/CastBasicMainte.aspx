<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastBasicMainte.aspx.cs" Inherits="Cast_CastBasicMainte" Title="出演者基本情報設定" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="出演者基本情報設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>
					<%= DisplayWordUtil.Replace("[出演者情報]") %>
				</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					<table border="0">
						<tr style="margin-bottom: 0px">
							<td valign="top">
								<table border="0" style="width: 540px" class="tableStyle">
									<tr>
										<td class="tdHeaderStyle">
											SEQ.
										</td>
										<td class="tdDataStyle">
											<asp:Label ID="lblUserSeq" runat="server" Text="Label"></asp:Label>
											<asp:Label ID="lblRegist" runat="server" Text="新規登録"></asp:Label>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											<%= DisplayWordUtil.Replace("出演者名") %>
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtCastNm" runat="server" MaxLength="50" Width="200px"></asp:TextBox>
											<asp:RequiredFieldValidator ID="vdrCastNm" runat="server" ErrorMessage="出演者名を入力して下さい。" ControlToValidate="txtCastNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											<%= DisplayWordUtil.Replace("出演者名(ｶﾅ)") %>
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtCastKanaNm" runat="server" MaxLength="50" Width="200px"></asp:TextBox>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											生年月日
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtBirthDay" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
											<asp:RangeValidator ID="vddBirthDay" runat="server" ErrorMessage="生年月日を正しく入力して下さい。" ControlToValidate="txtBirthDay" MaximumValue="2099/12/31" MinimumValue="1940/01/01"
												Type="Date" ValidationGroup="Detail" Display="Dynamic">*</asp:RangeValidator>
											<asp:RegularExpressionValidator ID="vdeBirthDay" runat="server" ErrorMessage="生年月日を正しく入力して下さい。" ControlToValidate="txtBirthDay" ValidationExpression="\d{4}/\d{2}/\d{2}"
												ValidationGroup="Detail" Display="Dynamic">*</asp:RegularExpressionValidator>
											<asp:CustomValidator ID="vdcBirthDay" runat="server" ControlToValidate="txtBirthDay" ErrorMessage="10歳未満です。" OnServerValidate="vdcBirthday_ServerValidate"
												ValidationGroup="Detail" Display="Dynamic">*</asp:CustomValidator>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											ログインID
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtLoginId" runat="server" MaxLength="8" Width="100px"></asp:TextBox>
											<asp:RequiredFieldValidator ID="vdrLoginId" runat="server" ErrorMessage="ログインIDを入力して下さい。" ControlToValidate="txtLoginId" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
											<asp:CustomValidator ID="vdcLoginId" runat="server" ControlToValidate="txtLoginId" ErrorMessage="既に他のユーザーが利用しています。" OnServerValidate="vdcLoginId_ServerValidate"
												ValidationGroup="Detail">*</asp:CustomValidator>
											<asp:Label ID="lblLoginId" runat="server" Visible="false"></asp:Label>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											ログインパスワード
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtLoginPassword" runat="server" MaxLength="8" Width="100px"></asp:TextBox>
											<asp:RequiredFieldValidator ID="vdrLoginPassword" runat="server" ErrorMessage="ログインパスワードを入力して下さい。" ControlToValidate="txtLoginPassword" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
											<asp:RegularExpressionValidator ID="vdeLoginPassword" runat="server" ErrorMessage="ログインパスワードは4〜8桁の英数字で入力して下さい。" ValidationExpression="\w{4,8}" ControlToValidate="txtLoginPassword"
												ValidationGroup="Detail">*</asp:RegularExpressionValidator>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											メールアドレス
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtEmailAddr" runat="server" MaxLength="60" Width="200px"></asp:TextBox>
											<asp:RequiredFieldValidator ID="vdrEmailAddr" runat="server" ErrorMessage="メールアドレスを入力して下さい。" ControlToValidate="txtEmailAddr" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
											<asp:CustomValidator ID="vdcEmailAddr" runat="server" ControlToValidate="txtEmailAddr" ErrorMessage="既に利用されているﾒｰﾙｱﾄﾞﾚｽです。" OnServerValidate="vdcEmailAddr_ServerValidate"
												ValidationGroup="Detail" Display="Dynamic">*</asp:CustomValidator>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											メールアドレス状態
										</td>
										<td class="tdDataStyle">
											<asp:RadioButtonList ID="rdoNonExistMailAddrFlag" runat="server" RepeatDirection="horizontal">
												<asp:ListItem Text="正常" Value="0"></asp:ListItem>
												<asp:ListItem Text="ﾌｨﾙﾀﾘﾝｸﾞｴﾗｰ" Value="1"></asp:ListItem>
												<asp:ListItem Text="不通ｴﾗｰ" Value="2"></asp:ListItem>
											</asp:RadioButtonList>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											MULLER3送信
										</td>
										<td class="tdDataStyle">
											<asp:RadioButtonList ID="rdoTxMuller3NgMailAddrFlag" runat="server" RepeatDirection="horizontal">
												<asp:ListItem Text="正常" Value="0"></asp:ListItem>
												<asp:ListItem Text="ﾌｨﾙﾀﾘﾝｸﾞｴﾗｰ" Value="1"></asp:ListItem>
											</asp:RadioButtonList>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											担当
										</td>
										<td class="tdDataStyle">
											<asp:DropDownList ID="lstManagerSeq" runat="server" Width="206px" DataSourceID="dsManager" DataTextField="MANAGER_NM" DataValueField="MANAGER_SEQ">
											</asp:DropDownList>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											利用端末種別
										</td>
										<td class="tdDataStyle">
											<asp:DropDownList ID="lstUseTerminalType" runat="server" Width="206px" DataSourceID="dsUseTerminalType" DataTextField="CODE_NM" DataValueField="CODE">
											</asp:DropDownList>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											<%= DisplayWordUtil.Replace("出演者電話番号") %>
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtTel" runat="server" MaxLength="11" Width="100px"></asp:TextBox>
											<asp:RequiredFieldValidator ID="vdrTel" runat="server" ErrorMessage="出演者電話番号を入力して下さい。" ControlToValidate="txtTel" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
											<asp:RegularExpressionValidator ID="vdeTel" runat="server" ErrorMessage="電話番号を正しく入力して下さい。" ValidationExpression="(0\d{9,10})" ControlToValidate="txtTel"
												ValidationGroup="Detail">*</asp:RegularExpressionValidator>
											<asp:CustomValidator ID="vdcTel" runat="server" ControlToValidate="txtTel" ErrorMessage="既に利用されている電話番号です。" OnServerValidate="vdcTel_ServerValidate" ValidationGroup="Detail"
												Display="Dynamic">*</asp:CustomValidator>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											固体識別番号<br />
											(iMode ID等)
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtIModeId" runat="server" MaxLength="32" Width="200px"></asp:TextBox>
											<asp:CustomValidator ID="vdcIModeId" runat="server" ControlToValidate="txtIModeId" ErrorMessage="既に利用されている固体識別番号です。" OnServerValidate="vdcIModeId_ServerValidate"
												ValidationGroup="Detail" Display="Dynamic">*</asp:CustomValidator>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											固体識別
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtTerminalUniqueId" runat="server" MaxLength="64" Width="200px"></asp:TextBox>
											<asp:CustomValidator ID="vdcTerminalUniqueId" runat="server" ControlToValidate="txtTerminalUniqueId" ErrorMessage="既に利用されている固体識別です。" OnServerValidate="vdcTerminalUniqueId_ServerValidate"
												ValidationGroup="Detail" Display="Dynamic">*</asp:CustomValidator>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											URI
										</td>
										<td class="tdDataStyle">
											<asp:Label ID="lblUri" runat="server" Text="" Width="200px"></asp:Label>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											端末パスワード
										</td>
										<td class="tdDataStyle">
											<asp:Label ID="lblTerminalPassword" runat="server" Text="" Width="100px"></asp:Label>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											<%= DisplayWordUtil.Replace("出演者ステータス") %>
										</td>
										<td class="tdDataStyle">
											<asp:DropDownList ID="lstUserStatus" runat="server" Width="206px" DataSourceID="dsUserStatus" DataTextField="CODE_NM" DataValueField="CODE">
											</asp:DropDownList>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											ユーザーランク
										</td>
										<td class="tdDataStyle">
											<asp:DropDownList ID="lstUserRank" runat="server" Width="206px" DataSourceID="dsCastRank" DataTextField="USER_RANK_NM" DataValueField="USER_RANK">
											</asp:DropDownList>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											モニター会話対象者
										</td>
										<td class="tdDataStyle">
											<asp:CheckBox ID="chkMonitorEnableFlag" runat="server" />
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											フリーダイアル利用
										</td>
										<td class="tdDataStyle">
											<asp:CheckBox ID="chkUseFreeDialFlag" runat="server" />フリーダイアルを利用する。
										</td>
									</tr>
                                    <tr>
                                        <td class="tdHeaderStyle">
                                            CROSMILE利用
                                        </td>
                                        <td class="tdDataStyle">
                                            <asp:CheckBox ID="chkUseCrosmileFlag" runat="server" />CROSMILEを利用する。
                                        </td>
                                    </tr>
									<tr>
										<td class="tdHeaderStyle">
											CROSMILE SIPURI
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtCrosmileUri" runat="server" MaxLength="64" Width="200px"></asp:TextBox>
										</td>
									</tr>
                                    <tr>
                                        <td class="tdHeaderStyle">
                                            音声通話ｱﾌﾟﾘ利用
                                        </td>
                                        <td class="tdDataStyle">
											<asp:CheckBox ID="chkUseVoiceappFlag" runat="server" />音声通話アプリを利用する。
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											オンラインライブ利用可能
										</td>
										<td class="tdDataStyle">
											<asp:CheckBox ID="chkOnlineLiveEnableFlag" runat="server" />
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											ツーショット利用不可
										</td>
										<td class="tdDataStyle">
											<asp:CheckBox ID="chkWShotNaFlag" runat="server" />
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											メール利用不可
										</td>
										<td class="tdDataStyle">
											<asp:CheckBox ID="chkMailNaFlag" runat="server" />
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											紹介元コード
										</td>
										<td class="tdDataStyle">
											<asp:Label ID="lblIntroducerFriendCd" runat="server"></asp:Label>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											精算要求
										</td>
										<td class="tdDataStyle">
											<asp:CheckBox ID="chkWaitPaymentFlag" runat="server" />
										</td>
									</tr>
									<asp:PlaceHolder ID="plcStaffIdSupport" runat="server" Visible="true">
										<tr>
											<td class="tdHeaderStyle">
												事業者コード
											</td>
											<td class="tdDataStyle">
												<asp:TextBox ID="txtStaffId" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
												<asp:RegularExpressionValidator ID="vdeStaffId" runat="server" ErrorMessage="事業者コードは10桁以下の英数字で入力して下さい。" ValidationExpression="\w{1,10}" ControlToValidate="txtStaffId"
													ValidationGroup="Detail" Display="Dynamic">*</asp:RegularExpressionValidator>
											</td>
										</tr>
									</asp:PlaceHolder>
									<tr>
										<td class="tdHeaderStyle">
											郵便番号
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtZipCode" runat="server" MaxLength="8" Width="100px"></asp:TextBox>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											都道府県
										</td>
										<td class="tdDataStyle">
											<asp:DropDownList ID="lstPrefectureCd" runat="server" Width="206px" DataSourceID="dsPrefectureCd" DataTextField="CODE_NM" DataValueField="CODE">
											</asp:DropDownList>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											住所1
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtAddress1" runat="server" MaxLength="80" Width="350px"></asp:TextBox>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											住所2
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtAddress2" runat="server" MaxLength="80" Width="350px"></asp:TextBox>
										</td>
									</tr>
									<asp:PlaceHolder ID="plcBank" runat="server">
										<tr>
											<td class="tdHeaderStyle">
												銀行名
											</td>
											<td class="tdDataStyle">
												<asp:TextBox ID="txtBankNm" runat="server" MaxLength="20" Width="150px"></asp:TextBox>
											</td>
										</tr>
										<asp:PlaceHolder ID="plcBankCd" runat="server">
											<tr>
												<td class="tdHeaderStyle">
													金融機関コード
												</td>
												<td class="tdDataStyle">
													<asp:TextBox ID="txtBankCd" runat="server" MaxLength="4" Width="40px"></asp:TextBox>
												</td>
											</tr>
										</asp:PlaceHolder>
										<tr>
											<td class="tdHeaderStyle">
												支店名
											</td>
											<td class="tdDataStyle">
												<asp:TextBox ID="txtBankOfficeNm" runat="server" MaxLength="20" Width="150px"></asp:TextBox>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderStyle">
												支店名カナ
											</td>
											<td class="tdDataStyle">
												<asp:TextBox ID="txtBankOfficeKanaNm" runat="server" MaxLength="32" Width="150px"></asp:TextBox>
											</td>
										</tr>
										<asp:PlaceHolder ID="plcBankOfficeCd" runat="server">
											<tr>
												<td class="tdHeaderStyle">
													支店コード
												</td>
												<td class="tdDataStyle">
													<asp:TextBox ID="txtBankOfficeCd" runat="server" MaxLength="3" Width="40px"></asp:TextBox>
												</td>
											</tr>
										</asp:PlaceHolder>
										<asp:PlaceHolder ID="plcBankAccountType" runat="server">
											<tr>
												<td class="tdHeaderStyle">
													口座種類
												</td>
												<td class="tdDataStyle">
													<asp:TextBox ID="txtBankAccountType" runat="server" MaxLength="12" Width="80px"></asp:TextBox>
												</td>
											</tr>
										</asp:PlaceHolder>
										<asp:PlaceHolder ID="plcBankAccountType2" runat="server">
											<tr>
												<td class="tdHeaderStyle">
													口座種類
												</td>
												<td class="tdDataStyle">
													<asp:RadioButton ID="rdoNormalAccount" runat="server" Text="普通" GroupName="BankAccountTypeGrouping" />
													<asp:RadioButton ID="rdoCheckingAccount" runat="server" Text="当座" GroupName="BankAccountTypeGrouping" />
													<asp:RadioButton ID="rdoSavingsAccounts" runat="server" Text="貯蓄" GroupName="BankAccountTypeGrouping" />
												</td>
											</tr>
										</asp:PlaceHolder>
										<tr>
											<td class="tdHeaderStyle">
												口座番号
											</td>
											<td class="tdDataStyle">
												<asp:TextBox ID="txtBankAccountNo" runat="server" MaxLength="7" Width="120px"></asp:TextBox>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderStyle">
												口座名義
											</td>
											<td class="tdDataStyle">
												<asp:TextBox ID="txtBankAccountHolderNm" runat="server" MaxLength="40" Width="150px"></asp:TextBox>
											</td>
										</tr>
										<asp:PlaceHolder ID="plcInvalidBankAccount" runat="server">
											<tr>
												<td class="tdHeaderStyle">
													口座情報無効
												</td>
												<td class="tdDataStyle">
													<asp:CheckBox ID="chkBankAccountInvalidFlag" runat="server" />
												</td>
											</tr>
										</asp:PlaceHolder>
									</asp:PlaceHolder>
									<asp:PlaceHolder ID="plcPaymentTimingType" runat="server">
										<tr>
											<td class="tdHeaderStyle">
												精算ﾀｲﾐﾝｸﾞ種別
											</td>
											<td class="tdDataStyle">
												<asp:RadioButton ID="rdoAutoMonthAndRequest" runat="server" Text="月末自動・申請精算共に行う" GroupName="PaymentTimingTypeGrouping" />
												<asp:RadioButton ID="rdoAutoMonthOnly" runat="server" Text="月末自動精算のみ行う" GroupName="PaymentTimingTypeGrouping" />
												<asp:RadioButton ID="rdoRequestOnly" runat="server" Text="申請精算のみ行う" GroupName="PaymentTimingTypeGrouping" />
											</td>
										</tr>
									</asp:PlaceHolder>
									<tr>
										<td class="tdHeaderStyle">
											要注意者
										</td>
										<td class="tdDataStyle">
											<asp:CheckBox ID="chkCautionFlag" runat="server" />
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											ﾃｽﾄﾕｰｻﾞｰﾌﾗｸﾞ
										</td>
										<td class="tdDataStyle">
											<asp:CheckBox ID="chkAdminFlag" runat="server" />サイトのテスト用ユーザーにする。
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											<%= DisplayWordUtil.Replace("保護者氏名") %>
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtGuardianNm" runat="server" MaxLength="50" Width="200px"></asp:TextBox>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											<%= DisplayWordUtil.Replace("保護者の電話番号") %>
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtGuardianTel" runat="server" MaxLength="11" Width="100px"></asp:TextBox>
											<asp:RegularExpressionValidator ID="vdeGuardianTel" runat="server" ErrorMessage="保護者の電話番号を正しく入力して下さい。" ValidationExpression="(0\d{9,10})" ControlToValidate="txtGuardianTel"
												ValidationGroup="Detail">*</asp:RegularExpressionValidator>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle2">
											備 考
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtRemarks1" runat="server" MaxLength="1000" Width="360px " Height="48px" TextMode="MultiLine"></asp:TextBox><br />
											<asp:TextBox ID="txtRemarks2" runat="server" MaxLength="256" Width="360px"></asp:TextBox><br />
											<asp:TextBox ID="txtRemarks3" runat="server" MaxLength="256" Width="360px"></asp:TextBox><br />
											<asp:TextBox ID="txtRemarks4" runat="server" MaxLength="1000" Width="360px" Height="140px" TextMode="MultiLine"></asp:TextBox>
										</td>
									</tr>
								</table>
								<table border="0" style="width: 540px">
									<tr>
										<td>
											<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
											<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
										</td>
										<td>
											<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekrightbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
										</td>
									</tr>
								</table>
							</td>
							<td valign="top">
								<asp:Panel runat="server" ID="pnlPic" ScrollBars="auto" Height="753px">
									<table border="0" style="width: 520px" class="tableStyle">
										<asp:Repeater ID="rptUpload" runat="server">
											<ItemTemplate>
												<tr>
													<td class="tdHeaderSmallStyle2">
														<%# string.Concat("身分証画像<br />", Container.ItemIndex + 1) %>
													</td>
													<td class="tdDataStyle">
														<asp:FileUpload ID="uldIdPic" runat="server" Width="400px" />
														<asp:Button ID="btnUpload" runat="server" CommandArgument='<%# Container.ItemIndex %>' CommandName="UPLOAD" CssClass="seekbutton" OnCommand="btnUpload_Command"
															Text='<%# string.Concat("ｱﾌﾟﾛｰﾄﾞ", Container.ItemIndex + 1) %>' ValidationGroup='<%# string.Concat("Upload", Container.ItemIndex + 1) %>' Enabled='<%# IsOverRightLocalStaff() %>' /><br />
														<asp:Button ID="btnRotate" runat="server" CommandArgument='<%# string.Format("{0},{1}", Container.ItemIndex, Container.DataItem) %>' CommandName="ROTATE"
															CssClass="seekbutton" OnCommand="btnRotate_Command" Text="回転" Enabled='<%# IsOverRightLocalStaff() %>' />
														<asp:RequiredFieldValidator ID="rfvUplod" runat="server" ControlToValidate="uldIdPic" ErrorMessage='<%# string.Format("身分証ファイル{0}が入力されていません。", Container.ItemIndex + 1) %>'
															SetFocusOnError="True" ValidationGroup='<%# string.Concat("Upload", Container.ItemIndex+1) %>'></asp:RequiredFieldValidator>
														<asp:Button ID="btnDelete" runat="server" CommandArgument='<%# Container.ItemIndex %>' CommandName="DELETE" CssClass="seekbutton" OnCommand="btnDelete_Command"
															Text='<%# string.Concat("削除", Container.ItemIndex + 1) %>' Enabled='<%# IsOverRightLocalStaff() %>' /><br />
														<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
													</td>
												</tr>
												<tr>
													<td class="tdDataStyle" colspan="3">
														<asp:Panel ID="Panel1" runat="server" Height="300px" ScrollBars="Auto" Width="500px">
															<asp:Image ID="imgIdPic" runat="server" ImageUrl='<%# GetImgUrl(Container.DataItem) %>' Visible='<%# GetImgVisible(Container.DataItem) %>' Width="500px" /></asp:Panel>
														<asp:Label ID="lblNoImage" runat="server" ForeColor="Red" Text="未登録" Visible="<%# !GetImgVisible(Container.DataItem) %>"></asp:Label>
													</td>
												</tr>
											</ItemTemplate>
										</asp:Repeater>
									</table>
								</asp:Panel>
							</td>
						</tr>
					</table>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsManager" runat="server" SelectMethod="GetList" TypeName="Manager"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUserStatus" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="51" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUseTerminalType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="55" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsPrefectureCd" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="67" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastRank" runat="server" SelectMethod="GetList" TypeName="CastRank"></asp:ObjectDataSource>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtBankAccountNo" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrCastNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrLoginId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdcLoginId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdrLoginPassword" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdeLoginPassword" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vdrEmailAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8" TargetControlID="vdrTel" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9" TargetControlID="vdeTel" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender12" TargetControlID="vdeStaffId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdcBirthDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10" TargetControlID="vddBirthDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11" TargetControlID="vdeBirthDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender13" TargetControlID="vdcEmailAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender14" TargetControlID="vdcTel" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender15" TargetControlID="vdcIModeId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender16" TargetControlID="vdcTerminalUniqueId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender17" TargetControlID="vdeGuardianTel" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:MaskedEditExtender ID="mskBirthDay" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtBirthDay">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskZipCode" runat="server" MaskType="None" Mask="999-9999" UserDateFormat="None" ClearMaskOnLostFocus="false" TargetControlID="txtZipCode">
	</ajaxToolkit:MaskedEditExtender>
</asp:Content>
