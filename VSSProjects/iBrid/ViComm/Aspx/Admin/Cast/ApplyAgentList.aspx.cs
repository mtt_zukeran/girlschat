﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: エージェントID認証
--	Progaram ID		: ApplyAgentList
--
--  Creation Date	: 2012.01.19
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Cast_ApplyAgentList : System.Web.UI.Page {
	private string recCount = "";

	private bool Certifiable {
		get { return Session["AdminType"].ToString().CompareTo(ViCommConst.RIGHT_LOCAL_STAFF) > 0; }
	}

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			this.InitPage();
			this.GetList();
		}
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			this.GetList();
		}
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		this.Response.Redirect(string.Format("../Cast/ApplyAgentMainte.aspx?sitecd={0}",lstSiteCd.SelectedValue));
	}

	protected void dsAgentRegist_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsAgentRegist_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	private void InitPage() {
		this.pnlKey.Enabled = true;
		this.grdAgentRegist.DataSourceID = string.Empty;
		this.grdAgentRegist.PageSize = 100;
		this.ClearField();
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["SiteCd"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["SiteCd"]);
		} else if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		this.lstProductionSeq.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["PRODUCTION_SEQ"]))) {
			this.lstProductionSeq.SelectedValue = iBridUtil.GetStringValue(Session["PRODUCTION_SEQ"]);
		}

		this.lstSiteCd.Enabled = this.Certifiable;
		this.lstProductionSeq.Enabled = this.Certifiable;
	}

	private void ClearField() {
		if (this.Certifiable) {
			this.lstSiteCd.SelectedIndex = 0;
		}

		this.recCount = "0";
	}

	private void GetList() {
		this.grdAgentRegist.DataSourceID = "dsAgentRegist";
		this.grdAgentRegist.PageIndex = 0;
		this.grdAgentRegist.DataBind();
		this.pnlCount.DataBind();
	}

	protected string GetAgeByBirthDay(object pBirthday) {
		if (pBirthday.ToString().Length == 10) {
			return ViCommPrograms.Age(iBridUtil.GetStringValue(pBirthday)).ToString();
		} else {
			return string.Empty;
		}
	}
}
