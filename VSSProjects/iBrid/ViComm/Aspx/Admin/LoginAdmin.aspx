﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LoginAdmin.aspx.cs" Inherits="LoginAdmin" Title="管理画面" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Card Sericve Amin Login</title>
</head>
<body onLoad="document.getElementById('ctlLogin_UserName').focus()">
	<form id="form1" runat="server" target="_parent">
		<div id="container">
			<div id="header">
				<asp:Label ID="Label1" runat="server" Text="MAQIA Site Management" CssClass="sysname"></asp:Label>
			</div>
			<br />
			<table align="center" cellpadding="0" cellspacing="0" style="height: 214px">
				<tr>
					<td style="width: 209px; height: 102px;">
						<asp:Login ID="ctlLogin" runat="server" BackColor="#F7F6F3" BorderColor="#E6E2D8" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" OnAuthenticate="ctlLogin_Authenticate" BorderPadding="4" DisplayRememberMe="False"
							ForeColor="#333333" Height="106px" InstructionText=" " TitleText="MAQIA Site User Authenticate" Width="398px" LoginButtonText="Login" PasswordLabelText="Password:" UserNameLabelText="User ID:">
							<TitleTextStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" Font-Size="0.9em" />
							<InstructionTextStyle BorderStyle="None" Font-Italic="True" ForeColor="Black" />
							<TextBoxStyle Font-Size="0.8em" Width="100px" />
							<LoginButtonStyle BackColor="#FFFBFF" BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" ForeColor="#284775" />
							<LabelStyle BorderWidth="0px" />
						</asp:Login>
					</td>
				</tr>
			</table>
		</div>
	</form>
</body>
</html>
