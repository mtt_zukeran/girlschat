﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ユーザー振分け明細
--	Progaram ID		: DispatchDtlInquiry
--
--  Creation Date	: 2010.03.19
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.UI.WebControls;
using ViComm;

public partial class Affiliate_DispatchDtlInquiry:System.Web.UI.Page {
	private string OKCount = "0";
	private string NGCount = "0";
	private string RecCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			if (!Session["AvailableMaqiaAffiliate"].ToString().Equals("1")) {
				string sRoot = ConfigurationManager.AppSettings["Root"].ToString().ToLower();
				Response.Redirect(sRoot + "/error/WebError.aspx?errtype=" + ViComm.ViCommConst.USER_INFO_INTERNAL_ERROR);
			}
			FisrtLoad();
			InitPage();
		}
	}

	private void FisrtLoad() {
		grdList.PageSize = 50;
		grdList.DataSourceID = "";
		DataBind();

		lstPartnerSiteCd.Items.Insert(0,new ListItem("",""));
		lstPartnerSiteCd.DataSourceID = "";
		lstRegistResult.Items.Insert(0,new ListItem("",""));
		lstRegistResult.DataSourceID = "";
	}

	private void InitPage() {
		pnlInfo.Visible = false;
		ClearField();
		grdList.DataSourceID = "";
		DataBind();
		if (!IsPostBack) {
			txtRegistDayFrom.Text = DateTime.Today.ToString("yyyy/MM/dd");
			txtRegistDayTo.Text = txtRegistDayFrom.Text;
		}
	}

	protected void ClearField() {
		lstPartnerSiteCd.SelectedIndex = 0;
		//txtRegistDayFrom.Text = "";
		//txtRegistDayTo.Text = "";
		OKCount = "0";
		NGCount = "0";
		RecCount = "0";
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (txtRegistDayTo.Text.Equals("")) {
			txtRegistDayTo.Text = txtRegistDayFrom.Text;
		}

		GetList();
	}
	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	private void GetList() {
		OKCount = "0";
		NGCount = "0";
		using (DispatchDtl oDispatchDtl = new DispatchDtl())
		using (DataSet ds = oDispatchDtl.GetCount(
					lstPartnerSiteCd.SelectedValue,
					txtRegistDayFrom.Text,
					txtRegistDayTo.Text,
					lstRegistResult.SelectedValue)) {

			foreach (DataRow dr in ds.Tables[0].Rows) {
				switch (int.Parse(dr["REGIST_RESULT"].ToString())) {
					case ViCommConst.RESULT_USER_REGIST:
						OKCount = dr["CNT"].ToString();
						break;
					case ViCommConst.RESULT_USER_EXIST:
						NGCount = dr["CNT"].ToString();
						break;
				}
			}
		}

		grdList.PageIndex = 0;
		grdList.DataSourceID = "dsRegistDtlCnt";
		DataBind();
		pnlInfo.Visible = true;
	}

	protected void lnkRegistResult_Command(object sender,CommandEventArgs e) {
		string sURL = "";
		using (Sys oSys = new Sys()) {
			oSys.GetValue("MAQIA_AFFILIATE_IF_URL",out sURL);
		}
		sURL = string.Format(
						"{0}?userseq={1}&hisseq={2}&afid={3}",
						sURL,
						e.CommandArgument.ToString().Split(':')[0],
						e.CommandArgument.ToString().Split(':')[1],
						e.CommandArgument.ToString().Split(':')[2]);

		ViCommInterface.TransToParent(sURL,false);
		grdList.DataBind();
	}

	protected void dsRegistDtlCnt_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstPartnerSiteCd.SelectedValue;
		e.InputParameters[1] = txtRegistDayFrom.Text;
		e.InputParameters[2] = txtRegistDayTo.Text;
		e.InputParameters[3] = lstRegistResult.SelectedValue;
	}
	protected void dsRegistDtlCnt_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			RecCount = e.ReturnValue.ToString();
		}
	}
	protected void dsPartnerSite_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = "";
	}
	protected string GetOKCount() {
		return OKCount;
	}

	protected string GetNGCount() {
		return NGCount;
	}

	protected string GetRecCount() {
		return RecCount;
	}

	protected void grdList_DataBound(object sender,EventArgs e) {
		JoinCells();
		SetResult();
	}

	private void JoinCells() {
		int iRow = grdList.Rows.Count;
		int iBaseidx = 0;

		while (iBaseidx < iRow) {
			int iNextIdx = iBaseidx + 1;

			while (iNextIdx < iRow) {

				if (GetLnkText(grdList.Rows[iBaseidx].Cells[0]).Equals(GetLnkText(grdList.Rows[iNextIdx].Cells[0])) &&
					GetText(grdList.Rows[iBaseidx].Cells[1]).Equals(GetText(grdList.Rows[iNextIdx].Cells[1])) &&
					GetText(grdList.Rows[iBaseidx].Cells[2]).Equals(GetText(grdList.Rows[iNextIdx].Cells[2])) &&
					GetText(grdList.Rows[iBaseidx].Cells[3]).Equals(GetText(grdList.Rows[iNextIdx].Cells[3]))) {
					if (grdList.Rows[iBaseidx].Cells[0].RowSpan == 0) {
						for (int iCol = 0;iCol <= 3;iCol++) {
							grdList.Rows[iBaseidx].Cells[iCol].RowSpan = 2;
						}
					} else {
						for (int iCol = 0;iCol <= 3;iCol++) {
							grdList.Rows[iBaseidx].Cells[iCol].RowSpan++;
						}
					}
					for (int iCol = 3;iCol >= 0;iCol--) {
//						grdList.Rows[iNextIdx].Cells.Remove(grdList.Rows[iNextIdx].Cells[iCol]);
						grdList.Rows[iNextIdx].Cells[iCol].Visible = false;
					}

					iNextIdx++;
				} else {
					break;
				}
			}
			iBaseidx = iNextIdx;
		}
	}

	private string GetText(TableCell tc) {
		Label dblc =
		  (Label)tc.Controls[1];
		return dblc.Text;
	}

	private string GetLnkText(TableCell tc) {
		HyperLink dblc =
		  (HyperLink)tc.Controls[1];
		return dblc.Text;
	}

	private void SetResult() {
		foreach (GridViewRow gr in grdList.Rows) {
			Label lblCd = (Label)gr.FindControl("lblRegistResult") as Label;
			Label lblNm = (Label)gr.FindControl("lblRegistResultNm") as Label;
			LinkButton lnk = (LinkButton)gr.FindControl("lnkRegistResultNm") as LinkButton;

			switch (int.Parse(lblCd.Text)) {
				case ViCommConst.RESULT_USER_REGISTING:
					lnk.Attributes["onclick"] = "return confirm('再送信しますか？');";
					lblNm.Visible = false;
					break;
				default:
					lnk.Visible = false;
					break;
			}
		}
	}
}
