﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PartnerSiteList.aspx.cs" Inherits="Affiliate_PartnerSiteList" Title="パートナーサイト設定" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="パートナーサイト設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[パートナーサイト情報]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					<asp:Label ID="lblPartnerSiteSeq" runat="server" Text="" Visible="false"></asp:Label>
					<table border="0" style="width: 800px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle">
								パートナーサイト名称
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtPartnerSiteNm" runat="server" MaxLength="60" Width="360px"></asp:TextBox>
								<asp:RequiredFieldValidator ID="vdrPartnerSiteNm" runat="server" ErrorMessage="パートナーサイト名称を入力して下さい。" ControlToValidate="txtPartnerSiteNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								<asp:CustomValidator ID="vdcPartnerSiteNm" runat="server" ControlToValidate="txtPartnerSiteNm" ErrorMessage="文字数を減らしてください。" OnServerValidate="vdcPartnerSiteNm_ServerValidate"
									ValidationGroup="Detail">*</asp:CustomValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								パートナーサイト略称
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtPartnerSiteIni" runat="server" MaxLength="30" Width="120px"></asp:TextBox>
								<asp:RequiredFieldValidator ID="vdrPartnerSiteIni" runat="server" ErrorMessage="パートナーサイト略称を入力して下さい。" ControlToValidate="txtPartnerSiteIni" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								<asp:CustomValidator ID="vdcPartnerSiteIni" runat="server" ControlToValidate="txtPartnerSiteIni" ErrorMessage="文字数を減らしてください。" OnServerValidate="vdcPartnerSiteIni_ServerValidate"
									ValidationGroup="Detail">*</asp:CustomValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								会員登録受付URL
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtRegistUserAcceptUrl" runat="server" MaxLength="100" Width="600px" Rows="3" TextMode="MultiLine"></asp:TextBox>
								<asp:RequiredFieldValidator ID="vdrRegistUserAcceptUrl" runat="server" ErrorMessage="会員登録受付URLを入力して下さい。" ControlToValidate="txtRegistUserAcceptUrl" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								<asp:CustomValidator ID="vdcRegistUserAcceptUrl" runat="server" ControlToValidate="txtRegistUserAcceptUrl" ErrorMessage="URL中に改行が含まれています" OnServerValidate="vdcRegistUserAcceptUrl_ServerValidate"
									ValidationGroup="Detail">*</asp:CustomValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle2">
								停止フラグ
							</td>
							<td class="tdDataStyle">
								<asp:CheckBox ID="chkStopFlag" runat="server">
								</asp:CheckBox>
							</td>
						</tr>
					</table>
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
					<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[パートナーサイト一覧]</legend>
			<asp:GridView ID="grdPartnerSite" runat="server" AllowPaging="True" DataSourceID="dsPartnerSite" AllowSorting="True" SkinID="GridViewColor" AutoGenerateColumns="False"
				Width="650px">
				<Columns>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkPartnerSiteNm" runat="server" Text='<%# Eval("PARTNER_SITE_NM") %>' CommandArgument='<%# Eval("PARTNER_SITE_SEQ") %>' OnCommand="lnkPartnerSiteNm_Command"
								CausesValidation="False"></asp:LinkButton>
						</ItemTemplate>
						<HeaderTemplate>
							パートナーサイト名
						</HeaderTemplate>
					</asp:TemplateField>
					<asp:BoundField DataField="PARTNER_SITE_INITIAL" HeaderText="略称">
						<ItemStyle HorizontalAlign="Left" />
					</asp:BoundField>
					<asp:BoundField DataField="STOP_FLAG" HeaderText="停止">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
					<asp:BoundField DataField="UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<a class="reccount">Record Count
				<%#GetRecCount() %>
			</a>
			<br>
			<a class="reccount">Current viewing page
				<%=grdPartnerSite.PageIndex + 1%>
				of
				<%=grdPartnerSite.PageCount%>
			</a>
			<div class="button">
				<asp:Button ID="btnRegist" runat="server" Text="パートナーサイト追加" OnClick="btnRegist_Click" />
			</div>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsPartnerSite" runat="server" SelectMethod="GetPageCollection" SelectCountMethod="GetPageCount" OnSelected="dsPartnerSite_Selected"
		EnablePaging="True" TypeName="PartnerSite"></asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrPartnerSiteNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender14" TargetControlID="vdcPartnerSiteNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender13" TargetControlID="vdrPartnerSiteIni" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender15" TargetControlID="vdcPartnerSiteIni" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrRegistUserAcceptUrl" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdcRegistUserAcceptUrl" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Custom" TargetControlID="txtRegistUserAcceptUrl"
		 FilterMode="InvalidChars" InvalidChars="&#13;&#10;" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	</ajaxToolkit:MaskedEditExtender>
</asp:Content>
