﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 日別ユーザー振分け実績

--	Progaram ID		: DailyDispatchReportInquiry
--
--  Creation Date	: 2010.03.19
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Affiliate_DailyDispatchReportInquiry:System.Web.UI.Page {

	private const int TEMPLATE_FILED_WIDTH_DATE = 38;
	private const int TEMPLATE_FILED_WIDTH_OTHER = 60;

	private DataSet ds;
	private DataTable dt;

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			if(!Session["AvailableMaqiaAffiliate"].ToString().Equals("1")){
				string sRoot = ConfigurationManager.AppSettings["Root"].ToString().ToLower();
				Response.Redirect(sRoot + "/error/WebError.aspx?errtype=" + ViComm.ViCommConst.USER_INFO_INTERNAL_ERROR);
			}
			
			InitPage();
		}
	}

	private void InitPage() {
		pnlInfo.Visible = false;
		ClearField();

		if (!IsPostBack) {

            SysPrograms.SetupYear(lstYYYY);

			lstMM.SelectedValue = DateTime.Now.ToString("MM");
			GetList();
		}
	}

	private void ClearField() {
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void grdReport_RowDataBound(object sender,GridViewRowEventArgs e) {
	}

	private void GetList() {
		using (DailyDispatchReport oDailyReport = new DailyDispatchReport()) {
			ds = oDailyReport.DailyDispatchReportInquiry(lstYYYY.SelectedValue, lstMM.SelectedValue);
			dt = ds.Tables[0];
		}
		SetTable();
		pnlInfo.Visible = true;
	}

	protected void SetTable() {

		//ヘッダ１段目
		TableHeaderRow thr1 = new TableHeaderRow();
		thr1.CssClass = "HeaderStyle";

		foreach (DataColumn dc in dt.Columns) {

			TableHeaderCell thc = new TableHeaderCell();
			thc.CssClass = "HeaderStyle";
			thc.Font.Size = FontUnit.XSmall;

			switch (dc.ColumnName) {
				case "REPORT_DAY":
					thc.ColumnSpan = 2;
					thc.Text = "";
					thr1.Cells.Add(thc);
					break;

				case "TOTAL_REGIST":
					thc.ColumnSpan = 2;
					thc.Text = "合計";
					thr1.Cells.Add(thc);
					break;

				default:
					if (dc.ColumnName.IndexOf("REGIST") > 0) {
						thc.ColumnSpan = 2;
						thc.Text = dc.ColumnName.ToString().Split(':')[1];
						thr1.Cells.Add(thc);
					}
					break;
			}
		}
		tblReport.Rows.Add(thr1);

		//ヘッダ２段目
		TableHeaderRow thr2 = new TableHeaderRow();
		thr2.CssClass = "HeaderStyle";
		thr2.Height = Unit.Pixel(30);

		foreach (DataColumn dc in dt.Columns) {

			TableHeaderCell thc = new TableHeaderCell();
			thc.CssClass = "HeaderStyle";
			thc.Font.Size = FontUnit.XSmall;

			switch (dc.ColumnName) {
				case "REPORT_DAY":
					TableHeaderCell thcDayOfWeek = new TableHeaderCell();
					thcDayOfWeek.CssClass = "HeaderStyle";
					thcDayOfWeek.Font.Size = FontUnit.XSmall;

					thc.Text = "日付";
					thcDayOfWeek.Text = "曜日";

					thr2.Cells.Add(thc);
					thr2.Cells.Add(thcDayOfWeek);
					break;

				default:
					if (dc.ColumnName.IndexOf("REGIST") > 0) {
						thc.Text = "登録";
					}
					if (dc.ColumnName.IndexOf("REFUSE") > 0) {
						thc.Text = "拒否";
					}
					thr2.Cells.Add(thc);
					break;
			}
		}
		tblReport.Rows.Add(thr2);

		//データ
		foreach (DataRow dr in dt.Rows) {
			if (dr["REPORT_DAY"].ToString().Equals("合計")) {
				break;
			}

			TableRow tr = new TableRow();

			foreach (DataColumn dc in dt.Columns) {

				TableCell tc = new TableCell();
				Label lbl = new Label();
				tc.Font.Size = FontUnit.XSmall;
				tc.HorizontalAlign = HorizontalAlign.Center;
				tc.VerticalAlign = VerticalAlign.Middle;

				switch (dc.ColumnName) {
					case "REPORT_DAY":
						TableCell tcDayOfWeek = new TableCell();
						Label lblDayOfWeek = new Label();

						tcDayOfWeek.Font.Size = FontUnit.XSmall;
						tcDayOfWeek.HorizontalAlign = HorizontalAlign.Center;
						tcDayOfWeek.VerticalAlign = VerticalAlign.Middle;

						lbl.Text = GetDay(dr[dc.ColumnName]);
						lbl.Width = Unit.Pixel(TEMPLATE_FILED_WIDTH_DATE);

						lblDayOfWeek.Text = GetDayOfWeek(dr[dc.ColumnName]);
						lblDayOfWeek.Width = Unit.Pixel(TEMPLATE_FILED_WIDTH_DATE);

						tc.BackColor = GetBackColor(GetDayOfWeek(dr[dc.ColumnName]));
						tcDayOfWeek.BackColor = GetBackColor(GetDayOfWeek(dr[dc.ColumnName]));

						tc.CssClass = "RowStyleNoPad";
						tcDayOfWeek.CssClass = "RowStyleNoPad";

						tc.Controls.Add(lbl);
						tcDayOfWeek.Controls.Add(lblDayOfWeek);

						tr.Cells.Add(tc);
						tr.Cells.Add(tcDayOfWeek);
						break;
					default:
						lbl.Text = dr[dc.ColumnName].ToString();
						lbl.Width = Unit.Pixel(TEMPLATE_FILED_WIDTH_OTHER);

						tc.HorizontalAlign = HorizontalAlign.Right;

						tc.CssClass = "RowStylePad";
						tc.Controls.Add(lbl);
						tr.Cells.Add(tc);
						break;
				}
			}
			tblReport.Rows.Add(tr);
		}

		//フッター
		TableFooterRow tfr = new TableFooterRow();
		tfr.CssClass = "FooterStyle";
		tfr.ForeColor = Color.Black;
		tfr.BackColor = Color.LightYellow;

		DataRow drFooter = dt.Rows[dt.Rows.Count - 1];

		foreach (DataColumn dc in dt.Columns) {

			TableCell tc = new TableCell();
			tc.Font.Size = FontUnit.XSmall;
			tc.HorizontalAlign = HorizontalAlign.Center;

			switch (dc.ColumnName) {
				case "REPORT_DAY":
					TableCell tcDayOfWeek = new TableCell();
					
					tcDayOfWeek.Font.Size = FontUnit.XSmall;
					tcDayOfWeek.HorizontalAlign = HorizontalAlign.Center;
					tcDayOfWeek.Text = drFooter[dc.ColumnName].ToString();

					tfr.Cells.Add(tc);
					tfr.Cells.Add(tcDayOfWeek);
					break;

				default:
					tc.Text = drFooter[dc.ColumnName].ToString();
					tc.HorizontalAlign = HorizontalAlign.Right;
					tfr.Cells.Add(tc);
					break;
			}
		}
		tblReport.Rows.Add(tfr);
	}

	protected Color GetBackColor(object pDayOfWeek) {
		if (pDayOfWeek.ToString().Equals("Sat")) {
			return Color.FromArgb(0xCCCCFF);
		} else {
			if (pDayOfWeek.ToString().Equals("Sun")) {
				return Color.FromArgb(0xFFCCCC);
			} else {
				return Color.Empty;
			}
		}
	}

	protected string GetDayOfWeek(object pDay) {
		DateTime dt = DateTime.Parse(pDay.ToString());
		return dt.ToString("ddd",new System.Globalization.CultureInfo("en-US"));
	}

	protected string GetDay(object pDay) {
		return string.Format("{0}日",pDay.ToString().Split('/')[2]);
	}
}
