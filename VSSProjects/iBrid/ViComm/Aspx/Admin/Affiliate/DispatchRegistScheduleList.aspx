<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DispatchRegistScheduleList.aspx.cs" Inherits="Affiliate_DispatchRegistScheduleList"
	Title="新規登録振分け予定設定" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="新規登録振分け予定設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<asp:Label ID="lblScheduleSeq" runat="server" Text="" Visible="false"></asp:Label>
			<fieldset class="fieldset-inner">
				<legend>[新規登録振分け予定設定]</legend>
				<table border="0">
					<tr valign="top">
						<td>
							<asp:Calendar ID="objCalender" runat="server" BackColor="White" BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt"
								ShowGridLines="True" ForeColor="Black" Height="228px" Width="248px" OnSelectionChanged="objCalender_SelectionChanged" OnDayRender="objCalender_DayRender">
								<SelectedDayStyle BackColor="#FFE4C4" Font-Bold="True" ForeColor="#0000ff" />
								<SelectorStyle BackColor="#CCCCCC" />
								<WeekendDayStyle BackColor="#FFFFCC" ForeColor="Red" />
								<TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
								<OtherMonthDayStyle ForeColor="Gray" />
								<NextPrevStyle VerticalAlign="Bottom" />
								<DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
								<TitleStyle BackColor="#999999" Font-Bold="True" />
							</asp:Calendar>
						</td>
						<td style="padding-left: 10px;">
							<asp:Panel runat="server" ID="pnlDtl">
								<table border="0" style="width: 400px" class="tableStyle">
									<tr>
										<td class="tdHeaderStyle">
											パートナーサイト
										</td>
										<td class="tdDataStyle">
											<asp:DropDownList ID="lstPartnerSite" runat="server" DataSourceID="dsPartnerSite" DataTextField="PARTNER_SITE_NM" DataValueField="PARTNER_SITE_SEQ" Width="240px"
												AutoPostBack="True" OnSelectedIndexChanged="lstPartnerSite_SelectedIndexChanged">
											</asp:DropDownList>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											登録開始日時
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtRegistUserStartDay" runat="server" MaxLength="10" Width="65px"></asp:TextBox>
											<asp:DropDownList ID="lstRegistUserStartTime" runat="server">
												<asp:ListItem Value="00:00:00" Text="0:00"></asp:ListItem>
												<asp:ListItem Value="00:30:00" Text="0:30"></asp:ListItem>
												<asp:ListItem Value="01:00:00" Text="1:00"></asp:ListItem>
												<asp:ListItem Value="01:30:00" Text="1:30"></asp:ListItem>
												<asp:ListItem Value="02:00:00" Text="2:00"></asp:ListItem>
												<asp:ListItem Value="02:30:00" Text="2:30"></asp:ListItem>
												<asp:ListItem Value="03:00:00" Text="3:00"></asp:ListItem>
												<asp:ListItem Value="03:30:00" Text="3:30"></asp:ListItem>
												<asp:ListItem Value="04:00:00" Text="4:00"></asp:ListItem>
												<asp:ListItem Value="04:30:00" Text="4:30"></asp:ListItem>
												<asp:ListItem Value="05:00:00" Text="5:00"></asp:ListItem>
												<asp:ListItem Value="05:30:00" Text="5:30"></asp:ListItem>
												<asp:ListItem Value="06:00:00" Text="6:00"></asp:ListItem>
												<asp:ListItem Value="06:30:00" Text="6:30"></asp:ListItem>
												<asp:ListItem Value="07:00:00" Text="7:00"></asp:ListItem>
												<asp:ListItem Value="07:30:00" Text="7:30"></asp:ListItem>
												<asp:ListItem Value="08:00:00" Text="8:00"></asp:ListItem>
												<asp:ListItem Value="08:30:00" Text="8:30"></asp:ListItem>
												<asp:ListItem Value="09:00:00" Text="9:00"></asp:ListItem>
												<asp:ListItem Value="09:30:00" Text="9:30"></asp:ListItem>
												<asp:ListItem Value="10:00:00" Text="10:00"></asp:ListItem>
												<asp:ListItem Value="10:30:00" Text="10:30"></asp:ListItem>
												<asp:ListItem Value="11:00:00" Text="11:00"></asp:ListItem>
												<asp:ListItem Value="11:30:00" Text="11:30"></asp:ListItem>
												<asp:ListItem Value="12:00:00" Text="12:00"></asp:ListItem>
												<asp:ListItem Value="12:30:00" Text="12:30"></asp:ListItem>
												<asp:ListItem Value="13:00:00" Text="13:00"></asp:ListItem>
												<asp:ListItem Value="13:30:00" Text="13:30"></asp:ListItem>
												<asp:ListItem Value="14:00:00" Text="14:00"></asp:ListItem>
												<asp:ListItem Value="14:30:00" Text="14:30"></asp:ListItem>
												<asp:ListItem Value="15:00:00" Text="15:00"></asp:ListItem>
												<asp:ListItem Value="15:30:00" Text="15:30"></asp:ListItem>
												<asp:ListItem Value="16:00:00" Text="16:00"></asp:ListItem>
												<asp:ListItem Value="16:30:00" Text="16:30"></asp:ListItem>
												<asp:ListItem Value="17:00:00" Text="17:00"></asp:ListItem>
												<asp:ListItem Value="17:30:00" Text="17:30"></asp:ListItem>
												<asp:ListItem Value="18:00:00" Text="18:00"></asp:ListItem>
												<asp:ListItem Value="18:30:00" Text="18:30"></asp:ListItem>
												<asp:ListItem Value="19:00:00" Text="19:00"></asp:ListItem>
												<asp:ListItem Value="19:30:00" Text="19:30"></asp:ListItem>
												<asp:ListItem Value="20:00:00" Text="20:00"></asp:ListItem>
												<asp:ListItem Value="20:30:00" Text="20:30"></asp:ListItem>
												<asp:ListItem Value="21:00:00" Text="21:00"></asp:ListItem>
												<asp:ListItem Value="21:30:00" Text="21:30"></asp:ListItem>
												<asp:ListItem Value="22:00:00" Text="22:00"></asp:ListItem>
												<asp:ListItem Value="22:30:00" Text="22:30"></asp:ListItem>
												<asp:ListItem Value="23:00:00" Text="23:00"></asp:ListItem>
												<asp:ListItem Value="23:30:00" Text="23:30"></asp:ListItem>
											</asp:DropDownList>
											<asp:RequiredFieldValidator ID="vdrRegistUserStartDay" runat="server" ErrorMessage="開始日を入力して下さい。" ControlToValidate="txtRegistUserStartDay" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
											<asp:RangeValidator ID="vdeRegistUserStartDay" runat="server" ErrorMessage="正しく入力して下さい。<br />過去日は入力できません。" ControlToValidate="txtRegistUserStartDay" MaximumValue="2099/12/31"
												MinimumValue="1990/01/01" Type="Date" ValidationGroup="Detail" Display="Dynamic">*</asp:RangeValidator>
											<asp:CustomValidator ID="vdcRegistUserTime" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToValidate="lstRegistUserStartTime" OnServerValidate="vdcRegistUserTime_ServerValidate"
												ValidationGroup="Detail">*</asp:CustomValidator>
											<asp:CustomValidator ID="vdcRegistUserStartDay" runat="server" ErrorMessage="重複しているデータが存在します。" ControlToValidate="lstRegistUserStartTime" OnServerValidate="vdcRegistUserStartDay_ServerValidate"
												ValidationGroup="Detail">*</asp:CustomValidator>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											登録終了日時
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtRegistUserEndDay" runat="server" MaxLength="10" Width="65px"></asp:TextBox>
											<asp:DropDownList ID="lstRegistUserEndTime" runat="server">
												<asp:ListItem Value="00:00:00" Text="0:00"></asp:ListItem>
												<asp:ListItem Value="00:30:00" Text="0:30"></asp:ListItem>
												<asp:ListItem Value="01:00:00" Text="1:00"></asp:ListItem>
												<asp:ListItem Value="01:30:00" Text="1:30"></asp:ListItem>
												<asp:ListItem Value="02:00:00" Text="2:00"></asp:ListItem>
												<asp:ListItem Value="02:30:00" Text="2:30"></asp:ListItem>
												<asp:ListItem Value="03:00:00" Text="3:00"></asp:ListItem>
												<asp:ListItem Value="03:30:00" Text="3:30"></asp:ListItem>
												<asp:ListItem Value="04:00:00" Text="4:00"></asp:ListItem>
												<asp:ListItem Value="04:30:00" Text="4:30"></asp:ListItem>
												<asp:ListItem Value="05:00:00" Text="5:00"></asp:ListItem>
												<asp:ListItem Value="05:30:00" Text="5:30"></asp:ListItem>
												<asp:ListItem Value="06:00:00" Text="6:00"></asp:ListItem>
												<asp:ListItem Value="06:30:00" Text="6:30"></asp:ListItem>
												<asp:ListItem Value="07:00:00" Text="7:00"></asp:ListItem>
												<asp:ListItem Value="07:30:00" Text="7:30"></asp:ListItem>
												<asp:ListItem Value="08:00:00" Text="8:00"></asp:ListItem>
												<asp:ListItem Value="08:30:00" Text="8:30"></asp:ListItem>
												<asp:ListItem Value="09:00:00" Text="9:00"></asp:ListItem>
												<asp:ListItem Value="09:30:00" Text="9:30"></asp:ListItem>
												<asp:ListItem Value="10:00:00" Text="10:00"></asp:ListItem>
												<asp:ListItem Value="10:30:00" Text="10:30"></asp:ListItem>
												<asp:ListItem Value="11:00:00" Text="11:00"></asp:ListItem>
												<asp:ListItem Value="11:30:00" Text="11:30"></asp:ListItem>
												<asp:ListItem Value="12:00:00" Text="12:00"></asp:ListItem>
												<asp:ListItem Value="12:30:00" Text="12:30"></asp:ListItem>
												<asp:ListItem Value="13:00:00" Text="13:00"></asp:ListItem>
												<asp:ListItem Value="13:30:00" Text="13:30"></asp:ListItem>
												<asp:ListItem Value="14:00:00" Text="14:00"></asp:ListItem>
												<asp:ListItem Value="14:30:00" Text="14:30"></asp:ListItem>
												<asp:ListItem Value="15:00:00" Text="15:00"></asp:ListItem>
												<asp:ListItem Value="15:30:00" Text="15:30"></asp:ListItem>
												<asp:ListItem Value="16:00:00" Text="16:00"></asp:ListItem>
												<asp:ListItem Value="16:30:00" Text="16:30"></asp:ListItem>
												<asp:ListItem Value="17:00:00" Text="17:00"></asp:ListItem>
												<asp:ListItem Value="17:30:00" Text="17:30"></asp:ListItem>
												<asp:ListItem Value="18:00:00" Text="18:00"></asp:ListItem>
												<asp:ListItem Value="18:30:00" Text="18:30"></asp:ListItem>
												<asp:ListItem Value="19:00:00" Text="19:00"></asp:ListItem>
												<asp:ListItem Value="19:30:00" Text="19:30"></asp:ListItem>
												<asp:ListItem Value="20:00:00" Text="20:00"></asp:ListItem>
												<asp:ListItem Value="20:30:00" Text="20:30"></asp:ListItem>
												<asp:ListItem Value="21:00:00" Text="21:00"></asp:ListItem>
												<asp:ListItem Value="21:30:00" Text="21:30"></asp:ListItem>
												<asp:ListItem Value="22:00:00" Text="22:00"></asp:ListItem>
												<asp:ListItem Value="22:30:00" Text="22:30"></asp:ListItem>
												<asp:ListItem Value="23:00:00" Text="23:00"></asp:ListItem>
												<asp:ListItem Value="23:30:00" Text="23:30"></asp:ListItem>
												<asp:ListItem Value="24:00:00" Text="24:00"></asp:ListItem>
											</asp:DropDownList>
											<asp:RequiredFieldValidator ID="vdrRegistUserEndDay" runat="server" ErrorMessage="終了日を入力して下さい。" ControlToValidate="txtRegistUserEndDay" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
											<asp:RangeValidator ID="vdeRegistUserEndDay" runat="server" ErrorMessage="正しく入力して下さい。<br />過去日は入力できません。" ControlToValidate="txtRegistUserEndDay" MaximumValue="2099/12/31"
												MinimumValue="1990/01/01" Type="Date" ValidationGroup="Detail" Display="Dynamic">*</asp:RangeValidator>
											<asp:CompareValidator ID="vdcRegistUserDay" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtRegistUserStartDay" ControlToValidate="txtRegistUserEndDay"
												Operator="GreaterThanEqual" ValidationGroup="Detail" Display="Dynamic">*</asp:CompareValidator>
											<asp:CustomValidator ID="vdcRegistUserEndDay" runat="server" ErrorMessage="重複しているデータが存在します。" ControlToValidate="lstRegistUserEndTime" OnServerValidate="vdcRegistUserEndDay_ServerValidate"
												ValidationGroup="Detail">*</asp:CustomValidator>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											MAQIAｱﾌﾘｴｰﾄ<br />
											ｱｶｳﾝﾄﾛｯｸ解除日数
										</td>
										<td class="tdDataStyle" colspan="4">
											<asp:TextBox ID="txtAccountLockDays" runat="server" Width="50px" MaxLength="2"></asp:TextBox>
											<asp:RequiredFieldValidator ID="vdrAccountLockDays" runat="server" ErrorMessage="アカウントロック解除日数を入力してください。" ControlToValidate="txtAccountLockDays" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle2">
											停止
										</td>
										<td class="tdDataStyle">
											<asp:CheckBox ID="chkStopFlag" runat="server">
											</asp:CheckBox>
										</td>
									</tr>
								</table>
								<table border="0">
									<tr>
										<td style="padding-bottom: 10px;">
											<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" OnClientClick='return confirm("更新を行いますか？");' />
											<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" CausesValidation="false" OnClientClick='return confirm("削除を行いますか？");' />
											<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
										</td>
									</tr>
								</table>
							</asp:Panel>
							<asp:Panel runat="server" ID="pnlList">
								<fieldset class="fieldset-inner">
									<legend>[リスト（日付検索）]</legend>
									<asp:GridView ID="grdDispatchRegistSchedule" runat="server" AllowPaging="True" DataSourceID="dsDispatchRegistSchedule" AllowSorting="True" SkinID="GridViewColor"
										AutoGenerateColumns="False" Width="680px" Font-Size="X-Small" OnDataBound="grdDispatchRegistSchedule_DataBound">
										<Columns>
											<asp:TemplateField>
												<ItemTemplate>
													<asp:Label ID="lblPartnerStopFlag" runat="server" Text='<%# Eval("PARTNER_STOP_FLAG") %>' Visible="false"></asp:Label>
													<asp:Label ID="lblPartnerSiteNm" runat="server" Text='<%# Eval("PARTNER_SITE_NM") %>'></asp:Label>
													<asp:LinkButton ID="lnkPartnerSiteNm" runat="server" Text='<%# Eval("PARTNER_SITE_NM") %>' CommandArgument='<%# string.Format("{0}:{1}",Eval("PARTNER_SITE_SEQ"),Eval("SCHEDULE_SEQ")) %>'
														OnCommand="lnkPartnerSiteNm_Command" CausesValidation="False"></asp:LinkButton>
												</ItemTemplate>
												<HeaderTemplate>
													パートナーサイト名
												</HeaderTemplate>
											</asp:TemplateField>
											<asp:BoundField DataField="SCHEDULE_STOP_FLAG" HeaderText="停止">
												<ItemStyle HorizontalAlign="Center" Width="20px" />
											</asp:BoundField>
											<asp:BoundField DataField="REGIST_USER_START_DATE" HeaderText="開始日時" DataFormatString="{0:yyyy/MM/dd HH:mm}" HtmlEncode="False">
												<ItemStyle HorizontalAlign="Center" Width="120px" />
											</asp:BoundField>
											<asp:BoundField DataField="REGIST_USER_END_DATE" HeaderText="終了日時" DataFormatString="{0:yyyy/MM/dd HH:mm}" HtmlEncode="False">
												<ItemStyle HorizontalAlign="Center" Width="120px" />
											</asp:BoundField>
											<asp:BoundField DataField="ACCOUNT_LOCK_DAYS" HeaderText="ﾛｯｸ日数">
												<ItemStyle HorizontalAlign="Right" Width="40px" />
											</asp:BoundField>
											<asp:BoundField DataField="UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yyyy/MM/dd HH:mm}" HtmlEncode="False">
												<ItemStyle HorizontalAlign="Center" Width="120px" />
											</asp:BoundField>
										</Columns>
										<PagerSettings Mode="NumericFirstLast" />
									</asp:GridView>
								</fieldset>
							</asp:Panel>
							<asp:Panel runat="server" ID="pnlListS">
								<br />
								<fieldset class="fieldset-inner">
									<legend>[リスト（パートナーサイト検索）]</legend>
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td style="padding-bottom: 7px;">
												<table border="0" style="width: 400px" class="tableStyle">
													<tr>
														<td class="tdHeaderStyle2">
															パートナーサイト
														</td>
														<td class="tdDataStyle">
															<asp:DropDownList ID="lstSeekPartnerSite" runat="server" DataSourceID="dsPartnerSite" DataTextField="PARTNER_SITE_NM" DataValueField="PARTNER_SITE_SEQ"
																Width="240px">
															</asp:DropDownList>
														</td>
													</tr>
												</table>
											</td>
											<td style="padding-left: 10px; padding-bottom: 7px;">
												<asp:Label ID="lblDay" runat="server" Text="" Font-Size="X-Small"></asp:Label><asp:Label ID="Label1" runat="server" Text="以降" Font-Size="X-Small"></asp:Label>
											</td>
											<td style="padding-left: 10px; padding-bottom: 7px;">
												<asp:Button runat="server" ID="btnSelect" Text="検索" CssClass="seekbutton" OnClick="btnSelect_Click" />
											</td>
										</tr>
									</table>
									<asp:GridView ID="grdDispatchRegistScheduleS" runat="server" AllowPaging="True" DataSourceID="dsDispatchRegistScheduleS" AllowSorting="True" SkinID="GridViewColor"
										AutoGenerateColumns="False" Width="680px" Font-Size="X-Small" OnDataBound="grdDispatchRegistScheduleS_DataBound">
										<Columns>
											<asp:TemplateField>
												<ItemTemplate>
													<asp:Label ID="lblPartnerStopFlag" runat="server" Text='<%# Eval("PARTNER_STOP_FLAG") %>' Visible="false"></asp:Label>
													<asp:Label ID="lblPartnerSiteNm" runat="server" Text='<%# Eval("PARTNER_SITE_NM") %>'></asp:Label>
													<asp:LinkButton ID="lnkPartnerSiteNm" runat="server" Text='<%# Eval("PARTNER_SITE_NM") %>' CommandArgument='<%# string.Format("{0}:{1}",Eval("PARTNER_SITE_SEQ"),Eval("SCHEDULE_SEQ")) %>'
														OnCommand="lnkPartnerSiteNm_Command" CausesValidation="False"></asp:LinkButton>
												</ItemTemplate>
												<HeaderTemplate>
													パートナーサイト名
												</HeaderTemplate>
											</asp:TemplateField>
											<asp:BoundField DataField="SCHEDULE_STOP_FLAG" HeaderText="停止">
												<ItemStyle HorizontalAlign="Center" Width="20px" />
											</asp:BoundField>
											<asp:BoundField DataField="REGIST_USER_START_DATE" HeaderText="開始日時" DataFormatString="{0:yyyy/MM/dd HH:mm}" HtmlEncode="False">
												<ItemStyle HorizontalAlign="Center" Width="120px" />
											</asp:BoundField>
											<asp:BoundField DataField="REGIST_USER_END_DATE" HeaderText="終了日時" DataFormatString="{0:yyyy/MM/dd HH:mm}" HtmlEncode="False">
												<ItemStyle HorizontalAlign="Center" Width="120px" />
											</asp:BoundField>
											<asp:BoundField DataField="ACCOUNT_LOCK_DAYS" HeaderText="ﾛｯｸ日数">
												<ItemStyle HorizontalAlign="Right" Width="40px" />
											</asp:BoundField>
											<asp:BoundField DataField="UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yyyy/MM/dd HH:mm}" HtmlEncode="False">
												<ItemStyle HorizontalAlign="Center" Width="120px" />
											</asp:BoundField>
										</Columns>
										<PagerSettings Mode="NumericFirstLast" />
									</asp:GridView>
								</fieldset>
							</asp:Panel>
							<table border="0">
								<tr>
									<td>
										<asp:Button runat="server" ID="btnRegist" Text="登録予定追加" CssClass="seekbutton" OnClick="btnRegist_Click" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</fieldset>
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsPartnerSite" runat="server" SelectMethod="GetList" TypeName="PartnerSite">
		<SelectParameters>
			<asp:Parameter DefaultValue="0" Name="pStopFlag" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsDispatchRegistSchedule" runat="server" SelectMethod="GetPageCollection" TypeName="DispatchRegistSchedule" SelectCountMethod="GetPageCount"
		EnablePaging="True" OnSelected="dsDispatchRegistSchedule_Selected" OnSelecting="dsDispatchRegistSchedule_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pRegistDay" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsDispatchRegistScheduleS" runat="server" SelectMethod="GetPageCollection" TypeName="DispatchRegistSchedule" SelectCountMethod="GetPageCount"
		EnablePaging="True" OnSelected="dsDispatchRegistScheduleS_Selected" OnSelecting="dsDispatchRegistScheduleS_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pPartnerSiteSeq" Type="String" />
			<asp:Parameter Name="pRegistDay" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrRegistUserStartDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeRegistUserStartDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrRegistUserEndDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdeRegistUserEndDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdcRegistUserDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdcRegistUserTime" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vdcRegistUserStartDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8" TargetControlID="vdcRegistUserEndDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender64" TargetControlID="vdrAccountLockDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskRegistUserStartDay" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtRegistUserStartDay">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskRegistUserEndDay" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtRegistUserEndDay">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtAccountLockDays" />
</asp:Content>
