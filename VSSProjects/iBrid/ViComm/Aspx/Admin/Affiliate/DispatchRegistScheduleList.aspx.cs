﻿/*************************************************************************
--	System			: ViComm Admin
--	Sub System Name	: Admin
--	Title			: 新規登録振分け予定設定
--	Progaram ID		: Affiliate_DispatchRegistScheduleList
--
--  Creation Date	: 2010.04.13
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class Affiliate_DispatchRegistScheduleList:System.Web.UI.Page {
	string sEndTime = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
		vdeRegistUserStartDay.MinimumValue = DateTime.Today.ToString("yyyy/MM/dd");
		vdeRegistUserEndDay.MinimumValue = DateTime.Today.ToString("yyyy/MM/dd");
	}

	private void FirstLoad() {
		DataBind();
		lstPartnerSite.DataSourceID = "";
		lstSeekPartnerSite.DataSourceID = "";
		lstSeekPartnerSite.Items.Insert(0,new ListItem("",""));
	}

	private void InitPage() {
		grdDispatchRegistSchedule.PageSize = int.Parse(Session["PageSize"].ToString());
		grdDispatchRegistSchedule.PageIndex = 0;
		grdDispatchRegistScheduleS.PageSize = int.Parse(Session["PageSize"].ToString());
		grdDispatchRegistScheduleS.PageIndex = 0;
		pnlDtl.Visible = false;
		ClearField();
		GetList();
	}

	private void ClearField() {
		lstPartnerSite.SelectedIndex = 0;
		lstSeekPartnerSite.SelectedIndex = 0;
		txtRegistUserStartDay.Text = "";
		txtRegistUserEndDay.Text = "";
		txtAccountLockDays.Text = "0";
		lstRegistUserStartTime.SelectedIndex = 0;
		lstRegistUserEndTime.SelectedIndex = lstRegistUserEndTime.Items.Count - 1;
		chkStopFlag.Checked = false;
		lblScheduleSeq.Text = "";
		DispCalender();
	}

	private void DispCalender() {
		if (objCalender.SelectedDate.Equals(DateTime.MinValue)) {
			objCalender.VisibleDate = DateTime.MinValue;
		} else {
			objCalender.VisibleDate = objCalender.SelectedDate;
		}
	}

	protected void objCalender_SelectionChanged(object sender,EventArgs e) {
		GetList();
		txtRegistUserStartDay.Text = string.Format("{0:yyyy/MM/dd}",objCalender.SelectedDate);
	}

	protected void objCalender_DayRender(object sender,DayRenderEventArgs e) {
		using (DispatchRegistSchedule oSchedule = new DispatchRegistSchedule()) {
			int iCnt = oSchedule.GetDataCnt(string.Format("{0:yyyy/MM/dd}",e.Day.Date));
			if (iCnt > 0) {
				if (e.Day.IsWeekend) {
					e.Cell.BackColor = Color.Khaki;
				} else {
					e.Cell.BackColor = Color.Lavender;
				}
				e.Cell.Controls.Add(new LiteralControl("<br /><span style=\"font-size:x-small;\">" + String.Format("({0})</span>",iCnt.ToString())));
			} else {
				e.Cell.Controls.Add(new LiteralControl("<br />&nbsp;"));
			}
		}
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (!ViewState["ROWID"].ToString().Equals("")) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		GetData();
	}

	protected void btnSelect_Click(object sender,EventArgs e) {
		grdDispatchRegistScheduleS.DataBind();
	}

	protected void lnkPartnerSiteNm_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		lstPartnerSite.SelectedValue = sKeys[0];
		lblScheduleSeq.Text = sKeys[1];
		GetData();
		DispCalender();
	}

	protected void vdcRegistUserTime_ServerValidate(object source,ServerValidateEventArgs args) {
		if (IsValid) {
			args.IsValid = true;

			//終了時間が24:00の場合は、次の日の0:00として登録する
			sEndTime = lstRegistUserEndTime.SelectedValue;
			if (lstRegistUserEndTime.SelectedIndex.Equals(lstRegistUserEndTime.Items.Count - 1)) {
				txtRegistUserEndDay.Text = DateTime.Parse(txtRegistUserEndDay.Text).AddDays(1).ToString("yyyy/MM/dd");
				sEndTime = "00:00:00";
			}

			//開始日と終了日が同じ場合の、時間大小チェック
			if (txtRegistUserStartDay.Text.Equals(txtRegistUserEndDay.Text)) {
				if (lstRegistUserStartTime.SelectedIndex >= lstRegistUserEndTime.SelectedIndex) {
					args.IsValid = false;
					vdcRegistUserTime.ErrorMessage = "大小関係が正しくありません。";
				}
			}
			if (args.IsValid) {
				//開始日が今日の場合の時刻チェック
				if (txtRegistUserStartDay.Text.Equals(DateTime.Today.ToString("yyyy/MM/dd"))) {
					if (int.Parse(lstRegistUserStartTime.SelectedValue.Replace(":","")) <=
						int.Parse(DateTime.Now.ToString("HHmmss"))) {
						args.IsValid = false;
						vdcRegistUserTime.ErrorMessage = "過去は入力できません。";
					}
				}
			}
			if (args.IsValid) {
				//終了日が今日の場合の時刻チェック
				if (txtRegistUserEndDay.Text.Equals(DateTime.Today.ToString("yyyy/MM/dd"))) {
					if (int.Parse(sEndTime.Replace(":","")) <=
						int.Parse(DateTime.Now.ToString("HHmmss"))) {
						args.IsValid = false;
						vdcRegistUserTime.ErrorMessage = "過去は入力できません。";
					}
				}
			}
		}
	}

	protected void vdcRegistUserStartDay_ServerValidate(object source,ServerValidateEventArgs args) {
		if (IsValid) {
			//開始日重複チェック
			if (!txtRegistUserStartDay.Text.Equals("")) {
				args.IsValid = CheckStartDate(string.Format("{0} {1}",txtRegistUserStartDay.Text,lstRegistUserStartTime.SelectedValue));
			}
			if (args.IsValid) {
				//期間重複チェック
				if (!txtRegistUserEndDay.Text.Equals("")) {
					args.IsValid = CheckDate(
										string.Format("{0} {1}",txtRegistUserStartDay.Text,lstRegistUserStartTime.SelectedValue),
										string.Format("{0} {1}",txtRegistUserEndDay.Text,sEndTime)
									);
				}
			}
		}
	}

	protected void vdcRegistUserEndDay_ServerValidate(object source,ServerValidateEventArgs args) {
		if (IsValid) {
			//終了日重複チェック
			if (!txtRegistUserEndDay.Text.Equals("")) {
				args.IsValid = CheckEndDate(string.Format("{0} {1}",txtRegistUserEndDay.Text,sEndTime));
			}
		}
	}

	private bool CheckStartDate(string pRegistDate) {
		bool bOK = true;

		using (DispatchRegistSchedule oSchedule = new DispatchRegistSchedule()) {
			if (oSchedule.IsExistStart(lstPartnerSite.SelectedValue,lblScheduleSeq.Text,pRegistDate)) {
				bOK = false;
			}
		}
		return bOK;
	}

	private bool CheckEndDate(string pRegistDate) {
		bool bOK = true;

		using (DispatchRegistSchedule oSchedule = new DispatchRegistSchedule()) {
			if (oSchedule.IsExistEnd(lstPartnerSite.SelectedValue,lblScheduleSeq.Text,pRegistDate)) {
				bOK = false;
			}
		}
		return bOK;
	}

	private bool CheckDate(string pRegistStartDate,string pRegistEndDate) {
		bool bOK = true;

		using (DispatchRegistSchedule oSchedule = new DispatchRegistSchedule()) {
			if (oSchedule.IsExist(lstPartnerSite.SelectedValue,lblScheduleSeq.Text,pRegistStartDate,pRegistEndDate)) {
				bOK = false;
			}
		}
		return bOK;
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("DISPATCH_REGIST_SCHEDULE_GET");
			db.ProcedureInParm("PPARTNER_SITE_SEQ",DbSession.DbType.VARCHAR2,lstPartnerSite.SelectedValue);
			db.ProcedureInParm("PSCHEDULE_SEQ",DbSession.DbType.VARCHAR2,lblScheduleSeq.Text);
			db.ProcedureOutParm("PREGIST_USER_START_DAY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREGIST_USER_START_TIME",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREGIST_USER_END_DAY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREGIST_USER_END_TIME",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTOP_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PACCOUNT_LOCK_DAYS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				//更新の場合
				txtRegistUserStartDay.Text = db.GetStringValue("PREGIST_USER_START_DAY");
				txtRegistUserEndDay.Text = db.GetStringValue("PREGIST_USER_END_DAY");
				txtAccountLockDays.Text = db.GetStringValue("PACCOUNT_LOCK_DAYS");
				lstRegistUserStartTime.SelectedValue = db.GetStringValue("PREGIST_USER_START_TIME");
				lstRegistUserEndTime.SelectedValue = db.GetStringValue("PREGIST_USER_END_TIME");
				chkStopFlag.Checked = (db.GetStringValue("PSTOP_FLAG").Equals("0") == false);
				lstPartnerSite.Enabled = false;
			} else {
				//新規の場合
				ClearField();
				if (!objCalender.SelectedDate.Equals(DateTime.MinValue)) {
					txtRegistUserStartDay.Text = string.Format("{0:yyyy/MM/dd}",objCalender.SelectedDate);
					txtRegistUserEndDay.Text = txtRegistUserStartDay.Text;
				}
				lstPartnerSite.Enabled = true;
			}
		}
		GetListS();
		pnlDtl.Visible = true;
	}

	private void UpdateData(int pDelModeFlag) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("DISPATCH_REGIST_SCHEDULE_MNT");
			db.ProcedureInParm("PPARTNER_SITE_SEQ",DbSession.DbType.VARCHAR2,lstPartnerSite.SelectedValue);
			db.ProcedureInParm("PREGIST_USER_START_DAY",DbSession.DbType.VARCHAR2,txtRegistUserStartDay.Text);
			db.ProcedureInParm("PREGIST_USER_START_TIME",DbSession.DbType.VARCHAR2,lstRegistUserStartTime.SelectedValue);
			db.ProcedureInParm("PREGIST_USER_START_DAY",DbSession.DbType.VARCHAR2,txtRegistUserEndDay.Text);
			db.ProcedureInParm("PREGIST_USER_START_TIME",DbSession.DbType.VARCHAR2,sEndTime);
			db.ProcedureInParm("PSTOP_FLAG",DbSession.DbType.NUMBER,chkStopFlag.Checked);
			db.ProcedureInParm("PACCOUNT_LOCK_DAYS",DbSession.DbType.NUMBER,int.Parse(txtAccountLockDays.Text));
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,ViewState["REVISION_NO"].ToString());
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelModeFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		InitPage();
	}

	private void GetList() {
		ClearField();
		grdDispatchRegistSchedule.DataSourceID = "dsDispatchRegistSchedule";
		grdDispatchRegistSchedule.DataBind();
		grdDispatchRegistScheduleS.DataSourceID = "dsDispatchRegistScheduleS";
		grdDispatchRegistScheduleS.DataBind();
	}

	private void GetListS() {
		if (!lstSeekPartnerSite.SelectedIndex.Equals(0)) {
			lstSeekPartnerSite.SelectedValue = lstPartnerSite.SelectedValue;
			grdDispatchRegistScheduleS.DataBind();
		}
	}

	protected void lstPartnerSite_SelectedIndexChanged(object sender,EventArgs e) {
		GetListS();
	}

	protected void dsDispatchRegistSchedule_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		if (objCalender.SelectedDate.Equals(DateTime.MinValue)) {
			e.InputParameters[0] = string.Format("{0:yyyy/MM/dd}",DateTime.Today);
		} else {
			e.InputParameters[0] = string.Format("{0:yyyy/MM/dd}",objCalender.SelectedDate);
		}
	}

	protected void dsDispatchRegistSchedule_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
	}

	protected void grdDispatchRegistSchedule_DataBound(object sender,EventArgs e) {
		GridView grd = (GridView)sender as GridView;
		SetResult(grd);
	}

	protected void dsDispatchRegistScheduleS_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		if (objCalender.SelectedDate.Equals(DateTime.MinValue)) {
			lblDay.Text = string.Format("{0:yyyy/MM/dd}",DateTime.Today);
		} else {
			lblDay.Text = string.Format("{0:yyyy/MM/dd}",objCalender.SelectedDate);
		}
		e.InputParameters[0] = lstSeekPartnerSite.SelectedValue;
		e.InputParameters[1] = lblDay.Text;
	}

	protected void dsDispatchRegistScheduleS_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
	}

	protected void grdDispatchRegistScheduleS_DataBound(object sender,EventArgs e) {
		GridView grd = (GridView)sender as GridView;
		SetResult(grd);
	}

	private void SetResult(GridView grd) {
		foreach (GridViewRow gr in grd.Rows) {
			Label lblStop = (Label)gr.FindControl("lblPartnerStopFlag") as Label;
			Label lblNm = (Label)gr.FindControl("lblPartnerSiteNm") as Label;
			LinkButton lnkNm = (LinkButton)gr.FindControl("lnkPartnerSiteNm") as LinkButton;

			switch (int.Parse(lblStop.Text)) {
				case 0:
					lblNm.Visible = false;
					break;
				default:
					lnkNm.Visible = false;
					break;
			}
		}
	}
}
