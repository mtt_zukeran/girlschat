﻿/*************************************************************************
--	System			: ViComm Admin
--	Sub System Name	: Admin
--	Title			: パートナーサイト設定
--	Progaram ID		: Affiliate_PartnerSiteList
--
--  Creation Date	: 2010.03.15
--  Creater			: iBrid(A.Koyanagi)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;

public partial class Affiliate_PartnerSiteList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			if (!Session["AvailableMaqiaAffiliate"].ToString().Equals("1")) {
				string sRoot = ConfigurationManager.AppSettings["Root"].ToString().ToLower();
				Response.Redirect(sRoot + "/error/WebError.aspx?errtype=" + ViComm.ViCommConst.USER_INFO_INTERNAL_ERROR);
			}
			InitPage();
		}
	}

	protected void dsPartnerSite_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		grdPartnerSite.PageSize = int.Parse(Session["PageSize"].ToString());
		grdPartnerSite.PageIndex = 0;
		lblPartnerSiteSeq.Text = "";
		ClearField();
		pnlMainte.Visible = false;
		DataBind();
	}

	private void ClearField() {
		txtPartnerSiteNm.Text = "";
		txtPartnerSiteIni.Text = "";
		txtRegistUserAcceptUrl.Text = "";
		chkStopFlag.Checked = false;
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		GetData();
	}

	protected void lnkPartnerSiteNm_Command(object sender,CommandEventArgs e) {
		lblPartnerSiteSeq.Text = e.CommandArgument.ToString();
		GetData();
	}

	protected bool UrlCheck(string sUrl) {
		if (sUrl.IndexOf("\n") >= 0) {
			return false;
		} else {
			return true;
		}
	}

	protected void vdcPartnerSiteNm_ServerValidate(object source,ServerValidateEventArgs args) {
		if (System.Text.Encoding.GetEncoding(65001).GetByteCount(txtPartnerSiteNm.Text) <= txtPartnerSiteNm.MaxLength) {
			args.IsValid = true;
		} else {
			args.IsValid = false;
		}
	}

	protected void vdcPartnerSiteIni_ServerValidate(object source,ServerValidateEventArgs args) {
		if (System.Text.Encoding.GetEncoding(65001).GetByteCount(txtPartnerSiteIni.Text) <= txtPartnerSiteIni.MaxLength) {
			args.IsValid = true;
		} else {
			args.IsValid = false;
		}
	}

	protected void vdcRegistUserAcceptUrl_ServerValidate(object source,ServerValidateEventArgs args) {
		args.IsValid = UrlCheck(txtRegistUserAcceptUrl.Text);
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("PARTNER_SITE_GET");
			db.ProcedureInParm("PPARTNER_SITE_SEQ",DbSession.DbType.VARCHAR2,lblPartnerSiteSeq.Text);
			db.ProcedureOutParm("PPARTNER_SITE_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPARTNER_SITE_INITIAL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREGIST_USER_ACCEPT_URL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTOP_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				lblPartnerSiteSeq.Text = db.GetStringValue("PPARTNER_SITE_SEQ");
				txtPartnerSiteNm.Text = db.GetStringValue("PPARTNER_SITE_NM");
				txtPartnerSiteIni.Text = db.GetStringValue("PPARTNER_SITE_INITIAL");
				txtRegistUserAcceptUrl.Text = db.GetStringValue("PREGIST_USER_ACCEPT_URL");
				chkStopFlag.Checked = (db.GetStringValue("PSTOP_FLAG").Equals("0") == false);
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
	}

	private void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("PARTNER_SITE_MAINTE");
			db.ProcedureInParm("PPARTNER_SITE_SEQ",DbSession.DbType.VARCHAR2,lblPartnerSiteSeq.Text);
			db.ProcedureInParm("PPARTNER_SITE_NM",DbSession.DbType.VARCHAR2,txtPartnerSiteNm.Text);
			db.ProcedureInParm("PPARTNER_SITE_INITIAL",DbSession.DbType.VARCHAR2,txtPartnerSiteIni.Text);
			db.ProcedureInParm("PREGIST_USER_ACCEPT_URL",DbSession.DbType.VARCHAR2,txtRegistUserAcceptUrl.Text);
			db.ProcedureInParm("PSTOP_FLAG",DbSession.DbType.NUMBER,chkStopFlag.Checked);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		InitPage();
	}

}