<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DailyDispatchReportInquiry.aspx.cs" Inherits="Affiliate_DailyDispatchReportInquiry"
	Title="日別ユーザー振分け実績" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="日別ユーザー振分け実績"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle2">
							<asp:Label ID="lblSalesUnit" runat="server" Text="報告年月"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstYYYY" runat="server" Width="54px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstMM" runat="server" Width="43px">
								<asp:ListItem Value="">--</asp:ListItem>
								<asp:ListItem Value="01">01</asp:ListItem>
								<asp:ListItem Value="02">02</asp:ListItem>
								<asp:ListItem Value="03">03</asp:ListItem>
								<asp:ListItem Value="04">04</asp:ListItem>
								<asp:ListItem Value="05">05</asp:ListItem>
								<asp:ListItem Value="06">06</asp:ListItem>
								<asp:ListItem Value="07">07</asp:ListItem>
								<asp:ListItem Value="08">08</asp:ListItem>
								<asp:ListItem Value="09">09</asp:ListItem>
								<asp:ListItem Value="10">10</asp:ListItem>
								<asp:ListItem Value="11">11</asp:ListItem>
								<asp:ListItem Value="12">12</asp:ListItem>
							</asp:DropDownList>
							<asp:Label ID="lblMonth" runat="server" Text="月"></asp:Label>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[日別ユーザー振分け実績]</legend>
				<div style="height: 600px; width: auto; overflow: auto;">
					<asp:Table ID="tblReport" runat="server" CssClass="DataWebControlStyle" CellSpacing="0" CellPadding="20" rules="all" Border="1">
					</asp:Table>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
</asp:Content>
