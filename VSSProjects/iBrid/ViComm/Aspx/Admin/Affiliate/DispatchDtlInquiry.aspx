﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DispatchDtlInquiry.aspx.cs" Inherits="Affiliate_DispatchDtlInquiry"
	Title="ユーザー振分け明細" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="ユーザー振分け明細"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[抽出条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 680px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							パートナーサイト
						</td>
						<td class="tdDataStyle" colspan="3">
							<asp:DropDownList ID="lstPartnerSiteCd" runat="server" DataSourceID="dsPartnerSite" DataTextField="PARTNER_SITE_NM" DataValueField="PARTNER_SITE_SEQ" Width="240px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							<asp:Label ID="lblRegistDate" runat="server" Text="登録日"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtRegistDayFrom" runat="server" MaxLength="10" Width="62px"></asp:TextBox>&nbsp;～
							<asp:TextBox ID="txtRegistDayTo" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
							<asp:RequiredFieldValidator ID="vdqRegistDayFrom" runat="server" ErrorMessage="登録日Fromを入力して下さい。" ControlToValidate="txtRegistDayFrom" ValidationGroup="Key">*</asp:RequiredFieldValidator>
							<asp:RangeValidator ID="vdrRegistDayFrom" runat="server" ErrorMessage="登録日Fromを正しく入力して下さい。" ControlToValidate="txtRegistDayFrom" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:RangeValidator ID="vdrRegistDayTo" runat="server" ErrorMessage="登録日Toを正しく入力して下さい。" ControlToValidate="txtRegistDayTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
								Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:CompareValidator ID="vdcRegistDayFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtRegistDayFrom" ControlToValidate="txtRegistDayTo"
								Operator="GreaterThanEqual" ValidationGroup="Key" Display="Dynamic">*</asp:CompareValidator>
						</td>
						<td class="tdHeaderStyle2">
							<asp:Label ID="lblReault" runat="server" Text="結果"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstRegistResult" runat="server" DataSourceID="dsRegistResult" DataTextField="CODE_NM" DataValueField="CODE">
							</asp:DropDownList>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" CausesValidation="false" />
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[結果]</legend>
				<asp:Panel runat="server" ID="pnlRegiCount">
					<a class="reccount">OK&nbsp;<%#GetOKCount()%>件&nbsp;&nbsp;重複&nbsp;<%#GetNGCount()%>件</a>
				</asp:Panel>
				<br />
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="430px">
					<asp:GridView ID="grdList" runat="server" AllowPaging="True" AutoGenerateColumns="False" AllowSorting="True" SkinID="GridViewColor" Width="940px" OnDataBound="grdList_DataBound"
						Font-Size="X-Small">
						<Columns>
							<asp:TemplateField HeaderText="ﾛｸﾞｲﾝID">
								<ItemTemplate>
									<asp:HyperLink ID="lnkLoginID" runat="server" NavigateUrl='<%# string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>'
										Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle VerticalAlign="Top" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾊﾟｽﾜｰﾄﾞ">
								<ItemTemplate>
									<asp:Label ID="lblLoginPass" runat="server" Text='<%# Eval("LOGIN_PASSWORD") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle VerticalAlign="Top" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="電話番号">
								<ItemTemplate>
									<asp:Label ID="lblTel" runat="server" Text='<%# Eval("TEL") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle VerticalAlign="Top" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="登録日">
								<ItemTemplate>
									<asp:Label ID="lblUserRegistDate" runat="server" Text='<%# Eval("USER_REGIST_DATE") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle Width="130px" VerticalAlign="Top" HorizontalAlign="Center" />
							</asp:TemplateField>
							<asp:BoundField DataField="PARTNER_SITE_INITIAL" HeaderText="ﾊﾟｰﾄﾅｰ">
							</asp:BoundField>
							<asp:BoundField DataField="REGIST_DATE" HeaderText="ﾊﾟｰﾄﾅｰｻｲﾄ  登録完了日">
								<ItemStyle Width="130px" HorizontalAlign="Center" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="結果">
								<ItemTemplate>
									<asp:Label ID="lblRegistResult" runat="server" Text='<%# Eval("REGIST_RESULT") %>' Visible="false"></asp:Label>
									<asp:Label ID="lblRegistResultNm" runat="server" Text='<%# Eval("REGIST_RESULT_NM") %>'></asp:Label>
									<asp:LinkButton ID="lnkRegistResultNm" runat="server" CommandArgument='<%# string.Format("{0}:{1}:{2}",Eval("USER_SEQ"),Eval("HISTORY_SEQ"),Eval("HOST_NM")) %>' OnCommand="lnkRegistResult_Command"
										Text='<%# Eval("REGIST_RESULT_NM") %>'></asp:LinkButton>
								</ItemTemplate>
								<ItemStyle Width="40px" HorizontalAlign="Center" />
							</asp:TemplateField>
							<asp:BoundField DataField="PARTNER_SITE_LOGIN_ID" HeaderText="ﾊﾟｰﾄﾅｰｻｲﾄID"></asp:BoundField>
							<asp:BoundField DataField="AD_CD" HeaderText="広告ｺｰﾄﾞ"></asp:BoundField>
							<asp:BoundField DataField="AD_NM" HeaderText="広告名称"></asp:BoundField>
						</Columns>
					</asp:GridView>
				</asp:Panel>
				<br />
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count
						<%#GetRecCount()%>
					</a>
					<br>
					<a class="reccount">Current viewing page
						<%=grdList.PageIndex + 1%>
						of
						<%=grdList.PageCount%>
					</a>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsPartnerSite" runat="server" SelectMethod="GetList" TypeName="PartnerSite" OnSelecting="dsPartnerSite_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pStopFlag" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsRegistResult" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="97" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsRegistDtlCnt" runat="server" SelectMethod="GetPageCollection" TypeName="DispatchDtl" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsRegistDtlCnt_Selecting" OnSelected="dsRegistDtlCnt_Selected">
		<SelectParameters>
			<asp:Parameter Name="pPartnerSiteCd" />
			<asp:Parameter Name="pRegistDateFrom" />
			<asp:Parameter Name="pRegistDateTo" />
			<asp:Parameter Name="pRegistResult" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdqRegistDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrRegistDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrRegistDayTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdcRegistDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskRegistDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtRegistDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskRegistDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtRegistDayTo">
	</ajaxToolkit:MaskedEditExtender>
</asp:Content>
