﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ListYakyukenPicView.ascx.cs" Inherits="Controls_ListYakyukenPicView" %>
<asp:Panel ID="pnlDataNotFound" runat="server">
    登録されていません。
</asp:Panel>
<asp:Panel ID="pnlData" runat="server" Visible="false">
    <table>
        <asp:Repeater ID="rptObject" runat="server">
            <ItemTemplate>
                <%# this.NeedTrStart(Container.ItemIndex) ? "<tr>" : string.Empty%>
                <td colspan="<%# this.GetColSpan(Container.ItemIndex) %>" align="left" style="padding-left: 2px;
                    padding-right: 2px;">
                    <div style="width: 80px; ">
                        <%--<asp:Image ID="imgPicSmall" runat="server" ImageUrl='<%# Eval("OBJ_SMALL_PHOTO_IMG_PATH","../{0}") %>'
							BorderStyle="solid" BorderWidth="2px" Height="80px" BorderColor='<%# GetBorderColor(Container.DataItem) %>'>
						</asp:Image>--%>
                        <asp:ImageButton ID="btnPicSmall" runat="server" ImageUrl='<%# Eval("SMALL_PHOTO_IMG_PATH","../{0}") %>'
                            BorderStyle="solid" BorderWidth="1px" Height="80px" OnCommand="btnPicSmall_Command"
                            CommandArgument='<%# string.Format("{0},{1}",Eval("PIC_SEQ"),Eval("AUTH_FLAG")) %>' />
                        <div style="text-align: center; font-size: 10px; ">
                                <%# GetStatusMark(Container.DataItem)%>
                            <br />
                            <span>
                                <%# DataBinder.Eval(Container.DataItem, "CREATE_DATE")%>
                            </span>
                        </div>
                    </div>
                </td>
                <%# this.NeedTrEnd(Container.ItemIndex) ? "</tr>":string.Empty %>
            </ItemTemplate>
        </asp:Repeater>
    </table>
</asp:Panel>
<asp:ObjectDataSource ID="dsYakyukenPic" runat="server" TypeName="YakyukenPic" SelectMethod="GetList"
    OnSelecting="dsYakyukenPic_Selecting" OnSelected="dsYakyukenPic_Selected">
    <SelectParameters>
        <asp:Parameter Name="pSiteCd" Type="String" />
        <asp:Parameter Name="pUserSeq" Type="String" />
        <asp:Parameter Name="pUserCharNo" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
