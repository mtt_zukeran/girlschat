﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: ゲームお宝動画--	Progaram ID		: ListSocialGamePicView
--
--  Creation Date	: 2013.05.17
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.ComponentModel;
using iBridCommLib;
using ViComm;

public partial class Controls_ListSocialGamePicView:System.Web.UI.UserControl,IPickupObj {
	private string siteCd = null;
	private string userSeq = null;
	private string userCharNo = null;
	private string picType = ViCommConst.ATTACHED_SOCIAL_GAME.ToString();
	private int columnSize = 1;
	private int dataCount = 0;
	private bool isColoredObj = false;

	[Browsable(true)]
	public string SiteCd {
		get {
			return this.siteCd;
		}
		set {
			this.siteCd = value;
		}
	}
	[Browsable(true)]
	public string UserSeq {
		get {
			return this.userSeq;
		}
		set {
			this.userSeq = value;
		}
	}
	[Browsable(true)]
	public string UserCharNo {
		get {
			return this.userCharNo;
		}
		set {
			this.userCharNo = value;
		}
	}
	[Browsable(true)]
	public int ColumnSize {
		get {
			return this.columnSize;
		}
		set {
			this.columnSize = value;
		}
	}
	[Browsable(true)]
	public bool IsColoredObj {
		get {
			return this.isColoredObj;
		}
		set {
			this.isColoredObj = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
	}

	public void Display() {
		this.rptObject.DataSource = this.dsManTreasure;
		this.DataBind();
		this.pnlData.Visible = (this.dataCount > 0);
		this.pnlDataNotFound.Visible = (this.dataCount == 0);
	}

	protected void dsManTreasure_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = this.SiteCd;
		e.InputParameters["pUserSeq"] = this.UserSeq;
		e.InputParameters["pUserCharNo"] = this.UserCharNo;
		this.dataCount = 0;
	}

	protected void dsManTreasure_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue is DataSet) {
			DataSet oReturnDataSet = (DataSet)e.ReturnValue;
			if (oReturnDataSet.Tables.Count > 0) {
				this.dataCount = oReturnDataSet.Tables[0].Rows.Count;
			}
		}
	}

	protected bool NeedTrEnd(int pItemIndex) {
		bool bNeedTrEnd = false;
		bNeedTrEnd = bNeedTrEnd || ((pItemIndex + 1) >= this.dataCount);
		bNeedTrEnd = bNeedTrEnd || ((pItemIndex + 1) % this.columnSize) == 0;
		return bNeedTrEnd;
	}
	protected bool NeedTrStart(int pItemIndex) {
		bool bNeedTrEnd = false;
		bNeedTrEnd = bNeedTrEnd || (pItemIndex % this.columnSize) == 0;
		return bNeedTrEnd;
	}
	protected string GetColSpan(int pItemIndex) {
		int iColSpan = 1;
		if ((pItemIndex + 1) >= this.dataCount) {
			iColSpan = this.columnSize - (pItemIndex % this.columnSize);
		}
		return iColSpan.ToString();
	}

	protected void btnPicSmall_Command(object sender,CommandEventArgs e) {
		if (this.ObjClick == null) {
			return;
		}
		string sCastGamePicSeq = iBridUtil.GetStringValue(e.CommandArgument);
		string sPicType = ViCommConst.ATTACHED_SOCIAL_GAME.ToString();

		this.ObjClick(this,new ObjClickEventArgs(sCastGamePicSeq,sPicType,ViCommConst.FLAG_OFF_STR));
	}

	#region IPickupObj メンバ

	[Browsable(true)]
	public event ObjClickEventHandler ObjClick;

	#endregion
}
