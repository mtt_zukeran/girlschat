﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ListResembledPicView.ascx.cs" Inherits="Controls_ListResembledPicView" %>
<asp:Panel ID="pnlDataNotFound" runat="server">
    類似画像は見つかりませんでした。
</asp:Panel>
<asp:Panel ID="pnlData" runat="server" Visible="false">
    <table>
        <asp:Repeater ID="rptObject" runat="server">
            <ItemTemplate>
                <%# this.NeedTrStart(Container.ItemIndex) ? "<tr>" : string.Empty%>
                <td colspan="<%# this.GetColSpan(Container.ItemIndex) %>" align="left" style="padding-left: 2px;
                    padding-right: 2px;">
                    <div style="width: 80px; ">
                        <asp:ImageButton ID="btnPicSmall" runat="server" ImageUrl='<%# Eval("OBJ_SMALL_PHOTO_IMG_PATH","../{0}") %>'
                            BorderStyle="solid" BorderWidth="1px" Height="80px" OnCommand="btnPicSmall_Command"
                            CommandArgument='<%# string.Format("{0},{1},{2},{3}",Eval("PIC_SEQ"),Eval("PIC_TYPE"),Eval("OBJ_NOT_APPROVE_FLAG"),Eval("OBJ_NOT_PUBLISH_FLAG")) %>' />
                        <div style="text-align: center; font-size: 10px; ">
                            <span style="<%# GetStatusColor(Container.DataItem) %>">
                                <%# GetStatusMark(Container.DataItem)%>
                            </span>
                            <br />
                            <span>
                                <%# DataBinder.Eval(Container.DataItem, "UPLOAD_DATE")%>
                            </span>
                        </div>
                    </div>
                </td>
                <%# this.NeedTrEnd(Container.ItemIndex) ? "</tr>":string.Empty %>
            </ItemTemplate>
        </asp:Repeater>
    </table>
</asp:Panel>
<asp:ObjectDataSource ID="dsCastPic" runat="server" TypeName="CastPic" SelectMethod="SeekCastPicResembled"
    OnSelecting="dsCastPic_Selecting" OnSelected="dsCastPic_Selected">
    <SelectParameters>
        <asp:Parameter Name="pSiteCd" Type="String" />
        <asp:Parameter Name="pUserSeq" Type="String" />
        <asp:Parameter Name="pUserCharNo" Type="String" />
        <asp:Parameter Name="pLoginId" Type="string" />
        <asp:Parameter Name="pOriginalPicSeq" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
