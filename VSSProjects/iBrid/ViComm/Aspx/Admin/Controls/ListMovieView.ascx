﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ListMovieView.ascx.cs"
    Inherits="Controls_ListMovieView" %>
<asp:Panel ID="pnlDataNotFound" runat="server">
    登録されていません。
</asp:Panel>
<asp:Panel ID="pnlData" runat="server" Visible="false">
    <table>
        <asp:Repeater ID="rptObject" runat="server">
            <ItemTemplate>
                <%# this.NeedTrStart(Container.ItemIndex) ? "<tr>" : string.Empty%>
                <td colspan="<%# this.GetColSpan(Container.ItemIndex) %>" align="left" style="padding-left: 2px;
                    padding-right: 2px;">
                    <div style="width: 80px; ">
                        <asp:ImageButton ID="btnPicSmall" runat="server" ImageUrl='<%# Eval("THUMBNAIL_IMG_PATH","../{0}") %>'
                            BorderStyle="solid" BorderWidth="1px" Height="80px" Width="80px" OnCommand="btnPicSmall_Command"
                            CommandArgument='<%# string.Format("{0},{1},{2},{3}",Eval("MOVIE_SEQ"),Eval("MOVIE_TYPE"),Eval("OBJ_NOT_APPROVE_FLAG"),Eval("OBJ_NOT_PUBLISH_FLAG")) %>' />
                        <div style="text-align: center; font-size: 10px; ">
                            <span style="<%# GetStatusColor(Eval("NOT_APPROVE_MARK")) %>">
                                <%# DataBinder.Eval(Container.DataItem, "NOT_APPROVE_MARK")%>
                            </span>
                            <br />
                            <span>
                                <%# DataBinder.Eval(Container.DataItem, "UPLOAD_DATE")%>
                            </span>
                        </div>
                    </div>
                </td>
                <%# this.NeedTrEnd(Container.ItemIndex) ? "</tr>":string.Empty %>
            </ItemTemplate>
        </asp:Repeater>
    </table>
</asp:Panel>
<asp:ObjectDataSource ID="dsCastMovie" runat="server" TypeName="CastMovie" SelectMethod="GetList"
    OnSelecting="dsCastMovie_Selecting" OnSelected="dsCastMovie_Selected">
    <SelectParameters>
        <asp:Parameter Name="pSiteCd" Type="String" />
        <asp:Parameter Name="pUserSeq" Type="String" />
        <asp:Parameter Name="pUserCharNo" Type="String" />
        <asp:Parameter Name="pMovieType" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
