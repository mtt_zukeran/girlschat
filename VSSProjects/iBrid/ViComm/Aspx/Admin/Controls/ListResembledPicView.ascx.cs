﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.ComponentModel;

using iBridCommLib;
using ViComm;

public partial class Controls_ListResembledPicView:System.Web.UI.UserControl,IPickupObj {
	private string siteCd = null;
	private string userSeq = null;
	private string userCharNo = null;
	private string loginId = null;
	private string picType = ViCommConst.ATTACHED_PROFILE.ToString();
	private string originalPicSeq = null;
	private int columnSize = 1;
	private int dataCount = 0;
	private bool isColoredObj = false;

	[Browsable(true)]
	public string SiteCd {
		get {
			return this.siteCd;
		}
		set {
			this.siteCd = value;
		}
	}
	[Browsable(true)]
	public string UserSeq {
		get {
			return this.userSeq;
		}
		set {
			this.userSeq = value;
		}
	}
	[Browsable(true)]
	public string UserCharNo {
		get {
			return this.userCharNo;
		}
		set {
			this.userCharNo = value;
		}
	}
	[Browsable(true)]
	public string PicType {
		get {
			return this.picType;
		}
		set {
			this.picType = value;
		}
	}
	[Browsable(true)]
	public string LoginId {
		get {
			return this.loginId;
		}
		set {
			this.loginId = value;
		}
	}
	[Browsable(true)]
	public string OriginalPicSeq {
		get {
			return this.originalPicSeq;
		}
		set {
			this.originalPicSeq = value;
		}
	}
	[Browsable(true)]
	public int ColumnSize {
		get {
			return this.columnSize;
		}
		set {
			this.columnSize = value;
		}
	}
	[Browsable(true)]
	public bool IsColoredObj {
		get {
			return this.isColoredObj;
		}
		set {
			this.isColoredObj = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
	}
	public void Display() {
		this.rptObject.DataSource = this.dsCastPic;
		this.DataBind();
		this.pnlData.Visible = (this.dataCount > 0);
		this.pnlDataNotFound.Visible = (this.dataCount == 0);

	}

	protected void dsCastPic_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = this.SiteCd;
		e.InputParameters["pUserSeq"] = this.UserSeq;
		e.InputParameters["pUserCharNo"] = this.UserCharNo;
		e.InputParameters["pLoginId"] = this.LoginId;
		e.InputParameters["pOriginalPicSeq"] = this.OriginalPicSeq;

		this.dataCount = 0;
	}

	protected void dsCastPic_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue is DataSet) {
			DataSet oReturnDataSet = (DataSet)e.ReturnValue;
			if (oReturnDataSet.Tables.Count > 0) {
				this.dataCount = oReturnDataSet.Tables[0].Rows.Count;
			}
		}
	}

	protected bool NeedTrEnd(int pItemIndex) {
		bool bNeedTrEnd = false;
		bNeedTrEnd = bNeedTrEnd || ((pItemIndex + 1) >= this.dataCount);
		bNeedTrEnd = bNeedTrEnd || ((pItemIndex + 1) % this.columnSize) == 0;
		return bNeedTrEnd;
	}
	protected bool NeedTrStart(int pItemIndex) {
		bool bNeedTrEnd = false;
		bNeedTrEnd = bNeedTrEnd || (pItemIndex % this.columnSize) == 0;
		return bNeedTrEnd;
	}
	protected string GetColSpan(int pItemIndex) {
		int iColSpan = 1;
		if ((pItemIndex + 1) >= this.dataCount) {
			iColSpan = this.columnSize - (pItemIndex % this.columnSize);
		}
		return iColSpan.ToString();
	}

	protected string GetStatusMark(object pDataItem) {
		string sNotApproveFlag = iBridUtil.GetStringValue(DataBinder.Eval(pDataItem,"OBJ_NOT_APPROVE_FLAG"));
		string sNotPublishFlag = iBridUtil.GetStringValue(DataBinder.Eval(pDataItem,"OBJ_NOT_PUBLISH_FLAG"));
		string sPicType = iBridUtil.GetStringValue(DataBinder.Eval(pDataItem,"PIC_TYPE"));
		return GetStatusMark(sNotApproveFlag,sNotPublishFlag,sPicType);
	}
	protected string GetStatusMark(object pNotApproveFlag,object pNotPublishFlag,object pPicType) {
		string sStatusMark = "公開中";
		if (pNotApproveFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			sStatusMark = "申請中";
		} else if (pNotPublishFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			sStatusMark = "非公開";
		} else if (pPicType.Equals(ViCommConst.ATTACHED_HIDE.ToString())) {
			sStatusMark = "非表示";
		}
		return sStatusMark;
	}

	protected string GetStatusColor(object pDataItem) {
		if (this.IsColoredObj) {
			string sNotApproveFlag = iBridUtil.GetStringValue(DataBinder.Eval(pDataItem,"OBJ_NOT_APPROVE_FLAG"));
			string sNotPublishFlag = iBridUtil.GetStringValue(DataBinder.Eval(pDataItem,"OBJ_NOT_PUBLISH_FLAG"));
			string sPicType = iBridUtil.GetStringValue(DataBinder.Eval(pDataItem,"PIC_TYPE"));

			string sBgColor = "color:{0};";
			if (sNotApproveFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				return string.Format(sBgColor,"green");
			} else if (sNotPublishFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				return string.Format(sBgColor,"red");
			} else if (sPicType.Equals(ViCommConst.ATTACHED_HIDE.ToString())) {
				return string.Format(sBgColor,"blue");
			}
			return string.Empty;
		}
		return string.Empty;
	}

	protected void btnPicSmall_Command(object sender,CommandEventArgs e) {
		if (this.ObjClick == null) {
			return;
		}
		string[] sArgs = iBridUtil.GetStringValue(e.CommandArgument).Split(',');
		string sPicType = sArgs[1];
		string sNotApproveFlag = sArgs[2];
		string sNotPublishFlag = sArgs[3];
		bool bAddable = sNotApproveFlag.Equals(ViCommConst.FLAG_OFF_STR) && sNotPublishFlag.Equals(ViCommConst.FLAG_OFF_STR) && !sPicType.Equals(ViCommConst.ATTACHED_HIDE.ToString());

		this.ObjClick(this,new ObjClickEventArgs(sArgs[0],sArgs[1],bAddable ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR));
	}

	#region IPickupObj メンバ


	[Browsable(true)]
	public event ObjClickEventHandler ObjClick;

	#endregion
}
