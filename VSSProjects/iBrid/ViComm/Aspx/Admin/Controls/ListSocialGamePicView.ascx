﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ListSocialGamePicView.ascx.cs" Inherits="Controls_ListSocialGamePicView" %>
<asp:Panel ID="pnlDataNotFound" runat="server">
    登録されていません。</asp:Panel>
<asp:Panel ID="pnlData" runat="server" Visible="false">
    <table>
        <asp:Repeater ID="rptObject" runat="server">
            <ItemTemplate>
                <%# this.NeedTrStart(Container.ItemIndex) ? "<tr>" : string.Empty%>
                <td colspan="<%# this.GetColSpan(Container.ItemIndex) %>" align="left" style="padding-left: 2px;padding-right: 2px;">
                    <div style="width: 80px; ">
                        <asp:ImageButton ID="btnPicSmall" runat="server" ImageUrl='<%# Eval("OBJ_SMALL_PHOTO_IMG_PATH","../{0}") %>'
                            BorderStyle="solid" BorderWidth="1px" Height="80px" OnCommand="btnPicSmall_Command"
                            CommandArgument='<%# Eval("CAST_GAME_PIC_SEQ") %>' />
                        <div style="text-align: center; font-size: 10px; ">
							<asp:Label runat="server" Text='<%# Eval("PUBLISH_FLAG").ToString().Equals("1") ? "公開中" : "認証待" %>'></asp:Label>
                            <br />
                            <span>
                                <%# DataBinder.Eval(Container.DataItem,"UPLOAD_DATE")%>
                            </span>
                        </div>
                    </div>
                </td>
                <%# this.NeedTrEnd(Container.ItemIndex) ? "</tr>":string.Empty %>
            </ItemTemplate>
        </asp:Repeater>
    </table>
</asp:Panel>
<asp:ObjectDataSource ID="dsManTreasure" runat="server" TypeName="ManTreasure" SelectMethod="GetList"
    OnSelecting="dsManTreasure_Selecting" OnSelected="dsManTreasure_Selected">
    <SelectParameters>
        <asp:Parameter Name="pSiteCd" Type="String" />
        <asp:Parameter Name="pUserSeq" Type="String" />
        <asp:Parameter Name="pUserCharNo" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
