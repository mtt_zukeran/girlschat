﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.ComponentModel;

using ViComm;
using iBridCommLib;

public partial class Controls_ListMovieView : System.Web.UI.UserControl,IPickupObj {
	private string siteCd = null;
	private string userSeq = null;
	private string userCharNo = null;
	private string movieType = ViCommConst.ATTACHED_PROFILE.ToString();
	private int columnSize = 1;
	private int dataCount = 0;
	private bool isColoredObj = false;

	protected int DataCount{
		get{ return this.dataCount;}
	}

	[Browsable(true)]
	public string SiteCd {
		get { return this.siteCd; }
		set { this.siteCd = value; }
	}
	[Browsable(true)]
	public string UserSeq {
		get { return this.userSeq; }
		set { this.userSeq = value; }
	}
	[Browsable(true)]
	public string UserCharNo {
		get { return this.userCharNo; }
		set { this.userCharNo = value; }
	}
	[Browsable(true)]
	public string MovieType {
		get { return this.movieType; }
		set { this.movieType = value; }
	}
	[Browsable(true)]
	public int ColumnSize {
		get { return this.columnSize; }
		set { this.columnSize = value; }
	}
	[Browsable(true)]
	public bool IsColoredObj {
		get { return this.isColoredObj; }
		set { this.isColoredObj = value; }
	}

	protected void Page_Load(object sender, EventArgs e) {
	}
	public void Display() {
		this.rptObject.DataSource = this.dsCastMovie;
		this.DataBind();

		this.pnlData.Visible = (this.dataCount>0);
		this.pnlDataNotFound.Visible = (this.dataCount == 0);
		
	}

	protected void dsCastMovie_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = this.SiteCd;
		e.InputParameters["pUserSeq"] = this.UserSeq;
		e.InputParameters["pUserCharNo"] = this.UserCharNo;
		e.InputParameters["pMovieType"] = this.MovieType;

		this.dataCount = 0;
	}

	protected void dsCastMovie_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue is DataSet) {
			DataSet oReturnDataSet = (DataSet)e.ReturnValue;
			if (oReturnDataSet.Tables.Count > 0) {
				this.dataCount = oReturnDataSet.Tables[0].Rows.Count;
			}
		}
	}

	protected bool NeedTrEnd(int pItemIndex) {
		bool bNeedTrEnd = false;
		bNeedTrEnd = bNeedTrEnd || ((pItemIndex + 1) >= this.dataCount);
		bNeedTrEnd = bNeedTrEnd || ((pItemIndex + 1) % this.columnSize) == 0;
		return bNeedTrEnd;
	}
	protected bool NeedTrStart(int pItemIndex) {
		bool bNeedTrEnd = false;
		bNeedTrEnd = bNeedTrEnd || (pItemIndex % this.columnSize) == 0;
		return bNeedTrEnd;
	}
	protected string GetColSpan(int pItemIndex) {
		int iColSpan = 1;
		if ((pItemIndex + 1) >= this.dataCount) {
			iColSpan = this.columnSize - (pItemIndex % this.columnSize);
		}
		return iColSpan.ToString();
	}

	//protected string GetObjDivBackgroundColor(object pNotApproveMark) {
	//    if (this.IsColoredObj) {
	//        string sBgColor = "background-color:{0};";
	//        switch (iBridUtil.GetStringValue(pNotApproveMark)) {
	//            case "非公開":
	//                return string.Format(sBgColor,"red");
	//            case "未認証":
	//                return string.Format(sBgColor,"green");
	//            default:
	//                return string.Format(sBgColor,"blue");
	//        }
	//    }
	//    return string.Empty;
	//}

	//protected string GetInfoDivColor() {
	//    if (this.IsColoredObj) {
	//        return "color:white;";
	//    }
	//    return string.Empty;
	//}

	protected string GetStatusColor(object pNotApproveMark) {
		if (this.IsColoredObj) {
			string sBgColor = "color:{0};";
			switch (iBridUtil.GetStringValue(pNotApproveMark)) {
				case "非公開":
					return string.Format(sBgColor,"red");
				case "未認証":
					return string.Format(sBgColor,"green");
				default:
					return string.Empty;
			}
		}
		return string.Empty;
	}

	protected void btnPicSmall_Command(object sender,CommandEventArgs e) {
		if (this.ObjClick == null) {
			return;
		}
		string[] sArgs = iBridUtil.GetStringValue(e.CommandArgument).Split(',');
		string sMovieType = sArgs[1];
		string sNotApproveFlag = sArgs[2];
		string sNotPublishFlag = sArgs[3];
		bool bAddable = sNotApproveFlag.Equals(ViCommConst.FLAG_OFF_STR) && sNotPublishFlag.Equals(ViCommConst.FLAG_OFF_STR) && !sMovieType.Equals(ViCommConst.ATTACHED_HIDE.ToString());

		if (sMovieType.Equals(ViCommConst.ATTACHED_SOCIAL_GAME.ToString())) {
			bAddable = false;
		}

		this.ObjClick(this,new ObjClickEventArgs(sArgs[0],sMovieType,bAddable ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR));
	}

	#region IPickupObj メンバ

	[Browsable(true)]
	public event ObjClickEventHandler ObjClick;

	#endregion
}
