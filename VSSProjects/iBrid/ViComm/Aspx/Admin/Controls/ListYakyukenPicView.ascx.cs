﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.ComponentModel;

using iBridCommLib;
using ViComm;

public partial class Controls_ListYakyukenPicView:System.Web.UI.UserControl,IPickupObj {
	private string siteCd = null;
	private string userSeq = null;
	private string userCharNo = null;
	private string picType = ViCommConst.ATTACHED_PROFILE.ToString();
	private int columnSize = 1;
	private int dataCount = 0;
	private bool isColoredObj = false;

	[Browsable(true)]
	public string SiteCd {
		get {
			return this.siteCd;
		}
		set {
			this.siteCd = value;
		}
	}
	[Browsable(true)]
	public string UserSeq {
		get {
			return this.userSeq;
		}
		set {
			this.userSeq = value;
		}
	}
	[Browsable(true)]
	public string UserCharNo {
		get {
			return this.userCharNo;
		}
		set {
			this.userCharNo = value;
		}
	}
	[Browsable(true)]
	public int ColumnSize {
		get {
			return this.columnSize;
		}
		set {
			this.columnSize = value;
		}
	}
	[Browsable(true)]
	public bool IsColoredObj {
		get {
			return this.isColoredObj;
		}
		set {
			this.isColoredObj = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
	}
	public void Display() {
		this.rptObject.DataSource = this.dsYakyukenPic;
		this.DataBind();
		this.pnlData.Visible = (this.dataCount > 0);
		this.pnlDataNotFound.Visible = (this.dataCount == 0);

	}

	protected void dsYakyukenPic_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = this.SiteCd;
		e.InputParameters["pUserSeq"] = this.UserSeq;
		e.InputParameters["pUserCharNo"] = this.UserCharNo;

		this.dataCount = 0;
	}

	protected void dsYakyukenPic_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue is DataSet) {
			DataSet oReturnDataSet = (DataSet)e.ReturnValue;
			if (oReturnDataSet.Tables.Count > 0) {
				this.dataCount = oReturnDataSet.Tables[0].Rows.Count;
			}
		}
	}

	protected bool NeedTrEnd(int pItemIndex) {
		bool bNeedTrEnd = false;
		bNeedTrEnd = bNeedTrEnd || ((pItemIndex + 1) >= this.dataCount);
		bNeedTrEnd = bNeedTrEnd || ((pItemIndex + 1) % this.columnSize) == 0;
		return bNeedTrEnd;
	}
	protected bool NeedTrStart(int pItemIndex) {
		bool bNeedTrEnd = false;
		bNeedTrEnd = bNeedTrEnd || (pItemIndex % this.columnSize) == 0;
		return bNeedTrEnd;
	}
	protected string GetColSpan(int pItemIndex) {
		int iColSpan = 1;
		if ((pItemIndex + 1) >= this.dataCount) {
			iColSpan = this.columnSize - (pItemIndex % this.columnSize);
		}
		return iColSpan.ToString();
	}

	protected string GetStatusMark(object pDataItem) {
		string sAuthFlag = iBridUtil.GetStringValue(DataBinder.Eval(pDataItem,"AUTH_FLAG"));
		return GetStatusMark(sAuthFlag);
	}
	protected string GetStatusMark(string pAuthFlag) {
		string sStatusMark = "認証待ち";
		if (pAuthFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			sStatusMark = "公開中";
		}
		return sStatusMark;
	}

	//protected string GetObjDivBackgroundColor(object pDataItem) {
	//    if (this.IsColoredObj) {
	//        string sNotApproveFlag = iBridUtil.GetStringValue(DataBinder.Eval(pDataItem,"OBJ_NOT_APPROVE_FLAG"));
	//        string sNotPublishFlag = iBridUtil.GetStringValue(DataBinder.Eval(pDataItem,"OBJ_NOT_PUBLISH_FLAG"));
	//        string sPicType = iBridUtil.GetStringValue(DataBinder.Eval(pDataItem,"PIC_TYPE"));

	//        string sBgColor = "background-color:{0};";
	//        if (sNotApproveFlag.Equals(ViCommConst.FLAG_ON_STR)) {
	//            return string.Format(sBgColor,"green");
	//        } else if (sNotPublishFlag.Equals(ViCommConst.FLAG_ON_STR) || sPicType.Equals(ViCommConst.ATTACHED_HIDE.ToString())) {
	//            return string.Format(sBgColor,"red");
	//        }
	//        return string.Format(sBgColor,"blue");
	//    }
	//    return string.Empty;
	//}

	//protected string GetInfoDivColor() {
	//    if (this.IsColoredObj) {
	//        return "color:white;"; 
	//    }
	//    return string.Empty;
	//}

	protected void btnPicSmall_Command(object sender,CommandEventArgs e) {
		if (this.ObjClick == null) {
			return;
		}
		string[] sArgs = iBridUtil.GetStringValue(e.CommandArgument).Split(',');
		string sPicType = ViCommConst.ATTACHED_YAKYUKEN.ToString();

		this.ObjClick(this,new ObjClickEventArgs(sArgs[0],sPicType,ViCommConst.FLAG_OFF_STR));
	}

	#region IPickupObj メンバ


	[Browsable(true)]
	public event ObjClickEventHandler ObjClick;

	#endregion
}
