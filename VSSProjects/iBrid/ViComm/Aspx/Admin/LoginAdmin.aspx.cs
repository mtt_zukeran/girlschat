﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 管理者ログイン
--	Progaram ID		: LoginAdmin
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Text.RegularExpressions;
using iBridCommLib;
using ViComm;

public partial class LoginAdmin:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
		}
	}

	protected void ctlLogin_Authenticate(object sender,AuthenticateEventArgs e) {
		bool bOk;
		string sUserId = "DUMMY";
		string sPassword = "DUMMY";
		string sAdminType = "";
		string sManagerSeq = "";
		string sProductionSeq = "";
		string sSiteCd = "";
		string sAvailableMaqiaAffiliate = "";
		string sViewSalesFlag = "0";

		int iCompanyMask = 0;
		int iSysOwner = 0;

		Session["AD_GROUP_CD"] = "";

		using (Admin oAdmin = new Admin())
		using (Manager oManager = new Manager())
		using (Sys oSys = new Sys()) {
			if (oManager.GetOne(ctlLogin.UserName,ctlLogin.Password)) {
				bOk = true;
				sAdminType = ViCommConst.RIGHT_PRODUCTION;
				sSiteCd = "";
				sManagerSeq = oManager.managerSeq;
				sProductionSeq = oManager.productionSeq;
			} else {
				bOk = oAdmin.GetOne(ctlLogin.UserName,ctlLogin.Password);
				if (bOk) {
					sAdminType = oAdmin.adminType;
					sSiteCd = oAdmin.siteCd;
					sManagerSeq = oAdmin.managerSeq;
					sProductionSeq = oAdmin.productionSeq;
					sViewSalesFlag = oAdmin.viewSalesFlag;
				}
			}

			oSys.GetValue("AVAILABLE_MAQIA_AFFILIATE_FLAG",out sAvailableMaqiaAffiliate);

			if (bOk) {
				bool bAdManageOnly = false;
				using (ManageCompany oCompany = new ManageCompany()) {
					if (oCompany.GetOne()) {
						bAdManageOnly = oCompany.adManageHost.Equals(Request.Url.Host);
						iCompanyMask = oCompany.companyMask;
					}
				}
				bool bPassIp;

				if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["CheckAuthIp"]).Equals(ViCommConst.FLAG_ON_STR)) {
					using (AuthIP oAuthIp = new AuthIP()) {
						bPassIp = oAuthIp.IsExist(Request.UserHostAddress);
					}
				} else {
					bPassIp = true;
				}

				bool bAllowLoginAdminType = false;
				bAllowLoginAdminType = sAdminType.Equals(ViCommConst.RIGHT_AD_MANAGE);

				if ((!sAdminType.Equals(string.Empty) && !bAllowLoginAdminType && bPassIp == false))
				{
					Response.Clear();
					Response.StatusCode = 403;
					Response.End();
				}

				if (bAdManageOnly == false) {
					switch (sAdminType) {
						case ViCommConst.RIGHT_SYS_OWNER:
							sUserId = "ivp-owner";
							sPassword = "ivp-owner";
							iSysOwner = 1;
							break;

						case ViCommConst.RIGHT_SITE_OWNER:
							sUserId = "ivp-admin";
							sPassword = "ivp-admin";
							break;

						case ViCommConst.RIGHT_SITE_MANAGER:
							sUserId = "ivp-manager";
							sPassword = "ivp-manager";
							break;

						case ViCommConst.RIGHT_STAFF:
							sUserId = "ivp-staff";
							sPassword = "ivp-staff";
							Session["AD_GROUP_CD"] = oAdmin.adGroupCd;
							break;

						case ViCommConst.RIGHT_LOCAL_STAFF:
							sUserId = "ivp-local-staff";
							sPassword = "ivp-local-staff";
							Session["AD_GROUP_CD"] = oAdmin.adGroupCd;
							break;

						case ViCommConst.RIGHT_PRODUCTION:
							sUserId = "ivp-local-staff";
							sPassword = "ivp-local-staff";
							Session["AD_GROUP_CD"] = oAdmin.adGroupCd;
							break;

						case ViCommConst.RIGHT_AD_MANAGE:
							sUserId = "ivp-staff";
							sPassword = "ivp-staff";
							Session["AD_GROUP_CD"] = oAdmin.adGroupCd;
							Session["AD_CD"] = oAdmin.adCd;
							break;
					}
				} else {
					sAdminType = ViCommConst.RIGHT_AD_MANAGE;
					sUserId = "ivp-staff";
					sPassword = "ivp-staff";
					Session["AD_GROUP_CD"] = oAdmin.adGroupCd;
					Session["AD_CD"] = oAdmin.adCd;
				}
			}
		}


		if (Membership.ValidateUser(sUserId,sPassword)) {
			Session["PageSize"] = ConfigurationManager.AppSettings["PageSize"];
			Session["Root"] = ConfigurationManager.AppSettings["Root"];
			Session["Login"] = "1";
			Session["SysLocNm"] = "MAQIA";
			Session["SystemOwner"] = iSysOwner;
			Session["AdminType"] = sAdminType;
			Session["MANAGER_SEQ"] = sManagerSeq;
			Session["PRODUCTION_SEQ"] = sProductionSeq;
			Session["ViewSalesFlag"] = sViewSalesFlag;
			Session["AdminId"] = ctlLogin.UserName;
			Session["SiteCd"] = sSiteCd;
			Session["CompanyMask"] = iCompanyMask;
			Session["AvailableMaqiaAffiliate"] = sAvailableMaqiaAffiliate;
			SessionObjs userObjs = new SessionObjs();
			Session["objs"] = userObjs;

			ParseViComm oParseViComm = new ParseViComm(
					@"((\$\w{1,})\(?(;?|[^;]*;,[^)&]*|[^)""&]*)?\)?;)",
					RegexOptions.IgnoreCase,
					ViComm.ViCommConst.CARRIER_OTHERS,
					userObjs);

			// &はPostbackのJavaScriptに含まれる 
			// javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$HolderContent$btnListSeek&quot;, &quot;&quot;, true, &quot;Key&quot;, &quot;&quot;, false, false))" 


			Session["parse"] = new ParseHTML(oParseViComm);
			oParseViComm.parseContainer = (ParseHTML)Session["parse"];
			oParseViComm.parseContainer.IsPcDesign = true;

			//後で調べる 
			userObjs.site.GetOne("A001");

			//必ずMain.aspxにリダイレクトする 
			double dTimeout = ViCommConst.DEFAULT_COOKIE_TIMEOUT;
			if (ConfigurationManager.AppSettings["CookieTimeout"] != null) {
				dTimeout = double.Parse(ConfigurationManager.AppSettings["CookieTimeout"]);
			}
			FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
													1,
													sUserId,
													DateTime.Now,
													DateTime.Now.AddMinutes(dTimeout),
													false,
													"");

			HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName,FormsAuthentication.Encrypt(ticket));
			Response.Cookies.Add(cookie);
			Response.Redirect(string.Format("{0}/Main.aspx",Session["Root"].ToString()));

			//FormsAuthentication.RedirectFromLoginPage(sUserId,false);
		}
	}

}
