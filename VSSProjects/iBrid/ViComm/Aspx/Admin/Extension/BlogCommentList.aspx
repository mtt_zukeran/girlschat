﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="BlogCommentList.aspx.cs" Inherits="Extension_BlogCommentList" Title="ブログコメント一覧"
    ValidateRequest="false" %>
<%@ Import Namespace="ViComm" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="ブログコメント一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 800px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" Enabled="false"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="tdHeaderStyle2">
                                会員ID
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtLoginId" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle2">
                                キーワード<br />
                                (空白区切り/全てを含む)
                            </td>
                            <td class="tdDataStyle" colspan="3">
                                <asp:TextBox ID="txtKeyword" runat="server" Width="300px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[ｺﾒﾝﾄ一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdBlogComment.PageIndex + 1 %>
                        of
                        <%= grdBlogComment.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="443px">
                    <asp:GridView ID="grdBlogComment" runat="server" AllowPaging="True" AutoGenerateColumns="False" OnSorting="grdBlogComment_Sorting" EnableSortingAndPagingCallbacks="false"
                        DataSourceID="dsBlogComment" SkinID="GridViewColor" AllowSorting="true" DataKeyNames="LOGIN_ID,SITE_CD,USER_SEQ,USER_CHAR_NO">
                        <Columns>
                            <asp:TemplateField HeaderText="ﾊﾝﾄﾞﾙ名<br>会員ID" SortExpression="LOGIN_ID">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblHandleNm" runat="server" NavigateUrl='<%# string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>'
                                        Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:HyperLink><br />
                                    <asp:Label ID="lblLoginId" runat="server" Text='<%# Eval("LOGIN_ID") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="120px" HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="COMMENT_DOC" HeaderText="コメント" >
                                <ItemStyle HorizontalAlign="left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DEL_STATUS" HeaderText="状態" >
                                <ItemStyle HorizontalAlign="center" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="投稿日時" SortExpression="CREATE_DATE">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("CREATE_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
            <br />
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsBlogComment" runat="server" SelectMethod="GetPageCollection" ConvertNullToDBNull="true" SortParameterName=""
        TypeName="BlogComment" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelected="dsBlogComment_Selected"
        OnSelecting="dsBlogComment_Selecting">
        <SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
</asp:Content>
