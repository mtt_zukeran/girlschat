﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ソーシャルゲーム基本設定
--	Progaram ID		: SocialGameList
--
--  Creation Date	: 2011.07.21
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using iBridCommLib;
using System.Drawing;
using ViComm;

public partial class Extension_SocialGameList:System.Web.UI.Page {
	string recCount;

	protected string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		private set {
			this.ViewState["SiteCd"] = value;
		}
	}

	protected string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		private set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	protected string Rowid {
		get {
			return iBridUtil.GetStringValue(this.ViewState["Rowid"]);
		}
		private set {
			this.ViewState["Rowid"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		if (!this.IsPostBack) {
			this.InitPage();
		}
	}

	protected void lnkSiteCd_Command(object sender,CommandEventArgs e) {
		this.SiteCd = e.CommandArgument.ToString();
		this.pnlMainte.Visible = true;
		this.pnlDtl.Visible = true;
		this.GetData();
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlMainte.Visible = false;
		this.pnlDtl.Visible = false;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.UpdateData(ViCommConst.FLAG_OFF);
		this.InitPage();
	}

	protected void dsSocialGame_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		this.pnlMainte.Visible = false;
		this.pnlDtl.Visible = false;
		this.ClearFields();
		this.GetList();
	}

	private void ClearFields() {
		this.pnlDtl.DataBind();
		this.txtManGameServicePoint.Text = string.Empty;
		this.txtManInitialMissionForce.Text = string.Empty;
		this.txtManInitialAttackForce.Text = string.Empty;
		this.txtManInitialDefenceForce.Text = string.Empty;
		this.txtCastGameServicePoint.Text = string.Empty;
		this.txtCastInitialMissionForce.Text = string.Empty;
		this.txtCastInitialAttackForce.Text = string.Empty;
		this.txtCastInitialDefenceForce.Text = string.Empty;
		this.txtAddFellowForce.Text = string.Empty;
		this.txtRemoveFellowForce.Text = string.Empty;
		this.txtRemoveFellowPartnerForce.Text = string.Empty;
		this.txtMosaicErasePoint.Text = string.Empty;
		this.txtMovieTicketPoint.Text = string.Empty;
		this.txtPresentPointRate.Text = string.Empty;
		this.txtManTreasureDropRate.Text = string.Empty;
		this.txtCastTreasureDropRate.Text = string.Empty;
		this.txtDefFroceDecreaseRateMin.Text = string.Empty;
		this.txtDefFroceDecreaseRateMax.Text = string.Empty;
		this.txtPoliceComeRate.Text = string.Empty;
		this.txtPoliceComeRateLvDefference.Text = string.Empty;
		this.txtPoliceLvDifference.Text = string.Empty;
		this.txtPtPoliceComeRate.Text = string.Empty;
		this.txtPtPoliceComeRateLvDefference.Text = string.Empty;
		this.txtPtPoliceLvDifference.Text = string.Empty;
		this.txtSuppPoliceComeRate.Text = string.Empty;
		this.txtSuppPoliceComeRateLvDefference.Text = string.Empty;
		this.txtSuppPoliceLvDifference.Text = string.Empty;
		this.txtGetTreasureFrinedlyPoint.Text = string.Empty;
		this.txtHugFriendlyPoint.Text = string.Empty;
		this.txtHugFriendlyPointByFellow.Text = string.Empty;
		this.txtHugCooperationPoint.Text = string.Empty;
		this.txtHuggedAddExpByFellow.Text = string.Empty;
		this.txtForceRecoveryCoopPoint.Text = string.Empty;
		this.txtFriendlyPointDelDays.Text = string.Empty;
		this.txtGameLogDelDays.Text = string.Empty;
		this.txtSameUserAttackLimitCount.Text = string.Empty;
		this.txtPtBattleAttackLimitCount.Text = string.Empty;
		this.txtPtBattleAttackIntervalMin.Text = string.Empty;
		this.txtDateLimitCount.Text = string.Empty;
		this.txtPartTimeJobLimitCount.Text = string.Empty;
		this.txtBattleWinExp.Text = string.Empty;
		this.txtBattleLossExp.Text = string.Empty;
		this.txtPartyBattleWinExp.Text = string.Empty;
		this.txtPartyBattleLossExp.Text = string.Empty;
		this.txtBattleDecreaseEndurance.Text = string.Empty;
		this.txtSupportBattleWinFriendly.Text = string.Empty;
		this.txtSupportBattleLossFriendly.Text = string.Empty;
		this.txtSupportBattleWinCoop.Text = string.Empty;
		this.txtSupportBattleLossCoop.Text = string.Empty;
		this.txtSupportRequestCount.Text = string.Empty;
		this.txtHugBonusRate.Text = string.Empty;
		this.txtPresentFriendlyPtRate.Text = string.Empty;
		this.txtPresentFriendlyPtRateByFellow.Text = string.Empty;
		this.txtReturnFriendlyPtRate.Text = string.Empty;
		this.txtReturnFriendlyPtRateByFellow.Text = string.Empty;
		this.txtPresentCoopPtRate.Text = string.Empty;
		this.txtPresentCoopPtRateByFellow.Text = string.Empty;
		this.txtPresentCoopPtRateNotCharge.Text = string.Empty;
		this.txtPresentCoopPtRateByFellowNotCharge.Text = string.Empty;
		this.txtAcceptPicPoint.Text = string.Empty;
		this.txtAcceptMoviePoint.Text = string.Empty;
		this.txtFreePresentLimitCount.Text = string.Empty;
	}

	private void GetData() {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("SOCIAL_GAME_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureOutParm("pMAN_GAME_SERVICE_POINT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pCAST_GAME_SERVICE_POINT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pMAN_INITIAL_MISSION_FORCE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pCAST_INITIAL_MISSION_FORCE",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pMAN_INITIAL_ATTACK_FORCE",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pCAST_INITIAL_ATTACK_FORCE",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pMAN_INITIAL_DEFENCE_FORCE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pCAST_INITIAL_DEFENCE_FORCE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pADD_FELLOW_FORCE",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pREMOVE_FELLOW_FORCE",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pREMOVE_FELLOW_PARTNER_FORCE",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pMOSAIC_ERASE_POINT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pMOVIE_TICKET_POINT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPRESENT_POINT_RATE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pMAN_TREASURE_DROP_RATE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pCAST_TREASURE_DROP_RATE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pDEF_FORCE_DECREASE_RATE_MIN",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pDEF_FORCE_DECREASE_RATE_MAX",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPOLICE_COME_RATE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPOLICE_COME_RATE_LV_DIFF",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPOLICE_LV_DIFFERENCE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPT_POLICE_COME_RATE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPT_POLICE_COME_RATE_LV_DIFF",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPT_POLICE_LV_DIFFERENCE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSUPP_POLICE_COME_RATE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSUPP_POLICE_COME_RATE_LV_DIFF",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSUPP_POLICE_LV_DIFFERENCE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pGET_TREASURE_FRIENDLY_POINT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pHUG_FRIENDLY_POINT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pHUG_FRIENDLY_POINT_BY_FELLOW",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pHUG_COOPERATION_POINT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pHUGGED_ADD_EXP_BY_FELLOW",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pHUG_BONUS_RATE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPRESENT_FRIEND_PT_RATE",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pPRESENT_FRIEND_PT_RATE_FELLOW",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pRETURN_FRIEND_PT_RATE",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pRETURN_FRIEND_PT_RATE_FELLOW",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pPRESENT_COOP_PT_RATE",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pPRESENT_COOP_PT_RATE_FELLOW",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pPRESENT_COOP_PT_RATE_NC",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pPRESENT_COOP_PT_RATE_FLW_NC",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pFORCE_RECOVERY_COOP_POINT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pFRIENDLY_POINT_DEL_DAYS",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pGAME_LOG_DEL_DAYS",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSAME_USER_ATTACK_LIMIT_COUNT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPT_BATTLE_ATTACK_LIMIT_COUNT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPT_BATTLE_ATTACK_INTERVAL_MIN",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pDATE_LIMIT_COUNT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPART_TIME_JOB_LIMIT_COUNT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pBATTLE_WIN_EXP",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pBATTLE_LOSS_EXP",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPARTY_BATTLE_WIN_EXP",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPARTY_BATTLE_LOSS_EXP",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pBATTLE_DECREASE_ENDURANCE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPSUPPORT_BATTLE_WIN_FRIENDLY",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPSUPPORT_BATTLE_LOSS_FRIENDLY",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPSUPPORT_BATTLE_WIN_COOP",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPSUPPORT_BATTLE_LOSS_COOP",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPSUPPORT_REQUEST_COUNT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pACCEPT_PIC_POINT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pACCEPT_MOVIE_POINT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pFREE_PRESENT_LIMIT_COUNT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pROWID",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pROW_COUNT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
			this.Rowid = oDbSession.GetStringValue("pROWID");

			if (int.Parse(oDbSession.GetStringValue("pROW_COUNT")) > 0) {
				this.pnlDtl.DataBind();
				this.txtManGameServicePoint.Text = oDbSession.GetStringValue("pMAN_GAME_SERVICE_POINT");
				this.txtManInitialMissionForce.Text = oDbSession.GetStringValue("pMAN_INITIAL_MISSION_FORCE");
				this.txtManInitialAttackForce.Text = oDbSession.GetStringValue("pMAN_INITIAL_ATTACK_FORCE");
				this.txtManInitialDefenceForce.Text = oDbSession.GetStringValue("pMAN_INITIAL_DEFENCE_FORCE");
				this.txtCastGameServicePoint.Text = oDbSession.GetStringValue("pCAST_GAME_SERVICE_POINT");
				this.txtCastInitialMissionForce.Text = oDbSession.GetStringValue("pCAST_INITIAL_MISSION_FORCE");
				this.txtCastInitialAttackForce.Text = oDbSession.GetStringValue("pCAST_INITIAL_ATTACK_FORCE");
				this.txtCastInitialDefenceForce.Text = oDbSession.GetStringValue("pCAST_INITIAL_DEFENCE_FORCE");
				this.txtAddFellowForce.Text = oDbSession.GetStringValue("pADD_FELLOW_FORCE");
				this.txtRemoveFellowForce.Text = oDbSession.GetStringValue("pREMOVE_FELLOW_FORCE");
				this.txtRemoveFellowPartnerForce.Text = oDbSession.GetStringValue("pREMOVE_FELLOW_PARTNER_FORCE");
				this.txtMosaicErasePoint.Text = oDbSession.GetStringValue("pMOSAIC_ERASE_POINT");
				this.txtMovieTicketPoint.Text = oDbSession.GetStringValue("pMOVIE_TICKET_POINT");
				this.txtPresentPointRate.Text = oDbSession.GetStringValue("pPRESENT_POINT_RATE");
				this.txtManTreasureDropRate.Text = oDbSession.GetStringValue("pMAN_TREASURE_DROP_RATE");
				this.txtCastTreasureDropRate.Text = oDbSession.GetStringValue("pCAST_TREASURE_DROP_RATE");
				this.txtDefFroceDecreaseRateMin.Text = oDbSession.GetStringValue("pDEF_FORCE_DECREASE_RATE_MIN");
				this.txtDefFroceDecreaseRateMax.Text = oDbSession.GetStringValue("pDEF_FORCE_DECREASE_RATE_MAX");
				this.txtPoliceComeRate.Text = oDbSession.GetStringValue("pPOLICE_COME_RATE");
				this.txtPoliceComeRateLvDefference.Text = oDbSession.GetStringValue("pPOLICE_COME_RATE_LV_DIFF");
				this.txtPoliceLvDifference.Text = oDbSession.GetStringValue("pPOLICE_LV_DIFFERENCE");
				this.txtPtPoliceComeRate.Text = oDbSession.GetStringValue("pPT_POLICE_COME_RATE");
				this.txtPtPoliceComeRateLvDefference.Text = oDbSession.GetStringValue("pPT_POLICE_COME_RATE_LV_DIFF");
				this.txtPtPoliceLvDifference.Text = oDbSession.GetStringValue("pPT_POLICE_LV_DIFFERENCE");
				this.txtSuppPoliceComeRate.Text = oDbSession.GetStringValue("pSUPP_POLICE_COME_RATE");
				this.txtSuppPoliceComeRateLvDefference.Text = oDbSession.GetStringValue("pSUPP_POLICE_COME_RATE_LV_DIFF");
				this.txtSuppPoliceLvDifference.Text = oDbSession.GetStringValue("pSUPP_POLICE_LV_DIFFERENCE");
				this.txtGetTreasureFrinedlyPoint.Text = oDbSession.GetStringValue("pGET_TREASURE_FRIENDLY_POINT");
				this.txtHugFriendlyPoint.Text = oDbSession.GetStringValue("pHUG_FRIENDLY_POINT");
				this.txtHugFriendlyPointByFellow.Text = oDbSession.GetStringValue("pHUG_FRIENDLY_POINT_BY_FELLOW");
				this.txtHugCooperationPoint.Text = oDbSession.GetStringValue("pHUG_COOPERATION_POINT");
				this.txtHuggedAddExpByFellow.Text = oDbSession.GetStringValue("pHUGGED_ADD_EXP_BY_FELLOW");
				this.txtHugBonusRate.Text = oDbSession.GetStringValue("pHUG_BONUS_RATE");
				this.txtPresentFriendlyPtRate.Text = oDbSession.GetStringValue("pPRESENT_FRIEND_PT_RATE");
				this.txtPresentFriendlyPtRateByFellow.Text = oDbSession.GetStringValue("pPRESENT_FRIEND_PT_RATE_FELLOW");
				this.txtReturnFriendlyPtRate.Text = oDbSession.GetStringValue("pRETURN_FRIEND_PT_RATE");
				this.txtReturnFriendlyPtRateByFellow.Text = oDbSession.GetStringValue("pRETURN_FRIEND_PT_RATE_FELLOW");
				this.txtPresentCoopPtRate.Text = oDbSession.GetStringValue("pPRESENT_COOP_PT_RATE");
				this.txtPresentCoopPtRateByFellow.Text = oDbSession.GetStringValue("pPRESENT_COOP_PT_RATE_FELLOW");
				this.txtPresentCoopPtRateNotCharge.Text = oDbSession.GetStringValue("pPRESENT_COOP_PT_RATE_NC");
				this.txtPresentCoopPtRateByFellowNotCharge.Text = oDbSession.GetStringValue("pPRESENT_COOP_PT_RATE_FLW_NC");
				this.txtForceRecoveryCoopPoint.Text = oDbSession.GetStringValue("pFORCE_RECOVERY_COOP_POINT");
				this.txtFriendlyPointDelDays.Text = oDbSession.GetStringValue("pFRIENDLY_POINT_DEL_DAYS");
				this.txtGameLogDelDays.Text = oDbSession.GetStringValue("pGAME_LOG_DEL_DAYS");
				this.txtSameUserAttackLimitCount.Text = oDbSession.GetStringValue("pSAME_USER_ATTACK_LIMIT_COUNT");
				this.txtPtBattleAttackLimitCount.Text = oDbSession.GetStringValue("pPT_BATTLE_ATTACK_LIMIT_COUNT");
				this.txtPtBattleAttackIntervalMin.Text = oDbSession.GetStringValue("pPT_BATTLE_ATTACK_INTERVAL_MIN");
				this.txtDateLimitCount.Text = oDbSession.GetStringValue("pDATE_LIMIT_COUNT");
				this.txtPartTimeJobLimitCount.Text = oDbSession.GetStringValue("pPART_TIME_JOB_LIMIT_COUNT");
				this.txtBattleWinExp.Text = oDbSession.GetStringValue("pBATTLE_WIN_EXP");
				this.txtBattleLossExp.Text = oDbSession.GetStringValue("pBATTLE_LOSS_EXP");
				this.txtPartyBattleWinExp.Text = oDbSession.GetStringValue("pPARTY_BATTLE_WIN_EXP");
				this.txtPartyBattleLossExp.Text = oDbSession.GetStringValue("pPARTY_BATTLE_LOSS_EXP");
				this.txtBattleDecreaseEndurance.Text = oDbSession.GetStringValue("pBATTLE_DECREASE_ENDURANCE");
				this.txtSupportBattleWinFriendly.Text = oDbSession.GetStringValue("pPSUPPORT_BATTLE_WIN_FRIENDLY");
				this.txtSupportBattleLossFriendly.Text = oDbSession.GetStringValue("pPSUPPORT_BATTLE_LOSS_FRIENDLY");
				this.txtSupportBattleWinCoop.Text = oDbSession.GetStringValue("pPSUPPORT_BATTLE_WIN_COOP");
				this.txtSupportBattleLossCoop.Text = oDbSession.GetStringValue("pPSUPPORT_BATTLE_LOSS_COOP");
				this.txtSupportRequestCount.Text = oDbSession.GetStringValue("pPSUPPORT_REQUEST_COUNT");
				this.txtAcceptPicPoint.Text = oDbSession.GetStringValue("pACCEPT_PIC_POINT");
				this.txtAcceptMoviePoint.Text = oDbSession.GetStringValue("pACCEPT_MOVIE_POINT");
				this.txtFreePresentLimitCount.Text = oDbSession.GetStringValue("pFREE_PRESENT_LIMIT_COUNT");
			} else {
				this.ClearFields();
			}
		}
	}

	private void GetList() {
		this.grdSocialGame.DataBind();
		this.pnlCount.DataBind();
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void UpdateData(int pDelFlag) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("SOCIAL_GAME_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pMAN_GAME_SERVICE_POINT",DbSession.DbType.VARCHAR2,this.txtManGameServicePoint.Text);
			oDbSession.ProcedureInParm("pCAST_GAME_SERVICE_POINT",DbSession.DbType.VARCHAR2,this.txtCastGameServicePoint.Text);
			oDbSession.ProcedureInParm("pMAN_INITIAL_MISSION_FORCE",DbSession.DbType.VARCHAR2,this.txtManInitialMissionForce.Text);
			oDbSession.ProcedureInParm("pCAST_INITIAL_MISSION_FORCE",DbSession.DbType.VARCHAR2,this.txtCastInitialMissionForce.Text);
			oDbSession.ProcedureInParm("pMAN_INITIAL_ATTACK_FORCE",DbSession.DbType.VARCHAR2,this.txtManInitialAttackForce.Text);
			oDbSession.ProcedureInParm("pCAST_INITIAL_ATTACK_FORCE",DbSession.DbType.VARCHAR2,this.txtCastInitialAttackForce.Text);
			oDbSession.ProcedureInParm("pMAN_INITIAL_DEFENCE_FORCE",DbSession.DbType.VARCHAR2,this.txtManInitialDefenceForce.Text);
			oDbSession.ProcedureInParm("pCAST_INITIAL_DEFENCE_FORCE",DbSession.DbType.VARCHAR2,this.txtCastInitialDefenceForce.Text);
			oDbSession.ProcedureInParm("pADD_FELLOW_FORCE",DbSession.DbType.NUMBER,this.txtAddFellowForce.Text);
			oDbSession.ProcedureInParm("pREMOVE_FELLOW_FORCE",DbSession.DbType.NUMBER,this.txtRemoveFellowForce.Text);
			oDbSession.ProcedureInParm("pREMOVE_FELLOW_PARTNER_FORCE",DbSession.DbType.NUMBER,this.txtRemoveFellowPartnerForce.Text);
			oDbSession.ProcedureInParm("pMOSAIC_ERASE_POINT",DbSession.DbType.VARCHAR2,this.txtMosaicErasePoint.Text);
			oDbSession.ProcedureInParm("pMOVIE_TICKET_POINT",DbSession.DbType.VARCHAR2,this.txtMovieTicketPoint.Text);
			oDbSession.ProcedureInParm("pPRESENT_POINT_RATE",DbSession.DbType.VARCHAR2,this.txtPresentPointRate.Text);
			oDbSession.ProcedureInParm("pMAN_TREASURE_DROP_RATE",DbSession.DbType.VARCHAR2,this.txtManTreasureDropRate.Text);
			oDbSession.ProcedureInParm("pCAST_TREASURE_DROP_RATE",DbSession.DbType.VARCHAR2,this.txtCastTreasureDropRate.Text);
			oDbSession.ProcedureInParm("pDEF_FORCE_DECREASE_RATE_MIN",DbSession.DbType.VARCHAR2,this.txtDefFroceDecreaseRateMin.Text);
			oDbSession.ProcedureInParm("pDEF_FORCE_DECREASE_RATE_MAX",DbSession.DbType.VARCHAR2,this.txtDefFroceDecreaseRateMax.Text);
			oDbSession.ProcedureInParm("pPOLICE_COME_RATE",DbSession.DbType.VARCHAR2,this.txtPoliceComeRate.Text);
			oDbSession.ProcedureInParm("pPOLICE_COME_RATE_LV_DIFF",DbSession.DbType.VARCHAR2,this.txtPoliceComeRateLvDefference.Text);
			oDbSession.ProcedureInParm("pPOLICE_LV_DIFFERENCE",DbSession.DbType.VARCHAR2,this.txtPoliceLvDifference.Text);
			oDbSession.ProcedureInParm("pPT_POLICE_COME_RATE",DbSession.DbType.VARCHAR2,this.txtPtPoliceComeRate.Text);
			oDbSession.ProcedureInParm("pPT_POLICE_COME_RATE_LV_DIFF",DbSession.DbType.VARCHAR2,this.txtPtPoliceComeRateLvDefference.Text);
			oDbSession.ProcedureInParm("pPT_POLICE_LV_DIFFERENCE",DbSession.DbType.VARCHAR2,this.txtPtPoliceLvDifference.Text);
			oDbSession.ProcedureInParm("pSUPP_POLICE_COME_RATE",DbSession.DbType.VARCHAR2,this.txtSuppPoliceComeRate.Text);
			oDbSession.ProcedureInParm("pSUPP_POLICE_COME_RATE_LV_DIFF",DbSession.DbType.VARCHAR2,this.txtSuppPoliceComeRateLvDefference.Text);
			oDbSession.ProcedureInParm("pSUPP_POLICE_LV_DIFFERENCE",DbSession.DbType.VARCHAR2,this.txtSuppPoliceLvDifference.Text);
			oDbSession.ProcedureInParm("pGET_TREASURE_FRIENDLY_POINT",DbSession.DbType.VARCHAR2,this.txtGetTreasureFrinedlyPoint.Text);
			oDbSession.ProcedureInParm("pHUG_FRIENDLY_POINT",DbSession.DbType.VARCHAR2,this.txtHugFriendlyPoint.Text);
			oDbSession.ProcedureInParm("pHUG_FRIENDLY_POINT_BY_FELLOW",DbSession.DbType.VARCHAR2,this.txtHugFriendlyPointByFellow.Text);
			oDbSession.ProcedureInParm("pHUG_COOPERATION_POINT",DbSession.DbType.VARCHAR2,this.txtHugCooperationPoint.Text);
			oDbSession.ProcedureInParm("pHUGGED_ADD_EXP_BY_FELLOW",DbSession.DbType.VARCHAR2,this.txtHuggedAddExpByFellow.Text);
			oDbSession.ProcedureInParm("pHUG_BONUS_RATE",DbSession.DbType.VARCHAR2,this.txtHugBonusRate.Text);
			oDbSession.ProcedureInParm("pPRESENT_FRIEND_PT_RATE",DbSession.DbType.NUMBER,this.txtPresentFriendlyPtRate.Text);
			oDbSession.ProcedureInParm("pPRESENT_FRIEND_PT_RATE_FELLOW",DbSession.DbType.NUMBER,this.txtPresentFriendlyPtRateByFellow.Text);
			oDbSession.ProcedureInParm("pRETURN_FRIEND_PT_RATE",DbSession.DbType.NUMBER,this.txtReturnFriendlyPtRate.Text);
			oDbSession.ProcedureInParm("pRETURN_FRIEND_PT_RATE_FELLOW",DbSession.DbType.NUMBER,this.txtReturnFriendlyPtRateByFellow.Text);
			oDbSession.ProcedureInParm("pPRESENT_COOP_PT_RATE",DbSession.DbType.NUMBER,this.txtPresentCoopPtRate.Text);
			oDbSession.ProcedureInParm("pPRESENT_COOP_PT_RATE_FELLOW",DbSession.DbType.NUMBER,this.txtPresentCoopPtRateByFellow.Text);
			oDbSession.ProcedureInParm("pPRESENT_COOP_PT_RATE_NC",DbSession.DbType.NUMBER,this.txtPresentCoopPtRateNotCharge.Text);
			oDbSession.ProcedureInParm("pPRESENT_COOP_PT_RATE_FLW_NC",DbSession.DbType.NUMBER,this.txtPresentCoopPtRateByFellowNotCharge.Text);
			oDbSession.ProcedureInParm("pFORCE_RECOVERY_COOP_POINT",DbSession.DbType.VARCHAR2,this.txtForceRecoveryCoopPoint.Text);
			oDbSession.ProcedureInParm("pFRIENDLY_POINT_DEL_DAYS",DbSession.DbType.VARCHAR2,this.txtFriendlyPointDelDays.Text);
			oDbSession.ProcedureInParm("pGAME_LOG_DEL_DAYS",DbSession.DbType.VARCHAR2,this.txtGameLogDelDays.Text);
			oDbSession.ProcedureInParm("pSAME_USER_ATTACK_LIMIT_COUNT",DbSession.DbType.VARCHAR2,this.txtSameUserAttackLimitCount.Text);
			oDbSession.ProcedureInParm("pPT_BATTLE_ATTACK_LIMIT_COUNT",DbSession.DbType.VARCHAR2,this.txtPtBattleAttackLimitCount.Text);
			oDbSession.ProcedureInParm("pPT_BATTLE_ATTACK_INTERVAL_MIN",DbSession.DbType.VARCHAR2,this.txtPtBattleAttackIntervalMin.Text);
			oDbSession.ProcedureInParm("pDATE_LIMIT_COUNT",DbSession.DbType.VARCHAR2,this.txtDateLimitCount.Text);
			oDbSession.ProcedureInParm("pPART_TIME_JOB_LIMIT_COUNT",DbSession.DbType.VARCHAR2,this.txtPartTimeJobLimitCount.Text);
			oDbSession.ProcedureInParm("pBATTLE_WIN_EXP",DbSession.DbType.VARCHAR2,this.txtBattleWinExp.Text);
			oDbSession.ProcedureInParm("pBATTLE_LOSS_EXP",DbSession.DbType.VARCHAR2,this.txtBattleLossExp.Text);
			oDbSession.ProcedureInParm("pPARTY_BATTLE_WIN_EXP",DbSession.DbType.VARCHAR2,this.txtPartyBattleWinExp.Text);
			oDbSession.ProcedureInParm("pPARTY_BATTLE_LOSS_EXP",DbSession.DbType.VARCHAR2,this.txtPartyBattleLossExp.Text);
			oDbSession.ProcedureInParm("pBATTLE_DECREASE_ENDURANCE",DbSession.DbType.VARCHAR2,this.txtBattleDecreaseEndurance.Text);
			oDbSession.ProcedureInParm("pPSUPPORT_BATTLE_WIN_FRIENDLY",DbSession.DbType.VARCHAR2,this.txtSupportBattleWinFriendly.Text);
			oDbSession.ProcedureInParm("pPSUPPORT_BATTLE_LOSS_FRIENDLY",DbSession.DbType.VARCHAR2,this.txtSupportBattleLossFriendly.Text);
			oDbSession.ProcedureInParm("pPSUPPORT_BATTLE_WIN_COOP",DbSession.DbType.VARCHAR2,this.txtSupportBattleWinCoop.Text);
			oDbSession.ProcedureInParm("pPSUPPORT_BATTLE_LOSS_COOP",DbSession.DbType.VARCHAR2,this.txtSupportBattleLossCoop.Text);
			oDbSession.ProcedureInParm("pPSUPPORT_REQUEST_COUNT",DbSession.DbType.VARCHAR2,this.txtSupportRequestCount.Text);
			oDbSession.ProcedureInParm("pACCEPT_PIC_POINT",DbSession.DbType.VARCHAR2,this.txtAcceptPicPoint.Text);
			oDbSession.ProcedureInParm("pACCEPT_MOVIE_POINT",DbSession.DbType.VARCHAR2,this.txtAcceptMoviePoint.Text);
			oDbSession.ProcedureInParm("pFREE_PRESENT_LIMIT_COUNT",DbSession.DbType.VARCHAR2,this.txtFreePresentLimitCount.Text);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pROWID",DbSession.DbType.VARCHAR2,this.Rowid);
			oDbSession.ProcedureInParm("pDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}
	}
}