﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 美人時計設定
--	Progaram ID		: BeautyClockList
--
--  Creation Date	: 2011.02.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;


public partial class Extension_BeautyClockList:System.Web.UI.Page {
	private string BEAUTY_COLOR_NO_REGIST = "#FFFFFF";
	private string BEAUTY_COLOR_REGIST = "#FFB2B2";
	private string BEAUTY_COLOR_CAST_NOT_NOMAL = "#999999";
	private string BEAUTY_CLOCK_RETURN_URL = HttpUtility.UrlEncode("./BeautyClockList.aspx");
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);

		if (!IsPostBack) {

			lstSeekSiteCd.DataBind();
			if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
				lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}
			
			lstSeekSiteCd.DataSourceID = "";

			pnlInfo.Visible = true;
			
			rptTimeList.DataSource = CreateDataSource();
			rptTimeList.DataBind();
			
		}
	}
	
	protected ICollection CreateDataSource() {
		DataTable dt = new DataTable();
		DataRow dr;

		dt.Columns.Add(new DataColumn("TIME_ROW",typeof(DataTable)));
		
		using(BeautyClock oBeauty = new BeautyClock()){
			DataSet dsBeauty =  oBeauty.GetList(lstSeekSiteCd.SelectedValue);
			int iCount = 0;
			for (int i = 0; i < 60; i++) {
				dr = dt.NewRow();
				
				DataTable dt2 = new DataTable();
				dt2.Columns.Add(new DataColumn("TIME",typeof(string)));
				dt2.Columns.Add(new DataColumn("ASSIGNED_TIME",typeof(int)));
				dt2.Columns.Add(new DataColumn("CELL_COLOR",typeof(string)));
				dt2.Columns.Add(new DataColumn("LINK_ENABLE",typeof(Boolean)));
				
				for (int j = 0; j < 24; j++) {
					int iAssignedTime = j * 60 + i;
					DataRow dr2 = dt2.NewRow();
					dr2["TIME"] = ViCommPrograms.ConvertAssignedToTime(iAssignedTime.ToString());
					dr2["ASSIGNED_TIME"] = iAssignedTime;
					dr2["CELL_COLOR"] = this.BEAUTY_COLOR_NO_REGIST;
					dr2["LINK_ENABLE"] = false;
					if(iCount < dsBeauty.Tables[0].Rows.Count) {
						if (dr2["ASSIGNED_TIME"].ToString().Equals(dsBeauty.Tables[0].Rows[iCount]["ASSIGNED_TIME"].ToString())) {
							if (dsBeauty.Tables[0].Rows[iCount]["NA_FLAG"].ToString().Equals(ViCommConst.FLAG_OFF_STR)) {
								dr2["CELL_COLOR"] = this.BEAUTY_COLOR_REGIST;
							}else{
								dr2["CELL_COLOR"] = this.BEAUTY_COLOR_CAST_NOT_NOMAL;
							}
							dr2["LINK_ENABLE"] = true;
							
							iCount++;
						}
					}
					dt2.Rows.Add(dr2);				
				}
				
				dr[0] = dt2;
				dt.Rows.Add(dr);
			}
			
		}

		DataView dv = new DataView(dt);
		return dv;
	}
	
	protected void btnListSeek_Click(object sender, EventArgs e){

		pnlInfo.Visible = true;
		
		rptTimeList.DataSource = CreateDataSource();
		rptTimeList.DataBind();

	}
	
	protected string GetBeautyClockRegistUrl(object pTime){
		return string.Format("./BeautyClockMainte.aspx?site_cd={0}&time={1}&return={2}",lstSeekSiteCd.SelectedValue,pTime.ToString(),BEAUTY_CLOCK_RETURN_URL);
	}

	protected string GetBeautyClockViewUrl(object pTime) {
		return string.Format("./BeautyClockView.aspx?site_cd={0}&time={1}",lstSeekSiteCd.SelectedValue,pTime.ToString());
	}

	public void lnkTime_Command(object sender,CommandEventArgs e) {
		string sKey = e.CommandArgument.ToString();
		ViewState["SITE_CD"] = lstSeekSiteCd.SelectedValue;
		ViewState["ASSIGNED_TIME"] = sKey;

	}


	protected void btnInit_Click(object sender,EventArgs e) {
		InitBeautyClockDirectrys();
	}

	protected void InitBeautyClockDirectrys() {
		string sSiteCd = "A001";
		string sUserSeq = "237573";
		string sCharNo = ViCommConst.MAIN_CHAR_NO;
		int iAssignedTime = 0;

		string sWebPhisicalDir = string.Empty;
		using (Site oSite = new Site()) {
			oSite.GetValue(sSiteCd.ToString(),"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
		}

		string sFileNm = string.Empty,sPath = string.Empty,sFullPath = string.Empty;

		string sSourceFileNm = "source";
		string sSourceFullPath = ConfigurationManager.AppSettings["MailParseDir"] + "\\Source\\" + sSourceFileNm + ".jpg";

		using (BeautyClock objBeauty = new BeautyClock()) {
			using (CastCharacter oCast = new CastCharacter()) {
				DataSet dsInitUser = oCast.GetOne(sSiteCd,sUserSeq,sCharNo);

				using (DbSession db = new DbSession()) {
					for (int j = 0;j < 24;j++) {
						for (int i = 0;i < 60;i++) {
							sSourceFileNm = string.Format("{0:D2}{1:D2}",j,i);
							sSourceFullPath = ConfigurationManager.AppSettings["MailParseDir"] + "\\Source\\" + sSourceFileNm + ".jpg";
							iAssignedTime = (j * 60) + i;
							decimal dNo = objBeauty.GetPicNo();

							sFileNm = ViCommPrograms.GeneratePicFileNm(dNo);
							sPath = ViCommPrograms.GetBeautyClockPicDir(sWebPhisicalDir,sSiteCd,iAssignedTime.ToString());
							sFullPath = sPath + "\\" + sFileNm + ViCommConst.PIC_FOODER;

							using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,string.Empty,ViCommConst.FILE_UPLOAD_PASSWORD)) {
								if (!System.IO.File.Exists(sFullPath)) {
									System.IO.File.Copy(sSourceFullPath,sFullPath);

									// オリジナル画像をMailToolsディレクトリに配置
									string sInputDir = ConfigurationManager.AppSettings["MailParseDir"] + "\\";
									System.IO.File.Copy(sFullPath,System.IO.Path.Combine(sInputDir,sFileNm + ViCommConst.PIC_FOODER));
									ImageHelper.ConvertMobileImage(sSiteCd,sInputDir,sPath,sFileNm);
								}
							}

							db.PrepareProcedure("BEAUTY_CLOCK_REGIST");
							db.ProcedureInParm("PASSIGNED_TIME",DbSession.DbType.NUMBER,iAssignedTime);
							db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,sSiteCd);
							db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.NUMBER,int.Parse(sUserSeq));
							db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,sCharNo);
							db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.NUMBER,dNo);
							db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
							db.ExecuteProcedure();
						}
					}
				}
			}
		}

	}

}
