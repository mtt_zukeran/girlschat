﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ランキング
--	Progaram ID		: Ranking
--
--  Creation Date	: 2011.04.05
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using iBridCommLib;
using ViComm;
using System.Data;

public partial class Extension_RankingEx : Page {

	private string SummaryType {
		get { return iBridUtil.GetStringValue(this.ViewState["SummaryType"]); }
		set { this.ViewState["SummaryType"] = value; }
	}

	private DataSet RankingCtlData {
		get { return this.ViewState["RankingCtlData"] as DataSet; }
		set { this.ViewState["RankingCtlData"] = value; }
	}

	private string SiteCd {
		get { return iBridUtil.GetStringValue(this.ViewState["SiteCd"]); }
		set { this.ViewState["SiteCd"] = value; }
	}

	private string Seq {
		get { return iBridUtil.GetStringValue(this.ViewState["Seq"]); }
		set { this.ViewState["Seq"] = value; }
	}

	private string recCount = "";

	protected void Page_Load(object sender, EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);

		if (!IsPostBack) {
			this.InitPage();
			this.SiteCd = Request.QueryString["sitecd"];
			this.Seq = Request.QueryString["seq"];
			this.SummaryType = Request.QueryString["summarytype"];

			if (!string.IsNullOrEmpty(this.SiteCd) && !string.IsNullOrEmpty(this.Seq)) {
				this.lstSiteCd.SelectedValue = this.SiteCd;
				this.SetRemarks();
				this.GetList();
			}
		}

		switch (this.SummaryType) {
			case ViCommConst.ExRanking.EX_RANKING_MARKING:
				this.lblPgmTitle.Text = "足あとランキング";
				this.Title = "足あとランキング";
				break;
			case ViCommConst.ExRanking.EX_RANKING_FAVORIT_MAN:
				this.lblPgmTitle.Text = "モテキング";
				this.Title = "モテキング";
				break;
			case ViCommConst.ExRanking.EX_RANKING_FAVORIT_WOMAN:
				this.lblPgmTitle.Text = "モテコン";
				this.Title = "モテコン";
				break;
		}
	}

	protected void btnListSeek_Click(object sender, EventArgs e) {
		this.GetList();
	}

	protected void btnBack_Click(object sender, EventArgs e) {
		this.Response.Redirect(string.Format("~/Extension/RankingCtlMainte.aspx?sitecd={0}&summarytype={1}", this.SiteCd, this.SummaryType));
	}

	protected void dsRankingEx_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void grdRankingEx_RowDataBound(object sender, GridViewRowEventArgs e) {
	}

	private void InitPage() {
		this.lblRemarks.Text = string.Empty;
		this.pnlInfo.Visible = false;
		this.grdRankingEx.PageSize = 300;
		this.grdRankingEx.DataSourceID = string.Empty;

		this.DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void GetList() {
		this.recCount = "0";
		this.grdRankingEx.PageIndex = 0;
		this.grdRankingEx.DataSourceID = "dsRankingEx";
		this.grdRankingEx.DataBind();
		this.pnlInfo.Visible = true;
		this.pnlCount.DataBind();
	}

	protected string GetViewUrl(object pSiteCd, object pLoignId) {
		switch (this.SummaryType) {
			case ViCommConst.ExRanking.EX_RANKING_MARKING:
			case ViCommConst.ExRanking.EX_RANKING_FAVORIT_WOMAN:
				return string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}", pLoignId, "Ranking.aspx");
			case ViCommConst.ExRanking.EX_RANKING_FAVORIT_MAN:
				return string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}", pSiteCd, pLoignId);
			default:
				return string.Empty;
		}
	}

	protected string GetRecCount() {
		return recCount;
	}

	private void SetRemarks() {
		if (this.RankingCtlData == null) {
			using (RankingCtl oRankingCtl = new RankingCtl()) {
				this.RankingCtlData = oRankingCtl.GetList(this.SiteCd, this.SummaryType, this.Seq);
			}
		}

		if (this.RankingCtlData.Tables[0].Rows.Count > 0) {
			DataRow oDataRow = this.RankingCtlData.Tables[0].Rows[0];
			this.lblRemarks.Text = iBridUtil.GetStringValue(oDataRow["REMARKS"]);
			this.lblDate.Text = string.Format("{0:yyyy/MM/dd HH:mm:ss} ～ {1:yyyy/MM/dd HH:mm:ss}", oDataRow["SUMMARY_START_DATE"], oDataRow["SUMMARY_END_DATE"]);
		}
	}
}
