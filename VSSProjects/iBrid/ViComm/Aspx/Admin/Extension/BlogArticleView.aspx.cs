﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ブログ投稿詳細

--	Progaram ID		: BlogArticleView
--
--  Creation Date	: 2011.03.31
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.Collections.Generic;

public partial class Extension_BlogArticleView : System.Web.UI.Page {
	private DataSet BlogArticleData {
		get {
			return this.ViewState["BlogArticleData"] as DataSet;
		}
		set {
			this.ViewState["BlogArticleData"] = value;
		}
	}

	protected void Page_Load(object sender, EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		if (!this.IsPostBack) {
			// チェック済みに変更
			using (BlogArticle oBlogArticle = new BlogArticle()) {
				oBlogArticle.CheckBlogArticleMainte(
					Request.QueryString["sitecd"],
					Request.QueryString["userseq"],
					Request.QueryString["usercharno"],
					Request.QueryString["blogseq"],
					Request.QueryString["blogarticleseq"],
					ViCommConst.FLAG_ON_STR
				);
			}
			this.DataBind();
		}
	}

	protected void btnSetPriority_OnCommand(object sender, CommandEventArgs e) {
		int iDelFlag;
		string sPriority;
		if ((this.dvwBlogArticle.FindControl("rdoPickupON") as CheckBox).Checked) {
			iDelFlag = ViCommConst.FLAG_OFF;
			sPriority = (this.dvwBlogArticle.FindControl("lstPickup") as DropDownList).SelectedValue;
		} else {
			iDelFlag = ViCommConst.FLAG_ON;
			sPriority = e.CommandArgument.ToString();
		}

		if (!string.IsNullOrEmpty(sPriority)) {
			using (DbSession oDbSession = new DbSession()) {
				oDbSession.PrepareProcedure("BLOG_ARTICLE_PICKUP_MAINTE");
				oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.Request.QueryString["sitecd"]);
				oDbSession.ProcedureInParm("pUSER_SEQ", DbSession.DbType.VARCHAR2, this.Request.QueryString["userseq"]);
				oDbSession.ProcedureInParm("pUSER_CHAR_NO", DbSession.DbType.VARCHAR2, this.Request.QueryString["usercharno"]);
				oDbSession.ProcedureInParm("pBLOG_SEQ", DbSession.DbType.VARCHAR2, this.Request.QueryString["blogseq"]);
				oDbSession.ProcedureInParm("pBLOG_ARTICLE_SEQ", DbSession.DbType.VARCHAR2, this.Request.QueryString["blogarticleseq"]);
				oDbSession.ProcedureInParm("pPRIORITY", DbSession.DbType.NUMBER, sPriority);
				oDbSession.ProcedureInParm("pDEL_FLAG", DbSession.DbType.NUMBER, iDelFlag);
				oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
				oDbSession.ExecuteProcedure();
			}
		}
		
		this.DataBind();
	}

	protected void lnkSetPublish_OnCommand(object sender, CommandEventArgs e) {
		int iRowIndex = int.Parse(e.CommandArgument.ToString());

		DataRow oDataRow = this.BlogArticleData.Tables[0].Rows[iRowIndex];
		string sBlogArticleStatus;
		switch (iBridUtil.GetStringValue(oDataRow["BLOG_ARTICLE_STATUS"])) {
			case ViCommConst.BlogArticleStatus.PRIVATE:
				sBlogArticleStatus = ViCommConst.BlogArticleStatus.PUBLIC;
				break;
			case ViCommConst.BlogArticleStatus.PUBLIC:
				sBlogArticleStatus = ViCommConst.BlogArticleStatus.PRIVATE;
				break;
			default:
				sBlogArticleStatus = iBridUtil.GetStringValue(oDataRow["BLOG_ARTICLE_STATUS"]);
				break;
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("BLOG_ARTICLE_STATUS_UPDATE");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, iBridUtil.GetStringValue(oDataRow["SITE_CD"]));
			oDbSession.ProcedureInParm("pUSER_SEQ", DbSession.DbType.NUMBER, iBridUtil.GetStringValue(oDataRow["USER_SEQ"]));
			oDbSession.ProcedureInParm("pUSER_CHAR_NO", DbSession.DbType.VARCHAR2, iBridUtil.GetStringValue(oDataRow["USER_CHAR_NO"]));
			oDbSession.ProcedureInParm("pBLOG_SEQ", DbSession.DbType.NUMBER, iBridUtil.GetStringValue(oDataRow["BLOG_SEQ"]));
			oDbSession.ProcedureInParm("pBLOG_ARTICLE_SEQ", DbSession.DbType.NUMBER, iBridUtil.GetStringValue(oDataRow["BLOG_ARTICLE_SEQ"]));
			oDbSession.ProcedureInParm("pBLOG_ARTICLE_STATUS", DbSession.DbType.VARCHAR2, sBlogArticleStatus);
			oDbSession.ProcedureInParm("pREVISION_NO", DbSession.DbType.NUMBER, iBridUtil.GetStringValue(oDataRow["REVISION_NO"]));
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
		}

		this.DataBind();
	}

	protected void lnkSetDraft_OnCommend(object sender, CommandEventArgs e) {
		int iRowIndex = int.Parse(e.CommandArgument.ToString());
		DataRow oDataRow = this.BlogArticleData.Tables[0].Rows[iRowIndex];

		if (iBridUtil.GetStringValue(oDataRow["BLOG_ARTICLE_STATUS"]).Equals(ViCommConst.BlogArticleStatus.PRIVATE)) {
			using (DbSession oDbSession = new DbSession()) {
				oDbSession.PrepareProcedure("BLOG_ARTICLE_STATUS_UPDATE");
				oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(oDataRow["SITE_CD"]));
				oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.NUMBER,iBridUtil.GetStringValue(oDataRow["USER_SEQ"]));
				oDbSession.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(oDataRow["USER_CHAR_NO"]));
				oDbSession.ProcedureInParm("pBLOG_SEQ",DbSession.DbType.NUMBER,iBridUtil.GetStringValue(oDataRow["BLOG_SEQ"]));
				oDbSession.ProcedureInParm("pBLOG_ARTICLE_SEQ",DbSession.DbType.NUMBER,iBridUtil.GetStringValue(oDataRow["BLOG_ARTICLE_SEQ"]));
				oDbSession.ProcedureInParm("pBLOG_ARTICLE_STATUS",DbSession.DbType.VARCHAR2,ViCommConst.BlogArticleStatus.DRAFT);
				oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,iBridUtil.GetStringValue(oDataRow["REVISION_NO"]));
				oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
				oDbSession.cmd.BindByName = true;
				oDbSession.ExecuteProcedure();
			}

			this.Response.Redirect("~/Extension/BlogArticleList.aspx");
		}
	}

	/// <summary>
	/// 未チェック状態に戻すリンク押下
	/// </summary>
	protected void lnkSetUnCheck_OnCommand(object sender,CommandEventArgs e) {
		int iRowIndex = int.Parse(e.CommandArgument.ToString());
		DataRow oDataRow = this.BlogArticleData.Tables[0].Rows[iRowIndex];

		// 未チェックに戻す
		using (DbSession oDbSession = new DbSession()) {
			using (BlogArticle oBlogArticle = new BlogArticle()) {
				oBlogArticle.CheckBlogArticleMainte(
					iBridUtil.GetStringValue(oDataRow["SITE_CD"]),
					iBridUtil.GetStringValue(oDataRow["USER_SEQ"]),
					iBridUtil.GetStringValue(oDataRow["USER_CHAR_NO"]),
					iBridUtil.GetStringValue(oDataRow["BLOG_SEQ"]),
					iBridUtil.GetStringValue(oDataRow["BLOG_ARTICLE_SEQ"]),
					ViCommConst.FLAG_OFF_STR
				);
			}
		}

		this.Response.Redirect("~/Extension/BlogArticleList.aspx");
	}

	protected void dsBlogArticle_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
		DataSet oBlogArticleDataSet = e.ReturnValue as DataSet;
		if (oBlogArticleDataSet != null && (oBlogArticleDataSet).Tables[0].Rows.Count > 0) {
			this.BlogArticleData = oBlogArticleDataSet;
		}
	}

	protected void lstPickup_DataBound(object sender, EventArgs e) {
		DropDownList oDropDwonList = sender as DropDownList;
		if (oDropDwonList != null) {
			if (oDropDwonList.Items.Count > 0) {
				ListItem oLastListItem = oDropDwonList.Items[oDropDwonList.Items.Count - 1];
				oDropDwonList.Items.Add(new ListItem("最後", (int.Parse(oLastListItem.Value) + 1).ToString()));
			} else {
				oDropDwonList.Items.Add(new ListItem("1番", "1"));
			}
		}
	}

	protected string GetSetPublishLinkText(object pBlogArticleStatus) {
		switch (iBridUtil.GetStringValue(pBlogArticleStatus)) {
			case ViCommConst.BlogArticleStatus.PRIVATE:
				return "[公開する]";
			case ViCommConst.BlogArticleStatus.PUBLIC:
				return "[非公開にする]";
			default:
				return string.Empty;
		}
	}

	protected string GetPointAcquiredMark(object pPointAcquiredFlag) {
		switch (iBridUtil.GetStringValue(pPointAcquiredFlag)) {
			case ViCommConst.FLAG_ON_STR:
				return "ポイント付与済";
			case ViCommConst.FLAG_OFF_STR:
				return "ポイント未付与";
			default:
				return string.Empty;
		}
	}

	protected string GetPriorityMark(object pPriority) {
		if (string.IsNullOrEmpty(iBridUtil.GetStringValue(pPriority))) {
			return "オススメ未設定";
		} else {
			return string.Format("オススメ設定中({0}番)", pPriority);
		}
	}

	protected string GetStatusMark(object pBlogArticleStatus,object pDelFlag) {
		if (iBridUtil.GetStringValue(pDelFlag).Equals(ViCommConst.FLAG_ON_STR)) {
			return "ﾕｰｻﾞｰ削除";
		} else {
			switch (iBridUtil.GetStringValue(pBlogArticleStatus)) {
				case ViCommConst.BlogArticleStatus.PRIVATE:
					return "非公開";
				case ViCommConst.BlogArticleStatus.PUBLIC:
					return "公開中";
				default:
					return string.Empty;
			}
		}
	}

	protected bool GetImageVisible(object pBlogFileType) {
		string sBlogFileType = iBridUtil.GetStringValue(pBlogFileType);

		switch (sBlogFileType) {
			case ViCommConst.BlogFileType.PIC:
				return true;
			default:
				return false;
		}
	}

	protected bool GetMovieVisible(object pBlogFileType) {
		string sBlogFileType = iBridUtil.GetStringValue(pBlogFileType);

		switch (sBlogFileType) {
			case ViCommConst.BlogFileType.MOVIE:
				return true;
			default:
				return false;
		}
	}

	protected bool GetDraftVisible(object pBlogArticleStatus) {
		string sBlogArticleStatus = iBridUtil.GetStringValue(pBlogArticleStatus);

		switch (sBlogArticleStatus) {
			case ViCommConst.BlogArticleStatus.PRIVATE:
				return true;
			case ViCommConst.BlogArticleStatus.PUBLIC:
				return false;
			default:
				return false;
		}
	}

	protected string GetMovieLink(object pSiteCd, object pLoginId, object pObjSeq) {
		return string.Format("..{0}/{1}/operator/{2}/{3}{4}", ViCommConst.WEB_MOVIE_PATH, pSiteCd, pLoginId, iBridUtil.addZero(iBridUtil.GetStringValue(pObjSeq), ViCommConst.OBJECT_NM_LENGTH), ViCommConst.MOVIE_FOODER);
	}

	protected string GetMovieLink1(object pSiteCd, object pLoginId, object pObjSeq) {
		return "http://" + Request.Url.Authority + ConfigurationManager.AppSettings["Root"] + string.Format("{0}/{1}/operator/{2}/{3}{4}", ViCommConst.WEB_MOVIE_PATH, pSiteCd, pLoginId, iBridUtil.addZero(iBridUtil.GetStringValue(pObjSeq), ViCommConst.OBJECT_NM_LENGTH), ViCommConst.MOVIE_FOODER);
	}
}
