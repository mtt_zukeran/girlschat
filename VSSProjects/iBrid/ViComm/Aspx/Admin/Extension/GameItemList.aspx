﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="GameItemList.aspx.cs" Inherits="Extension_GameItemList" Title="アイテム一覧"
    ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="アイテム一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 900px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle2">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
							<td class="tdHeaderStyle2">
								アイテム名(部分一致)
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtGameItemName" runat="server" Width="100px"></asp:TextBox>
							</td>
							<td class="tdHeaderStyle2">
								アイテムSEQ
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtGameItemSeq" runat="server" Width="100px"></asp:TextBox>
							</td>
                        </tr>
						<tr>
							<td class="tdHeaderStyle2">
								入手種別
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstSearchItemGetCd" runat="server" DataSourceID="dsItemGet" DataTextField="CODE_NM"
									DataValueField="CODE" OnDataBound="lst_DataBound" Width="170px">
								</asp:DropDownList>
								戦利品<asp:CheckBox ID="chkSearchPrizeOfWar" runat="server" />
							</td>
							<td class="tdHeaderStyle2">
								アイテムカテゴリ
							</td>
							<td class="tdDataStyle" colspan="3">
								<asp:DropDownList ID="lstSearchGameItemCategory" runat="server" DataSourceID="dsGameItemCategory"
									DataTextField="GAME_ITEM_CATEGORY_NM" DataValueField="GAME_ITEM_CATEGORY_TYPE"
									OnDataBound="lst_DataBound" Width="170px">
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle2">
						      公開フラグ
							</td>
						    <td class="tdDataStyle">
                             <asp:CheckBox ID="chkSearchPublishFlag" runat="server" Checked ="true" />
                            </td>
                            <td class="tdHeaderStyle2">
						      プレゼントフラグ
							</td>
						    <td class="tdDataStyle" colspan="3">
                             <asp:CheckBox ID="chkSearchPresentFlag" runat="server"/>
                            </td>
						</tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton" ValidationGroup="Create"
                    OnClick="btnCreate_Click" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlGameItemInfo">
            <fieldset class="fieldset">
                <legend>[アイテム情報]</legend>
                <table border="0" style="width: 600px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            アイテムID(SEQ)
                        </td>
                        <td class="tdDataStyle">
                            <asp:Label ID="lblGameItemId" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            アイテム名
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtGameItemNm" runat="server" MaxLength="64" Width="200px"></asp:TextBox>
                            <% // 必須チェック %>
                            <asp:RequiredFieldValidator ID="vdrGameItemNm" runat="server" ErrorMessage="アイテム名を入力してください。"
                                ControlToValidate="txtGameItemNm" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                            <ajaxToolkit:ValidatorCalloutExtender ID="vdeGameItemNm" runat="Server" TargetControlID="vdrGameItemNm"
                                HighlightCssClass="validatorCallout" />
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            アイテムカテゴリ
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstGameItemCategory" runat="server" DataSourceID="dsGameItemCategory"
                                DataTextField="GAME_ITEM_CATEGORY_NM" DataValueField="GAME_ITEM_CATEGORY_TYPE"
                                OnDataBound="lst_DataBound" Width="170px">
                            </asp:DropDownList>
                            <% // 必須チェック %>
                            <asp:RequiredFieldValidator ID="vdrGameItemCategory" runat="server" ErrorMessage="アイテムカテゴリを選択してください。"
                                ControlToValidate="lstGameItemCategory" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                            <ajaxToolkit:ValidatorCalloutExtender ID="vdeGameItemCategory" runat="Server"
                                TargetControlID="vdrGameItemCategory" HighlightCssClass="validatorCallout" />
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            攻撃力
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtAttackPower" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
                             <% // 必須チェック %>
                            <asp:RequiredFieldValidator ID="vdrAttackPower" runat="server" ErrorMessage="攻撃力を入力してください。"
                                ControlToValidate="txtAttackPower" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                            <ajaxToolkit:ValidatorCalloutExtender ID="vceAttackPower" runat="Server"
                                TargetControlID="vdrAttackPower" HighlightCssClass="validatorCallout" />
                       </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            防御力
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtDefencePower" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
                             <% // 必須チェック %>
                            <asp:RequiredFieldValidator ID="vdrDefencePower" runat="server" ErrorMessage="防御力を入力してください。"
                                ControlToValidate="txtDefencePower" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                            <ajaxToolkit:ValidatorCalloutExtender ID="vceDefencePower" runat="Server"
                                TargetControlID="vdrDefencePower" HighlightCssClass="validatorCallout" />
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            耐久力
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtEndurance" runat="server" MaxLength="4" Width="80px"></asp:TextBox>
                              <% // 必須チェック %>
                            <asp:RequiredFieldValidator ID="vdrEndurance" runat="server" ErrorMessage="耐久力を入力してください。"
                                ControlToValidate="txtEndurance" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                            <ajaxToolkit:ValidatorCalloutExtender ID="vceEndurance" runat="Server"
                                TargetControlID="vdrEndurance" HighlightCssClass="validatorCallout" />
                       </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            入手種別
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstItemGetCd" runat="server" DataSourceID="dsItemGet" DataTextField="CODE_NM"
                                DataValueField="CODE" OnDataBound="lst_DataBound" Width="170px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            価格
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtPrice" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
                             <% // 必須チェック %>
                            <asp:RequiredFieldValidator ID="vdrPrice" runat="server" ErrorMessage="価格を入力してください。"
                                ControlToValidate="txtPrice" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                            <ajaxToolkit:ValidatorCalloutExtender ID="vcePrice" runat="Server"
                                TargetControlID="vdrPrice" HighlightCssClass="validatorCallout" />
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            説明文
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtDescription" runat="server" MaxLength="30" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            公開フラグ
                        </td>
                        <td class="tdDataStyle">
                            <asp:CheckBox ID="chkPublishFlag" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            プレゼントフラグ
                        </td>
                        <td class="tdDataStyle">
                            <asp:CheckBox ID="chkPresentFlag" runat="server" AutoPostBack="true" OnCheckedChanged="chkPresentFlag_CheckedChanged" />
                            <asp:CustomValidator ID="vdcPresentFlag" runat="server" ErrorMessage="" OnServerValidate="vdcPresentFlag_ServerValidate"
                                ValidationGroup="Update"></asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            プレゼントアイテム
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstGameItemCategoryPresent" runat="server" DataSourceID="dsGameItemCategory"
                                OnDataBound="lst_DataBound" OnSelectedIndexChanged="lstGameItemCategory_IndexChanged"
                                DataTextField="GAME_ITEM_CATEGORY_NM" DataValueField="GAME_ITEM_CATEGORY_TYPE"
                                Width="170px" AutoPostBack="True">
                            </asp:DropDownList>
                            <asp:DropDownList ID="lstGameItemPresent" runat="server" DataSourceID="dsGameItemPresent"
                                DataTextField="GAME_ITEM_NM" OnDataBound="lst_DataBound" DataValueField="GAME_ITEM_SEQ"
                                Width="170px">
                            </asp:DropDownList>
                            <% // 必須チェック %>
                            <asp:CustomValidator ID="vdcGameItemPresent" runat="server" ErrorMessage="" OnServerValidate="vdcGameItemPresent_ServerValidate"
                                ValidationGroup="Update"></asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            販売開始ステージ
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstStageSeq" runat="server" DataSourceID="dsStage" OnDataBound="lst_DataBound"
                                ValidationGroup="Update" DataTextField="STAGE_DISPLAY" DataValueField="STAGE_SEQ"
                                Width="170px">
                            </asp:DropDownList>
                            <% // 必須チェック %>
                            <asp:RequiredFieldValidator ID="vdrStageSeq" runat="server" ErrorMessage="ステージを選択してください。"
                                ControlToValidate="lstStageSeq" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                TargetControlID="vdrStageSeq" HighlightCssClass="validatorCallout" />
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            備考
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtRemarks" runat="server" Height="30px" MaxLength="1024" Rows="3"
                                TextMode="MultiLine" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="pnlCommand" runat="server">
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Update"
                        OnClick="btnUpdate_Click" />
                    <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" ValidationGroup="Delete"
                        OnClick="btnDelete_Click" />
                    <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" CausesValidation="False"
                        Visible="true" OnClick="btnCancel_Click" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[アイテム一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdGameItem.PageIndex + 1 %>
                        of
                        <%= grdGameItem.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="600px">
                    <asp:GridView ID="grdGameItem" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        PageSize="30" EnableSortingAndPagingCallbacks="false" DataSourceID="dsGameItem"
                        SkinID="GridViewColor" AllowSorting="true" DataKeyNames="SITE_CD,GAME_ITEM_SEQ">
                        <Columns>
                            <asp:TemplateField HeaderText="ｱｲﾃﾑ名<br>ｱｲﾃﾑSEQ">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%# Eval("GAME_ITEM_SEQ") %>'
                                        Text='<%# Eval("GAME_ITEM_NM") %>' OnCommand="lnkEdit_Command"></asp:LinkButton><br />
                                    <asp:Label ID="lblGameItemSeq" runat="server" Text='<%# Eval("GAME_ITEM_SEQ") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="入手">
                                <ItemTemplate>
                                    <asp:Label ID="lblType" runat="server" Text='<%# string.IsNullOrEmpty(Eval("GAME_ITEM_GET_CD","{0}")) ? "戦利品" : "購入品" %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="ATTACK_POWER" DataFormatString="{0:N0}" HeaderText="攻撃力">
                                <ItemStyle HorizontalAlign="right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DEFENCE_POWER" DataFormatString="{0:N0}" HeaderText="防御力">
                                <ItemStyle HorizontalAlign="right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ENDURANCE" DataFormatString="{0:N0}" HeaderText="耐久力">
                                <ItemStyle HorizontalAlign="right" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="課金">
                                <ItemTemplate>
                                    <asp:Label ID="lblBuyType" runat="server" Text='<%# string.IsNullOrEmpty(Eval("GAME_ITEM_GET_CD","{0}")) ? "" : "1".Equals(Eval("GAME_ITEM_GET_CD","{0}")) ? "課金" : "無課金" %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="DESCRIPTION" HeaderText="説明">
                                <ItemStyle HorizontalAlign="left" Width="250px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="PRICE" DataFormatString="{0:N0}" HeaderText="価格">
                                <ItemStyle HorizontalAlign="right" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="イメージ">
                                <ItemTemplate>
                                    <asp:Image ID="imgBoss" runat="server" Width="80px" ImageUrl='<%# GetImagePath(Eval("GAME_ITEM_SEQ")) %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="STAGE_NM" HeaderText="販売開始ｽﾃｰｼﾞ">
                                <ItemStyle HorizontalAlign="left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="公開">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkSetPublish" runat="server" Text='<%# GetPublishFlagMark(Eval("PUBLISH_FLAG")) %>'
                                        OnCommand="lnkSetPublish_Command" CommandArgument='<%# Container.DataItemIndex %>'>
                                    </asp:LinkButton>
                                    <asp:HiddenField ID="hdnPublishFlag" runat="server" Value='<%# Eval("PUBLISH_FLAG") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="公開日時">
                                <ItemTemplate>
                                    <asp:Label ID="lblPublishDate" runat="server" Text='<%# Eval("PUBLISH_DATE", "{0:yyyy/MM/dd}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="作成日時">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("CREATE_DATE", "{0:yyyy/MM/dd}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
            <br />
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsGameItem" runat="server" SelectMethod="GetPageCollection"
        ConvertNullToDBNull="true" SortParameterName="" TypeName="GameItem" SelectCountMethod="GetPageCount"
        EnablePaging="True" OnSelected="dsGameItem_Selected" OnSelecting="dsGameItem_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
            <asp:Parameter Name="pGameItemName" Type="String" DefaultValue="" />
            <asp:Parameter Name="pGameItemSeq" Type="String" DefaultValue="" />
            <asp:Parameter Name="pGameItemGetCd" Type="String" DefaultValue="" />
            <asp:Parameter Name="pGameItemCategoryType" Type="String" DefaultValue="" />
            <asp:Parameter Name="pPublishFlag" Type="String" DefaultValue="" />
            <asp:Parameter Name="pPresentFlag" Type="String" DefaultValue="" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItemPresent" runat="server" SelectMethod="GetListNotRegisterdPresentItemByCategoryType"
        TypeName="GameItem" OnSelecting="dsGameItemPresent_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:Parameter Name="pSexCd" Type="String" />
            <asp:Parameter Name="pGameItemCategoryType" Type="String" DefaultValue="" />
            <asp:Parameter Name="pPresentFlag" Type="String" DefaultValue="" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItemCategory" runat="server" SelectMethod="GetList"
        TypeName="GameItemCategory">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsItemGet" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="A2" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsStage" runat="server" SelectMethod="GetList" TypeName="Stage">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtAttackPower" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtDefencePower" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtEndurance" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtPrice" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="アイテム情報を更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete"
        ConfirmText="アイテム情報を削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>
