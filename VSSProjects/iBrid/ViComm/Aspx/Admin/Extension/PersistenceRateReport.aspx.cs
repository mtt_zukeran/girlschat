﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ポイント明細レポート

--	Progaram ID		: PersistenceRateReport
--
--  Creation Date	: 2011.08.11
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using iBridCommLib;
using System.Drawing;
using ViComm;
using ViComm.Extension.Pwild;

public partial class Extension_PersistenceRateReport : System.Web.UI.Page {

	protected string recCount {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RecCount"]);
		}
		private set {
			this.ViewState["RecCount"] = value;
		}
	}

	protected string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		private set {
			this.ViewState["SexCd"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		if (!this.IsPostBack) {
			this.SexCd = this.Request.QueryString["sexcd"];
			if (ViCommConst.MAN.Equals(this.SexCd)) {
				this.lblPgmTitle.Text = "男性" + this.lblPgmTitle.Text;
			} else {
				this.lblPgmTitle.Text = "女性" + this.lblPgmTitle.Text;
			}
			this.Title = this.lblPgmTitle.Text;

			this.InitPage();

			this.GetList();
			this.pnlInfo.Visible = true;
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		this.GetList();
		this.pnlInfo.Visible = true;
	}

	protected void dsPersistenceRateReport_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsPersistenceRateReport_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pReportDayFrom"] = string.Format("{0}/{1}/{2}",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue);
		e.InputParameters["pReportDayTo"] = string.Format("{0}/{1}/{2}",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,this.lstToDD.SelectedValue);
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	private void InitPage() {
		this.pnlKey.DataBind();
		this.pnlInfo.Visible = false;
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		SysPrograms.SetupFromToDay(this.lstFromYYYY,this.lstFromMM,this.lstFromDD,this.lstToYYYY,this.lstToMM,this.lstToDD,false);
		this.lstFromYYYY.SelectedValue = DateTime.Today.ToString("yyyy");
		this.lstFromMM.SelectedValue = DateTime.Today.ToString("MM");
		this.lstFromDD.SelectedValue = DateTime.Today.ToString("dd");
		this.lstToYYYY.SelectedValue = DateTime.Today.ToString("yyyy");
		this.lstToMM.SelectedValue = DateTime.Today.ToString("MM");
		this.lstToDD.SelectedValue = DateTime.Today.ToString("dd");
	}

	private void GetList() {
		this.grdPersistenceRateReport.DataBind();
		this.pnlCount.DataBind();
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	protected void btnCSV_Click(object sender,EventArgs e) {

		if (grdPersistenceRateReport.Rows.Count == 0) {
			return;
		}

		DataTable oCsvData;
		using (PersistenceRateReport oPersistenceRateReport = new PersistenceRateReport()) {
			using (DataSet oDataSet = oPersistenceRateReport.GetPageCollection(
				this.lstSeekSiteCd.SelectedValue,
				this.SexCd,
				string.Format("{0}/{1}/{2}",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue),
				string.Format("{0}/{1}/{2}",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,this.lstToDD.SelectedValue),
				0,
				int.Parse(this.recCount))) {

				if (oDataSet.Tables[0].Rows.Count > 0) {
					oCsvData = oDataSet.Tables[0];
				} else {
					return;
				}
			}
		}

		//ヘッダ作成
		string sHeader = "年月日,入会数,1日後継続率,3日後継続率,7日後継続率,14日後継続率,30日後継続率,60日後継続率";

		//CSV出力 
		Response.ContentType = "application/download";
		Response.AppendHeader("Content-Disposition","attachment;filename=PersistenceRateReport.csv");
		Response.ContentEncoding = System.Text.Encoding.GetEncoding("Shift_JIS");
		
		string sAfter1Day = null;
		string sAfter3Day = null;
		string sAfter7Day = null;
		string sAfter14Day = null;
		string sAfter30Day = null;
		string sAfter60Day = null;
		
		Response.Write(sHeader + "\r\n");
		foreach (DataRow oCsvRow in oCsvData.Rows) {
			sAfter1Day = oCsvRow["AFTER_1DAY"].ToString() ;
			sAfter3Day = oCsvRow["AFTER_3DAY"].ToString();
			sAfter7Day = oCsvRow["AFTER_7DAY"].ToString();
			sAfter14Day = oCsvRow["AFTER_14DAY"].ToString();
			sAfter30Day = oCsvRow["AFTER_30DAY"].ToString();
			sAfter60Day = oCsvRow["AFTER_60DAY"].ToString();

			if (oCsvRow["AFTER_1DAY"].ToString() == "-1") {
				sAfter1Day = "-";
			} else {
				sAfter1Day = sAfter1Day + "%";
			}
			if (oCsvRow["AFTER_3DAY"].ToString() == "-1") {
				sAfter3Day = "-";
			} else {
				sAfter3Day = sAfter3Day + "%";
			}
			if (oCsvRow["AFTER_7DAY"].ToString() == "-1") {
				sAfter7Day = "-";
			} else {
				sAfter7Day = sAfter7Day + "%";
			}
			if (oCsvRow["AFTER_14DAY"].ToString() == "-1") {
				sAfter14Day = "-";
			} else {
				sAfter14Day = sAfter14Day + "%";
			}
			if (oCsvRow["AFTER_30DAY"].ToString() == "-1") {
				sAfter30Day = "-";
			} else {
				sAfter30Day = sAfter30Day + "%";
			}
			if (oCsvRow["AFTER_60DAY"].ToString() == "-1") {
				sAfter60Day = "-";
			} else {
				sAfter60Day = sAfter60Day + "%";
			}
		
			string sData =
				SetCsvString(oCsvRow["REGIST_DAY"].ToString()) + "," +
				SetCsvString(oCsvRow["REGIST_COUNT"].ToString()) + "," +
				SetCsvString(sAfter1Day) + "," +
				SetCsvString(sAfter3Day) + "," +
				SetCsvString(sAfter7Day) + "," +
				SetCsvString(sAfter14Day) + "," +
				SetCsvString(sAfter30Day) + "," +
				SetCsvString(sAfter60Day);

			Response.Write(sData + "\r\n");
		}
		Response.End();
	}

	private string SetCsvString(string pData) {
		if (pData.IndexOf('"') > -1 ||
			pData.IndexOf(',') > -1 ||
			pData.IndexOf('\r') > -1 ||
			pData.IndexOf('\n') > -1 ||
			pData.StartsWith(" ") || pData.StartsWith("\t") ||
			pData.EndsWith(" ") || pData.EndsWith("\t")) {
			if (pData.IndexOf('"') > -1) {
				//"を""とする
				pData = pData.Replace("\"","\"\"");
			}
			if (pData.IndexOf('\r') > -1 || pData.IndexOf('\n') > -1) {
				//改行しない 
				pData = pData.Replace("\r","");
				pData = pData.Replace("\n","");
			}
			pData = "\"" + pData + "\"";
		}
		return pData;
	}
}