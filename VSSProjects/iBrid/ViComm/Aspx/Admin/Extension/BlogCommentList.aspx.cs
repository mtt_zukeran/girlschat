﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ブログコメント

--	Progaram ID		: BlogCommentList
--
--  Creation Date	: 2012.12.29
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.Collections.Generic;

public partial class Extension_BlogCommentList:System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string LoginId {
		get {
			return iBridUtil.GetStringValue(this.ViewState["LoginId"]);
		}
		set {
			this.ViewState["LoginId"] = value;
		}
	}
	
	private string BlogArticleSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["blog_article_seq"]);
		}
		set {
			this.ViewState["blog_article_seq"] = value;
		}
	}

	private string SortExpression {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortExpression"]);
		}
		set {
			this.ViewState["SortExpression"] = value;
		}
	}

	private string SortDirect {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortDirect"]);
		}
		set {
			this.ViewState["SortDirect"] = value;
		}
	}

	private DataSet BlogCommentData {
		get {
			return this.ViewState["BlogCommentData"] as DataSet;
		}
		set {
			this.ViewState["BlogCommentData"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();

			this.pnlInfo.Visible = true;
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.SortDirect = string.Empty;
		this.SortExpression = string.Empty;

		this.pnlInfo.Visible = true;
		this.GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void grdBlogComment_Sorting(object sender,GridViewSortEventArgs e) {
		if (this.SortExpression.Equals(e.SortExpression)) {
			if (this.SortDirect.Equals("ASC")) {
				this.SortDirect = "DESC";
			} else if (this.SortDirect.Equals("DESC")) {
				this.SortDirect = "ASC";
			}
		} else {
			this.SortDirect = "ASC";
		}

		this.SortExpression = e.SortExpression;

		e.Cancel = true;
		this.GetList();
	}

	protected void dsBlogComment_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		DataSet oBlogCommentDataSet = e.ReturnValue as DataSet;
		if (oBlogCommentDataSet != null && (oBlogCommentDataSet).Tables[0].Rows.Count > 0) {
			this.BlogCommentData = oBlogCommentDataSet;
		}
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsBlogComment_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		BlogComment.SearchCondition oSearchCondition = new BlogComment.SearchCondition();
		oSearchCondition.Keyword = this.txtKeyword.Text.Trim();
		oSearchCondition.LoginId = this.txtLoginId.Text.Trim();
		
		oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
		oSearchCondition.BlogArticleSeq = this.BlogArticleSeq;
		e.InputParameters[0] = oSearchCondition;
	}

	private void InitPage() {

		this.recCount = "0";
		this.txtLoginId.Text = string.Empty;
		this.txtKeyword.Text = string.Empty;
		this.grdBlogComment.DataSourceID = string.Empty;

		this.pnlInfo.Visible = false;
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		if (!this.IsPostBack) {
			this.SiteCd = this.Request.QueryString["sitecd"];
			this.LoginId = this.Request.QueryString["loginid"];
			this.BlogArticleSeq = this.Request.QueryString["blog_article_seq"];

			if (!string.IsNullOrEmpty(this.SiteCd) && !string.IsNullOrEmpty(this.LoginId)) {
				this.lstSiteCd.SelectedValue = this.SiteCd;
				this.txtLoginId.Text = this.LoginId;

				this.pnlInfo.Visible = true;
				this.GetList();
			}
		}

		this.SortDirect = string.Empty;
		this.SortExpression = string.Empty;
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.grdBlogComment.PageIndex = 0;
		this.grdBlogComment.PageSize = 10;
		this.grdBlogComment.DataSourceID = "dsBlogComment";
		this.grdBlogComment.DataBind();
		this.pnlCount.DataBind();
	}
}
