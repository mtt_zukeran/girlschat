﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="PersistenceRateReport.aspx.cs" Inherits="Extension_PersistenceRateReport"
    Title="継続率レポート" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="継続率レポート"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset>
                <legend>[設定]</legend>
                <table border="0" style="width: 640px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            サイトコード
                        </td>
                        <td class="tdDataStyle" colspan="3">
                            <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="180px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            集計期間
                        </td>
                        <td class="tdDataStyle" colspan="3">
                            <asp:DropDownList ID="lstFromYYYY" runat="server" Width="54px">
                            </asp:DropDownList>年
                            <asp:DropDownList ID="lstFromMM" runat="server" Width="43px">
                            </asp:DropDownList>月
                            <asp:DropDownList ID="lstFromDD" runat="server" Width="43px">
                            </asp:DropDownList>日～
                            <asp:DropDownList ID="lstToYYYY" runat="server" Width="54px">
                            </asp:DropDownList>年
                            <asp:DropDownList ID="lstToMM" runat="server" Width="43px">
                            </asp:DropDownList>月
                            <asp:DropDownList ID="lstToDD" runat="server" Width="43px">
                            </asp:DropDownList>日
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    CausesValidation="False" />
                <asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" CausesValidation="False" OnClick="btnCSV_Click" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[継続率レポート]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdPersistenceRateReport.PageIndex + 1%>
                        of
                        <%= grdPersistenceRateReport.PageCount%>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="552px">
                    <asp:GridView ID="grdPersistenceRateReport" runat="server" AutoGenerateColumns="False"
                        DataSourceID="dsPersistenceRateReport" AllowPaging="true" AllowSorting="true"
                        SkinID="GridViewColor">
                        <Columns>
                            <asp:TemplateField HeaderText="年月日">
                                <ItemTemplate>
                                    <asp:Label ID="lblRegistDay" runat="server" Text='<%# Eval("REGIST_DAY") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="入会数">
                                <ItemTemplate>
                                    <asp:Label ID="lblRegistCount" runat="server" Text='<%# Eval("REGIST_COUNT", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="1日後継続率">
                                <ItemTemplate>
                                    <asp:Label ID="lblAfter1Day" runat="server" Text='<%# Eval("AFTER_1DAY", "{0}").Equals("-1") ? "-" : Eval("AFTER_1DAY", "{0:N2}%") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="3日後継続率">
                                <ItemTemplate>
                                    <asp:Label ID="lblAfter3Day" runat="server" Text='<%# Eval("AFTER_3DAY", "{0}").Equals("-1") ? "-" : Eval("AFTER_3DAY", "{0:N2}%") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="7日後継続率">
                                <ItemTemplate>
                                    <asp:Label ID="lblAfter7Day" runat="server" Text='<%# Eval("AFTER_7DAY", "{0}").Equals("-1") ? "-" : Eval("AFTER_7DAY", "{0:N2}%") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="14日後継続率">
                                <ItemTemplate>
                                    <asp:Label ID="lblAfter14Day" runat="server" Text='<%# Eval("AFTER_14DAY", "{0}").Equals("-1") ? "-" : Eval("AFTER_14DAY", "{0:N2}%") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="30日後継続率">
                                <ItemTemplate>
                                    <asp:Label ID="lblAfter30Day" runat="server" Text='<%# Eval("AFTER_30DAY", "{0}").Equals("-1") ? "-" : Eval("AFTER_30DAY", "{0:N2}%") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="60日後継続率">
                                <ItemTemplate>
                                    <asp:Label ID="lblAfter60Day" runat="server" Text='<%# Eval("AFTER_60DAY", "{0}").Equals("-1") ? "-" : Eval("AFTER_60DAY", "{0:N2}%") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsPersistenceRateReport" runat="server" SelectMethod="GetPageCollection"
        SelectCountMethod="GetPageCount" EnablePaging="True" OnSelecting="dsPersistenceRateReport_Selecting"
        TypeName="PersistenceRateReport" OnSelected="dsPersistenceRateReport_Selected">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSeekSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
            <asp:Parameter Name="pReportDayFrom" Type="String" />
            <asp:Parameter Name="pReportDayTo" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
