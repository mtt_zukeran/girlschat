﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 掲示板レス一覧

--	Progaram ID		: BbsResList
--
--  Creation Date	: 2011.04.11
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Text;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web;
using ViComm;
using iBridCommLib;

public partial class Extension_BbsResList : System.Web.UI.Page {
	private string SiteCd {
		get { return iBridUtil.GetStringValue(this.ViewState["SiteCd"]); }
		set { this.ViewState["SiteCd"] = value; }
	}

	private string UserSeq {
		get { return iBridUtil.GetStringValue(this.ViewState["UserSeq"]); }
		set { this.ViewState["UserSeq"] = value; }
	}

	private string UserCharNo {
		get { return iBridUtil.GetStringValue(this.ViewState["UserCharNo"]); }
		set { this.ViewState["UserCharNo"] = value; }
	}

	private string BbsThreadSeq {
		get { return iBridUtil.GetStringValue(this.ViewState["BbsThreadSeq"]); }
		set { this.ViewState["BbsThreadSeq"] = value; }
	}

	private string BbsThreadSubSeq {
		get { return iBridUtil.GetStringValue(this.ViewState["BbsThreadSubSeq"]); }
		set { this.ViewState["BbsThreadSubSeq"] = value; }
	}

	string recCount;

	protected void Page_Load(object sender, EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.SiteCd = this.Request.QueryString["sitecd"];
			this.UserSeq = this.Request.QueryString["userseq"];
			this.UserCharNo = this.Request.QueryString["usercharno"];
			this.BbsThreadSeq = this.Request.QueryString["bbsthreadseq"];
			
			this.InitPage();
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender, EventArgs e) {
		this.GetList();
	}

	protected void btnClear_Click(object sender, EventArgs e) {
		this.InitPage();
	}

	protected void lnkUpdateDelFlag_Command(object sender, CommandEventArgs e) {
		if (!e.CommandName.Equals("DELETE")) {
			return;
		}

		int iRowIndex = int.Parse(e.CommandArgument.ToString());
		IDictionary oDataKeys = this.grdBbsRes.DataKeys[iRowIndex].Values;

		this.BbsThreadSubSeq = iBridUtil.GetStringValue(oDataKeys["BBS_THREAD_SUB_SEQ"]);
		string sCurrentDelFlag = (this.grdBbsRes.Rows[iRowIndex].FindControl("hdnDelFlag") as HiddenField).Value;

		this.UpdateData(ViCommConst.FLAG_OFF_STR.Equals(sCurrentDelFlag) ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF);
	}

	protected void grdBbsRes_RowDataBound(object sender, GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			if (ViCommConst.FLAG_ON_STR.Equals(DataBinder.Eval(e.Row.DataItem, "DEL_FLAG").ToString())) {
				e.Row.BackColor = Color.Gainsboro;
			}
		}
	}

	protected void dsBbsRes_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		this.recCount = "0";
		this.chkPagingOff.Checked = false;
		this.grdBbsRes.DataSourceID = string.Empty;

		this.pnlInfo.Visible = false;
		this.lstSeekSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		this.pnlCount.DataBind();
	}

	protected string GetDelFlagMark(object pDelFlag) {
		return ViCommConst.FLAG_ON_STR.Equals(iBridUtil.GetStringValue(pDelFlag)) ? "削除の取消" : "削除";
	}

	protected string GetOnClientClick(object pDelFlag) {
		return ViCommConst.FLAG_ON_STR.Equals(iBridUtil.GetStringValue(pDelFlag)) ? "return confirm('削除の取消を実行しますか？');" : "return confirm('削除を実行しますか？');";
	}

	protected string GetHandleNmMark(object pHandleNm, object pAnonymousFlag) {
		if (ViCommConst.FLAG_OFF_STR.Equals(iBridUtil.GetStringValue(pAnonymousFlag))) {
			return string.Concat(pHandleNm);
		} else {
			return string.Concat(pHandleNm, " (匿名)");
		}
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.grdBbsRes.PageIndex = 0;
		if (chkPagingOff.Checked) {
			this.grdBbsRes.PageSize = 99999;
		} else {
			this.grdBbsRes.PageSize = 20;
		}

		this.pnlInfo.Visible = true;
		this.grdBbsRes.DataSourceID = "dsBbsRes";
		this.grdBbsRes.DataBind();
		this.pnlCount.DataBind();
	}

	private void UpdateData(int pDelFlag) {
		if (!this.IsValid) {
			return;
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("BBS_RES_DEL_FLAG_UPDATE");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.SiteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ", DbSession.DbType.NUMBER, this.UserSeq);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO", DbSession.DbType.VARCHAR2, this.UserCharNo);
			oDbSession.ProcedureInParm("pBBS_THREAD_SEQ", DbSession.DbType.NUMBER, this.BbsThreadSeq);
			oDbSession.ProcedureInParm("pBBS_THREAD_SUB_SEQ", DbSession.DbType.NUMBER, this.BbsThreadSubSeq);
			oDbSession.ProcedureInParm("pDEL_FLAG", DbSession.DbType.NUMBER, pDelFlag);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.GetList();
	}
}
