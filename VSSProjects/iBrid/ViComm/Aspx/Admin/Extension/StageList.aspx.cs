﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ステージ一覧

--	Progaram ID		: StageList
--
--  Creation Date	: 2011.07.19
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.IO;
using ViComm.Extension.Pwild;

public partial class Extension_StageList : System.Web.UI.Page {
	protected string[] DummyArray = new string[3];
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		set {
			this.ViewState["SexCd"] = value;
		}
	}

	private string StageSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["StageSeq"]);
		}
		set {
			this.ViewState["StageSeq"] = value;
		}
	}

	private string GameItemCategoryType {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemCategoryType"]);
		}
		set {
			this.ViewState["GameItemCategoryType"] = value;
		}
	}

	private string GameItemCategoryType2 {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemCategoryType2"]);
		}
		set {
			this.ViewState["GameItemCategoryType2"] = value;
		}
	}

	private string GameItemCategoryType3 {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemCategoryType3"]);
		}
		set {
			this.ViewState["GameItemCategoryType3"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	private string GameItemGetCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemGetCd"]);
		}
		set {
			this.ViewState["GameItemGetCd"] = value;
		}
	}

	private string GameItemGetCd2 {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemGetCd2"]);
		}
		set {
			this.ViewState["GameItemGetCd2"] = value;
		}
	}

	private string GameItemGetCd3 {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemGetCd3"]);
		}
		set {
			this.ViewState["GameItemGetCd3"] = value;
		}
	}

	private string GameItemPresent {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemPresent"]);
		}
		set {
			this.ViewState["GameItemPresent"] = value;
		}
	}

	private string GameItemPresent2 {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemPresent2"]);
		}
		set {
			this.ViewState["GameItemPresent2"] = value;
		}
	}

	private string GameItemPresent3 {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemPresent3"]);
		}
		set {
			this.ViewState["GameItemPresent3"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();
			this.SexCd = iBridUtil.GetStringValue(this.Request.QueryString["sexcd"]);
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
		this.StageSeq = string.Empty;
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.UpdateData(false);
		this.pnlStageInfo.Visible = false;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		this.UpdateData(true);
		this.pnlStageInfo.Visible = false;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.ClearFileds();
		this.pnlKey.Enabled = true;
		this.pnlStageInfo.Visible = false;
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.StageSeq = string.Empty;
		this.ClearFileds();
		this.pnlKey.Enabled = false;
		this.pnlStageInfo.Visible = true;
	}

	protected void lnkStageNm_Command(object sender,CommandEventArgs e) {

		this.GameItemGetCd = null;
		this.GameItemGetCd2 = null;
		this.GameItemGetCd3 = null;
		this.GameItemPresent = null;
		this.GameItemPresent2 = null;
		this.GameItemPresent3 = null;
		lstItemPresent1.SelectedValue = null;
		lstItemPresent2.SelectedValue = null;
		lstItemPresent3.SelectedValue = null;
		
		if (!this.IsValid) {
			return;
		}
		this.StageSeq = iBridUtil.GetStringValue(e.CommandArgument);
		this.GetData();
		this.pnlKey.Enabled = false;
		this.pnlStageInfo.Visible = true;
	
	}

	protected void dsStage_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	protected void lstLevel_DataBound(object sender,EventArgs e) {
		DropDownList oDropDwonList = sender as DropDownList;
		if (oDropDwonList != null) {
			if (oDropDwonList.Items.Count > 0) {
				ListItem oLastListItem = oDropDwonList.Items[oDropDwonList.Items.Count - 1];
				if (string.IsNullOrEmpty(this.StageSeq)) {
					oDropDwonList.Items.Add(new ListItem("最後",(int.Parse(oLastListItem.Value) + 1).ToString()));
				}
			} else {
				oDropDwonList.Items.Add(new ListItem("最初","1"));
			}
			oDropDwonList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		}
	}

	protected void lstGameItemCategory_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		switch (oDropDownList.ID) {
			case "lstGameItemCategory1":
				this.GameItemCategoryType = oDropDownList.SelectedValue;
				this.lstGameItem1.DataBind();
				break;
			case "lstGameItemCategory2":
				this.GameItemCategoryType2 = oDropDownList.SelectedValue;
				this.lstGameItem2.DataBind();
				break;
			case "lstGameItemCategory3":
				this.GameItemCategoryType3 = oDropDownList.SelectedValue;
				this.lstGameItem3.DataBind();
				break;
		}
	}

	protected void lstItemGetCd_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		switch (oDropDownList.ID) {
			case "lstItemGetCd1":
				this.GameItemGetCd = oDropDownList.SelectedValue;
				this.lstGameItem1.DataBind();
				break;
			case "lstItemGetCd2":
				this.GameItemGetCd2 = oDropDownList.SelectedValue;
				this.lstGameItem2.DataBind();
				break;
			case "lstItemGetCd3":
				this.GameItemGetCd3 = oDropDownList.SelectedValue;
				this.lstGameItem3.DataBind();
				break;
		}
	}

	protected void lstItemPresent_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		switch (oDropDownList.ID) {
			case "lstItemPresent1":
				this.GameItemPresent = oDropDownList.SelectedValue;
				this.lstGameItem1.DataBind();
				break;
			case "lstItemPresent2":
				this.GameItemPresent2 = oDropDownList.SelectedValue;
				this.lstGameItem2.DataBind();
				break;
			case "lstItemPresent3":
				this.GameItemPresent3 = oDropDownList.SelectedValue;
				this.lstGameItem3.DataBind();
				break;
		}
	}

	protected void dsGameItem_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pGameItemCategoryType"] = this.GameItemCategoryType;
		e.InputParameters["pGameItemGetCd"] = this.GameItemGetCd;
		e.InputParameters["pPresentFlag"] = this.GameItemPresent;
	}

	protected void dsGameItem2_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pGameItemCategoryType"] = this.GameItemCategoryType2;
		e.InputParameters["pGameItemGetCd"] = this.GameItemGetCd2;
		e.InputParameters["pPresentFlag"] = this.GameItemPresent2;
	}

	protected void dsGameItem3_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pGameItemCategoryType"] = this.GameItemCategoryType3;
		e.InputParameters["pGameItemGetCd"] = this.GameItemGetCd3;
		e.InputParameters["pPresentFlag"] = this.GameItemPresent3;
	}

	protected void vdcGameItemGet1_ServerValidate(object source,ServerValidateEventArgs args) {
		if (string.IsNullOrEmpty(this.lstGameItem1.SelectedValue)) {
			return;
		}

		if (string.IsNullOrEmpty(this.txtGameItemGetCount1.Text)) {
			args.IsValid = false;
			this.vdcGameItemGet1.ErrorMessage = "個数を入力してください。";
		}
	}

	protected void vdcGameItemGet2_ServerValidate(object source,ServerValidateEventArgs args) {
		if (string.IsNullOrEmpty(this.lstGameItem2.SelectedValue)) {
			return;
		}

		if (string.IsNullOrEmpty(this.txtGameItemGetCount2.Text)) {
			args.IsValid = false;
			this.vdcGameItemGet2.ErrorMessage = "個数を入力してください。";
		}
	}

	protected void vdcGameItemGet3_ServerValidate(object source,ServerValidateEventArgs args) {
		if (string.IsNullOrEmpty(this.lstGameItem3.SelectedValue)) {
			return;
		}

		if (string.IsNullOrEmpty(this.txtGameItemGetCount3.Text)) {
			args.IsValid = false;
			this.vdcGameItemGet3.ErrorMessage = "個数を入力してください。";
		}
	}

	private void InitPage() {
		this.pnlInfo.Visible = true;
		this.pnlStageInfo.Visible = false;

		this.StageSeq = string.Empty;
		this.pnlKey.Enabled = true;
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void ClearFileds() {
		this.lblStageId.Text = string.Empty;
		this.txtBossHp.Text = string.Empty;
		this.txtBossHp1.Text = string.Empty;
		this.txtBossHp2.Text = string.Empty;
		this.txtBossHp3.Text = string.Empty;
		this.txtBossDescription.Text = string.Empty;
		this.txtBossNm.Text = string.Empty;
		this.txtExp.Text = string.Empty;
		this.txtHonor.Text = string.Empty;
		this.txtIncomeMax.Text = string.Empty;
		this.txtIncomeMin.Text = string.Empty;
		this.txtStageNm.Text = string.Empty;
		this.txtGameItemGetCount1.Text = string.Empty;
		this.txtGameItemGetCount2.Text = string.Empty;
		this.txtGameItemGetCount3.Text = string.Empty;
		this.pnlClearItem.DataBind();

		this.lstLevel.DataBind();
		this.lstStageGroup.DataBind();
		this.pnlKey.Enabled = true;
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.StageSeq = string.Empty;

		this.pnlClearItem.DataBind();

		this.grdStage.PageIndex = 0;
		this.grdStage.DataSourceID = "dsStage";
		this.grdStage.DataBind();
		this.pnlCount.DataBind();
	}

	private void GetData() {
		this.ClearFileds();

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("STAGE_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pSTAGE_SEQ",DbSession.DbType.NUMBER,this.StageSeq);
			oDbSession.ProcedureOutParm("pSTAGE_NM",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTAGE_LEVEL",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTAGE_GROUP_TYPE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSEX_CD",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pBOSS_NM",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pBOSS_DESCRIPTION",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pEXP",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pINCOME_MIN",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pINCOME_MAX",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pHONOR",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pBOSS_HP",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pBOSS_HP_FRIEND_1",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pBOSS_HP_FRIEND_2",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pBOSS_HP_FRIEND_3",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutArrayParm("pGET_ITEM_SEQ_ARR",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutArrayParm("pGET_ITEM_COUNT_ARR",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutArrayParm("pGET_ITEM_CATEGORY_TYPE_ARR",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.lblStageId.Text = oDbSession.GetStringValue("pSTAGE_SEQ");
			this.txtStageNm.Text = oDbSession.GetStringValue("pSTAGE_NM");
			this.lstLevel.SelectedValue = oDbSession.GetStringValue("pSTAGE_LEVEL");
			this.lstStageGroup.SelectedValue = oDbSession.GetStringValue("pSTAGE_GROUP_TYPE");
			this.txtIncomeMin.Text = oDbSession.GetStringValue("pINCOME_MIN");
			this.txtIncomeMax.Text = oDbSession.GetStringValue("pINCOME_MAX");
			this.txtHonor.Text = oDbSession.GetStringValue("pHONOR");
			this.txtBossNm.Text = oDbSession.GetStringValue("pBOSS_NM");
			this.txtBossDescription.Text = oDbSession.GetStringValue("pBOSS_DESCRIPTION");
			this.txtBossHp.Text = oDbSession.GetStringValue("pBOSS_HP");
			this.txtBossHp1.Text = oDbSession.GetStringValue("pBOSS_HP_FRIEND_1");
			this.txtBossHp2.Text = oDbSession.GetStringValue("pBOSS_HP_FRIEND_2");
			this.txtBossHp3.Text = oDbSession.GetStringValue("pBOSS_HP_FRIEND_3");
			this.txtExp.Text = oDbSession.GetStringValue("pEXP");
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");

			this.lstGameItemCategory1.SelectedValue = oDbSession.GetArryIntValue("pGET_ITEM_CATEGORY_TYPE_ARR",0);
			this.GameItemCategoryType = this.lstGameItemCategory1.SelectedValue;
			this.lstGameItem1.DataBind();
			this.lstGameItemCategory2.SelectedValue = oDbSession.GetArryIntValue("pGET_ITEM_CATEGORY_TYPE_ARR",1);
			this.GameItemCategoryType2 = this.lstGameItemCategory2.SelectedValue;
			this.lstGameItem2.DataBind();
			this.lstGameItemCategory3.SelectedValue = oDbSession.GetArryIntValue("pGET_ITEM_CATEGORY_TYPE_ARR",2);
			this.GameItemCategoryType3 = this.lstGameItemCategory3.SelectedValue;
			this.lstGameItem3.DataBind();

			this.lstGameItem1.SelectedValue = oDbSession.GetArryIntValue("pGET_ITEM_SEQ_ARR",0);
			this.lstGameItem2.SelectedValue = oDbSession.GetArryIntValue("pGET_ITEM_SEQ_ARR",1);
			this.lstGameItem3.SelectedValue = oDbSession.GetArryIntValue("pGET_ITEM_SEQ_ARR",2);

			this.txtGameItemGetCount1.Text = oDbSession.GetArryIntValue("pGET_ITEM_COUNT_ARR",0);
			this.txtGameItemGetCount2.Text = oDbSession.GetArryIntValue("pGET_ITEM_COUNT_ARR",1);
			this.txtGameItemGetCount3.Text = oDbSession.GetArryIntValue("pGET_ITEM_COUNT_ARR",2);
		}
	}

	protected string GetImagePath(object pStageSeq) {
		return string.Format("../{0}/{1}/{2}/{3}/{4}/{5}{6}",
			"Image",
			this.SiteCd,
			"game",
			ViCommConst.MAN.Equals(this.SexCd) ? ViCommConst.MAN_DIRECTORY.Replace(@"\",string.Empty) : ViCommConst.OPERATOR_DIRECTORY.Replace(@"\",string.Empty),
			ViCommConst.BOSS_IMAGE_DIRECTORY.Replace(@"\",string.Empty),
			iBridUtil.GetStringValue(pStageSeq).PadLeft(15,'0'),
			PwViCommConst.GAME_PIC_FOODER);
	}

	protected bool GetImageVisible(object pBlogFileType) {
		string sBlogFileType = iBridUtil.GetStringValue(pBlogFileType);

		switch (sBlogFileType) {
			case ViCommConst.BlogFileType.PIC:
				return true;
			default:
				return false;
		}
	}

	private void UpdateData(bool pDelFlag) {
		List<string> oGetItemSeqList = new List<string>();
		List<string> oGetItemCountList = new List<string>();
		if (!string.IsNullOrEmpty(this.lstGameItem1.SelectedValue)) {
			oGetItemSeqList.Add(this.lstGameItem1.SelectedValue);
			oGetItemCountList.Add(this.txtGameItemGetCount1.Text);
		}
		if (!string.IsNullOrEmpty(this.lstGameItem2.SelectedValue)) {
			oGetItemSeqList.Add(this.lstGameItem2.SelectedValue);
			oGetItemCountList.Add(this.txtGameItemGetCount2.Text);
		}
		if (!string.IsNullOrEmpty(this.lstGameItem3.SelectedValue)) {
			oGetItemSeqList.Add(this.lstGameItem3.SelectedValue);
			oGetItemCountList.Add(this.txtGameItemGetCount3.Text);
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("STAGE_MAINTE");
			oDbSession.cmd.BindByName = true;
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureBothParm("pSTAGE_SEQ",DbSession.DbType.NUMBER,this.StageSeq);
			oDbSession.ProcedureInParm("pSTAGE_NM",DbSession.DbType.VARCHAR2,this.txtStageNm.Text.TrimEnd());
			oDbSession.ProcedureInParm("pSTAGE_LEVEL",DbSession.DbType.VARCHAR2,this.lstLevel.SelectedValue);
			oDbSession.ProcedureInParm("pSTAGE_GROUP_TYPE",DbSession.DbType.VARCHAR2,this.lstStageGroup.SelectedValue);
			oDbSession.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,this.SexCd);
			oDbSession.ProcedureInParm("pBOSS_NM",DbSession.DbType.VARCHAR2,this.txtBossNm.Text.TrimEnd());
			oDbSession.ProcedureInParm("pBOSS_DESCRIPTION",DbSession.DbType.VARCHAR2,this.txtBossDescription.Text);
			oDbSession.ProcedureInParm("pEXP",DbSession.DbType.VARCHAR2,this.txtExp.Text);
			oDbSession.ProcedureInParm("pINCOME_MIN",DbSession.DbType.VARCHAR2,this.txtIncomeMin.Text);
			oDbSession.ProcedureInParm("pINCOME_MAX",DbSession.DbType.VARCHAR2,this.txtIncomeMax.Text);
			oDbSession.ProcedureInParm("pHONOR",DbSession.DbType.VARCHAR2,this.txtHonor.Text.TrimEnd());
			oDbSession.ProcedureInParm("pBOSS_HP",DbSession.DbType.VARCHAR2,this.txtBossHp.Text);
			oDbSession.ProcedureInParm("pBOSS_HP_FRIEND_1",DbSession.DbType.VARCHAR2,this.txtBossHp1.Text.TrimEnd());
			oDbSession.ProcedureInParm("pBOSS_HP_FRIEND_2",DbSession.DbType.VARCHAR2,this.txtBossHp2.Text.TrimEnd());
			oDbSession.ProcedureInParm("pBOSS_HP_FRIEND_3",DbSession.DbType.VARCHAR2,this.txtBossHp3.Text.TrimEnd());
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInArrayParm("pGET_ITEM_SEQ_ARR",DbSession.DbType.NUMBER,oGetItemSeqList.ToArray());
			oDbSession.ProcedureInArrayParm("pGET_ITEM_COUNT_ARR",DbSession.DbType.NUMBER,oGetItemCountList.ToArray());
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDelFlag ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.ClearFileds();
		this.GetList();
	}

	protected void lstItemGetCd_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		oDropDownList.Items.Insert(3,new ListItem("戦利品","3"));
	}
}
