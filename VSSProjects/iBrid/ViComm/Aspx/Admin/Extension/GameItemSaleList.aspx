﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="GameItemSaleList.aspx.cs" Inherits="Extension_GameItemSaleList" Title="セールアイテム一覧"
    ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="セールアイテム一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 600px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle2">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    Enabled="false" DataValueField="SITE_CD" Width="170px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton" ValidationGroup="Create"
                    OnClick="btnCreate_Click" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" Visible="false" />
                <asp:Button ID="btnBack" runat="server" CausesValidation="False" CssClass="seekbutton"
                    OnClick="btnBack_Click" Text="戻る" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlItemInfo">
            <fieldset class="fieldset">
                <legend>[セールアイテム情報]</legend>
                <table border="0" style="width: 650px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            セールアイテム
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstGameItemCategory" runat="server" DataSourceID="dsGameItemCategory"
                                OnDataBound="lst_DataBound" OnSelectedIndexChanged="lstGameItemCategory_IndexChanged"
                                DataTextField="GAME_ITEM_CATEGORY_NM" DataValueField="GAME_ITEM_CATEGORY_TYPE"
                                Width="170px" AutoPostBack="True">
                            </asp:DropDownList>
                            <asp:DropDownList ID="lstItemGetCd" runat="server" DataSourceID="dsItemGet" DataTextField="CODE_NM"
								DataValueField="CODE" OnDataBound="lstItemGetCd_DataBound" OnSelectedIndexChanged="lstItemGetCd_IndexChanged" AutoPostBack="True">
							</asp:DropDownList>
							<asp:DropDownList ID="lstItemPresent" runat="server" OnSelectedIndexChanged="lstItemPresent_IndexChanged" AutoPostBack="True">
								<asp:ListItem Value=""></asp:ListItem>
								<asp:ListItem Value="0">通常</asp:ListItem>
								<asp:ListItem Value="1">ﾌﾟﾚｾﾞﾝﾄ</asp:ListItem>
							</asp:DropDownList>
                            <asp:DropDownList ID="lstGameItem" runat="server" DataSourceID="dsGameItem" DataTextField="GAME_ITEM_NM"
                                OnDataBound="lst_DataBound" DataValueField="GAME_ITEM_SEQ" Width="170px">
                            </asp:DropDownList>
                            <br />
                            <asp:Label ID="lblErrorMessageItem" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            割引率
                        </td>
                        <td class="tdDataStyle">
                            元値から<asp:TextBox ID="txtDiscountRate" runat="server" MaxLength="2" Width="30px"></asp:TextBox>%引き価格にする。
                            <br />
                            <asp:Label ID="lblErrorMessageRate" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            予告表示</td>
                        <td class="tdDataStyle">
                            <asp:RadioButtonList ID="rdoNoticeDisp" runat="server" RepeatDirection="horizontal">
                                <asp:ListItem Text="有り" Value="1"></asp:ListItem>
                                <asp:ListItem Text="無し" Value="0" Selected="True"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="pnlCommand" runat="server">
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Update"
                        OnClick="btnUpdate_Click" />
                    <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" ValidationGroup="Delete"
                        OnClick="btnDelete_Click" />
                    <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" CausesValidation="False"
                        Visible="true" OnClick="btnCancel_Click" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[セールアイテム一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdGameItemSale.PageIndex + 1 %>
                        of
                        <%= grdGameItemSale.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="600px">
                    <asp:GridView ID="grdGameItemSale" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        PageSize="30" EnableSortingAndPagingCallbacks="false" DataSourceID="dsGameItemSale"
                        SkinID="GridViewColor" AllowSorting="true">
                        <Columns>
                            <asp:TemplateField HeaderText="アイテムSEQ">
                                <ItemTemplate>
                                    <asp:Label ID="lblGameItemSeq" runat="server" Text='<%# Eval("GAME_ITEM_SEQ") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="アイテム名">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%# Container.DataItemIndex %>'
                                        OnCommand="lnkEdit_Command" Text='<%# Eval("GAME_ITEM_NM") %>'></asp:LinkButton>
                                    <asp:HiddenField ID="hdnSaleScheduleSeq" runat="server" Value='<%# Eval("SALE_SCHEDULE_SEQ") %>' />
                                    <asp:HiddenField ID="hdnGameItemSeq" runat="server" Value='<%# Eval("GAME_ITEM_SEQ") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="DISCOUNT_RATE" HeaderText="割引率(%)" DataFormatString="{0:N0}">
                                <ItemStyle HorizontalAlign="right" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="予告表示">
                                <ItemTemplate>
                                    <asp:Label ID="lblNoticeDispFlag" runat="server" Text='<%# GetNoticeDispFlagMark(Eval("NOTICE_DISP_FLAG")) %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
            <br />
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsGameItemSale" runat="server" ConvertNullToDBNull="false"
        EnablePaging="True" OnSelected="dsGameItemSale_Selected" SelectCountMethod="GetPageCount"
        SelectMethod="GetPageCollection" SortParameterName="" TypeName="GameItemSale">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:QueryStringParameter Name="pSaleScheduleSeq" QueryStringField="salescheduleseq"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItemCategory" runat="server" SelectMethod="GetList"
        TypeName="GameItemCategory">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItem" runat="server" SelectMethod="GetListByCategoryType"
        TypeName="GameItem" OnSelecting="dsGameItem_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
            <asp:Parameter Name="pGameItemCategoryType" Type="String" DefaultValue="" />
            <asp:Parameter Name="pGameItemGetCd" Type="String" DefaultValue="" />
            <asp:Parameter Name="pPresentFlag" Type="String" DefaultValue="" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsItemGet" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="A2" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtDiscountRate">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="セールアイテム情報を更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete"
        ConfirmText="セールアイテム情報を削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>
