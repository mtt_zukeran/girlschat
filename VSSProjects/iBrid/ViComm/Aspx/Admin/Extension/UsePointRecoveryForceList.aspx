﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="UsePointRecoveryForceList.aspx.cs" Inherits="Extension_UsePointRecoveryForceList"
    Title="ポイント使用部隊回復設定" ValidateRequest="false" %>

<%@ Import Namespace="System.Data" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="ポイント使用部隊回復設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlMainte">
            <fieldset>
                <legend>[設定]</legend>
                <asp:Panel runat="server" ID="pnlDtl">
                    <fieldset class="fieldset-inner">
                        <legend>[サイト内容]</legend>
                        <table border="0" style="width: 400px;" class="tableStyle">
                            <tr>
                                <td class="tdHeaderStyle">
                                    部隊選択
                                </td>
                                <td class="tdDataStyle">
                                    <asp:CheckBoxList ID="chkRecoveryForce" runat="server">
                                        <asp:ListItem Text="ナンパ部隊" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="攻撃部隊" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="防御部隊" Value="2"></asp:ListItem>
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    使用ポイント
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtRecoveryForcePerPoint" runat="server" MaxLength="3" Width="50px"></asp:TextBox>Pt
                                    <asp:RequiredFieldValidator ID="vdrRecoveryForcePerPoint" runat="server" ControlToValidate="txtRecoveryForcePerPoint"
                                        ErrorMessage="使用ポイントを入力して下さい" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdrRecoveryForcePerPoint">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:RegularExpressionValidator ID="vdeRecoveryForcePerPoint" runat="server" ControlToValidate="txtRecoveryForcePerPoint"
                                        ErrorMessage="使用ポイントを正しく入力して下さい" ValidationExpression="0|[1-9]\d{0,2}" ValidationGroup="Update">*</asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdeRecoveryForcePerPoint">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle2">
                                    回復部隊数
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtUsePointRecoveryForce" runat="server" MaxLength="3" Width="50px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vdrUsePointRecoveryForce" runat="server" ControlToValidate="txtUsePointRecoveryForce"
                                        ErrorMessage="回復部隊数を入力して下さい" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdrUsePointRecoveryForce">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:RegularExpressionValidator ID="vdeUsePointRecoveryForce" runat="server" ControlToValidate="txtUsePointRecoveryForce"
                                        ErrorMessage="回復部隊数を正しく入力して下さい" ValidationExpression="[1-9]\d{0,2}" ValidationGroup="Update">*</asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdeUsePointRecoveryForce">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                </td>
                            </tr>
                        </table>
                        <asp:Button ID="btnUpdate" runat="server" CausesValidation="True" ValidationGroup="Update"
                            CssClass="seekbutton" OnClick="btnUpdate_Click" Text="更新" OnClientClick="return confirm('更新を行いますか？');" />
                        <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click"
                            CausesValidation="False" />
                    </fieldset>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel ID="pnlList" runat="server" ScrollBars="auto">
            <fieldset>
                <legend>[サイト一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdSocialGame.PageIndex + 1 %>
                        of
                        <%= grdSocialGame.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server">
                    <asp:GridView ID="grdSocialGame" runat="server" DataSourceID="dsSocialGame" AutoGenerateColumns="False"
                        AllowPaging="true" AllowSorting="true" SkinID="GridViewColor">
                        <Columns>
                            <asp:TemplateField HeaderText="サイトコード">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkSiteCd" runat="server" Text='<%# Eval("SITE_CD") %>' CommandArgument='<%# string.Format("{0},{1},{2},{3}",Eval("SITE_CD"),Eval("RECOVERY_FORCE_PER_POINT"),Eval("USE_POINT_RECOVERY_FORCE"),Eval("RECOVERY_FORCE_MASK")) %>'
                                        OnCommand="lnkSiteCd_Command" CausesValidation="False">
                                    </asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}"
                                HtmlEncode="False">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsSocialGame" runat="server" SelectMethod="GetPageCollection"
        SelectCountMethod="GetPageCount" EnablePaging="true" OnSelected="dsSocialGame_Selected"
        TypeName="SocialGame"></asp:ObjectDataSource>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtRecoveryForcePerPoint" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtUsePointRecoveryForce" />
</asp:Content>
