﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 日別サービスレポート

--	Progaram ID		: DailyServiceReport
--
--  Creation Date	: 2011.08.10
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using iBridCommLib;
using System.Drawing;
using ViComm;

public partial class Extension_DailyServiceReport : System.Web.UI.Page {
	private int iPageViewCountSum = 0;
	private int iPageViewUniqueCountSum = 0;
	private int iRegistCountSum = 0;
	private int iWithdrawalCountSum = 0;
	private int iUsedPointSum = 0;
	private int iUsedPointCountSum = 0;

	protected string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		private set {
			this.ViewState["SexCd"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		if (!this.IsPostBack) {
			this.SexCd = this.Request.QueryString["sexcd"];
			if (ViCommConst.MAN.Equals(this.SexCd)) {
				this.lblPgmTitle.Text = "男性" + this.lblPgmTitle.Text;
			} else {
				this.lblPgmTitle.Text = "女性" + this.lblPgmTitle.Text;
			}
			this.Title = this.lblPgmTitle.Text;

			this.InitPage();

			this.GetList();
			this.pnlInfo.Visible = true;
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		this.GetList();
		this.pnlInfo.Visible = true;
	}

	protected void dsDailyServiceReport_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pReportMonth"] = string.Format("{0}/{1}",this.lstYYYY.SelectedValue,this.lstMM.SelectedValue);
	}

	protected void grdDailyServiceReport_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			iPageViewCountSum += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PAGE_VIEW_COUNT"));
			iPageViewUniqueCountSum += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PAGE_VIEW_UNIQUE_COUNT"));
			iRegistCountSum += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"REGIST_COUNT"));
			iWithdrawalCountSum += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"WITHDRAWAL_COUNT"));
			iUsedPointSum += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"USED_POINT"));
			iUsedPointCountSum += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"USED_POINT_COUNT"));

		} else if (e.Row.RowType == DataControlRowType.Footer) {
			Label lblPageViewCountSum = e.Row.FindControl("lblPageViewCountSum") as Label;
			Label lblPageViewUniqueCountSum = e.Row.FindControl("lblPageViewUniqueCountSum") as Label;
			Label lblRegistCountSum = e.Row.FindControl("lblRegistCountSum") as Label;
			Label lblWithdrawalCountSum = e.Row.FindControl("lblWithdrawalCountSum") as Label;
			Label lblUsedPointSum = e.Row.FindControl("lblUsedPointSum") as Label;
			Label lblUsedPointCountSum = e.Row.FindControl("lblUsedPointCountSum") as Label;

			lblPageViewCountSum.Text = iPageViewCountSum.ToString("N0");
			lblPageViewUniqueCountSum.Text = iPageViewUniqueCountSum.ToString("N0");
			lblRegistCountSum.Text = iRegistCountSum.ToString("N0");
			lblWithdrawalCountSum.Text = iWithdrawalCountSum.ToString("N0");
			lblUsedPointSum.Text = string.Concat(iUsedPointSum.ToString("N0"),"Pt");
			lblUsedPointCountSum.Text = iUsedPointCountSum.ToString("N0");
		}
	}

	private void InitPage() {
		this.grdDailyServiceReport.DataSourceID = string.Empty;
		this.pnlKey.DataBind();
		this.pnlInfo.Visible = false;
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		SysPrograms.SetupDay(lstYYYY,lstMM,new DropDownList(),false);
		this.lstYYYY.SelectedValue = DateTime.Today.ToString("yyyy");
		this.lstMM.SelectedValue = DateTime.Today.ToString("MM");
	}

	private void GetList() {
		this.grdDailyServiceReport.DataSourceID = "dsDailyServiceReport";
		this.grdDailyServiceReport.DataBind();
	}
	
	protected void btnCSV_Click(object sender,EventArgs e) {

		if (grdDailyServiceReport.Rows.Count == 0) {
			return;
		}

		DataTable oCsvData;
		using (DailyKpiReport oDailyKpiReport = new DailyKpiReport()) {
			using (DataSet oDataSet = oDailyKpiReport.GetListServiseReport(
				this.lstSeekSiteCd.SelectedValue,
				string.Format("{0}/{1}",this.lstYYYY.SelectedValue,this.lstMM.SelectedValue),
				this.SexCd)) {

				if (oDataSet.Tables[0].Rows.Count > 0) {
					oCsvData = oDataSet.Tables[0];
				} else {
					return;
				}
			}
		}

		//ヘッダ作成
		string sHeader = "年月日,PV,UU,消費UU,消費率,ARPPU,ARPU,入会数,退会数,ﾎﾟｲﾝﾄ利用回数,売上";

		//CSV出力 
		Response.ContentType = "application/download";
		Response.AppendHeader("Content-Disposition","attachment;filename=DailyServiceReport.csv");
		Response.ContentEncoding = System.Text.Encoding.GetEncoding("Shift_JIS");

		Response.Write(sHeader + "\r\n");
		foreach (DataRow oCsvRow in oCsvData.Rows) {
			string sData =
				SetCsvString(oCsvRow["REPORT_DAY"].ToString()) + "," +
				SetCsvString(oCsvRow["PAGE_VIEW_COUNT"].ToString()) + "," +
				SetCsvString(oCsvRow["PAGE_VIEW_UNIQUE_COUNT"].ToString()) + "," +
				SetCsvString(oCsvRow["USED_POINT_COUNT_UNIQUE"].ToString()) + "," +
				SetCsvString(oCsvRow["CONSUMPTION_RATE"].ToString()) + "%," +
				SetCsvString(oCsvRow["ARPPU"].ToString()) + "Pt," +
				SetCsvString(oCsvRow["ARPU"].ToString()) + "Pt," +
				SetCsvString(oCsvRow["REGIST_COUNT"].ToString()) + "," +
				SetCsvString(oCsvRow["WITHDRAWAL_COUNT"].ToString()) + "," +
				SetCsvString(oCsvRow["USED_POINT_COUNT"].ToString()) + "," +
				SetCsvString(oCsvRow["USED_POINT"].ToString()) + "Pt";

			Response.Write(sData + "\r\n");
		}
		Response.End();
	}

	private string SetCsvString(string pData) {
		if (pData.IndexOf('"') > -1 ||
			pData.IndexOf(',') > -1 ||
			pData.IndexOf('\r') > -1 ||
			pData.IndexOf('\n') > -1 ||
			pData.StartsWith(" ") || pData.StartsWith("\t") ||
			pData.EndsWith(" ") || pData.EndsWith("\t")) {
			if (pData.IndexOf('"') > -1) {
				//"を""とする
				pData = pData.Replace("\"","\"\"");
			}
			if (pData.IndexOf('\r') > -1 || pData.IndexOf('\n') > -1) {
				//改行しない 
				pData = pData.Replace("\r","");
				pData = pData.Replace("\n","");
			}
			pData = "\"" + pData + "\"";
		}
		return pData;
	}
}