﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ゲームアイテムセールスケジュール一覧

--	Progaram ID		: GameItemSaleScheduleList
--
--  Creation Date	: 2011.07.19
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.IO;

public partial class Extension_GameItemSaleScheduleList : System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		set {
			this.ViewState["SexCd"] = value;
		}
	}

	private string SaleScheduleSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SaleScheduleSeq"]);
		}
		set {
			this.ViewState["SaleScheduleSeq"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();
			this.SexCd = iBridUtil.GetStringValue(this.Request.QueryString["sexcd"]);
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
		this.SaleScheduleSeq = string.Empty;
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		if (!this.IsCorrectDate()) {
			return;
		}
		this.UpdateData(false);
		this.pnlKey.Enabled = true;
		this.pnlGameItemSaleScheduleInfo.Visible = false;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		this.UpdateData(true);
		this.pnlKey.Enabled = true;
		this.pnlGameItemSaleScheduleInfo.Visible = false;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.ClearFileds();
		this.pnlKey.Enabled = true;
		this.pnlGameItemSaleScheduleInfo.Visible = false;
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		this.SaleScheduleSeq = string.Empty;
		this.ClearFileds();
		this.pnlKey.Enabled = false;
		this.pnlGameItemSaleScheduleInfo.Visible = true;
	}

	protected void lnkEdit_Command(object sender,CommandEventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.SaleScheduleSeq = iBridUtil.GetStringValue(e.CommandArgument);
		this.GetData();
		this.pnlKey.Enabled = false;
		this.pnlGameItemSaleScheduleInfo.Visible = true;
	}

	protected void dsGameItemSaleSchedule_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	private void InitPage() {
		this.pnlInfo.Visible = true;
		this.pnlGameItemSaleScheduleInfo.Visible = false;

		this.SaleScheduleSeq = string.Empty;
		this.pnlKey.Enabled = true;
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		this.ClearFileds();
	}

	private void ClearFileds() {
		this.txtSaleStartDate.Text = string.Empty;
		this.txtSaleEndDate.Text = string.Empty;
		this.txtSaleComment.Text = string.Empty;
		this.txtSaleNoticeComment.Text = string.Empty;
		this.txtRemarks.Text = string.Empty;
		this.lblErrorMessageSaleEnd.Visible = false;
		this.lblErrorMessageSaleStart.Visible = false;
		this.pnlKey.Enabled = true;
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.SaleScheduleSeq = string.Empty;

		this.grdGameItemSaleSchedule.PageIndex = 0;
		this.grdGameItemSaleSchedule.DataSourceID = "dsGameItemSaleSchedule";
		this.grdGameItemSaleSchedule.DataBind();
		this.pnlCount.DataBind();
	}

	private void GetData() {
		this.ClearFileds();

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("GAME_ITEM_SALE_SCHEDULE_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pSALE_SCHEDULE_SEQ",DbSession.DbType.VARCHAR2,this.SaleScheduleSeq);
			oDbSession.ProcedureOutParm("pSEX_CD",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSALE_COMMENT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSALE_NOTICE_COMMENT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSALE_START_DATE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSALE_END_DATE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREMARKS",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.txtSaleComment.Text = oDbSession.GetStringValue("pSALE_COMMENT");
			this.txtSaleNoticeComment.Text = oDbSession.GetStringValue("pSALE_NOTICE_COMMENT");
			this.txtSaleStartDate.Text = oDbSession.GetStringValue("pSALE_START_DATE");
			this.txtSaleEndDate.Text = oDbSession.GetStringValue("pSALE_END_DATE");
			this.txtRemarks.Text = oDbSession.GetStringValue("pREMARKS");
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
		}
	}

	private bool IsCorrectDate() {
		this.lblErrorMessageSaleEnd.Visible = false;
		this.lblErrorMessageSaleStart.Visible = false;

		DateTime? dtSaleStart = SysPrograms.TryParseOrDafult(this.txtSaleStartDate.Text,(DateTime?)null);
		DateTime? dtSaleEnd = SysPrograms.TryParseOrDafult(this.txtSaleEndDate.Text,(DateTime?)null);

		if (!dtSaleStart.HasValue) {
			this.lblErrorMessageSaleStart.Visible = true;
			this.lblErrorMessageSaleStart.Text = "セール開始日時を正しく入力してください。";
			return false;
		}
		if (!dtSaleEnd.HasValue) {
			this.lblErrorMessageSaleEnd.Visible = true;
			this.lblErrorMessageSaleEnd.Text = "セール終了日時を正しく入力してください。";
			return false;
		}
		if (dtSaleEnd < dtSaleStart) {
			this.lblErrorMessageSaleEnd.Visible = true;
			this.lblErrorMessageSaleEnd.Text = "セール期間の大小関係が不正です。";
			return false;
		}

		using (GameItemSaleSchedule oGameItemSaleSchedule = new GameItemSaleSchedule()) {
			if (oGameItemSaleSchedule.IsDuplicateDate(this.SiteCd,this.SaleScheduleSeq,this.SexCd,dtSaleStart.Value,dtSaleEnd.Value)) {
				this.lblErrorMessageSaleEnd.Visible = true;
				this.lblErrorMessageSaleEnd.Text = "セール期間が他のセール設定と重複しています。";
				return false;
			}
		}

		return true;
	}

	private void UpdateData(bool pDeleteFlag) {
		DateTime? dtStart = SysPrograms.TryParseOrDafult(this.txtSaleStartDate.Text,(DateTime?)null);
		DateTime? dtEnd = SysPrograms.TryParseOrDafult(this.txtSaleEndDate.Text,(DateTime?)null);
		if (dtEnd.HasValue) {
			dtEnd = (DateTime?)dtEnd.Value.AddSeconds(-1);
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("GAME_ITEM_SALE_SCHEDULE_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureBothParm("pSALE_SCHEDULE_SEQ",DbSession.DbType.VARCHAR2,this.SaleScheduleSeq);
			oDbSession.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,this.SexCd);
			oDbSession.ProcedureInParm("pSALE_COMMENT",DbSession.DbType.VARCHAR2,this.txtSaleComment.Text);
			oDbSession.ProcedureInParm("pSALE_NOTICE_COMMENT",DbSession.DbType.VARCHAR2,this.txtSaleNoticeComment.Text);
			oDbSession.ProcedureInParm("pSALE_START_DATE",DbSession.DbType.DATE,dtStart);
			oDbSession.ProcedureInParm("pSALE_END_DATE",DbSession.DbType.DATE,dtEnd);
			oDbSession.ProcedureInParm("pREMARKS",DbSession.DbType.VARCHAR2,this.txtRemarks.Text);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.VARCHAR2,this.RevisionNo);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDeleteFlag ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.ClearFileds();
		this.GetList();
	}
}
