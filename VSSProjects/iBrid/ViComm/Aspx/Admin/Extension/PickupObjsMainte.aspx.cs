﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ピックアップメンテナンス
--	Progaram ID		: MultiPickupMainte
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using iBridCommLib;
using ViComm;

public partial class Extension_PickupObjsMainte : System.Web.UI.Page {
	protected void Page_Load(object sender, EventArgs e) {
		if (!this.IsPostBack) {
			this.pnlKey.DataBind();

			string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			string sPickupId = iBridUtil.GetStringValue(Request.QueryString["pickupid"]);

			if (!string.IsNullOrEmpty(sSiteCd) && !string.IsNullOrEmpty(sPickupId)) {
				this.lstSiteCd.SelectedValue = sSiteCd;
				this.lstPickupId.SelectedValue = sPickupId;
			}
		}
	}

	protected void btnUpdate_Click(object sender, EventArgs e) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("PICKUP_OBJS_STATUS_UPDATE");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pPICKUP_ID", DbSession.DbType.VARCHAR2, this.lstPickupId.SelectedValue);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
		}

		this.Response.Redirect(this.GetNavigateUrl());
	}

	protected void lnkMultiPickupMainte_Click(object sender, EventArgs e) {
		this.Response.Redirect(this.GetNavigateUrl());
	}

	private string GetNavigateUrl() {
		return string.Format("~/CastAdmin/MultiPickupMainte.aspx?sitecd={0}&pickupid={1}", this.lstSiteCd.SelectedValue, this.lstPickupId.SelectedValue);
	}

	protected void lstPickupId_DataBound(object sender, EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0, new ListItem(string.Empty, string.Empty));
	}
}
