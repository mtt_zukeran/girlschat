﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BeautyClockView.aspx.cs" Inherits="Extension_BeautyClockView" 
	Title="美人時計詳細" ValidateRequest="false" %>

<%@ Import Namespace="System.Data" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="美人時計詳細"></asp:Label>
</asp:Content>

<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent" style="width:700px;">
		<asp:Panel runat="server" ID="pnlDetail">
			<fieldset>
				<legend>
					<%# DisplayWordUtil.Replace("[時間詳細]") %>
				</legend>

				<asp:Panel ID="pnlList" runat="server"  style="width: 665px;">
				    <table>
						<tr>
							<td align="left">
								<asp:LinkButton ID="lnkPrevTime" runat="server" OnCommand="lnkPrevTime_OnCommand" Text="＜＜前の時刻へ"></asp:LinkButton>
							</td>
							<td align="right">
								<asp:LinkButton ID="lnkNextTime" runat="server" OnCommand="lnkNextTime_OnCommand" Text="次の時刻へ＞＞"></asp:LinkButton>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<div class="tdHeaderStyle" style="font-size:large; text-align: center; width: 665px;height:30px;">
									<asp:Label ID="lblTime" runat="server" style="line-height: 30px;"></asp:Label>
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2" align="right">
								<asp:LinkButton ID="lnkBeautyClockRegist" runat="server" OnCommand="lnkBeautyClockRegist_OnCommand" Text="新規登録"></asp:LinkButton>
							</td>
						</tr>
					</table>
					<asp:Repeater id="rptPublish" runat="server" EnableViewState="false">
						<HeaderTemplate>
							<div id='nPublish' style="width:300px;">
								<div>
									現在掲載中のアイドル
								</div>
								<table border="2" bordercolor="#000000" cellspacing="0">
						</HeaderTemplate>
						<ItemTemplate>
									<tr>
										<td style="text-align:left">
											<asp:Label runat="server" Text='<%# string.Format("{0:yyyy/M/d HH:mm}",DataBinder.Eval(Container.DataItem,"UPDATE_DATE" )) %>'></asp:Label><br />
											<asp:HyperLink runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"HANDLE_NM" ) %>' NavigateUrl='<%# string.Format("../Cast/CastView.aspx?loginid={0}",DataBinder.Eval(Container.DataItem, "LOGIN_ID")) %>'></asp:HyperLink>
										</td>
									</tr>
									<tr>
										<td style="text-align:center">
											<asp:Image ID="Image1" runat="server" ImageUrl='<%# string.Format("../{0}",DataBinder.Eval(Container.DataItem,"BEAUTY_IMG_PATH" ).ToString()) %>'></asp:Image><br />
										</td>
									</tr>
						</ItemTemplate>
						<FooterTemplate>
								</table>
							</div>
						</FooterTemplate>
					</asp:Repeater>
								<asp:Repeater id="rptUnPublish" runat="server" OnItemCommand="rptUnPublish_OnItemCommand">
									<HeaderTemplate>
										<div id='unPublish'>
											<div>
												過去に掲載したアイドル
											</div>
											<table>
												<tr>												
									</HeaderTemplate>
									<ItemTemplate>
												<%# ((Container.ItemIndex % 3) == 0 && Container.ItemIndex > 0) ? "</tr><tr>" : "" %>
												<td  style="width:300px;">
													<table border="2" bordercolor="#000000" cellspacing="0">
														<tr>
															<td >
																<table>
																	<tr>
																		<td>
																			<asp:Label ID="Label1" runat="server" Text='<%# string.Format("{0:yyyy/M/d HH:mm}",DataBinder.Eval(Container.DataItem,"UPDATE_DATE" )) %>'></asp:Label><br />
																		</td>
																		<td><br /></td>
																	</tr>
																	<tr>
																		<td>
																			<asp:HyperLink ID="HyperLink1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"HANDLE_NM" ) %>' NavigateUrl='<%# string.Format("../Cast/CastView.aspx?loginid={0}",DataBinder.Eval(Container.DataItem, "LOGIN_ID")) %>'></asp:HyperLink>
																		</td>
																		<td><asp:LinkButton runat="server"  CommandArgument='<%# string.Format("{0}:{1}",DataBinder.Eval(Container.DataItem,"USER_SEQ"),DataBinder.Eval(Container.DataItem,"PIC_SEQ")) %>' CommandName="" OnCommand="lnkChange_OnCommand" Text="差替" ></asp:LinkButton></td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td style="text-align:center">
																<asp:Image runat="server" ImageUrl='<%# string.Format("../{0}",DataBinder.Eval(Container.DataItem,"BEAUTY_IMG_PATH" ).ToString()) %>'></asp:Image><br />
															</td>
														</tr>
													</table>
												</td>
									</ItemTemplate>
									<FooterTemplate>
											</tr>
											</table>
										</div>									
									</FooterTemplate>
								</asp:Repeater>
				</asp:Panel>
			</fieldset>
	</asp:Panel>
	</div>
</asp:Content>