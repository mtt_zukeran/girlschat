﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="StageGroupList.aspx.cs" Inherits="Extension_StageGroupList" Title="ステージグループ一覧"
    ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="ステージグループ一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 600px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle2">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton" ValidationGroup="Create"
                    OnClick="btnCreate_Click" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" Visible="false" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlStageGroupInfo">
            <fieldset class="fieldset">
                <legend>[ステージグループ情報]</legend>
                <table border="0" style="width: 600px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            ステージグループタイプ
                        </td>
                        <td class="tdDataStyle">
                            <asp:Label ID="lblStageGroupType" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            ステージグループ名
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtStageGroupNm" runat="server" MaxLength="64" Width="200px"></asp:TextBox>
                            <% // 必須チェック %>
                            <asp:RequiredFieldValidator ID="vdrStageGroupNm" runat="server" ErrorMessage="ステージグループ名を入力してください。"
                                ControlToValidate="txtStageGroupNm" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                            <ajaxToolkit:ValidatorCalloutExtender ID="vdeStageGroupNm" runat="Server" TargetControlID="vdrStageGroupNm"
                                HighlightCssClass="validatorCallout" />
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="pnlCastCompleteItem" runat="server">
                    <fieldset class="fieldset-inner">
                        <legend>[お宝コンプリート時獲得アイテム]</legend>
                        <table border="0" style="width: 720px" class="tableStyle">
                            <tr>
                                <td class="tdHeaderStyle">
                                    アイテム１
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstCastCompleteItemCategory1" runat="server" DataSourceID="dsGameItemCategory"
                                        OnDataBound="lst_DataBound" OnSelectedIndexChanged="lstGameItemCategory_IndexChanged"
                                        DataTextField="GAME_ITEM_CATEGORY_NM" DataValueField="GAME_ITEM_CATEGORY_TYPE"
                                        Width="170px" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="lstItemGetCd1" runat="server" DataSourceID="dsItemGet" DataTextField="CODE_NM"
										DataValueField="CODE" OnDataBound="lstItemGetCd_DataBound" OnSelectedIndexChanged="lstItemGetCd_IndexChanged" AutoPostBack="True">
									</asp:DropDownList>
									<asp:DropDownList ID="lstItemPresent1" runat="server" OnSelectedIndexChanged="lstItemPresent_IndexChanged" AutoPostBack="True">
										<asp:ListItem Value=""></asp:ListItem>
										<asp:ListItem Value="0">通常</asp:ListItem>
										<asp:ListItem Value="1">ﾌﾟﾚｾﾞﾝﾄ</asp:ListItem>
									</asp:DropDownList>
                                    <asp:DropDownList ID="lstCastCompleteItem1" runat="server" DataSourceID="dsGameItem"
                                        DataTextField="GAME_ITEM_NM" OnDataBound="lst_DataBound" DataValueField="GAME_ITEM_SEQ"
                                        Width="170px">
                                    </asp:DropDownList>
                                    &nbsp;
                                    <asp:TextBox ID="txtCastCompleteItemGetCount1" runat="server" MaxLength="10" Width="50px"></asp:TextBox>個<br />
                                    <asp:CustomValidator ID="vdcCastCompleteItemGet1" runat="server" ErrorMessage=""
                                        OnServerValidate="vdcCastCompleteItemGet1_ServerValidate" ValidationGroup="Update"></asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    アイテム２
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstCastCompleteItemCategory2" runat="server" DataSourceID="dsGameItemCategory"
                                        OnDataBound="lst_DataBound" OnSelectedIndexChanged="lstGameItemCategory_IndexChanged"
                                        DataTextField="GAME_ITEM_CATEGORY_NM" DataValueField="GAME_ITEM_CATEGORY_TYPE"
                                        Width="170px" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="lstItemGetCd2" runat="server" DataSourceID="dsItemGet" DataTextField="CODE_NM"
										DataValueField="CODE" OnDataBound="lstItemGetCd_DataBound" OnSelectedIndexChanged="lstItemGetCd_IndexChanged" AutoPostBack="True">
									</asp:DropDownList>
									<asp:DropDownList ID="lstItemPresent2" runat="server" OnSelectedIndexChanged="lstItemPresent_IndexChanged" AutoPostBack="True">
										<asp:ListItem Value=""></asp:ListItem>
										<asp:ListItem Value="0">通常</asp:ListItem>
										<asp:ListItem Value="1">ﾌﾟﾚｾﾞﾝﾄ</asp:ListItem>
									</asp:DropDownList>
                                    <asp:DropDownList ID="lstCastCompleteItem2" runat="server" DataSourceID="dsGameItem2"
                                        DataTextField="GAME_ITEM_NM" OnDataBound="lst_DataBound" DataValueField="GAME_ITEM_SEQ"
                                        Width="170px">
                                    </asp:DropDownList>
                                    &nbsp;
                                    <asp:TextBox ID="txtCastCompleteItemGetCount2" runat="server" MaxLength="10" Width="50px"></asp:TextBox>個<br />
                                    <asp:CustomValidator ID="vdcCastCompleteItemGet2" runat="server" ErrorMessage=""
                                        OnServerValidate="vdcCastCompleteItemGet2_ServerValidate" ValidationGroup="Update"></asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle2">
                                    アイテム３
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstCastCompleteItemCategory3" runat="server" DataSourceID="dsGameItemCategory"
                                        OnDataBound="lst_DataBound" OnSelectedIndexChanged="lstGameItemCategory_IndexChanged"
                                        DataTextField="GAME_ITEM_CATEGORY_NM" DataValueField="GAME_ITEM_CATEGORY_TYPE"
                                        Width="170px" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="lstItemGetCd3" runat="server" DataSourceID="dsItemGet" DataTextField="CODE_NM"
										DataValueField="CODE" OnDataBound="lstItemGetCd_DataBound" OnSelectedIndexChanged="lstItemGetCd_IndexChanged" AutoPostBack="True">
									</asp:DropDownList>
									<asp:DropDownList ID="lstItemPresent3" runat="server" OnSelectedIndexChanged="lstItemPresent_IndexChanged" AutoPostBack="True">
										<asp:ListItem Value=""></asp:ListItem>
										<asp:ListItem Value="0">通常</asp:ListItem>
										<asp:ListItem Value="1">ﾌﾟﾚｾﾞﾝﾄ</asp:ListItem>
									</asp:DropDownList>
                                    <asp:DropDownList ID="lstCastCompleteItem3" runat="server" DataSourceID="dsGameItem3"
                                        DataTextField="GAME_ITEM_NM" OnDataBound="lst_DataBound" DataValueField="GAME_ITEM_SEQ"
                                        Width="170px">
                                    </asp:DropDownList>
                                    &nbsp;
                                    <asp:TextBox ID="txtCastCompleteItemGetCount3" runat="server" MaxLength="10" Width="50px"></asp:TextBox>個<br />
                                    <asp:CustomValidator ID="vdcCastCompleteItemGet3" runat="server" ErrorMessage=""
                                        OnServerValidate="vdcCastCompleteItemGet3_ServerValidate" ValidationGroup="Update"></asp:CustomValidator>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </asp:Panel>
                <asp:Panel ID="pnlCommand" runat="server">
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Update"
                        OnClick="btnUpdate_Click" />
                    <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" ValidationGroup="Delete"
                        OnClick="btnDelete_Click" />
                    <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" CausesValidation="False"
                        Visible="true" OnClick="btnCancel_Click" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[ステージグループ一覧]</legend>
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="600px">
                    <asp:GridView ID="grdStageGroup" runat="server" AutoGenerateColumns="False" EnableSortingAndPagingCallbacks="false"
                        DataSourceID="dsStageGroup" SkinID="GridViewColor" Font-Size="small" AllowSorting="true"
                        DataKeyNames="SITE_CD,STAGE_GROUP_TYPE">
                        <Columns>
                            <asp:TemplateField HeaderText="ｽﾃｰｼﾞｸﾞﾙｰﾌﾟ名/<br/>ｽﾃｰｼﾞｸﾞﾙｰﾌﾟﾀｲﾌﾟ">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkStageGroupNm" runat="server" CommandArgument='<%# Eval("STAGE_GROUP_TYPE") %>'
                                        Text='<%# Eval("STAGE_GROUP_NM") %>' OnCommand="lnkStageGroupNm_Command"></asp:LinkButton>/
                                    <asp:Label ID="lblStageGroupType" runat="server" Text='<%# Eval("STAGE_GROUP_TYPE") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="UPDATE_DATE" HeaderText="更新日時">
                                <ItemStyle HorizontalAlign="right" />
                            </asp:BoundField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsStageGroup" runat="server" SelectMethod="GetList" TypeName="StageGroup">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItemCategory" runat="server" SelectMethod="GetList"
        TypeName="GameItemCategory">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItem" runat="server" SelectMethod="GetListByCategoryType"
        TypeName="GameItem" OnSelecting="dsGameItem_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
            <asp:Parameter Name="pGameItemCategoryType" Type="String" DefaultValue="" />
            <asp:Parameter Name="pGameItemGetCd" Type="String" DefaultValue="" />
            <asp:Parameter Name="pPresentFlag" Type="String" DefaultValue="" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItem2" runat="server" SelectMethod="GetListByCategoryType"
        TypeName="GameItem" OnSelecting="dsGameItem2_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
            <asp:Parameter Name="pGameItemCategoryType" Type="String" DefaultValue="" />
            <asp:Parameter Name="pGameItemGetCd" Type="String" DefaultValue="" />
            <asp:Parameter Name="pPresentFlag" Type="String" DefaultValue="" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItem3" runat="server" SelectMethod="GetListByCategoryType"
        TypeName="GameItem" OnSelecting="dsGameItem3_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
            <asp:Parameter Name="pGameItemCategoryType" Type="String" DefaultValue="" />
            <asp:Parameter Name="pGameItemGetCd" Type="String" DefaultValue="" />
            <asp:Parameter Name="pPresentFlag" Type="String" DefaultValue="" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsItemGet" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="A2" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="ステージグループ情報を更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete"
        ConfirmText="ステージグループ情報を削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>
