﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="LevelUpExpList.aspx.cs" Inherits="Extension_LevelUpExpList"
	Title="レベルアップ必要経験値一覧" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="レベルアップ必要経験値一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlKey">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel ID="pnlSeekCondition" runat="server">
					<table border="0" style="width: 600px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle2">
								サイトコード
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px">
								</asp:DropDownList>
							</td>
						</tr>
					</table>
				</asp:Panel>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Key" CausesValidation="True" />
				<asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton" ValidationGroup="Create" OnClick="btnCreate_Click" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" CausesValidation="False" Visible="false" />
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlLevelInfo">
			<fieldset class="fieldset">
				<legend>[レベル情報]</legend>
				<table border="0" style="width: 600px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							レベル
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtGameCharacterLevel" runat="server" MaxLength="3" Width="80px"></asp:TextBox>
							<% // 必須チェック %>
							<asp:RequiredFieldValidator ID="vdrGameCharacterLevel" runat="server" ErrorMessage="ﾚﾍﾞﾙを入力してください。" ControlToValidate="txtGameCharacterLevel" ValidationGroup="Update">*</asp:RequiredFieldValidator>
							<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" TargetControlID="vdrGameCharacterLevel" HighlightCssClass="validatorCallout" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle" style="white-space:nowrap">
							次のﾚﾍﾞﾙまでの必要経験値
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtNeedExperiencePoint" runat="server" MaxLength="8" Width="80px"></asp:TextBox>
							<% // 必須チェック %>
							<asp:RequiredFieldValidator ID="vdrNeedExperiencePoint" runat="server" ErrorMessage="必要経験値を入力してください。" ControlToValidate="txtNeedExperiencePoint" ValidationGroup="Update">*</asp:RequiredFieldValidator>
							<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" TargetControlID="vdrNeedExperiencePoint" HighlightCssClass="validatorCallout" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							ﾎﾞｽ戦時ｷｬﾗｸﾀｰHP
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtCharacterHp" runat="server" MaxLength="8" Width="80px"></asp:TextBox>
							<% // 必須チェック %>
							<asp:RequiredFieldValidator ID="vdrCharacterHp" runat="server" ErrorMessage="ﾎﾞｽ戦時ｷｬﾗｸﾀｰHPを入力してください。" ControlToValidate="txtCharacterHp" ValidationGroup="Update">*</asp:RequiredFieldValidator>
							<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" TargetControlID="vdrCharacterHp" HighlightCssClass="validatorCallout" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							仲間上限数
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtFerrowCountLimit" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
							<% // 必須チェック %>
							<asp:RequiredFieldValidator ID="vdrFerrowCountLimit" runat="server" ErrorMessage="仲間上限数入力してください。" ControlToValidate="txtFerrowCountLimit" ValidationGroup="Update">*</asp:RequiredFieldValidator>
							<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" TargetControlID="vdrFerrowCountLimit" HighlightCssClass="validatorCallout" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							部隊増加数
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtAddForceCount" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
							<% // 必須チェック %>
							<asp:RequiredFieldValidator ID="vdrAddForceCount" runat="server" ErrorMessage="部隊増加数入力してください。" ControlToValidate="txtAddForceCount" ValidationGroup="Update">*</asp:RequiredFieldValidator>
							<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" TargetControlID="vdrAddForceCount" HighlightCssClass="validatorCallout" />
						</td>
					</tr>
				</table>
				<asp:Panel ID="pnlCommand" runat="server">
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Update" OnClick="btnUpdate_Click" />
					<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" ValidationGroup="Delete" OnClick="btnDelete_Click" />
					<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" CausesValidation="False" Visible="true" OnClick="btnCancel_Click" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[レベル一覧]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count
						<%# GetRecCount() %>
					</a>
					<br />
					<a class="reccount">Current viewing page
						<%= grdLevelUpExp.PageIndex + 1 %>
						of
						<%= grdLevelUpExp.PageCount %>
					</a>
				</asp:Panel>
				&nbsp;
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="603px">
					<asp:GridView ID="grdLevelUpExp" runat="server" AllowPaging="True" AutoGenerateColumns="False" PageSize="30" EnableSortingAndPagingCallbacks="false" DataSourceID="dsLevelUpExp"
						SkinID="GridViewColor" AllowSorting="true" DataKeyNames="SITE_CD,GAME_CHARACTER_LEVEL,SEX_CD">
						<Columns>
							<asp:TemplateField HeaderText="レベル">
								<ItemTemplate>
									<asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%# Eval("GAME_CHARACTER_LEVEL") %>' OnCommand="lnkEdit_Command" Text='<%# Eval("GAME_CHARACTER_LEVEL") %>'></asp:LinkButton>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="left" />
							</asp:TemplateField>
							<asp:BoundField DataField="NEED_EXPERIENCE_POINT" HeaderText="必要経験値" DataFormatString="{0:N0}">
								<ItemStyle HorizontalAlign="right" />
							</asp:BoundField>
							<asp:BoundField DataField="TOTAL_NEED_EXPERIENCE_POINT" HeaderText="必要累計経験値" DataFormatString="{0:N0}">
								<ItemStyle HorizontalAlign="right" />
							</asp:BoundField>
							<asp:BoundField DataField="CHARACTER_HP" DataFormatString="{0:N0}" HeaderText="ﾎﾞｽ戦時ｷｬﾗｸﾀｰHP">
								<ItemStyle HorizontalAlign="right" />
							</asp:BoundField>
							<asp:BoundField DataField="FELLOW_COUNT_LIMIT" DataFormatString="{0:N0}" HeaderText="仲間上限数">
								<ItemStyle HorizontalAlign="right" />
							</asp:BoundField>
							<asp:BoundField DataField="ADD_FORCE_COUNT" DataFormatString="{0:N0}" HeaderText="部隊増加数">
								<ItemStyle HorizontalAlign="right" />
							</asp:BoundField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
			<br />
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsLevelUpExp" runat="server" ConvertNullToDBNull="false" EnablePaging="True" OnSelected="dsLevelUpExp_Selected" SelectCountMethod="GetPageCount"
		SelectMethod="GetPageCollection" SortParameterName="" TypeName="LevelUpExp">
		<SelectParameters>
			<asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue" Type="String" />
			<asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtNeedExperiencePoint">
	</ajaxToolkit:FilteredTextBoxExtender>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtCharacterHp">
	</ajaxToolkit:FilteredTextBoxExtender>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtFerrowCountLimit">
	</ajaxToolkit:FilteredTextBoxExtender>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtAddForceCount">
	</ajaxToolkit:FilteredTextBoxExtender>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtGameCharacterLevel">
	</ajaxToolkit:FilteredTextBoxExtender>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="レベル情報を更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="レベル情報を削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>
