﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: リッチーノランクメンテナンス
--	Progaram ID		: RichinoRankList
--
--  Creation Date	: 2011.04.18
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Extension_RichinoRankList : System.Web.UI.Page {
	private string RevisionNo {
		get { return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]); }
		set { this.ViewState["RevisionNo"] = value; }
	}
	private string Rowid {
		get { return iBridUtil.GetStringValue(this.ViewState["Rowid"]); }
		set { this.ViewState["Rowid"] = value; }
	}

	private string recCount = "";

	protected void Page_Load(object sender, EventArgs e) {
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
	}

	protected void btnSeek_Click(object sender, EventArgs e) {
		GetData();
	}

	protected void btnRegist_Click(object sender, EventArgs e) {
		InitPage();
		pnlMainte.Visible = true;
		pnlKey.Enabled = true;
		pnlDtl.Visible = false;
		lstSiteCd.SelectedIndex = lstSeekSiteCd.SelectedIndex;
	}

	protected void btnUpdate_Click(object sender, EventArgs e) {
		if (IsValid) {
			UpdateData(ViCommConst.FLAG_OFF);
		}
	}

	protected void btnDelete_Click(object sender, EventArgs e) {
		if (IsValid) {
			UpdateData(ViCommConst.FLAG_ON);
		}
	}

	protected void btnCancel_Click(object sender, EventArgs e) {
		InitPage();
	}

	protected void btnListSeek_Click(object sender, EventArgs e) {
		GetList();
	}

	protected void lnkRichinoRank_Command(object sender, CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		lstSiteCd.SelectedValue = sKeys[0];
		txtRichinoRank.Text = sKeys[1];
		GetData();
	}

	protected void dsRichinoRank_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void vdcPointAffiliateRichinoRankFlag_ServerValidate(object source, ServerValidateEventArgs args) {
		if (IsValid) {
		}
	}

	private void FirstLoad() {
		grdRichinoRank.PageSize = 999;
		string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		if (sSiteCd.Equals("")) {
			lstSeekSiteCd.SelectedIndex = 0;
		} else {
			lstSeekSiteCd.SelectedValue = sSiteCd;
		}
		lstSeekSiteCd.DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		DataBind();
		int iCompanyMask = int.Parse(Session["CompanyMask"].ToString());

	}

	private void InitPage() {
		txtRichinoRank.Text = "";
		ClearField();
		pnlMainte.Visible = false;
	}

	private void ClearField() {
		this.txtRichinoRankNm.Text = string.Empty;
		this.txtRichinoMinReceiptAmt.Text = "0";
		this.txtRichinoRankKeepAmt.Text = "0";
		this.txtSpecialMovieCount.Text = "0";
		this.txtSpecialPicCount.Text = "0";
		this.txtMailRequestLimitCount.Text = "0";
		this.txtManTweetDailyCount.Text = "0";
		this.txtManTweetIntervalMin.Text = "0";
		this.recCount = "0";
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		grdRichinoRank.PageIndex = 0;
		grdRichinoRank.DataBind();
		pnlCount.DataBind();
	}

	private void GetData() {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("RICHINO_RANK_GET");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.lstSeekSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pRICHINO_RANK", DbSession.DbType.VARCHAR2, this.txtRichinoRank.Text.Trim());
			oDbSession.ProcedureOutParm("pRICHINO_RANK_NM", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pRICHINO_MIN_RECEIPT_AMT", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pRICHINO_RANK_KEEP_AMT", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSPECIAL_MOVIE_COUNT", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSPECIAL_PIC_COUNT", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pMAIL_REQUEST_LIMIT_COUNT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pMAN_TWEET_DAILY_COUNT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pMAN_TWEET_INTERVAL_MIN",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pREVISION_NO", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pRECORD_COUNT", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pROWID", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.RevisionNo = oDbSession.GetStringValue("PREVISION_NO");
			this.Rowid = oDbSession.GetStringValue("PROWID");

			if (int.Parse(oDbSession.GetStringValue("PRECORD_COUNT")) > 0) {
				this.txtRichinoRankNm.Text = oDbSession.GetStringValue("pRICHINO_RANK_NM");
				this.txtRichinoMinReceiptAmt.Text = oDbSession.GetStringValue("pRICHINO_MIN_RECEIPT_AMT");
				this.txtRichinoRankKeepAmt.Text = oDbSession.GetStringValue("pRICHINO_RANK_KEEP_AMT");
				this.txtSpecialMovieCount.Text = oDbSession.GetStringValue("pSPECIAL_MOVIE_COUNT");
				this.txtSpecialPicCount.Text = oDbSession.GetStringValue("pSPECIAL_PIC_COUNT");
				this.txtMailRequestLimitCount.Text = oDbSession.GetStringValue("pMAIL_REQUEST_LIMIT_COUNT");
				this.txtManTweetDailyCount.Text = oDbSession.GetStringValue("pMAN_TWEET_DAILY_COUNT");
				this.txtManTweetIntervalMin.Text = oDbSession.GetStringValue("pMAN_TWEET_INTERVAL_MIN");
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		lstSeekSiteCd.SelectedIndex = lstSiteCd.SelectedIndex;
		pnlKey.Enabled = false;
	}

	private void UpdateData(int pDelFlag) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("RICHINO_RANK_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pRICHINO_RANK", DbSession.DbType.VARCHAR2, this.txtRichinoRank.Text);
			oDbSession.ProcedureInParm("pRICHINO_RANK_NM", DbSession.DbType.VARCHAR2, this.txtRichinoRankNm.Text);
			oDbSession.ProcedureInParm("pRICHINO_MIN_RECEIPT_AMT", DbSession.DbType.VARCHAR2, this.txtRichinoMinReceiptAmt.Text);
			oDbSession.ProcedureInParm("pRICHINO_RANK_KEEP_AMT", DbSession.DbType.VARCHAR2, this.txtRichinoRankKeepAmt.Text);
			oDbSession.ProcedureInParm("pSPECIAL_MOVIE_COUNT", DbSession.DbType.VARCHAR2, this.txtSpecialMovieCount.Text);
			oDbSession.ProcedureInParm("pSPECIAL_PIC_COUNT", DbSession.DbType.VARCHAR2, this.txtSpecialPicCount.Text);
			oDbSession.ProcedureInParm("pMAIL_REQUEST_LIMIT_COUNT",DbSession.DbType.VARCHAR2,this.txtMailRequestLimitCount.Text);
			oDbSession.ProcedureInParm("pMAN_TWEET_DAILY_COUNT",DbSession.DbType.VARCHAR2,this.txtManTweetDailyCount.Text);
			oDbSession.ProcedureInParm("pMAN_TWEET_INTERVAL_MIN",DbSession.DbType.VARCHAR2,this.txtManTweetIntervalMin.Text);
			oDbSession.ProcedureInParm("pREVISION_NO", DbSession.DbType.NUMBER, this.RevisionNo);
			oDbSession.ProcedureInParm("pROWID", DbSession.DbType.VARCHAR2, this.Rowid);
			oDbSession.ProcedureInParm("pDEL_FLAG", DbSession.DbType.NUMBER, pDelFlag);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		int iIdx = lstSiteCd.SelectedIndex;
		InitPage();
		GetList();
		lstSeekSiteCd.SelectedIndex = iIdx;
	}
}
