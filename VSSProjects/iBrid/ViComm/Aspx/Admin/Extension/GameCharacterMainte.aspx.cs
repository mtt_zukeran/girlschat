﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ゲームキャラクター情報設定--	Progaram ID		: GameCharacterMainte
--
--  Creation Date	: 20119.08.22
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class Extension_GameCharacterMainte : System.Web.UI.Page
{
    private string SiteCd {
        get { return iBridUtil.GetStringValue(this.ViewState["SITE_CD"]); }
        set { this.ViewState["SITE_CD"] = value; }
    }
    private string UserSeq {
        get { return iBridUtil.GetStringValue(this.ViewState["USER_SEQ"]); }
        set { this.ViewState["USER_SEQ"] = value; }
    }
    private string UserCharNo {
        get { return iBridUtil.GetStringValue(this.ViewState["USER_CHAR_NO"]); }
        set { this.ViewState["USER_CHAR_NO"] = value; }
    }
    private string SexCd {
        get { return iBridUtil.GetStringValue(this.ViewState["SEX_CD"]); }
        set { this.ViewState["SEX_CD"] = value; }
    }
    private string LoginId {
        get { return iBridUtil.GetStringValue(this.ViewState["LOGIN_ID"]); }
        set { this.ViewState["LOGIN_ID"] = value; }
    }
    private string Return {
        get { return iBridUtil.GetStringValue(this.ViewState["RETURN"]); }
        set { this.ViewState["RETURN"] = value; }
    }
    private string RevisionNo {
        get { return iBridUtil.GetStringValue(this.ViewState["REVISION_NO"]); }
        set { this.ViewState["REVISION_NO"] = value; }
    }


    protected void Page_Load(object sender, EventArgs e) {
        Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
        if (!IsPostBack) {
            InitPage();
        }
    }

    protected void InitPage() {

        this.UserSeq = iBridUtil.GetStringValue(Request.QueryString["user_seq"]);
        this.UserCharNo = iBridUtil.GetStringValue(Request.QueryString["user_char_no"]);
        this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["site_cd"]);
        this.SexCd = iBridUtil.GetStringValue(Request.QueryString["sex_cd"]);
        this.LoginId = iBridUtil.GetStringValue(Request.QueryString["login_id"]);
        this.Return = iBridUtil.GetStringValue(Request.QueryString["return"]);

        ClearField();
        DataBind();
        GetData();
    }
    private void ClearField()
    {
        txtUnassignedForce.Text = string.Empty;
        txtGameHandleNm.Text = string.Empty;
        txtCooperationPoint.Text = string.Empty;
        
    }
    protected void ReturnUrl() {
        if (this.SexCd == ViCommConst.MAN) {
            Server.Transfer(string.Format("../Man/ManView.aspx?site={0}&manloginid={1}", this.SiteCd, this.LoginId));
        } else {
            Server.Transfer(string.Format("../Cast/CastView.aspx?loginid={0}&return={1}", this.LoginId, this.Return));
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e) {
        if (IsValid) {
            UpdateData(0);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e) {
        ReturnUrl();
    }


    private void GetData() {

        this.pnlGameCharacter.Visible = true;
        using (DbSession oGameDbSession = new DbSession()) {
            oGameDbSession.PrepareProcedure("GAME_CHARACTER_GET");
            oGameDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.SiteCd);
            oGameDbSession.ProcedureInParm("pUSER_SEQ", DbSession.DbType.VARCHAR2, this.UserSeq);
            oGameDbSession.ProcedureInParm("pUSER_CHAR_NO", DbSession.DbType.VARCHAR2, this.UserCharNo);
            oGameDbSession.ProcedureOutParm("pGAME_HANDLE_NM", DbSession.DbType.VARCHAR2);
            oGameDbSession.ProcedureOutParm("pUNASSIGNED_FORCE_COUNT", DbSession.DbType.NUMBER);
            oGameDbSession.ProcedureOutParm("pCOOPERATION_POINT", DbSession.DbType.NUMBER);
            oGameDbSession.ProcedureOutParm("pREVISION_NO", DbSession.DbType.VARCHAR2);
            oGameDbSession.ProcedureOutParm("pROW_COUNT", DbSession.DbType.NUMBER);
            oGameDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
            oGameDbSession.ExecuteProcedure();
            if (int.Parse(oGameDbSession.GetStringValue("pROW_COUNT")) > 0) {
                this.txtGameHandleNm.Text = oGameDbSession.GetStringValue("pGAME_HANDLE_NM");
                this.txtUnassignedForce.Text = oGameDbSession.GetStringValue("pUNASSIGNED_FORCE_COUNT");
                this.txtCooperationPoint.Text = oGameDbSession.GetStringValue("pCOOPERATION_POINT");
                this.RevisionNo = oGameDbSession.GetStringValue("pREVISION_NO");
            } else {
                this.pnlGameCharacter.Visible = false;
            }
        }
    }


    private void UpdateData(int pDelFlag) {
        bool bUseGameCharacter = false;
        if (this.SexCd == ViCommConst.MAN) {
            using (UserManCharacterEx oCharacterEx = new UserManCharacterEx()) {
                using (DataSet oDataSet = oCharacterEx.GetOne(this.SiteCd, this.UserSeq)) {
                    if (oDataSet.Tables[0].Rows.Count > 0) {
                        DataRow dr = oDataSet.Tables[0].Rows[0];
                        if (dr["SITE_USE_STATUS"].ToString().Equals(ViCommConst.SiteUseStatus.GAME_ONLY) ||
                            dr["SITE_USE_STATUS"].ToString().Equals(ViCommConst.SiteUseStatus.LIVE_CHAT_AND_GAME)) {
                            bUseGameCharacter = true;
                        }
                    }
                }
            }
        }else{
            using (CastCharacterEx oCharacterEx = new CastCharacterEx()) {
                using (DataSet oDataSet = oCharacterEx.GetOne(this.SiteCd, this.UserSeq,this.UserCharNo)) {
                    if (oDataSet.Tables[0].Rows.Count > 0) {
                        DataRow dr = oDataSet.Tables[0].Rows[0];
                        if (dr["SITE_USE_STATUS"].ToString().Equals(ViCommConst.SiteUseStatus.GAME_ONLY) ||
                            dr["SITE_USE_STATUS"].ToString().Equals(ViCommConst.SiteUseStatus.LIVE_CHAT_AND_GAME)) {
                                bUseGameCharacter = true;
                        }
                    }
                }
            }
        
        }
        
        if(bUseGameCharacter) {
             using (DbSession oGameDbSession = new DbSession()) {
                oGameDbSession.PrepareProcedure("GAME_CHARACTER_MAINTE");
                oGameDbSession.ProcedureInParm("PSITE_CD", DbSession.DbType.VARCHAR2, this.SiteCd);
                oGameDbSession.ProcedureInParm("PUSER_SEQ", DbSession.DbType.VARCHAR2, this.UserSeq);
                oGameDbSession.ProcedureInParm("pUSER_CHAR_NO", DbSession.DbType.VARCHAR2, this.UserCharNo);
                oGameDbSession.ProcedureInParm("pGAME_HANDLE_NM", DbSession.DbType.VARCHAR2, this.txtGameHandleNm.Text);
                oGameDbSession.ProcedureInParm("pUNASSIGNED_FORCE_COUNT", DbSession.DbType.NUMBER, this.txtUnassignedForce.Text);
                oGameDbSession.ProcedureInParm("pCOOPERATION_POINT", DbSession.DbType.NUMBER, this.txtCooperationPoint.Text);
                oGameDbSession.ProcedureInParm("pREVISION_NO", DbSession.DbType.VARCHAR2, this.RevisionNo);
                oGameDbSession.ProcedureInParm("PDEL_FLAG", DbSession.DbType.NUMBER, pDelFlag);
                oGameDbSession.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);
                oGameDbSession.ExecuteProcedure();
            }
        }

        ReturnUrl();
    }

}
