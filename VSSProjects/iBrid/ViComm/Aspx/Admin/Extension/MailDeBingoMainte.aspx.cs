﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メールdeビンゴ開催設定
--	Progaram ID		: MailDeBingoMainte
--
--  Creation Date	: 2011.07.06
--  Creater			: iBrid
--
**************************************************************************/

// [ this.Update History ]
/*------------------------------------------------------------------------

  Date        this.Updater    this.Update Explain

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;
using System.Windows.Forms;
using System.Drawing;

public partial class Extension_MailDeBingoMainte : System.Web.UI.Page {
	protected static readonly string[] MinuteArray = new string[] { "00", "15", "30", "45" };
	protected static readonly string[] HourArray = new string[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" };
	protected static readonly string[] DayArray = new string[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" };
	protected static readonly string[] MonthArray = new string[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" };
	protected string[] YearArray;

	private string SiteCd {
		get { return iBridUtil.GetStringValue(this.ViewState["SiteCd"]); }
		set { this.ViewState["SiteCd"] = value; }
	}

    private string SexCd {
        get { return iBridUtil.GetStringValue(this.ViewState["SexCd"]); }
        set { this.ViewState["SexCd"] = value;}
    }

	private string BingoTermSeq {
		get { return iBridUtil.GetStringValue(this.ViewState["BINGO_TERM_SEQ"]); }
        set { this.ViewState["BINGO_TERM_SEQ"] = value; }
	}

	private string RevisionNo {
		get { return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]); }
		set { this.ViewState["RevisionNo"] = value; }
	}

	private string Rowid {
		get { return iBridUtil.GetStringValue(this.ViewState["Rowid"]); }
		set { this.ViewState["Rowid"] = value; }
	}

	private string recCount = "";

	protected void Page_Load(object sender, EventArgs e) {
		if (!IsPostBack) {
            this.SiteCd = Request.QueryString["sitecd"];
            this.SexCd = Request.QueryString["sexcd"];
			InitPage();
			FirstLoad();
		}

    }
    protected void btnSeek_Click(object sender, EventArgs e) {
        this.BingoTermSeq = string.Empty;
        this.GetData();
    }
    protected void btnListSeek_Click(object sender, EventArgs e) {
        this.lstSiteCd.SelectedValue = this.lstSeekSiteCd.SelectedValue;
        this.pnlMainte.Visible = false;
        this.GetList();
    }

    protected void btnRegist_Click(object sender, EventArgs e) {
        InitPage();
        this.pnlMainte.Visible = true;
        this.lstSiteCd.SelectedValue = this.lstSeekSiteCd.SelectedValue;

        this.BingoTermSeq = string.Empty;
        this.GetData();
    }
    protected void btnUpdate_Click(object sender, EventArgs e) {
        if (IsValid) {
            this.UpdateData(ViCommConst.FLAG_OFF);
        }
    }

    protected void btnDelete_Click(object sender, EventArgs e) {
        if (IsValid) {
            this.UpdateData(ViCommConst.FLAG_ON);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e) {
        this.InitPage();
    }
    protected void dsMailDeBingo_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
        if (e.ReturnValue != null) {
            this.recCount = e.ReturnValue.ToString();
        }
    }

    protected void tdcRemarks_ServerValidate(object sender, ServerValidateEventArgs e)
    {
        if (this.IsValid) {

            if (this.txtRemarks.Text.Length > this.txtRemarks.MaxLength)
            {
                e.IsValid = false;
            }
        }
    }

    protected void lnkStartDate_Command(object sender, CommandEventArgs e) {
        int iRowIndex = int.Parse(e.CommandArgument.ToString());
        IDictionary oDataKeys = this.grdMailDeBingo.DataKeys[iRowIndex].Values;
        this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(oDataKeys["SITE_CD"]);
        this.BingoTermSeq = iBridUtil.GetStringValue(oDataKeys["BINGO_TERM_SEQ"]);
        this.GetData();
    }
	private void FirstLoad() {

        if (iBridUtil.GetStringValue(Request.QueryString["sexcd"]).Equals(ViCommConst.MAN)) {
            this.lblPgmTitle.Text = "男性" + lblPgmTitle.Text;
            this.Title = "男性" + this.Title;
        } else {
            this.lblPgmTitle.Text = "女性" + lblPgmTitle.Text;
            this.Title = "女性" + this.Title;
        }
        this.grdMailDeBingo.PageSize = 100;
        this.YearArray = new string[] { DateTime.Today.ToString("yyyy"), DateTime.Today.AddYears(1).ToString("yyyy") };

        this.DataBind();
        if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
            this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
        }
        if (!string.IsNullOrEmpty(this.SiteCd)) {
            this.lstSeekSiteCd.SelectedValue = this.SiteCd;
        }
        this.GetList();
	}

	private void InitPage() {
		this.ClearField();
		this.pnlMainte.Visible = false;
	}

	private void ClearField() {
		this.recCount = "0";
        this.txtRemarks.Text = string.Empty;
        this.txtJackpot.Text = "0";

        this.lstFromYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
        this.lstFromMM.SelectedValue = DateTime.Now.ToString("MM");
        this.lstFromDD.SelectedValue = DateTime.Now.ToString("dd");
        this.lstFromHH.SelectedValue = DateTime.Now.ToString("HH");
        this.lstToYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
        this.lstToMM.SelectedValue = DateTime.Now.ToString("MM");
        this.lstToDD.SelectedValue = DateTime.Now.ToString("dd");
        this.lstToHH.SelectedValue = DateTime.Now.ToString("HH");

        this.lstFromMI.SelectedIndex = 0;
        this.lstToMI.SelectedIndex = 0;
	}

	protected string GetRecCount() {
		return this.recCount;
	}

    private void GetList() {
        this.grdMailDeBingo.PageIndex = 0;
        this.grdMailDeBingo.DataSourceID = "dsMailDeBingo";
        this.grdMailDeBingo.DataBind();
        this.pnlCount.DataBind();
	}

	private void GetData() {

        this.lblErrorMessageFrom.Visible = false;
        this.lblErrorMessageTo.Visible = false;
        this.lstFromYYYY.Enabled = true;
        this.lstFromMM.Enabled = true;
        this.lstFromDD.Enabled = true;
        this.lstFromHH.Enabled = true;
        this.lstFromMI.Enabled = true;
        this.lstToYYYY.Enabled = true;
        this.lstToMM.Enabled = true;
        this.lstToDD.Enabled = true;
        this.lstToHH.Enabled = true;
        this.lstToMI.Enabled = true;

        using (DbSession oDbSession = new DbSession()) {
            oDbSession.PrepareProcedure("MAIL_DE_BINGO_GET");
            oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.lstSiteCd.SelectedValue);
            oDbSession.ProcedureInParm("pBINGO_TERM_SEQ", DbSession.DbType.VARCHAR2, this.BingoTermSeq);
            oDbSession.ProcedureInParm("pSEX_CD", DbSession.DbType.VARCHAR2, this.SexCd);
            oDbSession.ProcedureOutParm("pBINGO_START_DATE", DbSession.DbType.DATE);
            oDbSession.ProcedureOutParm("pBINGO_END_DATE", DbSession.DbType.DATE);
            oDbSession.ProcedureOutParm("pJACKPOT", DbSession.DbType.NUMBER);
            oDbSession.ProcedureOutParm("pREMARKS", DbSession.DbType.VARCHAR2);
            oDbSession.ProcedureOutParm("pBINGO_APPLICATION_DATE", DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pBINGO_BALL_FLAG",DbSession.DbType.NUMBER);
            oDbSession.ProcedureOutParm("pROWID", DbSession.DbType.VARCHAR2);
            oDbSession.ProcedureOutParm("pREVISION_NO", DbSession.DbType.NUMBER);
            oDbSession.ProcedureOutParm("pRECORD_COUNT", DbSession.DbType.NUMBER);
            oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
            oDbSession.ExecuteProcedure();

            this.RevisionNo = oDbSession.GetStringValue("PREVISION_NO");
            this.Rowid = oDbSession.GetStringValue("PROWID");

            if (int.Parse(oDbSession.GetStringValue("PRECORD_COUNT")) > 0)
            {
                string sFromDate = string.Format("{0:yyyy/MM/dd HH:mm:ss}", oDbSession.GetDateTimeValue("pBINGO_START_DATE"));
                DateTime? oToDate = oDbSession.GetDateTimeValue("pBINGO_END_DATE");
                if (oToDate != null)
                {
                    oToDate = oToDate.Value.AddSeconds(1);
                }
                string sToDate = string.Format("{0:yyyy/MM/dd HH:mm:ss}", oToDate);
                if (!string.IsNullOrEmpty(sFromDate))
                {
                    this.lstFromYYYY.SelectedValue = sFromDate.Substring(0, 4);
                    this.lstFromMM.SelectedValue = sFromDate.Substring(5, 2);
                    this.lstFromDD.SelectedValue = sFromDate.Substring(8, 2);
                    this.lstFromHH.SelectedValue = sFromDate.Substring(11, 2);
                    if (this.lstFromMI.Items.FindByValue(sFromDate.Substring(14, 2)) != null)
                    {
                        this.lstFromMI.SelectedValue = sFromDate.Substring(14, 2);
                    }

                    if (DateTime.Parse(sFromDate) < DateTime.Now)
                    {
                        this.lstFromYYYY.Enabled = false;
                        this.lstFromMM.Enabled = false;
                        this.lstFromDD.Enabled = false;
                        this.lstFromHH.Enabled = false;
                        this.lstFromMI.Enabled = false;
                    }
                }

                if (!string.IsNullOrEmpty(sToDate))
                {
                    this.lstToYYYY.SelectedValue = sToDate.Substring(0, 4);
                    this.lstToMM.SelectedValue = sToDate.Substring(5, 2);
                    this.lstToDD.SelectedValue = sToDate.Substring(8, 2);
                    this.lstToHH.SelectedValue = sToDate.Substring(11, 2);
                    if (this.lstToMI.Items.FindByValue(sToDate.Substring(14, 2)) != null)
                    {
                        this.lstToMI.SelectedValue = sToDate.Substring(14, 2);
                    }

                    if (DateTime.Parse(sToDate) < DateTime.Now)
                    {
                        this.lstToYYYY.Enabled = false;
                        this.lstToMM.Enabled = false;
                        this.lstToDD.Enabled = false;
                        this.lstToHH.Enabled = false;
                        this.lstToMI.Enabled = false;
                    }
                }
                this.txtRemarks.Text = oDbSession.GetStringValue("pREMARKS");
                this.txtJackpot.Text = oDbSession.GetStringValue("pJACKPOT");

				if (oDbSession.GetStringValue("pBINGO_BALL_FLAG").Equals(ViCommConst.FLAG_ON_STR)) {
					rdoBingoBallFlagOn.Checked = true;
					rdoBingoBallFlagOff.Checked = false;
				} else {
					rdoBingoBallFlagOn.Checked = false;
					rdoBingoBallFlagOff.Checked = true;
				}

                this.btnDelete.Enabled = true;
            } else {
				rdoBingoBallFlagOn.Checked = false;
				rdoBingoBallFlagOff.Checked = true;

                this.btnDelete.Enabled = false;
            }
        }

        this.pnlMainte.Visible = true;
        this.pnlDtl.Visible = true;
        this.lstSeekSiteCd.SelectedValue = this.lstSiteCd.SelectedValue;
    }

	private bool CheckDate(out DateTime dtFrom, out DateTime dtTo) {
        
        dtFrom = new DateTime();
        dtTo = new DateTime();
        
        this.lblErrorMessageFrom.Visible = false;
        this.lblErrorMessageTo.Visible = false;
        if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:00",
            this.lstFromYYYY.Text, this.lstFromMM.SelectedValue, this.lstFromDD.SelectedValue, this.lstFromHH.SelectedValue, this.lstFromMI.SelectedValue), out dtFrom)) {
            this.lblErrorMessageFrom.Text = "日時を正しく入力してください";
            this.lblErrorMessageFrom.Visible = true;
            return false;
        }
        if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:00",
            this.lstToYYYY.Text, this.lstToMM.SelectedValue, this.lstToDD.SelectedValue, this.lstToHH.SelectedValue, this.lstToMI.SelectedValue), out dtTo)) {
            this.lblErrorMessageTo.Text = "日時を正しく入力してください";
            this.lblErrorMessageTo.Visible = true;
            return false;
        } else {
            dtTo = dtTo.AddSeconds(-1);
        }
        if (dtFrom > dtTo) {
            this.lblErrorMessageTo.Text = "日時の大小関係を正しく入力してください";
            this.lblErrorMessageTo.Visible = true;
            return false;
        }


        using (MailDeBingo oMailDeBingo = new MailDeBingo()) {
            if (oMailDeBingo.IsDuplicateDate(this.lstSiteCd.SelectedValue, this.SexCd, dtFrom, dtTo, this.BingoTermSeq)) {
                this.lblErrorMessageTo.Text = "日時が他のメールdeビンゴ開催設定と重複しています";
                this.lblErrorMessageTo.Visible = true;
                return false;
            }
        }
        
		return true;
	}

	private void UpdateData(int pDelFlag) {
        DateTime dtFrom = new DateTime();
        DateTime dtTo = new DateTime();
        if (pDelFlag == 0) {
            if (!this.CheckDate(out dtFrom, out dtTo)) {
                return;
            }
        }

		int iBingoBallFlag = ViCommConst.FLAG_OFF;

		if (rdoBingoBallFlagOn.Checked == true) {
			iBingoBallFlag = ViCommConst.FLAG_ON;
		}

        using (DbSession oDbSession = new DbSession()) {
            string sJackpot = this.txtJackpot.Text.Trim();
            if (string.IsNullOrEmpty(sJackpot)) {
                sJackpot = "0";
            }
            oDbSession.PrepareProcedure("MAIL_DE_BINGO_MAINTE");
            oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.lstSiteCd.SelectedValue);
            oDbSession.ProcedureInParm("pBIGNO_TERM_SEQ", DbSession.DbType.VARCHAR2, this.BingoTermSeq);
            oDbSession.ProcedureInParm("pSEX_CD", DbSession.DbType.VARCHAR2, this.SexCd);
            oDbSession.ProcedureInParm("pBINGO_START_DATE", DbSession.DbType.DATE, dtFrom);
            oDbSession.ProcedureInParm("pBINGO_END_DATE", DbSession.DbType.DATE, dtTo);
            oDbSession.ProcedureInParm("pJACKPOT", DbSession.DbType.NUMBER, int.Parse(sJackpot));
            oDbSession.ProcedureInParm("pREMARKS", DbSession.DbType.VARCHAR2, this.txtRemarks.Text.Trim());
            oDbSession.ProcedureInParm("pBINGO_APPLICATION_DATE", DbSession.DbType.DATE, null);
			oDbSession.ProcedureInParm("pBINGO_BALL_FLAG",DbSession.DbType.NUMBER,iBingoBallFlag);
            oDbSession.ProcedureInParm("pROWID", DbSession.DbType.VARCHAR2, this.Rowid);
            oDbSession.ProcedureInParm("pREVISION_NO", DbSession.DbType.NUMBER, decimal.Parse(this.RevisionNo));
            oDbSession.ProcedureInParm("pDEL_FLAG", DbSession.DbType.NUMBER, pDelFlag);
            oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
            oDbSession.ExecuteProcedure();
        }
        this.InitPage();
        this.GetList();
        this.lstSeekSiteCd.SelectedValue = this.lstSiteCd.SelectedValue;

	}

    protected string GetPrizeGetNm(object pSexCd) {
        string sPrizeGetNm = string.Empty;
        if (iBridUtil.GetStringValue(pSexCd).Equals(ViCommConst.MAN)) {
            sPrizeGetNm = "男性獲得一覧";
        } else {
            sPrizeGetNm = "女性獲得一覧";
        }

        return sPrizeGetNm;
    }

    protected string SubStringRemarks(object pRemarks) {
        string sRemarks = iBridUtil.GetStringValue(pRemarks);

        if (sRemarks.Length > 10) {
            sRemarks = sRemarks.Substring(0, 10);
        }

        return sRemarks;
    }

	protected string GetBingoBallFlagStr(object pBingoBallFlag) {
		string sValue = string.Empty;
		string sBingoBallFlag = iBridUtil.GetStringValue(pBingoBallFlag);

		if (sBingoBallFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			sValue = "有効";
		} else {
			sValue = "無効";
		}

		return sValue;
	}

	protected Color GetBingoBallFlagForeColor(object pBingoBallFlag) {
		Color foreColor = Color.Empty;
		string sBingoBallFlag = iBridUtil.GetStringValue(pBingoBallFlag);

		if (sBingoBallFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			foreColor = Color.Red;
		}

		return foreColor;
	}
}
