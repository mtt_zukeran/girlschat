﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="DailyServiceReport.aspx.cs" Inherits="Extension_DailyServiceReport"
    Title="日別サービスレポート" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="日別サービスレポート"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset>
                <legend>[設定]</legend>
                <table border="0" style="width: 640px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            サイトコード
                        </td>
                        <td class="tdDataStyle" colspan="3">
                            <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="180px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            集計日
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstYYYY" runat="server" Width="54px">
                            </asp:DropDownList>年
                            <asp:DropDownList ID="lstMM" runat="server" Width="43px">
                            </asp:DropDownList>月
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    CausesValidation="False" />
                <asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" CausesValidation="False" OnClick="btnCSV_Click" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[日別サービスレポート]</legend>
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="552px">
                    <asp:GridView ID="grdDailyServiceReport" runat="server" AutoGenerateColumns="False" DataSourceID="dsDailyServiceReport"
                        AllowSorting="true" SkinID="GridViewColor" ShowFooter="True" OnRowDataBound="grdDailyServiceReport_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="年月日">
                                <ItemTemplate>
                                    <asp:Label ID="lblReportDay" runat="server" Text='<%# Eval("REPORT_DAY") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                                <FooterTemplate>
                                    <asp:Label ID="lblReportDaySum" runat="server" Text="合計"></asp:Label>
                                </FooterTemplate>
                                <FooterStyle HorizontalAlign="Center" Font-Bold="true" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PV">
                                <ItemTemplate>
                                    <asp:Label ID="lblPageViewCount" runat="server" Text='<%# Eval("PAGE_VIEW_COUNT", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                                <FooterTemplate>
                                    <asp:Label ID="lblPageViewCountSum" runat="server"></asp:Label>
                                </FooterTemplate>
                                <FooterStyle HorizontalAlign="Right" Width="80px" Font-Bold="true" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="UU">
                                <ItemTemplate>
                                    <asp:Label ID="lblPageViewUniqueCount" runat="server" Text='<%# Eval("PAGE_VIEW_UNIQUE_COUNT", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                                <FooterTemplate>
                                    <asp:Label ID="lblPageViewUniqueCountSum" runat="server"></asp:Label>
                                </FooterTemplate>
                                <FooterStyle HorizontalAlign="Right" Font-Bold="true" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="消費UU">
                                <ItemTemplate>
                                    <asp:Label ID="lblUsedPointCountUnique" runat="server" Text='<%# Eval("USED_POINT_COUNT_UNIQUE", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                                <FooterTemplate>
                                    <asp:Label ID="lblUsedPointCountUniqueSum" runat="server" Text="-"></asp:Label>
                                </FooterTemplate>
                                <FooterStyle Font-Bold="true" HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="消費率">
                                <ItemTemplate>
                                    <asp:Label ID="lblConsumptionRate" runat="server" Text='<%# Eval("CONSUMPTION_RATE", "{0:N2}%") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                                <FooterTemplate>
                                    <asp:Label ID="lblConsumptionRateSum" runat="server" Text="-"></asp:Label>
                                </FooterTemplate>
                                <FooterStyle Font-Bold="true" HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ARPPU">
                                <ItemTemplate>
                                    <asp:Label ID="lblArppu" runat="server" Text='<%# Eval("ARPPU", "{0:N2}Pt") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                                <FooterTemplate>
                                    <asp:Label ID="lblArppuSum" runat="server" Text="-"></asp:Label>
                                </FooterTemplate>
                                <FooterStyle Font-Bold="true" HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ARPU">
                                <ItemTemplate>
                                    <asp:Label ID="lblArpu" runat="server" Text='<%# Eval("ARPU", "{0:N2}Pt") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                                <FooterTemplate>
                                    <asp:Label ID="lblArpuSum" runat="server" Text="-"></asp:Label>
                                </FooterTemplate>
                                <FooterStyle Font-Bold="true" HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="入会数">
                                <ItemTemplate>
                                    <asp:Label ID="lblRegistCount" runat="server" Text='<%# Eval("REGIST_COUNT", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                                <FooterTemplate>
                                    <asp:Label ID="lblRegistCountSum" runat="server"></asp:Label>
                                </FooterTemplate>
                                <FooterStyle HorizontalAlign="Right" Font-Bold="true" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="退会数">
                                <ItemTemplate>
                                    <asp:Label ID="lblWithdrawalCount" runat="server" Text='<%# Eval("WITHDRAWAL_COUNT", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                                <FooterTemplate>
                                    <asp:Label ID="lblWithdrawalCountSum" runat="server"></asp:Label>
                                </FooterTemplate>
                                <FooterStyle HorizontalAlign="Right" Font-Bold="true" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ﾎﾟｲﾝﾄ利用回数">
                                <ItemTemplate>
                                    <asp:Label ID="lblUsedPointCount" runat="server" Text='<%# Eval("USED_POINT_COUNT", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="100px" />
                                <FooterTemplate>
                                    <asp:Label ID="lblUsedPointCountSum" runat="server"></asp:Label>
                                </FooterTemplate>
                                <FooterStyle HorizontalAlign="Right" Font-Bold="true" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="売上">
                                <ItemTemplate>
                                    <asp:Label ID="lblUsedPoint" runat="server" Text='<%# Eval("USED_POINT", "{0:N0}Pt") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                                <FooterTemplate>
                                    <asp:Label ID="lblUsedPointSum" runat="server"></asp:Label>
                                </FooterTemplate>
                                <FooterStyle HorizontalAlign="Right" Font-Bold="true" />
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle ForeColor="Black" BackColor="LightYellow" Font-Bold="true" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsDailyServiceReport" runat="server" SelectMethod="GetListServiseReport"
        OnSelecting="dsDailyServiceReport_Selecting" TypeName="DailyKpiReport">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSeekSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:Parameter Name="pReportMonth" Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
