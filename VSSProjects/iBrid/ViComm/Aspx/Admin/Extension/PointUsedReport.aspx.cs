﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ポイント明細レポート

--	Progaram ID		: PointUsedReport
--
--  Creation Date	: 2011.08.11
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using iBridCommLib;
using System.Drawing;
using ViComm;
using ViComm.Extension.Pwild;

public partial class Extension_PointUsedReport : System.Web.UI.Page {
	private static readonly int iGameCategoryColumnIndex = 4;
	private static readonly int iGameItemNmColunIndex = 5;
	private static readonly int iBuyQuantityColumnIndex = 6;
	private static readonly int iDatePlanNmColumnIndex = 7;
	private static readonly int iLotteryNmColumnIndex = 8;

	private int iBuyQuantitySum = 0;
	private int iUsedPointSum = 0;

	protected string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		private set {
			this.ViewState["SexCd"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		if (!this.IsPostBack) {
			this.SexCd = this.Request.QueryString["sexcd"];
			if (ViCommConst.MAN.Equals(this.SexCd)) {
				this.lblPgmTitle.Text = "男性" + this.lblPgmTitle.Text;
			} else {
				this.lblPgmTitle.Text = "女性" + this.lblPgmTitle.Text;
			}
			this.Title = this.lblPgmTitle.Text;

			this.InitPage();

			this.GetList();
			this.pnlInfo.Visible = true;
		}

		this.grdPointUsedReport.DataSourceID = string.Empty;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsCorrectInput()) {
			return;
		}
		this.GetList();
		this.pnlInfo.Visible = true;
	}

	protected void dsPointUsedReport_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pReportDayFrom"] = string.Format("{0}/{1}/{2}",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue);
		e.InputParameters["pReportDayTo"] = string.Format("{0}/{1}/{2}",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,this.lstToDD.SelectedValue);
	}

	protected void grdPointUsedReport_DataBound(object sender,EventArgs e) {
		switch (this.lstGameCharge.SelectedValue) {
			case PwViCommConst.GameCharge.ITEM:
				this.grdPointUsedReport.Columns[iGameCategoryColumnIndex].Visible = true;
				this.grdPointUsedReport.Columns[iGameItemNmColunIndex].Visible = true;
				this.grdPointUsedReport.Columns[iBuyQuantityColumnIndex].Visible = true;
				this.grdPointUsedReport.Columns[iDatePlanNmColumnIndex].Visible = false;
				this.grdPointUsedReport.Columns[iLotteryNmColumnIndex].Visible = false;
				break;
			case PwViCommConst.GameCharge.DATE:
				this.grdPointUsedReport.Columns[iGameCategoryColumnIndex].Visible = false;
				this.grdPointUsedReport.Columns[iGameItemNmColunIndex].Visible = false;
				this.grdPointUsedReport.Columns[iBuyQuantityColumnIndex].Visible = false;
				this.grdPointUsedReport.Columns[iDatePlanNmColumnIndex].Visible = true;
				this.grdPointUsedReport.Columns[iLotteryNmColumnIndex].Visible = false;
				break;
			case PwViCommConst.GameCharge.LOTTERY:
				this.grdPointUsedReport.Columns[iGameCategoryColumnIndex].Visible = false;
				this.grdPointUsedReport.Columns[iGameItemNmColunIndex].Visible = false;
				this.grdPointUsedReport.Columns[iBuyQuantityColumnIndex].Visible = false;
				this.grdPointUsedReport.Columns[iDatePlanNmColumnIndex].Visible = false;
				this.grdPointUsedReport.Columns[iLotteryNmColumnIndex].Visible = true;
				break;
		}
	}

	protected void grdPointUsedReport_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			iBuyQuantitySum += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"BUY_QUANTITY"));
			iUsedPointSum += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"USED_POINT"));

		} else if (e.Row.RowType == DataControlRowType.Footer) {
			Label lblBuyQuantitySum = e.Row.FindControl("lblBuyQuantitySum") as Label;
			Label lblUsedPointSum = e.Row.FindControl("lblUsedPointSum") as Label;

			lblBuyQuantitySum.Text = iBuyQuantitySum.ToString("N0");
			lblUsedPointSum.Text = string.Concat(iUsedPointSum.ToString("N0"),"Pt");
		}
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	protected void lstGameCharge_DataBound(object sender,EventArgs e) {
		if (ViCommConst.OPERATOR.Equals(this.SexCd)) {
			ListItem oListItem = this.lstGameCharge.Items.FindByValue(PwViCommConst.GameCharge.DATE);
			if (oListItem != null) {
				this.lstGameCharge.Items.Remove(oListItem);
			}
		}
	}

	protected void lstGameCharge_IndexChanged(object sender,EventArgs e) {
		if (PwViCommConst.GameCharge.ITEM.Equals(this.lstGameCharge.SelectedValue)) {
			this.lstGameItemCategory.Enabled = true;
		} else {
			this.lstGameItemCategory.SelectedIndex = 0;
			this.lstGameItemCategory.Enabled = false;
		}
	}

	private void InitPage() {
		this.grdPointUsedReport.DataSourceID = string.Empty;
		this.pnlKey.DataBind();
		this.pnlInfo.Visible = false;
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		SysPrograms.SetupFromToDay(this.lstFromYYYY,this.lstFromMM,this.lstFromDD,this.lstToYYYY,this.lstToMM,this.lstToDD,false);
		this.lstFromYYYY.SelectedValue = DateTime.Today.ToString("yyyy");
		this.lstFromMM.SelectedValue = DateTime.Today.ToString("MM");
		this.lstFromDD.SelectedValue = DateTime.Today.ToString("dd");
		this.lstToYYYY.SelectedValue = DateTime.Today.ToString("yyyy");
		this.lstToMM.SelectedValue = DateTime.Today.ToString("MM");
		this.lstToDD.SelectedValue = DateTime.Today.ToString("dd");
		this.lblErrorMessageStartDate.Visible = false;
		this.lblErrorMessageEndDate.Visible = false;
	}

	private void GetList() {
		this.grdPointUsedReport.DataSourceID = "dsPointUsedReport";
		this.grdPointUsedReport.DataBind();
	}

	protected string GetViewUrl(object pSiteCd,object pLoginId) {
		if (ViCommConst.MAN.Equals(this.SexCd)) {
			return string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",pSiteCd,pLoginId);
		} else {
			return string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}",pLoginId,"PointUsedReport.aspx");
		}
	}

	protected void btnCSV_Click(object sender,EventArgs e) {

		if (grdPointUsedReport.Rows.Count == 0) {
			return;
		}

		DataTable oCsvData;
		using (PointUsedLog oPointUsedLog = new PointUsedLog()) {
			using (DataSet oDataSet = oPointUsedLog.GetListReport(
				this.lstSeekSiteCd.SelectedValue,
				this.SexCd,
				string.Format("{0}/{1}/{2}",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue),
				string.Format("{0}/{1}/{2}",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,this.lstToDD.SelectedValue),
				this.lstGameCharge.SelectedValue,
				this.lstGameItemCategory.SelectedValue)) {

				if (oDataSet.Tables[0].Rows.Count > 0) {
					oCsvData = oDataSet.Tables[0];
				} else {
					return;
				}
			}
		}

		//ヘッダ作成
		string sHeader = null;
		switch (this.lstGameCharge.SelectedValue) {
			case PwViCommConst.GameCharge.ITEM:
				sHeader = "利用日時,ﾕｰｻﾞｰID,ﾊﾝﾄﾞﾙ名,ｹﾞｰﾑﾊﾝﾄﾞﾙ名,ｱｲﾃﾑｶﾃｺﾞﾘ,ｱｲﾃﾑ名,購入個数,利用ﾎﾟｲﾝﾄ";
				break;
			case PwViCommConst.GameCharge.DATE:
				sHeader = "利用日時,ﾕｰｻﾞｰID,ﾊﾝﾄﾞﾙ名,ｹﾞｰﾑﾊﾝﾄﾞﾙ名,ﾃﾞｰﾄ名,利用ﾎﾟｲﾝﾄ";
				break;
			case PwViCommConst.GameCharge.LOTTERY:
				sHeader = "利用日時,ﾕｰｻﾞｰID,ﾊﾝﾄﾞﾙ名,ｹﾞｰﾑﾊﾝﾄﾞﾙ名,抽選名,利用ﾎﾟｲﾝﾄ";
				break;
		}
		
		//CSV出力 
		Response.ContentType = "application/download";
		Response.AppendHeader("Content-Disposition","attachment;filename=PointUsedReport.csv");
		Response.ContentEncoding = System.Text.Encoding.GetEncoding("Shift_JIS");

		Response.Write(sHeader + "\r\n");
		foreach (DataRow oCsvRow in oCsvData.Rows) {
			if (string.IsNullOrEmpty(oCsvRow["LOTTERY_NM"].ToString())) {
				oCsvRow["LOTTERY_NM"] = "未設定";
			}
			string sData = null;
			switch (this.lstGameCharge.SelectedValue) {
				case PwViCommConst.GameCharge.ITEM:
					sData =
					SetCsvString(oCsvRow["POINT_USED_DATE"].ToString()) + "," +
					SetCsvString(oCsvRow["LOGIN_ID"].ToString()) + "," +
					SetCsvString(oCsvRow["HANDLE_NM"].ToString()) + "," +
					SetCsvString(oCsvRow["GAME_HANDLE_NM"].ToString()) + "," +
					SetCsvString(oCsvRow["GAME_ITEM_CATEGORY_NM"].ToString()) + "," +
					SetCsvString(oCsvRow["GAME_ITEM_NM"].ToString()) + "," +
					SetCsvString(oCsvRow["BUY_QUANTITY"].ToString()) + "," +
					SetCsvString(oCsvRow["USED_POINT"].ToString()) + "Pt";
					break;
				case PwViCommConst.GameCharge.DATE:
					sData =
					SetCsvString(oCsvRow["POINT_USED_DATE"].ToString()) + "," +
					SetCsvString(oCsvRow["LOGIN_ID"].ToString()) + "," +
					SetCsvString(oCsvRow["HANDLE_NM"].ToString()) + "," +
					SetCsvString(oCsvRow["GAME_HANDLE_NM"].ToString()) + "," +
					SetCsvString(oCsvRow["DATE_PLAN_NM"].ToString()) + "," +
					SetCsvString(oCsvRow["USED_POINT"].ToString()) + "Pt";
					break;
				case PwViCommConst.GameCharge.LOTTERY:
					sData =
					SetCsvString(oCsvRow["POINT_USED_DATE"].ToString()) + "," +
					SetCsvString(oCsvRow["LOGIN_ID"].ToString()) + "," +
					SetCsvString(oCsvRow["HANDLE_NM"].ToString()) + "," +
					SetCsvString(oCsvRow["GAME_HANDLE_NM"].ToString()) + "," +
					SetCsvString(oCsvRow["LOTTERY_NM"].ToString()) + "," +
					SetCsvString(oCsvRow["USED_POINT"].ToString()) + "Pt";
					break;
			}

			Response.Write(sData + "\r\n");
		}
		Response.End();
	}

	private string SetCsvString(string pData) {
		if (pData.IndexOf('"') > -1 ||
			pData.IndexOf(',') > -1 ||
			pData.IndexOf('\r') > -1 ||
			pData.IndexOf('\n') > -1 ||
			pData.StartsWith(" ") || pData.StartsWith("\t") ||
			pData.EndsWith(" ") || pData.EndsWith("\t")) {
			if (pData.IndexOf('"') > -1) {
				//"を""とする
				pData = pData.Replace("\"","\"\"");
			}
			if (pData.IndexOf('\r') > -1 || pData.IndexOf('\n') > -1) {
				//改行しない 
				pData = pData.Replace("\r","");
				pData = pData.Replace("\n","");
			}
			pData = "\"" + pData + "\"";
		}
		return pData;
	}

	private bool IsCorrectInput() {
		bool bResult = true;
		this.lblErrorMessageStartDate.Visible = false;
		this.lblErrorMessageEndDate.Visible = false;
		
		DateTime oDate;
		if (!DateTime.TryParse(string.Format("{0}/{1}/{2}",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue),out oDate)) {
			this.lblErrorMessageStartDate.Text = "入力書式が正しくありません。";
			this.lblErrorMessageStartDate.Visible = true;
			bResult = false;
		}
		if (!DateTime.TryParse(string.Format("{0}/{1}/{2}",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,this.lstToDD.SelectedValue),out oDate)) {
			this.lblErrorMessageEndDate.Text = "入力書式が正しくありません。";
			this.lblErrorMessageEndDate.Visible = true;
			bResult = false;
		}
		return bResult;
	}
}