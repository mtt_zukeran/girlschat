﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="BlogArticleView.aspx.cs" Inherits="Extension_BlogArticleView" Title="ブログ投稿管理＞書込詳細" %>

<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="iBridCommLib" %>
<%@ Import Namespace="System" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="ブログ投稿管理＞書込詳細"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlDtl" HorizontalAlign="Left">
            <asp:Panel runat="server" ID="pnlInfo">
                <fieldset class="fieldset-inner">
                    <asp:DetailsView ID="dvwBlogArticle" runat="server" DataSourceID="dsBlogArticle"
                        AutoGenerateRows="False" SkinID="DetailsView" Width="1000px">
                        <Fields>
                            <asp:TemplateField HeaderText="作成者">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblHandleNm" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}",Eval("LOGIN_ID"),"WantedApplicantList.aspx") %>'
                                        Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:HyperLink><br />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="タイトル">
                                <ItemTemplate>
                                    <asp:Label ID="lblBlogArticleTitle" runat="server" Text='<%# Eval("BLOG_ARTICLE_TITLE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="内容">
                                <ItemTemplate>
                                    <asp:Label ID="lblBlogArticleBody" runat="server" Text='<%# string.Concat(Eval("BLOG_ARTICLE_BODY1"), Eval("BLOG_ARTICLE_BODY2"), Eval("BLOG_ARTICLE_BODY3"), Eval("BLOG_ARTICLE_BODY4"), Eval("BLOG_ARTICLE_BODY5")).Replace(System.Environment.NewLine, "<br />") %>'
                                        CssClass="Warp" Width="800px"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="下書き作成日時" SortExpression="REGIST_DATE">
                                <ItemTemplate>
                                    <asp:Label ID="lblRegistDate" runat="server" Text='<%# Eval("REGIST_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ファイル">
                                <ItemTemplate>
                                    <asp:Repeater ID="rptAttachedFile" runat="server" DataSourceID="dsBlogArticleObjs">
                                        <ItemTemplate>
                                            <asp:PlaceHolder ID="plcImage" runat="server" Visible='<%# GetImageVisible(Eval("BLOG_FILE_TYPE")) %>'>
                                                <asp:Image ID="imgObj" runat="server" ImageUrl='<%# Eval("PHOTO_IMG_PATH", "../{0}") %>'
                                                    ImageAlign="NotSet" />
                                            </asp:PlaceHolder>
                                            <asp:PlaceHolder ID="plcMovie" runat="server" Visible='<%# GetMovieVisible(Eval("BLOG_FILE_TYPE")) %>'>
                                                <object classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" codebase="http://www.apple.com/qtactivex/qtplugin.cab"
                                                    height="200" width="240">
                                                    <param name="src" value='<%# GetMovieLink(Eval("SITE_CD"), Eval("LOGIN_ID"), Eval("OBJ_SEQ")) %>'>
                                                    <param name="autoplay" value="false">
                                                    <param name="controller" value="true">
                                                    <embed autoplay="false" controller="true" height="200" pluginspage="http://www.apple.com/quicktime/download/"
                                                        src='<%# GetMovieLink(Eval("SITE_CD"), Eval("LOGIN_ID"), Eval("OBJ_SEQ")) %>'
                                                        type="video/quicktime" width="240"></embed>
                                                </object>
                                            </asp:PlaceHolder>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="公開状態">
                                <ItemTemplate>
                                    <asp:Label ID="lblBlogArticleStatus" runat="server" Text='<%# GetStatusMark(Eval("BLOG_ARTICLE_STATUS"),Eval("DEL_FLAG")) %>'></embed></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="公開操作日時" SortExpression="PUBLISH_START_DATE">
                                <ItemTemplate>
                                    <asp:Label ID="lblPublishStartDate" runat="server" Text='<%# Eval("PUBLISH_START_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ポイント付与状況">
                                <ItemTemplate>
                                    <asp:Label ID="lblPointAcquired" runat="server" Text='<%# GetPointAcquiredMark(Eval("POINT_ACQUIRED_FLAG")) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="操作">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkSetPublish" runat="server" CommandArgument='<%# Container.DataItemIndex %>'
                                        Text='<%# GetSetPublishLinkText(Eval("BLOG_ARTICLE_STATUS")) %>' OnCommand="lnkSetPublish_OnCommand"></asp:LinkButton>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="lnkSetDraft" runat="server"  CommandArgument='<%# Container.DataItemIndex %>' Visible='<%# GetDraftVisible(Eval("BLOG_ARTICLE_STATUS")) %>'
                                        Text="[下書きに戻す]" OnCommand="lnkSetDraft_OnCommend"></asp:LinkButton>
                                    <br />
                                    <asp:LinkButton ID="lnkSetUnCheck" runat="server" CommandArgument='<%# Container.DataItemIndex %>'
                                        Text="[未チェック状態に戻す]" OnCommand="lnkSetUnCheck_OnCommand"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="オススメ設定">
                                <ItemTemplate>
                                    <table border="0" cellspacing="0px">
                                        <tr>
                                            <td>
                                                <asp:RadioButton ID="rdoPickupOFF" runat="server" GroupName="pickup" Text=" しない" Checked="true" /><br />
                                                <asp:RadioButton ID="rdoPickupON" runat="server" GroupName="pickup" Text="" />
                                                <asp:DropDownList ID="lstPickup" runat="server" DataSourceID="dsBlogArticlePickup"
                                                    Width="70px" AutoPostBack="true" DataTextField="PRIORITY_TEXT" DataValueField="PRIORITY"
                                                    OnDataBound="lstPickup_DataBound">
                                                </asp:DropDownList>に挿入する
                                            </td>
                                            <td style="width:100px">
                                                <asp:Button ID="btnSetPriority" runat="server" Text="設定" OnCommand="btnSetPriority_OnCommand"
                                                    CommandArgument='<%# Eval("PRIORITY") %>' />
                                            </td>
                                            <td class="VerHeaderStyle" style="width:60px">
                                                現在
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPriority" runat="server" Text='<%# GetPriorityMark(Eval("PRIORITY")) %>'></asp:Label><br />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Fields>
                        <HeaderStyle Wrap="False" />
                    </asp:DetailsView>
                </fieldset>
            </asp:Panel>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsBlogArticlePickup" runat="server" SelectMethod="GetPriorityList"
        TypeName="BlogArticlePickup">
        <SelectParameters>
            <asp:QueryStringParameter Name="pSiteCd" QueryStringField="sitecd" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsBlogArticle" runat="server" SelectMethod="GetOne" TypeName="BlogArticle"
        OnSelected="dsBlogArticle_Selected">
        <SelectParameters>
            <asp:QueryStringParameter Name="pSiteCd" QueryStringField="sitecd" Type="String" />
            <asp:QueryStringParameter Name="pUserSeq" QueryStringField="userseq" Type="String" />
            <asp:QueryStringParameter Name="pUserCharNo" QueryStringField="usercharno" Type="String" />
            <asp:QueryStringParameter Name="pBlogSeq" QueryStringField="blogseq" Type="String" />
            <asp:QueryStringParameter Name="pBlogArticleSeq" QueryStringField="blogarticleseq"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsBlogArticleObjs" runat="server" SelectMethod="GetList"
        TypeName="BlogArticleObjs">
        <SelectParameters>
            <asp:QueryStringParameter Name="pSiteCd" QueryStringField="sitecd" Type="String" />
            <asp:QueryStringParameter Name="pUserSeq" QueryStringField="userseq" Type="String" />
            <asp:QueryStringParameter Name="pUserCharNo" QueryStringField="usercharno" Type="String" />
            <asp:QueryStringParameter Name="pBlogSeq" QueryStringField="blogseq" Type="String" />
            <asp:QueryStringParameter Name="pBlogArticleSeq" QueryStringField="blogarticleseq"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
