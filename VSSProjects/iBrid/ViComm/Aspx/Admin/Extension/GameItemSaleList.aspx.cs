﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: セールアイテム一覧

--	Progaram ID		: GameItemSaleList
--
--  Creation Date	: 2011.07.25
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Ratelain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.IO;

public partial class Extension_GameItemSaleList : System.Web.UI.Page {
	protected string[] DummyArray = new string[3];
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		set {
			this.ViewState["SexCd"] = value;
		}
	}

	private string SaleScheduleSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SaleScheduleSeq"]);
		}
		set {
			this.ViewState["SaleScheduleSeq"] = value;
		}
	}

	private string GameItemSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemSeq"]);
		}
		set {
			this.ViewState["GameItemSeq"] = value;
		}
	}

	private string GameItemCategoryType {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemCategoryType"]);
		}
		set {
			this.ViewState["GameItemCategoryType"] = value;
		}
	}

	private string CreateFlag {
		get {
			return iBridUtil.GetStringValue(this.ViewState["CreateFlag"]);
		}
		set {
			this.ViewState["CreateFlag"] = value;
		}
	}

	private string GameItemGetCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemGetCd"]);
		}
		set {
			this.ViewState["GameItemGetCd"] = value;
		}
	}

	private string GameItemPresent {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemPresent"]);
		}
		set {
			this.ViewState["GameItemPresent"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();

			this.SiteCd = iBridUtil.GetStringValue(this.Request.QueryString["sitecd"]);
			this.SaleScheduleSeq = iBridUtil.GetStringValue(this.Request.QueryString["salescheduleseq"]);
			this.SexCd = iBridUtil.GetStringValue(this.Request.QueryString["sexcd"]);
			this.CreateFlag = iBridUtil.GetStringValue(this.Request.QueryString["create"]);

			if (!string.IsNullOrEmpty(this.SiteCd) && !string.IsNullOrEmpty(this.SaleScheduleSeq)) {
				this.lstSiteCd.SelectedValue = this.SiteCd;

				if (this.CreateFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					this.GameItemSeq = string.Empty;
					this.pnlKey.Enabled = false;
					this.pnlItemInfo.Visible = true;
				}
				else {
					this.GetList();
				}
			}
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
		this.GameItemSeq = string.Empty;
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnBack_Click(object sender,EventArgs e) {
		this.Response.Redirect(string.Concat("~/Extension/GameItemSaleScheduleList.aspx?sexcd=",this.SexCd));
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		if (!this.IsCorrectInput()) {
			return;
		}
		this.UpdateData(false);
		this.pnlItemInfo.Visible = false;
		this.pnlKey.Enabled = true;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		this.UpdateData(true);
		this.pnlItemInfo.Visible = false;
		this.pnlKey.Enabled = true;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlItemInfo.Visible = false;
		this.pnlKey.Enabled = true;
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.GameItemSeq = string.Empty;
		this.CreateFlag = ViCommConst.FLAG_ON_STR;
		this.pnlKey.Enabled = false;
		this.pnlItemInfo.Visible = true;
		this.ClearFileds();
	}

	protected void lnkEdit_Command(object sender,CommandEventArgs e) {
		
		this.GameItemGetCd = null;
		this.GameItemPresent = null;
		
		if (!this.IsValid) {
			return;
		}
		int iIndex = int.Parse(iBridUtil.GetStringValue(e.CommandArgument));

		GridViewRow oRow = this.grdGameItemSale.Rows[iIndex];
		HiddenField oSaleScheduleSeqHiddenField = oRow.FindControl("hdnSaleScheduleSeq") as HiddenField;
		HiddenField oGameItemSeqHiddenFiled = oRow.FindControl("hdnGameItemSeq") as HiddenField;

		this.SaleScheduleSeq = oSaleScheduleSeqHiddenField.Value;
		this.GameItemSeq = oGameItemSeqHiddenFiled.Value;
		this.CreateFlag = string.Empty;
		this.pnlKey.Enabled = false;
		this.GetData();

		this.pnlItemInfo.Visible = true;
	}

	protected void dsGameItemSale_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	protected void lstGameItemCategory_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		this.GameItemCategoryType = oDropDownList.SelectedValue;
		this.lstGameItem.DataBind();
	}

	protected void lstItemGetCd_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		this.GameItemGetCd = oDropDownList.SelectedValue;
		this.lstGameItem.DataBind();
	}

	protected void lstItemPresent_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		this.GameItemPresent = oDropDownList.SelectedValue;
		this.lstGameItem.DataBind();
	}
	
	protected void dsGameItem_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pGameItemCategoryType"] = this.GameItemCategoryType;
		e.InputParameters["pGameItemGetCd"] = this.GameItemGetCd;
		e.InputParameters["pPresentFlag"] = this.GameItemPresent;
	}

	private void InitPage() {
		this.pnlInfo.Visible = true;
		this.pnlItemInfo.Visible = false;

		this.SaleScheduleSeq = string.Empty;
		this.GameItemSeq = string.Empty;
		this.pnlKey.Enabled = true;
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void ClearFileds() {
		this.txtDiscountRate.Text = string.Empty;
		this.lstGameItemCategory.DataBind();
		this.lstGameItem.DataBind();
		this.rdoNoticeDisp.DataBind();
		this.lblErrorMessageItem.Visible = false;
		this.lblErrorMessageRate.Visible = false;
		this.lstItemGetCd.SelectedValue = null;
		this.lstItemPresent.SelectedValue = null;
		this.GameItemGetCd = null;
		this.GameItemPresent = null;
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	protected string GetNoticeDispFlagMark(object pNoticeDispFlag) {
		return ViCommConst.FLAG_ON_STR.Equals(iBridUtil.GetStringValue(pNoticeDispFlag)) ? "有り" : "無し";
	}

	private void GetList() {
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.GameItemSeq = string.Empty;

		this.grdGameItemSale.PageIndex = 0;
		this.grdGameItemSale.DataSourceID = "dsGameItemSale";
		this.grdGameItemSale.DataBind();
		this.pnlCount.DataBind();
	}

	private void GetData() {
		this.ClearFileds();

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("GAME_ITEM_SALE_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pSALE_SCHEDULE_SEQ",DbSession.DbType.VARCHAR2,this.SaleScheduleSeq);
			oDbSession.ProcedureInParm("pGAME_ITEM_SEQ",DbSession.DbType.VARCHAR2,this.GameItemSeq);
			oDbSession.ProcedureOutParm("pGAME_ITEM_CATEGORY_TYPE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pDISCOUNT_RATE",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pNOTICE_DISP_FLAG",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.txtDiscountRate.Text = oDbSession.GetStringValue("pDISCOUNT_RATE");
			this.lstGameItemCategory.SelectedValue = oDbSession.GetStringValue("pGAME_ITEM_CATEGORY_TYPE");
			this.GameItemCategoryType = this.lstGameItemCategory.SelectedValue;
			this.lstGameItem.DataBind();
			this.lstGameItem.SelectedValue = this.GameItemSeq;
			this.rdoNoticeDisp.SelectedValue = oDbSession.GetStringValue("pNOTICE_DISP_FLAG");
		}
	}

	private void UpdateData(bool pDeleteFlag) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("GAME_ITEM_SALE_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pSALE_SCHEDULE_SEQ",DbSession.DbType.VARCHAR2,this.SaleScheduleSeq);
			oDbSession.ProcedureInParm("pGAME_ITEM_SEQ",DbSession.DbType.VARCHAR2,this.lstGameItem.SelectedValue);
			oDbSession.ProcedureInParm("pDISCOUNT_RATE",DbSession.DbType.NUMBER,this.txtDiscountRate.Text);
			oDbSession.ProcedureInParm("pNOTICE_DISP_FLAG",DbSession.DbType.NUMBER,this.rdoNoticeDisp.SelectedValue);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDeleteFlag ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.ClearFileds();
		this.GetList();
	}

	private bool IsCorrectInput() {
		this.lblErrorMessageItem.Visible = false;
		this.lblErrorMessageRate.Visible = false;

		bool bResult = true;

		if (string.IsNullOrEmpty(this.txtDiscountRate.Text)) {
			this.lblErrorMessageRate.Text = "割引率を入力してください。";
			this.lblErrorMessageRate.Visible = true;
			bResult = false;
		}

		if (string.IsNullOrEmpty(this.lstGameItem.SelectedValue)) {
			this.lblErrorMessageItem.Text = "セールアイテムを選択してください。";
			this.lblErrorMessageItem.Visible = true;
			bResult = false;
		}

		using (GameItemSale oLotteryGameItem = new GameItemSale()) {
			if (ViCommConst.FLAG_ON_STR.Equals(this.CreateFlag) && oLotteryGameItem.IsDupulicateItem(this.SiteCd,this.SaleScheduleSeq,this.lstGameItem.SelectedValue)) {
				this.lblErrorMessageItem.Text = "セールアイテムが重複しています。";
				this.lblErrorMessageItem.Visible = true;
				bResult = false;
			}
		}

		return bResult;
	}

	protected void lstItemGetCd_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		oDropDownList.Items.Insert(3,new ListItem("戦利品","3"));
	}
}
 