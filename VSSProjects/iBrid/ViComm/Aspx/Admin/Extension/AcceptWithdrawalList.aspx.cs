﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 退会申請検索
--	Progaram ID		: AcceptWithdrawalList
--
--  Creation Date	: 2011.06.01
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using iBridCommLib;
using ViComm;

public partial class Extension_AcceptWithdrawalList:System.Web.UI.Page {
	private string recCount = "";

	protected string[] YearArray;
	protected static readonly string[] MonthArray = new string[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" };
	protected static readonly string[] DayArray = new string[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" };
	protected static readonly string[] HourArray = new string[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" };
	protected static readonly string[] MinuteArray = new string[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" };

	private static readonly int colLastPointUsedDate = 4;
	private static readonly int colTotalReceiptAmt = 5;
	private static readonly int colTotalReceiptCount = 6;
	private static readonly int colTotalPaymentAmt = 7;

	private string RegUserSeq {
		get { return iBridUtil.GetStringValue(this.ViewState["RegUserSeq"]); }
		set { this.ViewState["RegUserSeq"] = value; }
	}

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			FirstLoad();
			GetList();
			SetSession();
		}
	}

	private void FirstLoad() {
		grdAcceptWithdrawal.PageSize = 100;
		if (iBridUtil.GetStringValue(Request.QueryString["sexcd"]).Equals(ViCommConst.MAN)) {
			lstWithdrawalReason.DataSourceID = "dsWithdrawalReasonMan";
			lblPgmTitle.Text = "男性" + lblPgmTitle.Text;
		} else {
			lstWithdrawalReason.DataSourceID = "dsWithdrawalReasonCast";
			lblPgmTitle.Text = "女性" + lblPgmTitle.Text;
		}
		ViewState["SEX_CD"] = iBridUtil.GetStringValue(Request.QueryString["sexcd"]);
		lstSiteCd.DataBind();
		lstWithdrawalReason.DataBind();
		lstWithdrawalStatus.DataBind();

		lstWithdrawalReason.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		lstWithdrawalStatus.Items.Insert(0,new ListItem(string.Empty,string.Empty));

		lstSiteCd.DataSourceID = string.Empty;
		lstWithdrawalReason.DataSourceID = string.Empty;
		lstWithdrawalStatus.DataSourceID = string.Empty;

		SysPrograms.SetupFromToDay(lstFromYYYY,lstFromMM,lstFromDD,lstToYYYY,lstToMM,lstToDD,false);
		ClearField();
		if (iBridUtil.GetStringValue(Request.QueryString["reload"]).Equals(ViCommConst.FLAG_ON_STR)) {
			GetSession();
		}

		this.lstRegSiteCd.DataBind();
		this.YearArray = new string[] { DateTime.Today.AddYears(-1).ToString("yyyy"),DateTime.Today.ToString("yyyy"), DateTime.Today.AddYears(1).ToString("yyyy") };
		this.lstRegYYYY.DataBind();
		this.lstRegMM.DataBind();
		this.lstRegDD.DataBind();
		this.lstRegHH.DataBind();
		this.lstRegMI.DataBind();
	}

	private void ClearField() {
		if (iBridUtil.GetStringValue(Request.QueryString["reload"]).Equals(ViCommConst.FLAG_ON_STR) && !IsPostBack) {
			//各詳細画面から戻ってくる時は何もしない。
		} else {
			lstSiteCd.SelectedIndex = 0;
			lstWithdrawalReason.SelectedIndex = 0;
			lstWithdrawalStatus.SelectedIndex = 0;
			lstWithdrawalStatus.Enabled = true;
			txtWithdrawalReasonDoc.Text = string.Empty;
			txtLoginId.Text = string.Empty;
			txtRemarks.Text = string.Empty;
			pnlInfo.Visible = false;

			DateTime dtFrom = DateTime.Now.AddDays(-7);
			DateTime dtTo = DateTime.Now;
			lstFromYYYY.SelectedValue = dtFrom.ToString("yyyy");
			lstToYYYY.SelectedValue = dtTo.ToString("yyyy");
			lstFromMM.SelectedValue = dtFrom.ToString("MM");
			lstToMM.SelectedValue = dtTo.ToString("MM");
			lstFromDD.SelectedValue = dtFrom.ToString("dd");
			lstToDD.SelectedValue = dtTo.ToString("dd");
			chkIntroduceFriend.Checked = false;
			if (iBridUtil.GetStringValue(Request.QueryString["unconfirm"]).Equals(ViCommConst.FLAG_ON_STR) && !IsPostBack) {
				chkUnConfirmed.Checked = true;
				chkIntroduceFriend.Checked = true;
				lstFromYYYY.SelectedValue = SysConst.SYSTEM_FIRST_YEAR.ToString();
				lstFromMM.SelectedIndex = 0;
				lstFromDD.SelectedIndex = 0;
			} else {
				chkUnConfirmed.Checked = false;
			}
			chkManualFlag.Checked = false;
		}
	}

	protected string GetRecCount() {
		return recCount;
	}

	private void GetSession() {
		lstSiteCd.SelectedValue = Session["WithdrawalSiteCd"].ToString();
		txtLoginId.Text = Session["WithdrawalLoginId"].ToString();
		lstWithdrawalStatus.SelectedValue = Session["WithdrawalStatus"].ToString();
		lstWithdrawalReason.SelectedValue = Session["WithdrawalReasonCd"].ToString();
		txtWithdrawalReasonDoc.Text = Session["WithdrawalReasonDoc"].ToString();
		txtRemarks.Text = Session["WithdrawalRemarks"].ToString();
		lstFromYYYY.SelectedValue = Session["WithdrawalFromYYYY"].ToString();
		lstFromMM.SelectedValue = Session["WithdrawalFromMM"].ToString();
		lstFromDD.SelectedValue = Session["WithdrawalFromDD"].ToString();
		lstToYYYY.SelectedValue = Session["WithdrawalToYYYY"].ToString();
		lstToMM.SelectedValue = Session["WithdrawalToMM"].ToString();
		lstToDD.SelectedValue = Session["WithdrawalToDD"].ToString();
		chkIntroduceFriend.Checked = ViCommConst.FLAG_ON_STR.Equals(Session["WithdrawalIntroduceFriend"].ToString());
		chkUnConfirmed.Checked = ViCommConst.FLAG_ON_STR.Equals(Session["WithdrawalUnConfirmed"].ToString());
		chkManualFlag.Checked = ViCommConst.FLAG_ON_STR.Equals(Session["WithdrawalManualFlag"].ToString());
	}
	private void SetSession() {
		Session["WithdrawalSiteCd"] = lstSiteCd.SelectedValue;
		Session["WithdrawalLoginId"] = txtLoginId.Text;
		Session["WithdrawalStatus"] = lstWithdrawalStatus.SelectedValue;
		Session["WithdrawalReasonCd"] = lstWithdrawalReason.SelectedValue;
		Session["WithdrawalReasonDoc"] = txtWithdrawalReasonDoc.Text;
		Session["WithdrawalRemarks"] = txtRemarks.Text;
		Session["WithdrawalFromYYYY"] = lstFromYYYY.SelectedValue;
		Session["WithdrawalFromMM"] = lstFromMM.SelectedValue;
		Session["WithdrawalFromDD"] = lstFromDD.SelectedValue;
		Session["WithdrawalToYYYY"] = lstToYYYY.SelectedValue;
		Session["WithdrawalToMM"] = lstToMM.SelectedValue;
		Session["WithdrawalToDD"] = lstToDD.SelectedValue;
		Session["WithdrawalIntroduceFriend"] = chkIntroduceFriend.Checked ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
		Session["WithdrawalUnConfirmed"] = chkUnConfirmed.Checked ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
		Session["WithdrawalManualFlag"] = chkManualFlag.Checked ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
	}
	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.pnlWithdrawal.Visible = false;
		GetList();
		SetSession();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		ClearField();
	}

	private void GetList() {
		pnlInfo.Visible = true;
		grdAcceptWithdrawal.PageIndex = 0;
		grdAcceptWithdrawal.DataSourceID = "dsAcceptWithdrawal";
		grdAcceptWithdrawal.DataBind();
		pnlCount.DataBind();

		bool bIsMan = ViewState["SEX_CD"].ToString().Equals(ViCommConst.MAN);

		this.grdAcceptWithdrawal.Columns[colLastPointUsedDate].Visible = bIsMan;
		this.grdAcceptWithdrawal.Columns[colTotalReceiptAmt].Visible = bIsMan;
		this.grdAcceptWithdrawal.Columns[colTotalReceiptCount].Visible = bIsMan;
		this.grdAcceptWithdrawal.Columns[colTotalPaymentAmt].Visible = !bIsMan;
	}

	protected void dsAcceptWithdrawal_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsAcceptWithdrawal_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = txtLoginId.Text;
		e.InputParameters[2] = lstWithdrawalStatus.SelectedValue;
		e.InputParameters[3] = lstWithdrawalReason.SelectedValue;
		e.InputParameters[4] = txtWithdrawalReasonDoc.Text;
		e.InputParameters[5] = txtRemarks.Text;
		e.InputParameters[6] = string.Format("{0}/{1}/{2}",lstFromYYYY.SelectedValue,lstFromMM.SelectedValue,lstFromDD.SelectedValue);
		e.InputParameters[7] = string.Format("{0}/{1}/{2}",lstToYYYY.SelectedValue,lstToMM.SelectedValue,lstToDD.SelectedValue);
		e.InputParameters[8] = ViewState["SEX_CD"].ToString();
		e.InputParameters[9] = chkIntroduceFriend.Checked ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
		e.InputParameters[10] = chkUnConfirmed.Checked ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
		e.InputParameters[11] = chkManualFlag.Checked ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
	}

	protected string GetUserLink(object pSiteCd,object pLoginId,object pWithdrawalSeq) {
		if (ViewState["SEX_CD"].ToString().Equals(ViCommConst.MAN)) {
			return string.Format("../Man/ManView.aspx?site={0}&manloginid={1}&withdrawalseq={2}",pSiteCd,pLoginId,pWithdrawalSeq);
		} else {
			return string.Format("../Cast/CastView.aspx?loginid={0}&withdrawalseq={1}",pLoginId,pWithdrawalSeq);
		}
	}

	/// <summary>
	/// ユーザ（会員、出演者）詳細へ遷移＆利用履歴を表示させるリンクURLを返す
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pLoginId"></param>
	/// <returns></returns>
	protected string GetUserLinkDispUsedLog(object pSiteCd,object pLoginId,object pAdCd,object pWithdrawalSeq) {
		// 友達紹介による登録以外の場合、リンクURLなし
		if (!pAdCd.ToString().StartsWith("os")) {
			return string.Empty;
		}

		if (ViewState["SEX_CD"].ToString().Equals(ViCommConst.MAN)) {
			return string.Format("../Man/ManView.aspx?site={0}&manloginid={1}&checkedseq={2}",pSiteCd,pLoginId,pWithdrawalSeq);
		} else {
			return string.Empty;
		}
	}

	protected void grdAcceptWithdrawal_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			string sWithdrawalStatus = DataBinder.Eval(e.Row.DataItem,"WITHDRAWAL_STATUS").ToString();
			if (sWithdrawalStatus.Equals(ViCommConst.WITHDRAWAL_STATUS_CHOICE)) {
				e.Row.BackColor = Color.White;
			} else if (sWithdrawalStatus.Equals(ViCommConst.WITHDRAWAL_STATUS_ACCEPT)) {
				e.Row.BackColor = Color.Azure;
			} else {
				e.Row.BackColor = Color.Silver;
			}
		}
	}

	protected void vdcLoginId_ServerValidate(object source, ServerValidateEventArgs args) {
		if (!args.IsValid) {
			return;
		}

		if (ViCommConst.MAN.Equals(iBridUtil.GetStringValue(this.ViewState["SEX_CD"]))) {
			using (UserManCharacter oUserManCharacter = new UserManCharacter()) {
				args.IsValid = oUserManCharacter.IsExistLoginId(this.lstRegSiteCd.SelectedValue, txtRegLoginId.Text, ViCommConst.MAIN_CHAR_NO);
				if (args.IsValid) {
					lblHandleNm.Text = oUserManCharacter.handleNm;
					this.RegUserSeq = oUserManCharacter.userSeq;
				} else {
					lblHandleNm.Text = string.Empty;
				}
			}
		} else {
			using (CastCharacter oCastCharacter = new CastCharacter()) {
				args.IsValid = oCastCharacter.IsExistLoginId(this.lstRegSiteCd.SelectedValue, txtRegLoginId.Text, ViCommConst.MAIN_CHAR_NO);
				if (args.IsValid) {
					lblHandleNm.Text = oCastCharacter.handleNm;
					this.RegUserSeq = oCastCharacter.userSeq;
				} else {
					lblHandleNm.Text = string.Empty;
				}
			}
		}
	}

	protected void btnRegist_Click(object sender, EventArgs e) {
		this.lstRegSiteCd.SelectedValue = this.lstSiteCd.SelectedValue;
		this.lstRegWithdrawalReason.DataSourceID = ViCommConst.MAN.Equals(iBridUtil.GetStringValue(this.ViewState["SEX_CD"])) ? "dsWithdrawalReasonMan" : "dsWithdrawalReasonCast";
		this.lstRegWithdrawalReason.DataBind();
		this.lstRegWithdrawalReason.Items.Insert(0, new ListItem(string.Empty, string.Empty));
		this.lstRegWithdrawalReason.SelectedIndex = 0;
		this.txtRegLoginId.Text = string.Empty;
		this.lblHandleNm.Text = string.Empty;
		this.txtRegWithdrawalReasonDoc.Text = string.Empty;
		this.txtRegWithdrawalRemarks.Text = string.Empty;
		this.lstRegYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstRegMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstRegDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstRegHH.SelectedValue = DateTime.Now.ToString("HH");
		this.lstRegMI.SelectedValue = DateTime.Now.ToString("mm");

		this.pnlWithdrawal.Visible = true;
	}

	protected void btnUpdate_Click(object sender, EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		string sWithdrawalSeq = string.Empty;
		string sWithdrawal = string.Empty;
		DateTime dtTemp;
		if (DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:00", this.lstRegYYYY.SelectedValue, this.lstRegMM.SelectedValue, this.lstRegDD.SelectedValue, this.lstRegHH.SelectedValue, this.lstRegMI.SelectedValue), out dtTemp)) {
			sWithdrawal = dtTemp.ToString("yyyy/MM/dd HH:mm:ss");
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("WITHDRAWAL_MAINTE");
			oDbSession.cmd.BindByName = true;
			oDbSession.ProcedureBothParm("pWITHDRAWAL_SEQ", DbSession.DbType.VARCHAR2, string.Empty);
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.lstRegSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pUSER_SEQ", DbSession.DbType.VARCHAR2, this.RegUserSeq);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO", DbSession.DbType.VARCHAR2, ViCommConst.MAIN_CHAR_NO);
			oDbSession.ProcedureInParm("pWITHDRAWAL_STATUS", DbSession.DbType.VARCHAR2, ViCommConst.WITHDRAWAL_STATUS_ACCEPT);
			oDbSession.ProcedureInParm("pWITHDRAWAL_REASON_CD", DbSession.DbType.VARCHAR2, this.lstRegWithdrawalReason.SelectedValue);
			oDbSession.ProcedureInParm("pWITHDRAWAL_REASON_DOC", DbSession.DbType.VARCHAR2, this.txtRegWithdrawalReasonDoc.Text);
			oDbSession.ProcedureInParm("pREMARKS", DbSession.DbType.VARCHAR2, this.txtRegWithdrawalRemarks.Text);
			oDbSession.ProcedureInParm("pDEL_FLAG", DbSession.DbType.NUMBER, ViCommConst.FLAG_OFF);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureInParm("pWITHDRAWAL_DATE", DbSession.DbType.VARCHAR2, sWithdrawal);
			oDbSession.ExecuteProcedure();
		}

		this.pnlWithdrawal.Visible = false;
		this.GetList();
		this.SetSession();
	}

	protected void btnCancel_Click(object sender, EventArgs e) {
		this.pnlWithdrawal.Visible = false;
	}

	protected void vdcFromToDate_SeverValidate(object source, ServerValidateEventArgs args) {
		if (!this.IsValid) {
			return;
		}

		DateTime dtFrom;
		if (!DateTime.TryParse(string.Format("{0}/{1}/{2}",this.lstFromYYYY.SelectedValue, this.lstFromMM.SelectedValue, this.lstFromDD.SelectedValue), out dtFrom)) {
			this.vdcFromToDate.ErrorMessage = "申請日Fromが不正です";
			args.IsValid = false;
			return;
		}
		DateTime dtTo;
		if (!DateTime.TryParse(string.Format("{0}/{1}/{2}", this.lstToYYYY.SelectedValue, this.lstToMM.SelectedValue, this.lstToDD.SelectedValue), out dtTo)) {
			this.vdcFromToDate.ErrorMessage = "申請日Toが不正です";
			args.IsValid = false;
			return;
		}
		if (dtTo < dtFrom) {
			this.vdcFromToDate.ErrorMessage = "申請日FromToの大小関係が不正です";
			args.IsValid = false;
			return;
		}
	}

	protected void vdcRegDate_SeverValidate(object source, ServerValidateEventArgs args) {
		if (!this.IsValid) {
			return;
		}

		DateTime dtWithdrawal;
		if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:00", this.lstRegYYYY.SelectedValue, this.lstRegMM.SelectedValue, this.lstRegDD.SelectedValue, this.lstRegHH.SelectedValue, this.lstRegMI.SelectedValue), out dtWithdrawal)) {
			this.vdcRegDate.ErrorMessage = "申請日時が不正です";
			args.IsValid = false;
		}
	}
}
