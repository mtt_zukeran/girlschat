﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CastTreasureDropRateList.aspx.cs" Inherits="Extension_CastTreasureDropRateList"
    Title="女性用お宝別ドロップ率一覧" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="女性用お宝別ドロップ率一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 800px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle2">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    Enabled="false" DataValueField="SITE_CD" Width="170px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button ID="btnBack" runat="server" CausesValidation="False" CssClass="seekbutton"
                    OnClick="btnBack_Click" Text="戻る" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[ドロップ率一覧]</legend>
                <table class="tableStyle" border="0">
                    <tr>
                        <td class="tdHeaderStyle2" style="white-space: nowrap">
                        </td>
                        <asp:Repeater ID="rptCastTreasure" runat="server" DataSourceID="dsCastTreasure" EnableViewState="false">
                            <ItemTemplate>
                                <td class="tdHeaderStyle2" align="center">
                                    <asp:Label ID="lblCastTreasureNm" runat="server" Text='<%# Eval("CAST_TREASURE_NM")%>'></asp:Label>
                                </td>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tr>
                    <tr>
                        <asp:Repeater ID="rptCharacterType1" runat="server" DataSourceID="dsCastTreasureDropRate"
                            EnableViewState="true" OnDataBinding="rptCharacterType_DataBinding">
                            <ItemTemplate>
                                <%# Container.ItemIndex == 0 ? "<td class='tdDataStyle' align='Left' style='white-space:nowrap'>" + Eval("GAME_CHARACTER_TYPE_NM") + "</td>" : string.Empty %>
                                <td class="tdDataStyle" align="center">
                                    <asp:TextBox ID="txtDropRate" runat="server" MaxLength="3" Text='<%# Eval("DROP_RATE") %>'></asp:TextBox>
                                    <asp:HiddenField ID="hdnRevisionNo" runat="server" Value='<%# Eval("REVISION_NO") %>' />
                                    <asp:HiddenField ID="hdnCastTreasureSeq" runat="server" Value='<%# Eval("CAST_TREASURE_SEQ") %>' />
                                    <asp:HiddenField ID="hdnGameCharacterType" runat="server" Value='<%# Eval("GAME_CHARACTER_TYPE") %>' />
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                        Enabled="true" FilterType="Numbers" TargetControlID="txtDropRate">
                                    </ajaxToolkit:FilteredTextBoxExtender>
                                </td>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tr>
                    <tr>
                        <asp:Repeater ID="rptCharacterType2" runat="server" DataSourceID="dsCastTreasureDropRate"
                            EnableViewState="true" OnDataBinding="rptCharacterType_DataBinding">
                            <ItemTemplate>
                                <%# Container.ItemIndex == 0 ? "<td class='tdDataStyle' align='Left' style='white-space:nowrap'>" + Eval("GAME_CHARACTER_TYPE_NM") + "</td>" : string.Empty%>
                                <td align="center" class="tdDataStyle">
                                    <asp:TextBox ID="txtDropRate" runat="server" MaxLength="3" Text='<%# Eval("DROP_RATE") %>'></asp:TextBox>
                                    <asp:HiddenField ID="hdnRevisionNo" runat="server" Value='<%# Eval("REVISION_NO") %>' />
                                    <asp:HiddenField ID="hdnCastTreasureSeq" runat="server" Value='<%# Eval("CAST_TREASURE_SEQ") %>' />
                                    <asp:HiddenField ID="hdnGameCharacterType" runat="server" Value='<%# Eval("GAME_CHARACTER_TYPE") %>' />
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                                        Enabled="true" FilterType="Numbers" TargetControlID="txtDropRate">
                                    </ajaxToolkit:FilteredTextBoxExtender>
                                </td>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tr>
                    <tr>
                        <asp:Repeater ID="rptCharacterType3" runat="server" DataSourceID="dsCastTreasureDropRate"
                            EnableViewState="true" OnDataBinding="rptCharacterType_DataBinding">
                            <ItemTemplate>
                                <%# Container.ItemIndex == 0 ? "<td class='tdDataStyle' align='Left' style='white-space:nowrap'>" + Eval("GAME_CHARACTER_TYPE_NM") + "</td>" : string.Empty%>
                                <td align="center" class="tdDataStyle">
                                    <asp:TextBox ID="txtDropRate" runat="server" MaxLength="3" Text='<%# Eval("DROP_RATE") %>'></asp:TextBox>
                                    <asp:HiddenField ID="hdnRevisionNo" runat="server" Value='<%# Eval("REVISION_NO") %>' />
                                    <asp:HiddenField ID="hdnCastTreasureSeq" runat="server" Value='<%# Eval("CAST_TREASURE_SEQ") %>' />
                                    <asp:HiddenField ID="hdnGameCharacterType" runat="server" Value='<%# Eval("GAME_CHARACTER_TYPE") %>' />
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
                                        Enabled="true" FilterType="Numbers" TargetControlID="txtDropRate">
                                    </ajaxToolkit:FilteredTextBoxExtender>
                                </td>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tr>
                </table>
                <asp:Panel ID="pnlCommand" runat="server">
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Update"
                        OnClick="btnUpdate_Click" />
                    <asp:Label ID="lblErrorMessageRate" runat="server" ForeColor="red" Visible="false"></asp:Label>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsCastTreasureDropRate" runat="server" ConvertNullToDBNull="false"
        OnSelecting="dsCastTreasureDropRate_Selecting" SelectMethod="GetListByGameCharacterType"
        SortParameterName="" TypeName="CastTreasureDropRate">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:Parameter Name="pGameCharacterType" DefaultValue="" Type="String" />
            <asp:QueryStringParameter Name="pStageGroupType" QueryStringField="stagegrouptype" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsCastTreasure" runat="server" SelectMethod="GetList" TypeName="CastTreasure">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:QueryStringParameter Name="pStageGroupType" QueryStringField="stagegrouptype" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="ドロップ率情報を更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>
