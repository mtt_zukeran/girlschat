﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ビンゴカード一覧

--	Progaram ID		: BingoCardView
--
--  Creation Date	: 2011.07.06
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.Collections.Generic;
using System.Drawing;

public partial class Extension_BingoCardView : System.Web.UI.Page {
	protected const int HORIZONTAL_LIMIT_COUNT = 5;
	private const int MAX_CARD_NO = 999;

	private string SiteCd {
		get { return iBridUtil.GetStringValue(this.ViewState["SiteCd"]); }
		set { this.ViewState["SiteCd"] = value; }
	}

	private string BingoTermSeq {
		get { return iBridUtil.GetStringValue(this.ViewState["BingoTermSeq"]); }
		set { this.ViewState["BingoTermSeq"] = value; }
	}

	private string SexCd {
		get { return iBridUtil.GetStringValue(this.ViewState["SexCd"]); }
		set { this.ViewState["SexCd"] = value; }
	}

	protected void Page_Load(object sender,EventArgs e) {
		if (!this.IsPostBack) {
			this.pnlRegist.Visible = false;
			this.lstSiteCd.DataSourceID = string.Empty;
			this.rptBingoCard.DataSourceID = string.Empty;

			this.SiteCd = iBridUtil.GetStringValue(this.Request.QueryString["sitecd"]);
			this.BingoTermSeq = iBridUtil.GetStringValue(this.Request.QueryString["seq"]);
			this.SexCd = iBridUtil.GetStringValue(this.Request.QueryString["sexcd"]);

			this.lstSiteCd.DataSourceID = "dsSite";
			this.lstSiteCd.DataBind();
			this.lstSiteCd.SelectedValue = this.SiteCd;
			this.GetList();
		}
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		this.txtCount.Text = string.Empty;
		this.pnlRegist.Visible = true;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlRegist.Visible = false;
	}

	protected void btnHighProbability_Click(object sender,EventArgs e) {
		this.UpdateHighProbabilityFlag(ViCommConst.FLAG_ON_STR,string.Empty);
		this.GetList();
	}

	protected void btnNormalProbability_Click(object sender,EventArgs e) {
		this.UpdateHighProbabilityFlag(ViCommConst.FLAG_OFF_STR,string.Empty);
		this.GetList();
	}

	protected void btnBack_Click(object sender,EventArgs e) {
		this.Response.Redirect(string.Concat("~/Extension/MailDeBingoMainte.aspx?sexcd=",this.SexCd));
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("CREATE_BINGO_CARD");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pBINGO_TERM_SEQ",DbSession.DbType.VARCHAR2,this.BingoTermSeq);
			oDbSession.ProcedureInParm("pCREATE_CARD_COUNT",DbSession.DbType.NUMBER,int.Parse(this.txtCount.Text));
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.GetList();
	}

	protected void btnUpdate_Command(object sender,CommandEventArgs e) {
		if (!e.CommandName.Equals("UPDATE")) {
			return;
		}

		int iIndex = int.Parse(iBridUtil.GetStringValue(e.CommandArgument));
		RepeaterItem oCardItem = this.rptBingoCard.Items[iIndex];
		HiddenField oBingoCardNoHiddenField = oCardItem.FindControl("hdnBingoCardNo") as HiddenField;
		HiddenField oHighProbabilityFlagHiddenField = oCardItem.FindControl("hdnHighProbabilityFlag") as HiddenField;

		string sHighProbabilityFlag = ViCommConst.FLAG_ON_STR.Equals(oHighProbabilityFlagHiddenField.Value) ? ViCommConst.FLAG_OFF_STR : ViCommConst.FLAG_ON_STR;
		oHighProbabilityFlagHiddenField.Value = sHighProbabilityFlag;

		this.UpdateHighProbabilityFlag(sHighProbabilityFlag,oBingoCardNoHiddenField.Value);

		LinkButton oUpdateLinkButton = oCardItem.FindControl("btnUpdate") as LinkButton;
		oUpdateLinkButton.Text = this.GetHighProbabilityMark(sHighProbabilityFlag);
		oUpdateLinkButton.ForeColor = this.GetHighProbabilityMarkColor(sHighProbabilityFlag);
	}

	protected void dsBingoCard_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = this.SiteCd;
		e.InputParameters["pBingoTermSeq"] = this.BingoTermSeq;
	}

	protected void vdcCount_ServerValidate(object source,ServerValidateEventArgs e) {
		if (!this.IsValid) {
			return;
		}

		int iCount = this.GetCardCount();
		int iCreateCount;
		if (!int.TryParse(this.txtCount.Text,out iCreateCount)) {
			this.vdcCount.ErrorMessage = "数値を入力してください。";
			e.IsValid = false;
			return;
		}
		if (iCount + iCreateCount > MAX_CARD_NO) {
			this.vdcCount.ErrorMessage = string.Format("1～{0} の数値を入力してください。",MAX_CARD_NO - iCount);
			e.IsValid = false;
			return;
		}
	}

	protected int GetCardCount() {
		using (BingoCard oBingoCard = new BingoCard()) {
			return oBingoCard.GetCount(this.SiteCd,this.BingoTermSeq);
		}
	}

	private void GetList() {
		this.pnlRegist.Visible = false;

		this.rptBingoCard.DataSourceID = "dsBingoCard";
		this.rptBingoCard.DataBind();
		this.lblCount.DataBind();
	}

	protected string GetHighProbabilityMark(object pHighProbabilityFlag) {
		if (ViCommConst.FLAG_ON == int.Parse(iBridUtil.GetStringValue(pHighProbabilityFlag))) {
			return "高確率中";
		}
		else {
			return "通常";
		}
	}

	protected Color GetHighProbabilityMarkColor(object pHighProbabilityFlag) {
		if (ViCommConst.FLAG_ON == int.Parse(iBridUtil.GetStringValue(pHighProbabilityFlag))) {
			return Color.Red;
		}
		else {
			return Color.Empty;
		}
	}

	private void UpdateHighProbabilityFlag(string pHighProbabilityFlag,string pBingoCardNo) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("UPDATE_HIGH_PROBABILITY_FLAG");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pBINGO_TERM_SEQ",DbSession.DbType.NUMBER,this.BingoTermSeq);
			oDbSession.ProcedureInParm("pBINGO_CARD_NO",DbSession.DbType.VARCHAR2,pBingoCardNo);
			oDbSession.ProcedureInParm("pHIGH_PROBABILITY_FLAG",DbSession.DbType.NUMBER,pHighProbabilityFlag);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}
	}
}
