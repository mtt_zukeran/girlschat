﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="TownList.aspx.cs" Inherits="Extension_TownList" Title="街一覧" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="街一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 600px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" Enabled="false"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True" OnSelectedIndexChanged="lstSiteCd_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle2">
                                ステージ
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstStageSeq" runat="server" DataSourceID="dsStage" OnDataBound="lst_DataBound"
                                    ValidationGroup="Create" DataTextField="STAGE_DISPLAY" DataValueField="STAGE_SEQ"
                                    Width="170px">
                                </asp:DropDownList>
                                <% // 必須チェック %>
                                <asp:RequiredFieldValidator ID="vdrStageSeq" runat="server" ErrorMessage="ステージを選択してください。"
                                    ControlToValidate="lstStageSeq" ValidationGroup="Create">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                    TargetControlID="vdrStageSeq" HighlightCssClass="validatorCallout" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton" ValidationGroup="Create"
                    OnClick="btnCreate_Click" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" Visible="false" />
                <asp:Button ID="btnBack" runat="server" CausesValidation="False" CssClass="seekbutton"
                    OnClick="btnBack_Click" Text="戻る" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlTownInfo">
            <fieldset class="fieldset">
                <legend>[街情報]</legend>
                <fieldset class="fieldset-inner">
                    <table border="0" style="width: 600px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                街ID(SEQ)
                            </td>
                            <td class="tdDataStyle">
                                <asp:Label ID="lblTownId" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                街名
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtTownNm" runat="server" MaxLength="64" Width="200px"></asp:TextBox>
                                <% // 必須チェック %>
                                <asp:RequiredFieldValidator ID="vdrTownNm" runat="server" ErrorMessage="街名を入力してください。"
                                    ControlToValidate="txtTownNm" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="vdeTownNm" runat="Server" TargetControlID="vdrTownNm"
                                    HighlightCssClass="validatorCallout" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                街名補足
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtTownNmExtra" runat="server" MaxLength="64" Width="200px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                街レベル
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstLevel" runat="server" DataSourceID="dsTownLevel" DataTextField="TOWN_DISPLAY"
                                    DataValueField="TOWN_LEVEL" Width="150px" OnDataBound="lstLevel_DataBound" />に挿入
                                <% // 必須チェック %>
                                <asp:RequiredFieldValidator ID="vdrStageLevel" runat="server" ErrorMessage="ステージレベルを選択してください。"
                                    ControlToValidate="lstLevel" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="vdeStageLevel" runat="Server" TargetControlID="vdrStageLevel"
                                    HighlightCssClass="validatorCallout" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                経験値
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtExp" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
                                <% // 必須チェック %>
                                <asp:RequiredFieldValidator ID="vdrExp" runat="server" ErrorMessage="経験値を入力して下さい。"
                                    ControlToValidate="txtExp" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                    TargetControlID="vdrExp" HighlightCssClass="validatorCallout" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                報酬
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtIncomeMin" runat="server" MaxLength="10" Width="80px"></asp:TextBox>～<asp:TextBox
                                    ID="txtIncomeMax" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
                                <% // 必須チェック %>
                                <asp:RequiredFieldValidator ID="vdrIncomeMin" runat="server" ControlToValidate="txtIncomeMin"
                                    ErrorMessage="報酬最小値を入力して下さい。" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server"
                                    HighlightCssClass="validatorCallout" TargetControlID="vdrIncomeMin">
                                </ajaxToolkit:ValidatorCalloutExtender>
                                <asp:RequiredFieldValidator ID="vdrIncomeMax" runat="server" ControlToValidate="txtIncomeMax"
                                    ErrorMessage="報酬最大値を入力して下さい。" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server"
                                    HighlightCssClass="validatorCallout" TargetControlID="vdrIncomeMax">
                                </ajaxToolkit:ValidatorCalloutExtender>
                                <asp:CompareValidator ID="vdcIncome" runat="server" ControlToValidate="txtIncomeMax"
                                    ControlToCompare="txtIncomeMin" Operator="GreaterThanEqual" Type="Integer" ErrorMessage="報酬の大小関係が不正です"
                                    ValidationGroup="Update">*</asp:CompareValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18" runat="Server"
                                    HighlightCssClass="validatorCallout" TargetControlID="vdcIncome">
                                </ajaxToolkit:ValidatorCalloutExtender>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                減少部隊数
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtLossForceCount" runat="server" MaxLength="60" Width="120px"></asp:TextBox>
                                <% // 必須チェック %>
                                <asp:RequiredFieldValidator ID="vdrLossForceCount" runat="server" ErrorMessage="減少部隊数を入力して下さい。"
                                    ControlToValidate="txtLossForceCount" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
                                    TargetControlID="vdrLossForceCount" HighlightCssClass="validatorCallout" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle2">
                                目標人数
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtTargetCount" runat="server" MaxLength="60" Width="120px"></asp:TextBox>
                                <% // 必須チェック %>
                                <asp:RequiredFieldValidator ID="vdrTargetCount" runat="server" ErrorMessage="目標人数を入力して下さい。"
                                    ControlToValidate="txtTargetCount" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server"
                                    TargetControlID="vdrTargetCount" HighlightCssClass="validatorCallout" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <asp:Panel ID="pnlGetItem" runat="server">
                    <fieldset class="fieldset-inner">
                        <legend>[クリア時獲得アイテム]</legend>
                        <table border="0" style="width: 870px" class="tableStyle">
                            <tr>
                                <td class="tdHeaderStyle">
                                    アイテム１
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstGameItemCategoryGet1" runat="server" DataSourceID="dsGameItemCategory"
                                        OnDataBound="lst_DataBound" OnSelectedIndexChanged="lstGameItemCategory_IndexChanged"
                                        DataTextField="GAME_ITEM_CATEGORY_NM" DataValueField="GAME_ITEM_CATEGORY_TYPE"
                                        Width="170px" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="lstItemGetCd1" runat="server" DataSourceID="dsItemGet" DataTextField="CODE_NM"
										DataValueField="CODE" OnDataBound="lstItemGetCd_DataBound" OnSelectedIndexChanged="lstItemGetCd_IndexChanged" AutoPostBack="True">
									</asp:DropDownList>
									<asp:DropDownList ID="lstItemPresent1" runat="server" OnSelectedIndexChanged="lstItemPresent_IndexChanged" AutoPostBack="True">
										<asp:ListItem Value=""></asp:ListItem>
										<asp:ListItem Value="0">通常</asp:ListItem>
										<asp:ListItem Value="1">ﾌﾟﾚｾﾞﾝﾄ</asp:ListItem>
									</asp:DropDownList>
                                    <asp:DropDownList ID="lstGameItemGet1" runat="server" DataSourceID="dsGameItem" DataTextField="GAME_ITEM_NM"
                                        OnDataBound="lst_DataBound" DataValueField="GAME_ITEM_SEQ" Width="170px">
                                    </asp:DropDownList>
                                    &nbsp;
                                    <asp:TextBox ID="txtGameItemGetCount1" runat="server" MaxLength="10" Width="50px"></asp:TextBox>個
                                    &nbsp;
                                    <asp:TextBox ID="txtGameItemGetDropRate1" runat="server" MaxLength="3" Width="50px"></asp:TextBox>%(ﾄﾞﾛｯﾌﾟ率)<br />
                                    <asp:CustomValidator ID="vdcGameItemGet1" runat="server" ErrorMessage="" OnServerValidate="vdcGameItemGet1_ServerValidate"
                                        ValidationGroup="Update"></asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    アイテム２
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstGameItemCategoryGet2" runat="server" DataSourceID="dsGameItemCategory"
                                        OnDataBound="lst_DataBound" OnSelectedIndexChanged="lstGameItemCategory_IndexChanged"
                                        DataTextField="GAME_ITEM_CATEGORY_NM" DataValueField="GAME_ITEM_CATEGORY_TYPE"
                                        Width="170px" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="lstItemGetCd2" runat="server" DataSourceID="dsItemGet" DataTextField="CODE_NM"
										DataValueField="CODE" OnDataBound="lstItemGetCd_DataBound" OnSelectedIndexChanged="lstItemGetCd_IndexChanged" AutoPostBack="True">
									</asp:DropDownList>
									<asp:DropDownList ID="lstItemPresent2" runat="server" OnSelectedIndexChanged="lstItemPresent_IndexChanged" AutoPostBack="True">
										<asp:ListItem Value=""></asp:ListItem>
										<asp:ListItem Value="0">通常</asp:ListItem>
										<asp:ListItem Value="1">ﾌﾟﾚｾﾞﾝﾄ</asp:ListItem>
									</asp:DropDownList>
                                    <asp:DropDownList ID="lstGameItemGet2" runat="server" DataSourceID="dsGameItem2" DataTextField="GAME_ITEM_NM"
                                        OnDataBound="lst_DataBound" DataValueField="GAME_ITEM_SEQ" Width="170px">
                                    </asp:DropDownList>
                                    &nbsp;
                                    <asp:TextBox ID="txtGameItemGetCount2" runat="server" MaxLength="10" Width="50px"></asp:TextBox>個
                                    &nbsp;
                                    <asp:TextBox ID="txtGameItemGetDropRate2" runat="server" MaxLength="3" Width="50px"></asp:TextBox>%(ﾄﾞﾛｯﾌﾟ率)<br />
                                    <asp:CustomValidator ID="vdcGameItemGet2" runat="server" ErrorMessage="" OnServerValidate="vdcGameItemGet2_ServerValidate"
                                        ValidationGroup="Update"></asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle2">
                                    アイテム３
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstGameItemCategoryGet3" runat="server" DataSourceID="dsGameItemCategory"
                                        OnDataBound="lst_DataBound" OnSelectedIndexChanged="lstGameItemCategory_IndexChanged"
                                        DataTextField="GAME_ITEM_CATEGORY_NM" DataValueField="GAME_ITEM_CATEGORY_TYPE"
                                        Width="170px" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="lstItemGetCd3" runat="server" DataSourceID="dsItemGet" DataTextField="CODE_NM"
										DataValueField="CODE" OnDataBound="lstItemGetCd_DataBound" OnSelectedIndexChanged="lstItemGetCd_IndexChanged" AutoPostBack="True">
									</asp:DropDownList>
									<asp:DropDownList ID="lstItemPresent3" runat="server" OnSelectedIndexChanged="lstItemPresent_IndexChanged" AutoPostBack="True">
										<asp:ListItem Value=""></asp:ListItem>
										<asp:ListItem Value="0">通常</asp:ListItem>
										<asp:ListItem Value="1">ﾌﾟﾚｾﾞﾝﾄ</asp:ListItem>
									</asp:DropDownList>
                                    <asp:DropDownList ID="lstGameItemGet3" runat="server" DataSourceID="dsGameItem3" DataTextField="GAME_ITEM_NM"
                                        OnDataBound="lst_DataBound" DataValueField="GAME_ITEM_SEQ" Width="170px">
                                    </asp:DropDownList>
                                    &nbsp;
                                    <asp:TextBox ID="txtGameItemGetCount3" runat="server" MaxLength="10" Width="50px"></asp:TextBox>個
                                    &nbsp;
                                    <asp:TextBox ID="txtGameItemGetDropRate3" runat="server" MaxLength="3" Width="50px"></asp:TextBox>%(ﾄﾞﾛｯﾌﾟ率)<br />
                                    <asp:CustomValidator ID="vdcGameItemGet3" runat="server" ErrorMessage="" OnServerValidate="vdcGameItemGet3_ServerValidate"
                                        ValidationGroup="Update"></asp:CustomValidator>
                                </td>
                            </tr>
                        </table>
                        <asp:CustomValidator ID="vdcGameItemGetDropRate" runat="server" ErrorMessage="" OnServerValidate="vdcGameItemGetDropRate_ServerValidate"
                            ValidationGroup="Update"></asp:CustomValidator>
                    </fieldset>
                </asp:Panel>
                <asp:Panel ID="pnlUseItem" runat="server">
                    <fieldset class="fieldset-inner">
                        <legend>[クリア時必要アイテム]</legend>
                        <table border="0" style="width: 730px" class="tableStyle">
                            <tr>
                                <td class="tdHeaderStyle">
                                    アイテム１
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstGameItemCategoryUse1" runat="server" DataSourceID="dsGameItemCategory"
                                        OnDataBound="lst_DataBound" OnSelectedIndexChanged="lstGameItemCategory_IndexChanged"
                                        DataTextField="GAME_ITEM_CATEGORY_NM" DataValueField="GAME_ITEM_CATEGORY_TYPE"
                                        Width="170px" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="lstItemGetCdUse1" runat="server" DataSourceID="dsItemGet" DataTextField="CODE_NM"
										DataValueField="CODE" OnDataBound="lstItemGetCd_DataBound" OnSelectedIndexChanged="lstItemGetCd_IndexChanged" AutoPostBack="True">
									</asp:DropDownList>
									<asp:DropDownList ID="lstItemPresentUse1" runat="server" OnSelectedIndexChanged="lstItemPresent_IndexChanged" AutoPostBack="True">
										<asp:ListItem Value=""></asp:ListItem>
										<asp:ListItem Value="0">通常</asp:ListItem>
										<asp:ListItem Value="1">ﾌﾟﾚｾﾞﾝﾄ</asp:ListItem>
									</asp:DropDownList>
                                    <asp:DropDownList ID="lstGameItemUse1" runat="server" DataSourceID="dsGameItemUse" DataTextField="GAME_ITEM_NM"
                                        OnDataBound="lst_DataBound" DataValueField="GAME_ITEM_SEQ" Width="170px">
                                    </asp:DropDownList>
                                    &nbsp;
                                    <asp:TextBox ID="txtGameItemUseCount1" runat="server" MaxLength="10" Width="50px"></asp:TextBox>個<br />
                                    <asp:CustomValidator ID="vdcGameItemUse1" runat="server" ErrorMessage="" OnServerValidate="vdcGameItemUse1_ServerValidate"
                                        ValidationGroup="Update"></asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    アイテム２
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstGameItemCategoryUse2" runat="server" DataSourceID="dsGameItemCategory"
                                        OnDataBound="lst_DataBound" OnSelectedIndexChanged="lstGameItemCategory_IndexChanged"
                                        DataTextField="GAME_ITEM_CATEGORY_NM" DataValueField="GAME_ITEM_CATEGORY_TYPE"
                                        Width="170px" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="lstItemGetCdUse2" runat="server" DataSourceID="dsItemGet" DataTextField="CODE_NM"
										DataValueField="CODE" OnDataBound="lstItemGetCd_DataBound" OnSelectedIndexChanged="lstItemGetCd_IndexChanged" AutoPostBack="True">
									</asp:DropDownList>
									<asp:DropDownList ID="lstItemPresentUse2" runat="server" OnSelectedIndexChanged="lstItemPresent_IndexChanged" AutoPostBack="True">
										<asp:ListItem Value=""></asp:ListItem>
										<asp:ListItem Value="0">通常</asp:ListItem>
										<asp:ListItem Value="1">ﾌﾟﾚｾﾞﾝﾄ</asp:ListItem>
									</asp:DropDownList>
                                    <asp:DropDownList ID="lstGameItemUse2" runat="server" DataSourceID="dsGameItemUse2" DataTextField="GAME_ITEM_NM"
                                        OnDataBound="lst_DataBound" DataValueField="GAME_ITEM_SEQ" Width="170px">
                                    </asp:DropDownList>
                                    &nbsp;
                                    <asp:TextBox ID="txtGameItemUseCount2" runat="server" MaxLength="10" Width="50px"></asp:TextBox>個<br />
                                    <asp:CustomValidator ID="vdcGameItemUse2" runat="server" ErrorMessage="" OnServerValidate="vdcGameItemUse2_ServerValidate"
                                        ValidationGroup="Update"></asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle2">
                                    アイテム３
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstGameItemCategoryUse3" runat="server" DataSourceID="dsGameItemCategory"
                                        OnDataBound="lst_DataBound" OnSelectedIndexChanged="lstGameItemCategory_IndexChanged"
                                        DataTextField="GAME_ITEM_CATEGORY_NM" DataValueField="GAME_ITEM_CATEGORY_TYPE"
                                        Width="170px" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="lstItemGetCdUse3" runat="server" DataSourceID="dsItemGet" DataTextField="CODE_NM"
										DataValueField="CODE" OnDataBound="lstItemGetCd_DataBound" OnSelectedIndexChanged="lstItemGetCd_IndexChanged" AutoPostBack="True">
									</asp:DropDownList>
									<asp:DropDownList ID="lstItemPresentUse3" runat="server" OnSelectedIndexChanged="lstItemPresent_IndexChanged" AutoPostBack="True">
										<asp:ListItem Value=""></asp:ListItem>
										<asp:ListItem Value="0">通常</asp:ListItem>
										<asp:ListItem Value="1">ﾌﾟﾚｾﾞﾝﾄ</asp:ListItem>
									</asp:DropDownList>
                                    <asp:DropDownList ID="lstGameItemUse3" runat="server" DataSourceID="dsGameItemUse3" DataTextField="GAME_ITEM_NM"
                                        OnDataBound="lst_DataBound" DataValueField="GAME_ITEM_SEQ" Width="170px">
                                    </asp:DropDownList>
                                    &nbsp;
                                    <asp:TextBox ID="txtGameItemUseCount3" runat="server" MaxLength="10" Width="50px"></asp:TextBox>個<br />
                                    <asp:CustomValidator ID="vdcGameItemUse3" runat="server" ErrorMessage="" OnServerValidate="vdcGameItemUse3_ServerValidate"
                                        ValidationGroup="Update"></asp:CustomValidator>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </asp:Panel>
                <asp:Panel ID="pnlCommand" runat="server">
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Update"
                        OnClick="btnUpdate_Click" />
                    <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" ValidationGroup="Delete"
                        OnClick="btnDelete_Click" />
                    <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" CausesValidation="False"
                        Visible="true" OnClick="btnCancel_Click" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[街一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdTown.PageIndex + 1 %>
                        of
                        <%= grdTown.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="443px">
                    <asp:GridView ID="grdTown" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        PageSize="30" EnableSortingAndPagingCallbacks="false" DataSourceID="dsTown" SkinID="GridViewColor"
                        AllowSorting="true" DataKeyNames="SITE_CD,STAGE_SEQ,TOWN_SEQ">
                        <Columns>
                            <asp:TemplateField HeaderText="街SEQ">
                                <ItemTemplate>
                                    <asp:Label ID="lblTownLevel" runat="server" Text='<%# Eval("TOWN_SEQ") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="街名">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%# Container.DataItemIndex %>'
                                        OnCommand="lnkEdit_OnCommand" Text='<%# Eval("TOWN_NM") %>'></asp:LinkButton>
                                    <asp:HiddenField ID="hdnStageSeq" runat="server" Value='<%# Eval("STAGE_SEQ") %>' />
                                    <asp:HiddenField ID="hdnTownSeq" runat="server" Value='<%# Eval("TOWN_SEQ") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="街名補足">
                                <ItemTemplate>
									<asp:Label ID="lblTownNmExtra" runat="server" Text='<%# Eval("TOWN_NM_EXTRA") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="TOWN_LEVEL" HeaderText="街レベル">
                                <ItemStyle HorizontalAlign="center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="EXP" HeaderText="経験値" DataFormatString="{0:N0}">
                                <ItemStyle HorizontalAlign="right" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="報酬">
                                <ItemTemplate>
                                    <asp:Label ID="lblIncomeMin" runat="server" Text='<%# Eval("INCOME_MIN", "{0:N0}") %>'></asp:Label>～
                                    <asp:Label ID="lblIncomeMax" runat="server" Text='<%# Eval("INCOME_MAX", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="LOSS_FORCE_COUNT" HeaderText="減少部隊数" DataFormatString="{0:N0}">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="TARGET_COUNT" HeaderText="目標人数" DataFormatString="{0:N0}">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
            <br />
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsTown" runat="server" ConvertNullToDBNull="false" EnablePaging="True"
        OnSelected="dsTown_Selected" SelectCountMethod="GetPageCount" SelectMethod="GetPageCollection"
        SortParameterName="" TypeName="Town">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:ControlParameter ControlID="lstStageSeq" Name="pStageSeq" PropertyName="SelectedValue"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsTownLevel" runat="server" SelectMethod="GetLevelList"
        TypeName="Town" OnSelecting="dsTownLevel_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:Parameter DefaultValue="" Name="pStageSeq" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItemCategory" runat="server" SelectMethod="GetList"
        TypeName="GameItemCategory">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItem" runat="server" SelectMethod="GetListByCategoryType"
        TypeName="GameItem" OnSelecting="dsGameItem_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
            <asp:Parameter Name="pGameItemCategoryType" Type="String" DefaultValue="" />
            <asp:Parameter Name="pGameItemGetCd" Type="String" DefaultValue="" />
            <asp:Parameter Name="pPresentFlag" Type="String" DefaultValue="" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItem2" runat="server" SelectMethod="GetListByCategoryType"
        TypeName="GameItem" OnSelecting="dsGameItem2_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
            <asp:Parameter Name="pGameItemCategoryType" Type="String" DefaultValue="" />
            <asp:Parameter Name="pGameItemGetCd" Type="String" DefaultValue="" />
            <asp:Parameter Name="pPresentFlag" Type="String" DefaultValue="" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItem3" runat="server" SelectMethod="GetListByCategoryType"
        TypeName="GameItem" OnSelecting="dsGameItem3_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
            <asp:Parameter Name="pGameItemCategoryType" Type="String" DefaultValue="" />
            <asp:Parameter Name="pGameItemGetCd" Type="String" DefaultValue="" />
            <asp:Parameter Name="pPresentFlag" Type="String" DefaultValue="" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItemUse" runat="server" SelectMethod="GetListByCategoryType"
        TypeName="GameItem" OnSelecting="dsGameItemUse_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
            <asp:Parameter Name="pGameItemCategoryType" Type="String" DefaultValue="" />
            <asp:Parameter Name="pGameItemGetCd" Type="String" DefaultValue="" />
            <asp:Parameter Name="pPresentFlag" Type="String" DefaultValue="" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItemUse2" runat="server" SelectMethod="GetListByCategoryType"
        TypeName="GameItem" OnSelecting="dsGameItemUse2_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
            <asp:Parameter Name="pGameItemCategoryType" Type="String" DefaultValue="" />
            <asp:Parameter Name="pGameItemGetCd" Type="String" DefaultValue="" />
            <asp:Parameter Name="pPresentFlag" Type="String" DefaultValue="" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItemUse3" runat="server" SelectMethod="GetListByCategoryType"
        TypeName="GameItem" OnSelecting="dsGameItemUse3_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
            <asp:Parameter Name="pGameItemCategoryType" Type="String" DefaultValue="" />
            <asp:Parameter Name="pGameItemGetCd" Type="String" DefaultValue="" />
            <asp:Parameter Name="pPresentFlag" Type="String" DefaultValue="" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsItemGet" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="A2" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsStage" runat="server" SelectMethod="GetList" TypeName="Stage">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtIncomeMin">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtIncomeMax">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtExp">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtLossForceCount">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtTargetCount">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtGameItemGetCount1">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtGameItemGetCount2">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtGameItemGetCount3">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtGameItemUseCount1">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtGameItemUseCount2">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtGameItemUseCount3">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtGameItemGetDropRate1">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtGameItemGetDropRate2">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtGameItemGetDropRate3">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="街情報を更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete"
        ConfirmText="街情報を削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>
