﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 1日1回プレゼント一覧

--	Progaram ID		: OnceDayPresentList
--
--  Creation Date	: 2011.12.02
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;

public partial class Extension_OnceDayPresentList : System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string GameItemSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemSeq"]);
		}
		set {
			this.ViewState["GameItemSeq"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {

			this.InitPage();
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.GameItemSeq = this.lstGameItem.SelectedValue;
		this.UpdateData(false);
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.ClearFileds();
		this.pnlGameItemInfo.Visible = false;
		this.pnlKey.Enabled = true;
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.ClearFileds();
		this.pnlGameItemInfo.Visible = true;
		this.pnlKey.Enabled = false;
	}

	protected void dsOnceDayPresent_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void lnkEdit_Command(object sender,CommandEventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.GameItemSeq = iBridUtil.GetStringValue(e.CommandArgument);
	}

	protected void lnkDelete_Command(object sender,CommandEventArgs e) {
		if (!this.IsValid) {
			return;
		}

		string[] sArguments = iBridUtil.GetStringValue(e.CommandArgument).Split(',');
		this.GameItemSeq = sArguments[0];
		this.RevisionNo = sArguments[1];
		this.UpdateData(true);
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	protected void lstGameItemCategory_IndexChanged(object sender,EventArgs e) {
		this.lstGameItem.DataBind();
	}

	protected void vdcDuplicate_ServerValidate(object source,ServerValidateEventArgs args) {
		if (string.IsNullOrEmpty( this.lstGameItem.SelectedValue)) {
			return;
		}

		using (OnceDayPresent oOnceDayPresent = new OnceDayPresent()) {
			args.IsValid = !oOnceDayPresent.IsDuplicate(this.SiteCd,this.lstGameItem.SelectedValue);
		}

		if (!args.IsValid) {
			this.vdcDuplicate.ErrorMessage = "すでに登録されています。";
		} else {
			this.vdcDuplicate.ErrorMessage = string.Empty;
		}
	}

	private void InitPage() {
		this.pnlInfo.Visible = true;
		this.pnlGameItemInfo.Visible = false;

		this.pnlKey.Enabled = true;
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		this.lstGameItemCategory.DataBind();
		this.lstGameItem.DataBind();
	}

	private void ClearFileds() {
		this.GameItemSeq = string.Empty;
		this.RevisionNo = string.Empty;
		this.lstGameItemCategory.SelectedIndex = 0;
		this.lstGameItem.SelectedIndex = 0;
		this.pnlKey.Enabled = true;
	}

	private void GetList() {
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.GameItemSeq = string.Empty;
		this.RevisionNo = string.Empty;

		this.grdOnceDayPresent.PageIndex = 0;
        this.grdOnceDayPresent.DataBind();
		this.pnlCount.DataBind();
	}
	
	protected string GetRecCount() {
		return this.recCount;
	}

	private void UpdateData(bool pDelFlag) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ONCE_DAY_PRESENT_MAINTE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			db.ProcedureInParm("pGAME_ITEM_SEQ",DbSession.DbType.VARCHAR2,this.GameItemSeq);
			db.ProcedureInParm("pREVISION_NO",DbSession.DbType.VARCHAR2,this.RevisionNo);
			db.ProcedureInParm("pDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		this.pnlGameItemInfo.Visible = false;
		this.ClearFileds();
		this.GetList();
	}
}
