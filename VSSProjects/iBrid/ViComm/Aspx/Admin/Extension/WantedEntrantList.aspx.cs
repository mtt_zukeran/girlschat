﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: この娘を探せ・男性エントリー(詳細)

--	Progaram ID		: WantedEntrantList
--
--  Creation Date	: 2011.03.28
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using iBridCommLib;
using System.Drawing;
using ViComm;

public partial class Extension_WantedEntrantList : System.Web.UI.Page {
	string recCount = string.Empty;

	protected string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		private set {
			this.ViewState["SiteCd"] = value;
		}
	}
	
	protected string CaughtFlag {
		get {
			return iBridUtil.GetStringValue(this.ViewState["CaughtFlag"]);
		}
		set {
			this.ViewState["CaughtFlag"] = value;
		}
	}

	protected string PointAcquiredFlag {
		get {
			return iBridUtil.GetStringValue(this.ViewState["PointAcquiredFlag"]);
		}
		set {
			this.ViewState["PointAcquiredFlag"] = value;
		}
	}

	protected void Page_Load(object sender, EventArgs e) {
		if (!this.IsPostBack) {
			this.SiteCd = this.Request.QueryString["sitecd"];
			this.CaughtFlag = this.Request.QueryString["caughtflag"];
			this.PointAcquiredFlag = this.Request.QueryString["pointacquiredflag"];

			this.InitPage();

			if (!string.IsNullOrEmpty(this.SiteCd) && !string.IsNullOrEmpty(this.Request.QueryString["executionday"])) {
				this.lstCaught.SelectedValue = this.CaughtFlag;
				this.lstPointAcquired.SelectedValue = this.PointAcquiredFlag;
				this.lstSeekSiteCd.SelectedValue = this.SiteCd;
				this.GetList();
				this.pnlInfo.Visible = true;
			}
		}
	}

	protected void btnListSeek_Click(object sender, EventArgs e) {
		this.CaughtFlag = this.lstCaught.SelectedValue;
		this.PointAcquiredFlag = this.lstPointAcquired.SelectedValue;
		this.GetList();
		this.pnlInfo.Visible = true;
	}

	protected void btnCancel_Click(object sender, EventArgs e) {
		this.pnlInfo.Visible = false;
	}

	protected void btnBack_Click(object sender, EventArgs e) {
		this.Response.Redirect(string.Format("~/Extension/WantedEntrantSummaryList.aspx?sitecd={0}&targetmonth={1}", this.SiteCd, this.Request.QueryString["executionday"].Substring(0, 7)));
	}

	protected void btnClear_Click(object sender, EventArgs e) {
		this.InitPage();
	}

	protected void dsWantedEntrant_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pCaughtFlag"] = this.CaughtFlag;
		e.InputParameters["pPointAcquiredFlag"] = this.PointAcquiredFlag;
	}

	protected void dsWantedEntrant_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		this.grdWantedEntrant.DataSourceID = string.Empty;
		this.pnlKey.DataBind();
		this.chkPagingOff.Checked = false;
		this.lstPointAcquired.SelectedIndex = 0;
		this.lstCaught.SelectedIndex = 0;
		this.pnlInfo.Visible = false;
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}
	private void GetList() {
		this.grdWantedEntrant.PageIndex = 0;
		if (chkPagingOff.Checked) {
			this.grdWantedEntrant.PageSize = 99999;
		} else {
			this.grdWantedEntrant.PageSize = 20;
		}

		this.grdWantedEntrant.DataSourceID = "dsWantedEntrant";
		this.grdWantedEntrant.DataBind();
		this.pnlCount.DataBind();
	}

	protected string GetRecCount() {
		return recCount;
	}

}