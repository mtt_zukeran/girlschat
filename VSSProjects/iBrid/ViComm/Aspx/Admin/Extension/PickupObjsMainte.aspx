﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="PickupObjsMainte.aspx.cs" Inherits="Extension_PickupObjsMainte" Title="ピックアップ一括公開"
    ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="iBridCommLib" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="ピックアップ一括公開"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <table border="0" style="width: 640px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            サイト
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="240px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            ピックアップ種別
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstPickupId" runat="server" DataSourceID="dsPickup" DataTextField="PICKUP_TITLE" OnDataBound="lstPickupId_DataBound"
                                DataValueField="PICKUP_ID" Width="240px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <asp:Button ID="btnUpdate" runat="server" CssClass="seektopbutton" OnClick="btnUpdate_Click"
                    Text="実行" />
                <asp:Label ID="lblNoCharacter" runat="server" Text="※公開設定がなされているピックアップ画像/動画をサイトに公開します。" ForeColor="Red"></asp:Label>
                &nbsp;&nbsp;
                <asp:LinkButton ID="lnkMultiPickupMainte" runat="server" OnClick="lnkMultiPickupMainte_Click"
                    Text="ピックアップを確認"></asp:LinkButton>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsPickup" runat="server" SelectMethod="GetList" TypeName="Pickup">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
