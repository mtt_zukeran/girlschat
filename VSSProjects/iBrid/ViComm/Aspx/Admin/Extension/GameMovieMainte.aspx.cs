﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ゲーム動画認証
--	Progaram ID		: GameMovieMainte
--
--  Creation Date	: 2009.08.26
--  Creater			: i-Brid(Y.Inoue)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater		Update Explain

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class Extension_GameMovieMainte : System.Web.UI.Page {

	private string SiteCd {
		get { return iBridUtil.GetStringValue(this.ViewState["SiteCd"]); }
		set { this.ViewState["SiteCd"] = value; }
	}
	private string UserSeq {
		get { return iBridUtil.GetStringValue(this.ViewState["UserSeq"]); }
		set { this.ViewState["UserSeq"] = value; }
	}
	private string UserCharNo {
		get { return iBridUtil.GetStringValue(this.ViewState["UserCharNo"]); }
		set { this.ViewState["UserCharNo"] = value; }
	}
	private string MovieSeq {
		get { return iBridUtil.GetStringValue(this.ViewState["MovieSeq"]); }
		set { this.ViewState["MovieSeq"] = value; }
	}
	private string MovieTitle {
		get { return iBridUtil.GetStringValue(this.ViewState["MovieTitle"]); }
		set { this.ViewState["MovieTitle"] = value; }
	}
	private string ObjNotPublishFlag {
		get { return iBridUtil.GetStringValue(this.ViewState["ObjNotPublishFlag"]); }
		set { this.ViewState["ObjNotPublishFlag"] = value; }
	}
	private string Rowid {
		get { return iBridUtil.GetStringValue(this.ViewState["Rowid"]); }
		set { this.ViewState["Rowid"] = value; }
	}
	private string RevisionNo {
		get { return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]); }
		set { this.ViewState["RevisionNo"] = value; }
	}
	private string ReturnPage {
		get { return iBridUtil.GetStringValue(this.ViewState["ReturnPage"]); }
		set { this.ViewState["ReturnPage"] = value; }
	}
	protected string PrevNonPublicFlag {
		get { return this.ViewState["PrevNonPublicFlag"] as string; }
		set { this.ViewState["PrevNonPublicFlag"] = value; }
	}

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		this.SiteCd = Request.QueryString["sitecd"];
		this.UserSeq = Request.QueryString["userseq"];
		this.UserCharNo = Request.QueryString["usercharno"];
		this.MovieSeq = Request.QueryString["movieseq"];
		this.ReturnPage = Request.QueryString["return"];
		this.ObjNotPublishFlag = Request.QueryString["closed"];

		lblSiteCd.Text = this.SiteCd;
		lblUserSeq.Text = this.UserSeq;
		lblUserCharNo.Text = this.UserCharNo;

		using (Cast oCast = new Cast()) {
			string sName = string.Empty,sId = string.Empty;
			oCast.GetValue(lblUserSeq.Text,"CAST_NM",ref sName);
			oCast.GetValue(lblUserSeq.Text,"LOGIN_ID",ref sId);
			lblUserNm.Text = sName;
			lblLoginId.Text = sId;
		}
		ClearField();
		DataBind();
		GetData();
	}

	private void ClearField() {
		txtChargePoint.Text = string.Empty;
		txtMovieDoc.Text = string.Empty;
		lstAuthType.SelectedIndex = 0;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(ViCommConst.FLAG_OFF);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(ViCommConst.FLAG_ON);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		ReturnToCall();
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {

			db.PrepareProcedure("CAST_MOVIE_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,this.UserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,this.UserCharNo);
			db.ProcedureInParm("PMOVIE_SEQ",DbSession.DbType.VARCHAR2,this.MovieSeq);
			db.ProcedureOutParm("PMOVIE_TITLE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMOVIE_DOC",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCHARGE_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PUPLOAD_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMOVIE_TYPE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_MOVIE_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_MOVIE_ATTR_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPLAY_TIME",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMOVIE_SERIES",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSAMPLE_MOVIE_SEQ",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PTHUMBNAIL_PIC_SEQ",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			this.RevisionNo = db.GetStringValue("PREVISION_NO");
			this.Rowid = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtMovieDoc.Text = db.GetStringValue("PMOVIE_DOC");
				txtChargePoint.Text = db.GetStringValue("PCHARGE_POINT");
				this.MovieTitle = db.GetStringValue("PMOVIE_TITLE");
				string sUnAtuthFlag = db.GetStringValue("POBJ_NOT_APPROVE_FLAG");
				string sNotPublishFlag = db.GetStringValue("POBJ_NOT_PUBLISH_FLAG");
				if (sUnAtuthFlag.Equals("0")) {
					if (sNotPublishFlag.Equals("0")) {
						lstAuthType.SelectedValue = ViCommConst.MOVIE_APPLY_OK;
					} else if (sNotPublishFlag.Equals("1")) {
						lstAuthType.SelectedValue = ViCommConst.MOVIE_APPLY_NG;
					}
				} else if (sUnAtuthFlag.Equals("1")) {
					lstAuthType.SelectedValue = ViCommConst.MOVIE_APPLY_WAIT;
				}
			} else {
				ClearField();
			}
		}
		pnlDtl.Visible = true;
		this.PrevNonPublicFlag = lstAuthType.SelectedValue;
	}

	private void UpdateData(int pDelFlag) {
		int iNotApproveFlag = ViCommConst.FLAG_OFF;
		int iDelFlag = ViCommConst.FLAG_OFF;
		int iNotPublishFlag = ViCommConst.FLAG_OFF;

		if (pDelFlag == ViCommConst.FLAG_OFF) {
			switch (lstAuthType.SelectedValue) {
				case ViCommConst.MOVIE_APPLY_WAIT:	// 認証待ち
					iDelFlag = ViCommConst.FLAG_OFF;
					iNotApproveFlag = ViCommConst.FLAG_ON;
					iNotPublishFlag = ViCommConst.FLAG_OFF;
					break;

				case ViCommConst.MOVIE_APPLY_OK:	// 公開
					iDelFlag = ViCommConst.FLAG_OFF;
					iNotApproveFlag = ViCommConst.FLAG_OFF;
					iNotPublishFlag = ViCommConst.FLAG_OFF;
					break;

				case ViCommConst.MOVIE_APPLY_REMOVE: // 削除
					iDelFlag = ViCommConst.FLAG_ON;
					iNotApproveFlag = ViCommConst.FLAG_OFF;
					iNotPublishFlag = ViCommConst.FLAG_OFF;
					break;

				case ViCommConst.MOVIE_APPLY_NG:	// 非公開
					iDelFlag = ViCommConst.FLAG_OFF;
					iNotApproveFlag = ViCommConst.FLAG_OFF;
					iNotPublishFlag = ViCommConst.FLAG_ON;
					break;
			}
		} else {
			iDelFlag = ViCommConst.FLAG_ON;
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_MOVIE_MAINTE");
			db.ProcedureInParm("PMOVIE_SEQ",DbSession.DbType.VARCHAR2,this.MovieSeq);
			db.ProcedureInParm("PMOVIE_TITLE",DbSession.DbType.VARCHAR2,this.MovieTitle);
			db.ProcedureInParm("PMOVIE_DOC",DbSession.DbType.VARCHAR2,txtMovieDoc.Text);
			db.ProcedureInParm("PCHARGE_POINT",DbSession.DbType.NUMBER,decimal.Parse(txtChargePoint.Text));
			db.ProcedureInParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,iNotApproveFlag);
			db.ProcedureInParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER,iNotPublishFlag);
			db.ProcedureInParm("MOVIE_TYPE",DbSession.DbType.NUMBER,ViCommConst.ATTACHED_SOCIAL_GAME);
			db.ProcedureInParm("PCAST_MOVIE_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,ViCommConst.DEFAULT_CAST_MOVIE_ATTR_TYPE_SEQ);
			db.ProcedureInParm("PCAST_MOVIE_ATTR_SEQ",DbSession.DbType.VARCHAR2,ViCommConst.DEFAULT_CAST_MOVIE_ATTR_SEQ);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,this.Rowid);
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,iDelFlag);
			db.ProcedureInParm("PPLAY_TIME",DbSession.DbType.VARCHAR2,null);
			db.ProcedureInParm("PMOVIE_SERIES_SEQ",DbSession.DbType.NUMBER,null);
			db.ProcedureInParm("PTHUMBNAIL_PIC_SEQ",DbSession.DbType.NUMBER,null);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		if (iNotApproveFlag == ViCommConst.FLAG_OFF) {
			using (DbSession oDbSession = new DbSession()) {
				oDbSession.PrepareProcedure("ADD_CAST_GAME_POINT");
				oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
				oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,this.UserSeq);
				oDbSession.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,this.UserCharNo);
				oDbSession.ProcedureInParm("pOBJ_SEQ",DbSession.DbType.VARCHAR2,this.MovieSeq);
				oDbSession.ProcedureInParm("pOBJ_TYPE",DbSession.DbType.VARCHAR2,PwViCommConst.CastFile.MOVIE);
				oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
				oDbSession.ExecuteProcedure();
			}
		}

		bool bTxNotPublicNotice;
		using (ManageCompany oManageCompany = new ManageCompany()) {
			bTxNotPublicNotice = oManageCompany.IsAvailableService(ViCommConst.RELEASE_CAST_OBJ_NON_PUBLIC_INFO,2);
		}

		if (iDelFlag == ViCommConst.FLAG_ON) {
			string sWebPhisicalDir = "";
			using (Site oSite = new Site()) {
				oSite.GetValue(ViewState["SITE_CD"].ToString(),"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
			}

			using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
				string sMovie = sWebPhisicalDir +
									ViCommConst.MOVIE_DIRECTRY + "\\" + this.SiteCd + "\\Operator\\" + lblLoginId.Text + "\\" +
									ViCommConst.MOVIE_HEADER + iBridUtil.addZero(this.MovieSeq,ViCommConst.OBJECT_NM_LENGTH) +
									ViCommConst.MOVIE_FOODER;

				if (System.IO.File.Exists(sMovie)) {
					System.IO.File.Delete(sMovie);
				}

				sMovie = sWebPhisicalDir +
									ViCommConst.MOVIE_DIRECTRY + "\\" + this.SiteCd + "\\Operator\\" + lblLoginId.Text + "\\" +
									ViCommConst.MOVIE_HEADER + iBridUtil.addZero(this.MovieSeq,ViCommConst.OBJECT_NM_LENGTH) +
									".3g2";

				if (System.IO.File.Exists(sMovie)) {
					System.IO.File.Delete(sMovie);
				}
			}

			if (bTxNotPublicNotice) {
				Server.Transfer(string.Format("../CastAdmin/TxCastObjNonPublicMail.aspx?site={0}&userseq={1}&usercharNo={2}&mailtype={3}&return={4}",this.SiteCd,this.UserSeq,this.UserCharNo,ViCommConst.MAIL_TP_CAST_GAME_MOVIE_NP,this.ReturnPage));
			}
		}

		if (bTxNotPublicNotice && !this.PrevNonPublicFlag.Equals(ViCommConst.MOVIE_APPLY_NG) && this.lstAuthType.SelectedValue.Equals(ViCommConst.MOVIE_APPLY_NG)) {
			Server.Transfer(string.Format("../CastAdmin/TxCastObjNonPublicMail.aspx?site={0}&userseq={1}&usercharNo={2}&mailtype={3}&return={4}",this.SiteCd,this.UserSeq,this.UserCharNo,ViCommConst.MAIL_TP_CAST_GAME_MOVIE_NP,this.ReturnPage));
		} else {
			ReturnToCall();
		}
	}
	
	private void ReturnToCall() {
		if (this.ReturnPage.Equals("GameMovieCheckList")) {
			Server.Transfer(string.Format("../Extension/GameMovieCheckList.aspx?sitecd={0}&closed={1}",this.SiteCd,this.ObjNotPublishFlag));

		} else if (this.ReturnPage.Equals("GameMovieOpenList")) {
			Server.Transfer(string.Format("../Extension/GameMovieOpenList.aspx?sitecd={0}",this.SiteCd));

		} else {
			Server.Transfer(string.Format("../Cast/CastView.aspx?loginid={0}&sitecd={1}",lblLoginId.Text,this.SiteCd));
		}
	}

	protected void lstAuthType_DataBound(object sender,EventArgs e) {
		ListItem oItemDel = this.lstAuthType.Items.FindByValue(ViCommConst.MOVIE_APPLY_REMOVE);
		if (oItemDel != null) {
			this.lstAuthType.Items.Remove(oItemDel);
		}
	}
}
