﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 美人時計メンテ
--	Progaram ID		: BeautyClockMainte
--
--  Creation Date	: 2011.02.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.IO;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Extension_BeautyClockMainte:System.Web.UI.Page {
	protected string sSiteCd;
	protected string sAssignedTime;
	protected string sTime;
	protected void Page_Load(object sender,EventArgs e) {

		if (!IsPostBack) {
			InitPage();
		}
	}
	
	protected void InitPage(){

		ViewState["SITE_CD"] = iBridUtil.GetStringValue(this.Request["site_cd"]);
		ViewState["ASSIGNED_TIME"] = iBridUtil.GetStringValue(this.Request["time"]);
		ViewState["TIME"] = ViCommPrograms.ConvertAssignedToTime(ViewState["ASSIGNED_TIME"].ToString());
		ViewState["RETURN"] = iBridUtil.GetStringValue(this.Request["return"]);

		this.lblTime.Text = "「" + ViewState["TIME"].ToString()+ "」";
		this.lblTimeHeader.Text = ViewState["TIME"].ToString();
		this.lblTimeDetailHeader.Text = ViewState["TIME"].ToString();
		
		
	}

	protected void btnUpload_Click(object sender,EventArgs e) {

		ViewState["ACCESS_TIME"] = string.Format("{0:D4}{1:D2}{2:D2}{3:D2}{4:D2}{5:D2}{6:D6}",DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,DateTime.Now.Hour,DateTime.Now.Minute,DateTime.Now.Second,DateTime.Now.Millisecond);		
		
		this.lblNotFound.Visible = false;
		this.pnlMainte.Visible = false;
		this.pnlDetail.Visible = true;
		this.rptBeauty.DataSource = rptBeauty_DataBind();
		this.rptBeauty.DataBind();
		
		if(!this.lblNotFound.Visible) {
			if(uldBeautyPic.HasFile) {
				string sWebPhisicalDir = string.Empty;
				string sSiteCd = ViewState["SITE_CD"].ToString();
				string sAssignedTime = ViewState["ASSIGNED_TIME"].ToString();
				string sFileNm = string.Empty,sPath = string.Empty,sFullPath = string.Empty;

				using (Site oSite = new Site()) {
					oSite.GetValue(sSiteCd.ToString(),"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
				}

				sFileNm = this.GetTempPicFileName();
				sPath = ViCommPrograms.GetBeautyClockPicDir(sWebPhisicalDir,sSiteCd,sAssignedTime);
				sFullPath = sPath + "\\" + sFileNm + ViCommConst.PIC_FOODER;

				using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,string.Empty,ViCommConst.FILE_UPLOAD_PASSWORD)) {
					if (!System.IO.File.Exists(sFullPath)) {
						uldBeautyPic.SaveAs(sFullPath);
					}
				}
			}		
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		string sReturnUrl = HttpUtility.UrlDecode(ViewState["RETURN"].ToString());
		Response.Redirect(sReturnUrl);
		
	}

	protected void btnPublish_Click(object sender,EventArgs e) {
		this.UploadPicture();
	}

	protected void btnMainte_Click(object sender,EventArgs e) {

		this.pnlMainte.Visible = true;
		this.pnlDetail.Visible = false;

	}

	private void RedirectBeautyClockList() {
		UrlBuilder oUrlBuilder = new UrlBuilder("BeautyClockList.aspx");
		Response.Redirect(oUrlBuilder.ToString());
	}
	
	private void UploadPicture() {
		string sWebPhisicalDir = string.Empty;
		string sSiteCd = ViewState["SITE_CD"].ToString();
		string sAssignedTime = ViewState["ASSIGNED_TIME"].ToString();

		using (Site oSite = new Site()) {
			oSite.GetValue(sSiteCd.ToString(),"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
		}

		using (BeautyClock objBeauty = new BeautyClock()) {
			decimal dNo = objBeauty.GetPicNo();

			string sFileNm = string.Empty,sPath = string.Empty,sFullPath = string.Empty;
			string sSourceFileNm = string.Empty;
			string sSourceFullPath = string.Empty;
			
			sFileNm = ViCommPrograms.GeneratePicFileNm(dNo);
			sPath = ViCommPrograms.GetBeautyClockPicDir(sWebPhisicalDir,sSiteCd,sAssignedTime);
			sFullPath = sPath + "\\" + sFileNm + ViCommConst.PIC_FOODER;

			sSourceFileNm = this.GetTempPicFileName();
			sSourceFullPath = sPath+"\\"+sSourceFileNm+ViCommConst.PIC_FOODER;
			using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,string.Empty,ViCommConst.FILE_UPLOAD_PASSWORD)) {
				
				if (!System.IO.File.Exists(sFullPath)) {
					System.IO.File.Move(sSourceFullPath,sFullPath);
						
					// オリジナル画像をMailToolsディレクトリに配置
					string sInputDir = ConfigurationManager.AppSettings["MailParseDir"] + "\\";
					System.IO.File.Copy(sFullPath,Path.Combine(sInputDir,sFileNm + ViCommConst.PIC_FOODER));
					ImageHelper.ConvertMobileImage(sSiteCd,sInputDir,sPath,sFileNm);
				}	
			}
			
			
			using (CastCharacter oCast = new CastCharacter()){
				DataSet ds = oCast.GetOne(sSiteCd,lblUserId.Text,ViCommConst.MAIN_CHAR_NO);
				using (DbSession db = new DbSession()) {
					db.PrepareProcedure("BEAUTY_CLOCK_REGIST");
					db.ProcedureInParm("PASSIGNED_TIME",DbSession.DbType.NUMBER,int.Parse(ViewState["ASSIGNED_TIME"].ToString()));
					db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,sSiteCd);
					db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.NUMBER,int.Parse(ds.Tables[0].Rows[0]["USER_SEQ"].ToString()));
					db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViCommConst.MAIN_CHAR_NO);
					db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.NUMBER,dNo);
					db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
					db.ExecuteProcedure();
				}
			}
			this.RedirectBeautyClockList();			
		}
	}

	protected ICollection rptBeauty_DataBind() {
		string sSiteCd = ViewState["SITE_CD"].ToString();
		string sUserId = lblUserId.Text;
		
		DataSet ds;
		using(CastCharacter oCast = new CastCharacter()){
			ds = oCast.GetOne(sSiteCd,lblUserId.Text,ViCommConst.MAIN_CHAR_NO);
			if(ds.Tables[0].Rows.Count <= 0){
				NotFoundData();
			}
		}
		DataView dv = new DataView(ds.Tables[0]);
		return dv;
	}
	
	protected void NotFoundData(){
		this.pnlMainte.Visible = true;
		this.pnlDetail.Visible = false;
		
		this.lblNotFound.Visible = true;
	}
	
	protected string GetTime() {
	
		return ViewState["TIME"].ToString();
	}

	protected string GetRegistTime() {

		return string.Format("{0:yyyy/M/d HH:mm}",DateTime.Now);
	}
	
	protected string GetTempPicFileName(){
		return string.Format("{0}{1}",ViCommConst.PIC_HEADER,ViewState["ACCESS_TIME"].ToString());
	}
	
	protected string GetTempPicPath(){
		return string.Format("../extensionres/beautyclock/{0}/{1}/{2}.jpg",ViewState["SITE_CD"].ToString(),ViewState["ASSIGNED_TIME"].ToString(),ViewState["ACCESS_TIME"].ToString());
	}
}
