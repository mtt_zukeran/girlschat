<%@ Import Namespace="ViComm" %>

<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="QuickResponseSummary.aspx.cs" Inherits="Extension_QuickResponseSummary"
	Title="10分以内の返信集計" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="10分以内の返信集計"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlKey">
			<fieldset class="fieldset">
				<legend>[検索]</legend>
				<asp:Panel ID="pnlSeekCondition" runat="server">
					<table border="0" style="width: 600px" class="tableStyle">
						<tr>
							<td class="tdHeaderSmallStyle2">
								サイト
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px">
								</asp:DropDownList>
							</td>
							<td class="tdHeaderSmallStyle2">
								稼動日
							</td>
							<td class="tdDataStyle" colspan="3">
								<asp:DropDownList ID="lstYYYY" runat="server" Width="60px">
								</asp:DropDownList>年
								<asp:DropDownList ID="lstMM" runat="server" Width="40px">
								</asp:DropDownList>月
								<asp:DropDownList ID="lstDD" runat="server" Width="40px">
								</asp:DropDownList>日
							</td>
						</tr>
					</table>
					<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" />
					<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[返信集計一覧]</legend>
				<asp:Panel ID="pnlGrid" runat="server">
					<asp:GridView ID="grdQuickResSummary" runat="server" AllowPaging="false" AutoGenerateColumns="False" DataSourceID="dsQuickResReport" SkinID="GridViewColor">
						<Columns>
							<asp:TemplateField HeaderText="時刻">
								<ItemTemplate>
									<asp:Label runat="server" ID="lblReportHour" Text='<%# Eval("REPORT_HOUR") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Wrap="false" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾒｰﾙ件数">
								<ItemTemplate>
									<asp:HyperLink ID="lnkLoginId" runat="server" Text='<%# Eval("RX_MAIL_COUNT") %>' NavigateUrl='<%# GetDetailLink(Eval("SITE_CD"),Eval("REPORT_DAY"),Eval("REPORT_HOUR")) %>'></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" Wrap="false" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="最大入室人数">
								<ItemTemplate>
									<asp:Label runat="server" ID="lblLoginId" Text='<%# Eval("MAX_ROOM_IN_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" Wrap="false" />
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
			<br />
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsQuickResReport" runat="server" SelectMethod="GetPageCollection" TypeName="QuickResReport" SelectCountMethod="GetPageCount"
		EnablePaging="True" OnSelecting="dsQuickResReport_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pReportDay" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
