﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: バイト一覧

--	Progaram ID		: PartTimeJobPlanList
--
--  Creation Date	: 2011.07.25
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.IO;
using System.Drawing;

public partial class Extension_PartTimeJobPlanList : System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string PartTimeJobSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["PartTimeJobSeq"]);
		}
		set {
			this.ViewState["PartTimeJobSeq"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
		this.PartTimeJobSeq = string.Empty;
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.UpdateData(false);
		this.pnlPartTimeJobPlanInfo.Visible = false;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		this.UpdateData(true);
		this.pnlPartTimeJobPlanInfo.Visible = false;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.ClearFileds();
		this.pnlKey.Enabled = true;
		this.pnlPartTimeJobPlanInfo.Visible = false;
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		this.PartTimeJobSeq = string.Empty;
		this.ClearFileds();
		this.pnlKey.Enabled = false;
		this.pnlPartTimeJobPlanInfo.Visible = true;
	}

	protected void lnkEdit_Command(object sender,CommandEventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.PartTimeJobSeq = iBridUtil.GetStringValue(e.CommandArgument);
		this.GetData();
		this.pnlKey.Enabled = false;
		this.pnlPartTimeJobPlanInfo.Visible = true;
	}

	protected void dsPartTimeJobPlan_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	private void InitPage() {
		this.pnlInfo.Visible = true;
		this.pnlPartTimeJobPlanInfo.Visible = false;

		this.PartTimeJobSeq = string.Empty;
		this.pnlKey.Enabled = true;
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		this.lstStageSeq.DataBind();
	}

	private void ClearFileds() {
		this.lblPartTimeJobId.Text = string.Empty;
		this.txtPartTimeJobNm.Text = string.Empty;
		this.txtIncome.Text = string.Empty;
		this.txtWaitingMin.Text = string.Empty;
		this.lstStageSeq.DataBind();
		this.pnlKey.Enabled = true;
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.PartTimeJobSeq = string.Empty;

		this.grdPartTimeJobPlan.PageIndex = 0;
		this.grdPartTimeJobPlan.DataSourceID = "dsPartTimeJobPlan";
		this.grdPartTimeJobPlan.DataBind();
		this.pnlCount.DataBind();
	}

	private void GetData() {
		this.ClearFileds();

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("PART_TIME_JOB_PLAN_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pPART_TIME_JOB_SEQ",DbSession.DbType.VARCHAR2,this.PartTimeJobSeq);
			oDbSession.ProcedureOutParm("pPART_TIME_JOB_NM",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pINCOME",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pWAITING_MIN",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTAGE_SEQ",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.lblPartTimeJobId.Text = this.PartTimeJobSeq;
			this.txtPartTimeJobNm.Text = oDbSession.GetStringValue("pPART_TIME_JOB_NM");
			this.txtIncome.Text = oDbSession.GetStringValue("pINCOME");
			this.txtWaitingMin.Text = oDbSession.GetStringValue("pWAITING_MIN");
			this.lstStageSeq.SelectedValue = oDbSession.GetStringValue("pSTAGE_SEQ");
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
		}
	}

	private void UpdateData(bool pDeleteFlag) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("PART_TIME_JOB_PLAN_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureBothParm("pPART_TIME_JOB_SEQ",DbSession.DbType.VARCHAR2,this.PartTimeJobSeq);
			oDbSession.ProcedureInParm("pPART_TIME_JOB_NM",DbSession.DbType.VARCHAR2,this.txtPartTimeJobNm.Text);
			oDbSession.ProcedureInParm("pINCOME",DbSession.DbType.NUMBER,this.txtIncome.Text);
			oDbSession.ProcedureInParm("pWAITING_MIN",DbSession.DbType.NUMBER,this.txtWaitingMin.Text);
			oDbSession.ProcedureInParm("pSTAGE_SEQ",DbSession.DbType.NUMBER,this.lstStageSeq.SelectedValue);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDeleteFlag ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.ClearFileds();
		this.GetList();
	}
}
