﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 美人時計詳細
--	Progaram ID		: BeautyClockList
--
--  Creation Date	: 2011.02.24
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.IO;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Extension_BeautyClockView:System.Web.UI.Page {
    private readonly int TIME_PREV = -1;
	private readonly int TIME_NEXT = 1;
	private readonly int TIME_CURRENT = 0;
	protected void Page_Load(object sender,EventArgs e) {
		if (!this.IsPostBack) {
			ViewState["SITE_CD"] = iBridUtil.GetStringValue(this.Request["site_cd"]);
			ViewState["ASSIGNED_TIME"] = iBridUtil.GetStringValue(this.Request["time"]);
			ViewState["TIME"] = ViCommPrograms.ConvertAssignedToTime(ViewState["ASSIGNED_TIME"].ToString());
			this.lblTime.Text = ViewState["TIME"].ToString();

			this.lnkPrevTime.Visible = !(int.Parse(ViewState["ASSIGNED_TIME"].ToString()) <= ViCommConst.BeautyClock.MIN_ASSIGNED_TIME) ;
			this.lnkNextTime.Visible = !(ViCommConst.BeautyClock.MAX_ASSIGNED_TIME <= int.Parse(ViewState["ASSIGNED_TIME"].ToString()) );

			this.InitPage();
		}
	}

	protected void InitPage() {
		using (BeautyClock oBeauty = new BeautyClock()) {
			DataSet ds = oBeauty.GetListByAssignedTime(ViewState["SITE_CD"].ToString(),ViewState["ASSIGNED_TIME"].ToString());

			rptPublish.DataSource = PublishDataSource(ds.Tables[0]);
			rptPublish.DataBind();

			rptUnPublish.DataSource = UnPublishDataSource(ds.Tables[0]);
			rptUnPublish.DataBind();

		}
	
	}
	protected ICollection PublishDataSource(DataTable pDt){

		DataView dv = null;
		DataTable dt = pDt.Clone();
		if(pDt.Rows.Count > 0) {
			if (pDt.Rows[0]["PUBLISH_FLAG"].ToString().Equals(ViCommConst.FLAG_ON_STR)){
				dt.ImportRow(pDt.Rows[0]);
				dv = new DataView(dt);
			}
		}
		return dv;
	}

	protected ICollection UnPublishDataSource(DataTable pDt) {

		DataView dv = null;
		DataTable dt = pDt.Clone();
		
		if(pDt.Rows.Count > 0){
			for (int i = 0;i < pDt.Rows.Count;i++) {
				if (pDt.Rows[i]["PUBLISH_FLAG"].ToString().Equals(ViCommConst.FLAG_OFF_STR)){
					dt.ImportRow(pDt.Rows[i]);
				}
			}
		}

		if(dt.Rows.Count > 0) {
			dv = new DataView(dt);
		}
		return dv;
	}

	protected void lnkChange_OnCommand(object sender,CommandEventArgs e) {
		string[] sKey  = e.CommandArgument.ToString().Split(':');
		string sUserSeq = sKey[0];
		string sPicSeq = sKey[1];
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("BEAUTY_CLOCK_CHANGE");
			db.ProcedureInParm("PASSIGNED_TIME",DbSession.DbType.NUMBER,int.Parse(ViewState["ASSIGNED_TIME"].ToString()));
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,ViewState["SITE_CD"].ToString());
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.NUMBER,int.Parse(sUserSeq.ToString()));
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViCommConst.MAIN_CHAR_NO);
			db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.NUMBER,int.Parse(sPicSeq.ToString()));
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
	
	protected void rptUnPublish_OnItemCommand(object sender, RepeaterCommandEventArgs e){
		this.InitPage();
	}
	
	protected void lnkPrevTime_OnCommand(object sender,CommandEventArgs e) {
		Response.Redirect(GetPagingUrl(TIME_PREV));
	}

	protected void lnkNextTime_OnCommand(object sender,CommandEventArgs e) {
		Response.Redirect(GetPagingUrl(TIME_NEXT));
	}

	protected void lnkBeautyClockRegist_OnCommand(object sender,CommandEventArgs e) {
		string sRreturnUrl = HttpUtility.UrlEncode(string.Format("{0}",GetPagingUrl(TIME_CURRENT)));
		Response.Redirect(string.Format("./BeautyClockMainte.aspx?site_cd={0}&time={1}&return={2}",ViewState["SITE_CD"].ToString(),ViewState["ASSIGNED_TIME"].ToString(),sRreturnUrl));
	}
	protected string GetPagingUrl(int iDiffTime){
		return string.Format("./BeautyClockView.aspx?site_cd={0}&time={1}",ViewState["SITE_CD"].ToString(),(int.Parse(ViewState["ASSIGNED_TIME"].ToString()) + iDiffTime));
	}
}
