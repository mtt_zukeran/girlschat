﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ブログ投稿

--	Progaram ID		: BlogArticleList
--
--  Creation Date	: 2011.03.30
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using ViComm;
using iBridCommLib;
using System.Collections.Generic;

public partial class Extension_BlogArticleList:System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string LoginId {
		get {
			return iBridUtil.GetStringValue(this.ViewState["LoginId"]);
		}
		set {
			this.ViewState["LoginId"] = value;
		}
	}

	private string SortExpression {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortExpression"]);
		}
		set {
			this.ViewState["SortExpression"] = value;
		}
	}

	private string SortDirect {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortDirect"]);
		}
		set {
			this.ViewState["SortDirect"] = value;
		}
	}

	private DataSet BlogArticleData {
		get {
			return this.ViewState["BlogArticleData"] as DataSet;
		}
		set {
			this.ViewState["BlogArticleData"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();

			this.pnlInfo.Visible = true;
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.SortDirect = string.Empty;
		this.SortExpression = string.Empty;

		this.pnlInfo.Visible = true;
		this.GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void lnkSetPublish_OnCommand(object sender,CommandEventArgs e) {
		int iRowIndex = int.Parse(e.CommandArgument.ToString());

		DataRow oDataRow = this.BlogArticleData.Tables[0].Rows[iRowIndex];
		string sBlogArticleStatus;
		switch (iBridUtil.GetStringValue(oDataRow["BLOG_ARTICLE_STATUS"])) {
			case ViCommConst.BlogArticleStatus.PRIVATE:
				sBlogArticleStatus = ViCommConst.BlogArticleStatus.PUBLIC;
				break;
			case ViCommConst.BlogArticleStatus.PUBLIC:
				sBlogArticleStatus = ViCommConst.BlogArticleStatus.PRIVATE;
				break;
			default:
				sBlogArticleStatus = iBridUtil.GetStringValue(oDataRow["BLOG_ARTICLE_STATUS"]);
				break;
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("BLOG_ARTICLE_STATUS_UPDATE");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, iBridUtil.GetStringValue(oDataRow["SITE_CD"]));
			oDbSession.ProcedureInParm("pUSER_SEQ", DbSession.DbType.NUMBER, iBridUtil.GetStringValue(oDataRow["USER_SEQ"]));
			oDbSession.ProcedureInParm("pUSER_CHAR_NO", DbSession.DbType.VARCHAR2, iBridUtil.GetStringValue(oDataRow["USER_CHAR_NO"]));
			oDbSession.ProcedureInParm("pBLOG_SEQ", DbSession.DbType.NUMBER, iBridUtil.GetStringValue(oDataRow["BLOG_SEQ"]));
			oDbSession.ProcedureInParm("pBLOG_ARTICLE_SEQ", DbSession.DbType.NUMBER, iBridUtil.GetStringValue(oDataRow["BLOG_ARTICLE_SEQ"]));
			oDbSession.ProcedureInParm("pBLOG_ARTICLE_STATUS", DbSession.DbType.VARCHAR2, sBlogArticleStatus);
			oDbSession.ProcedureInParm("pREVISION_NO", DbSession.DbType.NUMBER, iBridUtil.GetStringValue(oDataRow["REVISION_NO"]));
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
		}

		int iPageIndex = this.grdBlogArticle.PageIndex;
		this.GetList();
		this.grdBlogArticle.PageIndex = iPageIndex;
	}

	protected void lnkSetDraft_OnCommand(object sender,CommandEventArgs e) {
		int iRowIndex = int.Parse(e.CommandArgument.ToString());
		DataRow oDataRow = this.BlogArticleData.Tables[0].Rows[iRowIndex];

		if (iBridUtil.GetStringValue(oDataRow["BLOG_ARTICLE_STATUS"]).Equals(ViCommConst.BlogArticleStatus.PRIVATE)) {
			using (DbSession oDbSession = new DbSession()) {
				oDbSession.PrepareProcedure("BLOG_ARTICLE_STATUS_UPDATE");
				oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(oDataRow["SITE_CD"]));
				oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.NUMBER,iBridUtil.GetStringValue(oDataRow["USER_SEQ"]));
				oDbSession.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(oDataRow["USER_CHAR_NO"]));
				oDbSession.ProcedureInParm("pBLOG_SEQ",DbSession.DbType.NUMBER,iBridUtil.GetStringValue(oDataRow["BLOG_SEQ"]));
				oDbSession.ProcedureInParm("pBLOG_ARTICLE_SEQ",DbSession.DbType.NUMBER,iBridUtil.GetStringValue(oDataRow["BLOG_ARTICLE_SEQ"]));
				oDbSession.ProcedureInParm("pBLOG_ARTICLE_STATUS",DbSession.DbType.VARCHAR2,ViCommConst.BlogArticleStatus.DRAFT);
				oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,iBridUtil.GetStringValue(oDataRow["REVISION_NO"]));
				oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
				oDbSession.cmd.BindByName = true;
				oDbSession.ExecuteProcedure();
			}

			int iPageIndex = this.grdBlogArticle.PageIndex;
			this.GetList();
			this.grdBlogArticle.PageIndex = iPageIndex;
		}
	}

	protected void grdBlogArticle_Sorting(object sender,GridViewSortEventArgs e) {
		if (this.SortExpression.Equals(e.SortExpression)) {
			if (this.SortDirect.Equals("ASC")) {
				this.SortDirect = "DESC";
			} else if (this.SortDirect.Equals("DESC")) {
				this.SortDirect = "ASC";
			}
		} else {
			this.SortDirect = "ASC";
		}

		this.SortExpression = e.SortExpression;

		e.Cancel = true;
		this.GetList();
	}

	protected void grdBlogArticle_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			if (ViCommConst.FLAG_ON_STR.Equals(DataBinder.Eval(e.Row.DataItem,"DEL_FLAG").ToString())) {
				e.Row.BackColor = Color.Gainsboro;
			}
		}
	}

	protected void dsBlogArticle_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		DataSet oBlogArticleDataSet = e.ReturnValue as DataSet;
		if (oBlogArticleDataSet != null && (oBlogArticleDataSet).Tables[0].Rows.Count > 0) {
			this.BlogArticleData = oBlogArticleDataSet;
		}
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsBlogArticle_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		BlogArticle.SearchCondition oSearchCondition = new BlogArticle.SearchCondition();
		oSearchCondition.AttachedMovieFlag = this.chkMovie.Checked;
		oSearchCondition.AttachedPicFlag = this.chkPic.Checked;
		if (this.rdoPublishedNG.Checked) {
			oSearchCondition.BlogArticleStatus = ViCommConst.BlogArticleStatus.PRIVATE;
		}
		if (this.rdoPublishedOK.Checked) {
			oSearchCondition.BlogArticleStatus = ViCommConst.BlogArticleStatus.PUBLIC;
		}
		oSearchCondition.Keyword = this.txtKeyword.Text.Trim();
		oSearchCondition.LoginId = this.txtLoginId.Text.Trim();
		if (this.rdoPickupOff.Checked) {
			oSearchCondition.PickupFlag = ViCommConst.FLAG_OFF_STR;
		}
		if (this.rdoPickupOn.Checked) {
			oSearchCondition.PickupFlag = ViCommConst.FLAG_ON_STR;
		}
		oSearchCondition.RegistDateFrom = string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue,this.lstFromHH.SelectedValue,this.lstFromMI.SelectedValue);
		string sToDD;
		if (this.lstToDD.SelectedIndex == 0) {
			sToDD = DateTime.DaysInMonth(int.Parse(this.lstToYYYY.SelectedValue),int.Parse(this.lstToMM.SelectedValue)).ToString();
		} else {
			sToDD = this.lstToDD.SelectedValue;
		}
		oSearchCondition.RegistDateTo = string.Format("{0}/{1}/{2} {3}:{4}:59",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,sToDD,this.lstToHH.SelectedValue,this.lstToMI.SelectedValue);
		oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
		// チェック状態
		if (chkChecked.Checked && !chkUnChecked.Checked) {
			oSearchCondition.CheckedFlag = ViCommConst.FLAG_ON_STR;
		} else if (!chkChecked.Checked && chkUnChecked.Checked) {
			oSearchCondition.CheckedFlag = ViCommConst.FLAG_OFF_STR;
		} else {
			oSearchCondition.CheckedFlag = string.Empty;
		}
		oSearchCondition.SortDirection = this.SortDirect;
		oSearchCondition.SortExpression = this.SortExpression;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void vdcFromTo_ServerValidate(object source,ServerValidateEventArgs e) {
		if (this.IsValid) {
			if (e.IsValid) {
				DateTime dtFrom;
				DateTime dtTo;
				if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue,this.lstFromHH.SelectedValue,this.lstFromMI.SelectedValue),out dtFrom)) {
					this.vdcFromTo.Text = "作成日時Fromに正しい日時を設定してください。";
					e.IsValid = false;
					return;
				}
				string sToDD;
				if (this.lstToDD.SelectedIndex == 0) {
					sToDD = DateTime.DaysInMonth(int.Parse(this.lstToYYYY.SelectedValue),int.Parse(this.lstToMM.SelectedValue)).ToString();
				} else {
					sToDD = this.lstToDD.SelectedValue;
				}
				if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:59",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,sToDD,this.lstToHH.SelectedValue,this.lstToMI.SelectedValue),out dtTo)) {
					this.vdcFromTo.Text = "作成日時Toに正しい日時を設定してください。";
					e.IsValid = false;
					return;
				}
				if (dtFrom >= dtTo) {
					this.vdcFromTo.Text = "作成日時の大小関係に誤りがあります。";
					e.IsValid = false;
				}
			}
		}
	}

	private void InitPage() {
		if (!this.IsPostBack) {
			SysPrograms.SetupFromToDayTime(this.lstFromYYYY,this.lstFromMM,this.lstFromDD,this.lstFromHH,this.lstToYYYY,this.lstToMM,this.lstToDD,this.lstToHH,false);
			this.lstFromMM.Items.Insert(0,new ListItem("--","01"));
			this.lstToMM.Items.Insert(0,new ListItem("--","12"));
			this.lstFromDD.Items.Insert(0,new ListItem("--","01"));
			this.lstToDD.Items.Insert(0,new ListItem("--","31"));
			this.lstFromHH.Items.Insert(0,new ListItem("--","00"));
			this.lstToHH.Items.Insert(0,new ListItem("--","23"));

			for (int i = 0;i < 60;i++) {
				this.lstFromMI.Items.Add(new ListItem(string.Format("{0:D2}",i)));
				this.lstToMI.Items.Add(new ListItem(string.Format("{0:D2}",i)));
			}
			this.lstFromMI.Items.Insert(0,new ListItem("--","00"));
			this.lstToMI.Items.Insert(0,new ListItem("--","59"));
		}

		this.recCount = "0";
		this.txtLoginId.Text = string.Empty;
		this.txtKeyword.Text = string.Empty;
		this.grdBlogArticle.DataSourceID = string.Empty;

		this.lstFromYYYY.SelectedIndex = 0;
		this.lstToYYYY.SelectedIndex = 0;
		this.lstFromMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstToMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstFromDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstToDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstFromHH.SelectedIndex = 0;
		this.lstToHH.SelectedIndex = 0;
		this.lstFromMI.SelectedIndex = 0;
		this.lstToMI.SelectedIndex = 0;

		this.chkMovie.Checked = true;
		this.chkPic.Checked = true;
		this.rdoPickupAll.Checked = true;
		this.rdoPublishedAll.Checked = true;

		this.pnlInfo.Visible = false;
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		if (!this.IsPostBack) {
			this.SiteCd = this.Request.QueryString["sitecd"];
			this.LoginId = this.Request.QueryString["loginid"];

			// チェック状態
			if (iBridUtil.GetStringValue(Request.QueryString["uncheck"]).Equals("1")) {
				// 非表示
				rdoPublishedAll.Checked = false;
				rdoPublishedOK.Checked = false;
				rdoPublishedNG.Checked = true;
				// 未チェック
				chkChecked.Checked = false;
				chkUnChecked.Checked = true;
				// 対象日付（開始日）
				lstFromYYYY.SelectedIndex = lstFromYYYY.Items.Count - 1;
				lstFromMM.SelectedIndex = 0;
				lstFromDD.SelectedIndex = 0;
			} else {
				chkChecked.Checked = false;
				chkUnChecked.Checked = false;
			}

			if (!string.IsNullOrEmpty(this.SiteCd) && !string.IsNullOrEmpty(this.LoginId)) {
				this.lstSiteCd.SelectedValue = this.SiteCd;
				this.txtLoginId.Text = this.LoginId;

				this.pnlInfo.Visible = true;
				this.GetList();
			}
		}

		this.SortDirect = string.Empty;
		this.SortExpression = string.Empty;
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.grdBlogArticle.PageIndex = 0;
		this.grdBlogArticle.PageSize = 10;
		this.grdBlogArticle.DataSourceID = "dsBlogArticle";
		this.grdBlogArticle.DataBind();
		this.pnlCount.DataBind();
	}

	protected string GetBlogArticleViewLink(object pDataItem) {
		DataRowView oDataRowView = (DataRowView)pDataItem;

		return string.Format("~/Extension/BlogArticleView.aspx?sitecd={0}&userseq={1}&usercharno={2}&blogseq={3}&blogarticleseq={4}",
			oDataRowView["SITE_CD"],oDataRowView["USER_SEQ"],oDataRowView["USER_CHAR_NO"],oDataRowView["BLOG_SEQ"],oDataRowView["BLOG_ARTICLE_SEQ"]);
	}

	protected string GetSetPublishLinkText(object pBlogArticleStatus) {
		switch (iBridUtil.GetStringValue(pBlogArticleStatus)) {
			case ViCommConst.BlogArticleStatus.PRIVATE:
				return "[公開する]";
			case ViCommConst.BlogArticleStatus.PUBLIC:
				return "[非公開にする]";
			default:
				return string.Empty;
		}
	}

	protected string GetDisplayBlogArticleBody(object pBlogArticleBody) {
		string sBody = iBridUtil.GetStringValue(pBlogArticleBody);
		if (sBody.Length > 75) {
			return SysPrograms.Substring(iBridUtil.GetStringValue(pBlogArticleBody),75);
		} else {
			return sBody;
		}
	}

	protected bool GetImageVisible(object pBlogFileType) {
		string sBlogFileType = iBridUtil.GetStringValue(pBlogFileType);

		switch (sBlogFileType) {
			case ViCommConst.BlogFileType.PIC:
				return true;
			default:
				return false;
		}
	}

	protected bool GetDraftVisible(object pBlogArticleStatus) {
		string sBlogArticleStatus = iBridUtil.GetStringValue(pBlogArticleStatus);

		switch (sBlogArticleStatus) {
			case ViCommConst.BlogArticleStatus.PRIVATE:
				return true;
			case ViCommConst.BlogArticleStatus.PUBLIC:
				return false;
			default:
				return false;
		}
	}

	protected bool GetDelFlagVisible(object pDelFlag) {
		string sDelFlag = iBridUtil.GetStringValue(pDelFlag);

		switch (sDelFlag) {
			case ViCommConst.FLAG_ON_STR:
				return true;
			case ViCommConst.FLAG_OFF_STR:
				return false;
			default:
				return false;
		}
	}
}
