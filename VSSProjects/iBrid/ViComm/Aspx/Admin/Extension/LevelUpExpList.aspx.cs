﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ステージ一覧

--	Progaram ID		: GameItemList
--
--  Creation Date	: 2011.07.25
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.IO;
using System.Drawing;

public partial class Extension_LevelUpExpList:System.Web.UI.Page {
	protected string[] DummyArray = new string[3];
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		set {
			this.ViewState["SexCd"] = value;
		}
	}

	private string GameCharacterLevel {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameCharacterLevel"]);
		}
		set {
			this.ViewState["GameCharacterLevel"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();
			this.SexCd = iBridUtil.GetStringValue(this.Request.QueryString["sexcd"]);
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.UpdateData(false);
		this.pnlLevelInfo.Visible = false;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		this.UpdateData(true);
		this.pnlLevelInfo.Visible = false;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.ClearFileds();
		this.pnlKey.Enabled = true;
		this.pnlLevelInfo.Visible = false;
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		this.ClearFileds();
		this.pnlKey.Enabled = false;
		this.pnlLevelInfo.Visible = true;
	}

	protected void dsLevelUpExp_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void lnkEdit_Command(object sender,CommandEventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.GameCharacterLevel = iBridUtil.GetStringValue(e.CommandArgument);
		this.GetData();
		this.pnlKey.Enabled = false;
		this.pnlLevelInfo.Visible = true;
		this.txtGameCharacterLevel.ReadOnly = true;
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	private void InitPage() {
		this.pnlInfo.Visible = true;
		this.pnlLevelInfo.Visible = false;

		this.GameCharacterLevel = string.Empty;
		this.pnlKey.Enabled = true;
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void ClearFileds() {
		this.txtGameCharacterLevel.Text = string.Empty;
		this.txtNeedExperiencePoint.Text = string.Empty;
		this.txtCharacterHp.Text = string.Empty;
		this.txtAddForceCount.Text = string.Empty;
		this.txtFerrowCountLimit.Text = string.Empty;
		this.txtGameCharacterLevel.ReadOnly = false;

		this.pnlKey.Enabled = true;
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.GameCharacterLevel = string.Empty;
		this.grdLevelUpExp.PageSize = 30;

		this.grdLevelUpExp.PageIndex = 0;
		this.grdLevelUpExp.DataSourceID = "dsLevelUpExp";
		this.grdLevelUpExp.DataBind();
		this.pnlCount.DataBind();
	}

	private void GetData() {
		this.ClearFileds();

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("LEVEL_UP_EXP_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pGAME_CHARACTER_LEVEL",DbSession.DbType.VARCHAR2,this.GameCharacterLevel);
			oDbSession.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,this.SexCd);
			oDbSession.ProcedureOutParm("pNEED_EXPERIENCE_POINT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pCHARACTER_HP",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pFELLOW_COUNT_LIMIT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pADD_FORCE_COUNT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.txtGameCharacterLevel.Text = this.GameCharacterLevel;
			this.txtNeedExperiencePoint.Text = oDbSession.GetStringValue("pNEED_EXPERIENCE_POINT");
			this.txtCharacterHp.Text = oDbSession.GetStringValue("pCHARACTER_HP");
			this.txtAddForceCount.Text = oDbSession.GetStringValue("pADD_FORCE_COUNT");
			this.txtFerrowCountLimit.Text = oDbSession.GetStringValue("pFELLOW_COUNT_LIMIT");
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
		}
	}

	private void UpdateData(bool pDeleteFlag) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("LEVEL_UP_EXP_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pGAME_CHARACTER_LEVEL",DbSession.DbType.VARCHAR2,this.txtGameCharacterLevel.Text);
			oDbSession.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,this.SexCd);
			oDbSession.ProcedureInParm("pNEED_EXPERIENCE_POINT",DbSession.DbType.NUMBER,this.txtNeedExperiencePoint.Text);
			oDbSession.ProcedureInParm("pCHARACTER_HP",DbSession.DbType.NUMBER,this.txtCharacterHp.Text);
			oDbSession.ProcedureInParm("pFELLOW_COUNT_LIMIT",DbSession.DbType.NUMBER,this.txtFerrowCountLimit.Text);
			oDbSession.ProcedureInParm("pADD_FORCE_COUNT",DbSession.DbType.NUMBER,this.txtAddForceCount.Text);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDeleteFlag ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.ClearFileds();
		this.GetList();
	}
}
