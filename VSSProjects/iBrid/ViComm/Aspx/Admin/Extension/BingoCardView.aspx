﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="BingoCardView.aspx.cs" Inherits="Extension_BingoCardView" Title="ビンゴカード一覧"
    ValidateRequest="false" %>
<%@ Import Namespace="ViComm" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="ビンゴカード一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 400px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" Enabled="false"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle2">
                                作成済み枚数
                            </td>
                            <td class="tdDataStyle">
                                <asp:Label ID="lblCount" runat="server" Text='<%# string.Concat(GetCardCount(), "枚") %>'></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnRegist" Text="追加" CssClass="seekbutton" OnClick="btnRegist_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button ID="btnBack" runat="server" Text="戻る" CssClass="seekbutton" OnClick="btnBack_Click" />
            </fieldset>
        </asp:Panel>
        <asp:Panel ID="pnlRegist" runat="server">
            <fieldset class="fieldset">
                <legend>[ビンゴカード作成]</legend>
                <table border="0" style="width: 400px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle2">
                            作成枚数
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtCount" runat="server" MaxLength="2"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnCreate" Text="作成" CssClass="seekbutton" OnClick="btnCreate_Click"
                    ValidationGroup="Create" CausesValidation="True" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="false" CssClass="seekbutton"
                    OnClick="btnCancel_Click" Text="キャンセル" />
                <asp:CustomValidator ID="vdcCount" runat="server" ErrorMessage="" OnServerValidate="vdcCount_ServerValidate"
                    ValidationGroup="Create"></asp:CustomValidator>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[ビンゴカード一覧]</legend>
                <table style="width:900px; height:443px">
                    <tr><td>
                        <asp:Button ID="btnHighProbability" runat="server" CausesValidation="false" CssClass="seekbutton"
                            OnClick="btnHighProbability_Click" Text="一括高確率設定" />
                        <asp:Button ID="btnNormalProbability" runat="server" CausesValidation="false" CssClass="seekbutton"
                            OnClick="btnNormalProbability_Click" Text="一括通常設定" />
                    </td></tr>
                    <tr><td>
                        <asp:Panel ID="pnlCard" runat="server" ScrollBars="auto" Height="443px">
                            <asp:Repeater ID="rptBingoCard" runat="server" DataSourceID="dsBingoCard">
                                <HeaderTemplate>
                                    <table><tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <td>
                                    <table>
                                        <tr><td>
                                            <table class="tableStyle" border="0" style="width: 100px">
                                                <tr><td class="tdHeaderStyle2" colspan="5"><%# Eval("BINGO_CARD_NO") %></td></tr>
                                                <tr><td class="tdDataStyle"><%# Eval("BALL_NO_1", "{0:00}") %></td><td class="tdDataStyle"><%# Eval("BALL_NO_2", "{0:00}") %></td><td class="tdDataStyle"><%# Eval("BALL_NO_3", "{0:00}") %></td><td class="tdDataStyle"><%# Eval("BALL_NO_4", "{0:00}") %></td><td class="tdDataStyle"><%# Eval("BALL_NO_5", "{0:00}") %></td></tr>
                                                <tr><td class="tdDataStyle"><%# Eval("BALL_NO_6", "{0:00}") %></td><td class="tdDataStyle"><%# Eval("BALL_NO_7", "{0:00}") %></td><td class="tdDataStyle"><%# Eval("BALL_NO_8", "{0:00}") %></td><td class="tdDataStyle"><%# Eval("BALL_NO_9", "{0:00}") %></td><td class="tdDataStyle"><%# Eval("BALL_NO_10", "{0:00}") %></td></tr>
                                                <tr><td class="tdDataStyle"><%# Eval("BALL_NO_11", "{0:00}") %></td><td class="tdDataStyle"><%# Eval("BALL_NO_12", "{0:00}") %></td><td class="tdDataStyle"><%# Eval("BALL_NO_13", "{0:00}") %></td><td class="tdDataStyle"><%# Eval("BALL_NO_14", "{0:00}") %></td><td class="tdDataStyle"><%# Eval("BALL_NO_15", "{0:00}") %></td></tr>
                                                <tr><td class="tdDataStyle"><%# Eval("BALL_NO_16", "{0:00}") %></td><td class="tdDataStyle"><%# Eval("BALL_NO_17", "{0:00}") %></td><td class="tdDataStyle"><%# Eval("BALL_NO_18", "{0:00}") %></td><td class="tdDataStyle"><%# Eval("BALL_NO_19", "{0:00}") %></td><td class="tdDataStyle"><%# Eval("BALL_NO_20", "{0:00}") %></td></tr>
                                                <tr><td class="tdDataStyle"><%# Eval("BALL_NO_21", "{0:00}") %></td><td class="tdDataStyle"><%# Eval("BALL_NO_22", "{0:00}") %></td><td class="tdDataStyle"><%# Eval("BALL_NO_23", "{0:00}") %></td><td class="tdDataStyle"><%# Eval("BALL_NO_24", "{0:00}") %></td><td class="tdDataStyle"><%# Eval("BALL_NO_25", "{0:00}") %></td></tr>
                                            </table>
                                        </td></tr>
                                        <tr><td align="center">
                                            <asp:LinkButton ID="btnUpdate" runat="server" Text='<%# GetHighProbabilityMark(Eval("HIGH_PROBABILITY_FLAG")) %>' CommandArgument='<%# Container.ItemIndex %>' CommandName="UPDATE" OnCommand="btnUpdate_Command" ForeColor='<%# GetHighProbabilityMarkColor(Eval("HIGH_PROBABILITY_FLAG")) %>'></asp:LinkButton>
                                            <asp:HiddenField ID="hdnHighProbabilityFlag" runat="server" Value='<%# Eval("HIGH_PROBABILITY_FLAG")%>' />
                                            <asp:HiddenField ID="hdnBingoCardNo" runat="server" Value='<%# Eval("BINGO_CARD_NO") %>' />
                                        </td></tr>
                                    </table>
                                    <%# ((Container.ItemIndex + 1) % HORIZONTAL_LIMIT_COUNT) == 0 ? "</td></tr><tr>" : "</td>" %>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tr></table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </asp:Panel>
                    </td></tr>
                </table>
            </fieldset>
            <br />
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsBingoCard" runat="server" SelectMethod="GetList" TypeName="BingoCard" OnSelecting="dsBingoCard_Selecting">
        <SelectParameters>
            <asp:Parameter Name="pSiteCd" Type="String" />
            <asp:Parameter Name="pBingoTermSeq" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtCount" />
</asp:Content>
