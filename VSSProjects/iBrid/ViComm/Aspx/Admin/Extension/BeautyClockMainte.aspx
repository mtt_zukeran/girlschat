﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
	CodeFile="BeautyClockMainte.aspx.cs" Inherits="Extension_BeautyClockMainte" Title="美人時計画像アップロード" %>
<%@ Import Namespace="System.Data" %>


<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="美人時計画像アップロード"></asp:Label>
</asp:Content>

<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent" style="width:700px;">
        <asp:Panel runat="server" ID="pnlMainte">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel runat="server" ID="pnlDtl" style="width: 665px;">
					<div class="tdHeaderStyle" style="font-size:large; text-align: center; width: 648px;height:30px;">
						<asp:Label ID="lblTimeHeader" runat="server" Text=""  style="line-height: 30px;"></asp:Label>
					</div>
                    <fieldset class="fieldset-inner">
                        <legend>
                            <%# DisplayWordUtil.Replace("[美人時計画像アップロード]")%>
                        </legend>
                        <asp:Label ID="lblTime" runat="server" ForeColor="red" Text=""></asp:Label>のアイドルを登録します
                        <table border="0" style="width: 640px" class="tableStyle">
                            <tr>
                                <td class="tdHeaderStyle">
                                    <asp:Label ID="lblHeader2" runat="server" Text="ユーザーID"></asp:Label>
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="lblUserId" runat="server" Text=""></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle2">
                                    <asp:Label ID="lblHeader3" runat="server" Text="画像"></asp:Label>
                                </td>
                                <td class="tdDataStyle">
                                    <asp:FileUpload ID="uldBeautyPic" runat="server" Width="400px" />
                                    <asp:RequiredFieldValidator ID="rfvUplod" runat="server" ErrorMessage="アップロードファイルが入力されていません。"
                                        SetFocusOnError="True" ValidationGroup="Upload" ControlToValidate="uldBeautyPic" >*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                       </table>
                        <asp:Button ID="btnUpload" runat="server" ValidationGroup="Upload" Text="実行" CssClass="seekbutton"
                            OnClick="btnUpload_Click" />
                        <asp:Button runat="server" ID="btnCancel" Text="戻る" CssClass="seekbutton" OnClick="btnCancel_Click"
                            CausesValidation="False" />
                        <asp:Label ID="lblNotFound" runat="server" Text="指定された出演者は存在しません。" ForeColor="Red" Font-Bold="true" Visible="false"></asp:Label>
                    </fieldset>
                </asp:Panel>
            </fieldset>
        </asp:Panel>    
        <asp:Panel ID="pnlDetail" runat="server"  Visible="false">
			<fieldset class="fieldset">
				<legend>[確認]</legend>
				<asp:Panel ID="pnlData" runat="server" style="width: 665px;">
					<div class="tdHeaderStyle" style="font-size:large; text-align: center; width: 648px;height:30px;">
						<asp:Label ID="lblTimeDetailHeader" runat="server" Text=""  style="line-height: 30px;"></asp:Label>
					</div>
					<fieldset class="fieldset-inner">
						<asp:Repeater id="rptBeauty" runat="server" EnableViewState="false">
							<ItemTemplate>
							<div id="names">
								<asp:Label runat="server" Text='<%# string.Format("「{0}」",GetTime()) %>' ForeColor="red"></asp:Label>のアイドルでよろしいですか？
							</div>
							<table border="2" bordercolor="#000000" cellspacing="0">
								<tr>
									<td style="text-align:left">
										<asp:Label runat="server" Text='<%# GetRegistTime() %>'></asp:Label><br />
										<asp:HyperLink runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "HANDLE_NM")%>' NavigateUrl='<%# string.Format("../Cast/CastView.aspx?loginid={0}",DataBinder.Eval(Container.DataItem, "LOGIN_ID")) %>'></asp:HyperLink>
									</td>
								</tr>
								<tr>
									<td style="text-align:center">
										<asp:Image runat="server" ImageUrl="<%# GetTempPicPath() %>"></asp:Image>
									</td>
								</tr>
							</table>
							</ItemTemplate>
						</asp:Repeater>
                        <asp:Button ID="btnMainte" runat="server" ValidationGroup="Mainte" Text="編集" CssClass="seekbutton" OnClick="btnMainte_Click" />
                        <asp:Button ID="btnPublish" runat="server" CausesValidation="False" Text="公開" CssClass="seekbutton" OnClick="btnPublish_Click" />
					</fieldset>
				</asp:Panel>
			</fieldset>        
        </asp:Panel>
    </div>
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="rfvUplod" HighlightCssClass="validatorCallout" />
</asp:Content>
