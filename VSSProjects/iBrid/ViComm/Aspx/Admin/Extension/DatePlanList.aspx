﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="DatePlanList.aspx.cs" Inherits="Extension_DatePlanList" Title="デートプラン一覧"
    ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="デートプラン一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 540px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle2">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton" ValidationGroup="Create"
                    OnClick="btnCreate_Click" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlDatePlanInfo">
            <fieldset class="fieldset">
                <legend>[デートプラン情報]</legend>
                <table border="0" style="width: 600px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            デートプランID(SEQ)
                        </td>
                        <td class="tdDataStyle">
                            <asp:Label ID="lblDatePlanId" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            デートプラン名
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtDatePlanNm" runat="server" MaxLength="64" Width="200px"></asp:TextBox>
                            <% // 必須チェック %>
                            <asp:RequiredFieldValidator ID="vdrDatePlanNm" runat="server" ErrorMessage="デートプラン名を入力してください。"
                                ControlToValidate="txtDatePlanNm" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                            <ajaxToolkit:ValidatorCalloutExtender ID="vdeDatePlanNm" runat="Server" TargetControlID="vdrDatePlanNm"
                                HighlightCssClass="validatorCallout" />
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            課金フラグ
                        </td>
                        <td class="tdDataStyle">
                            <asp:CheckBox ID="chkChargeFlag" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            価格
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtPrice" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            報酬
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtPayment" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            親密ポイント
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtFriendlyPoint" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            待機分
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtWaitingMin" runat="server" MaxLength="3" Width="80px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                           公開ステージ
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstStageSeq" runat="server" DataSourceID="dsStage" OnDataBound="lst_DataBound"
                                ValidationGroup="Update" DataTextField="STAGE_DISPLAY" DataValueField="STAGE_SEQ"
                                Width="170px">
                            </asp:DropDownList>
                            <% // 必須チェック %>
                            <asp:RequiredFieldValidator ID="vdrStageSeq" runat="server" ErrorMessage="公開ステージを選択してください。"
                                ControlToValidate="lstStageSeq" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                TargetControlID="vdrStageSeq" HighlightCssClass="validatorCallout" />
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="pnlCommand" runat="server">
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Update"
                        OnClick="btnUpdate_Click" />
                    <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" ValidationGroup="Delete"
                        OnClick="btnDelete_Click" />
                    <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" CausesValidation="False"
                        Visible="true" OnClick="btnCancel_Click" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[デートプラン一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdDatePlan.PageIndex + 1 %>
                        of
                        <%= grdDatePlan.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="600px">
                    <asp:GridView ID="grdDatePlan" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        PageSize="30" EnableSortingAndPagingCallbacks="false" DataSourceID="dsDatePlan"
                        SkinID="GridViewColor" AllowSorting="true" DataKeyNames="SITE_CD,DATE_PLAN_SEQ">
                        <Columns>
                            <asp:TemplateField HeaderText="デートプランSEQ">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%# Eval("DATE_PLAN_SEQ") %>'
                                        Text='<%# Eval("DATE_PLAN_SEQ") %>' OnCommand="lnkEdit_Command"></asp:LinkButton><br />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="デートプラン名">
                                <ItemTemplate>
                                    <asp:Label ID="lblDatePlanNm" runat="server" Text='<%# Eval("DATE_PLAN_NM") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="課金">
                                <ItemTemplate>
                                    <asp:Label ID="lblChargeFlag" runat="server" Text='<%# GetChargeFlagMark(Eval("CHARGE_FLAG")) %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="PRICE" DataFormatString="{0:N0}" HeaderText="価格">
                                <ItemStyle HorizontalAlign="right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="PAYMENT" DataFormatString="{0:N0}" HeaderText="報酬">
                                <ItemStyle HorizontalAlign="right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="FRIENDLY_POINT" DataFormatString="{0:N0}" HeaderText="親密ポイント">
                                <ItemStyle HorizontalAlign="right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="WAITING_MIN" DataFormatString="{0:N0}" HeaderText="待機分数">
                                <ItemStyle HorizontalAlign="right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="STAGE_NM" HeaderText="公開ステージ">
                                <ItemStyle HorizontalAlign="left" />
                            </asp:BoundField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
            <br />
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsDatePlan" runat="server" SelectMethod="GetPageCollection"
        ConvertNullToDBNull="true" SortParameterName="" TypeName="DatePlan" SelectCountMethod="GetPageCount"
        EnablePaging="True" OnSelected="dsDatePlan_Selected">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsItemGet" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="A2" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsStage" runat="server" SelectMethod="GetList" TypeName="Stage">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:Parameter Name="pSexCd" DefaultValue="1" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtPrice">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtPayment">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtFriendlyPoint">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="デートプラン情報を更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete"
        ConfirmText="デートプラン情報を削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>
