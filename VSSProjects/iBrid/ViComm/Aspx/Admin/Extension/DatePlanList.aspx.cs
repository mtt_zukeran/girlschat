﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: デート一覧

--	Progaram ID		: DatePlanList
--
--  Creation Date	: 2011.07.25
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.IO;
using System.Drawing;

public partial class Extension_DatePlanList : System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string DatePlanSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["DatePlanSeq"]);
		}
		set {
			this.ViewState["DatePlanSeq"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
		this.DatePlanSeq = string.Empty;
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.UpdateData(false);
		this.pnlDatePlanInfo.Visible = false;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		this.UpdateData(true);
		this.pnlDatePlanInfo.Visible = false;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.ClearFileds();
		this.pnlKey.Enabled = true;
		this.pnlDatePlanInfo.Visible = false;
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		this.DatePlanSeq = string.Empty;
		this.ClearFileds();
		this.pnlKey.Enabled = false;
		this.pnlDatePlanInfo.Visible = true;
	}

	protected void lnkEdit_Command(object sender,CommandEventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.DatePlanSeq = iBridUtil.GetStringValue(e.CommandArgument);
		this.GetData();
		this.pnlKey.Enabled = false;
		this.pnlDatePlanInfo.Visible = true;
	}

	protected void dsDatePlan_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	private void InitPage() {
		this.pnlInfo.Visible = true;
		this.pnlDatePlanInfo.Visible = false;

		this.DatePlanSeq = string.Empty;
		this.pnlKey.Enabled = true;
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		this.lstStageSeq.DataBind();
	}

	private void ClearFileds() {
		this.lblDatePlanId.Text = string.Empty;
		this.txtDatePlanNm.Text = string.Empty;
		this.txtPrice.Text = string.Empty;
		this.txtPayment.Text = string.Empty;
		this.txtFriendlyPoint.Text = string.Empty;
		this.txtWaitingMin.Text = string.Empty;
		this.lstStageSeq.DataBind();
		this.pnlKey.Enabled = true;
	}

	protected string GetRecCount() {
		return this.recCount;
	}


	protected string GetChargeFlagMark(object pChargeFlag) {
		return ViCommConst.FLAG_ON_STR.Equals(iBridUtil.GetStringValue(pChargeFlag)) ? "課金" : "無課金";
	}

	private void GetList() {
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.DatePlanSeq = string.Empty;

		this.grdDatePlan.PageIndex = 0;
		this.grdDatePlan.DataSourceID = "dsDatePlan";
		this.grdDatePlan.DataBind();
		this.pnlCount.DataBind();
	}

	private void GetData() {
		this.ClearFileds();

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("DATE_PLAN_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pDATE_PLAN_SEQ",DbSession.DbType.VARCHAR2,this.DatePlanSeq);
			oDbSession.ProcedureOutParm("pDATE_PLAN_NM",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pCHARGE_FLAG",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pPRICE",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pPAYMENT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pFRIENDLY_POINT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pWAITING_MIN",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTAGE_SEQ",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.lblDatePlanId.Text = this.DatePlanSeq;
			this.txtDatePlanNm.Text = oDbSession.GetStringValue("pDATE_PLAN_NM");
			this.txtPrice.Text = oDbSession.GetStringValue("pPRICE");
			this.txtPayment.Text = oDbSession.GetStringValue("pPAYMENT");
			this.txtFriendlyPoint.Text = oDbSession.GetStringValue("pFRIENDLY_POINT");
			this.txtWaitingMin.Text = oDbSession.GetStringValue("pWAITING_MIN");
			this.lstStageSeq.SelectedValue = oDbSession.GetStringValue("pSTAGE_SEQ");
			this.chkChargeFlag.Checked = ViCommConst.FLAG_ON_STR.Equals(oDbSession.GetStringValue("pCHARGE_FLAG"));
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
		}
	}

	private void UpdateData(bool pDeleteFlag) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("DATE_PLAN_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureBothParm("pDATE_PLAN_SEQ",DbSession.DbType.VARCHAR2,this.DatePlanSeq);
			oDbSession.ProcedureInParm("pDATE_PLAN_NM",DbSession.DbType.VARCHAR2,this.txtDatePlanNm.Text);
			oDbSession.ProcedureInParm("pCHARGE_FLAG",DbSession.DbType.NUMBER,this.chkChargeFlag.Checked ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR);
			oDbSession.ProcedureInParm("pPRICE",DbSession.DbType.NUMBER,this.txtPrice.Text);
			oDbSession.ProcedureInParm("pPAYMENT",DbSession.DbType.NUMBER,this.txtPayment.Text);
			oDbSession.ProcedureInParm("pFRIENDLY_POINT",DbSession.DbType.NUMBER,this.txtFriendlyPoint.Text);
			oDbSession.ProcedureInParm("pWAITING_MIN",DbSession.DbType.NUMBER,this.txtWaitingMin.Text);
			oDbSession.ProcedureInParm("pSTAGE_SEQ",DbSession.DbType.NUMBER,this.lstStageSeq.SelectedValue);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDeleteFlag ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.ClearFileds();
		this.GetList();
	}
}
