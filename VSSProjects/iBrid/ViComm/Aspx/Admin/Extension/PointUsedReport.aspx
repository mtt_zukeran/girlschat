﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="PointUsedReport.aspx.cs" Inherits="Extension_PointUsedReport" Title="ポイント明細レポート"
    ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="ポイント明細レポート"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset>
                <legend>[設定]</legend>
                <table border="0" style="width: 640px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            サイトコード
                        </td>
                        <td class="tdDataStyle" colspan="3">
                            <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="180px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            集計期間
                        </td>
                        <td class="tdDataStyle" colspan="3">
                            <asp:DropDownList ID="lstFromYYYY" runat="server" Width="54px">
                            </asp:DropDownList>年
                            <asp:DropDownList ID="lstFromMM" runat="server" Width="43px">
                            </asp:DropDownList>月
                            <asp:DropDownList ID="lstFromDD" runat="server" Width="43px">
                            </asp:DropDownList>日～							<asp:Label ID="lblErrorMessageStartDate" runat="server" ForeColor="red" Visible="false"></asp:Label>
                            <asp:DropDownList ID="lstToYYYY" runat="server" Width="54px">
                            </asp:DropDownList>年
                            <asp:DropDownList ID="lstToMM" runat="server" Width="43px">
                            </asp:DropDownList>月
                            <asp:DropDownList ID="lstToDD" runat="server" Width="43px">
                            </asp:DropDownList>日
                            <asp:Label ID="lblErrorMessageEndDate" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            課金種別
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstGameCharge" runat="server" AutoPostBack="True" OnSelectedIndexChanged="lstGameCharge_IndexChanged"
                                OnDataBound="lstGameCharge_DataBound" Width="180px">
                                <asp:ListItem Text="アイテム" Value="1"></asp:ListItem>
                                <asp:ListItem Text="デート" Value="2"></asp:ListItem>
                                <asp:ListItem Text="抽選" Value="3"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="tdHeaderStyle2">
                            アイテムカテゴリ
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstGameItemCategory" runat="server" DataSourceID="dsGameItemCategory"
                                DataTextField="GAME_ITEM_CATEGORY_NM" DataValueField="GAME_ITEM_CATEGORY_TYPE"
                                OnDataBound="lst_DataBound" Width="170px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    CausesValidation="False" />
                <asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" CausesValidation="False" OnClick="btnCSV_Click" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[ポイント明細レポート]</legend>
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="552px">
                    <asp:GridView ID="grdPointUsedReport" runat="server" AutoGenerateColumns="False"
                        DataSourceID="dsPointUsedReport" AllowSorting="true" SkinID="GridViewColor" ShowFooter="True"
                        OnRowDataBound="grdPointUsedReport_RowDataBound" OnDataBound="grdPointUsedReport_DataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="利用日時">
                                <ItemTemplate>
                                    <asp:Label ID="lblPointUsedDate" runat="server" Text='<%# Eval("POINT_USED_DATE","{0:yyyy/MM/dd HH:mm:ss}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                                <FooterTemplate>
                                    <asp:Label ID="lblReportDaySum" runat="server" Text="合計"></asp:Label>
                                </FooterTemplate>
                                <FooterStyle HorizontalAlign="Center" Font-Bold="true" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ﾕｰｻﾞｰID">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# GetViewUrl(Eval("SITE_CD"), Eval("LOGIN_ID")) %>'
                                        Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ﾊﾝﾄﾞﾙ名">
                                <ItemTemplate>
                                    <asp:Label ID="lblHandleNm" runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ｹﾞｰﾑﾊﾝﾄﾞﾙ名">
                                <ItemTemplate>
                                    <asp:Label ID="lblGameHandleNm" runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("GAME_HANDLE_NM")) %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ｱｲﾃﾑｶﾃｺﾞﾘ">
                                <ItemTemplate>
                                    <asp:Label ID="lblGameItemCategoryNm" runat="server" Text='<%# Eval("GAME_ITEM_CATEGORY_NM") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ｱｲﾃﾑ名">
                                <ItemTemplate>
                                    <asp:Label ID="lblGameItemNm" runat="server" Text='<%# Eval("GAME_ITEM_NM") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="購入個数">
                                <ItemTemplate>
                                    <asp:Label ID="lblBuyQuantity" runat="server" Text='<%# Eval("BUY_QUANTITY", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                                <FooterTemplate>
                                    <asp:Label ID="lblBuyQuantitySum" runat="server"></asp:Label>
                                </FooterTemplate>
                                <FooterStyle Font-Bold="true" HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ﾃﾞｰﾄ名">
                                <ItemTemplate>
                                    <asp:Label ID="lblDatePlanNm" runat="server" Text='<%# Eval("DATE_PLAN_NM") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="抽選名">
                                <ItemTemplate>
                                    <asp:Label ID="lblLotteryNm" runat="server" Text='<%# string.IsNullOrEmpty(Eval("LOTTERY_NM","{0}")) ? "(未設定)" : Eval("LOTTERY_NM") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="利用ﾎﾟｲﾝﾄ">
                                <ItemTemplate>
                                    <asp:Label ID="lblUsedPoint" runat="server" Text='<%# Eval("USED_POINT", "{0:N0}Pt") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                                <FooterTemplate>
                                    <asp:Label ID="lblUsedPointSum" runat="server"></asp:Label>
                                </FooterTemplate>
                                <FooterStyle HorizontalAlign="Right" Font-Bold="true" />
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle ForeColor="Black" BackColor="LightYellow" Font-Bold="true" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItemCategory" runat="server" SelectMethod="GetList"
        TypeName="GameItemCategory">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSeekSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsPointUsedReport" runat="server" SelectMethod="GetListReport"
        OnSelecting="dsPointUsedReport_Selecting" TypeName="PointUsedLog">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSeekSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:ControlParameter ControlID="lstGameCharge" Name="pPointUsedType" PropertyName="SelectedValue"
                Type="String" />
            <asp:ControlParameter ControlID="lstGameItemCategory" Name="pGameItemCategoryType"
                PropertyName="SelectedValue" Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
            <asp:Parameter Name="pReportDayFrom" Type="String" />
            <asp:Parameter Name="pReportDayTo" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
