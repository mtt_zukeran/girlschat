<%@ Import Namespace="ViComm" %>

<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="QuickResponseDetail.aspx.cs" Inherits="Extension_QuickResponseDetail"
	Title="10分以内の返信・ﾒｰﾙ受信明細" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="10分以内の返信・ﾒｰﾙ受信明細"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlKey">
			<fieldset class="fieldset">
				<legend>[検索]</legend>
				<asp:Panel ID="pnlSeekCondition" runat="server">
					<table border="0" style="width: 600px" class="tableStyle">
						<tr>
							<td class="tdHeaderSmallStyle2">
								サイト
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px">
								</asp:DropDownList>
							</td>
							<td class="tdHeaderSmallStyle2">
								稼動日時
							</td>
							<td class="tdDataStyle" colspan="3">
								<asp:DropDownList ID="lstYYYY" runat="server" Width="60px">
								</asp:DropDownList>年
								<asp:DropDownList ID="lstMM" runat="server" Width="40px">
								</asp:DropDownList>月
								<asp:DropDownList ID="lstDD" runat="server" Width="40px">
								</asp:DropDownList>日
								<asp:DropDownList ID="lstHH" runat="server" Width="40px">
								</asp:DropDownList>時
							</td>
						</tr>
					</table>
					<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" />
					<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[ﾒｰﾙ受信一覧]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count
						<%#GetRecCount() %>
					</a>
					<br />
					<a class="reccount">Current viewing page
						<%=grdQuickResMail.PageIndex + 1%>
						of
						<%=grdQuickResMail.PageCount%>
					</a>
					<br />
				</asp:Panel>
				<asp:Panel ID="pnlGrid" runat="server">
					<asp:GridView ID="grdQuickResMail" runat="server" AllowPaging="true" AutoGenerateColumns="False" DataSourceID="dsQuickResMail" SkinID="GridViewColor" PageSize="30">
						<Columns>
							<asp:TemplateField HeaderText="受信日時">
								<ItemTemplate>
									<asp:Label runat="server" ID="lblCreateDate" Text='<%# Eval("CREATE_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Wrap="False" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾛｸﾞｲﾝID">
								<HeaderStyle Wrap="False" />
								<ItemTemplate>
									<asp:HyperLink ID="lnkTxLoginId" runat="server" NavigateUrl='<%# GetUserLink(Eval("TX_SITE_CD"),Eval("TX_LOGIN_ID"),"1") %>' Text='<%# Eval("TX_LOGIN_ID") %>'></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Left" Wrap="False" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾊﾝﾄﾞﾙﾈｰﾑ">
								<HeaderStyle Wrap="False" />
								<ItemTemplate>
									<asp:Label ID="lblTxHandleNm" runat="server" Text='<%# ViCommPrograms.DefHandleName(ViCommPrograms.DefHandleName(Eval("TX_HANDLE_NM"))) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Left" Wrap="False" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾛｸﾞｲﾝID">
								<HeaderStyle Wrap="False" />
								<ItemTemplate>
									<asp:HyperLink ID="lnkRxLoginId" runat="server" NavigateUrl='<%# GetUserLink(Eval("TX_SITE_CD"),Eval("RX_LOGIN_ID"),"3") %>' Text='<%# Eval("RX_LOGIN_ID") %>'></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Left" Wrap="False" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾊﾝﾄﾞﾙﾈｰﾑ">
								<HeaderStyle Wrap="False" />
								<ItemTemplate>
									<asp:Label ID="lblRxHandleNm" runat="server" Text='<%# ViCommPrograms.DefHandleName(ViCommPrograms.DefHandleName(Eval("RX_HANDLE_NM"))) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Left" Wrap="False" />
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
			<br />
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsQuickResMail" runat="server" SelectMethod="GetPageCollection" TypeName="QuickResMail" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsQuickResMail_Selecting" OnSelected="dsQuickResMail_Selected">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pReportDay" Type="String" />
			<asp:Parameter Name="pReportHour" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
