﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="GameLoginBonusMonthlyList.aspx.cs" Inherits="Extension_GameLoginBonusMonthlyList"
    Title="月別ログインボーナスアイテム一覧" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="月別ログインボーナスアイテム一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 800px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle2">
                                サイトコード
                            </td>
                            <td class="tdDataStyle" style="width: 200px">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px">
                                </asp:DropDownList>
                            </td>
                            <td class="tdHeaderStyle2">
                                対象月
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtSeekBonusMonth" runat="server" Width="50px"></asp:TextBox>
                                <ajaxToolkit:MaskedEditExtender ID="mskSeekBonusMonth" runat="server" MaskType="None"
                                    Mask="9999/99" UserDateFormat="YearMonthDay" UserTimeFormat="TwentyFourHour"
                                    ClearMaskOnLostFocus="false" TargetControlID="txtSeekBonusMonth">
                                </ajaxToolkit:MaskedEditExtender>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton" ValidationGroup="Create"
                    OnClick="btnCreate_Click" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" Visible="false" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlItemInfo">
            <fieldset class="fieldset">
                <legend>[ボーナスアイテム情報]</legend>
                <table border="0" style="width: 650px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            対象月
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtBonusMonth" runat="server" Width="50px"></asp:TextBox>
                            <ajaxToolkit:MaskedEditExtender ID="mskBonusMonth" runat="server" MaskType="None"
                                Mask="9999/99" UserDateFormat="YearMonthDay" UserTimeFormat="TwentyFourHour"
                                ClearMaskOnLostFocus="false" TargetControlID="txtBonusMonth">
                            </ajaxToolkit:MaskedEditExtender>
                            <% // 必須チェック %>
                            <asp:RequiredFieldValidator ID="vdrBonusMonth" runat="server" ErrorMessage="対象月を入力してください。"
                                ControlToValidate="txtBonusMonth" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                            <ajaxToolkit:ValidatorCalloutExtender ID="vceBonusMonth" runat="Server" TargetControlID="vdrBonusMonth"
                                HighlightCssClass="validatorCallout" />
                            <% // 範囲チェック %>
                            <asp:CustomValidator ID="vdcBonusMonth" runat="server" ErrorMessage="" ControlToValidate="txtBonusMonth"
                                ValidationGroup="Update" OnServerValidate="vdcBonusMonth_ServerValidate"></asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            ボーナスアイテム
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstGameItemCategory" runat="server" DataSourceID="dsGameItemCategory"
                                OnDataBound="lst_DataBound" OnSelectedIndexChanged="lstGameItemCategory_IndexChanged"
                                DataTextField="GAME_ITEM_CATEGORY_NM" DataValueField="GAME_ITEM_CATEGORY_TYPE"
                                Width="170px" AutoPostBack="True">
                            </asp:DropDownList>
                            <asp:DropDownList ID="lstItemGetCd" runat="server" DataSourceID="dsItemGet" DataTextField="CODE_NM"
								DataValueField="CODE" OnDataBound="lstItemGetCd_DataBound" OnSelectedIndexChanged="lstItemGetCd_IndexChanged" AutoPostBack="True">
							</asp:DropDownList>
							<asp:DropDownList ID="lstItemPresent" runat="server" OnSelectedIndexChanged="lstItemPresent_IndexChanged" AutoPostBack="True">
								<asp:ListItem Value=""></asp:ListItem>
								<asp:ListItem Value="0">通常</asp:ListItem>
								<asp:ListItem Value="1">ﾌﾟﾚｾﾞﾝﾄ</asp:ListItem>
							</asp:DropDownList>
                            <asp:DropDownList ID="lstGameItem" runat="server" DataSourceID="dsGameItem" DataTextField="GAME_ITEM_NM"
                                OnDataBound="lst_DataBound" DataValueField="GAME_ITEM_SEQ" Width="170px">
                            </asp:DropDownList>
                            <br />
                            <asp:Label ID="lblErrorMessageItem" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            個数
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtItemCount" runat="server" MaxLength="3" Width="50px"></asp:TextBox>
                            <br />
                            <asp:Label ID="lblErrorMessageCount" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            必要ログイン数
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtNeedLoginCount" runat="server" MaxLength="4" Width="50px"></asp:TextBox>
                            <br />
                            <asp:Label ID="lblErrorMessageLoginCount" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="pnlCommand" runat="server">
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Update"
                        OnClick="btnUpdate_Click" />
                    <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" ValidationGroup="Delete"
                        OnClick="btnDelete_Click" />
                    <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" CausesValidation="False"
                        Visible="true" OnClick="btnCancel_Click" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[セールアイテム一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdGameLoginBonusMonthly.PageIndex + 1 %>
                        of
                        <%= grdGameLoginBonusMonthly.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="600px">
                    <asp:GridView ID="grdGameLoginBonusMonthly" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        PageSize="30" EnableSortingAndPagingCallbacks="false" DataSourceID="dsGameLoginBonusMonthly"
                        SkinID="GridViewColor" AllowSorting="true">
                        <Columns>
                            <asp:TemplateField HeaderText="対象月">
                                <ItemTemplate>
                                    <asp:Label ID="lblGameLoginBonusMonth" runat="server" Text='<%# Eval("GAME_LOGIN_BONUS_MONTH") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="アイテムSEQ">
                                <ItemTemplate>
                                    <asp:Label ID="lblGameItemSeq" runat="server" Text='<%# Eval("GAME_ITEM_SEQ") %>'></asp:Label>
                                    <asp:HiddenField ID="hdnGameItemSeq" runat="server" Value='<%# Eval("GAME_ITEM_SEQ") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="アイテム名">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%# Container.DataItemIndex %>'
                                        OnCommand="lnkEdit_Command" Text='<%# Eval("GAME_ITEM_NM") %>'></asp:LinkButton>
                                    <asp:HiddenField ID="hdnGameLoginBonusSeq" runat="server" Value='<%# Eval("GAME_LOGIN_BONUS_SEQ") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="ITEM_COUNT" HeaderText="個数" DataFormatString="{0:N0}">
                                <ItemStyle HorizontalAlign="right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NEED_LOGIN_COUNT" DataFormatString="{0:N0}" HeaderText="必要ログイン数">
                                <ItemStyle HorizontalAlign="right" />
                            </asp:BoundField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
            <br />
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsGameLoginBonusMonthly" runat="server" ConvertNullToDBNull="false"
        EnablePaging="True" OnSelected="dsGameLoginBonusMonthly_Selected" OnSelecting="dsGameLoginBonusMonthly_Selecting"
        SelectCountMethod="GetPageCount" SelectMethod="GetPageCollection" SortParameterName=""
        TypeName="GameLoginBonusMonthly">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:Parameter Name="pGameLoginBonusMonth" DefaultValue="" Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItemCategory" runat="server" SelectMethod="GetList"
        TypeName="GameItemCategory">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItem" runat="server" SelectMethod="GetListByCategoryType"
        TypeName="GameItem" OnSelecting="dsGameItem_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
            <asp:Parameter Name="pGameItemCategoryType" Type="String" DefaultValue="" />
            <asp:Parameter Name="pGameItemGetCd" Type="String" DefaultValue="" />
            <asp:Parameter Name="pPresentFlag" Type="String" DefaultValue="" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsItemGet" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="A2" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtItemCount">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtNeedLoginCount">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="ボーナスアイテム情報を更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete"
        ConfirmText="ボーナスアイテム情報を削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>
