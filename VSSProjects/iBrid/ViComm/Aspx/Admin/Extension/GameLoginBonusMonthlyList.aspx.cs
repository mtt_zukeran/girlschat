﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 月別ｹﾞｰﾑﾛｸﾞｲﾝﾎﾞｰﾅｽ一覧

--	Progaram ID		: GameLoginBonusMonthlyList
--
--  Creation Date	: 2011.07.25
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Ratelain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.IO;

public partial class Extension_GameLoginBonusMonthlyList : System.Web.UI.Page {
	protected string[] DummyArray = new string[3];
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		set {
			this.ViewState["SexCd"] = value;
		}
	}

	private string GameLoginBonusSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameLoginBonusSeq"]);
		}
		set {
			this.ViewState["GameLoginBonusSeq"] = value;
		}
	}

	private string GameLoginBonusMonth {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameLoginBonusMonth"]);
		}
		set {
			this.ViewState["GameLoginBonusMonth"] = value;
		}
	}

	private string GameItemSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemSeq"]);
		}
		set {
			this.ViewState["GameItemSeq"] = value;
		}
	}

	private string GameItemCategoryType {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemCategoryType"]);
		}
		set {
			this.ViewState["GameItemCategoryType"] = value;
		}
	}

	private string CreateFlag {
		get {
			return iBridUtil.GetStringValue(this.ViewState["CreateFlag"]);
		}
		set {
			this.ViewState["CreateFlag"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	private string GameItemGetCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemGetCd"]);
		}
		set {
			this.ViewState["GameItemGetCd"] = value;
		}
	}

	private string GameItemPresent {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemPresent"]);
		}
		set {
			this.ViewState["GameItemPresent"] = value;
		}
	}
	
	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();

			this.SiteCd = iBridUtil.GetStringValue(this.Request.QueryString["sitecd"]);
			this.SexCd = iBridUtil.GetStringValue(this.Request.QueryString["sexcd"]);
			this.CreateFlag = iBridUtil.GetStringValue(this.Request.QueryString["create"]);

			if (!string.IsNullOrEmpty(this.SiteCd) && !string.IsNullOrEmpty(this.GameLoginBonusSeq)) {
				this.lstSiteCd.SelectedValue = this.SiteCd;

				if (this.CreateFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					this.GameItemSeq = string.Empty;
					this.pnlKey.Enabled = false;
					this.pnlItemInfo.Visible = true;
				} else {
					this.GetList();
				}
			}
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
		this.GameItemSeq = string.Empty;
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		if (!this.IsCorrectInput()) {
			return;
		}
		this.UpdateData(false);
		this.pnlItemInfo.Visible = false;
		this.pnlKey.Enabled = true;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		this.UpdateData(true);
		this.pnlItemInfo.Visible = false;
		this.pnlKey.Enabled = true;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlItemInfo.Visible = false;
		this.pnlKey.Enabled = true;
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.GameLoginBonusMonth = string.Empty;
		this.GameItemSeq = string.Empty;
		this.CreateFlag = ViCommConst.FLAG_ON_STR;
		this.pnlKey.Enabled = false;
		this.pnlItemInfo.Visible = true;
		this.ClearFileds();
	}

	protected void lnkEdit_Command(object sender,CommandEventArgs e) {
		
		this.GameItemGetCd = null;
		this.GameItemPresent = null;
		
		if (!this.IsValid) {
			return;
		}
		this.SiteCd = this.lstSiteCd.SelectedValue;
		int iIndex = int.Parse(iBridUtil.GetStringValue(e.CommandArgument));

		GridViewRow oRow = this.grdGameLoginBonusMonthly.Rows[iIndex];
		HiddenField oGameLoginBonusSeqHiddenField = oRow.FindControl("hdnGameLoginBonusSeq") as HiddenField;
		HiddenField oGameItemSeqHiddenFiled = oRow.FindControl("hdnGameItemSeq") as HiddenField;
		Label oGameLoginBonusMonthLabel = oRow.FindControl("lblGameLoginBonusMonth") as Label;

		this.GameLoginBonusSeq = oGameLoginBonusSeqHiddenField.Value;
		this.GameItemSeq = oGameItemSeqHiddenFiled.Value;
		this.GameLoginBonusMonth = oGameLoginBonusMonthLabel.Text;
		this.CreateFlag = string.Empty;
		this.pnlKey.Enabled = false;
		this.GetData();

		this.pnlItemInfo.Visible = true;
	}

	protected void dsGameLoginBonusMonthly_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsGameLoginBonusMonthly_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pGameLoginBonusMonth"] = this.txtSeekBonusMonth.Text.Equals("____/__") ? string.Empty : this.txtSeekBonusMonth.Text;
	}

	protected void dsGameItem_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pGameItemCategoryType"] = this.GameItemCategoryType;
		e.InputParameters["pGameItemGetCd"] = this.GameItemGetCd;
		e.InputParameters["pPresentFlag"] = this.GameItemPresent;
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	protected void lstGameItemCategory_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		this.GameItemCategoryType = oDropDownList.SelectedValue;
		this.lstGameItem.DataBind();
	}

	protected void lstItemGetCd_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		this.GameItemGetCd = oDropDownList.SelectedValue;
		this.lstGameItem.DataBind();
	}

	protected void lstItemPresent_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		this.GameItemPresent = oDropDownList.SelectedValue;
		this.lstGameItem.DataBind();
	}
	
	protected void vdcBonusMonth_ServerValidate(object source,ServerValidateEventArgs args) {
		DateTime oDate;
		if (DateTime.TryParse(string.Concat(this.txtBonusMonth.Text,"/01"),out oDate)) {
			this.GameLoginBonusMonth = oDate.ToString("yyyy/MM");
		} else {
			args.IsValid = false;
			this.vdcBonusMonth.ErrorMessage = "入力書式が正しくありません。";
		}
	}

	private void InitPage() {
		this.pnlInfo.Visible = true;
		this.pnlItemInfo.Visible = false;

		this.GameLoginBonusSeq = string.Empty;
		this.GameItemSeq = string.Empty;
		this.pnlKey.Enabled = true;
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void ClearFileds() {
		this.txtSeekBonusMonth.Text = string.Empty;
		this.txtBonusMonth.Text = string.Empty;
		this.txtItemCount.Text = string.Empty;
		this.txtNeedLoginCount.Text = string.Empty;
		this.lstGameItemCategory.DataBind();
		this.lstGameItem.DataBind();
		this.lblErrorMessageItem.Visible = false;
		this.lblErrorMessageCount.Visible = false;
		this.lstItemGetCd.SelectedValue = null;
		this.lstItemPresent.SelectedValue = null;
		this.GameItemGetCd = null;
		this.GameItemPresent = null;
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.GameItemSeq = string.Empty;

		this.grdGameLoginBonusMonthly.PageIndex = 0;
		this.grdGameLoginBonusMonthly.DataSourceID = "dsGameLoginBonusMonthly";
		this.grdGameLoginBonusMonthly.DataBind();
		this.pnlCount.DataBind();
	}

	private void GetData() {
		this.ClearFileds();

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("GAME_LOGIN_BONUS_MONTHLY_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pGAME_LOGIN_BONUS_MONTH",DbSession.DbType.VARCHAR2,this.GameLoginBonusMonth);
			oDbSession.ProcedureInParm("pGAME_LOGIN_BONUS_SEQ",DbSession.DbType.VARCHAR2,this.GameLoginBonusSeq);
			oDbSession.ProcedureOutParm("pSEX_CD",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pGAME_ITEM_SEQ",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pGAME_ITEM_CATEGORY_TYPE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pITEM_COUNT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pNEED_LOGIN_COUNT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
			this.txtItemCount.Text = oDbSession.GetStringValue("pITEM_COUNT");
			this.txtNeedLoginCount.Text = oDbSession.GetStringValue("pNEED_LOGIN_COUNT");
			this.lstGameItemCategory.SelectedValue = oDbSession.GetStringValue("pGAME_ITEM_CATEGORY_TYPE");
			this.GameItemCategoryType = this.lstGameItemCategory.SelectedValue;
			this.lstGameItem.DataBind();
			this.lstGameItem.SelectedValue = oDbSession.GetStringValue("pGAME_ITEM_SEQ");
		}
	}

	private void UpdateData(bool pDeleteFlag) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("GAME_LOGIN_BONUS_MONTH_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pGAME_LOGIN_BONUS_MONTH",DbSession.DbType.VARCHAR2,this.GameLoginBonusMonth);
			oDbSession.ProcedureBothParm("pGAME_LOGIN_BONUS_SEQ",DbSession.DbType.VARCHAR2,this.GameLoginBonusSeq);
			oDbSession.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,this.SexCd);
			oDbSession.ProcedureInParm("pGAME_ITEM_SEQ",DbSession.DbType.VARCHAR2,this.lstGameItem.SelectedValue);
			oDbSession.ProcedureInParm("pITEM_COUNT",DbSession.DbType.NUMBER,this.txtItemCount.Text);
			oDbSession.ProcedureInParm("pNEED_LOGIN_COUNT",DbSession.DbType.NUMBER,this.txtNeedLoginCount.Text);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDeleteFlag ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.ClearFileds();
		this.GetList();
	}

	private bool IsCorrectInput() {
		this.lblErrorMessageItem.Visible = false;
		this.lblErrorMessageCount.Visible = false;

		bool bResult = true;

		if (string.IsNullOrEmpty(this.txtItemCount.Text)) {
			this.lblErrorMessageCount.Text = "個数を入力してください。";
			this.lblErrorMessageCount.Visible = true;
			bResult = false;
		}

		if (string.IsNullOrEmpty(this.txtNeedLoginCount.Text)) {
			this.lblErrorMessageLoginCount.Text = "必要ログイン数を入力してください。";
			this.lblErrorMessageLoginCount.Visible = true;
			bResult = false;
		}

		if (string.IsNullOrEmpty(this.lstGameItem.SelectedValue)) {
			this.lblErrorMessageItem.Text = "ボーナスアイテムを選択してください。";
			this.lblErrorMessageItem.Visible = true;
			bResult = false;
		}

		using (GameLoginBonusMonthly oGameLoginBonus = new GameLoginBonusMonthly()) {
			if (ViCommConst.FLAG_ON_STR.Equals(this.CreateFlag) && oGameLoginBonus.IsDupulicateItem(this.SiteCd,this.GameLoginBonusMonth,this.lstGameItem.SelectedValue)) {
				this.lblErrorMessageItem.Text = "ボーナスアイテムが重複しています。";
				this.lblErrorMessageItem.Visible = true;
				bResult = false;
			}
		}

		return bResult;
	}

	protected void lstItemGetCd_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		oDropDownList.Items.Insert(3,new ListItem("戦利品","3"));
	}
}
