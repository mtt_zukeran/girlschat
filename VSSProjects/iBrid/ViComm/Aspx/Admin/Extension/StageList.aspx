﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="StageList.aspx.cs" Inherits="Extension_StageList" Title="ステージ一覧" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="ステージ一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 600px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle2">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton" ValidationGroup="Create"
                    OnClick="btnCreate_Click" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" Visible="false" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlStageInfo">
            <fieldset class="fieldset">
                <legend>[ステージ情報]</legend>
                <fieldset class="fieldset-inner">
                    <table border="0" class="tableStyle" style="width: 600px">
                        <tr>
                            <td class="tdHeaderStyle">
                                ステージID(SEQ)
                            </td>
                            <td class="tdDataStyle">
                                <asp:Label ID="lblStageId" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                ステージ名
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtStageNm" runat="server" MaxLength="64" Width="200px"></asp:TextBox>
                                <% // 必須チェック %>
                                <asp:RequiredFieldValidator ID="vdrStageNm" runat="server" ErrorMessage="ステージ名を入力してください。"
                                    ControlToValidate="txtStageNm" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="vdeStageNm" runat="Server" TargetControlID="vdrStageNm"
                                    HighlightCssClass="validatorCallout" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                ステージレベル
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstLevel" runat="server" DataSourceID="dsStageLevel" DataTextField="STAGE_DISPLAY"
                                    DataValueField="STAGE_LEVEL" Width="150px" OnDataBound="lstLevel_DataBound" />に挿入
                                <% // 必須チェック %>
                                <asp:RequiredFieldValidator ID="vdrStageLevel" runat="server" ErrorMessage="ステージレベルを選択してください。"
                                    ControlToValidate="lstLevel" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="vdeStageLevel" runat="Server" TargetControlID="vdrStageLevel"
                                    HighlightCssClass="validatorCallout" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                ステージグループ
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstStageGroup" runat="server" DataSourceID="dsStageGroup" DataTextField="STAGE_GROUP_NM"
                                    DataValueField="STAGE_GROUP_TYPE" Width="150px" OnDataBound="lst_DataBound" />
                                <% // 必須チェック %>
                                <asp:RequiredFieldValidator ID="vdrStageGroup" runat="server" ErrorMessage="ステージグループを選択してください。"
                                    ControlToValidate="lstStageGroup" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="vdeStageGroup" runat="Server" TargetControlID="vdrStageGroup"
                                    HighlightCssClass="validatorCallout" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                ボス名
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtBossNm" runat="server" MaxLength="30" Width="200px"></asp:TextBox>
                                <% // 必須チェック %>
                                <asp:RequiredFieldValidator ID="vdrBossNm" runat="server" ErrorMessage="ボス名を入力して下さい。"
                                    ControlToValidate="txtBossNm" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server"
                                    TargetControlID="vdrBossNm" HighlightCssClass="validatorCallout" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                ボス説明文
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtBossDescription" runat="server" MaxLength="120" Width="300px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                経験値
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtExp" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
                            </td>
                            <% // 必須チェック %>
                            <asp:RequiredFieldValidator ID="vdrExp" runat="server" ErrorMessage="経験値を入力して下さい。"
                                ControlToValidate="txtExp" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                TargetControlID="vdrExp" HighlightCssClass="validatorCallout" />
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                BOSS HP
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtBossHp" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
                                <% // 必須チェック %>
                                <asp:RequiredFieldValidator ID="vdrBossHp" runat="server" ErrorMessage="BOSS HPを入力して下さい。"
                                    ControlToValidate="txtBossHp" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server"
                                    TargetControlID="vdrBossHp" HighlightCssClass="validatorCallout" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                BOSS HP(仲間1人)
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtBossHp1" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
                                <% // 必須チェック %>
                                <asp:RequiredFieldValidator ID="vdrBossHp1" runat="server" ErrorMessage="BOSS HP(仲間1人)を入力して下さい。"
                                    ControlToValidate="txtBossHp1" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                    TargetControlID="vdrBossHp1" HighlightCssClass="validatorCallout" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                BOSS HP(仲間2人)
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtBossHp2" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
                                <% // 必須チェック %>
                                <asp:RequiredFieldValidator ID="vdrBossHp2" runat="server" ControlToValidate="txtBossHp2"
                                    ErrorMessage="BOSS HP(仲間2人)を入力して下さい。" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                    HighlightCssClass="validatorCallout" TargetControlID="vdrBossHp2">
                                </ajaxToolkit:ValidatorCalloutExtender>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                BOSS HP(仲間3人)
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtBossHp3" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
                                <% // 必須チェック %>
                                <asp:RequiredFieldValidator ID="vdrBossHp3" runat="server" ControlToValidate="txtBossHp3"
                                    ErrorMessage="BOSS HP(仲間3人)を入力して下さい。" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
                                    HighlightCssClass="validatorCallout" TargetControlID="vdrBossHp3">
                                </ajaxToolkit:ValidatorCalloutExtender>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                報酬
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtIncomeMin" runat="server" MaxLength="10" Width="80px"></asp:TextBox>～<asp:TextBox
                                    ID="txtIncomeMax" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
                                <% // 必須チェック %>
                                <asp:RequiredFieldValidator ID="vdrIncomeMin" runat="server" ControlToValidate="txtIncomeMin"
                                    ErrorMessage="報酬最小値を入力して下さい。" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server"
                                    HighlightCssClass="validatorCallout" TargetControlID="vdrIncomeMin">
                                </ajaxToolkit:ValidatorCalloutExtender>
                                <asp:RequiredFieldValidator ID="vdrIncomeMax" runat="server" ControlToValidate="txtIncomeMax"
                                    ErrorMessage="報酬最大値を入力して下さい。" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server"
                                    HighlightCssClass="validatorCallout" TargetControlID="vdrIncomeMax">
                                </ajaxToolkit:ValidatorCalloutExtender>
                                <asp:CompareValidator ID="vdcIncome" runat="server" ControlToValidate="txtIncomeMax"
                                    ControlToCompare="txtIncomeMin" Operator="GreaterThanEqual" Type="Integer" ErrorMessage="報酬の大小関係が不正です"
                                    ValidationGroup="Update">*</asp:CompareValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18" runat="Server"
                                    HighlightCssClass="validatorCallout" TargetControlID="vdcIncome">
                                </ajaxToolkit:ValidatorCalloutExtender>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle2">
                                称号
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtHonor" runat="server" MaxLength="60" Width="120px"></asp:TextBox>
                                <% // 必須チェック %>
                                <asp:RequiredFieldValidator ID="vdrHonor" runat="server" ErrorMessage="称号を入力して下さい。"
                                    ControlToValidate="txtHonor" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server"
                                    TargetControlID="vdrHonor" HighlightCssClass="validatorCallout" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <asp:Panel ID="pnlClearItem" runat="server">
                    <fieldset class="fieldset-inner">
                        <legend>[クリア時獲得アイテム]</legend>
                        <table border="0" style="width: 720px" class="tableStyle">
                            <tr>
                                <td class="tdHeaderStyle">
                                    アイテム１
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstGameItemCategory1" runat="server" DataSourceID="dsGameItemCategory"
                                        OnDataBound="lst_DataBound" OnSelectedIndexChanged="lstGameItemCategory_IndexChanged"
                                        DataTextField="GAME_ITEM_CATEGORY_NM" DataValueField="GAME_ITEM_CATEGORY_TYPE"
                                        Width="170px" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="lstItemGetCd1" runat="server" DataSourceID="dsItemGet" DataTextField="CODE_NM"
										DataValueField="CODE" OnDataBound="lstItemGetCd_DataBound" OnSelectedIndexChanged="lstItemGetCd_IndexChanged" AutoPostBack="True">
									</asp:DropDownList>
									<asp:DropDownList ID="lstItemPresent1" runat="server" OnSelectedIndexChanged="lstItemPresent_IndexChanged" AutoPostBack="True">
										<asp:ListItem Value=""></asp:ListItem>
										<asp:ListItem Value="0">通常</asp:ListItem>
										<asp:ListItem Value="1">ﾌﾟﾚｾﾞﾝﾄ</asp:ListItem>
									</asp:DropDownList>
                                    <asp:DropDownList ID="lstGameItem1" runat="server" DataSourceID="dsGameItem" DataTextField="GAME_ITEM_NM"
                                        OnDataBound="lst_DataBound" DataValueField="GAME_ITEM_SEQ" Width="170px">
                                    </asp:DropDownList>
                                    &nbsp;
                                    <asp:TextBox ID="txtGameItemGetCount1" runat="server" MaxLength="10" Width="50px"></asp:TextBox>個<br />
                                    <asp:CustomValidator ID="vdcGameItemGet1" runat="server" ErrorMessage="" OnServerValidate="vdcGameItemGet1_ServerValidate"
                                        ValidationGroup="Update"></asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    アイテム２
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstGameItemCategory2" runat="server" DataSourceID="dsGameItemCategory"
                                        OnDataBound="lst_DataBound" OnSelectedIndexChanged="lstGameItemCategory_IndexChanged"
                                        DataTextField="GAME_ITEM_CATEGORY_NM" DataValueField="GAME_ITEM_CATEGORY_TYPE"
                                        Width="170px" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="lstItemGetCd2" runat="server" DataSourceID="dsItemGet" DataTextField="CODE_NM"
										DataValueField="CODE" OnDataBound="lstItemGetCd_DataBound" OnSelectedIndexChanged="lstItemGetCd_IndexChanged" AutoPostBack="True">
									</asp:DropDownList>
									<asp:DropDownList ID="lstItemPresent2" runat="server" OnSelectedIndexChanged="lstItemPresent_IndexChanged" AutoPostBack="True">
										<asp:ListItem Value=""></asp:ListItem>
										<asp:ListItem Value="0">通常</asp:ListItem>
										<asp:ListItem Value="1">ﾌﾟﾚｾﾞﾝﾄ</asp:ListItem>
									</asp:DropDownList>
                                    <asp:DropDownList ID="lstGameItem2" runat="server" DataSourceID="dsGameItem2" DataTextField="GAME_ITEM_NM"
                                        OnDataBound="lst_DataBound" DataValueField="GAME_ITEM_SEQ" Width="170px">
                                    </asp:DropDownList>
                                    &nbsp;
                                    <asp:TextBox ID="txtGameItemGetCount2" runat="server" MaxLength="10" Width="50px"></asp:TextBox>個<br />
                                    <asp:CustomValidator ID="vdcGameItemGet2" runat="server" ErrorMessage="" OnServerValidate="vdcGameItemGet2_ServerValidate"
                                        ValidationGroup="Update"></asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle2">
                                    アイテム３
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstGameItemCategory3" runat="server" DataSourceID="dsGameItemCategory"
                                        OnDataBound="lst_DataBound" OnSelectedIndexChanged="lstGameItemCategory_IndexChanged"
                                        DataTextField="GAME_ITEM_CATEGORY_NM" DataValueField="GAME_ITEM_CATEGORY_TYPE"
                                        Width="170px" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="lstItemGetCd3" runat="server" DataSourceID="dsItemGet" DataTextField="CODE_NM"
										DataValueField="CODE" OnDataBound="lstItemGetCd_DataBound" OnSelectedIndexChanged="lstItemGetCd_IndexChanged" AutoPostBack="True">
									</asp:DropDownList>
									<asp:DropDownList ID="lstItemPresent3" runat="server" OnSelectedIndexChanged="lstItemPresent_IndexChanged" AutoPostBack="True">
										<asp:ListItem Value=""></asp:ListItem>
										<asp:ListItem Value="0">通常</asp:ListItem>
										<asp:ListItem Value="1">ﾌﾟﾚｾﾞﾝﾄ</asp:ListItem>
									</asp:DropDownList>
                                    <asp:DropDownList ID="lstGameItem3" runat="server" DataSourceID="dsGameItem3" DataTextField="GAME_ITEM_NM"
                                        OnDataBound="lst_DataBound" DataValueField="GAME_ITEM_SEQ" Width="170px">
                                    </asp:DropDownList>
                                    &nbsp;
                                    <asp:TextBox ID="txtGameItemGetCount3" runat="server" MaxLength="10" Width="50px"></asp:TextBox>個<br />
                                    <asp:CustomValidator ID="vdcGameItemGet3" runat="server" ErrorMessage="" OnServerValidate="vdcGameItemGet3_ServerValidate"
                                        ValidationGroup="Update"></asp:CustomValidator>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </asp:Panel>
                <asp:Panel ID="pnlCommand" runat="server">
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Update"
                        OnClick="btnUpdate_Click" />
                    <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" ValidationGroup="Delete"
                        OnClick="btnDelete_Click" />
                    <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" CausesValidation="False"
                        Visible="true" OnClick="btnCancel_Click" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[ステージ一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdStage.PageIndex + 1 %>
                        of
                        <%= grdStage.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="600px">
                    <asp:GridView ID="grdStage" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        PageSize="30" EnableSortingAndPagingCallbacks="false" DataSourceID="dsStage"
                        SkinID="GridViewColor" Font-Size="small" AllowSorting="true" DataKeyNames="SITE_CD,STAGE_SEQ">
                        <Columns>
                            <asp:TemplateField HeaderText="ｽﾃｰｼﾞ名/<br/>ｽﾃｰｼﾞSEQ">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkStageNm" runat="server" CommandArgument='<%# Eval("STAGE_SEQ") %>'
                                        Text='<%# Eval("STAGE_NM") %>' OnCommand="lnkStageNm_Command"></asp:LinkButton>
                                    <br />
                                    <asp:Label ID="lblStageLevel" runat="server" Text='<%# Eval("STAGE_SEQ") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="STAGE_LEVEL" HeaderText="ｽﾃｰｼﾞﾚﾍﾞﾙ">
                                <ItemStyle HorizontalAlign="center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="BOSS_NM" HeaderText="ﾎﾞｽ名">
                                <ItemStyle HorizontalAlign="left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="BOSS_DESCRIPTION" HeaderText="ﾎﾞｽ説明">
                                <ItemStyle HorizontalAlign="left" Width="200px" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="ﾎﾞｽｲﾒｰｼﾞ">
                                <ItemTemplate>
                                    <asp:Image ID="imgBoss" runat="server" Width="80px" ImageUrl='<%# GetImagePath(Eval("STAGE_SEQ")) %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="BOSS_HP" DataFormatString="{0:#,##0}" HeaderText="BOSS HP">
                                <ItemStyle HorizontalAlign="right" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="BOSS HP<br/>仲間1人">
                                <ItemTemplate>
                                    <asp:Label ID="lblBossHpFriend1" runat="server" Text='<%# Eval("BOSS_HP_FRIEND_1", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="BOSS HP<br/>仲間2人">
                                <ItemTemplate>
                                    <asp:Label ID="lblBossHpFriend2" runat="server" Text='<%# Eval("BOSS_HP_FRIEND_2", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="BOSS HP<br/>仲間3人">
                                <ItemTemplate>
                                    <asp:Label ID="lblBossHpFriend3" runat="server" Text='<%# Eval("BOSS_HP_FRIEND_3", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="EXP" HeaderText="経験値" DataFormatString="{0:N0}">
                                <ItemStyle HorizontalAlign="right" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="報酬">
                                <ItemTemplate>
                                    <asp:Label ID="lblIncomeMin" runat="server" Text='<%# Eval("INCOME_MIN", "{0:N0}") %>'></asp:Label>～
                                    <asp:Label ID="lblIncomeMax" runat="server" Text='<%# Eval("INCOME_MAX", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="HONOR" HeaderText="称号">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkTownList" runat="server" NavigateUrl='<%# string.Format("~/Extension/TownList.aspx?sitecd={0}&stageseq={1}&sexcd={2}", Eval("SITE_CD"), Eval("STAGE_SEQ"), Eval("SEX_CD"))%>'
                                        Text="街一覧"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsStage" runat="server" SelectMethod="GetPageCollection"
        ConvertNullToDBNull="true" SortParameterName="" TypeName="Stage" SelectCountMethod="GetPageCount"
        EnablePaging="True" OnSelected="dsStage_Selected">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsStageLevel" runat="server" SelectMethod="GetLevelList"
        TypeName="Stage">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsStageGroup" runat="server" SelectMethod="GetList" TypeName="StageGroup">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItemCategory" runat="server" SelectMethod="GetList"
        TypeName="GameItemCategory">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItem" runat="server" SelectMethod="GetListByCategoryType"
        TypeName="GameItem" OnSelecting="dsGameItem_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
            <asp:Parameter Name="pGameItemCategoryType" Type="String" DefaultValue="" />
            <asp:Parameter Name="pGameItemGetCd" Type="String" DefaultValue="" />
            <asp:Parameter Name="pPresentFlag" Type="String" DefaultValue="" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItem2" runat="server" SelectMethod="GetListByCategoryType"
        TypeName="GameItem" OnSelecting="dsGameItem2_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
            <asp:Parameter Name="pGameItemCategoryType" Type="String" DefaultValue="" />
            <asp:Parameter Name="pGameItemGetCd" Type="String" DefaultValue="" />
            <asp:Parameter Name="pPresentFlag" Type="String" DefaultValue="" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItem3" runat="server" SelectMethod="GetListByCategoryType"
        TypeName="GameItem" OnSelecting="dsGameItem3_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
            <asp:Parameter Name="pGameItemCategoryType" Type="String" DefaultValue="" />
            <asp:Parameter Name="pGameItemGetCd" Type="String" DefaultValue="" />
            <asp:Parameter Name="pPresentFlag" Type="String" DefaultValue="" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsItemGet" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="A2" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtIncomeMin" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtIncomeMax" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtBossHp" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtBossHp1" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtBossHp2" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtBossHp3" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtExp">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="ステージ情報を更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete"
        ConfirmText="ステージ情報を削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>
