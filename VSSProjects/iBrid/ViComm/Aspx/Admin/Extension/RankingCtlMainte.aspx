<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RankingCtlMainte.aspx.cs" Inherits="Extension_RankingCtlMainte" Title="足あとランキングタイマー設定"
    ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="足あとランキングタイマー設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlMainte">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel runat="server" ID="pnlDtl">
                    <table border="0" style="width: 800px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    Enabled="false" DataValueField="SITE_CD" Width="180px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                備考
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtRemarks" runat="server" MaxLength="1000" Width="650px" Rows="3"
                                    TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                履歴開始日
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstFromYYYY" runat="server" Width="60px" DataSource='<%# YearArray %>'>
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstFromMM" runat="server" Width="40px" DataSource='<%# MonthArray %>'>
                                </asp:DropDownList>月
                                <asp:DropDownList ID="lstFromDD" runat="server" Width="40px" DataSource='<%# DayArray %>'>
                                </asp:DropDownList>日
                                <asp:DropDownList ID="lstFromHH" runat="server" Width="40px" DataSource='<%# HourArray %>'>
                                </asp:DropDownList>時
                                <asp:DropDownList ID="lstFromMI" runat="server" DataSource='<%# MinuteArray %>'>
                                </asp:DropDownList>分
                                <asp:Label ID="lblErrorMessageFrom" runat="server" Text="" Visible="false" ForeColor="red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle2">
                                履歴終了日
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstToYYYY" runat="server" Width="60px" DataSource='<%# YearArray %>'>
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstToMM" runat="server" Width="40px" DataSource='<%# MonthArray %>'>
                                </asp:DropDownList>月
                                <asp:DropDownList ID="lstToDD" runat="server" Width="40px" DataSource='<%# DayArray %>'>
                                </asp:DropDownList>日
                                <asp:DropDownList ID="lstToHH" runat="server" Width="40px" DataSource='<%# HourArray %>'>
                                </asp:DropDownList>時
                                <asp:DropDownList ID="lstToMI" runat="server" DataSource='<%# MinuteArray %>'>
                                </asp:DropDownList>分
                                <asp:Label ID="lblErrorMessageTo" runat="server" Text="" Visible="false" ForeColor="red"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click"
                        ValidationGroup="Detail" />
                    <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click"
                        ValidationGroup="Key" />
                    <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click"
                        CausesValidation="False" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <fieldset class="fieldset">
            <legend>[検索条件]</legend>
            <fieldset class="fieldset-inner">
                <table border="0" style="width: 800px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle2">
                            サイトコード
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="180px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    CausesValidation="False" />
                <asp:Button runat="server" ID="btnRegist" Text="追加" CssClass="seekbutton" CausesValidation="False"
                    OnClick="btnRegist_Click" />
            </fieldset>
            <fieldset class="fieldset-inner">
                <legend>[設定一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%#GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%=grdRankingCtl.PageIndex + 1%>
                        of
                        <%=grdRankingCtl.PageCount%>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel runat="server" ID="pnlGrid">
                    <asp:GridView ID="grdRankingCtl" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        DataSourceID="dsRankingCtl" AllowSorting="True" SkinID="GridViewColor" DataKeyNames="SITE_CD,SUMMARY_TYPE,RANKING_CTL_SEQ">
                        <Columns>
                            <asp:TemplateField HeaderText="開始日時">
                                <ItemTemplate>
                                    <asp:Label ID="lblStartDate" runat="server" Text='<%# Eval("SUMMARY_START_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>'
                                        Visible='<%# !GetLinkButtonVisible() %>'></asp:Label>
                                    <asp:LinkButton ID="lnkStartDate" runat="server" CausesValidation="False" CommandArgument='<%# Container.DataItemIndex %>'
                                        OnCommand="lnkStartDate_Command" Text='<%# Eval("SUMMARY_START_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>'
                                        Visible='<%# GetLinkButtonVisible() %>'>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="SUMMARY_END_DATE" HeaderText="終了日時" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}"
                                HtmlEncode="False">
                                <ItemStyle HorizontalAlign="Center" Wrap="false" />
                            </asp:BoundField>
                            <asp:BoundField DataField="REMARKS" HeaderText="備考">
                                <ItemStyle HorizontalAlign="Left" Wrap="true" />
                            </asp:BoundField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkRanking" runat="server" NavigateUrl='<%# string.Format("~/Extension/RankingEx.aspx?sitecd={0}&seq={1}&summarytype={2}",Eval("SITE_CD"),Eval("RANKING_CTL_SEQ"),Eval("SUMMARY_TYPE")) %>'
                                        Text="ランキング"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </fieldset>
        <br />
    </div>
    <asp:ObjectDataSource ID="dsRankingCtl" runat="server" EnablePaging="True" OnSelected="dsRankingCtl_Selected"
        SelectCountMethod="GetPageCount" SelectMethod="GetPageCollection" TypeName="RankingCtl">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSummaryType" QueryStringField="summarytype" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete"
        ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
