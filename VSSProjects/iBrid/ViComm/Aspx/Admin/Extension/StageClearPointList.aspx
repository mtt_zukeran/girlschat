﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="StageClearPointList.aspx.cs" Inherits="Extension_StageClearPointList" 
    Title="ｽﾃｰｼﾞｸﾘｱﾎﾟｲﾝﾄ設定" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="ｽﾃｰｼﾞｸﾘｱﾎﾟｲﾝﾄ設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlMainte">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel runat="server" ID="pnlDtl">
                    <table border="0" style="width: 820px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                (男性)ｹﾞｰﾑ登録からの日数
                            </td>
                            <td class="tdDataStyle">
                                 <asp:TextBox ID="txtManClearFromRegistDays" runat="server" Text="" MaxLength="2" Width="30px"></asp:TextBox>日
								<asp:RegularExpressionValidator ID="vdeManClearFromRegistDays" runat="server" ErrorMessage="日数は1～2桁の数字で入力して下さい。" ValidationExpression="\d{1,2}" ControlToValidate="txtManClearFromRegistDays"
									ValidationGroup="Detail">*</asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                (女性)ｹﾞｰﾑ登録からの日数
                            </td>
                            <td class="tdDataStyle">
                                 <asp:TextBox ID="txtCastClearFromRegistDays" runat="server" Text="" MaxLength="2" Width="30px"></asp:TextBox>日
								<asp:RegularExpressionValidator ID="vdeCastClearFromRegistDays" runat="server" ErrorMessage="日数は1～2桁の数字で入力して下さい。" ValidationExpression="\d{1,2}" ControlToValidate="txtCastClearFromRegistDays"
									ValidationGroup="Detail">*</asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                (男性)ｸﾘｱ対象のｽﾃｰｼﾞ
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstManClearPointTargetStageSeq" runat="server" DataSourceID="dsManStage"
                                    DataTextField="STAGE_DISPLAY" DataValueField="STAGE_SEQ" Width="170px" OnDataBound="lst_DataBound" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                (女性)ｸﾘｱ対象のｽﾃｰｼﾞ
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstCastClearPointTargetStageSeq" runat="server" DataSourceID="dsCastStage"
                                    DataTextField="STAGE_DISPLAY" DataValueField="STAGE_SEQ" Width="170px" OnDataBound="lst_DataBound" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                (男性)付与ｹﾞｰﾑﾎﾟｲﾝﾄ
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtManStageClearPoint" runat="server" Text=""  />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle2">
                                (女性)付与ｹﾞｰﾑﾎﾟｲﾝﾄ
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtCastStageClearPoint" runat="server" Text=""  />
                            </td>
                        </tr>
                    </table>
                    <table border="0" style="width: 820px" >
                        <tr>
                            <td >
                                <asp:HiddenField ID="hdnSiteCd" runat="server" />
                                <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click"
                                    ValidationGroup="Detail" />
                                <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click"
                                    CausesValidation="False" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel ID="pnlList" runat="server">
            <fieldset>
                <legend>[サイト一覧]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count
						<%# GetRecCount() %>
					</a>
					<br />
					<a class="reccount">Current viewing page
						<%= grdStageClearPoint.PageIndex + 1%>
						of
						<%= grdStageClearPoint.PageCount%>
					</a>
				</asp:Panel>
				&nbsp;
				<asp:Panel ID="pnlGrid" runat="server">
					<asp:GridView ID="grdStageClearPoint" runat="server" DataSourceID="dsStageClearPoint" AutoGenerateColumns="False" AllowPaging="true" AllowSorting="true" SkinID="GridViewColor">
						<Columns>
							<asp:TemplateField HeaderText="サイトコード">
								<ItemTemplate>
									<asp:LinkButton ID="lnkSiteCd" runat="server" Text='<%# Eval("SITE_CD") %>' CommandArgument='<%# Eval("SITE_CD") %>' OnCommand="lnkSiteCd_Command" CausesValidation="False">
									</asp:LinkButton>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Left" />
							</asp:TemplateField>
							<asp:BoundField DataField="UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtManClearFromRegistDays" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtCastClearFromRegistDays" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtManStageClearPoint" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtCastStageClearPoint" />
    <asp:ObjectDataSource ID="dsManStage" runat="server" SelectMethod="GetList" TypeName="Stage">
        <SelectParameters>
            <asp:ControlParameter ControlID="hdnSiteCd" Name="pSiteCd" PropertyName="Value" Type="String" />
            <asp:Parameter  Name="pSexCd" DefaultValue="1" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
        <asp:ObjectDataSource ID="dsCastStage" runat="server" SelectMethod="GetList" TypeName="Stage">
        <SelectParameters>
            <asp:ControlParameter ControlID="hdnSiteCd" Name="pSiteCd" PropertyName="Value" Type="String" />
            <asp:Parameter  Name="pSexCd" DefaultValue="3" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsStageClearPoint" runat="server" SelectMethod="GetPageCollection" SelectCountMethod="GetPageCount" EnablePaging="true" OnSelected="dsStageClearPoint_Selected"
		TypeName="StageClearPoint"></asp:ObjectDataSource>
     
</asp:Content>