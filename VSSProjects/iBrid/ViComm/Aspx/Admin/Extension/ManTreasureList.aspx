<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ManTreasureList.aspx.cs" Inherits="Extension_ManTreasureList"
	Title="男性お宝画像一覧" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="男性お宝画像一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 740px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle">
							出現日
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtDisplayDay" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RangeValidator ID="vdeDisplayDay" runat="server" ControlToValidate="txtDisplayDay" ErrorMessage="出現日を正しく入力して下さい。" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
								Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
						</td>
						<td class="tdHeaderStyle">
							属性
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstManTreasureAttr" runat="server" DataSourceID="dsManTreasureAttr" DataTextField="CAST_GAME_PIC_ATTR_NM" DataValueField="CAST_GAME_PIC_ATTR_SEQ" OnDataBound="lst_DataBound">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							認証状態
						</td>
						<td class="tdDataStyle">
							<asp:CheckBox ID="chkPublish" runat="server" Text="認証済" />
							<asp:CheckBox ID="chkNonPublish" runat="server" Text="未認証" />
						</td>
						<td class="tdHeaderStyle">
							ログインID
						</td>
						<td colspan="3" class="tdDataStyle">
							<asp:TextBox ID="txtLoginId" runat="server"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							チェック状態
						</td>
						<td class="tdDataStyle">
							<asp:CheckBox ID="chkChecked" runat="server" Text="チェック済" />
							<asp:CheckBox ID="chkUnChecked" runat="server" Text="未チェック" />
						</td>
						<td class="tdHeaderStyle2">
						</td>
						<td colspan="3" class="tdDataStyle">
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="True" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
			</asp:Panel>
		</fieldset>
		<fieldset>
			<legend>[男性お宝画像一覧]</legend>
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="550px">
				<asp:GridView ID="grdManTreasure" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsManTreasure" SkinID="GridViewColor" DataKeyNames="SITE_CD,USER_SEQ,USER_CHAR_NO,CAST_GAME_PIC_SEQ,LOGIN_ID">
					<Columns>
						<asp:TemplateField HeaderText="写真">
							<ItemTemplate>
								<a href="<%# PickupHelper.GeneratePickupObjsViewerUrl(this, Eval("SITE_CD").ToString(),Eval("CAST_GAME_PIC_SEQ").ToString(), ViComm.ViCommConst.PicupTypes.SOCIAL_GAME_PIC,ViComm.ViCommConst.FLAG_OFF) %>">
									<asp:Image ID="imgObjSmallPhoto" runat="server" ImageUrl='<%# string.Format("../{0}",Eval("OBJ_SMALL_PHOTO_IMG_PATH").ToString()) %>'>
									</asp:Image>
								</a>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField DataField="LOGIN_ID" HeaderText="ﾛｸﾞｲﾝID">
							<ItemStyle Wrap="false" />
						</asp:BoundField>
						<asp:TemplateField HeaderText="ﾊﾝﾄﾞﾙ名/ｹﾞｰﾑﾊﾝﾄﾞﾙ名">
							<ItemStyle Wrap="false" />
							<ItemTemplate>
								<asp:HyperLink ID="lnkHandleNm" runat="server" NavigateUrl='<%# string.Format("../Cast/CastView.aspx?loginid={0}&return={1}",Eval("LOGIN_ID"),"ManTreasureList.aspx") %>'
									Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) + " / " + Eval("GAME_HANDLE_NM") %>'></asp:HyperLink>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField DataField="CAST_GAME_PIC_ATTR_NM" HeaderText="属性">
							<ItemStyle Wrap="false" />
						</asp:BoundField>
						<asp:TemplateField HeaderText="認証状態">
							<ItemStyle HorizontalAlign="Center" />
							<ItemTemplate>
								<asp:Label ID="lblPublish" runat="server" Text='<%# Eval("PUBLISH_FLAG").ToString().Equals(ViCommConst.FLAG_ON_STR) ? "認証済" : "未認証" %>'></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField DataField="DISPLAY_DAY" HeaderText="出現日">
							<ItemStyle Wrap="false" />
						</asp:BoundField>
						<asp:BoundField DataField="UPLOAD_DATE" HeaderText="アップロード日時" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" Wrap="false" />
						</asp:BoundField>
						<asp:HyperLinkField HeaderText="設定" Text="設定" DataNavigateUrlFields="SITE_CD,USER_SEQ,USER_CHAR_NO,CAST_GAME_PIC_SEQ" DataNavigateUrlFormatString="../Extension/ManTreasureMainte.aspx?sitecd={0}&userseq={1}&usercharno={2}&picseq={3}&return=ManTreasureList">
							<HeaderStyle Wrap="false" />
							<ItemStyle HorizontalAlign="Center" />
						</asp:HyperLinkField>
					</Columns>
					<PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
				</asp:GridView>
			</asp:Panel>
			&nbsp;
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br />
				<a class="reccount">Current viewing page
					<%=grdManTreasure.PageIndex + 1%>
					of
					<%=grdManTreasure.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsManTreasure" runat="server" SelectMethod="GetPageCollection" TypeName="ManTreasure" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsManTreasure_Selected" OnSelecting="dsManTreasure_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pDisplayDay" Type="String" />
			<asp:Parameter Name="pPublishFlag" Type="Boolean" />
			<asp:Parameter Name="pNonPublishFlag" Type="Boolean" />
			<asp:Parameter Name="pLoginId" Type="String" />
			<asp:Parameter Name="pCastGamePicAttrSeq" Type="String" />
			<asp:Parameter Name="pCheckedFlag" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsManTreasureAttr" runat="server" SelectMethod="GetList" TypeName="ManTreasureAttr" OnSelecting="dsManTreasureAttr_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:MaskedEditExtender ID="mskDisplayDay" runat="server" TargetControlID="txtDisplayDay" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay"
		ClearMaskOnLostFocus="true">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:ValidatorCalloutExtender ID="vceDisplayDay" runat="Server" TargetControlID="vdeDisplayDay" HighlightCssClass="validatorCallout" />
</asp:Content>
