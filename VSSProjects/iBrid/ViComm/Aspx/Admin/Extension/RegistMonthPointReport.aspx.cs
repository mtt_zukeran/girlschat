﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 日別サービスレポート

--	Progaram ID		: RegistMonthPointReport
--
--  Creation Date	: 2011.08.10
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using iBridCommLib;
using System.Drawing;
using ViComm;

public partial class Extension_RegistMonthPointReport : System.Web.UI.Page {
	private int iUsedPointSum = 0;
	private int iRegistCount = 0;

	protected string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		private set {
			this.ViewState["SexCd"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		if (!this.IsPostBack) {
			this.SexCd = this.Request.QueryString["sexcd"];
			if (ViCommConst.MAN.Equals(this.SexCd)) {
				this.lblPgmTitle.Text = "男性" + this.lblPgmTitle.Text;
			} else {
				this.lblPgmTitle.Text = "女性" + this.lblPgmTitle.Text;
			}
			this.Title = this.lblPgmTitle.Text;

			this.InitPage();

			this.GetList();
			this.pnlInfo.Visible = true;
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		this.GetList();
		this.pnlInfo.Visible = true;
	}

	protected void dsRegistMonthPointReport_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pRegistMonth"] = string.Format("{0}/{1}",this.lstYYYY.SelectedValue,this.lstMM.SelectedValue);
	}

	protected void grdRegistMonthPointReport_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			iUsedPointSum += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"USED_POINT"));
			iRegistCount = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"REGIST_COUNT"));

		} else if (e.Row.RowType == DataControlRowType.Footer) {
			Label lblUsedPointSum = e.Row.FindControl("lblUsedPointSum") as Label;
			lblUsedPointSum.Text = string.Concat(iUsedPointSum.ToString("N0"),"Pt");
		}
	}

	private void InitPage() {
		this.grdRegistMonthPointReport.DataSourceID = string.Empty;
		this.pnlKey.DataBind();
		this.pnlInfo.Visible = false;
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		SysPrograms.SetupDay(lstYYYY,lstMM,new DropDownList(),false);
		this.lstYYYY.SelectedValue = DateTime.Today.ToString("yyyy");
		this.lstMM.SelectedValue = DateTime.Today.ToString("MM");
	}

	private void GetList() {
		this.grdRegistMonthPointReport.DataSourceID = "dsRegistMonthPointReport";
		this.grdRegistMonthPointReport.DataBind();
		this.lblRegistCount.DataBind();
	}

	protected string GetRegistCountMark() {
		return string.Format("指定月({0}年{1}月)登録者数：{2}人",this.lstYYYY.SelectedValue,this.lstMM.SelectedValue,iRegistCount.ToString("N0"));
	}

	protected void btnCSV_Click(object sender,EventArgs e) {

		if (grdRegistMonthPointReport.Rows.Count == 0) {
			return;
		}

		DataTable oCsvData;
		using (RegistMonthPointReport oRegistMonthPointReport = new RegistMonthPointReport()) {
			using (DataSet oDataSet = oRegistMonthPointReport.GetListReport(
				this.lstSeekSiteCd.SelectedValue,
				string.Format("{0}/{1}",this.lstYYYY.SelectedValue,this.lstMM.SelectedValue),
				this.SexCd)) {

				if (oDataSet.Tables[0].Rows.Count > 0) {
					oCsvData = oDataSet.Tables[0];
				} else {
					return;
				}
			}
		}

		//ヘッダ作成
		string sHeader = "課金月,ﾎﾟｲﾝﾄ消費,ﾎﾟｲﾝﾄ消費/指定月登録者数,累計ﾎﾟｲﾝﾄ消費/指定月登録者数,消費UU,ARPPU";

		//CSV出力 
		Response.ContentType = "application/download";
		Response.AppendHeader("Content-Disposition","attachment;filename=RegistMonthPointReport.csv");
		Response.ContentEncoding = System.Text.Encoding.GetEncoding("Shift_JIS");

		Response.Write(sHeader + "\r\n");
		foreach (DataRow oCsvRow in oCsvData.Rows) {
			string sData =
				SetCsvString(oCsvRow["REPORT_MONTH"].ToString()) + "," +
				SetCsvString(oCsvRow["USED_POINT"].ToString()) + "Pt," +
				SetCsvString(oCsvRow["POINT_PER_REGIST_COUNT"].ToString()) + "Pt," +
				SetCsvString(oCsvRow["POINT_SUM_PER_REGIST_COUNT"].ToString()) + "Pt," +
				SetCsvString(oCsvRow["USED_POINT_COUNT_UNIQUE"].ToString()) + "," +
				SetCsvString(oCsvRow["ARPPU"].ToString()) + "Pt";

			Response.Write(sData + "\r\n");
		}
		Response.End();
	}

	private string SetCsvString(string pData) {
		if (pData.IndexOf('"') > -1 ||
			pData.IndexOf(',') > -1 ||
			pData.IndexOf('\r') > -1 ||
			pData.IndexOf('\n') > -1 ||
			pData.StartsWith(" ") || pData.StartsWith("\t") ||
			pData.EndsWith(" ") || pData.EndsWith("\t")) {
			if (pData.IndexOf('"') > -1) {
				//"を""とする
				pData = pData.Replace("\"","\"\"");
			}
			if (pData.IndexOf('\r') > -1 || pData.IndexOf('\n') > -1) {
				//改行しない 
				pData = pData.Replace("\r","");
				pData = pData.Replace("\n","");
			}
			pData = "\"" + pData + "\"";
		}
		return pData;
	}
}