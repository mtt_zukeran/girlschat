﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="PartTimeJobPlanList.aspx.cs" Inherits="Extension_PartTimeJobPlanList" Title="バイトプラン一覧"
    ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="バイトプラン一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 540px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle2">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" /><asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton" ValidationGroup="Create"
                    OnClick="btnCreate_Click" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlPartTimeJobPlanInfo">
            <fieldset class="fieldset">
                <legend>[バイトプラン情報]</legend>
                <table border="0" style="width: 600px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            バイトプランID(SEQ)
                        </td>
                        <td class="tdDataStyle">
                            <asp:Label ID="lblPartTimeJobId" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            バイトプラン名
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtPartTimeJobNm" runat="server" MaxLength="64" Width="200px"></asp:TextBox>
                            <% // 必須チェック %>
                            <asp:RequiredFieldValidator ID="vdrPartTimeJobNm" runat="server" ErrorMessage="バイトプラン名を入力してください。"
                                ControlToValidate="txtPartTimeJobNm" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                            <ajaxToolkit:ValidatorCalloutExtender ID="vdePartTimeJobNm" runat="Server" TargetControlID="vdrPartTimeJobNm"
                                HighlightCssClass="validatorCallout" />
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            収入
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtIncome" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            待機分数
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtWaitingMin" runat="server" MaxLength="3" Width="80px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                           公開ステージ
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstStageSeq" runat="server" DataSourceID="dsStage" OnDataBound="lst_DataBound"
                                ValidationGroup="Update" DataTextField="STAGE_DISPLAY" DataValueField="STAGE_SEQ"
                                Width="170px">
                            </asp:DropDownList>
                            <% // 必須チェック %>
                            <asp:RequiredFieldValidator ID="vdrStageSeq" runat="server" ErrorMessage="公開ステージを選択してください。"
                                ControlToValidate="lstStageSeq" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                TargetControlID="vdrStageSeq" HighlightCssClass="validatorCallout" />
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="pnlCommand" runat="server">
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Update"
                        OnClick="btnUpdate_Click" />
                    <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" ValidationGroup="Delete"
                        OnClick="btnDelete_Click" />
                    <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" CausesValidation="False"
                        Visible="true" OnClick="btnCancel_Click" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[バイトプラン一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdPartTimeJobPlan.PageIndex + 1 %>
                        of
                        <%= grdPartTimeJobPlan.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="600px">
                    <asp:GridView ID="grdPartTimeJobPlan" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        PageSize="30" EnableSortingAndPagingCallbacks="false" DataSourceID="dsPartTimeJobPlan"
                        SkinID="GridViewColor" AllowSorting="true" DataKeyNames="SITE_CD,PART_TIME_JOB_SEQ">
                        <Columns>
                            <asp:TemplateField HeaderText="バイトSEQ">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%# Eval("PART_TIME_JOB_SEQ") %>'
                                        Text='<%# Eval("PART_TIME_JOB_SEQ") %>' OnCommand="lnkEdit_Command"></asp:LinkButton><br />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="バイト名">
                                <ItemTemplate>
                                    <asp:Label ID="lblPartTimeJobPlanNm" runat="server" Text='<%# Eval("PART_TIME_JOB_NM") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="INCOME" DataFormatString="{0:N0}" HeaderText="収入">
                                <ItemStyle HorizontalAlign="right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="WAITING_MIN" DataFormatString="{0:N0}" HeaderText="待機分数">
                                <ItemStyle HorizontalAlign="right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="STAGE_NM" HeaderText="公開ステージ">
                                <ItemStyle HorizontalAlign="left" />
                            </asp:BoundField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
            <br />
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsPartTimeJobPlan" runat="server" SelectMethod="GetPageCollection"
        ConvertNullToDBNull="true" SortParameterName="" TypeName="PartTimeJobPlan" SelectCountMethod="GetPageCount"
        EnablePaging="True" OnSelected="dsPartTimeJobPlan_Selected">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsStage" runat="server" SelectMethod="GetList" TypeName="Stage">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:Parameter Name="pSexCd" DefaultValue="3" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtIncome" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtWaitingMin" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="バイトプラン情報を更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete"
        ConfirmText="バイトプラン情報を削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>
