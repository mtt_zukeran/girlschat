﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ｷｬｽﾄ用お宝一覧

--	Progaram ID		: CastTreasureList
--
--  Creation Date	: 2011.07.25
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.IO;
using System.Drawing;

public partial class Extension_CastTreasureList : System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string StageGroupType {
		get {
			return iBridUtil.GetStringValue(this.ViewState["StageGroupType"]);
		}
		set {
			this.ViewState["StageGroupType"] = value;
		}
	}

	private string CastTreasureSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["CastTreasureSeq"]);
		}
		set {
			this.ViewState["CastTreasureSeq"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
		this.CastTreasureSeq = string.Empty;
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
		this.ClearFileds();
		this.lstSeekStageGroupType.SelectedIndex = 0;
		this.GetList();
		
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.UpdateData(false);
		this.pnlCastTreasureInfo.Visible = false;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		this.UpdateData(true);
		this.pnlCastTreasureInfo.Visible = false;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.ClearFileds();
		this.pnlKey.Enabled = true;
		this.pnlCastTreasureInfo.Visible = false;
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		this.ClearFileds();
		this.CastTreasureSeq = string.Empty;
		this.pnlKey.Enabled = false;
		this.pnlCastTreasureInfo.Visible = true;
	}

	protected void lnkEdit_Command(object sender,CommandEventArgs e) {
		if (!this.IsValid) {
			return;
		}
		int iIndex = int.Parse(iBridUtil.GetStringValue(e.CommandArgument));

		GridViewRow oRow = this.grdCastTreasure.Rows[iIndex];
		HiddenField oCastTreasureSeqHiddenField = oRow.FindControl("hdnCastTreasureSeq") as HiddenField;
		HiddenField oStageGroupTypeHiddenFiled = oRow.FindControl("hdnStageGroupType") as HiddenField;

		this.CastTreasureSeq = oCastTreasureSeqHiddenField.Value;
		this.StageGroupType = oStageGroupTypeHiddenFiled.Value;
		this.GetData();
		this.lstStageGroupType.Enabled = false;
		this.pnlKey.Enabled = false;
		this.pnlCastTreasureInfo.Visible = true;
	}

	protected void dsCastTreasure_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void lstSiteCd_SelectedIndexChanged(object sender,EventArgs e) {
		this.lstSeekStageGroupType.DataBind();
		this.lstStageGroupType.DataBind();
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	private void InitPage() {
		this.pnlInfo.Visible = true;
		this.pnlCastTreasureInfo.Visible = false;

		this.CastTreasureSeq = string.Empty;
		this.pnlKey.Enabled = true;
		this.lstSiteCd.DataBind();
		this.lstStageGroupType.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void ClearFileds() {
		this.lblCastTreasureId.Text = string.Empty;
		this.txtCastTreasureNm.Text = string.Empty;
		this.pnlKey.Enabled = true;
		this.lstStageGroupType.SelectedIndex = 0;
		this.lstStageGroupType.Enabled = true;
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.CastTreasureSeq = string.Empty;

		this.grdCastTreasure.PageIndex = 0;
		this.grdCastTreasure.DataSourceID = "dsCastTreasure";
		this.grdCastTreasure.DataBind();
		this.pnlCount.DataBind();
	}

	private void GetData() {
		this.ClearFileds();

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("CAST_TREASURE_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pSTAGE_GROUP_TYPE", DbSession.DbType.VARCHAR2, this.StageGroupType);
			oDbSession.ProcedureInParm("pCAST_TREASURE_SEQ",DbSession.DbType.VARCHAR2,this.CastTreasureSeq);
			oDbSession.ProcedureOutParm("pCAST_TREASURE_NM",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTOTAL_CAST_POSSESSION_COUNT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.lstStageGroupType.SelectedValue = this.StageGroupType;
			this.lblCastTreasureId.Text = this.CastTreasureSeq;
			this.txtCastTreasureNm.Text = oDbSession.GetStringValue("pCAST_TREASURE_NM");
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
		}
	}

	private void UpdateData(bool pDeleteFlag) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("CAST_TREASURE_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pSTAGE_GROUP_TYPE", DbSession.DbType.VARCHAR2, this.lstStageGroupType.SelectedValue);
			oDbSession.ProcedureBothParm("pCAST_TREASURE_SEQ",DbSession.DbType.VARCHAR2,this.CastTreasureSeq);
			oDbSession.ProcedureInParm("pCAST_TREASURE_NM",DbSession.DbType.VARCHAR2,this.txtCastTreasureNm.Text.TrimEnd());
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDeleteFlag ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.ClearFileds();
		this.GetList();
	}
}
