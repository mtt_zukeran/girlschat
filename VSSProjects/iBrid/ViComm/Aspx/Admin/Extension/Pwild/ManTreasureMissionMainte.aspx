﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ManTreasureMissionMainte.aspx.cs" Inherits="Extension_Pwild_ManTreasureMissionMainte" Title="男性ナンパ練習用お宝" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="男性ナンパ練習用お宝設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" Runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 600px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle2">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton" ValidationGroup="Create"
                    OnClick="btnCreate_Click" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" Visible="false" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlManTreasureMissionInfo">
            <fieldset class="fieldset">
                <legend>[男性ナンパ練習用お宝設定情報]</legend>
                <table border="0" style="width: 600px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
							お宝属性
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstCastGamePicAttr" runat="server" DataSourceID="dsManTreasureAttr"
								DataTextField="CAST_GAME_PIC_ATTR_NM" DataValueField="CAST_GAME_PIC_ATTR_SEQ"
								OnDataBound="lst_DataBound" Width="170px">
							</asp:DropDownList>
							<% // 必須チェック %>
                            <asp:RequiredFieldValidator ID="vdrManTreasureAttr" runat="server" ErrorMessage="お宝属性を選択してください。"
                                ControlToValidate="lstCastGamePicAttr" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                            <ajaxToolkit:ValidatorCalloutExtender ID="vdeManTreasureAttr" runat="Server"
                                TargetControlID="vdrManTreasureAttr" HighlightCssClass="validatorCallout" />
						</td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            コメント
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtPicDoc" runat="server" Width="500px" TextMode="MultiLine" Height="100px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="Panel1" runat="server">
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Update"
                        OnClick="btnUpdate_Click" />
                    <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" CausesValidation="False"
                        Visible="true" OnClick="btnCancel_Click" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[男性ナンパ練習用お宝設定一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdManTreasureMission.PageIndex + 1 %>
                        of
                        <%= grdManTreasureMission.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="600px">
                    <asp:GridView ID="grdManTreasureMission" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        PageSize="30" EnableSortingAndPagingCallbacks="false" DataSourceID="dsManTreasureMission"
                        SkinID="GridViewColor" Font-Size="small" AllowSorting="true" DataKeyNames="SITE_CD,MAN_TREASURE_MISSION_SEQ">
                        <Columns>
							<asp:TemplateField HeaderText="写真">
                                <ItemTemplate>
                                    <asp:Image ID="imgBoss" runat="server" Width="80px" ImageUrl='<%# Eval("OBJ_TUTORIAL_TREASURE_IMG_PATH","../../{0}") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="お宝属性">
                                <ItemTemplate>
                                    <asp:Label ID="lblCastGamePicAttrNm" runat="server" Text='<%# Eval("CAST_GAME_PIC_ATTR_NM", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="コメント">
                                <ItemTemplate>
                                    <asp:Label ID="lblPicDoc" runat="server" Text='<%# ConvertNull2Br(Eval("PIC_DOC")) %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="最終更新日">
                                <ItemTemplate>
                                    <asp:Label ID="lblUpdateDate" runat="server" Text='<%# Eval("UPDATE_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkManTreasureMission" runat="server" CommandArgument='<%# Eval("MAN_TREASURE_MISSION_SEQ") %>'
                                        Text='設定' OnCommand="lnkManTreasureMission_Command"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsManTreasureMission" runat="server" SelectMethod="GetPageCollection"
        ConvertNullToDBNull="true" SortParameterName="" TypeName="ManTreasureMission" SelectCountMethod="GetPageCount"
        EnablePaging="True" OnSelected="dsManTreasureMission_Selected">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsManTreasureAttr" runat="server" SelectMethod="GetList"
        TypeName="ManTreasureAttr">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="男性ナンパ練習用お宝情報を更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>

