﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 男性ナンパ練習用お宝設定

--	Progaram ID		: ManTreasureMissionMainte
--
--  Creation Date	: 2012.09.07
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using ViComm;
using iBridCommLib;
using System.IO;
using ViComm.Extension.Pwild;

public partial class Extension_Pwild_ManTreasureMissionMainte:System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}
	
	private string ManTreasureMissionSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["ManTreasureMissionSeq"]);
		}
		set {
			this.ViewState["ManTreasureMissionSeq"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
		this.ManTreasureMissionSeq = string.Empty;
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.ManTreasureMissionSeq = string.Empty;
		this.ClearFileds();
		this.pnlKey.Enabled = false;
		this.pnlManTreasureMissionInfo.Visible = true;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.UpdateData(false);
		this.pnlManTreasureMissionInfo.Visible = false;
		this.pnlKey.Enabled = true;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.ClearFileds();
		this.pnlKey.Enabled = true;
		this.pnlManTreasureMissionInfo.Visible = false;
	}

	protected void lnkManTreasureMission_Command(object sender,CommandEventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.ManTreasureMissionSeq = iBridUtil.GetStringValue(e.CommandArgument);
		this.GetData();
		this.pnlKey.Enabled = false;
		this.pnlManTreasureMissionInfo.Visible = true;
	}

	protected void dsManTreasureMission_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	protected void lnkQuestId_Command(object sender,CommandEventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.ManTreasureMissionSeq = iBridUtil.GetStringValue(e.CommandArgument);
		this.GetData();
		this.pnlKey.Enabled = false;
		this.pnlManTreasureMissionInfo.Visible = true;
	}

	private void InitPage() {
		this.pnlInfo.Visible = true;
		this.pnlManTreasureMissionInfo.Visible = false;

		ViewState["SITE_CD"] = iBridUtil.GetStringValue(Request.Params["sitecd"]);;

		this.ManTreasureMissionSeq = string.Empty;
		this.pnlKey.Enabled = true;
		this.lstSiteCd.DataBind();
		this.lstCastGamePicAttr.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void ClearFileds() {
		this.txtPicDoc.Text = string.Empty;
		this.lstCastGamePicAttr.SelectedIndex = 0;
		this.pnlKey.Enabled = true;
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.ManTreasureMissionSeq = string.Empty;

		this.grdManTreasureMission.PageIndex = 0;
		this.grdManTreasureMission.DataSourceID = "dsManTreasureMission";
		this.grdManTreasureMission.DataBind();

		this.pnlCount.DataBind();
	}

	private void GetData() {
		this.ClearFileds();

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("MAN_TREASURE_MISSION_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pMAN_TREASURE_MISSION_SEQ",DbSession.DbType.VARCHAR2,this.ManTreasureMissionSeq);
			oDbSession.ProcedureOutParm("pUSER_SEQ",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pCAST_GAME_PIC_ATTR_SEQ",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPIC_DOC",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.lstCastGamePicAttr.SelectedValue = oDbSession.GetStringValue("pCAST_GAME_PIC_ATTR_SEQ");
			this.txtPicDoc.Text = oDbSession.GetStringValue("pPIC_DOC");
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
		}
	}

	private void UpdateData(bool pDelFlag) {
		string sManTreasureMissionSeq = string.Empty;
		
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("MAN_TREASURE_MISSION_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureBothParm("pMAN_TREASURE_MISSION_SEQ",DbSession.DbType.VARCHAR2,this.ManTreasureMissionSeq);
			oDbSession.ProcedureInParm("pCAST_GAME_PIC_ATTR_SEQ",DbSession.DbType.VARCHAR2,this.lstCastGamePicAttr.SelectedValue);
			oDbSession.ProcedureInParm("pPIC_DOC",DbSession.DbType.VARCHAR2,this.txtPicDoc.Text);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
			
			sManTreasureMissionSeq = oDbSession.GetStringValue("pMAN_TREASURE_MISSION_SEQ");
		}

		this.ClearFileds();
		this.GetList();
	}

	protected string ConvertNull2Br(object pText) {
		string sText = iBridUtil.GetStringValue(pText);
		return sText.Replace("\n","<br />");
	}
}
