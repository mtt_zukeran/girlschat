﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: お宝deビンゴ賞金獲得者
--	Progaram ID		: BbsBingoEntryPrizeList
--  Creation Date	: 2013.10.30
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using ViComm.Extension.Pwild;

public partial class Extension_Pwild_BbsBingoEntryPrizeList:System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SITE_CD"]);
		}
		set {
			this.ViewState["SITE_CD"] = value;
		}
	}

	private string TermSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["TERM_SEQ"]);
		}
		set {
			this.ViewState["TERM_SEQ"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			this.TermSeq = iBridUtil.GetStringValue(Request.QueryString["termseq"]);

			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	protected void btnBack_Click(object sender,EventArgs e) {
		this.Response.Redirect(string.Format("~/Extension/Pwild/BbsBingoTermList.aspx?sitecd={0}",this.SiteCd));
		return;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			grdBbsBingoEntryPrize.PageIndex = 0;
			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	protected void dsBbsBingoEntryPrize_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		BbsBingoEntry.SearchCondition oSearchCondition = new BbsBingoEntry.SearchCondition();
		oSearchCondition.SiteCd = this.SiteCd;
		oSearchCondition.TermSeq = this.TermSeq;
		oSearchCondition.PrizeFlag = ViCommConst.FLAG_ON_STR;
		oSearchCondition.LoginId = this.txtLoginId.Text;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsBbsBingoEntryPrize_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected string GetBingoBallFlagStr(object pBingoBallFlag) {
		return (pBingoBallFlag.ToString().Equals(ViCommConst.FLAG_ON_STR)) ? "一発ﾋﾞﾝｺﾞ" : "";
	}

	protected string GetRecCount() {
		return this.recCount;
	}
}
