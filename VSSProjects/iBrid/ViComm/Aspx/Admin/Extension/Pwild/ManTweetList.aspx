﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ManTweetList.aspx.cs" Inherits="Extension_Pwild_ManTweetList" Title="会員つぶやき検索"
    ValidateRequest="false" %>
<%@ Import Namespace="ViComm" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="会員つぶやき検索"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 800px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                ﾛｸﾞｲﾝID
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtLoginId" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                つぶやきSEQ
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtManTweetSeq" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                書込日時From
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstFromYYYY" runat="server" Width="60px">
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstFromMM" runat="server" Width="40px">
                                </asp:DropDownList>月
                                <asp:DropDownList ID="lstFromDD" runat="server" Width="40px">
                                </asp:DropDownList>日
                                <asp:DropDownList ID="lstFromHH" runat="server" Width="40px">
                                </asp:DropDownList>時
                                <asp:DropDownList ID="lstFromMI" runat="server" Width="40px">
                                </asp:DropDownList>分
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                書込日時To
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstToYYYY" runat="server" Width="60px">
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstToMM" runat="server" Width="40px">
                                </asp:DropDownList>月
                                <asp:DropDownList ID="lstToDD" runat="server" Width="40px">
                                </asp:DropDownList>日
                                <asp:DropDownList ID="lstToHH" runat="server" Width="40px">
                                </asp:DropDownList>時
                                <asp:DropDownList ID="lstToMI" runat="server" Width="40px">
                                </asp:DropDownList>分
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle2">
                                キーワード<br />
                                (空白区切り/全てを含む)
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtKeyword" runat="server" Width="300px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" />
                <asp:Button runat="server" ID="btnCsv" Text="ＣＳＶ出力" CssClass="seekbutton" OnClick="btnCSV_Click" />
                <asp:CustomValidator ID="vdcFromTo" runat="server" ErrorMessage="" OnServerValidate="vdcFromTo_ServerValidate"
                    ValidationGroup="Key"></asp:CustomValidator>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[記事一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdManTweet.PageIndex + 1 %>
                        of
                        <%= grdManTweet.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;<br />
				<asp:Button runat="server" ID="btnAdminCheck" Text="選択したものを確認済にする" CssClass="seektopbutton" OnClick="btnAdminCheck_Click" />&nbsp;
				<asp:Button runat="server" ID="btnAdminCheckAll" Text="表示されているものを確認済にする" CssClass="seektopbutton" OnClick="btnAdminCheckAll_Click" />
				<br /><br />
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="443px">
                    <asp:GridView ID="grdManTweet" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        DataSourceID="dsManTweet" SkinID="GridViewColor" AllowSorting="true" OnRowDataBound="grdManTweet_RowDataBound">
                        <Columns>
							<asp:TemplateField HeaderText="確認">
								<ItemTemplate>
									<asp:Label ID="lblAdminChek" runat="server" Text="済" Visible='<%# Eval("ADMIN_CHECK_FLAG").ToString().Equals(ViCommConst.FLAG_ON_STR) ? true : false %>'></asp:Label>
									<asp:CheckBox ID="chkAdminCheck" runat="server" Visible='<%# Eval("ADMIN_CHECK_FLAG").ToString().Equals(ViCommConst.FLAG_OFF_STR) ? true : false %>' />
	                                <asp:HiddenField ID="hdnManTweetSeq" runat="server" Value='<%# Eval("MAN_TWEET_SEQ") %>' />
								</ItemTemplate>
								<ItemStyle HorizontalAlign="center" />
							</asp:TemplateField>
                            <asp:TemplateField HeaderText="書込日時">
                                <ItemTemplate>
                                    <asp:Label ID="lblTweetDate" runat="server" Text='<%# Eval("TWEET_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>'></asp:Label><br />
                                    <asp:LinkButton ID="lnkDel" runat="server" CommandArgument='<%#string.Format("{0},{1},{2}",Eval("SITE_CD"),Eval("MAN_TWEET_SEQ"),Eval("ADMIN_DEL_FLAG")) %>'
                                        OnClientClick="return confirm('削除を実行しますか？');" OnCommand="lnkDelManTweet_Command"
                                        Text='<%# ViCommConst.FLAG_ON_STR.Equals(Eval("ADMIN_DEL_FLAG","{0}")) ? "復活" : "削除" %>'></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ﾛｸﾞｲﾝID">
                                <ItemTemplate><asp:HyperLink ID="lblLoginId" runat="server" NavigateUrl='<%# string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>' Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink></ItemTemplate>
                                <ItemStyle Width="120px" HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ﾊﾝﾄﾞﾙ名">
                                <ItemTemplate>
                                    <asp:Label ID="lblHandleNm" runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="120px" HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="つぶやきSEQ">
                                <ItemTemplate><asp:Label ID="lblManTweetSeq" runat="server" Text='<%# Eval("MAN_TWEET_SEQ") %>'></asp:Label></ItemTemplate>
                                <ItemStyle Width="120px" HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="内容">
                                <ItemTemplate>
                                    <asp:Label ID="lblTweetText" runat="server" Text='<%# Eval("TWEET_TEXT") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="画像">
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl='<%# Eval("MAN_TWEET_SMALL_IMG_PATH", "../../{0}") %>'
                                        OnClientClick='<%# GetClientScript(Eval("MAN_TWEET_IMG_PATH"))  %>' Visible='<%# GetPicVisible(Eval("MAN_TWEET_PIC_SEQ")) %>' />
                                    <br />
                                    <%# ViCommConst.FLAG_ON_STR.Equals(Eval("MAN_TWEET_UNAUTH_PIC_FLAG","{0}")) ? "認証待ち" : "" %>
                                </ItemTemplate>
								<ItemStyle VerticalAlign="Top" HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="コメント数">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkManTweetCommentList" runat="server" NavigateUrl='<%# string.Format("ManTweetCommentList.aspx?sitecd={0}&tweetseq={1}",Eval("SITE_CD"),Eval("MAN_TWEET_SEQ")) %>'
                                        Text='<%# Eval("COMMENT_COUNT") %>' Width="50px"
                                        CssClass="Warp"></asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="right" />
                            </asp:TemplateField>
 							<asp:TemplateField HeaderText="削除">
								<ItemTemplate>
									<%# Eval("ADMIN_DEL_STATUS") %><br />
									<%# Eval("DEL_STATUS") %>
								</ItemTemplate>
								<ItemStyle VerticalAlign="Top" HorizontalAlign="Center" />
							</asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
            <br />
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsManTweet" runat="server" SelectMethod="GetPageCollection" ConvertNullToDBNull="true" SortParameterName=""
        TypeName="ManTweet" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelected="dsManTweet_Selected"
        OnSelecting="dsManTweet_Selecting">
        <SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
</asp:Content>
