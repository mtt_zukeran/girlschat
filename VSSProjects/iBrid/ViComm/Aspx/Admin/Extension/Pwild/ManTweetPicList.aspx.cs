﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 会員つぶやき画像

--	Progaram ID		: ManTweetPicList
--
--  Creation Date	: 2013.03.21
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.Collections.Generic;

public partial class Extension_Pwild_ManTweetPicList:System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string LoginId {
		get {
			return iBridUtil.GetStringValue(this.ViewState["LoginId"]);
		}
		set {
			this.ViewState["LoginId"] = value;
		}
	}
	
	private string UnAuthPicFlag {
		get {
			return iBridUtil.GetStringValue(this.ViewState["UnAuthPicFlag"]);
		}
		set {
			this.ViewState["UnAuthPicFlag"] = value;
		}
	}

	private DataSet ManTweetData {
		get {
			return this.ViewState["ManTweetData"] as DataSet;
		}
		set {
			this.ViewState["ManTweetData"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();

			this.pnlInfo.Visible = true;
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}
	
	protected void btnDelete_OnClick(object sender,EventArgs e) {
		string sSiteCd = (grdManTweet.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][0]).ToString();
		string sManTweetSeq = (grdManTweet.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][1]).ToString();
		string sPicSeq = (grdManTweet.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][2]).ToString();
		DeletePicture(sSiteCd,sManTweetSeq,sPicSeq);
	}

	protected void btnAuth_OnClick(object sender,EventArgs e) {
		string sSiteCd = (grdManTweet.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][0]).ToString();
		string sManTweetSeq = (grdManTweet.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][1]).ToString();
		AuthPicture(sSiteCd,sManTweetSeq);
	}

	protected void dsManTweet_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		DataSet oManTweetDataSet = e.ReturnValue as DataSet;
		if (oManTweetDataSet != null && (oManTweetDataSet).Tables[0].Rows.Count > 0) {
			this.ManTweetData = oManTweetDataSet;
		}
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsManTweet_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		ManTweet.SearchCondition oSearchCondition = new ManTweet.SearchCondition();
		oSearchCondition.LoginId = this.txtLoginId.Text.Trim();
		oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
		oSearchCondition.UnAuthPicFlag = this.Request.QueryString["unauth"];
		e.InputParameters[0] = oSearchCondition;
	}

	private void InitPage() {
		this.recCount = "0";
		this.txtLoginId.Text = string.Empty;
		this.grdManTweet.DataSourceID = string.Empty;

		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		if (!this.IsPostBack) {
			this.SiteCd = this.Request.QueryString["site"];
			this.LoginId = this.Request.QueryString["loginid"];
			this.UnAuthPicFlag = this.Request.QueryString["unauth"];

			if (!string.IsNullOrEmpty(this.SiteCd) && !string.IsNullOrEmpty(this.LoginId)) {
				this.lstSiteCd.SelectedValue = this.SiteCd;
				this.txtLoginId.Text = this.LoginId;

				this.GetList();
			}
		}
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.grdManTweet.PageIndex = 0;
		this.grdManTweet.PageSize = 50;
		this.grdManTweet.DataSourceID = "dsManTweet";
		this.grdManTweet.DataBind();
		this.pnlCount.DataBind();
	}

	protected string GetClientScript(object pPicUrl) {
		string sRoot = Request.Url.Authority + ConfigurationManager.AppSettings["Root"];
		return string.Format("javascript:win=window.open('http://{1}/ViewPic.html?url={0}','PictureZoom','width=300,height=320,resizable=yes,directories=no,scrollbars=no' , false);win.focus();return false;",pPicUrl,sRoot);
	}

	private void DeletePicture(string pSiteCd,string pManTweetSeq,string pPicSeq) {

		if (!pPicSeq.Equals("")) {
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("DELETE_MAN_TWEET_PIC");
				db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
				db.ProcedureInParm("PMAN_TWEET_SEQ",DbSession.DbType.VARCHAR2,pManTweetSeq);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
			}

			DeletePicObj(pSiteCd,pPicSeq);

		}
		DataBind();
	}

	private void DeletePicObj(string pSiteCd,string pPicSeq) {
		string sWebPhisicalDir = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(pSiteCd,"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
		}
		string sFullPath = sWebPhisicalDir + ViCommConst.PIC_DIRECTRY + "\\" + pSiteCd + "\\man\\" + pPicSeq.PadLeft(15,'0') + ViCommConst.PIC_FOODER;
		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
			if (System.IO.File.Exists(sFullPath)) {
				System.IO.File.Delete(sFullPath);
			}
			sFullPath = sWebPhisicalDir + ViCommConst.PIC_DIRECTRY + "\\" + pSiteCd + "\\man\\" + pPicSeq.PadLeft(15,'0') + ViCommConst.PIC_FOODER_SMALL;
			if (System.IO.File.Exists(sFullPath)) {
				System.IO.File.Delete(sFullPath);
			}
		}
	}

	private void AuthPicture(string pSiteCd,string pManTweetSeq) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_MAN_TWEET_PIC_APPROVE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PMAN_TWEET_SEQ",DbSession.DbType.VARCHAR2,pManTweetSeq);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		DataBind();
	}
}
