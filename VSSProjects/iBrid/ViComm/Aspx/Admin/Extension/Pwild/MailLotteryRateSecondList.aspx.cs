/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メールdeガチャ(二次)確立設定一覧
--	Progaram ID		: MailLotteryRateSecondList
--  Creation Date	: 2014.12.29
--  Creater			: Y.Ikemiya
**************************************************************************/
using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;

public partial class Extension_Pwild_MailLotteryRateSecondList:System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SITE_CD"]);
		}
		set {
			this.ViewState["SITE_CD"] = value;
		}
	}

	private string MailLotterySeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MAIL_LOTTERY_SEQ"]);
		}
		set {
			this.ViewState["MAIL_LOTTERY_SEQ"] = value;
		}
	}

	private string MailLotteryRateSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MAIL_LOTTERY_RATE_SEQ"]);
		}
		set {
			this.ViewState["MAIL_LOTTERY_RATE_SEQ"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["REVISION_NO"]);
		}
		set {
			this.ViewState["REVISION_NO"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			this.MailLotterySeq = iBridUtil.GetStringValue(Request.QueryString["maillotteryseq"]);

			this.pnlSearch.Enabled = true;
			this.pnlInput.Visible = false;

			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	protected void btnBack_Click(object sender,EventArgs e) {
		this.Response.Redirect(string.Format("~/Extension/Pwild/MailLotteryMainte.aspx?sitecd={0}",this.SiteCd));
		return;
	}

	protected void dsMailLotteryRate_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		MailLotteryRate.SearchCondition oSearchCondition = new MailLotteryRate.SearchCondition();
		oSearchCondition.SiteCd = this.SiteCd;
		oSearchCondition.MailLotterySeq = this.MailLotterySeq;
		oSearchCondition.SecondLotteryFlag = ViCommConst.FLAG_ON_STR;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsMailLotteryRate_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void grdMailLotteryRate_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
		}
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			grdMailLotteryRate.PageIndex = 0;
			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		this.MailLotteryRateSeq = string.Empty;
		this.RevisionNo = string.Empty;
		this.ClearFileds();

		this.pnlSearch.Enabled = false;
		this.pnlInput.Visible = true;
		this.btnDelete.Enabled = false;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (CheckInput()) {
			string sResult;
			MailLotteryRateMainte(ViCommConst.FLAG_OFF,out sResult);

			this.pnlSearch.Enabled = true;
			this.pnlInput.Visible = false;
			grdMailLotteryRate.PageIndex = 0;
			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlSearch.Enabled = true;
		this.pnlInput.Visible = false;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		string sResult;
		MailLotteryRateMainte(ViCommConst.FLAG_ON,out sResult);

		this.pnlSearch.Enabled = true;
		this.pnlInput.Visible = false;
		grdMailLotteryRate.PageIndex = 0;
		pnlGrid.DataBind();
		pnlCount.DataBind();
	}

	protected void lnkEdit_Command(object sender,CommandEventArgs e) {
		int iIndex = int.Parse(iBridUtil.GetStringValue(e.CommandArgument));
		GridViewRow oRow = this.grdMailLotteryRate.Rows[iIndex];
		HiddenField oHiddenField = oRow.FindControl("hdnMailLotteryRateSeq") as HiddenField;
		this.MailLotteryRateSeq = oHiddenField.Value;

		this.ClearFileds();
		this.MailLotteryRateGet();

		this.pnlSearch.Enabled = false;
		this.pnlInput.Visible = true;
		this.btnDelete.Enabled = true;
	}

	private void ClearFileds() {
		this.lblErrorMessage.Text = string.Empty;
		this.txtMailLotteryRateNm.Text = string.Empty;
		this.txtRewardPoint.Text = string.Empty;
		this.txtRate.Text = string.Empty;
	}

	private bool CheckInput() {

		if (string.IsNullOrEmpty(this.txtMailLotteryRateNm.Text)) {
			this.lblErrorMessage.Text = "名称を入力して下さい";
			return false;
		}

		if (string.IsNullOrEmpty(this.txtRewardPoint.Text)) {
			this.lblErrorMessage.Text = "報酬ptを入力して下さい";
			return false;
		}

		decimal dRate = 0;
		if (string.IsNullOrEmpty(this.txtRate.Text)) {
			this.lblErrorMessage.Text = "確立を入力して下さい";
			return false;
		} else if (!decimal.TryParse(this.txtRate.Text,out dRate)) {
			this.lblErrorMessage.Text = "確立は数値を入力して下さい";
			return false;
		}

		return true;
	}

	private void MailLotteryRateGet() {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("MAIL_LOTTERY_RATE_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pMAIL_LOTTERY_SEQ",DbSession.DbType.VARCHAR2,this.MailLotterySeq);
			oDbSession.ProcedureInParm("pMAIL_LOTTERY_RATE_SEQ",DbSession.DbType.VARCHAR2,this.MailLotteryRateSeq);
			oDbSession.ProcedureOutParm("pMAIL_LOTTERY_RATE_NM",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREWARD_POINT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pRATE",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();

			this.txtMailLotteryRateNm.Text = oDbSession.GetStringValue("pMAIL_LOTTERY_RATE_NM");
			this.txtRewardPoint.Text = oDbSession.GetStringValue("pREWARD_POINT");

			decimal dRate = 0;
			if (decimal.TryParse(oDbSession.GetStringValue("pRATE"),out dRate)) {
				this.txtRate.Text = iBridUtil.GetStringValue(dRate / 100);
			}

			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
		}
	}

	private void MailLotteryRateMainte(int pDeleteFlag,out string pResult) {

		decimal dRate = 0;
		if (decimal.TryParse(this.txtRate.Text,out dRate)) {
			dRate = dRate * 100;
		}
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("MAIL_LOTTERY_RATE_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pMAIL_LOTTERY_SEQ",DbSession.DbType.NUMBER,this.MailLotterySeq);
			oDbSession.ProcedureInParm("pMAIL_LOTTERY_RATE_SEQ",DbSession.DbType.NUMBER,this.MailLotteryRateSeq);
			oDbSession.ProcedureInParm("pMAIL_LOTTERY_RATE_NM",DbSession.DbType.VARCHAR2,this.txtMailLotteryRateNm.Text);
			oDbSession.ProcedureInParm("pREWARD_POINT",DbSession.DbType.NUMBER,this.txtRewardPoint.Text);
			oDbSession.ProcedureInParm("pRATE",DbSession.DbType.NUMBER,dRate.ToString());
			oDbSession.ProcedureInParm("pSECOND_LOTTERY_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_ON);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDeleteFlag);
			oDbSession.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
			pResult = oDbSession.GetStringValue("pRESULT");
		}
	}

	protected string GetRateString(string pRate) {
		string sValue = string.Empty;
		decimal dRate;

		if (decimal.TryParse(pRate,out dRate)) {
			dRate = dRate / 100;
			sValue = iBridUtil.GetStringValue(dRate);
		}

		return sValue;
	}
}