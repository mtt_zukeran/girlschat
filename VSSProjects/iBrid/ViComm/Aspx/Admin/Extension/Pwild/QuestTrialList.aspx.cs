﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 美女コレ クエストクリア条件一覧

--	Progaram ID		: QuestTrialList
--
--  Creation Date	: 2011.07.25
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Ratelain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.IO;
using ViComm.Extension.Pwild;

public partial class Extension_QuestTrialList:System.Web.UI.Page {
	
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		set {
			this.ViewState["SexCd"] = value;
		}
	}

	private string QuestSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["QuestSeq"]);
		}
		set {
			this.ViewState["QuestSeq"] = value;
		}
	}

	private string QuestTrialSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["QuestTrialSeq"]);
		}
		set {
			this.ViewState["QuestTrialSeq"] = value;
		}
	}

	private string LevelQuestSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["LevelQuestSeq"]);
		}
		set {
			this.ViewState["LevelQuestSeq"] = value;
		}
	}

	private string LittleQuestSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["LittleQuestSeq"]);
		}
		set {
			this.ViewState["LittleQuestSeq"] = value;
		}
	}

	private string OtherQuestSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["OtherQuestSeq"]);
		}
		set {
			this.ViewState["OtherQuestSeq"] = value;
		}
	}

	private string QuestType {
		get {
			return iBridUtil.GetStringValue(this.ViewState["QuestType"]);
		}
		set {
			this.ViewState["QuestType"] = value;
		}
	}

	private string TrialCategoryNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["TrialCategoryNo"]);
		}
		set {
			this.ViewState["TrialCategoryNo"] = value;
		}
	}

	private string TrialCategoryDetailNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["TrialCategoryDetailNo"]);
		}
		set {
			this.ViewState["TrialCategoryDetailNo"] = value;
		}
	}

	private string CreateFlag {
		get {
			return iBridUtil.GetStringValue(this.ViewState["CreateFlag"]);
		}
		set {
			this.ViewState["CreateFlag"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();

			this.SiteCd = iBridUtil.GetStringValue(this.Request.QueryString["sitecd"]);
			this.QuestSeq = iBridUtil.GetStringValue(this.Request.QueryString["questseq"]);
			this.LevelQuestSeq = iBridUtil.GetStringValue(this.Request.QueryString["levelquestseq"]);
			this.LittleQuestSeq = iBridUtil.GetStringValue(this.Request.QueryString["littlequestseq"]);
			this.OtherQuestSeq = iBridUtil.GetStringValue(this.Request.QueryString["otherquestseq"]);
			this.QuestType = iBridUtil.GetStringValue(this.Request.QueryString["questtype"]);
			this.SexCd = iBridUtil.GetStringValue(this.Request.QueryString["sexcd"]);
			this.CreateFlag = iBridUtil.GetStringValue(this.Request.QueryString["create"]);
			
			if (!string.IsNullOrEmpty(this.SiteCd) && !string.IsNullOrEmpty(this.QuestTrialSeq)) {
				this.lstSiteCd.SelectedValue = this.SiteCd;

				if (this.CreateFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					this.TrialCategoryNo = string.Empty;
					this.TrialCategoryDetailNo = string.Empty;
					this.pnlKey.Enabled = false;
					this.pnlQuestTrialInfo.Visible = true;
				} else {
					this.GetList();
				}
			}
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.btnDelete.Enabled = true;
		
		this.GetList();
		this.QuestTrialSeq = string.Empty;
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		if (!this.IsCorrectInput()) {
			return;
		}
		if (!this.UpdateData(false)) {
			return;
		}
		this.pnlQuestTrialInfo.Visible = false;
		this.pnlKey.Enabled = true;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		this.UpdateData(true);
		this.pnlQuestTrialInfo.Visible = false;
		this.pnlKey.Enabled = true;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlQuestTrialInfo.Visible = false;
		this.pnlKey.Enabled = true;
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		this.QuestTrialSeq = string.Empty;
		this.ClearFileds();
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.TrialCategoryNo = string.Empty;
		this.TrialCategoryDetailNo = string.Empty;
		this.CreateFlag = ViCommConst.FLAG_ON_STR;
		this.pnlKey.Enabled = false;
		this.btnDelete.Enabled = false;
		this.pnlQuestTrialInfo.Visible = true;
		this.ClearFileds();
	}

	protected void btnBack_Click(object sender,EventArgs e) {
		string sBackPath = string.Empty;

		if (this.QuestType.Equals(PwViCommConst.GameQuestType.LITTLE_QUEST)) {
			sBackPath = string.Format("LittleQuestList.aspx?questseq={0}&levelquestseq={1}&littlequestseq={2}&sexcd={3}",this.QuestSeq,this.LevelQuestSeq,this.LittleQuestSeq,this.SexCd);
		} else if (this.QuestType.Equals(PwViCommConst.GameQuestType.EX_QUEST)) {
			sBackPath = string.Format("LevelQuestList.aspx?questseq={0}&levelquestseq={1}&sexcd={2}",this.QuestSeq,this.LevelQuestSeq,this.SexCd);
		} else if (this.QuestType.Equals(PwViCommConst.GameQuestType.OTHER_QUEST)) {
			sBackPath = string.Format("OtherQuestList.aspx?questseq={0}&otherquestseq={1}&sexcd={2}",this.QuestSeq,this.OtherQuestSeq,this.SexCd);
		}

		this.Response.Redirect(string.Format("~/Extension/Pwild/{0}",sBackPath));
	}

	protected void lnkQuestTrial_Command(object sender,CommandEventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.QuestTrialSeq = iBridUtil.GetStringValue(e.CommandArgument);
		
		this.CreateFlag = string.Empty;
		this.GetData();
		this.pnlKey.Enabled = false;

		this.pnlQuestTrialInfo.Visible = true;
	}

	protected void dsQuestTrial_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsQuestTrialDetail_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pTrialCategoryNo"] = this.TrialCategoryNo;
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	protected void lstQuestTrialCategory_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		this.TrialCategoryNo = oDropDownList.SelectedValue;
		this.lstQuestTrialDetail.DataBind();
	}

	private void InitPage() {
		this.pnlInfo.Visible = true;
		this.pnlQuestTrialInfo.Visible = false;

		this.QuestTrialSeq = string.Empty;
		this.TrialCategoryNo = string.Empty;
		this.TrialCategoryDetailNo = string.Empty;
		this.pnlKey.Enabled = true;
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void ClearFileds() {
		this.txtQuestTrialCount.Text = string.Empty;
		this.txtCountUnit.Text = string.Empty;
		this.lstQuestTrialCategory.DataBind();
		this.lstQuestTrialDetail.DataBind();
		rdoQuestCountStartType.SelectedValue = PwViCommConst.GameQuestCountStartType.QUEST_ENTRY;

		this.lblErrorMessageQuestTrial.Text = string.Empty;
		this.pnlKey.Enabled = true;
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.QuestTrialSeq = string.Empty;

		this.grdQuestTrial.PageIndex = 0;
		this.grdQuestTrial.DataSourceID = "dsQuestTrial";
		this.grdQuestTrial.DataBind();
		this.grdQuestTrial.DataBind();
	}

	private void GetData() {
		this.ClearFileds();

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("QUEST_TRIAL_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pQUEST_SEQ",DbSession.DbType.VARCHAR2,this.QuestSeq);
			oDbSession.ProcedureInParm("pQUEST_TRIAL_SEQ",DbSession.DbType.VARCHAR2,this.QuestTrialSeq);
			oDbSession.ProcedureOutParm("pQUEST_TYPE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pLEVEL_QUEST_SEQ",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pLITTLE_QUEST_SEQ",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pOTHER_QUEST_SEQ",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTRIAL_CATEGORY_NO",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTRIAL_CATEGORY_DETAIL_NO",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pQUEST_TRIAL_COUNT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pCOUNT_UNIT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pQUEST_COUNT_START_TYPE",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
			
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
			this.txtQuestTrialCount.Text = oDbSession.GetStringValue("pQUEST_TRIAL_COUNT");
			this.txtCountUnit.Text = oDbSession.GetStringValue("pCOUNT_UNIT");
			this.rdoQuestCountStartType.SelectedValue = oDbSession.GetStringValue("pQUEST_COUNT_START_TYPE");

			this.lstQuestTrialCategory.SelectedValue = oDbSession.GetStringValue("pTRIAL_CATEGORY_NO");
			this.TrialCategoryDetailNo = oDbSession.GetStringValue("pTRIAL_CATEGORY_DETAIL_NO");
			this.TrialCategoryNo = this.lstQuestTrialCategory.SelectedValue;
			this.lstQuestTrialDetail.DataBind();
			this.lstQuestTrialDetail.SelectedValue = this.TrialCategoryDetailNo;
		}
	}

	private bool UpdateData(bool pDeleteFlag) {

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("QUEST_TRIAL_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureBothParm("pQUEST_SEQ",DbSession.DbType.VARCHAR2,this.QuestSeq);
			oDbSession.ProcedureBothParm("pQUEST_TRIAL_SEQ",DbSession.DbType.VARCHAR2,this.QuestTrialSeq);
			oDbSession.ProcedureInParm("pQUEST_TYPE",DbSession.DbType.VARCHAR2,this.QuestType);
			oDbSession.ProcedureInParm("pLEVEL_QUEST_SEQ",DbSession.DbType.VARCHAR2,this.LevelQuestSeq);
			oDbSession.ProcedureInParm("pLITTLE_QUEST_SEQ",DbSession.DbType.VARCHAR2,this.LittleQuestSeq);
			oDbSession.ProcedureInParm("pOTHER_QUEST_SEQ",DbSession.DbType.VARCHAR2,this.OtherQuestSeq);
			oDbSession.ProcedureInParm("pTRIAL_CATEGORY_NO",DbSession.DbType.VARCHAR2,this.lstQuestTrialCategory.SelectedValue);
			oDbSession.ProcedureInParm("pTRIAL_CATEGORY_DETAIL_NO",DbSession.DbType.VARCHAR2,this.lstQuestTrialDetail.SelectedValue);
			oDbSession.ProcedureInParm("pQUEST_TRIAL_COUNT",DbSession.DbType.NUMBER,this.txtQuestTrialCount.Text);
			oDbSession.ProcedureInParm("pCOUNT_UNIT",DbSession.DbType.VARCHAR2,this.txtCountUnit.Text);
			oDbSession.ProcedureInParm("pQUEST_COUNT_START_TYPE",DbSession.DbType.NUMBER,this.rdoQuestCountStartType.SelectedValue);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDeleteFlag ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.ClearFileds();
		this.GetList();
		return true;
	}

	private bool IsCorrectInput() {
		this.lblErrorMessageQuestTrial.Visible = false;
		bool bResult = true;

		if (string.IsNullOrEmpty(this.lstQuestTrialCategory.SelectedValue)) {
			this.lblErrorMessageQuestTrial.Text = "クリア条件カテゴリを選択してください。";
			this.lblErrorMessageQuestTrial.Visible = true;
			bResult = false;
		} else if (string.IsNullOrEmpty(this.lstQuestTrialDetail.SelectedValue)) {
			this.lblErrorMessageQuestTrial.Text = "クリア条件を選択してください。";
			this.lblErrorMessageQuestTrial.Visible = true;
			bResult = false;
		}

		if (string.IsNullOrEmpty(this.txtQuestTrialCount.Text)) {
			this.txtQuestTrialCount.Text = "0";
		}

		return bResult;
	}
}
