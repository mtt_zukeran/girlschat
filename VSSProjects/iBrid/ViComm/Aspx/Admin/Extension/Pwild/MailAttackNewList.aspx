﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MailAttackNewList.aspx.cs" Inherits="Extension_Pwild_MailAttackNewList" Title="新人メールアタック設定" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="新人メールアタック設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlNotes">
			<fieldset class="fieldset">
				<legend>[注意事項]</legend>
				<asp:Panel ID="pnlNoteDtl" runat="server">
					<div>登録24時間以内　返信イベント用</div>
					<br />
					<div>ユーザー定義ポイント設定が必要</div>
					<div>MAIL_ATTACK_NEW</div>
					<div>MAIL_ATTACK_NEW_MOVIE</div>
					<div>MAIL_ATTACK_NEW_PIC</div>
					<br />
					<div>設定方法</div>
					<div>登録経過日数とイベント開催期間の設定をします。</div>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlSearch">
			<fieldset class="fieldset">
				<legend>[検索]</legend>
				<asp:Panel ID="pnlSeekCondition" runat="server">
					<table border="0" style="width: 800px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle2">
								サイトコード
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px">
								</asp:DropDownList>
							</td>
						</tr>
					</table>
				</asp:Panel>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Seek" />
				<asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton"  OnClick="btnCreate_Click" ValidationGroup="Create" />
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlInput">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Label ID="lblErrorMessage" runat="server" Text="" ForeColor="red"></asp:Label>
				<table border="0" style="width: 650px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							会員登録後経過日数
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtAfterRegistDays" runat="server" Width="40px"></asp:TextBox>
							<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtAfterRegistDays" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							開始日時
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstStartDateYYYY" runat="server" Width="60px" DataSource='<%# YearArray %>'>
							</asp:DropDownList>年
							<asp:DropDownList ID="lstStartDateMM" runat="server" Width="40px" DataSource='<%# MonthArray %>'>
							</asp:DropDownList>月
							<asp:DropDownList ID="lstStartDateDD" runat="server" Width="40px" DataSource='<%# DayArray %>'>
							</asp:DropDownList>日
							<asp:DropDownList ID="lstStartDateHH" runat="server" Width="40px" DataSource='<%# HourArray %>'>
							</asp:DropDownList>時
							<asp:DropDownList ID="lstStartDateMI" runat="server" DataSource='<%# MinuteArray %>'>
							</asp:DropDownList>分
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							終了日時
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstEndDateYYYY" runat="server" Width="60px" DataSource='<%# YearArray %>'>
							</asp:DropDownList>年
							<asp:DropDownList ID="lstEndDateMM" runat="server" Width="40px" DataSource='<%# MonthArray %>'>
							</asp:DropDownList>月							<asp:DropDownList ID="lstEndDateDD" runat="server" Width="40px" DataSource='<%# DayArray %>'>
							</asp:DropDownList>日
							<asp:DropDownList ID="lstEndDateHH" runat="server" Width="40px" DataSource='<%# HourArray %>'>
							</asp:DropDownList>時							<asp:DropDownList ID="lstEndDateMI" runat="server" DataSource='<%# MinuteArray %>'>
							</asp:DropDownList>分
						</td>
					</tr>
				</table>
				<asp:Panel ID="pnlCommand" runat="server" Width="660px">
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Update" />
					<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" />
					<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="delBtnStyle" OnClick="btnDelete_Click" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlList">
			<fieldset>
				<legend>[一覧]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">
						Record Count <%# GetRecCount() %>
					</a>
					<br />
					<a class="reccount">
						Current viewing page <%= grdMailAttackNew.PageIndex + 1%> of <%= grdMailAttackNew.PageCount%>
					</a>
				</asp:Panel>
				&nbsp;
				<asp:Panel ID="pnlGrid" runat="server">
					<asp:GridView ID="grdMailAttackNew" DataSourceID="dsMailAttackNew" runat="server" AllowPaging="True" AutoGenerateColumns="False" PageSize="30" EnableSortingAndPagingCallbacks="false" SkinID="GridViewColor" AllowSorting="true">
						<Columns>
							<asp:TemplateField>
								<ItemTemplate>
									<asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%# Container.DataItemIndex %>' OnCommand="lnkEdit_Command" Text='<%# Eval("MAIL_ATTACK_NEW_SEQ") %>'></asp:LinkButton>
									<asp:HiddenField ID="hdnMailAttackNewSeq" runat="server" Value='<%# Eval("MAIL_ATTACK_NEW_SEQ") %>' />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="AFTER_REGIST_DAYS" HeaderText="会員登録後経過日数">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:BoundField DataField="START_DATE" HeaderText="開始日時" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
								<ItemStyle HorizontalAlign="Left" Wrap="false" />
							</asp:BoundField>
							<asp:BoundField DataField="END_DATE" HeaderText="終了日時" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
								<ItemStyle HorizontalAlign="Left" Wrap="false" />
							</asp:BoundField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
			<br />
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsMailAttackNew" runat="server" ConvertNullToDBNull="false" EnablePaging="True" OnSelecting="dsMailAttackNew_Selecting" OnSelected="dsMailAttackNew_Selected"
		SelectCountMethod="GetPageCount" SelectMethod="GetPageCollection" SortParameterName="" TypeName="MailAttackNew">
		<SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>