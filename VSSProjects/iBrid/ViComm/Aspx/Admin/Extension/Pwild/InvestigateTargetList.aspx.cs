﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: この娘を探せ・女性ピックアップ

--	Progaram ID		: InvestigateTargetList
--
--  Creation Date	: 2015.02.26
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using iBridCommLib;
using ViComm;

public partial class Extension_Pwild_InvestigateTargetList:System.Web.UI.Page {
	private string recCount = string.Empty;

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();

			string sSiteCd = this.Request.QueryString["sitecd"];
			if (!string.IsNullOrEmpty(sSiteCd)) {
				this.lstSeekSiteCd.SelectedValue = sSiteCd;
				this.pnlInfo.Visible = true;
				this.GetList();
			} else {
				this.pnlInfo.Visible = true;
				this.GetList();
			}
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		this.pnlInfo.Visible = true;
		this.GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void lnkDelete_Command(object sender,CommandEventArgs e) {
		if (!e.CommandName.Equals("DELETE")) {
			return;
		}

		int iRowIndex = int.Parse(e.CommandArgument.ToString());
		IDictionary oDataKeys = this.grdInvestigateTarget.DataKeys[iRowIndex].Values;

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("DELETE_INVESTIGATE_TARGET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.lstSeekSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pEXECUTION_DAY",DbSession.DbType.VARCHAR2,oDataKeys["EXECUTION_DAY"]);
			oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,oDataKeys["USER_SEQ"]);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,oDataKeys["USER_CHAR_NO"]);
			oDbSession.ProcedureInParm("pINVESTIGATE_TARGET_SEQ",DbSession.DbType.VARCHAR2,oDataKeys["INVESTIGATE_TARGET_SEQ"]);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.GetList();
	}

	protected void grdInvestigateTarget_DataBound(object sender,EventArgs e) {
		this.JoinCells(this.grdInvestigateTarget,0);
	}

	protected void grdInvestigateTarget_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType != DataControlRowType.DataRow) {
			return;
		}

		string sExecutionDay = iBridUtil.GetStringValue(DataBinder.Eval(e.Row.DataItem,"EXECUTION_DAY"));

		DateTime oExecutionDay = DateTime.Parse(sExecutionDay);
		if (DateTime.Today >= oExecutionDay) {
			LinkButton oDeleteLink = e.Row.FindControl("lnkDelete") as LinkButton;
			if (oDeleteLink != null) {
				oDeleteLink.Enabled = false;
			}
		}
	}

	protected void dsInvestigateTarget_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsInvestigateTarget_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		InvestigateTarget.SearchCondition oSearchCondition = new InvestigateTarget.SearchCondition();
		oSearchCondition.SiteCd = this.lstSeekSiteCd.SelectedValue;
		oSearchCondition.AccessDay = DateTime.Now.ToString("yyyy/MM/dd");

		e.InputParameters[0] = oSearchCondition;
	}

	private void InitPage() {
		this.recCount = "0";
		this.chkPagingOff.Checked = false;
		this.grdInvestigateTarget.DataSourceID = string.Empty;
		this.pnlInfo.Visible = false;
		this.lstSeekSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	protected string GetRecCount() {
		return recCount;
	}

	private void GetList() {
		this.grdInvestigateTarget.PageIndex = 0;
		if (chkPagingOff.Checked) {
			this.grdInvestigateTarget.PageSize = 999999;
		} else {
			this.grdInvestigateTarget.PageSize = 27;
		}

		this.grdInvestigateTarget.DataSourceID = "dsInvestigateTarget";
		this.grdInvestigateTarget.DataBind();
		this.pnlCount.DataBind();
	}

	protected string GetDisplayComment(object pComment) {
		string sComment = iBridUtil.GetStringValue(pComment);
		if (sComment.Length > 10) {
			return string.Concat(SysPrograms.Substring(sComment,9),"…");
		} else {
			return sComment;
		}
	}

	protected Color GetDayOfWeekColor(object pDayOfWeek) {
		switch (iBridUtil.GetStringValue(pDayOfWeek)) {
			case "土":
				return Color.Blue;
			case "日":
				return Color.Red;
			default:
				return Color.Empty;
		}
	}

	protected string GetPicCountMark(object pPicCount) {
		int iPicCount;
		if (int.TryParse(iBridUtil.GetStringValue(pPicCount),out iPicCount)) {
			return string.Format("{0}枚",iPicCount);
		} else {
			return "0枚";
		}
	}

	private void JoinCells(GridView pGrd,int pCol) {
		int iRow = pGrd.Rows.Count;
		int iBaseidx = 0;

		while (iBaseidx < iRow) {
			int iNextIdx = iBaseidx + 1;
			TableCell celBase = pGrd.Rows[iBaseidx].Cells[pCol];

			while (iNextIdx < iRow) {

				TableCell celNext = pGrd.Rows[iNextIdx].Cells[pCol];

				if (this.GetText(celBase).Equals(this.GetText(celNext))) {
					if (celBase.RowSpan == 0) {
						celBase.RowSpan = 2;
					} else {
						celBase.RowSpan++;
					}
					celNext.Visible = false;
					iNextIdx++;
				} else {
					break;
				}
			}
			iBaseidx = iNextIdx;
		}
	}

	private string GetText(TableCell tc) {
		return ((Label)tc.FindControl("lblDay")).Text;
	}
}
