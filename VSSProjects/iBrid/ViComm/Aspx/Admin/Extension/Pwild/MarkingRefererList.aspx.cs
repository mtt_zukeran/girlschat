﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 足あとリンク元設定--	Progaram ID		: MarkingRefererList
--  Creation Date	: 2015.07.24
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using iBridCommLib;
using ViComm;

public partial class Extension_Pwild_MarkingRefererList:System.Web.UI.Page {
	private int recCount = 0;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string MarkingRefererSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MarkingRefererSeq"]);
		}
		set {
			this.ViewState["MarkingRefererSeq"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.txtAspxNm.Style.Add("ime-mode","disabled");

		if (!this.IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		ClearFields();
		pnlKey.Enabled = true;
		pnlMainte.Visible = false;

		grdData.DataSourceID = "dsData";
		grdData.PageIndex = 0;
		pnlGrid.DataBind();
		pnlCount.DataBind();
	}

	private void ClearFields() {
		this.lblErrorMessage.Text = string.Empty;
		this.txtAspxNm.Text = string.Empty;
		this.txtDisplayNm.Text = string.Empty;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		grdData.PageIndex = 0;
		pnlGrid.DataBind();
		pnlCount.DataBind();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		this.SiteCd = lstSiteCd.SelectedValue;
		this.MarkingRefererSeq = string.Empty;
		this.RevisionNo = string.Empty;
		ClearFields();
		pnlKey.Enabled = false;
		btnDelete.Enabled = false;
		pnlMainte.Visible = true;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void lnkEdit_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		this.SiteCd = sKeys[0];
		this.MarkingRefererSeq = sKeys[1];
		btnDelete.Enabled = true;
		ClearFields();
		GetData();
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MARKING_REFERER_GET");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			db.ProcedureInParm("pMARKING_REFERER_SEQ",DbSession.DbType.NUMBER,int.Parse(this.MarkingRefererSeq));
			db.ProcedureOutParm("pASPX_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pDISPLAY_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			this.txtAspxNm.Text = db.GetStringValue("pASPX_NM");
			this.txtDisplayNm.Text = db.GetStringValue("pDISPLAY_NM");
			this.RevisionNo = db.GetIntValue("pREVISION_NO").ToString();
		}

		pnlMainte.Visible = true;
		pnlKey.Enabled = false;
	}

	private void UpdateData(int pDelFlag) {
		this.lblErrorMessage.Text = string.Empty;
		string sResult = "0";

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MARKING_REFERER_MAINTE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			db.ProcedureInParm("pMARKING_REFERER_SEQ",DbSession.DbType.NUMBER,this.MarkingRefererSeq);
			db.ProcedureInParm("pASPX_NM",DbSession.DbType.VARCHAR2,txtAspxNm.Text.Trim());
			db.ProcedureInParm("pDISPLAY_NM",DbSession.DbType.VARCHAR2,txtDisplayNm.Text.Trim());
			db.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			db.ProcedureInParm("pDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sResult = db.GetStringValue("pRESULT");
		}

		if (sResult.Equals("0")) {
			InitPage();
		} else {
			this.lblErrorMessage.Text = "入力されたASPXファイル名は既に登録済みです";
		}
	}

	protected int GetRecCount() {
		return this.recCount;
	}

	protected void dsData_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		MarkingReferer.SearchCondition oSearchCondition = new MarkingReferer.SearchCondition();
		oSearchCondition.SiteCd = lstSiteCd.SelectedValue;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsData_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null && !(e.ReturnValue is DataSet)) {
			recCount = int.Parse(iBridUtil.GetStringValue(e.ReturnValue));
		}
	}
}
