﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: クエスト一覧

--	Progaram ID		: QuestList
--
--  Creation Date	: 2012.07.05
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.IO;
using ViComm.Extension.Pwild;

public partial class Extension_Pwild_QuestList:System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		set {
			this.ViewState["SexCd"] = value;
		}
	}

	private string QuestSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["QuestSeq"]);
		}
		set {
			this.ViewState["QuestSeq"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();
			this.SexCd = iBridUtil.GetStringValue(this.Request.QueryString["sexcd"]);
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
		this.QuestSeq = string.Empty;
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.QuestSeq = string.Empty;
		this.ClearFileds();
		this.pnlKey.Enabled = false;
		this.btnDelete.Enabled = false;
		this.pnlQuestInfo.Visible = true;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		if (!this.IsCorrectDate()) {
			return;
		}
		this.UpdateData(false);
		this.pnlQuestInfo.Visible = false;
		this.btnDelete.Enabled = true;
		this.pnlKey.Enabled = true;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		this.UpdateData(true);
		this.pnlQuestInfo.Visible = false;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.ClearFileds();
		this.pnlKey.Enabled = true;
		this.pnlQuestInfo.Visible = false;
	}

	protected void lnkQuest_Command(object sender,CommandEventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.QuestSeq = iBridUtil.GetStringValue(e.CommandArgument);
		this.GetData();
		this.btnDelete.Enabled = true;
		this.pnlKey.Enabled = false;
		this.pnlQuestInfo.Visible = true;
	}

	protected void dsQuest_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	protected void lnkQuestId_Command(object sender,CommandEventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.QuestSeq = iBridUtil.GetStringValue(e.CommandArgument);
		this.GetData();
		this.pnlKey.Enabled = false;
		this.pnlQuestInfo.Visible = true;
	}

	private void InitPage() {
		this.pnlInfo.Visible = true;
		this.pnlQuestInfo.Visible = false;

		this.QuestSeq = string.Empty;
		this.pnlKey.Enabled = true;
		this.btnDelete.Enabled = true;
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void ClearFileds() {
		this.lblQuestId.Text = string.Empty;
		this.txtQuestNm.Text = string.Empty;
		this.txtGetRewardEndDate.Text = string.Empty;
		this.txtPublishStartDate.Text = string.Empty;
		this.txtPublishEndDate.Text = string.Empty;
		this.chkPublishFlag.Checked = true;

		this.lblErrorMessageRewardEnd.Visible = false;
		this.lblErrorMessagePublishEnd.Visible = false;
		this.lblErrorMessagePublishStart.Visible = false;
		this.lblErrorMessageRewardEnd.Text = string.Empty;
		this.lblErrorMessagePublishEnd.Text = string.Empty;
		this.lblErrorMessagePublishStart.Text = string.Empty;
		this.pnlKey.Enabled = true;
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.QuestSeq = string.Empty;

		this.grdQuest.PageIndex = 0;
		this.grdQuest.DataSourceID = "dsQuest";
		this.grdQuest.DataBind();

		this.pnlCount.DataBind();
	}

	private void GetData() {
		this.ClearFileds();

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("QUEST_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pQUEST_SEQ",DbSession.DbType.VARCHAR2,this.QuestSeq);
			oDbSession.ProcedureOutParm("pQUEST_NM",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSEX_CD",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pGET_REWARD_END_DATE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPUBLISH_START_DATE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPUBLISH_END_DATE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPUBLISH_FLAG",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.lblQuestId.Text = oDbSession.GetStringValue("pQUEST_SEQ");
			this.txtQuestNm.Text = oDbSession.GetStringValue("pQUEST_NM");
			this.txtGetRewardEndDate.Text = oDbSession.GetStringValue("pGET_REWARD_END_DATE");
			this.txtPublishStartDate.Text = oDbSession.GetStringValue("pPUBLISH_START_DATE");
			this.txtPublishEndDate.Text = oDbSession.GetStringValue("pPUBLISH_END_DATE");
			this.chkPublishFlag.Checked = ViCommConst.FLAG_ON_STR.Equals(oDbSession.GetStringValue("pPUBLISH_FLAG"));
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
		}
	}

	protected string GetPublishFlagMark(object pPublishFlag) {
		string sPublishFlag = iBridUtil.GetStringValue(pPublishFlag);

		return ViCommConst.FLAG_ON_STR.Equals(sPublishFlag) ? "公開中" : "非公開中";
	}

	private void UpdateData(bool pDelFlag) {
		DateTime? dtPublishStart = SysPrograms.TryParseOrDafult(this.txtPublishStartDate.Text,(DateTime?)null);
		DateTime? dtPublishEnd = SysPrograms.TryParseOrDafult(this.txtPublishEndDate.Text,(DateTime?)null);
		DateTime? dtGetRewardEnd = SysPrograms.TryParseOrDafult(this.txtGetRewardEndDate.Text,(DateTime?)null);

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("QUEST_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureBothParm("pQUEST_SEQ",DbSession.DbType.VARCHAR2,this.QuestSeq);
			oDbSession.ProcedureInParm("pQUEST_NM",DbSession.DbType.VARCHAR2,this.txtQuestNm.Text);
			oDbSession.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,this.SexCd);
			oDbSession.ProcedureInParm("pGET_REWARD_END_DATE",DbSession.DbType.DATE,dtGetRewardEnd);
			oDbSession.ProcedureInParm("pPUBLISH_START_DATE",DbSession.DbType.DATE,dtPublishStart);
			oDbSession.ProcedureInParm("pPUBLISH_END_DATE",DbSession.DbType.DATE,dtPublishEnd);
			oDbSession.ProcedureInParm("pPUBLISH_FLAG",DbSession.DbType.VARCHAR2,this.chkPublishFlag.Checked ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.ClearFileds();
		this.GetList();
	}

	private bool IsCorrectDate() {
		this.lblErrorMessageRewardEnd.Visible = false;
		this.lblErrorMessagePublishEnd.Visible = false;
		this.lblErrorMessagePublishStart.Visible = false;
		this.lblErrorMessageRewardEnd.Text = string.Empty;
		this.lblErrorMessagePublishEnd.Text = string.Empty;
		this.lblErrorMessagePublishStart.Text = string.Empty;

		DateTime? dtPublishStart = SysPrograms.TryParseOrDafult(this.txtPublishStartDate.Text,(DateTime?)null);
		DateTime? dtPublishEnd = SysPrograms.TryParseOrDafult(this.txtPublishEndDate.Text,(DateTime?)null);
		DateTime? dtGetRewardEnd = SysPrograms.TryParseOrDafult(this.txtGetRewardEndDate.Text,(DateTime?)null);

		if (!dtPublishStart.HasValue) {
			this.lblErrorMessagePublishStart.Visible = true;
			this.lblErrorMessagePublishStart.Text = "公開開始日時を正しく入力してください。";
			return false;
		}
		if (!dtPublishEnd.HasValue) {
			this.lblErrorMessagePublishEnd.Visible = true;
			this.lblErrorMessagePublishEnd.Text = "公開終了日時を正しく入力してください。";
			return false;
		}
		if (dtPublishEnd < dtPublishStart) {
			this.lblErrorMessagePublishEnd.Visible = true;
			this.lblErrorMessagePublishEnd.Text = "公開期間の大小関係が不正です。";
			return false;
		}
		if (!dtGetRewardEnd.HasValue) {
			this.lblErrorMessageRewardEnd.Visible = true;
			this.lblErrorMessageRewardEnd.Text = "報酬受取期限を正しく入力してください。";
			return false;
		}
		if (dtGetRewardEnd.HasValue) {
			if (dtGetRewardEnd < dtPublishStart) {
				this.lblErrorMessageRewardEnd.Visible = true;
				this.lblErrorMessageRewardEnd.Text = "公開期間と報酬受取期限の大小関係が不正です";
				return false;
			}
			if (dtGetRewardEnd < dtPublishEnd) {
				this.lblErrorMessageRewardEnd.Visible = true;
				this.lblErrorMessageRewardEnd.Text = "公開期間と報酬受取期限の大小関係が不正です";
				return false;
			}
		}

		return true;
	}
}
