/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: プレゼントメールアイテム一覧
--	Progaram ID		: PresentMailItemList
--  Creation Date	: 2014.12.01
--  Creater			: Y.Ikemiya
**************************************************************************/
using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;

public partial class Extension_Pwild_PresentMailItemList:System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SITE_CD"]);
		}
		set {
			this.ViewState["SITE_CD"] = value;
		}
	}

	private string PresentMailTermSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["PRESENT_MAIL_TERM_SEQ"]);
		}
		set {
			this.ViewState["PRESENT_MAIL_TERM_SEQ"] = value;
		}
	}

	private string PresentMailItemSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["PRESENT_MAIL_ITEM_SEQ"]);
		}
		set {
			this.ViewState["PRESENT_MAIL_ITEM_SEQ"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["REVISION_NO"]);
		}
		set {
			this.ViewState["REVISION_NO"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			this.PresentMailTermSeq = iBridUtil.GetStringValue(Request.QueryString["presentmailtermseq"]);

			this.pnlSearch.Enabled = true;
			this.pnlInput.Visible = false;

			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	protected void btnBack_Click(object sender,EventArgs e) {
		this.Response.Redirect(string.Format("~/Extension/Pwild/PresentMailTermList.aspx?sitecd={0}",this.SiteCd));
		return;
	}

	protected void dsPresentMailItem_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		PresentMailItem.SearchCondition oSearchCondition = new PresentMailItem.SearchCondition();
		oSearchCondition.SiteCd = this.SiteCd;
		oSearchCondition.PresentMailTermSeq = this.PresentMailTermSeq;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsPresentMailItem_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void grdPresentMailItem_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
		}
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			grdPresentMailItem.PageIndex = 0;
			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		this.PresentMailItemSeq = string.Empty;
		this.RevisionNo = string.Empty;
		this.ClearFileds();

		this.pnlSearch.Enabled = false;
		this.pnlInput.Visible = true;
		this.btnDelete.Enabled = false;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (CheckInput()) {
			string sResult;
			PresentMailItemMainte(ViCommConst.FLAG_OFF,out sResult);

			this.pnlSearch.Enabled = true;
			this.pnlInput.Visible = false;
			grdPresentMailItem.PageIndex = 0;
			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlSearch.Enabled = true;
		this.pnlInput.Visible = false;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		string sResult;
		PresentMailItemMainte(ViCommConst.FLAG_ON,out sResult);

		this.pnlSearch.Enabled = true;
		this.pnlInput.Visible = false;
		grdPresentMailItem.PageIndex = 0;
		pnlGrid.DataBind();
		pnlCount.DataBind();
	}

	protected void lnkEdit_Command(object sender,CommandEventArgs e) {
		int iIndex = int.Parse(iBridUtil.GetStringValue(e.CommandArgument));
		GridViewRow oRow = this.grdPresentMailItem.Rows[iIndex];
		HiddenField oHiddenField = oRow.FindControl("hdnPresentMailItemSeq") as HiddenField;
		this.PresentMailItemSeq = oHiddenField.Value;

		this.ClearFileds();
		this.PresentMailItemGet();

		this.pnlSearch.Enabled = false;
		this.pnlInput.Visible = true;
		this.btnDelete.Enabled = true;
	}

	private void ClearFileds() {
		this.lblErrorMessage.Text = string.Empty;
		this.txtPresentMailItemNm.Text = string.Empty;
		this.txtChargePoint.Text = string.Empty;
		this.txtRewardPoint.Text = string.Empty;
	}

	private bool CheckInput() {

		if (string.IsNullOrEmpty(this.txtPresentMailItemNm.Text)) {
			this.lblErrorMessage.Text = "名称を入力して下さい";
			return false;
		}
		
		if (string.IsNullOrEmpty(this.txtChargePoint.Text)) {
			this.lblErrorMessage.Text = "価格を入力して下さい";
			return false;
		}
		
		if (string.IsNullOrEmpty(this.txtRewardPoint.Text)) {
			this.lblErrorMessage.Text = "報酬ptを入力して下さい";
			return false;
		}

		return true;
	}

	private void PresentMailItemGet() {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("PRESENT_MAIL_ITEM_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pPRESENT_MAIL_TERM_SEQ",DbSession.DbType.VARCHAR2,this.PresentMailTermSeq);
			oDbSession.ProcedureInParm("pPRESENT_MAIL_ITEM_SEQ",DbSession.DbType.VARCHAR2,this.PresentMailItemSeq);
			oDbSession.ProcedureOutParm("pPRESENT_MAIL_ITEM_NM",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pCHARGE_POINT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pREWARD_POINT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();

			this.txtPresentMailItemNm.Text = oDbSession.GetStringValue("pPRESENT_MAIL_ITEM_NM");
			this.txtChargePoint.Text = oDbSession.GetStringValue("pCHARGE_POINT");
			this.txtRewardPoint.Text = oDbSession.GetStringValue("pREWARD_POINT");
			
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
		}
	}

	private void PresentMailItemMainte(int pDeleteFlag,out string pResult) {

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("PRESENT_MAIL_ITEM_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pPRESENT_MAIL_TERM_SEQ",DbSession.DbType.NUMBER,this.PresentMailTermSeq);
			oDbSession.ProcedureInParm("pPRESENT_MAIL_ITEM_SEQ",DbSession.DbType.NUMBER,this.PresentMailItemSeq);
			oDbSession.ProcedureInParm("pPRESENT_MAIL_ITEM_NM",DbSession.DbType.VARCHAR2,this.txtPresentMailItemNm.Text);
			oDbSession.ProcedureInParm("pCHARGE_POINT",DbSession.DbType.NUMBER,this.txtChargePoint.Text);
			oDbSession.ProcedureInParm("pREWARD_POINT",DbSession.DbType.NUMBER,this.txtRewardPoint.Text);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDeleteFlag);
			oDbSession.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
			pResult = oDbSession.GetStringValue("pRESULT");
		}
	}
}