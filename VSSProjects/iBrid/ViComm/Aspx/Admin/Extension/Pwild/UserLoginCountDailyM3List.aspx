﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UserLoginCountDailyM3List.aspx.cs" Inherits="Extension_Pwild_UserLoginCountDailyM3List"
	Title="課金者ログイン集計一覧(MULLER3)" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="課金者ログイン集計一覧(MULLER3)"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							<asp:Label ID="lblReportMonth" runat="server" Text="報告年月"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstYYYY" runat="server" Width="54px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstMM" runat="server" Width="43px">
								<asp:ListItem Value="01">01</asp:ListItem>
								<asp:ListItem Value="02">02</asp:ListItem>
								<asp:ListItem Value="03">03</asp:ListItem>
								<asp:ListItem Value="04">04</asp:ListItem>
								<asp:ListItem Value="05">05</asp:ListItem>
								<asp:ListItem Value="06">06</asp:ListItem>
								<asp:ListItem Value="07">07</asp:ListItem>
								<asp:ListItem Value="08">08</asp:ListItem>
								<asp:ListItem Value="09">09</asp:ListItem>
								<asp:ListItem Value="10">10</asp:ListItem>
								<asp:ListItem Value="11">11</asp:ListItem>
								<asp:ListItem Value="12">12</asp:ListItem>
							</asp:DropDownList>月
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							<asp:Label ID="lblSinceLastLoginDays" runat="server" Text="ログイン"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSinceLastLoginDays" runat="server">
								<asp:ListItem Value="1">前日</asp:ListItem>
								<asp:ListItem Value="2">2日前</asp:ListItem>
								<asp:ListItem Value="3">3日前</asp:ListItem>
								<asp:ListItem Value="4">4日前</asp:ListItem>
								<asp:ListItem Value="5">5日前</asp:ListItem>
								<asp:ListItem Value="6">6日前</asp:ListItem>
								<asp:ListItem Value="7">1週間以上前</asp:ListItem>
							</asp:DropDownList>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
                <asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" CausesValidation="False"
                    OnClick="btnCSV_Click" />
			</asp:Panel>
		</fieldset>
		<fieldset>
			<legend>[集計]</legend>
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="700px">
				<asp:GridView ID="grdUserLoginCountDailyM3" runat="server" AutoGenerateColumns="False" DataSourceID="dsUserLoginCountDailyM3" OnRowDataBound="grdUserLoginCountDailyM3_RowDataBound"
					SkinID="GridViewFreeRowStyle">
					<Columns>
						<asp:TemplateField HeaderText="日付">
							<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" CssClass="RowStyleNoPad" />
							<ItemTemplate>
								<asp:Label ID="Label1" runat="server" Text='<%# Eval("DISPLAY_DAY") %>' BackColor='<%# GetBackColor(Eval("REPORT_DAY_OF_WEEK")) %>' Width="38px"></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="曜日">
							<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" CssClass="RowStyleNoPad" />
							<ItemTemplate>
								<asp:Label ID="Label1" runat="server" Text='<%# Eval("REPORT_DAY_OF_WEEK") %>' BackColor='<%# GetBackColor(Eval("REPORT_DAY_OF_WEEK")) %>' Width="38px"></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="正常">
							<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Font-Size="X-Small" />
							<ItemTemplate>
								<asp:Label ID="lblConviniCount" runat="server" Text='<%# Eval("START_MULLER3_OK") %>' Width="50px"></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ﾌｨﾙﾀﾘﾝｸﾞ<br>ｴﾗｰ">
							<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Font-Size="X-Small" />
							<ItemTemplate>
								<asp:Label ID="lblConviniAmt" runat="server" Text='<%# Eval("START_MULLER3_NG") %>' Width="50px"></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="合計">
							<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Font-Size="X-Small" BackColor="lightyellow" />
							<ItemTemplate>
								<asp:Label ID="lblConviniAmt" runat="server" Text='<%# Eval("TOTAL_START_COUNT") %>' Width="50px"></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="正常">
							<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Font-Size="X-Small" BackColor="#EFF3FB" />
							<ItemTemplate>
								<asp:Label ID="lblConviniCount" runat="server" Text='<%# Eval("MULLER3_OK_OK") %>' Width="50px"></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ﾌｨﾙﾀﾘﾝｸﾞ<br>ｴﾗｰ">
							<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Font-Size="X-Small" BackColor="#EFF3FB" />
							<ItemTemplate>
								<asp:Label ID="lblConviniAmt" runat="server" Text='<%# Eval("MULLER3_OK_NG") %>' Width="50px"></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="合計">
							<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Font-Size="X-Small" BackColor="lightyellow" />
							<ItemTemplate>
								<asp:Label ID="lblConviniAmt" runat="server" Text='<%# Eval("TOTAL_START_OK_LOGIN") %>' Width="50px"></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="正常">
							<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Font-Size="X-Small" />
							<ItemTemplate>
								<asp:Label ID="lblConviniCount" runat="server" Text='<%# Eval("MULLER3_NG_OK") %>' Width="50px"></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ﾌｨﾙﾀﾘﾝｸﾞ<br>ｴﾗｰ">
							<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Font-Size="X-Small" />
							<ItemTemplate>
								<asp:Label ID="lblConviniAmt" runat="server" Text='<%# Eval("MULLER3_NG_NG") %>' Width="50px"></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="合計">
							<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Font-Size="X-Small" BackColor="lightyellow" />
							<ItemTemplate>
								<asp:Label ID="lblConviniAmt" runat="server" Text='<%# Eval("TOTAL_START_NG_LOGIN") %>' Width="50px"></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="正常">
							<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Font-Size="X-Small" BackColor="#EFF3FB" />
							<ItemTemplate>
								<asp:Label ID="lblConviniCount" runat="server" Text='<%# Eval("TOTAL_END_OK_LOGIN") %>' Width="50px"></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ﾌｨﾙﾀﾘﾝｸﾞ<br>ｴﾗｰ">
							<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Font-Size="X-Small" BackColor="#EFF3FB" />
							<ItemTemplate>
								<asp:Label ID="lblConviniAmt" runat="server" Text='<%# Eval("TOTAL_END_NG_LOGIN") %>' Width="50px"></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="合計">
							<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Font-Size="X-Small" BackColor="#FFEBCD" />
							<ItemTemplate>
								<asp:Label ID="lblConviniAmt" runat="server" Text='<%# Eval("TOTAL_LOGIN_COUNT") %>' Width="50px"></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
					<FooterStyle ForeColor="Black" BackColor="LightYellow" />
				</asp:GridView>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsUserLoginCountDailyM3" runat="server" SelectMethod="GetList" TypeName="UserLoginCountDailyM3" OnSelecting="dsUserLoginCountDailyM3_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pYear" Type="String" />
			<asp:Parameter Name="pMonth" Type="String" />
			<asp:Parameter Name="pSinceLastLoginDays" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
</asp:Content>
