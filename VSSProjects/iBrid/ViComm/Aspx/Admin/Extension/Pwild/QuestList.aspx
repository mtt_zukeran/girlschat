﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="QuestList.aspx.cs" Inherits="Extension_Pwild_QuestList" Title="クエスト一覧" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="クエスト一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" Runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 600px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle2">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton" ValidationGroup="Create"
                    OnClick="btnCreate_Click" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" Visible="false" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlQuestInfo">
            <fieldset class="fieldset">
                <legend>[クエスト情報]</legend>
                <table border="0" style="width: 600px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            クエストID(SEQ)
                        </td>
                        <td class="tdDataStyle">
                            <asp:Label ID="lblQuestId" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            クエスト名
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtQuestNm" runat="server" MaxLength="100" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            公開開始日時
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtPublishStartDate" runat="server" MaxLength="16" Width="120px"></asp:TextBox>
                            <ajaxToolkit:MaskedEditExtender ID="mskPublishStartDate" runat="server" MaskType="DateTime"
                                Mask="9999/99/99 99:99:99" UserDateFormat="YearMonthDay" UserTimeFormat="TwentyFourHour"
                                ClearMaskOnLostFocus="true" TargetControlID="txtPublishStartDate">
                            </ajaxToolkit:MaskedEditExtender>
                            <br />
                            <asp:Label ID="lblErrorMessagePublishStart" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            公開終了日時
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtPublishEndDate" runat="server" MaxLength="16" Width="120px"></asp:TextBox>
                            <ajaxToolkit:MaskedEditExtender ID="mskPublishEndDate" runat="server" MaskType="DateTime"
                                Mask="9999/99/99 99:99:99" UserDateFormat="YearMonthDay" UserTimeFormat="TwentyFourHour"
                                ClearMaskOnLostFocus="true" TargetControlID="txtPublishEndDate">
                            </ajaxToolkit:MaskedEditExtender>
                            <br />
                            <asp:Label ID="lblErrorMessagePublishEnd" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            報酬受取期限
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtGetRewardEndDate" runat="server" MaxLength="16" Width="120px"></asp:TextBox>
                            <ajaxToolkit:MaskedEditExtender ID="mskGetRewardEndDate" runat="server" MaskType="DateTime"
                                Mask="9999/99/99 99:99:99" UserDateFormat="YearMonthDay" UserTimeFormat="TwentyFourHour"
                                ClearMaskOnLostFocus="true" TargetControlID="txtGetRewardEndDate">
                            </ajaxToolkit:MaskedEditExtender>
                            <br />
                            <asp:Label ID="lblErrorMessageRewardEnd" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            公開フラグ
                        </td>
                        <td class="tdDataStyle">
                            <asp:CheckBox ID="chkPublishFlag" runat="server" />
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="Panel1" runat="server">
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Update"
                        OnClick="btnUpdate_Click" />
                    <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" ValidationGroup="Delete"
                        OnClick="btnDelete_Click" />
                    <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" CausesValidation="False"
                        Visible="true" OnClick="btnCancel_Click" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[クエスト一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdQuest.PageIndex + 1 %>
                        of
                        <%= grdQuest.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="600px">
                    <asp:GridView ID="grdQuest" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        PageSize="30" EnableSortingAndPagingCallbacks="false" DataSourceID="dsQuest"
                        SkinID="GridViewColor" Font-Size="small" AllowSorting="true" DataKeyNames="SITE_CD,QUEST_SEQ">
                        <Columns>
                            <asp:TemplateField HeaderText="クエストID">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkQuestId" runat="server" CommandArgument='<%# Eval("QUEST_SEQ") %>'
                                        Text='<%# Eval("QUEST_SEQ") %>' OnCommand="lnkQuestId_Command"></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="クエスト名">
                                <ItemTemplate>
                                    <asp:Label ID="lblQestNm" runat="server" Text='<%# Eval("QUEST_NM", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="公開開始日時">
                                <ItemTemplate>
                                    <asp:Label ID="lblPublishStartDate" runat="server" Text='<%# Eval("PUBLISH_START_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="公開終了日時">
                                <ItemTemplate>
                                    <asp:Label ID="lblPublishEndDate" runat="server" Text='<%# Eval("PUBLISH_END_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="報酬受取期限">
                                <ItemTemplate>
                                    <asp:Label ID="lblGetRewardEndDate" runat="server" Text='<%# Eval("GET_REWARD_END_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="公開">
                                <ItemTemplate>
                                    <asp:Label ID="lblPublishFlag" runat="server" Text='<%# GetPublishFlagMark(Eval("PUBLISH_FLAG")) %>'></asp:Label>
                                    <asp:HiddenField ID="hdnPublishFlag" runat="server" Value='<%# Eval("PUBLISH_FLAG") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkLevelQuestList" runat="server" NavigateUrl='<%# string.Format("~/Extension/Pwild/LevelQuestList.aspx?sitecd={0}&questseq={1}&sexcd={2}", Eval("SITE_CD"), Eval("QUEST_SEQ"), Eval("SEX_CD"))%>'
                                        Text="ﾚﾍﾞﾙｸｴｽﾄ"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkOtherQuestList" runat="server" NavigateUrl='<%# string.Format("~/Extension/Pwild/OtherQuestList.aspx?sitecd={0}&questseq={1}&sexcd={2}", Eval("SITE_CD"), Eval("QUEST_SEQ"), Eval("SEX_CD"))%>'
                                        Text="裏ｸｴｽﾄ"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsQuest" runat="server" SelectMethod="GetPageCollection"
        ConvertNullToDBNull="true" SortParameterName="" TypeName="Quest" SelectCountMethod="GetPageCount"
        EnablePaging="True" OnSelected="dsQuest_Selected">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="クエスト情報を更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete"
        ConfirmText="クエスト情報を削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>

