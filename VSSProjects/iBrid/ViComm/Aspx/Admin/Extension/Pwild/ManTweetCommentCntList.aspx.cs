﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 会員つぶやきコメントランキング
--	Progaram ID		: ManTweetCommentCntList
--
--  Creation Date	: 2013.03.27
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iBridCommLib;
using ViComm;
using System.Data;

public partial class Extension_Pwild_ManTweetCommentCntList:Page {

	private DataSet ManTweetCommentTermData {
		get {
			return this.ViewState["ManTweetCommentTermData"] as DataSet;
		}
		set {
			this.ViewState["ManTweetCommentTermData"] = value;
		}
	}

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string TermSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["TermSeq"]);
		}
		set {
			this.ViewState["TermSeq"] = value;
		}
	}

	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!IsPostBack) {
			this.InitPage();
			this.SiteCd = Request.QueryString["sitecd"];
			this.TermSeq = Request.QueryString["termseq"];

			if (!string.IsNullOrEmpty(this.SiteCd) && !string.IsNullOrEmpty(this.TermSeq)) {
				this.lstSiteCd.SelectedValue = this.SiteCd;
				this.SetDate();
				this.GetList();
			}
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		this.GetList();
	}

	protected void btnBack_Click(object sender,EventArgs e) {
		this.Response.Redirect(string.Format("~/Extension/Pwild/ManTweetCommentTermList.aspx?sitecd={0}",this.SiteCd));
	}

	protected void dsManTweetCommentCnt_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		ManTweetCommentCnt.SearchCondition oSearchCondition = new ManTweetCommentCnt.SearchCondition();
		oSearchCondition.SiteCd = lstSiteCd.SelectedValue;
		oSearchCondition.TermSeq = this.TermSeq;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsManTweetCommentCnt_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void grdManTweetCommentCnt_RowDataBound(object sender,GridViewRowEventArgs e) {
	}

	private void InitPage() {
		this.pnlInfo.Visible = false;
		this.grdManTweetCommentCnt.PageSize = 100;
		this.grdManTweetCommentCnt.DataSourceID = string.Empty;

		this.DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void GetList() {
		this.recCount = "0";
		this.grdManTweetCommentCnt.PageIndex = 0;
		this.grdManTweetCommentCnt.DataSourceID = "dsManTweetCommentCnt";
		this.grdManTweetCommentCnt.DataBind();
		this.pnlInfo.Visible = true;
		this.pnlCount.DataBind();
	}

	protected string GetViewUrl(object pLoignId) {
		return string.Format("~/Cast/CastView.aspx?loginid={0}",pLoignId);
	}

	protected string GetRecCount() {
		return recCount;
	}

	private void SetDate() {
		if (this.ManTweetCommentTermData == null) {
			using (ManTweetCommentTerm oManTweetCommentTerm = new ManTweetCommentTerm()) {
				this.ManTweetCommentTermData = oManTweetCommentTerm.GetList(this.SiteCd,this.TermSeq);
			}
		}

		if (this.ManTweetCommentTermData.Tables[0].Rows.Count > 0) {
			DataRow oDataRow = this.ManTweetCommentTermData.Tables[0].Rows[0];
			this.lblDate.Text = string.Format("{0:yyyy/MM/dd HH:mm:ss} ～ {1:yyyy/MM/dd HH:mm:ss}",oDataRow["START_DATE"],oDataRow["END_DATE"]);
		}
	}

	protected void lnkAddBonusPointTweetComment_DataBinding(object sender,CommandEventArgs e) {
		string[] arguments = e.CommandArgument.ToString().Split(',');

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("ADD_BONUS_POINT_TWEET_COMMENT");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,arguments[0]);
			oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,arguments[1]);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,arguments[2]);
			oDbSession.ProcedureInParm("pTERM_SEQ",DbSession.DbType.VARCHAR2,arguments[3]);
			oDbSession.ProcedureInParm("pRANK_NO",DbSession.DbType.VARCHAR2,arguments[4]);
			oDbSession.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.GetList();
	}

	protected void lnkAddRewardStatus_DataBinding(object sender,EventArgs e) {
		if (sender is LinkButton) {
			LinkButton lnk = (LinkButton)sender;
			if (lnk.Text.Equals("付与")) {
				lnk.Visible = true;
			}
		} else if (sender is Label) {
			Label lbl = (Label)sender;
			if (lbl.Text.Equals("付与済み")) {
				lbl.Visible = true;
			}
		}
	}
}
