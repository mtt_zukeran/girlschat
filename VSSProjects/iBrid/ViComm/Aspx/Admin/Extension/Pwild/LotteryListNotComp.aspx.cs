﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 抽選一覧(非コンプ)

--	Progaram ID		: LotteryListNotComp
--
--  Creation Date	: 2012.06.12
--  Creater			: M&TT A.Taba
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.IO;
using ViComm.Extension.Pwild;

public partial class Extension_Pwild_LotteryListNotComp:System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		set {
			this.ViewState["SexCd"] = value;
		}
	}

	private string LotterySeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["LotterySeq"]);
		}
		set {
			this.ViewState["LotterySeq"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();
			this.SexCd = iBridUtil.GetStringValue(this.Request.QueryString["sexcd"]);
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
		this.LotterySeq = string.Empty;
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		if (!this.IsCorrectDate()) {
			return;
		}
		this.UpdateData(false);
		this.pnlKey.Enabled = true;
		this.pnlLotteryInfo.Visible = false;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		this.UpdateData(true);
		this.pnlKey.Enabled = true;
		this.pnlLotteryInfo.Visible = false;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.ClearFileds();
		this.pnlKey.Enabled = true;
		this.pnlLotteryInfo.Visible = false;
		this.LotterySeq = string.Empty;
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.pnlKey.Enabled = false;
		this.pnlLotteryInfo.Visible = true;
	}

	protected void lnkLotteryId_Command(object sender,CommandEventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.LotterySeq = iBridUtil.GetStringValue(e.CommandArgument);
		this.GetData();
		this.pnlKey.Enabled = false;
		this.pnlLotteryInfo.Visible = true;
	}

	protected void dsLottery_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	private void InitPage() {
		this.pnlInfo.Visible = true;
		this.pnlLotteryInfo.Visible = false;

		this.LotterySeq = string.Empty;
		this.pnlKey.Enabled = true;
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		this.ClearFileds();
	}

	private void ClearFileds() {
		this.lblLotteryId.Text = string.Empty;
		this.txtLotteryNm.Text = string.Empty;
		this.txtFirstBuyFreeStartDate.Text = string.Empty;
		this.txtFirstBuyFreeEndDate.Text = string.Empty;
		this.txtPublishStartDate.Text = string.Empty;
		this.txtPublishEndDate.Text = string.Empty;
		this.txtPrice.Text = string.Empty;
		this.txtPrice1.Text = string.Empty;
		this.txtPrice2.Text = string.Empty;
		this.txtPrice3.Text = string.Empty;
		this.txtLotteryCount1.Text = string.Empty;
		this.txtLotteryCount2.Text = string.Empty;
		this.txtLotteryCount3.Text = string.Empty;
		this.chkLotteryFlag1.Checked = false;
		this.chkLotteryFlag2.Checked = false;
		this.chkLotteryFlag3.Checked = false;
		this.txtPremiumCount1.Text = string.Empty;
		this.txtPremiumCount2.Text = string.Empty;
		this.txtPremiumCount3.Text = string.Empty;

		this.lblErrorMessageLotteryNm.Visible = false;
		this.lblErrorMessageFreeEnd.Visible = false;
		this.lblErrorMessageFreeStart.Visible = false;
		this.lblErrorMessagePublishEnd.Visible = false;
		this.lblErrorMessagePublishStart.Visible = false;
		this.lblErrorMessageFreeEnd.Text = string.Empty;
		this.lblErrorMessageFreeStart.Text = string.Empty;
		this.lblErrorMessagePublishEnd.Text = string.Empty;
		this.lblErrorMessagePublishStart.Text = string.Empty;
		this.lblErrorMessagePrice1.Visible = false;
		this.lblErrorMessageLotteryCount1.Visible = false;
		this.lblErrorMessagePremiumCount1.Visible = false;
		this.lblErrorMessagePrice2.Visible = false;
		this.lblErrorMessageLotteryCount2.Visible = false;
		this.lblErrorMessagePremiumCount2.Visible = false;
		this.lblErrorMessagePrice3.Visible = false;
		this.lblErrorMessageLotteryCount3.Visible = false;
		this.lblErrorMessagePremiumCount3.Visible = false;
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.LotterySeq = string.Empty;

		this.grdLottery.PageIndex = 0;
		this.grdLottery.DataSourceID = "dsLottery";
		this.grdLottery.DataBind();
		this.pnlCount.DataBind();
	}

	private void GetData() {
		this.ClearFileds();

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("LOTTERY_GET_NOT_COMP");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pLOTTERY_SEQ",DbSession.DbType.VARCHAR2,this.LotterySeq);
			oDbSession.ProcedureOutParm("pLOTTERY_NM",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSEX_CD",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pFIRST_BUY_FREE_START_DATE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pFIRST_BUY_FREE_END_DATE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPUBLISH_START_DATE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPUBLISH_END_DATE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPRICE",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pPRICE_1",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pPRICE_2",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pPRICE_3",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pLOTTERY_COUNT_1",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pLOTTERY_COUNT_2",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pLOTTERY_COUNT_3",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pLOTTERY_SET_FLG_1",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pLOTTERY_SET_FLG_2",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pLOTTERY_SET_FLG_3",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pPREMIUM_COUNT_1",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pPREMIUM_COUNT_2",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pPREMIUM_COUNT_3",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.lblLotteryId.Text = oDbSession.GetStringValue("pLOTTERY_SEQ");
			this.txtLotteryNm.Text = oDbSession.GetStringValue("pLOTTERY_NM");
			this.txtFirstBuyFreeStartDate.Text = oDbSession.GetStringValue("pFIRST_BUY_FREE_START_DATE");
			this.txtFirstBuyFreeEndDate.Text = oDbSession.GetStringValue("pFIRST_BUY_FREE_END_DATE");
			this.txtPublishStartDate.Text = oDbSession.GetStringValue("pPUBLISH_START_DATE");
			this.txtPublishEndDate.Text = oDbSession.GetStringValue("pPUBLISH_END_DATE");
			this.txtPrice.Text = oDbSession.GetStringValue("pPRICE");
			this.txtPrice1.Text = oDbSession.GetStringValue("pPRICE_1");
			this.txtPrice2.Text = oDbSession.GetStringValue("pPRICE_2");
			this.txtPrice3.Text = oDbSession.GetStringValue("pPRICE_3");
			this.txtLotteryCount1.Text = oDbSession.GetStringValue("pLOTTERY_COUNT_1");
			this.txtLotteryCount2.Text = oDbSession.GetStringValue("pLOTTERY_COUNT_2");
			this.txtLotteryCount3.Text = oDbSession.GetStringValue("pLOTTERY_COUNT_3");
			this.txtPremiumCount1.Text = oDbSession.GetStringValue("pPREMIUM_COUNT_1");
			this.txtPremiumCount2.Text = oDbSession.GetStringValue("pPREMIUM_COUNT_2");
			this.txtPremiumCount3.Text = oDbSession.GetStringValue("pPREMIUM_COUNT_3");
			this.chkLotteryFlag1.Checked = ViCommConst.FLAG_ON_STR.Equals(oDbSession.GetStringValue("pLOTTERY_SET_FLG_1"));
			this.chkLotteryFlag2.Checked = ViCommConst.FLAG_ON_STR.Equals(oDbSession.GetStringValue("pLOTTERY_SET_FLG_2"));
			this.chkLotteryFlag3.Checked = ViCommConst.FLAG_ON_STR.Equals(oDbSession.GetStringValue("pLOTTERY_SET_FLG_3"));
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");

		}
	}

	protected string GetLotteryDistinctMark(object pPublishFlag) {
		string sPublishFlag = iBridUtil.GetStringValue(pPublishFlag);

		return ViCommConst.FLAG_ON_STR.Equals(sPublishFlag) ? "通常" : "セット";
	}

	private bool IsCorrectDate() {
		this.lblErrorMessageFreeEnd.Visible = false;
		this.lblErrorMessageFreeStart.Visible = false;
		this.lblErrorMessagePublishEnd.Visible = false;
		this.lblErrorMessagePublishStart.Visible = false;
		this.lblErrorMessageFreeEnd.Text = string.Empty;
		this.lblErrorMessageFreeStart.Text = string.Empty;
		this.lblErrorMessagePublishEnd.Text = string.Empty;
		this.lblErrorMessagePublishStart.Text = string.Empty;
		this.lblErrorMessagePrice1.Visible = false;
		this.lblErrorMessageLotteryCount1.Visible = false;
		this.lblErrorMessagePremiumCount1.Visible = false;
		this.lblErrorMessagePrice2.Visible = false;
		this.lblErrorMessageLotteryCount2.Visible = false;
		this.lblErrorMessagePremiumCount2.Visible = false;
		this.lblErrorMessagePrice3.Visible = false;
		this.lblErrorMessageLotteryCount3.Visible = false;
		this.lblErrorMessagePremiumCount3.Visible = false;

		DateTime? dtPublishStart = SysPrograms.TryParseOrDafult(this.txtPublishStartDate.Text,(DateTime?)null);
		DateTime? dtPublishEnd = SysPrograms.TryParseOrDafult(this.txtPublishEndDate.Text,(DateTime?)null);
		DateTime? dtFirstBuyFreeStart = SysPrograms.TryParseOrDafult(this.txtFirstBuyFreeStartDate.Text,(DateTime?)null);
		DateTime? dtFirstBuyFreeEnd = SysPrograms.TryParseOrDafult(this.txtFirstBuyFreeEndDate.Text,(DateTime?)null);

		bool bResult = true;
		
		if (string.IsNullOrEmpty(txtLotteryNm.Text)) {
			this.lblErrorMessageLotteryNm.Visible = true;
			this.lblErrorMessageLotteryNm.Text = "抽選名を入力してください。";
			bResult = false;
		}
		if (chkLotteryFlag1.Checked) {
			if (string.IsNullOrEmpty(this.txtPrice1.Text)) {
				this.lblErrorMessagePrice1.Visible = true;
				this.lblErrorMessagePrice1.Text = "価格1を入力してください。";
				bResult = false;
			}
			if (string.IsNullOrEmpty(this.txtLotteryCount1.Text)) {
				this.lblErrorMessageLotteryCount1.Visible = true;
				this.lblErrorMessageLotteryCount1.Text = "セット1を入力してください。";
				bResult = false;
			}
			if (string.IsNullOrEmpty(this.txtPremiumCount1.Text)) {
				this.lblErrorMessagePremiumCount1.Visible = true;
				this.lblErrorMessagePremiumCount1.Text = "セット1を入力してください。";
				bResult = false;
			}
			if (!string.IsNullOrEmpty(this.txtLotteryCount1.Text) && !string.IsNullOrEmpty(this.txtPremiumCount1.Text)) {
				if (int.Parse(txtLotteryCount1.Text) < int.Parse(txtPremiumCount1.Text) ) {
					this.lblErrorMessagePremiumCount1.Visible = true;
					this.lblErrorMessagePremiumCount1.Text = "ガチャ回数1より低く設定してください。";
					bResult = false;
				}
			}
		}
		if (chkLotteryFlag2.Checked) {
			if (string.IsNullOrEmpty(this.txtPrice2.Text)) {
				this.lblErrorMessagePrice2.Visible = true;
				this.lblErrorMessagePrice2.Text = "価格2を入力してください。";
				bResult = false;
			}
			if (string.IsNullOrEmpty(this.txtLotteryCount2.Text)) {
				this.lblErrorMessageLotteryCount2.Visible = true;
				this.lblErrorMessageLotteryCount2.Text = "セット2を入力してください。";
				bResult = false;
			}
			if (string.IsNullOrEmpty(this.txtPremiumCount2.Text)) {
				this.lblErrorMessagePremiumCount2.Visible = true;
				this.lblErrorMessagePremiumCount2.Text = "セット2を入力してください。";
				bResult = false;
			}
			if (!string.IsNullOrEmpty(this.txtLotteryCount2.Text) && !string.IsNullOrEmpty(this.txtPremiumCount2.Text)) {
				if (int.Parse(txtLotteryCount2.Text) < int.Parse(txtPremiumCount2.Text)) {
					this.lblErrorMessagePremiumCount2.Visible = true;
					this.lblErrorMessagePremiumCount2.Text = "ガチャ回数2より低く設定してください。";
					bResult = false;
				}
			}
		}
		if (chkLotteryFlag3.Checked) {
			if (string.IsNullOrEmpty(this.txtPrice3.Text)) {
				this.lblErrorMessagePrice3.Visible = true;
				this.lblErrorMessagePrice3.Text = "価格3を入力してください。";
				bResult = false;
			}
			if (string.IsNullOrEmpty(this.txtLotteryCount3.Text)) {
				this.lblErrorMessageLotteryCount3.Visible = true;
				this.lblErrorMessageLotteryCount3.Text = "セット3を入力してください。";
				bResult = false;
			}
			if (string.IsNullOrEmpty(this.txtPremiumCount3.Text)) {
				this.lblErrorMessagePremiumCount3.Visible = true;
				this.lblErrorMessagePremiumCount3.Text = "セット3を入力してください。";
				bResult = false;
			}
			if (!string.IsNullOrEmpty(this.txtLotteryCount3.Text) && !string.IsNullOrEmpty(this.txtPremiumCount3.Text)) {
				if (int.Parse(txtLotteryCount3.Text) < int.Parse(txtPremiumCount3.Text)) {
					this.lblErrorMessagePremiumCount3.Visible = true;
					this.lblErrorMessagePremiumCount3.Text = "ガチャ回数3より低く設定してください。";
					bResult = false;
				}
			}
		}
		if (!dtPublishStart.HasValue) {
			this.lblErrorMessagePublishStart.Visible = true;
			this.lblErrorMessagePublishStart.Text = "公開開始日時を正しく入力してください。";
			bResult = false;
		}
		if (!dtPublishEnd.HasValue) {
			this.lblErrorMessagePublishEnd.Visible = true;
			this.lblErrorMessagePublishEnd.Text = "公開終了日時を正しく入力してください。";
			bResult = false;
		}
		if (dtPublishEnd < dtPublishStart) {
			this.lblErrorMessagePublishEnd.Visible = true;
			this.lblErrorMessagePublishEnd.Text = "公開期間の大小関係が不正です。";
			bResult = false;
		}
		if (!dtFirstBuyFreeStart.HasValue && dtFirstBuyFreeEnd.HasValue) {
			this.lblErrorMessageFreeStart.Visible = true;
			this.lblErrorMessageFreeStart.Text = "初回無料期間開始日時を正しく入力してください。";
			bResult = false;
		}
		if (dtFirstBuyFreeStart.HasValue && !dtFirstBuyFreeEnd.HasValue) {
			this.lblErrorMessageFreeEnd.Visible = true;
			this.lblErrorMessageFreeEnd.Text = "初回無料期間終了日時を正しく入力してください。";
			bResult = false;
		}
		if (dtFirstBuyFreeStart.HasValue && dtFirstBuyFreeEnd.HasValue) {
			if (dtFirstBuyFreeStart < dtPublishStart) {
				this.lblErrorMessageFreeStart.Visible = true;
				this.lblErrorMessageFreeStart.Text = "公開開始日時と初回無料期間開始日時の大小関係が不正です。";
				bResult = false;
			}
			if (dtPublishEnd < dtFirstBuyFreeEnd) {
				this.lblErrorMessagePublishEnd.Visible = true;
				this.lblErrorMessagePublishEnd.Text = "初回無料期間終了日時と公開終了日時の大小関係が不正です。";
				bResult = false;
			}
			if (dtFirstBuyFreeEnd < dtFirstBuyFreeStart) {
				this.lblErrorMessageFreeEnd.Visible = true;
				this.lblErrorMessageFreeEnd.Text = "初回無料期間の大小関係が不正です。";
				bResult = false;
			}
		}

		return bResult;
	}

	private void UpdateData(bool pDelFlag) {
		DateTime? dtPublishStart = SysPrograms.TryParseOrDafult(this.txtPublishStartDate.Text,(DateTime?)null);
		DateTime? dtPublishEnd = SysPrograms.TryParseOrDafult(this.txtPublishEndDate.Text,(DateTime?)null);
		DateTime? dtFirstBuyFreeStart = SysPrograms.TryParseOrDafult(this.txtFirstBuyFreeStartDate.Text,(DateTime?)null);
		DateTime? dtFirstBuyFreeEnd = SysPrograms.TryParseOrDafult(this.txtFirstBuyFreeEndDate.Text,(DateTime?)null);
		if (dtPublishEnd.HasValue) {
			dtPublishEnd = (DateTime?)dtPublishEnd.Value.AddSeconds(-1);
		}
		if (dtFirstBuyFreeEnd.HasValue) {
			dtFirstBuyFreeEnd = (DateTime?)dtFirstBuyFreeEnd.Value.AddSeconds(-1);
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("LOTTERY_MAINTE_NOT_COMP");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureBothParm("pLOTTERY_SEQ",DbSession.DbType.VARCHAR2,this.LotterySeq);
			oDbSession.ProcedureInParm("pLOTTERY_NM",DbSession.DbType.VARCHAR2,this.txtLotteryNm.Text);
			oDbSession.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,this.SexCd);
			oDbSession.ProcedureInParm("pFIRST_BUY_FREE_START_DATE",DbSession.DbType.DATE,dtFirstBuyFreeStart);
			oDbSession.ProcedureInParm("pFIRST_BUY_FREE_END_DATE",DbSession.DbType.DATE,dtFirstBuyFreeEnd);
			oDbSession.ProcedureInParm("pPUBLISH_START_DATE",DbSession.DbType.DATE,dtPublishStart);
			oDbSession.ProcedureInParm("pPUBLISH_END_DATE",DbSession.DbType.DATE,dtPublishEnd);
			oDbSession.ProcedureInParm("pPRICE",DbSession.DbType.NUMBER,this.txtPrice.Text);
			if (string.IsNullOrEmpty(txtPrice1.Text)) {
				oDbSession.ProcedureInParm("pPRICE_1",DbSession.DbType.NUMBER,null);
			}else {
				oDbSession.ProcedureInParm("pPRICE_1",DbSession.DbType.NUMBER,this.txtPrice1.Text);
			}
			if (string.IsNullOrEmpty(txtPrice2.Text)) {
				oDbSession.ProcedureInParm("pPRICE_2",DbSession.DbType.NUMBER,null);
			}else {
				oDbSession.ProcedureInParm("pPRICE_2",DbSession.DbType.NUMBER,this.txtPrice2.Text);
			}
			if (string.IsNullOrEmpty(txtPrice3.Text)) {
				oDbSession.ProcedureInParm("pPRICE_3",DbSession.DbType.NUMBER,null);
			} else {
				oDbSession.ProcedureInParm("pPRICE_3",DbSession.DbType.NUMBER,this.txtPrice3.Text);
			}
			if (string.IsNullOrEmpty(txtLotteryCount1.Text)) {
				oDbSession.ProcedureInParm("pLOTTERY_COUNT_1",DbSession.DbType.NUMBER,null);
			} else {
				oDbSession.ProcedureInParm("pLOTTERY_COUNT_1",DbSession.DbType.NUMBER,this.txtLotteryCount1.Text);
			}
			if (string.IsNullOrEmpty(txtLotteryCount2.Text)) {
				oDbSession.ProcedureInParm("pLOTTERY_COUNT_2",DbSession.DbType.NUMBER,null);
			} else {
				oDbSession.ProcedureInParm("pLOTTERY_COUNT_2",DbSession.DbType.NUMBER,this.txtLotteryCount2.Text);
			}
			if (string.IsNullOrEmpty(txtLotteryCount3.Text)) {
				oDbSession.ProcedureInParm("pLOTTERY_COUNT_3",DbSession.DbType.NUMBER,null);
			} else {
				oDbSession.ProcedureInParm("pLOTTERY_COUNT_3",DbSession.DbType.NUMBER,this.txtLotteryCount3.Text);
			}
			oDbSession.ProcedureInParm("LOTTERY_SET_FLG_1",DbSession.DbType.NUMBER,this.chkLotteryFlag1.Checked ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR);
			oDbSession.ProcedureInParm("LOTTERY_SET_FLG_2",DbSession.DbType.NUMBER,this.chkLotteryFlag2.Checked ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR);
			oDbSession.ProcedureInParm("LOTTERY_SET_FLG_3",DbSession.DbType.NUMBER,this.chkLotteryFlag3.Checked ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR);
			if (string.IsNullOrEmpty(txtPremiumCount1.Text)) {
				oDbSession.ProcedureInParm("pPREMIUM_COUNT_1",DbSession.DbType.NUMBER,null);
			} else {
				oDbSession.ProcedureInParm("pPREMIUM_COUNT_1",DbSession.DbType.NUMBER,this.txtPremiumCount1.Text);
			}
			if (string.IsNullOrEmpty(txtPremiumCount2.Text)) {
				oDbSession.ProcedureInParm("pPREMIUM_COUNT_2",DbSession.DbType.NUMBER,null);
			} else {
				oDbSession.ProcedureInParm("pPREMIUM_COUNT_2",DbSession.DbType.NUMBER,this.txtPremiumCount2.Text);
			}
			if (string.IsNullOrEmpty(txtPremiumCount3.Text)) {
				oDbSession.ProcedureInParm("pPREMIUM_COUNT_3",DbSession.DbType.NUMBER,null);
			} else {
				oDbSession.ProcedureInParm("pPREMIUM_COUNT_3",DbSession.DbType.NUMBER,this.txtPremiumCount3.Text);
			}
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.ClearFileds();
		this.GetList();
	}
}
