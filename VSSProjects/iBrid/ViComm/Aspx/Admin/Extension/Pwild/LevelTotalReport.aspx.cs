﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 男性レベル分布レポート

--	Progaram ID		: LevelTotalReport
--
--  Creation Date	: 2012.12.01
--  Creater			: M&TT A.Taba
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using iBridCommLib;
using System.Drawing;
using ViComm;

public partial class Extension_Pwild_LevelTotalReport:System.Web.UI.Page {
	
	protected string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		private set {
			this.ViewState["SexCd"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		if (!this.IsPostBack) {
			this.SexCd = this.Request.QueryString["sexcd"];
			if (ViCommConst.MAN.Equals(this.SexCd)) {
				this.lblPgmTitle.Text = "男性" + this.lblPgmTitle.Text;
			} else {
				this.lblPgmTitle.Text = "女性" + this.lblPgmTitle.Text;
			}
			this.Title = this.lblPgmTitle.Text;

			this.InitPage();

			this.GetList();
			this.pnlInfo.Visible = true;
		}
	}

	private void InitPage() {
		this.grdLevelTotalReport.DataSourceID = string.Empty;
		this.pnlKey.DataBind();
		this.pnlInfo.Visible = false;
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void GetList() {
		this.grdLevelTotalReport.DataSourceID = "dsLevelTotalReport";
		this.grdLevelTotalReport.DataBind();
	}

	protected void btnCSV_Click(object sender,EventArgs e) {

		if (grdLevelTotalReport.Rows.Count == 0) {
			return;
		}

		DataTable oCsvData;
		using (LevelTotalReport oLevelTotalReport = new LevelTotalReport()) {
			using (DataSet oDataSet = oLevelTotalReport.GetList(
				this.lstSeekSiteCd.SelectedValue,
				this.SexCd)) {

				if (oDataSet.Tables[0].Rows.Count > 0) {
					oCsvData = oDataSet.Tables[0];
				} else {
					return;
				}
			}
		}

		//ヘッダ作成
		string sHeader = "レベル,人数";

		//CSV出力 
		Response.ContentType = "application/download";
		Response.AppendHeader("Content-Disposition","attachment;filename=LevelTotalReport.csv");
		Response.ContentEncoding = System.Text.Encoding.GetEncoding("Shift_JIS");

		Response.Write(sHeader + "\r\n");
		foreach (DataRow oCsvRow in oCsvData.Rows) {
			string sData =
				SetCsvString(oCsvRow["GAME_CHARACTER_LEVEL"].ToString()) + "," +
				SetCsvString(oCsvRow["GAME_CHARACTER_LEVEL_COUNT"].ToString());

			Response.Write(sData + "\r\n");
		}
		Response.End();
	}

	private string SetCsvString(string pData) {
		if (pData.IndexOf('"') > -1 ||
			pData.IndexOf(',') > -1 ||
			pData.IndexOf('\r') > -1 ||
			pData.IndexOf('\n') > -1 ||
			pData.StartsWith(" ") || pData.StartsWith("\t") ||
			pData.EndsWith(" ") || pData.EndsWith("\t")) {
			if (pData.IndexOf('"') > -1) {
				//"を""とする
				pData = pData.Replace("\"","\"\"");
			}
			if (pData.IndexOf('\r') > -1 || pData.IndexOf('\n') > -1) {
				//改行しない 
				pData = pData.Replace("\r","");
				pData = pData.Replace("\n","");
			}
			pData = "\"" + pData + "\"";
		}
		return pData;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		this.GetList();
		this.pnlInfo.Visible = true;
	}
}