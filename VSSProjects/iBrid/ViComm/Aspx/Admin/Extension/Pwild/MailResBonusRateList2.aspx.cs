﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メール返信ボーナスポイント確率設定2
--	Progaram ID		: MailResBonusRateList2
--  Creation Date	: 2016.09.05
--  Creater			: M&TT Zukeran
**************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;

public partial class Extension_Pwild_MailResBonusRateList2:System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SITE_CD"]);
		}
		set {
			this.ViewState["SITE_CD"] = value;
		}
	}

	private string MailResBonusSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MAIL_RES_BONUS_SEQ"]);
		}
		set {
			this.ViewState["MAIL_RES_BONUS_SEQ"] = value;
		}
	}

	private string MailResBonusRateSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MAIL_RES_BONUS_RATE_SEQ"]);
		}
		set {
			this.ViewState["MAIL_RES_BONUS_RATE_SEQ"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["REVISION_NO"]);
		}
		set {
			this.ViewState["REVISION_NO"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			this.MailResBonusSeq = iBridUtil.GetStringValue(Request.QueryString["mailresbonusseq"]);

			this.pnlSearch.Enabled = true;
			this.pnlInput.Visible = false;

			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	/// <summary>
	/// 設定欄の入力値、エラー表示を初期化
	/// </summary>
	private void ClearFileds() {
		this.lblErrorMessage.Text = string.Empty;
		this.txtAddPoint.Text = string.Empty;
		this.txtRate.Text = string.Empty;
	}

	/// <summary>
	/// 数値フィールド値チェック
	/// </summary>
	/// <param name="sText"></param>
	/// <param name="sName"></param>
	/// <param name="iResult"></param>
	/// <param name="sErrMsg"></param>
	/// <returns></returns>
	private bool CheckIntFieldValue(string sText,string sName,out int iResult,out string sErrMsg) {
		iResult = 0;
		sErrMsg = "";

		if (String.IsNullOrEmpty(sText)) {
			sErrMsg = String.Format("{0}を入力してください",sName);
			return false;
		}
		if (!int.TryParse(sText,out iResult)) {
			sErrMsg = String.Format("{0}を正しく入力してください",sName);
			return false;
		}

		return true;
	}

	/// <summary>
	/// 入力値チェック
	/// </summary>
	/// <returns></returns>
	private bool CheckInput() {
		StringBuilder errorMessage = new StringBuilder();
		string sErrorMessage;

		int iResult;

		if (!CheckIntFieldValue(txtAddPoint.Text,"付与ポイント",out iResult,out sErrorMessage)) {
			errorMessage.AppendLine(sErrorMessage);
		}
		if (!CheckIntFieldValue(txtRate.Text,"確率",out iResult,out sErrorMessage)) {
			errorMessage.AppendLine(sErrorMessage);
		}

		// エラーメッセージを設定
		if (errorMessage.Length > 0) {
			this.lblErrorMessage.Text = errorMessage.ToString().Replace("\n","<br />");
			return false;
		}

		return true;
	}

	private void GetData() {
		//検索条件を設定
		MailResBonusRate2.SearchCondition oSearchCondition = new MailResBonusRate2.SearchCondition();
		oSearchCondition.SiteCd = lstSiteCd.SelectedValue;
		oSearchCondition.MailResBonusSeq = this.MailResBonusSeq;
		oSearchCondition.MailResBonusRateSeq = this.MailResBonusRateSeq;

		// データ取得
		MailResBonusRate2 oMailResBonusRate2 = new MailResBonusRate2();
		DataRow dr = oMailResBonusRate2.GetData(oSearchCondition);

		// 付与ポイント
		txtAddPoint.Text = dr["ADD_POINT"].ToString();
		// 確率
		txtRate.Text = dr["RATE"].ToString();
		// リビジョンNo
		this.RevisionNo = dr["REVISION_NO"].ToString();
	}

	/// <summary>
	/// メール返信ボーナス確率のデータ反映
	/// </summary>
	/// <param name="pDeleteFlag"></param>
	/// <param name="pResult"></param>
	private void MailResBonusRateMainte(int pDeleteFlag,out string pResult) {

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("MAIL_RES_BONUS_RATE2_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pMAIL_RES_BONUS_SEQ",DbSession.DbType.NUMBER,this.MailResBonusSeq);
			oDbSession.ProcedureInParm("pMAIL_RES_BONUS_RATE_SEQ",DbSession.DbType.NUMBER,this.MailResBonusRateSeq);
			oDbSession.ProcedureInParm("pADD_POINT",DbSession.DbType.NUMBER,this.txtAddPoint.Text);
			oDbSession.ProcedureInParm("pRATE",DbSession.DbType.NUMBER,this.txtRate.Text);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDeleteFlag);
			oDbSession.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
			pResult = oDbSession.GetStringValue("pRESULT");
		}
	}

	protected void dsMailResBonusRate_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		MailResBonusRate2.SearchCondition oSearchCondition = new MailResBonusRate2.SearchCondition();
		oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
		oSearchCondition.MailResBonusSeq = this.MailResBonusSeq;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsMailResBonusRate_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	/// <summary>
	/// 戻るボタンのクリックイベント
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnBack_Click(object sender,EventArgs e) {
		this.Response.Redirect(string.Format("~/Extension/Pwild/MailResBonusList2.aspx?sitecd={0}",this.SiteCd));
		return;
	}

	/// <summary>
	/// 検索ボタンのクリックイベント
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			grdMailResBonusRate.PageIndex = 0;
			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	/// <summary>
	/// 追加ボタンのクリックイベント
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnCreate_Click(object sender,EventArgs e) {
		this.MailResBonusRateSeq = string.Empty;
		this.RevisionNo = string.Empty;
		this.ClearFileds();

		this.pnlSearch.Enabled = false;
		this.pnlInput.Visible = true;
		this.btnDelete.Enabled = false;
	}

	/// <summary>
	/// 更新ボタンのクリックイベント
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (CheckInput()) {
			string sResult;
			MailResBonusRateMainte(ViCommConst.FLAG_OFF,out sResult);

			this.pnlSearch.Enabled = true;
			this.pnlInput.Visible = false;
			grdMailResBonusRate.PageIndex = 0;
			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	/// <summary>
	/// キャンセルボタンのクリックイベント
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlSearch.Enabled = true;
		this.pnlInput.Visible = false;
	}

	/// <summary>
	/// 削除ボタンのクリックイベント
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnDelete_Click(object sender,EventArgs e) {
		string sResult;
		MailResBonusRateMainte(ViCommConst.FLAG_ON,out sResult);

		this.pnlSearch.Enabled = true;
		this.pnlInput.Visible = false;
		grdMailResBonusRate.PageIndex = 0;
		pnlGrid.DataBind();
		pnlCount.DataBind();
	}

	/// <summary>
	/// シーケンスNoリンクのクリックイベント
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void lnkEdit_Command(object sender,CommandEventArgs e) {
		int iIndex = int.Parse(iBridUtil.GetStringValue(e.CommandArgument));
		GridViewRow oRow = this.grdMailResBonusRate.Rows[iIndex];
		HiddenField oHiddenField = oRow.FindControl("hdnMailResBonusRateSeq") as HiddenField;
		this.MailResBonusRateSeq = oHiddenField.Value;

		this.ClearFileds();
		this.GetData();

		this.pnlSearch.Enabled = false;
		this.pnlInput.Visible = true;
		this.btnDelete.Enabled = true;
	}
}