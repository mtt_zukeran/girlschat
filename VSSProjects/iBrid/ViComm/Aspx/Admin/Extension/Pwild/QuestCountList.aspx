﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="QuestCountList.aspx.cs" Inherits="Extension_Pwild_QuestCountList" Title="クエスト分布レポート" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="クエスト分布レポート"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" Runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 600px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle2">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[クエスト分布レポート]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdQuest.PageIndex + 1 %>
                        of
                        <%= grdQuest.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="600px">
                    <asp:GridView ID="grdQuest" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        PageSize="30" EnableSortingAndPagingCallbacks="false" DataSourceID="dsQuest"
                        SkinID="GridViewColor" Font-Size="small" AllowSorting="true" DataKeyNames="SITE_CD,QUEST_SEQ">
                        <Columns>
                            <asp:TemplateField HeaderText="クエストID">
                                <ItemTemplate>
                                    <asp:Label ID="lblQuestId" runat="server" Text='<%# Eval("QUEST_SEQ") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="クエスト名">
                                <ItemTemplate>
                                    <asp:Label ID="lblQestNm" runat="server" Text='<%# Eval("QUEST_NM", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkLevelQuestList" runat="server" NavigateUrl='<%# string.Format("~/Extension/Pwild/LevelQuestCountList.aspx?sitecd={0}&questseq={1}&sexcd={2}", Eval("SITE_CD"), Eval("QUEST_SEQ"), Eval("SEX_CD"))%>'
                                        Text="ﾚﾍﾞﾙｸｴｽﾄ"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkOtherQuestCountList" runat="server" NavigateUrl='<%# string.Format("~/Extension/Pwild/OtherQuestCountList.aspx?sitecd={0}&questseq={1}&sexcd={2}", Eval("SITE_CD"), Eval("QUEST_SEQ"), Eval("SEX_CD"))%>'
                                        Text="裏ｸｴｽﾄ"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsQuest" runat="server" SelectMethod="GetPageCollection"
        ConvertNullToDBNull="true" SortParameterName="" TypeName="Quest" SelectCountMethod="GetPageCount"
        EnablePaging="True" OnSelected="dsQuest_Selected">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
</asp:Content>

