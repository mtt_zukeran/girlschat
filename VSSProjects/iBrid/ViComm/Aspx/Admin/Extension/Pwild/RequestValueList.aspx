﻿<%@ Import Namespace="ViComm" %>

<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RequestValueList.aspx.cs" Inherits="Extension_Pwild_RequestValueList" Title="評価一覧" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="評価一覧"></asp:Label>
</asp:Content>

<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" Runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[評価編集]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					<asp:LinkButton ID="lnkPictographSample" runat="server" OnClick="lnkPictographSample_Click">
						絵文字一覧
					</asp:LinkButton>
					<table border="0" style="width: 820px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle">
								お願いタイトル
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblRequestTitle" runat="server"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								お願い提案内容
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblRequestText" runat="server"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								評価投稿者
							</td>
							<td class="tdDataStyle">
								<asp:HyperLink ID="lnkLoginId" runat="server"></asp:HyperLink>
								<br />
								<asp:Label ID="lblHandleNm" runat="server"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								評価
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblValueNm" runat="server"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								コメント
							</td>
							<td class="tdDataStyle">
								$NO_TRANS_START;
								<asp:TextBox ID="txtCommentText" runat="server" Width="650px" Rows="5" TextMode="MultiLine"></asp:TextBox>
								$NO_TRANS_END;
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								コメントプレビュー
							</td>
							<td class="tdDataStyle" valign="top">
								<asp:Label ID="lblCommentText" runat="server"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								投稿日時
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblCreateDate" runat="server"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								コメント公開日時
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblCommentPublicDate" runat="server"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle2">
								コメント状態
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstCommentPublicStatus" runat="server">
									<asp:ListItem Value="1">申請中</asp:ListItem>
									<asp:ListItem Value="2">公開</asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
					</table>
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
					<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<fieldset class="fieldset-inner">
				<table border="0" style="width: 840px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイトコード
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle">
							コメント有無
						</td>
						<td class="tdDataStyle">
							<asp:CheckBox ID="chkCommentFlag" runat="server" Text="ｺﾒﾝﾄありのみ" />
						</td>
						<td class="tdHeaderStyle">
							お願いSEQ
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtSeekRequestSeq" runat="server" Width="70px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							投稿者ID
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtSeekLoginId" runat="server" Width="70px"></asp:TextBox>
						</td>
						<td class="tdHeaderStyle2">
							コメント状態
						</td>
						<td class="tdDataStyle">
							<asp:CheckBox ID="chkSeekApply" runat="server" Text="申請中" />&nbsp;
							<asp:CheckBox ID="chkSeekPublic" runat="server" Text="公開" />
						</td>
						<td class="tdHeaderStyle2">
							評価
						</td>
						<td class="tdDataStyle">
							<asp:CheckBox ID="chkSeekGood" runat="server" Text="良い" />&nbsp;
							<asp:CheckBox ID="chkSeekBad" runat="server" Text="悪い" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							ｷｰﾜｰﾄﾞ検索
						</td>
						<td class="tdDataStyle" colspan="5">
							<asp:TextBox ID="txtSeekKeyword" runat="server" Width="170px"></asp:TextBox>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Key" />
			</fieldset>
			<fieldset class="fieldset-inner">
				<legend>[評価一覧]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count
						<%#GetRecCount() %>
					</a>
					<br />
					<a class="reccount">Current viewing page
						<%=grdRequestValue.PageIndex + 1%>
						of
						<%=grdRequestValue.PageCount%>
					</a>
					<br />
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlGrid">
					<asp:GridView ID="grdRequestValue" runat="server" AllowPaging="True" AutoGenerateColumns="False" OnSorting="grdRequestValue_Sorting"
						EnableSortingAndPagingCallbacks="false" DataSourceID="dsRequestValue" SkinID="GridView" AllowSorting="True"
						DataKeyNames="SITE_CD,REQUEST_VALUE_SEQ" OnRowDataBound="grdRequestValue_RowDataBound">
						<Columns>
							<asp:TemplateField HeaderText="お願いﾀｲﾄﾙ">
								<ItemTemplate>
									<asp:Label ID="lblRequestTitle" runat="server" Text='<%# GetConcatStr(Eval("REQUEST_TITLE"),30) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle Width="200px" />
							</asp:TemplateField>

							<asp:TemplateField HeaderText="評価">
								<ItemTemplate>
									<asp:LinkButton ID="lnkValueNm" runat="server" CommandArgument='<%# Container.DataItemIndex %>'
										CommandName="MAINTE" OnCommand="lnkMainte_Command" Text='<%# Eval("VALUE_NM") %>'></asp:LinkButton>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ｺﾒﾝﾄ">
								<ItemTemplate>
									<asp:Label ID="lblCommentText" runat="server" Text='<%# GetConcatStr(Eval("COMMENT_TEXT"),30) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle Width="200px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="投稿者">
								<ItemTemplate>
									<asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# this.GetProfUrl(Eval("SITE_CD"),Eval("SEX_CD"),Eval("LOGIN_ID")) %>' Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
									<br />
									<asp:Label ID="lblHandleNm" runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="COMMENT_PUBLIC_STATUS_NM" HeaderText="ｺﾒﾝﾄ状態">
							</asp:BoundField>
							<asp:TemplateField HeaderText="投稿日時" SortExpression="CREATE_DATE">
								<ItemTemplate>
									<asp:Label ID="lblCreateDate" runat="server" Text='<%# DateTime.Parse(Eval("CREATE_DATE").ToString()).ToString("yy/MM/dd HH:mm") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</fieldset>
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsRequestValue" runat="server" SelectMethod="GetPageCollection" ConvertNullToDBNull="true" SortParameterName=""
		TypeName="RequestValue" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelected="dsRequestValue_Selected" OnSelecting="dsRequestValue_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
</asp:Content>

