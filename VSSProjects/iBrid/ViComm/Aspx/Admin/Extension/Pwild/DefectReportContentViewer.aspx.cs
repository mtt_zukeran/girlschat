﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 不具合報告内容表示ビューワ
--	Progaram ID		: DefectReportContentViewer
--
--  Creation Date	: 2015.07.01
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Windows.Forms;
using System.Drawing;
using MobileLib;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class Extension_Pwild_DefectReportContentViewer:System.Web.UI.Page {
	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			string sSiteCd = iBridUtil.GetStringValue(this.Request.QueryString["site"]);
			string sDefectReportSeq = iBridUtil.GetStringValue(this.Request.QueryString["reportseq"]);
			
			if (string.IsNullOrEmpty(sSiteCd) || string.IsNullOrEmpty(sDefectReportSeq)) {
				return;
			}
			
			using (DefectReport oDefectReport = new DefectReport()) {
				DataSet oDataSet = oDefectReport.GetOne(sSiteCd,sDefectReportSeq);
				
				if (oDataSet.Tables[0].Rows.Count > 0) {
					this.lblTitle.Text = oDataSet.Tables[0].Rows[0]["TITLE"].ToString();
					this.lblText.Text = oDataSet.Tables[0].Rows[0]["TEXT"].ToString().Replace("\n","<br />");
				}
			}
		}
	}
}
