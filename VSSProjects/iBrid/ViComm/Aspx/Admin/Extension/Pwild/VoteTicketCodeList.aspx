﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="VoteTicketCodeList.aspx.cs" Inherits="Extension_Pwild_VoteTicketCodeList" Title="投票券コード一覧" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="投票券コード一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlRegist">
			<fieldset class="fieldset">
				<legend>[コード追加]</legend>
				<asp:Label ID="lblRegistComplete" runat="server" Text="コード追加が完了しました" ForeColor="blue" Visible="false"></asp:Label>
				<asp:Label ID="lblRegistError" runat="server" Text="" ForeColor="red"></asp:Label>
				<table border="0" style="width: 650px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							コード追加数
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtAddCount" runat="server" Width="50px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							投票券引換数
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtTicketCount" runat="server" Width="50px"></asp:TextBox>枚/1コード
						</td>
					</tr>
				</table>
				<asp:Panel ID="pnlCommand" runat="server" Width="660px">
					<asp:Button runat="server" ID="btnRegist" Text="追加" CssClass="seekbutton" OnClick="btnRegist_Click" />
					<asp:Button runat="server" ID="btnBack" Text="戻る" CssClass="seekbutton" OnClick="btnBack_Click" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlList">
			<fieldset>
				<legend>[一覧]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">
						Record Count <%# GetRecCount() %>
					</a>
					<br />
					<a class="reccount">
						Current viewing page <%= grdVoteTicketCode.PageIndex + 1%> of <%= grdVoteTicketCode.PageCount %>
					</a>
				</asp:Panel>
				&nbsp;
				<asp:Panel ID="pnlGrid" runat="server">
					<asp:GridView ID="grdVoteTicketCode" DataSourceID="dsVoteTicketCode" runat="server" AllowPaging="True" AutoGenerateColumns="False"
						PageSize="9999" EnableSortingAndPagingCallbacks="false" SkinID="GridViewColor" AllowSorting="true">
						<Columns>
							<asp:BoundField DataField="CODE_STR" HeaderText="コード">
								<ItemStyle HorizontalAlign="Left" />
							</asp:BoundField>
							<asp:BoundField DataField="TICKET_COUNT" HeaderText="引換数" DataFormatString="{0}枚">
								<ItemStyle HorizontalAlign="Left" />
							</asp:BoundField>
							<asp:BoundField DataField="USED_DATE" HeaderText="引換日時" DataFormatString="{0:yy/MM/dd HH:mm}">
								<ItemStyle HorizontalAlign="Left" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="ログインID">
								<ItemTemplate>
									<asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>' Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
									</asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="HANDLE_NM" HeaderText="ハンドル名">
								<ItemStyle HorizontalAlign="Left" />
							</asp:BoundField>
							<asp:BoundField DataField="CREATE_DATE" HeaderText="作成日時" DataFormatString="{0:yy/MM/dd HH:mm}">
								<ItemStyle HorizontalAlign="Left" />
							</asp:BoundField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
			<br />
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsVoteTicketCode" runat="server" ConvertNullToDBNull="false" EnablePaging="True" OnSelecting="dsVoteTicketCode_Selecting"
		OnSelected="dsVoteTicketCode_Selected" SelectCountMethod="GetPageCount" SelectMethod="GetPageCollection" SortParameterName="" TypeName="VoteTicketCode">
		<SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
