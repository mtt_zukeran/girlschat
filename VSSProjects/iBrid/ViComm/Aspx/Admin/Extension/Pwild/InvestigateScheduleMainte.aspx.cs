﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: この娘を探せ・スケジュール設定
--	Progaram ID		: InvestigateScheduleMainte
--
--  Creation Date	: 2015.02.26
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using iBridCommLib;
using System.Drawing;

public partial class Extension_Pwild_InvestigateScheduleMainte:System.Web.UI.Page {
	protected static readonly string[] MinuteArray = new string[] { "00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59" };
	protected static readonly string[] HourArray = new string[] { "00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23" };
	protected static readonly string[] MonthArray = new string[] { "1","2","3","4","5","6","7","8","9","10","11","12" };
	protected string[] YearArray;

	protected string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		private set {
			this.ViewState["SiteCd"] = value;
		}
	}

	protected string TargetMonth {
		get {
			return iBridUtil.GetStringValue(this.ViewState["TargetMonth"]);
		}
		set {
			this.ViewState["TargetMonth"] = value;
		}
	}

	protected int SelectedRowIndex {
		get {
			return int.Parse(iBridUtil.GetStringValue(this.ViewState["SelectedRowIndex"]));
		}
		set {
			this.ViewState["SelectedRowIndex"] = iBridUtil.GetStringValue(value);
		}
	}

	protected string ExecutionDay {
		get {
			return iBridUtil.GetStringValue(this.ViewState["ExecutionDay"]);
		}
		set {
			this.ViewState["ExecutionDay"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		if (!this.IsPostBack) {
			this.SiteCd = this.Request.QueryString["sitecd"];
			this.TargetMonth = this.Request.QueryString["targetmonth"];

			this.InitPage();

			if (!string.IsNullOrEmpty(this.SiteCd) && !string.IsNullOrEmpty(this.TargetMonth)) {
				this.lstSeekSiteCd.SelectedValue = this.SiteCd;
				this.pnlLink.DataBind();
				this.GetList();
				this.pnlList.Visible = true;
			} else {
				this.TargetMonth = DateTime.Today.ToString("yyyy/MM");
				this.pnlLink.DataBind();
				this.GetList();
				this.pnlList.Visible = true;
			}
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		this.TargetMonth = DateTime.Today.ToString("yyyy/MM");
		this.pnlLink.DataBind();
		this.GetList();
		this.pnlList.Visible = true;
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlMainte.Visible = false;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		HiddenField oRevisionNoHiddenField = this.GetGridViewRowControl(this.SelectedRowIndex,"hdrRevisionNo") as HiddenField;

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("INVESTIGATE_SCHEDULE_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.lstSeekSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pEXECUTION_DAY",DbSession.DbType.VARCHAR2,this.ExecutionDay);
			oDbSession.ProcedureInParm("pBOUNTY_POINT",DbSession.DbType.VARCHAR2,this.txtBountyPoint.Text.Trim());
			oDbSession.ProcedureInParm("pFIRST_ANNOUNCE_TIME",DbSession.DbType.VARCHAR2,string.Format("{0}:{1}:00",this.lstFirstHintAnnounceHH.SelectedValue,this.lstFirstHintAnnounceMI.SelectedValue));
			oDbSession.ProcedureInParm("pSECOND_ANNOUNCE_TIME",DbSession.DbType.VARCHAR2,string.Format("{0}:{1}:00",this.lstSecondHintAnnounceHH.SelectedValue,this.lstSecondHintAnnounceMI.SelectedValue));
			oDbSession.ProcedureInParm("pTHIRD_ANNOUNCE_TIME",DbSession.DbType.VARCHAR2,string.Format("{0}:{1}:00",this.lstThirdHintAnnounceHH.SelectedValue,this.lstThirdHintAnnounceMI.SelectedValue));
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,oRevisionNoHiddenField.Value);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.pnlMainte.Visible = false;
		this.GetList();
	}

	protected void btnUpdate_Command(object sender,CommandEventArgs e) {
		int iRowIndex = int.Parse(e.CommandArgument.ToString());

		this.SelectedRowIndex = iRowIndex;
		this.ExecutionDay = iBridUtil.GetStringValue(this.grdInvestigateSchedule.DataKeys[iRowIndex].Values["EXECUTION_DAY"]);

		string[] sFirstHintAnnounce = (this.GetGridViewRowControl("lblFirstHintAnnounceTime") as Label).Text.Split(':');
		string[] sSecondHintAnnounce = (this.GetGridViewRowControl("lblSecondHintAnnounceTime") as Label).Text.Split(':');
		string[] sThirdHintAnnounce = (this.GetGridViewRowControl("lblThirdHintAnnounceTime") as Label).Text.Split(':');

		this.lstFirstHintAnnounceHH.SelectedValue = sFirstHintAnnounce[0];
		this.lstFirstHintAnnounceMI.SelectedValue = sFirstHintAnnounce[1];
		this.lstSecondHintAnnounceHH.SelectedValue = sSecondHintAnnounce[0];
		this.lstSecondHintAnnounceMI.SelectedValue = sSecondHintAnnounce[1];
		this.lstThirdHintAnnounceHH.SelectedValue = sThirdHintAnnounce[0];
		this.lstThirdHintAnnounceMI.SelectedValue = sThirdHintAnnounce[1];
		this.txtBountyPoint.Text = (this.GetGridViewRowControl("lblBountyPoint") as Label).Text;

		this.pnlMainte.Visible = true;
	}

	protected void grdInvestigateSchedule_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType != DataControlRowType.DataRow) {
			return;
		}

		string sExecutionDay = iBridUtil.GetStringValue(DataBinder.Eval(e.Row.DataItem,"EXECUTION_DAY"));

		DateTime oExecutionDay = DateTime.Parse(sExecutionDay);
		if (DateTime.Today == oExecutionDay) {
			e.Row.BackColor = Color.LavenderBlush;
		}
		if (DateTime.Today >= oExecutionDay) {
			Button oUpdateButton = e.Row.FindControl("btnUpdate") as Button;
			if (oUpdateButton != null) {
				oUpdateButton.Enabled = false;
			}
		}
	}

	protected void dsInvestigateSchedule_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pTargetMonth"] = this.TargetMonth;
	}

	protected void dsInvestigateSchedule_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		DataSet oInvestigateScheduleDataSet = e.ReturnValue as DataSet;
		if (oInvestigateScheduleDataSet == null || (oInvestigateScheduleDataSet).Tables[0].Rows.Count == 0) {
			this.lblNoData.Visible = true;
		}
	}

	protected void vdcTime_ServerValidate(object source,ServerValidateEventArgs e) {
		if (this.IsValid) {
			DateTime oFirstHintAnnounceDate;
			DateTime oSecondHintAnnounceDate;
			DateTime oThirdHintAnnounceDate;

			if (!DateTime.TryParse(string.Format("{0} {1}:{2}:00",this.ExecutionDay,this.lstFirstHintAnnounceHH.SelectedValue,this.lstFirstHintAnnounceMI.SelectedValue),out oFirstHintAnnounceDate)) {
				this.vdcTime.ErrorMessage = "第一発表時間を正しく選択して下さい";
				e.IsValid = false;
				return;
			}

			if (!DateTime.TryParse(string.Format("{0} {1}:{2}:00",this.ExecutionDay,this.lstSecondHintAnnounceHH.SelectedValue,this.lstSecondHintAnnounceMI.SelectedValue),out oSecondHintAnnounceDate)) {
				this.vdcTime.ErrorMessage = "第二発表時間を正しく選択して下さい";
				e.IsValid = false;
				return;
			}

			if (!DateTime.TryParse(string.Format("{0} {1}:{2}:00",this.ExecutionDay,this.lstThirdHintAnnounceHH.SelectedValue,this.lstThirdHintAnnounceMI.SelectedValue),out oThirdHintAnnounceDate)) {
				this.vdcTime.ErrorMessage = "第三発表時間を正しく選択して下さい";
				e.IsValid = false;
				return;
			}
		}
	}

	private void InitPage() {
		this.pnlKey.DataBind();
		this.pnlDtl.DataBind();
		this.grdInvestigateSchedule.DataSourceID = string.Empty;
		this.pnlList.Visible = false;
		this.pnlMainte.Visible = false;
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private Control GetGridViewRowControl(int pRowIndex,string pControlName) {
		GridViewRow oGridViewRow = this.grdInvestigateSchedule.Rows[pRowIndex];
		return oGridViewRow.FindControl(pControlName);
	}

	private Control GetGridViewRowControl(string pControlName) {
		GridViewRow oGridViewRow = this.grdInvestigateSchedule.Rows[this.SelectedRowIndex];
		return oGridViewRow.FindControl(pControlName);
	}

	protected Color GetDayOfWeekColor(object pDayOfWeek) {
		switch (iBridUtil.GetStringValue(pDayOfWeek)) {
			case "土":
				return Color.Blue;
			case "日":
				return Color.Red;
			default:
				return Color.Empty;
		}
	}

	protected Color GetMonthLinkColor(object pMonth) {
		if (pMonth.Equals(this.TargetMonth)) {
			return Color.Red;
		} else {
			return Color.Empty;
		}
	}

	protected string GetYear(int pAddYear) {
		DateTime oTargetDay;
		if (!DateTime.TryParse(string.Format("{0}/01",this.TargetMonth),out oTargetDay)) {
			return string.Empty;
		}

		return oTargetDay.AddYears(pAddYear).ToString("yyyy");
	}

	protected string GetHour(object pTime) {
		DateTime oDateTime;
		if (!DateTime.TryParse(string.Format("{0}/01 {1}",this.TargetMonth,pTime),out oDateTime)) {
			return string.Empty;
		}

		return oDateTime.ToString("HH");
	}

	protected string GetMinute(object pTime) {
		DateTime oDateTime;
		if (!DateTime.TryParse(string.Format("{0}/01 {1}",this.TargetMonth,pTime),out oDateTime)) {
			return string.Empty;
		}

		return oDateTime.ToString("mm");
	}

	protected string GetTargetMonthLink(string pTargetMonth) {
		return string.Format("~/Extension/Pwild/InvestigateScheduleMainte.aspx?sitecd={0}&targetmonth={1}",this.lstSeekSiteCd.SelectedValue,pTargetMonth);
	}

	private void GetList() {
		this.grdInvestigateSchedule.DataSourceID = "dsInvestigateSchedule";
		this.grdInvestigateSchedule.DataBind();
	}
}