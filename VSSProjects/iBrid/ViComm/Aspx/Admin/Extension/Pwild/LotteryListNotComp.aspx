﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="LotteryListNotComp.aspx.cs" Inherits="Extension_Pwild_LotteryListNotComp" Title="抽選一覧(非コンプ)"
    ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="抽選一覧(非コンプ)"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 600px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle2">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    Enabled="false" DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton" ValidationGroup="Create"
                    OnClick="btnCreate_Click" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" Visible="false" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlLotteryInfo">
            <fieldset class="fieldset">
                <legend>[抽選情報]</legend>
                <table border="0" style="width: 600px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            抽選ID(SEQ)
                        </td>
                        <td class="tdDataStyle">
                            <asp:Label ID="lblLotteryId" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            抽選名
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtLotteryNm" runat="server" MaxLength="100" Width="300px"></asp:TextBox>
                            <br />
                            <asp:Label ID="lblErrorMessageLotteryNm" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            価格
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtPrice" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
                            <% // 必須チェック %>
                            <asp:RequiredFieldValidator ID="vdrPrice" runat="server" ErrorMessage="価格を入力してください。"
                                ControlToValidate="txtPrice" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                            <ajaxToolkit:ValidatorCalloutExtender ID="vdePrice" runat="Server" TargetControlID="vdrPrice"
                                HighlightCssClass="validatorCallout" />
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            価格1
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtPrice1" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
                            <br />
                            <asp:Label ID="lblErrorMessagePrice1" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            価格2
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtPrice2" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
                            <br />
                            <asp:Label ID="lblErrorMessagePrice2" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            価格3
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtPrice3" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
                            <br />
                            <asp:Label ID="lblErrorMessagePrice3" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                           ガチャ回数
                        </td>
                        <td class="tdDataStyle">
                            セット1：<asp:TextBox ID="txtLotteryCount1" runat="server" MaxLength="10" Width="50px"></asp:TextBox>
                            セット2：<asp:TextBox ID="txtLotteryCount2" runat="server" MaxLength="10" Width="50px"></asp:TextBox>
                            セット3：<asp:TextBox ID="txtLotteryCount3" runat="server" MaxLength="10" Width="50px"></asp:TextBox>
							<br />
							<asp:Label ID="lblErrorMessageLotteryCount1" runat="server" ForeColor="red" Visible="false"></asp:Label>
							<asp:Label ID="lblErrorMessageLotteryCount2" runat="server" ForeColor="red" Visible="false"></asp:Label>
							<asp:Label ID="lblErrorMessageLotteryCount3" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            プレミアアイテム取得数
                        </td>
                        <td class="tdDataStyle">
                            セット1：<asp:TextBox ID="txtPremiumCount1" runat="server" MaxLength="10" Width="50px"></asp:TextBox>
                            セット2：<asp:TextBox ID="txtPremiumCount2" runat="server" MaxLength="10" Width="50px"></asp:TextBox>
                            セット3：<asp:TextBox ID="txtPremiumCount3" runat="server" MaxLength="10" Width="50px"></asp:TextBox>
                            <br />
							<asp:Label ID="lblErrorMessagePremiumCount1" runat="server" ForeColor="red" Visible="false"></asp:Label>
							<asp:Label ID="lblErrorMessagePremiumCount2" runat="server" ForeColor="red" Visible="false"></asp:Label>
							<asp:Label ID="lblErrorMessagePremiumCount3" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
							ガチャセット表示
                        </td>
                        <td class="tdDataStyle">
                            セット1:<asp:CheckBox ID="chkLotteryFlag1" runat="server" />
                            セット2:<asp:CheckBox ID="chkLotteryFlag2" runat="server" />
                            セット3:<asp:CheckBox ID="chkLotteryFlag3" runat="server" />
                        </td>
                    </tr>                  
                    <tr>
                        <td class="tdHeaderStyle">
                            公開開始日時
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtPublishStartDate" runat="server" MaxLength="16" Width="120px"></asp:TextBox>
                            <ajaxToolkit:MaskedEditExtender ID="mskPublishStartDate" runat="server" MaskType="DateTime"
                                Mask="9999/99/99 99:99:99" UserDateFormat="YearMonthDay" UserTimeFormat="TwentyFourHour"
                                ClearMaskOnLostFocus="true" TargetControlID="txtPublishStartDate">
                            </ajaxToolkit:MaskedEditExtender>
                            <br />
                            <asp:Label ID="lblErrorMessagePublishStart" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            公開終了日時
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtPublishEndDate" runat="server" MaxLength="16" Width="120px"></asp:TextBox>
                            <ajaxToolkit:MaskedEditExtender ID="mskPublishEndDate" runat="server" MaskType="DateTime"
                                Mask="9999/99/99 99:99:99" UserDateFormat="YearMonthDay" UserTimeFormat="TwentyFourHour"
                                ClearMaskOnLostFocus="true" TargetControlID="txtPublishEndDate">
                            </ajaxToolkit:MaskedEditExtender>
                            <br />
                            <asp:Label ID="lblErrorMessagePublishEnd" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            初回無料開始日時
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtFirstBuyFreeStartDate" runat="server" MaxLength="16" Width="120px"></asp:TextBox>
                            <ajaxToolkit:MaskedEditExtender ID="mskFirstBuyFreeStartDate" runat="server" MaskType="DateTime"
                                Mask="9999/99/99 99:99:99" UserDateFormat="YearMonthDay" UserTimeFormat="TwentyFourHour"
                                ClearMaskOnLostFocus="true" TargetControlID="txtFirstBuyFreeStartDate">
                            </ajaxToolkit:MaskedEditExtender>
                            <br />
                            <asp:Label ID="lblErrorMessageFreeStart" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            初回無料終了日時
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtFirstBuyFreeEndDate" runat="server" MaxLength="16" Width="120px"></asp:TextBox>
                            <ajaxToolkit:MaskedEditExtender ID="mskFirstBuyFreeEndDate" runat="server" MaskType="DateTime"
                                Mask="9999/99/99 99:99:99" UserDateFormat="YearMonthDay" UserTimeFormat="TwentyFourHour"
                                ClearMaskOnLostFocus="true" TargetControlID="txtFirstBuyFreeEndDate">
                            </ajaxToolkit:MaskedEditExtender>
                            <br />
                            <asp:Label ID="lblErrorMessageFreeEnd" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="pnlCommand" runat="server">
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Update"
                        OnClick="btnUpdate_Click" />
                    <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" ValidationGroup="Delete"
                        OnClick="btnDelete_Click" />
                    <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" CausesValidation="False"
                        Visible="true" OnClick="btnCancel_Click" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[抽選一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdLottery.PageIndex + 1 %>
                        of
                        <%= grdLottery.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="600px">
                    <asp:GridView ID="grdLottery" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        PageSize="30" EnableSortingAndPagingCallbacks="false" DataSourceID="dsLottery"
                        SkinID="GridViewColor" Font-Size="small" AllowSorting="true" DataKeyNames="SITE_CD,LOTTERY_SEQ">
                        <Columns>
                            <asp:TemplateField HeaderText="抽選ID">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkLotteryId" runat="server" CommandArgument='<%# Eval("LOTTERY_SEQ") %>'
                                        Text='<%# Eval("LOTTERY_SEQ") %>' OnCommand="lnkLotteryId_Command"></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="抽選名">
                                <ItemTemplate>
                                    <asp:Label ID="lblLotteryNm" runat="server" Text='<%# string.IsNullOrEmpty(Eval("LOTTERY_NM","{0}")) ? "(未設定)" : Eval("LOTTERY_NM") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="公開開始日時">
                                <ItemTemplate>
                                    <asp:Label ID="lblPublishStartDate" runat="server" Text='<%# Eval("PUBLISH_START_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="公開終了日時">
                                <ItemTemplate>
                                    <asp:Label ID="lblPublishEndDate" runat="server" Text='<%# Eval("PUBLISH_END_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="初回購入無料開始日時">
                                <ItemTemplate>
                                    <asp:Label ID="lblFirstBuyFreeStartDate" runat="server" Text='<%# Eval("FIRST_BUY_FREE_START_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="初回購入無料終了日時">
                                <ItemTemplate>
                                    <asp:Label ID="lblFirstBuyFreeEndDate" runat="server" Text='<%# Eval("FIRST_BUY_FREE_END_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkTownList" runat="server" NavigateUrl='<%# string.Format("~/Extension/Pwild/LotteryGetItemListNotComp.aspx?sitecd={0}&lotteryseq={1}&sexcd={2}", Eval("SITE_CD"), Eval("LOTTERY_SEQ"), Eval("SEX_CD"))%>'
                                        Text="アイテム一覧"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsLottery" runat="server" SelectMethod="GetPageCollection_2"
        ConvertNullToDBNull="true" SortParameterName="" TypeName="Lottery" SelectCountMethod="GetPageCount_2"
        EnablePaging="True" OnSelected="dsLottery_Selected">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItemCategory" runat="server" SelectMethod="GetList"
        TypeName="GameItemCategory">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtPrice" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtPrice1" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtPrice2" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtPrice3" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtLotteryCount1" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtLotteryCount2" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtLotteryCount3" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtPremiumCount1" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtPremiumCount2" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtPremiumCount3" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="抽選情報を更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete"
        ConfirmText="抽選情報を削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>
