﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastPageViewDailyList.aspx.cs" Inherits="Extension_Pwild_CastPageViewDailyList" Title="出演者ページビュー(日別)" %>
<%@ Import Namespace="ViComm" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="出演者ページビュー(日別)"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[検索条件]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 800px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="tdHeaderStyle">
                                アクセス種別
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstAccessType" runat="server">
                                    <asp:ListItem Value="1" Text="ｱｸｾｽ総数"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="ｱｸｾｽUU"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="1stｱｸｾｽ"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="2ndｱｸｾｽ"></asp:ListItem>
                                    <asp:ListItem Value="5" Text="3rdｱｸｾｽ"></asp:ListItem>
                                    <asp:ListItem Value="6" Text="4thｱｸｾｽ"></asp:ListItem>
                                    <asp:ListItem Value="7" Text="5thｱｸｾｽ"></asp:ListItem>
                                    <asp:ListItem Value="8" Text="1st～5thｱｸｾｽ"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                アクセス年月
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstYear" runat="server" Width="60px">
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstMonth" runat="server" Width="40px">
                                </asp:DropDownList>月
                            </td>
                            <td class="tdHeaderStyle">
                                ログイン種別
                            </td>
                            <td class="tdDataStyle">
								<asp:DropDownList ID="lstLoginType" runat="server">
								    <asp:ListItem Value="" Text="指定しない"></asp:ListItem>
								    <asp:ListItem Value="0" Text="登録前"></asp:ListItem>
								    <asp:ListItem Value="1" Text="登録後"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                端末種別
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstPageUserAgentType" runat="server">
                                    <asp:ListItem Value="" Text="指定しない"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="3G端末"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="ｽﾏｰﾄﾌｫﾝ"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="Android"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="iPhone"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="tdHeaderStyle">
                                0アクセス
                            </td>
                            <td class="tdDataStyle">
                                <asp:RadioButtonList ID="rdoZeroAccessNotDisp" runat="server" RepeatDirection="horizontal">
                                    <asp:ListItem Text="非表示" Value="1" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="表示" Value="0"></asp:ListItem>
							    </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                精算履歴
                            </td>
                            <td class="tdDataStyle">
                                <asp:RadioButtonList ID="rdoPaymentFlag" runat="server" RepeatDirection="horizontal">
                                    <asp:ListItem Text="指定しない" Value="" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="精算有" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="精算無" Value="0"></asp:ListItem>
							    </asp:RadioButtonList>
                            </td>
                            <td class="tdHeaderStyle">
                                新人フラグ
                            </td>
                            <td class="tdDataStyle">
                                <asp:RadioButtonList ID="rdoNewUserFlag" runat="server" RepeatDirection="horizontal">
                                    <asp:ListItem Text="指定しない" Value="" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="有" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="無" Value="0"></asp:ListItem>
							    </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle2">
                                ユーザー端末
                            </td>
                            <td class="tdDataStyle2" colspan="3">
                                <asp:DropDownList ID="lstCarrierType" runat="server">
                                    <asp:ListItem Value="" Text="指定しない"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="3G端末"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Android"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="iPhone"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" />
				<asp:Button runat="server" ID="btnCsv" Text="ＣＳＶ出力" CssClass="seekbutton" OnClick="btnCSV_Click" />
            </fieldset>
        </asp:Panel>
		<asp:Panel runat="server" ID="pnlList">
			<fieldset>
				<legend>[ページビュー]</legend>
				<asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdCastPageViewDaily.PageIndex + 1%>
                        of
                        <%= grdCastPageViewDaily.PageCount%>
                    </a>
                </asp:Panel>
                &nbsp;<br />
				<asp:Panel ID="pnlGrid" runat="server">
					<asp:GridView ID="grdCastPageViewDaily" DataSourceID="dsCastPageViewDaily" runat="server" AllowPaging="true" AutoGenerateColumns="False"  PageSize="50"
						EnableSortingAndPagingCallbacks="false" ShowFooter="true" SkinID="GridViewColor" AllowSorting="true" OnRowDataBound="grdCastPageViewDaily_RowDataBound">
						<Columns>
							<asp:TemplateField HeaderText="画面名称">
								<ItemTemplate>
									<asp:HyperLink ID="lnkProgramNm" runat="server" NavigateUrl='<%# GetNavigateUrlPageMainte(Eval("PROGRAM_ROOT"),Eval("PROGRAM_ID"),Eval("HTML_DOC_TYPE")) %>' Text='<%# GetPageNm(Eval("PROGRAM_ROOT"),Eval("HTML_DOC_TYPE"),Eval("PROGRAM_NM")) %>' ></asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="期間">
								<ItemTemplate>
									<asp:HyperLink ID="lnkTerm" runat="server" NavigateUrl='<%# GetNavigateUrl(Eval("PROGRAM_ROOT"),Eval("PROGRAM_ID"),Eval("HTML_DOC_TYPE")) %>' Text='■'>
									</asp:HyperLink>
									<ItemStyle HorizontalAlign="Center" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="TOTAL_COUNT" HeaderText="合計">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount01" runat="server" Text="1" NavigateUrl='<%# GetNavigateUrlDaily("01") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount01" runat="server" Text='<%# Eval("COUNT01") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount02" runat="server" Text="2" NavigateUrl='<%# GetNavigateUrlDaily("02") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount02" runat="server" Text='<%# Eval("COUNT02") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount03" runat="server" Text="3" NavigateUrl='<%# GetNavigateUrlDaily("03") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount03" runat="server" Text='<%# Eval("COUNT03") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount04" runat="server" Text="4" NavigateUrl='<%# GetNavigateUrlDaily("04") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount04" runat="server" Text='<%# Eval("COUNT04") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount05" runat="server" Text="5" NavigateUrl='<%# GetNavigateUrlDaily("05") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount05" runat="server" Text='<%# Eval("COUNT05") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount06" runat="server" Text="6" NavigateUrl='<%# GetNavigateUrlDaily("06") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount06" runat="server" Text='<%# Eval("COUNT06") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount07" runat="server" Text="7" NavigateUrl='<%# GetNavigateUrlDaily("07") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount07" runat="server" Text='<%# Eval("COUNT07") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount08" runat="server" Text="8" NavigateUrl='<%# GetNavigateUrlDaily("08") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount08" runat="server" Text='<%# Eval("COUNT08") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount09" runat="server" Text="9" NavigateUrl='<%# GetNavigateUrlDaily("09") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount09" runat="server" Text='<%# Eval("COUNT09") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount10" runat="server" Text="10" NavigateUrl='<%# GetNavigateUrlDaily("10") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount10" runat="server" Text='<%# Eval("COUNT10") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount11" runat="server" Text="11" NavigateUrl='<%# GetNavigateUrlDaily("11") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount11" runat="server" Text='<%# Eval("COUNT11") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount12" runat="server" Text="12" NavigateUrl='<%# GetNavigateUrlDaily("12") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount12" runat="server" Text='<%# Eval("COUNT12") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount13" runat="server" Text="13" NavigateUrl='<%# GetNavigateUrlDaily("13") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount13" runat="server" Text='<%# Eval("COUNT13") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount14" runat="server" Text="14" NavigateUrl='<%# GetNavigateUrlDaily("14") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount14" runat="server" Text='<%# Eval("COUNT14") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount15" runat="server" Text="15" NavigateUrl='<%# GetNavigateUrlDaily("15") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount15" runat="server" Text='<%# Eval("COUNT15") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount16" runat="server" Text="16" NavigateUrl='<%# GetNavigateUrlDaily("16") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount16" runat="server" Text='<%# Eval("COUNT16") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount17" runat="server" Text="17" NavigateUrl='<%# GetNavigateUrlDaily("17") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount17" runat="server" Text='<%# Eval("COUNT17") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount18" runat="server" Text="18" NavigateUrl='<%# GetNavigateUrlDaily("18") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount18" runat="server" Text='<%# Eval("COUNT18") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount19" runat="server" Text="19" NavigateUrl='<%# GetNavigateUrlDaily("19") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount19" runat="server" Text='<%# Eval("COUNT19") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount20" runat="server" Text="20" NavigateUrl='<%# GetNavigateUrlDaily("20") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount20" runat="server" Text='<%# Eval("COUNT20") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount21" runat="server" Text="21" NavigateUrl='<%# GetNavigateUrlDaily("21") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount21" runat="server" Text='<%# Eval("COUNT21") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount22" runat="server" Text="22" NavigateUrl='<%# GetNavigateUrlDaily("22") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount22" runat="server" Text='<%# Eval("COUNT22") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount23" runat="server" Text="23" NavigateUrl='<%# GetNavigateUrlDaily("23") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount23" runat="server" Text='<%# Eval("COUNT23") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount24" runat="server" Text="24" NavigateUrl='<%# GetNavigateUrlDaily("24") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount24" runat="server" Text='<%# Eval("COUNT24") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount25" runat="server" Text="25" NavigateUrl='<%# GetNavigateUrlDaily("25") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount25" runat="server" Text='<%# Eval("COUNT25") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount26" runat="server" Text="26" NavigateUrl='<%# GetNavigateUrlDaily("26") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount26" runat="server" Text='<%# Eval("COUNT26") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount27" runat="server" Text="27" NavigateUrl='<%# GetNavigateUrlDaily("27") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount27" runat="server" Text='<%# Eval("COUNT27") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount28" runat="server" Text="28" NavigateUrl='<%# GetNavigateUrlDaily("28") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount28" runat="server" Text='<%# Eval("COUNT28") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount29" runat="server" Text="29" NavigateUrl='<%# GetNavigateUrlDaily("29") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount29" runat="server" Text='<%# Eval("COUNT29") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount30" runat="server" Text="30" NavigateUrl='<%# GetNavigateUrlDaily("30") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount30" runat="server" Text='<%# Eval("COUNT30") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<HeaderTemplate>
									<asp:HyperLink ID="lnkCount31" runat="server" Text="31" NavigateUrl='<%# GetNavigateUrlDaily("31") %>'></asp:HyperLink>
								</HeaderTemplate>
								<ItemTemplate>
									<asp:Label ID="lblCount31" runat="server" Text='<%# Eval("COUNT31") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</asp:GridView>
				</asp:Panel>
			</fieldset>
			<br />
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsCastPageViewDaily" runat="server" ConvertNullToDBNull="false" EnablePaging="true" OnSelecting="dsCastPageViewDaily_Selecting" OnSelected="dsCastPageViewDaily_Selected"
		SelectMethod="GetPageCollection" SelectCountMethod="GetPageCount" SortParameterName="" TypeName="CastPageViewDaily">
		<SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
</asp:Content>

