<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MailLotteryMainte.aspx.cs" Inherits="Extension_Pwild_MailLotteryMainte" Title="メールdeガチャ設定一覧" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="メールdeガチャ設定一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlSearch">
			<fieldset class="fieldset">
				<legend>[検索]</legend>
				<asp:Panel ID="pnlSeekCondition" runat="server">
					<table border="0" style="width: 800px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle2">
								サイトコード
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px">
								</asp:DropDownList>
							</td>
						</tr>
					</table>
				</asp:Panel>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Seek" />
				<asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton"  OnClick="btnCreate_Click" ValidationGroup="Create" />
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlInput">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Label ID="lblErrorMessage" runat="server" Text="" ForeColor="red"></asp:Label>
				<table border="0" style="width: 1000px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							開始日時
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstStartDateYYYY" runat="server" Width="60px" DataSource='<%# YearArray %>'>
							</asp:DropDownList>年
							<asp:DropDownList ID="lstStartDateMM" runat="server" Width="40px" DataSource='<%# MonthArray %>'>
							</asp:DropDownList>月
							<asp:DropDownList ID="lstStartDateDD" runat="server" Width="40px" DataSource='<%# DayArray %>'>
							</asp:DropDownList>日
							<asp:DropDownList ID="lstStartDateHH" runat="server" Width="40px" DataSource='<%# HourArray %>'>
							</asp:DropDownList>時
							<asp:DropDownList ID="lstStartDateMI" runat="server" DataSource='<%# MinuteArray %>'>
							</asp:DropDownList>分
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							エントリー終了日時
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstEndDateYYYY" runat="server" Width="60px" DataSource='<%# YearArray %>'>
							</asp:DropDownList>年
							<asp:DropDownList ID="lstEndDateMM" runat="server" Width="40px" DataSource='<%# MonthArray %>'>
							</asp:DropDownList>月							<asp:DropDownList ID="lstEndDateDD" runat="server" Width="40px" DataSource='<%# DayArray %>'>
							</asp:DropDownList>日
							<asp:DropDownList ID="lstEndDateHH" runat="server" Width="40px" DataSource='<%# HourArray %>'>
							</asp:DropDownList>時							<asp:DropDownList ID="lstEndDateMI" runat="server" DataSource='<%# MinuteArray %>'>
							</asp:DropDownList>分
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							分割チケット必要枚数
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtNeedSecondLotteryCount" runat="server" Width="40px"></asp:TextBox>
							<asp:RequiredFieldValidator ID="vdeNeedSecondLotteryCount" runat="server" ErrorMessage="分割チケット必要枚数を正しく入力して下さい" ControlToValidate="txtNeedSecondLotteryCount" ValidationGroup="Update">*</asp:RequiredFieldValidator>
							<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender33" TargetControlID="vdeNeedSecondLotteryCount" HighlightCssClass="validatorCallout" />
							<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtNeedSecondLotteryCount" />
						</td>
					</tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            備考
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtRemarks" runat="server" MaxLength="1000" Width="650px" Rows="3"
                                TextMode="MultiLine"></asp:TextBox>
                            <asp:CustomValidator ID="vdcRemarks" runat="server" ControlToValidate="txtRemarks" 
                                ErrorMessage='<%# string.Format("備考は{0}文字以内で入力してください",txtRemarks.MaxLength) %>' ValidationGroup="Detail" OnServerValidate="tdcRemarks_ServerValidate">*</asp:CustomValidator>
                            <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender2"
                                TargetControlID="vdcRemarks" HighlightCssClass="validatorCallout" />
                        </td>
                    </tr>
				</table>
				<asp:Panel ID="pnlCommand" runat="server" Width="660px">
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" />
					<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" />
					<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="delBtnStyle" OnClick="btnDelete_Click" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlList">
			<fieldset>
				<legend>[一覧]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">
						Record Count <%# GetRecCount() %>
					</a>
					<br />
					<a class="reccount">
						Current viewing page <%= grdMailLottery.PageIndex + 1%> of <%= grdMailLottery.PageCount%>
					</a>
				</asp:Panel>
				&nbsp;
				<asp:Panel ID="pnlGrid" runat="server">
					<asp:GridView ID="grdMailLottery" DataSourceID="dsMailLottery" runat="server" AllowPaging="True" AutoGenerateColumns="False" PageSize="30" EnableSortingAndPagingCallbacks="false" SkinID="GridViewColor" AllowSorting="true">
						<Columns>
							<asp:TemplateField HeaderText="開始日時">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkStartDate" runat="server" CausesValidation="False" CommandArgument='<%# Container.DataItemIndex %>'
                                        OnCommand="lnkEdit_Command" Text='<%# Eval("START_DATE", "{0:yyyy/MM/dd HH:mm}") %>' >
                                    </asp:LinkButton>
									<asp:HiddenField ID="hdnMailLotterySeq" runat="server" Value='<%# Eval("MAIL_LOTTERY_SEQ") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
							<asp:BoundField DataField="END_DATE" HeaderText="終了日時" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
								<ItemStyle HorizontalAlign="Left" Wrap="false" />
							</asp:BoundField>
                            <asp:TemplateField HeaderText="分割ﾁｹｯﾄ<br>必要枚数">
                                <ItemTemplate>
                                    <asp:Label ID="lblNeedSecondLotteryCount" runat="server" Text='<%# Eval("NEED_SECOND_LOTTERY_COUNT") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Wrap="true"  />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="備考">
                                <ItemTemplate>
                                    <asp:Label ID="lblRemarks" runat="server" Text='<%# SubStringRemarks(Eval("REMARKS")) %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Wrap="true"  />
                            </asp:TemplateField>
							<asp:TemplateField>
								<ItemTemplate>
									<asp:HyperLink ID="lnkMailLotteryRate" runat="server" NavigateUrl='<%# string.Format("~/Extension/Pwild/MailLotteryRateFirstList.aspx?sitecd={0}&maillotteryseq={1}",Eval("SITE_CD"),Eval("MAIL_LOTTERY_SEQ")) %>' Text="一次ｶﾞﾁｬ確率設定">
									</asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<ItemTemplate>
									<asp:HyperLink ID="lnkMailLotteryRateSecond" runat="server" NavigateUrl='<%# string.Format("~/Extension/Pwild/MailLotteryRateSecondList.aspx?sitecd={0}&maillotteryseq={1}",Eval("SITE_CD"),Eval("MAIL_LOTTERY_SEQ")) %>' Text="二次ｶﾞﾁｬ確率設定">
									</asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
			<br />
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsMailLottery" runat="server" ConvertNullToDBNull="false" EnablePaging="True" OnSelecting="dsMailLottery_Selecting" OnSelected="dsMailLottery_Selected"
		SelectCountMethod="GetPageCount" SelectMethod="GetPageCollection" SortParameterName="" TypeName="MailLottery">
		<SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>