﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 野球拳ジャンケン画像
--	Progaram ID		: YakyukenJyankenPicList
--
--  Creation Date	: 2013.05.01
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;

public partial class Extension_Pwild_YakyukenJyankenPicList:System.Web.UI.Page {
	private string recCount = string.Empty;

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			chkAuth.Checked = false;
			chkUnAuth.Checked = false;

			// 未認証レコードの検索
			if (iBridUtil.GetStringValue(Request.QueryString["unauth"]).Equals("1")) {
				chkAuth.Checked = false;
				chkUnAuth.Checked = true;
			}
			DataBind();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			lstSiteCd.DataSourceID = "";
			grdYakyukenJyankenPic.PageIndex = 0;
			DataBind();
		}
	}

	protected void lnkAuth_OnCommand(object sender,CommandEventArgs e) {
		string[] arguments = e.CommandArgument.ToString().Split(',');
		string sSiteCd = arguments[0];
		string sCastUserSeq = arguments[1];
		string sCastCharNo = arguments[2];
		string sJyankenType = arguments[3];
		string sRevisionNo = arguments[4];

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("AUTH_YAKYUKEN_JYANKEN_PIC");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,sSiteCd);
			oDbSession.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.VARCHAR2,sCastUserSeq);
			oDbSession.ProcedureInParm("pCAST_CHAR_NO",DbSession.DbType.VARCHAR2,sCastCharNo);
			oDbSession.ProcedureInParm("pJYANKEN_TYPE",DbSession.DbType.VARCHAR2,sJyankenType);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,sRevisionNo);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		lstSiteCd.DataSourceID = "";
		DataBind();
	}

	protected void lnkDelete_OnCommand(object sender,CommandEventArgs e) {
		string[] arguments = e.CommandArgument.ToString().Split(',');
		string sSiteCd = arguments[0];
		string sCastUserSeq = arguments[1];
		string sCastCharNo = arguments[2];
		string sJyankenType = arguments[3];
		string sRevisionNo = arguments[4];

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("DELETE_YAKYUKEN_JYANKEN_PIC");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,sSiteCd);
			oDbSession.ProcedureInParm("pCAST_USER_SEQ",DbSession.DbType.VARCHAR2,sCastUserSeq);
			oDbSession.ProcedureInParm("pCAST_CHAR_NO",DbSession.DbType.VARCHAR2,sCastCharNo);
			oDbSession.ProcedureInParm("pJYANKEN_TYPE",DbSession.DbType.VARCHAR2,sJyankenType);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,sRevisionNo);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		lstSiteCd.DataSourceID = "";
		grdYakyukenJyankenPic.PageIndex = 0;
		DataBind();
	}

	protected void dsYakyukenJyankenPic_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		YakyukenJyankenPic.SearchCondition oSearchCondition = new YakyukenJyankenPic.SearchCondition();
		oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
		oSearchCondition.LoginId = this.txtLoginId.Text.Trim();
		// 認証状態
		string sAuth = string.Empty;
		if (chkAuth.Checked && !chkUnAuth.Checked) {
			sAuth = ViCommConst.FLAG_ON_STR;
		} else if (!chkAuth.Checked && chkUnAuth.Checked) {
			sAuth = ViCommConst.FLAG_OFF_STR;
		}
		oSearchCondition.AuthFlag = sAuth;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsYakyukenJyankenPic_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void grdYakyukenJyankenPic_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			string sAuthFlag = DataBinder.Eval(e.Row.DataItem,"AUTH_FLAG").ToString();

			if (sAuthFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
				e.Row.BackColor = Color.LavenderBlush;
			} else {
				e.Row.BackColor = Color.White;
			}
		}
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected string GetJyankenTypeNm(object pJyankenType) {
		string sValue = string.Empty;
		string sJyankenType = iBridUtil.GetStringValue(pJyankenType);

		switch (sJyankenType) {
			case "1":
				sValue = "グー";
				break;
			case "2":
				sValue = "チョキ";
				break;
			case "3":
				sValue = "パー";
				break;
		}

		return sValue;
	}

	protected string GetClientScript(object pPicUrl) {
		string sRoot = Request.Url.Authority + ConfigurationManager.AppSettings["Root"];
		return string.Format("javascript:win=window.open('http://{1}/ViewPic.html?url={0}','PictureZoom','width=300,height=320,resizable=yes,directories=no,scrollbars=no' , false);win.focus();return false;",pPicUrl,sRoot);
	}
}
