<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="InvestigateEntSummaryList.aspx.cs" Inherits="Extension_Pwild_InvestigateEntSummaryList"
    Title="この娘を探せ - 男性エントリー(合計)" ValidateRequest="false" %>

<%@ Import Namespace="System.Data" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="この娘を探せ - 男性エントリー(合計)"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset>
                <legend>[設定]</legend>
                <table border="0" style="width: 640px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle2">
                            サイトコード
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="180px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    CausesValidation="False" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" />
                <asp:Button runat="server" ID="btnCsv" Text="CSV出力" CssClass="seekbutton" OnClick="btnCsv_Click"
                    CausesValidation="False" />
            </fieldset>
        </asp:Panel>
        <asp:Panel ID="pnlList" runat="server" ScrollBars="auto">
            <fieldset>
                <legend>[男性エントリー(合計)]</legend>
                <asp:Panel runat="server" ID="pnlLink">
                    <asp:HyperLink runat="server" ID="lnkLastYear" Text='<%# GetYear(-1) + "年へ" %>' NavigateUrl='<%# GetTargetMonthLink(string.Format("{0}/12", GetYear(-1))) %>'></asp:HyperLink>
                    &nbsp;
                    <asp:Repeater runat="server" ID="rptMonthLink" DataSource='<%# MonthList %>'>
                        <ItemTemplate>
                            <asp:HyperLink runat="server" ID="lnkMonth" Text='<%# Container.DataItem + "月" %>'
                                ForeColor='<%# GetMonthLinkColor(string.Format("{0}/{1:D2}", GetYear(0), Int32.Parse(Container.DataItem.ToString()))) %>'
                                NavigateUrl='<%# GetTargetMonthLink(string.Format("{0}/{1:D2}", GetYear(0), Int32.Parse(Container.DataItem.ToString()))) %>'></asp:HyperLink>
                            &nbsp;
                        </ItemTemplate>
                    </asp:Repeater>
                    <asp:HyperLink runat="server" ID="lnkNextYear" Text='<%# GetYear(1) + "年へ" %>' NavigateUrl='<%# GetTargetMonthLink(string.Format("{0}/01", GetYear(1))) %>'></asp:HyperLink>
                    <br />
                </asp:Panel>
                <asp:Label runat="server" ID="lblNoData" Text="登録されたデータがありません。" ForeColor="red" Visible="false"></asp:Label>
                <asp:GridView ID="grdInvestigateEntSummary" runat="server" AutoGenerateColumns="False"
                    DataSourceID="dsInvestigateEntSummary" SkinID="GridViewColor" OnRowDataBound="grdInvestigateEntSummary_RowDataBound"
                    DataKeyNames="SITE_CD,EXECUTION_DAY">
                    <Columns>
                        <asp:TemplateField HeaderText="日">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblDay" Text='<%# Eval("DAY") %>' />
                                (<asp:Label runat="server" ID="lblFormattedDay" Text='<%# Eval("DAY_OF_WEEK") %>'
                                    ForeColor='<%# GetDayOfWeekColor(Eval("DAY_OF_WEEK")) %>' />)
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="参加数">
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="lnkEntryCount" Text='<%# Eval("ENTRY_COUNT") %>'
                                    NavigateUrl='<%# GetInvestigateEntrantLink(Eval("EXECUTION_DAY"), string.Empty, string.Empty) %>'></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="発見数">
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="lnkCaughtCount" Text='<%# Eval("CAUGHT_COUNT") %>'
                                    NavigateUrl='<%# GetInvestigateEntrantLink(Eval("EXECUTION_DAY"), "1", string.Empty) %>'></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="口数">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblPointDivisor" Text='<%# Eval("POINT_DIVISOR") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="pt獲得数">
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="lnkPointAcquiredCount" Text='<%# Eval("POINT_ACQUIRED_COUNT") %>'
                                    NavigateUrl='<%# GetInvestigateEntrantLink(Eval("EXECUTION_DAY"), "1", "1") %>'></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="付与pt(全体)">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblPointPerEntrant" Text='<%# string.Format("{0}({1})", Eval("POINT_PER_ENTRANT"), Eval("BOUNTY_POINT")) %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </fieldset>
            <fieldset class="fieldset">
				<legend>[月間情報]</legend>
				<asp:Panel runat="server" ID="Panel1">
					<table border="0" style="width: 740px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle">
								ﾕﾆｰｸの参加数
							</td>
							<td class="tdHeaderStyle">
								発見数
							</td>
							<td class="tdHeaderStyle">
								発見口数
							</td>
							<td class="tdHeaderStyle">
								1口以上の参加者数
							</td>
							<td class="tdHeaderStyle">
								ﾎﾟｲﾝﾄ獲得者数
							</td>
							<td class="tdHeaderStyle">
								1口当たりの付与Pt
							</td>
						</tr>
						<tr>
							<td class="tdDataStyle" align="right">
								<asp:Label ID="lblUniqueUser" runat="server"></asp:Label>
							</td>
							<td class="tdDataStyle" align="right">
								<asp:Label ID="lblCaughtCount" runat="server"></asp:Label>
							</td>
							<td class="tdDataStyle" align="right">
								<asp:Label ID="lblCompleteCount" runat="server"></asp:Label>
							</td>
							<td class="tdDataStyle" align="right">
								<asp:Label ID="lblCompleteUserCount" runat="server"></asp:Label>
							</td>
							<td class="tdDataStyle" align="right">
								<asp:Label ID="lblAcquiredUserCount" runat="server"></asp:Label>
							</td>
							<td class="tdDataStyle" align="right">
								<asp:Label ID="lblBountyPointPerSheet" runat="server"></asp:Label>
							</td>
						</tr>
					</table>
				</asp:Panel>
			</fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsInvestigateEntSummary" runat="server" SelectMethod="GetList"
        OnSelecting="dsInvestigateEntSummary_Selecting" OnSelected="dsInvestigateEntSummary_Selected"
        TypeName="InvestigateEntSummary">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSeekSiteCd" DbType="String" PropertyName="SelectedValue"
                Name="pSiteCd" />
            <asp:Parameter Name="pTargetMonth" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
