﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メール返信ボーナス期間設定一覧(2段階設定)
--	Progaram ID		: MailResBonusList2
--  Creation Date	: 2016.09.02
--  Creater			: M&TT Zukeran
**************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using ViComm.Extension.Pwild;

public partial class Extension_Pwild_MailResBonusList2:System.Web.UI.Page {
	/// <summary>
	/// 選択肢の開始年
	/// </summary>
	private const int START_YEAR = 2016;

	/// <summary>
	/// 選択肢の終了年 (未来年を表示する数)
	/// </summary>
	private const int FUTURE_YEAR = 1;

	private string recCount = string.Empty;
	protected string[] YearArray;
	protected string[] MonthArray;
	protected string[] DayArray;
	protected string[] HourArray;
	protected string[] MinuteArray;

	private string MailResBonusSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MAIL_RES_BONUS_SEQ"]);
		}
		set {
			this.ViewState["MAIL_RES_BONUS_SEQ"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["REVISION_NO"]);
		}
		set {
			this.ViewState["REVISION_NO"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);

			if (string.IsNullOrEmpty(sSiteCd)) {
				if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
					lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
				} else {
					lstSiteCd.SelectedIndex = 0;
				}
			} else {
				lstSiteCd.SelectedValue = sSiteCd;
			}

			this.pnlSearch.Enabled = true;
			this.pnlInput.Visible = false;

			lstSiteCd.DataBind();
			pnlGrid.DataBind();
			pnlCount.DataBind();

			SetDateList();
			pnlInput.DataBind();
		}
	}

	/// <summary>
	/// 日付リストの選択肢を設定
	/// </summary>
	private void SetDateList() {
		// 開始日時の選択肢を設定
		SysPrograms.SetYearList(lstStartDateYYYY,START_YEAR,FUTURE_YEAR,true);
		SysPrograms.SetMonthList(lstStartDateMM);
		SysPrograms.SetDayList(lstStartDateDD);
		SysPrograms.SetHourList(lstStartDateHH);
		SysPrograms.SetMinuteList(lstStartDateMI);
		// 終了日時の選択肢を設定
		SysPrograms.SetYearList(lstEndDateYYYY,START_YEAR,FUTURE_YEAR,true);
		SysPrograms.SetMonthList(lstEndDateMM);
		SysPrograms.SetDayList(lstEndDateDD);
		SysPrograms.SetHourList(lstEndDateHH);
		SysPrograms.SetMinuteList(lstEndDateMI);
	}

	/// <summary>
	/// 設定欄の入力値、エラー表示を初期化
	/// </summary>
	private void ClearFileds() {
		this.lblErrorMessage.Text = string.Empty;

		// メール未受信日数
		this.txtUnreceivedDays.Text = string.Empty;
		// 1通目の返信期限秒数
		this.txtResLimitSec1.Text = string.Empty;
		// 2通目以降の返信期限秒数
		this.txtResLimitSec2.Text = string.Empty;
		// ボーナス付与最大返信数
		this.txtResBonusMaxNum.Text = string.Empty;

		// 開始日時
		this.lstStartDateYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstStartDateMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstStartDateDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstStartDateHH.SelectedValue = "00";
		this.lstStartDateMI.SelectedValue = "00";

		// 終了日時
		this.lstEndDateYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstEndDateMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstEndDateDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstEndDateHH.SelectedValue = "23";
		this.lstEndDateMI.SelectedValue = "59";
	}

	/// <summary>
	/// 入力された開始日時を取得
	/// </summary>
	/// <returns></returns>
	private string GetStartDate() {
		return string.Format(
			"{0}/{1}/{2} {3}:{4}:00",
			lstStartDateYYYY.SelectedValue,
			lstStartDateMM.SelectedValue,
			lstStartDateDD.SelectedValue,
			lstStartDateHH.SelectedValue,
			lstStartDateMI.SelectedValue
		);
	}

	/// <summary>
	/// 入力された終了日時を取得
	/// </summary>
	/// <returns></returns>
	private string GetEndDate() {
		return string.Format(
			"{0}/{1}/{2} {3}:{4}:59",
			lstEndDateYYYY.SelectedValue,
			lstEndDateMM.SelectedValue,
			lstEndDateDD.SelectedValue,
			lstEndDateHH.SelectedValue,
			lstEndDateMI.SelectedValue
		);
	}

	/// <summary>
	/// 数値フィールド値チェック
	/// </summary>
	/// <param name="sText"></param>
	/// <param name="sName"></param>
	/// <param name="iResult"></param>
	/// <param name="sErrMsg"></param>
	/// <returns></returns>
	private bool CheckIntFieldValue(string sText,string sName,out int iResult,out string sErrMsg) {
		iResult = 0;
		sErrMsg = "";

		if (String.IsNullOrEmpty(sText)) {
			sErrMsg = String.Format("{0}を入力してください",sName);
			return false;
		}
		if (!int.TryParse(sText,out iResult)) {
			sErrMsg = String.Format("{0}を正しく入力してください",sName);
			return false;
		}

		return true;
	}

	/// <summary>
	/// 入力値チェック
	/// </summary>
	/// <returns></returns>
	private bool CheckInput() {
		StringBuilder errorMessage = new StringBuilder();
		string sErrorMessage;

		int iResult;

		if (!CheckIntFieldValue(txtUnreceivedDays.Text,"メール未受信日数",out iResult,out sErrorMessage)) {
			errorMessage.AppendLine(sErrorMessage);
		}
		if (!CheckIntFieldValue(txtResLimitSec1.Text,"1通目の返信期限秒数",out iResult,out sErrorMessage)) {
			errorMessage.AppendLine(sErrorMessage);
		}
		if (!CheckIntFieldValue(txtResLimitSec2.Text,"2通目以降の返信期限秒数",out iResult,out sErrorMessage)) {
			errorMessage.AppendLine(sErrorMessage);
		}
		if (!CheckIntFieldValue(txtResBonusMaxNum.Text,"ボーナス付与最大返信数",out iResult,out sErrorMessage)) {
			errorMessage.AppendLine(sErrorMessage);
		}

		DateTime dtStartDate = new DateTime();
		DateTime dtEndDate = new DateTime();
		bool bStartDate = true;
		bool bEndDate = true;

		if (!(bStartDate = DateTime.TryParse(GetStartDate(),out dtStartDate))) {
			errorMessage.AppendLine("開始日時が正しくありません");
		}

		if (!(bEndDate = DateTime.TryParse(GetEndDate(),out dtEndDate))) {
			errorMessage.AppendLine("終了日時が正しくありません");
		}

		if (bStartDate && bEndDate && dtStartDate > dtEndDate) {
			errorMessage.AppendLine("期間の大小関係が正しくありません");
		}

		// エラーメッセージを設定
		if (errorMessage.Length > 0) {
			this.lblErrorMessage.Text = errorMessage.ToString().Replace("\n", "<br />");
			return false;
		}

		return true;
	}

	/// <summary>
	/// データ取得
	/// </summary>
	private void GetData() {
		// 検索条件を設定
		MailResBonus2.SearchCondition oSearchCondition = new MailResBonus2.SearchCondition();
		oSearchCondition.SiteCd = lstSiteCd.SelectedValue;
		oSearchCondition.MailResBonusSeq = this.MailResBonusSeq;

		// データ取得
		MailResBonus2 oMailResBonus2 = new MailResBonus2();
		DataRow dr = oMailResBonus2.GetData(oSearchCondition);

		// 1通目の返信期限秒数
		txtResLimitSec1.Text = dr["RES_LIMIT_SEC1"].ToString();
		// 2通目以降の返信期限秒数
		txtResLimitSec2.Text = dr["RES_LIMIT_SEC2"].ToString();
		// ボーナス付与最大返信数
		txtResBonusMaxNum.Text = dr["RES_BONUS_MAX_NUM"].ToString();
		// メール未受信日数
		txtUnreceivedDays.Text = dr["UNRECEIVED_DAYS"].ToString();
		// 開始日時
		DateTime dtStartDate = (DateTime)dr["START_DATE"];
		lstStartDateYYYY.SelectedValue = dtStartDate.ToString("yyyy");
		lstStartDateMM.SelectedValue = dtStartDate.ToString("MM");
		lstStartDateDD.SelectedValue = dtStartDate.ToString("dd");
		lstStartDateHH.SelectedValue = dtStartDate.ToString("HH");
		lstStartDateMI.SelectedValue = dtStartDate.ToString("mm");
		// 終了日時
		DateTime dtEndDate = (DateTime)dr["END_DATE"];
		lstEndDateYYYY.SelectedValue = dtEndDate.ToString("yyyy");
		lstEndDateMM.SelectedValue = dtEndDate.ToString("MM");
		lstEndDateDD.SelectedValue = dtEndDate.ToString("dd");
		lstEndDateHH.SelectedValue = dtEndDate.ToString("HH");
		lstEndDateMI.SelectedValue = dtEndDate.ToString("mm");
		// リビジョンNo
		this.RevisionNo = dr["REVISION_NO"].ToString();
	}

	/// <summary>
	/// メール返信ボーナス期間のデータ反映
	/// </summary>
	/// <param name="pDeleteFlag"></param>
	/// <param name="pResult"></param>
	private void MailResBonusMainte(int pDeleteFlag,out string pResult) {
		DateTime dtStartDate = DateTime.Parse(GetStartDate());
		DateTime dtEndDate = DateTime.Parse(GetEndDate());

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("MAIL_RES_BONUS2_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pMAIL_RES_BONUS_SEQ",DbSession.DbType.NUMBER,this.MailResBonusSeq);
			oDbSession.ProcedureInParm("pSTART_DATE",DbSession.DbType.DATE,dtStartDate);
			oDbSession.ProcedureInParm("pEND_DATE",DbSession.DbType.DATE,dtEndDate);
			oDbSession.ProcedureInParm("pUNRECEIVED_DAYS",DbSession.DbType.NUMBER,txtUnreceivedDays.Text);
			oDbSession.ProcedureInParm("pRES_LIMIT_SEC1",DbSession.DbType.NUMBER,txtResLimitSec1.Text);
			oDbSession.ProcedureInParm("pRES_LIMIT_SEC2",DbSession.DbType.NUMBER,txtResLimitSec2.Text);
			oDbSession.ProcedureInParm("pRES_BONUS_MAX_NUM",DbSession.DbType.NUMBER,txtResBonusMaxNum.Text);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDeleteFlag);
			oDbSession.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
			pResult = oDbSession.GetStringValue("pRESULT");
		}
	}

	protected void dsMailResBonus_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		MailResBonus2.SearchCondition oSearchCondition = new MailResBonus2.SearchCondition();
		oSearchCondition.SiteCd = lstSiteCd.SelectedValue;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsMailResBonus_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	/// <summary>
	/// 検索ボタンのクリックイベント
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			grdMailResBonus.PageIndex = 0;
			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	/// <summary>
	/// 追加ボタンのクリックイベント
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnCreate_Click(object sender,EventArgs e) {
		this.MailResBonusSeq = string.Empty;
		this.RevisionNo = string.Empty;
		this.ClearFileds();

		this.pnlSearch.Enabled = false;
		this.pnlInput.Visible = true;
		this.btnDelete.Enabled = false;
	}

	/// <summary>
	/// 更新ボタンのクリックイベント
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (CheckInput()) {
			string sResult;
			MailResBonusMainte(ViCommConst.FLAG_OFF,out sResult);

			if (sResult.Equals(PwViCommConst.MailAttackMainteResult.RESULT_OK)) {
				this.pnlSearch.Enabled = true;
				this.pnlInput.Visible = false;
				grdMailResBonus.PageIndex = 0;
				pnlGrid.DataBind();
				pnlCount.DataBind();
			} else if (sResult.Equals(PwViCommConst.MailAttackMainteResult.RESULT_NG_DATE)) {
				this.lblErrorMessage.Text = "期間が他と重複しています";
			}
		}
	}

	/// <summary>
	/// キャンセルボタンのクリックイベント
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlSearch.Enabled = true;
		this.pnlInput.Visible = false;
	}

	/// <summary>
	/// 削除ボタンのクリックイベント
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnDelete_Click(object sender,EventArgs e) {
		string sResult;
		MailResBonusMainte(ViCommConst.FLAG_ON,out sResult);

		this.pnlSearch.Enabled = true;
		this.pnlInput.Visible = false;
		grdMailResBonus.PageIndex = 0;
		pnlGrid.DataBind();
		pnlCount.DataBind();
	}

	/// <summary>
	/// シーケンスNoリンクのクリックイベント
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void lnkEdit_Command(object sender,CommandEventArgs e) {
		int iIndex = int.Parse(iBridUtil.GetStringValue(e.CommandArgument));
		GridViewRow oRow = this.grdMailResBonus.Rows[iIndex];
		HiddenField oHiddenField = oRow.FindControl("hdnMailResBonusSeq") as HiddenField;
		this.MailResBonusSeq = oHiddenField.Value;

		this.ClearFileds();
		this.GetData();

		this.pnlSearch.Enabled = false;
		this.pnlInput.Visible = true;
		this.btnDelete.Enabled = true;
	}
}
