﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ManTweetCommentList.aspx.cs" Inherits="Extension_Pwild_ManTweetCommentList" Title="会員つぶやきコメント検索"
    ValidateRequest="false" %>
<%@ Import Namespace="ViComm" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="会員つぶやきコメント検索"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 800px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                ﾛｸﾞｲﾝID
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtLoginId" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                書込日時From
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstFromYYYY" runat="server" Width="60px">
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstFromMM" runat="server" Width="40px">
                                </asp:DropDownList>月
                                <asp:DropDownList ID="lstFromDD" runat="server" Width="40px">
                                </asp:DropDownList>日
                                <asp:DropDownList ID="lstFromHH" runat="server" Width="40px">
                                </asp:DropDownList>時
                                <asp:DropDownList ID="lstFromMI" runat="server" Width="40px">
                                </asp:DropDownList>分
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                書込日時To
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstToYYYY" runat="server" Width="60px">
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstToMM" runat="server" Width="40px">
                                </asp:DropDownList>月
                                <asp:DropDownList ID="lstToDD" runat="server" Width="40px">
                                </asp:DropDownList>日
                                <asp:DropDownList ID="lstToHH" runat="server" Width="40px">
                                </asp:DropDownList>時
                                <asp:DropDownList ID="lstToMI" runat="server" Width="40px">
                                </asp:DropDownList>分
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle2">
                                キーワード<br />
                                (空白区切り/全てを含む)
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtKeyword" runat="server" Width="300px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" />
                <asp:Button runat="server" ID="btnCsv" Text="ＣＳＶ出力" CssClass="seekbutton" OnClick="btnCSV_Click" />
                <asp:CustomValidator ID="vdcFromTo" runat="server" ErrorMessage="" OnServerValidate="vdcFromTo_ServerValidate"
                    ValidationGroup="Key"></asp:CustomValidator>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[記事一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdManTweetComment.PageIndex + 1 %>
                        of
                        <%= grdManTweetComment.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="443px">
                    <asp:GridView ID="grdManTweetComment" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        DataSourceID="dsManTweetComment" SkinID="GridViewColor" AllowSorting="true" OnRowDataBound="grdManTweetComment_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="書込日時">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblTweetDate" runat="server" Text='<%# Eval("COMMENT_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>' NavigateUrl='<%# string.Format("ManTweetList.aspx?sitecd={0}&tweetseq={1}&tweetdate={2}",Eval("SITE_CD"),Eval("MAN_TWEET_SEQ"),Eval("TWEET_DATE")) %>'></asp:HyperLink><br />
                                    <asp:LinkButton ID="lnkDel" runat="server" CommandArgument='<%#string.Format("{0},{1},{2}",Eval("SITE_CD"),Eval("MAN_TWEET_COMMENT_SEQ"),Eval("ADMIN_DEL_FLAG")) %>'
                                        OnClientClick="return confirm('削除を実行しますか？');" OnCommand="lnkDelManTweetComment_Command"
                                        Text='<%# ViCommConst.FLAG_ON_STR.Equals(Eval("ADMIN_DEL_FLAG","{0}")) ? "復活" : "削除" %>'></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ﾛｸﾞｲﾝID">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblLoginId" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}",Eval("LOGIN_ID")) %>'
                                        Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle Width="120px" HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ﾊﾝﾄﾞﾙ名">
                                <ItemTemplate>
                                    <asp:Label ID="lblHandleNm" runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="120px" HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="内容">
                                <ItemTemplate>
                                    <asp:Label ID="lblCommentText" runat="server" Text='<%# Eval("COMMENT_TEXT") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
 							<asp:TemplateField HeaderText="削除">
								<ItemTemplate>
									<%# Eval("ADMIN_DEL_STATUS") %><br />
									<%# Eval("DEL_STATUS") %>
								</ItemTemplate>
								<ItemStyle VerticalAlign="Top" HorizontalAlign="Center" />
							</asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
            <br />
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsManTweetComment" runat="server" SelectMethod="GetPageCollection" ConvertNullToDBNull="true" SortParameterName=""
        TypeName="ManTweetComment" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelected="dsManTweetComment_Selected"
        OnSelecting="dsManTweetComment_Selecting">
        <SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
</asp:Content>
