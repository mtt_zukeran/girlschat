﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: つぶやきコメントランキング表示順位管理
--	Progaram ID		: ManTweetCommentRankList
--
--  Creation Date	: 2017.04.04
--  Creater			: K.Yogi
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;
using System.Text;


public partial class Extension_Pwild_ManTweetCommentRankList : System.Web.UI.Page
{
    #region 変数定義
    private string recCount = string.Empty;

    private string DispRankSeq
    {
        get
        {
            return iBridUtil.GetStringValue(ViewState["DISP_RANK_SEQ"]);
        }
        set
        {
            ViewState["DISP_RANK_SEQ"] = value;
        }
    }

    private string SexCD
    {
        get
        {
            return iBridUtil.GetStringValue(ViewState["SEX_CD"]);
        }
        set
        {
            ViewState["SEX_CD"] = value;
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
        txtDispRankNow.Style.Add("ime-mode", "disabled");
        txtDispRankPast.Style.Add("ime-mode", "disabled");
        txtDispRankManage.Style.Add("ime-mode", "disabled");

        if (!IsPostBack)
        {
            ClearCtrl();
            UpdateGrid();

            //クエリ文字列に表示順位と性別が指定されている場合編集状態にする
            if (!string.IsNullOrEmpty(Request.QueryString["disprankseq"]) && !string.IsNullOrEmpty(Request.QueryString["sexcd"]))
            {
                DispRankSeq = Request.QueryString["disprankseq"];
                SexCD = Request.QueryString["sexcd"];

                btnBack.Visible = true;
                pnlKey.Enabled = false;
                pnlItemInfo.Visible = true;
                btnDelete.Enabled = true;

                UpdateCtrl();
            }
            else
            {
                DispRankSeq = string.Empty;
                SexCD = ViCommConst.OPERATOR;

                pnlKey.Enabled = true;
                pnlInfo.Visible = true;
                pnlItemInfo.Visible = false;
            }
        }
    }

    protected void btnCreate_Click(object sender, EventArgs e)
    {
        ClearCtrl();
        pnlKey.Enabled = false;
        btnDelete.Enabled = false;
        pnlItemInfo.Visible = true;

        DispRankSeq = string.Empty;
        SexCD = ViCommConst.OPERATOR;
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        this.Response.Redirect(string.Format("~/Extension/Pwild/ManTweetCommentTermList.aspx?sitecd={0}", iBridUtil.GetStringValue(Session["MENU_SITE"])));
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        if (CheckCtrlData())
        {
            UpdateData(ViCommConst.FLAG_OFF);
            ClearCtrl();
            UpdateGrid();
            pnlItemInfo.Visible = false;
            pnlKey.Enabled = true;
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ClearCtrl();
        pnlItemInfo.Visible = false;
        pnlKey.Enabled = true;

        DispRankSeq = string.Empty;
        SexCD = ViCommConst.OPERATOR;
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        UpdateData(ViCommConst.FLAG_ON);
        ClearCtrl();
        UpdateGrid();
        pnlItemInfo.Visible = false;
        pnlKey.Enabled = true;
    }

    protected void lnkEdit_Command(object sender, CommandEventArgs e)
    {
        int iIndex = int.Parse(iBridUtil.GetStringValue(e.CommandArgument));

        GridViewRow oRow = grdManTweetCommentDispRank.Rows[iIndex];

        if (   string.IsNullOrEmpty(((HiddenField)oRow.FindControl("hdnDispRankSeq")).Value)
            || string.IsNullOrEmpty(((HiddenField)oRow.FindControl("hdnSexCD")).Value))
            return;

        DispRankSeq = ((HiddenField)oRow.FindControl("hdnDispRankSeq")).Value;
        SexCD = ((HiddenField)oRow.FindControl("hdnSexCD")).Value;

        pnlKey.Enabled = false;
        pnlItemInfo.Visible = true;
        btnDelete.Enabled = true;
        UpdateCtrl();
    }

    protected void dsManTweetCommentDispRank_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.ReturnValue != null)
        {
            recCount = e.ReturnValue.ToString();
        }
    }

    protected void dsManTweetCommentDispRank_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        //ManTweetCommentDispRank.SearchCondition oSearchCondition = new ManTweetCommentDispRank.SearchCondition();
        ////oSearchCondition.SiteCd = lstSiteCd.SelectedValue;
        //e.InputParameters[0] = oSearchCondition;
    }


    /// <summary>
    /// グリッドのレコード件数を取得
    /// </summary>
    /// <returns></returns>
    protected string GetRecCount()
    {
        return recCount;
    }

    /// <summary>
    /// 表示用のデータを取得してグリッドを更新
    /// </summary>
    private void UpdateGrid()
    {
        grdManTweetCommentDispRank.PageIndex = 0;
        grdManTweetCommentDispRank.DataSourceID = "dsManTweetCommentDispRank";
        grdManTweetCommentDispRank.DataBind();
        pnlCount.DataBind();
    }

    /// <summary>
    /// ページ内コントロールをクリア
    /// </summary>
    private void ClearCtrl()
    {
        lblErrorMessage.Text = string.Empty;
        lblSeq.Text = string.Empty;
        txtDispRankNow.Text = string.Empty;
        txtDispRankPast.Text = string.Empty;
        txtDispRankManage.Text = string.Empty;
    }

    /// <summary>
    /// ページ内コントロールをを更新
    /// </summary>
    private void UpdateCtrl()
    {
        ClearCtrl();

        if (string.IsNullOrEmpty(DispRankSeq) || string.IsNullOrEmpty(SexCD))
            return;

        using (ManTweetCommentDispRank obMTCDR = new ManTweetCommentDispRank())
        {
            DataTable dtRecord = obMTCDR.GetRecord(DispRankSeq, SexCD);

            if (dtRecord.Rows.Count > 0)
            {
                DispRankSeq = dtRecord.Rows[0]["DISP_RANK_SEQ"].ToString();
                SexCD = dtRecord.Rows[0]["SEX_CD"].ToString();

                lblSeq.Text = DispRankSeq;
                txtDispRankNow.Text = dtRecord.Rows[0]["DISP_RANK_NO_NOW"].ToString();
                txtDispRankPast.Text = dtRecord.Rows[0]["DISP_RANK_NO_PAST"].ToString();
                txtDispRankManage.Text = dtRecord.Rows[0]["DISP_RANK_NO_MANAGE"].ToString();
            }
        }
    }

    /// <summary>
    /// ページ内コントロールの値チェック
    /// </summary>
    private bool CheckCtrlData()
    {
        this.lblErrorMessage.Visible = false;

        if (string.IsNullOrEmpty(txtDispRankNow.Text))
        {
            this.lblErrorMessage.Text = "現在ランキング表示順位を入力してください";
            this.lblErrorMessage.Visible = true;
            return false;
        }

        if (string.IsNullOrEmpty(txtDispRankPast.Text))
        {
            this.lblErrorMessage.Text = "過去ランキング表示順位を入力してください";
            this.lblErrorMessage.Visible = true;
            return false;
        }

        if (string.IsNullOrEmpty(txtDispRankManage.Text))
        {
            this.lblErrorMessage.Text = "管理画面ランキング表示順位を入力してください";
            this.lblErrorMessage.Visible = true;
            return false;
        }

        return true;
    }

    /// <summary>
    /// データ更新
    /// </summary>
    private void UpdateData(int nDeleteFlag)
    {
        string[] sNows = ConversionArrayRankNo(txtDispRankNow.Text);
        string[] sPasts = ConversionArrayRankNo(txtDispRankPast.Text);
        string[] sManages = ConversionArrayRankNo(txtDispRankManage.Text);

        using (DbSession oDbSession = new DbSession())
        {
            oDbSession.PrepareProcedure("TWEET_COMMENT_DISP_RANK_MAINTE");
            oDbSession.ProcedureInParm("pDISP_RANK_SEQ", DbSession.DbType.NUMBER, DispRankSeq);
            oDbSession.ProcedureInParm("pSEX_CD", DbSession.DbType.VARCHAR2, SexCD);
            oDbSession.ProcedureInArrayParm("pDISP_RANK_NO_NOW", DbSession.DbType.NUMBER, sNows);
            oDbSession.ProcedureInArrayParm("pDISP_RANK_NO_PAST", DbSession.DbType.NUMBER, sPasts);
            oDbSession.ProcedureInArrayParm("pDISP_RANK_NO_MANAGE", DbSession.DbType.NUMBER, sManages);
            oDbSession.ProcedureInParm("pDELETE_FLAG", DbSession.DbType.NUMBER, nDeleteFlag);
            oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
            oDbSession.cmd.BindByName = true;
            oDbSession.ExecuteProcedure();
        }
    }

    /// <summary>
    /// 文字列で入力された表示順位を数値型の配列変換
    /// </summary>
    private string[] ConversionArrayRankNo(string sVal)
    {
        //カンマ区切りを配列に変換
        string[] sVals = sVal.Split(',');
        List<string> lstDispRank = new List<string>();

        foreach (string str in sVals)
        {
            //ハイフンがある場合は連番
            if (str.IndexOf("-") > 0)
            {
                string[] sSeq = str.Split('-');
                int nRankStart = 0;
                int nRankEnd = 0;
                int.TryParse(sSeq[0], out nRankStart);
                int.TryParse(sSeq[sSeq.Length-1], out nRankEnd);
                for (int nRank = nRankStart; nRank <= nRankEnd; nRank++)
                    lstDispRank.Add(nRank.ToString());
            }
            else
            {
                int nRank = 0;
                int.TryParse(str, out nRank);
                if (nRank > 0)
                    lstDispRank.Add(str);
            }
        }

        return lstDispRank.ToArray();
    }
    
}
