﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 投票設定一覧
--	Progaram ID		: VoteTermList
--  Creation Date	: 2013.10.15
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using ViComm.Extension.Pwild;

public partial class Extension_Pwild_VoteTermList:System.Web.UI.Page {
	private string recCount = string.Empty;
	protected string[] YearArray;
	protected string[] MonthArray;
	protected string[] DayArray;
	protected string[] HourArray;
	protected string[] MinuteArray;

	private string VoteTermSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["VOTE_TERM_SEQ"]);
		}
		set {
			this.ViewState["VOTE_TERM_SEQ"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["REVISION_NO"]);
		}
		set {
			this.ViewState["REVISION_NO"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);

			if (string.IsNullOrEmpty(sSiteCd)) {
				if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
					lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
				} else {
					lstSiteCd.SelectedIndex = 0;
				}
			} else {
				lstSiteCd.SelectedValue = sSiteCd;
			}

			this.pnlSearch.Enabled = true;
			this.pnlInput.Visible = false;

			lstSiteCd.DataBind();
			pnlGrid.DataBind();
			pnlCount.DataBind();

			SetDateArray();
			pnlInput.DataBind();
		}
	}

	protected void dsVoteTerm_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		VoteTerm.SearchCondition oSearchCondition = new VoteTerm.SearchCondition();
		oSearchCondition.SiteCd = lstSiteCd.SelectedValue;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsVoteTerm_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected string GetFixRankFlagStr(object pFixRankFlag) {
		return (pFixRankFlag.ToString().Equals(ViCommConst.FLAG_ON_STR)) ? "固定" : "ﾘｱﾙﾀｲﾑ";
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			grdVoteTerm.PageIndex = 0;
			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		this.VoteTermSeq = string.Empty;
		this.RevisionNo = string.Empty;
		this.ClearFileds();

		this.pnlSearch.Enabled = false;
		this.pnlInput.Visible = true;
		this.btnVoteFixRankUpdate.Enabled = false;
		this.btnDelete.Enabled = false;
	}

	protected void btnVoteRankUpdate_Click(object sender,EventArgs e) {
		VoteFixRankUpdate();
		this.VoteTermGet();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (CheckInput()) {
			string sResult;
			VoteTermMainte(ViCommConst.FLAG_OFF,out sResult);

			if (sResult.Equals(PwViCommConst.VoteTermMainteResult.RESULT_OK)) {
				this.pnlSearch.Enabled = true;
				this.pnlInput.Visible = false;
				grdVoteTerm.PageIndex = 0;
				pnlGrid.DataBind();
				pnlCount.DataBind();
			} else if (sResult.Equals(PwViCommConst.VoteTermMainteResult.RESULT_NG_DATE)) {
				this.lblErrorMessage.Text = "投票期間が他と重複しています";
			}
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlSearch.Enabled = true;
		this.pnlInput.Visible = false;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		string sResult;
		VoteTermMainte(ViCommConst.FLAG_ON,out sResult);

		this.pnlSearch.Enabled = true;
		this.pnlInput.Visible = false;
		grdVoteTerm.PageIndex = 0;
		pnlGrid.DataBind();
		pnlCount.DataBind();
	}

	protected void lnkEdit_Command(object sender,CommandEventArgs e) {
		int iIndex = int.Parse(iBridUtil.GetStringValue(e.CommandArgument));
		GridViewRow oRow = this.grdVoteTerm.Rows[iIndex];
		HiddenField oHiddenField = oRow.FindControl("hdnVoteTermSeq") as HiddenField;
		this.VoteTermSeq = oHiddenField.Value;

		this.ClearFileds();
		this.VoteTermGet();

		this.pnlSearch.Enabled = false;
		this.pnlInput.Visible = true;
		this.btnVoteFixRankUpdate.Enabled = true;
		this.btnDelete.Enabled = true;
	}

	private void SetDateArray() {
		this.YearArray = new string[] { DateTime.Today.AddYears(-1).ToString("yyyy"),DateTime.Today.ToString("yyyy"),DateTime.Today.AddYears(1).ToString("yyyy"),DateTime.Today.AddYears(2).ToString("yyyy") };
		this.MonthArray = new string[] { "01","02","03","04","05","06","07","08","09","10","11","12" };
		this.DayArray = new string[] { "01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31" };
		this.HourArray = new string[] { "00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23" };

		List<string> oMinuteList = new List<string>();

		for (int i = 0;i < 60;i++) {
			oMinuteList.Add(string.Format("{0:D2}",i));
		}

		this.MinuteArray = oMinuteList.ToArray();
	}

	private bool CheckInput() {
		DateTime dtVoteStartDate = new DateTime();
		DateTime dtVoteEndDate = new DateTime();
		int iFreeVoteCount;
		int iTicketPoint;

		if (string.IsNullOrEmpty(this.txtVoteTermNm.Text)) {
			this.lblErrorMessage.Text = "名称を入力して下さい";
			return false;
		}

		if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstVoteStartDateYYYY.SelectedValue,this.lstVoteStartDateMM.SelectedValue,this.lstVoteStartDateDD.SelectedValue,this.lstVoteStartDateHH.SelectedValue,this.lstVoteStartDateMI.SelectedValue),out dtVoteStartDate)) {
			this.lblErrorMessage.Text = "投票開始日時が正しくありません";
			return false;
		}

		if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:59",this.lstVoteEndDateYYYY.SelectedValue,this.lstVoteEndDateMM.SelectedValue,this.lstVoteEndDateDD.SelectedValue,this.lstVoteEndDateHH.SelectedValue,this.lstVoteEndDateMI.SelectedValue),out dtVoteEndDate)) {
			this.lblErrorMessage.Text = "投票終了日時が正しくありません";
			return false;
		}

		if (dtVoteStartDate > dtVoteEndDate) {
			this.lblErrorMessage.Text = "投票期間の大小関係が正しくありません";
			return false;
		}

		if (string.IsNullOrEmpty(this.txtFreeVoteCount.Text)) {
			this.lblErrorMessage.Text = "無料投票回数を入力して下さい";
			return false;
		} else if (!int.TryParse(this.txtFreeVoteCount.Text,out iFreeVoteCount)) {
			this.lblErrorMessage.Text = "無料投票回数が正しくありません";
			return false;
		}

		if (string.IsNullOrEmpty(this.txtTicketPoint.Text)) {
			this.lblErrorMessage.Text = "投票券追加消費ポイントを入力して下さい";
			return false;
		} else if (!int.TryParse(this.txtTicketPoint.Text,out iTicketPoint)) {
			this.lblErrorMessage.Text = "投票券追加消費ポイントが正しくありません";
			return false;
		}

		return true;
	}

	private void ClearFileds() {
		this.lblErrorMessage.Text = string.Empty;
		this.txtVoteTermNm.Text = string.Empty;

		this.lstVoteStartDateYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstVoteStartDateMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstVoteStartDateDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstVoteStartDateHH.SelectedValue = "00";
		this.lstVoteStartDateMI.SelectedValue = "00";

		this.lstVoteEndDateYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstVoteEndDateMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstVoteEndDateDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstVoteEndDateHH.SelectedValue = "23";
		this.lstVoteEndDateMI.SelectedValue = "59";

		this.txtFreeVoteCount.Text = "1";
		this.txtTicketPoint.Text = "5";
		this.rdoFixRankFlagOff.Checked = true;
		this.rdoFixRankFlagOn.Checked = false;
		this.lblFixRankDate.Text = string.Empty;
	}

	private void VoteTermGet() {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("VOTE_TERM_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pVOTE_TERM_SEQ",DbSession.DbType.VARCHAR2,this.VoteTermSeq);
			oDbSession.ProcedureOutParm("pVOTE_TERM_NM",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pVOTE_START_DATE",DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pVOTE_END_DATE",DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pFREE_VOTE_COUNT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pTICKET_POINT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pFIX_RANK_FLAG",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pFIX_RANK_DATE",DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();

			this.txtVoteTermNm.Text = oDbSession.GetStringValue("pVOTE_TERM_NM");
			DateTime dtVoteStartDate = (DateTime)oDbSession.GetDateTimeValue("pVOTE_START_DATE");
			DateTime dtVoteEndDate = (DateTime)oDbSession.GetDateTimeValue("pVOTE_END_DATE");

			if (dtVoteStartDate != null) {
				this.lstVoteStartDateYYYY.SelectedValue = dtVoteStartDate.ToString("yyyy");
				this.lstVoteStartDateMM.SelectedValue = dtVoteStartDate.ToString("MM");
				this.lstVoteStartDateDD.SelectedValue = dtVoteStartDate.ToString("dd");
				this.lstVoteStartDateHH.SelectedValue = dtVoteStartDate.ToString("HH");
				this.lstVoteStartDateMI.SelectedValue = dtVoteStartDate.ToString("mm");
			}

			if (dtVoteEndDate != null) {
				this.lstVoteEndDateYYYY.SelectedValue = dtVoteEndDate.ToString("yyyy");
				this.lstVoteEndDateMM.SelectedValue = dtVoteEndDate.ToString("MM");
				this.lstVoteEndDateDD.SelectedValue = dtVoteEndDate.ToString("dd");
				this.lstVoteEndDateHH.SelectedValue = dtVoteEndDate.ToString("HH");
				this.lstVoteEndDateMI.SelectedValue = dtVoteEndDate.ToString("mm");
			}

			this.txtFreeVoteCount.Text = oDbSession.GetIntValue("pFREE_VOTE_COUNT").ToString();
			this.txtTicketPoint.Text = oDbSession.GetIntValue("pTICKET_POINT").ToString();
			int iFixRankFlag = oDbSession.GetIntValue("pFIX_RANK_FLAG");

			if (iFixRankFlag == 1) {
				this.rdoFixRankFlagOff.Checked = false;
				this.rdoFixRankFlagOn.Checked = true;
			} else {
				this.rdoFixRankFlagOff.Checked = true;
				this.rdoFixRankFlagOn.Checked = false;
			}

			this.lblFixRankDate.Text = oDbSession.GetDateTimeValue("pFIX_RANK_DATE").ToString();
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
		}
	}

	private void VoteTermMainte(int pDeleteFlag,out string pResult) {
		DateTime dtVoteStartDate = DateTime.Parse(string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstVoteStartDateYYYY.SelectedValue,this.lstVoteStartDateMM.SelectedValue,this.lstVoteStartDateDD.SelectedValue,this.lstVoteStartDateHH.SelectedValue,this.lstVoteStartDateMI.SelectedValue));
		DateTime dtVoteEndDate = DateTime.Parse(string.Format("{0}/{1}/{2} {3}:{4}:59",this.lstVoteEndDateYYYY.SelectedValue,this.lstVoteEndDateMM.SelectedValue,this.lstVoteEndDateDD.SelectedValue,this.lstVoteEndDateHH.SelectedValue,this.lstVoteEndDateMI.SelectedValue));
		int iFixRankFlag = (this.rdoFixRankFlagOn.Checked) ? 1 : 0;

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("VOTE_TERM_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pVOTE_TERM_SEQ",DbSession.DbType.NUMBER,this.VoteTermSeq);
			oDbSession.ProcedureInParm("pVOTE_TERM_NM",DbSession.DbType.VARCHAR2,this.txtVoteTermNm.Text);
			oDbSession.ProcedureInParm("pVOTE_START_DATE",DbSession.DbType.DATE,dtVoteStartDate);
			oDbSession.ProcedureInParm("pVOTE_END_DATE",DbSession.DbType.DATE,dtVoteEndDate);
			oDbSession.ProcedureInParm("pFREE_VOTE_COUNT",DbSession.DbType.NUMBER,int.Parse(this.txtFreeVoteCount.Text));
			oDbSession.ProcedureInParm("pTICKET_POINT",DbSession.DbType.NUMBER,int.Parse(this.txtTicketPoint.Text));
			oDbSession.ProcedureInParm("pFIX_RANK_FLAG",DbSession.DbType.NUMBER,iFixRankFlag);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDeleteFlag);
			oDbSession.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
			pResult = oDbSession.GetStringValue("pRESULT");
		}
	}

	private void VoteFixRankUpdate() {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("VOTE_FIX_RANK_UPDATE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pVOTE_TERM_SEQ",DbSession.DbType.NUMBER,this.VoteTermSeq);
			oDbSession.ProcedureInParm("pWITHOUT_COMMIT",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
		}		
	}
}
