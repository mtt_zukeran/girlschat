﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 新人メールアタック設定一覧
--	Progaram ID		: MailAttackNewList
--  Creation Date	: 2016.07.25
--  Creater			: Y.Ikemiya
**************************************************************************/
using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using ViComm.Extension.Pwild;

public partial class Extension_Pwild_MailAttackNewList:System.Web.UI.Page {
	private string recCount = string.Empty;
	protected string[] YearArray;
	protected string[] MonthArray;
	protected string[] DayArray;
	protected string[] HourArray;
	protected string[] MinuteArray;

	private string MailAttackNewSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MAIL_ATTACK_NEW_SEQ"]);
		}
		set {
			this.ViewState["MAIL_ATTACK_NEW_SEQ"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["REVISION_NO"]);
		}
		set {
			this.ViewState["REVISION_NO"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);

			if (string.IsNullOrEmpty(sSiteCd)) {
				if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
					lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
				} else {
					lstSiteCd.SelectedIndex = 0;
				}
			} else {
				lstSiteCd.SelectedValue = sSiteCd;
			}

			this.pnlSearch.Enabled = true;
			this.pnlInput.Visible = false;

			lstSiteCd.DataBind();
			pnlGrid.DataBind();
			pnlCount.DataBind();

			SetDateArray();
			pnlInput.DataBind();
		}
	}

	protected void dsMailAttackNew_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		MailAttackNew.SearchCondition oSearchCondition = new MailAttackNew.SearchCondition();
		oSearchCondition.SiteCd = lstSiteCd.SelectedValue;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsMailAttackNew_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			grdMailAttackNew.PageIndex = 0;
			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		this.MailAttackNewSeq = string.Empty;
		this.RevisionNo = string.Empty;
		this.ClearFileds();

		this.pnlSearch.Enabled = false;
		this.pnlInput.Visible = true;
		this.btnDelete.Enabled = false;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (CheckInput()) {
			string sResult;
			MailAttackNewMainte(ViCommConst.FLAG_OFF,out sResult);

			if (sResult.Equals(PwViCommConst.MailAttackMainteResult.RESULT_OK)) {
				this.pnlSearch.Enabled = true;
				this.pnlInput.Visible = false;
				grdMailAttackNew.PageIndex = 0;
				pnlGrid.DataBind();
				pnlCount.DataBind();
			} else if (sResult.Equals(PwViCommConst.MailAttackMainteResult.RESULT_NG_DATE)) {
				this.lblErrorMessage.Text = "期間が他と重複しています";
			}
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlSearch.Enabled = true;
		this.pnlInput.Visible = false;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		string sResult;
		MailAttackNewMainte(ViCommConst.FLAG_ON,out sResult);

		this.pnlSearch.Enabled = true;
		this.pnlInput.Visible = false;
		grdMailAttackNew.PageIndex = 0;
		pnlGrid.DataBind();
		pnlCount.DataBind();
	}

	protected void lnkEdit_Command(object sender,CommandEventArgs e) {
		int iIndex = int.Parse(iBridUtil.GetStringValue(e.CommandArgument));
		GridViewRow oRow = this.grdMailAttackNew.Rows[iIndex];
		HiddenField oHiddenField = oRow.FindControl("hdnMailAttackNewSeq") as HiddenField;
		this.MailAttackNewSeq = oHiddenField.Value;

		this.ClearFileds();
		this.MailAttackNewGet();

		this.pnlSearch.Enabled = false;
		this.pnlInput.Visible = true;
		this.btnDelete.Enabled = true;
	}

	private void SetDateArray() {
		int iMinYear = 2015;
		List<string> oYearList = new List<string>();
		for (int i = iMinYear;i <= DateTime.Today.AddYears(1).Year;i++) {
			oYearList.Add(i.ToString());
		}
		this.YearArray = oYearList.ToArray();

		this.MonthArray = new string[] { "01","02","03","04","05","06","07","08","09","10","11","12" };
		this.DayArray = new string[] { "01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31" };
		this.HourArray = new string[] { "00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23" };

		List<string> oMinuteList = new List<string>();

		for (int i = 0;i < 60;i++) {
			oMinuteList.Add(string.Format("{0:D2}",i));
		}

		this.MinuteArray = oMinuteList.ToArray();
	}

	private bool CheckInput() {
		int iAfterRegistDays;

		if (!int.TryParse(txtAfterRegistDays.Text,out iAfterRegistDays)) {
			this.lblErrorMessage.Text = "会員登録後経過日数を正しく入力してください";
			return false;
		}

		DateTime dtStartDate = new DateTime();
		DateTime dtEndDate = new DateTime();

		if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstStartDateYYYY.SelectedValue,this.lstStartDateMM.SelectedValue,this.lstStartDateDD.SelectedValue,this.lstStartDateHH.SelectedValue,this.lstStartDateMI.SelectedValue),out dtStartDate)) {
			this.lblErrorMessage.Text = "開始日時が正しくありません";
			return false;
		}

		if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:59",this.lstEndDateYYYY.SelectedValue,this.lstEndDateMM.SelectedValue,this.lstEndDateDD.SelectedValue,this.lstEndDateHH.SelectedValue,this.lstEndDateMI.SelectedValue),out dtEndDate)) {
			this.lblErrorMessage.Text = "終了日時が正しくありません";
			return false;
		}

		if (dtStartDate > dtEndDate) {
			this.lblErrorMessage.Text = "期間の大小関係が正しくありません";
			return false;
		}

		return true;
	}

	private void ClearFileds() {
		this.lblErrorMessage.Text = string.Empty;

		this.txtAfterRegistDays.Text = string.Empty;

		this.lstStartDateYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstStartDateMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstStartDateDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstStartDateHH.SelectedValue = "00";
		this.lstStartDateMI.SelectedValue = "00";

		this.lstEndDateYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstEndDateMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstEndDateDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstEndDateHH.SelectedValue = "23";
		this.lstEndDateMI.SelectedValue = "59";
	}

	private void MailAttackNewGet() {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("MAIL_ATTACK_NEW_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pMAIL_ATTACK_NEW_SEQ",DbSession.DbType.VARCHAR2,this.MailAttackNewSeq);
			oDbSession.ProcedureOutParm("pAFTER_REGIST_DAYS",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTART_DATE",DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pEND_DATE",DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();

			this.txtAfterRegistDays.Text = oDbSession.GetStringValue("pAFTER_REGIST_DAYS");
			DateTime dtStartDate = (DateTime)oDbSession.GetDateTimeValue("pSTART_DATE");
			DateTime dtEndDate = (DateTime)oDbSession.GetDateTimeValue("pEND_DATE");

			if (dtStartDate != null) {
				this.lstStartDateYYYY.SelectedValue = dtStartDate.ToString("yyyy");
				this.lstStartDateMM.SelectedValue = dtStartDate.ToString("MM");
				this.lstStartDateDD.SelectedValue = dtStartDate.ToString("dd");
				this.lstStartDateHH.SelectedValue = dtStartDate.ToString("HH");
				this.lstStartDateMI.SelectedValue = dtStartDate.ToString("mm");
			}

			if (dtEndDate != null) {
				this.lstEndDateYYYY.SelectedValue = dtEndDate.ToString("yyyy");
				this.lstEndDateMM.SelectedValue = dtEndDate.ToString("MM");
				this.lstEndDateDD.SelectedValue = dtEndDate.ToString("dd");
				this.lstEndDateHH.SelectedValue = dtEndDate.ToString("HH");
				this.lstEndDateMI.SelectedValue = dtEndDate.ToString("mm");
			}
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
		}
	}

	private void MailAttackNewMainte(int pDeleteFlag,out string pResult) {
		DateTime dtStartDate = DateTime.Parse(string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstStartDateYYYY.SelectedValue,this.lstStartDateMM.SelectedValue,this.lstStartDateDD.SelectedValue,this.lstStartDateHH.SelectedValue,this.lstStartDateMI.SelectedValue));
		DateTime dtEndDate = DateTime.Parse(string.Format("{0}/{1}/{2} {3}:{4}:59",this.lstEndDateYYYY.SelectedValue,this.lstEndDateMM.SelectedValue,this.lstEndDateDD.SelectedValue,this.lstEndDateHH.SelectedValue,this.lstEndDateMI.SelectedValue));

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("MAIL_ATTACK_NEW_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pMAIL_ATTACK_NEW_SEQ",DbSession.DbType.NUMBER,this.MailAttackNewSeq);
			oDbSession.ProcedureInParm("pAFTER_REGIST_DAYS",DbSession.DbType.VARCHAR2,txtAfterRegistDays.Text);
			oDbSession.ProcedureInParm("pSTART_DATE",DbSession.DbType.DATE,dtStartDate);
			oDbSession.ProcedureInParm("pEND_DATE",DbSession.DbType.DATE,dtEndDate);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDeleteFlag);
			oDbSession.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
			pResult = oDbSession.GetStringValue("pRESULT");
		}
	}
}
