﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 投票券コード一覧
--	Progaram ID		: VoteTicketCodeList
--  Creation Date	: 2013.10.15
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;

public partial class Extension_Pwild_VoteTicketCodeList:System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SITE_CD"]);
		}
		set {
			this.ViewState["SITE_CD"] = value;
		}
	}

	private string VoteTermSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["VOTE_TERM_SEQ"]);
		}
		set {
			this.ViewState["VOTE_TERM_SEQ"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		lblRegistComplete.Visible = false;
		lblRegistError.Text = string.Empty;

		if (!this.IsPostBack) {
			this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			this.VoteTermSeq = iBridUtil.GetStringValue(Request.QueryString["votetermseq"]);

			txtTicketCount.Text = "5";

			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	protected void btnBack_Click(object sender,EventArgs e) {
		this.Response.Redirect(string.Format("~/Extension/Pwild/VoteTermList.aspx?sitecd={0}",this.SiteCd));
		return;
	}

	protected void dsVoteTicketCode_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		VoteTicketCode.SearchCondition oSearchCondition = new VoteTicketCode.SearchCondition();
		oSearchCondition.SiteCd = this.SiteCd;
		oSearchCondition.VoteTermSeq = this.VoteTermSeq;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsVoteTicketCode_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		lblRegistError.Text = string.Empty;
		int iAddCount;
		int iTicketCount;

		if (string.IsNullOrEmpty(txtAddCount.Text)) {
			lblRegistError.Text = "コード追加数を入力して下さい";
			return;
		} else if (!int.TryParse(txtAddCount.Text,out iAddCount)) {
			lblRegistError.Text = "コード追加数が正しくありません";
			return;
		}

		if (string.IsNullOrEmpty(txtTicketCount.Text)) {
			lblRegistError.Text = "投票券引換数を入力して下さい";
			return;
		} else if (!int.TryParse(txtTicketCount.Text,out iTicketCount)) {
			lblRegistError.Text = "投票券引換数が正しくありません";
			return;
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("VOTE_TICKET_CODE_REGIST");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pVOTE_TERM_SEQ",DbSession.DbType.NUMBER,this.VoteTermSeq);
			oDbSession.ProcedureInParm("pADD_COUNT",DbSession.DbType.NUMBER,iAddCount);
			oDbSession.ProcedureInParm("pTICKET_COUNT",DbSession.DbType.NUMBER,iTicketCount);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
		}

		lblRegistComplete.Visible = true;

		pnlGrid.DataBind();
		pnlCount.DataBind();
	}
}
