/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ���̖���T���E�^�[�Q�b�g���҈ꗗ

--	Progaram ID		: InvestigateTargetCandidate
--
--  Creation Date	: 2015.02.26
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using iBridCommLib;
using ViComm;

public partial class Extension_Pwild_InvestigateTargetCandidateList:System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SortExpression {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortExpression"]);
		}
		set {
			this.ViewState["SortExpression"] = value;
		}
	}

	private string SortDirect {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortDirect"]);
		}
		set {
			this.ViewState["SortDirect"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();

			this.pnlInfo.Visible = true;
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		this.pnlInfo.Visible = true;
		this.GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		List<string> oUserSeqList = new List<string>();
		List<string> oUserCharNoList = new List<string>();
		List<string> oInvestigateTargetSeqList = new List<string>();

		foreach (GridViewRow oGridViewRow in this.grdInvestigateTargetCandidate.Rows) {
			CheckBox oRegTargetCheckBox = oGridViewRow.FindControl("chkRegTarget") as CheckBox;
			if (oRegTargetCheckBox != null && oRegTargetCheckBox.Checked) {
				IDictionary oDataKeys = this.grdInvestigateTargetCandidate.DataKeys[oGridViewRow.RowIndex].Values;
				oUserSeqList.Add(oDataKeys["USER_SEQ"].ToString());
				oUserCharNoList.Add(oDataKeys["USER_CHAR_NO"].ToString());
			}
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("INVESTIGATE_TARGET_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.lstSeekSiteCd.SelectedValue);
			oDbSession.ProcedureInArrayParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,oUserSeqList.ToArray());
			oDbSession.ProcedureInArrayParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,oUserCharNoList.ToArray());
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.Response.Redirect(string.Format("~/Extension/Pwild/InvestigateTargetList.aspx?sitecd={0}",this.lstSeekSiteCd.SelectedValue));
	}

	protected void grdInvestigateTargetCandidate_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			bool bOk = true;
			
			DataRowView drv = (DataRowView)e.Row.DataItem;

			Regex regex = new Regex("�s��",RegexOptions.Compiled);

			if (string.IsNullOrEmpty(iBridUtil.GetStringValue(drv["CAST_ATTR_VALUE01"]))) {
				bOk = false;
			} else if (string.IsNullOrEmpty(iBridUtil.GetStringValue(drv["CAST_ATTR_VALUE02"]))) {
				bOk = false;
			} else if (string.IsNullOrEmpty(iBridUtil.GetStringValue(drv["CAST_ATTR_VALUE03"]))) {
				bOk = false;
			} else if (string.IsNullOrEmpty(iBridUtil.GetStringValue(drv["CAST_ATTR_VALUE04"]))) {
				bOk = false;
			} else if (string.IsNullOrEmpty(iBridUtil.GetStringValue(drv["CAST_ATTR_VALUE05"]))) {
				bOk = false;
			} else if (string.IsNullOrEmpty(iBridUtil.GetStringValue(drv["CAST_ATTR_VALUE06"]))) {
				bOk = false;
			} else if (string.IsNullOrEmpty(iBridUtil.GetStringValue(drv["CAST_ATTR_VALUE07"]))) {
				bOk = false;
			} else if (string.IsNullOrEmpty(iBridUtil.GetStringValue(drv["CAST_ATTR_VALUE08"]))) {
				bOk = false;
			} else if (string.IsNullOrEmpty(iBridUtil.GetStringValue(drv["CAST_ATTR_VALUE09"]))) {
				bOk = false;
			} else if (string.IsNullOrEmpty(iBridUtil.GetStringValue(drv["CAST_ATTR_VALUE10"]))) {
				bOk = false;
			} else if (string.IsNullOrEmpty(iBridUtil.GetStringValue(drv["CAST_ATTR_VALUE11"]))) {
				bOk = false;
			} else if (string.IsNullOrEmpty(iBridUtil.GetStringValue(drv["CAST_ATTR_VALUE12"]))) {
				bOk = false;
			} else if (string.IsNullOrEmpty(iBridUtil.GetStringValue(drv["COMMENT_LIST"]))) {
				bOk = false;
			} else if (regex.Match(iBridUtil.GetStringValue(drv["CAST_ATTR_VALUE02"])).Success) {
				bOk = false;
			} else if (regex.Match(iBridUtil.GetStringValue(drv["CAST_ATTR_VALUE03"])).Success) {
				bOk = false;
			} else if (regex.Match(iBridUtil.GetStringValue(drv["CAST_ATTR_VALUE05"])).Success) {
				bOk = false;
			} else if (regex.Match(iBridUtil.GetStringValue(drv["CAST_ATTR_VALUE06"])).Success) {
				bOk = false;
			} else if (regex.Match(iBridUtil.GetStringValue(drv["CAST_ATTR_VALUE08"])).Success) {
				bOk = false;
			} else if (regex.Match(iBridUtil.GetStringValue(drv["CAST_ATTR_VALUE09"])).Success) {
				bOk = false;
			} else if (regex.Match(iBridUtil.GetStringValue(drv["CAST_ATTR_VALUE10"])).Success) {
				bOk = false;
			} else if (regex.Match(iBridUtil.GetStringValue(drv["CAST_ATTR_VALUE11"])).Success) {
				bOk = false;
			} else if (regex.Match(iBridUtil.GetStringValue(drv["CAST_ATTR_VALUE12"])).Success) {
				bOk = false;
			}

			CheckBox chkRegTarget = (CheckBox)e.Row.FindControl("chkRegTarget");
			
			if (bOk == false) {
				chkRegTarget.Enabled = false;
				e.Row.BackColor = Color.FromArgb(0xDCDCDC);
			} else {
				chkRegTarget.Enabled = true;
				e.Row.BackColor = Color.FromArgb(0xFFFFFF);
			}
		}
	}

	protected void dsInvestigateTargetCandidate_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		InvestigateTargetCandidate.SearchCondition oSearchCondition = new InvestigateTargetCandidate.SearchCondition();
		oSearchCondition.SiteCd = this.lstSeekSiteCd.SelectedValue;
		oSearchCondition.LoginId = this.txtLoginId.Text.Trim();
		oSearchCondition.LastWantedTargetDate = DateTime.Now.AddDays(-13).ToString("yyyy/MM/dd");
		oSearchCondition.SortExpression = this.SortExpression;
		oSearchCondition.SortDirection = this.SortDirect;

		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsInvestigateTargetCandidate_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		this.recCount = "0";
		this.txtLoginId.Text = string.Empty;
		this.chkPagingOff.Checked = false;
		this.grdInvestigateTargetCandidate.DataSourceID = string.Empty;
		this.pnlInfo.Visible = false;
		this.lstSeekSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		this.SortExpression = string.Empty;
		this.SortDirect = string.Empty;
	}

	protected string GetRecCount() {
		if (ViCommConst.FLAG_OFF_STR.Equals(recCount)) {
			this.btnUpdate.Visible = false;
			this.btnUpdate1.Visible = false;
		} else {
			this.btnUpdate.Visible = true;
			this.btnUpdate1.Visible = true;
		}
		return recCount;
	}

	private void GetList() {
		this.grdInvestigateTargetCandidate.PageIndex = 0;
		if (chkPagingOff.Checked) {
			this.grdInvestigateTargetCandidate.PageSize = 99999;
		} else {
			this.grdInvestigateTargetCandidate.PageSize = 20;
		}

		this.grdInvestigateTargetCandidate.DataSourceID = "dsInvestigateTargetCandidate";
		this.grdInvestigateTargetCandidate.DataBind();
		this.pnlCount.DataBind();
	}

	protected string GetDisplayComment(object pComment) {
		string sComment = iBridUtil.GetStringValue(pComment);
		if (sComment.Length > 10) {
			return string.Concat(SysPrograms.Substring(sComment,9),"�c");
		} else {
			return sComment;
		}
	}

	protected string GetPicCountMark(object pPicCount) {
		int iPicCount;
		if (int.TryParse(iBridUtil.GetStringValue(pPicCount),out iPicCount)) {
			return string.Format("{0}��",iPicCount);
		} else {
			return "0��";
		}
	}

	protected void grdInvestigateTargetCandidate_Sorting(object sender,GridViewSortEventArgs e) {
		if (this.SortExpression.Equals(e.SortExpression)) {
			if (this.SortDirect.Equals("ASC")) {
				this.SortDirect = "DESC";
			} else if (this.SortDirect.Equals("DESC")) {
				this.SortDirect = "ASC";
			}
		} else {
			this.SortDirect = "ASC";
		}

		this.SortExpression = e.SortExpression;

		e.Cancel = true;

		this.GetList();
	}
}
