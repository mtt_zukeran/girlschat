﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="GameIntroList.aspx.cs" Inherits="Extension_Pwild_GameIntroList" Title="ゲーム友達紹介一覧" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="ゲーム友達紹介一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" Runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 600px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle2">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton" ValidationGroup="Create"
                    OnClick="btnCreate_Click" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" Visible="false" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlGameIntroInfo">
             <fieldset class="fieldset">
                <legend>[ゲーム友達紹介情報]</legend>
                <fieldset class="fieldset-inner">
                    <table border="0" class="tableStyle" style="width: 600px">
                        <tr>
                            <td class="tdHeaderStyle">
                                紹介人数
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtIntroMin" runat="server" MaxLength="10" Width="40px"></asp:TextBox>人～
                                <asp:TextBox ID="txtIntroMax" runat="server" MaxLength="10" Width="40px"></asp:TextBox>人
                                <asp:RequiredFieldValidator ID="vdrIntroMin" runat="server" ErrorMessage="紹介人数(最小値)を入力して下さい。"
                                    ControlToValidate="txtIntroMin" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="vdeIntroMin" runat="Server" TargetControlID="vdrIntroMin" HighlightCssClass="validatorCallout" />
                                <asp:RequiredFieldValidator ID="vdrIntroMax" runat="server" ErrorMessage="紹介人数(最大値)を入力して下さい。"
                                    ControlToValidate="txtIntroMax" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="vdeIntroMax" runat="Server" TargetControlID="vdrIntroMax" HighlightCssClass="validatorCallout" />
                                <asp:RangeValidator ID="vdrIntroMin2" runat="server" ControlToValidate="txtIntroMin" ErrorMessage="紹介人数(最小値)を正しく入力して下さい。"
                                    MaximumValue="9999999999" MinimumValue="1" ValidationGroup="Update">*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="vdeIntroMin2" runat="Server" TargetControlID="vdrIntroMin2" HighlightCssClass="validatorCallout" />
                                <asp:RangeValidator ID="vdrIntroMax2" runat="server" ControlToValidate="txtIntroMax" ErrorMessage="紹介人数(最大値)を正しく入力して下さい。"
                                    MaximumValue="9999999999" MinimumValue="1" ValidationGroup="Update">*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="vdeIntroMax2" runat="Server" TargetControlID="vdrIntroMax2" HighlightCssClass="validatorCallout" />
                                <asp:CompareValidator ID="vdcIntro" runat="server" ControlToValidate="txtIntroMax"
                                    ControlToCompare="txtIntroMin" Operator="GreaterThanEqual" Type="Integer" ErrorMessage="紹介人数の大小関係が不正です"
                                    ValidationGroup="Update">*</asp:CompareValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="vdeIntro" runat="Server" HighlightCssClass="validatorCallout" TargetControlID="vdcIntro">
                                </ajaxToolkit:ValidatorCalloutExtender>
                                <br />
                                <asp:Label ID="lblErrMsgIntroCount" runat="server" ForeColor="red" Visible="false"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                コメント
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtIntroComment" runat="server" Height="30px" MaxLength="1024" Rows="3" TextMode="MultiLine" Width="300px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <asp:Panel ID="pnlItem" runat="server">
                    <fieldset class="fieldset-inner">
                        <legend>[獲得アイテム]</legend>
                        <table border="0" style="width: 600px" class="tableStyle">
                            <tr>
                                <td class="tdHeaderStyle">
                                    アイテム１
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstGameItemCategory1" runat="server" DataSourceID="dsGameItemCategory"
                                        OnDataBound="lst_DataBound" OnSelectedIndexChanged="lstGameItemCategory_IndexChanged"
                                        DataTextField="GAME_ITEM_CATEGORY_NM" DataValueField="GAME_ITEM_CATEGORY_TYPE"
                                        Width="170px" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="lstGameItem1" runat="server" DataSourceID="dsGameItem" DataTextField="GAME_ITEM_NM"
                                        OnDataBound="lst_DataBound" DataValueField="GAME_ITEM_SEQ" Width="170px">
                                    </asp:DropDownList>
                                    &nbsp;
                                    <asp:TextBox ID="txtItemCount1" runat="server" MaxLength="10" Width="40px"></asp:TextBox>個
								    <asp:RangeValidator ID="vdrItemCount1" runat="server" ControlToValidate="txtItemCount1" ErrorMessage="個数を正しく入力して下さい。"
								        MaximumValue="9999999999" MinimumValue="1" ValidationGroup="Update">*</asp:RangeValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="vdeItemCount1" runat="Server" TargetControlID="vdrItemCount1" HighlightCssClass="validatorCallout" />
								    <br>
                                    <asp:CustomValidator ID="vdcItemCount1" runat="server" ErrorMessage="" OnServerValidate="vdcItemCount1_ServerValidate"
                                        ValidationGroup="Update"></asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    アイテム２
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstGameItemCategory2" runat="server" DataSourceID="dsGameItemCategory"
                                        OnDataBound="lst_DataBound" OnSelectedIndexChanged="lstGameItemCategory_IndexChanged"
                                        DataTextField="GAME_ITEM_CATEGORY_NM" DataValueField="GAME_ITEM_CATEGORY_TYPE"
                                        Width="170px" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="lstGameItem2" runat="server" DataSourceID="dsGameItem" DataTextField="GAME_ITEM_NM"
                                        OnDataBound="lst_DataBound" DataValueField="GAME_ITEM_SEQ" Width="170px">
                                    </asp:DropDownList>
                                    &nbsp;
                                    <asp:TextBox ID="txtItemCount2" runat="server" MaxLength="10" Width="40px"></asp:TextBox>個
								    <asp:RangeValidator ID="vdrItemCount2" runat="server" ControlToValidate="txtItemCount2" ErrorMessage="個数を正しく入力して下さい。"
								        MaximumValue="9999999999" MinimumValue="1" ValidationGroup="Update">*</asp:RangeValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="vdeItemCount2" runat="Server" TargetControlID="vdrItemCount2" HighlightCssClass="validatorCallout" />
								    <br>
                                    <asp:CustomValidator ID="vdcItemCount2" runat="server" ErrorMessage="" OnServerValidate="vdcItemCount2_ServerValidate"
                                        ValidationGroup="Update"></asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    アイテム３
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstGameItemCategory3" runat="server" DataSourceID="dsGameItemCategory"
                                        OnDataBound="lst_DataBound" OnSelectedIndexChanged="lstGameItemCategory_IndexChanged"
                                        DataTextField="GAME_ITEM_CATEGORY_NM" DataValueField="GAME_ITEM_CATEGORY_TYPE"
                                        Width="170px" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="lstGameItem3" runat="server" DataSourceID="dsGameItem" DataTextField="GAME_ITEM_NM"
                                        OnDataBound="lst_DataBound" DataValueField="GAME_ITEM_SEQ" Width="170px">
                                    </asp:DropDownList>
                                    &nbsp;
                                    <asp:TextBox ID="txtItemCount3" runat="server" MaxLength="10" Width="40px"></asp:TextBox>個
								    <asp:RangeValidator ID="vdrItemCount3" runat="server" ControlToValidate="txtItemCount3" ErrorMessage="個数を正しく入力して下さい。"
								        MaximumValue="9999999999" MinimumValue="1" ValidationGroup="Update">*</asp:RangeValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="vdeItemCount3" runat="Server" TargetControlID="vdrItemCount3" HighlightCssClass="validatorCallout" />
								    <br>
                                    <asp:CustomValidator ID="vdcItemCount3" runat="server" ErrorMessage="" OnServerValidate="vdcItemCount3_ServerValidate"
                                        ValidationGroup="Update"></asp:CustomValidator>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </asp:Panel>
                <asp:Panel ID="pnlCommand" runat="server">
					<table border="0" style="width: 640px">
						<tr>
							<td>
                                <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Update" OnClick="btnUpdate_Click" />
                                <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" CausesValidation="False" Visible="true" OnClick="btnCancel_Click" />
							</td>
							<td>
                                <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" ValidationGroup="Delete" OnClick="btnDelete_Click" />
							</td>
						</tr>
					</table>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[ゲーム友達紹介一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdGameIntro.PageIndex + 1 %>
                        of
                        <%= grdGameIntro.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="600px">
                    <asp:GridView ID="grdGameIntro" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        PageSize="30" EnableSortingAndPagingCallbacks="false" DataSourceID="dsGameIntro"
                        SkinID="GridViewColor" Font-Size="small" AllowSorting="true" DataKeyNames="SITE_CD,GAME_INTRO_SEQ">
                        <Columns>
                            <asp:TemplateField HeaderText="紹介人数">
                                <ItemTemplate>
                                    <asp:Label ID="lblIntroMin" runat="server" Text='<%# Eval("INTRO_MIN", "{0:N0}") %>'></asp:Label>～
                                    <asp:Label ID="lblIntroMax" runat="server" Text='<%# Eval("INTRO_MAX", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkGameIntro" runat="server" CommandArgument='<%# Eval("GAME_INTRO_SEQ") %>'
                                        Text='詳細' OnCommand="lnkGameIntro_Command">
                                    </asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsGameIntro" runat="server" SelectMethod="GetPageCollection"
        ConvertNullToDBNull="true" SortParameterName="" TypeName="GameIntro" SelectCountMethod="GetPageCount"
        EnablePaging="True" OnSelected="dsGameIntro_Selected">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItemCategory" runat="server" SelectMethod="GetList"
        TypeName="GameItemCategory">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItem" runat="server" SelectMethod="GetListByCategoryType"
        TypeName="GameItem" OnSelecting="dsGameItem_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:Parameter Name="pSexCd" Type="String" DefaultValue="" />
            <asp:Parameter Name="pGameItemCategoryType" Type="String" DefaultValue="" />
            <asp:Parameter Name="pGameItemGetCd" Type="String" DefaultValue="" />
            <asp:Parameter Name="pPresentFlag" Type="String" DefaultValue="1" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtIntroMin" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtIntroMax" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtItemCount1" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtItemCount2" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtItemCount3" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="ゲーム友達紹介情報を更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete"
        ConfirmText="ゲーム友達紹介情報を削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>

