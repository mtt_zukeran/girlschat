﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 投票順位一覧(エントリー制)
--	Progaram ID		: ElectionEntryList
--  Creation Date	: 2013.12.10
--  Creater			: Y.Ikemiya
**************************************************************************/
using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;

public partial class Extension_Pwild_ElectionEntryList:System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SITE_CD"]);
		}
		set {
			this.ViewState["SITE_CD"] = value;
		}
	}

	private string ElectionPeriodSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["ELECTION_PERIOD_SEQ"]);
		}
		set {
			this.ViewState["ELECTION_PERIOD_SEQ"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		lblBulkUpdateError.Text = string.Empty;
		lblBulkUpdateComplete.Visible = false;

		if (!this.IsPostBack) {
			this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			this.ElectionPeriodSeq = iBridUtil.GetStringValue(Request.QueryString["electionperiodseq"]);

			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	protected void btnBack_Click(object sender,EventArgs e) {
		this.Response.Redirect(string.Format("~/Extension/Pwild/ElectionPeriodList.aspx?sitecd={0}",this.SiteCd));
		return;
	}

	protected void dsElectionEntry_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		ElectionEntry.SearchCondition oSearchCondition = new ElectionEntry.SearchCondition();
		oSearchCondition.SiteCd = this.SiteCd;
		oSearchCondition.ElectionPeriodSeq = this.ElectionPeriodSeq;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsElectionEntry_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void grdElectionEntry_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			int iNaFlag = int.Parse(DataBinder.Eval(e.Row.DataItem,"NA_FLAG").ToString());
			int iRetireFlag = int.Parse(DataBinder.Eval(e.Row.DataItem,"RETIRE_FLAG").ToString());
			int iRefuseFlag = int.Parse(DataBinder.Eval(e.Row.DataItem,"REFUSE_FLAG").ToString());
			int iSecondPeriodFlag = int.Parse(DataBinder.Eval(e.Row.DataItem,"SECOND_PERIOD_FLAG").ToString());

			if (!iNaFlag.Equals(ViCommConst.NaFlag.OK)) {
				e.Row.BackColor = Color.LightGray;
			} else if (iRetireFlag.Equals(ViCommConst.FLAG_ON)) {
				e.Row.BackColor = Color.LightGray;
			} else if (iRefuseFlag.Equals(ViCommConst.FLAG_ON)) {
				e.Row.BackColor = Color.LightGray;
			} else if (iSecondPeriodFlag.Equals(ViCommConst.FLAG_ON)) {
				e.Row.BackColor = Color.MistyRose;
			} else {
				e.Row.BackColor = Color.White;
			}
		}
	}

	protected void btnBulkUpdate_Click(object sender,EventArgs e) {
		lblBulkUpdateError.Text = string.Empty;
		lblBulkUpdateComplete.Visible = false;

		List<string> lstFirstAddVoteCount = new List<string>();
		List<string> lstSecondAddVoteCount = new List<string>();
		List<string> lstElectionEntrySeq = new List<string>();
		List<string> lstRevisionNo = new List<string>();

		foreach (GridViewRow oRow in this.grdElectionEntry.Rows) {
			int iFirstAddVoteCount;
			int iSecondAddVoteCount;
			TextBox oFirstAddVoteCount = oRow.FindControl("txtFirstAddVoteCount") as TextBox;
			TextBox oSecondAddVoteCount = oRow.FindControl("txtSecondAddVoteCount") as TextBox;
			HiddenField oElectionEntrySeq = oRow.FindControl("hdnElectionEntrySeq") as HiddenField;
			HiddenField oRevisionNo = oRow.FindControl("hdnRevisionNo") as HiddenField;

			if (string.IsNullOrEmpty(oFirstAddVoteCount.Text)) {
				lblBulkUpdateError.Text = "加算数(1次)を入力して下さい";
				return;
			} else if (!int.TryParse(oFirstAddVoteCount.Text,out iFirstAddVoteCount)) {
				lblBulkUpdateError.Text = "加算数(1次)が正しくありません";
				return;
			} else if (string.IsNullOrEmpty(oSecondAddVoteCount.Text)) {
				lblBulkUpdateError.Text = "加算数(2次)を入力して下さい";
				return;
			} else if (!int.TryParse(oSecondAddVoteCount.Text,out iSecondAddVoteCount)) {
				lblBulkUpdateError.Text = "加算数(2次)が正しくありません";
				return;
			} else {
				lstFirstAddVoteCount.Add(oFirstAddVoteCount.Text);
				lstSecondAddVoteCount.Add(oSecondAddVoteCount.Text);
				lstElectionEntrySeq.Add(oElectionEntrySeq.Value);
				lstRevisionNo.Add(oRevisionNo.Value);
			}
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("ELECTION_RANK_BULK_UPDATE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pELECTION_PERIOD_SEQ",DbSession.DbType.NUMBER,this.ElectionPeriodSeq);
			oDbSession.ProcedureInArrayParm("pELECTION_ENTRY_SEQ",DbSession.DbType.NUMBER,lstElectionEntrySeq.ToArray());
			oDbSession.ProcedureInArrayParm("pFIRST_ADD_VOTE_COUNT",DbSession.DbType.NUMBER,lstFirstAddVoteCount.ToArray());
			oDbSession.ProcedureInArrayParm("pSECOND_ADD_VOTE_COUNT",DbSession.DbType.NUMBER,lstSecondAddVoteCount.ToArray());
			oDbSession.ProcedureInArrayParm("pREVISION_NO",DbSession.DbType.NUMBER,lstRevisionNo.ToArray());
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
		}

		pnlGrid.DataBind();
		pnlCount.DataBind();
		lblBulkUpdateComplete.Visible = true;
	}

	protected void lnkRetire_Command(object sender,CommandEventArgs e) {
		int iIndex = int.Parse(iBridUtil.GetStringValue(e.CommandArgument));
		GridViewRow oRow = this.grdElectionEntry.Rows[iIndex];
		HiddenField oElectionEntrySeq = oRow.FindControl("hdnElectionEntrySeq") as HiddenField;

		if (!string.IsNullOrEmpty(oElectionEntrySeq.Value)) {
			using (DbSession oDbSession = new DbSession()) {
				oDbSession.PrepareProcedure("ELECTION_ENTRY_UPDATE");
				oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
				oDbSession.ProcedureInParm("pELECTION_PERIOD_SEQ",DbSession.DbType.NUMBER,this.ElectionPeriodSeq);
				oDbSession.ProcedureInParm("pELECTION_ENTRY_SEQ",DbSession.DbType.NUMBER,oElectionEntrySeq.Value);
				oDbSession.ProcedureInParm("pRETIRE_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_ON);
				oDbSession.ProcedureInParm("pREFUSE_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
				oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
				oDbSession.cmd.BindByName = true;
				oDbSession.ExecuteProcedure();
			}

			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	protected void lnkDelete_Command(object sender,CommandEventArgs e) {
		int iIndex = int.Parse(iBridUtil.GetStringValue(e.CommandArgument));
		GridViewRow oRow = this.grdElectionEntry.Rows[iIndex];
		HiddenField oElectionEntrySeq = oRow.FindControl("hdnElectionEntrySeq") as HiddenField;
		HiddenField oRefuseFlag = oRow.FindControl("hdnRefuseFlag") as HiddenField;

		if (!string.IsNullOrEmpty(oElectionEntrySeq.Value)) {
			using (DbSession oDbSession = new DbSession()) {
				oDbSession.PrepareProcedure("ELECTION_ENTRY_UPDATE");
				oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
				oDbSession.ProcedureInParm("pELECTION_PERIOD_SEQ",DbSession.DbType.NUMBER,this.ElectionPeriodSeq);
				oDbSession.ProcedureInParm("pELECTION_ENTRY_SEQ",DbSession.DbType.NUMBER,oElectionEntrySeq.Value);
				oDbSession.ProcedureInParm("pRETIRE_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
				oDbSession.ProcedureInParm("pREFUSE_FLAG",DbSession.DbType.NUMBER,oRefuseFlag.Value.Equals(ViCommConst.FLAG_OFF_STR) ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR);
				oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
				oDbSession.cmd.BindByName = true;
				oDbSession.ExecuteProcedure();
			}

			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	protected bool GetElectionEntryVisible(object pRetireFlag,object pRefuseFlag,object pNaFlag) {
		if (int.Parse(pRetireFlag.ToString()).Equals(ViCommConst.FLAG_ON)) {
			return false;
		} else if (int.Parse(pRefuseFlag.ToString()).Equals(ViCommConst.FLAG_ON)) {
			return false;
		} else if (!int.Parse(pNaFlag.ToString()).Equals(ViCommConst.NaFlag.OK)) {
			return false;
		} else {
			return true;
		}
	}

	protected string GetDelFlagMark(object pRetireFlag,object pRefuseFlag) {
		if (pRefuseFlag.ToString().Equals(ViCommConst.FLAG_ON_STR)) {
			return "管理者削除";
		} else if (pRetireFlag.ToString().Equals(ViCommConst.FLAG_ON_STR)) {
			return "辞退";
		} else {
			return string.Empty;
		}
	}
}