﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者ページビュー表示(時間別)
--	Progaram ID		: CastPageViewTermList
--  Creation Date	: 2014.02.12
--  Creater			: Y.Ikemiya
**************************************************************************/
using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using ViComm.Extension.Pwild;

public partial class Extension_Pwild_CastPageViewTermList:System.Web.UI.Page {
	private string recCount = string.Empty;

	/// <summary>CSVファイル名</summary>
	private const string CSV_FILE_NM_CAST_PV_TERM = "CastPageViewTermList.csv";

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SITE_CD"]);
		}
		set {
			this.ViewState["SITE_CD"] = value;
		}
	}

	private string ProgramRoot {
		get {
			return iBridUtil.GetStringValue(this.ViewState["PROGRAM_ROOT"]);
		}
		set {
			this.ViewState["PROGRAM_ROOT"] = value;
		}
	}

	private string ProgramId {
		get {
			return iBridUtil.GetStringValue(this.ViewState["PROGRAM_ID"]);
		}
		set {
			this.ViewState["PROGRAM_ID"] = value;
		}
	}

	private string HtmlDocType {
		get {
			return iBridUtil.GetStringValue(this.ViewState["HTML_DOC_TYPE"]);
		}
		set {
			this.ViewState["HTML_DOC_TYPE"] = value;
		}
	}

	private string AccessType {
		get {
			return iBridUtil.GetStringValue(this.ViewState["ACCESS_TYPE"]);
		}
		set {
			this.ViewState["ACCESS_TYPE"] = value;
		}
	}

	private string YearFrom {
		get {
			return iBridUtil.GetStringValue(this.ViewState["YEAR_FROM"]);
		}
		set {
			this.ViewState["YEAR_FROM"] = value;
		}
	}

	private string MonthFrom {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MONTH_FROM"]);
		}
		set {
			this.ViewState["MONTH_FROM"] = value;
		}
	}

	private string DayFrom {
		get {
			return iBridUtil.GetStringValue(this.ViewState["DAY_FROM"]);
		}
		set {
			this.ViewState["DAY_FROM"] = value;
		}
	}

	private string YearTo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["YEAR_TO"]);
		}
		set {
			this.ViewState["YEAR_TO"] = value;
		}
	}

	private string MonthTo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MONTH_TO"]);
		}
		set {
			this.ViewState["MONTH_TO"] = value;
		}
	}

	private string DayTo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["DAY_TO"]);
		}
		set {
			this.ViewState["DAY_TO"] = value;
		}
	}

	private string LoginFlag {
		get {
			return iBridUtil.GetStringValue(this.ViewState["LOGIN_FLAG"]);
		}
		set {
			this.ViewState["LOGIN_FLAG"] = value;
		}
	}

	private string PageUserAgentType {
		get {
			return iBridUtil.GetStringValue(this.ViewState["PAGE_USER_AGENT_TYPE"]);
		}
		set {
			this.ViewState["PAGE_USER_AGENT_TYPE"] = value;
		}
	}

	private string ZeroAccessDispFrag {
		get {
			return iBridUtil.GetStringValue(this.ViewState["ZERO_ACCESS_DISP_FLAG"]);
		}
		set {
			this.ViewState["ZERO_ACCESS_DISP_FLAG"] = value;
		}
	}

	private string PaymentFlag {
		get {
			return iBridUtil.GetStringValue(this.ViewState["PAYMENT_FLAG"]);
		}
		set {
			this.ViewState["PAYMENT_FLAG"] = value;
		}
	}

	private string NewUserFlag {
		get {
			return iBridUtil.GetStringValue(this.ViewState["NEW_USER_FLAG"]);
		}
		set {
			this.ViewState["NEW_USER_FLAG"] = value;
		}
	}

	private string CarrierType {
		get {
			return iBridUtil.GetStringValue(this.ViewState["CARRIER_TYPE"]);
		}
		set {
			this.ViewState["CARRIER_TYPE"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			this.ProgramRoot = iBridUtil.GetStringValue(Request.QueryString["programroot"]);
			this.ProgramId = iBridUtil.GetStringValue(Request.QueryString["programid"]);
			this.HtmlDocType = iBridUtil.GetStringValue(Request.QueryString["htmldoctype"]);
			this.AccessType = iBridUtil.GetStringValue(Request.QueryString["accesstype"]);
			this.YearFrom = iBridUtil.GetStringValue(Request.QueryString["yearfrom"]);
			this.MonthFrom = iBridUtil.GetStringValue(Request.QueryString["monthfrom"]);
			this.DayFrom = iBridUtil.GetStringValue(Request.QueryString["dayfrom"]);
			this.YearTo = iBridUtil.GetStringValue(Request.QueryString["yearto"]);
			this.MonthTo = iBridUtil.GetStringValue(Request.QueryString["monthto"]);
			this.DayTo = iBridUtil.GetStringValue(Request.QueryString["dayto"]);
			this.LoginFlag = iBridUtil.GetStringValue(Request.QueryString["loginflag"]);
			this.PageUserAgentType = iBridUtil.GetStringValue(Request.QueryString["pageuseragenttype"]);
			this.PaymentFlag = iBridUtil.GetStringValue(Request.QueryString["paymentflag"]);
			this.NewUserFlag = iBridUtil.GetStringValue(Request.QueryString["newuserflag"]);
			this.CarrierType = iBridUtil.GetStringValue(Request.QueryString["carriertype"]);

			this.CreateList();
			this.InitPage();

			if (!string.IsNullOrEmpty(this.ProgramRoot) && !string.IsNullOrEmpty(this.ProgramId) && !string.IsNullOrEmpty(this.HtmlDocType)) {
				lstPage.SelectedValue = string.Format("{0}:{1}:{2}",this.ProgramRoot,this.ProgramId,this.HtmlDocType);
			}

			if (!string.IsNullOrEmpty(this.YearFrom) && !string.IsNullOrEmpty(this.MonthFrom) && !string.IsNullOrEmpty(this.DayFrom)) {
				this.lstYearFrom.SelectedValue = this.YearFrom;
				this.lstMonthFrom.SelectedValue = this.MonthFrom;
				this.lstDayFrom.SelectedValue = this.DayFrom;
			}

			if (!string.IsNullOrEmpty(this.YearTo) && !string.IsNullOrEmpty(this.MonthTo) && !string.IsNullOrEmpty(this.DayTo)) {
				this.lstYearTo.SelectedValue = this.YearTo;
				this.lstMonthTo.SelectedValue = this.MonthTo;
				this.lstDayTo.SelectedValue = this.DayTo;
			}

			if (!string.IsNullOrEmpty(this.LoginFlag)) {
				lstLoginType.SelectedValue = this.LoginFlag;
			}

			if (!string.IsNullOrEmpty(this.PageUserAgentType)) {
				lstPageUserAgentType.SelectedValue = this.PageUserAgentType;
			}

			if (!string.IsNullOrEmpty(this.PaymentFlag)) {
				rdoPaymentFlag.SelectedValue = this.PaymentFlag;
			}

			if (!string.IsNullOrEmpty(this.NewUserFlag)) {
				rdoNewUserFlag.SelectedValue = this.NewUserFlag;
			}

			if (!string.IsNullOrEmpty(this.CarrierType)) {
				lstCarrierType.SelectedValue = this.CarrierType;
			}

			pnlGrid.DataBind();
		}
	}

	/// <summary>
	/// 検索条件のオブジェクトに入力値を設定
	/// </summary>
	/// <param name="oSearchCondition"></param>
	private void SetSearchConditions(ref CastPageViewHourly.SearchCondition oSearchCondition) {
		string sProgramRoot = string.Empty;
		string sProgramId = string.Empty;
		string sHtmlDocType = string.Empty;

		if (!string.IsNullOrEmpty(lstPage.SelectedValue)) {
			string[] sPageArr = lstPage.SelectedValue.Split(':');
			sProgramRoot = sPageArr[0];
			sProgramId = sPageArr[1];
			sHtmlDocType = sPageArr[2];
		}

		oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
		oSearchCondition.ProgramRoot = sProgramRoot;
		oSearchCondition.ProgramId = sProgramId;
		oSearchCondition.HtmlDocType = sHtmlDocType;
		oSearchCondition.AccessType = this.lstAccessType.SelectedValue;
		oSearchCondition.ReportDayFrom = string.Format("{0}-{1}-{2}",this.lstYearFrom.SelectedValue,this.lstMonthFrom.SelectedValue,this.lstDayFrom.SelectedValue);
		oSearchCondition.ReportDayTo = string.Format("{0}-{1}-{2}",this.lstYearTo.SelectedValue,this.lstMonthTo.SelectedValue,this.lstDayTo.SelectedValue);
		oSearchCondition.LoginFlag = this.lstLoginType.SelectedValue;
		oSearchCondition.PageUserAgentType = this.lstPageUserAgentType.SelectedValue;
		oSearchCondition.PaymentFlag = this.rdoPaymentFlag.SelectedValue;
		oSearchCondition.NewUserFlag = this.rdoNewUserFlag.SelectedValue;
		oSearchCondition.CarrierType = this.lstCarrierType.SelectedValue;
	}

	protected void dsCastPageViewTerm_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		CastPageViewHourly.SearchCondition oSearchCondition = new CastPageViewHourly.SearchCondition();
		this.SetSearchConditions(ref oSearchCondition);

		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsCastPageViewTerm_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		if (!this.IsCorrectDate()) {
			return;
		}

		this.grdCastPageViewTerm.PageIndex = 0;
		this.GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	/// <summary>
	/// CSV出力ボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnCSV_Click(object sender,EventArgs e) {
		string fileName = CSV_FILE_NM_CAST_PV_TERM;

		Response.AddHeader("Content-Disposition","attachment;filename=" + fileName);
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		CastPageViewHourly.SearchCondition oSearchCondition = new CastPageViewHourly.SearchCondition();
		this.SetSearchConditions(ref oSearchCondition);

		using (CastPageViewHourly oCastPageViewHourly = new CastPageViewHourly()) {
			DataSet ds = oCastPageViewHourly.GetPageCollectionTerm(oSearchCondition,0,SysConst.DB_MAX_ROWS);
			DataRow dr;

			StringBuilder oCsvBuilder = new StringBuilder();

			oCsvBuilder.AppendLine(string.Format("集計日付＝{0}～{1}",
				oSearchCondition.ReportDayFrom.Replace("-","/"),
				oSearchCondition.ReportDayTo.Replace("-","/")
			));

			// タイトル行
			oCsvBuilder.Append("年月日");
			oCsvBuilder.Append(",曜日");
			oCsvBuilder.Append(",合計");
			for (int i = 0;i <= 23;i++) {
				oCsvBuilder.Append(",");
				oCsvBuilder.Append(i);
			}
			oCsvBuilder.AppendLine("");

			// データ行
			for (int i = 0;i < ds.Tables[0].Rows.Count;i++) {
				dr = ds.Tables[0].Rows[i];
				DateTime dt = DateTime.Parse(dr["REPORT_DATE"].ToString());
				oCsvBuilder.Append(dt.ToString("yyyy/MM/dd"));
				oCsvBuilder.Append(dt.ToString(",ddd",new CultureInfo("en-US")));
				oCsvBuilder.Append(string.Format(",{0}",dr["TOTAL_COUNT"].ToString()));
				for (int j = 0;j <= 23;j++) {
					oCsvBuilder.Append(string.Format(",{0}",dr[string.Format("COUNT{0:D2}",j)].ToString()));
				}
				oCsvBuilder.AppendLine("");
			}

			string sCsvRec = DisplayWordUtil.Replace(oCsvBuilder.ToString());

			Response.BinaryWrite(encoding.GetBytes(sCsvRec));
			Response.End();
		}
	}

	private void GetList() {
		this.grdCastPageViewTerm.PageIndex = 0;
		this.grdCastPageViewTerm.PageSize = 50;
		this.grdCastPageViewTerm.DataSourceID = "dsCastPageViewTerm";
		this.grdCastPageViewTerm.DataBind();
		this.pnlCount.DataBind();
	}

	protected void grdCastPageViewTerm_RowDataBound(object sender,GridViewRowEventArgs e) {
		DateTime dtThisHour = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd HH:00:00"));
		if (e.Row.RowType == DataControlRowType.DataRow) {
			e.Row.Cells[1].HorizontalAlign = HorizontalAlign.Center;

			for (int i = 2;i < e.Row.Cells.Count;i++) {
				e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
			}

			DataRowView drv = (DataRowView)e.Row.DataItem;
			string sRefineDay = DateTime.Parse(drv["REPORT_DATE"].ToString()).ToString("yyyy/MM/dd");

			for (int i = 0;i < 24;i++) {
				if (dtThisHour <= DateTime.Parse(string.Format("{0} {1}:00:00",sRefineDay,string.Format("{0:D2}",i)))) {
					e.Row.Cells[i + 3].Text = string.Empty;
				}
			}
			
			int iWeekDayNo = int.Parse(DateTime.Parse(sRefineDay).DayOfWeek.ToString("d"));
			
			if (iWeekDayNo == 0) {
				e.Row.BackColor = Color.Pink;
			} else if (iWeekDayNo == 6) {
				e.Row.BackColor = Color.LightBlue;
			}
		} else if (e.Row.RowType == DataControlRowType.Footer) {
			e.Row.BackColor = Color.LightGoldenrodYellow;
			CastPageViewHourly.SearchCondition oSearchCondition = new CastPageViewHourly.SearchCondition();
			this.SetSearchConditions(ref oSearchCondition);

			DataSet oDataSet;

			using (CastPageViewHourly oCastPageViewHourly = new CastPageViewHourly()) {
				oDataSet = oCastPageViewHourly.GetTotalCountCollection(oSearchCondition);
			}

			e.Row.Cells[0].Text = "合計";
			e.Row.Cells[2].Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["TOTAL_COUNT"]);

			for (int i = 0;i < 24;i++) {
				e.Row.Cells[i + 3].Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0][string.Format("COUNT{0:D2}",i)]);
			}

			e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Left;
		}
	}

	private void InitPage() {
		// 月初を初期値に設定
		DateTime oDateFrom = DateTime.Today.AddDays(DateTime.Today.Day * -1 + 1);
		this.lstYearFrom.SelectedValue = oDateFrom.ToString("yyyy");
		this.lstMonthFrom.SelectedValue = oDateFrom.ToString("MM");
		this.lstDayFrom.SelectedValue = oDateFrom.ToString("dd");

		// 当日を初期値に設定
		DateTime oDateTo = DateTime.Today;
		this.lstYearTo.SelectedValue = oDateTo.ToString("yyyy");
		this.lstMonthTo.SelectedValue = oDateTo.ToString("MM");
		this.lstDayTo.SelectedValue = oDateTo.ToString("dd");

		lstAccessType.SelectedValue = PwViCommConst.AccessType.ALL_ACCESS;
		lstLoginType.SelectedValue = ViCommConst.FLAG_ON_STR;
		lstPageUserAgentType.SelectedValue = ViCommConst.DEVICE_SMART_PHONE;
		rdoPaymentFlag.SelectedIndex = 0;
		rdoNewUserFlag.SelectedIndex = 0;
		lstCarrierType.SelectedIndex = 0;
	}

	private void CreateList() {
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		int iStartYear = 2014;
		int iCurYear = DateTime.Now.Year;
		lstYearFrom.Items.Clear();

		for (int i = 0;i + iStartYear <= iCurYear;i++) {

			lstYearFrom.Items.Add(new ListItem(iBridUtil.GetStringValue(i + iStartYear)));
		}

		lstMonthFrom.Items.Clear();
		for (int i = 1;i <= 12;i++) {
			lstMonthFrom.Items.Add(new ListItem(string.Format("{0:D2}",i)));
		}

		lstDayFrom.Items.Clear();
		for (int i = 1;i <= 31;i++) {
			lstDayFrom.Items.Add(new ListItem(string.Format("{0:D2}",i)));
		}

		lstYearTo.Items.Clear();

		for (int i = 0;i + iStartYear <= iCurYear;i++) {

			lstYearTo.Items.Add(new ListItem(iBridUtil.GetStringValue(i + iStartYear)));
		}

		lstMonthTo.Items.Clear();
		for (int i = 1;i <= 12;i++) {
			lstMonthTo.Items.Add(new ListItem(string.Format("{0:D2}",i)));
		}

		lstDayTo.Items.Clear();
		for (int i = 1;i <= 31;i++) {
			lstDayTo.Items.Add(new ListItem(string.Format("{0:D2}",i)));
		}

		lstPage.Items.Clear();
		lstPage.Items.Add(new ListItem("指定しない",string.Empty));
		DataSet oPageDataSet = null;
		using (PageList oPageList = new PageList()) {
			string sPageNm;
			string sProgramRoot;
			string sProgramId;
			string sHtmlDocType;
			string sProgramNm;
			oPageDataSet = oPageList.GetList(lstSiteCd.SelectedValue);
			foreach (DataRow dr in oPageDataSet.Tables[0].Rows) {
				sPageNm = string.Empty;
				sProgramRoot = string.Empty;
				sProgramId = string.Empty;
				sHtmlDocType = string.Empty;
				sProgramNm = string.Empty;
				sProgramRoot = iBridUtil.GetStringValue(dr["PROGRAM_ROOT"]);
				sProgramId = iBridUtil.GetStringValue(dr["PROGRAM_ID"]);
				sHtmlDocType = iBridUtil.GetStringValue(dr["HTML_DOC_TYPE"]);
				sProgramNm = iBridUtil.GetStringValue(dr["PROGRAM_NM"]);
				if (!sHtmlDocType.Equals("-")) {
					sPageNm = sHtmlDocType + " ";
				}

				sPageNm += sProgramNm;

				if (sProgramRoot.Equals(ViCommConst.PGM_ROOT_MAN)) {
					sPageNm += " ♂";
				}

				lstPage.Items.Add(new ListItem(sPageNm,string.Format("{0}:{1}:{2}",sProgramRoot,sProgramId,sHtmlDocType)));
			}
		}
	}

	private bool IsCorrectDate() {
		this.lblErrorMessageReportDayFrom.Visible = false;
		this.lblErrorMessageReportDayTo.Visible = false;

		DateTime? dtReportDayFrom = SysPrograms.TryParseOrDafult(string.Format("{0}/{1}/{2}",this.lstYearFrom.SelectedValue,this.lstMonthFrom.SelectedValue,this.lstDayFrom.SelectedValue),(DateTime?)null);
		DateTime? dtReportDayTo = SysPrograms.TryParseOrDafult(string.Format("{0}/{1}/{2}",this.lstYearTo.SelectedValue,this.lstMonthTo.SelectedValue,this.lstDayTo.SelectedValue),(DateTime?)null);

		if (!dtReportDayFrom.HasValue) {
			this.lblErrorMessageReportDayFrom.Visible = true;
			this.lblErrorMessageReportDayFrom.Text = "集計開始日を正しく入力してください。";
			return false;
		}

		if (!dtReportDayTo.HasValue) {
			this.lblErrorMessageReportDayTo.Visible = true;
			this.lblErrorMessageReportDayTo.Text = "集計終了日を正しく入力してください。";
			return false;
		}

		return true;
	}

	protected string GetRecCount() {
		return this.recCount;
	}
}