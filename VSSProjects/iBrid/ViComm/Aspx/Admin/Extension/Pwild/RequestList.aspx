﻿<%@ Import Namespace="ViComm" %>

<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RequestList.aspx.cs" Inherits="Extension_Pwild_RequestList" Title="お願い一覧" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="お願い一覧"></asp:Label>
</asp:Content>

<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" Runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[お願い編集]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					<asp:LinkButton ID="lnkPictographSample" runat="server" OnClick="lnkPictographSample_Click">
						絵文字一覧
					</asp:LinkButton>
					<table border="0" style="width: 820px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle">
								投稿者
							</td>
							<td class="tdDataStyle">
								<asp:HyperLink ID="lnkLoginId" runat="server"></asp:HyperLink>
								<br />
								<asp:Label ID="lblHandleNm" runat="server"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								タイトル
							</td>
							<td class="tdDataStyle">
								$NO_TRANS_START;
								<asp:TextBox ID="txtTitle" runat="server" Width="400px"></asp:TextBox>
								$NO_TRANS_END;
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								タイトルプレビュー
							</td>
							<td class="tdDataStyle" valign="top">
								<asp:Label ID="lblTitle" runat="server"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								カテゴリ
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstRequestCategorySeq" runat="server" DataSourceID="dsRequestCategory" DataTextField="REQUEST_CATEGORY_NM" DataValueField="REQUEST_CATEGORY_SEQ">
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								進捗状況
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstRequestProgressSeq" runat="server" DataSourceID="dsRequestProgress" DataTextField="REQUEST_PROGRESS_NM" DataValueField="REQUEST_PROGRESS_SEQ">
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								提案内容
							</td>
							<td class="tdDataStyle">
								$NO_TRANS_START;
								<asp:TextBox ID="txtText" runat="server" Width="650px" Rows="5" TextMode="MultiLine"></asp:TextBox>
								$NO_TRANS_END;
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								提案内容プレビュー
							</td>
							<td class="tdDataStyle" valign="top">
								<asp:Label ID="lblText" runat="server"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								管理者コメント
							</td>
							<td class="tdDataStyle">
								$NO_TRANS_START;
								<asp:TextBox ID="txtAdminComment" runat="server" Width="650px" Rows="5" TextMode="MultiLine"></asp:TextBox>
								$NO_TRANS_END;
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								管理者コメントプレビュー
							</td>
							<td class="tdDataStyle" valign="top">
								<asp:Label ID="lblAdminComment" runat="server"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								投稿日時
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblCreateDate" runat="server"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								公開日時
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblPublicDate" runat="server"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle2">
								状態
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstPublicStatus" runat="server">
									<asp:ListItem Value="1">申請中</asp:ListItem>
									<asp:ListItem Value="2">公開</asp:ListItem>
									<asp:ListItem Value="3">非公開</asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
					</table>
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
					<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<fieldset class="fieldset-inner">
				<table border="0" style="width: 700px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイトコード
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle">
							カテゴリ
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSeekRequestCategorySeq" runat="server" DataSourceID="dsRequestCategory" DataTextField="REQUEST_CATEGORY_NM" DataValueField="REQUEST_CATEGORY_SEQ">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							投稿者ID
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtSeekLoginId" runat="server" Width="70px"></asp:TextBox>
						</td>
						<td class="tdHeaderStyle">
							進捗状況
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSeekRequestProgressSeq" runat="server" DataSourceID="dsRequestProgress" DataTextField="REQUEST_PROGRESS_NM" DataValueField="REQUEST_PROGRESS_SEQ">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							ｷｰﾜｰﾄﾞ検索
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtSeekKeyword" runat="server" Width="200px"></asp:TextBox>
						</td>
						<td class="tdHeaderStyle2">
							状態
						</td>
						<td class="tdDataStyle">
							<asp:CheckBox ID="chkSeekApply" runat="server" Text="申請中" />&nbsp;
							<asp:CheckBox ID="chkSeekPublic" runat="server" Text="公開" />&nbsp;
							<asp:CheckBox ID="chkSeekPrivate" runat="server" Text="非公開" />
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Key" />
			</fieldset>
			<fieldset class="fieldset-inner">
				<legend>[お願い一覧]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count
						<%#GetRecCount() %>
					</a>
					<br />
					<a class="reccount">Current viewing page
						<%=grdRequest.PageIndex + 1%>
						of
						<%=grdRequest.PageCount%>
					</a>
					<br />
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlGrid">
					<asp:GridView ID="grdRequest" runat="server" AllowPaging="True" AutoGenerateColumns="False" OnSorting="grdRequest_Sorting"
						EnableSortingAndPagingCallbacks="false" DataSourceID="dsRequest" SkinID="GridView" AllowSorting="True"
						DataKeyNames="SITE_CD,REQUEST_SEQ" OnRowDataBound="grdRequest_RowDataBound">
						<Columns>
							<asp:TemplateField HeaderText="ﾀｲﾄﾙ">
								<ItemTemplate>
									<asp:LinkButton ID="lnkTitle" runat="server" CommandArgument='<%# Container.DataItemIndex %>'
										CommandName="MAINTE" OnCommand="lnkMainte_Command" Text='<%# GetConcatStr(Eval("TITLE"),30) %>'></asp:LinkButton>
								</ItemTemplate>
								<ItemStyle Width="200px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="投稿者">
								<ItemTemplate>
									<asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# this.GetProfUrl(Eval("SITE_CD"),Eval("SEX_CD"),Eval("LOGIN_ID")) %>' Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
									<br />
									<asp:Label ID="lblHandleNm" runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="REQUEST_CATEGORY_NM" HeaderText="ｶﾃｺﾞﾘ">
							</asp:BoundField>
							<asp:BoundField DataField="REQUEST_PROGRESS_NM" HeaderText="進捗状況">
							</asp:BoundField>
							<asp:BoundField DataField="PUBLIC_STATUS_NM" HeaderText="状態">
							</asp:BoundField>
							<asp:TemplateField HeaderText="投稿日時" SortExpression="CREATE_DATE">
								<ItemTemplate>
									<asp:Label ID="lblCreateDate" runat="server" Text='<%# DateTime.Parse(Eval("CREATE_DATE").ToString()).ToString("yy/MM/dd HH:mm") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="良い" SortExpression="GOOD_COUNT">
								<ItemTemplate>
									<asp:HyperLink ID="lnkGoodCount" runat="server" NavigateUrl='<%# this.GetRequestValueUrl(Eval("SITE_CD"),Eval("SEX_CD"),Eval("REQUEST_SEQ"),"&good=1") %>' Text='<%# Eval("GOOD_COUNT") %>'></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="悪い" SortExpression="BAD_COUNT">
								<ItemTemplate>
									<asp:HyperLink ID="lnkBadCount" runat="server" NavigateUrl='<%# this.GetRequestValueUrl(Eval("SITE_CD"),Eval("SEX_CD"),Eval("REQUEST_SEQ"),"&bad=1") %>' Text='<%# Eval("BAD_COUNT") %>'></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="right" />
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</fieldset>
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsRequestCategory" runat="server" SelectMethod="GetRequestCategoryList" TypeName="Request">
		<SelectParameters>
			<asp:ControlParameter ControlID="lstSeekSiteCd" Name="pSiteCd" PropertyName="SelectedValue" Type="String" />
			<asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" DefaultValue="" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsRequestProgress" runat="server" SelectMethod="GetRequestProgressList" TypeName="Request">
		<SelectParameters>
			<asp:ControlParameter ControlID="lstSeekSiteCd" Name="pSiteCd" PropertyName="SelectedValue" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsRequest" runat="server" SelectMethod="GetPageCollection" ConvertNullToDBNull="true" SortParameterName=""
		TypeName="Request" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelected="dsRequest_Selected" OnSelecting="dsRequest_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
</asp:Content>

