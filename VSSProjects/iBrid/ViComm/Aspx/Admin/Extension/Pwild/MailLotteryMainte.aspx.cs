/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メールdeガチャ設定一覧
--	Progaram ID		: MailLotteryMainte
--  Creation Date	: 2013.12.29
--  Creater			: Y.Ikemiya
**************************************************************************/
using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using ViComm.Extension.Pwild;

public partial class Extension_Pwild_MailLotteryMainte:System.Web.UI.Page {
	private string recCount = string.Empty;
	protected string[] YearArray;
	protected string[] MonthArray;
	protected string[] DayArray;
	protected string[] HourArray;
	protected string[] MinuteArray;

	private string MailLotterySeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MAIL_LOTTERY_SEQ"]);
		}
		set {
			this.ViewState["MAIL_LOTTERY_SEQ"] = value;
		}
	}

	private string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		set {
			this.ViewState["SexCd"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["REVISION_NO"]);
		}
		set {
			this.ViewState["REVISION_NO"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			this.SexCd = iBridUtil.GetStringValue(Request.QueryString["sexcd"]);

			if (iBridUtil.GetStringValue(Request.QueryString["sexcd"]).Equals(ViCommConst.MAN)) {
				this.lblPgmTitle.Text = "男性" + lblPgmTitle.Text;
				this.Title = "男性" + this.Title;
			} else {
				this.lblPgmTitle.Text = "女性" + lblPgmTitle.Text;
				this.Title = "女性" + this.Title;
			}

			if (string.IsNullOrEmpty(sSiteCd)) {
				if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
					lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
				} else {
					lstSiteCd.SelectedIndex = 0;
				}
			} else {
				lstSiteCd.SelectedValue = sSiteCd;
			}

			this.pnlSearch.Enabled = true;
			this.pnlInput.Visible = false;

			lstSiteCd.DataBind();
			pnlGrid.DataBind();
			pnlCount.DataBind();

			SetDateArray();
			pnlInput.DataBind();
		}
	}

	protected void dsMailLottery_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		MailLottery.SearchCondition oSearchCondition = new MailLottery.SearchCondition();
		oSearchCondition.SiteCd = lstSiteCd.SelectedValue;
		oSearchCondition.SexCd = this.SexCd;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsMailLottery_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			grdMailLottery.PageIndex = 0;
			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		this.MailLotterySeq = string.Empty;
		this.RevisionNo = string.Empty;
		this.ClearFileds();

		this.txtNeedSecondLotteryCount.Text = "5";
		this.txtRemarks.Text = string.Empty;
		
		this.pnlSearch.Enabled = false;
		this.pnlInput.Visible = true;
		this.btnDelete.Enabled = false;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (CheckInput()) {
			string sResult;
			MailLotteryMainte(ViCommConst.FLAG_OFF,out sResult);

			if (sResult.Equals(PwViCommConst.MailLotteryMainteResult.RESULT_OK)) {
				this.pnlSearch.Enabled = true;
				this.pnlInput.Visible = false;
				grdMailLottery.PageIndex = 0;
				pnlGrid.DataBind();
				pnlCount.DataBind();
			} else if (sResult.Equals(PwViCommConst.MailLotteryMainteResult.RESULT_NG_DATE)) {
				this.lblErrorMessage.Text = "期間が他と重複しています";
			}
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlSearch.Enabled = true;
		this.pnlInput.Visible = false;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		string sResult;
		MailLotteryMainte(ViCommConst.FLAG_ON,out sResult);

		this.pnlSearch.Enabled = true;
		this.pnlInput.Visible = false;
		grdMailLottery.PageIndex = 0;
		pnlGrid.DataBind();
		pnlCount.DataBind();
	}

	protected void lnkEdit_Command(object sender,CommandEventArgs e) {
		int iIndex = int.Parse(iBridUtil.GetStringValue(e.CommandArgument));
		GridViewRow oRow = this.grdMailLottery.Rows[iIndex];
		HiddenField oHiddenField = oRow.FindControl("hdnMailLotterySeq") as HiddenField;
		this.MailLotterySeq = oHiddenField.Value;

		this.ClearFileds();
		this.MailLotteryGet();

		this.pnlSearch.Enabled = false;
		this.pnlInput.Visible = true;
		this.btnDelete.Enabled = true;
	}

	private void SetDateArray() {
		this.YearArray = new string[] { DateTime.Today.AddYears(-1).ToString("yyyy"),DateTime.Today.ToString("yyyy"),DateTime.Today.AddYears(1).ToString("yyyy"),DateTime.Today.AddYears(2).ToString("yyyy") };
		this.MonthArray = new string[] { "01","02","03","04","05","06","07","08","09","10","11","12" };
		this.DayArray = new string[] { "01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31" };
		this.HourArray = new string[] { "00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23" };

		List<string> oMinuteList = new List<string>();

		for (int i = 0;i < 60;i++) {
			oMinuteList.Add(string.Format("{0:D2}",i));
		}

		this.MinuteArray = oMinuteList.ToArray();
	}

	private bool CheckInput() {
		DateTime dtStartDate = new DateTime();
		DateTime dtEndDate = new DateTime();

		if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstStartDateYYYY.SelectedValue,this.lstStartDateMM.SelectedValue,this.lstStartDateDD.SelectedValue,this.lstStartDateHH.SelectedValue,this.lstStartDateMI.SelectedValue),out dtStartDate)) {
			this.lblErrorMessage.Text = "期間開始日時が正しくありません";
			return false;
		}

		if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:59",this.lstEndDateYYYY.SelectedValue,this.lstEndDateMM.SelectedValue,this.lstEndDateDD.SelectedValue,this.lstEndDateHH.SelectedValue,this.lstEndDateMI.SelectedValue),out dtEndDate)) {
			this.lblErrorMessage.Text = "期間終了日時が正しくありません";
			return false;
		}

		if (dtStartDate > dtEndDate) {
			this.lblErrorMessage.Text = "期間の大小関係が正しくありません";
			return false;
		}

		return true;
	}

	private void ClearFileds() {
		this.lblErrorMessage.Text = string.Empty;

		this.lstStartDateYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstStartDateMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstStartDateDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstStartDateHH.SelectedValue = "00";
		this.lstStartDateMI.SelectedValue = "00";

		this.lstEndDateYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstEndDateMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstEndDateDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstEndDateHH.SelectedValue = "23";
		this.lstEndDateMI.SelectedValue = "59";
	}

	private void MailLotteryGet() {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("MAIL_LOTTERY_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pMAIL_LOTTERY_SEQ",DbSession.DbType.VARCHAR2,this.MailLotterySeq);
			oDbSession.ProcedureOutParm("pSTART_DATE",DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pEND_DATE",DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pSEX_CD",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pNEED_SECOND_LOTTERY_COUNT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREMARKS",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();

			this.txtNeedSecondLotteryCount.Text = oDbSession.GetStringValue("pNEED_SECOND_LOTTERY_COUNT");
			this.txtRemarks.Text = oDbSession.GetStringValue("pREMARKS");
			DateTime dtStartDate = (DateTime)oDbSession.GetDateTimeValue("pSTART_DATE");
			DateTime dtEndDate = (DateTime)oDbSession.GetDateTimeValue("pEND_DATE");

			if (dtStartDate != null) {
				this.lstStartDateYYYY.SelectedValue = dtStartDate.ToString("yyyy");
				this.lstStartDateMM.SelectedValue = dtStartDate.ToString("MM");
				this.lstStartDateDD.SelectedValue = dtStartDate.ToString("dd");
				this.lstStartDateHH.SelectedValue = dtStartDate.ToString("HH");
				this.lstStartDateMI.SelectedValue = dtStartDate.ToString("mm");
			}

			if (dtEndDate != null) {
				this.lstEndDateYYYY.SelectedValue = dtEndDate.ToString("yyyy");
				this.lstEndDateMM.SelectedValue = dtEndDate.ToString("MM");
				this.lstEndDateDD.SelectedValue = dtEndDate.ToString("dd");
				this.lstEndDateHH.SelectedValue = dtEndDate.ToString("HH");
				this.lstEndDateMI.SelectedValue = dtEndDate.ToString("mm");
			}
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
		}
	}

	private void MailLotteryMainte(int pDeleteFlag,out string pResult) {
		DateTime dtStartDate = DateTime.Parse(string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstStartDateYYYY.SelectedValue,this.lstStartDateMM.SelectedValue,this.lstStartDateDD.SelectedValue,this.lstStartDateHH.SelectedValue,this.lstStartDateMI.SelectedValue));
		DateTime dtEndDate = DateTime.Parse(string.Format("{0}/{1}/{2} {3}:{4}:59",this.lstEndDateYYYY.SelectedValue,this.lstEndDateMM.SelectedValue,this.lstEndDateDD.SelectedValue,this.lstEndDateHH.SelectedValue,this.lstEndDateMI.SelectedValue));

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("MAIL_LOTTERY_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pMAIL_LOTTERY_SEQ",DbSession.DbType.NUMBER,this.MailLotterySeq);
			oDbSession.ProcedureInParm("pSTART_DATE",DbSession.DbType.DATE,dtStartDate);
			oDbSession.ProcedureInParm("pEND_DATE",DbSession.DbType.DATE,dtEndDate);
			oDbSession.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,this.SexCd);
			oDbSession.ProcedureInParm("pNEED_SECOND_LOTTERY_COUNT",DbSession.DbType.VARCHAR2,this.txtNeedSecondLotteryCount.Text);
			oDbSession.ProcedureInParm("pREMARKS",DbSession.DbType.VARCHAR2,this.txtRemarks.Text);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDeleteFlag);
			oDbSession.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
			pResult = oDbSession.GetStringValue("pRESULT");
		}
	}

	protected void tdcRemarks_ServerValidate(object sender,ServerValidateEventArgs e) {
		if (this.IsValid) {

			if (this.txtRemarks.Text.Length > this.txtRemarks.MaxLength) {
				e.IsValid = false;
			}
		}
	}

	protected string SubStringRemarks(object pRemarks) {
		string sRemarks = iBridUtil.GetStringValue(pRemarks);

		if (sRemarks.Length > 10) {
			sRemarks = sRemarks.Substring(0,10);
		}

		return sRemarks;
	}
}
