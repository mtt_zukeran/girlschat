﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 美女コレ クエストクリア報酬一覧

--	Progaram ID		: QuestRewardList
--
--  Creation Date	: 2011.07.25
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Ratelain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.IO;
using ViComm.Extension.Pwild;

public partial class Extension_QuestRewardList:System.Web.UI.Page {
	
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		set {
			this.ViewState["SexCd"] = value;
		}
	}

	private string QuestSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["QuestSeq"]);
		}
		set {
			this.ViewState["QuestSeq"] = value;
		}
	}

	private string QuestRewardSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["QuestRewardSeq"]);
		}
		set {
			this.ViewState["QuestRewardSeq"] = value;
		}
	}

	private string LevelQuestSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["LevelQuestSeq"]);
		}
		set {
			this.ViewState["LevelQuestSeq"] = value;
		}
	}

	private string LittleQuestSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["LittleQuestSeq"]);
		}
		set {
			this.ViewState["LittleQuestSeq"] = value;
		}
	}

	private string OtherQuestSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["OtherQuestSeq"]);
		}
		set {
			this.ViewState["OtherQuestSeq"] = value;
		}
	}

	private string QuestType {
		get {
			return iBridUtil.GetStringValue(this.ViewState["QuestType"]);
		}
		set {
			this.ViewState["QuestType"] = value;
		}
	}

	private string GameItemSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemSeq"]);
		}
		set {
			this.ViewState["GameItemSeq"] = value;
		}
	}

	private string GameItemCategoryType {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemCategoryType"]);
		}
		set {
			this.ViewState["GameItemCategoryType"] = value;
		}
	}

	private string LotterySeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["LotterySeq"]);
		}
		set {
			this.ViewState["LotterySeq"] = value;
		}
	}

	private string CreateFlag {
		get {
			return iBridUtil.GetStringValue(this.ViewState["CreateFlag"]);
		}
		set {
			this.ViewState["CreateFlag"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	private string GameItemGetCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemGetCd"]);
		}
		set {
			this.ViewState["GameItemGetCd"] = value;
		}
	}

	private string GameItemPresent {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemPresent"]);
		}
		set {
			this.ViewState["GameItemPresent"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();

			this.SiteCd = iBridUtil.GetStringValue(this.Request.QueryString["sitecd"]);
			this.QuestSeq = iBridUtil.GetStringValue(this.Request.QueryString["questseq"]);
			this.LevelQuestSeq = iBridUtil.GetStringValue(this.Request.QueryString["levelquestseq"]);
			this.LittleQuestSeq = iBridUtil.GetStringValue(this.Request.QueryString["littlequestseq"]);
			this.OtherQuestSeq = iBridUtil.GetStringValue(this.Request.QueryString["otherquestseq"]);
			this.QuestType = iBridUtil.GetStringValue(this.Request.QueryString["questtype"]);
			this.SexCd = iBridUtil.GetStringValue(this.Request.QueryString["sexcd"]);
			this.CreateFlag = iBridUtil.GetStringValue(this.Request.QueryString["create"]);

			if (!string.IsNullOrEmpty(this.SiteCd) && !string.IsNullOrEmpty(this.QuestRewardSeq)) {
				this.lstSiteCd.SelectedValue = this.SiteCd;

				if (this.CreateFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					this.GameItemCategoryType = string.Empty;
					this.GameItemSeq = string.Empty;
					this.LotterySeq = string.Empty;
					this.pnlKey.Enabled = false;
					this.pnlQuestRewardInfo.Visible = true;
				} else {
					this.GetList();
				}
			}
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
		this.QuestRewardSeq = string.Empty;
		this.btnDelete.Enabled = true;
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		this.lblErrorMessageCount.Text = string.Empty;
		this.lblErrorMessageTicketCount.Text = string.Empty;
		this.lblErrorMessageGamePoint.Text = string.Empty;
		this.lblErrorMessageItem.Text = string.Empty;
		if (!this.IsValid) {
			return;
		}
		if (!this.IsCorrectInput()) {
			return;
		}
		if (!this.UpdateData(false)) {
			return;
		}
		this.pnlQuestRewardInfo.Visible = false;
		this.pnlKey.Enabled = true;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		this.UpdateData(true);
		this.pnlQuestRewardInfo.Visible = false;
		this.pnlKey.Enabled = true;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlQuestRewardInfo.Visible = false;
		this.pnlKey.Enabled = true;
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		this.QuestRewardSeq = string.Empty;
		this.ClearFileds();
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.GameItemCategoryType = string.Empty;
		this.GameItemSeq = string.Empty;
		this.CreateFlag = ViCommConst.FLAG_ON_STR;
		this.btnDelete.Enabled = false;
		this.pnlKey.Enabled = false;
		this.pnlQuestRewardInfo.Visible = true;
		this.ClearFileds();
	}

	protected void btnBack_Click(object sender,EventArgs e) {
		string sBackPath = string.Empty;
		
		if(this.QuestType.Equals(PwViCommConst.GameQuestType.LEVEL_QUEST)) {
			sBackPath = string.Format("LevelQuestList.aspx?questseq={0}&levelquestseq={1}&sexcd={2}",this.QuestSeq,this.LevelQuestSeq,this.SexCd);
		} else if(this.QuestType.Equals(PwViCommConst.GameQuestType.LITTLE_QUEST)) {
			sBackPath = string.Format("LittleQuestList.aspx?questseq={0}&levelquestseq={1}&littlequestseq={2}&sexcd={3}",this.QuestSeq,this.LevelQuestSeq,this.LittleQuestSeq,this.SexCd);
		} else if(this.QuestType.Equals(PwViCommConst.GameQuestType.EX_QUEST)) {
			sBackPath = string.Format("LevelQuestList.aspx?questseq={0}&levelquestseq={1}&sexcd={2}",this.QuestSeq,this.LevelQuestSeq,this.SexCd);
		} else if(this.QuestType.Equals(PwViCommConst.GameQuestType.OTHER_QUEST)) {
			sBackPath = string.Format("OtherQuestList.aspx?questseq={0}&otherquestseq={1}&sexcd={2}",this.QuestSeq,this.OtherQuestSeq,this.SexCd);
		}

		this.Response.Redirect(string.Format("~/Extension/Pwild/{0}",sBackPath));
	}

	protected void lnkQuestReward_Command(object sender,CommandEventArgs e) {
		
		this.GameItemGetCd = null;
		this.GameItemPresent = null;
		
		if (!this.IsValid) {
			return;
		}
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.QuestRewardSeq = iBridUtil.GetStringValue(e.CommandArgument);

		this.CreateFlag = string.Empty;
		this.pnlKey.Enabled = false;
		this.btnDelete.Enabled = true;
		this.GetData();

		this.pnlQuestRewardInfo.Visible = true;
	}

	protected void dsQuestReward_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsGameItem_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pGameItemCategoryType"] = this.GameItemCategoryType;
		e.InputParameters["pGameItemGetCd"] = this.GameItemGetCd;
		e.InputParameters["pPresentFlag"] = this.GameItemPresent;
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	protected void lstGameItemCategory_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		this.GameItemCategoryType = oDropDownList.SelectedValue;
		this.lstGameItem.DataBind();
	}

	protected void lstItemGetCd_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		this.GameItemGetCd = oDropDownList.SelectedValue;
		this.lstGameItem.DataBind();
	}

	protected void lstItemPresent_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		this.GameItemPresent = oDropDownList.SelectedValue;
		this.lstGameItem.DataBind();
	}

	private void InitPage() {
		this.pnlInfo.Visible = true;
		this.pnlQuestRewardInfo.Visible = false;

		this.QuestRewardSeq = string.Empty;
		this.GameItemSeq = string.Empty;
		this.pnlKey.Enabled = true;
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void ClearFileds() {
		this.txtItemCount.Text = string.Empty;
		this.txtTicketCount.Text = string.Empty;
		this.txtGamePoint.Text = string.Empty;
		this.lstGameItemCategory.DataBind();
		this.lstGameItem.DataBind();
		this.lstLottery.DataBind();

		this.lblErrorMessageItem.Text = string.Empty;
		this.lstItemGetCd.SelectedValue = null;
		this.lstItemPresent.SelectedValue = null;
		this.GameItemGetCd = null;
		this.GameItemPresent = null;
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.QuestRewardSeq = string.Empty;

		this.grdQuestReward.PageIndex = 0;
		this.grdQuestReward.DataSourceID = "dsQuestReward";
		this.grdQuestReward.DataBind();
	}

	protected string CheckNoItem(object pGameItemNm) {
		if (iBridUtil.GetStringValue(pGameItemNm).Equals(string.Empty)) {
			return "ｱｲﾃﾑなし";
		} else {
			return pGameItemNm.ToString();
		}
	}

	private void GetData() {
		this.ClearFileds();

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("QUEST_REWARD_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pQUEST_SEQ",DbSession.DbType.VARCHAR2,this.QuestSeq);
			oDbSession.ProcedureInParm("pQUEST_REWARD_SEQ",DbSession.DbType.VARCHAR2,this.QuestRewardSeq);
			oDbSession.ProcedureOutParm("pQUEST_TYPE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pLEVEL_QUEST_SEQ",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pLITTLE_QUEST_SEQ",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pOTHER_QUEST_SEQ",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pGAME_POINT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pGAME_ITEM_SEQ",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pGAME_ITEM_CATEGORY_TYPE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pGAME_ITEM_COUNT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pLOTTERY_SEQ",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pTICKET_COUNT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
			this.txtGamePoint.Text = oDbSession.GetStringValue("pGAME_POINT");
			this.txtItemCount.Text = oDbSession.GetStringValue("pGAME_ITEM_COUNT");
			this.txtTicketCount.Text = oDbSession.GetStringValue("pTICKET_COUNT");
			this.lstGameItemCategory.SelectedValue = oDbSession.GetStringValue("pGAME_ITEM_CATEGORY_TYPE");
			this.GameItemCategoryType = this.lstGameItemCategory.SelectedValue;
			this.lstGameItem.DataBind();
			this.GameItemSeq = oDbSession.GetStringValue("pGAME_ITEM_SEQ");
			this.lstGameItem.SelectedValue = this.GameItemSeq;
			this.lstLottery.DataBind();
			this.LotterySeq = oDbSession.GetStringValue("pLOTTERY_SEQ");
			this.lstLottery.SelectedValue = this.LotterySeq;
		}
	}

	private bool UpdateData(bool pDeleteFlag) {

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("QUEST_REWARD_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureBothParm("pQUEST_SEQ",DbSession.DbType.VARCHAR2,this.QuestSeq);
			oDbSession.ProcedureBothParm("pQUEST_REWARD_SEQ",DbSession.DbType.VARCHAR2,this.QuestRewardSeq);
			oDbSession.ProcedureInParm("pQUEST_TYPE",DbSession.DbType.VARCHAR2,this.QuestType);
			oDbSession.ProcedureInParm("pLEVEL_QUEST_SEQ",DbSession.DbType.VARCHAR2,this.LevelQuestSeq);
			oDbSession.ProcedureInParm("pLITTLE_QUEST_SEQ",DbSession.DbType.VARCHAR2,this.LittleQuestSeq);
			oDbSession.ProcedureInParm("pOTHER_QUEST_SEQ",DbSession.DbType.VARCHAR2,this.OtherQuestSeq);
			oDbSession.ProcedureInParm("pGAME_POINT",DbSession.DbType.VARCHAR2,this.txtGamePoint.Text);
			oDbSession.ProcedureInParm("pGAME_ITEM_SEQ",DbSession.DbType.VARCHAR2,this.lstGameItem.SelectedValue);
			oDbSession.ProcedureInParm("pGAME_ITEM_COUNT",DbSession.DbType.NUMBER,this.txtItemCount.Text);
			oDbSession.ProcedureInParm("pLOTTERY_SEQ",DbSession.DbType.VARCHAR2,this.lstLottery.SelectedValue);
			oDbSession.ProcedureInParm("pTICKET_COUNT",DbSession.DbType.NUMBER,this.txtTicketCount.Text);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDeleteFlag ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.ClearFileds();
		this.GetList();
		return true;
	}

	private bool IsCorrectInput() {
		this.lblErrorMessageItem.Visible = false;
		bool bResult = true;

		if (string.IsNullOrEmpty(this.lstGameItem.SelectedValue) && (string.IsNullOrEmpty(this.txtGamePoint.Text) || this.txtGamePoint.Text == "0") && string.IsNullOrEmpty(this.lstLottery.SelectedValue)) {
			this.lblErrorMessageItem.Text = "アイテム、ゲームポイント、ガチャチケットのいずれかを選択してください。";
			this.lblErrorMessageItem.Visible = true;
			bResult = false;
		}

		if (string.IsNullOrEmpty(this.txtItemCount.Text)) {
			this.txtItemCount.Text = "0";
		}
		if (string.IsNullOrEmpty(this.txtGamePoint.Text)) {
			this.txtGamePoint.Text = "0";
		}
		if (string.IsNullOrEmpty(this.txtTicketCount.Text)) {
			this.txtTicketCount.Text = "0";
		}
		
		if(!string.IsNullOrEmpty(this.lstGameItem.SelectedValue) && this.txtItemCount.Text == "0") {
			this.lblErrorMessageCount.Text = "アイテム個数を入力してください";
			this.lblErrorMessageCount.Visible = true;
			bResult = false;
		}

		if (!string.IsNullOrEmpty(this.lstLottery.SelectedValue) && this.txtTicketCount.Text == "0") {
			this.lblErrorMessageTicketCount.Text = "チケット枚数を入力してください";
			this.lblErrorMessageTicketCount.Visible = true;
			bResult = false;
		}

		return bResult;
	}

	protected void lstItemGetCd_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		oDropDownList.Items.Insert(3,new ListItem("戦利品","3"));
	}
}
