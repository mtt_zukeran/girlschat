﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;

public partial class Extension_Pwild_PageViewSummaryTargetList:System.Web.UI.Page {
	private int iRecCount = 0;

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	/// <summary>
	/// 初期化
	/// </summary>
	private void InitPage() {
		pnlAppend.Visible = false;
		pnlList.Visible = false;
		grdPageViewTarget.DataSourceID = "";
		iRecCount = 0;

		// 検索項目
		if (lstSiteCd.Items.Count > 0) {
			lstSiteCd.SelectedIndex = 0;
		}
		lstSiteCd.Enabled = true;
		txtProgramNm.Text = string.Empty;
		txtProgramId.Text = string.Empty;
		txtHtmlDocType.Text = string.Empty;
		rdoSexCd.SelectedIndex = 0;

		// エラー表示
		vdcInProgramId.IsValid = true;
		vdcInProgramIdExists.IsValid = true;
		vdcInHtmlDocType.IsValid = true;
		vdcInHtmlDocTypeExists.IsValid = true;
	}

	/// <summary>
	/// 一覧データ取得
	/// </summary>
	private void GetList() {
		grdPageViewTarget.PageIndex = 0;
		grdPageViewTarget.PageSize = 50;
		grdPageViewTarget.DataSourceID = "dsPageViewTarget";
		grdPageViewTarget.DataBind();
		pnlCount.DataBind();
		pnlList.Visible = true;
	}

	/// <summary>
	/// 検索前イベント
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void dsPageViewTarget_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		PageViewTarget.SearchCondition oSearchCondition = new PageViewTarget.SearchCondition();
		oSearchCondition.SiteCd = lstSiteCd.SelectedValue;
		oSearchCondition.ProgramNm = txtProgramNm.Text;
		oSearchCondition.ProgramId = txtProgramId.Text;
		oSearchCondition.HtmlDocType = txtHtmlDocType.Text;
		oSearchCondition.SexCd = rdoSexCd.SelectedValue;
		e.InputParameters[0] = oSearchCondition;
	}

	/// <summary>
	/// 検索終了イベント
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void dsPageViewTarget_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null && iRecCount.GetType().Equals(e.ReturnValue.GetType())) {
			this.iRecCount = int.Parse(e.ReturnValue.ToString());
		}
	}

	protected void grdPageViewTarget_RowDataBound(object sender,GridViewRowEventArgs e) {
	}

	/// <summary>
	/// 検索ボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		GetList();
	}

	/// <summary>
	/// クリアボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	/// <summary>
	/// 対象追加ボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnAppend_Click(object sender,EventArgs e) {
		pnlAppend.Visible = true;
		lstSiteCd.Enabled = false;

		// 入力欄初期化
		txtInProgramId.Text = string.Empty;
		txtInHtmlDocType.Text = string.Empty;
		rdoInSexCd.SelectedIndex = 0;
	}

	/// <summary>
	/// 登録ボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnRegist_Click(object sender,EventArgs e) {
		if (!validates()) {
			return;
		}
		lstSiteCd.Enabled = true;

		// 入力されたデータを設定
		PageViewTarget.MainteData oMainteData = new PageViewTarget.MainteData();
		oMainteData.SiteCd = lstSiteCd.SelectedValue;
		oMainteData.ProgramId = txtInProgramId.Text;
		oMainteData.HtmlDocType = txtInHtmlDocType.Text;
		oMainteData.SexCd = rdoInSexCd.SelectedValue;

		// 登録処理実行
		string sStatus = string.Empty;
		PageViewTarget oPageViewTarget = new PageViewTarget();
		oPageViewTarget.Mainte(oMainteData,out sStatus);
		pnlAppend.Visible = false;

		// 一覧データ再取得
		GetList();
	}

	/// <summary>
	/// 削除リンク押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void lnkDelete_Command(object sender,CommandEventArgs e) {
		string[] arguments = e.CommandArgument.ToString().Split(':');

		// 入力されたデータを設定
		PageViewTarget.MainteData oMainteData = new PageViewTarget.MainteData();
		oMainteData.SiteCd = arguments[0];
		oMainteData.ProgramRoot = arguments[1];
		oMainteData.ProgramId = arguments[2];
		oMainteData.HtmlDocType = arguments[3];
		oMainteData.DelFlag = ViCommConst.FLAG_ON_STR;

		// 登録処理実行
		string sStatus = string.Empty;
		PageViewTarget oPageViewTarget = new PageViewTarget();
		oPageViewTarget.Mainte(oMainteData,out sStatus);

		// 一覧データ再取得
		GetList();
	}

	/// <summary>
	/// 登録・編集時のバリデーション
	/// </summary>
	/// <returns></returns>
	private bool validates() {
		txtInProgramId.Text = txtInProgramId.Text.Trim();
		txtInHtmlDocType.Text = txtInHtmlDocType.Text.Trim();

		// プログラムIDとHTML文章コードの両方、未入力はNG
		if (string.IsNullOrEmpty(txtInProgramId.Text)
			&& string.IsNullOrEmpty(txtInHtmlDocType.Text)
		) {
			vdcInProgramId.IsValid = false;
			vdcInHtmlDocType.IsValid = false;
			return false;
		}

		// HTML文章コード入力時、プログラムIDは静的ページ以外NG
		if (!string.IsNullOrEmpty(txtInHtmlDocType.Text)
			&& !string.IsNullOrEmpty(txtInProgramId.Text)
			&& !(txtInProgramId.Text.Equals("DisplayDoc.aspx")
				|| txtInProgramId.Text.Equals("GameDisplayDoc.aspx"))
		) {
			vdcInProgramId.IsValid = false;
			vdcInHtmlDocType.IsValid = false;
			return false;
		}

		// 入力されたデータを設定
		PageViewTarget.SearchCondition oParam = new PageViewTarget.SearchCondition();
		oParam.SiteCd = lstSiteCd.SelectedValue;
		oParam.ProgramId = txtInProgramId.Text;
		oParam.HtmlDocType = txtInHtmlDocType.Text;
		oParam.SexCd = rdoInSexCd.SelectedValue;

		// 入力されたページが存在するかをチェック
		PageViewTarget oPageViewTarget = new PageViewTarget();
		if (!oPageViewTarget.IsExistsTargetPage(oParam)) {
			if (!string.IsNullOrEmpty(txtInHtmlDocType.Text)) {
				vdcInHtmlDocTypeExists.IsValid = false;
			} else {
				vdcInProgramIdExists.IsValid = false;
			}
			return false;
		}

		return true;
	}

	/// <summary>
	/// 画面名称リンクのURL取得
	/// </summary>
	/// <param name="pProgramRoot"></param>
	/// <param name="pProgramId"></param>
	/// <param name="pHtmlDocType"></param>
	/// <returns></returns>
	protected string GetNavigateUrlPageMainte(object pProgramRoot,object pProgramId,object pHtmlDocType) {
		string sProgramRoot = pProgramRoot.ToString();
		string sProgramId = pProgramId.ToString();
		string sHtmlDocType = pHtmlDocType.ToString();

		string sNavigateUrl = string.Empty;

		if (sProgramId.Equals("DisplayDoc.aspx") || sProgramId.Equals("GameDisplayDoc.aspx")) {
			sNavigateUrl = string.Format("~/Site/SiteHtmlDocList.aspx?sitecd={0}&htmldoctype={1}&direct=1",
								this.lstSiteCd.SelectedValue,
								sHtmlDocType
							);
		} else {
			sNavigateUrl = string.Format("~/Site/UserViewList.aspx?sitecd={0}&pgmroot={1}&pgmid={2}&direct=1",
								this.lstSiteCd.SelectedValue,
								sProgramRoot,
								sProgramId
							);
		}

		return sNavigateUrl;
	}

	/// <summary>
	/// 検索結果の件数取得
	/// </summary>
	/// <returns></returns>
	protected int GetRecCount() {
		return this.iRecCount;
	}

}
