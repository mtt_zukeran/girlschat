﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: アイドルつぶやき

--	Progaram ID		: CastTweetCommentList
--
--  Creation Date	: 2013.02.21
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.Collections.Generic;

public partial class Extension_Pwild_CastTweetCommentList:System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string LoginId {
		get {
			return iBridUtil.GetStringValue(this.ViewState["LoginId"]);
		}
		set {
			this.ViewState["LoginId"] = value;
		}
	}

	private string CastTweetSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["CastTweetSeq"]);
		}
		set {
			this.ViewState["CastTweetSeq"] = value;
		}
	}

	private DataSet CastTweetCommentData {
		get {
			return this.ViewState["CastTweetCommentData"] as DataSet;
		}
		set {
			this.ViewState["CastTweetCommentData"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();

			this.pnlInfo.Visible = true;
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void dsCastTweetComment_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		DataSet oCastTweetCommentDataSet = e.ReturnValue as DataSet;
		if (oCastTweetCommentDataSet != null && (oCastTweetCommentDataSet).Tables[0].Rows.Count > 0) {
			this.CastTweetCommentData = oCastTweetCommentDataSet;
		}
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void grdCastTweetComment_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			if (DataBinder.Eval(e.Row.DataItem,"ADMIN_DEL_FLAG").ToString().Equals(ViCommConst.FLAG_ON_STR)) {
				e.Row.ForeColor = Color.Gray;
			}
		}
	}

	protected void dsCastTweetComment_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		CastTweetComment.SearchCondition oSearchCondition = new CastTweetComment.SearchCondition();
		oSearchCondition.Keyword = this.txtKeyword.Text.Trim();
		oSearchCondition.LoginId = this.txtLoginId.Text.Trim();
		oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
		oSearchCondition.CastTweetSeq = this.CastTweetSeq;
		e.InputParameters[0] = oSearchCondition;
	}

	private void InitPage() {

		this.recCount = "0";
		this.txtLoginId.Text = string.Empty;
		this.txtKeyword.Text = string.Empty;
		this.grdCastTweetComment.DataSourceID = string.Empty;

		this.pnlInfo.Visible = false;
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		if (!this.IsPostBack) {
			this.SiteCd = this.Request.QueryString["sitecd"];
			this.CastTweetSeq = this.Request.QueryString["tweetseq"];


			if (!string.IsNullOrEmpty(this.SiteCd) && !string.IsNullOrEmpty(this.CastTweetSeq)) {
				this.lstSiteCd.SelectedValue = this.SiteCd;

				this.pnlInfo.Visible = true;
				this.GetList();
			}
		}
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.grdCastTweetComment.PageIndex = 0;
		this.grdCastTweetComment.PageSize = 10;
		this.grdCastTweetComment.DataSourceID = "dsCastTweetComment";
		this.grdCastTweetComment.DataBind();
		this.pnlCount.DataBind();
	}

	protected void lnkDelCastTweetComment_Command(object sender,CommandEventArgs e) {
		string[] arguments = e.CommandArgument.ToString().Split(',');

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("DELETE_CAST_TWEET_COMMENT_ADM");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,arguments[0]);
			oDbSession.ProcedureInParm("pCAST_TWEET_COMMENT_SEQ",DbSession.DbType.VARCHAR2,arguments[1]);
			oDbSession.ProcedureInParm("pDEL_FLAG",DbSession.DbType.VARCHAR2,ViCommConst.FLAG_ON_STR.Equals(arguments[2]) ? ViCommConst.FLAG_OFF_STR : ViCommConst.FLAG_ON_STR);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.GetList();
	}
}
