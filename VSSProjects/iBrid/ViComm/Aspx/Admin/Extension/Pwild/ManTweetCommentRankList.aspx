﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ManTweetCommentRankList.aspx.cs" Inherits="Extension_Pwild_ManTweetCommentRankList" Title="つぶやきコメントランキング表示順位設定" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="つぶやきコメントランキング表示順位設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HolderContent" Runat="Server">
	<div class="admincontent">
	    <asp:Panel runat="server" ID="pnlKey">
	        <fieldset class="fieldset">
	            <asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton" ValidationGroup="Create" OnClick="btnCreate_Click" />
	            <asp:Button runat="server" ID="btnBack" Text="戻る" CssClass="seekbutton" OnClick="btnBack_Click" Visible="False" />
	        </fieldset>
	    </asp:Panel>
		<asp:Panel runat="server" ID="pnlItemInfo">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Label ID="lblErrorMessage" runat="server" Text="" Visible="false" ForeColor="red"></asp:Label>&nbsp;
				<table border="0" style="width: 650px" class="tableStyle">
				    <tr>
						<td class="tdHeaderStyle">
							SEQ
						</td>
						<td class="tdDataStyle">
						    <asp:Label ID="lblSeq" runat="server" ></asp:Label>
						</td>
                    </tr>
					<tr style="display:none">
						<td class="tdHeaderStyle">
							性別
						</td>
<%--						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSex" runat="server" Width="60px" DataSource='<%# SexArray %>'>
							</asp:DropDownList>
						</td>
--%>				</tr>
					<tr>
						<td class="tdHeaderStyle">
							現在ランキング表示順位
						</td>
						<td class="tdDataStyle">
						    <asp:TextBox ID="txtDispRankNow" runat="server" Width="99%"></asp:TextBox>
						</td>
                    </tr>
                    <tr>
						<td class="tdHeaderStyle">
							過去ランキング表示順位
						</td>
						<td class="tdDataStyle">
						    <asp:TextBox ID="txtDispRankPast" runat="server" Width="99%"></asp:TextBox>
						</td>
                    </tr>
                    <tr>
						<td class="tdHeaderStyle2">
							管理画面ランキング表示順位
						</td>
                        <td class="tdDataStyle">
						    <asp:TextBox ID="txtDispRankManage" runat="server" Width="99%"></asp:TextBox>
						</td>
                    </tr>
				</table>
				<asp:Panel ID="pnlCommand" runat="server" Width="660px">
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Update" OnClick="btnUpdate_Click" />
					<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" CausesValidation="False" Visible="true" OnClick="btnCancel_Click" />
					<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="delBtnStyle" ValidationGroup="Delete" OnClick="btnDelete_Click" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlInfo" CssClass="f_clear">
			<fieldset>
				<legend>[設定一覧]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">
						Record Count <%# GetRecCount() %>
					</a>
					<br />
					<a class="reccount">
						Current viewing page <%= grdManTweetCommentDispRank.PageIndex + 1%> of <%= grdManTweetCommentDispRank.PageCount%>
					</a>
				</asp:Panel>
				&nbsp;
				<asp:Panel ID="pnlGrid" runat="server">
					<asp:GridView ID="grdManTweetCommentDispRank" runat="server" AllowPaging="True" AutoGenerateColumns="False" PageSize="30" SkinID="GridViewColor" AllowSorting="True">
					    <Columns>
					        <asp:BoundField DataField="DISP_RANK_SEQ" HeaderText="SEQ" HtmlEncode="False">
                                <ItemStyle HorizontalAlign="Right" Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="SEX_CD" HeaderText="性別" HtmlEncode="False" Visible="False">
                                <ItemStyle HorizontalAlign="Center" Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DISP_RANK_NO_NOW" HeaderText="表示順位（現在）" HtmlEncode="False">
                                <ItemStyle HorizontalAlign="Left" Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DISP_RANK_NO_PAST" HeaderText="表示順位（過去）" HtmlEncode="False">
                                <ItemStyle HorizontalAlign="Left" Wrap="False" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DISP_RANK_NO_MANAGE" HeaderText="表示順位（管理）" HtmlEncode="False">
                                <ItemStyle HorizontalAlign="Left" Wrap="False" />
                            </asp:BoundField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%# Container.DataItemIndex %>' OnCommand="lnkEdit_Command" Text="編集"></asp:LinkButton>
                                    <asp:HiddenField ID="hdnDispRankSeq" runat="server" Value='<%# Eval("DISP_RANK_SEQ") %>' />
                                    <asp:HiddenField ID="hdnSexCD" runat="server" Value='<%# Eval("SEX_CD") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
					    </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
			<br />
		</asp:Panel>
	</div>
    <asp:ObjectDataSource ID="dsManTweetCommentDispRank" runat="server" ConvertNullToDBNull="false" EnablePaging="True"
	 SortParameterName="" TypeName="ManTweetCommentDispRank" OnSelected="dsManTweetCommentDispRank_Selected" OnSelecting="dsManTweetCommentDispRank_Selecting" SelectCountMethod="GetPageCount" SelectMethod="GetPageCollection">
    </asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除するとランキングデータも削除されます。それでも削除しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>

