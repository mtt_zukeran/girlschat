﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BbsBingoCardList.aspx.cs" Inherits="Extension_Pwild_BbsBingoCardList" Title="お宝deビンゴカード" ValidateRequest="false" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="お宝deビンゴカード"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlRegist">
			<fieldset class="fieldset">
				<legend>[カード追加]</legend>
				<asp:Label ID="lblRegistError" runat="server" Text="" ForeColor="red"></asp:Label>
				<table border="0" style="width: 650px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							作成済み枚数
						</td>
						<td class="tdDataStyle">
							<asp:Label ID="lblCardCount" runat="server" Text='<%# GetCardCount().ToString() %>'></asp:Label>枚
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							追加数
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtAddCount" runat="server" Width="50px"></asp:TextBox>枚
						</td>
					</tr>
				</table>
				<asp:Panel ID="pnlCommand" runat="server" Width="660px">
					<asp:Button runat="server" ID="btnRegist" Text="追加" CssClass="seekbutton" OnClick="btnRegist_Click" />
					<asp:Button runat="server" ID="btnBack" Text="戻る" CssClass="seekbutton" OnClick="btnBack_Click" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlList">
			<fieldset>
				<legend>[一覧]</legend>
				<table style="width:900px;">
					<tr>
						<td>
							<asp:Button ID="btnHighPrbFlagOn" runat="server" CausesValidation="false" CssClass="seekbutton" OnClick="btnHighPrbFlagOn_Click" Text="一括高確率設定" />
							<asp:Button ID="btnHighPrbFlagOff" runat="server" CausesValidation="false" CssClass="seekbutton" OnClick="btnHighPrbFlagOff_Click" Text="一括通常設定" />
						</td>
					</tr>
					<tr>
						<td>
							<asp:Panel ID="pnlCard" runat="server">
								<asp:Repeater ID="rptBbsBingoCard" runat="server" DataSourceID="">
									<HeaderTemplate>
										<table><tr>
									</HeaderTemplate>
									<ItemTemplate>
										<td>
											<table>
												<tr>
													<td>
														<table class="tableStyle" border="0" style="width: 100px">
															<tr><td class="tdHeaderStyle2" colspan="5"><%# Eval("CARD_NO") %></td></tr>
															<tr><td class="tdDataStyle"><%# Eval("BINGO_NO_1","{0:00}")%></td><td class="tdDataStyle"><%# Eval("BINGO_NO_2","{0:00}")%></td><td class="tdDataStyle"><%# Eval("BINGO_NO_3","{0:00}")%></td><td class="tdDataStyle"><%# Eval("BINGO_NO_4","{0:00}")%></td><td class="tdDataStyle"><%# Eval("BINGO_NO_5","{0:00}")%></td></tr>
															<tr><td class="tdDataStyle"><%# Eval("BINGO_NO_6","{0:00}")%></td><td class="tdDataStyle"><%# Eval("BINGO_NO_7","{0:00}")%></td><td class="tdDataStyle"><%# Eval("BINGO_NO_8","{0:00}")%></td><td class="tdDataStyle"><%# Eval("BINGO_NO_9","{0:00}")%></td><td class="tdDataStyle"><%# Eval("BINGO_NO_10","{0:00}")%></td></tr>
															<tr><td class="tdDataStyle"><%# Eval("BINGO_NO_11","{0:00}")%></td><td class="tdDataStyle"><%# Eval("BINGO_NO_12","{0:00}")%></td><td class="tdDataStyle"><%# Eval("BINGO_NO_13","{0:00}")%></td><td class="tdDataStyle"><%# Eval("BINGO_NO_14","{0:00}")%></td><td class="tdDataStyle"><%# Eval("BINGO_NO_15","{0:00}")%></td></tr>
															<tr><td class="tdDataStyle"><%# Eval("BINGO_NO_16","{0:00}")%></td><td class="tdDataStyle"><%# Eval("BINGO_NO_17","{0:00}")%></td><td class="tdDataStyle"><%# Eval("BINGO_NO_18","{0:00}")%></td><td class="tdDataStyle"><%# Eval("BINGO_NO_19","{0:00}")%></td><td class="tdDataStyle"><%# Eval("BINGO_NO_20","{0:00}")%></td></tr>
															<tr><td class="tdDataStyle"><%# Eval("BINGO_NO_21","{0:00}")%></td><td class="tdDataStyle"><%# Eval("BINGO_NO_22","{0:00}")%></td><td class="tdDataStyle"><%# Eval("BINGO_NO_23","{0:00}")%></td><td class="tdDataStyle"><%# Eval("BINGO_NO_24","{0:00}")%></td><td class="tdDataStyle"><%# Eval("BINGO_NO_25","{0:00}")%></td></tr>
														</table>
													</td>
												</tr>
												<tr>
													<td align="center">
														<asp:LinkButton ID="lnkUpdate" runat="server" Text='<%# GetHighPrbFlagStr(Eval("HIGH_PRB_FLAG")) %>' CommandArgument='<%# Container.ItemIndex %>' CommandName="UPDATE" OnCommand="lnkUpdate_Command" ForeColor='<%# GetHighPrbFlagColor(Eval("HIGH_PRB_FLAG")) %>'></asp:LinkButton>
														<asp:HiddenField ID="hdnCardNo" runat="server" Value='<%# Eval("CARD_NO") %>' />
														<asp:HiddenField ID="hdnHighPrbFlag" runat="server" Value='<%# Eval("HIGH_PRB_FLAG")%>' />
													</td>
												</tr>
											</table>
	                                    <%# ((Container.ItemIndex + 1) % HORIZONTAL_LIMIT_COUNT) == 0 ? "</td></tr><tr>" : "</td>" %>
									</ItemTemplate>
									<FooterTemplate>
										</tr></table>
									</FooterTemplate>
								</asp:Repeater>
							</asp:Panel>
						</td>
					</tr>
				</table>
			</fieldset>
			<br />
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsBbsBingoCard" runat="server" TypeName="BbsBingoCard" SelectMethod="GetList" OnSelecting="dsBbsBingoCard_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
