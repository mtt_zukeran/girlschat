﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 投票設定一覧(エントリー制)
--	Progaram ID		: ElectionPeriodList
--  Creation Date	: 2013.12.10
--  Creater			: Y.Ikemiya
**************************************************************************/
using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using ViComm.Extension.Pwild;

public partial class Extension_Pwild_ElectionPeriodList:System.Web.UI.Page {
	private string recCount = string.Empty;
	protected string[] YearArray;
	protected string[] MonthArray;
	protected string[] DayArray;
	protected string[] HourArray;
	protected string[] MinuteArray;

	private string ElectionPeriodSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["ELECTION_PERIOD_SEQ"]);
		}
		set {
			this.ViewState["ELECTION_PERIOD_SEQ"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["REVISION_NO"]);
		}
		set {
			this.ViewState["REVISION_NO"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);

			if (string.IsNullOrEmpty(sSiteCd)) {
				if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
					lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
				} else {
					lstSiteCd.SelectedIndex = 0;
				}
			} else {
				lstSiteCd.SelectedValue = sSiteCd;
			}

			this.pnlSearch.Enabled = true;
			this.pnlInput.Visible = false;

			lstSiteCd.DataBind();
			pnlGrid.DataBind();
			pnlCount.DataBind();

			SetDateArray();
			pnlInput.DataBind();
		}
	}

	protected void dsElectionPeriod_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		ElectionPeriod.SearchCondition oSearchCondition = new ElectionPeriod.SearchCondition();
		oSearchCondition.SiteCd = lstSiteCd.SelectedValue;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsElectionPeriod_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected string GetFixRankFlagStr(object pFixRankFlag) {
		return (pFixRankFlag.ToString().Equals(ViCommConst.FLAG_ON_STR)) ? "固定" : "ﾘｱﾙﾀｲﾑ";
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			grdElectionPeriod.PageIndex = 0;
			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		this.ElectionPeriodSeq = string.Empty;
		this.RevisionNo = string.Empty;
		this.ClearFileds();

		this.pnlSearch.Enabled = false;
		this.pnlInput.Visible = true;
		this.btnElectionFixRankUpdate.Enabled = false;
		this.btnDelete.Enabled = false;
	}

	protected void btnElectionRankUpdate_Click(object sender,EventArgs e) {
		ElectionFixRankUpdate();
		this.ElectionPeriodGet();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (CheckInput()) {
			string sResult;
			ElectionPeriodMainte(ViCommConst.FLAG_OFF,out sResult);

			if (sResult.Equals(PwViCommConst.ElectionPeriodMainteResult.RESULT_OK)) {
				this.pnlSearch.Enabled = true;
				this.pnlInput.Visible = false;
				grdElectionPeriod.PageIndex = 0;
				pnlGrid.DataBind();
				pnlCount.DataBind();
			} else if (sResult.Equals(PwViCommConst.ElectionPeriodMainteResult.RESULT_NG_DATE)) {
				this.lblErrorMessage.Text = "投票期間が他と重複しています";
			}
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlSearch.Enabled = true;
		this.pnlInput.Visible = false;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		string sResult;
		ElectionPeriodMainte(ViCommConst.FLAG_ON,out sResult);

		this.pnlSearch.Enabled = true;
		this.pnlInput.Visible = false;
		grdElectionPeriod.PageIndex = 0;
		pnlGrid.DataBind();
		pnlCount.DataBind();
	}

	protected void lnkEdit_Command(object sender,CommandEventArgs e) {
		int iIndex = int.Parse(iBridUtil.GetStringValue(e.CommandArgument));
		GridViewRow oRow = this.grdElectionPeriod.Rows[iIndex];
		HiddenField oHiddenField = oRow.FindControl("hdnElectionPeriodSeq") as HiddenField;
		this.ElectionPeriodSeq = oHiddenField.Value;

		this.ClearFileds();
		this.ElectionPeriodGet();

		this.pnlSearch.Enabled = false;
		this.pnlInput.Visible = true;
		this.btnElectionFixRankUpdate.Enabled = true;
		this.btnDelete.Enabled = true;
	}

	private void SetDateArray() {
		List<string> oYearList = new List<string>();
		for (int i = 2013;i <= DateTime.Today.AddYears(2).Year;i++) {
			oYearList.Add(i.ToString());
		}
		this.YearArray = oYearList.ToArray();
		this.MonthArray = new string[] { "01","02","03","04","05","06","07","08","09","10","11","12" };
		this.DayArray = new string[] { "01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31" };
		this.HourArray = new string[] { "00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23" };

		List<string> oMinuteList = new List<string>();

		for (int i = 0;i < 60;i++) {
			oMinuteList.Add(string.Format("{0:D2}",i));
		}

		this.MinuteArray = oMinuteList.ToArray();
	}

	private bool CheckInput() {
		DateTime dtEntryStartDate = new DateTime();
		DateTime dtEntryEndDate = new DateTime();
		DateTime dtFirstPeriodStartDate = new DateTime();
		DateTime dtFirstPeriodEndDate = new DateTime();
		DateTime dtSecondPeriodStartDate = new DateTime();
		DateTime dtSecondPeriodEndDate = new DateTime();
		int iCanEntrySecondPeriodCount;
		int iFirstPeriodFreeVoteCount;
		int iSecondPeriodFreeVoteCount;
		int iTicketPoint;
		int iAddVotePerFanclubMember;
		int iAddOneVoteNeedStars;

		if (string.IsNullOrEmpty(this.txtElectionPeriodNm.Text)) {
			this.lblErrorMessage.Text = "名称を入力して下さい";
			return false;
		}

		if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstEntryStartDateYYYY.SelectedValue,this.lstEntryStartDateMM.SelectedValue,this.lstEntryStartDateDD.SelectedValue,this.lstEntryStartDateHH.SelectedValue,this.lstEntryStartDateMI.SelectedValue),out dtEntryStartDate)) {
			this.lblErrorMessage.Text = "エントリー開始日時が正しくありません";
			return false;
		}

		if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:59",this.lstEntryEndDateYYYY.SelectedValue,this.lstEntryEndDateMM.SelectedValue,this.lstEntryEndDateDD.SelectedValue,this.lstEntryEndDateHH.SelectedValue,this.lstEntryEndDateMI.SelectedValue),out dtEntryEndDate)) {
			this.lblErrorMessage.Text = "エントリー終了日時が正しくありません";
			return false;
		}

		if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstFirstPeriodStartDateYYYY.SelectedValue,this.lstFirstPeriodStartDateMM.SelectedValue,this.lstFirstPeriodStartDateDD.SelectedValue,this.lstFirstPeriodStartDateHH.SelectedValue,this.lstFirstPeriodStartDateMI.SelectedValue),out dtFirstPeriodStartDate)) {
			this.lblErrorMessage.Text = "1次投票開始日時が正しくありません";
			return false;
		}

		if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:59",this.lstFirstPeriodEndDateYYYY.SelectedValue,this.lstFirstPeriodEndDateMM.SelectedValue,this.lstFirstPeriodEndDateDD.SelectedValue,this.lstFirstPeriodEndDateHH.SelectedValue,this.lstFirstPeriodEndDateMI.SelectedValue),out dtFirstPeriodEndDate)) {
			this.lblErrorMessage.Text = "1次投票終了日時が正しくありません";
			return false;
		}

		if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstSecondPeriodStartDateYYYY.SelectedValue,this.lstSecondPeriodStartDateMM.SelectedValue,this.lstSecondPeriodStartDateDD.SelectedValue,this.lstSecondPeriodStartDateHH.SelectedValue,this.lstSecondPeriodStartDateMI.SelectedValue),out dtSecondPeriodStartDate)) {
			this.lblErrorMessage.Text = "2次投票開始日時が正しくありません";
			return false;
		}

		if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:59",this.lstSecondPeriodEndDateYYYY.SelectedValue,this.lstSecondPeriodEndDateMM.SelectedValue,this.lstSecondPeriodEndDateDD.SelectedValue,this.lstSecondPeriodEndDateHH.SelectedValue,this.lstSecondPeriodEndDateMI.SelectedValue),out dtSecondPeriodEndDate)) {
			this.lblErrorMessage.Text = "2次投票終了日時が正しくありません";
			return false;
		}

		if (dtEntryStartDate > dtEntryEndDate) {
			this.lblErrorMessage.Text = "エントリー期間の大小関係が正しくありません";
			return false;
		}

		if (dtFirstPeriodStartDate > dtFirstPeriodEndDate) {
			this.lblErrorMessage.Text = "1次投票期間の大小関係が正しくありません";
			return false;
		}

		if (dtSecondPeriodStartDate > dtSecondPeriodEndDate) {
			this.lblErrorMessage.Text = "2次投票期間の大小関係が正しくありません";
			return false;
		}

		if (dtSecondPeriodStartDate < dtFirstPeriodEndDate) {
			this.lblErrorMessage.Text = "1次投票期間と2次投票期間が重複しています";
			return false;
		}

		if (dtEntryEndDate > dtFirstPeriodEndDate) {
			this.lblErrorMessage.Text = "エントリー終了が1次投票終了以降に設定されています";
			return false;
		}

		if (dtEntryStartDate > dtFirstPeriodStartDate) {
			this.lblErrorMessage.Text = "1次投票開始がエントリー開始よりも前に設定されています";
			return false;
		}

		if (string.IsNullOrEmpty(this.txtCanEntrySecondPeriodCount.Text)) {
			this.lblErrorMessage.Text = "2次投票進出可能数を入力して下さい";
			return false;
		} else if (!int.TryParse(this.txtCanEntrySecondPeriodCount.Text,out iCanEntrySecondPeriodCount)) {
			this.lblErrorMessage.Text = "2次投票進出可能数が正しくありません";
			return false;
		}

		if (string.IsNullOrEmpty(this.txtFirstPeriodFreeVoteCount.Text)) {
			this.lblErrorMessage.Text = "1次無料投票回数を入力して下さい";
			return false;
		} else if (!int.TryParse(this.txtFirstPeriodFreeVoteCount.Text,out iFirstPeriodFreeVoteCount)) {
			this.lblErrorMessage.Text = "1次無料投票回数が正しくありません";
			return false;
		}

		if (string.IsNullOrEmpty(this.txtSecondPeriodFreeVoteCount.Text)) {
			this.lblErrorMessage.Text = "2次無料投票回数を入力して下さい";
			return false;
		} else if (!int.TryParse(this.txtSecondPeriodFreeVoteCount.Text,out iSecondPeriodFreeVoteCount)) {
			this.lblErrorMessage.Text = "2次無料投票回数が正しくありません";
			return false;
		}

		if (string.IsNullOrEmpty(this.txtTicketPoint.Text)) {
			this.lblErrorMessage.Text = "投票券追加消費ポイントを入力して下さい";
			return false;
		} else if (!int.TryParse(this.txtTicketPoint.Text,out iTicketPoint)) {
			this.lblErrorMessage.Text = "投票券追加消費ポイントが正しくありません";
			return false;
		}

		if (string.IsNullOrEmpty(this.txtAddVotePerFanclubMember.Text)) {
			this.lblErrorMessage.Text = "ファンクラブ決済得票数を入力して下さい";
			return false;
		} else if (!int.TryParse(this.txtAddVotePerFanclubMember.Text,out iAddVotePerFanclubMember)) {
			this.lblErrorMessage.Text = "ファンクラブ決済得票数が正しくありません";
			return false;
		}

		if (string.IsNullOrEmpty(this.txtAddOneVoteNeedStars.Text)) {
			this.lblErrorMessage.Text = "獲得スター得票数を入力して下さい";
			return false;
		} else if (!int.TryParse(this.txtAddOneVoteNeedStars.Text,out iAddOneVoteNeedStars)) {
			this.lblErrorMessage.Text = "獲得スター得票数得票数が正しくありません";
			return false;
		}

		return true;
	}

	private void ClearFileds() {
		this.lblErrorMessage.Text = string.Empty;
		this.txtElectionPeriodNm.Text = string.Empty;

		this.lstEntryStartDateYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstEntryStartDateMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstEntryStartDateDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstEntryStartDateHH.SelectedValue = "00";
		this.lstEntryStartDateMI.SelectedValue = "00";

		this.lstEntryEndDateYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstEntryEndDateMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstEntryEndDateDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstEntryEndDateHH.SelectedValue = "23";
		this.lstEntryEndDateMI.SelectedValue = "59";

		this.lstFirstPeriodStartDateYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstFirstPeriodStartDateMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstFirstPeriodStartDateDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstFirstPeriodStartDateHH.SelectedValue = "00";
		this.lstFirstPeriodStartDateMI.SelectedValue = "00";

		this.lstFirstPeriodEndDateYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstFirstPeriodEndDateMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstFirstPeriodEndDateDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstFirstPeriodEndDateHH.SelectedValue = "23";
		this.lstFirstPeriodEndDateMI.SelectedValue = "59";

		this.lstSecondPeriodStartDateYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstSecondPeriodStartDateMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstSecondPeriodStartDateDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstSecondPeriodStartDateHH.SelectedValue = "00";
		this.lstSecondPeriodStartDateMI.SelectedValue = "00";

		this.lstSecondPeriodEndDateYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstSecondPeriodEndDateMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstSecondPeriodEndDateDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstSecondPeriodEndDateHH.SelectedValue = "23";
		this.lstSecondPeriodEndDateMI.SelectedValue = "59";

		this.txtCanEntrySecondPeriodCount.Text = "50";
		this.txtFirstPeriodFreeVoteCount.Text = "1";
		this.txtSecondPeriodFreeVoteCount.Text = "1";
		this.txtTicketPoint.Text = "5";
		this.txtAddVotePerFanclubMember.Text = "10";
		this.txtAddOneVoteNeedStars.Text = "10";
		this.rdoFixRankFlagOff.Checked = false;
		this.rdoFixRankFlagOn.Checked = true;
		this.lblFixRankDate.Text = string.Empty;
	}

	private void ElectionPeriodGet() {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("ELECTION_PERIOD_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pELECTION_PERIOD_SEQ",DbSession.DbType.VARCHAR2,this.ElectionPeriodSeq);
			oDbSession.ProcedureOutParm("pELECTION_PERIOD_NM",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pENTRY_START_DATE",DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pENTRY_END_DATE",DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pFIRST_PERIOD_START_DATE",DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pFIRST_PERIOD_END_DATE",DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pSECOND_PERIOD_START_DATE",DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pSECOND_PERIOD_END_DATE",DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pCAN_ENTRY_SECOND_PERIOD_COUNT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pFIRST_PERIOD_FREE_VOTE_COUNT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSECOND_PERIOD_FREE_VOTE_COUNT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pTICKET_POINT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pADD_VOTE_PER_FANCLUB_MEMBER",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pADD_ONE_VOTE_NEED_STARS",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pFIX_RANK_FLAG",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pFIX_RANK_DATE",DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();

			this.txtElectionPeriodNm.Text = oDbSession.GetStringValue("pELECTION_PERIOD_NM");
			DateTime dtEntryStartDate = (DateTime)oDbSession.GetDateTimeValue("pENTRY_START_DATE");
			DateTime dtEntryEndDate = (DateTime)oDbSession.GetDateTimeValue("pENTRY_END_DATE");
			DateTime dtFirstPeriodStartDate = (DateTime)oDbSession.GetDateTimeValue("pFIRST_PERIOD_START_DATE");
			DateTime dtFirstPeriodEndDate = (DateTime)oDbSession.GetDateTimeValue("pFIRST_PERIOD_END_DATE");
			DateTime dtSecondPeriodStartDate = (DateTime)oDbSession.GetDateTimeValue("pSECOND_PERIOD_START_DATE");
			DateTime dtSecondPeriodEndDate = (DateTime)oDbSession.GetDateTimeValue("pSECOND_PERIOD_END_DATE");

			if (dtEntryStartDate != null) {
				this.lstEntryStartDateYYYY.SelectedValue = dtEntryStartDate.ToString("yyyy");
				this.lstEntryStartDateMM.SelectedValue = dtEntryStartDate.ToString("MM");
				this.lstEntryStartDateDD.SelectedValue = dtEntryStartDate.ToString("dd");
				this.lstEntryStartDateHH.SelectedValue = dtEntryStartDate.ToString("HH");
				this.lstEntryStartDateMI.SelectedValue = dtEntryStartDate.ToString("mm");
			}

			if (dtEntryEndDate != null) {
				this.lstEntryEndDateYYYY.SelectedValue = dtEntryEndDate.ToString("yyyy");
				this.lstEntryEndDateMM.SelectedValue = dtEntryEndDate.ToString("MM");
				this.lstEntryEndDateDD.SelectedValue = dtEntryEndDate.ToString("dd");
				this.lstEntryEndDateHH.SelectedValue = dtEntryEndDate.ToString("HH");
				this.lstEntryEndDateMI.SelectedValue = dtEntryEndDate.ToString("mm");
			}

			if (dtFirstPeriodStartDate != null) {
				this.lstFirstPeriodStartDateYYYY.SelectedValue = dtFirstPeriodStartDate.ToString("yyyy");
				this.lstFirstPeriodStartDateMM.SelectedValue = dtFirstPeriodStartDate.ToString("MM");
				this.lstFirstPeriodStartDateDD.SelectedValue = dtFirstPeriodStartDate.ToString("dd");
				this.lstFirstPeriodStartDateHH.SelectedValue = dtFirstPeriodStartDate.ToString("HH");
				this.lstFirstPeriodStartDateMI.SelectedValue = dtFirstPeriodStartDate.ToString("mm");
			}

			if (dtFirstPeriodEndDate != null) {
				this.lstFirstPeriodEndDateYYYY.SelectedValue = dtFirstPeriodEndDate.ToString("yyyy");
				this.lstFirstPeriodEndDateMM.SelectedValue = dtFirstPeriodEndDate.ToString("MM");
				this.lstFirstPeriodEndDateDD.SelectedValue = dtFirstPeriodEndDate.ToString("dd");
				this.lstFirstPeriodEndDateHH.SelectedValue = dtFirstPeriodEndDate.ToString("HH");
				this.lstFirstPeriodEndDateMI.SelectedValue = dtFirstPeriodEndDate.ToString("mm");
			}

			if (dtSecondPeriodStartDate != null) {
				this.lstSecondPeriodStartDateYYYY.SelectedValue = dtSecondPeriodStartDate.ToString("yyyy");
				this.lstSecondPeriodStartDateMM.SelectedValue = dtSecondPeriodStartDate.ToString("MM");
				this.lstSecondPeriodStartDateDD.SelectedValue = dtSecondPeriodStartDate.ToString("dd");
				this.lstSecondPeriodStartDateHH.SelectedValue = dtSecondPeriodStartDate.ToString("HH");
				this.lstSecondPeriodStartDateMI.SelectedValue = dtSecondPeriodStartDate.ToString("mm");
			}

			if (dtSecondPeriodEndDate != null) {
				this.lstSecondPeriodEndDateYYYY.SelectedValue = dtSecondPeriodEndDate.ToString("yyyy");
				this.lstSecondPeriodEndDateMM.SelectedValue = dtSecondPeriodEndDate.ToString("MM");
				this.lstSecondPeriodEndDateDD.SelectedValue = dtSecondPeriodEndDate.ToString("dd");
				this.lstSecondPeriodEndDateHH.SelectedValue = dtSecondPeriodEndDate.ToString("HH");
				this.lstSecondPeriodEndDateMI.SelectedValue = dtSecondPeriodEndDate.ToString("mm");
			}

			this.txtFirstPeriodFreeVoteCount.Text = oDbSession.GetIntValue("pFIRST_PERIOD_FREE_VOTE_COUNT").ToString();
			this.txtSecondPeriodFreeVoteCount.Text = oDbSession.GetIntValue("pSECOND_PERIOD_FREE_VOTE_COUNT").ToString();
			this.txtTicketPoint.Text = oDbSession.GetIntValue("pTICKET_POINT").ToString();
			this.txtCanEntrySecondPeriodCount.Text = oDbSession.GetIntValue("pCAN_ENTRY_SECOND_PERIOD_COUNT").ToString();
			this.txtAddVotePerFanclubMember.Text = oDbSession.GetIntValue("pADD_VOTE_PER_FANCLUB_MEMBER").ToString();
			this.txtAddOneVoteNeedStars.Text = oDbSession.GetIntValue("pADD_ONE_VOTE_NEED_STARS").ToString();
			int iFixRankFlag = oDbSession.GetIntValue("pFIX_RANK_FLAG");

			if (iFixRankFlag == 1) {
				this.rdoFixRankFlagOff.Checked = false;
				this.rdoFixRankFlagOn.Checked = true;
			} else {
				this.rdoFixRankFlagOff.Checked = true;
				this.rdoFixRankFlagOn.Checked = false;
			}

			this.lblFixRankDate.Text = oDbSession.GetDateTimeValue("pFIX_RANK_DATE").ToString();
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
		}
	}

	private void ElectionPeriodMainte(int pDeleteFlag,out string pResult) {
		DateTime dtEntryStartDate = DateTime.Parse(string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstEntryStartDateYYYY.SelectedValue,this.lstEntryStartDateMM.SelectedValue,this.lstEntryStartDateDD.SelectedValue,this.lstEntryStartDateHH.SelectedValue,this.lstEntryStartDateMI.SelectedValue));
		DateTime dtEntryEndDate = DateTime.Parse(string.Format("{0}/{1}/{2} {3}:{4}:59",this.lstEntryEndDateYYYY.SelectedValue,this.lstEntryEndDateMM.SelectedValue,this.lstEntryEndDateDD.SelectedValue,this.lstEntryEndDateHH.SelectedValue,this.lstEntryEndDateMI.SelectedValue));
		DateTime dtFirstPeriodStartDate = DateTime.Parse(string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstFirstPeriodStartDateYYYY.SelectedValue,this.lstFirstPeriodStartDateMM.SelectedValue,this.lstFirstPeriodStartDateDD.SelectedValue,this.lstFirstPeriodStartDateHH.SelectedValue,this.lstFirstPeriodStartDateMI.SelectedValue));
		DateTime dtFirstPeriodEndDate = DateTime.Parse(string.Format("{0}/{1}/{2} {3}:{4}:59",this.lstFirstPeriodEndDateYYYY.SelectedValue,this.lstFirstPeriodEndDateMM.SelectedValue,this.lstFirstPeriodEndDateDD.SelectedValue,this.lstFirstPeriodEndDateHH.SelectedValue,this.lstFirstPeriodEndDateMI.SelectedValue));
		DateTime dtSecondPeriodStartDate = DateTime.Parse(string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstSecondPeriodStartDateYYYY.SelectedValue,this.lstSecondPeriodStartDateMM.SelectedValue,this.lstSecondPeriodStartDateDD.SelectedValue,this.lstSecondPeriodStartDateHH.SelectedValue,this.lstSecondPeriodStartDateMI.SelectedValue));
		DateTime dtSecondPeriodEndDate = DateTime.Parse(string.Format("{0}/{1}/{2} {3}:{4}:59",this.lstSecondPeriodEndDateYYYY.SelectedValue,this.lstSecondPeriodEndDateMM.SelectedValue,this.lstSecondPeriodEndDateDD.SelectedValue,this.lstSecondPeriodEndDateHH.SelectedValue,this.lstSecondPeriodEndDateMI.SelectedValue));
		int iFixRankFlag = (this.rdoFixRankFlagOn.Checked) ? 1 : 0;

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("ELECTION_PERIOD_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pELECTION_PERIOD_SEQ",DbSession.DbType.NUMBER,this.ElectionPeriodSeq);
			oDbSession.ProcedureInParm("pELECTION_PERIOD_NM",DbSession.DbType.VARCHAR2,this.txtElectionPeriodNm.Text);
			oDbSession.ProcedureInParm("pENTRY_START_DATE",DbSession.DbType.DATE,dtEntryStartDate);
			oDbSession.ProcedureInParm("pENTRY_END_DATE",DbSession.DbType.DATE,dtEntryEndDate);
			oDbSession.ProcedureInParm("pFIRST_PERIOD_START_DATE",DbSession.DbType.DATE,dtFirstPeriodStartDate);
			oDbSession.ProcedureInParm("pFIRST_PERIOD_END_DATE",DbSession.DbType.DATE,dtFirstPeriodEndDate);
			oDbSession.ProcedureInParm("pSECOND_PERIOD_START_DATE",DbSession.DbType.DATE,dtSecondPeriodStartDate);
			oDbSession.ProcedureInParm("pSECOND_PERIOD_END_DATE",DbSession.DbType.DATE,dtSecondPeriodEndDate);
			oDbSession.ProcedureInParm("pCAN_ENTRY_SECOND_PERIOD_COUNT",DbSession.DbType.NUMBER,int.Parse(this.txtCanEntrySecondPeriodCount.Text));
			oDbSession.ProcedureInParm("pFIRST_PERIOD_FREE_VOTE_COUNT",DbSession.DbType.NUMBER,int.Parse(this.txtFirstPeriodFreeVoteCount.Text));
			oDbSession.ProcedureInParm("pSECOND_PERIOD_FREE_VOTE_COUNT",DbSession.DbType.NUMBER,int.Parse(this.txtSecondPeriodFreeVoteCount.Text));
			oDbSession.ProcedureInParm("pTICKET_POINT",DbSession.DbType.NUMBER,int.Parse(this.txtTicketPoint.Text));
			oDbSession.ProcedureInParm("pADD_VOTE_PER_FANCLUB_MEMBER",DbSession.DbType.NUMBER,int.Parse(this.txtAddVotePerFanclubMember.Text));
			oDbSession.ProcedureInParm("pADD_ONE_VOTE_NEED_STARS",DbSession.DbType.NUMBER,int.Parse(this.txtAddOneVoteNeedStars.Text));
			oDbSession.ProcedureInParm("pFIX_RANK_FLAG",DbSession.DbType.NUMBER,iFixRankFlag);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDeleteFlag);
			oDbSession.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
			pResult = oDbSession.GetStringValue("pRESULT");
		}
	}

	private void ElectionFixRankUpdate() {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("ELECTION_FIX_RANK_UPDATE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pELECTION_PERIOD_SEQ",DbSession.DbType.NUMBER,this.ElectionPeriodSeq);
			oDbSession.ProcedureInParm("pWITHOUT_COMMIT",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
		}
	}
}
