/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: この娘を探せ・男性エントリー(詳細)

--	Progaram ID		: InvestigateEntrantList
--
--  Creation Date	: 2015.02.26
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using iBridCommLib;
using System.Drawing;
using ViComm;

public partial class Extension_Pwild_InvestigateEntrantList:System.Web.UI.Page {
	string recCount = string.Empty;

	protected string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		private set {
			this.ViewState["SiteCd"] = value;
		}
	}

	protected string ExecutionDay {
		get {
			return iBridUtil.GetStringValue(this.ViewState["ExecutionDay"]);
		}
		set {
			this.ViewState["ExecutionDay"] = value;
		}
	}

	protected string CaughtFlag {
		get {
			return iBridUtil.GetStringValue(this.ViewState["CaughtFlag"]);
		}
		set {
			this.ViewState["CaughtFlag"] = value;
		}
	}

	protected string PointAcquiredFlag {
		get {
			return iBridUtil.GetStringValue(this.ViewState["PointAcquiredFlag"]);
		}
		set {
			this.ViewState["PointAcquiredFlag"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		if (!this.IsPostBack) {
			this.SiteCd = this.Request.QueryString["sitecd"];
			this.ExecutionDay = this.Request.QueryString["executionday"];
			this.CaughtFlag = this.Request.QueryString["caughtflag"];
			this.PointAcquiredFlag = this.Request.QueryString["pointacquiredflag"];

			this.InitPage();

			if (!string.IsNullOrEmpty(this.SiteCd) && !string.IsNullOrEmpty(this.Request.QueryString["executionday"])) {
				this.lstCaught.SelectedValue = this.CaughtFlag;
				this.lstPointAcquired.SelectedValue = this.PointAcquiredFlag;
				this.lstSeekSiteCd.SelectedValue = this.SiteCd;
				this.GetList();
				this.pnlInfo.Visible = true;
			}
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		this.CaughtFlag = this.lstCaught.SelectedValue;
		this.PointAcquiredFlag = this.lstPointAcquired.SelectedValue;
		this.GetList();
		this.pnlInfo.Visible = true;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlInfo.Visible = false;
	}

	protected void btnBack_Click(object sender,EventArgs e) {
		this.Response.Redirect(string.Format("~/Extension/Pwild/InvestigateEntSummaryList.aspx?sitecd={0}&targetmonth={1}",this.SiteCd,this.Request.QueryString["executionday"].Substring(0,7)));
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void dsInvestigateEntrant_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		InvestigateEntrant.SearchCondition oSearchCondition = new InvestigateEntrant.SearchCondition();
		oSearchCondition.SiteCd = this.lstSeekSiteCd.SelectedValue;
		oSearchCondition.ExecutionDay = this.ExecutionDay;
		oSearchCondition.CaughtFlag = this.CaughtFlag;
		oSearchCondition.PointAcquiredFlag = this.PointAcquiredFlag;

		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsInvestigateEntrant_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		this.grdInvestigateEntrant.DataSourceID = string.Empty;
		this.pnlKey.DataBind();
		this.chkPagingOff.Checked = false;
		this.lstPointAcquired.SelectedIndex = 0;
		this.lstCaught.SelectedIndex = 0;
		this.pnlInfo.Visible = false;
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}
	private void GetList() {
		this.grdInvestigateEntrant.PageIndex = 0;
		if (chkPagingOff.Checked) {
			this.grdInvestigateEntrant.PageSize = 99999;
		} else {
			this.grdInvestigateEntrant.PageSize = 20;
		}

		this.grdInvestigateEntrant.DataSourceID = "dsInvestigateEntrant";
		this.grdInvestigateEntrant.DataBind();
		this.pnlCount.DataBind();
	}

	protected string GetRecCount() {
		return recCount;
	}

}