﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MailRequestLogDailyList.aspx.cs" Inherits="Extension_Pwild_MailRequestLogDailyList" Title="メールおねだり集計(日別)"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="メールおねだり集計(日別)"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
                            <td class="tdHeaderStyle">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                表示月
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstYear" runat="server" Width="60px">
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstMonth" runat="server" Width="40px">
                                </asp:DropDownList>月
                                <br />
								<asp:Label ID="lblErrorMessageReportDayFrom" runat="server" ForeColor="red" Visible="false"></asp:Label>
                            </td>
                        </tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
				<asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" CausesValidation="True" OnClick="btnCSV_Click" />
			</asp:Panel>
		</fieldset>
		<fieldset>
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="750px">
				<asp:GridView ID="grdMailRequestLog" runat="server" AutoGenerateColumns="False" DataSourceID="dsMailRequestLog" CellPadding="0" AllowSorting="True" ShowFooter="true"
					OnRowDataBound="grdMailRequestLog_RowDataBound" SkinID="GridViewColorNoRowStyle">
					<Columns>
						<asp:TemplateField HeaderText="日付">
							<ItemStyle HorizontalAlign="Center" Width="70px" />
							<ItemTemplate>
								<asp:Label ID="Label1" runat="server" Text='<%# GetDisplayReportDay(Eval("DAYS").ToString()) %>' BackColor='' Width="40px"></asp:Label>
							</ItemTemplate>
							<FooterStyle HorizontalAlign="center" Wrap="false" />
							<FooterTemplate>
								<asp:Label ID="lblReportDayFooter" Text="合計" runat="server"></asp:Label>
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="曜日">
							<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
							<ItemTemplate>
								<asp:Label ID="Label2" runat="server" Text='<%# GetDisplayDayOfWeek(Eval("DAYS").ToString()) %>' BackColor='' Width="60px"></asp:Label>
							</ItemTemplate>
							<FooterTemplate>
								<asp:Label ID="lblDayOfWeekFooter" Text="" runat="server"></asp:Label>
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="送信件数">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("TOTAL_COUNT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblTotalCountFooter" Text="" runat="server"></asp:Label>
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="有効送信件数">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("VALID_COUNT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblValidCountFooter" Text="" runat="server"></asp:Label>
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="メール<br>未送信数">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("NO_MAIL_COUNT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblNoMailCountFooter" Text="" runat="server"></asp:Label>
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="メール<br>送信数">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("RX_MAIL_COUNT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblRxMailCountFooter" Text="" runat="server"></asp:Label>
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="おねだり<br>送信者数">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("UNIQUE_USER_COUNT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblUniqueUserCountFooter" Text="" runat="server"></asp:Label>
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="出演者<br>ﾒｰﾙ送信率">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# GetRxMailRate(Eval("VALID_COUNT").ToString(),Eval("RX_MAIL_COUNT").ToString()) %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblCastTxMailRateFooter" Text="" runat="server"></asp:Label>
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="会員<br>ﾒｰﾙ送信率">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# GetManTxMailRate(Eval("RX_MAIL_COUNT").ToString(),Eval("MAN_TX_MAIL_COUNT").ToString()) %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblManTxMailRateFooter" Text="" runat="server"></asp:Label>
							</FooterTemplate>
						</asp:TemplateField>
					</Columns>
					<FooterStyle ForeColor="Black" BackColor="LightYellow" />
				</asp:GridView>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsMailRequestLog" runat="server" SelectMethod="GetDailyRegistCount" TypeName="MailRequestLog">
		<SelectParameters>
			<asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstYear" Name="pYear" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstMonth" Name="pMonth" PropertyName="SelectedValue" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
</asp:Content>
