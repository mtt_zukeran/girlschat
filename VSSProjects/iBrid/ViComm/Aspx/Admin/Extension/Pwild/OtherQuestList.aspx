﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="OtherQuestList.aspx.cs" Inherits="Extension_Pwild_OtherQuestList" Title="裏クエスト一覧" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="ViComm.Extension.Pwild" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="裏クエスト一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" Runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 600px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle2">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton" ValidationGroup="Create"
                    OnClick="btnCreate_Click" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" Visible="false" />
                <asp:Button ID="btnBack" runat="server" CausesValidation="False" CssClass="seekbutton"
                    OnClick="btnBack_Click" Text="戻る" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlOtherQuestInfo">
            <fieldset class="fieldset">
                <legend>[裏クエスト情報]</legend>
                <table border="0" style="width: 600px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            裏クエストID(SEQ)
                        </td>
                        <td class="tdDataStyle">
                            <asp:Label ID="lblOtherQuestId" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            裏クエスト名
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtOtherQuestNm" runat="server" MaxLength="300" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            裏クエスト説明
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtRemarks" runat="server" MaxLength="300" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            クエスト制限時間
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtTimeLimitD" runat="server" MaxLength="5" Width="30px"></asp:TextBox>日
                            <asp:TextBox ID="txtTimeLimitH" runat="server" MaxLength="5" Width="30px"></asp:TextBox>時
                            <asp:TextBox ID="txtTimeLimitM" runat="server" MaxLength="5" Width="30px"></asp:TextBox>分
                            <asp:Label ID="lblErrorMessageTimeLimit" runat="server" Text="" Visible="false" ForeColor="red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            達成回数制限
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtClearLimit" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="vdrClearLimit" runat="server" ErrorMessage="達成回数制限を入力して下さい。"
                                    ControlToValidate="txtClearLimit" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                            <ajaxToolkit:ValidatorCalloutExtender ID="vdeClearLimit" runat="Server" TargetControlID="vdrClearLimit" HighlightCssClass="validatorCallout" />
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            日別達成回数制限
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtDailyClearLimit" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="vdrDailyClearLimit" runat="server" ErrorMessage="日別達成回数制限を入力して下さい。"
                                    ControlToValidate="txtDailyClearLimit" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                            <ajaxToolkit:ValidatorCalloutExtender ID="vdeDailyClearLimit" runat="Server" TargetControlID="vdrDailyClearLimit" HighlightCssClass="validatorCallout" />
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="Panel1" runat="server">
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Update"
                        OnClick="btnUpdate_Click" />
                    <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" ValidationGroup="Delete"
                        OnClick="btnDelete_Click" />
                    <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" CausesValidation="False"
                        Visible="true" OnClick="btnCancel_Click" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[裏クエスト一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdOtherQuest.PageIndex + 1 %>
                        of
                        <%= grdOtherQuest.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="600px">
                    <asp:GridView ID="grdOtherQuest" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        PageSize="30" EnableSortingAndPagingCallbacks="false" DataSourceID="dsQuest"
                        SkinID="GridViewColor" Font-Size="small" AllowSorting="true" DataKeyNames="SITE_CD,QUEST_SEQ">
                        <Columns>
                            <asp:TemplateField HeaderText="裏ｸｴｽﾄID">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkOtherQuestId" runat="server" CommandArgument='<%# Eval("OTHER_QUEST_SEQ") %>'
                                        Text='<%# Eval("OTHER_QUEST_SEQ") %>' OnCommand="lnkOtherQuestId_Command"></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="裏ｸｴｽﾄ名">
                                <ItemTemplate>
                                    <asp:Label ID="lblOtherQuestNm" runat="server" Text='<%# Eval("OTHER_QUEST_NM") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="裏ｸｴｽﾄ説明">
                                <ItemTemplate>
                                    <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("REMARKS") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="裏ｸｴｽﾄ制限時間">
                                <ItemTemplate>
                                    <asp:Label ID="lblTimeLimitD" runat="server" Text='<%# Eval("TIME_LIMIT_D") %>'></asp:Label>日
                                    <asp:Label ID="lblTimeLimitH" runat="server" Text='<%# Eval("TIME_LIMIT_H") %>'></asp:Label>時間
                                    <asp:Label ID="lblTimeLimitM" runat="server" Text='<%# Eval("TIME_LIMIT_M") %>'></asp:Label>分
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="達成回数制限">
                                <ItemTemplate>
                                    <asp:Label ID="lblClearLimit" runat="server" Text='<%# Eval("CLEAR_LIMIT") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="日別達成回数制限">
                                <ItemTemplate>
                                    <asp:Label ID="lblDailyClearLimit" runat="server" Text='<%# Eval("DAILY_CLEAR_LIMIT") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkQuestTrialList" runat="server" NavigateUrl='<%# string.Format("~/Extension/Pwild/QuestTrialList.aspx?sitecd={0}&questseq={1}&otherquestseq={2}&questtype={3}&sexcd={4}", Eval("SITE_CD"), Eval("QUEST_SEQ"), Eval("OTHER_QUEST_SEQ"),PwViCommConst.GameQuestType.OTHER_QUEST, Eval("SEX_CD"))%>'
                                        Text="ｸﾘｱ条件一覧"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkQuestRewardList" runat="server" NavigateUrl='<%# string.Format("~/Extension/Pwild/QuestRewardList.aspx?sitecd={0}&questseq={1}&otherquestseq={2}&questtype={3}&sexcd={4}", Eval("SITE_CD"), Eval("QUEST_SEQ"), Eval("OTHER_QUEST_SEQ"), PwViCommConst.GameQuestType.OTHER_QUEST, Eval("SEX_CD"))%>'
                                        Text="報酬一覧"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsOtherQuest" runat="server" SelectMethod="GetPageCollection"
        ConvertNullToDBNull="true" SortParameterName="" TypeName="OtherQuest" SelectCountMethod="GetPageCount"
        EnablePaging="True" OnSelected="dsOtherQuest_Selected">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pQuestSeq" QueryStringField="questseq" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtTimeLimitD" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtTimeLimitH" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtTimeLimitM" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtClearLimit" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtDailyClearLimit" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="裏クエスト情報を更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete"
        ConfirmText="裏クエスト情報を削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>

