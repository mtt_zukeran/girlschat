﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="QuestRewardList.aspx.cs" Inherits="Extension_QuestRewardList"
    Title="クエストクリア報酬一覧" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="ViComm.Extension.Pwild" %>
<%@ Import Namespace="iBridCommLib" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="クエストクリア報酬一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 400px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle2">
                                サイトコード
                            </td>
                            <td class="tdDataStyle" style="width: 200px">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton" ValidationGroup="Create"
                    OnClick="btnCreate_Click" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" Visible="false" />
                <asp:Button ID="btnBack" runat="server" CausesValidation="False" CssClass="seekbutton"
                    OnClick="btnBack_Click" Text="戻る" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlQuestRewardInfo">
            <fieldset class="fieldset">
                <legend>[クエスト報酬情報]</legend>
                <table border="0" style="width: 650px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            アイテム
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstGameItemCategory" runat="server" DataSourceID="dsGameItemCategory"
                                OnDataBound="lst_DataBound" OnSelectedIndexChanged="lstGameItemCategory_IndexChanged"
                                DataTextField="GAME_ITEM_CATEGORY_NM" DataValueField="GAME_ITEM_CATEGORY_TYPE"
                                Width="170px" AutoPostBack="True">
                            </asp:DropDownList>
                            <asp:DropDownList ID="lstItemGetCd" runat="server" DataSourceID="dsItemGet" DataTextField="CODE_NM"
								DataValueField="CODE" OnDataBound="lstItemGetCd_DataBound" OnSelectedIndexChanged="lstItemGetCd_IndexChanged" AutoPostBack="True">
							</asp:DropDownList>
							<asp:DropDownList ID="lstItemPresent" runat="server" OnSelectedIndexChanged="lstItemPresent_IndexChanged" AutoPostBack="True">
								<asp:ListItem Value=""></asp:ListItem>
								<asp:ListItem Value="0">通常</asp:ListItem>
								<asp:ListItem Value="1">ﾌﾟﾚｾﾞﾝﾄ</asp:ListItem>
							</asp:DropDownList>
                            <asp:DropDownList ID="lstGameItem" runat="server" DataSourceID="dsGameItem" DataTextField="GAME_ITEM_NM"
                                OnDataBound="lst_DataBound" DataValueField="GAME_ITEM_SEQ" Width="170px">
                            </asp:DropDownList>
                            <br />
                            <asp:Label ID="lblErrorMessageItem" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            アイテム個数
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtItemCount" runat="server" MaxLength="3" Width="50px"></asp:TextBox>
                            <br />
                            <asp:Label ID="lblErrorMessageCount" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            ガチャチケット
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstLottery" runat="server" DataSourceID="dsLottery"
                                OnDataBound="lst_DataBound"
                                DataTextField="TICKET_NM" DataValueField="LOTTERY_SEQ"
                                Width="170px" AutoPostBack="True">
                            </asp:DropDownList>
                            <br />
                            <asp:Label ID="Label1" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            チケット枚数
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtTicketCount" runat="server" MaxLength="3" Width="50px"></asp:TextBox>
                            <br />
                            <asp:Label ID="lblErrorMessageTicketCount" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            ゲームポイント
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtGamePoint" runat="server" MaxLength="10" Width="50px"></asp:TextBox>
                            <br />
                            <asp:Label ID="lblErrorMessageGamePoint" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="pnlCommand" runat="server">
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Update"
                        OnClick="btnUpdate_Click" />
                    <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" ValidationGroup="Delete"
                        OnClick="btnDelete_Click" />
                    <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" CausesValidation="False"
                        Visible="true" OnClick="btnCancel_Click" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[クエストクリア報酬一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdQuestReward.PageIndex + 1 %>
                        of
                        <%= grdQuestReward.PageCount%>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="600px">
                    <asp:GridView ID="grdQuestReward" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        PageSize="30" EnableSortingAndPagingCallbacks="false" DataSourceID="dsQuestReward"
                        SkinID="GridViewColor" AllowSorting="true">
                        <Columns>
                            <asp:TemplateField HeaderText="アイテムSEQ">
                                <ItemTemplate>
                                    <asp:Label ID="lblGameItemSeq" runat="server" Text='<%# Eval("GAME_ITEM_SEQ") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="アイテム名">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkQuestRewardId" runat="server" CommandArgument='<%# Eval("QUEST_REWARD_SEQ") %>'
                                        OnCommand="lnkQuestReward_Command" Text='<%# CheckNoItem(Eval("GAME_ITEM_NM")) %>'></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="GAME_ITEM_COUNT" HeaderText="アイテム個数" DataFormatString="{0:N0}">
                                <ItemStyle HorizontalAlign="right" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="ガチャチケット">
                                <ItemTemplate>
                                    <asp:Label ID="lblTicketNm" runat="server" Text='<%# Eval("TICKET_NM") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="TICKET_COUNT" HeaderText="チケット枚数" DataFormatString="{0:N0}">
                                <ItemStyle HorizontalAlign="right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="GAME_POINT" HeaderText="ｹﾞｰﾑﾎﾟｲﾝﾄ" DataFormatString="{0:N0}">
                                <ItemStyle HorizontalAlign="right" />
                            </asp:BoundField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
            <br />
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsQuestReward" runat="server" ConvertNullToDBNull="false"
        EnablePaging="True" OnSelected="dsQuestReward_Selected" SelectCountMethod="GetPageCount"
        SelectMethod="GetPageCollection" SortParameterName="" TypeName="QuestReward">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:QueryStringParameter Name="pQuestSeq" QueryStringField="questseq" Type="String" />
            <asp:QueryStringParameter Name="pLevelQuestSeq" QueryStringField="levelquestseq" Type="String" />
            <asp:QueryStringParameter Name="pLittleQuestSeq" QueryStringField="littlequestseq" Type="String" />
            <asp:QueryStringParameter Name="pOtherQuestSeq" QueryStringField="otherquestseq" Type="String" />
            <asp:QueryStringParameter Name="pQuestType" QueryStringField="questtype" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItemCategory" runat="server" SelectMethod="GetList"
        TypeName="GameItemCategory">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItem" runat="server" SelectMethod="GetListByCategoryType"
        TypeName="GameItem" OnSelecting="dsGameItem_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
            <asp:Parameter Name="pGameItemCategoryType" Type="String" DefaultValue="" />
            <asp:Parameter Name="pGameItemGetCd" Type="String" DefaultValue="" />
            <asp:Parameter Name="pPresentFlag" Type="String" DefaultValue="" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsItemGet" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="A2" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsLottery" runat="server" SelectMethod="GetList"
        TypeName="Lottery">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtItemCount">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtTicketCount">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="クエスト クリア条件情報を更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete"
        ConfirmText="クエスト クリア条件情報を削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>
