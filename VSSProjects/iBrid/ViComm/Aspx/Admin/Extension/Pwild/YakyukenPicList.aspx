﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="YakyukenPicList.aspx.cs" Inherits="Extension_Pwild_YakyukenPicList" Title="Untitled Page" %>
<%@ Import Namespace="ViComm" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="野球拳画像"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlKey">
			<fieldset class="fieldset">
				<legend>[検索]</legend>
				<asp:Panel ID="pnlSeekCondition" runat="server">
					<table border="0" style="width: 800px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle">
								サイトコード
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle2">
								ログインID
							</td>
							<td class="tdDataStyle">
                                <asp:TextBox ID="txtLoginId" runat="server" Width="100px"></asp:TextBox>
							</td>
						</tr>
					</table>
				</asp:Panel>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Seek" />
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[一覧]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count <%# GetRecCount() %></a>
					<br />
					<a class="reccount">Current viewing page <%= grdYakyukenPic.PageIndex + 1 %> of <%= grdYakyukenPic.PageCount %></a>
				</asp:Panel>
				&nbsp;
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="443px">
					<asp:GridView ID="grdYakyukenPic" runat="server" AllowPaging="True" AutoGenerateColumns="False" PageSize="30"
						DataSourceID="dsYakyukenPic" SkinID="GridViewColor" AllowSorting="true" OnRowDataBound="grdYakyukenPic_RowDataBound">
						<Columns>
							<asp:TemplateField HeaderText="ﾊﾝﾄﾞﾙ名<br>会員ID" SortExpression="LOGIN_ID">
								<ItemTemplate>
									<asp:HyperLink ID="lblHandleNm" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}",Eval("LOGIN_ID")) %>'
										Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:HyperLink><br />
									<asp:Label ID="lblLoginId" runat="server" Text='<%# Eval("LOGIN_ID") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle Width="120px" HorizontalAlign="left" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="画像">
								<ItemTemplate>
									<a href="<%# PickupHelper.GeneratePickupObjsViewerUrl(this, Eval("SITE_CD").ToString(),Eval("PIC_SEQ").ToString(), ViComm.ViCommConst.PicupTypes.YAKYUKEN_PIC,ViComm.ViCommConst.FLAG_OFF) %>">
										<asp:Image ID="imgYakyukenPic" runat="server" ImageUrl='<%# string.Format("../../{0}",Eval("SMALL_PHOTO_IMG_PATH").ToString()) %>'>
										</asp:Image>
									</a>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="コメント">
								<ItemTemplate>
									<asp:Label ID="lblCommentText" runat="server" Text='<%# Eval("COMMENT_TEXT").ToString().Replace(System.Environment.NewLine, "") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="left" Width="300px" Wrap="true" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="投稿日時<br />属性">
								<ItemTemplate>
									<asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("CREATE_DATE", "{0:yy/MM/dd HH:mm:ss}") %>'></asp:Label>
									<br />
									<asp:Label ID="lblYakyukenType" runat="server" Text='<%# GetYakyukenTypeNm(Eval("YAKYUKEN_TYPE")) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="center" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="顔出し">
								<ItemTemplate>
									<asp:LinkButton ID="lnkShowFaceFlag" runat="server" CommandArgument='<%#string.Format("{0},{1},{2},{3}",Eval("SITE_CD"),Eval("YAKYUKEN_PIC_SEQ"),Eval("SHOW_FACE_FLAG"),Eval("REVISION_NO")) %>'
										Text='<%# Eval("SHOW_FACE_FLAG").ToString().Equals("1") ? "あり" : "なし" %>' OnCommand="lnkShowFaceFlag_OnCommand"></asp:LinkButton>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="center" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="状態">
								<ItemTemplate>
									<asp:Label ID="lblAuthFlag" runat="server" Text='<%# Eval("AUTH_FLAG").ToString().Equals("1") ? "公開中" : "認証待ち" %>'></asp:Label>
									<br />
									<asp:LinkButton ID="lnkPublish" runat="server" CommandArgument='<%#string.Format("{0},{1},{2}",Eval("SITE_CD"),Eval("YAKYUKEN_PIC_SEQ"),Eval("REVISION_NO")) %>'
										Text='[公開]' OnCommand="lnkAuth_OnCommand" Visible='<%# Eval("AUTH_FLAG").ToString().Equals("1") ? false : true %>'></asp:LinkButton>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="閲覧数" SortExpression="VIEW_COUNT">
								<ItemTemplate>
									<asp:Label ID="lblViewCount" runat="server" Text='<%# Eval("VIEW_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="right" />
							</asp:TemplateField>
 							<asp:TemplateField HeaderText="削除">
								<ItemTemplate>
									<asp:LinkButton ID="lnkDelete" runat="server" CommandArgument='<%#string.Format("{0},{1},{2}",Eval("SITE_CD"),Eval("YAKYUKEN_PIC_SEQ"),Eval("REVISION_NO")) %>'
										Text='[削除]' OnCommand="lnkDelete_OnCommand" Visible="true" OnClientClick="return confirm('削除します。よろしいですか？');" ></asp:LinkButton>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" />
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
			<br />
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsYakyukenPic" runat="server" SelectMethod="GetPageCollection" ConvertNullToDBNull="true" SortParameterName=""
		TypeName="YakyukenPic" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelected="dsYakyukenPic_Selected"
		OnSelecting="dsYakyukenPic_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>

