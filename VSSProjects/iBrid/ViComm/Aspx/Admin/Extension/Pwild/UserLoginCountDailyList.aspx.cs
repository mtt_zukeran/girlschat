﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 課金者ログイン集計一覧
--	Progaram ID		: UserLoginCountDailyList
--
--  Creation Date	: 2016.06.27
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Extension_Pwild_UserLoginCountDailyList:System.Web.UI.Page {

	public class colInfo {
		public string title;
		public string okField;
		public string ftField;
		public string ngField;
		public string totalField;

		public colInfo(string pTitle,string pOkFiled,string pFtField,string pNgField,string pTotalField) {
			title = pTitle;
			okField = pOkFiled;
			ftField = pFtField;
			ngField = pNgField;
			totalField = pTotalField;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
			GetList();
		}
	}

	private void InitPage() {
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		grdUserLoginCountDaily.DataSourceID = "";
		ClearField();
		DataBind();

		if (!IsPostBack) {
			int iCurYear = int.Parse(DateTime.Now.ToString("yyyy"));
			int iStartYear = 2016;
			int iCount = iCurYear - iStartYear;
			lstYYYY.Items.Clear();
			for (int i = 0;i <= iCount;i++) {
				lstYYYY.Items.Add(new ListItem(iCurYear.ToString()));
				iCurYear--;
			}

			lstMM.SelectedValue = DateTime.Now.ToString("MM");
			lstSiteCd.DataSourceID = "";
		}
	}

	private void ClearField() {
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		Response.Redirect(Request.Url.PathAndQuery);
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		DataSet oDataSet;
		using (UserLoginCountDaily oUserLoginCountDaily = new UserLoginCountDaily()) {
			oDataSet = oUserLoginCountDaily.GetListCsv(this.lstSiteCd.SelectedValue,lstYYYY.SelectedValue,lstMM.SelectedValue);
		}

		//ヘッダ作成
		string sHeader = "報告日,ログイン,0時正常,0時ﾌｨﾙﾀﾘﾝｸﾞｴﾗｰ,0時不通ｴﾗｰ,0時合計,正常-正常,正常-ﾌｨﾙﾀﾘﾝｸﾞｴﾗｰ,正常-不通ｴﾗｰ,0時正常合計,ﾌｨﾙﾀﾘﾝｸﾞｴﾗｰ-正常,ﾌｨﾙﾀﾘﾝｸﾞｴﾗｰ-ﾌｨﾙﾀﾘﾝｸﾞｴﾗｰ,ﾌｨﾙﾀﾘﾝｸﾞｴﾗｰ-不通ｴﾗｰ,0時ﾌｨﾙﾀﾘﾝｸﾞｴﾗｰ合計,不通ｴﾗｰ-正常,不通ｴﾗｰ-ﾌｨﾙﾀﾘﾝｸﾞｴﾗｰ,不通ｴﾗｰ-不通ｴﾗｰ,0時不通ｴﾗｰ合計,ﾛｸﾞｲﾝ正常合計,ﾛｸﾞｲﾝﾌｨﾙﾀﾘﾝｸﾞｴﾗｰ合計,ﾛｸﾞｲﾝ不通ｴﾗｰ合計,ﾛｸﾞｲﾝ合計";

		//CSV出力 
		Response.ContentType = "application/download";
		Response.AppendHeader("Content-Disposition","attachment;filename=UserLoginCountDailyList.csv");
		Response.ContentEncoding = System.Text.Encoding.GetEncoding("Shift_JIS");
		
		string[] sSinceLastLoginDaysArr = {"","前日","2日前","3日前","4日前","5日前","6日前","1週間以上前"};

		Response.Write(sHeader + "\r\n");
		if (oDataSet.Tables[0].Rows.Count > 0) {
			foreach (DataRow oCsvRow in oDataSet.Tables[0].Rows) {
				string sData =
					oCsvRow["REPORT_DAY"].ToString() + "," +
					sSinceLastLoginDaysArr[int.Parse(oCsvRow["SINCE_LAST_LOGIN_DAYS"].ToString())] + "," +
					oCsvRow["START_MAIL_ADDR_STATUS_OK"].ToString() + "," +
					oCsvRow["START_MAIL_ADDR_STATUS_FT"].ToString() + "," +
					oCsvRow["START_MAIL_ADDR_STATUS_NG"].ToString() + "," +
					oCsvRow["TOTAL_START_COUNT"].ToString() + "," +
					oCsvRow["MAIL_ADDR_STATUS_OK_OK"].ToString() + "," +
					oCsvRow["MAIL_ADDR_STATUS_OK_FT"].ToString() + "," +
					oCsvRow["MAIL_ADDR_STATUS_OK_NG"].ToString() + "," +
					oCsvRow["TOTAL_START_OK_LOGIN"].ToString() + "," +
					oCsvRow["MAIL_ADDR_STATUS_FT_OK"].ToString() + "," +
					oCsvRow["MAIL_ADDR_STATUS_FT_FT"].ToString() + "," +
					oCsvRow["MAIL_ADDR_STATUS_FT_NG"].ToString() + "," +
					oCsvRow["TOTAL_START_FT_LOGIN"].ToString() + "," +
					oCsvRow["MAIL_ADDR_STATUS_NG_OK"].ToString() + "," +
					oCsvRow["MAIL_ADDR_STATUS_NG_FT"].ToString() + "," +
					oCsvRow["MAIL_ADDR_STATUS_NG_NG"].ToString() + "," +
					oCsvRow["TOTAL_START_NG_LOGIN"].ToString() + "," +
					oCsvRow["TOTAL_END_OK_LOGIN"].ToString() + "," +
					oCsvRow["TOTAL_END_FT_LOGIN"].ToString() + "," +
					oCsvRow["TOTAL_END_NG_LOGIN"].ToString() + "," +
					oCsvRow["TOTAL_LOGIN_COUNT"].ToString();

				Response.Write(sData + "\r\n");
			}
		}
		Response.End();
	}

	protected void grdUserLoginCountDaily_RowDataBound(object sender,GridViewRowEventArgs e) {
	}

	private void GetList() {
		grdUserLoginCountDaily.PageIndex = 0;
		grdUserLoginCountDaily.DataSourceID = "dsUserLoginCountDaily";
		DataBind();
		AddHeader();
	}

	protected void dsUserLoginCountDaily_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = lstYYYY.SelectedValue;
		e.InputParameters[2] = lstMM.SelectedValue;
		e.InputParameters[3] = lstSinceLastLoginDays.SelectedValue;
	}
	
	protected Color GetBackColor(object pDayOfWeek) {
		if (pDayOfWeek.ToString().Equals("Sat")) {
			return Color.FromArgb(0xCCCCFF);
		} else {
			if (pDayOfWeek.ToString().Equals("Sun")) {
				return Color.FromArgb(0xFFCCCC);
			} else {
				return Color.Empty;
			}
		}
	}

	private void AddHeader() {

		GridViewRow row = new GridViewRow(-1,-1,DataControlRowType.Header,DataControlRowState.Normal);
		TableCell cell;
		cell = new TableCell();
		cell.ColumnSpan = 2;
		cell.RowSpan = 1;
		cell.Text = "";
		row.Cells.Add(cell);

		cell = new TableCell();
		cell.ColumnSpan = 4;
		cell.Text = "0時時点";
		cell.HorizontalAlign = HorizontalAlign.Center;
		row.Cells.Add(cell);

		cell = new TableCell();
		cell.ColumnSpan = 4;
		cell.Text = "0時時点正常";
		cell.HorizontalAlign = HorizontalAlign.Center;
		row.Cells.Add(cell);

		cell = new TableCell();
		cell.ColumnSpan = 4;
		cell.Text = "0時時点ﾌｨﾙﾀﾘﾝｸﾞｴﾗｰ";
		cell.HorizontalAlign = HorizontalAlign.Center;
		row.Cells.Add(cell);

		cell = new TableCell();
		cell.ColumnSpan = 4;
		cell.Text = "0時時点不通ｴﾗｰ";
		cell.HorizontalAlign = HorizontalAlign.Center;
		row.Cells.Add(cell);

		cell = new TableCell();
		cell.ColumnSpan = 4;
		cell.Text = "合計";
		cell.HorizontalAlign = HorizontalAlign.Center;
		row.Cells.Add(cell);
		grdUserLoginCountDaily.Controls[0].Controls.AddAt(0,row);
	}
}