﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ElectionPeriodList.aspx.cs" Inherits="Extension_Pwild_ElectionPeriodList" Title="投票設定一覧(エントリー制)" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="投票設定一覧(エントリー制)"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlSearch">
			<fieldset class="fieldset">
				<legend>[検索]</legend>
				<asp:Panel ID="pnlSeekCondition" runat="server">
					<table border="0" style="width: 800px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle2">
								サイトコード
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px">
								</asp:DropDownList>
							</td>
						</tr>
					</table>
				</asp:Panel>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Seek" />
				<asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton"  OnClick="btnCreate_Click" ValidationGroup="Create" />
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlInput">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Label ID="lblErrorMessage" runat="server" Text="" ForeColor="red"></asp:Label>
				<table border="0" style="width: 650px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							名称
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtElectionPeriodNm" runat="server" Width="200px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							エントリー開始日時
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstEntryStartDateYYYY" runat="server" Width="60px" DataSource='<%# YearArray %>'>
							</asp:DropDownList>年
							<asp:DropDownList ID="lstEntryStartDateMM" runat="server" Width="40px" DataSource='<%# MonthArray %>'>
							</asp:DropDownList>月
							<asp:DropDownList ID="lstEntryStartDateDD" runat="server" Width="40px" DataSource='<%# DayArray %>'>
							</asp:DropDownList>日
							<asp:DropDownList ID="lstEntryStartDateHH" runat="server" Width="40px" DataSource='<%# HourArray %>'>
							</asp:DropDownList>時
							<asp:DropDownList ID="lstEntryStartDateMI" runat="server" DataSource='<%# MinuteArray %>'>
							</asp:DropDownList>分
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							エントリー終了日時
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstEntryEndDateYYYY" runat="server" Width="60px" DataSource='<%# YearArray %>'>
							</asp:DropDownList>年
							<asp:DropDownList ID="lstEntryEndDateMM" runat="server" Width="40px" DataSource='<%# MonthArray %>'>
							</asp:DropDownList>月							<asp:DropDownList ID="lstEntryEndDateDD" runat="server" Width="40px" DataSource='<%# DayArray %>'>
							</asp:DropDownList>日
							<asp:DropDownList ID="lstEntryEndDateHH" runat="server" Width="40px" DataSource='<%# HourArray %>'>
							</asp:DropDownList>時							<asp:DropDownList ID="lstEntryEndDateMI" runat="server" DataSource='<%# MinuteArray %>'>
							</asp:DropDownList>分
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							1次投票開始日時
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstFirstPeriodStartDateYYYY" runat="server" Width="60px" DataSource='<%# YearArray %>'>
							</asp:DropDownList>年
							<asp:DropDownList ID="lstFirstPeriodStartDateMM" runat="server" Width="40px" DataSource='<%# MonthArray %>'>
							</asp:DropDownList>月
							<asp:DropDownList ID="lstFirstPeriodStartDateDD" runat="server" Width="40px" DataSource='<%# DayArray %>'>
							</asp:DropDownList>日
							<asp:DropDownList ID="lstFirstPeriodStartDateHH" runat="server" Width="40px" DataSource='<%# HourArray %>'>
							</asp:DropDownList>時
							<asp:DropDownList ID="lstFirstPeriodStartDateMI" runat="server" DataSource='<%# MinuteArray %>'>
							</asp:DropDownList>分
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							1次投票終了日時
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstFirstPeriodEndDateYYYY" runat="server" Width="60px" DataSource='<%# YearArray %>'>
							</asp:DropDownList>年
							<asp:DropDownList ID="lstFirstPeriodEndDateMM" runat="server" Width="40px" DataSource='<%# MonthArray %>'>
							</asp:DropDownList>月							<asp:DropDownList ID="lstFirstPeriodEndDateDD" runat="server" Width="40px" DataSource='<%# DayArray %>'>
							</asp:DropDownList>日
							<asp:DropDownList ID="lstFirstPeriodEndDateHH" runat="server" Width="40px" DataSource='<%# HourArray %>'>
							</asp:DropDownList>時							<asp:DropDownList ID="lstFirstPeriodEndDateMI" runat="server" DataSource='<%# MinuteArray %>'>
							</asp:DropDownList>分
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							2次投票開始日時
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSecondPeriodStartDateYYYY" runat="server" Width="60px" DataSource='<%# YearArray %>'>
							</asp:DropDownList>年
							<asp:DropDownList ID="lstSecondPeriodStartDateMM" runat="server" Width="40px" DataSource='<%# MonthArray %>'>
							</asp:DropDownList>月
							<asp:DropDownList ID="lstSecondPeriodStartDateDD" runat="server" Width="40px" DataSource='<%# DayArray %>'>
							</asp:DropDownList>日
							<asp:DropDownList ID="lstSecondPeriodStartDateHH" runat="server" Width="40px" DataSource='<%# HourArray %>'>
							</asp:DropDownList>時
							<asp:DropDownList ID="lstSecondPeriodStartDateMI" runat="server" DataSource='<%# MinuteArray %>'>
							</asp:DropDownList>分
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							2次投票終了日時
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSecondPeriodEndDateYYYY" runat="server" Width="60px" DataSource='<%# YearArray %>'>
							</asp:DropDownList>年
							<asp:DropDownList ID="lstSecondPeriodEndDateMM" runat="server" Width="40px" DataSource='<%# MonthArray %>'>
							</asp:DropDownList>月							<asp:DropDownList ID="lstSecondPeriodEndDateDD" runat="server" Width="40px" DataSource='<%# DayArray %>'>
							</asp:DropDownList>日
							<asp:DropDownList ID="lstSecondPeriodEndDateHH" runat="server" Width="40px" DataSource='<%# HourArray %>'>
							</asp:DropDownList>時							<asp:DropDownList ID="lstSecondPeriodEndDateMI" runat="server" DataSource='<%# MinuteArray %>'>
							</asp:DropDownList>分
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							2次投票進出可能数
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtCanEntrySecondPeriodCount" runat="server" Width="50px"></asp:TextBox>回
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							1次投票無料投票回数
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtFirstPeriodFreeVoteCount" runat="server" Width="50px"></asp:TextBox>回
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							2次投票無料投票回数
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtSecondPeriodFreeVoteCount" runat="server" Width="50px"></asp:TextBox>回
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							投票券追加消費ポイント
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtTicketPoint" runat="server" Width="50px"></asp:TextBox>pt/1枚
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							ファンクラブ決済得票数
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtAddVotePerFanclubMember" runat="server" Width="50px"></asp:TextBox>pt/1枚
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							獲得スター得票数
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtAddOneVoteNeedStars" runat="server" Width="50px"></asp:TextBox>pt毎に1票加算
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							順位表示
						</td>
						<td class="tdDataStyle">
							<asp:RadioButton ID="rdoFixRankFlagOn" runat="server" Text="固定" GroupName="grpFixRankFlag" />
							<asp:RadioButton ID="rdoFixRankFlagOff" runat="server" Text="リアルタイム" GroupName="grpFixRankFlag" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							固定順位更新日時
						</td>
						<td class="tdDataStyle">
							<asp:Label ID="lblFixRankDate" runat="server"></asp:Label>&nbsp;
							<asp:Button runat="server" ID="btnElectionFixRankUpdate" Text="固定順位を更新" OnClick="btnElectionRankUpdate_Click" />
						</td>
					</tr>
				</table>
				<asp:Panel ID="pnlCommand" runat="server" Width="660px">
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" />
					<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" />
					<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="delBtnStyle" OnClick="btnDelete_Click" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlList">
			<fieldset>
				<legend>[一覧]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">
						Record Count <%# GetRecCount() %>
					</a>
					<br />
					<a class="reccount">
						Current viewing page <%= grdElectionPeriod.PageIndex + 1%> of <%= grdElectionPeriod.PageCount%>
					</a>
				</asp:Panel>
				&nbsp;
				<asp:Panel ID="pnlGrid" runat="server">
					<asp:GridView ID="grdElectionPeriod" DataSourceID="dsElectionPeriod" runat="server" AllowPaging="True" AutoGenerateColumns="False" PageSize="30" EnableSortingAndPagingCallbacks="false" SkinID="GridViewColor" AllowSorting="true">
						<Columns>
							<asp:BoundField DataField="ELECTION_PERIOD_NM" HeaderText="名称">
								<ItemStyle HorizontalAlign="Left" />
							</asp:BoundField>
							<asp:BoundField DataField="ENTRY_START_DATE" HeaderText="ｴﾝﾄﾘｰ開始" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
								<ItemStyle HorizontalAlign="Left" Wrap="false" />
							</asp:BoundField>
							<asp:BoundField DataField="ENTRY_END_DATE" HeaderText="ｴﾝﾄﾘｰ終了" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
								<ItemStyle HorizontalAlign="Left" Wrap="false" />
							</asp:BoundField>
							<asp:BoundField DataField="FIRST_PERIOD_START_DATE" HeaderText="1次投票開始" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
								<ItemStyle HorizontalAlign="Left" Wrap="false" />
							</asp:BoundField>
							<asp:BoundField DataField="FIRST_PERIOD_END_DATE" HeaderText="1次投票終了" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
								<ItemStyle HorizontalAlign="Left" Wrap="false" />
							</asp:BoundField>
							<asp:BoundField DataField="SECOND_PERIOD_START_DATE" HeaderText="2次投票開始" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
								<ItemStyle HorizontalAlign="Left" Wrap="false" />
							</asp:BoundField>
							<asp:BoundField DataField="SECOND_PERIOD_END_DATE" HeaderText="2次投票終了" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
								<ItemStyle HorizontalAlign="Left" Wrap="false" />
							</asp:BoundField>
							<asp:TemplateField>
								<HeaderTemplate>
									順位表示
								</HeaderTemplate>
								<ItemTemplate>
									<%# GetFixRankFlagStr(Eval("FIX_RANK_FLAG")) %>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="ADD_VOTE_PER_FANCLUB_MEMBER" HeaderText="FC票">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:BoundField DataField="ADD_ONE_VOTE_NEED_STARS" HeaderText="獲得ｽﾀｰ票" DataFormatString="{0}pt毎に1票">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:TemplateField>
								<ItemTemplate>
									<asp:HyperLink ID="lnkElectionRank" runat="server" NavigateUrl='<%# string.Format("~/Extension/Pwild/ElectionEntryList.aspx?sitecd={0}&electionperiodseq={1}",Eval("SITE_CD"),Eval("ELECTION_PERIOD_SEQ")) %>' Text="順位">
									</asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<ItemTemplate>
									<asp:HyperLink ID="lnkElectionVoteTicketCd" runat="server" NavigateUrl='<%# string.Format("~/Extension/Pwild/ElectionVoteTicketCdList.aspx?sitecd={0}&electionperiodseq={1}",Eval("SITE_CD"),Eval("ELECTION_PERIOD_SEQ")) %>' Text="投票券ｺｰﾄﾞ">
									</asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<ItemTemplate>
									<asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%# Container.DataItemIndex %>' OnCommand="lnkEdit_Command" Text="編集"></asp:LinkButton>
									<asp:HiddenField ID="hdnElectionPeriodSeq" runat="server" Value='<%# Eval("ELECTION_PERIOD_SEQ") %>' />
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
			<br />
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsElectionPeriod" runat="server" ConvertNullToDBNull="false" EnablePaging="True" OnSelecting="dsElectionPeriod_Selecting" OnSelected="dsElectionPeriod_Selected"
		SelectCountMethod="GetPageCount" SelectMethod="GetPageCollection" SortParameterName="" TypeName="ElectionPeriod">
		<SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>