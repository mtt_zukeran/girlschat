﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="StageLevelCountReport.aspx.cs" Inherits="Extension_Pwild_StageLevelCountReport" Title="ステージレベル分布レポート" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="ステージレベル分布レポート"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset>
                <legend>[設定]</legend>
                <table border="0" style="width: 640px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            サイトコード

                        </td>
                        <td class="tdDataStyle" colspan="3">
                            <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="180px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    CausesValidation="False" />
            </fieldset>
        </asp:Panel>
        ステージ名:<asp:Label ID="lblStageNm" runat="server" Text=""></asp:Label>&nbsp;クリア人数：<asp:Label ID="lblStageClearCount" runat="server" Text=""></asp:Label>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[ステージレベル分布レポート]</legend>
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="552px">
                    <asp:GridView ID="grdStageLevelCountReport" runat="server" AutoGenerateColumns="False" DataSourceID="dsStageLevelCountReport"
                        AllowSorting="true" SkinID="GridViewColor">
                        <Columns>
                            <asp:TemplateField HeaderText="レベル">
                                <ItemTemplate>
                                    <asp:Label ID="lblLevel" runat="server" Text='<%# Eval("GAME_CHARACTER_LEVEL") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="人数">
                                <ItemTemplate>
                                    <asp:Label ID="lblCount" runat="server" Text='<%# Eval("GAME_CHARACTER_LEVEL_COUNT") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle ForeColor="Black" BackColor="LightYellow" Font-Bold="true" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsStageLevelCountReport" runat="server" SelectMethod="GetList" TypeName="StageLevelCountReport">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSeekSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
            <asp:QueryStringParameter Name="pStageSeq" QueryStringField="stageseq" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
