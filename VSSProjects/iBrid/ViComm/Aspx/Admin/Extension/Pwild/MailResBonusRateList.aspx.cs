﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メール返信ボーナスポイント確率設定
--	Progaram ID		: MailResBonusRateList
--  Creation Date	: 2016.07.28
--  Creater			: Y.Ikemiya
**************************************************************************/
using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;

public partial class Extension_Pwild_MailResBonusRateList:System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SITE_CD"]);
		}
		set {
			this.ViewState["SITE_CD"] = value;
		}
	}

	private string MailResBonusSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MAIL_RES_BONUS_SEQ"]);
		}
		set {
			this.ViewState["MAIL_RES_BONUS_SEQ"] = value;
		}
	}

	private string MailResBonusRateSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MAIL_RES_BONUS_RATE_SEQ"]);
		}
		set {
			this.ViewState["MAIL_RES_BONUS_RATE_SEQ"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["REVISION_NO"]);
		}
		set {
			this.ViewState["REVISION_NO"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			this.MailResBonusSeq = iBridUtil.GetStringValue(Request.QueryString["mailresbonusseq"]);

			this.pnlSearch.Enabled = true;
			this.pnlInput.Visible = false;

			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	protected void btnBack_Click(object sender,EventArgs e) {
		this.Response.Redirect(string.Format("~/Extension/Pwild/MailResBonusList.aspx?sitecd={0}",this.SiteCd));
		return;
	}

	protected void dsMailResBonusRate_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		MailResBonusRate.SearchCondition oSearchCondition = new MailResBonusRate.SearchCondition();
		oSearchCondition.SiteCd = this.SiteCd;
		oSearchCondition.MailResBonusSeq = this.MailResBonusSeq;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsMailResBonusRate_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void grdMailResBonusRate_RowDataBound(object sender,GridViewRowEventArgs e) {
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			grdMailResBonusRate.PageIndex = 0;
			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		this.MailResBonusRateSeq = string.Empty;
		this.RevisionNo = string.Empty;
		this.ClearFileds();

		this.pnlSearch.Enabled = false;
		this.pnlInput.Visible = true;
		this.btnDelete.Enabled = false;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (CheckInput()) {
			string sResult;
			MailResBonusRateMainte(ViCommConst.FLAG_OFF,out sResult);

			this.pnlSearch.Enabled = true;
			this.pnlInput.Visible = false;
			grdMailResBonusRate.PageIndex = 0;
			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlSearch.Enabled = true;
		this.pnlInput.Visible = false;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		string sResult;
		MailResBonusRateMainte(ViCommConst.FLAG_ON,out sResult);

		this.pnlSearch.Enabled = true;
		this.pnlInput.Visible = false;
		grdMailResBonusRate.PageIndex = 0;
		pnlGrid.DataBind();
		pnlCount.DataBind();
	}

	protected void lnkEdit_Command(object sender,CommandEventArgs e) {
		int iIndex = int.Parse(iBridUtil.GetStringValue(e.CommandArgument));
		GridViewRow oRow = this.grdMailResBonusRate.Rows[iIndex];
		HiddenField oHiddenField = oRow.FindControl("hdnMailResBonusRateSeq") as HiddenField;
		this.MailResBonusRateSeq = oHiddenField.Value;

		this.ClearFileds();
		this.MailResBonusRateGet();

		this.pnlSearch.Enabled = false;
		this.pnlInput.Visible = true;
		this.btnDelete.Enabled = true;
	}

	private void ClearFileds() {
		this.lblErrorMessage.Text = string.Empty;
		this.txtAddPoint.Text = string.Empty;
		this.txtRate.Text = string.Empty;
	}

	private bool CheckInput() {

		if (string.IsNullOrEmpty(this.txtAddPoint.Text)) {
			this.lblErrorMessage.Text = "付与ポイントを入力して下さい";
			return false;
		}

		if (string.IsNullOrEmpty(this.txtRate.Text)) {
			this.lblErrorMessage.Text = "確率を入力して下さい";
			return false;
		}

		return true;
	}

	private void MailResBonusRateGet() {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("MAIL_RES_BONUS_RATE_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pMAIL_RES_BONUS_SEQ",DbSession.DbType.VARCHAR2,this.MailResBonusSeq);
			oDbSession.ProcedureInParm("pMAIL_RES_BONUS_RATE_SEQ",DbSession.DbType.VARCHAR2,this.MailResBonusRateSeq);
			oDbSession.ProcedureOutParm("pADD_POINT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pRATE",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();

			this.txtAddPoint.Text = oDbSession.GetStringValue("pADD_POINT");
			this.txtRate.Text = oDbSession.GetStringValue("pRATE");
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
		}
	}

	private void MailResBonusRateMainte(int pDeleteFlag,out string pResult) {

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("MAIL_RES_BONUS_RATE_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pMAIL_RES_BONUS_SEQ",DbSession.DbType.NUMBER,this.MailResBonusSeq);
			oDbSession.ProcedureInParm("pMAIL_RES_BONUS_RATE_SEQ",DbSession.DbType.NUMBER,this.MailResBonusRateSeq);
			oDbSession.ProcedureInParm("pADD_POINT",DbSession.DbType.NUMBER,this.txtAddPoint.Text);
			oDbSession.ProcedureInParm("pRATE",DbSession.DbType.NUMBER,this.txtRate.Text);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDeleteFlag);
			oDbSession.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
			pResult = oDbSession.GetStringValue("pRESULT");
		}
	}
}