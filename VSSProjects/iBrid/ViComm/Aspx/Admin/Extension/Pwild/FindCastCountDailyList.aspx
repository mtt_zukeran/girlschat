﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="FindCastCountDailyList.aspx.cs" Inherits="Extension_Pwild_FindCastCountDailyList"
	Title="女性検索回数集計一覧" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="女性検索回数集計一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							<asp:Label ID="lblReportMonth" runat="server" Text="報告年月"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstYYYY" runat="server" Width="54px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstMM" runat="server" Width="43px">
							</asp:DropDownList>月
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
                <asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" CausesValidation="False"
                    OnClick="btnCSV_Click" />
			</asp:Panel>
		</fieldset>
		<fieldset>
			<legend>[集計]</legend>
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="700px">
				<asp:GridView ID="grdFindCastCountDaily" runat="server" AutoGenerateColumns="False" DataSourceID="dsFindCastCountDaily" OnRowDataBound="grdFindCastCountDaily_RowDataBound"
					SkinID="GridViewColor">
					<Columns>
						<asp:TemplateField HeaderText="日付">
							<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" CssClass="RowStyleNoPad" />
							<ItemTemplate>
								<asp:Label ID="lblDisp" runat="server" Text='<%# Eval("DISPLAY_DAY") %>' BackColor='<%# GetBackColor(Eval("REPORT_DAY_OF_WEEK")) %>' Width="38px"></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="曜日">
							<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" CssClass="RowStyleNoPad" />
							<ItemTemplate>
								<asp:Label ID="lblWeek" runat="server" Text='<%# Eval("REPORT_DAY_OF_WEEK") %>' BackColor='<%# GetBackColor(Eval("REPORT_DAY_OF_WEEK")) %>' Width="38px"></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField HeaderText="検索総数" DataField="TOTAL_COUNT" ItemStyle-HorizontalAlign="Right" />
						<asp:BoundField HeaderText="検索人数(ﾕﾆｰｸ)" DataField="UNIQUE_USER_COUNT" ItemStyle-HorizontalAlign="Right" />
						<asp:BoundField HeaderText="年齢" DataField="AGE_ALL_COUNT" ItemStyle-HorizontalAlign="Right" />
						<asp:TemplateField HeaderText="地域">
							<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" CssClass="RowStyleNoPad" />
							<ItemTemplate>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField HeaderText="ｴｯﾁ度" DataField="H_ALL_COUNT" ItemStyle-HorizontalAlign="Right" />
						<asp:BoundField HeaderText="SM度" DataField="SM_ALL_COUNT" ItemStyle-HorizontalAlign="Right" />
						<asp:BoundField HeaderText="ｽﾀｲﾙ" DataField="STYLE_ALL_COUNT" ItemStyle-HorizontalAlign="Right" />
						<asp:BoundField HeaderText="ﾀｲﾌﾟ" DataField="LIKE_TYPE_ALL_COUNT" ItemStyle-HorizontalAlign="Right" />
						<asp:BoundField HeaderText="職業" DataField="JOB_ALL_COUNT" ItemStyle-HorizontalAlign="Right" />
						<asp:BoundField HeaderText="出没時間" DataField="TIMEZONE_ALL_COUNT" ItemStyle-HorizontalAlign="Right" />
						<asp:BoundField HeaderText="TEL待ち" DataField="WAIT_TEL_ALL_COUNT" ItemStyle-HorizontalAlign="Right" />
						<asp:BoundField HeaderText="顔出し" DataField="FACE_DISP_ALL_COUNT" ItemStyle-HorizontalAlign="Right" />
						<asp:BoundField HeaderText="名前含" DataField="HANDLE_NM_ALL_COUNT" ItemStyle-HorizontalAlign="Right" />
						<asp:TemplateField HeaderText="ｱﾀﾞﾙﾄNG">
							<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" CssClass="RowStyleNoPad" />
							<ItemTemplate>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField HeaderText="身長" DataField="HEIGHT_ALL_COUNT" ItemStyle-HorizontalAlign="Right" />
						<asp:BoundField HeaderText="ﾊﾞｽﾄ" DataField="BUST_ALL_COUNT" ItemStyle-HorizontalAlign="Right" />
						<asp:BoundField HeaderText="血液型" DataField="BLOOD_TYPE_ALL_COUNT" ItemStyle-HorizontalAlign="Right" />
						<asp:BoundField HeaderText="ﾌﾘｰﾜｰﾄﾞ" DataField="COMMENT_ALL_COUNT" ItemStyle-HorizontalAlign="Right" />
						<asp:BoundField HeaderText="全て" DataField="STATUS_ALL_COUNT" ItemStyle-HorizontalAlign="Right" />
						<asp:BoundField HeaderText="ﾛｸﾞｲﾝ中" DataField="STATUS_LOGIN_COUNT" ItemStyle-HorizontalAlign="Right" />
						<asp:BoundField HeaderText="待機中" DataField="STATUS_WAIT_COUNT" ItemStyle-HorizontalAlign="Right" />
						<asp:BoundField HeaderText="指定なし" DataField="AREA_ALL_COUNT" ItemStyle-HorizontalAlign="Right" />
						<asp:BoundField HeaderText="ﾃﾞﾌｫ" DataField="AREA_DEFAULT_COUNT" ItemStyle-HorizontalAlign="Right" />
						<asp:BoundField HeaderText="他地域" DataField="AREA_OTHER_COUNT" ItemStyle-HorizontalAlign="Right" />
						<asp:BoundField HeaderText="問わない" DataField="ADULT_ALL_COUNT" ItemStyle-HorizontalAlign="Right" />
						<asp:BoundField HeaderText="しない" DataField="ADULT_NG_COUNT" ItemStyle-HorizontalAlign="Right" />
					</Columns>
					<FooterStyle ForeColor="Black" BackColor="LightYellow" />
				</asp:GridView>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsFindCastCountDaily" runat="server" SelectMethod="GetList" TypeName="FindCastCountDaily" OnSelecting="dsFindCastCountDaily_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pYear" Type="String" />
			<asp:Parameter Name="pMonth" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
</asp:Content>
