﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ElectionEntryList.aspx.cs" Inherits="Extension_Pwild_ElectionEntryList" Title="投票順位一覧(エントリー制)" %>
<%@ Import Namespace="ViComm" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="投票順位一覧(エントリー制)"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlList">
			<fieldset>
				<legend>[一覧]</legend>
				<asp:Label ID="lblBulkUpdateError" runat="server" Text="" ForeColor="red"></asp:Label>
				<asp:Label ID="lblBulkUpdateComplete" runat="server" Text="一括更新が完了しました" ForeColor="blue" Visible="false"></asp:Label>
				<asp:Panel ID="pnlButtonBulkUpdate" runat="server" Width="660px">
					<asp:Button ID="btnBulkUpdate" runat="server" Text="一括更新" OnClick="btnBulkUpdate_Click" />
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">
						Record Count <%# GetRecCount() %>
					</a>
					<br />
					<a class="reccount">
						Current viewing page <%= grdElectionEntry.PageIndex + 1%> of <%= grdElectionEntry.PageCount%>
					</a>
				</asp:Panel>
				&nbsp;
				<asp:Panel ID="pnlGrid" runat="server">
					<asp:GridView ID="grdElectionEntry" DataSourceID="dsElectionEntry" runat="server" AllowPaging="True" AutoGenerateColumns="False" PageSize="50"
						EnableSortingAndPagingCallbacks="false" SkinID="GridViewColor" AllowSorting="true" OnRowDataBound="grdElectionEntry_RowDataBound">
						<Columns>
							<asp:TemplateField HeaderText="順位">
								<ItemTemplate>
									<asp:Label ID="lblElectionEntry" runat="server" Text='<%# Eval("VOTE_RANK") %>' Visible='<%# GetElectionEntryVisible(Eval("RETIRE_FLAG"),Eval("REFUSE_FLAG"),Eval("NA_FLAG")) %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ログインID">
								<ItemTemplate>
									<asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}",Eval("LOGIN_ID")) %>' Text='<%# Eval("LOGIN_ID") %>'>
									</asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="HANDLE_NM" HeaderText="ハンドル名">
								<ItemStyle HorizontalAlign="Left" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="投票数<br>(1次)">
								<ItemTemplate>
									<asp:Label ID="lblFirstVoteCount" runat="server" Text='<%# Eval("FIRST_VOTE_COUNT") %>'></asp:Label>
									
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="実投票数<br>(1次)">
								<ItemTemplate>
									<asp:Label ID="lblFirstRealVoteCount" runat="server" Text='<%# Eval("FIRST_REAL_VOTE_COUNT") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="FC会費分<br>(1次)">
								<ItemTemplate>
									<asp:Label ID="lblFirstFanclubVoteCount" runat="server" Text='<%# Eval("FIRST_FANCLUB_VOTE_COUNT") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="獲得ｽﾀｰ分<br>(1次)">
								<ItemTemplate>
									<asp:Label ID="lblFirstGetPointVoteCount" runat="server" Text='<%# Eval("FIRST_GET_POINT_VOTE_COUNT") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="加算数<br>(1次)">
								<ItemTemplate>
									<asp:TextBox ID="txtFirstAddVoteCount" runat="server" Text='<%# Eval("FIRST_ADD_VOTE_COUNT") %>' Width="50px"></asp:TextBox>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="投票数<br>(2次)">
								<ItemTemplate>
									<asp:Label ID="lblSecondVoteCount" runat="server" Text='<%# Eval("SECOND_VOTE_COUNT") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="実投票数<br>(2次)">
								<ItemTemplate>
									<asp:Label ID="lblSecondRealVoteCount" runat="server" Text='<%# Eval("SECOND_REAL_VOTE_COUNT") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="FC会費分<br>(2次)">
								<ItemTemplate>
									<asp:Label ID="lblSecondFanclubVoteCount" runat="server" Text='<%# Eval("SECOND_FANCLUB_VOTE_COUNT") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="獲得ｽﾀｰ分<br>(2次)">
								<ItemTemplate>
									<asp:Label ID="lblSecondGetPointVoteCount" runat="server" Text='<%# Eval("SECOND_GET_POINT_VOTE_COUNT") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="加算数<br>(2次)">
								<ItemTemplate>
									<asp:TextBox ID="txtSecondAddVoteCount" runat="server" Text='<%# Eval("SECOND_ADD_VOTE_COUNT") %>' Width="50px"></asp:TextBox>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="VOTE_COUNT" HeaderText="合計投票数">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:TemplateField>
								<ItemTemplate>
									<asp:LinkButton ID="lnkRetire" runat="server" CommandArgument='<%# Container.DataItemIndex %>' OnCommand="lnkRetire_Command" Text="辞退" OnClientClick="return confirm('辞退します。よろしいですか？');">
									</asp:LinkButton>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<ItemTemplate>
									<asp:LinkButton ID="lnkDel" runat="server" CommandArgument='<%# Container.DataItemIndex %>'
                                        OnClientClick="return confirm('削除します。よろしいですか？');" OnCommand="lnkDelete_Command"
                                        Text='<%# ViCommConst.FLAG_ON_STR.Equals(Eval("REFUSE_FLAG","{0}")) ? "復活" : "削除" %>'></asp:LinkButton>
									<asp:HiddenField ID="hdnElectionEntrySeq" runat="server" Value='<%# Eval("ELECTION_ENTRY_SEQ") %>' />
									<asp:HiddenField ID="hdnRefuseFlag" runat="server" Value='<%# Eval("REFUSE_FLAG") %>' />
									<asp:HiddenField ID="hdnRevisionNo" runat="server" Value='<%# Eval("REVISION_NO") %>' />
								</ItemTemplate>
							</asp:TemplateField>
 							<asp:TemplateField HeaderText="状態">
								<ItemTemplate>
									<%# GetDelFlagMark(Eval("RETIRE_FLAG"),Eval("REFUSE_FLAG")) %>
								</ItemTemplate>
								<ItemStyle VerticalAlign="Top" HorizontalAlign="Center" />
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
			<br />
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsElectionEntry" runat="server" ConvertNullToDBNull="false" EnablePaging="True" OnSelecting="dsElectionEntry_Selecting" OnSelected="dsElectionEntry_Selected"
		SelectCountMethod="GetPageCount" SelectMethod="GetPageCollection" SortParameterName="" TypeName="ElectionEntry">
		<SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnBulkUpdate" ConfirmText="一括更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>

