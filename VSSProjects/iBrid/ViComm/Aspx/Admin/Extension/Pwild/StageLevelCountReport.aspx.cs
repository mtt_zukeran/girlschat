﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ステージレベル分布レポート

--	Progaram ID		: StageLevelCountReport
--
--  Creation Date	: 2012.12.04
--  Creater			: M&TT A.Taba
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using iBridCommLib;
using System.Drawing;
using ViComm;

public partial class Extension_Pwild_StageLevelCountReport:System.Web.UI.Page {

	protected string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		private set {
			this.ViewState["SexCd"] = value;
		}
	}

	protected string StageSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["StageSeq"]);
		}
		private set {
			this.ViewState["StageSeq"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		if (!this.IsPostBack) {
			this.SexCd = this.Request.QueryString["sexcd"];
			this.StageSeq = this.Request.QueryString["stageseq"];
			if (ViCommConst.MAN.Equals(this.SexCd)) {
				this.lblPgmTitle.Text = "男性" + this.lblPgmTitle.Text;
			} else {
				this.lblPgmTitle.Text = "女性" + this.lblPgmTitle.Text;
			}
			this.Title = this.lblPgmTitle.Text;
			
			this.InitPage();

			this.GetList();
			this.pnlInfo.Visible = true;
		}
	}

	private void InitPage() {
		this.grdStageLevelCountReport.DataSourceID = string.Empty;
		this.pnlKey.DataBind();
		this.pnlInfo.Visible = false;
		
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		using (StageLevelCountReport oStageLevelCountReport = new StageLevelCountReport()) {
			lblStageNm.Text = oStageLevelCountReport.GetStageNm(this.lstSeekSiteCd.SelectedValue,this.StageSeq,this.SexCd);
		}

		using (StageLevelCountReport oStageLevelCountReport = new StageLevelCountReport()) {
			lblStageClearCount.Text = oStageLevelCountReport.GetStageClearCount(this.lstSeekSiteCd.SelectedValue,this.StageSeq);
		}
	}

	private void GetList() {
		this.grdStageLevelCountReport.DataSourceID = "dsStageLevelCountReport";
		this.grdStageLevelCountReport.DataBind();
	}

	protected void btnCSV_Click(object sender,EventArgs e) {

		if (grdStageLevelCountReport.Rows.Count == 0) {
			return;
		}

		DataTable oCsvData;
		using (StageLevelCountReport oStageLevelCountReport = new StageLevelCountReport()) {
			using (DataSet oDataSet = oStageLevelCountReport.GetList(
				this.lstSeekSiteCd.SelectedValue,
				this.SexCd,
				this.StageSeq)) {

				if (oDataSet.Tables[0].Rows.Count > 0) {
					oCsvData = oDataSet.Tables[0];
				} else {
					return;
				}
			}
		}

		//ヘッダ作成
		string sHeader = "ｽﾃｰｼﾞ名,人数";

		//CSV出力 
		Response.ContentType = "application/download";
		Response.AppendHeader("Content-Disposition","attachment;filename=StageLevelCountReport.csv");
		Response.ContentEncoding = System.Text.Encoding.GetEncoding("Shift_JIS");

		Response.Write(sHeader + "\r\n");
		foreach (DataRow oCsvRow in oCsvData.Rows) {
			string sData =
				SetCsvString(oCsvRow["STAGE_NM"].ToString()) + "," +
				SetCsvString(oCsvRow["STAGE_COUNT"].ToString());

			Response.Write(sData + "\r\n");
		}
		Response.End();
	}

	private string SetCsvString(string pData) {
		if (pData.IndexOf('"') > -1 ||
			pData.IndexOf(',') > -1 ||
			pData.IndexOf('\r') > -1 ||
			pData.IndexOf('\n') > -1 ||
			pData.StartsWith(" ") || pData.StartsWith("\t") ||
			pData.EndsWith(" ") || pData.EndsWith("\t")) {
			if (pData.IndexOf('"') > -1) {
				//"を""とする
				pData = pData.Replace("\"","\"\"");
			}
			if (pData.IndexOf('\r') > -1 || pData.IndexOf('\n') > -1) {
				//改行しない 
				pData = pData.Replace("\r","");
				pData = pData.Replace("\n","");
			}
			pData = "\"" + pData + "\"";
		}
		return pData;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		this.GetList();
		this.pnlInfo.Visible = true;
	}
}