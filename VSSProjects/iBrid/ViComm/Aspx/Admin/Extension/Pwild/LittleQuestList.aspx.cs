﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 小クエスト一覧

--	Progaram ID		: LittleQuestList
--
--  Creation Date	: 2012.07.06
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.IO;
using ViComm.Extension.Pwild;

public partial class Extension_Pwild_LittleQuestList:System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string QuestSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["QuestSeq"]);
		}
		set {
			this.ViewState["QuestSeq"] = value;
		}
	}

	private string LevelQuestSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["LevelQuestSeq"]);
		}
		set {
			this.ViewState["LevelQuestSeq"] = value;
		}
	}

	private string LittleQuestSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["LittleQuestSeq"]);
		}
		set {
			this.ViewState["LittleQuestSeq"] = value;
		}
	}

	private string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		set {
			this.ViewState["SexCd"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();
			this.QuestSeq = iBridUtil.GetStringValue(this.Request.QueryString["questseq"]);
			this.LevelQuestSeq = iBridUtil.GetStringValue(this.Request.QueryString["levelquestseq"]);
			this.SexCd = iBridUtil.GetStringValue(this.Request.QueryString["sexcd"]);
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
		this.LittleQuestSeq = string.Empty;
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.LittleQuestSeq = string.Empty;
		this.ClearFileds();
		this.pnlKey.Enabled = false;
		this.btnDelete.Enabled = false;
		this.pnlLittleQuestInfo.Visible = true;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		if (!this.IsCorrectInput()) {
			return;
		}

		this.UpdateData(false);
		this.pnlLittleQuestInfo.Visible = false;
		this.pnlKey.Enabled = true;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		this.UpdateData(true);
		this.pnlLittleQuestInfo.Visible = false;
	}

	protected void btnBack_Click(object sender,EventArgs e) {
		string sBackPath = string.Empty;

		sBackPath = string.Format("LevelQuestList.aspx?questseq={0}&levelquestseq={1}&sexcd={2}",this.QuestSeq,this.LevelQuestSeq,this.SexCd);

		this.Response.Redirect(string.Format("~/Extension/Pwild/{0}",sBackPath));
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.ClearFileds();
		this.pnlKey.Enabled = true;
		this.pnlLittleQuestInfo.Visible = false;
	}

	protected void dsLittleQuest_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	protected void lnkLittleQuestId_Command(object sender,CommandEventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.LittleQuestSeq = iBridUtil.GetStringValue(e.CommandArgument);
		this.GetData();
		this.btnDelete.Enabled = true;
		this.pnlKey.Enabled = false;
		this.pnlLittleQuestInfo.Visible = true;
	}

	private void InitPage() {
		this.pnlInfo.Visible = true;
		this.pnlLittleQuestInfo.Visible = false;

		this.LittleQuestSeq = string.Empty;
		this.pnlKey.Enabled = true;
		this.btnDelete.Enabled = true;
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void ClearFileds() {
		this.txtLevelQuestSubSeq.Text = string.Empty;
		this.txtLittleQuestNm.Text = string.Empty;
		this.txtRemarks.Text = string.Empty;
		this.txtTimeLimitD.Text = string.Empty;
		this.txtTimeLimitH.Text = string.Empty;
		this.txtTimeLimitM.Text = string.Empty;
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.LittleQuestSeq = string.Empty;

		this.grdLittleQuest.PageIndex = 0;
		this.grdLittleQuest.DataSourceID = "dsLittleQuest";
		this.grdLittleQuest.DataBind();

		this.pnlCount.DataBind();
	}

	private void GetData() {
		this.ClearFileds();

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("LITTLE_QUEST_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pQUEST_SEQ",DbSession.DbType.VARCHAR2,this.QuestSeq);
			oDbSession.ProcedureInParm("pLEVEL_QUEST_SEQ",DbSession.DbType.VARCHAR2,this.LevelQuestSeq);
			oDbSession.ProcedureInParm("pLITTLE_QUEST_SEQ",DbSession.DbType.VARCHAR2,this.LittleQuestSeq);
			oDbSession.ProcedureOutParm("pLEVEL_QUEST_SUB_SEQ",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pLITTLE_QUEST_NM",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREMARKS",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTIME_LIMIT_D",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTIME_LIMIT_H",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTIME_LIMIT_M",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.txtLevelQuestSubSeq.Text = oDbSession.GetStringValue("pLEVEL_QUEST_SUB_SEQ");
			this.txtLittleQuestNm.Text = oDbSession.GetStringValue("pLITTLE_QUEST_NM");
			this.txtRemarks.Text = oDbSession.GetStringValue("pREMARKS");
			this.txtTimeLimitD.Text = oDbSession.GetStringValue("pTIME_LIMIT_D");
			this.txtTimeLimitH.Text = oDbSession.GetStringValue("pTIME_LIMIT_H");
			this.txtTimeLimitM.Text = oDbSession.GetStringValue("pTIME_LIMIT_M");
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
		}
	}

	protected string GetPublishFlagMark(object pPublishFlag) {
		string sPublishFlag = iBridUtil.GetStringValue(pPublishFlag);

		return ViCommConst.FLAG_ON_STR.Equals(sPublishFlag) ? "公開中" : "非公開中";
	}

	private void UpdateData(bool pDelFlag) {

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("LITTLE_QUEST_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureBothParm("pQUEST_SEQ",DbSession.DbType.VARCHAR2,this.QuestSeq);
			oDbSession.ProcedureBothParm("pLEVEL_QUEST_SEQ",DbSession.DbType.VARCHAR2,this.LevelQuestSeq);
			oDbSession.ProcedureBothParm("pLITTLE_QUEST_SEQ",DbSession.DbType.VARCHAR2,this.LittleQuestSeq);
			oDbSession.ProcedureInParm("pLEVEL_QUEST_SUB_SEQ",DbSession.DbType.VARCHAR2,this.txtLevelQuestSubSeq.Text);
			oDbSession.ProcedureInParm("pLITTLE_QUEST_NM",DbSession.DbType.VARCHAR2,this.txtLittleQuestNm.Text);
			oDbSession.ProcedureInParm("pREMARKS",DbSession.DbType.VARCHAR2,this.txtRemarks.Text);
			oDbSession.ProcedureInParm("pTIME_LIMIT_D",DbSession.DbType.VARCHAR2,this.txtTimeLimitD.Text);
			oDbSession.ProcedureInParm("pTIME_LIMIT_H",DbSession.DbType.VARCHAR2,this.txtTimeLimitH.Text);
			oDbSession.ProcedureInParm("pTIME_LIMIT_M",DbSession.DbType.VARCHAR2,this.txtTimeLimitM.Text);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.ClearFileds();
		this.GetList();
	}

	private bool IsCorrectInput() {
		this.lblErrMsgQuestLevel.Visible = false;

		bool bResult = true;

		if (int.Parse(this.txtLevelQuestSubSeq.Text) == 0 || int.Parse(this.txtLevelQuestSubSeq.Text) > 5) {
			this.lblErrMsgQuestLevel.Text = "小クエスト番号には1～5の数字を入力してください。";
			this.lblErrMsgQuestLevel.Visible = true;
			bResult = false;
		} else {
			using (LittleQuest oLittleQuest = new LittleQuest()) {
				if (oLittleQuest.IsDupulicateLevelQuestSubSeq(this.SiteCd,this.QuestSeq,this.LevelQuestSeq,this.LittleQuestSeq,this.txtLevelQuestSubSeq.Text)) {
					this.lblErrMsgQuestLevel.Text = "小クエスト番号が重複しています。";
					this.lblErrMsgQuestLevel.Visible = true;
					bResult = false;
				}
			}
		}

		if ((string.IsNullOrEmpty(txtTimeLimitD.Text) || int.Parse(txtTimeLimitD.Text) == 0) &&
			(string.IsNullOrEmpty(txtTimeLimitH.Text) || int.Parse(txtTimeLimitH.Text) == 0) &&
			(string.IsNullOrEmpty(txtTimeLimitM.Text) || int.Parse(txtTimeLimitM.Text) == 0)) {
			this.lblErrorMessageTimeLimit.Text = "クエスト制限時間を入力してください。";
			lblErrorMessageTimeLimit.Visible = true;
			bResult = false;
		}

		return bResult;
	}
}
