﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="MailRequestLogList.aspx.cs" Inherits="Extension_Pwild_MailRequestLogList" Title="メールおねだり一覧"
    ValidateRequest="false" %>
<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="iBridCommLib" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="メールおねだり一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 800px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                会員ﾛｸﾞｲﾝID
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtManLoginId" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                出演者ﾛｸﾞｲﾝID
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtCastLoginId" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                送信日時From
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstFromYYYY" runat="server" Width="60px">
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstFromMM" runat="server" Width="40px">
                                </asp:DropDownList>月
                                <asp:DropDownList ID="lstFromDD" runat="server" Width="40px">
                                </asp:DropDownList>日
                                <asp:DropDownList ID="lstFromHH" runat="server" Width="40px">
                                </asp:DropDownList>時
                                <asp:DropDownList ID="lstFromMI" runat="server" Width="40px">
                                </asp:DropDownList>分
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle2">
                                送信日時To
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstToYYYY" runat="server" Width="60px">
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstToMM" runat="server" Width="40px">
                                </asp:DropDownList>月
                                <asp:DropDownList ID="lstToDD" runat="server" Width="40px">
                                </asp:DropDownList>日
                                <asp:DropDownList ID="lstToHH" runat="server" Width="40px">
                                </asp:DropDownList>時
                                <asp:DropDownList ID="lstToMI" runat="server" Width="40px">
                                </asp:DropDownList>分
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                ﾒｰﾙ送信
                            </td>
                            <td class="tdDataStyle">
								<asp:RadioButtonList ID="rdoRxMailFlag" RepeatDirection="horizontal" runat="server">
									<asp:ListItem Text="すべて" Value=""></asp:ListItem>
									<asp:ListItem Text="送信済" Value="1"></asp:ListItem>
									<asp:ListItem Text="未送信" Value="0"></asp:ListItem>
								</asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" />
                <asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" CausesValidation="True" OnClick="btnCSV_Click" />
                <asp:CustomValidator ID="vdcFromTo" runat="server" ErrorMessage="" OnServerValidate="vdcFromTo_ServerValidate"
                    ValidationGroup="Key"></asp:CustomValidator>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[記事一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdMailRequestLog.PageIndex + 1 %>
                        of
                        <%= grdMailRequestLog.PageCount %>
                    </a>
                </asp:Panel>
				<br /><br />
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="443px">
                    <asp:GridView ID="grdMailRequestLog" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        DataSourceID="dsMailRequestLog" SkinID="GridViewColor" AllowSorting="true" OnRowDataBound="grdMailRequestLog_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="送信日時日時">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("CREATE_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>'></asp:Label><br />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="会員<br>ﾛｸﾞｲﾝID">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblManLoginId" runat="server" NavigateUrl='<%# string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("MAN_LOGIN_ID")) %>'
                                        Text='<%# Eval("MAN_LOGIN_ID") %>'></asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle Width="120px" HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="会員<br>ﾊﾝﾄﾞﾙ名">
                                <ItemTemplate>
                                    <asp:Label ID="lblManHandleNm" runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("MAN_HANDLE_NM")) %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="120px" HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="出演者<br>ﾛｸﾞｲﾝID">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblCastLoginId" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?site={0}&loginid={1}",Eval("SITE_CD"),Eval("CAST_LOGIN_ID")) %>'
                                        Text='<%# Eval("CAST_LOGIN_ID") %>'></asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle Width="120px" HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="出演者<br>ﾊﾝﾄﾞﾙ名">
                                <ItemTemplate>
                                    <asp:Label ID="lblCastHandleNm" runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("CAST_HANDLE_NM")) %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="120px" HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ﾒｰﾙ<br>送信">
                                <ItemTemplate>
                                    <asp:Label ID="lblRxMail" runat="server" Text='<%# iBridUtil.GetStringValue(Eval("RX_MAIL_FLAG")).Equals(ViCommConst.FLAG_ON_STR) ? "済" : "" %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
            <br />
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsMailRequestLog" runat="server" SelectMethod="GetPageCollection" ConvertNullToDBNull="true" SortParameterName=""
        TypeName="MailRequestLog" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelected="dsMailRequestLog_Selected"
        OnSelecting="dsMailRequestLog_Selecting">
        <SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
</asp:Content>
