<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PresentMailItemList.aspx.cs" Inherits="Extension_Pwild_PresentMailItemList" Title="[Κκ(Gg[§)" %>
<%@ Import Namespace="ViComm" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="[Κκ(Gg[§)"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlSearch">
			<fieldset class="fieldset">
				<legend>[υ]</legend>
				<asp:Panel ID="pnlSeekCondition" runat="server">
					<table border="0" style="width: 800px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle2">
								TCgR[h
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px">
								</asp:DropDownList>
							</td>
						</tr>
					</table>
				</asp:Panel>
				<asp:Button runat="server" ID="btnListSeek" Text="κυ" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Seek" />
				<asp:Button runat="server" ID="btnCreate" Text="ΗΑ" CssClass="seekbutton"  OnClick="btnCreate_Click" ValidationGroup="Create" />
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlInput">
			<fieldset class="fieldset">
				<legend>[έθ]</legend>
				<asp:Label ID="lblErrorMessage" runat="server" Text="" ForeColor="red"></asp:Label>
				<table border="0" style="width: 650px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							ΌΜ
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtPresentMailItemNm" runat="server" Width="200px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							Ώi
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtChargePoint" runat="server" Width="50px" MaxLength="3"></asp:TextBox>pt
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							ρVpt
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtRewardPoint" runat="server" Width="50px" MaxLength="3"></asp:TextBox>pt
						</td>
					</tr>
				</table>
				<asp:Panel ID="pnlCommand" runat="server" Width="660px">
					<asp:Button runat="server" ID="btnUpdate" Text="XV" CssClass="seekbutton" OnClick="btnUpdate_Click" />
					<asp:Button runat="server" ID="btnCancel" Text="LZ" CssClass="seekbutton" OnClick="btnCancel_Click" />
					<asp:Button runat="server" ID="btnDelete" Text="ν" CssClass="delBtnStyle" OnClick="btnDelete_Click" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlList">
			<fieldset>
				<legend>[κ]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">
						Record Count <%# GetRecCount() %>
					</a>
					<br />
					<a class="reccount">
						Current viewing page <%= grdPresentMailItem.PageIndex + 1%> of <%= grdPresentMailItem.PageCount%>
					</a>
				</asp:Panel>
				&nbsp;
				<asp:Panel ID="pnlGrid" runat="server">
					<asp:GridView ID="grdPresentMailItem" DataSourceID="dsPresentMailItem" runat="server" AllowPaging="True" AutoGenerateColumns="False" PageSize="50"
						EnableSortingAndPagingCallbacks="false" SkinID="GridViewColor" AllowSorting="true" OnRowDataBound="grdPresentMailItem_RowDataBound">
						<Columns>
							<asp:TemplateField>
								<ItemTemplate>
									<asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%# Container.DataItemIndex %>' OnCommand="lnkEdit_Command" Text='<%# Eval("PRESENT_MAIL_ITEM_SEQ") %>'></asp:LinkButton>
									<asp:HiddenField ID="hdnPresentMailItemSeq" runat="server" Value='<%# Eval("PRESENT_MAIL_ITEM_SEQ") %>' />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="PRESENT_MAIL_ITEM_NM" HeaderText="ΜίΪΎήέΔΌ">
								<ItemStyle HorizontalAlign="Left" />
							</asp:BoundField>
                            <asp:TemplateField HeaderText="ζ">
                                <ItemTemplate>
                                    <asp:Image ID="imgItem" runat="server" Width="80px" ImageUrl='<%# string.Format("../../Image/{0}/presentmail/{1}.gif",Eval("SITE_CD"),Eval("PRESENT_MAIL_ITEM_SEQ")) %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ζ1">
                                <ItemTemplate>
                                    <asp:Image ID="imgItem1" runat="server" Width="80px" ImageUrl='<%# string.Format("../../Image/{0}/presentmail/{1}_1.gif",Eval("SITE_CD"),Eval("PRESENT_MAIL_ITEM_SEQ")) %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ζ2">
                                <ItemTemplate>
                                    <asp:Image ID="imgItem2" runat="server" Width="80px" ImageUrl='<%# string.Format("../../Image/{0}/presentmail/{1}_2.gif",Eval("SITE_CD"),Eval("PRESENT_MAIL_ITEM_SEQ")) %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ζ3">
                                <ItemTemplate>
                                    <asp:Image ID="imgItem3" runat="server" Width="80px" ImageUrl='<%# string.Format("../../Image/{0}/presentmail/{1}_3.gif",Eval("SITE_CD"),Eval("PRESENT_MAIL_ITEM_SEQ")) %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
							<asp:TemplateField HeaderText="Ώi">
								<ItemTemplate>
									<asp:Label ID="lblChargePoint" runat="server" Text='<%# Eval("CHARGE_POINT") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ρVpt">
								<ItemTemplate>
									<asp:Label ID="lblRewardPoint" runat="server" Text='<%# Eval("REWARD_POINT") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
			<br />
		</asp:Panel>
	</div>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtChargePoint" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtRewardPoint" />
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsPresentMailItem" runat="server" ConvertNullToDBNull="false" EnablePaging="True" OnSelecting="dsPresentMailItem_Selecting" OnSelected="dsPresentMailItem_Selected"
		SelectCountMethod="GetPageCount" SelectMethod="GetPageCollection" SortParameterName="" TypeName="PresentMailItem">
		<SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>

