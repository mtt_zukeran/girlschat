﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 野球拳女性勝利コメント一覧
--	Progaram ID		: YakyukenCommentList
--
--  Creation Date	: 2013.05.06
--  Creater			: Y.Ikemiya
--
**************************************************************************/
using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;

public partial class Extension_Pwild_YakyukenCommentList:System.Web.UI.Page {
	private string recCount = string.Empty;

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			DataBind();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			lstSiteCd.DataSourceID = "";
			grdYakyukenComment.PageIndex = 0;
			DataBind();
		}
	}

	protected void btnAdminCheck_Click(object sender,EventArgs e) {
		List<string> oYakyukenCommentSeqList = new List<string>();

		for (int i = 0;i < this.grdYakyukenComment.Rows.Count;i++) {
			GridViewRow row = this.grdYakyukenComment.Rows[i];
			CheckBox oChk = (CheckBox)row.FindControl("chkAdminCheck");

			if (oChk.Checked) {
				HiddenField hdnYakyukenCommentSeq = row.FindControl("hdnYakyukenCommentSeq") as HiddenField;
				oYakyukenCommentSeqList.Add(hdnYakyukenCommentSeq.Value);
			}
		}

		if (oYakyukenCommentSeqList.Count > 0) {
			using (DbSession oDbSession = new DbSession()) {
				oDbSession.PrepareProcedure("ADMIN_CHECK_YAKYUKEN_COMMENT");
				oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.lstSiteCd.SelectedValue);
				oDbSession.ProcedureInArrayParm("pYAKYUKEN_COMMENT_SEQ",DbSession.DbType.VARCHAR2,oYakyukenCommentSeqList.ToArray());
				oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
				oDbSession.ExecuteProcedure();
			}

			lstSiteCd.DataSourceID = "";
			DataBind();
		}
	}

	protected void lnkDelete_OnCommand(object sender,CommandEventArgs e) {
		string[] arguments = e.CommandArgument.ToString().Split(',');
		string sSiteCd = arguments[0];
		string sYakyukenCommentSeq = arguments[1];
		string sDelFlag = (arguments[2].Equals(ViCommConst.FLAG_ON_STR)) ? ViCommConst.FLAG_OFF_STR : ViCommConst.FLAG_ON_STR;
		string sRevisionNo = arguments[3];

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("DELETE_YAKYUKEN_COMMENT");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,sSiteCd);
			oDbSession.ProcedureInParm("pYAKYUKEN_COMMENT_TYPE",DbSession.DbType.VARCHAR2,sYakyukenCommentSeq);
			oDbSession.ProcedureInParm("pDEL_FLAG",DbSession.DbType.NUMBER,sDelFlag);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,sRevisionNo);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		lstSiteCd.DataSourceID = "";
		DataBind();
	}

	protected void dsYakyukenComment_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		YakyukenComment.SearchCondition oSearchCondition = new YakyukenComment.SearchCondition();
		oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
		oSearchCondition.LoginId = this.txtLoginId.Text.Trim();
		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsYakyukenComment_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void grdYakyukenComment_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			string sDelFlag = DataBinder.Eval(e.Row.DataItem,"DEL_FLAG").ToString();
			string sAdminCheckFlag = DataBinder.Eval(e.Row.DataItem,"ADMIN_CHECK_FLAG").ToString();

			if (sDelFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				e.Row.BackColor = Color.LightGray;
			} else if (sAdminCheckFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
				e.Row.BackColor = Color.LavenderBlush;
			} else {
				e.Row.BackColor = Color.White;
			}
		}
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected string GetClientScript(object pPicUrl) {
		string sRoot = Request.Url.Authority + ConfigurationManager.AppSettings["Root"];
		return string.Format("javascript:win=window.open('http://{1}/ViewPic.html?url={0}','PictureZoom','width=300,height=320,resizable=yes,directories=no,scrollbars=no' , false);win.focus();return false;",pPicUrl,sRoot);
	}
}