/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ﾌﾟﾚｾﾞﾝﾄﾒｰﾙ期間設定一覧
--	Progaram ID		: PresentMailTermList
--  Creation Date	: 2013.12.10
--  Creater			: Y.Ikemiya
**************************************************************************/
using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using ViComm.Extension.Pwild;

public partial class Extension_Pwild_PresentMailTermList:System.Web.UI.Page {
	private string recCount = string.Empty;
	protected string[] YearArray;
	protected string[] MonthArray;
	protected string[] DayArray;
	protected string[] HourArray;
	protected string[] MinuteArray;

	private string PresentMailTermSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["PRESENT_MAIL_TERM_SEQ"]);
		}
		set {
			this.ViewState["PRESENT_MAIL_TERM_SEQ"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["REVISION_NO"]);
		}
		set {
			this.ViewState["REVISION_NO"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);

			if (string.IsNullOrEmpty(sSiteCd)) {
				if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
					lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
				} else {
					lstSiteCd.SelectedIndex = 0;
				}
			} else {
				lstSiteCd.SelectedValue = sSiteCd;
			}

			this.pnlSearch.Enabled = true;
			this.pnlInput.Visible = false;

			lstSiteCd.DataBind();
			pnlGrid.DataBind();
			pnlCount.DataBind();

			SetDateArray();
			pnlInput.DataBind();
		}
	}

	protected void dsPresentMailTerm_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		PresentMailTerm.SearchCondition oSearchCondition = new PresentMailTerm.SearchCondition();
		oSearchCondition.SiteCd = lstSiteCd.SelectedValue;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsPresentMailTerm_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			grdPresentMailTerm.PageIndex = 0;
			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		this.PresentMailTermSeq = string.Empty;
		this.RevisionNo = string.Empty;
		this.ClearFileds();

		this.pnlSearch.Enabled = false;
		this.pnlInput.Visible = true;
		this.btnDelete.Enabled = false;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (CheckInput()) {
			string sResult;
			PresentMailTermMainte(ViCommConst.FLAG_OFF,out sResult);

			if (sResult.Equals(PwViCommConst.PresentMailTermMainteResult.RESULT_OK)) {
				this.pnlSearch.Enabled = true;
				this.pnlInput.Visible = false;
				grdPresentMailTerm.PageIndex = 0;
				pnlGrid.DataBind();
				pnlCount.DataBind();
			} else if (sResult.Equals(PwViCommConst.PresentMailTermMainteResult.RESULT_NG_DATE)) {
				this.lblErrorMessage.Text = "期間が他と重複しています";
			}
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlSearch.Enabled = true;
		this.pnlInput.Visible = false;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		string sResult;
		PresentMailTermMainte(ViCommConst.FLAG_ON,out sResult);

		this.pnlSearch.Enabled = true;
		this.pnlInput.Visible = false;
		grdPresentMailTerm.PageIndex = 0;
		pnlGrid.DataBind();
		pnlCount.DataBind();
	}

	protected void lnkEdit_Command(object sender,CommandEventArgs e) {
		int iIndex = int.Parse(iBridUtil.GetStringValue(e.CommandArgument));
		GridViewRow oRow = this.grdPresentMailTerm.Rows[iIndex];
		HiddenField oHiddenField = oRow.FindControl("hdnPresentMailTermSeq") as HiddenField;
		this.PresentMailTermSeq = oHiddenField.Value;

		this.ClearFileds();
		this.PresentMailTermGet();

		this.pnlSearch.Enabled = false;
		this.pnlInput.Visible = true;
		this.btnDelete.Enabled = true;
	}

	private void SetDateArray() {
		this.YearArray = new string[] { DateTime.Today.AddYears(-1).ToString("yyyy"),DateTime.Today.ToString("yyyy"),DateTime.Today.AddYears(1).ToString("yyyy"),DateTime.Today.AddYears(2).ToString("yyyy") };
		this.MonthArray = new string[] { "01","02","03","04","05","06","07","08","09","10","11","12" };
		this.DayArray = new string[] { "01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31" };
		this.HourArray = new string[] { "00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23" };

		List<string> oMinuteList = new List<string>();

		for (int i = 0;i < 60;i++) {
			oMinuteList.Add(string.Format("{0:D2}",i));
		}

		this.MinuteArray = oMinuteList.ToArray();
	}

	private bool CheckInput() {
		DateTime dtTermStartDate = new DateTime();
		DateTime dtTermEndDate = new DateTime();

		if (string.IsNullOrEmpty(this.txtPresentMailTermNm.Text)) {
			this.lblErrorMessage.Text = "名称を入力して下さい";
			return false;
		}

		if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstTermStartDateYYYY.SelectedValue,this.lstTermStartDateMM.SelectedValue,this.lstTermStartDateDD.SelectedValue,this.lstTermStartDateHH.SelectedValue,this.lstTermStartDateMI.SelectedValue),out dtTermStartDate)) {
			this.lblErrorMessage.Text = "期間開始日時が正しくありません";
			return false;
		}

		if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:59",this.lstTermEndDateYYYY.SelectedValue,this.lstTermEndDateMM.SelectedValue,this.lstTermEndDateDD.SelectedValue,this.lstTermEndDateHH.SelectedValue,this.lstTermEndDateMI.SelectedValue),out dtTermEndDate)) {
			this.lblErrorMessage.Text = "期間終了日時が正しくありません";
			return false;
		}

		if (dtTermStartDate > dtTermEndDate) {
			this.lblErrorMessage.Text = "期間の大小関係が正しくありません";
			return false;
		}

		return true;
	}

	private void ClearFileds() {
		this.lblErrorMessage.Text = string.Empty;
		this.txtPresentMailTermNm.Text = string.Empty;

		this.lstTermStartDateYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstTermStartDateMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstTermStartDateDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstTermStartDateHH.SelectedValue = "00";
		this.lstTermStartDateMI.SelectedValue = "00";

		this.lstTermEndDateYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstTermEndDateMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstTermEndDateDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstTermEndDateHH.SelectedValue = "23";
		this.lstTermEndDateMI.SelectedValue = "59";
	}

	private void PresentMailTermGet() {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("PRESENT_MAIL_TERM_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pPRESENT_MAIL_TERM_SEQ",DbSession.DbType.VARCHAR2,this.PresentMailTermSeq);
			oDbSession.ProcedureOutParm("pTERM_START_DATE",DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pTERM_END_DATE",DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pPRESENT_MAIL_TERM_NM",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();

			this.txtPresentMailTermNm.Text = oDbSession.GetStringValue("pPRESENT_MAIL_TERM_NM");
			DateTime dtTermStartDate = (DateTime)oDbSession.GetDateTimeValue("pTERM_START_DATE");
			DateTime dtTermEndDate = (DateTime)oDbSession.GetDateTimeValue("pTERM_END_DATE");

			if (dtTermStartDate != null) {
				this.lstTermStartDateYYYY.SelectedValue = dtTermStartDate.ToString("yyyy");
				this.lstTermStartDateMM.SelectedValue = dtTermStartDate.ToString("MM");
				this.lstTermStartDateDD.SelectedValue = dtTermStartDate.ToString("dd");
				this.lstTermStartDateHH.SelectedValue = dtTermStartDate.ToString("HH");
				this.lstTermStartDateMI.SelectedValue = dtTermStartDate.ToString("mm");
			}

			if (dtTermEndDate != null) {
				this.lstTermEndDateYYYY.SelectedValue = dtTermEndDate.ToString("yyyy");
				this.lstTermEndDateMM.SelectedValue = dtTermEndDate.ToString("MM");
				this.lstTermEndDateDD.SelectedValue = dtTermEndDate.ToString("dd");
				this.lstTermEndDateHH.SelectedValue = dtTermEndDate.ToString("HH");
				this.lstTermEndDateMI.SelectedValue = dtTermEndDate.ToString("mm");
			}
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
		}
	}

	private void PresentMailTermMainte(int pDeleteFlag,out string pResult) {
		DateTime dtTermStartDate = DateTime.Parse(string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstTermStartDateYYYY.SelectedValue,this.lstTermStartDateMM.SelectedValue,this.lstTermStartDateDD.SelectedValue,this.lstTermStartDateHH.SelectedValue,this.lstTermStartDateMI.SelectedValue));
		DateTime dtTermEndDate = DateTime.Parse(string.Format("{0}/{1}/{2} {3}:{4}:59",this.lstTermEndDateYYYY.SelectedValue,this.lstTermEndDateMM.SelectedValue,this.lstTermEndDateDD.SelectedValue,this.lstTermEndDateHH.SelectedValue,this.lstTermEndDateMI.SelectedValue));

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("PRESENT_MAIL_TERM_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pPRESENT_MAIL_TERM_SEQ",DbSession.DbType.NUMBER,this.PresentMailTermSeq);
			oDbSession.ProcedureInParm("pTERM_START_DATE",DbSession.DbType.DATE,dtTermStartDate);
			oDbSession.ProcedureInParm("pTERM_END_DATE",DbSession.DbType.DATE,dtTermEndDate);
			oDbSession.ProcedureInParm("pPRESENT_MAIL_TERM_NM",DbSession.DbType.VARCHAR2,this.txtPresentMailTermNm.Text);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDeleteFlag);
			oDbSession.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
			pResult = oDbSession.GetStringValue("pRESULT");
		}
	}
}
