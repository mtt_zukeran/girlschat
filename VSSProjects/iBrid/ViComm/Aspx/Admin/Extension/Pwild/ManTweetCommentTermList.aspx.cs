﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 会員つぶやきコメントランキング
--	Progaram ID		: ManTweetCommentTermList
--
--  Creation Date	: 2013.03.26
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Extension_Pwild_ManTweetCommentTermList:System.Web.UI.Page {
	private string recCount = string.Empty;
	protected string[] YearArray;
	protected static readonly string[] MonthArray = new string[] { "01","02","03","04","05","06","07","08","09","10","11","12" };
	protected static readonly string[] DayArray = new string[] { "01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31" };
	protected static readonly string[] HourArray = new string[] { "00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23" };
	protected string[] MinuteArray;
    protected string[] DispRankSeqs;
    protected string SexCD = ViCommConst.OPERATOR;
    //protected string[] DispRankSeqArray;

	private string TermSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["TERM_SEQ"]);
		}
		set {
			this.ViewState["TERM_SEQ"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["REVISION_NO"]);
		}
		set {
			this.ViewState["REVISION_NO"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!IsPostBack) {
			this.YearArray = new string[] { DateTime.Today.AddYears(-1).ToString("yyyy"),DateTime.Today.ToString("yyyy"),DateTime.Today.AddYears(1).ToString("yyyy"),DateTime.Today.AddYears(2).ToString("yyyy") };
			this.CreateMinuteArray();
			this.pnlKey.Enabled = true;
			this.pnlInfo.Visible = true;
			this.pnlItemInfo.Visible = false;
			this.TermSeq = string.Empty;
			this.lstSiteCd.DataBind();
			this.pnlItemInfo.DataBind();

            ////2017.04.10 yogi ランキング表示順機能追加
            //ManTweetCommentDispRank ob = new ManTweetCommentDispRank();
            //DispRankSeqArray = ob.GetDispRankSeqAll(SexCD);
            //lstDispRankSeq.DataSource = DispRankSeqArray;
            //lstDispRankSeq.DataBind();

			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
				this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}

			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		this.ClearFileds();
		this.pnlKey.Enabled = false;
		this.btnDelete.Enabled = false;
		this.pnlItemInfo.Visible = true;
		this.TermSeq = string.Empty;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlItemInfo.Visible = false;
		this.pnlKey.Enabled = true;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (CheckInput()) {
			this.UpdateData(ViCommConst.FLAG_OFF);
			this.ClearFileds();
			this.GetList();
			this.pnlItemInfo.Visible = false;
			this.pnlKey.Enabled = true;
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		this.UpdateData(ViCommConst.FLAG_ON);
		this.GetList();
		this.pnlItemInfo.Visible = false;
		this.pnlKey.Enabled = true;
	}

	protected void lnkEdit_Command(object sender,CommandEventArgs e) {
		int iIndex = int.Parse(iBridUtil.GetStringValue(e.CommandArgument));

		GridViewRow oRow = this.grdManTweetCommentTerm.Rows[iIndex];
		HiddenField oHiddenField = oRow.FindControl("hdnTermSeq") as HiddenField;

		this.TermSeq = oHiddenField.Value;
		this.pnlKey.Enabled = false;
		this.pnlItemInfo.Visible = true;
		this.btnDelete.Enabled = true;
		this.ClearFileds();
		this.GetData();
	}

	private bool CheckInput() {
		this.lblErrorMessage.Visible = false;
		DateTime dtFrom = new DateTime();
		DateTime dtTo = new DateTime();

		if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstFromYYYY.Text,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue,this.lstFromHH.SelectedValue,this.lstFromMI.SelectedValue),out dtFrom)) {
			this.lblErrorMessage.Text = "開始日時を正しく入力してください";
			this.lblErrorMessage.Visible = true;
			return false;
		}

		if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:59",this.lstToYYYY.Text,this.lstToMM.SelectedValue,this.lstToDD.SelectedValue,this.lstToHH.SelectedValue,this.lstToMI.SelectedValue),out dtTo)) {
			this.lblErrorMessage.Text = "終了日時を正しく入力してください";
			this.lblErrorMessage.Visible = true;
			return false;
		}

		if (dtFrom > dtTo) {
			this.lblErrorMessage.Text = "日時の大小関係を正しく入力してください";
			this.lblErrorMessage.Visible = true;
			return false;
		}

		using (ManTweetCommentTerm oManTweetCommentTerm = new ManTweetCommentTerm()) {
			if (oManTweetCommentTerm.IsDuplicateDate(this.lstSiteCd.SelectedValue,this.TermSeq,dtFrom,dtTo)) {
				this.lblErrorMessage.Text = "期間が他の設定と重複しています";
				this.lblErrorMessage.Visible = true;
				return false;
			}
		}

        //2017.04.10 yogi ランキング表示順位を追加
        if (string.IsNullOrEmpty(txtDispRankSeq.Text))
        {
            this.lblErrorMessage.Text = "ランキング表示順位を入力してください";
            this.lblErrorMessage.Visible = true;
            return false;
        }
        using (ManTweetCommentDispRank obManTweetCommentDispRank = new ManTweetCommentDispRank())
        {
            if (!obManTweetCommentDispRank.IsExistDispRankSeq(txtDispRankSeq.Text, SexCD))
            {
                this.lblErrorMessage.Text = "表示順位Seqが存在しません";
                this.lblErrorMessage.Visible = true;
                return false;
            }
        }
        //if (string.IsNullOrEmpty(lstDispRankSeq.Text))
        //{
        //    this.lblErrorMessage.Text = "ランキング表示順位を入力してください";
        //    this.lblErrorMessage.Visible = true;
        //    return false;
        //}

		return true;
	}

	private void UpdateData(int pDeleteFlag) {
		DateTime dtStartDate = DateTime.Parse(string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstFromYYYY.Text,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue,this.lstFromHH.SelectedValue,this.lstFromMI.SelectedValue));
		DateTime dtEndDate = DateTime.Parse(string.Format("{0}/{1}/{2} {3}:{4}:59",this.lstToYYYY.Text,this.lstToMM.SelectedValue,this.lstToDD.SelectedValue,this.lstToHH.SelectedValue,this.lstToMI.SelectedValue));

        using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("MAN_TWEET_COMMENT_TERM_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pTERM_SEQ",DbSession.DbType.VARCHAR2,this.TermSeq);
			oDbSession.ProcedureInParm("pSTART_DATE",DbSession.DbType.DATE,dtStartDate);
			oDbSession.ProcedureInParm("pEND_DATE",DbSession.DbType.DATE,dtEndDate);
            oDbSession.ProcedureInParm("pDISP_RANK_SEQ", DbSession.DbType.NUMBER, this.txtDispRankSeq.Text);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDeleteFlag);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
		}
	}

	protected void dsManTweetCommentTerm_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		ManTweetCommentTerm.SearchCondition oSearchCondition = new ManTweetCommentTerm.SearchCondition();
		oSearchCondition.SiteCd = lstSiteCd.SelectedValue;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsManTweetCommentTerm_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	private void GetList() {
		this.grdManTweetCommentTerm.PageIndex = 0;
		this.grdManTweetCommentTerm.DataSourceID = "dsManTweetCommentTerm";
		this.grdManTweetCommentTerm.DataBind();
		this.pnlCount.DataBind();
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetData() {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("MAN_TWEET_COMMENT_TERM_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pTERM_SEQ",DbSession.DbType.VARCHAR2,this.TermSeq);
			oDbSession.ProcedureOutParm("pSTART_DATE",DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pEND_DATE",DbSession.DbType.DATE);
            oDbSession.ProcedureOutParm("pDISP_RANK_SEQ", DbSession.DbType.NUMBER);   //2017.04.10 yogi
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();

			DateTime dtStartDate = (DateTime)oDbSession.GetDateTimeValue("pSTART_DATE");
			DateTime dtEndDate = (DateTime)oDbSession.GetDateTimeValue("pEND_DATE");
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
            this.txtDispRankSeq.Text = oDbSession.GetStringValue("pDISP_RANK_SEQ");
            //lstDispRankSeq.SelectedValue = oDbSession.GetStringValue("pDISP_RANK_SEQ");

			if (dtStartDate != null) {
				this.lstFromYYYY.SelectedValue = dtStartDate.ToString("yyyy");
				this.lstFromMM.SelectedValue = dtStartDate.ToString("MM");
				this.lstFromDD.SelectedValue = dtStartDate.ToString("dd");
				this.lstFromHH.SelectedValue = dtStartDate.ToString("HH");
				this.lstFromMI.SelectedValue = dtStartDate.ToString("mm");
			}

			if (dtEndDate != null) {
				this.lstToYYYY.SelectedValue = dtEndDate.ToString("yyyy");
				this.lstToMM.SelectedValue = dtEndDate.ToString("MM");
				this.lstToDD.SelectedValue = dtEndDate.ToString("dd");
				this.lstToHH.SelectedValue = dtEndDate.ToString("HH");
				this.lstToMI.SelectedValue = dtEndDate.ToString("mm");
			}
		}
	}

	private void ClearFileds() {
		this.lblErrorMessage.Visible = false;

		this.lstFromYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstFromMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstFromDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstFromHH.SelectedValue = "17";
		this.lstFromMI.SelectedValue = "00";

		this.lstToYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstToMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstToDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstToHH.SelectedValue = "16";
		this.lstToMI.SelectedValue = "59";

        this.txtDispRankSeq.Text = string.Empty;
	}

	private void CreateMinuteArray() {
		List<string> oMinuteList = new List<string>();

		for (int i = 0;i < 60;i++) {
			oMinuteList.Add(string.Format("{0:D2}",i));
		}

		MinuteArray = oMinuteList.ToArray();
	}
}
