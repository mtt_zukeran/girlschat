﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="EventAreaList.aspx.cs" Inherits="Extension_Pwild_EventAreaList" Title="イベントエリア一覧" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="イベントエリア一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 600px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                都道府県
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSeekPrefectureCd" runat="server" DataSourceID="dsPrefecture" DataTextField="CODE_NM" DataValueField="CODE" OnDataBound="lst_DataBound">
								</asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle2">
                                エリア名
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtSeekEventAreaNm" runat="server" Width="400px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="True" />
                <asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton" ValidationGroup="Create"
                    OnClick="btnCreate_Click" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" Visible="false" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlEventAreaInfo">
            <fieldset class="fieldset">
                <legend>[エリア内容]</legend>
                <fieldset class="fieldset-inner">
                    <table border="0" class="tableStyle" style="width: 600px">
                        <tr>
                            <td class="tdHeaderStyle">
                                エリアCD
                            </td>
                            <td class="tdDataStyle">
								<asp:Label ID="lblEventAreaCd" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                都道府県
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstPrefectureCd" runat="server" DataSourceID="dsPrefecture" DataTextField="CODE_NM" DataValueField="CODE" OnDataBound="lst_DataBound">
								</asp:DropDownList>
								
								<% // 必須チェック %>
								<asp:RequiredFieldValidator ID="vdrPrefectureCd" runat="server" ErrorMessage="都道府県を選択してください。"
								    ControlToValidate="lstPrefectureCd" ValidationGroup="Update">*</asp:RequiredFieldValidator>
								<ajaxToolkit:ValidatorCalloutExtender ID="vdePrefectureCd" runat="Server"
								    TargetControlID="vdrPrefectureCd" HighlightCssClass="validatorCallout" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle2">
                                エリア名
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtEventAreaNm" runat="server" MaxLength="120" Width="400px"></asp:TextBox>
                                <% // 必須チェック %>
                                <asp:RequiredFieldValidator ID="vdrEventAreaNm" runat="server" ErrorMessage="エリア名を入力して下さい。"
                                    ControlToValidate="txtEventAreaNm" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server"
                                    TargetControlID="vdrEventAreaNm" HighlightCssClass="validatorCallout" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <asp:Panel ID="pnlCommand" runat="server">
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Update"
                        OnClick="btnUpdate_Click" />
                    <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" ValidationGroup="Delete"
                        OnClick="btnDelete_Click" />
                    <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" CausesValidation="False"
                        Visible="true" OnClick="btnCancel_Click" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[イベント一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdEventArea.PageIndex + 1 %>
                        of
                        <%= grdEventArea.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="600px">
                    <asp:GridView ID="grdEventArea" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        PageSize="30" EnableSortingAndPagingCallbacks="false" DataSourceID="dsEvent"
                        SkinID="GridViewColor" Font-Size="small" AllowSorting="true">
                        <Columns>
                            <asp:TemplateField HeaderText="エリアCD">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEventAreaCd" runat="server" CommandArgument='<%# Eval("EVENT_AREA_CD") %>'
                                        Text='<%# Eval("EVENT_AREA_CD") %>' OnCommand="lnkEventAreaCd_Command"></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="PREFECTURE_NM" HeaderText="都道府県">
                                <ItemStyle HorizontalAlign="left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="エリア名">
								<ItemTemplate>
									<asp:Label ID="lblEventAreaNm" runat="server" Text='<%# Eval("EVENT_AREA_NM") %>'></asp:Label>
								</ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsEventArea" runat="server" SelectMethod="GetPageCollection"
        ConvertNullToDBNull="true" SortParameterName="" TypeName="EventArea" SelectCountMethod="GetPageCount"
        EnablePaging="True" OnSelected="dsEventArea_Selected" OnSelecting="dsEventArea_Selecting">
        <SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsPrefecture" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="67" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="イベントエリア情報を更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete"
        ConfirmText="イベントエリア情報を削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>
