﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ManTweetCommentCntList.aspx.cs" Inherits="Extension_Pwild_ManTweetCommentCntList" Title="つぶやきコメントランキング一覧"
    ValidateRequest="false" %>

<%@ Import Namespace="System" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="つぶやきコメントランキング一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <fieldset class="fieldset">
            <legend>[検索条件]</legend>
            <asp:Panel runat="server" ID="pnlKey">
                <table border="0" style="width: 820px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            サイト
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" Enabled="false"
                                DataValueField="SITE_CD" Width="180px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            集計期間
                        </td>
                        <td class="tdDataStyle" style="width: 300px">
                            <asp:Label ID="lblDate" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" />
                <asp:Button runat="server" ID="btnBack" Text="戻る" CssClass="seekbutton" OnClick="btnBack_Click" />
            </asp:Panel>
        </fieldset>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[ランキング]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%#GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%=grdManTweetCommentCnt.PageIndex + 1%>
                        of
                        <%=grdManTweetCommentCnt.PageCount%>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="500px">
                    <asp:GridView ID="grdManTweetCommentCnt" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        EnableViewState="true" DataSourceID="dsManTweetCommentCnt" AllowSorting="True" SkinID="GridViewColor"
                        OnRowDataBound="grdManTweetCommentCnt_RowDataBound">
                        <Columns>
                            <asp:BoundField DataField="RANK_NO" HeaderText="順位" ItemStyle-HorizontalAlign="Right" />
                            <asp:TemplateField HeaderText="ログインID">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# GetViewUrl(Eval("LOGIN_ID")) %>'
                                        Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="HANDLE_NM" HeaderText="ハンドル名" />
                            <asp:BoundField DataField="COMMENT_COUNT" HeaderText="コメント数" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="REWARD_POINT" HeaderText="報酬ﾎﾟｲﾝﾄ" ItemStyle-HorizontalAlign="Right" />
                            <asp:TemplateField HeaderText="報酬付与">
                                <ItemTemplate>
                                        <asp:LinkButton ID="lnkAddReward" runat="server" Text='<%# Eval("ADD_REWARD_STATUS") %>' OnCommand="lnkAddBonusPointTweetComment_DataBinding" OnClientClick="return confirm('報酬を付与しますか？');"
                                            CommandArgument='<%# string.Format("{0},{1},{2},{3},{4}",Eval("SITE_CD"),Eval("CAST_USER_SEQ"),Eval("CAST_CHAR_NO"),Eval("TERM_SEQ"),Eval("RANK_NO")) %>' Visible="false"
                                            OnDataBinding="lnkAddRewardStatus_DataBinding"></asp:LinkButton>
									    <asp:Label Text='<%# Eval("ADD_REWARD_STATUS") %>' runat="server" Visible="false" OnDataBinding="lnkAddRewardStatus_DataBinding" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="最終コメント日">
								<ItemTemplate>
									<asp:Label ID="lblLastCommentDate" runat="server" Text='<%# Eval("LAST_COMMENT_DATE", "{0:yyyy/MM/dd HH:mm}") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <br />
    </div>
    <asp:ObjectDataSource ID="dsManTweetCommentCnt" runat="server" ConvertNullToDBNull="false" EnablePaging="True" OnSelecting="dsManTweetCommentCnt_Selecting" OnSelected="dsManTweetCommentCnt_Selected"
		SelectCountMethod="GetPageCount" SelectMethod="GetPageCollection" SortParameterName="" TypeName="ManTweetCommentCnt">
		<SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site">
    </asp:ObjectDataSource>
</asp:Content>
