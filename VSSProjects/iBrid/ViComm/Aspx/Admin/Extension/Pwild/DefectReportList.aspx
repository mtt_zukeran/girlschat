﻿<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="ViComm.Extension.Pwild" %>

<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DefectReportList.aspx.cs" Inherits="Extension_Pwild_DefectReportList" Title="不具合報告一覧" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="不具合報告一覧"></asp:Label>
</asp:Content>

<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" Runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[不具合報告編集]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					<asp:LinkButton ID="lnkPictographSample" runat="server" OnClick="lnkPictographSample_Click">
						絵文字一覧
					</asp:LinkButton>
					<table border="0" style="width: 820px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle">
								SEQ
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblDefectReportSeq" runat="server"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								投稿者
							</td>
							<td class="tdDataStyle">
								<asp:HyperLink ID="lnkLoginId" runat="server"></asp:HyperLink>
								<br />
								<asp:Label ID="lblHandleNm" runat="server"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								タイトル
							</td>
							<td class="tdDataStyle">
								$NO_TRANS_START;
								<asp:TextBox ID="txtTitle" runat="server" Width="400px"></asp:TextBox>
								$NO_TRANS_END;
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								タイトルプレビュー
							</td>
							<td class="tdDataStyle" valign="top">
								<asp:Label ID="lblTitle" runat="server"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								カテゴリ
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstCategorySeq" runat="server" DataSourceID="dsCategory" DataTextField="DEFECT_REPORT_CATEGORY_NM" DataValueField="DEFECT_REPORT_CATEGORY_SEQ">
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								対応状況
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstProgressSeq" runat="server" DataSourceID="dsProgress" DataTextField="DEFECT_REPORT_PROGRESS_NM" DataValueField="DEFECT_REPORT_PROGRESS_SEQ">
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								報告内容
							</td>
							<td class="tdDataStyle">
								$NO_TRANS_START;
								<asp:TextBox ID="txtText" runat="server" Width="650px" Rows="5" TextMode="MultiLine"></asp:TextBox>
								$NO_TRANS_END;
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								報告内容プレビュー
							</td>
							<td class="tdDataStyle" valign="top">
								<asp:Label ID="lblText" runat="server"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								管理者コメント
							</td>
							<td class="tdDataStyle">
								$NO_TRANS_START;
								<asp:TextBox ID="txtAdminComment" runat="server" Width="650px" Rows="5" TextMode="MultiLine"></asp:TextBox>
								$NO_TRANS_END;
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								管理者コメントプレビュー
							</td>
							<td class="tdDataStyle" valign="top">
								<asp:Label ID="lblAdminComment" runat="server"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								投稿日時
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblCreateDate" runat="server"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								公開日時
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblPublicDate" runat="server"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								状態
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstPublicStatus" runat="server">
									<asp:ListItem Value="1">申請中</asp:ListItem>
									<asp:ListItem Value="2">採用</asp:ListItem>
									<asp:ListItem Value="3">公開</asp:ListItem>
									<asp:ListItem Value="4">非公開</asp:ListItem>
									<asp:ListItem Value="5">重複</asp:ListItem>
									<asp:ListItem Value="6">移動</asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								不具合報告重複SEQ
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtOverlapDefectReportSeq" runat="server" Width="100px"></asp:TextBox>
								<asp:Label ID="lblErrorMessageOverlapDefectReportSeq" runat="server" Text="" Visible="false" ForeColor="red"></asp:Label>
								<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtOverlapDefectReportSeq" />
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle2">
								移動先カテゴリ
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstRequestCategorySeq" runat="server" DataSourceID="dsRequestCategory" DataTextField="REQUEST_CATEGORY_NM" DataValueField="REQUEST_CATEGORY_SEQ">
								</asp:DropDownList>
								<asp:Label ID="lblErrorMessageRequestCategorySeq" runat="server" Text="" Visible="false" ForeColor="red"></asp:Label>
							</td>
						</tr>
					</table>
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
					<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<fieldset class="fieldset-inner">
				<table border="0" style="width: 1000px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイトコード
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle">
							カテゴリ
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSeekCategorySeq" runat="server" DataSourceID="dsCategory" DataTextField="DEFECT_REPORT_CATEGORY_NM" DataValueField="DEFECT_REPORT_CATEGORY_SEQ">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							投稿者ID
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtSeekLoginId" runat="server" Width="70px"></asp:TextBox>
						</td>
						<td class="tdHeaderStyle">
							対応状況
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSeekProgressSeq" runat="server" DataSourceID="dsProgress" DataTextField="DEFECT_REPORT_PROGRESS_NM" DataValueField="DEFECT_REPORT_PROGRESS_SEQ">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							ｷｰﾜｰﾄﾞ検索
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtSeekKeyword" runat="server" Width="200px"></asp:TextBox>
						</td>
						<td class="tdHeaderStyle2">
							状態
						</td>
						<td class="tdDataStyle">
							<asp:CheckBox ID="chkSeekApply" runat="server" Text="申請中" />&nbsp;
							<asp:CheckBox ID="chkSeekAccept" runat="server" Text="採用" />&nbsp;
							<asp:CheckBox ID="chkSeekPublic" runat="server" Text="公開" />&nbsp;
							<asp:CheckBox ID="chkSeekPrivate" runat="server" Text="非公開" />&nbsp;
							<asp:CheckBox ID="chkSeekOverlap" runat="server" Text="重複" />&nbsp;
							<asp:CheckBox ID="chkSeekRemove" runat="server" Text="移動" />
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Key" />
			</fieldset>
			<fieldset class="fieldset-inner">
				<legend>[不具合報告一覧]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count
						<%#GetRecCount() %>
					</a>
					<br />
					<a class="reccount">Current viewing page
						<%=grdDefectReport.PageIndex + 1%>
						of
						<%=grdDefectReport.PageCount%>
					</a>
                </asp:Panel>
				<br />
                &nbsp;<br />
				<asp:Button runat="server" ID="btnGroupOverlap" Text="選択したものの状態を重複にする" CssClass="seektopbutton" OnClick="btnGroupOverlap_Click" ValidationGroup="GroupOverlap" />
				<asp:TextBox ID="txtGroupOverlapSeq" runat="server" Width="100px"></asp:TextBox>
				<asp:Label ID="lblErrorMessageGroupOverlapSeq" runat="server" Text="" Visible="false" ForeColor="red"></asp:Label>
				<asp:RequiredFieldValidator ID="vdrGroupOverlapSeq" runat="server" ErrorMessage="重複SEQを入力して下さい。"
                    ControlToValidate="txtGroupOverlapSeq" ValidationGroup="GroupOverlap">*</asp:RequiredFieldValidator>
                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server"
                    TargetControlID="vdrGroupOverlapSeq" HighlightCssClass="validatorCallout" />
                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtGroupOverlapSeq" />
				<br /><br />
				<asp:Panel runat="server" ID="pnlGrid">
					<asp:GridView ID="grdDefectReport" runat="server" AllowPaging="True" AutoGenerateColumns="False" OnSorting="grdDefectReport_Sorting"
						EnableSortingAndPagingCallbacks="false" DataSourceID="dsDefectReport" SkinID="GridView" AllowSorting="True"
						DataKeyNames="SITE_CD,DEFECT_REPORT_SEQ,HANDLE_NM,LOGIN_ID" OnRowDataBound="grdDefectReport_RowDataBound">
						<Columns>
							<asp:TemplateField HeaderText="">
								<ItemTemplate>
									<asp:CheckBox ID="chkGroupOverlap" runat="server" Visible='<%# Eval("PUBLIC_STATUS").ToString().Equals(PwViCommConst.DefectReportPublicStatus.APPLY) ? true : false %>' />
	                                <asp:HiddenField ID="hdnDefectReportSeq" runat="server" Value='<%# Eval("DEFECT_REPORT_SEQ") %>' />
								</ItemTemplate>
								<ItemStyle HorizontalAlign="center" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="SEQ">
								<ItemTemplate>
									<asp:Label ID="lblDefectReportSeq" runat="server" Text='<%# Eval("DEFECT_REPORT_SEQ") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾀｲﾄﾙ">
								<ItemTemplate>
									<asp:LinkButton ID="lnkTitle" runat="server" CommandArgument='<%# Container.DataItemIndex %>'
										CommandName="MAINTE" OnCommand="lnkMainte_Command" Text='<%# GetConcatStr(Eval("TITLE"),30) %>'></asp:LinkButton>
								</ItemTemplate>
								<ItemStyle Width="200px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="投稿者">
								<ItemTemplate>
									<asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# this.GetProfUrl(Eval("SITE_CD"),Eval("SEX_CD"),Eval("LOGIN_ID")) %>' Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
									<br />
									<asp:Label ID="lblHandleNm" runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="DEFECT_REPORT_CATEGORY_NM" HeaderText="ｶﾃｺﾞﾘ">
							</asp:BoundField>
							<asp:TemplateField HeaderText="内容">
								<ItemTemplate>
									<asp:LinkButton id='lnkDefectReportContent' runat='server' Text='内容' OnClientClick='<%# GetClientScript(Eval("SITE_CD"),Eval("DEFECT_REPORT_SEQ"))  %>' />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="DEFECT_REPORT_PROGRESS_NM" HeaderText="対応状況">
								<ItemStyle HorizontalAlign="center" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="状態">
								<ItemTemplate>
									<asp:Label ID="lblPublicStatus" runat="server" Text='<%# this.GetPublicStatusNm(Eval("PUBLIC_STATUS")) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="center" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="重複件数" SortExpression="OVERLAP_COUNT">
								<ItemTemplate>
									<asp:Label ID="lblOverlapCount" runat="server" Text='<%# Eval("OVERLAP_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="投稿日時" SortExpression="CREATE_DATE">
								<ItemTemplate>
									<asp:Label ID="lblCreateDate" runat="server" Text='<%# DateTime.Parse(Eval("CREATE_DATE").ToString()).ToString("yy/MM/dd HH:mm") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</fieldset>
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCategory" runat="server" SelectMethod="GetDefectReportCategoryList" TypeName="DefectReport">
		<SelectParameters>
			<asp:ControlParameter ControlID="lstSeekSiteCd" Name="pSiteCd" PropertyName="SelectedValue" Type="String" />
			<asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" DefaultValue="" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsProgress" runat="server" SelectMethod="GetDefectReportProgressList" TypeName="DefectReport">
		<SelectParameters>
			<asp:ControlParameter ControlID="lstSeekSiteCd" Name="pSiteCd" PropertyName="SelectedValue" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsRequestCategory" runat="server" SelectMethod="GetRequestCategoryList" TypeName="Request">
		<SelectParameters>
			<asp:ControlParameter ControlID="lstSeekSiteCd" Name="pSiteCd" PropertyName="SelectedValue" Type="String" />
			<asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" DefaultValue="" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsDefectReport" runat="server" SelectMethod="GetPageCollection" ConvertNullToDBNull="true" SortParameterName=""
		TypeName="DefectReport" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelected="dsDefectReport_Selected" OnSelecting="dsDefectReport_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
</asp:Content>

