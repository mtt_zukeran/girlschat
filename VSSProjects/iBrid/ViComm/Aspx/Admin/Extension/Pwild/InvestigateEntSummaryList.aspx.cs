/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: この娘を探せ・男性会員エントリー(合計)

--	Progaram ID		: InvestigateEntSummaryList
--
--  Creation Date	: 2015.02.26
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class Extension_Pwild_InvestigateEntSummaryList:System.Web.UI.Page {
	private Stream filter;

	protected string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		private set {
			this.ViewState["SiteCd"] = value;
		}
	}

	protected string TargetMonth {
		get {
			return iBridUtil.GetStringValue(this.ViewState["TargetMonth"]);
		}
		set {
			this.ViewState["TargetMonth"] = value;
		}
	}

	protected List<string> MonthList {
		get {
			return this.ViewState["MonthList"] as List<string>;
		}
		set {
			this.ViewState["MonthList"] = value;
		}
	}

	protected List<string> HourList {
		get {
			return this.ViewState["HourList"] as List<string>;
		}
		set {
			this.ViewState["HourList"] = value;
		}
	}

	protected List<string> MinuteList {
		get {
			return this.ViewState["MinuteList"] as List<string>;
		}
		set {
			this.ViewState["MinuteList"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		
		if (!this.IsPostBack) {
			this.SiteCd = this.Request.QueryString["sitecd"];
			this.TargetMonth = this.Request.QueryString["targetmonth"];

			this.CreateLists();
			this.InitPage();

			if (!string.IsNullOrEmpty(this.SiteCd) && !string.IsNullOrEmpty(this.TargetMonth)) {
				this.lstSeekSiteCd.SelectedValue = this.SiteCd;
				this.pnlLink.DataBind();
				this.GetList();
				this.pnlList.Visible = true;
			} else {
				this.SiteCd = PwViCommConst.MAIN_SITE_CD;
				this.TargetMonth = DateTime.Today.ToString("yyyy/MM");
				this.pnlLink.DataBind();
				this.GetList();
				this.pnlList.Visible = true;
			}

			this.SetInvestigateMonthlyData();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		this.TargetMonth = DateTime.Today.ToString("yyyy/MM");
		this.pnlLink.DataBind();
		this.GetList();
		this.SetInvestigateMonthlyData();
		this.pnlList.Visible = true;
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnCsv_Click(object sender,EventArgs e) {

		string sReportMonth = this.TargetMonth.Replace("/","");
		
		string sFileNm = string.Format("MONTHLY_WONTED_{0}.CSV",sReportMonth);
		Response.Filter = filter;
		Response.AddHeader("Content-Disposition",string.Format("attachment;filename={0}",sFileNm));
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		DataSet ds;
		using (InvestigateEntSummary oInvestigateEntSummary = new InvestigateEntSummary()) {
			ds = oInvestigateEntSummary.GetWantedCompSheetCntList(this.SiteCd,sReportMonth);
			SetCsvData(ds);
			Response.End();
		}
	}

	public void SetCsvData(DataSet pDataSet) {

		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		string sHeaderString = "ログインID,ハンドル名,口数\r\n";
		Response.BinaryWrite(encoding.GetBytes(sHeaderString));

		string sDtl = "";

		DataRow dr = null;
		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];

			sDtl = string.Format("{0},{1},{2}",
							dr["LOGIN_ID"].ToString(),
							dr["HANDLE_NM"].ToString().Replace(",","."),
							dr["COMPLETE_COUNT"].ToString()
							);
			sDtl += "\r\n";
			Response.BinaryWrite(encoding.GetBytes(sDtl));
		}
	}

	protected void dsInvestigateEntSummary_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pTargetMonth"] = this.TargetMonth;
	}

	protected void dsInvestigateEntSummary_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		DataSet oInvestigateEntSummaryDataSet = e.ReturnValue as DataSet;
		if (oInvestigateEntSummaryDataSet == null || (oInvestigateEntSummaryDataSet).Tables[0].Rows.Count == 0) {
			this.lblNoData.Visible = true;
		}
	}

	protected void grdInvestigateEntSummary_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType != DataControlRowType.DataRow) {
			return;
		}

		string sExecutionDay = iBridUtil.GetStringValue(DataBinder.Eval(e.Row.DataItem,"EXECUTION_DAY"));

		DateTime oExecutionDay = DateTime.Parse(sExecutionDay);
		if (DateTime.Today == oExecutionDay) {
			e.Row.BackColor = Color.LavenderBlush;
		}

		DateTime oAnnounceDate = DateTime.Parse(string.Format("{0}",sExecutionDay));
		if (DateTime.Now < oAnnounceDate) {
			HyperLink oPointAcquiredCountLink = e.Row.FindControl("lnkPointAcquiredCount") as HyperLink;
			oPointAcquiredCountLink.Text = "集計中";
		}
	}

	private void InitPage() {
		this.grdInvestigateEntSummary.DataSourceID = string.Empty;
		this.pnlList.Visible = false;
		this.lstSeekSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	protected Color GetDayOfWeekColor(object pDayOfWeek) {
		switch (iBridUtil.GetStringValue(pDayOfWeek)) {
			case "土":
				return Color.Blue;
			case "日":
				return Color.Red;
			default:
				return Color.Empty;
		}
	}

	protected Color GetMonthLinkColor(object pMonth) {
		if (pMonth.Equals(this.TargetMonth)) {
			return Color.Red;
		} else {
			return Color.Empty;
		}
	}

	protected string GetYear(int pAddYear) {
		DateTime oTargetDay;
		if (!DateTime.TryParse(string.Format("{0}/01",this.TargetMonth),out oTargetDay)) {
			return string.Empty;
		}

		return oTargetDay.AddYears(pAddYear).ToString("yyyy");
	}

	protected string GetTargetMonthLink(string pTargetMonth) {
		if (pTargetMonth.Equals(this.TargetMonth)) {
			return string.Empty;
		}
		return string.Format("~/Extension/Pwild/InvestigateEntSummaryList.aspx?sitecd={0}&targetmonth={1}",this.lstSeekSiteCd.SelectedValue,pTargetMonth);
	}

	protected string GetInvestigateEntrantLink(object pExecutionDay,string pCaughtFlag,string pPointAcquiredFlag) {
		return string.Format("~/Extension/Pwild/InvestigateEntrantList.aspx?sitecd={0}&executionday={1}&caughtflag={2}&pointacquiredflag={3}",this.lstSeekSiteCd.SelectedValue,pExecutionDay,pCaughtFlag,pPointAcquiredFlag);
	}

	private void GetList() {
		this.grdInvestigateEntSummary.DataSourceID = "dsInvestigateEntSummary";
		this.grdInvestigateEntSummary.DataBind();
	}

	private void CreateLists() {
		List<string> oMonthList = new List<string>();
		List<string> oHourList = new List<string>();
		List<string> oMinuteList = new List<string>();

		for (int iIndex = 0;iIndex < 59;iIndex++) {
			string sValue = string.Format("{0:00}",iIndex);
			if (iIndex < 12) {
				oMonthList.Add((iIndex + 1).ToString());
			}
			if (iIndex < 24) {
				oHourList.Add(sValue);
			}
			oMinuteList.Add(sValue);
		}

		this.MonthList = oMonthList;
		this.HourList = oHourList;
		this.MinuteList = oMinuteList;
	}

	private void SetInvestigateMonthlyData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("INVESTIGATE_MONTHLY_DATA_GET");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			db.ProcedureInParm("pREPORT_MONTH",DbSession.DbType.VARCHAR2,this.TargetMonth);
			db.ProcedureOutParm("pUNIQUE_USER_COUNT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pCAUGHT_COUNT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pCOMPLETE_COUNT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pCOMPLETE_USER_COUNT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pACQUIRED_USER_COUNT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pBOUNTY_POINT_PER_SHEET",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			this.lblUniqueUser.Text = db.GetStringValue("pUNIQUE_USER_COUNT");
			this.lblCaughtCount.Text = db.GetStringValue("pCAUGHT_COUNT");
			this.lblCompleteCount.Text = db.GetStringValue("pCOMPLETE_COUNT");
			this.lblCompleteUserCount.Text = db.GetStringValue("pCOMPLETE_USER_COUNT");
			this.lblAcquiredUserCount.Text = db.GetStringValue("pACQUIRED_USER_COUNT");
			this.lblBountyPointPerSheet.Text = db.GetStringValue("pBOUNTY_POINT_PER_SHEET");
		}
	}
}