﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 評価一覧
--	Progaram ID		: RequestValueList
--
--  Creation Date	: 2012.10.29
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Windows.Forms;
using System.Drawing;
using MobileLib;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class Extension_Pwild_RequestValueList:System.Web.UI.Page {
	private string recCount = "";

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string RequestValueSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RequestValueSeq"]);
		}
		set {
			this.ViewState["RequestValueSeq"] = value;
		}
	}

	private string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		set {
			this.ViewState["SexCd"] = value;
		}
	}

	private string SortExpression {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortExpression"]);
		}
		set {
			this.ViewState["SortExpression"] = value;
		}
	}

	private string SortDirect {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortDirect"]);
		}
		set {
			this.ViewState["SortDirect"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!IsPostBack) {
			this.SexCd = iBridUtil.GetStringValue(Request.QueryString["sexcd"]);

			if (this.SexCd.Equals(ViCommConst.MAN)) {
				this.lblPgmTitle.Text = "男性" + lblPgmTitle.Text;
				this.Title = "男性" + this.Title;
			} else {
				this.lblPgmTitle.Text = "女性" + lblPgmTitle.Text;
				this.Title = "女性" + this.Title;
			}

			this.recCount = "0";
			this.SortDirect = string.Empty;
			this.SortExpression = string.Empty;

			this.lstSeekSiteCd.DataBind();
			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
				this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}

			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["requestseq"]))) {
				this.txtSeekRequestSeq.Text = iBridUtil.GetStringValue(Request.QueryString["requestseq"]);
			}
			if (iBridUtil.GetStringValue(Request.QueryString["good"]).Equals(ViCommConst.FLAG_ON_STR)) {
				this.chkSeekGood.Checked = true;
			}
			if (iBridUtil.GetStringValue(Request.QueryString["bad"]).Equals(ViCommConst.FLAG_ON_STR)) {
				this.chkSeekBad.Checked = true;
			}

			if (iBridUtil.GetStringValue(Request.QueryString["comment"]).Equals(ViCommConst.FLAG_ON_STR)) {
				chkCommentFlag.Checked = true;
			}

			this.grdRequestValue.PageIndex = 0;
			this.grdRequestValue.PageSize = 50;

			this.GetList();
			this.pnlMainte.Visible = false;
		}
	}

	private void GetList() {
		this.grdRequestValue.DataSourceID = "dsRequestValue";
		this.grdRequestValue.DataBind();
		this.pnlCount.DataBind();
	}

	protected string GetRecCount() {
		return recCount;
	}

	private void UpdateData() {
		if (!this.IsValid) {
			return;
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("REQUEST_VALUE_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pREQUEST_VALUE_SEQ",DbSession.DbType.VARCHAR2,this.RequestValueSeq);
			oDbSession.ProcedureInParm("pCOMMENT_TEXT",DbSession.DbType.VARCHAR2,this.txtCommentText.Text);
			oDbSession.ProcedureInParm("pCOMMENT_PUBLIC_STATUS",DbSession.DbType.NUMBER,this.lstCommentPublicStatus.SelectedValue);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			oDbSession.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
		}
	}

	private void ClearField() {
		this.SiteCd = string.Empty;
		this.RequestValueSeq = string.Empty;

		this.lblRequestTitle.Text = string.Empty;
		this.lblRequestText.Text = string.Empty;
		this.lnkLoginId.Text = string.Empty;
		this.lnkLoginId.NavigateUrl = string.Empty;
		this.lblHandleNm.Text = string.Empty;
		this.lblValueNm.Text = string.Empty;
		this.txtCommentText.Text = string.Empty;
		this.lblCommentText.Text = string.Empty;
		this.lblCreateDate.Text = string.Empty;
		this.lblCommentPublicDate.Text = string.Empty;
		this.lstCommentPublicStatus.SelectedIndex = 0;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		this.SortDirect = string.Empty;
		this.SortExpression = string.Empty;
		this.pnlMainte.Visible = false;
		this.GetList();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		this.UpdateData();
		this.GetList();
		this.pnlMainte.Visible = false;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlMainte.Visible = false;
		this.ClearField();
	}

	protected void lnkMainte_Command(object sender,CommandEventArgs e) {
		if (!e.CommandName.Equals("MAINTE")) {
			return;
		}

		this.ClearField();

		DataSet oDataSet;
		int iRowIndex = int.Parse(e.CommandArgument.ToString());
		IDictionary oDataKeys = this.grdRequestValue.DataKeys[iRowIndex].Values;
		this.SiteCd = iBridUtil.GetStringValue(oDataKeys["SITE_CD"]);
		this.RequestValueSeq = iBridUtil.GetStringValue(oDataKeys["REQUEST_VALUE_SEQ"]);

		RequestValue.SearchCondition oSearchCondition = new RequestValue.SearchCondition();
		oSearchCondition.SiteCd = this.SiteCd;
		oSearchCondition.RequestValueSeq = this.RequestValueSeq;
		oSearchCondition.SexCd = this.SexCd;

		using (RequestValue oRequestValue = new RequestValue()) {
			oDataSet = oRequestValue.GetPageCollection(oSearchCondition,0,1);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			DataRow oDataRow = oDataSet.Tables[0].Rows[0];
			this.lblRequestTitle.Text = iBridUtil.GetStringValue(oDataRow["REQUEST_TITLE"]);
			this.lblRequestText.Text = iBridUtil.GetStringValue(oDataRow["REQUEST_TEXT"]);
			this.lnkLoginId.Text = iBridUtil.GetStringValue(oDataRow["LOGIN_ID"]);
			this.lnkLoginId.NavigateUrl = this.GetProfUrl(oDataRow["SITE_CD"],oDataRow["SEX_CD"],oDataRow["LOGIN_ID"]);
			this.lblHandleNm.Text = iBridUtil.GetStringValue(oDataRow["HANDLE_NM"]);
			this.lblValueNm.Text = iBridUtil.GetStringValue(oDataRow["VALUE_NM"]);
			this.txtCommentText.Text = iBridUtil.GetStringValue(oDataRow["COMMENT_TEXT"]);
			this.lblCommentText.Text = iBridUtil.GetStringValue(oDataRow["COMMENT_TEXT"]).Replace("\n","<br />");
			this.lblCreateDate.Text = iBridUtil.GetStringValue(oDataRow["CREATE_DATE"]);
			this.lblCommentPublicDate.Text = iBridUtil.GetStringValue(oDataRow["COMMENT_PUBLIC_DATE"]);

			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(oDataRow["COMMENT_PUBLIC_STATUS"]))) {
				this.lstCommentPublicStatus.SelectedValue = iBridUtil.GetStringValue(oDataRow["COMMENT_PUBLIC_STATUS"]);
			}
		}

		this.pnlMainte.Visible = true;
	}

	protected void dsRequestValue_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsRequestValue_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		RequestValue.SearchCondition oSearchCondition = new RequestValue.SearchCondition();
		oSearchCondition.SiteCd = this.lstSeekSiteCd.SelectedValue;
		oSearchCondition.SexCd = this.SexCd;

		List<string> oValueNo = new List<string>();
		List<string> oCommentPublicStatus = new List<string>();

		if (this.chkSeekGood.Checked) {
			oValueNo.Add("1");
		}
		if (this.chkSeekBad.Checked) {
			oValueNo.Add("2");
		}

		if (this.chkSeekApply.Checked) {
			oCommentPublicStatus.Add("1");
		}
		if (this.chkSeekPublic.Checked) {
			oCommentPublicStatus.Add("2");
		}

		oSearchCondition.ValueNo = oValueNo;
		oSearchCondition.CommentPublicStatus = oCommentPublicStatus;
		oSearchCondition.LoginId = this.txtSeekLoginId.Text.Trim();
		oSearchCondition.RequestSeq = this.txtSeekRequestSeq.Text.Trim();
		oSearchCondition.Keyword = this.txtSeekKeyword.Text.Trim();

		if (chkCommentFlag.Checked.Equals(true)) {
			oSearchCondition.CommentFlag = ViCommConst.FLAG_ON_STR;
		}

		oSearchCondition.SortDirection = this.SortDirect;
		oSearchCondition.SortExpression = this.SortExpression;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void grdRequestValue_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			string sCommentPublicStatus = DataBinder.Eval(e.Row.DataItem,"COMMENT_PUBLIC_STATUS").ToString();

			if (sCommentPublicStatus.Equals(PwViCommConst.RequestValueCommentPublicStatus.APPLY)) {
				e.Row.BackColor = Color.LavenderBlush;
			} else {
				e.Row.BackColor = Color.White;
			}
		}
	}

	protected void grdRequestValue_Sorting(object sender,GridViewSortEventArgs e) {
		e.Cancel = true;

		if (this.SortExpression.Equals(e.SortExpression)) {
			if (this.SortDirect.Equals("ASC")) {
				this.SortDirect = "DESC";
			} else if (this.SortDirect.Equals("DESC")) {
				this.SortDirect = "ASC";
			}
		} else {
			this.SortDirect = "ASC";
		}

		this.SortExpression = e.SortExpression;

		this.GetList();
	}

	protected string GetConcatStr(object pText,int pLength) {
		string sText = iBridUtil.GetStringValue(pText);

		if (sText.Length > pLength) {
			return string.Concat(SysPrograms.Substring(sText,(pLength - 1)),"…");
		} else {
			return sText;
		}
	}

	protected string GetProfUrl(object pSiteCd,object pSexCd,object pLoginId) {
		string sValue = string.Empty;

		if (iBridUtil.GetStringValue(pSexCd).Equals(ViCommConst.MAN)) {
			sValue = string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",iBridUtil.GetStringValue(pSiteCd),iBridUtil.GetStringValue(pLoginId));
		} else {
			sValue = string.Format("~/Cast/CastView.aspx?loginid={0}",iBridUtil.GetStringValue(pLoginId));
		}

		return sValue;
	}

	protected void lnkPictographSample_Click(object sender,EventArgs e) {
		string sURL = string.Concat(Session["Root"],"/Site/PictographSample.aspx");
		string sScripts = string.Format("<script language=\"javascript\">window.open('{0}','','width=1060,height=900,resizable=yes,directories=no,scrollbars=yes' , false);</script>",sURL);
		ClientScript.RegisterStartupScript(Page.GetType(),"OpenNewWindow",sScripts);
	}
}
