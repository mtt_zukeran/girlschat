﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="YakyukenCommentList.aspx.cs" Inherits="Extension_Pwild_YakyukenCommentList" Title="Untitled Page" %>
<%@ Import Namespace="ViComm" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="野球拳女性勝利コメント一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlKey">
			<fieldset class="fieldset">
				<legend>[検索]</legend>
				<asp:Panel ID="pnlSeekCondition" runat="server">
					<table border="0" style="width: 800px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle">
								サイトコード
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle2">
								ログインID
							</td>
							<td class="tdDataStyle">
                                <asp:TextBox ID="txtLoginId" runat="server" Width="100px"></asp:TextBox>
							</td>
						</tr>
					</table>
				</asp:Panel>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Seek" />
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[一覧]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count <%# GetRecCount() %></a>
					<br />
					<a class="reccount">Current viewing page <%= grdYakyukenComment.PageIndex + 1 %> of <%= grdYakyukenComment.PageCount %></a>
				</asp:Panel>
				<br />
				<asp:Button runat="server" ID="btnAdminCheck" Text="選択したものを確認済にする" CssClass="seektopbutton" OnClick="btnAdminCheck_Click" />
				<br />
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="443px" CssClass="clear">
					<asp:GridView ID="grdYakyukenComment" runat="server" AllowPaging="True" AutoGenerateColumns="False" PageSize="30"
						DataSourceID="dsYakyukenComment" SkinID="GridViewColor" AllowSorting="true" OnRowDataBound="grdYakyukenComment_RowDataBound">
						<Columns>
							<asp:TemplateField HeaderText="確認">
								<ItemTemplate>
									<asp:Label ID="lblAdminChek" runat="server" Text="済" Visible='<%# Eval("ADMIN_CHECK_FLAG").ToString().Equals(ViCommConst.FLAG_ON_STR) ? true : false %>'></asp:Label>
									<asp:CheckBox ID="chkAdminCheck" runat="server" Visible='<%# Eval("ADMIN_CHECK_FLAG").ToString().Equals(ViCommConst.FLAG_OFF_STR) ? true : false %>' />
	                                <asp:HiddenField ID="hdnYakyukenCommentSeq" runat="server" Value='<%# Eval("YAKYUKEN_COMMENT_SEQ") %>' />
								</ItemTemplate>
								<ItemStyle HorizontalAlign="center" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾊﾝﾄﾞﾙ名<br>会員ID" SortExpression="LOGIN_ID">
								<ItemTemplate>
									<asp:HyperLink ID="lblHandleNm" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}",Eval("LOGIN_ID")) %>'
										Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:HyperLink><br />
									<asp:Label ID="lblLoginId" runat="server" Text='<%# Eval("LOGIN_ID") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle Width="120px" HorizontalAlign="left" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="コメント">
								<ItemTemplate>
									<asp:Label ID="lblCommentText" runat="server" Text='<%# Eval("COMMENT_TEXT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="left" Width="300px" Wrap="true" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="書込日時" SortExpression="CREATE_DATE">
								<ItemTemplate>
									<asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("CREATE_DATE", "{0:yy/MM/dd HH:mm:ss}") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="center" />
							</asp:TemplateField>
 							<asp:TemplateField HeaderText="削除">
								<ItemTemplate>
									<%# ViCommConst.FLAG_ON_STR.Equals(Eval("DEL_FLAG","{0}")) ? "削除済み<br />" : "" %>
									<asp:LinkButton ID="lnkDelete" runat="server" CommandArgument='<%#string.Format("{0},{1},{2},{3}",Eval("SITE_CD"),Eval("YAKYUKEN_COMMENT_SEQ"),Eval("DEL_FLAG"),Eval("REVISION_NO")) %>'
										Text='<%# ViCommConst.FLAG_ON_STR.Equals(Eval("DEL_FLAG","{0}")) ? "[元に戻す]" : "[削除]" %>' OnCommand="lnkDelete_OnCommand" Visible="true" OnClientClick="return confirm('削除/復活します。よろしいですか？');" ></asp:LinkButton>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" />
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
			<br />
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsYakyukenComment" runat="server" SelectMethod="GetPageCollection" ConvertNullToDBNull="true" SortParameterName=""
		TypeName="YakyukenComment" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelected="dsYakyukenComment_Selected"
		OnSelecting="dsYakyukenComment_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>

