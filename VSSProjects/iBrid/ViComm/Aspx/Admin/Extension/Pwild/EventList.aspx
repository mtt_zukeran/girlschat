﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="EventList.aspx.cs" Inherits="Extension_Pwild_EventList" Title="イベント情報一覧" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="イベント情報一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 600px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                大カテゴリ
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstEventCategorySearch" runat="server" DataSourceID="dsEventCategory" DataTextField="EVENT_CATEGORY_NM"
                                    DataValueField="EVENT_CATEGORY_SEQ" Width="170px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                エリア
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstPrefectureCdSearch" runat="server" DataSourceID="dsPrefecture" DataTextField="CODE_NM"
									DataValueField="CODE" OnDataBound="lst_DataBound" OnSelectedIndexChanged="lstPrefectureCdSearch_IndexChanged" AutoPostBack="True">
								</asp:DropDownList>
								<asp:DropDownList ID="lstEventAreaCdSearch" runat="server" DataSourceID="dsEventArea" DataTextField="EVENT_AREA_NM"
									OnDataBound="lst_DataBound" DataValueField="EVENT_AREA_CD">
								</asp:DropDownList>
								<% // 必須チェック %>
								<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="地域を選択してください。"
								    ControlToValidate="lstEventAreaCd" ValidationGroup="Update">*</asp:RequiredFieldValidator>
								<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
								    TargetControlID="vdrEventArea" HighlightCssClass="validatorCallout" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                イベント名
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtEventNmSearch" runat="server" Width="400px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
	                      <td class="tdHeaderStyle2">
                                開催日
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstYYYYSearchFrom" runat="server" Width="60px" DataSource='<%# YearArraySearch %>'>
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstMMSearchFrom" runat="server" Width="40px" DataSource='<%# MonthArray %>'>
                                </asp:DropDownList>月
                                <asp:DropDownList ID="lstDDSearchFrom" runat="server" Width="40px" DataSource='<%# DayArray %>'>
                                </asp:DropDownList>日
                                ～
                                <asp:DropDownList ID="lstYYYYSearchTo" runat="server" Width="60px" DataSource='<%# YearArraySearch %>'>
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstMMSearchTo" runat="server" Width="40px" DataSource='<%# MonthArray %>'>
                                </asp:DropDownList>月
                                <asp:DropDownList ID="lstDDSearchTo" runat="server" Width="40px" DataSource='<%# DayArray %>'>
                                </asp:DropDownList>日
                                <asp:Label ID="lblErrorMessageSearchFrom" runat="server" Text="" Visible="false" ForeColor="red"></asp:Label>
                                <asp:Label ID="lblErrorMessageSearchTo" runat="server" Text="" Visible="false" ForeColor="red"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton" ValidationGroup="Create"
                    OnClick="btnCreate_Click" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" Visible="false" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlEventInfo">
            <fieldset class="fieldset">
                <legend>[イベント内容]</legend>
                <fieldset class="fieldset-inner">
                    <table border="0" class="tableStyle" style="width: 600px">
                        <tr>
                            <td class="tdHeaderStyle">
                                大カテゴリ
                            </td>
                            <td class="tdDataStyle">
								<asp:DropDownList ID="lstEventCategory" runat="server" DataSourceID="dsEventCategory"
									DataTextField="EVENT_CATEGORY_NM" DataValueField="EVENT_CATEGORY_SEQ"
									OnDataBound="lst_DataBound" Width="170px">
								</asp:DropDownList>
								<% // 必須チェック %>
								<asp:RequiredFieldValidator ID="vdrEventCategory" runat="server" ErrorMessage="イベント大カテゴリを選択してください。"
								    ControlToValidate="lstEventCategory" ValidationGroup="Update">*</asp:RequiredFieldValidator>
								<ajaxToolkit:ValidatorCalloutExtender ID="vdeEventCategory" runat="Server"
								    TargetControlID="vdrEventCategory" HighlightCssClass="validatorCallout" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                エリア
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstPrefectureCd" runat="server" DataSourceID="dsPrefecture" DataTextField="CODE_NM"
									DataValueField="CODE" OnDataBound="lst_DataBound" OnSelectedIndexChanged="lstPrefectureCd_IndexChanged" AutoPostBack="True">
								</asp:DropDownList>
								<asp:DropDownList ID="lstEventAreaCd" runat="server" DataSourceID="dsEventArea" DataTextField="EVENT_AREA_NM"
									OnDataBound="lst_DataBound" DataValueField="EVENT_AREA_CD">
								</asp:DropDownList>
								<% // 必須チェック %>
								<asp:RequiredFieldValidator ID="vdrEventArea" runat="server" ErrorMessage="地域を選択してください。"
								    ControlToValidate="lstEventAreaCd" ValidationGroup="Update">*</asp:RequiredFieldValidator>
								<ajaxToolkit:ValidatorCalloutExtender ID="vdeEventArea" runat="Server"
								    TargetControlID="vdrEventArea" HighlightCssClass="validatorCallout" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                イベント名
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtEventNm" runat="server" MaxLength="120" Width="400px"></asp:TextBox>
                                <% // 必須チェック %>
                                <asp:RequiredFieldValidator ID="vdrEventNm" runat="server" ErrorMessage="イベント名を入力して下さい。"
                                    ControlToValidate="txtEventNm" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server"
                                    TargetControlID="vdrEventNm" HighlightCssClass="validatorCallout" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                開始日
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstFromYYYY" runat="server" Width="60px" DataSource='<%# YearArray %>'>
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstFromMM" runat="server" Width="40px" DataSource='<%# MonthArray %>'>
                                </asp:DropDownList>月
                                <asp:DropDownList ID="lstFromDD" runat="server" Width="40px" DataSource='<%# DayArray %>'>
                                </asp:DropDownList>日
                                <asp:Label ID="lblErrorMessageFrom" runat="server" Text="" Visible="false" ForeColor="red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                終了日
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstToYYYY" runat="server" Width="60px" DataSource='<%# YearArray %>'>
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstToMM" runat="server" Width="40px" DataSource='<%# MonthArray %>'>
                                </asp:DropDownList>月
                                <asp:DropDownList ID="lstToDD" runat="server" Width="40px" DataSource='<%# DayArray %>'>
                                </asp:DropDownList>日
                                <asp:Label ID="lblErrorMessageTo" runat="server" Text="" Visible="false" ForeColor="red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle2">
                                詳細
                            </td>
                            <td class="tdDataStyle">
								<asp:TextBox ID="txtEventDetail" runat="server" Height="30px" MaxLength="50" Rows="3"
								   TextMode="MultiLine" Width="500px"></asp:TextBox>
							</td>
                        </tr>
                    </table>
                </fieldset>
                <asp:Panel ID="pnlCommand" runat="server">
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Update"
                        OnClick="btnUpdate_Click" />
                    <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" ValidationGroup="Delete"
                        OnClick="btnDelete_Click" />
                    <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" CausesValidation="False"
                        Visible="true" OnClick="btnCancel_Click" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[イベント一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdEvent.PageIndex + 1 %>
                        of
                        <%= grdEvent.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="600px">
                    <asp:GridView ID="grdEvent" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        PageSize="30" EnableSortingAndPagingCallbacks="false" DataSourceID="dsEvent"
                        SkinID="GridViewColor" Font-Size="small" AllowSorting="true">
                        <Columns>
                            <asp:TemplateField HeaderText="イベント名">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEventNm" runat="server" CommandArgument='<%# Eval("EVENT_SEQ") %>'
                                        Text='<%# Eval("EVENT_NM") %>' OnCommand="lnkEventNm_Command"></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="EVENT_CATEGORY_NM" HeaderText="大カテゴリ">
                                <ItemStyle HorizontalAlign="left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="エリア">
								<ItemTemplate>
									<asp:Label ID="lblPrefectureNm" runat="server" Text='<%# Eval("PREFECTURE_NM") %>'></asp:Label>&nbsp;
									<asp:Label ID="lblEventAreaNm" runat="server" Text='<%# Eval("EVENT_AREA_NM") %>'></asp:Label>
								</ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="EVENT_DETAIL" HeaderText="イベント詳細">
                                <ItemStyle HorizontalAlign="left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="開始日">
                                <ItemTemplate>
                                    <asp:Label ID="lblEventStartDate" runat="server" Text='<%# Eval("EVENT_START_DATE", "{0:yyyy/MM/dd}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="終了日">
                                <ItemTemplate>
                                    <asp:Label ID="lblEventEndDate" runat="server" Text='<%# Eval("EVENT_END_DATE", "{0:yyyy/MM/dd}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsEvent" runat="server" SelectMethod="GetPageCollection"
        ConvertNullToDBNull="true" SortParameterName="" TypeName="Event" SelectCountMethod="GetPageCount"
        EnablePaging="True" OnSelected="dsEvent_Selected" OnSelecting="dsEvent_Selecting">
        <SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsEventCategory" runat="server" SelectMethod="GetList" TypeName="EventCategory">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsPrefecture" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="67" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsEventArea" runat="server" SelectMethod="GetList"
        TypeName="EventArea" OnSelecting="dsEventArea_Selecting">
        <SelectParameters>
            <asp:Parameter Name="pPrefectureCd" Type="String" DefaultValue="" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="イベント大カテゴリ情報を更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete"
        ConfirmText="イベント大カテゴリ情報を削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>
