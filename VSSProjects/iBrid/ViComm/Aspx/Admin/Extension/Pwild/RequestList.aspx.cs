﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: お願い一覧
--	Progaram ID		: RequestList
--
--  Creation Date	: 2012.10.29
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Windows.Forms;
using System.Drawing;
using MobileLib;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class Extension_Pwild_RequestList:System.Web.UI.Page {
	private string recCount = "";

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string RequestSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RequestSeq"]);
		}
		set {
			this.ViewState["RequestSeq"] = value;
		}
	}

	private string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		set {
			this.ViewState["SexCd"] = value;
		}
	}

	private string SortExpression {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortExpression"]);
		}
		set {
			this.ViewState["SortExpression"] = value;
		}
	}

	private string SortDirect {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortDirect"]);
		}
		set {
			this.ViewState["SortDirect"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!IsPostBack) {
			this.SexCd = iBridUtil.GetStringValue(Request.QueryString["sexcd"]);

			if (this.SexCd.Equals(ViCommConst.MAN)) {
				this.lblPgmTitle.Text = "男性" + lblPgmTitle.Text;
				this.Title = "男性" + this.Title;
			} else {
				this.lblPgmTitle.Text = "女性" + lblPgmTitle.Text;
				this.Title = "女性" + this.Title;
			}

			this.recCount = "0";
			this.SortDirect = string.Empty;
			this.SortExpression = string.Empty;

			this.lstSeekSiteCd.DataBind();
			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
				this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}

			this.lstSeekRequestCategorySeq.DataBind();
			this.lstSeekRequestCategorySeq.Items.Insert(0,new ListItem(string.Empty,string.Empty));

			this.lstSeekRequestProgressSeq.DataBind();
			this.lstSeekRequestProgressSeq.Items.Insert(0,new ListItem(string.Empty,string.Empty));

			this.chkSeekApply.Checked = true;
			this.chkSeekPublic.Checked = true;

			this.grdRequest.PageIndex = 0;
			this.grdRequest.PageSize = 50;

			this.GetList();
			this.pnlMainte.Visible = false;
		}
	}

	private void GetList() {
		this.grdRequest.DataSourceID = "dsRequest";
		this.grdRequest.DataBind();
		this.pnlCount.DataBind();
	}

	protected string GetRecCount() {
		return recCount;
	}

	private void UpdateData() {
		if (!this.IsValid) {
			return;
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("REQUEST_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pREQUEST_SEQ",DbSession.DbType.VARCHAR2,this.RequestSeq);
			oDbSession.ProcedureInParm("pTITLE",DbSession.DbType.VARCHAR2,this.txtTitle.Text);
			oDbSession.ProcedureInParm("pTEXT",DbSession.DbType.VARCHAR2,this.txtText.Text);
			oDbSession.ProcedureInParm("pREQUEST_CATEGORY_SEQ",DbSession.DbType.VARCHAR2,this.lstRequestCategorySeq.SelectedValue);
			oDbSession.ProcedureInParm("pADMIN_COMMENT",DbSession.DbType.VARCHAR2,this.txtAdminComment.Text);
			oDbSession.ProcedureInParm("pREQUEST_PROGRESS_SEQ",DbSession.DbType.VARCHAR2,this.lstRequestProgressSeq.SelectedValue);
			oDbSession.ProcedureInParm("pPUBLIC_STATUS",DbSession.DbType.NUMBER,this.lstPublicStatus.SelectedValue);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			oDbSession.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
		}
	}

	private void ClearField() {
		this.SiteCd = string.Empty;
		this.RequestSeq = string.Empty;

		this.lnkLoginId.Text = string.Empty;
		this.lnkLoginId.NavigateUrl = string.Empty;
		this.lblHandleNm.Text = string.Empty;
		this.txtTitle.Text = string.Empty;
		this.lstRequestCategorySeq.SelectedIndex = 0;
		this.lstRequestProgressSeq.SelectedIndex = 0;
		this.txtText.Text = string.Empty;
		this.lblText.Text = string.Empty;
		this.txtAdminComment.Text = string.Empty;
		this.lblAdminComment.Text = string.Empty;
		this.lblCreateDate.Text = string.Empty;
		this.lblPublicDate.Text = string.Empty;
		this.lstPublicStatus.SelectedIndex = 0;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		this.SortDirect = string.Empty;
		this.SortExpression = string.Empty;
		this.pnlMainte.Visible = false;
		this.GetList();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		this.UpdateData();
		this.GetList();
		this.pnlMainte.Visible = false;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlMainte.Visible = false;
		this.ClearField();
	}

	protected void lnkMainte_Command(object sender, CommandEventArgs e) {
		if (!e.CommandName.Equals("MAINTE")) {
			return;
		}

		this.lstRequestCategorySeq.DataBind();
		this.lstRequestProgressSeq.DataBind();
		this.ClearField();

		DataSet oDataSet;
		int iRowIndex = int.Parse(e.CommandArgument.ToString());
		IDictionary oDataKeys = this.grdRequest.DataKeys[iRowIndex].Values;
		this.SiteCd = iBridUtil.GetStringValue(oDataKeys["SITE_CD"]);
		this.RequestSeq = iBridUtil.GetStringValue(oDataKeys["REQUEST_SEQ"]);

		Request.SearchCondition oSearchCondition = new Request.SearchCondition();
		oSearchCondition.SiteCd = this.SiteCd;
		oSearchCondition.RequestSeq = this.RequestSeq;
		oSearchCondition.SexCd = this.SexCd;

		using (Request oRequest = new Request()) {
			oDataSet = oRequest.GetPageCollection(oSearchCondition,0,1);
		}

		if (oDataSet.Tables[0].Rows.Count > 0) {
			DataRow oDataRow = oDataSet.Tables[0].Rows[0];
			this.lnkLoginId.Text = iBridUtil.GetStringValue(oDataRow["LOGIN_ID"]);
			this.lnkLoginId.NavigateUrl = this.GetProfUrl(oDataRow["SITE_CD"],oDataRow["SEX_CD"],oDataRow["LOGIN_ID"]);
			this.lblHandleNm.Text = iBridUtil.GetStringValue(oDataRow["HANDLE_NM"]);
			this.txtTitle.Text = iBridUtil.GetStringValue(oDataRow["TITLE"]);
			this.lblTitle.Text = iBridUtil.GetStringValue(oDataRow["TITLE"]);
			this.lstRequestCategorySeq.SelectedValue = iBridUtil.GetStringValue(oDataRow["REQUEST_CATEGORY_SEQ"]);
			this.lstRequestProgressSeq.SelectedValue = iBridUtil.GetStringValue(oDataRow["REQUEST_PROGRESS_SEQ"]);
			this.txtText.Text = iBridUtil.GetStringValue(oDataRow["TEXT"]);
			this.lblText.Text = iBridUtil.GetStringValue(oDataRow["TEXT"]).Replace("\n","<br />");
			this.txtAdminComment.Text = iBridUtil.GetStringValue(oDataRow["ADMIN_COMMENT"]);
			this.lblAdminComment.Text = iBridUtil.GetStringValue(oDataRow["ADMIN_COMMENT"]).Replace("\n","<br />");
			this.lblCreateDate.Text = iBridUtil.GetStringValue(oDataRow["CREATE_DATE"]);
			this.lblPublicDate.Text = iBridUtil.GetStringValue(oDataRow["PUBLIC_DATE"]);
			this.lstPublicStatus.SelectedValue = iBridUtil.GetStringValue(oDataRow["PUBLIC_STATUS"]);
		}

		this.pnlMainte.Visible = true;
	}

	protected void dsRequest_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsRequest_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		Request.SearchCondition oSearchCondition = new Request.SearchCondition();
		oSearchCondition.SiteCd = this.lstSeekSiteCd.SelectedValue;
		oSearchCondition.SexCd = this.SexCd;
		oSearchCondition.RequestCategorySeq = this.lstSeekRequestCategorySeq.SelectedValue;
		oSearchCondition.RequestProgressSeq = this.lstSeekRequestProgressSeq.SelectedValue;

		List<string> oPublicStatus = new List<string>();

		if (this.chkSeekApply.Checked) {
			oPublicStatus.Add("1");
		}
		if (this.chkSeekPublic.Checked) {
			oPublicStatus.Add("2");
		}
		if (this.chkSeekPrivate.Checked) {
			oPublicStatus.Add("3");
		}

		oSearchCondition.PublicStatus = oPublicStatus;
		oSearchCondition.LoginId = this.txtSeekLoginId.Text.Trim();
		oSearchCondition.Keyword = this.txtSeekKeyword.Text.Trim();
		oSearchCondition.SortDirection = this.SortDirect;
		oSearchCondition.SortExpression = this.SortExpression;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void grdRequest_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			string sPublicStatus = DataBinder.Eval(e.Row.DataItem,"PUBLIC_STATUS").ToString();

			if (sPublicStatus.Equals(PwViCommConst.RequestPublicStatus.APPLY)) {
				e.Row.BackColor = Color.LavenderBlush;
			} else if (sPublicStatus.Equals(PwViCommConst.RequestPublicStatus.PRIVATE)) {
				e.Row.BackColor = Color.Gainsboro;
			} else {
				e.Row.BackColor = Color.White;
			}
		}
	}

	protected void grdRequest_Sorting(object sender,GridViewSortEventArgs e) {
		e.Cancel = true;

		if (this.SortExpression.Equals(e.SortExpression)) {
			if (this.SortDirect.Equals("ASC")) {
				this.SortDirect = "DESC";
			} else if (this.SortDirect.Equals("DESC")) {
				this.SortDirect = "ASC";
			}
		} else {
			this.SortDirect = "ASC";
		}

		this.SortExpression = e.SortExpression;

		this.GetList();
	}

	protected string GetConcatStr(object pText,int pLength) {
		string sText = iBridUtil.GetStringValue(pText);

		if (sText.Length > pLength) {
			return string.Concat(SysPrograms.Substring(sText,(pLength - 1)),"…");
		} else {
			return sText;
		}
	}

	protected string GetProfUrl(object pSiteCd,object pSexCd,object pLoginId) {
		string sValue = string.Empty;

		if (iBridUtil.GetStringValue(pSexCd).Equals(ViCommConst.MAN)) {
			sValue = string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",iBridUtil.GetStringValue(pSiteCd),iBridUtil.GetStringValue(pLoginId));
		} else {
			sValue = string.Format("~/Cast/CastView.aspx?loginid={0}",iBridUtil.GetStringValue(pLoginId));
		}

		return sValue;
	}

	protected string GetRequestValueUrl(object pSiteCd,object pSexCd,object pRequestSeq,string pExtension) {
		string sValue = string.Empty;
		sValue = string.Format("RequestValueList.aspx?site={0}&sexcd={1}&requestseq={2}{3}",iBridUtil.GetStringValue(pSiteCd),iBridUtil.GetStringValue(pSexCd),iBridUtil.GetStringValue(pRequestSeq),pExtension);
		return sValue;
	}

	protected void lnkPictographSample_Click(object sender,EventArgs e) {
		string sURL = string.Concat(Session["Root"],"/Site/PictographSample.aspx");
		string sScripts = string.Format("<script language=\"javascript\">window.open('{0}','','width=1060,height=900,resizable=yes,directories=no,scrollbars=yes' , false);</script>",sURL);
		ClientScript.RegisterStartupScript(Page.GetType(),"OpenNewWindow",sScripts);
	}
}
