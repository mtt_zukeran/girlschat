﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 野球拳画像
--	Progaram ID		: YakyukenPicList
--
--  Creation Date	: 2013.05.02
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;

public partial class Extension_Pwild_YakyukenPicList:System.Web.UI.Page {
	private string recCount = string.Empty;

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			DataBind();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			lstSiteCd.DataSourceID = "";
			grdYakyukenPic.PageIndex = 0;
			DataBind();
		}
	}

	protected void lnkShowFaceFlag_OnCommand(object sender,CommandEventArgs e) {
		string[] arguments = e.CommandArgument.ToString().Split(',');
		string sSiteCd = arguments[0];
		string sYakyukenPicSeq = arguments[1];
		string sOldShowFaceFlag = arguments[2];
		string sRevisionNo = arguments[3];

		int iNewShowFaceFlag = sOldShowFaceFlag.Equals(ViCommConst.FLAG_ON_STR) ? ViCommConst.FLAG_OFF : ViCommConst.FLAG_ON;

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("UPDATE_YAKYUKEN_PIC_SHOW_FACE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,sSiteCd);
			oDbSession.ProcedureInParm("pYAKYUKEN_PIC_SEQ",DbSession.DbType.VARCHAR2,sYakyukenPicSeq);
			oDbSession.ProcedureInParm("pSHOW_FACE_FLAG",DbSession.DbType.NUMBER,iNewShowFaceFlag);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,sRevisionNo);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		lstSiteCd.DataSourceID = "";
		DataBind();
	}

	protected void lnkAuth_OnCommand(object sender,CommandEventArgs e) {
		string[] arguments = e.CommandArgument.ToString().Split(',');
		string sSiteCd = arguments[0];
		string sYakyukenPicSeq = arguments[1];
		string sRevisionNo = arguments[2];

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("AUTH_YAKYUKEN_PIC");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,sSiteCd);
			oDbSession.ProcedureInParm("pYAKYUKEN_PIC_SEQ",DbSession.DbType.VARCHAR2,sYakyukenPicSeq);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,sRevisionNo);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		lstSiteCd.DataSourceID = "";
		DataBind();
	}

	protected void lnkDelete_OnCommand(object sender,CommandEventArgs e) {
		string[] arguments = e.CommandArgument.ToString().Split(',');
		string sSiteCd = arguments[0];
		string sYakyukenPicSeq = arguments[1];
		string sRevisionNo = arguments[2];

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("DELETE_YAKYUKEN_PIC");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,sSiteCd);
			oDbSession.ProcedureInParm("pYAKYUKEN_PIC_SEQ",DbSession.DbType.VARCHAR2,sYakyukenPicSeq);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,sRevisionNo);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		lstSiteCd.DataSourceID = "";
		grdYakyukenPic.PageIndex = 0;
		DataBind();
	}

	protected void dsYakyukenPic_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		YakyukenPic.SearchCondition oSearchCondition = new YakyukenPic.SearchCondition();
		oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
		oSearchCondition.LoginId = this.txtLoginId.Text.Trim();
		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsYakyukenPic_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void grdYakyukenPic_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			string sAuthFlag = DataBinder.Eval(e.Row.DataItem,"AUTH_FLAG").ToString();

			if (sAuthFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
				e.Row.BackColor = Color.LavenderBlush;
			} else {
				e.Row.BackColor = Color.White;
			}
		}
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected string GetYakyukenTypeNm(object pYakyukenType) {
		string sValue = string.Empty;
		string sYakyukenType = iBridUtil.GetStringValue(pYakyukenType);

		switch (sYakyukenType) {
			case "1":
				sValue = "洋服着用";
				break;
			case "2":
				sValue = "上半身下着";
				break;
			case "3":
				sValue = "全身下着";
				break;
			case "4":
				sValue = "ﾊﾟﾝﾃｨ姿/手ぶら";
				break;
			case "5":
				sValue = "ﾊﾟﾝﾃｨ姿/おっぱい";
				break;
			case "6":
				sValue = "裸/下陰部隠し";
				break;
		}

		return sValue;
	}

	protected string GetClientScript(object pPicUrl) {
		string sRoot = Request.Url.Authority + ConfigurationManager.AppSettings["Root"];
		return string.Format("javascript:win=window.open('http://{1}/ViewPic.html?url={0}','PictureZoom','width=300,height=320,resizable=yes,directories=no,scrollbars=no' , false);win.focus();return false;",pPicUrl,sRoot);
	}
}
