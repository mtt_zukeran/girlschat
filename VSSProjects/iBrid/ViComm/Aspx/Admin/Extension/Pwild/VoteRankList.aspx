﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="VoteRankList.aspx.cs" Inherits="Extension_Pwild_VoteRankList" Title="投票順位一覧" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="投票順位一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlRegist">
			<fieldset class="fieldset">
				<legend>[出演者追加]</legend>
				<asp:Label ID="lblRegistError" runat="server" Text="" ForeColor="red"></asp:Label>
				<table border="0" style="width: 650px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle2">
							ログインID
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtLoginId" runat="server" Width="100px" Rows="3" TextMode="MultiLine"></asp:TextBox>
						</td>
					</tr>
				</table>
				<asp:Panel ID="pnlCommand" runat="server" Width="660px">
					<asp:Button runat="server" ID="btnRegist" Text="追加" CssClass="seekbutton" OnClick="btnRegist_Click" />
					<asp:Button runat="server" ID="btnBack" Text="戻る" CssClass="seekbutton" OnClick="btnBack_Click" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlList">
			<fieldset>
				<legend>[一覧]</legend>
				<asp:Label ID="lblBulkUpdateError" runat="server" Text="" ForeColor="red"></asp:Label>
				<asp:Label ID="lblBulkUpdateComplete" runat="server" Text="一括更新が完了しました" ForeColor="blue" Visible="false"></asp:Label>
				<asp:Panel ID="pnlButtonBulkUpdate" runat="server" Width="660px">
					<asp:Button ID="btnBulkUpdate" runat="server" Text="一括更新" OnClick="btnBulkUpdate_Click" />
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">
						Record Count <%# GetRecCount() %>
					</a>
					<br />
					<a class="reccount">
						Current viewing page <%= grdVoteRank.PageIndex + 1%> of <%= grdVoteRank.PageCount%>
					</a>
				</asp:Panel>
				&nbsp;
				<asp:Panel ID="pnlGrid" runat="server">
					<asp:GridView ID="grdVoteRank" DataSourceID="dsVoteRank" runat="server" AllowPaging="True" AutoGenerateColumns="False" PageSize="50"
						EnableSortingAndPagingCallbacks="false" SkinID="GridViewColor" AllowSorting="true" OnRowDataBound="grdVoteRank_RowDataBound">
						<Columns>
							<asp:TemplateField HeaderText="順位">
								<ItemTemplate>
									<asp:Label ID="lblVoteRank" runat="server" Text='<%# Eval("VOTE_RANK") %>' Visible='<%# GetVoteRankVisible(Eval("NA_FLAG")) %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ログインID">
								<ItemTemplate>
									<asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}",Eval("LOGIN_ID")) %>' Text='<%# Eval("LOGIN_ID") %>'>
									</asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="HANDLE_NM" HeaderText="ハンドル名">
								<ItemStyle HorizontalAlign="Left" />
							</asp:BoundField>
							<asp:BoundField DataField="VOTE_COUNT" HeaderText="投票数">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:BoundField DataField="REAL_VOTE_COUNT" HeaderText="実投票数">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="加算数">
								<ItemTemplate>
									<asp:TextBox ID="txtAddVoteCount" runat="server" Text='<%# Eval("ADD_VOTE_COUNT") %>' Width="50px"></asp:TextBox>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField>
								<ItemTemplate>
									<asp:LinkButton ID="lnkDelete" runat="server" CommandArgument='<%# Container.DataItemIndex %>' OnCommand="lnkDelete_Command" Text="削除" OnClientClick="return confirm('削除します。よろしいですか？');">
									</asp:LinkButton>
									<asp:HiddenField ID="hdnVoteRankSeq" runat="server" Value='<%# Eval("VOTE_RANK_SEQ") %>' />
									<asp:HiddenField ID="hdnRevisionNo" runat="server" Value='<%# Eval("REVISION_NO") %>' />
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
			<br />
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsVoteRank" runat="server" ConvertNullToDBNull="false" EnablePaging="True" OnSelecting="dsVoteRank_Selecting" OnSelected="dsVoteRank_Selected"
		SelectCountMethod="GetPageCount" SelectMethod="GetPageCollection" SortParameterName="" TypeName="VoteRank">
		<SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnBulkUpdate" ConfirmText="一括更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>

