﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 小クエスト分布レポート

--	Progaram ID		: LittleQuestCountList
--
--  Creation Date	: 2012.12.05
--  Creater			: M&TT A.Taba
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using iBridCommLib;
using System.Drawing;
using ViComm;

public partial class Extension_Pwild_LittleQuestCountList:System.Web.UI.Page {
	protected string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		private set {
			this.ViewState["SexCd"] = value;
		}
	}

	private string QuestSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["QuestSeq"]);
		}
		set {
			this.ViewState["QuestSeq"] = value;
		}
	}

	private string LevelQuestSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["LevelQuestSeq"]);
		}
		set {
			this.ViewState["LevelQuestSeq"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		if (!this.IsPostBack) {
			this.SexCd = this.Request.QueryString["sexcd"];
			this.QuestSeq = iBridUtil.GetStringValue(this.Request.QueryString["questseq"]);
			this.LevelQuestSeq = iBridUtil.GetStringValue(this.Request.QueryString["levelquestseq"]);
			if (ViCommConst.MAN.Equals(this.SexCd)) {
				this.lblPgmTitle.Text = "男性" + this.lblPgmTitle.Text;
			} else {
				this.lblPgmTitle.Text = "女性" + this.lblPgmTitle.Text;
			}
			this.Title = this.lblPgmTitle.Text;

			this.InitPage();

			this.GetList();
			this.pnlInfo.Visible = true;
		}
	}

	private void InitPage() {
		this.grdLittleQuestCount.DataSourceID = string.Empty;
		this.pnlKey.DataBind();
		this.pnlInfo.Visible = false;
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		using (Quest oQuest = new Quest()) {
			lblQuestNm.Text = oQuest.GetQuestNm(this.lstSeekSiteCd.SelectedValue,this.QuestSeq,this.SexCd);
		}
		using (LevelQuestCount oLevelQuestCount = new LevelQuestCount()) {
			lblQuestLevel.Text = oLevelQuestCount.GetQuestLevel(this.lstSeekSiteCd.SelectedValue,this.QuestSeq,this.LevelQuestSeq);
		}
	}

	private void GetList() {
		this.grdLittleQuestCount.DataSourceID = "dsLittleQuestCount";
		this.grdLittleQuestCount.DataBind();
	}

	protected void btnCSV_Click(object sender,EventArgs e) {

		if (grdLittleQuestCount.Rows.Count == 0) {
			return;
		}

		DataTable oCsvData;
		using (LittleQuestCount oLittleQuestCount = new LittleQuestCount()) {
			using (DataSet oDataSet = oLittleQuestCount.GetList(
				this.lstSeekSiteCd.SelectedValue,
				this.QuestSeq,
				this.LevelQuestSeq,
				this.SexCd

				)) {

				if (oDataSet.Tables[0].Rows.Count > 0) {
					oCsvData = oDataSet.Tables[0];
				} else {
					return;
				}
			}
		}

		//ヘッダ作成
		string sHeader = "レベル,小クエスト名,エントリー数,クリア人数,リタイヤ人数";

		//CSV出力 
		Response.ContentType = "application/download";
		Response.AppendHeader("Content-Disposition","attachment;filename=LittleQuestCount.csv");
		Response.ContentEncoding = System.Text.Encoding.GetEncoding("Shift_JIS");

		Response.Write(sHeader + "\r\n");
		foreach (DataRow oCsvRow in oCsvData.Rows) {
			string sData =
				SetCsvString(oCsvRow["LEVEL_QUEST_SUB_SEQ"].ToString()) + "," +
				SetCsvString(oCsvRow["LITTLE_QUEST_NM"].ToString()) + "," +
				SetCsvString(oCsvRow["ENTRY_COUNT"].ToString()) + "," +
				SetCsvString(oCsvRow["CLEAR_COUNT"].ToString()) + "," +
				SetCsvString(oCsvRow["RETIRE_COUNT"].ToString());

			Response.Write(sData + "\r\n");
		}
		Response.End();
	}

	private string SetCsvString(string pData) {
		if (pData.IndexOf('"') > -1 ||
			pData.IndexOf(',') > -1 ||
			pData.IndexOf('\r') > -1 ||
			pData.IndexOf('\n') > -1 ||
			pData.StartsWith(" ") || pData.StartsWith("\t") ||
			pData.EndsWith(" ") || pData.EndsWith("\t")) {
			if (pData.IndexOf('"') > -1) {
				//"を""とする
				pData = pData.Replace("\"","\"\"");
			}
			if (pData.IndexOf('\r') > -1 || pData.IndexOf('\n') > -1) {
				//改行しない 
				pData = pData.Replace("\r","");
				pData = pData.Replace("\n","");
			}
			pData = "\"" + pData + "\"";
		}
		return pData;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		this.GetList();
		this.pnlInfo.Visible = true;
	}

	protected void btnBack_Click(object sender,EventArgs e) {
		string sBackPath = string.Empty;

		sBackPath = string.Format("LevelQuestCountList.aspx?questseq={0}&sexcd={1}",this.QuestSeq,this.SexCd);

		this.Response.Redirect(string.Format("~/Extension/Pwild/{0}",sBackPath));
	}
}