﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PageViewSummaryTargetList.aspx.cs" Inherits="Extension_Pwild_PageViewSummaryTargetList" Title="ページビュー集計対象設定" %>
<%@ Import Namespace="ViComm" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="ページビュー集計対象設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<%-- ============================== --%>
		<%--  Search Condition              --%>
		<%-- ============================== --%>
		<asp:Panel runat="server" ID="pnlSeekCondition">
			<fieldset class="fieldset">
				<legend>[検索条件]</legend>
				<asp:Panel ID="pnlSeekConditionDtl" runat="server">
					<table border="0" style="width: 800px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイトコード
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
								DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							画面名称
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtProgramNm" runat="server" Width="200px" MaxLength="80"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							プログラムID
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtProgramId" runat="server" Width="200px" MaxLength="64"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							HTML文章コード
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtHtmlDocType" runat="server" Width="50px" MaxLength="4"></asp:TextBox>
							<ajaxToolkit:FilteredTextBoxExtender ID="fltHtmlDocType" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtHtmlDocType" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							性別
						</td>
						<td class="tdDataStyle">
							<asp:RadioButtonList ID="rdoSexCd" runat="server" RepeatDirection="horizontal">
								<asp:ListItem Selected="true" Text="全て" Value=""></asp:ListItem>
								<asp:ListItem Text="男性" Value="1"></asp:ListItem>
								<asp:ListItem Text="女性" Value="3"></asp:ListItem>
							</asp:RadioButtonList>
						</td>
					</tr>
					</table>
				</asp:Panel>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
					ValidationGroup="Key" CausesValidation="True" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
					CausesValidation="False" />
				<asp:Button runat="server" ID="btnAppend" Text="対象追加" CssClass="seekbutton" OnClick="btnAppend_Click"
					CausesValidation="False" />
			</fieldset>
		</asp:Panel>

		<%-- ============================== --%>
		<%--  Regist Form                   --%>
		<%-- ============================== --%>
		<asp:Panel runat="server" ID="pnlAppend" Visible="false">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel ID="pnlAppendDtl" runat="server">
					<table border="0" style="width: 800px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							プログラムID
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtInProgramId" runat="server" Width="200px" MaxLength="64"></asp:TextBox>
							<asp:CustomValidator ID="vdcInProgramId" runat="server" ControlToValidate="txtInProgramId" ErrorMessage='プログラムIDかHTML文章コードのいずれかを入力してください' ValidationGroup="Detail">*</asp:CustomValidator>
							<asp:CustomValidator ID="vdcInProgramIdExists" runat="server" ControlToValidate="txtInProgramId" ErrorMessage='登録済みのプログラムIDを入力してください' ValidationGroup="Detail">*</asp:CustomValidator>
							<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdcInProgramIdExists" HighlightCssClass="validatorCallout" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							HTML文章コード
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtInHtmlDocType" runat="server" Width="50px" MaxLength="4"></asp:TextBox>
							<ajaxToolkit:FilteredTextBoxExtender ID="fltInHtmlDocType" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtInHtmlDocType" />
							<asp:CustomValidator ID="vdcInHtmlDocType" runat="server" ControlToValidate="txtInHtmlDocType" ErrorMessage='プログラムIDかHTML文章コードのいずれかを入力してください' ValidationGroup="Detail">*</asp:CustomValidator>
							<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdcInHtmlDocType" HighlightCssClass="validatorCallout" />
							<asp:CustomValidator ID="vdcInHtmlDocTypeExists" runat="server" ControlToValidate="txtInHtmlDocType" ErrorMessage='登録済みのHTML文章コードを入力してください' ValidationGroup="Detail">*</asp:CustomValidator>
							<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdcInHtmlDocTypeExists" HighlightCssClass="validatorCallout" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							性別
						</td>
						<td class="tdDataStyle">
							<asp:RadioButtonList ID="rdoInSexCd" runat="server" RepeatDirection="horizontal">
								<asp:ListItem Selected="true" Text="全て" Value=""></asp:ListItem>
								<asp:ListItem Text="男性" Value="1"></asp:ListItem>
								<asp:ListItem Text="女性" Value="3"></asp:ListItem>
							</asp:RadioButtonList>
						</td>
					</tr>
					</table>
				</asp:Panel>
				<br />
				<asp:Button runat="server" ID="btnRegist" Text="登録" CssClass="seekbutton" OnClick="btnRegist_Click"
					ValidationGroup="Key" CausesValidation="True" />
				<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnRegist" ConfirmText="登録を行ないますか？" ConfirmOnFormSubmit="true" />
			</fieldset>
		</asp:Panel>

		<%-- ============================== --%>
		<%--  Data                          --%>
		<%-- ============================== --%>
		<asp:Panel runat="server" ID="pnlList" Visible="false">
			<fieldset>
				<legend>[一覧]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count
						<%# GetRecCount() %>
					</a>
					<br />
					<a class="reccount">Current viewing page
						<%= grdPageViewTarget.PageIndex + 1%>
						of
						<%= grdPageViewTarget.PageCount%>
					</a>
				</asp:Panel>
				&nbsp;<br />
				<asp:Panel ID="pnlGrid" runat="server">
					<asp:GridView ID="grdPageViewTarget" DataSourceID="" runat="server" AllowPaging="true" AutoGenerateColumns="False"  PageSize="50"
						EnableSortingAndPagingCallbacks="false" ShowFooter="false" SkinID="GridViewColor" AllowSorting="true" OnRowDataBound="grdPageViewTarget_RowDataBound">
						<Columns>
							<asp:TemplateField HeaderText="画面名称">
								<ItemTemplate>
									<asp:HyperLink ID="lnkProgramNm" runat="server"
										NavigateUrl='<%# GetNavigateUrlPageMainte(Eval("PROGRAM_ROOT"),Eval("PROGRAM_ID"),Eval("HTML_DOC_TYPE")) %>'
										Text='<%# Eval("PROGRAM_NM") %>'>
									</asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="プログラムID">
								<ItemTemplate>
									<asp:Label ID="lblProgramId" runat="server" Text='<%# Eval("PROGRAM_ID") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="HTML文章コード">
								<ItemTemplate>
									<asp:Label ID="lblHtmlDocType" runat="server" Text='<%# Eval("HTML_DOC_TYPE") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="性別">
								<ItemTemplate>
									<asp:Label ID="lblSexNm" runat="server" Text='<%# Eval("SEX_NM") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="操作">
								<ItemTemplate>
									<asp:LinkButton ID="lnkDelete" runat="server" Text="削除"
										CommandArgument='<%# string.Format("{0}:{1}:{2}:{3}",Eval("SITE_CD"),Eval("PROGRAM_ROOT"),Eval("PROGRAM_ID"),Eval("HTML_DOC_TYPE")) %>'
										OnCommand="lnkDelete_Command" CausesValidation="False">
									</asp:LinkButton>
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsPageViewTarget" runat="server" ConvertNullToDBNull="false" EnablePaging="true" OnSelecting="dsPageViewTarget_Selecting" OnSelected="dsPageViewTarget_Selected"
		SelectMethod="GetPageCollection" SelectCountMethod="GetPageCount" SortParameterName="" TypeName="PageViewTarget">
		<SelectParameters>
			<asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
