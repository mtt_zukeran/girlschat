﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: イベント情報一覧

--	Progaram ID		: EventList
--
--  Creation Date	: 2013.07.04
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.IO;
using ViComm.Extension.Pwild;

public partial class Extension_Pwild_EventList:System.Web.UI.Page {
	protected static readonly string[] DayArray = new string[] { "--","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31" };
	protected static readonly string[] MonthArray = new string[] { "--","01","02","03","04","05","06","07","08","09","10","11","12" };
	protected string[] YearArray;
	protected string[] YearArraySearch;
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string EventSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["EventSeq"]);
		}
		set {
			this.ViewState["EventSeq"] = value;
		}
	}

	private string PrefectureCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["PrefectureCd"]);
		}
		set {
			this.ViewState["PrefectureCd"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.FirstLoad();
			this.InitPage();
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
		this.EventSeq = string.Empty;
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		if (!this.UpdateData(false)) {
			return;
		}
		this.pnlEventInfo.Visible = false;
		this.pnlKey.Enabled = true;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		this.UpdateData(true);
		this.pnlEventInfo.Visible = false;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.ClearFileds();
		this.pnlKey.Enabled = true;
		this.pnlEventInfo.Visible = false;
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.EventSeq = string.Empty;
		this.ClearFileds();
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.pnlKey.Enabled = false;
		this.btnDelete.Enabled = false;
		this.pnlEventInfo.Visible = true;
	}

	protected void lnkEventNm_Command(object sender,CommandEventArgs e) {

		if (!this.IsValid) {
			return;
		}
		this.EventSeq = iBridUtil.GetStringValue(e.CommandArgument);
		this.GetData();
		this.pnlKey.Enabled = false;
		this.btnDelete.Enabled = true;
		this.pnlEventInfo.Visible = true;

	}

	protected void dsEvent_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsEvent_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		this.lblErrorMessageSearchFrom.Visible = false;
		this.lblErrorMessageSearchTo.Visible = false;
		Event.SearchCondition oSearchCondition = new Event.SearchCondition();		
		oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
		oSearchCondition.EventCategorySeq = this.lstEventCategorySearch.SelectedValue;
		oSearchCondition.PrefectureCd = this.lstPrefectureCdSearch.SelectedValue;
		oSearchCondition.EventAreaCd = this.lstEventAreaCdSearch.SelectedValue;
		oSearchCondition.EventNm = this.txtEventNmSearch.Text.Trim();

		DateTime dtFrom = new DateTime();
		if (lstYYYYSearchFrom.SelectedIndex != 0 || lstMMSearchFrom.SelectedIndex != 0 || lstDDSearchFrom.SelectedIndex != 0) {
			if (DateTime.TryParse(string.Format("{0}/{1}/{2}",
					this.lstYYYYSearchFrom.SelectedValue,this.lstMMSearchFrom.SelectedValue,this.lstDDSearchFrom.SelectedValue),out dtFrom)) {
				oSearchCondition.EventDateFrom = string.Format("{0}/{1}/{2}",this.lstYYYYSearchFrom.SelectedValue,this.lstMMSearchFrom.SelectedValue,this.lstDDSearchFrom.SelectedValue);
			} else {
				lblErrorMessageSearchFrom.Text = "開催期間(開始)を正しく入力してください。";
				lblErrorMessageSearchFrom.Visible = true;
			}
		}

		DateTime dtTo = new DateTime();
		if (lstYYYYSearchTo.SelectedIndex != 0 || lstMMSearchTo.SelectedIndex != 0 || lstDDSearchTo.SelectedIndex != 0) {
			if (DateTime.TryParse(string.Format("{0}/{1}/{2}",
					this.lstYYYYSearchTo.SelectedValue,this.lstMMSearchTo.SelectedValue,this.lstDDSearchTo.SelectedValue),out dtTo)) {
				oSearchCondition.EventDateTo = string.Format("{0}/{1}/{2}",this.lstYYYYSearchTo.SelectedValue,this.lstMMSearchTo.SelectedValue,this.lstDDSearchTo.SelectedValue);
			} else {
				lblErrorMessageSearchTo.Text = "開催期間(終了)を正しく入力してください。";
				lblErrorMessageSearchTo.Visible = true;
			}
		}
		
		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsEventArea_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pPrefectureCd"] = this.PrefectureCd;
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	protected void lstPrefectureCd_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		this.PrefectureCd = oDropDownList.SelectedValue;
		this.lstEventAreaCd.DataBind();
	}

	protected void lstPrefectureCdSearch_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		this.PrefectureCd = oDropDownList.SelectedValue;
		this.lstEventAreaCdSearch.DataBind();
	}

	private void InitPage() {
		this.pnlInfo.Visible = true;
		this.pnlEventInfo.Visible = false;

		this.EventSeq = string.Empty;
		this.pnlKey.Enabled = true;
		this.lstSiteCd.DataBind();
		this.lstYYYYSearchFrom.DataBind();
		this.lstMMSearchFrom.DataBind();
		this.lstDDSearchFrom.DataBind();
		this.lstYYYYSearchTo.DataBind();
		this.lstMMSearchTo.DataBind();
		this.lstDDSearchTo.DataBind();
		this.lstEventCategorySearch.DataBind();
		this.lstEventCategorySearch.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void FirstLoad() {
		this.YearArray = new string[] { "----",DateTime.Today.ToString("yyyy"),DateTime.Today.AddYears(1).ToString("yyyy"),DateTime.Today.AddYears(2).ToString("yyyy") };
		this.YearArraySearch = new string[] { "----",DateTime.Today.AddYears(-1).ToString("yyyy"),DateTime.Today.ToString("yyyy"),DateTime.Today.AddYears(1).ToString("yyyy"),DateTime.Today.AddYears(2).ToString("yyyy") };
		this.pnlEventInfo.DataBind();
	}

	private void ClearFileds() {

		this.pnlKey.Enabled = true;
		this.lstEventCategory.DataBind();
		this.lstPrefectureCd.DataBind();
		this.lstEventAreaCd.DataBind();
		this.lstFromYYYY.SelectedIndex = 0;
		this.lstFromMM.SelectedIndex = 0;
		this.lstFromDD.SelectedIndex = 0;
		this.lstToYYYY.SelectedIndex = 0;
		this.lstToMM.SelectedIndex = 0;
		this.lstToDD.SelectedIndex = 0;
		this.txtEventNm.Text = string.Empty;
		this.txtEventDetail.Text = string.Empty;

		this.lblErrorMessageFrom.Visible = false;
		this.lblErrorMessageTo.Visible = false;
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.EventSeq = string.Empty;

		this.grdEvent.PageIndex = 0;
		this.grdEvent.DataSourceID = "dsEvent";
		this.grdEvent.DataBind();
		this.pnlCount.DataBind();
	}

	private void GetData() {
		this.ClearFileds();

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("EVENT_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pEVENT_SEQ",DbSession.DbType.VARCHAR2,this.EventSeq);
			oDbSession.ProcedureOutParm("pEVENT_NM",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pEVENT_CATEGORY_SEQ",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPREFECTURE_CD",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pEVENT_AREA_CD",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pEVENT_START_DATE",DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pEVENT_END_DATE",DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pEVENT_DETAIL",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			string sFromDate = string.Format("{0:yyyy/MM/dd}",oDbSession.GetDateTimeValue("pEVENT_START_DATE"));
			string sToDate = string.Format("{0:yyyy/MM/dd}",oDbSession.GetDateTimeValue("pEVENT_END_DATE"));
			if (!string.IsNullOrEmpty(sFromDate)) {
				this.lstFromYYYY.SelectedValue = sFromDate.Substring(0,4);
				this.lstFromMM.SelectedValue = sFromDate.Substring(5,2);
				this.lstFromDD.SelectedValue = sFromDate.Substring(8,2);
			}
			if (!string.IsNullOrEmpty(sToDate)) {
				this.lstToYYYY.SelectedValue = sToDate.Substring(0,4);
				this.lstToMM.SelectedValue = sToDate.Substring(5,2);
				this.lstToDD.SelectedValue = sToDate.Substring(8,2);
			}
			this.txtEventNm.Text = oDbSession.GetStringValue("pEVENT_NM");
			this.lstEventCategory.SelectedValue = oDbSession.GetStringValue("pEVENT_CATEGORY_SEQ");
			this.txtEventDetail.Text = oDbSession.GetStringValue("pEVENT_DETAIL");
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
			
			if (!oDbSession.GetStringValue("pPREFECTURE_CD").Equals(string.Empty)) {
				this.lstPrefectureCd.SelectedValue = oDbSession.GetStringValue("pPREFECTURE_CD");
				this.PrefectureCd = this.lstPrefectureCd.SelectedValue;
				this.lstEventAreaCd.DataBind();
				this.lstEventAreaCd.SelectedValue = oDbSession.GetStringValue("pEVENT_AREA_CD");
			}
		}
	}

	private bool UpdateData(bool pDelFlag) {
		string sFrom = string.Empty;
		string sTo = string.Empty;
		if (!pDelFlag) {
			if (!this.CheckDate(out sFrom,out sTo)) {
				return false;
			}
		}
		
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("EVENT_MAINTE");
			oDbSession.cmd.BindByName = true;
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pEVENT_SEQ",DbSession.DbType.VARCHAR2,this.EventSeq);
			oDbSession.ProcedureInParm("pEVENT_NM",DbSession.DbType.VARCHAR2,this.txtEventNm.Text);
			oDbSession.ProcedureInParm("pEVENT_CATEGORY_SEQ",DbSession.DbType.VARCHAR2,this.lstEventCategory.SelectedValue);
			oDbSession.ProcedureInParm("pEVENT_AREA_CD",DbSession.DbType.VARCHAR2,this.lstEventAreaCd.SelectedValue);
			oDbSession.ProcedureInParm("pEVENT_START_DATE",DbSession.DbType.VARCHAR2,sFrom);
			oDbSession.ProcedureInParm("pEVENT_END_DATE",DbSession.DbType.VARCHAR2,sTo);
			oDbSession.ProcedureInParm("pEVENT_DETAIL",DbSession.DbType.VARCHAR2,this.txtEventDetail.Text);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.ClearFileds();
		this.GetList();
		return true;
	}
	
	private bool CheckDate(out string sFrom,out string sTo) {

		DateTime dtFrom = new DateTime();
		DateTime dtTo = new DateTime();
		sFrom = string.Empty;
		sTo = string.Empty;

		this.lblErrorMessageFrom.Visible = false;
		this.lblErrorMessageTo.Visible = false;
		if (this.lstFromYYYY.SelectedIndex != 0 || this.lstFromMM.SelectedIndex != 0 || this.lstFromDD.SelectedIndex != 0) {
			if (!DateTime.TryParse(string.Format("{0}/{1}/{2}",
				this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue),out dtFrom)) {
				this.lblErrorMessageFrom.Text = "開始日を正しく入力してください";
				this.lblErrorMessageFrom.Visible = true;
				return false;
			}
			sFrom = dtFrom.ToString("yyyy/MM/dd");
		}

		if (this.lstToYYYY.SelectedIndex != 0 || this.lstToMM.SelectedIndex != 0 || this.lstToDD.SelectedIndex != 0) {
			if (!DateTime.TryParse(string.Format("{0}/{1}/{2}",
				this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,this.lstToDD.SelectedValue),out dtTo)) {
				this.lblErrorMessageTo.Text = "終了日を正しく入力してください";
				this.lblErrorMessageTo.Visible = true;
				return false;
			}
			sTo = dtTo.ToString("yyyy/MM/dd");
		}

		if (!string.IsNullOrEmpty(sFrom) && !string.IsNullOrEmpty(sTo)) {
			if (dtFrom > dtTo) {
				this.lblErrorMessageTo.Text = "日付の大小関係を正しく入力してください";
				this.lblErrorMessageTo.Visible = true;
				return false;
			}
		}

		return true;
	}
}
