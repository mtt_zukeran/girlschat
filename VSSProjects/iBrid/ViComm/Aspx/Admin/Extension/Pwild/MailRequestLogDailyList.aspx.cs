﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メールおねだり集計(日別)
--	Progaram ID		: MailRequestLogDailyList
--
--  Creation Date	: 2014.06.18
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.IO;
using iBridCommLib;
using ViComm;

public partial class Extension_Pwild_MailRequestLogDailyList:System.Web.UI.Page {
	private string[] Week = { "日","月","火","水","木","金","土" };

	private double dTotalCount = 0;
	private double dValidCount = 0;
	private double dNoMailCount = 0;
	private double dRxMailCount = 0;
	private double dUserCount = 0;
	private double dManTxMailCount = 0;
	private Stream filter;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SITE_CD"]);
		}
		set {
			this.ViewState["SITE_CD"] = value;
		}
	}

	private string Year {
		get {
			return iBridUtil.GetStringValue(this.ViewState["YEAR"]);
		}
		set {
			this.ViewState["YEAR"] = value;
		}
	}

	private string Month {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MONTH"]);
		}
		set {
			this.ViewState["MONTH"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			this.Year = iBridUtil.GetStringValue(Request.QueryString["year"]);
			this.Month = iBridUtil.GetStringValue(Request.QueryString["month"]);

			this.CreateList();
			this.InitPage();

			if (!string.IsNullOrEmpty(this.Year) && !string.IsNullOrEmpty(this.Month)) {
				this.lstYear.SelectedValue = this.Year;
				this.lstMonth.SelectedValue = this.Month;
			}

			pnlGrid.DataBind();
		}
	}

	private void InitPage() {
		this.lstYear.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstMonth.SelectedValue = DateTime.Now.ToString("MM");
	}

	private void ClearField() {
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnCSV_Click(object sender,EventArgs e) {

		string sSiteCd = this.lstSiteCd.SelectedValue;
		string sYear = this.lstYear.SelectedValue;
		string sMonth = this.lstMonth.SelectedValue;
		
		Response.Filter = filter;
		Response.AddHeader("Content-Disposition",string.Format("attachment;filename=MAIL_REQUEST_LOG_DAILY_{0}_{1}{2}.CSV",lstSiteCd.SelectedValue,sYear,sMonth));
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		DataSet ds;
		using (MailRequestLog oMailRequestLog = new MailRequestLog()) {
			ds = oMailRequestLog.GetDailyRegistCount(sSiteCd,sYear,sMonth);
			SetCsvData(ds);
			Response.End();
		}
	}

	public void SetCsvData(DataSet pDataSet) {

		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		string sHeaderString = "日付,曜日,送信件数,有効送信件数,ﾒｰﾙ未送信数,ﾒｰﾙ送信数,おねだり送信者数,出演者ﾒｰﾙ送信率,会員ﾒｰﾙ送信率\r\n";
		Response.BinaryWrite(encoding.GetBytes(sHeaderString));

		string sDtl = "";

		DataRow dr = null;
		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];

			sDtl = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8}",
							dr["DAYS"].ToString(),
							GetDisplayDayOfWeek(dr["DAYS"].ToString()),
							dr["TOTAL_COUNT"].ToString(),
							dr["VALID_COUNT"].ToString(),
							dr["NO_MAIL_COUNT"].ToString(),
							dr["RX_MAIL_COUNT"].ToString(),
							dr["UNIQUE_USER_COUNT"].ToString(),
							GetRxMailRate(dr["VALID_COUNT"].ToString(),dr["RX_MAIL_COUNT"].ToString()),
							GetManTxMailRate(dr["RX_MAIL_COUNT"].ToString(),dr["MAN_TX_MAIL_COUNT"].ToString())
							);
			sDtl += "\r\n";
			Response.BinaryWrite(encoding.GetBytes(sDtl));
		}
	}

	protected void grdMailRequestLog_RowDataBound(object sender,GridViewRowEventArgs e) {

		if (e.Row.RowType == DataControlRowType.DataRow) {
			DataRowView drv = (DataRowView)e.Row.DataItem;
			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(drv["TOTAL_COUNT"]))) {
				dTotalCount = dTotalCount + int.Parse(iBridUtil.GetStringValue(drv["TOTAL_COUNT"]));
				dValidCount = dValidCount + int.Parse(iBridUtil.GetStringValue(drv["VALID_COUNT"]));
				dNoMailCount = dNoMailCount + int.Parse(iBridUtil.GetStringValue(drv["NO_MAIL_COUNT"]));
				dRxMailCount = dRxMailCount + int.Parse(iBridUtil.GetStringValue(drv["RX_MAIL_COUNT"]));
				dUserCount = dUserCount + int.Parse(iBridUtil.GetStringValue(drv["UNIQUE_USER_COUNT"]));
				dManTxMailCount = dManTxMailCount + int.Parse(iBridUtil.GetStringValue(drv["MAN_TX_MAIL_COUNT"]));
			}

			DateTime dtReportDay = DateTime.Parse(drv["DAYS"].ToString());
			int iDayOfWeek = (int)dtReportDay.DayOfWeek;

			if (iDayOfWeek == 0) {
				e.Row.BackColor = Color.FromArgb(0xFFCCCC);
			} else if (iDayOfWeek == 6) {
				e.Row.BackColor = Color.FromArgb(0xCCCCFF);
			}
		} else if (e.Row.RowType == DataControlRowType.Footer) {
			DateTime dtSelectedmonth = DateTime.Parse(string.Format("{0}/{1}/01",lstYear.SelectedValue,lstMonth.SelectedValue));
			DateTime dtNowMonth = DateTime.Parse(string.Format("{0}/{1}/01",DateTime.Now.ToString("yyyy"),DateTime.Now.ToString("MM")));

			int iDaysInMonth = DateTime.DaysInMonth(dtSelectedmonth.Year,dtSelectedmonth.Month);
			int iPastDays = 0;

			if (dtSelectedmonth == dtNowMonth) {
				iPastDays = DateTime.Now.Day - 1;
			} else {
				iPastDays = iDaysInMonth;
			}

			Label lblTotalCountFooter = (Label)e.Row.FindControl("lblTotalCountFooter");
			Label lblValidCountFooter = (Label)e.Row.FindControl("lblValidCountFooter");
			Label lblNoMailCountFooter = (Label)e.Row.FindControl("lblNoMailCountFooter");
			Label lblRxMailCountFooter = (Label)e.Row.FindControl("lblRxMailCountFooter");
			Label lblUniqueUserCountFooter = (Label)e.Row.FindControl("lblUniqueUserCountFooter");
			Label lblCastTxMailRateFooter = (Label)e.Row.FindControl("lblCastTxMailRateFooter");
			Label lblManTxMailRateFooter = (Label)e.Row.FindControl("lblManTxMailRateFooter");

			lblTotalCountFooter.Text = dTotalCount.ToString();
			lblValidCountFooter.Text = dValidCount.ToString();
			lblNoMailCountFooter.Text = dNoMailCount.ToString();
			lblRxMailCountFooter.Text = dRxMailCount.ToString();
			lblUniqueUserCountFooter.Text = dUserCount.ToString();

			if (dValidCount > 0) {
				double dRxMailRate;
				dRxMailRate = dRxMailCount / dValidCount * 100;
				dRxMailRate = Math.Round(dRxMailRate,2);
				lblCastTxMailRateFooter.Text = string.Format("{0}%",dRxMailRate);
			} else {
				lblCastTxMailRateFooter.Text = "-";
			}
			
			if (dRxMailCount > 0) {
				double dManTxMailRate;
				dManTxMailRate = dManTxMailCount / dRxMailCount * 100;
				dManTxMailRate = Math.Round(dManTxMailRate,2);
				lblManTxMailRateFooter.Text = string.Format("{0}%",dManTxMailRate);
			} else {
				lblManTxMailRateFooter.Text = "-";
			}
		}
	}

	private void GetList() {

		grdMailRequestLog.PageIndex = 0;
		grdMailRequestLog.DataSourceID = "dsMailRequestLog";
		DataBind();
	}

	protected Color GetBackColor(object pDayOfWeek) {
		if (pDayOfWeek.ToString().Equals("Sat")) {
			return Color.FromArgb(0xCCCCFF);
		} else {
			if (pDayOfWeek.ToString().Equals("Sun")) {
				return Color.FromArgb(0xFFCCCC);
			} else {
				return Color.Empty;
			}
		}
	}

	private void CreateList() {
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		int iStartYear = 2015;
		int iCurYear = DateTime.Now.Year;
		lstYear.Items.Clear();

		for (int i = 0;i + iStartYear <= iCurYear;i++) {

			lstYear.Items.Add(new ListItem(iBridUtil.GetStringValue(i + iStartYear)));
		}

		lstMonth.Items.Clear();
		for (int i = 1;i <= 12;i++) {
			lstMonth.Items.Add(new ListItem(string.Format("{0:D2}",i)));
		}
	}

	private bool IsCorrectDate() {
		return true;
	}

	protected string GetDisplayReportDay(string pReportDay) {
		DateTime dtReportDay = DateTime.Parse(pReportDay);
		return dtReportDay.Day.ToString();
	}

	protected string GetDisplayDayOfWeek(string pReportDay) {
		DateTime dtReportDay = DateTime.Parse(pReportDay);
		int iDayOfWeekNo = (int)dtReportDay.DayOfWeek;

		return Week[iDayOfWeekNo];
	}
	
	protected string GetRxMailRate(string pTotalCount,string pRxMailCount) {
		double dTotalCount;
		double dRxMailCount;
		string sValue = "-";

		if (double.TryParse(pTotalCount,out dTotalCount) && double.TryParse(pRxMailCount,out dRxMailCount) && dTotalCount > 0) {
			double dValue = dRxMailCount / dTotalCount * 100;
			
			dValue = Math.Round(dValue,2);
			sValue = string.Format("{0}%",dValue.ToString());
		}
		
		return sValue;
	}
	
	protected string GetManTxMailRate(string pRxMailCount,string pManTxMailCount) {
		double dRxMailCount;
		double dManTxMailCount;
		string sValue = "-";

		if (double.TryParse(pRxMailCount,out dRxMailCount) && double.TryParse(pManTxMailCount,out dManTxMailCount) && dRxMailCount > 0) {
			double dValue = dManTxMailCount / dRxMailCount * 100;

			dValue = Math.Round(dValue,2);
			sValue = string.Format("{0}%",dValue.ToString());
		}

		return sValue;
	}
}
