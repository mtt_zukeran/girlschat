﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UserPageViewHourlyList.aspx.cs" Inherits="Extension_Pwild_UserPageViewHourlyList" Title="会員ページビュー(時間別)" %>
<%@ Import Namespace="ViComm" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="会員ページビュー(時間別)"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[検索条件]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 800px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="tdHeaderStyle">
                                アクセス種別
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstAccessType" runat="server">
                                    <asp:ListItem Value="1" Text="ｱｸｾｽ総数"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="ｱｸｾｽUU"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="1stｱｸｾｽ"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="2ndｱｸｾｽ"></asp:ListItem>
                                    <asp:ListItem Value="5" Text="3rdｱｸｾｽ"></asp:ListItem>
                                    <asp:ListItem Value="6" Text="4thｱｸｾｽ"></asp:ListItem>
                                    <asp:ListItem Value="7" Text="5thｱｸｾｽ"></asp:ListItem>
                                    <asp:ListItem Value="8" Text="1st～5thｱｸｾｽ"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                アクセス年月日
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstYear" runat="server" Width="60px">
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstMonth" runat="server" Width="40px">
                                </asp:DropDownList>月
                                <asp:DropDownList ID="lstDay" runat="server" Width="40px">
                                </asp:DropDownList>日
                                <br />
								<asp:Label ID="lblErrorMessageReportDay" runat="server" ForeColor="red" Visible="false"></asp:Label>
                            </td>
                            <td class="tdHeaderStyle">
                                ログイン種別
                            </td>
                            <td class="tdDataStyle">
								<asp:DropDownList ID="lstLoginType" runat="server">
								    <asp:ListItem Value="" Text="指定しない"></asp:ListItem>
								    <asp:ListItem Value="0" Text="登録前"></asp:ListItem>
								    <asp:ListItem Value="1" Text="登録後"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                端末種別
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstPageUserAgentType" runat="server">
                                    <asp:ListItem Value="" Text="指定しない"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="3G端末"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="ｽﾏｰﾄﾌｫﾝ"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="Android"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="iPhone"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="tdHeaderStyle">
                                0アクセス
                            </td>
                            <td class="tdDataStyle">
                                <asp:RadioButtonList ID="rdoZeroAccessNotDisp" runat="server" RepeatDirection="horizontal">
                                    <asp:ListItem Text="非表示" Value="1" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="表示" Value="0"></asp:ListItem>
							    </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                会員ランク
                            </td>
                            <td class="tdDataStyle">
                                <asp:RadioButtonList ID="rdoUserRank" runat="server" RepeatDirection="horizontal">
                                    <asp:ListItem Text="指定しない" Value="" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="振無" Value="A"></asp:ListItem>
                                    <asp:ListItem Text="振有" Value="B"></asp:ListItem>
							    </asp:RadioButtonList>
                            </td>
                            <td class="tdHeaderStyle">
                                新人フラグ
                            </td>
                            <td class="tdDataStyle">
                                <asp:RadioButtonList ID="rdoNewUserFlag" runat="server" RepeatDirection="horizontal">
                                    <asp:ListItem Text="指定しない" Value="" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="有" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="無" Value="0"></asp:ListItem>
							    </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle2">
                                ユーザー端末
                            </td>
                            <td class="tdDataStyle2" colspan="3">
                                <asp:DropDownList ID="lstCarrierType" runat="server">
                                    <asp:ListItem Value="" Text="指定しない"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="3G端末"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Android"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="iPhone"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" />
				<asp:Button runat="server" ID="btnCsv" Text="ＣＳＶ出力" CssClass="seekbutton" OnClick="btnCSV_Click" />
            </fieldset>
        </asp:Panel>
		<asp:Panel runat="server" ID="pnlList">
			<fieldset>
				<legend>[ページビュー]</legend>
				<asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdUserPageViewHourly.PageIndex + 1%>
                        of
                        <%= grdUserPageViewHourly.PageCount%>
                    </a>
                </asp:Panel>
                &nbsp;<br />
				<asp:Panel ID="pnlGrid" runat="server">
					<asp:GridView ID="grdUserPageViewHourly" DataSourceID="dsUserPageViewHourly" runat="server" AllowPaging="true" AutoGenerateColumns="False" PageSize="50"
						EnableSortingAndPagingCallbacks="false" ShowFooter="true" SkinID="GridViewColor" AllowSorting="true" OnRowDataBound="grdUserPageViewHourly_RowDataBound">
						<Columns>
							<asp:TemplateField HeaderText="画面名称">
								<ItemTemplate>
									<asp:HyperLink ID="lnkProgramNm" runat="server" NavigateUrl='<%# GetNavigateUrlPageMainte(Eval("PROGRAM_ROOT"),Eval("PROGRAM_ID"),Eval("HTML_DOC_TYPE")) %>' Text='<%# GetPageNm(Eval("PROGRAM_ROOT"),Eval("HTML_DOC_TYPE"),Eval("PROGRAM_NM")) %>' ></asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="期間">
								<ItemTemplate>
									<asp:HyperLink ID="lnkTerm" runat="server" NavigateUrl='<%# GetNavigateUrl(Eval("PROGRAM_ROOT"),Eval("PROGRAM_ID"),Eval("HTML_DOC_TYPE")) %>' Text='■'>
									</asp:HyperLink>
									<ItemStyle HorizontalAlign="Center" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="TOTAL_COUNT" HeaderText="合計">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="0">
								<ItemTemplate>
									<asp:Label ID="lblCount00" runat="server" Text='<%# Eval("COUNT00") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="1">
								<ItemTemplate>
									<asp:Label ID="lblCount01" runat="server" Text='<%# Eval("COUNT01") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="2">
								<ItemTemplate>
									<asp:Label ID="lblCount02" runat="server" Text='<%# Eval("COUNT02") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="3">
								<ItemTemplate>
									<asp:Label ID="lblCount03" runat="server" Text='<%# Eval("COUNT03") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="4">
								<ItemTemplate>
									<asp:Label ID="lblCount04" runat="server" Text='<%# Eval("COUNT04") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="5">
								<ItemTemplate>
									<asp:Label ID="lblCount05" runat="server" Text='<%# Eval("COUNT05") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="6">
								<ItemTemplate>
									<asp:Label ID="lblCount06" runat="server" Text='<%# Eval("COUNT06") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="7">
								<ItemTemplate>
									<asp:Label ID="lblCount07" runat="server" Text='<%# Eval("COUNT07") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="8">
								<ItemTemplate>
									<asp:Label ID="lblCount08" runat="server" Text='<%# Eval("COUNT08") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="9">
								<ItemTemplate>
									<asp:Label ID="lblCount09" runat="server" Text='<%# Eval("COUNT09") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="10">
								<ItemTemplate>
									<asp:Label ID="lblCount10" runat="server" Text='<%# Eval("COUNT10") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="11">
								<ItemTemplate>
									<asp:Label ID="lblCount11" runat="server" Text='<%# Eval("COUNT11") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="12">
								<ItemTemplate>
									<asp:Label ID="lblCount12" runat="server" Text='<%# Eval("COUNT12") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="13">
								<ItemTemplate>
									<asp:Label ID="lblCount13" runat="server" Text='<%# Eval("COUNT13") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="14">
								<ItemTemplate>
									<asp:Label ID="lblCount14" runat="server" Text='<%# Eval("COUNT14") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="15">
								<ItemTemplate>
									<asp:Label ID="lblCount15" runat="server" Text='<%# Eval("COUNT15") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="16">
								<ItemTemplate>
									<asp:Label ID="lblCount16" runat="server" Text='<%# Eval("COUNT16") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="17">
								<ItemTemplate>
									<asp:Label ID="lblCount17" runat="server" Text='<%# Eval("COUNT17") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="18">
								<ItemTemplate>
									<asp:Label ID="lblCount18" runat="server" Text='<%# Eval("COUNT18") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="19">
								<ItemTemplate>
									<asp:Label ID="lblCount19" runat="server" Text='<%# Eval("COUNT19") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="20">
								<ItemTemplate>
									<asp:Label ID="lblCount20" runat="server" Text='<%# Eval("COUNT20") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="21">
								<ItemTemplate>
									<asp:Label ID="lblCount21" runat="server" Text='<%# Eval("COUNT21") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="22">
								<ItemTemplate>
									<asp:Label ID="lblCount22" runat="server" Text='<%# Eval("COUNT22") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="23">
								<ItemTemplate>
									<asp:Label ID="lblCount23" runat="server" Text='<%# Eval("COUNT23") %>'></asp:Label>
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</asp:GridView>
				</asp:Panel>
			</fieldset>
			<br />
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsUserPageViewHourly" runat="server" ConvertNullToDBNull="false" EnablePaging="true" OnSelecting="dsUserPageViewHourly_Selecting" OnSelected="dsUserPageViewHourly_Selected"
		SelectMethod="GetPageCollection" SelectCountMethod="GetPageCount" SortParameterName="" TypeName="UserPageViewHourly">
		<SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
</asp:Content>

