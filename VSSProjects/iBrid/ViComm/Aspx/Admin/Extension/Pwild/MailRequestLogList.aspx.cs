﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メールおねだり履歴

--	Progaram ID		: MailRequestLogList
--
--  Creation Date	: 2015.01.12
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using ViComm;
using iBridCommLib;
using System.Collections.Generic;

public partial class Extension_Pwild_MailRequestLogList:System.Web.UI.Page {
	private string recCount = string.Empty;
	private Stream filter;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string ManLoginId {
		get {
			return iBridUtil.GetStringValue(this.ViewState["ManLoginId"]);
		}
		set {
			this.ViewState["ManLoginId"] = value;
		}
	}

	private string CastLoginId {
		get {
			return iBridUtil.GetStringValue(this.ViewState["CastLoginId"]);
		}
		set {
			this.ViewState["CastLoginId"] = value;
		}
	}

	private DataSet MailRequestLogData {
		get {
			return this.ViewState["MailRequestLogData"] as DataSet;
		}
		set {
			this.ViewState["MailRequestLogData"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();

			this.pnlInfo.Visible = true;
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.grdMailRequestLog.PageIndex = 0;
		this.GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void dsMailRequestLog_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		DataSet oMailRequestLogDataSet = e.ReturnValue as DataSet;
		if (oMailRequestLogDataSet != null && (oMailRequestLogDataSet).Tables[0].Rows.Count > 0) {
			this.MailRequestLogData = oMailRequestLogDataSet;
		}
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void grdMailRequestLog_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			if (DataBinder.Eval(e.Row.DataItem,"NOT_DISPLAY_FLAG").ToString().Equals(ViCommConst.FLAG_ON_STR)) {
				e.Row.BackColor = Color.LightGray;
			} else {
				e.Row.BackColor = Color.White;
			}
		}
	}

	protected void dsMailRequestLog_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		MailRequestLog.SearchCondition oSearchCondition = new MailRequestLog.SearchCondition();
		oSearchCondition.ManLoginId = this.txtManLoginId.Text.Trim();
		oSearchCondition.CastLoginId = this.txtCastLoginId.Text.Trim();
		oSearchCondition.CreateDateFrom = string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue,this.lstFromHH.SelectedValue,this.lstFromMI.SelectedValue);
		string sToDD;
		if (this.lstToDD.SelectedIndex == 0) {
			sToDD = DateTime.DaysInMonth(int.Parse(this.lstToYYYY.SelectedValue),int.Parse(this.lstToMM.SelectedValue)).ToString();
		} else {
			sToDD = this.lstToDD.SelectedValue;
		}
		oSearchCondition.CreateDateTo = string.Format("{0}/{1}/{2} {3}:{4}:59",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,sToDD,this.lstToHH.SelectedValue,this.lstToMI.SelectedValue);
		oSearchCondition.RxMailFlag = rdoRxMailFlag.Text;
		oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void vdcFromTo_ServerValidate(object source,ServerValidateEventArgs e) {
		if (this.IsValid) {
			if (e.IsValid) {
				DateTime dtFrom;
				DateTime dtTo;
				if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue,this.lstFromHH.SelectedValue,this.lstFromMI.SelectedValue),out dtFrom)) {
					this.vdcFromTo.Text = "送信日時Fromに正しい日時を設定してください。";
					e.IsValid = false;
					return;
				}
				string sToDD;
				if (this.lstToDD.SelectedIndex == 0) {
					sToDD = DateTime.DaysInMonth(int.Parse(this.lstToYYYY.SelectedValue),int.Parse(this.lstToMM.SelectedValue)).ToString();
				} else {
					sToDD = this.lstToDD.SelectedValue;
				}
				if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,sToDD,this.lstToHH.SelectedValue,this.lstToMI.SelectedValue),out dtTo)) {
					this.vdcFromTo.Text = "送信日時Toに正しい日時を設定してください。";
					e.IsValid = false;
					return;
				}
				if (dtFrom > dtTo) {
					this.vdcFromTo.Text = "送信日時の大小関係に誤りがあります。";
					e.IsValid = false;
				}
			}
		}
	}

	private void InitPage() {
		if (!this.IsPostBack) {
			SysPrograms.SetupFromToDayTime(this.lstFromYYYY,this.lstFromMM,this.lstFromDD,this.lstFromHH,this.lstToYYYY,this.lstToMM,this.lstToDD,this.lstToHH,false);
			this.lstFromMM.Items.Insert(0,new ListItem("--","01"));
			this.lstToMM.Items.Insert(0,new ListItem("--","12"));
			this.lstFromDD.Items.Insert(0,new ListItem("--","01"));
			this.lstToDD.Items.Insert(0,new ListItem("--","31"));
			this.lstFromHH.Items.Insert(0,new ListItem("--","00"));
			this.lstToHH.Items.Insert(0,new ListItem("--","23"));

			for (int i = 0;i < 60;i++) {
				this.lstFromMI.Items.Add(new ListItem(string.Format("{0:D2}",i)));
				this.lstToMI.Items.Add(new ListItem(string.Format("{0:D2}",i)));
			}
			this.lstFromMI.Items.Insert(0,new ListItem("--","00"));
			this.lstToMI.Items.Insert(0,new ListItem("--","59"));
		}

		this.recCount = "0";
		this.txtManLoginId.Text = string.Empty;
		this.txtCastLoginId.Text = string.Empty;
		this.grdMailRequestLog.DataSourceID = string.Empty;

		this.lstFromYYYY.SelectedIndex = 0;
		this.lstToYYYY.SelectedIndex = 0;
		this.lstFromMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstToMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstFromDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstToDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstFromHH.SelectedIndex = 0;
		this.lstToHH.SelectedIndex = 0;
		this.lstFromMI.SelectedIndex = 0;
		this.lstToMI.SelectedIndex = 0;

		this.pnlInfo.Visible = false;
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		if (!this.IsPostBack) {
			this.SiteCd = this.Request.QueryString["site"];
			this.ManLoginId = this.Request.QueryString["manloginid"];
			this.CastLoginId = this.Request.QueryString["castloginid"];

			string sCreateDate = iBridUtil.GetStringValue(this.Request.QueryString["createdate"]);
			if (!string.IsNullOrEmpty(sCreateDate)) {
				DateTime oCreateDate = DateTime.Parse(sCreateDate);

				if (!string.IsNullOrEmpty(sCreateDate)) {
					this.lstFromYYYY.SelectedValue = oCreateDate.ToString("yyyy");
					this.lstToYYYY.SelectedValue = oCreateDate.ToString("yyyy");
					this.lstFromMM.SelectedValue = oCreateDate.ToString("MM");
					this.lstToMM.SelectedValue = oCreateDate.ToString("MM");
					this.lstFromDD.SelectedValue = oCreateDate.ToString("dd");
					this.lstToDD.SelectedValue = oCreateDate.ToString("dd");
					this.lstFromHH.SelectedValue = oCreateDate.ToString("HH");
					this.lstToHH.SelectedValue = oCreateDate.ToString("HH");
					this.lstFromMI.SelectedValue = oCreateDate.ToString("mm");
					this.lstToMI.SelectedValue = oCreateDate.ToString("mm");
				}
			}

			if (!string.IsNullOrEmpty(this.SiteCd)) {
				this.lstSiteCd.SelectedValue = this.SiteCd;
			}

			if (!string.IsNullOrEmpty(this.ManLoginId)) {
				this.txtManLoginId.Text = this.ManLoginId;
			}

			if (!string.IsNullOrEmpty(this.CastLoginId)) {
				this.txtCastLoginId.Text = this.CastLoginId;
			}
		}
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.grdMailRequestLog.PageSize = 30;
		this.grdMailRequestLog.DataSourceID = "dsMailRequestLog";
		this.grdMailRequestLog.DataBind();
		this.pnlCount.DataBind();
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		Response.Filter = filter;
		Response.AddHeader("Content-Disposition",string.Format("attachment;filename=MAIL_REQUEST_LOG_{0}.CSV",lstSiteCd.SelectedValue));
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		MailRequestLog.SearchCondition oSearchCondition = new MailRequestLog.SearchCondition();
		oSearchCondition.ManLoginId = this.txtManLoginId.Text.Trim();
		oSearchCondition.CastLoginId = this.txtCastLoginId.Text.Trim();
		oSearchCondition.CreateDateFrom = string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue,this.lstFromHH.SelectedValue,this.lstFromMI.SelectedValue);
		string sToDD;
		if (this.lstToDD.SelectedIndex == 0) {
			sToDD = DateTime.DaysInMonth(int.Parse(this.lstToYYYY.SelectedValue),int.Parse(this.lstToMM.SelectedValue)).ToString();
		} else {
			sToDD = this.lstToDD.SelectedValue;
		}
		oSearchCondition.RxMailFlag = rdoRxMailFlag.Text;
		oSearchCondition.CreateDateTo = string.Format("{0}/{1}/{2} {3}:{4}:59",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,sToDD,this.lstToHH.SelectedValue,this.lstToMI.SelectedValue);
		oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
		
		DataSet ds;
		using (MailRequestLog oMailRequestLog = new MailRequestLog()) {
			ds = oMailRequestLog.GetCsvData(oSearchCondition);
			SetCsvData(ds);
			Response.End();
		}
	}

	public void SetCsvData(DataSet pDataSet) {

		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		string sHeaderString = "送信日時,会員ログインID,会員ハンドル名,出演者ログインID,出演者ハンドル名,ﾒｰﾙ送信,表示\r\n";
		Response.BinaryWrite(encoding.GetBytes(sHeaderString));
		
		string sDtl = "";

		DataRow dr = null;
		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];

			sDtl = string.Format("{0},{1},{2},{3},{4},{5},{6}",
							dr["CREATE_DATE"].ToString(),
							dr["MAN_LOGIN_ID"].ToString(),
							dr["MAN_HANDLE_NM"].ToString(),
							dr["CAST_LOGIN_ID"].ToString(),
							dr["CAST_HANDLE_NM"].ToString(),
							dr["RX_MAIL_MESSAGE"].ToString(),
							dr["NOT_DISPLAY_MESSAGE"].ToString()
							);
			sDtl += "\r\n";
			Response.BinaryWrite(encoding.GetBytes(sDtl));
		}
	}
}
