﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: イベントエリア一覧

--	Progaram ID		: EventAreaList
--
--  Creation Date	: 2015.06.30
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.IO;
using ViComm.Extension.Pwild;

public partial class Extension_Pwild_EventAreaList:System.Web.UI.Page {
	private string recCount = string.Empty;

	private string PrefectureCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["PrefectureCd"]);
		}
		set {
			this.ViewState["PrefectureCd"] = value;
		}
	}

	private string EventAreaCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["EventAreaCd"]);
		}
		set {
			this.ViewState["EventAreaCd"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.FirstLoad();
			this.InitPage();
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
		this.EventAreaCd = string.Empty;
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		if (!this.UpdateData(false)) {
			return;
		}
		this.pnlEventAreaInfo.Visible = false;
		this.pnlKey.Enabled = true;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		this.UpdateData(true);
		this.pnlEventAreaInfo.Visible = false;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.ClearFileds();
		this.pnlKey.Enabled = true;
		this.pnlEventAreaInfo.Visible = false;
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.EventAreaCd = string.Empty;
		this.ClearFileds();
		this.pnlKey.Enabled = false;
		this.btnDelete.Enabled = false;
		this.pnlEventAreaInfo.Visible = true;
	}

	protected void lnkEventAreaCd_Command(object sender,CommandEventArgs e) {

		if (!this.IsValid) {
			return;
		}
		this.EventAreaCd = iBridUtil.GetStringValue(e.CommandArgument);
		this.GetData();
		this.pnlKey.Enabled = false;
		this.btnDelete.Enabled = true;
		this.pnlEventAreaInfo.Visible = true;

	}

	protected void dsEventArea_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsEventArea_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		EventArea.SearchCondition oSearchCondition = new EventArea.SearchCondition();
		oSearchCondition.PrefectureCd = this.lstSeekPrefectureCd.SelectedValue;
		oSearchCondition.EventAreaNm = this.txtSeekEventAreaNm.Text.Trim();
		
		e.InputParameters[0] = oSearchCondition;
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	private void InitPage() {
		this.pnlInfo.Visible = true;
		this.pnlEventAreaInfo.Visible = false;

		this.EventAreaCd = string.Empty;
		this.pnlKey.Enabled = true;
	}

	private void FirstLoad() {
		this.pnlEventAreaInfo.DataBind();
	}

	private void ClearFileds() {

		this.pnlKey.Enabled = true;
		this.lblEventAreaCd.Text = string.Empty;
		this.lstPrefectureCd.DataBind();
		this.txtEventAreaNm.Text = string.Empty;
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.EventAreaCd = string.Empty;

		this.grdEventArea.PageIndex = 0;
		this.grdEventArea.DataSourceID = "dsEventArea";
		this.grdEventArea.DataBind();
		this.pnlCount.DataBind();
	}

	private void GetData() {
		this.ClearFileds();

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("EVENT_AREA_GET");
			oDbSession.ProcedureInParm("pEVENT_AREA_CD",DbSession.DbType.VARCHAR2,this.EventAreaCd);
			oDbSession.ProcedureOutParm("pPREFECTURE_CD",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pEVENT_AREA_NM",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.lblEventAreaCd.Text = this.EventAreaCd;
			this.txtEventAreaNm.Text = oDbSession.GetStringValue("pEVENT_AREA_NM");

			this.lstPrefectureCd.SelectedValue = oDbSession.GetStringValue("pPREFECTURE_CD");
		}
	}

	private bool UpdateData(bool pDelFlag) {

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("EVENT_AREA_MAINTE");
			oDbSession.cmd.BindByName = true;
			oDbSession.ProcedureInParm("pEVENT_AREA_CD",DbSession.DbType.VARCHAR2,this.EventAreaCd);
			oDbSession.ProcedureInParm("pPREFECTURE_CD",DbSession.DbType.VARCHAR2,this.lstPrefectureCd.SelectedValue);
			oDbSession.ProcedureInParm("pEVENT_AREA_NM",DbSession.DbType.VARCHAR2,this.txtEventAreaNm.Text);
			oDbSession.ProcedureInParm("pDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.ClearFileds();
		this.GetList();
		return true;
	}
}
