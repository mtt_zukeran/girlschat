﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="EventCategoryList.aspx.cs" Inherits="Extension_Pwild_EventCategoryList" Title="イベント大カテゴリ一覧" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="イベント大カテゴリ一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 600px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle2">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton" ValidationGroup="Create"
                    OnClick="btnCreate_Click" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" Visible="false" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlEventCategoryInfo">
            <fieldset class="fieldset">
                <legend>[大カテゴリ内容]</legend>
                <fieldset class="fieldset-inner">
                    <table border="0" class="tableStyle" style="width: 600px">
                        <tr>
                            <td class="tdHeaderStyle">
                                SEQ
                            </td>
                            <td class="tdDataStyle">
                                <asp:Label ID="lblEventCategorySeq" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                大カテゴリ名
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtEventCategoryNm" runat="server" MaxLength="64" Width="200px"></asp:TextBox>
                                <% // 必須チェック %>
                                <asp:RequiredFieldValidator ID="vdrEventCategoryNm" runat="server" ErrorMessage="大カテゴリ名を入力してください。"
                                    ControlToValidate="txtEventCategoryNm" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server"
                                    TargetControlID="vdrEventCategoryNm" HighlightCssClass="validatorCallout" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <asp:Panel ID="pnlCommand" runat="server">
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Update"
                        OnClick="btnUpdate_Click" />
                    <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" ValidationGroup="Delete"
                        OnClick="btnDelete_Click" />
                    <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" CausesValidation="False"
                        Visible="true" OnClick="btnCancel_Click" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[大カテゴリ一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdEventCategory.PageIndex + 1 %>
                        of
                        <%= grdEventCategory.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="600px">
                    <asp:GridView ID="grdEventCategory" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        PageSize="30" EnableSortingAndPagingCallbacks="false" DataSourceID="dsEventCategory"
                        SkinID="GridViewColor" Font-Size="small" AllowSorting="true">
                        <Columns>
							<asp:BoundField DataField="EVENT_CATEGORY_SEQ" HeaderText="SEQ">
                                <ItemStyle HorizontalAlign="center" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="大カテゴリ名">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEventCategoryNm" runat="server" CommandArgument='<%# Eval("EVENT_CATEGORY_SEQ") %>'
                                        Text='<%# Eval("EVENT_CATEGORY_NM") %>' OnCommand="lnkEventCategoryNm_Command"></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsEventCategory" runat="server" SelectMethod="GetPageCollection"
        ConvertNullToDBNull="true" SortParameterName="" TypeName="EventCategory" SelectCountMethod="GetPageCount"
        EnablePaging="True" OnSelected="dsEventCategory_Selected">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="イベント大カテゴリ情報を更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete"
        ConfirmText="イベント大カテゴリ情報を削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>
