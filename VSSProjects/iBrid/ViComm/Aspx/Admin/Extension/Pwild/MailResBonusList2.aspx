﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MailResBonusList2.aspx.cs" Inherits="Extension_Pwild_MailResBonusList2" Title="メール返信ボーナス期間設定2" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="メール返信ボーナス期間設定2"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlNotes">
			<fieldset class="fieldset">
				<legend>[注意事項]</legend>
				<asp:Panel ID="pnlNoteDtl" runat="server">
					<div>設定した期間内に受信がない男性から受信したメールに対して時間内に返信をするイベントです。</div>
					<br />
					<div>付与Pt確率設定が必要</div>
					<div>付与Ptと確立</div>
					<div>確率は同数にすれば平均的に出現します。</div>
					<br />
					<div>設定方法</div>
					<div>返信期限秒数・ボーナス付与最大返信数・メール未受信日数・イベント開催期間の設定をします。</div>
					<div>別途付与Pt確率設定も必要となります。</div>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlSearch">
			<fieldset class="fieldset">
				<legend>[検索]</legend>
				<asp:Panel ID="pnlSeekCondition" runat="server">
					<table border="0" style="width: 800px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle2">
								サイトコード
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px">
								</asp:DropDownList>
							</td>
						</tr>
					</table>
				</asp:Panel>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Seek" />
				<asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton"  OnClick="btnCreate_Click" ValidationGroup="Create" />
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlInput">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Label ID="lblErrorMessage" runat="server" Text="" ForeColor="red"></asp:Label>
				<table border="0" style="width: 650px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							1通目<br />返信期限秒数
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtResLimitSec1" runat="server" Width="40px" MaxLength="4"></asp:TextBox>
							<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtResLimitSec1" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							2通目以降<br />返信期限秒数
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtResLimitSec2" runat="server" Width="40px" MaxLength="4"></asp:TextBox>
							<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtResLimitSec2" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							ボーナス付与<br />最大返信数
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtResBonusMaxNum" runat="server" Width="40px" MaxLength="4"></asp:TextBox>
							<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtResBonusMaxNum" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							男性会員からの<br />メール未受信日数
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtUnreceivedDays" runat="server" Width="40px" MaxLength="3"></asp:TextBox>
							<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtUnreceivedDays" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							開始日時
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstStartDateYYYY" runat="server" Width="60px"></asp:DropDownList>年
							<asp:DropDownList ID="lstStartDateMM" runat="server" Width="40px"></asp:DropDownList>月
							<asp:DropDownList ID="lstStartDateDD" runat="server" Width="40px"></asp:DropDownList>日
							<asp:DropDownList ID="lstStartDateHH" runat="server" Width="40px"></asp:DropDownList>時
							<asp:DropDownList ID="lstStartDateMI" runat="server" Width="40px"></asp:DropDownList>分
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							終了日時
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstEndDateYYYY" runat="server" Width="60px"></asp:DropDownList>年
							<asp:DropDownList ID="lstEndDateMM" runat="server" Width="40px"></asp:DropDownList>月
							<asp:DropDownList ID="lstEndDateDD" runat="server" Width="40px"></asp:DropDownList>日
							<asp:DropDownList ID="lstEndDateHH" runat="server" Width="40px"></asp:DropDownList>時
							<asp:DropDownList ID="lstEndDateMI" runat="server" Width="40px"></asp:DropDownList>分
						</td>
					</tr>
				</table>
				<asp:Panel ID="pnlCommand" runat="server" Width="660px">
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Update" />
					<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" />
					<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="delBtnStyle" OnClick="btnDelete_Click" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlList">
			<fieldset>
				<legend>[一覧]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">
						Record Count <%# GetRecCount() %>
					</a>
					<br />
					<a class="reccount">
						Current viewing page <%= grdMailResBonus.PageIndex + 1%> of <%= grdMailResBonus.PageCount%>
					</a>
				</asp:Panel>
				&nbsp;
				<asp:Panel ID="pnlGrid" runat="server">
					<asp:GridView ID="grdMailResBonus" DataSourceID="dsMailResBonus" runat="server" AllowPaging="True" AutoGenerateColumns="False" PageSize="30" EnableSortingAndPagingCallbacks="false" SkinID="GridViewColor" AllowSorting="true">
						<Columns>
							<asp:TemplateField>
								<ItemTemplate>
									<asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%# Container.DataItemIndex %>' OnCommand="lnkEdit_Command" Text='<%# Eval("MAIL_RES_BONUS_SEQ") %>'></asp:LinkButton>
									<asp:HiddenField ID="hdnMailResBonusSeq" runat="server" Value='<%# Eval("MAIL_RES_BONUS_SEQ") %>' />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="1通目<br>返信期限秒数">
								<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Font-Size="X-Small" />
								<ItemTemplate>
									<asp:Label ID="lblResLimitSec1" runat="server" Text='<%# Eval("RES_LIMIT_SEC1") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="2通目以降<br>返信期限秒数">
								<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Font-Size="X-Small" />
								<ItemTemplate>
									<asp:Label ID="lblResLimitSec2" runat="server" Text='<%# Eval("RES_LIMIT_SEC2") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ボーナス付与<br>最大返信数">
								<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Font-Size="X-Small" />
								<ItemTemplate>
									<asp:Label ID="lblResBonusMaxNum" runat="server" Text='<%# Eval("RES_BONUS_MAX_NUM") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="男性会員からの<br>メール未受信日数">
								<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Font-Size="X-Small" />
								<ItemTemplate>
									<asp:Label ID="lblUnreceivedDays" runat="server" Text='<%# Eval("UNRECEIVED_DAYS") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="START_DATE" HeaderText="開始日時" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
								<ItemStyle HorizontalAlign="Left" Wrap="false" />
							</asp:BoundField>
							<asp:BoundField DataField="END_DATE" HeaderText="終了日時" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
								<ItemStyle HorizontalAlign="Left" Wrap="false" />
							</asp:BoundField>
							<asp:TemplateField>
								<ItemTemplate>
									<asp:HyperLink ID="lnkMailLotteryRate" runat="server" NavigateUrl='<%# string.Format("~/Extension/Pwild/MailResBonusRateList2.aspx?sitecd={0}&mailresbonusseq={1}",Eval("SITE_CD"),Eval("MAIL_RES_BONUS_SEQ")) %>' Text="付与Pt確率設定">
									</asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
			<br />
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsMailResBonus" runat="server" ConvertNullToDBNull="false" EnablePaging="True" OnSelecting="dsMailResBonus_Selecting" OnSelected="dsMailResBonus_Selected"
		SelectCountMethod="GetPageCount" SelectMethod="GetPageCollection" SortParameterName="" TypeName="MailResBonus2">
		<SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>