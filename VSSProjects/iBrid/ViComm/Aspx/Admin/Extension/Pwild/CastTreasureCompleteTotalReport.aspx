﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastTreasureCompleteTotalReport.aspx.cs" Inherits="Extension_Pwild_CastTreasureCompleteTotalReport" Title="女性お宝ｺﾝﾌﾟ人数レポート" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="女性お宝ｺﾝﾌﾟ人数レポート"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset>
                <legend>[設定]</legend>
                <table border="0" style="width: 640px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            サイトコード

                        </td>
                        <td class="tdDataStyle" colspan="3">
                            <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="180px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            ｺﾝﾌﾟ日時期間
                        </td>
                        <td class="tdDataStyle" colspan="3">
                            <asp:DropDownList ID="lstFromYYYY" runat="server" Width="54px">
                            </asp:DropDownList>年
                            <asp:DropDownList ID="lstFromMM" runat="server" Width="43px">
                            </asp:DropDownList>月

                            <asp:DropDownList ID="lstFromDD" runat="server" Width="43px">
                            </asp:DropDownList>日～

                            <asp:DropDownList ID="lstToYYYY" runat="server" Width="54px">
                            </asp:DropDownList>年
                            <asp:DropDownList ID="lstToMM" runat="server" Width="43px">
                            </asp:DropDownList>月

                            <asp:DropDownList ID="lstToDD" runat="server" Width="43px">
                            </asp:DropDownList>日
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    CausesValidation="False" />
                <asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" CausesValidation="False" OnClick="btnCSV_Click" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[女性お宝ｺﾝﾌﾟ人数レポート]</legend>
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="552px">
                    <asp:GridView ID="grdCastTreasureCompleteTotalReport" runat="server" AutoGenerateColumns="False"
                        DataSourceID="dsCastTreasureCompleteTotalReport" AllowPaging="true" AllowSorting="true"
                        SkinID="GridViewColor" PageSize="50">
                        <Columns>
                            <asp:TemplateField HeaderText="ステージグループ名">
                                <ItemTemplate>
                                    <asp:Label ID="lblAppearanceDay" runat="server" Text='<%# Eval("STAGE_GROUP_NM") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="人数">
                                <ItemTemplate>
                                    <asp:Label ID="lblCompCount" runat="server" Text='<%# Eval("COMP_COUNT") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsCastTreasureCompleteTotalReport" runat="server" SelectMethod="GetList"
        OnSelecting="dsCastTreasureCompleteTotalReport_Selecting" TypeName="CastTreasureCompleteTotalReport">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSeekSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:Parameter Name="pReportDayFrom" Type="String" />
            <asp:Parameter Name="pReportDayTo" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
