﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 会員つぶやき無制限イベント設定
--	Progaram ID		: ManTweetUnlimitedMainte
--  Creation Date	: 2017.03.23
--  Creater			: M&TT Zukeran
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;

public partial class Extension_Pwild_ManTweetUnlimitedMainte:System.Web.UI.Page {
	private int iRecCount;

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		// 設定欄を非表示
		pnlInput.Visible = false;

		if (!IsPostBack) {
			// サイトコードの初期値設定
			lstSiteCd.DataBind();
			lstSiteCd.DataSourceID = string.Empty;

			// 日付リストの選択肢を設定
			SysPrograms.SetupFromToDayTime(
				lstStartDateYYYY
				,lstStartDateMM
				,lstStartDateDD
				,lstStartDateHH
				,lstEndDateYYYY
				,lstEndDateMM
				,lstEndDateDD
				,lstEndDateHH
				,false
			);
			this.SetupMinuteTime(ref lstStartDateMI,ref lstEndDateMI,false);

			this.iRecCount = 0;
			grdData.PageIndex = 0;

			// 検索実行
			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	protected string GetRecCount() {
		return this.iRecCount.ToString();
	}

	protected void dsTweetUnlimited_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		TweetUnlimited.SearchCondition oSearchCondition = new TweetUnlimited.SearchCondition();
		oSearchCondition.SiteCd = lstSiteCd.SelectedValue;
		oSearchCondition.SexCd = ViCommConst.MAN;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsTweetUnlimited_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null
			&& e.ReturnValue.GetType() == iRecCount.GetType()
		) {
			this.iRecCount = int.Parse(e.ReturnValue.ToString());
		}
	}

	/// <summary>
	/// 検索ボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			grdData.PageIndex = 0;
			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	/// <summary>
	/// 追加ボタン押下(設定欄表示)
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnCreate_Click(object sender,EventArgs e) {
		// 初期値設定
		this.ClearFileds();

		pnlSearch.Enabled = false;
		pnlInput.Visible = true;
		btnDelete.Visible = false;
	}

	/// <summary>
	/// 更新ボタン押下(登録・更新処理)
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.Validates()) {
			return;
		}
		TweetUnlimited oTweetUnlimited = new TweetUnlimited();
		oTweetUnlimited.Mainte(this.SetMainteData(ViCommConst.FLAG_OFF_STR));

		pnlSearch.Enabled = true;
		pnlInput.Visible = false;

		grdData.PageIndex = 0;
		pnlGrid.DataBind();
		pnlCount.DataBind();
	}

	/// <summary>
	/// キャンセルボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnCancel_Click(object sender,EventArgs e) {
		pnlSearch.Enabled = true;
		pnlInput.Visible = false;
	}

	/// <summary>
	/// 削除ボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnDelete_Click(object sender,EventArgs e) {
		TweetUnlimited oTweetUnlimited = new TweetUnlimited();
		oTweetUnlimited.Mainte(this.SetMainteData(ViCommConst.FLAG_ON_STR));

		pnlSearch.Enabled = true;
		pnlInput.Visible = false;

		grdData.PageIndex = 0;
		pnlGrid.DataBind();
		pnlCount.DataBind();
	}

	/// <summary>
	/// 編集ボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void lnkEdit_Command(object sender,CommandEventArgs e) {
		string[] asArgs = e.CommandArgument.ToString().Split(',');
		string[] asStartDate = asArgs[3].Split('-');
		string[] asEndDate = asArgs[4].Split('-');

		// 更新用のレコードID、リビジョンNoを設定
		hdnTweetUnlimitedRowid.Value = asArgs[0];
		hdnTweetUnlimitedRevisionNo.Value = asArgs[1];

		// 入力値設定
		txtElapsedDays.Text = asArgs[2];
		lstStartDateYYYY.SelectedValue = asStartDate[0];
		lstStartDateMM.SelectedValue = asStartDate[1];
		lstStartDateDD.SelectedValue = asStartDate[2];
		lstStartDateHH.SelectedValue = asStartDate[3];
		lstStartDateMI.SelectedValue = asStartDate[4];

		lstEndDateYYYY.SelectedValue = asEndDate[0];
		lstEndDateMM.SelectedValue = asEndDate[1];
		lstEndDateDD.SelectedValue = asEndDate[2];
		lstEndDateHH.SelectedValue = asEndDate[3];
		lstEndDateMI.SelectedValue = asEndDate[4];

		lblErrorMessage.Text = string.Empty;
		pnlSearch.Enabled = false;
		pnlInput.Visible = true;
		btnDelete.Visible = true;
	}

	/// <summary>
	/// 設定欄の更新ボタン押下時のバリデーション
	/// </summary>
	/// <returns></returns>
	private bool Validates() {
		int iDays;
		string sDate = string.Empty;
		lblErrorMessage.Text = string.Empty;

		if (string.IsNullOrEmpty(txtElapsedDays.Text)) {
		}
		else if (!int.TryParse(txtElapsedDays.Text,out iDays)) {
			lblErrorMessage.Text = "初回入金後経過日数を正しく入力してください";
			return false;
		}

		DateTime dtStartDate = new DateTime();
		DateTime dtEndDate = new DateTime();

		sDate = string.Format(
			"{0}/{1}/{2} {3}:{4}:00"
			,lstStartDateYYYY.SelectedValue
			,lstStartDateMM.SelectedValue
			,lstStartDateDD.SelectedValue
			,lstStartDateHH.SelectedValue
			,lstStartDateMI.SelectedValue
		);
		if (!DateTime.TryParse(sDate,out dtStartDate)) {
			lblErrorMessage.Text = "開始日時が正しくありません";
			return false;
		}

		sDate = string.Format(
			"{0}/{1}/{2} {3}:{4}:59"
			,lstEndDateYYYY.SelectedValue
			,lstEndDateMM.SelectedValue
			,lstEndDateDD.SelectedValue
			,lstEndDateHH.SelectedValue
			,lstEndDateMI.SelectedValue
		);
		if (!DateTime.TryParse(sDate,out dtEndDate)) {
			lblErrorMessage.Text = "終了日時が正しくありません";
			return false;
		}

		if (dtStartDate > dtEndDate) {
			lblErrorMessage.Text = "期間の大小関係が正しくありません";
			return false;
		}

		// 重複チェック用の検索条件を設定
		TweetUnlimited.SearchCondition oSearchCondition = new TweetUnlimited.SearchCondition();
		oSearchCondition.SiteCd = lstSiteCd.SelectedValue;
		oSearchCondition.SexCd = ViCommConst.MAN;
		oSearchCondition.CheckStartDate = dtStartDate.ToString("yyyy-MM-dd HH:mm:ss");
		oSearchCondition.CheckEndDate = dtEndDate.ToString("yyyy-MM-dd HH:mm:ss");
		oSearchCondition.RowId = hdnTweetUnlimitedRowid.Value;

		TweetUnlimited oTweetUnlimited = new TweetUnlimited();
		if (oTweetUnlimited.IsDuplicateTerm(oSearchCondition)) {
			lblErrorMessage.Text = "期間が他と重複しています";
			return false;
		}

		return true;
	}

	/// <summary>
	/// 設定欄の初期値設定
	/// </summary>
	private void ClearFileds() {
		lblErrorMessage.Text = string.Empty;

		txtElapsedDays.Text = string.Empty;

		lstStartDateYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		lstStartDateMM.SelectedValue = DateTime.Now.ToString("MM");
		lstStartDateDD.SelectedValue = DateTime.Now.ToString("dd");
		lstStartDateHH.SelectedValue = "00";
		lstStartDateMI.SelectedValue = "00";

		lstEndDateYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		lstEndDateMM.SelectedValue = DateTime.Now.ToString("MM");
		lstEndDateDD.SelectedValue = DateTime.Now.ToString("dd");
		lstEndDateHH.SelectedValue = "23";
		lstEndDateMI.SelectedValue = "59";
	}

	/// <summary>
	/// 分の選択肢を設定
	/// </summary>
	/// <param name="pFromMI"></param>
	/// <param name="pToMI"></param>
	/// <param name="pAddDummy"></param>
	private void SetupMinuteTime(ref DropDownList pFromMI,ref DropDownList pToMI,bool pAddDummy) {
		if (pAddDummy) {
			pFromMI.Items.Add(new ListItem("---","00"));
			pToMI.Items.Add(new ListItem("---","60"));
		}

		for (int i = 0;i <= 59;i++) {
			pFromMI.Items.Add(new ListItem(string.Format("{0:D2}",i)));
			pToMI.Items.Add(new ListItem(string.Format("{0:D2}",i)));
		}
	}

	/// <summary>
	/// 更新用データの設定
	/// </summary>
	/// <returns></returns>
	private TweetUnlimited.MainteData SetMainteData(string sDelFlag) {
		TweetUnlimited.MainteData oMainteData = new TweetUnlimited.MainteData();
		oMainteData.SiteCd = lstSiteCd.SelectedValue;
		oMainteData.SexCd = ViCommConst.MAN;
		oMainteData.ElapsedDays = txtElapsedDays.Text;
		oMainteData.StartDate = string.Format(
			"{0}-{1}-{2} {3}:{4}:00"
			,lstStartDateYYYY.SelectedValue
			,lstStartDateMM.SelectedValue
			,lstStartDateDD.SelectedValue
			,lstStartDateHH.SelectedValue
			,lstStartDateMI.SelectedValue
		);
		oMainteData.EndDate = string.Format(
			"{0}-{1}-{2} {3}:{4}:59"
			,lstEndDateYYYY.SelectedValue
			,lstEndDateMM.SelectedValue
			,lstEndDateDD.SelectedValue
			,lstEndDateHH.SelectedValue
			,lstEndDateMI.SelectedValue
		);
		oMainteData.RowId = hdnTweetUnlimitedRowid.Value;
		oMainteData.RevisionNo = hdnTweetUnlimitedRevisionNo.Value;
		oMainteData.DelFlag = sDelFlag;

		return oMainteData;
	}
}
