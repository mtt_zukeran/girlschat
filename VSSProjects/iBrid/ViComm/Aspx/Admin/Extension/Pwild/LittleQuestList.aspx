﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="LittleQuestList.aspx.cs" Inherits="Extension_Pwild_LittleQuestList" Title="小クエスト一覧" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="ViComm.Extension.Pwild" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="小クエスト一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" Runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 600px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle2">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton" ValidationGroup="Create"
                    OnClick="btnCreate_Click" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" Visible="false" />
                <asp:Button ID="btnBack" runat="server" CausesValidation="False" CssClass="seekbutton"
                    OnClick="btnBack_Click" Text="戻る" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlLittleQuestInfo">
            <fieldset class="fieldset">
                <legend>[小クエスト情報]</legend>
                <table border="0" style="width: 600px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            小クエスト番号
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtLevelQuestSubSeq" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="vdrLevelQuestSubSeq" runat="server" ErrorMessage="クエスト番号を入力して下さい。"
                                    ControlToValidate="txtLevelQuestSubSeq" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                            <ajaxToolkit:ValidatorCalloutExtender ID="vdeLevelQuestSubSeq" runat="Server" TargetControlID="vdrLevelQuestSubSeq" HighlightCssClass="validatorCallout" />
                            <asp:Label ID="lblErrMsgQuestLevel" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            小クエスト名
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtLittleQuestNm" runat="server" MaxLength="300" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            小クエスト説明
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtRemarks" runat="server" MaxLength="300" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            クエスト制限時間
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtTimeLimitD" runat="server" MaxLength="5" Width="30px"></asp:TextBox>日
                            <asp:TextBox ID="txtTimeLimitH" runat="server" MaxLength="5" Width="30px"></asp:TextBox>時
                            <asp:TextBox ID="txtTimeLimitM" runat="server" MaxLength="5" Width="30px"></asp:TextBox>分
                            <asp:Label ID="lblErrorMessageTimeLimit" runat="server" Text="" Visible="false" ForeColor="red"></asp:Label>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="Panel1" runat="server">
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Update"
                        OnClick="btnUpdate_Click" />
                    <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" ValidationGroup="Delete"
                        OnClick="btnDelete_Click" />
                    <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" CausesValidation="False"
                        Visible="true" OnClick="btnCancel_Click" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[小クエスト一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdLittleQuest.PageIndex + 1 %>
                        of
                        <%= grdLittleQuest.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="600px">
                    <asp:GridView ID="grdLittleQuest" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        PageSize="30" EnableSortingAndPagingCallbacks="false" DataSourceID="dsQuest"
                        SkinID="GridViewColor" Font-Size="small" AllowSorting="true" DataKeyNames="SITE_CD,QUEST_SEQ">
                        <Columns>
                            <asp:TemplateField HeaderText="ﾚﾍﾞﾙ">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkLittleQuestId" runat="server" CommandArgument='<%# Eval("LITTLE_QUEST_SEQ") %>'
                                        Text='<%# Eval("LEVEL_QUEST_SUB_SEQ") %>' OnCommand="lnkLittleQuestId_Command"></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="小クエスト名">
                                <ItemTemplate>
                                    <asp:Label ID="lblLittleQuestNm" runat="server" Text='<%# Eval("LITTLE_QUEST_NM") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="小ｸｴｽﾄ説明">
                                <ItemTemplate>
                                    <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("REMARKS") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="小ｸｴｽﾄ制限時間">
                                <ItemTemplate>
                                    <asp:Label ID="lblTimeLimitD" runat="server" Text='<%# Eval("TIME_LIMIT_D") %>'></asp:Label>日
                                    <asp:Label ID="lblTimeLimitH" runat="server" Text='<%# Eval("TIME_LIMIT_H") %>'></asp:Label>時間
                                    <asp:Label ID="lblTimeLimitM" runat="server" Text='<%# Eval("TIME_LIMIT_M") %>'></asp:Label>分
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkQuestTrialList" runat="server" NavigateUrl='<%# string.Format("~/Extension/Pwild/QuestTrialList.aspx?sitecd={0}&questseq={1}&levelquestseq={2}&littlequestseq={3}&questtype={4}&sexcd={5}", Eval("SITE_CD"), Eval("QUEST_SEQ"), Eval("LEVEL_QUEST_SEQ"), Eval("LITTLE_QUEST_SEQ"), PwViCommConst.GameQuestType.LITTLE_QUEST, Eval("SEX_CD"))%>'
                                        Text="ｸﾘｱ条件"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkQuestRewardList" runat="server" NavigateUrl='<%# string.Format("~/Extension/Pwild/QuestRewardList.aspx?sitecd={0}&questseq={1}&levelquestseq={2}&littlequestseq={3}&questtype={4}&sexcd={5}", Eval("SITE_CD"), Eval("QUEST_SEQ"), Eval("LEVEL_QUEST_SEQ"), Eval("LITTLE_QUEST_SEQ"), PwViCommConst.GameQuestType.LITTLE_QUEST, Eval("SEX_CD"))%>'
                                        Text="報酬一覧"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsLittleQuest" runat="server" SelectMethod="GetPageCollection"
        ConvertNullToDBNull="true" SortParameterName="" TypeName="LittleQuest" SelectCountMethod="GetPageCount"
        EnablePaging="True" OnSelected="dsLittleQuest_Selected">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pQuestSeq" QueryStringField="questseq" Type="String" />
            <asp:QueryStringParameter Name="pLevelQuestSeq" QueryStringField="levelquestseq" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtLevelQuestSubSeq" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtTimeLimitD" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtTimeLimitH" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtTimeLimitM" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="小クエスト情報を更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete"
        ConfirmText="小クエスト情報を削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>

