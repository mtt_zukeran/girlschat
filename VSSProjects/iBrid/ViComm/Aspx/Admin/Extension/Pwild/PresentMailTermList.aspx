<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PresentMailTermList.aspx.cs" Inherits="Extension_Pwild_PresentMailTermList" Title="投票設定一覧(エントリー制)" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="プレゼントメール期間設定一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlSearch">
			<fieldset class="fieldset">
				<legend>[検索]</legend>
				<asp:Panel ID="pnlSeekCondition" runat="server">
					<table border="0" style="width: 800px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle2">
								サイトコード
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px">
								</asp:DropDownList>
							</td>
						</tr>
					</table>
				</asp:Panel>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Seek" />
				<asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton"  OnClick="btnCreate_Click" ValidationGroup="Create" />
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlInput">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Label ID="lblErrorMessage" runat="server" Text="" ForeColor="red"></asp:Label>
				<table border="0" style="width: 650px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							名称
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtPresentMailTermNm" runat="server" Width="200px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							エントリー開始日時
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstTermStartDateYYYY" runat="server" Width="60px" DataSource='<%# YearArray %>'>
							</asp:DropDownList>年
							<asp:DropDownList ID="lstTermStartDateMM" runat="server" Width="40px" DataSource='<%# MonthArray %>'>
							</asp:DropDownList>月
							<asp:DropDownList ID="lstTermStartDateDD" runat="server" Width="40px" DataSource='<%# DayArray %>'>
							</asp:DropDownList>日
							<asp:DropDownList ID="lstTermStartDateHH" runat="server" Width="40px" DataSource='<%# HourArray %>'>
							</asp:DropDownList>時
							<asp:DropDownList ID="lstTermStartDateMI" runat="server" DataSource='<%# MinuteArray %>'>
							</asp:DropDownList>分
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							エントリー終了日時
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstTermEndDateYYYY" runat="server" Width="60px" DataSource='<%# YearArray %>'>
							</asp:DropDownList>年
							<asp:DropDownList ID="lstTermEndDateMM" runat="server" Width="40px" DataSource='<%# MonthArray %>'>
							</asp:DropDownList>月							<asp:DropDownList ID="lstTermEndDateDD" runat="server" Width="40px" DataSource='<%# DayArray %>'>
							</asp:DropDownList>日
							<asp:DropDownList ID="lstTermEndDateHH" runat="server" Width="40px" DataSource='<%# HourArray %>'>
							</asp:DropDownList>時							<asp:DropDownList ID="lstTermEndDateMI" runat="server" DataSource='<%# MinuteArray %>'>
							</asp:DropDownList>分
						</td>
					</tr>
				</table>
				<asp:Panel ID="pnlCommand" runat="server" Width="660px">
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" />
					<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" />
					<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="delBtnStyle" OnClick="btnDelete_Click" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlList">
			<fieldset>
				<legend>[一覧]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">
						Record Count <%# GetRecCount() %>
					</a>
					<br />
					<a class="reccount">
						Current viewing page <%= grdPresentMailTerm.PageIndex + 1%> of <%= grdPresentMailTerm.PageCount%>
					</a>
				</asp:Panel>
				&nbsp;
				<asp:Panel ID="pnlGrid" runat="server">
					<asp:GridView ID="grdPresentMailTerm" DataSourceID="dsPresentMailTerm" runat="server" AllowPaging="True" AutoGenerateColumns="False" PageSize="30" EnableSortingAndPagingCallbacks="false" SkinID="GridViewColor" AllowSorting="true">
						<Columns>
							<asp:TemplateField>
								<ItemTemplate>
									<asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%# Container.DataItemIndex %>' OnCommand="lnkEdit_Command" Text='<%# Eval("PRESENT_MAIL_TERM_SEQ") %>'></asp:LinkButton>
									<asp:HiddenField ID="hdnPresentMailTermSeq" runat="server" Value='<%# Eval("PRESENT_MAIL_TERM_SEQ") %>' />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="PRESENT_MAIL_TERM_NM" HeaderText="名称">
								<ItemStyle HorizontalAlign="Left" />
							</asp:BoundField>
							<asp:BoundField DataField="TERM_START_DATE" HeaderText="ｴﾝﾄﾘｰ開始" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
								<ItemStyle HorizontalAlign="Left" Wrap="false" />
							</asp:BoundField>
							<asp:BoundField DataField="TERM_END_DATE" HeaderText="ｴﾝﾄﾘｰ終了" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
								<ItemStyle HorizontalAlign="Left" Wrap="false" />
							</asp:BoundField>
							<asp:TemplateField>
								<ItemTemplate>
									<asp:HyperLink ID="lnkPresentMailItem" runat="server" NavigateUrl='<%# string.Format("~/Extension/Pwild/PresentMailItemList.aspx?sitecd={0}&presentmailtermseq={1}",Eval("SITE_CD"),Eval("PRESENT_MAIL_TERM_SEQ")) %>' Text="ｱｲﾃﾑ一覧">
									</asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
			<br />
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsPresentMailTerm" runat="server" ConvertNullToDBNull="false" EnablePaging="True" OnSelecting="dsPresentMailTerm_Selecting" OnSelected="dsPresentMailTerm_Selected"
		SelectCountMethod="GetPageCount" SelectMethod="GetPageCollection" SortParameterName="" TypeName="PresentMailTerm">
		<SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>