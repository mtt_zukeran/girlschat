﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 会員つぶやき

--	Progaram ID		: ManTweetList
--
--  Creation Date	: 2013.02.19
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.Collections.Generic;

public partial class Extension_Pwild_ManTweetList:System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string LoginId {
		get {
			return iBridUtil.GetStringValue(this.ViewState["LoginId"]);
		}
		set {
			this.ViewState["LoginId"] = value;
		}
	}
	
	private string ManTweetSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["ManTweetSeq"]);
		}
		set {
			this.ViewState["ManTweetSeq"] = value;
		}
	}

	private DataSet ManTweetData {
		get {
			return this.ViewState["ManTweetData"] as DataSet;
		}
		set {
			this.ViewState["ManTweetData"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();

			this.pnlInfo.Visible = true;
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.grdManTweet.PageIndex = 0;
		this.GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	/// <summary>
	/// CSV出力ボタンが押された時の処理
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnCSV_Click(object sender,EventArgs e) {
		string fileName = ViCommConst.CSV_FILE_NM_MAN_TWEET;

		Response.AddHeader("Content-Disposition","attachment;filename=" + fileName);
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		// 検索条件を設定
		ManTweet.SearchCondition oSearchCondition = new ManTweet.SearchCondition();
		oSearchCondition.Keyword = this.txtKeyword.Text.Trim();
		oSearchCondition.LoginId = this.txtLoginId.Text.Trim();
		oSearchCondition.ManTweetSeq = this.txtManTweetSeq.Text.Trim();
		oSearchCondition.TweetDateFrom = string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue,this.lstFromHH.SelectedValue,this.lstFromMI.SelectedValue);
		string sToDD;
		if (this.lstToDD.SelectedIndex == 0) {
			sToDD = DateTime.DaysInMonth(int.Parse(this.lstToYYYY.SelectedValue),int.Parse(this.lstToMM.SelectedValue)).ToString();
		} else {
			sToDD = this.lstToDD.SelectedValue;
		}
		oSearchCondition.TweetDateTo = string.Format("{0}/{1}/{2} {3}:{4}:59",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,sToDD,this.lstToHH.SelectedValue,this.lstToMI.SelectedValue);
		oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;

		using (ManTweet oManTweet = new ManTweet()) {
			DataSet ds = oManTweet.GetList(oSearchCondition);
			// CSVに出力する文字列にタイトル行を設定
			System.Text.StringBuilder oCsvBuilder = new System.Text.StringBuilder();
			oCsvBuilder.Append("書込日時,");
			oCsvBuilder.Append("ログインID,");
			oCsvBuilder.Append("ハンドル名");
			oCsvBuilder.Append("\r\n");
			// CSVに出力する文字列にレコード行を設定
			DataRow dr;
			for (int i = 0;i < ds.Tables[0].Rows.Count;i++) {
				dr = ds.Tables[0].Rows[i];
				oCsvBuilder.Append(dr["TWEET_DATE"]);
				oCsvBuilder.Append(",");
				oCsvBuilder.Append(dr["LOGIN_ID"]);
				oCsvBuilder.Append(",");
				oCsvBuilder.Append(dr["HANDLE_NM"]);
				oCsvBuilder.Append("\r\n");
			}

			Response.BinaryWrite(encoding.GetBytes(oCsvBuilder.ToString()));
			Response.End();
		}
	}

	protected void dsManTweet_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		DataSet oManTweetDataSet = e.ReturnValue as DataSet;
		if (oManTweetDataSet != null && (oManTweetDataSet).Tables[0].Rows.Count > 0) {
			this.ManTweetData = oManTweetDataSet;
		}
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void grdManTweet_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			if (DataBinder.Eval(e.Row.DataItem,"ADMIN_DEL_FLAG").ToString().Equals(ViCommConst.FLAG_ON_STR)) {
				e.Row.BackColor = Color.Gray;
			} else if (DataBinder.Eval(e.Row.DataItem,"DEL_FLAG").ToString().Equals(ViCommConst.FLAG_ON_STR)) {
				e.Row.BackColor = Color.Gray;
			} else if (DataBinder.Eval(e.Row.DataItem,"ADMIN_CHECK_FLAG").ToString().Equals(ViCommConst.FLAG_OFF_STR)) {
				e.Row.BackColor = Color.LavenderBlush;
			} else {
				e.Row.BackColor = Color.White;
			}
		}
	}

	protected void dsManTweet_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		ManTweet.SearchCondition oSearchCondition = new ManTweet.SearchCondition();
		oSearchCondition.Keyword = this.txtKeyword.Text.Trim();
		oSearchCondition.LoginId = this.txtLoginId.Text.Trim();
		oSearchCondition.ManTweetSeq = this.txtManTweetSeq.Text.Trim();
		oSearchCondition.TweetDateFrom = string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue,this.lstFromHH.SelectedValue,this.lstFromMI.SelectedValue);
		string sToDD;
		if (this.lstToDD.SelectedIndex == 0) {
			sToDD = DateTime.DaysInMonth(int.Parse(this.lstToYYYY.SelectedValue),int.Parse(this.lstToMM.SelectedValue)).ToString();
		} else {
			sToDD = this.lstToDD.SelectedValue;
		}
		oSearchCondition.TweetDateTo = string.Format("{0}/{1}/{2} {3}:{4}:59",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,sToDD,this.lstToHH.SelectedValue,this.lstToMI.SelectedValue);
		oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void vdcFromTo_ServerValidate(object source,ServerValidateEventArgs e) {
		if (this.IsValid) {
			if (e.IsValid) {
				DateTime dtFrom;
				DateTime dtTo;
				if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue,this.lstFromHH.SelectedValue,this.lstFromMI.SelectedValue),out dtFrom)) {
					this.vdcFromTo.Text = "書込日時Fromに正しい日時を設定してください。";
					e.IsValid = false;
					return;
				}
				string sToDD;
				if (this.lstToDD.SelectedIndex == 0) {
					sToDD = DateTime.DaysInMonth(int.Parse(this.lstToYYYY.SelectedValue),int.Parse(this.lstToMM.SelectedValue)).ToString();
				} else {
					sToDD = this.lstToDD.SelectedValue;
				}
				if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,sToDD,this.lstToHH.SelectedValue,this.lstToMI.SelectedValue),out dtTo)) {
					this.vdcFromTo.Text = "書込日時Toに正しい日時を設定してください。";
					e.IsValid = false;
					return;
				}
				if (dtFrom > dtTo) {
					this.vdcFromTo.Text = "書込日時の大小関係に誤りがあります。";
					e.IsValid = false;
				}
			}
		}
	}

	private void InitPage() {
		if (!this.IsPostBack) {
			SysPrograms.SetupFromToDayTime(this.lstFromYYYY,this.lstFromMM,this.lstFromDD,this.lstFromHH,this.lstToYYYY,this.lstToMM,this.lstToDD,this.lstToHH,false);
			this.lstFromMM.Items.Insert(0,new ListItem("--","01"));
			this.lstToMM.Items.Insert(0,new ListItem("--","12"));
			this.lstFromDD.Items.Insert(0,new ListItem("--","01"));
			this.lstToDD.Items.Insert(0,new ListItem("--","31"));
			this.lstFromHH.Items.Insert(0,new ListItem("--","00"));
			this.lstToHH.Items.Insert(0,new ListItem("--","23"));

			for (int i = 0;i < 60;i++) {
				this.lstFromMI.Items.Add(new ListItem(string.Format("{0:D2}",i)));
				this.lstToMI.Items.Add(new ListItem(string.Format("{0:D2}",i)));
			}
			this.lstFromMI.Items.Insert(0,new ListItem("--","00"));
			this.lstToMI.Items.Insert(0,new ListItem("--","59"));
		}

		this.recCount = "0";
		this.txtLoginId.Text = string.Empty;
		this.txtManTweetSeq.Text = string.Empty;
		this.txtKeyword.Text = string.Empty;
		this.grdManTweet.DataSourceID = string.Empty;

		this.lstFromYYYY.SelectedIndex = 0;
		this.lstToYYYY.SelectedIndex = 0;
		this.lstFromMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstToMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstFromDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstToDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstFromHH.SelectedIndex = 0;
		this.lstToHH.SelectedIndex = 0;
		this.lstFromMI.SelectedIndex = 0;
		this.lstToMI.SelectedIndex = 0;

		this.pnlInfo.Visible = false;
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		if (!this.IsPostBack) {
			this.SiteCd = this.Request.QueryString["site"];
			this.LoginId = this.Request.QueryString["loginid"];
			this.ManTweetSeq = this.Request.QueryString["tweetseq"];

			string sTweetDate = iBridUtil.GetStringValue(this.Request.QueryString["tweetdate"]);
			if (!string.IsNullOrEmpty(sTweetDate)) {
				DateTime oTweetDate = DateTime.Parse(sTweetDate);

				if (!string.IsNullOrEmpty(sTweetDate)) {
					this.lstFromYYYY.SelectedValue = oTweetDate.ToString("yyyy");
					this.lstToYYYY.SelectedValue = oTweetDate.ToString("yyyy");
					this.lstFromMM.SelectedValue = oTweetDate.ToString("MM");
					this.lstToMM.SelectedValue = oTweetDate.ToString("MM");
					this.lstFromDD.SelectedValue = oTweetDate.ToString("dd");
					this.lstToDD.SelectedValue = oTweetDate.ToString("dd");
					this.lstFromHH.SelectedValue = oTweetDate.ToString("HH");
					this.lstToHH.SelectedValue = oTweetDate.ToString("HH");
					this.lstFromMI.SelectedValue = oTweetDate.ToString("mm");
					this.lstToMI.SelectedValue = oTweetDate.ToString("mm");
				}
			}

			if (!string.IsNullOrEmpty(this.SiteCd)) {
				this.lstSiteCd.SelectedValue = this.SiteCd;
			}

			if (!string.IsNullOrEmpty(this.LoginId)) {
				this.txtLoginId.Text = this.LoginId;
			}

			if (!string.IsNullOrEmpty(this.ManTweetSeq)) {
				this.txtManTweetSeq.Text = this.ManTweetSeq;
			}
		}
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.grdManTweet.PageSize = 30;
		this.grdManTweet.DataSourceID = "dsManTweet";
		this.grdManTweet.DataBind();
		this.pnlCount.DataBind();
	}

	protected string GetClientScript(object pPicUrl) {
		string sRoot = Request.Url.Authority + ConfigurationManager.AppSettings["Root"];
		return string.Format("javascript:win=window.open('http://{1}/ViewPic.html?url={0}','PictureZoom','width=300,height=320,resizable=yes,directories=no,scrollbars=no' , false);win.focus();return false;",pPicUrl,sRoot);
	}

	protected bool GetPicVisible(object pPicSeq) {
		return !string.IsNullOrEmpty(iBridUtil.GetStringValue(pPicSeq));
	}

	protected void lnkDelManTweet_Command(object sender,CommandEventArgs e) {
		string[] arguments = e.CommandArgument.ToString().Split(',');

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("DELETE_MAN_TWEET_ADMIN");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,arguments[0]);
			oDbSession.ProcedureInParm("pMAN_TWEET_SEQ",DbSession.DbType.VARCHAR2,arguments[1]);
			oDbSession.ProcedureInParm("pDEL_FLAG",DbSession.DbType.VARCHAR2,ViCommConst.FLAG_ON_STR.Equals(arguments[2]) ? ViCommConst.FLAG_OFF_STR : ViCommConst.FLAG_ON_STR);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.GetList();
	}

	protected void btnAdminCheck_Click(object sender,EventArgs e) {
		List<string> oManTweetSeqList = new List<string>();

		for (int i = 0;i < this.grdManTweet.Rows.Count;i++) {
			GridViewRow row = this.grdManTweet.Rows[i];
			CheckBox oChk = (CheckBox)row.FindControl("chkAdminCheck");

			if (oChk.Checked) {
				HiddenField hdnManTweetSeq = row.FindControl("hdnManTweetSeq") as HiddenField;
				oManTweetSeqList.Add(hdnManTweetSeq.Value);
			}
		}

		if (oManTweetSeqList.Count > 0) {
			using (DbSession oDbSession = new DbSession()) {
				oDbSession.PrepareProcedure("ADMIN_CHECK_MAN_TWEET");
				oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.lstSiteCd.SelectedValue);
				oDbSession.ProcedureInArrayParm("pMAN_TWEET_SEQ",DbSession.DbType.VARCHAR2,oManTweetSeqList.ToArray());
				oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
				oDbSession.ExecuteProcedure();
			}

			lstSiteCd.DataSourceID = "";
			DataBind();
		}
	}

	protected void btnAdminCheckAll_Click(object sender,EventArgs e) {
		List<string> oManTweetSeqList = new List<string>();

		for (int i = 0;i < this.grdManTweet.Rows.Count;i++) {
			GridViewRow row = this.grdManTweet.Rows[i];
			CheckBox oChk = (CheckBox)row.FindControl("chkAdminCheck");

			HiddenField hdnManTweetSeq = row.FindControl("hdnManTweetSeq") as HiddenField;
			oManTweetSeqList.Add(hdnManTweetSeq.Value);
		}

		if (oManTweetSeqList.Count > 0) {
			using (DbSession oDbSession = new DbSession()) {
				oDbSession.PrepareProcedure("ADMIN_CHECK_MAN_TWEET");
				oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.lstSiteCd.SelectedValue);
				oDbSession.ProcedureInArrayParm("pMAN_TWEET_SEQ",DbSession.DbType.VARCHAR2,oManTweetSeqList.ToArray());
				oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
				oDbSession.ExecuteProcedure();
			}

			lstSiteCd.DataSourceID = "";
			DataBind();
		}
	}
}
