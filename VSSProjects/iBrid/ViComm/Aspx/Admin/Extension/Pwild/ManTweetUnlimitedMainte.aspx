<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
	Title="会員つぶやき無制限設定" CodeFile="ManTweetUnlimitedMainte.aspx.cs" Inherits="Extension_Pwild_ManTweetUnlimitedMainte"
%>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="会員つぶやき無制限設定"></asp:Label>
</asp:Content>

<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlNotes">
			<fieldset class="fieldset">
				<legend>[注意事項]</legend>
				<asp:Panel ID="pnlNoteDtl" runat="server">
					<div>初回入金後経過日数が"0"の場合は、累計入金額が1,000円以上の会員を対象、"1以上"の場合は、初回入金後n日以内かつ累計入金額が1,000円以上の会員を対象に無制限でつぶやきができるようにします。</div>
					<div>初回入金後経過日数にかかわらず終了日時を過ぎると、無制限につぶやくことはできなくなります。</div>
				</asp:Panel>
			</fieldset>
		</asp:Panel>

		<%-- ============================== --%>
		<%--  Search Condition              --%>
		<%-- ============================== --%>
		<asp:Panel runat="server" ID="pnlSearch">
			<fieldset class="fieldset">
				<legend>[検索]</legend>
				<asp:Panel ID="pnlSeekCondition" runat="server">
					<table border="0" style="width: 800px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle2">
								サイトコード
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px">
								</asp:DropDownList>
							</td>
						</tr>
					</table>
				</asp:Panel>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Seek" />
				<asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton"  OnClick="btnCreate_Click" ValidationGroup="Create" />
			</fieldset>
		</asp:Panel>

		<%-- ============================== --%>
		<%--  Setting                       --%>
		<%-- ============================== --%>
		<asp:Panel runat="server" ID="pnlInput">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Label ID="lblErrorMessage" runat="server" Text="" ForeColor="red"></asp:Label>
				<table border="0" style="width: 650px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							初回入金後経過日数
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtElapsedDays" runat="server" MaxLength="6" Width="40px"></asp:TextBox>
							<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtElapsedDays" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							開始日時
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstStartDateYYYY" runat="server" Width="60px"></asp:DropDownList>年
							<asp:DropDownList ID="lstStartDateMM" runat="server" Width="40px"></asp:DropDownList>月
							<asp:DropDownList ID="lstStartDateDD" runat="server" Width="40px"></asp:DropDownList>日
							<asp:DropDownList ID="lstStartDateHH" runat="server" Width="40px"></asp:DropDownList>時
							<asp:DropDownList ID="lstStartDateMI" runat="server" Width="40px"></asp:DropDownList>分
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							終了日時
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstEndDateYYYY" runat="server" Width="60px"></asp:DropDownList>年
							<asp:DropDownList ID="lstEndDateMM" runat="server" Width="40px"></asp:DropDownList>月
							<asp:DropDownList ID="lstEndDateDD" runat="server" Width="40px"></asp:DropDownList>日
							<asp:DropDownList ID="lstEndDateHH" runat="server" Width="40px"></asp:DropDownList>時
							<asp:DropDownList ID="lstEndDateMI" runat="server" Width="40px"></asp:DropDownList>分
						</td>
					</tr>
				</table>
				<asp:Panel ID="pnlCommand" runat="server" Width="660px">
					<asp:HiddenField ID="hdnTweetUnlimitedRowid" runat="server" Value='' />
					<asp:HiddenField ID="hdnTweetUnlimitedRevisionNo" runat="server" Value='' />

					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Update" />
					<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" />
					<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="delBtnStyle" OnClick="btnDelete_Click" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>

		<%-- ============================== --%>
		<%--  Data                          --%>
		<%-- ============================== --%>
		<asp:Panel runat="server" ID="pnlList">
			<fieldset>
				<legend>[一覧]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">
						Record Count <%# GetRecCount() %>
					</a>
					<br />
					<a class="reccount">
						Current viewing page <%= grdData.PageIndex + 1%> of <%= grdData.PageCount%>
					</a>
				</asp:Panel>
				&nbsp;
				<asp:Panel ID="pnlGrid" runat="server">
					<asp:GridView ID="grdData" DataSourceID="dsTweetUnlimited" runat="server" AllowPaging="True" AutoGenerateColumns="False" PageSize="30" EnableSortingAndPagingCallbacks="false" SkinID="GridViewColor" AllowSorting="true">
						<Columns>
							<asp:BoundField DataField="ELAPSED_DAYS" HeaderText="初回入金後経過日数">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:BoundField DataField="START_DATE" HeaderText="開始日時" DataFormatString="{0:yyyy/MM/dd HH:mm}" HtmlEncode="False">
								<ItemStyle HorizontalAlign="Left" Wrap="false" />
							</asp:BoundField>
							<asp:BoundField DataField="END_DATE" HeaderText="終了日時" DataFormatString="{0:yyyy/MM/dd HH:mm}" HtmlEncode="False">
								<ItemStyle HorizontalAlign="Left" Wrap="false" />
							</asp:BoundField>
							<asp:TemplateField>
								<ItemTemplate>
									<asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%# string.Format("{0},{1},{2},{3},{4}",Eval("TWEET_UNLIMITED_ROWID"),Eval("REVISION_NO"),Eval("ELAPSED_DAYS"),Eval("START_DATE", "{0:yyyy-MM-dd-HH-mm}"),Eval("END_DATE", "{0:yyyy-MM-dd-HH-mm}")) %>'
										OnCommand="lnkEdit_Command" Text='編集'>
									</asp:LinkButton>
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
			<br />
		</asp:Panel>
	</div>

	<%-- ============================== --%>
	<%--  Data Source                   --%>
	<%-- ============================== --%>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsTweetUnlimited" runat="server" ConvertNullToDBNull="false" EnablePaging="True" OnSelecting="dsTweetUnlimited_Selecting" OnSelected="dsTweetUnlimited_Selected"
		SelectCountMethod="GetPageCount" SelectMethod="GetPageCollection" SortParameterName="" TypeName="TweetUnlimited">
		<SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>