﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="InvestigateScheduleMainte.aspx.cs" Inherits="Extension_Pwild_InvestigateScheduleMainte"
    Title="この娘を探せ - スケジュール設定" ValidateRequest="false" %>

<%@ Import Namespace="System.Data" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="この娘を探せ - スケジュール設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlMainte">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel runat="server" ID="pnlDtl">
                    <fieldset class="fieldset-inner">
                        <legend>[タイマー設定]</legend>
                        <table border="0" style="width: 800px" class="tableStyle">
                            <tr>
                                <td class="tdHeaderStyle">
                                    第一ヒント発表時間
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstFirstHintAnnounceHH" runat="server" Width="40px" DataSource='<%# HourArray %>'>
                                    </asp:DropDownList>時
                                    <asp:DropDownList ID="lstFirstHintAnnounceMI" runat="server" DataSource='<%# MinuteArray %>'>
                                    </asp:DropDownList>分
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    第二ヒント発表時間
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstSecondHintAnnounceHH" runat="server" Width="40px" DataSource='<%# HourArray %>'>
                                    </asp:DropDownList>時
                                    <asp:DropDownList ID="lstSecondHintAnnounceMI" runat="server" DataSource='<%# MinuteArray %>'>
                                    </asp:DropDownList>分
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    第三ヒント発表時間
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstThirdHintAnnounceHH" runat="server" Width="40px" DataSource='<%# HourArray %>'>
                                    </asp:DropDownList>時
                                    <asp:DropDownList ID="lstThirdHintAnnounceMI" runat="server" DataSource='<%# MinuteArray %>'>
                                    </asp:DropDownList>分
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle2">
                                    付与ポイント(全体)
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox runat="server" ID="txtBountyPoint" Width="50px"></asp:TextBox>
                                    <asp:RangeValidator ID="vdrBountyPoint" runat="server" ErrorMessage="懸賞ポイントを正しく入力して下さい。"
                                        ControlToValidate="txtBountyPoint" MaximumValue="999999" MinimumValue="1" Type="Integer"
                                        ValidationGroup="Update" Display="Dynamic">*</asp:RangeValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
                                        TargetControlID="vdrBountyPoint" HighlightCssClass="validatorCallout" />
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                        Enabled="true" FilterType="Numbers" TargetControlID="txtBountyPoint" />
                                </td>
                            </tr>
                        </table>
                        <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click"
                            ValidationGroup="Detail" />
                        <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click"
                            CausesValidation="False" />
                        <asp:CustomValidator ID="vdcTime" runat="server" ErrorMessage="" OnServerValidate="vdcTime_ServerValidate"
                            ValidationGroup="Detail"></asp:CustomValidator>
                    </fieldset>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset>
                <legend>[設定]</legend>
                <table border="0" style="width: 640px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle2">
                            サイトコード
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="180px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    CausesValidation="False" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" />
            </fieldset>
        </asp:Panel>
        <asp:Panel ID="pnlList" runat="server" ScrollBars="auto">
            <fieldset>
                <legend>[スケジュール設定]</legend>
                <asp:Panel runat="server" ID="pnlLink">
                    <asp:HyperLink runat="server" ID="lnkLastYear" Text='<%# GetYear(-1) + "年へ" %>' NavigateUrl='<%# GetTargetMonthLink(string.Format("{0}/12", GetYear(-1))) %>'></asp:HyperLink>
                    &nbsp;
                    <asp:Repeater runat="server" ID="rptMonthLink" DataSource='<%# MonthArray %>'>
                        <ItemTemplate>
                            <asp:HyperLink runat="server" ID="lnkMonth" Text='<%# Container.DataItem + "月" %>'
                                ForeColor='<%# GetMonthLinkColor(string.Format("{0}/{1:D2}", GetYear(0), Int32.Parse(Container.DataItem.ToString()))) %>'
                                NavigateUrl='<%# GetTargetMonthLink(string.Format("{0}/{1:D2}", GetYear(0), Int32.Parse(Container.DataItem.ToString()))) %>'></asp:HyperLink>
                            &nbsp;
                        </ItemTemplate>
                    </asp:Repeater>
                    <asp:HyperLink runat="server" ID="lnkNextYear" Text='<%# GetYear(1) + "年へ" %>' NavigateUrl='<%# GetTargetMonthLink(string.Format("{0}/01", GetYear(1))) %>'></asp:HyperLink>
                    <br />
                </asp:Panel>
                <asp:Label runat="server" ID="lblNoData" Text="登録されたデータがありません。" ForeColor="red" Visible="false"></asp:Label>
                <asp:GridView ID="grdInvestigateSchedule" runat="server" AutoGenerateColumns="False" DataSourceID="dsInvestigateSchedule"
                    SkinID="GridViewColor" OnRowDataBound="grdInvestigateSchedule_RowDataBound" DataKeyNames="SITE_CD,EXECUTION_DAY">
                    <Columns>
                        <asp:TemplateField HeaderText="日">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblDay" Text='<%# int.Parse(Eval("DAY").ToString()) %>' />
                                (<asp:Label runat="server" ID="lblFormattedDay" Text='<%# Eval("DAY_OF_WEEK") %>'
                                    ForeColor='<%# GetDayOfWeekColor(Eval("DAY_OF_WEEK")) %>' />)
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="第一ﾋﾝﾄ発表時間">
                            <ItemTemplate>
                                <asp:Label ID="lblFirstHintAnnounceTime" runat="server" Text='<%# Eval("FIRST_HINT_ANNOUNCE_TIME", "{0:HH:mm:ss}") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="第二ﾋﾝﾄ発表時間">
                            <ItemTemplate>
                                <asp:Label ID="lblSecondHintAnnounceTime" runat="server" Text='<%# Eval("SECOND_HINT_ANNOUNCE_TIME", "{0:HH:mm:ss}") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="第三ﾋﾝﾄ発表時間">
                            <ItemTemplate>
                                <asp:Label ID="lblThirdHintAnnounceTime" runat="server" Text='<%# Eval("THIRD_HINT_ANNOUNCE_TIME", "{0:HH:mm:ss}") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="付与pt(全体)">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblPointPerEntrant" Text='<%# Eval("POINT_PER_ENTRANT") %>' />
                                (<asp:Label runat="server" ID="lblBountyPoint" Text='<%# Eval("BOUNTY_POINT") %>' />)
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnUpdate" runat="server" CommandArgument='<%# Container.DataItemIndex %>'
                                    ValidationGroup="Update" OnCommand="btnUpdate_Command" Text="変更"></asp:Button>
                                <asp:HiddenField runat="server" ID="hdrRevisionNo" Value='<%# Eval("REVISION_NO") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsInvestigateSchedule" runat="server" SelectMethod="GetList"
        OnSelecting="dsInvestigateSchedule_Selecting" OnSelected="dsInvestigateSchedule_Selected"
        TypeName="InvestigateSchedule">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSeekSiteCd" DbType="String" PropertyName="SelectedValue"
                Name="pSiteCd" />
            <asp:Parameter Name="pTargetMonth" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
