﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="StageClearCountReport.aspx.cs" Inherits="Extension_Pwild_StageClearCountReport" Title="ステージクリア分布レポート" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="ステージクリア分布レポート"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset>
                <legend>[設定]</legend>
                <table border="0" style="width: 640px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            サイトコード

                        </td>
                        <td class="tdDataStyle" colspan="3">
                            <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="180px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    CausesValidation="False" />
                <asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" CausesValidation="False" OnClick="btnCSV_Click" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[ステージクリア分布レポート]</legend>
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="552px">
                    <asp:GridView ID="grdStageClearCountReport" runat="server" AutoGenerateColumns="False" DataSourceID="dsStageClearCountReport"
                        AllowSorting="true" SkinID="GridViewColor">
                        <Columns>
                            <asp:TemplateField HeaderText="ステージ">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkStageNm" runat="server" NavigateUrl='<%# string.Format("~/Extension/Pwild/StageLevelCountReport.aspx?stageseq={0}&sexcd={1}",Eval("STAGE_SEQ"),Eval("SEX_CD"))%>' 
                                    Text='<%# Eval("STAGE_NM") %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="人数">
                                <ItemTemplate>
                                    <asp:Label ID="lblCount" runat="server" Text='<%# Eval("STAGE_COUNT") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle ForeColor="Black" BackColor="LightYellow" Font-Bold="true" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsStageClearCountReport" runat="server" SelectMethod="GetList" TypeName="StageClearCountReport">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSeekSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
