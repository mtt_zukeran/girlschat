﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: イベント大カテゴリ一覧

--	Progaram ID		: EventCategoryList
--
--  Creation Date	: 2013.07.04
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.IO;
using ViComm.Extension.Pwild;

public partial class Extension_Pwild_EventCategoryList:System.Web.UI.Page {
	protected string[] DummyArray = new string[3];
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string EventCategorySeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["EventCategorySeq"]);
		}
		set {
			this.ViewState["EventCategorySeq"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
		this.EventCategorySeq = string.Empty;
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.UpdateData(false);
		this.pnlEventCategoryInfo.Visible = false;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		this.UpdateData(true);
		this.pnlEventCategoryInfo.Visible = false;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.ClearFileds();
		this.pnlKey.Enabled = true;
		this.pnlEventCategoryInfo.Visible = false;
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.EventCategorySeq = string.Empty;
		this.ClearFileds();
		this.pnlKey.Enabled = false;
		this.btnDelete.Enabled = false;
		this.pnlEventCategoryInfo.Visible = true;
	}

	protected void lnkEventCategoryNm_Command(object sender,CommandEventArgs e) {

		if (!this.IsValid) {
			return;
		}
		this.EventCategorySeq = iBridUtil.GetStringValue(e.CommandArgument);
		this.GetData();
		this.pnlKey.Enabled = false;
		this.btnDelete.Enabled = true;
		this.pnlEventCategoryInfo.Visible = true;

	}

	protected void dsEventCategory_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	private void InitPage() {
		this.pnlInfo.Visible = true;
		this.pnlEventCategoryInfo.Visible = false;

		this.EventCategorySeq = string.Empty;
		this.pnlKey.Enabled = true;
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void ClearFileds() {
		this.lblEventCategorySeq.Text = string.Empty;
		this.txtEventCategoryNm.Text = string.Empty;

		this.pnlKey.Enabled = true;
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.EventCategorySeq = string.Empty;

		this.grdEventCategory.PageIndex = 0;
		this.grdEventCategory.DataSourceID = "dsEventCategory";
		this.grdEventCategory.DataBind();
		this.pnlCount.DataBind();
	}

	private void GetData() {
		this.ClearFileds();

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("EVENT_CATEGORY_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pEVENT_CATEGORY_SEQ",DbSession.DbType.NUMBER,this.EventCategorySeq);
			oDbSession.ProcedureOutParm("pEVENT_CATEGORY_NM",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.lblEventCategorySeq.Text = oDbSession.GetStringValue("pEVENT_CATEGORY_SEQ");
			this.txtEventCategoryNm.Text = oDbSession.GetStringValue("pEVENT_CATEGORY_NM");
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
		}
	}

	private void UpdateData(bool pDelFlag) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("EVENT_CATEGORY_MAINTE");
			oDbSession.cmd.BindByName = true;
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pEVENT_CATEGORY_SEQ",DbSession.DbType.VARCHAR2,this.EventCategorySeq);
			oDbSession.ProcedureInParm("pEVENT_CATEGORY_NM",DbSession.DbType.VARCHAR2,this.txtEventCategoryNm.Text.TrimEnd());
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.ClearFileds();
		this.GetList();
	}
}
