﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ManTweetPicList.aspx.cs" Inherits="Extension_Pwild_ManTweetPicList" Title="会員つぶやき認証待ち画像"
    ValidateRequest="false" %>
<%@ Import Namespace="ViComm" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="会員つぶやき認証待ち画像"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 800px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="tdHeaderStyle">
                                ﾛｸﾞｲﾝID
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtLoginId" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[記事一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdManTweet.PageIndex + 1 %>
                        of
                        <%= grdManTweet.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="443px">
                    <asp:GridView ID="grdManTweet" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        DataSourceID="dsManTweet" SkinID="GridViewColor" AllowSorting="true" DataKeyNames="SITE_CD,MAN_TWEET_SEQ,MAN_TWEET_PIC_SEQ">
                        <Columns>
                            <asp:TemplateField HeaderText="写真" SortExpression="MAN_TWEET_PIC_SEQ">
							<ItemTemplate>
								<asp:ImageButton ID="ImageButton1" runat="server" ImageUrl='<%# Eval("MAN_TWEET_SMALL_IMG_PATH", "../../{0}") %>' OnClientClick='<%# GetClientScript(Eval("MAN_TWEET_IMG_PATH"))  %>'>
								</asp:ImageButton>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ログインID">
							<ItemStyle Wrap="false" />
							<ItemTemplate>
								<asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>'
									Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ﾊﾝﾄﾞﾙ名">
							<ItemStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>' ID="lblHandleNm" />
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="認証">
							<ItemStyle HorizontalAlign="Center" Width="50px" />
							<ItemTemplate>
								<asp:Button ID="btnAuth" runat="server" CommandArgument='<%# Container.DataItemIndex %>' OnClick="btnAuth_OnClick" Text="認証" OnClientClick="return confirm('認証を行いますか？');">
								</asp:Button>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="削除">
							<ItemStyle HorizontalAlign="Center" Width="50px" />
							<ItemTemplate>
								<asp:Button ID="btnDelete" runat="server" CommandArgument='<%# Container.DataItemIndex %>' OnClick="btnDelete_OnClick" Text="削除" OnClientClick="return confirm('削除を行いますか？');">
								</asp:Button>
							</ItemTemplate>
						</asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
            <br />
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsManTweet" runat="server" SelectMethod="GetPageCollection" ConvertNullToDBNull="true" SortParameterName=""
        TypeName="ManTweet" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelected="dsManTweet_Selected"
        OnSelecting="dsManTweet_Selecting">
        <SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
</asp:Content>
