﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 会員ページビュー表示(日別)
--	Progaram ID		: UserPageViewDailyList
--  Creation Date	: 2014.02.12
--  Creater			: Y.Ikemiya
**************************************************************************/
using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using ViComm.Extension.Pwild;

public partial class Extension_Pwild_UserPageViewDailyList:System.Web.UI.Page {
	private string recCount = string.Empty;

	/// <summary>CSVファイル名</summary>
	private const string CSV_FILE_NM_USER_PV_DAILY = "UserPageViewDaily.csv";

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SITE_CD"]);
		}
		set {
			this.ViewState["SITE_CD"] = value;
		}
	}

	private string AccessType {
		get {
			return iBridUtil.GetStringValue(this.ViewState["ACCESS_TYPE"]);
		}
		set {
			this.ViewState["ACCESS_TYPE"] = value;
		}
	}

	private string Year {
		get {
			return iBridUtil.GetStringValue(this.ViewState["YEAR"]);
		}
		set {
			this.ViewState["YEAR"] = value;
		}
	}

	private string Month {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MONTH"]);
		}
		set {
			this.ViewState["MONTH"] = value;
		}
	}

	private string LoginFlag {
		get {
			return iBridUtil.GetStringValue(this.ViewState["LOGIN_FLAG"]);
		}
		set {
			this.ViewState["LOGIN_FLAG"] = value;
		}
	}

	private string PageUserAgentType {
		get {
			return iBridUtil.GetStringValue(this.ViewState["PAGE_USER_AGENT_TYPE"]);
		}
		set {
			this.ViewState["PAGE_USER_AGENT_TYPE"] = value;
		}
	}

	private string ZeroAccessDispFrag {
		get {
			return iBridUtil.GetStringValue(this.ViewState["ZERO_ACCESS_DISP_FLAG"]);
		}
		set {
			this.ViewState["ZERO_ACCESS_DISP_FLAG"] = value;
		}
	}

	private string UserRank {
		get {
			return iBridUtil.GetStringValue(this.ViewState["USER_RANK"]);
		}
		set {
			this.ViewState["USER_RANK"] = value;
		}
	}

	private string NewUserFlag {
		get {
			return iBridUtil.GetStringValue(this.ViewState["NEW_USER_FLAG"]);
		}
		set {
			this.ViewState["NEW_USER_FLAG"] = value;
		}
	}

	private string CarrierType {
		get {
			return iBridUtil.GetStringValue(this.ViewState["CARRIER_TYPE"]);
		}
		set {
			this.ViewState["CARRIER_TYPE"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			this.CreateList();
			this.InitPage();
			pnlGrid.DataBind();
		}
	}

	/// <summary>
	/// 検索条件のオブジェクトに入力値を設定
	/// </summary>
	/// <param name="oSearchCondition"></param>
	private void SetSearchConditions(ref UserPageViewDaily.SearchCondition oSearchCondition) {
		oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
		oSearchCondition.AccessType = this.lstAccessType.SelectedValue;
		oSearchCondition.ReportMonth = string.Format("{0}-{1}",this.lstYear.SelectedValue,this.lstMonth.SelectedValue);
		oSearchCondition.LoginFlag = this.lstLoginType.SelectedValue;
		oSearchCondition.PageUserAgentType = this.lstPageUserAgentType.SelectedValue;
		oSearchCondition.ZeroAccessNotDisp = this.rdoZeroAccessNotDisp.SelectedValue;
		oSearchCondition.UserRank = this.rdoUserRank.SelectedValue;
		oSearchCondition.NewUserFlag = this.rdoNewUserFlag.SelectedValue;
		oSearchCondition.CarrierType = this.lstCarrierType.SelectedValue;
	}

	protected void dsUserPageViewDaily_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		UserPageViewDaily.SearchCondition oSearchCondition = new UserPageViewDaily.SearchCondition();
		this.SetSearchConditions(ref oSearchCondition);
		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsUserPageViewDaily_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		
		this.grdUserPageViewDaily.PageIndex = 0;
		this.GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	/// <summary>
	/// CSV出力ボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnCSV_Click(object sender,EventArgs e) {
		string fileName = CSV_FILE_NM_USER_PV_DAILY;
		int days = DateTime.DaysInMonth(int.Parse(lstYear.SelectedValue),int.Parse(lstMonth.SelectedValue));

		Response.AddHeader("Content-Disposition","attachment;filename=" + fileName);
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		UserPageViewDaily.SearchCondition oSearchCondition = new UserPageViewDaily.SearchCondition();
		this.SetSearchConditions(ref oSearchCondition);

		using (UserPageViewDaily oUserPageViewDaily = new UserPageViewDaily()) {
			DataSet ds = oUserPageViewDaily.GetPageCollection(oSearchCondition,0,SysConst.DB_MAX_ROWS);
			DataRow dr;

			StringBuilder oCsvBuilder = new StringBuilder();

			oCsvBuilder.Append("集計日付＝");
			oCsvBuilder.AppendLine(oSearchCondition.ReportMonth.Replace("-","/"));

			// タイトル行
			oCsvBuilder.Append("画面名称");
			oCsvBuilder.Append(",合計");
			for (int i = 1;i <= days;i++) {
				oCsvBuilder.Append(",");
				oCsvBuilder.Append(i);
			}
			oCsvBuilder.AppendLine("");

			// データ行
			for (int i = 0;i < ds.Tables[0].Rows.Count;i++) {
				dr = ds.Tables[0].Rows[i];
				oCsvBuilder.Append(GetPageNm(dr["PROGRAM_ROOT"],dr["HTML_DOC_TYPE"],dr["PROGRAM_NM"]));
				oCsvBuilder.Append(string.Format(",{0}",dr["TOTAL_COUNT"].ToString()));
				for (int j = 1;j <= days;j++) {
					oCsvBuilder.Append(string.Format(",{0}",dr[string.Format("COUNT{0:D2}",j)].ToString()));
				}
				oCsvBuilder.AppendLine("");
			}

			string sCsvRec = DisplayWordUtil.Replace(oCsvBuilder.ToString());

			Response.BinaryWrite(encoding.GetBytes(sCsvRec));
			Response.End();
		}
	}

	private void GetList() {
		this.grdUserPageViewDaily.PageIndex = 0;
		this.grdUserPageViewDaily.PageSize = 50;
		this.grdUserPageViewDaily.DataSourceID = "dsUserPageViewDaily";
		this.grdUserPageViewDaily.DataBind();
		this.pnlCount.DataBind();
	}

	protected void grdUserPageViewDaily_RowDataBound(object sender,GridViewRowEventArgs e) {
		DateTime dtToday = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd"));
		int iDayCount = DateTime.Parse(string.Format("{0}/{1}/01",this.lstYear.SelectedValue,this.lstMonth.SelectedValue)).AddMonths(1).AddDays(-1).Day;
		if (e.Row.RowType == DataControlRowType.DataRow) {
			e.Row.Cells[1].HorizontalAlign = HorizontalAlign.Center;

			for (int i = 2;i < e.Row.Cells.Count;i++) {
				e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
			}

			for (int i = 0;i < 31;i++) {
				if (iDayCount > i) {
					if (dtToday <= DateTime.Parse(string.Format("{0}/{1}/{2}",this.lstYear.SelectedValue,this.lstMonth.SelectedValue,string.Format("{0:D2}",i + 1)))) {
						e.Row.Cells[i + 3].Text = string.Empty;
					}
				} else {
					e.Row.Cells[i + 3].Visible = false;
				}
			}
		} else if (e.Row.RowType == DataControlRowType.Footer) {
			e.Row.BackColor = Color.LightGoldenrodYellow;
			UserPageViewDaily.SearchCondition oSearchCondition = new UserPageViewDaily.SearchCondition();
			this.SetSearchConditions(ref oSearchCondition);

			DataSet oDataSet;

			using (UserPageViewDaily oUserPageViewDaily = new UserPageViewDaily()) {
				oDataSet = oUserPageViewDaily.GetTotalCountCollection(oSearchCondition);
			}

			e.Row.Cells[0].Text = "合計";
			e.Row.Cells[2].Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["TOTAL_COUNT"]);

			for (int i = 0;i < 31;i++) {
				e.Row.Cells[i + 3].Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0][string.Format("COUNT{0:D2}",i + 1)]);

				if (iDayCount > i) {
					if (dtToday <= DateTime.Parse(string.Format("{0}/{1}/{2}",this.lstYear.SelectedValue,this.lstMonth.SelectedValue,string.Format("{0:D2}",i + 1)))) {
						e.Row.Cells[i + 3].Text = string.Empty;
					}
				} else {
					e.Row.Cells[i + 3].Visible = false;
				}
			}

			e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Left;
		} else if (e.Row.RowType == DataControlRowType.Header) {
			for (int i = 0;i < 31;i++) {
				if (iDayCount <= i) {
					e.Row.Cells[i + 3].Visible = false;
				}
			}
		}
	}

	private void InitPage() {
		lstAccessType.SelectedValue = PwViCommConst.AccessType.ALL_ACCESS;
		this.lstYear.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstMonth.SelectedValue = DateTime.Now.ToString("MM");
		lstLoginType.SelectedValue = ViCommConst.FLAG_ON_STR;
		lstPageUserAgentType.SelectedValue = ViCommConst.DEVICE_SMART_PHONE;
		rdoZeroAccessNotDisp.SelectedValue = ViCommConst.FLAG_ON_STR;
		rdoUserRank.SelectedIndex = 0;
		rdoNewUserFlag.SelectedIndex = 0;
		lstCarrierType.SelectedIndex = 0;
	}

	private void CreateList() {
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		
		int iStartYear = 2014;
		int iCurYear = DateTime.Now.Year;
		lstYear.Items.Clear();

		for (int i = 0;i + iStartYear <= iCurYear;i++) {

			lstYear.Items.Add(new ListItem(iBridUtil.GetStringValue(i + iStartYear)));
		}

		lstMonth.Items.Clear();
		for (int i = 1;i <= 12;i++) {
			lstMonth.Items.Add(new ListItem(string.Format("{0:D2}",i)));
		}
	}

	protected string GetPageNm(object pProgramRoot,object pHtmlDocType,object pProgramNm) {
		string sProgramRoot = iBridUtil.GetStringValue(pProgramRoot);
		string sHtmlDocType = iBridUtil.GetStringValue(pHtmlDocType);
		string sProgramNm = iBridUtil.GetStringValue(pProgramNm);

		string sPageNm = string.Empty;

		if (!sHtmlDocType.Equals("-")) {
			sPageNm = sHtmlDocType + " ";
		}

		sPageNm += sProgramNm;

		if (sProgramRoot.Equals(ViCommConst.PGM_ROOT_WOMAN)) {
			sPageNm += " ♀";
		}

		return sPageNm;
	}

	protected string GetNavigateUrl(object pProgramRoot,object pProgramId,object pHtmlDocType) {
		string sProgramRoot = iBridUtil.GetStringValue(pProgramRoot);
		string sProgramId = iBridUtil.GetStringValue(pProgramId);
		string sHtmlDocType = iBridUtil.GetStringValue(pHtmlDocType);

		string sNavigateUrl = string.Empty;
		
		string sLastDay = iBridUtil.GetStringValue(DateTime.Parse(string.Format("{0}/{1}/01",this.lstYear.SelectedValue,this.lstMonth.SelectedValue)).AddMonths(1).AddDays(-1).Day);

		sNavigateUrl = string.Format("UserPageViewTermList.aspx?site={0}&programroot={1}&programid={2}&htmldoctype={3}&accesstype={4}&yearfrom={5}&monthfrom={6}&dayfrom={7}&yearto={8}&monthto={9}&dayto={10}&loginflag={11}&pageuseragenttype={12}&zeroaccessnotdisp={13}&userrank={14}&newuserflag={15}&carriertype={16}",
							this.lstSiteCd.SelectedValue,
							sProgramRoot,
							sProgramId,
							sHtmlDocType,
							this.lstAccessType.SelectedValue,
							this.lstYear.SelectedValue,
							this.lstMonth.SelectedValue,
							"01",
							this.lstYear.SelectedValue,
							this.lstMonth.SelectedValue,
							sLastDay,
							this.lstLoginType.SelectedValue,
							this.lstPageUserAgentType.SelectedValue,
							this.rdoZeroAccessNotDisp.SelectedValue,
							this.rdoUserRank.SelectedValue,
							this.rdoNewUserFlag.SelectedValue,
							this.lstCarrierType.SelectedValue
						);

		return sNavigateUrl;
	}
	
	protected string GetNavigateUrlDaily(string pDay) {
		string sNavigateUrl = string.Empty;

		string sLastDay = iBridUtil.GetStringValue(DateTime.Parse(string.Format("{0}/{1}/01",this.lstYear.SelectedValue,this.lstMonth.SelectedValue)).AddMonths(1).AddDays(-1).Day);

		sNavigateUrl = string.Format("UserPageViewHourlyList.aspx?site={0}&accesstype={1}&year={2}&month={3}&day={4}&loginflag={5}&pageuseragenttype={6}&zeroaccessnotdisp={7}&userrank={8}&newuserflag={9}&carriertype={10}",
							this.lstSiteCd.SelectedValue,
							this.lstAccessType.SelectedValue,
							this.lstYear.SelectedValue,
							this.lstMonth.SelectedValue,
							pDay,
							this.lstLoginType.SelectedValue,
							this.lstPageUserAgentType.SelectedValue,
							this.rdoZeroAccessNotDisp.SelectedValue,
							this.rdoUserRank.SelectedValue,
							this.rdoNewUserFlag.SelectedValue,
							this.lstCarrierType.SelectedValue
						);

		return sNavigateUrl;
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	protected string GetNavigateUrlPageMainte(object pProgramRoot,object pProgramId,object pHtmlDocType) {
		string sProgramRoot = iBridUtil.GetStringValue(pProgramRoot);
		string sProgramId = iBridUtil.GetStringValue(pProgramId);
		string sHtmlDocType = iBridUtil.GetStringValue(pHtmlDocType);

		string sNavigateUrl = string.Empty;

		string sLastDay = iBridUtil.GetStringValue(DateTime.Parse(string.Format("{0}/{1}/01",this.lstYear.SelectedValue,this.lstMonth.SelectedValue)).AddMonths(1).AddDays(-1).Day);

		if (sProgramId.Equals("DisplayDoc.aspx") || sProgramId.Equals("GameDisplayDoc.aspx")) {
			sNavigateUrl = string.Format("~/Site/SiteHtmlDocList.aspx?sitecd={0}&htmldoctype={1}&useragenttype={2}&direct=1",
								this.lstSiteCd.SelectedValue,
								sHtmlDocType,
								this.lstPageUserAgentType.SelectedValue
							);
		} else {
			sNavigateUrl = string.Format("~/Site/UserViewList.aspx?sitecd={0}&pgmroot={1}&pgmid={2}&useragenttype={3}&direct=1",
								this.lstSiteCd.SelectedValue,
								sProgramRoot,
								sProgramId,
								this.lstPageUserAgentType.SelectedValue
							);
		}

		return sNavigateUrl;
	}
}