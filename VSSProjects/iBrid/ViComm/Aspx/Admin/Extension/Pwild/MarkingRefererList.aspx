﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MarkingRefererList.aspx.cs" Inherits="Extension_Pwild_MarkingRefererList" Title="足あとリンク元設定" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="足あとリンク元設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" Runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlKey">
			<fieldset>
				<legend>[検索条件]</legend>
				<table border="0" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle2">
							サイトコード
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
							</asp:DropDownList>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
				<asp:Button runat="server" ID="btnRegist" Text="追加" CssClass="seekbutton" OnClick="btnRegist_Click" CausesValidation="False" />
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Label ID="lblErrorMessage" runat="server" Text="" ForeColor="red"></asp:Label>
				<table border="0" style="margin-bottom:3px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							ASPXファイル名
						</td>
						<td class="tdDataStyle">
							<asp:TextBox runat="server" ID="txtAspxNm" MaxLength="50" Width="300px"></asp:TextBox>
							<asp:RequiredFieldValidator ID="vdrAspxNm" runat="server" ErrorMessage="ASPXファイル名を入力して下さい" ControlToValidate="txtAspxNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							表示名称
						</td>
						<td class="tdDataStyle">
							<asp:TextBox runat="server" ID="txtDisplayNm" MaxLength="50" Width="200px"></asp:TextBox>
							<asp:RequiredFieldValidator ID="vdrDisplayNm" runat="server" ErrorMessage="表示名称を入力して下さい" ControlToValidate="txtDisplayNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							ASPXファイル名の<br />
							記入例
						</td>
						<td class="tdDataStyle">
							サイト構成HTML文章の場合&nbsp;：&nbsp;DisplayDoc.aspx?doc=001<br />
							複製ページの場合&nbsp;：&nbsp;ListFavorit.aspx?scrid=01<br />
							ピックアップページの場合&nbsp;：&nbsp;ListPickup.aspx?pickupid=01<br />
							デコメ等からの直アクセスの場合&nbsp;：&nbsp;none<br />
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
				<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
			</fieldset>
		</asp:Panel>
		<fieldset class="fieldset">
			<legend>[一覧]</legend>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">
					Record Count <%# GetRecCount() %>
				</a>
				<br />
				<a class="reccount">
					Current viewing page <%=grdData.PageIndex + 1%> of <%=grdData.PageCount%>
				</a>
			</asp:Panel>
			&nbsp;
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto">
				<asp:GridView ID="grdData" runat="server" DataSourceID="" AllowSorting="false" AllowPaging="True" AutoGenerateColumns="False" SkinID="GridView" PageSize="100">
					<Columns>
						<asp:BoundField DataField="ASPX_NM" HeaderText="ASPXファイル名" HtmlEncode="False">
						</asp:BoundField>
						<asp:BoundField DataField="DISPLAY_NM" HeaderText="表示名称" HtmlEncode="False">
						</asp:BoundField>
						<asp:TemplateField HeaderText="操作">
							<ItemTemplate>
								<asp:LinkButton ID="lnkEdit" runat="server" Text="編集" CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("MARKING_REFERER_SEQ")) %>' OnCommand="lnkEdit_Command" CausesValidation="False">
								</asp:LinkButton>
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
				    <PagerSettings Mode="NumericFirstLast" />
				</asp:GridView>
			</asp:Panel>
		</fieldset>
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsData" runat="server" TypeName="MarkingReferer" SelectMethod="GetPageCollection" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelecting="dsData_Selecting" OnSelected="dsData_Selected">
		<SelectParameters>
			<asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrAspxNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrDisplayNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>

