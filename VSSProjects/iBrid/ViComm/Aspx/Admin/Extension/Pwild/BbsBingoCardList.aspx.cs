﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: お宝deビンゴカード
--	Progaram ID		: BbsBingoCardList
--  Creation Date	: 2013.10.29
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using ViComm.Extension.Pwild;

public partial class Extension_Pwild_BbsBingoCardList:System.Web.UI.Page {
	protected const int HORIZONTAL_LIMIT_COUNT = 5;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SITE_CD"]);
		}
		set {
			this.ViewState["SITE_CD"] = value;
		}
	}

	private string TermSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["TERM_SEQ"]);
		}
		set {
			this.ViewState["TERM_SEQ"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		this.lblRegistError.Text = string.Empty;
		this.rptBbsBingoCard.DataSourceID = string.Empty;

		if (!this.IsPostBack) {
			this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			this.TermSeq = iBridUtil.GetStringValue(Request.QueryString["termseq"]);
			this.GetList();
		}
	}

	private void GetList() {
		this.rptBbsBingoCard.DataSourceID = "dsBbsBingoCard";
		this.rptBbsBingoCard.DataBind();
		this.lblCardCount.DataBind();
	}

	protected void dsBbsBingoCard_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		BbsBingoCard.SearchCondition oSearchCondition = new BbsBingoCard.SearchCondition();
		oSearchCondition.SiteCd = this.SiteCd;
		oSearchCondition.TermSeq = this.TermSeq;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void btnBack_Click(object sender,EventArgs e) {
		this.Response.Redirect(string.Format("~/Extension/Pwild/BbsBingoTermList.aspx?sitecd={0}",this.SiteCd));
		return;
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		if (this.CheckInputRegist()) {
			string sResult = string.Empty;

			using (DbSession oDbSession = new DbSession()) {
				oDbSession.PrepareProcedure("BBS_BINGO_CARD_REGIST");
				oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
				oDbSession.ProcedureInParm("pTERM_SEQ",DbSession.DbType.NUMBER,this.TermSeq);
				oDbSession.ProcedureInParm("pADD_COUNT",DbSession.DbType.NUMBER,int.Parse(txtAddCount.Text));
				oDbSession.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
				oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
				oDbSession.cmd.BindByName = true;
				oDbSession.ExecuteProcedure();
				sResult = oDbSession.GetStringValue("pRESULT");
			}

			if (sResult.Equals(PwViCommConst.BbsBingoCardRegistResult.RESULT_OK)) {
				this.txtAddCount.Text = string.Empty;
				this.GetList();
			} else if (sResult.Equals(PwViCommConst.BbsBingoCardRegistResult.RESULT_NG_LIMIT)) {
				lblRegistError.Text = "作成できるのは99枚までです";
			}
		}
	}

	protected void btnHighPrbFlagOn_Click(object sender,EventArgs e) {
		this.BbsBingoCardUpdate(null,ViCommConst.FLAG_ON);
		this.GetList();
	}

	protected void btnHighPrbFlagOff_Click(object sender,EventArgs e) {
		this.BbsBingoCardUpdate(null,ViCommConst.FLAG_OFF);
		this.GetList();
	}

	protected void lnkUpdate_Command(object sender,CommandEventArgs e) {
		if (!e.CommandName.Equals("UPDATE")) {
			return;
		}

		int iIndex = int.Parse(iBridUtil.GetStringValue(e.CommandArgument));
		RepeaterItem oCardItem = this.rptBbsBingoCard.Items[iIndex];
		HiddenField oCardNo = oCardItem.FindControl("hdnCardNo") as HiddenField;
		HiddenField oHighPrbFlag = oCardItem.FindControl("hdnHighPrbFlag") as HiddenField;

		int iCardNo = int.Parse(oCardNo.Value);
		int iHighPrbFlag = ViCommConst.FLAG_ON_STR.Equals(oHighPrbFlag.Value) ? ViCommConst.FLAG_OFF : ViCommConst.FLAG_ON;

		this.BbsBingoCardUpdate(iCardNo,iHighPrbFlag);
		this.GetList();
	}

	private void BbsBingoCardUpdate(int? pCardNo,int pHighPrbFlag) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("BBS_BINGO_CARD_UPDATE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pTERM_SEQ",DbSession.DbType.NUMBER,this.TermSeq);
			oDbSession.ProcedureInParm("pCARD_NO",DbSession.DbType.NUMBER,pCardNo);
			oDbSession.ProcedureInParm("pHIGH_PRB_FLAG",DbSession.DbType.NUMBER,pHighPrbFlag);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
		}
	}

	protected string GetHighPrbFlagStr(object pHighPrbFlag) {
		return (pHighPrbFlag.ToString().Equals(ViCommConst.FLAG_ON_STR)) ? "高確率中" : "通常";
	}

	protected Color GetHighPrbFlagColor(object pHighPrbFlag) {
		return (pHighPrbFlag.ToString().Equals(ViCommConst.FLAG_ON_STR)) ? Color.Red : Color.Blue;
	}

	protected int GetCardCount() {
		BbsBingoCard.SearchCondition oCondition = new BbsBingoCard.SearchCondition();
		oCondition.SiteCd = this.SiteCd;
		oCondition.TermSeq = this.TermSeq;

		using (BbsBingoCard oBbsBingoCard = new BbsBingoCard()) {
			return oBbsBingoCard.GetCount(oCondition);
		}
	}

	private bool CheckInputRegist() {
		int iAddCount;

		if (string.IsNullOrEmpty(txtAddCount.Text)) {
			this.lblRegistError.Text = "追加数を入力して下さい";
			return false;
		} else if (!int.TryParse(this.txtAddCount.Text,out iAddCount)) {
			this.lblRegistError.Text = "追加数が正しくありません";
			return false;
		}

		return true;
	}
}
