﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="QuestTrialList.aspx.cs" Inherits="Extension_QuestTrialList"
    Title="クエストクリア条件一覧" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="ViComm.Extension.Pwild" %>
<%@ Import Namespace="iBridCommLib" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="クエストクリア条件一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 400px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle2">
                                サイトコード
                            </td>
                            <td class="tdDataStyle" style="width: 200px">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton" ValidationGroup="Create"
                    OnClick="btnCreate_Click" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" Visible="false" />
                <asp:Button ID="Button1" runat="server" CausesValidation="False" CssClass="seekbutton"
                    OnClick="btnBack_Click" Text="戻る" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlQuestTrialInfo">
            <fieldset class="fieldset">
                <legend>[クエストクリア条件情報]</legend>
                <table border="0" style="width: 600px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            クリア条件
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstQuestTrialCategory" runat="server" DataSourceID="dsQuestTrialCategory"
                                OnDataBound="lst_DataBound" OnSelectedIndexChanged="lstQuestTrialCategory_IndexChanged"
                                DataTextField="TRIAL_CATEGORY_NM" DataValueField="TRIAL_CATEGORY_NO"
                                Width="170px" AutoPostBack="True">
                            </asp:DropDownList>
                            <asp:DropDownList ID="lstQuestTrialDetail" runat="server" DataSourceID="dsQuestTrialDetail" DataTextField="TRIAL_CATEGORY_DETAIL"
                                OnDataBound="lst_DataBound" DataValueField="TRIAL_CATEGORY_DETAIL_NO">
                            </asp:DropDownList>
                            <br />
                            <asp:Label ID="lblErrorMessageQuestTrial" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            カウント(X)
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtQuestTrialCount" runat="server" MaxLength="10" Width="50px"></asp:TextBox>
                            <br />
                            <asp:Label ID="lblErrorMessageCount" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            カウント単位
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtCountUnit" runat="server" MaxLength="240" Width="50px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            カウント開始タイミング
                        </td>
                        <td class="tdDataStyle">
                            <asp:RadioButtonList ID="rdoQuestCountStartType" runat="server">
								<asp:ListItem Value="0">累計</asp:ListItem>
								<asp:ListItem Value="1">ｴﾝﾄﾘｰ後</asp:ListItem>
							</asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="pnlCommand" runat="server">
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Update"
                        OnClick="btnUpdate_Click" />
                    <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" ValidationGroup="Delete"
                        OnClick="btnDelete_Click" />
                    <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" CausesValidation="False"
                        Visible="true" OnClick="btnCancel_Click" />
                <asp:Button ID="btnBack" runat="server" CausesValidation="False" CssClass="seekbutton"
                    OnClick="btnBack_Click" Text="戻る" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[クエストクリア条件一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdQuestTrial.PageIndex + 1 %>
                        of
                        <%= grdQuestTrial.PageCount%>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="600px">
                    <asp:GridView ID="grdQuestTrial" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        PageSize="30" EnableSortingAndPagingCallbacks="false" DataSourceID="dsQuestTrial"
                        SkinID="GridViewColor" AllowSorting="true">
                        <Columns>
                            <asp:TemplateField HeaderText="ｸﾘｱ条件">
                                <ItemTemplate>
									<asp:LinkButton ID="lnkQuestTrialId" runat="server" CommandArgument='<%# Eval("QUEST_TRIAL_SEQ") %>'
                                        Text='<%# string.Format("{0}:{1}",Eval("TRIAL_CATEGORY_NM"),Eval("TRIAL_CATEGORY_DETAIL")) %>' OnCommand="lnkQuestTrial_Command"></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="QUEST_TRIAL_COUNT" HeaderText="ｶｳﾝﾄ(X)" DataFormatString="{0:N0}">
                                <ItemStyle HorizontalAlign="right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="COUNT_UNIT" HeaderText="ｶｳﾝﾄ単位">
                                <ItemStyle HorizontalAlign="left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="ｶｳﾝﾄ開始ﾀｲﾐﾝｸﾞ">
                                <ItemTemplate>
                                    <asp:Label ID="lblQuestTrialNm" runat="server" Text='<%# iBridUtil.GetStringValue(Eval("QUEST_COUNT_START_TYPE")) == PwViCommConst.GameQuestCountStartType.QUEST_ENTRY ? "ｴﾝﾄﾘｰ後" : "累計" %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
            <br />
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsQuestTrial" runat="server" ConvertNullToDBNull="false"
        EnablePaging="True" OnSelected="dsQuestTrial_Selected" SelectCountMethod="GetPageCount"
        SelectMethod="GetPageCollection" SortParameterName="" TypeName="QuestTrial">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:QueryStringParameter Name="pQuestSeq" QueryStringField="questseq" Type="String" />
            <asp:QueryStringParameter Name="pLevelQuestSeq" QueryStringField="levelquestseq" Type="String" />
            <asp:QueryStringParameter Name="pLittleQuestSeq" QueryStringField="littlequestseq" Type="String" />
            <asp:QueryStringParameter Name="pOtherQuestSeq" QueryStringField="otherquestseq" Type="String" />
            <asp:QueryStringParameter Name="pQuestType" QueryStringField="questtype" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsQuestTrialCategory" runat="server" SelectMethod="GetList"
        TypeName="QuestTrialCategory">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsQuestTrialDetail" runat="server" SelectMethod="GetListByTrialCategoryNo"
        TypeName="QuestTrialDetail" OnSelecting="dsQuestTrialDetail_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:Parameter Name="pTrialCategoryNo" Type="String" DefaultValue="" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtQuestTrialCount">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="クエスト クリア条件情報を更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete"
        ConfirmText="クエスト クリア条件情報を削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>
