﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DefectReportContentViewer.aspx.cs" Inherits="Extension_Pwild_DefectReportContentViewer"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Vicomm Site Management</title>
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Cache-Control" content="no-cache">
	<style type="text/css">
		.test
		{
			display:-moz-inline-box;
			display:inline-block;
			padding-right:100px;
		}
	</style>	
</head>
<body>
	<form id="form1" runat="server">
		<div id="container">
			<div id="header">
				<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" 
					EnableScriptGlobalization="True" EnableScriptLocalization="True">
				</ajaxToolkit:ToolkitScriptManager>
			</div>
			<div id="content">
				<div id="contentright">
					<div id="contentborder" style="width:580px">
						[<a href="javascript:close()">閉じる</a>]
						<asp:Panel ID="pnlView" runat="server">
							<fieldset class="fieldset">
								<table class="tableStyle">
									<tr>
										<td class="tdHeaderStyle">
											ﾀｲﾄﾙ
										</td>
										<td class="tdDataStyle">
											<asp:Label ID="lblTitle" runat="server"></asp:Label>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle2">
											報告内容
										</td>
										<td class="tdDataStyle">
											<asp:Label ID="lblText" runat="server"></asp:Label>
										</td>
									</tr>
								</table>
							</fieldset>
						</asp:Panel>
					</div>
				</div>
			</div>
		</div>		
	</form>
</body>
</html>


