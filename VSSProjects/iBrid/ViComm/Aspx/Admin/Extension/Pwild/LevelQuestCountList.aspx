﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="LevelQuestCountList.aspx.cs" Inherits="Extension_Pwild_LevelQuestCountList" Title="クエストレベル分布レポート" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="クエストレベル分布レポート"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset>
                <legend>[設定]</legend>
                <table border="0" style="width: 640px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            サイトコード

                        </td>
                        <td class="tdDataStyle" colspan="3">
                            <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="180px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    CausesValidation="False" />
                <asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" CausesValidation="False" OnClick="btnCSV_Click" />
                <asp:Button ID="btnBack" runat="server" CausesValidation="False" CssClass="seekbutton"
                    OnClick="btnBack_Click" Text="戻る" />
            </fieldset>
        </asp:Panel>
        クエスト名:<asp:Label ID="lblQuestNm" runat="server" Text=''></asp:Label>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[クエストレベル分布レポート]</legend>
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="552px">
                    <asp:GridView ID="grdLevelQuestCount" runat="server" AutoGenerateColumns="False" DataSourceID="dsLevelQuestCount"
                        AllowSorting="true" SkinID="GridViewColor">
                        <Columns>
                            <asp:TemplateField HeaderText="レベル">
                                <ItemTemplate>
                                    <asp:Label ID="lblLevel" runat="server" Text='<%# Eval("QUEST_LEVEL") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="EXクエスト説明">
                                <ItemTemplate>
                                    <asp:Label ID="lblQuestExRemarks" runat="server" Text='<%# Eval("QUEST_EX_REMARKS") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="レベルクリア人数">
                                <ItemTemplate>
                                    <asp:Label ID="lblLevelCount" runat="server" Text='<%# Eval("QUEST_LEVEL_COUNT") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="EXエントリー数">
                                <ItemTemplate>
                                    <asp:Label ID="lblExEntryCount" runat="server" Text='<%# Eval("QUEST_EX_ENTRY_COUNT") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="EXクリア人数">
                                <ItemTemplate>
                                    <asp:Label ID="lblExClearCount" runat="server" Text='<%# Eval("QUEST_EX_CLEAR_COUNT") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="EXリタイヤ人数">
                                <ItemTemplate>
                                    <asp:Label ID="lblExRetireCount" runat="server" Text='<%# Eval("QUEST_EX_RETIRE_COUNT") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkLittleQuestCountList" runat="server" NavigateUrl='<%# string.Format("~/Extension/Pwild/LittleQuestCountList.aspx?sitecd={0}&questseq={1}&levelquestseq={2}&sexcd={3}", Eval("SITE_CD"), Eval("QUEST_SEQ"), Eval("LEVEL_QUEST_SEQ"), Eval("SEX_CD"))%>'
                                        Text="小ｸｴｽﾄ一覧"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle ForeColor="Black" BackColor="LightYellow" Font-Bold="true" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsLevelQuestCount" runat="server" SelectMethod="GetList" TypeName="LevelQuestCount">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSeekSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:QueryStringParameter Name="pQuestSeq" QueryStringField="questseq" Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
