﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MailResBonusRateList2.aspx.cs" Inherits="Extension_Pwild_MailResBonusRateList2" Title="メール返信ボーナスポイント確率設定２" %>
<%@ Import Namespace="ViComm" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="メール返信ボーナスポイント確率設定２"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlSearch">
			<fieldset class="fieldset">
				<legend>[検索]</legend>
				<asp:Panel ID="pnlSeekCondition" runat="server">
					<table border="0" style="width: 800px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle2">
								サイトコード
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px">
								</asp:DropDownList>
							</td>
						</tr>
					</table>
				</asp:Panel>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Seek" />
				<asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton"  OnClick="btnCreate_Click" ValidationGroup="Create" />
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlInput">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Label ID="lblErrorMessage" runat="server" Text="" ForeColor="red"></asp:Label>
				<table border="0" style="width: 650px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							付与ポイント
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtAddPoint" runat="server" Width="50px" MaxLength="3"></asp:TextBox>pt
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							確率
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtRate" runat="server" Width="50px" MaxLength="5"></asp:TextBox>
						</td>
					</tr>
				</table>
				<asp:Panel ID="pnlCommand" runat="server" Width="660px">
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" />
					<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" />
					<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="delBtnStyle" OnClick="btnDelete_Click" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlList">
			<fieldset>
				<legend>[一覧]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">
						Record Count <%# GetRecCount() %>
					</a>
					<br />
					<a class="reccount">
						Current viewing page <%= grdMailResBonusRate.PageIndex + 1%> of <%= grdMailResBonusRate.PageCount%>
					</a>
				</asp:Panel>
				&nbsp;
				<asp:Panel ID="pnlGrid" runat="server">
					<asp:GridView ID="grdMailResBonusRate" DataSourceID="dsMailResBonusRate" runat="server" AllowPaging="True" AutoGenerateColumns="False" PageSize="50" SkinID="GridViewColor">
						<Columns>
							<asp:TemplateField HeaderText="SEQ">
								<ItemTemplate>
									<asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%# Container.DataItemIndex %>' OnCommand="lnkEdit_Command" Text='<%# Eval("MAIL_RES_BONUS_RATE_SEQ") %>'></asp:LinkButton>
									<asp:HiddenField ID="hdnMailResBonusRateSeq" runat="server" Value='<%# Eval("MAIL_RES_BONUS_RATE_SEQ") %>' />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="付与pt">
								<ItemTemplate>
									<asp:Label ID="lblAddPoint" runat="server" Text='<%# Eval("ADD_POINT") %>'></asp:Label>pt
								<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="確率">
								<ItemTemplate>
									<asp:Label ID="lblRate" runat="server" Text='<%# Eval("RATE") %>'></asp:Label>
									<ItemStyle HorizontalAlign="Right" />
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
			<br />
		</asp:Panel>
	</div>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtAddPoint" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtRate" />
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsMailResBonusRate" runat="server" ConvertNullToDBNull="false" EnablePaging="True" OnSelecting="dsMailResBonusRate_Selecting" OnSelected="dsMailResBonusRate_Selected"
		SelectCountMethod="GetPageCount" SelectMethod="GetPageCollection" SortParameterName="" TypeName="MailResBonusRate2">
		<SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>

