﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 不具合報告一覧
--	Progaram ID		: DefectReportList
--
--  Creation Date	: 2015.06.24
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using MobileLib;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class Extension_Pwild_DefectReportList:System.Web.UI.Page {
	private string recCount = "";

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string DefectReportSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["DefectReportSeq"]);
		}
		set {
			this.ViewState["DefectReportSeq"] = value;
		}
	}

	private string HandleNm {
		get {
			return iBridUtil.GetStringValue(this.ViewState["HandleNm"]);
		}
		set {
			this.ViewState["HandleNm"] = value;
		}
	}

	private string LoginId {
		get {
			return iBridUtil.GetStringValue(this.ViewState["LoginId"]);
		}
		set {
			this.ViewState["LoginId"] = value;
		}
	}

	private string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		set {
			this.ViewState["SexCd"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	private string SortExpression {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortExpression"]);
		}
		set {
			this.ViewState["SortExpression"] = value;
		}
	}

	private string SortDirect {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortDirect"]);
		}
		set {
			this.ViewState["SortDirect"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!IsPostBack) {
			this.SexCd = iBridUtil.GetStringValue(Request.QueryString["sexcd"]);
			
			if (string.IsNullOrEmpty(this.SexCd)) {
				this.SexCd = ViCommConst.MAN;
			}

			if (this.SexCd.Equals(ViCommConst.MAN)) {
				this.lblPgmTitle.Text = "会員" + lblPgmTitle.Text;
				this.Title = "会員" + this.Title;
			} else {
				this.lblPgmTitle.Text = "出演者" + lblPgmTitle.Text;
				this.Title = "出演者" + this.Title;
			}

			this.recCount = "0";
			this.SortDirect = string.Empty;
			this.SortExpression = string.Empty;

			this.lstSeekSiteCd.DataBind();
			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
				this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}

			this.lstSeekCategorySeq.DataBind();
			this.lstSeekCategorySeq.Items.Insert(0,new ListItem(string.Empty,string.Empty));

			this.lstSeekProgressSeq.DataBind();
			this.lstSeekProgressSeq.Items.Insert(0,new ListItem(string.Empty,string.Empty));

			this.chkSeekApply.Checked = true;
			this.chkSeekAccept.Checked = true;
			this.chkSeekPublic.Checked = true;

			this.grdDefectReport.PageIndex = 0;
			this.grdDefectReport.PageSize = 100;

			this.GetList();
			this.pnlMainte.Visible = false;
		}
	}

	private void GetList() {
		this.grdDefectReport.DataSourceID = "dsDefectReport";
		this.grdDefectReport.DataBind();
		this.pnlCount.DataBind();
	}

	protected string GetRecCount() {
		return recCount;
	}
	
	private bool CheckData() {
		lblErrorMessageOverlapDefectReportSeq.Visible = false;
		lblErrorMessageRequestCategorySeq.Visible = false;
		
		if (lstPublicStatus.SelectedValue.Equals(PwViCommConst.DefectReportPublicStatus.OVERLAP)) {
			if (string.IsNullOrEmpty(txtOverlapDefectReportSeq.Text)) {
				lblErrorMessageOverlapDefectReportSeq.Text = "不具合報告重複SEQを入力してください";
				lblErrorMessageOverlapDefectReportSeq.Visible = true;
				return false;
			}
		} else if (lstPublicStatus.SelectedValue.Equals(PwViCommConst.DefectReportPublicStatus.REMOVE)) {
			if (string.IsNullOrEmpty(lstRequestCategorySeq.SelectedValue)) {
				lblErrorMessageRequestCategorySeq.Text = "移動カテゴリSEQを入力してください";
				lblErrorMessageRequestCategorySeq.Visible = true;
				return false;
			}
		}
		
		return true;
	}

	private bool UpdateData() {
		if (!this.IsValid) {
			return false;
		}
		
		if (!this.CheckData()) {
			return false;
		}
		
		string sResult;
		bool bOK;

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("DEFECT_REPORT_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pDEFECT_REPORT_SEQ",DbSession.DbType.VARCHAR2,this.DefectReportSeq);
			oDbSession.ProcedureInParm("pTITLE",DbSession.DbType.VARCHAR2,this.txtTitle.Text);
			oDbSession.ProcedureInParm("pTEXT",DbSession.DbType.VARCHAR2,this.txtText.Text);
			oDbSession.ProcedureInParm("pDEFECT_REPORT_CATEGORY_SEQ",DbSession.DbType.VARCHAR2,this.lstCategorySeq.SelectedValue);
			oDbSession.ProcedureInParm("pADMIN_COMMENT",DbSession.DbType.VARCHAR2,this.txtAdminComment.Text);
			oDbSession.ProcedureInParm("pDEFECT_REPORT_PROGRESS_SEQ",DbSession.DbType.VARCHAR2,this.lstProgressSeq.SelectedValue);
			oDbSession.ProcedureInParm("pPUBLIC_STATUS",DbSession.DbType.NUMBER,this.lstPublicStatus.SelectedValue);
			oDbSession.ProcedureInParm("pOVERLAP_DEFECT_REPORT_SEQ",DbSession.DbType.VARCHAR2,this.txtOverlapDefectReportSeq.Text);
			oDbSession.ProcedureInParm("pREMOVE_REQUEST_CATEGORY_SEQ",DbSession.DbType.VARCHAR2,lstRequestCategorySeq.SelectedValue);
			oDbSession.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,this.SexCd);
			oDbSession.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			oDbSession.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();

			sResult = oDbSession.GetStringValue("pRESULT");
		}

		switch (sResult) {
			case PwViCommConst.DefectReportMainteResult.RESULT_OK:
				bOK = true;
				break;
			case PwViCommConst.DefectReportMainteResult.RESULT_NO_OVERLAP_ORIGINAL_SEQ:
				lblErrorMessageOverlapDefectReportSeq.Text = "不具合報告重複SEQが不正です";
				lblErrorMessageOverlapDefectReportSeq.Visible = true;
				bOK = false;
				break;
			default:
				bOK = false;
				break;
		}
		
		return bOK;
	}

	private void ClearField() {
		this.SiteCd = string.Empty;
		this.DefectReportSeq = string.Empty;

		this.lnkLoginId.Text = string.Empty;
		this.lnkLoginId.NavigateUrl = string.Empty;
		this.lblHandleNm.Text = string.Empty;
		this.txtTitle.Text = string.Empty;
		this.lstCategorySeq.SelectedIndex = 0;
		this.lstProgressSeq.SelectedIndex = 0;
		this.txtText.Text = string.Empty;
		this.lblText.Text = string.Empty;
		this.txtAdminComment.Text = string.Empty;
		this.lblAdminComment.Text = string.Empty;
		this.lblCreateDate.Text = string.Empty;
		this.lblPublicDate.Text = string.Empty;
		this.lstPublicStatus.SelectedIndex = 0;
		this.lstRequestCategorySeq.SelectedIndex = 0;
		
		this.txtOverlapDefectReportSeq.Text = string.Empty;
		this.txtGroupOverlapSeq.Text = string.Empty;
		
		this.lblErrorMessageOverlapDefectReportSeq.Visible = false;
		this.lblErrorMessageGroupOverlapSeq.Visible = false;
		this.lblErrorMessageRequestCategorySeq.Visible = false;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		this.SortDirect = string.Empty;
		this.SortExpression = string.Empty;
		this.GetList();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.UpdateData()) {
			return;
		}
		this.GetList();
		this.pnlMainte.Visible = false;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlMainte.Visible = false;
		this.ClearField();
	}

	protected void lnkMainte_Command(object sender,CommandEventArgs e) {
		if (!e.CommandName.Equals("MAINTE")) {
			return;
		}

		this.lstCategorySeq.DataBind();
		this.lstProgressSeq.DataBind();
		this.lstRequestCategorySeq.DataBind();
		this.lstRequestCategorySeq.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		this.ClearField();

		int iRowIndex = int.Parse(e.CommandArgument.ToString());
		IDictionary oDataKeys = this.grdDefectReport.DataKeys[iRowIndex].Values;
		this.SiteCd = iBridUtil.GetStringValue(oDataKeys["SITE_CD"]);
		this.DefectReportSeq = iBridUtil.GetStringValue(oDataKeys["DEFECT_REPORT_SEQ"]);
		this.HandleNm = iBridUtil.GetStringValue(oDataKeys["HANDLE_NM"]);
		this.LoginId = iBridUtil.GetStringValue(oDataKeys["LOGIN_ID"]);
		
		GetData();

		this.pnlMainte.Visible = true;
		
	}

	private void GetData() {

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("DEFECT_REPORT_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pDEFECT_REPORT_SEQ",DbSession.DbType.VARCHAR2,this.DefectReportSeq);
			oDbSession.ProcedureOutParm("pTITLE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTEXT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pDEFECT_REPORT_CATEGORY_SEQ",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pADMIN_COMMENT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pDEFECT_REPORT_PROGRESS_SEQ",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPUBLIC_STATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pOVERLAP_DEFECT_REPORT_SEQ",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pCREATE_DATE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPUBLIC_DATE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.lblDefectReportSeq.Text = this.DefectReportSeq;
			this.lblHandleNm.Text = this.HandleNm;
			this.lnkLoginId.Text = this.LoginId;
			this.lnkLoginId.NavigateUrl = this.GetProfUrl(this.SiteCd,this.SexCd,this.LoginId);
			this.txtTitle.Text = oDbSession.GetStringValue("pTITLE");
			this.lblTitle.Text = oDbSession.GetStringValue("pTITLE");
			this.txtText.Text = oDbSession.GetStringValue("pTEXT");
			this.lblText.Text = oDbSession.GetStringValue("pTEXT").Replace("\n","<br />");
			this.lstCategorySeq.SelectedValue = oDbSession.GetStringValue("pDEFECT_REPORT_CATEGORY_SEQ");
			this.txtAdminComment.Text = oDbSession.GetStringValue("pADMIN_COMMENT");
			this.lblAdminComment.Text = oDbSession.GetStringValue("pADMIN_COMMENT").Replace("\n","<br />");
			this.lstProgressSeq.SelectedValue = oDbSession.GetStringValue("pDEFECT_REPORT_PROGRESS_SEQ");
			this.lstPublicStatus.SelectedValue = oDbSession.GetStringValue("pPUBLIC_STATUS");
			this.txtOverlapDefectReportSeq.Text = oDbSession.GetStringValue("pOVERLAP_DEFECT_REPORT_SEQ");
			this.lblCreateDate.Text = oDbSession.GetStringValue("pCREATE_DATE");
			this.lblPublicDate.Text = oDbSession.GetStringValue("pPUBLIC_DATE");
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
		}
	}

	protected void dsDefectReport_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsDefectReport_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		DefectReport.SearchCondition oSearchCondition = new DefectReport.SearchCondition();
		oSearchCondition.SiteCd = this.lstSeekSiteCd.SelectedValue;
		oSearchCondition.SexCd = this.SexCd;
		oSearchCondition.DefectReportCategorySeq = this.lstSeekCategorySeq.SelectedValue;
		oSearchCondition.DefectReportProgressSeq = this.lstSeekProgressSeq.SelectedValue;

		List<string> oPublicStatus = new List<string>();

		if (this.chkSeekApply.Checked) {
			oPublicStatus.Add("1");
		}
		if (this.chkSeekAccept.Checked) {
			oPublicStatus.Add("2");
		}
		if (this.chkSeekPublic.Checked) {
			oPublicStatus.Add("3");
		}
		if (this.chkSeekPrivate.Checked) {
			oPublicStatus.Add("4");
		}
		if (this.chkSeekOverlap.Checked) {
			oPublicStatus.Add("5");
		}
		if (this.chkSeekRemove.Checked) {
			oPublicStatus.Add("6");
		}

		oSearchCondition.PublicStatus = oPublicStatus;
		oSearchCondition.LoginId = this.txtSeekLoginId.Text.Trim();
		oSearchCondition.Keyword = this.txtSeekKeyword.Text.Trim();
		oSearchCondition.SortDirection = this.SortDirect;
		oSearchCondition.SortExpression = this.SortExpression;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void grdDefectReport_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			string sPublicStatus = DataBinder.Eval(e.Row.DataItem,"PUBLIC_STATUS").ToString();

			if (sPublicStatus.Equals(PwViCommConst.DefectReportPublicStatus.APPLY)) {
				e.Row.BackColor = Color.LavenderBlush;
			} else if (sPublicStatus.Equals(PwViCommConst.DefectReportPublicStatus.PRIVATE) ||
						sPublicStatus.Equals(PwViCommConst.DefectReportPublicStatus.OVERLAP) ||
						sPublicStatus.Equals(PwViCommConst.DefectReportPublicStatus.REMOVE)
			) {
				e.Row.BackColor = Color.Gainsboro;
			} else {
				e.Row.BackColor = Color.White;
			}
		}
	}

	protected void grdDefectReport_Sorting(object sender,GridViewSortEventArgs e) {
		e.Cancel = true;

		if (this.SortExpression.Equals(e.SortExpression)) {
			if (this.SortDirect.Equals("ASC")) {
				this.SortDirect = "DESC";
			} else if (this.SortDirect.Equals("DESC")) {
				this.SortDirect = "ASC";
			}
		} else {
			this.SortDirect = "ASC";
		}

		this.SortExpression = e.SortExpression;

		this.GetList();
	}

	protected string GetConcatStr(object pText,int pLength) {
		string sText = iBridUtil.GetStringValue(pText);

		if (sText.Length > pLength) {
			return string.Concat(SysPrograms.Substring(sText,(pLength - 1)),"…");
		} else {
			return sText;
		}
	}

	protected string GetProfUrl(object pSiteCd,object pSexCd,object pLoginId) {
		string sValue = string.Empty;

		if (iBridUtil.GetStringValue(pSexCd).Equals(ViCommConst.MAN)) {
			sValue = string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",iBridUtil.GetStringValue(pSiteCd),iBridUtil.GetStringValue(pLoginId));
		} else {
			sValue = string.Format("~/Cast/CastView.aspx?loginid={0}",iBridUtil.GetStringValue(pLoginId));
		}

		return sValue;
	}

	protected void lnkPictographSample_Click(object sender,EventArgs e) {
		string sURL = string.Concat(Session["Root"],"/Site/PictographSample.aspx");
		string sScripts = string.Format("<script language=\"javascript\">window.open('{0}','','width=1060,height=900,resizable=yes,directories=no,scrollbars=yes' , false);</script>",sURL);
		ClientScript.RegisterStartupScript(Page.GetType(),"OpenNewWindow",sScripts);
	}
	
	protected string GetPublicStatusNm(object pPublicStatus) {
		string sValue = string.Empty;
		
		switch (iBridUtil.GetStringValue(pPublicStatus)) {
			case PwViCommConst.DefectReportPublicStatus.APPLY:
				sValue = "申請中";
				break;
			case PwViCommConst.DefectReportPublicStatus.ACCEPT:
				sValue = "採用";
				break;
			case PwViCommConst.DefectReportPublicStatus.PUBLIC:
				sValue = "公開";
				break;
			case PwViCommConst.DefectReportPublicStatus.PRIVATE:
				sValue = "非公開";
				break;
			case PwViCommConst.DefectReportPublicStatus.OVERLAP:
				sValue = "重複";
				break;
			case PwViCommConst.DefectReportPublicStatus.REMOVE:
				sValue = "移動";
				break;
		}
		
		return sValue;
	}

	protected string GetClientScript(object pSiteCd,object pDefectReportSeq) {
		string sRoot = Request.Url.Authority + ConfigurationManager.AppSettings["Root"];
		return string.Format("javascript:win=window.open('http://{2}/Extension/Pwild/DefectReportContentViewer.aspx?site={0}&reportseq={1}','DefectReportContentViewer','width=1000,height=460,resizable=yes,directories=no,scrollbars=yes' , false);win.focus();return false;",pSiteCd,pDefectReportSeq,sRoot);
	}

	protected void btnGroupOverlap_Click(object sender,EventArgs e) {
		lblErrorMessageGroupOverlapSeq.Visible = false;
		List<string> oDefectReportSeqList = new List<string>();

		string sOverlapDefectReportSeq = this.txtGroupOverlapSeq.Text;

		for (int i = 0;i < this.grdDefectReport.Rows.Count;i++) {
			GridViewRow row = this.grdDefectReport.Rows[i];
			CheckBox oChk = (CheckBox)row.FindControl("chkGroupOverlap");

			if (oChk.Checked) {
				HiddenField hdnDefectReportSeq = row.FindControl("hdnDefectReportSeq") as HiddenField;
				oDefectReportSeqList.Add(hdnDefectReportSeq.Value);
			}
		}

		if (oDefectReportSeqList.Count > 0) {
			string sResult;
			using (DbSession oDbSession = new DbSession()) {
				oDbSession.PrepareProcedure("UPDATE_DEFECT_REPORT_OVERLAP");
				oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.lstSeekSiteCd.SelectedValue);
				oDbSession.ProcedureInArrayParm("pDEFECT_REPORT_SEQ",DbSession.DbType.VARCHAR2,oDefectReportSeqList.ToArray());
				oDbSession.ProcedureInParm("pOVERLAP_DEFECT_REPORT_SEQ",DbSession.DbType.VARCHAR2,sOverlapDefectReportSeq);
				oDbSession.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,this.SexCd);
				oDbSession.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
				oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
				oDbSession.ExecuteProcedure();
				
				sResult = oDbSession.GetStringValue("pRESULT");
			}
			
			if (sResult.Equals(PwViCommConst.DefectReportMainteResult.RESULT_NO_OVERLAP_ORIGINAL_SEQ)) {
				lblErrorMessageGroupOverlapSeq.Text = "不具合報告重複SEQが不正です";
				lblErrorMessageGroupOverlapSeq.Visible = true;
				return;
			}

			lstSeekSiteCd.DataSourceID = "";
			GetList();
		}
	}
}
