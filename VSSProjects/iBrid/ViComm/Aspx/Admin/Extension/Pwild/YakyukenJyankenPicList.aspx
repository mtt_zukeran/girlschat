﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="YakyukenJyankenPicList.aspx.cs" Inherits="Extension_Pwild_YakyukenJyankenPicList" Title="Untitled Page" %>
<%@ Import Namespace="ViComm" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="野球拳ジャンケン画像"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlKey">
			<fieldset class="fieldset">
				<legend>[検索]</legend>
				<asp:Panel ID="pnlSeekCondition" runat="server">
					<table border="0" style="width: 800px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle">
								サイトコード
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								ログインID
							</td>
							<td class="tdDataStyle">
                                <asp:TextBox ID="txtLoginId" runat="server" Width="100px"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle2">
								状態
							</td>
							<td class="tdDataStyle">
								<asp:CheckBox ID="chkAuth" runat="server" Text="公開中" />
								<asp:CheckBox ID="chkUnAuth" runat="server" Text="認証待ち" />
							</td>
						</tr>
					</table>
				</asp:Panel>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Seek" />
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[一覧]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count <%# GetRecCount() %></a>
					<br />
					<a class="reccount">Current viewing page <%= grdYakyukenJyankenPic.PageIndex + 1 %> of <%= grdYakyukenJyankenPic.PageCount %></a>
				</asp:Panel>
				&nbsp;
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="443px">
					<asp:GridView ID="grdYakyukenJyankenPic" runat="server" AllowPaging="True" AutoGenerateColumns="False" PageSize="30"
						DataSourceID="dsYakyukenJyankenPic" SkinID="GridViewColor" AllowSorting="true" OnRowDataBound="grdYakyukenJyankenPic_RowDataBound">
						<Columns>
							<asp:TemplateField HeaderText="ﾊﾝﾄﾞﾙ名<br>会員ID" SortExpression="LOGIN_ID">
								<ItemTemplate>
									<asp:HyperLink ID="lblHandleNm" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}",Eval("LOGIN_ID")) %>'
										Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:HyperLink><br />
									<asp:Label ID="lblLoginId" runat="server" Text='<%# Eval("LOGIN_ID") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle Width="120px" HorizontalAlign="left" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="画像">
								<ItemTemplate>
									<asp:ImageButton ID="ImageButton1" runat="server" ImageUrl='<%# Eval("SMALL_PHOTO_IMG_PATH", "../../{0}") %>' OnClientClick='<%# GetClientScript(Eval("PHOTO_IMG_PATH"))  %>' />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="属性">
								<ItemTemplate>
									<asp:Label ID="lblJyankenType" runat="server" Text='<%# GetJyankenTypeNm(Eval("JYANKEN_TYPE")) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="center" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="投稿日時" SortExpression="CREATE_DATE">
								<ItemTemplate>
									<asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("CREATE_DATE", "{0:yy/MM/dd HH:mm:ss}") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="center" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="状態">
								<ItemTemplate>
									<asp:Label ID="lblAuthFlag" runat="server" Text='<%# Eval("AUTH_FLAG").ToString().Equals("1") ? "公開中" : "認証待ち" %>'></asp:Label>
									<br />
									<asp:LinkButton ID="lnkPublish" runat="server" CommandArgument='<%#string.Format("{0},{1},{2},{3},{4}",Eval("SITE_CD"),Eval("CAST_USER_SEQ"),Eval("CAST_CHAR_NO"),Eval("JYANKEN_TYPE"),Eval("REVISION_NO")) %>'
										Text='[公開]' OnCommand="lnkAuth_OnCommand" Visible='<%# Eval("AUTH_FLAG").ToString().Equals("1") ? false : true %>'></asp:LinkButton>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" />
							</asp:TemplateField>
 							<asp:TemplateField HeaderText="削除">
								<ItemTemplate>
									<asp:LinkButton ID="lnkDelete" runat="server" CommandArgument='<%#string.Format("{0},{1},{2},{3},{4}",Eval("SITE_CD"),Eval("CAST_USER_SEQ"),Eval("CAST_CHAR_NO"),Eval("JYANKEN_TYPE"),Eval("REVISION_NO")) %>'
										Text='[削除]' OnCommand="lnkDelete_OnCommand" Visible="true" OnClientClick="return confirm('削除します。よろしいですか？');" ></asp:LinkButton>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" />
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
			<br />
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsYakyukenJyankenPic" runat="server" SelectMethod="GetPageCollection" ConvertNullToDBNull="true" SortParameterName=""
		TypeName="YakyukenJyankenPic" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelected="dsYakyukenJyankenPic_Selected"
		OnSelecting="dsYakyukenJyankenPic_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>

