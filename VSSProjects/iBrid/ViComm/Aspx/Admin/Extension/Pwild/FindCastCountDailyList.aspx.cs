﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 女性検索回数集計一覧

--	Progaram ID		: FindCastCountDailyList
--
--  Creation Date	: 2016.10.14
--  Creater			: M&TT Zukeran
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Extension_Pwild_FindCastCountDailyList:System.Web.UI.Page {
	/// <summary>報告年月の開始年</summary>
	private const int LIST_START_YEAR = 2016;

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
			GetList();
		}
	}

	/// <summary>
	/// 初期化処理
	/// </summary>
	private void InitPage() {
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		// 表示されてる検索結果をクリアする
		grdFindCastCountDaily.DataSourceID = "";
		ClearField();
		DataBind();

		if (!IsPostBack) {
			// 報告年月リストの選択肢を設定する
			SysPrograms.SetYearList(lstYYYY,LIST_START_YEAR,0,false);
			SysPrograms.SetMonthList(lstMM);

			// 初期選択の設定
			lstYYYY.SelectedIndex = 0;
			lstMM.SelectedValue = DateTime.Now.ToString("MM");
			lstSiteCd.DataSourceID = "";
		}
	}

	private void ClearField() {
	}

	/// <summary>
	/// 検索ボタンの押下処理
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	/// <summary>
	/// クリアボタンの押下処理
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnClear_Click(object sender,EventArgs e) {
		Response.Redirect(Request.Url.PathAndQuery);
	}

	/// <summary>
	/// CSV出力ボタンの押下処理
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnCSV_Click(object sender,EventArgs e) {
		DataSet oDataSet;
		using (FindCastCountDaily oFindCastCountDaily = new FindCastCountDaily()) {
			oDataSet = oFindCastCountDaily.GetListCsv(this.lstSiteCd.SelectedValue,lstYYYY.SelectedValue,lstMM.SelectedValue);
		}

		//ヘッダ行
		string sHeader = "報告日,検索総数,検索人数(ﾕﾆｰｸ),全ての女性,ﾛｸﾞｲﾝ中の子のみ,待機中の子のみ,年齢(指定なし),地域(指定なし),地域(ﾃﾞﾌｫ),地域(他地域),ｴｯﾁ度(指定なし),SM度(指定なし),ｽﾀｲﾙ(指定なし),ﾀｲﾌﾟ(指定なし),職業(指定なし),出没時間(指定なし),TEL待ち(指定なし),顔だし(指定なし),名前(入力なし),ｱﾀﾞﾙﾄNG(問わない),ｱﾀﾞﾙﾄNG(しない),身長(指定なし),ﾊﾞｽﾄ(指定なし),血液型(指定なし),ﾌﾘｰﾜｰﾄﾞ(入力なし)";

		//CSV出力 
		Response.ContentType = "application/download";
		Response.AppendHeader("Content-Disposition","attachment;filename=FindCastCountDailyList.csv");
		Response.ContentEncoding = System.Text.Encoding.GetEncoding("Shift_JIS");

		Response.Write(sHeader + "\r\n");
		if (oDataSet.Tables[0].Rows.Count > 0) {
			foreach (DataRow oCsvRow in oDataSet.Tables[0].Rows) {
				string sData =
					oCsvRow["REPORT_DATE"].ToString() + "," +
					oCsvRow["TOTAL_COUNT"].ToString() + "," +
					oCsvRow["UNIQUE_USER_COUNT"].ToString() + "," +
					oCsvRow["STATUS_ALL_COUNT"].ToString() + "," +
					oCsvRow["STATUS_LOGIN_COUNT"].ToString() + "," +
					oCsvRow["STATUS_WAIT_COUNT"].ToString() + "," +
					oCsvRow["AGE_ALL_COUNT"].ToString() + "," +
					oCsvRow["AREA_ALL_COUNT"].ToString() + "," +
					oCsvRow["AREA_DEFAULT_COUNT"].ToString() + "," +
					oCsvRow["AREA_OTHER_COUNT"].ToString() + "," +
					oCsvRow["H_ALL_COUNT"].ToString() + "," +
					oCsvRow["SM_ALL_COUNT"].ToString() + "," +
					oCsvRow["STYLE_ALL_COUNT"].ToString() + "," +
					oCsvRow["LIKE_TYPE_ALL_COUNT"].ToString() + "," +
					oCsvRow["JOB_ALL_COUNT"].ToString() + "," +
					oCsvRow["TIMEZONE_ALL_COUNT"].ToString() + "," +
					oCsvRow["WAIT_TEL_ALL_COUNT"].ToString() + "," +
					oCsvRow["FACE_DISP_ALL_COUNT"].ToString() + "," +
					oCsvRow["HANDLE_NM_ALL_COUNT"].ToString() + "," +
					oCsvRow["ADULT_ALL_COUNT"].ToString() + "," +
					oCsvRow["ADULT_NG_COUNT"].ToString() + "," +
					oCsvRow["HEIGHT_ALL_COUNT"].ToString() + "," +
					oCsvRow["BUST_ALL_COUNT"].ToString() + "," +
					oCsvRow["BLOOD_TYPE_ALL_COUNT"].ToString() + "," +
					oCsvRow["COMMENT_ALL_COUNT"].ToString();

				Response.Write(sData + "\r\n");
			}
		}
		Response.End();
	}

	protected void grdFindCastCountDaily_RowDataBound(object sender,GridViewRowEventArgs e) {
		// データ行以外の場合
		if (e.Row.RowType != DataControlRowType.DataRow) {
			return;
		}
		// ヘッダー行が存在しない場合（データが存在しない場合）
		if (grdFindCastCountDaily.HeaderRow == null) {
			return;
		}

		GridViewRow oDataRow1 = new GridViewRow(-1,-1,DataControlRowType.DataRow,DataControlRowState.Normal);
		GridViewRow oDataRow2 = new GridViewRow(-1,-1,DataControlRowType.DataRow,DataControlRowState.Normal);
		GridViewRow oDataRowArea = new GridViewRow(-1,-1,DataControlRowType.DataRow,DataControlRowState.Normal);
		GridViewRow oDataRowAdult = new GridViewRow(-1,-1,DataControlRowType.DataRow,DataControlRowState.Normal);

		// 偶数行の背景色を変更
		int rowIndex = e.Row.RowIndex;
		if ((e.Row.RowIndex&1) == 1) {
			oDataRow1.CssClass = "AlternatingRowStyleColor";
			oDataRow2.CssClass = "AlternatingRowStyleColor";
		}

		// 1行目と2行目に表示するデータを分ける
		// (切り出していくため、後ろから処理する)
		for (int i = e.Row.Cells.Count - 1;i >= 0;i--) {
			if (i == 5 || i == 15) {
			} else if (i < 20) {
				oDataRow1.Cells.AddAt(0,e.Row.Cells[i]);
			} else if (i > 25) {
				// アダルトNG
				oDataRowAdult.Cells.AddAt(0,e.Row.Cells[i]);
			} else if (i > 22) {
				// 地域
				oDataRowArea.Cells.AddAt(0,e.Row.Cells[i]);
			} else {
				oDataRow2.Cells.AddAt(0,e.Row.Cells[i]);
			}
		}

		// アダルトNGを1行目のデータに追加
		oDataRow1.Cells.AddAt(14,oDataRowAdult.Cells[1]);
		oDataRow1.Cells.AddAt(14,oDataRowAdult.Cells[0]);

		// 地域を1行目のデータに追加
		oDataRow1.Cells.AddAt(5,oDataRowArea.Cells[2]);
		oDataRow1.Cells.AddAt(5,oDataRowArea.Cells[1]);
		oDataRow1.Cells.AddAt(5,oDataRowArea.Cells[0]);

		// 1行目に表示するデータの縦、横の(結合)セル数を設定
		for (int i = 0;i < oDataRow1.Cells.Count;i++) {
			if (i == 2) {
				oDataRow1.Cells[i].ColumnSpan = 3;
			} else {
				oDataRow1.Cells[i].RowSpan = 2;
			}
		}

		// 元のデータ行を削除＆データ行を挿入
		if (grdFindCastCountDaily.Controls.Count > 0) {
			grdFindCastCountDaily.Controls[0].Controls.Remove(e.Row);
			grdFindCastCountDaily.Controls[0].Controls.Add(oDataRow1);
			grdFindCastCountDaily.Controls[0].Controls.Add(oDataRow2);
		}
	}

	/// <summary>
	/// 検索処理
	/// </summary>
	private void GetList() {
		grdFindCastCountDaily.PageIndex = 0;
		grdFindCastCountDaily.DataSourceID = "dsFindCastCountDaily";
		DataBind();
		CustomHeader();
	}

	/// <summary>
	/// ヘッダー行のカスタマイズ
	/// </summary>
	private void CustomHeader() {
		// ヘッダー行が存在しない場合（データが存在しない場合）
		if (grdFindCastCountDaily.HeaderRow == null) {
			return;
		}

		GridViewRow oHeaderRow1 = new GridViewRow(-1,-1,DataControlRowType.Header,DataControlRowState.Normal);
		GridViewRow oHeaderRow2 = new GridViewRow(-1,-1,DataControlRowType.Header,DataControlRowState.Normal);

		// 1行目と2行目に表示するヘッダーを分ける
		// (切り出していくため、後ろから処理する)
		for (int i = grdFindCastCountDaily.HeaderRow.Cells.Count-1;i >= 0;i--) {
			// 項目名を改行する
			if (grdFindCastCountDaily.HeaderRow.Cells[i].Text.Equals("検索人数(ﾕﾆｰｸ)")) {
				grdFindCastCountDaily.HeaderRow.Cells[i].Text = "検索人数<br/>(ﾕﾆｰｸ)";
			}
			if (i < 20) {
				oHeaderRow1.Cells.AddAt(0,grdFindCastCountDaily.HeaderRow.Cells[i]);
			} else {
				oHeaderRow2.Cells.AddAt(0,grdFindCastCountDaily.HeaderRow.Cells[i]);
			}
		}

		// 1行目に表示するヘッダーの縦、横の(結合)セル数を設定
		for (int i = 0;i < oHeaderRow1.Cells.Count;i++) {
			if (i == 2) {
				oHeaderRow1.Cells[i].ColumnSpan = 3;
			} else if (i == 5) {
				oHeaderRow1.Cells[i].ColumnSpan = 3;
			} else if (i == 15) {
				oHeaderRow1.Cells[i].ColumnSpan = 2;
			} else {
				oHeaderRow1.Cells[i].RowSpan = 2;
			}
		}

		// 元のヘッダー行を削除＆ヘッダー行を挿入
		if (grdFindCastCountDaily.Controls.Count > 0) {
			grdFindCastCountDaily.Controls[0].Controls.Remove(grdFindCastCountDaily.HeaderRow);
			grdFindCastCountDaily.Controls[0].Controls.AddAt(0,oHeaderRow1);
			grdFindCastCountDaily.Controls[0].Controls.AddAt(1,oHeaderRow2);
		}
	}

	protected void dsFindCastCountDaily_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = lstYYYY.SelectedValue;
		e.InputParameters[2] = lstMM.SelectedValue;
	}

	/// <summary>
	/// 曜日セルの背景色を取得する
	/// </summary>
	/// <param name="pDayOfWeek"></param>
	/// <returns></returns>
	protected Color GetBackColor(object pDayOfWeek) {
		if (pDayOfWeek.ToString().Equals("Sat")) {
			return Color.FromArgb(0xCCCCFF);
		} else {
			if (pDayOfWeek.ToString().Equals("Sun")) {
				return Color.FromArgb(0xFFCCCC);
			} else {
				return Color.Empty;
			}
		}
	}
}