﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 女性お宝ｺﾝﾌﾟ人数レポート

--	Progaram ID		: CastTreasureCompleteTotalReport
--
--  Creation Date	: 2012.12.03
--  Creater			: M&TT A.Taba
--
**************************************************************************/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using iBridCommLib;
using System.Drawing;
using ViComm;
using ViComm.Extension.Pwild;

public partial class Extension_Pwild_CastTreasureCompleteTotalReport:System.Web.UI.Page {
	
	protected void Page_Load(object sender,EventArgs e) {
		if (!this.IsPostBack) {

			this.InitPage();

			this.GetList();
			this.pnlInfo.Visible = true;
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		this.GetList();
		this.pnlInfo.Visible = true;
	}

	protected void dsCastTreasureCompleteTotalReport_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pReportDayFrom"] = string.Format("{0}/{1}/{2}",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue);
		e.InputParameters["pReportDayTo"] = string.Format("{0}/{1}/{2}",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,this.lstToDD.SelectedValue);
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	private void InitPage() {
		this.pnlKey.DataBind();
		this.pnlInfo.Visible = false;
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		SysPrograms.SetupFromToDay(this.lstFromYYYY,this.lstFromMM,this.lstFromDD,this.lstToYYYY,this.lstToMM,this.lstToDD,false);
		this.lstFromYYYY.SelectedValue = DateTime.Today.ToString("yyyy");
		this.lstFromMM.SelectedValue = DateTime.Today.ToString("MM");
		this.lstFromDD.SelectedValue = DateTime.Today.ToString("dd");
		this.lstToYYYY.SelectedValue = DateTime.Today.ToString("yyyy");
		this.lstToMM.SelectedValue = DateTime.Today.ToString("MM");
		this.lstToDD.SelectedValue = DateTime.Today.ToString("dd");
	}

	private void GetList() {
		this.grdCastTreasureCompleteTotalReport.DataBind();
	}

	protected void btnCSV_Click(object sender,EventArgs e) {

		if (grdCastTreasureCompleteTotalReport.Rows.Count == 0) {
			return;
		}

		DataTable oCsvData;
		using (CastTreasureCompleteTotalReport oCastTreasureCompleteTotalReport = new CastTreasureCompleteTotalReport()) {
			using (DataSet oDataSet = oCastTreasureCompleteTotalReport.GetList(
				this.lstSeekSiteCd.SelectedValue,
				string.Format("{0}/{1}/{2}",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue),
				string.Format("{0}/{1}/{2}",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,this.lstToDD.SelectedValue)
				)) {

				if (oDataSet.Tables[0].Rows.Count > 0) {
					oCsvData = oDataSet.Tables[0];
				} else {
					return;
				}
			}
		}

		//ヘッダ作成
		string sHeader = "ステージグループ名,人数";

		//CSV出力 
		Response.ContentType = "application/download";
		Response.AppendHeader("Content-Disposition","attachment;filename=CastTreasureCompleteTotalReport.csv");
		Response.ContentEncoding = System.Text.Encoding.GetEncoding("Shift_JIS");

		Response.Write(sHeader + "\r\n");
		foreach (DataRow oCsvRow in oCsvData.Rows) {
			string sData =
				SetCsvString(oCsvRow["STAGE_GROUP_NM"].ToString()) + "," +
				SetCsvString(oCsvRow["COMP_COUNT"].ToString());

			Response.Write(sData + "\r\n");
		}
		Response.End();
	}

	private string SetCsvString(string pData) {
		if (pData.IndexOf('"') > -1 ||
			pData.IndexOf(',') > -1 ||
			pData.IndexOf('\r') > -1 ||
			pData.IndexOf('\n') > -1 ||
			pData.StartsWith(" ") || pData.StartsWith("\t") ||
			pData.EndsWith(" ") || pData.EndsWith("\t")) {
			if (pData.IndexOf('"') > -1) {
				//"を""とする
				pData = pData.Replace("\"","\"\"");
			}
			if (pData.IndexOf('\r') > -1 || pData.IndexOf('\n') > -1) {
				//改行しない 
				pData = pData.Replace("\r","");
				pData = pData.Replace("\n","");
			}
			pData = "\"" + pData + "\"";
		}
		return pData;
	}
}