﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 投票順位一覧
--	Progaram ID		: VoteRankList
--  Creation Date	: 2013.10.15
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;

public partial class Extension_Pwild_VoteRankList:System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SITE_CD"]);
		}
		set {
			this.ViewState["SITE_CD"] = value;
		}
	}

	private string VoteTermSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["VOTE_TERM_SEQ"]);
		}
		set {
			this.ViewState["VOTE_TERM_SEQ"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		lblRegistError.Text = string.Empty;
		lblBulkUpdateError.Text = string.Empty;
		lblBulkUpdateComplete.Visible = false;

		if (!this.IsPostBack) {
			this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			this.VoteTermSeq = iBridUtil.GetStringValue(Request.QueryString["votetermseq"]);

			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	protected void btnBack_Click(object sender,EventArgs e) {
		this.Response.Redirect(string.Format("~/Extension/Pwild/VoteTermList.aspx?sitecd={0}",this.SiteCd));
		return;
	}

	protected void dsVoteRank_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		VoteRank.SearchCondition oSearchCondition = new VoteRank.SearchCondition();
		oSearchCondition.SiteCd = this.SiteCd;
		oSearchCondition.VoteTermSeq = this.VoteTermSeq;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsVoteRank_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void grdVoteRank_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			int iNaFlag = int.Parse(DataBinder.Eval(e.Row.DataItem,"NA_FLAG").ToString());

			if (iNaFlag.Equals(ViCommConst.NaFlag.OK)) {
				e.Row.BackColor = Color.White;
			} else {
				e.Row.BackColor = Color.LightGray;
			}
		}
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		lblRegistError.Text = string.Empty;

		List<string> lstLoginId = new List<string>();
		string[] sLoginId = this.txtLoginId.Text.Split(new char[] {'\r','\n'});

		foreach (string sValue in sLoginId) {
			if (!string.IsNullOrEmpty(sValue.Trim())) {
				lstLoginId.Add(sValue.Trim());
			}
		}

		if (lstLoginId.Count == 0) {
			lblRegistError.Text = "ログインIDを入力して下さい";
		} else {
			using (DbSession oDbSession = new DbSession()) {
				oDbSession.PrepareProcedure("VOTE_RANK_REGIST");
				oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
				oDbSession.ProcedureInParm("pVOTE_TERM_SEQ",DbSession.DbType.NUMBER,this.VoteTermSeq);
				oDbSession.ProcedureInArrayParm("pLOGIN_ID",DbSession.DbType.VARCHAR2,lstLoginId.ToArray());
				oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
				oDbSession.cmd.BindByName = true;
				oDbSession.ExecuteProcedure();
			}

			txtLoginId.Text = string.Empty;
			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	protected void btnBulkUpdate_Click(object sender,EventArgs e) {
		lblBulkUpdateError.Text = string.Empty;
		lblBulkUpdateComplete.Visible = false;

		List<string> lstAddVoteCount = new List<string>();
		List<string> lstVoteRankSeq = new List<string>();
		List<string> lstRevisionNo = new List<string>();

		foreach (GridViewRow oRow in this.grdVoteRank.Rows) {
			int iAddVoteCount;
			TextBox oAddVoteCount = oRow.FindControl("txtAddVoteCount") as TextBox;
			HiddenField oVoteRankSeq = oRow.FindControl("hdnVoteRankSeq") as HiddenField;
			HiddenField oRevisionNo = oRow.FindControl("hdnRevisionNo") as HiddenField;

			if (string.IsNullOrEmpty(oAddVoteCount.Text)) {
				lblBulkUpdateError.Text = "加算数を入力して下さい";
				return;
			} else if (!int.TryParse(oAddVoteCount.Text,out iAddVoteCount)) {
				lblBulkUpdateError.Text = "加算数が正しくありません";
				return;
			} else {
				lstAddVoteCount.Add(oAddVoteCount.Text);
				lstVoteRankSeq.Add(oVoteRankSeq.Value);
				lstRevisionNo.Add(oRevisionNo.Value);
			}
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("VOTE_RANK_BULK_UPDATE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pVOTE_TERM_SEQ",DbSession.DbType.NUMBER,this.VoteTermSeq);
			oDbSession.ProcedureInArrayParm("pVOTE_RANK_SEQ",DbSession.DbType.NUMBER,lstVoteRankSeq.ToArray());
			oDbSession.ProcedureInArrayParm("pADD_VOTE_COUNT",DbSession.DbType.NUMBER,lstAddVoteCount.ToArray());
			oDbSession.ProcedureInArrayParm("pREVISION_NO",DbSession.DbType.NUMBER,lstRevisionNo.ToArray());
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
		}

		pnlGrid.DataBind();
		pnlCount.DataBind();
		lblBulkUpdateComplete.Visible = true;
	}

	protected void lnkDelete_Command(object sender,CommandEventArgs e) {
		int iIndex = int.Parse(iBridUtil.GetStringValue(e.CommandArgument));
		GridViewRow oRow = this.grdVoteRank.Rows[iIndex];
		HiddenField oVoteRankSeq = oRow.FindControl("hdnVoteRankSeq") as HiddenField;
		HiddenField oRevisionNo = oRow.FindControl("hdnRevisionNo") as HiddenField;

		if (!string.IsNullOrEmpty(oVoteRankSeq.Value) && !string.IsNullOrEmpty(oRevisionNo.Value)) {
			using (DbSession oDbSession = new DbSession()) {
				oDbSession.PrepareProcedure("VOTE_RANK_DELETE");
				oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
				oDbSession.ProcedureInParm("pVOTE_TERM_SEQ",DbSession.DbType.NUMBER,this.VoteTermSeq);
				oDbSession.ProcedureInParm("pVOTE_RANK_SEQ",DbSession.DbType.NUMBER,oVoteRankSeq.Value);
				oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,oRevisionNo.Value);
				oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
				oDbSession.cmd.BindByName = true;
				oDbSession.ExecuteProcedure();
			}

			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	protected bool GetVoteRankVisible(object pNaFlag) {
		if (int.Parse(pNaFlag.ToString()).Equals(ViCommConst.NaFlag.OK)) {
			return true;
		} else {
			return false;
		}
	}
}