﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SysExMainte.aspx.cs" Inherits="Extension_Pwild_SysExMainte" Title="システム設定拡張" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="システム設定拡張"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" Runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[システム設定拡張内容]</legend>
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									CSV出力パスワード
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCsvPassword" runat="server" Width="140px" MaxLength="20"></asp:TextBox>
									<asp:RegularExpressionValidator ID="vdeCsvPassword" runat="server" ErrorMessage="CSV出力パスワードは4～20桁の英数字で入力して下さい。" ValidationExpression="\w{4,20}" ControlToValidate="txtCsvPassword" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdeCsvPassword" HighlightCssClass="validatorCallout" />
</asp:Content>

