﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ゲーム友達紹介一覧

--	Progaram ID		: GameIntroList
--
--  Creation Date	: 2012.02.04
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.IO;
using ViComm.Extension.Pwild;

public partial class Extension_Pwild_GameIntroList:System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		set {
			this.ViewState["SexCd"] = value;
		}
	}

	private string GameItemCategoryType {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemCategoryType"]);
		}
		set {
			this.ViewState["GameItemCategoryType"] = value;
		}
	}

	private string GameIntroSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameIntroSeq"]);
		}
		set {
			this.ViewState["GameIntroSeq"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();
			this.SexCd = iBridUtil.GetStringValue(this.Request.QueryString["sexcd"]);
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
		this.GameIntroSeq = string.Empty;
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.GameIntroSeq = string.Empty;
		this.ClearFileds();
		this.pnlKey.Enabled = false;
		this.btnDelete.Enabled = false;
		this.pnlGameIntroInfo.Visible = true;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		if (!this.IsCorrectInput()) {
			return;
		}
		this.UpdateData(false);
		this.pnlGameIntroInfo.Visible = false;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		this.UpdateData(true);
		this.pnlGameIntroInfo.Visible = false;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.ClearFileds();
		this.pnlKey.Enabled = true;
		this.pnlGameIntroInfo.Visible = false;
	}

	protected void lnkGameIntro_Command(object sender,CommandEventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.GameIntroSeq = iBridUtil.GetStringValue(e.CommandArgument);
		this.GetData();
		this.pnlKey.Enabled = false;
		this.btnDelete.Enabled = true;
		this.pnlGameIntroInfo.Visible = true;
	}

	protected void dsGameIntro_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	protected void lstGameItemCategory_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		this.GameItemCategoryType = oDropDownList.SelectedValue;

		switch (oDropDownList.ID) {
			case "lstGameItemCategory1":
				this.lstGameItem1.DataBind();
				break;
			case "lstGameItemCategory2":
				this.lstGameItem2.DataBind();
				break;
			case "lstGameItemCategory3":
				this.lstGameItem3.DataBind();
				break;
		}
	}

	protected void dsGameItem_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pGameItemCategoryType"] = this.GameItemCategoryType;
		
		if (this.SexCd == ViCommConst.MAN) {
			e.InputParameters["pSexCd"] = ViCommConst.OPERATOR;
		} else {
			e.InputParameters["pSexCd"] = ViCommConst.MAN;
		}
	}

	protected void vdcItemCount1_ServerValidate(object source,ServerValidateEventArgs args) {
		if (string.IsNullOrEmpty(this.lstGameItem1.SelectedValue)) {
			return;
		}

		if (string.IsNullOrEmpty(this.txtItemCount1.Text)) {
			args.IsValid = false;
			this.vdcItemCount1.ErrorMessage = "個数を入力してください。";
		}
	}

	protected void vdcItemCount2_ServerValidate(object source,ServerValidateEventArgs args) {
		if (string.IsNullOrEmpty(this.lstGameItem2.SelectedValue)) {
			return;
		}

		if (string.IsNullOrEmpty(this.txtItemCount2.Text)) {
			args.IsValid = false;
			this.vdcItemCount2.ErrorMessage = "個数を入力してください。";
		}
	}

	protected void vdcItemCount3_ServerValidate(object source,ServerValidateEventArgs args) {
		if (string.IsNullOrEmpty(this.lstGameItem3.SelectedValue)) {
			return;
		}

		if (string.IsNullOrEmpty(this.txtItemCount3.Text)) {
			args.IsValid = false;
			this.vdcItemCount3.ErrorMessage = "個数を入力してください。";
		}
	}

	private void InitPage() {
		this.pnlInfo.Visible = true;
		this.pnlGameIntroInfo.Visible = false;

		this.GameIntroSeq = string.Empty;
		this.pnlKey.Enabled = true;
		this.btnDelete.Enabled = true;
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}		
	}

	private void ClearFileds() {
		this.lblErrMsgIntroCount.Visible = false;
		this.txtIntroMin.Text = string.Empty;
		this.txtIntroMax.Text = string.Empty;
		this.txtIntroComment.Text = string.Empty;
		this.txtItemCount1.Text = string.Empty;
		this.txtItemCount2.Text = string.Empty;
		this.txtItemCount3.Text = string.Empty;
		this.pnlItem.DataBind();
		this.pnlKey.Enabled = true;
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.GameIntroSeq = string.Empty;
		
		this.pnlItem.DataBind();
		
		this.grdGameIntro.PageIndex = 0;
		this.grdGameIntro.DataSourceID = "dsGameIntro";
		this.grdGameIntro.DataBind();
		
		this.pnlCount.DataBind();
	}

	private void GetData() {
		this.ClearFileds();

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("GAME_INTRO_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pGAME_INTRO_SEQ",DbSession.DbType.NUMBER,this.GameIntroSeq);
			oDbSession.ProcedureOutParm("pSEX_CD",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pINTRO_MIN",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pINTRO_MAX",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pINTRO_COMMENT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutArrayParm("pITEM_SEQ_ARR",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutArrayParm("pITEM_COUNT_ARR",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutArrayParm("pITEM_CATEGORY_TYPE_ARR",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.txtIntroMin.Text = oDbSession.GetStringValue("pINTRO_MIN");
			this.txtIntroMax.Text = oDbSession.GetStringValue("pINTRO_MAX");
			this.txtIntroComment.Text = oDbSession.GetStringValue("pINTRO_COMMENT");
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");

			this.lstGameItemCategory1.SelectedValue = oDbSession.GetArryIntValue("pITEM_CATEGORY_TYPE_ARR",0);
			this.GameItemCategoryType = this.lstGameItemCategory1.SelectedValue;
			this.lstGameItem1.DataBind();
			
			this.lstGameItemCategory2.SelectedValue = oDbSession.GetArryIntValue("pITEM_CATEGORY_TYPE_ARR",1);
			this.GameItemCategoryType = this.lstGameItemCategory2.SelectedValue;
			this.lstGameItem2.DataBind();
			
			this.lstGameItemCategory3.SelectedValue = oDbSession.GetArryIntValue("pITEM_CATEGORY_TYPE_ARR",2);
			this.GameItemCategoryType = this.lstGameItemCategory3.SelectedValue;
			this.lstGameItem3.DataBind();

			this.lstGameItem1.SelectedValue = oDbSession.GetArryIntValue("pITEM_SEQ_ARR",0);
			this.lstGameItem2.SelectedValue = oDbSession.GetArryIntValue("pITEM_SEQ_ARR",1);
			this.lstGameItem3.SelectedValue = oDbSession.GetArryIntValue("pITEM_SEQ_ARR",2);

			this.txtItemCount1.Text = oDbSession.GetArryIntValue("pITEM_COUNT_ARR",0);
			this.txtItemCount2.Text = oDbSession.GetArryIntValue("pITEM_COUNT_ARR",1);
			this.txtItemCount3.Text = oDbSession.GetArryIntValue("pITEM_COUNT_ARR",2);
		}
	}

	private void UpdateData(bool pDelFlag) {
		List<string> oItemSeqList = new List<string>();
		List<string> oItemCountList = new List<string>();
		
		if (!string.IsNullOrEmpty(this.lstGameItem1.SelectedValue)) {
			oItemSeqList.Add(this.lstGameItem1.SelectedValue);
			oItemCountList.Add(this.txtItemCount1.Text);
		}
		
		if (!string.IsNullOrEmpty(this.lstGameItem2.SelectedValue)) {
			oItemSeqList.Add(this.lstGameItem2.SelectedValue);
			oItemCountList.Add(this.txtItemCount2.Text);
		}
		
		if (!string.IsNullOrEmpty(this.lstGameItem3.SelectedValue)) {
			oItemSeqList.Add(this.lstGameItem3.SelectedValue);
			oItemCountList.Add(this.txtItemCount3.Text);
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("GAME_INTRO_MAINTE");
			oDbSession.cmd.BindByName = true;
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureBothParm("pGAME_INTRO_SEQ",DbSession.DbType.NUMBER,this.GameIntroSeq);
			oDbSession.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,this.SexCd);
			oDbSession.ProcedureInParm("pINTRO_MIN",DbSession.DbType.VARCHAR2,this.txtIntroMin.Text);
			oDbSession.ProcedureInParm("pINTRO_MAX",DbSession.DbType.VARCHAR2,this.txtIntroMax.Text);
			oDbSession.ProcedureInParm("pINTRO_COMMENT",DbSession.DbType.VARCHAR2,this.txtIntroComment.Text.TrimEnd());
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInArrayParm("pITEM_SEQ_ARR",DbSession.DbType.NUMBER,oItemSeqList.ToArray());
			oDbSession.ProcedureInArrayParm("pITEM_COUNT_ARR",DbSession.DbType.NUMBER,oItemCountList.ToArray());
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDelFlag ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.ClearFileds();
		this.GetList();
	}

	private bool IsCorrectInput() {
		this.lblErrMsgIntroCount.Visible = false;
		
		using (GameIntro oGameIntro = new GameIntro()) {
			if (oGameIntro.IsDupulicateIntroCount(this.SiteCd,this.GameIntroSeq,this.SexCd,this.txtIntroMin.Text,this.txtIntroMax.Text)) {
				this.lblErrMsgIntroCount.Text = "紹介人数が他と重複しています。";
				this.lblErrMsgIntroCount.Visible = true;
				return false;
			}
		}
		
		return true;
	}
}
