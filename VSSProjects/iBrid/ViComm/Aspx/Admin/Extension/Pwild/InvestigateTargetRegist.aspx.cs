/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: この娘を探せ・ターゲット出演者 ログインIDでの登録

--	Progaram ID		: InvestigateTargetRegist
--
--  Creation Date	: 2015.03.06
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using iBridCommLib;
using ViComm;

public partial class Extension_Pwild_InvestigateTargetRegist:System.Web.UI.Page {
	private const string RESULT_OK = "0";
	private const string RESULT_NOT_EXISTS = "1";
	private const string RESULT_NG = "9";

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();
		}
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {

		string sResult = string.Empty;
		lblErrorMessageLoginId.Text = string.Empty;
		lblErrorMessageLoginId.Visible = false;
		
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("REGIST_INVESTIGATE_TARGET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.lstSeekSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pLOGIN_ID",DbSession.DbType.VARCHAR2,txtLoginId.Text);
			oDbSession.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
			
			sResult = oDbSession.GetStringValue("pRESULT");
		}
		
		if (sResult.Equals(RESULT_NOT_EXISTS)) {
			lblErrorMessageLoginId.Text = "出演者が存在しません。";
			lblErrorMessageLoginId.Visible = true;
			return;
		} else {
			lblErrorMessageLoginId.Text = string.Format("出演者({0})を登録しました。",txtLoginId.Text);
			lblErrorMessageLoginId.Visible = true;
			txtLoginId.Text = string.Empty;
			
			return;
		}
	}
	
	private void InitPage() {
		this.txtLoginId.Text = string.Empty;
	}
}
