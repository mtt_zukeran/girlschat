﻿<%@ Import Namespace="ViComm" %>
<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="InvestigateTargetList.aspx.cs" Inherits="Extension_Pwild_InvestigateTargetList" Title="この娘を探せ - 女性ピックアップ"
    ValidateRequest="false" %>

<%@ Import Namespace="System" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="この娘を探せ - 女性ピックアップ"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[検索条件]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 640px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle2">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="180px">
                                </asp:DropDownList>
                            </td>
                            <td class="tdHeaderStyle2">
                                Paging Off
                            </td>
                            <td class="tdDataStyle">
                                <asp:CheckBox ID="chkPagingOff" runat="server" />
                            </td>
                        </tr>
                    </table>
                    <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                        CausesValidation="False" />
                    <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>
                    <%= DisplayWordUtil.Replace("[出演者一覧]") %>
                </legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdInvestigateTarget.PageIndex + 1 %>
                        of
                        <%= grdInvestigateTarget.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="530px">
                    <asp:GridView ID="grdInvestigateTarget" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        EnableViewState="true" DataSourceID="dsInvestigateTarget" AllowSorting="True" SkinID="GridView"
                        DataKeyNames="SITE_CD,EXECUTION_DAY,USER_SEQ,USER_CHAR_NO,INVESTIGATE_TARGET_SEQ"
                        OnDataBound="grdInvestigateTarget_DataBound" OnRowDataBound="grdInvestigateTarget_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="日">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblDay" Text='<%# Eval("DAY") %>' /><br />
                                    (<asp:Label runat="server" ID="lblDayOfWeek" Text='<%# Eval("DAY_OF_WEEK") %>' ForeColor='<%# GetDayOfWeekColor(Eval("DAY_OF_WEEK")) %>' />)
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ﾊﾝﾄﾞﾙ名<br>会員ID" SortExpression="LOGIN_ID">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkHandleNm" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}",Eval("LOGIN_ID"),"InvestigateTargetList.aspx") %>'
                                        Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:HyperLink><br />
                                    <asp:Label ID="lblLoginId" runat="server" Text='<%# Eval("LOGIN_ID") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="120px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="写真" SortExpression="PIC_SEQ">
                                <ItemTemplate>
                                    <a href="<%# PickupHelper.GeneratePickupObjsViewerUrl(this, Eval("SITE_CD").ToString(),Eval("USER_SEQ").ToString(), ViComm.ViCommConst.PicupTypes.CAST_CHARACTER) %>">
                                        <asp:Image ID="imgProfilePic" runat="server" ImageUrl='<%# string.Format("../../{0}",Eval("SMALL_PHOTO_IMG_PATH").ToString()) %>'>
                                        </asp:Image>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ｱﾙﾊﾞﾑ">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkPicCount" runat="server" NavigateUrl="" Text='<%# GetPicCountMark(Eval("PIC_COUNT")) %>'></asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="一言ｺﾒﾝﾄ<br>(ｵﾝﾏｳｽで全表示)">
                                <ItemTemplate>
                                    <asp:Label ID="lblCommentList" runat="server" Text='<%# GetDisplayComment(Eval("COMMENT_LIST")) %>'
                                        ToolTip='<%# "$NO_TRANS_START;" + Eval("COMMENT_LIST") + "$NO_TRANS_END;" %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="110px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ｽﾀｲﾙ">
                                <ItemTemplate>
                                    <asp:Label ID="lblCastAttrValue03" runat="server" Text='<%# Eval("CAST_ATTR_VALUE03") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="地域">
                                <ItemTemplate>
                                    <asp:Label ID="lblCastAttrValue01" runat="server" Text='<%# Eval("CAST_ATTR_VALUE01") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="年齢">
                                <ItemTemplate>
                                    <asp:Label ID="lblAge" runat="server" Text='<%# Eval("AGE") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ﾀｲﾌﾟ">
                                <ItemTemplate>
                                    <asp:Label ID="lblCastAttrValue02" runat="server" Text='<%# Eval("CAST_ATTR_VALUE02") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="H度">
                                <ItemTemplate>
                                    <asp:Label ID="lblCastAttrValue09" runat="server" Text='<%# Eval("CAST_ATTR_VALUE09") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="職業">
                                <ItemTemplate>
                                    <asp:Label ID="lblCastAttrValue04" runat="server" Text='<%# Eval("CAST_ATTR_VALUE04") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ﾊﾞｽﾄ">
                                <ItemTemplate>
                                    <asp:Label ID="lblCastAttrValue05" runat="server" Text='<%# Eval("CAST_ATTR_VALUE05") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="身長">
                                <ItemTemplate>
                                    <asp:Label ID="lblCastAttrValue06" runat="server" Text='<%# Eval("CAST_ATTR_VALUE06") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="血液型">
                                <ItemTemplate>
                                    <asp:Label ID="lblCastAttrValue07" runat="server" Text='<%# Eval("CAST_ATTR_VALUE07") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" ID="lnkDelete" Text="削除" OnCommand="lnkDelete_Command" CommandName="DELETE"
                                        CommandArgument='<%# Container.DataItemIndex %>' OnClientClick="return confirm('削除を行いますか？');"></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
            <br />
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsInvestigateTarget" runat="server" SelectMethod="GetPageCollection"
        TypeName="InvestigateTarget" SelectCountMethod="GetPageCount" EnablePaging="True"
        OnSelected="dsInvestigateTarget_Selected" OnSelecting="dsInvestigateTarget_Selecting">
        <SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
</asp:Content>
