﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: お宝deビンゴ開催設定
--	Progaram ID		: BbsBingoTermList
--  Creation Date	: 2013.10.29
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using ViComm.Extension.Pwild;

public partial class Extension_Pwild_BbsBingoTermList:System.Web.UI.Page {
	private string recCount = string.Empty;
	protected string[] YearArray;
	protected string[] MonthArray;
	protected string[] DayArray;
	protected string[] HourArray;
	protected string[] MinuteArray;

	private string TermSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["TERM_SEQ"]);
		}
		set {
			this.ViewState["TERM_SEQ"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["REVISION_NO"]);
		}
		set {
			this.ViewState["REVISION_NO"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);

			if (string.IsNullOrEmpty(sSiteCd)) {
				if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
					lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
				} else {
					lstSiteCd.SelectedIndex = 0;
				}
			} else {
				lstSiteCd.SelectedValue = sSiteCd;
			}

			this.pnlSearch.Enabled = true;
			this.pnlInput.Visible = false;

			lstSiteCd.DataBind();
			pnlGrid.DataBind();
			pnlCount.DataBind();

			SetDateArray();
			pnlInput.DataBind();
		}
	}

	protected void dsBbsBingoTerm_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		BbsBingoTerm.SearchCondition oSearchCondition = new BbsBingoTerm.SearchCondition();
		oSearchCondition.SiteCd = lstSiteCd.SelectedValue;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsBbsBingoTerm_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected string GetBingoBallFlagStr(object pBingoBallFlag) {
		return (pBingoBallFlag.ToString().Equals(ViCommConst.FLAG_ON_STR)) ? "有効" : "無効";
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			grdBbsBingoTerm.PageIndex = 0;
			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		this.TermSeq = string.Empty;
		this.RevisionNo = string.Empty;
		this.ClearFileds();

		this.pnlSearch.Enabled = false;
		this.pnlInput.Visible = true;
		this.btnDelete.Enabled = false;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (this.CheckInput()) {
			string sResult;
			this.BbsBingoTermMainte(ViCommConst.FLAG_OFF,out sResult);

			if (sResult.Equals(PwViCommConst.BbsBingoTermMainteResult.RESULT_OK)) {
				this.pnlSearch.Enabled = true;
				this.pnlInput.Visible = false;
				grdBbsBingoTerm.PageIndex = 0;
				pnlGrid.DataBind();
				pnlCount.DataBind();
			} else if (sResult.Equals(PwViCommConst.BbsBingoTermMainteResult.RESULT_NG_DATE)) {
				this.lblErrorMessage.Text = "期間が他と重複しています";
			}
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlSearch.Enabled = true;
		this.pnlInput.Visible = false;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		string sResult;
		this.BbsBingoTermMainte(ViCommConst.FLAG_ON,out sResult);

		this.pnlSearch.Enabled = true;
		this.pnlInput.Visible = false;
		grdBbsBingoTerm.PageIndex = 0;
		pnlGrid.DataBind();
		pnlCount.DataBind();
	}

	protected void lnkEdit_Command(object sender,CommandEventArgs e) {
		int iIndex = int.Parse(iBridUtil.GetStringValue(e.CommandArgument));
		GridViewRow oRow = this.grdBbsBingoTerm.Rows[iIndex];
		HiddenField hdnTermSeq = oRow.FindControl("hdnTermSeq") as HiddenField;
		this.TermSeq = hdnTermSeq.Value;

		this.ClearFileds();
		this.BbsBingoTermGet();

		this.pnlSearch.Enabled = false;
		this.pnlInput.Visible = true;
		this.btnDelete.Enabled = true;
	}

	private void SetDateArray() {
		this.YearArray = new string[] { DateTime.Today.AddYears(-1).ToString("yyyy"),DateTime.Today.ToString("yyyy"),DateTime.Today.AddYears(1).ToString("yyyy"),DateTime.Today.AddYears(2).ToString("yyyy") };
		this.MonthArray = new string[] { "01","02","03","04","05","06","07","08","09","10","11","12" };
		this.DayArray = new string[] { "01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31" };
		this.HourArray = new string[] { "00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23" };

		List<string> oMinuteList = new List<string>();

		for (int i = 0;i < 60;i++) {
			oMinuteList.Add(string.Format("{0:D2}",i));
		}

		this.MinuteArray = oMinuteList.ToArray();
	}

	private bool CheckInput() {
		DateTime dtStartDate = new DateTime();
		DateTime dtEndDate = new DateTime();

		if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstStartDateYYYY.SelectedValue,this.lstStartDateMM.SelectedValue,this.lstStartDateDD.SelectedValue,this.lstStartDateHH.SelectedValue,this.lstStartDateMI.SelectedValue),out dtStartDate)) {
			this.lblErrorMessage.Text = "開始日時が正しくありません";
			return false;
		}

		if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:59",this.lstEndDateYYYY.SelectedValue,this.lstEndDateMM.SelectedValue,this.lstEndDateDD.SelectedValue,this.lstEndDateHH.SelectedValue,this.lstEndDateMI.SelectedValue),out dtEndDate)) {
			this.lblErrorMessage.Text = "終了日時が正しくありません";
			return false;
		}

		if (dtStartDate > dtEndDate) {
			this.lblErrorMessage.Text = "期間の大小関係が正しくありません";
			return false;
		}

		return true;
	}

	private void ClearFileds() {
		this.lblErrorMessage.Text = string.Empty;

		this.lstStartDateYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstStartDateMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstStartDateDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstStartDateHH.SelectedValue = "00";
		this.lstStartDateMI.SelectedValue = "00";

		this.lstEndDateYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstEndDateMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstEndDateDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstEndDateHH.SelectedValue = "23";
		this.lstEndDateMI.SelectedValue = "59";

		this.rdoBingoBallFlagOff.Checked = true;
		this.rdoBingoBallFlagOn.Checked = false;
	}

	private void BbsBingoTermGet() {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("BBS_BINGO_TERM_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pTERM_SEQ",DbSession.DbType.VARCHAR2,this.TermSeq);
			oDbSession.ProcedureOutParm("pSTART_DATE",DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pEND_DATE",DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pBINGO_BALL_FLAG",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();

			DateTime dtStartDate = (DateTime)oDbSession.GetDateTimeValue("pSTART_DATE");
			DateTime dtEndDate = (DateTime)oDbSession.GetDateTimeValue("pEND_DATE");

			if (dtStartDate != null) {
				this.lstStartDateYYYY.SelectedValue = dtStartDate.ToString("yyyy");
				this.lstStartDateMM.SelectedValue = dtStartDate.ToString("MM");
				this.lstStartDateDD.SelectedValue = dtStartDate.ToString("dd");
				this.lstStartDateHH.SelectedValue = dtStartDate.ToString("HH");
				this.lstStartDateMI.SelectedValue = dtStartDate.ToString("mm");
			}

			if (dtEndDate != null) {
				this.lstEndDateYYYY.SelectedValue = dtEndDate.ToString("yyyy");
				this.lstEndDateMM.SelectedValue = dtEndDate.ToString("MM");
				this.lstEndDateDD.SelectedValue = dtEndDate.ToString("dd");
				this.lstEndDateHH.SelectedValue = dtEndDate.ToString("HH");
				this.lstEndDateMI.SelectedValue = dtEndDate.ToString("mm");
			}

			int iBingoBallFlag = oDbSession.GetIntValue("pBINGO_BALL_FLAG");

			if (iBingoBallFlag == 1) {
				this.rdoBingoBallFlagOff.Checked = false;
				this.rdoBingoBallFlagOn.Checked = true;
			} else {
				this.rdoBingoBallFlagOff.Checked = true;
				this.rdoBingoBallFlagOn.Checked = false;
			}

			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
		}
	}

	private void BbsBingoTermMainte(int pDeleteFlag,out string pResult) {
		DateTime dtStartDate = DateTime.Parse(string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstStartDateYYYY.SelectedValue,this.lstStartDateMM.SelectedValue,this.lstStartDateDD.SelectedValue,this.lstStartDateHH.SelectedValue,this.lstStartDateMI.SelectedValue));
		DateTime dtEndDate = DateTime.Parse(string.Format("{0}/{1}/{2} {3}:{4}:59",this.lstEndDateYYYY.SelectedValue,this.lstEndDateMM.SelectedValue,this.lstEndDateDD.SelectedValue,this.lstEndDateHH.SelectedValue,this.lstEndDateMI.SelectedValue));
		int iBingoBallFlag = (this.rdoBingoBallFlagOn.Checked) ? 1 : 0;

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("BBS_BINGO_TERM_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pTERM_SEQ",DbSession.DbType.NUMBER,this.TermSeq);
			oDbSession.ProcedureInParm("pSTART_DATE",DbSession.DbType.DATE,dtStartDate);
			oDbSession.ProcedureInParm("pEND_DATE",DbSession.DbType.DATE,dtEndDate);
			oDbSession.ProcedureInParm("pBINGO_BALL_FLAG",DbSession.DbType.NUMBER,iBingoBallFlag);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDeleteFlag);
			oDbSession.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
			pResult = oDbSession.GetStringValue("pRESULT");
		}
	}
}
