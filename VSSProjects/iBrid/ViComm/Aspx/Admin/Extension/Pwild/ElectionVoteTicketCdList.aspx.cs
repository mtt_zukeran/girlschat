﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 投票券コード一覧(エントリー制)
--	Progaram ID		: ElectionVoteTicketCdList
--  Creation Date	: 2013.12.10
--  Creater			: Y.Ikemiya
**************************************************************************/
using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;

public partial class Extension_Pwild_ElectionVoteTicketCdList:System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SITE_CD"]);
		}
		set {
			this.ViewState["SITE_CD"] = value;
		}
	}

	private string ElectionPeriodSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["ELECTION_PERIOD_SEQ"]);
		}
		set {
			this.ViewState["ELECTION_PERIOD_SEQ"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		lblRegistComplete.Visible = false;
		lblRegistError.Text = string.Empty;

		if (!this.IsPostBack) {
			this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			this.ElectionPeriodSeq = iBridUtil.GetStringValue(Request.QueryString["electionperiodseq"]);

			txtTicketCount.Text = "5";

			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	protected void btnBack_Click(object sender,EventArgs e) {
		this.Response.Redirect(string.Format("~/Extension/Pwild/ElectionPeriodList.aspx?sitecd={0}",this.SiteCd));
		return;
	}

	protected void dsElectionVoteTicketCd_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		ElectionVoteTicketCd.SearchCondition oSearchCondition = new ElectionVoteTicketCd.SearchCondition();
		oSearchCondition.SiteCd = this.SiteCd;
		oSearchCondition.ElectionPeriodSeq = this.ElectionPeriodSeq;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsElectionVoteTicketCd_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		lblRegistError.Text = string.Empty;
		int iAddCount;
		int iTicketCount;

		if (string.IsNullOrEmpty(txtAddCount.Text)) {
			lblRegistError.Text = "コード追加数を入力して下さい";
			return;
		} else if (!int.TryParse(txtAddCount.Text,out iAddCount)) {
			lblRegistError.Text = "コード追加数が正しくありません";
			return;
		}

		if (string.IsNullOrEmpty(txtTicketCount.Text)) {
			lblRegistError.Text = "投票券引換数を入力して下さい";
			return;
		} else if (!int.TryParse(txtTicketCount.Text,out iTicketCount)) {
			lblRegistError.Text = "投票券引換数が正しくありません";
			return;
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("ELECTION_VOTE_TICKET_CD_REGIST");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pELECTION_PERIOD_SEQ",DbSession.DbType.NUMBER,this.ElectionPeriodSeq);
			oDbSession.ProcedureInParm("pADD_COUNT",DbSession.DbType.NUMBER,iAddCount);
			oDbSession.ProcedureInParm("pTICKET_COUNT",DbSession.DbType.NUMBER,iTicketCount);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
		}

		lblRegistComplete.Visible = true;

		pnlGrid.DataBind();
		pnlCount.DataBind();
	}
}
