﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: システム設定拡張メンテナンス
--	Progaram ID		: SysExMainte
--  Creation Date	: 2012.09.07
--  Creater			: PW K.Miyazato
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;

public partial class Extension_Pwild_SysExMainte:System.Web.UI.Page {
	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData();
		}
	}

	private void InitPage() {
		ClearField();
		GetData();
	}

	private void ClearField() {
		txtCsvPassword.Text = string.Empty;
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SYS_EX_GET");
			db.ProcedureOutParm("pCSV_PASSWORD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("pREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("pROWID");

			if (int.Parse(db.GetStringValue("pRECORD_COUNT")) > 0) {
				txtCsvPassword.Text = db.GetStringValue("pCSV_PASSWORD");
			}
		}
	}

	private void UpdateData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SYS_EX_MAINTE");
			db.ProcedureInParm("pCSV_PASSWORD",DbSession.DbType.VARCHAR2,txtCsvPassword.Text);
			db.ProcedureInParm("pROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		InitPage();
	}
}
