﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 会員つぶやき

--	Progaram ID		: ManTweetCommentList
--
--  Creation Date	: 2013.02.19
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.Collections.Generic;

public partial class Extension_Pwild_ManTweetCommentList:System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string LoginId {
		get {
			return iBridUtil.GetStringValue(this.ViewState["LoginId"]);
		}
		set {
			this.ViewState["LoginId"] = value;
		}
	}

	private string ManTweetSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["ManTweetSeq"]);
		}
		set {
			this.ViewState["ManTweetSeq"] = value;
		}
	}

	private DataSet ManTweetCommentData {
		get {
			return this.ViewState["ManTweetCommentData"] as DataSet;
		}
		set {
			this.ViewState["ManTweetCommentData"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();

			this.pnlInfo.Visible = true;
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	/// <summary>
	/// CSV出力ボタンが押された時の処理
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnCSV_Click(object sender,EventArgs e) {
		string fileName = ViCommConst.CSV_FILE_NM_MAN_TWEET_COMMENT;

		Response.AddHeader("Content-Disposition","attachment;filename=" + fileName);
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		// 検索条件を設定
		ManTweetComment.SearchCondition oSearchCondition = new ManTweetComment.SearchCondition();
		oSearchCondition.Keyword = this.txtKeyword.Text.Trim();
		oSearchCondition.LoginId = this.txtLoginId.Text.Trim();
		oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
		oSearchCondition.ManTweetSeq = this.ManTweetSeq;
		oSearchCondition.TweetDateFrom = string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue,this.lstFromHH.SelectedValue,this.lstFromMI.SelectedValue);
		string sToDD;
		if (this.lstToDD.SelectedIndex == 0) {
			sToDD = DateTime.DaysInMonth(int.Parse(this.lstToYYYY.SelectedValue),int.Parse(this.lstToMM.SelectedValue)).ToString();
		} else {
			sToDD = this.lstToDD.SelectedValue;
		}
		oSearchCondition.TweetDateTo = string.Format("{0}/{1}/{2} {3}:{4}:59",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,sToDD,this.lstToHH.SelectedValue,this.lstToMI.SelectedValue);

		using (ManTweetComment oManTweetComment = new ManTweetComment()) {
			DataSet ds = oManTweetComment.GetList(oSearchCondition);
			// CSVに出力する文字列にタイトル行を設定
			System.Text.StringBuilder oCsvBuilder = new System.Text.StringBuilder();
			oCsvBuilder.Append("書込日時,");
			oCsvBuilder.Append("ログインID,");
			oCsvBuilder.Append("ハンドル名");
			oCsvBuilder.Append("\r\n");
			// CSVに出力する文字列にレコード行を設定
			DataRow dr;
			for (int i = 0;i < ds.Tables[0].Rows.Count;i++) {
				dr = ds.Tables[0].Rows[i];
				oCsvBuilder.Append(dr["COMMENT_DATE"]);
				oCsvBuilder.Append(",");
				oCsvBuilder.Append(dr["LOGIN_ID"]);
				oCsvBuilder.Append(",");
				oCsvBuilder.Append(dr["HANDLE_NM"]);
				oCsvBuilder.Append("\r\n");
			}

			Response.BinaryWrite(encoding.GetBytes(oCsvBuilder.ToString()));
			Response.End();
		}
	}

	protected void dsManTweetComment_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		DataSet oManTweetCommentDataSet = e.ReturnValue as DataSet;
		if (oManTweetCommentDataSet != null && (oManTweetCommentDataSet).Tables[0].Rows.Count > 0) {
			this.ManTweetCommentData = oManTweetCommentDataSet;
		}
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void grdManTweetComment_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			if (DataBinder.Eval(e.Row.DataItem,"ADMIN_DEL_FLAG").ToString().Equals(ViCommConst.FLAG_ON_STR)) {
				e.Row.ForeColor = Color.Gray;
			}
			if (DataBinder.Eval(e.Row.DataItem,"DEL_FLAG").ToString().Equals(ViCommConst.FLAG_ON_STR)) {
				e.Row.ForeColor = Color.Gray;
			}
		}
	}

	protected void dsManTweetComment_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		ManTweetComment.SearchCondition oSearchCondition = new ManTweetComment.SearchCondition();
		oSearchCondition.Keyword = this.txtKeyword.Text.Trim();
		oSearchCondition.LoginId = this.txtLoginId.Text.Trim();
		oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
		oSearchCondition.ManTweetSeq = this.ManTweetSeq;

		oSearchCondition.TweetDateFrom = string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue,this.lstFromHH.SelectedValue,this.lstFromMI.SelectedValue);
		string sToDD;
		if (this.lstToDD.SelectedIndex == 0) {
			sToDD = DateTime.DaysInMonth(int.Parse(this.lstToYYYY.SelectedValue),int.Parse(this.lstToMM.SelectedValue)).ToString();
		} else {
			sToDD = this.lstToDD.SelectedValue;
		}
		oSearchCondition.TweetDateTo = string.Format("{0}/{1}/{2} {3}:{4}:59",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,sToDD,this.lstToHH.SelectedValue,this.lstToMI.SelectedValue);
		
		e.InputParameters[0] = oSearchCondition;
	}

	protected void vdcFromTo_ServerValidate(object source,ServerValidateEventArgs e) {
		if (this.IsValid) {
			if (e.IsValid) {
				DateTime dtFrom;
				DateTime dtTo;
				if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue,this.lstFromHH.SelectedValue,this.lstFromMI.SelectedValue),out dtFrom)) {
					this.vdcFromTo.Text = "書込日時Fromに正しい日時を設定してください。";
					e.IsValid = false;
					return;
				}
				string sToDD;
				if (this.lstToDD.SelectedIndex == 0) {
					sToDD = DateTime.DaysInMonth(int.Parse(this.lstToYYYY.SelectedValue),int.Parse(this.lstToMM.SelectedValue)).ToString();
				} else {
					sToDD = this.lstToDD.SelectedValue;
				}
				if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:00",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,sToDD,this.lstToHH.SelectedValue,this.lstToMI.SelectedValue),out dtTo)) {
					this.vdcFromTo.Text = "書込日時Toに正しい日時を設定してください。";
					e.IsValid = false;
					return;
				}
				if (dtFrom >= dtTo) {
					this.vdcFromTo.Text = "書込日時の大小関係に誤りがあります。";
					e.IsValid = false;
				}
			}
		}
	}

	private void InitPage() {
		if (!this.IsPostBack) {
			SysPrograms.SetupFromToDayTime(this.lstFromYYYY,this.lstFromMM,this.lstFromDD,this.lstFromHH,this.lstToYYYY,this.lstToMM,this.lstToDD,this.lstToHH,false);
			this.lstFromMM.Items.Insert(0,new ListItem("--","01"));
			this.lstToMM.Items.Insert(0,new ListItem("--","12"));
			this.lstFromDD.Items.Insert(0,new ListItem("--","01"));
			this.lstToDD.Items.Insert(0,new ListItem("--","31"));
			this.lstFromHH.Items.Insert(0,new ListItem("--","00"));
			this.lstToHH.Items.Insert(0,new ListItem("--","23"));

			for (int i = 0;i < 60;i++) {
				this.lstFromMI.Items.Add(new ListItem(string.Format("{0:D2}",i)));
				this.lstToMI.Items.Add(new ListItem(string.Format("{0:D2}",i)));
			}
			this.lstFromMI.Items.Insert(0,new ListItem("--","00"));
			this.lstToMI.Items.Insert(0,new ListItem("--","59"));
		}

		this.recCount = "0";
		this.txtLoginId.Text = string.Empty;
		this.txtKeyword.Text = string.Empty;
		this.grdManTweetComment.DataSourceID = string.Empty;



		this.lstFromYYYY.SelectedIndex = 0;
		this.lstToYYYY.SelectedIndex = 0;
		this.lstFromMM.SelectedIndex = 0;
		this.lstToMM.SelectedIndex = 0;
		this.lstFromDD.SelectedIndex = 0;
		this.lstToDD.SelectedIndex = 0;
		this.lstFromHH.SelectedIndex = 0;
		this.lstToHH.SelectedIndex = 0;
		this.lstFromMI.SelectedIndex = 0;
		this.lstToMI.SelectedIndex = 0;

		this.pnlInfo.Visible = false;
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		if (!this.IsPostBack) {
			this.SiteCd = this.Request.QueryString["sitecd"];
			this.ManTweetSeq = this.Request.QueryString["tweetseq"];


			if (!string.IsNullOrEmpty(this.SiteCd) && !string.IsNullOrEmpty(this.ManTweetSeq)) {
				this.lstSiteCd.SelectedValue = this.SiteCd;

				this.pnlInfo.Visible = true;
				this.GetList();
			}
		}
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.grdManTweetComment.PageIndex = 0;
		this.grdManTweetComment.PageSize = 30;
		this.grdManTweetComment.DataSourceID = "dsManTweetComment";
		this.grdManTweetComment.DataBind();
		this.pnlCount.DataBind();
	}

	protected void lnkDelManTweetComment_Command(object sender,CommandEventArgs e) {
		string[] arguments = e.CommandArgument.ToString().Split(',');

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("DELETE_MAN_TWEET_COMMENT_ADMIN");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,arguments[0]);
			oDbSession.ProcedureInParm("pMAN_TWEET_COMMENT_SEQ",DbSession.DbType.VARCHAR2,arguments[1]);
			oDbSession.ProcedureInParm("pDEL_FLAG",DbSession.DbType.VARCHAR2,ViCommConst.FLAG_ON_STR.Equals(arguments[2]) ? ViCommConst.FLAG_OFF_STR : ViCommConst.FLAG_ON_STR);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.GetList();
	}
}
