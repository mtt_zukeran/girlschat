﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="WantedEntrantList.aspx.cs" Inherits="Extension_WantedEntrantList" Title="この娘を探せ - 男性エントリー(詳細)"
    ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="この娘を探せ - 男性エントリー(詳細)"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset>
                <legend>[設定]</legend>
                <table border="0" style="width: 640px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            サイトコード
                        </td>
                        <td class="tdDataStyle" colspan="3">
                            <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="180px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            発見
                        </td>
                        <td class="tdDataStyle" colspan="3">
                            <asp:DropDownList ID="lstCaught" runat="server" Width="100px">
                                <asp:ListItem Text="全て" Value="" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="未発見" Value="0"></asp:ListItem>
                                <asp:ListItem Text="発見済" Value="1"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            pt獲得
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstPointAcquired" runat="server" Width="100px">
                                <asp:ListItem Text="全て" Value="" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="未獲得" Value="0"></asp:ListItem>
                                <asp:ListItem Text="獲得済" Value="1"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="tdHeaderStyle2">
                            Paging Off
                        </td>
                        <td class="tdDataStyle">
                            <asp:CheckBox ID="chkPagingOff" runat="server" />
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    CausesValidation="False" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
                <asp:Button ID="btnBack" runat="server" Text="戻る" CssClass="seekbutton" OnClick="btnBack_Click" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[男性エントリー(詳細)]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdWantedEntrant.PageIndex + 1%>
                        of
                        <%= grdWantedEntrant.PageCount%>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="502px">
                    <asp:GridView ID="grdWantedEntrant" runat="server" AutoGenerateColumns="False" DataSourceID="dsWantedEntrant"
                        AllowPaging="true" AllowSorting="true" SkinID="GridViewColor" DataKeyNames="SITE_CD,EXECUTION_DAY,USER_SEQ,USER_CHAR_NO">
                        <Columns>
                            <asp:BoundField HeaderText="ID" DataField="LOGIN_ID" SortExpression="LOGIN_ID">
                                <ItemStyle HorizontalAlign="left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="ハンドル名">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkHandleNm" runat="server" NavigateUrl='<%# string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}", Eval("SITE_CD"), Eval("LOGIN_ID")) %>'
                                        Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="ENTRY_TIME" HeaderText="参加時間" SortExpression="ENTRY_TIME">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="CAUGHT_TIME" HeaderText="発見時間" SortExpression="CAUGHT_TIME">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="POINT_ACQUIRED_TIME" HeaderText="pt獲得時間" SortExpression="POINT_ACQUIRED_TIME">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsWantedEntrant" runat="server" SelectMethod="GetPageCollection"
        SelectCountMethod="GetPageCount" EnablePaging="True" OnSelecting="dsWantedEntrant_Selecting"
        OnSelected="dsWantedEntrant_Selected" TypeName="WantedEntrant">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSeekSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:QueryStringParameter Name="pExecutionDay" QueryStringField="executionday" Type="String" />
            <asp:Parameter Name="pCaughtFlag" Type="String" />
            <asp:Parameter Name="pPointAcquiredFlag" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
