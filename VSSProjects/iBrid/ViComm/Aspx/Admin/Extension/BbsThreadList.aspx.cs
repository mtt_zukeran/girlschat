﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 掲示板スレッド検索

--	Progaram ID		: BbsThreadList
--
--  Creation Date	: 2011.04.08
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Text;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web;
using ViComm;
using iBridCommLib;

public partial class Extension_BbsThreadList : System.Web.UI.Page {
	private string UserSeq {
		get { return iBridUtil.GetStringValue(this.ViewState["UserSeq"]); }
		set { this.ViewState["UserSeq"] = value; }
	}

	private string UserCharNo {
		get { return iBridUtil.GetStringValue(this.ViewState["UserCharNo"]); }
		set { this.ViewState["UserCharNo"] = value; }
	}

	private string BbsThreadSeq {
		get { return iBridUtil.GetStringValue(this.ViewState["BbsThreadSeq"]); }
		set { this.ViewState["BbsThreadSeq"] = value; }
	}

	private string BbsThreadHandleNm {
		get { return iBridUtil.GetStringValue(this.ViewState["BbsThreadHandleNm"]); }
		set { this.ViewState["BbsThreadHandleNm"] = value; }
	}

	private string DelFlag {
		get { return iBridUtil.GetStringValue(this.ViewState["DelFlag"]); }
		set { this.ViewState["DelFlag"] = value; }
	}

	private string RevisionNo {
		get { return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]); }
		set { this.ViewState["RevisionNo"] = value; }
	}

	string recCount;

	protected void Page_Load(object sender, EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender, EventArgs e) {
		this.GetList();
	}

	protected void btnRegister_Click(object sender, EventArgs e) {
		this.ClearField();
		this.lstSiteCd.SelectedValue = this.lstSeekSiteCd.SelectedValue;

		this.DelFlag = ViCommConst.FLAG_OFF_STR;
		this.SetCharNoVisible();

		this.pnlMainte.Visible = true;
		this.pnlRegister.Enabled = true;
	}

	protected void btnClear_Click(object sender, EventArgs e) {
		this.InitPage();
	}

	protected void btnCancel_Click(object sender, EventArgs e) {
		this.pnlMainte.Visible = false;
		this.ClearField();
	}

	protected void btnUpdate_Click(object sender, EventArgs e) {
		this.UpdateData();
	}

	protected void btnDelete_Click(object sender, EventArgs e) {
		this.UpdateDelFlag(ViCommConst.FLAG_OFF_STR.Equals(this.DelFlag) ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF);
	}

	protected void lnkUpdateDelFlag_Command(object sender, CommandEventArgs e) {
		if (!e.CommandName.Equals("DELETE")) {
			return;
		}

		this.ClearField();

		int iRowIndex = int.Parse(e.CommandArgument.ToString());
		IDictionary oDataKeys = this.grdBbsThread.DataKeys[iRowIndex].Values;

		this.UserSeq = iBridUtil.GetStringValue(oDataKeys["USER_SEQ"]);
		this.UserCharNo = iBridUtil.GetStringValue(oDataKeys["USER_CHAR_NO"]);
		this.BbsThreadSeq = iBridUtil.GetStringValue(oDataKeys["BBS_THREAD_SEQ"]);
		this.DelFlag = (this.grdBbsThread.Rows[iRowIndex].FindControl("hdnDelFlag") as HiddenField).Value;
		this.RevisionNo = (this.grdBbsThread.Rows[iRowIndex].FindControl("hdnRevisionNo") as HiddenField).Value;

		this.UpdateDelFlag(ViCommConst.FLAG_OFF_STR.Equals(this.DelFlag) ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF);
	}

	protected void lnkMainte_Command(object sender, CommandEventArgs e) {
		if (!e.CommandName.Equals("MAINTE")) {
			return;
		}

		this.ClearField();

		int iRowIndex = int.Parse(e.CommandArgument.ToString());
		IDictionary oDataKeys = this.grdBbsThread.DataKeys[iRowIndex].Values;

		this.UserSeq = iBridUtil.GetStringValue(oDataKeys["USER_SEQ"]);
		this.UserCharNo = iBridUtil.GetStringValue(oDataKeys["USER_CHAR_NO"]);
		this.BbsThreadSeq = iBridUtil.GetStringValue(oDataKeys["BBS_THREAD_SEQ"]);
		this.txtCharNo.Text = this.UserCharNo;
		this.txtLoginId.Text = (this.grdBbsThread.Rows[iRowIndex].FindControl("lnkLoginId") as HyperLink).Text;

		this.SetCharNoVisible();

		this.GetData();
		this.pnlMainte.Visible = true;
		this.pnlRegister.Enabled = false;
	}

	protected void grdBbsThread_RowDataBound(object sender, GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			if (ViCommConst.FLAG_ON_STR.Equals(DataBinder.Eval(e.Row.DataItem, "DEL_FLAG").ToString())) {
				e.Row.BackColor = Color.Gainsboro;
			}
		}
	}

	protected void dsBbsThread_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsBbsThread_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pLoginId"] = this.txtSeekLoginId.Text.Trim();
		e.InputParameters["pTitle"] = this.txtSeekTitle.Text.Trim();
		e.InputParameters["pBbsThreadSeq"] = this.txtBbsThreadSeq.Text.Trim();
		e.InputParameters["pAdminThreadFlag"] = this.chkSeekAdminThread.Checked ? ViCommConst.FLAG_ON_STR : string.Empty;
	}

	protected void vdcAll_ServerValidate(object source, ServerValidateEventArgs e) {
		if (this.IsValid) {
			if (!e.IsValid) {
				return;
			}

			using (CastCharacter oCastCharacter = new CastCharacter()) {
				e.IsValid = oCastCharacter.IsExistLoginId(this.lstSiteCd.SelectedValue, this.txtLoginId.Text, ViCommConst.MAIN_CHAR_NO);
				if (e.IsValid) {
					this.lblHandleNm.Text = oCastCharacter.handleNm;
					this.UserSeq = oCastCharacter.userSeq;
				} else {
					this.lblHandleNm.Text = string.Empty;
					this.vdcAll.ErrorMessage = DisplayWordUtil.Replace("出演者ID") + "が登録されていません。";
					return;
				}
			}
			
			if (this.tdHeaderCharNo.Visible) {
				using (CastCharacter oCastCharacter = new CastCharacter()) {
					e.IsValid = oCastCharacter.IsExistLoginId(this.lstSiteCd.SelectedValue, this.txtLoginId.Text, this.txtCharNo.Text);
					if (e.IsValid) {
						this.lblHandleNm.Text = oCastCharacter.handleNm;
					} else {
						this.lblHandleNm.Text = string.Empty;
						this.vdcAll.ErrorMessage = DisplayWordUtil.Replace("出演者キャラクターNO") + "が登録されていません。";
						return;
					}
				}
			}

			if (string.IsNullOrEmpty(this.lstThreadType.SelectedValue)) {
				this.vdcAll.ErrorMessage = "スレッド種別を選択して下さい。";
				e.IsValid = false;
				return;
			}

			if (this.txtTitle.Text.Length == 0 || Encoding.GetEncoding("UTF-8").GetByteCount(this.txtTitle.Text) > 3000) {
				this.vdcAll.ErrorMessage = "タイトルの入力が不正です。";
				e.IsValid = false;
				return;
			}

			if (this.txtDoc.Text.Length == 0 || Encoding.GetEncoding("UTF-8").GetByteCount(this.txtDoc.Text) > 12000) {
				this.vdcAll.ErrorMessage = "内容の入力が不正です。";
				e.IsValid = false;
				return;
			}
		}
	}

	protected void lstThreadType_DataBound(object sender, EventArgs e) {
		this.lstThreadType.Items.Insert(0, new ListItem(string.Empty, string.Empty));
	}

	private void InitPage() {
		this.ClearField();
		this.recCount = "0";
		this.chkPagingOff.Checked = false;
		this.txtSeekLoginId.Text = string.Empty;
		this.txtSeekTitle.Text = string.Empty;
		this.grdBbsThread.DataSourceID = string.Empty;

		this.pnlInfo.Visible = false;
		this.pnlMainte.Visible = false;
		this.pnlRegister.Enabled = false;
		this.lstSiteCd.DataBind();
		this.lstSeekSiteCd.DataBind();
		this.lstThreadType.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		this.pnlCount.DataBind();
	}

	private void ClearField() {
		this.UserSeq = string.Empty;
		this.UserCharNo = string.Empty;
		this.BbsThreadSeq = string.Empty;
		this.BbsThreadHandleNm = string.Empty;
		this.RevisionNo = string.Empty;
		this.DelFlag = string.Empty;

		this.lblHandleNm.Text = string.Empty;
		this.txtLoginId.Text = string.Empty;
		this.txtDoc.Text = string.Empty;
		this.txtTitle.Text = string.Empty;
		this.txtBbsThreadSeq.Text = string.Empty;
		this.chkAdminThread.Checked = false;
		this.lstThreadType.SelectedIndex = 0;
	}

	private void SetCharNoVisible() {
		using (Site oSite = new Site()) {
			if (oSite.GetMultiCharFlag(this.lstSiteCd.SelectedValue)) {
				this.tdHeaderCharNo.Visible = true;
				this.tdDataCharNo.Visible = true;
				this.tdDataLoginId.ColSpan = 1;
			} else {
				this.tdHeaderCharNo.Visible = false;
				this.tdDataCharNo.Visible = false;
				this.txtCharNo.Text = ViCommConst.MAIN_CHAR_NO;
				this.tdDataLoginId.ColSpan = 3;
			}
		}
	}

	protected string GetBbsThreadDoc(object pBbsThreadDoc) {
		string sBbsThreadDoc = iBridUtil.GetStringValue(pBbsThreadDoc);
		if (sBbsThreadDoc.Length > 30) {
			return string.Concat(SysPrograms.Substring(sBbsThreadDoc, 29), "…");
		} else {
			return sBbsThreadDoc;
		}
	}

	protected string GetDelFlagMark(object pDelFlag) {
		return ViCommConst.FLAG_ON_STR.Equals(iBridUtil.GetStringValue(pDelFlag)) ? "削除の取消" : "削除";
	}

	protected string GetOnClientClick(object pDelFlag) {
		return ViCommConst.FLAG_ON_STR.Equals(iBridUtil.GetStringValue(pDelFlag)) ? "return confirm('削除の取消を実行しますか？');" : "return confirm('削除を実行しますか？');";
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.grdBbsThread.PageIndex = 0;
		if (chkPagingOff.Checked) {
			this.grdBbsThread.PageSize = 99999;
		} else {
			this.grdBbsThread.PageSize = 20;
		}

		this.pnlInfo.Visible = true;
		this.grdBbsThread.DataSourceID = "dsBbsThread";
		this.grdBbsThread.DataBind();
		this.pnlCount.DataBind();
	}

	private void GetData() {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("BBS_THREAD_GET");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pUSER_SEQ", DbSession.DbType.VARCHAR2, this.UserSeq);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO", DbSession.DbType.VARCHAR2, this.UserCharNo);
			oDbSession.ProcedureInParm("pBBS_THREAD_SEQ", DbSession.DbType.VARCHAR2, this.BbsThreadSeq);
			oDbSession.ProcedureOutParm("pBBS_THREAD_HANDLE_NM", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pBBS_THREAD_TITLE", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutArrayParm("pBBS_THREAD_DOC", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pADMIN_THREAD_FLAG", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTHREAD_TYPE", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pDEL_FLAG", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pREVISION_NO", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pRECORD_COUNT", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			if (int.Parse(oDbSession.GetStringValue("pRECORD_COUNT")) > 0) {
				this.BbsThreadHandleNm = oDbSession.GetStringValue("pBBS_THREAD_HANDLE_NM");
				this.DelFlag = oDbSession.GetStringValue("pDEL_FLAG");
				this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");

				this.lstSiteCd.SelectedValue = this.lstSeekSiteCd.SelectedValue;

				StringBuilder oThreadDocBuilder = new StringBuilder();
				for (int i = 0; i < 4; i++) {
					oThreadDocBuilder.Append(oDbSession.GetArryStringValue("pBBS_THREAD_DOC", i));
				}
				this.txtDoc.Text = oThreadDocBuilder.ToString();
				this.txtTitle.Text = oDbSession.GetStringValue("pBBS_THREAD_TITLE");
				this.chkAdminThread.Checked = ViCommConst.FLAG_OFF_STR.Equals(oDbSession.GetStringValue("pADMIN_THREAD_FLAG")) ? false : true;
				this.lstThreadType.SelectedValue = oDbSession.GetStringValue("pTHREAD_TYPE");
			}
		}
	}

	private void UpdateData() {
		if (!this.IsValid) {
			return;
		}

		string[] oThreadDocArray = SysPrograms.SplitBytes(Encoding.GetEncoding("UTF-8"), this.txtDoc.Text.Trim(), 3000);

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("BBS_THREAD_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pUSER_SEQ", DbSession.DbType.VARCHAR2, this.UserSeq);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO", DbSession.DbType.VARCHAR2, this.txtCharNo.Text);
			oDbSession.ProcedureBothParm("pBBS_THREAD_SEQ", DbSession.DbType.VARCHAR2, this.BbsThreadSeq);
			oDbSession.ProcedureInParm("pBBS_THREAD_HANDLE_NM", DbSession.DbType.VARCHAR2, this.BbsThreadHandleNm);
			oDbSession.ProcedureInParm("pBBS_THREAD_TITLE", DbSession.DbType.VARCHAR2, this.txtTitle.Text.Trim());
			oDbSession.ProcedureInArrayParm("pBBS_THREAD_DOC", DbSession.DbType.VARCHAR2, oThreadDocArray);
			oDbSession.ProcedureInParm("pADMIN_THREAD_FLAG", DbSession.DbType.VARCHAR2, this.chkAdminThread.Checked ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR);
			oDbSession.ProcedureInParm("pTHREAD_TYPE", DbSession.DbType.NUMBER, this.lstThreadType.SelectedValue);
			oDbSession.ProcedureInParm("pDEL_FLAG", DbSession.DbType.NUMBER, this.DelFlag);
			oDbSession.ProcedureInParm("pREVISION_NO", DbSession.DbType.NUMBER, this.RevisionNo);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.GetList();
		this.GetData();
	}

	private void UpdateDelFlag(int pDelFlag) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("BBS_THREAD_DEL_FLAG_UPDATE");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pUSER_SEQ", DbSession.DbType.NUMBER, this.UserSeq);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO", DbSession.DbType.VARCHAR2, this.UserCharNo);
			oDbSession.ProcedureInParm("pBBS_THREAD_SEQ", DbSession.DbType.NUMBER, this.BbsThreadSeq);
			oDbSession.ProcedureInParm("pDEL_FLAG", DbSession.DbType.NUMBER, pDelFlag);
			oDbSession.ProcedureInParm("pREVISION_NO", DbSession.DbType.NUMBER, this.RevisionNo);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.GetList();
	}
}
