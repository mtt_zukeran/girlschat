<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SocialGameList.aspx.cs" Inherits="Extension_SocialGameList"
	Title="ゲーム基本設定" ValidateRequest="false" %>

<%@ Import Namespace="System.Data" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="ゲーム基本設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset>
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[ゲーム基本設定]</legend>
						<table border="0" style="width: 800px; margin-bottom: 10px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									男性ｹﾞｰﾑｻｰﾋﾞｽﾎﾟｲﾝﾄ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtManGameServicePoint" runat="server" MaxLength="8" Width="70"></asp:TextBox>P
									<asp:RequiredFieldValidator ID="vdrManGameServicePoint" runat="server" ErrorMessage="男性ｹﾞｰﾑｻｰﾋﾞｽﾎﾟｲﾝﾄを入力してください。" ControlToValidate="txtManGameServicePoint"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrManGameServicePoint" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtManGameServicePoint" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									女性ｹﾞｰﾑｻｰﾋﾞｽﾎﾟｲﾝﾄ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCastGameServicePoint" runat="server" MaxLength="8" Width="70"></asp:TextBox>P
									<asp:RequiredFieldValidator ID="vdrCastGameServicePoint" runat="server" ErrorMessage="女性ｹﾞｰﾑｻｰﾋﾞｽﾎﾟｲﾝﾄを入力してください。" ControlToValidate="txtCastGameServicePoint"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrCastGameServicePoint" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtCastGameServicePoint" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									男性ﾐｯｼｮﾝ用部隊初期数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtManInitialMissionForce" runat="server" MaxLength="3" Width="30"></asp:TextBox>人
									<asp:RequiredFieldValidator ID="vdrManInitialMissionForce" runat="server" ErrorMessage="男性ﾐｯｼｮﾝ用部隊初期数を入力してください。" ControlToValidate="txtManInitialMissionForce"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrManInitialMissionForce" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtManInitialMissionForce" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									男性攻撃用部隊初期数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtManInitialAttackForce" runat="server" MaxLength="3" Width="30"></asp:TextBox>人
									<asp:RequiredFieldValidator ID="vdrManInitialAttackForce" runat="server" ErrorMessage="男性攻撃用部隊初期数を入力してください。" ControlToValidate="txtManInitialAttackForce"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdrManInitialAttackForce" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtManInitialAttackForce" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									男性防御用部隊初期数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtManInitialDefenceForce" runat="server" MaxLength="3" Width="30"></asp:TextBox>人
									<asp:RequiredFieldValidator ID="vdrManInitialDefenceForce" runat="server" ErrorMessage="男性防御用部隊初期数を入力してください。" ControlToValidate="txtManInitialDefenceForce"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdrManInitialDefenceForce" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtManInitialDefenceForce" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									女性ﾐｯｼｮﾝ用部隊初期数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCastInitialMissionForce" runat="server" MaxLength="3" Width="30"></asp:TextBox>人
									<asp:RequiredFieldValidator ID="vdrCastInitialMissionForce" runat="server" ErrorMessage="女性ﾐｯｼｮﾝ用部隊初期数を入力してください。" ControlToValidate="txtCastInitialMissionForce"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdrCastInitialMissionForce" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtCastInitialMissionForce" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									女性攻撃用部隊初期数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCastInitialAttackForce" runat="server" MaxLength="3" Width="30"></asp:TextBox>人
									<asp:RequiredFieldValidator ID="vdrCastInitialAttackForce" runat="server" ErrorMessage="女性攻撃用部隊初期数を入力してください。" ControlToValidate="txtCastInitialAttackForce"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vdrCastInitialAttackForce" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtCastInitialAttackForce" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									女性防御用部隊初期数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCastInitialDefenceForce" runat="server" MaxLength="3" Width="30"></asp:TextBox>人
									<asp:RequiredFieldValidator ID="vdrCastInitialDefenceForce" runat="server" ErrorMessage="女性防御用部隊初期数を入力してください。" ControlToValidate="txtCastInitialDefenceForce"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8" TargetControlID="vdrCastInitialDefenceForce" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtCastInitialDefenceForce" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									仲間追加時増加部隊数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtAddFellowForce" runat="server" MaxLength="3" Width="30"></asp:TextBox>人加算される
									<asp:RequiredFieldValidator ID="vdrAddFellowForce" runat="server" ErrorMessage="仲間追加時増加部隊数を入力してください。" ControlToValidate="txtAddFellowForce" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender38" TargetControlID="vdrAddFellowForce" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender37" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtAddFellowForce" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									仲間解除時減少部隊数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtRemoveFellowForce" runat="server" MaxLength="3" Width="30"></asp:TextBox>人減算される(解除した側)
									<asp:RequiredFieldValidator ID="vdrRemoveFellowForce" runat="server" ErrorMessage="仲間解除時減少部隊数を入力してください。" ControlToValidate="txtRemoveFellowForce" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender36" TargetControlID="vdrRemoveFellowForce" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender35" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtRemoveFellowForce" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									仲間解除時減少部隊数<br />
									(相手)
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtRemoveFellowPartnerForce" runat="server" MaxLength="3" Width="30"></asp:TextBox>人減算される(解除された側)
									<asp:RequiredFieldValidator ID="vdrRemoveFellowPartnerForce" runat="server" ErrorMessage="仲間解除時減少部隊数(相手)を入力してください。" ControlToValidate="txtRemoveFellowPartnerForce"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender37" TargetControlID="vdrRemoveFellowPartnerForce" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender36" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtRemoveFellowPartnerForce" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									女性ﾓｻﾞ消しﾂｰﾙ報酬
								</td>
								<td class="tdDataStyle">
									ｱｯﾌﾟﾛｰﾄﾞした写真にﾓｻﾞ消しﾂｰﾙが使われた場合<asp:TextBox ID="txtMosaicErasePoint" runat="server" MaxLength="3" Width="30"></asp:TextBox>PTを報酬として付与する。
									<asp:RequiredFieldValidator ID="vdrMosaicErasePoint" runat="server" ErrorMessage="女性ﾓｻﾞ消しﾂｰﾙ報酬を入力してください。" ControlToValidate="txtMosaicErasePoint" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9" TargetControlID="vdrMosaicErasePoint" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtMosaicErasePoint" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									女性動画ﾁｹｯﾄ報酬
								</td>
								<td class="tdDataStyle">
									ｱｯﾌﾟﾛｰﾄﾞした動画に動画ﾁｹｯﾄが使われた場合<asp:TextBox ID="txtMovieTicketPoint" runat="server" MaxLength="3" Width="30"></asp:TextBox>PTを報酬として付与する。
									<asp:RequiredFieldValidator ID="vdrMovieTicketPoint" runat="server" ErrorMessage="女性動画ﾁｹｯﾄ報酬を入力してください。" ControlToValidate="txtMovieTicketPoint" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10" TargetControlID="vdrMovieTicketPoint" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtMovieTicketPoint" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									女性ﾌﾟﾚｾﾞﾝﾄ報酬
								</td>
								<td class="tdDataStyle">
									課金ｱｲﾃﾑﾌﾟﾚｾﾞﾝﾄ時、ｱｲﾃﾑ価格の<asp:TextBox ID="txtPresentPointRate" runat="server" MaxLength="3" Width="30"></asp:TextBox>％のPTを報酬として付与する。
									<asp:RequiredFieldValidator ID="vdrPresentPointRate" runat="server" ErrorMessage="女性ﾌﾟﾚｾﾞﾝﾄ報酬を入力してください。" ControlToValidate="txtPresentPointRate" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11" TargetControlID="vdrPresentPointRate" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPresentPointRate" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									男性お宝DROP率
								</td>
								<td class="tdDataStyle">
									ﾐｯｼｮﾝｸﾘｱ時に<asp:TextBox ID="txtManTreasureDropRate" runat="server" MaxLength="3" Width="30"></asp:TextBox>％の確率でｱｲﾃﾑを獲得できる。
									<asp:RequiredFieldValidator ID="vdrManTreasureDropRate" runat="server" ErrorMessage="男性お宝DROP率を入力してください。" ControlToValidate="txtManTreasureDropRate" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender12" TargetControlID="vdrManTreasureDropRate" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtManTreasureDropRate" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									女性お宝DROP率
								</td>
								<td class="tdDataStyle">
									ﾐｯｼｮﾝｸﾘｱ時に<asp:TextBox ID="txtCastTreasureDropRate" runat="server" MaxLength="3" Width="30"></asp:TextBox>％の確率でｱｲﾃﾑを獲得できる。
									<asp:RequiredFieldValidator ID="vdrCastTreasureDropRate" runat="server" ErrorMessage="女性お宝DROP率を入力してください。" ControlToValidate="txtCastTreasureDropRate"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender13" TargetControlID="vdrCastTreasureDropRate" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtCastTreasureDropRate" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									通常ﾊﾞﾄﾙ同一ﾕｰｻﾞｰ<br />
									攻撃制限回数
								</td>
								<td class="tdDataStyle">
									通常ﾊﾞﾄﾙ時、同一ﾕｰｻﾞｰには1日<asp:TextBox ID="txtSameUserAttackLimitCount" runat="server" MaxLength="2" Width="30"></asp:TextBox>回まで攻撃可能。
									<asp:RequiredFieldValidator ID="vdrSameUserAttackLimitCount" runat="server" ErrorMessage="通常ﾊﾞﾄﾙ同一ﾕｰｻﾞｰ攻撃制限数を入力してください。" ControlToValidate="txtSameUserAttackLimitCount"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender27" TargetControlID="vdrSameUserAttackLimitCount" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender26" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtSameUserAttackLimitCount" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾊﾟｰﾃｨﾊﾞﾄﾙ攻撃制限回数
								</td>
								<td class="tdDataStyle">
									ﾊﾟｰﾃｨﾊﾞﾄﾙは1日<asp:TextBox ID="txtPtBattleAttackLimitCount" runat="server" MaxLength="2" Width="30"></asp:TextBox>回まで攻撃可能。
									<asp:RequiredFieldValidator ID="vdrPtBattleAttackLimitCount" runat="server" ErrorMessage="ﾊﾟｰﾃｨﾊﾞﾄﾙ攻撃制限数を入力してください。" ControlToValidate="txtPtBattleAttackLimitCount"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender28" TargetControlID="vdrPtBattleAttackLimitCount" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender27" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPtBattleAttackLimitCount" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾊﾟｰﾃｨﾊﾞﾄﾙ攻撃間隔
								</td>
								<td class="tdDataStyle">
									ﾊﾟｰﾃｨﾊﾞﾄﾙは攻撃後<asp:TextBox ID="txtPtBattleAttackIntervalMin" runat="server" MaxLength="3" Width="30"></asp:TextBox>分間攻撃できない。
									<asp:RequiredFieldValidator ID="vdrPtBattleAttackIntervalMin" runat="server" ErrorMessage="ﾊﾟｰﾃｨﾊﾞﾄﾙ攻撃間隔を入力してください。" ControlToValidate="txtPtBattleAttackIntervalMin"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender29" TargetControlID="vdrPtBattleAttackIntervalMin" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender28" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPtBattleAttackIntervalMin" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									防御時部隊数減少率
								</td>
								<td class="tdDataStyle">
									ﾊﾞﾄﾙ防御時に<asp:TextBox ID="txtDefFroceDecreaseRateMin" runat="server" MaxLength="3" Width="30"></asp:TextBox>％〜
									<asp:TextBox ID="txtDefFroceDecreaseRateMax" runat="server" MaxLength="3" Width="30"></asp:TextBox>％だけ防御部隊数が減少する。
									<asp:RequiredFieldValidator ID="vdrDefFroceDecreaseRateMin" runat="server" ErrorMessage="防御時部隊数減少率を入力してください。" ControlToValidate="txtDefFroceDecreaseRateMin"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender14" TargetControlID="vdrDefFroceDecreaseRateMin" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtDefFroceDecreaseRateMin" />
									<asp:RequiredFieldValidator ID="vdrDefFroceDecreaseRateMax" runat="server" ErrorMessage="防御時部隊数減少率を入力してください。" ControlToValidate="txtDefFroceDecreaseRateMax"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender15" TargetControlID="vdrDefFroceDecreaseRateMin" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtDefFroceDecreaseRateMax" />
									<asp:CompareValidator ID="vdcDefFroceDecreaseRate" runat="server" ControlToValidate="txtDefFroceDecreaseRateMax" ControlToCompare="txtDefFroceDecreaseRateMin"
										Operator="GreaterThanEqual" Type="Integer" ErrorMessage="減少率の大小関係が不正です" ValidationGroup="Update">*</asp:CompareValidator>
									<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18" runat="Server" HighlightCssClass="validatorCallout" TargetControlID="vdcDefFroceDecreaseRate">
									</ajaxToolkit:ValidatorCalloutExtender>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									攻撃側ｱｲﾃﾑ耐久度減少数
								</td>
								<td class="tdDataStyle">
									攻撃後に<asp:TextBox ID="txtBattleDecreaseEndurance" runat="server" MaxLength="3" Width="30"></asp:TextBox>種類のｱｲﾃﾑの耐久度が1減少する
									<asp:RequiredFieldValidator ID="vdrBattleDecreaseEndurance" runat="server" ErrorMessage="攻撃側ｱｲﾃﾑ耐久度減少数を入力してください。" ControlToValidate="txtBattleDecreaseEndurance"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender42" TargetControlID="vdrBattleDecreaseEndurance" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender41" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtBattleDecreaseEndurance" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									通常ﾊﾞﾄﾙ時<br />
									警察出現条件
								</td>
								<td class="tdDataStyle">
									攻撃力が防御力の<asp:TextBox ID="txtPoliceComeRate" runat="server" MaxLength="4" Width="40"></asp:TextBox>％以上だったら警察が出現する。<br />
									ただし、攻撃側と防御側のﾚﾍﾞﾙ差が<asp:TextBox ID="txtPoliceLvDifference" runat="server" MaxLength="4" Width="40"></asp:TextBox>以上の場合に限り、<br />
									攻撃力が防御力の<asp:TextBox ID="txtPoliceComeRateLvDefference" runat="server" MaxLength="4" Width="40"></asp:TextBox>％以上だったら警察が出現する。
									<asp:RequiredFieldValidator ID="vdrPoliceComeRate" runat="server" ErrorMessage="警察出現条件を入力してください。" ControlToValidate="txtPoliceComeRate" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender16" TargetControlID="vdrPoliceComeRate" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPoliceComeRate" />
									<asp:RequiredFieldValidator ID="vdrPoliceLvDifference" runat="server" ErrorMessage="警察出現条件を入力してください。" ControlToValidate="txtPoliceLvDifference" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender17" TargetControlID="vdrPoliceLvDifference" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPoliceLvDifference" />
									<asp:RequiredFieldValidator ID="vdrPoliceComeRateLvDefference" runat="server" ErrorMessage="警察出現条件を入力してください。" ControlToValidate="txtPoliceComeRateLvDefference"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender19" TargetControlID="vdrPoliceComeRateLvDefference" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPoliceComeRateLvDefference" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾊﾟｰﾃｨﾊﾞﾄﾙ時<br />
									警察出現条件
								</td>
								<td class="tdDataStyle">
									攻撃力が防御力の<asp:TextBox ID="txtPtPoliceComeRate" runat="server" MaxLength="4" Width="40"></asp:TextBox>％以上だったら警察が出現する。<br />
									ただし、攻撃側と防御側のﾚﾍﾞﾙ差が<asp:TextBox ID="txtPtPoliceLvDifference" runat="server" MaxLength="4" Width="40"></asp:TextBox>以上の場合に限り、<br />
									攻撃力が防御力の<asp:TextBox ID="txtPtPoliceComeRateLvDefference" runat="server" MaxLength="4" Width="40"></asp:TextBox>％以上だったら警察が出現する。
									<asp:RequiredFieldValidator ID="vdrPtPoliceComeRate" runat="server" ErrorMessage="警察出現条件を入力してください。" ControlToValidate="txtPtPoliceComeRate" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender39" TargetControlID="vdrPtPoliceComeRate" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender38" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPtPoliceComeRate" />
									<asp:RequiredFieldValidator ID="vdrPtPoliceLvDifference" runat="server" ErrorMessage="警察出現条件を入力してください。" ControlToValidate="txtPtPoliceLvDifference" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender40" TargetControlID="vdrPtPoliceLvDifference" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender39" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPtPoliceLvDifference" />
									<asp:RequiredFieldValidator ID="vdrPtPoliceComeRateLvDefference" runat="server" ErrorMessage="警察出現条件を入力してください。" ControlToValidate="txtPtPoliceComeRateLvDefference"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender41" TargetControlID="vdrPtPoliceComeRateLvDefference" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender40" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPtPoliceComeRateLvDefference" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									応援ﾊﾞﾄﾙ時<br />
									警察出現条件
								</td>
								<td class="tdDataStyle">
									攻撃力が防御力の<asp:TextBox ID="txtSuppPoliceComeRate" runat="server" MaxLength="4" Width="40"></asp:TextBox>％以上だったら警察が出現する。<br />
									ただし、攻撃側と防御側のﾚﾍﾞﾙ差が<asp:TextBox ID="txtSuppPoliceLvDifference" runat="server" MaxLength="4" Width="40"></asp:TextBox>以上の場合に限り、<br />
									攻撃力が防御力の<asp:TextBox ID="txtSuppPoliceComeRateLvDefference" runat="server" MaxLength="4" Width="40"></asp:TextBox>％以上だったら警察が出現する。
									<asp:RequiredFieldValidator ID="vdrSuppPoliceComeRate" runat="server" ErrorMessage="警察出現条件を入力してください。" ControlToValidate="txtSuppPoliceComeRate" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender57" TargetControlID="vdrSuppPoliceComeRate" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender56" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtSuppPoliceComeRate" />
									<asp:RequiredFieldValidator ID="vdrSuppPoliceLvDifference" runat="server" ErrorMessage="警察出現条件を入力してください。" ControlToValidate="txtSuppPoliceLvDifference"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender58" TargetControlID="vdrSuppPoliceLvDifference" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender57" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtSuppPoliceLvDifference" />
									<asp:RequiredFieldValidator ID="vdrSuppPoliceComeRateLvDefference" runat="server" ErrorMessage="警察出現条件を入力してください。" ControlToValidate="txtSuppPoliceComeRateLvDefference"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender59" TargetControlID="vdrSuppPoliceComeRateLvDefference" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender58" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtSuppPoliceComeRateLvDefference" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									通常ﾊﾞﾄﾙ勝利経験値
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtBattleWinExp" runat="server" MaxLength="3" Width="30"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrBattleWinExp" runat="server" ErrorMessage="通常ﾊﾞﾄﾙ勝利経験値を入力してください。" ControlToValidate="txtBattleWinExp" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender32" TargetControlID="vdrBattleWinExp" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender31" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtBattleWinExp" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									通常ﾊﾞﾄﾙ敗北経験値
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtBattleLossExp" runat="server" MaxLength="3" Width="30"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrBattleLossExp" runat="server" ErrorMessage="通常ﾊﾞﾄﾙ敗北経験値を入力してください。" ControlToValidate="txtBattleLossExp" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender33" TargetControlID="vdrBattleLossExp" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender32" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtBattleLossExp" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾊﾟｰﾃｨﾊﾞﾄﾙ勝利経験値
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtPartyBattleWinExp" runat="server" MaxLength="3" Width="30"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrPartyBattleWinExp" runat="server" ErrorMessage="ﾊﾟｰﾃｨﾊﾞﾄﾙ勝利経験値を入力してください。" ControlToValidate="txtPartyBattleWinExp" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender34" TargetControlID="vdrPartyBattleWinExp" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender33" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPartyBattleWinExp" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾊﾟｰﾃｨﾊﾞﾄﾙ敗北経験値
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtPartyBattleLossExp" runat="server" MaxLength="3" Width="30"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrPartyBattleLossExp" runat="server" ErrorMessage="ﾊﾟｰﾃｨﾊﾞﾄﾙ敗北経験値を入力してください。" ControlToValidate="txtPartyBattleLossExp"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender35" TargetControlID="vdrPartyBattleLossExp" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender34" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPartyBattleLossExp" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									応援ﾊﾞﾄﾙ勝利親密ﾎﾟｲﾝﾄ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtSupportBattleWinFriendly" runat="server" MaxLength="3" Width="30"></asp:TextBox>P
									<asp:RequiredFieldValidator ID="vdrSupportBattleWinFriendly" runat="server" ErrorMessage="応援ﾊﾞﾄﾙ勝利親密ﾎﾟｲﾝﾄを入力してください。" ControlToValidate="txtSupportBattleWinFriendly"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender43" TargetControlID="vdrSupportBattleWinFriendly" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender42" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtSupportBattleWinFriendly" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									応援ﾊﾞﾄﾙ敗北親密ﾎﾟｲﾝﾄ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtSupportBattleLossFriendly" runat="server" MaxLength="3" Width="30"></asp:TextBox>P
									<asp:RequiredFieldValidator ID="vdrSupportBattleLossFriendly" runat="server" ErrorMessage="応援ﾊﾞﾄﾙ敗北親密ﾎﾟｲﾝﾄを入力してください。" ControlToValidate="txtSupportBattleLossFriendly"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender44" TargetControlID="vdrSupportBattleLossFriendly" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender43" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtSupportBattleLossFriendly" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									応援ﾊﾞﾄﾙ勝利連携ﾎﾟｲﾝﾄ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtSupportBattleWinCoop" runat="server" MaxLength="3" Width="30"></asp:TextBox>P
									<asp:RequiredFieldValidator ID="vdrSupportBattleWinCoop" runat="server" ErrorMessage="応援ﾊﾞﾄﾙ勝利連携ﾎﾟｲﾝﾄを入力してください。" ControlToValidate="txtSupportBattleWinCoop"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender45" TargetControlID="vdrSupportBattleWinCoop" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender44" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtSupportBattleWinCoop" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									応援ﾊﾞﾄﾙ敗北連携ﾎﾟｲﾝﾄ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtSupportBattleLossCoop" runat="server" MaxLength="3" Width="30"></asp:TextBox>P
									<asp:RequiredFieldValidator ID="vdrSupportBattleLossCoop" runat="server" ErrorMessage="応援ﾊﾞﾄﾙ敗北連携ﾎﾟｲﾝﾄを入力してください。" ControlToValidate="txtSupportBattleLossCoop"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender46" TargetControlID="vdrSupportBattleLossCoop" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender45" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtSupportBattleLossCoop" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									応援ﾊﾞﾄﾙ申請数
								</td>
								<td class="tdDataStyle">
									ﾊﾞﾄﾙ敗北時に応援を求めた時に<asp:TextBox ID="txtSupportRequestCount" runat="server" MaxLength="2" Width="30"></asp:TextBox>人の仲間に応援申請を送る。
									<asp:RequiredFieldValidator ID="vdrSupportRequestCount" runat="server" ErrorMessage="応援ﾊﾞﾄﾙ申請数を入力してください。" ControlToValidate="txtSupportRequestCount" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender47" TargetControlID="vdrSupportRequestCount" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender46" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtSupportRequestCount" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									お宝獲得時親密ﾎﾟｲﾝﾄ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtGetTreasureFrinedlyPoint" runat="server" MaxLength="3" Width="30"></asp:TextBox>P
									<asp:RequiredFieldValidator ID="vdrGetTreasureFrinedlyPoint" runat="server" ErrorMessage="お宝獲得時親密ﾎﾟｲﾝﾄを入力してください。" ControlToValidate="txtGetTreasureFrinedlyPoint"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender20" TargetControlID="vdrGetTreasureFrinedlyPoint" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtGetTreasureFrinedlyPoint" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾊｸﾞ時親密ﾎﾟｲﾝﾄ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtHugFriendlyPoint" runat="server" MaxLength="3" Width="30"></asp:TextBox>P
									<asp:RequiredFieldValidator ID="vdrHugFriendlyPoint" runat="server" ErrorMessage="ﾊｸﾞ時親密ﾎﾟｲﾝﾄを入力してください。" ControlToValidate="txtHugFriendlyPoint" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender21" TargetControlID="vdrHugFriendlyPoint" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtHugFriendlyPoint" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾊｸﾞ時親密ﾎﾟｲﾝﾄ(仲間)
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtHugFriendlyPointByFellow" runat="server" MaxLength="3" Width="30"></asp:TextBox>P
									<asp:RequiredFieldValidator ID="vdrHugFriendlyPointByFellow" runat="server" ErrorMessage="ﾊｸﾞ時親密ﾎﾟｲﾝﾄ(仲間)を入力してください。" ControlToValidate="txtHugFriendlyPointByFellow"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender22" TargetControlID="vdrHugFriendlyPointByFellow" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtHugFriendlyPointByFellow" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾊｸﾞ時連携ﾎﾟｲﾝﾄ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtHugCooperationPoint" runat="server" MaxLength="3" Width="30"></asp:TextBox>P
									<asp:RequiredFieldValidator ID="vdrHugCooperationPoint" runat="server" ErrorMessage="ﾊｸﾞ時連携ﾎﾟｲﾝﾄを入力してください。" ControlToValidate="txtHugCooperationPoint"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender23" TargetControlID="vdrHugCooperationPoint" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtHugCooperationPoint" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾊｸﾞされた時経験値
								</td>
								<td class="tdDataStyle">
									仲間からﾊｸﾞされた時に<asp:TextBox ID="txtHuggedAddExpByFellow" runat="server" MaxLength="3" Width="30"></asp:TextBox>経験値を加算する。
									<asp:RequiredFieldValidator ID="vdrHuggedAddExpByFellow" runat="server" ErrorMessage="ﾊｸﾞ時された時経験値を入力してください。" ControlToValidate="txtHuggedAddExpByFellow"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender24" TargetControlID="vdrHuggedAddExpByFellow" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender23" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtHuggedAddExpByFellow" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾊｸﾞﾎﾞｰﾅｽ加算率
								</td>
								<td class="tdDataStyle">
									ﾊｸﾞﾎﾞｰﾅｽ中のﾐｯｼｮﾝ取得報酬を<asp:TextBox ID="txtHugBonusRate" runat="server" MaxLength="3" Width="30"></asp:TextBox>％加算する。
									<asp:RequiredFieldValidator ID="vdrHugBonusRate" runat="server" ErrorMessage="ﾊｸﾞﾎﾞｰﾅｽ加算率を入力してください。" ControlToValidate="txtHugBonusRate" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender48" TargetControlID="vdrHugBonusRate" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender47" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtHugBonusRate" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾌﾟﾚｾﾞﾝﾄ時親密ﾎﾟｲﾝﾄ率
								</td>
								<td class="tdDataStyle">
									ﾌﾟﾚｾﾞﾝﾄ価格の<asp:TextBox ID="txtPresentFriendlyPtRate" runat="server" MaxLength="3" Width="30"></asp:TextBox>％加算する。
									<asp:RequiredFieldValidator ID="vdrPresentFriendlyPtRate" runat="server" ErrorMessage="ﾌﾟﾚｾﾞﾝﾄ時親密ﾎﾟｲﾝﾄ率を入力してください。" ControlToValidate="txtPresentFriendlyPtRate"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender49" TargetControlID="vdrPresentFriendlyPtRate" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender48" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPresentFriendlyPtRate" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾌﾟﾚｾﾞﾝﾄ時親密ﾎﾟｲﾝﾄ率<br />
									(仲間)
								</td>
								<td class="tdDataStyle">
									ﾌﾟﾚｾﾞﾝﾄ価格の<asp:TextBox ID="txtPresentFriendlyPtRateByFellow" runat="server" MaxLength="3" Width="30"></asp:TextBox>％加算する。
									<asp:RequiredFieldValidator ID="vdrPresentFriendlyPtRateByFellow" runat="server" ErrorMessage="ﾌﾟﾚｾﾞﾝﾄ時親密ﾎﾟｲﾝﾄ率(仲間)を入力してください。" ControlToValidate="txtPresentFriendlyPtRateByFellow"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender50" TargetControlID="vdrPresentFriendlyPtRateByFellow" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender49" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPresentFriendlyPtRateByFellow" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾌﾟﾚｾﾞﾝﾄお返し時<br />
									親密ﾎﾟｲﾝﾄ率
								</td>
								<td class="tdDataStyle">
									ﾌﾟﾚｾﾞﾝﾄ価格の<asp:TextBox ID="txtReturnFriendlyPtRate" runat="server" MaxLength="3" Width="30"></asp:TextBox>％加算する。
									<asp:RequiredFieldValidator ID="vdrReturnFriendlyPtRate" runat="server" ErrorMessage="ﾌﾟﾚｾﾞﾝﾄお返し時親密ﾎﾟｲﾝﾄ率を入力してください。" ControlToValidate="txtReturnFriendlyPtRate"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender53" TargetControlID="vdrReturnFriendlyPtRate" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender52" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtReturnFriendlyPtRate" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾌﾟﾚｾﾞﾝﾄお返し時<br />
									親密ﾎﾟｲﾝﾄ率(仲間)
								</td>
								<td class="tdDataStyle">
									ﾌﾟﾚｾﾞﾝﾄ価格の<asp:TextBox ID="txtReturnFriendlyPtRateByFellow" runat="server" MaxLength="3" Width="30"></asp:TextBox>％加算する。
									<asp:RequiredFieldValidator ID="vdrReturnFriendlyPtRateByFellow" runat="server" ErrorMessage="ﾌﾟﾚｾﾞﾝﾄお返し時親密ﾎﾟｲﾝﾄ率(仲間)を入力してください。" ControlToValidate="txtReturnFriendlyPtRateByFellow"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender54" TargetControlID="vdrReturnFriendlyPtRateByFellow" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender53" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtReturnFriendlyPtRateByFellow" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾌﾟﾚｾﾞﾝﾄ時連携ﾎﾟｲﾝﾄ率
								</td>
								<td class="tdDataStyle">
									ﾌﾟﾚｾﾞﾝﾄ価格の<asp:TextBox ID="txtPresentCoopPtRate" runat="server" MaxLength="3" Width="30"></asp:TextBox>％加算する。
									<asp:RequiredFieldValidator ID="vdrPresentCoopPtRate" runat="server" ErrorMessage="ﾌﾟﾚｾﾞﾝﾄ時連携ﾎﾟｲﾝﾄ率を入力してください。" ControlToValidate="txtPresentCoopPtRate"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender51" TargetControlID="vdrPresentCoopPtRate" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender50" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPresentCoopPtRate" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾌﾟﾚｾﾞﾝﾄ時連携ﾎﾟｲﾝﾄ率<br />
									(仲間)
								</td>
								<td class="tdDataStyle">
									ﾌﾟﾚｾﾞﾝﾄ価格の<asp:TextBox ID="txtPresentCoopPtRateByFellow" runat="server" MaxLength="3" Width="30"></asp:TextBox>％加算する。
									<asp:RequiredFieldValidator ID="vdrPresentCoopPtRateByFellow" runat="server" ErrorMessage="ﾌﾟﾚｾﾞﾝﾄ時連携ﾎﾟｲﾝﾄ率(仲間)を入力してください。" ControlToValidate="txtPresentCoopPtRateByFellow"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender52" TargetControlID="vdrPresentCoopPtRateByFellow" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender51" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPresentCoopPtRateByFellow" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									非課金ﾌﾟﾚｾﾞﾝﾄ時連携ﾎﾟｲﾝﾄ率
								</td>
								<td class="tdDataStyle">
									ﾌﾟﾚｾﾞﾝﾄ価格の10000分の<asp:TextBox ID="txtPresentCoopPtRateNotCharge" runat="server" MaxLength="3" Width="30"></asp:TextBox>を加算する。
									<asp:RequiredFieldValidator ID="vdrPresentCoopPtRateNotCharge" runat="server" ErrorMessage="ﾌﾟﾚｾﾞﾝﾄ時連携ﾎﾟｲﾝﾄ率を入力してください。" ControlToValidate="txtPresentCoopPtRateNotCharge"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender62" TargetControlID="vdrPresentCoopPtRateNotCharge" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender61" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPresentCoopPtRateNotCharge" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									非課金ﾌﾟﾚｾﾞﾝﾄ時連携ﾎﾟｲﾝﾄ率<br />
									(仲間)
								</td>
								<td class="tdDataStyle">
									ﾌﾟﾚｾﾞﾝﾄ価格の10000分の<asp:TextBox ID="txtPresentCoopPtRateByFellowNotCharge" runat="server" MaxLength="3" Width="30"></asp:TextBox>を加算する。
									<asp:RequiredFieldValidator ID="vdrPresentCoopPtRateByFellowNotCharge" runat="server" ErrorMessage="ﾌﾟﾚｾﾞﾝﾄ時連携ﾎﾟｲﾝﾄ率(仲間)を入力してください。" ControlToValidate="txtPresentCoopPtRateByFellowNotCharge"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender63" TargetControlID="vdrPresentCoopPtRateByFellowNotCharge" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender62" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPresentCoopPtRateByFellowNotCharge" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									部隊回復消費連携ﾎﾟｲﾝﾄ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtForceRecoveryCoopPoint" runat="server" MaxLength="3" Width="30"></asp:TextBox>P
									<asp:RequiredFieldValidator ID="vdrForceRecoveryCoopPoint" runat="server" ErrorMessage="部隊回復消費連携ﾎﾟｲﾝﾄを入力してください。" ControlToValidate="txtForceRecoveryCoopPoint"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender25" TargetControlID="vdrForceRecoveryCoopPoint" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender24" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtForceRecoveryCoopPoint" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾃﾞｰﾄ制限回数
								</td>
								<td class="tdDataStyle">
									1日<asp:TextBox ID="txtDateLimitCount" runat="server" MaxLength="2" Width="30"></asp:TextBox>回まで実行可能。
									<asp:RequiredFieldValidator ID="vdrDateLimitCount" runat="server" ErrorMessage="ﾃﾞｰﾄ制限回数を入力してください。" ControlToValidate="txtDateLimitCount" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender30" TargetControlID="vdrDateLimitCount" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender29" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtDateLimitCount" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾊﾞｲﾄ制限回数
								</td>
								<td class="tdDataStyle">
									1日<asp:TextBox ID="txtPartTimeJobLimitCount" runat="server" MaxLength="2" Width="30"></asp:TextBox>回まで実行可能。
									<asp:RequiredFieldValidator ID="vdrPartTimeJobLimitCount" runat="server" ErrorMessage="ﾊﾞｲﾄ制限回数を入力してください。" ControlToValidate="txtPartTimeJobLimitCount"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender31" TargetControlID="vdrPartTimeJobLimitCount" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender30" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPartTimeJobLimitCount" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									親密ﾎﾟｲﾝﾄ削除日数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtFriendlyPointDelDays" runat="server" MaxLength="2" Width="30"></asp:TextBox>日経過した親密ﾎﾟｲﾝﾄは削除する。但しﾓｻﾞ消しﾂｰﾙによって増加したﾎﾟｲﾝﾄは除く。
									<asp:RequiredFieldValidator ID="vdrFriendlyPointDelDays" runat="server" ErrorMessage="親密ﾎﾟｲﾝﾄ削除日数を入力してください。" ControlToValidate="txtFriendlyPointDelDays"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender26" TargetControlID="vdrFriendlyPointDelDays" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender25" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtFriendlyPointDelDays" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ｹﾞｰﾑﾛｸﾞ削除日数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtGameLogDelDays" runat="server" MaxLength="2" Width="30"></asp:TextBox>日経過したｹﾞｰﾑﾛｸﾞは削除する。
									<asp:RequiredFieldValidator ID="vdrGameLogDelDays" runat="server" ErrorMessage="ｹﾞｰﾑﾛｸﾞ削除日数を入力してください。" ControlToValidate="txtGameLogDelDays"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender60" TargetControlID="vdrGameLogDelDays" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender59" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtGameLogDelDays" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									画像認証時獲得ﾎﾟｲﾝﾄ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtAcceptPicPoint" runat="server" MaxLength="3" Width="30"></asp:TextBox>P
									<asp:RequiredFieldValidator ID="vdrAcceptPicPoint" runat="server" ErrorMessage="画像認証時獲得ﾎﾟｲﾝﾄを入力してください。" ControlToValidate="txtAcceptPicPoint" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender55" TargetControlID="vdrAcceptPicPoint" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender54" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtAcceptPicPoint" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									動画認証時獲得ﾎﾟｲﾝﾄ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtAcceptMoviePoint" runat="server" MaxLength="3" Width="30"></asp:TextBox>P
									<asp:RequiredFieldValidator ID="vdrAcceptMoviePoint" runat="server" ErrorMessage="動画認証時獲得ﾎﾟｲﾝﾄを入力してください。" ControlToValidate="txtAcceptMoviePoint" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender56" TargetControlID="vdrAcceptMoviePoint" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender55" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtAcceptMoviePoint" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									無料ﾌﾟﾚｾﾞﾝﾄ制限回数
								</td>
								<td class="tdDataStyle">
									1日<asp:TextBox ID="txtFreePresentLimitCount" runat="server" MaxLength="3" Width="30"></asp:TextBox>回まで実行可能。
									<asp:RequiredFieldValidator ID="vdrFreePresentLimitCount" runat="server" ErrorMessage="無料ﾌﾟﾚｾﾞﾝﾄ制限回数を入力してください。" ControlToValidate="txtFreePresentLimitCount" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender61" TargetControlID="vdrFreePresentLimitCount" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender60" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtFreePresentLimitCount" />
								</td>
							</tr>
						</table>
						<asp:Button ID="btnUpdate" runat="server" CausesValidation="True" ValidationGroup="Update" CssClass="seekbutton" OnClick="btnUpdate_Click" Text="更新" OnClientClick="return confirm('更新を行いますか？');" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<asp:Panel ID="pnlList" runat="server" ScrollBars="auto">
			<fieldset>
				<legend>[サイト一覧]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count
						<%# GetRecCount() %>
					</a>
					<br />
					<a class="reccount">Current viewing page
						<%= grdSocialGame.PageIndex + 1 %>
						of
						<%= grdSocialGame.PageCount %>
					</a>
				</asp:Panel>
				&nbsp;
				<asp:Panel ID="pnlGrid" runat="server">
					<asp:GridView ID="grdSocialGame" runat="server" DataSourceID="dsSocialGame" AutoGenerateColumns="False" AllowPaging="true" AllowSorting="true" SkinID="GridViewColor">
						<Columns>
							<asp:TemplateField HeaderText="サイトコード">
								<ItemTemplate>
									<asp:LinkButton ID="lnkSiteCd" runat="server" Text='<%# Eval("SITE_CD") %>' CommandArgument='<%# Eval("SITE_CD") %>' OnCommand="lnkSiteCd_Command" CausesValidation="False">
									</asp:LinkButton>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Left" />
							</asp:TemplateField>
							<asp:BoundField DataField="UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsSocialGame" runat="server" SelectMethod="GetPageCollection" SelectCountMethod="GetPageCount" EnablePaging="true" OnSelected="dsSocialGame_Selected"
		TypeName="SocialGame"></asp:ObjectDataSource>
</asp:Content>
