﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 抽選獲得アイテム一覧

--	Progaram ID		: TownList
--
--  Creation Date	: 2011.07.25
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Ratelain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.IO;
using ViComm.Extension.Pwild;

public partial class Extension_ManTreasureDropRateList : System.Web.UI.Page {
	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string GameCharacterType {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameCharacterType"]);
		}
		set {
			this.ViewState["GameCharacterType"] = value;
		}
	}

	private List<string> GameCharacterTypeList {
		get {
			return this.ViewState["GameCharacterTypeList"] as List<string>;
		}
		set {
			this.ViewState["GameCharacterTypeList"] = value;
		}
	}

	private List<string> CastGamePicAttrSeqList {
		get {
			return this.ViewState["CastGamePicAttrSeqList"] as List<string>;
		}
		set {
			this.ViewState["CastGamePicAttrSeqList"] = value;
		}
	}

	private List<string> DropRateList {
		get {
			return this.ViewState["DropRateList"] as List<string>;
		}
		set {
			this.ViewState["DropRateList"] = value;
		}
	}

	private List<string> ScheduleDropRateList　{
		get {
			return this.ViewState["ScheduleDropRateList"] as List<string>;
		}
		set {
			this.ViewState["ScheduleDropRateList"] = value;
		}
	}
	private List<string> RevisionNoList {
		get {
			return this.ViewState["RevisionNoList"] as List<string>;
		}
		set {
			this.ViewState["RevisionNoList"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();

			this.SiteCd = iBridUtil.GetStringValue(this.Request.QueryString["sitecd"]);
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.GetList();
	}

	protected void btnBack_Click(object sender,EventArgs e) {
		this.Response.Redirect(string.Concat("~/Extension/ManTreasureAttrList.aspx"));
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		this.UpdateData();
	}

	protected void dsManTreasureDropRate_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pGameCharacterType"] = this.GameCharacterType;
	}

	protected void rptCharacterType_DataBinding(object sender,EventArgs e) {
		Repeater oRepeater = sender as Repeater;
		if (oRepeater != null) {
			switch (oRepeater.ID) {
				case "rptCharacterType1":
					this.GameCharacterType = PwViCommConst.GameCharacterType.TYPE01;
					break;
				case "rptCharacterType2":
					this.GameCharacterType = PwViCommConst.GameCharacterType.TYPE02;
					break;
				case "rptCharacterType3":
					this.GameCharacterType = PwViCommConst.GameCharacterType.TYPE03;
					break;
				default:
					return;
			}
		}
	}

	private bool IsCorrectInput() {
		this.lblErrorMessageRate.Visible = false;
		
		foreach (Repeater oRepeater in new Repeater[] { this.rptCharacterType1,this.rptCharacterType2,this.rptCharacterType3 }) {
			int iRate = 0;
			int iRateSum = 0;

			int iScheduleRate = 0;
			int iScheduleRateSum = 0;

			foreach (RepeaterItem oRepeaterItem in oRepeater.Items) {
				TextBox txtDropRate = oRepeaterItem.FindControl("txtDropRate") as TextBox;

				if (int.TryParse(txtDropRate.Text,out iRate)) {
					if (iRate < 0 || 100 < iRate) {
						this.lblErrorMessageRate.Text = "ドロップ率は0～100の範囲で入力してください。";
						this.lblErrorMessageRate.Visible = true;
						return false;
					}
				} else {
					txtDropRate.Text = "0";
				}

				iRateSum += iRate;

				if (iRateSum > 100) {
					this.lblErrorMessageRate.Text = "同一ｹﾞｰﾑｷｬﾗｸﾀｰで、今日のﾄﾞﾛｯﾌﾟ率の合計が100％以下になるよう設定してください。";
					this.lblErrorMessageRate.Visible = true;
					return false;
				}

				TextBox txtScheduleDropRate = oRepeaterItem.FindControl("txtScheduleDropRate") as TextBox;

				if (int.TryParse(txtScheduleDropRate.Text, out iScheduleRate))　{
					if (iScheduleRate < 0 || 100 < iScheduleRate) {
						this.lblErrorMessageRate.Text = "ドロップ率は0～100の範囲で入力してください。";
						this.lblErrorMessageRate.Visible = true;
						return false;
					}
				} else {
					txtScheduleDropRate.Text = "0";
				}

				iScheduleRateSum += iScheduleRate;

				if (iScheduleRateSum > 100) {
					this.lblErrorMessageRate.Text = "同一ｹﾞｰﾑｷｬﾗｸﾀｰで、予約のﾄﾞﾛｯﾌﾟ率の合計が100％以下になるよう設定してください。";
					this.lblErrorMessageRate.Visible = true;
					return false;
				}

				HiddenField hdnCastGamePicAttrSeq = oRepeaterItem.FindControl("hdnCastGamePicAttrSeq") as HiddenField;
				HiddenField hdnGameCharacterType = oRepeaterItem.FindControl("hdnGameCharacterType") as HiddenField;
				HiddenField hdnRevisionNo = oRepeaterItem.FindControl("hdnRevisionNo") as HiddenField;

				this.CastGamePicAttrSeqList.Add(hdnCastGamePicAttrSeq.Value);
				this.DropRateList.Add(txtDropRate.Text);
				this.ScheduleDropRateList.Add(txtScheduleDropRate.Text);
				this.GameCharacterTypeList.Add(hdnGameCharacterType.Value);
				this.RevisionNoList.Add(hdnRevisionNo.Value);
			}
		}

		return true;
	}

	private void InitPage() {
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void GetList() {
		this.pnlInfo.DataBind();
	}

	private void UpdateData() {
		this.CastGamePicAttrSeqList = new List<string>();
		this.DropRateList = new List<string>();
		this.ScheduleDropRateList = new List<string>();
		this.GameCharacterTypeList = new List<string>();
		this.RevisionNoList = new List<string>();

		if (!this.IsCorrectInput()) {
			return;
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("MAN_TREASURE_DROP_RATE_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInArrayParm("pCAST_GAME_PIC_ATTR_SEQ",DbSession.DbType.VARCHAR2,this.CastGamePicAttrSeqList.ToArray());
			oDbSession.ProcedureInArrayParm("pGAME_CHARACTER_TYPE",DbSession.DbType.VARCHAR2,this.GameCharacterTypeList.ToArray());
			oDbSession.ProcedureInArrayParm("pDROP_RATE", DbSession.DbType.NUMBER, this.DropRateList.ToArray());
			oDbSession.ProcedureInArrayParm("pSCHEDULE_DROP_RATE", DbSession.DbType.NUMBER, this.ScheduleDropRateList.ToArray());
			oDbSession.ProcedureInArrayParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNoList.ToArray());
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.GetList();
	}
}