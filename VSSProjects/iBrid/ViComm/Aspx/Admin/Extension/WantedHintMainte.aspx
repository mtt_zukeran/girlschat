﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="WantedHintMainte.aspx.cs" Inherits="Extension_WantedHintMainte" Title="この娘を探せ - ヒント設定"
    ValidateRequest="false" %>

<%@ Import Namespace="System.Data" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="この娘を探せ - ヒント設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset>
                <legend>[設定]</legend>
                <table border="0" style="width: 640px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle2">
                            サイトコード
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="180px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    CausesValidation="False" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" />
            </fieldset>
            <br />
        </asp:Panel>
        <asp:Panel ID="pnlList" runat="server" ScrollBars="auto">
            <fieldset>
                <legend>[ヒント設定]</legend>
                <table class="tableStyle" border="0">
                    <tr>
                        <td class="tdHeaderStyle">
                        </td>
                        <td class="tdHeaderStyle" align="center">
                            日</td>
                        <td class="tdHeaderStyle" align="center">
                            月</td>
                        <td class="tdHeaderStyle" align="center">
                            火</td>
                        <td class="tdHeaderStyle" align="center">
                            水</td>
                        <td class="tdHeaderStyle" align="center">
                            木</td>
                        <td class="tdHeaderStyle" align="center">
                            金</td>
                        <td class="tdHeaderStyle" align="center">
                            土</td>
                    </tr>
                    <tr>
                        <td class="tdDataStyle">
                            第1ヒント</td>
                        <asp:Repeater ID="rptHint1" runat="server" DataSource='<%# DummyWeekHintArray %>'
                            EnableViewState="true">
                            <ItemTemplate>
                                <td class="tdDataStyle" align="center">
                                    <asp:DropDownList ID="lstCastAttrType" runat="server" DataSourceID="dsCastAttrType"
                                        Width="100px" OnDataBound="lstCastAttrType_DataBound" DataTextField="CAST_ATTR_TYPE_NM"
                                        DataValueField="CAST_ATTR_TYPE_SEQ">
                                        <asp:ListItem Text="" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tr>
                    <tr>
                        <td class="tdDataStyle">
                            第2ヒント</td>
                        <asp:Repeater ID="rptHint2" runat="server" DataSource='<%# DummyWeekHintArray %>'
                            EnableViewState="true">
                            <ItemTemplate>
                                <td class="tdDataStyle" align="center">
                                    <asp:DropDownList ID="lstCastAttrType" runat="server" DataSourceID="dsCastAttrType"
                                        Width="100px" OnDataBound="lstCastAttrType_DataBound" DataTextField="CAST_ATTR_TYPE_NM"
                                        DataValueField="CAST_ATTR_TYPE_SEQ">
                                        <asp:ListItem Text="" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tr>
                    <tr>
                        <td class="tdDataStyle">
                            第3ヒント</td>
                        <asp:Repeater ID="rptHint3" runat="server" DataSource='<%# DummyWeekHintArray %>'
                            EnableViewState="true">
                            <ItemTemplate>
                                <td class="tdDataStyle" align="center">
                                    <asp:DropDownList ID="lstCastAttrType" runat="server" DataSourceID="dsCastAttrType"
                                        Width="100px" OnDataBound="lstCastAttrType_DataBound" DataTextField="CAST_ATTR_TYPE_NM"
                                        DataValueField="CAST_ATTR_TYPE_SEQ">
                                        <asp:ListItem Text="" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tr>
                </table>
                <asp:Panel ID="pnlCommand" runat="server">
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Update"
                        OnClick="btnUpdate_Click" />
                    <asp:CustomValidator ID="vdcAll" runat="server" ErrorMessage="" OnServerValidate="vdcAll_ServerValidate"
                        ValidationGroup="Update"></asp:CustomValidator>
                </asp:Panel>
            </fieldset>
            <br />
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsCastAttrType" runat="server" SelectMethod="GetList" TypeName="CastAttrType">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSeekSiteCd" DbType="String" PropertyName="SelectedValue"
                Name="pSiteCd" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
