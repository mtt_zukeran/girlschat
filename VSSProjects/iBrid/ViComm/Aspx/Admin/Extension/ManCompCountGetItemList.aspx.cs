﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 男性お宝規定回数コンプリート時獲得アイテム一覧

--	Progaram ID		: ManCompCountGetItemList
--
--  Creation Date	: 2011.07.25
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Ratelain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.IO;

public partial class Extension_ManCompCountGetItemList : System.Web.UI.Page {
	protected string[] DummyArray = new string[3];
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string CompCountGetItemSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["CompCountGetItemSeq"]);
		}
		set {
			this.ViewState["CompCountGetItemSeq"] = value;
		}
	}

	private string DisplayMonth {
		get {
			return iBridUtil.GetStringValue(this.ViewState["DisplayMonth"]);
		}
		set {
			this.ViewState["DisplayMonth"] = value;
		}
	}

	private string GameItemSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemSeq"]);
		}
		set {
			this.ViewState["GameItemSeq"] = value;
		}
	}

	private string GameItemCategoryType {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemCategoryType"]);
		}
		set {
			this.ViewState["GameItemCategoryType"] = value;
		}
	}

	private string CreateFlag {
		get {
			return iBridUtil.GetStringValue(this.ViewState["CreateFlag"]);
		}
		set {
			this.ViewState["CreateFlag"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	private string GameItemGetCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemGetCd"]);
		}
		set {
			this.ViewState["GameItemGetCd"] = value;
		}
	}

	private string GameItemPresent {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemPresent"]);
		}
		set {
			this.ViewState["GameItemPresent"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();

			this.SiteCd = iBridUtil.GetStringValue(this.Request.QueryString["sitecd"]);
			if (string.IsNullOrEmpty(this.SiteCd)) {
				this.SiteCd = this.lstSiteCd.SelectedValue;
			}

			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
		this.GameItemSeq = string.Empty;
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		if (!this.IsCorrectInput()) {
			return;
		}
		this.UpdateData(false);
		this.pnlItemInfo.Visible = false;
		this.pnlKey.Enabled = true;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		this.UpdateData(true);
		this.pnlItemInfo.Visible = false;
		this.pnlKey.Enabled = true;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlItemInfo.Visible = false;
		this.pnlKey.Enabled = true;
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.GameItemSeq = string.Empty;
		this.CreateFlag = ViCommConst.FLAG_ON_STR;
		this.pnlKey.Enabled = false;
		this.pnlItemInfo.Visible = true;
		this.ClearFileds();
	}

	protected void lnkEdit_Command(object sender,CommandEventArgs e) {
		
		this.GameItemGetCd = null;
		this.GameItemPresent = null;
		
		if (!this.IsValid) {
			return;
		}
		int iIndex = int.Parse(iBridUtil.GetStringValue(e.CommandArgument));

		GridViewRow oRow = this.grdManCompCountGetItem.Rows[iIndex];
		HiddenField oCompCountGetItemSeqHiddenField = oRow.FindControl("hdnCompCountGetItemSeq") as HiddenField;
		HiddenField oGameItemSeqHiddenFiled = oRow.FindControl("hdnGameItemSeq") as HiddenField;
		Label oDisplayMonthLabel = oRow.FindControl("lblDisplayMonth") as Label;

		this.CompCountGetItemSeq = oCompCountGetItemSeqHiddenField.Value;
		this.GameItemSeq = oGameItemSeqHiddenFiled.Value;
		this.DisplayMonth = oDisplayMonthLabel.Text;
		this.CreateFlag = string.Empty;
		this.pnlKey.Enabled = false;
		this.GetData();
		this.txtDisplayMonth.Enabled = false;
		this.pnlItemInfo.Visible = true;
	}

	protected void dsManCompCountGetItem_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsManCompCountGetItem_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pDisplayMonth"] = this.txtSeekDisplayMonth.Text.Equals("____/__") ? string.Empty : this.txtSeekDisplayMonth.Text;
	}

	protected void dsGameItem_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pGameItemCategoryType"] = this.GameItemCategoryType;
		e.InputParameters["pGameItemGetCd"] = this.GameItemGetCd;
		e.InputParameters["pPresentFlag"] = this.GameItemPresent;
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	protected void lstGameItemCategory_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		this.GameItemCategoryType = oDropDownList.SelectedValue;
		this.lstGameItem.DataBind();
	}

	protected void lstItemGetCd_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		this.GameItemGetCd = oDropDownList.SelectedValue;
		this.lstGameItem.DataBind();
	}

	protected void lstItemPresent_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		this.GameItemPresent = oDropDownList.SelectedValue;
		this.lstGameItem.DataBind();
	}

	protected void vdcDisplayMonth_ServerValidate(object source,ServerValidateEventArgs args) {
		DateTime oDate;
		if (DateTime.TryParse(string.Concat(this.txtDisplayMonth.Text,"/01"),out oDate)) {
			this.DisplayMonth = oDate.ToString("yyyy/MM");
		} else {
			args.IsValid = false;
			this.vdcDisplayMonth.ErrorMessage = "入力書式が正しくありません。";
		}
	}

	private void InitPage() {
		this.pnlInfo.Visible = true;
		this.pnlItemInfo.Visible = false;

		this.CompCountGetItemSeq = string.Empty;
		this.GameItemSeq = string.Empty;
		this.pnlKey.Enabled = true;
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void ClearFileds() {
		this.txtDisplayMonth.Text = string.Empty;
		this.txtItemCount.Text = string.Empty;
		this.txtCompleteCount.Text = string.Empty;
		this.lstGameItemCategory.DataBind();
		this.lstGameItem.DataBind();
		this.lblErrorMessageItem.Visible = false;
		this.lblErrorMessageCount.Visible = false;
		this.txtDisplayMonth.Enabled = true;
		this.lstItemGetCd.SelectedValue = null;
		this.lstItemPresent.SelectedValue = null;
		this.GameItemGetCd = null;
		this.GameItemPresent = null;
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.GameItemSeq = string.Empty;

		this.grdManCompCountGetItem.PageIndex = 0;
		this.grdManCompCountGetItem.DataSourceID = "dsManCompCountGetItem";
		this.grdManCompCountGetItem.DataBind();
		this.pnlCount.DataBind();
	}

	private void GetData() {
		this.ClearFileds();

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("MAN_COMP_COUNT_GET_ITEM_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pDISPLAY_MONTH",DbSession.DbType.VARCHAR2,this.DisplayMonth);
			oDbSession.ProcedureInParm("pCOMP_COUNT_GET_ITEM_SEQ",DbSession.DbType.VARCHAR2,this.CompCountGetItemSeq);
			oDbSession.ProcedureOutParm("pCOMPLETE_COUNT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pGAME_ITEM_SEQ",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pGAME_ITEM_CATEGORY_TYPE",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pITEM_COUNT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");

			this.txtDisplayMonth.Text = this.DisplayMonth;
			this.txtItemCount.Text = oDbSession.GetStringValue("pITEM_COUNT");
			this.txtCompleteCount.Text = oDbSession.GetStringValue("pCOMPLETE_COUNT");
			this.lstGameItemCategory.SelectedValue = oDbSession.GetStringValue("pGAME_ITEM_CATEGORY_TYPE");
			this.GameItemCategoryType = this.lstGameItemCategory.SelectedValue;
			this.lstGameItem.DataBind();
			this.lstGameItem.SelectedValue = oDbSession.GetStringValue("pGAME_ITEM_SEQ");
		}
	}

	private void UpdateData(bool pDeleteFlag) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("MAN_COMP_COUNT_GET_ITEM_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pDISPLAY_MONTH",DbSession.DbType.VARCHAR2,this.txtDisplayMonth.Text);
			oDbSession.ProcedureBothParm("pCOMP_COUNT_GET_ITEM_SEQ",DbSession.DbType.VARCHAR2,this.CompCountGetItemSeq);
			oDbSession.ProcedureInParm("pCOMPLETE_COUNT",DbSession.DbType.NUMBER,this.txtCompleteCount.Text);
			oDbSession.ProcedureInParm("pGAME_ITEM_SEQ",DbSession.DbType.NUMBER,this.lstGameItem.SelectedValue);
			oDbSession.ProcedureInParm("pITEM_COUNT",DbSession.DbType.NUMBER,this.txtItemCount.Text);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDeleteFlag ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.ClearFileds();
		this.GetList();
	}

	private bool IsCorrectInput() {
		this.lblErrorMessageItem.Visible = false;
		this.lblErrorMessageCount.Visible = false;

		bool bResult = true;

		if (string.IsNullOrEmpty(this.txtItemCount.Text)) {
			this.lblErrorMessageCount.Text = "個数を入力してください。";
			this.lblErrorMessageCount.Visible = true;
			bResult = false;
		}

		if (string.IsNullOrEmpty(this.txtCompleteCount.Text)) {
			this.lblErrorMessageCompleteCount.Text = "コンプリート回数を入力してください。";
			this.lblErrorMessageCompleteCount.Visible = true;
			bResult = false;
		}

		if (string.IsNullOrEmpty(this.lstGameItem.SelectedValue)) {
			this.lblErrorMessageItem.Text = "ボーナスアイテムを選択してください。";
			this.lblErrorMessageItem.Visible = true;
			bResult = false;
		}

		using (ManCompCountGetItem oGameLoginBonus = new ManCompCountGetItem()) {
			if (ViCommConst.FLAG_ON_STR.Equals(this.CreateFlag) && oGameLoginBonus.IsDupulicateItem(this.SiteCd,this.txtDisplayMonth.Text,this.lstGameItem.SelectedValue,this.txtCompleteCount.Text)) {
				this.lblErrorMessageItem.Text = "ボーナスアイテムが重複しています。";
				this.lblErrorMessageItem.Visible = true;
				bResult = false;
			}
		}

		return bResult;
	}

	protected void lstItemGetCd_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		oDropDownList.Items.Insert(3,new ListItem("戦利品","3"));
	}
}
