﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ゲームユーザー別稼動状況集計
--	Progaram ID		: GameUserPerformanceInquiry
--
--  Creation Date	: 2011.08.19
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Extension_GameUserPerformanceInquiry:System.Web.UI.Page {

	private string orderField = "";

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		ClearField();
		lstSiteCd.DataBind();
		if (!IsPostBack) {
			lstSiteCd.DataSourceID = "";
			if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
				lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}
		}
	}

	private void ClearField() {
		orderField = "";
		SysPrograms.SetupFromToDay(lstFromYYYY,lstFromMM,lstFromDD,lstToYYYY,lstToMM,lstToDD,true);
		lstFromYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		lstToYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		lstFromMM.SelectedValue = DateTime.Now.ToString("MM");
		lstToMM.SelectedValue = DateTime.Now.ToString("MM");
		lstFromDD.SelectedValue = DateTime.Now.ToString("dd");
		lstToDD.SelectedValue = DateTime.Now.ToString("dd");
		lstSexCd.SelectedIndex = 0;
		grdPerformance.Visible = false;
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		grdPerformance.Sort("BATTLE_WIN_COUNT",SortDirection.Descending);
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	private void GetList() {
		grdPerformance.Visible = true;
		grdPerformance.DataBind();
	}

	protected string GetFromDay() {
		return lstFromYYYY.SelectedValue + lstFromMM.SelectedValue + lstFromDD.SelectedValue;
	}
	protected string GetToDay() {
		return lstToYYYY.SelectedValue + lstToMM.SelectedValue + lstToDD.SelectedValue;
	}

	protected void dsGameUserPerformance_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = lstFromYYYY.SelectedValue + "/" + lstFromMM.SelectedValue + "/" + lstFromDD.SelectedValue;
		e.InputParameters[2] = lstToYYYY.SelectedValue + "/" + lstToMM.SelectedValue + "/" + lstToDD.SelectedValue;
		e.InputParameters[3] = lstSexCd.SelectedValue;
		e.InputParameters[4] = orderField;
	}

	protected void grdPerformance_Sorting(object sender,GridViewSortEventArgs e) {
		e.SortDirection = SortDirection.Descending;
		orderField = "T1." + e.SortExpression + (e.SortDirection.Equals(SortDirection.Ascending) ? " ASC" : " DESC") + ",T1.SITE_CD,T1.USER_SEQ,T1.USER_CHAR_NO";
	}

	protected string GetViewUrl(object pSexCd,object pSiteCd,object pLoginId) {
		if (pSexCd.Equals(ViCommConst.MAN)) {
			return string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",pSiteCd.ToString(),pLoginId.ToString());
		} else {
			return string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}",pLoginId.ToString(),"UserPerformanceInquiry.aspx");
		}
	}

	protected void grdPerformance_DataBound(object sender, EventArgs e) {
		if (ViCommConst.MAN.Equals(this.lstSexCd.SelectedValue)) {
			this.grdPerformance.Columns[3].Visible = true;
			this.grdPerformance.Columns[5].Visible = false;
		} else {
			this.grdPerformance.Columns[3].Visible = false;
			this.grdPerformance.Columns[5].Visible = true;
		}
	}

	protected void btnCSV_Click(object sender,EventArgs e) {

		if (grdPerformance.Rows.Count == 0) {
			return;
		}
		
		int iMaxCount = 0;
		using (GameUserPerformance oGameUserPerformance = new GameUserPerformance()) {
			iMaxCount = oGameUserPerformance.GetPageCount(
					lstSiteCd.SelectedValue,
					lstFromYYYY.SelectedValue + "/" + lstFromMM.SelectedValue + "/" + lstFromDD.SelectedValue,
					lstToYYYY.SelectedValue + "/" + lstToMM.SelectedValue + "/" + lstToDD.SelectedValue,
					lstSexCd.SelectedValue,
					orderField);
		}
		
		DataTable oCsvData;
		using (GameUserPerformance oGameUserPerformance = new GameUserPerformance()) {
			using (DataSet oDataSet = oGameUserPerformance.GetPageCollection(
				lstSiteCd.SelectedValue,
				lstFromYYYY.SelectedValue + "/" + lstFromMM.SelectedValue + "/" + lstFromDD.SelectedValue,
				lstToYYYY.SelectedValue + "/" + lstToMM.SelectedValue + "/" + lstToDD.SelectedValue,
				lstSexCd.SelectedValue,
				orderField,
				0,
				iMaxCount)) {

				if (oDataSet.Tables[0].Rows.Count > 0) {
					oCsvData = oDataSet.Tables[0];
				} else {
					return;
				}
			}
		}

		//ヘッダ作成
		string sHeader = null;
		if (ViCommConst.MAN.Equals(this.lstSexCd.SelectedValue)) {
			sHeader = "会員ID,ﾊﾝﾄﾞﾙ名,ﾊﾞﾄﾙ勝利数,獲得親密ﾎﾟｲﾝﾄ,獲得経験値";
		} else {
			sHeader = "会員ID,ﾊﾝﾄﾞﾙ名,ﾊﾞﾄﾙ勝利数,獲得経験値,獲得報酬";
		}
		
		//CSV出力 
		Response.ContentType = "application/download";
		Response.AppendHeader("Content-Disposition","attachment;filename=GameUserPerformanceInquiry.csv");
		Response.ContentEncoding = System.Text.Encoding.GetEncoding("Shift_JIS");

		Response.Write(sHeader + "\r\n");
		foreach (DataRow oCsvRow in oCsvData.Rows) {
			string sData = null;
			if (ViCommConst.MAN.Equals(this.lstSexCd.SelectedValue)) {
				sData =
				SetCsvString(oCsvRow["LOGIN_ID"].ToString()) + "," +
				SetCsvString(oCsvRow["GAME_HANDLE_NM"].ToString()) + "," +
				SetCsvString(oCsvRow["BATTLE_WIN_COUNT"].ToString()) + "," +
				SetCsvString(oCsvRow["FRIENDLY_POINT"].ToString()) + "," +
				SetCsvString(oCsvRow["EXP"].ToString());
			} else {
				sData =
				SetCsvString(oCsvRow["LOGIN_ID"].ToString()) + "," +
				SetCsvString(oCsvRow["GAME_HANDLE_NM"].ToString()) + "," +
				SetCsvString(oCsvRow["BATTLE_WIN_COUNT"].ToString()) + "," +
				SetCsvString(oCsvRow["EXP"].ToString()) + "," +
				SetCsvString(oCsvRow["PAYMENT"].ToString());
			}

			Response.Write(sData + "\r\n");
		}
		Response.End();
	}

	private string SetCsvString(string pData) {
		if (pData.IndexOf('"') > -1 ||
			pData.IndexOf(',') > -1 ||
			pData.IndexOf('\r') > -1 ||
			pData.IndexOf('\n') > -1 ||
			pData.StartsWith(" ") || pData.StartsWith("\t") ||
			pData.EndsWith(" ") || pData.EndsWith("\t")) {
			if (pData.IndexOf('"') > -1) {
				//"を""とする
				pData = pData.Replace("\"","\"\"");
			}
			if (pData.IndexOf('\r') > -1 || pData.IndexOf('\n') > -1) {
				//改行しない 
				pData = pData.Replace("\r","");
				pData = pData.Replace("\n","");
			}
			pData = "\"" + pData + "\"";
		}
		return pData;
	}
}
