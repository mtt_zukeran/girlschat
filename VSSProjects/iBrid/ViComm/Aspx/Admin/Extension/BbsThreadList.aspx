﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="BbsThreadList.aspx.cs" Inherits="Extension_BbsThreadList" Title="掲示板スレッド検索"
    ValidateRequest="false" %>

<%@ Import Namespace="System" %>
<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="掲示板スレッド検索"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlMainte">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel runat="server" ID="pnlRegister">
                    <fieldset class="fieldset-inner">
                        <table border="0" style="width: 800px" class="tableStyle">
                            <tr>
                                <td class="tdHeaderStyle">
                                    サイトコード
                                </td>
                                <td class="tdDataStyle" colspan="3">
                                    <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                        Enabled="false" DataValueField="SITE_CD" Width="180px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle2">
                                    <%= DisplayWordUtil.Replace("出演者ID") %>
                                </td>
                                <td class="tdDataStyle" id="tdDataLoginId" colspan="3" runat="server">
                                    <asp:TextBox ID="txtLoginId" runat="server" Width="80px" MaxLength="11"></asp:TextBox>
                                    <asp:Label ID="lblHandleNm" runat="server"></asp:Label>
                                </td>
                                <td class="tdHeaderStyle2" id="tdHeaderCharNo" runat="server">
                                    <%= DisplayWordUtil.Replace("出演者キャラクターNO") %>
                                </td>
                                <td class="tdDataStyle" id="tdDataCharNo" runat="server">
                                    <asp:TextBox ID="txtCharNo" runat="server" Width="80px" MaxLength="2"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlDtl">
                    <fieldset class="fieldset-inner">
                        <legend>[掲示板スレッド]</legend>$NO_TRANS_START;
                        <table border="0" style="width: 800px" class="tableStyle">
                            <tr>
                                <td class="tdHeaderStyle">
                                    スレッド種別
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstThreadType" runat="server" DataSourceID="dsThreadType" DataTextField="CODE_NM" OnDataBound="lstThreadType_DataBound"
                                        DataValueField="CODE" Width="180px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    タイトル
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtTitle" runat="server" Width="300px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    内容
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtDoc" runat="server" Width="650px" Rows="5" TextMode="MultiLine"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle2">
                                    管理者スレッド
                                </td>
                                <td class="tdDataStyle">
                                    <asp:CheckBox ID="chkAdminThread" runat="server" />
                                </td>
                            </tr>
                        </table>
                        $NO_TRANS_END;
                        <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click"
                            ValidationGroup="Detail" />
                        <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click"
                            ValidationGroup="Detail" />
                        <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click"
                            CausesValidation="False" />
                        <asp:CustomValidator ID="vdcAll" runat="server" ErrorMessage="" OnServerValidate="vdcAll_ServerValidate"
                            ValidationGroup="Detail"></asp:CustomValidator>
                    </fieldset>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[検索条件]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 800px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="180px">
                                </asp:DropDownList>
                            </td>
                            <td class="tdHeaderStyle">
                                <%= DisplayWordUtil.Replace("出演者ID") %>
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtSeekLoginId" runat="server" Width="80px" MaxLength="11"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                タイトル
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtSeekTitle" runat="server" MaxLength="1024" Width="300px"></asp:TextBox>
                            </td>
                            <td class="tdHeaderStyle">
                                トピックNO
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtBbsThreadSeq" runat="server" MaxLength="15"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle2">
                                管理者スレッド
                            </td>
                            <td class="tdDataStyle">
                                <asp:CheckBox ID="chkSeekAdminThread" runat="server" />
                            </td>
                            <td class="tdHeaderStyle2">
                                Paging Off
                            </td>
                            <td class="tdDataStyle">
                                <asp:CheckBox ID="chkPagingOff" runat="server" />
                            </td>
                        </tr>
                    </table>
                    <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                        CausesValidation="False" />
                    <asp:Button runat="server" ID="btnRegister" Text="追加" CssClass="seekbutton" OnClick="btnRegister_Click" />
                    <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[掲示板スレッド一覧] </legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdBbsThread.PageIndex + 1 %>
                        of
                        <%= grdBbsThread.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="483px">
                    <asp:GridView ID="grdBbsThread" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        EnableViewState="true" DataSourceID="dsBbsThread" AllowSorting="True" SkinID="GridView"
                        DataKeyNames="SITE_CD,USER_SEQ,USER_CHAR_NO,BBS_THREAD_SEQ" OnRowDataBound="grdBbsThread_RowDataBound">
                        <Columns>
                            <asp:BoundField HeaderText="トピックNO" SortExpression="BBS_THREAD_SEQ" DataField="BBS_THREAD_SEQ">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="最終書込日時" DataField="UPDATE_DATE" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkMainte" runat="server" CommandArgument='<%# Container.DataItemIndex %>'
                                        CommandName="MAINTE" OnCommand="lnkMainte_Command" Text="編集"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="作成者">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}",Eval("LOGIN_ID"),"BbsThreadList.aspx") %>'
                                        Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
                                    <br />
                                    <asp:Label ID="lblHandleNm" runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="内容">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkResponse" runat="server" NavigateUrl='<%# string.Format("~/Extension/BbsResList.aspx?sitecd={0}&userseq={1}&usercharno={2}&bbsthreadseq={3}", Eval("SITE_CD"), Eval("USER_SEQ"), Eval("USER_CHAR_NO"), Eval("BBS_THREAD_SEQ")) %>'
                                        Text='<%# Eval("BBS_THREAD_TITLE") %>'></asp:HyperLink>
                                    <br />
                                    <asp:Label ID="lblBbsThreadDoc" runat="server" Text='<%# GetBbsThreadDoc(Eval("BBS_THREAD_DOC1")) %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="THREAD_TYPE_NM" HeaderText="スレッド種別">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="作成日時">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblCreateDate" Text='<%# Eval("CREATE_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>'></asp:Label>
                                    <br />
                                    <asp:LinkButton ID="lnkUpdateDelFlag" runat="server" CommandArgument='<%# Container.DataItemIndex %>'
                                        CommandName="DELETE" OnClientClick='<%# GetOnClientClick(Eval("DEL_FLAG")) %>'
                                        OnCommand="lnkUpdateDelFlag_Command" Text='<%# GetDelFlagMark(Eval("DEL_FLAG")) %>'></asp:LinkButton>
                                    <asp:HiddenField ID="hdnDelFlag" runat="server" Value='<%# Eval("DEL_FLAG") %>' />
                                    <asp:HiddenField ID="hdnRevisionNo" runat="server" Value='<%# Eval("REVISION_NO") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
            <br />
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsBbsThread" runat="server" SelectMethod="GetPageCollection"
        TypeName="BbsThread" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelected="dsBbsThread_Selected"
        OnSelecting="dsBbsThread_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSeekSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:Parameter Name="pLoginId" Type="String" DefaultValue="" />
            <asp:Parameter Name="pTitle" Type="String" DefaultValue="" />
            <asp:Parameter DefaultValue="" Name="pBbsThreadSeq" Type="String" />
            <asp:Parameter Name="pAdminThreadFlag" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsThreadType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="09" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
