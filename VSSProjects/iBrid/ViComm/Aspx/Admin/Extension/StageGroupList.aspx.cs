﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ステージ一覧

--	Progaram ID		: StageList
--
--  Creation Date	: 2011.07.19
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.IO;

public partial class Extension_StageGroupList : System.Web.UI.Page {
	protected string[] DummyArray = new string[3];

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		set {
			this.ViewState["SexCd"] = value;
		}
    }
    private string StageGroupType {
        get {
            return iBridUtil.GetStringValue(this.ViewState["StageGroupType"]);
        }
        set {
            this.ViewState["StageGroupType"] = value;
        }
    }

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	private string GameItemCategoryType {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemCategoryType"]);
		}
		set {
			this.ViewState["GameItemCategoryType"] = value;
		}
	}

	private string GameItemCategoryType2 {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemCategoryType2"]);
		}
		set {
			this.ViewState["GameItemCategoryType2"] = value;
		}
	}

	private string GameItemCategoryType3 {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemCategoryType3"]);
		}
		set {
			this.ViewState["GameItemCategoryType3"] = value;
		}
	}

	private string GameItemGetCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemGetCd"]);
		}
		set {
			this.ViewState["GameItemGetCd"] = value;
		}
	}

	private string GameItemGetCd2 {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemGetCd2"]);
		}
		set {
			this.ViewState["GameItemGetCd2"] = value;
		}
	}

	private string GameItemGetCd3 {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemGetCd3"]);
		}
		set {
			this.ViewState["GameItemGetCd3"] = value;
		}
	}

	private string GameItemPresent {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemPresent"]);
		}
		set {
			this.ViewState["GameItemPresent"] = value;
		}
	}

	private string GameItemPresent2 {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemPresent2"]);
		}
		set {
			this.ViewState["GameItemPresent2"] = value;
		}
	}

	private string GameItemPresent3 {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemPresent3"]);
		}
		set {
			this.ViewState["GameItemPresent3"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {

            if (iBridUtil.GetStringValue(Request.QueryString["sexcd"]).Equals(ViCommConst.MAN)) {
                this.lblPgmTitle.Text = "男性" + lblPgmTitle.Text;
                this.Title = "男性" + this.Title;
            } else {
                this.lblPgmTitle.Text = "女性" + lblPgmTitle.Text;
                this.Title = "女性" + this.Title;
            }
			this.InitPage();
			this.SexCd = iBridUtil.GetStringValue(this.Request.QueryString["sexcd"]);
			this.pnlCastCompleteItem.Visible = ViCommConst.OPERATOR.Equals(this.SexCd);
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.UpdateData(false);
		this.pnlStageGroupInfo.Visible = false;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		this.UpdateData(true);
        this.pnlStageGroupInfo.Visible = false;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.ClearFileds();
		this.pnlKey.Enabled = true;
        this.pnlStageGroupInfo.Visible = false;
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.ClearFileds();
		this.pnlKey.Enabled = false;
		this.pnlStageGroupInfo.Visible = true;
	}

    protected void lnkStageGroupNm_Command(object sender, CommandEventArgs e) {

		this.GameItemGetCd = null;
		this.GameItemGetCd2 = null;
		this.GameItemGetCd3 = null;
		this.GameItemPresent = null;
		this.GameItemPresent2 = null;
		this.GameItemPresent3 = null;
		lstItemPresent1.SelectedValue = null;
		lstItemPresent2.SelectedValue = null;
		lstItemPresent3.SelectedValue = null;
		
		if (!this.IsValid) {
			return;
		}
        this.StageGroupType = iBridUtil.GetStringValue(e.CommandArgument);
		this.GetData();
		this.pnlKey.Enabled = false;
		this.pnlStageGroupInfo.Visible = true;
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	protected void lstGameItemCategory_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		switch (oDropDownList.ID) {
			case "lstCastCompleteItemCategory1":
				this.GameItemCategoryType = oDropDownList.SelectedValue;
				this.lstCastCompleteItem1.DataBind();
				break;
			case "lstCastCompleteItemCategory2":
				this.GameItemCategoryType2 = oDropDownList.SelectedValue;
				this.lstCastCompleteItem2.DataBind();
				break;
			case "lstCastCompleteItemCategory3":
				this.GameItemCategoryType3 = oDropDownList.SelectedValue;
				this.lstCastCompleteItem3.DataBind();
				break;
		}
	}

	protected void lstItemGetCd_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		switch (oDropDownList.ID) {
			case "lstItemGetCd1":
				this.GameItemGetCd = oDropDownList.SelectedValue;
				this.lstCastCompleteItem1.DataBind();
				break;
			case "lstItemGetCd2":
				this.GameItemGetCd2 = oDropDownList.SelectedValue;
				this.lstCastCompleteItem2.DataBind();
				break;
			case "lstItemGetCd3":
				this.GameItemGetCd3 = oDropDownList.SelectedValue;
				this.lstCastCompleteItem3.DataBind();
				break;
		}
	}

	protected void lstItemPresent_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		switch (oDropDownList.ID) {
			case "lstItemPresent1":
				this.GameItemPresent = oDropDownList.SelectedValue;
				this.lstCastCompleteItem1.DataBind();
				break;
			case "lstItemPresent2":
				this.GameItemPresent2 = oDropDownList.SelectedValue;
				this.lstCastCompleteItem2.DataBind();
				break;
			case "lstItemPresent3":
				this.GameItemPresent3 = oDropDownList.SelectedValue;
				this.lstCastCompleteItem3.DataBind();
				break;
		}
	}

	protected void dsGameItem_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pGameItemCategoryType"] = this.GameItemCategoryType;
		e.InputParameters["pGameItemGetCd"] = this.GameItemGetCd;
		e.InputParameters["pPresentFlag"] = this.GameItemPresent;
	}

	protected void dsGameItem2_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pGameItemCategoryType"] = this.GameItemCategoryType2;
		e.InputParameters["pGameItemGetCd"] = this.GameItemGetCd2;
		e.InputParameters["pPresentFlag"] = this.GameItemPresent2;
	}

	protected void dsGameItem3_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pGameItemCategoryType"] = this.GameItemCategoryType3;
		e.InputParameters["pGameItemGetCd"] = this.GameItemGetCd3;
		e.InputParameters["pPresentFlag"] = this.GameItemPresent3;
	}

	protected void vdcCastCompleteItemGet1_ServerValidate(object source,ServerValidateEventArgs args) {
		if (string.IsNullOrEmpty(this.lstCastCompleteItem1.SelectedValue)) {
			return;
		}

		if (string.IsNullOrEmpty(this.txtCastCompleteItemGetCount1.Text)) {
			args.IsValid = false;
			this.vdcCastCompleteItemGet1.ErrorMessage = "個数を入力してください。";
		}
	}

	protected void vdcCastCompleteItemGet2_ServerValidate(object source,ServerValidateEventArgs args) {
		if (string.IsNullOrEmpty(this.lstCastCompleteItem2.SelectedValue)) {
			return;
		}

		if (string.IsNullOrEmpty(this.txtCastCompleteItemGetCount2.Text)) {
			args.IsValid = false;
			this.vdcCastCompleteItemGet2.ErrorMessage = "個数を入力してください。";
		}
	}

	protected void vdcCastCompleteItemGet3_ServerValidate(object source,ServerValidateEventArgs args) {
		if (string.IsNullOrEmpty(this.lstCastCompleteItem3.SelectedValue)) {
			return;
		}

		if (string.IsNullOrEmpty(this.txtCastCompleteItemGetCount3.Text)) {
			args.IsValid = false;
			this.vdcCastCompleteItemGet3.ErrorMessage = "個数を入力してください。";
		}
	}

	private void InitPage() {
		this.pnlInfo.Visible = true;
		this.pnlStageGroupInfo.Visible = false;

		this.pnlKey.Enabled = true;
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void ClearFileds() {
        this.lblStageGroupType.Text = string.Empty;
        this.txtStageGroupNm.Text = string.Empty;
		this.txtCastCompleteItemGetCount1.Text = string.Empty;
		this.txtCastCompleteItemGetCount2.Text = string.Empty;
		this.txtCastCompleteItemGetCount3.Text = string.Empty;
		this.pnlCastCompleteItem.DataBind();

		this.pnlKey.Enabled = true;
	}

	private void GetList() {
		this.SiteCd = this.lstSiteCd.SelectedValue;

		this.grdStageGroup.PageIndex = 0;
        this.grdStageGroup.DataSourceID = "dsStageGroup";
        this.grdStageGroup.DataBind();
	}

	private void GetData() {
		this.ClearFileds();

		using (DbSession oDbSession = new DbSession()) {
            
            oDbSession.PrepareProcedure("STAGE_GROUP_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pSTAGE_GROUP_TYPE",DbSession.DbType.VARCHAR2,this.StageGroupType);
            oDbSession.ProcedureInParm("pSEX_CD", DbSession.DbType.VARCHAR2, this.SexCd);
            oDbSession.ProcedureOutParm("pSTAGE_GROUP_NM", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutArrayParm("pCOMP_ITEM_SEQ_ARR",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutArrayParm("pCOMP_ITEM_COUNT_ARR",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutArrayParm("pCOMP_ITEM_CATEGORY_TYPE_ARR",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

            this.lblStageGroupType.Text = oDbSession.GetStringValue("pSTAGE_GROUP_TYPE");
            this.txtStageGroupNm.Text = oDbSession.GetStringValue("pSTAGE_GROUP_NM");
    		this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");

			if (ViCommConst.OPERATOR.Equals(this.SexCd)) {
				this.lstCastCompleteItemCategory1.SelectedValue = oDbSession.GetArryIntValue("pCOMP_ITEM_CATEGORY_TYPE_ARR",0);
				this.GameItemCategoryType = this.lstCastCompleteItemCategory1.SelectedValue;
				this.lstCastCompleteItem1.DataBind();
				this.lstCastCompleteItemCategory2.SelectedValue = oDbSession.GetArryIntValue("pCOMP_ITEM_CATEGORY_TYPE_ARR",1);
				this.GameItemCategoryType2 = this.lstCastCompleteItemCategory2.SelectedValue;
				this.lstCastCompleteItem2.DataBind();
				this.lstCastCompleteItemCategory3.SelectedValue = oDbSession.GetArryIntValue("pCOMP_ITEM_CATEGORY_TYPE_ARR",2);
				this.GameItemCategoryType3 = this.lstCastCompleteItemCategory3.SelectedValue;
				this.lstCastCompleteItem3.DataBind();

				this.lstCastCompleteItem1.SelectedValue = oDbSession.GetArryIntValue("pCOMP_ITEM_SEQ_ARR",0);
				this.lstCastCompleteItem2.SelectedValue = oDbSession.GetArryIntValue("pCOMP_ITEM_SEQ_ARR",1);
				this.lstCastCompleteItem3.SelectedValue = oDbSession.GetArryIntValue("pCOMP_ITEM_SEQ_ARR",2);

				this.txtCastCompleteItemGetCount1.Text = oDbSession.GetArryIntValue("pCOMP_ITEM_COUNT_ARR",0);
				this.txtCastCompleteItemGetCount2.Text = oDbSession.GetArryIntValue("pCOMP_ITEM_COUNT_ARR",1);
				this.txtCastCompleteItemGetCount3.Text = oDbSession.GetArryIntValue("pCOMP_ITEM_COUNT_ARR",2);
			}
		}
	}

	private void UpdateData(bool pDelFlag) {
        
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("STAGE_GROUP_MAINTE");
			oDbSession.cmd.BindByName = true;
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
            oDbSession.ProcedureInParm("pSTAGE_GROUP_TYPE", DbSession.DbType.VARCHAR2, this.lblStageGroupType.Text);
            oDbSession.ProcedureInParm("pSEX_CD", DbSession.DbType.VARCHAR2, this.SexCd);
            oDbSession.ProcedureInParm("pSTAGE_GROUP_NM",DbSession.DbType.VARCHAR2,this.txtStageGroupNm.Text.TrimEnd());
			if (ViCommConst.OPERATOR.Equals(this.SexCd)) {
				this.ProcessUpdateDataCastComp(oDbSession);
			} else {
				oDbSession.ProcedureInArrayParm("pCOMP_ITEM_SEQ_ARR",DbSession.DbType.NUMBER,new string[] { });
				oDbSession.ProcedureInArrayParm("pCOMP_ITEM_COUNT_ARR",DbSession.DbType.NUMBER,new string[] { });
			}
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDelFlag ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.ClearFileds();
		this.GetList();
         
	}

	private void ProcessUpdateDataCastComp(DbSession pDbSession) {
		List<string> oCompItemSeqList = new List<string>();
		List<string> oCompItemCountList = new List<string>();
		if (!string.IsNullOrEmpty(this.lstCastCompleteItem1.SelectedValue)) {
			oCompItemSeqList.Add(this.lstCastCompleteItem1.SelectedValue);
			oCompItemCountList.Add(this.txtCastCompleteItemGetCount1.Text);
		}
		if (!string.IsNullOrEmpty(this.lstCastCompleteItem2.SelectedValue)) {
			oCompItemSeqList.Add(this.lstCastCompleteItem2.SelectedValue);
			oCompItemCountList.Add(this.txtCastCompleteItemGetCount2.Text);
		}
		if (!string.IsNullOrEmpty(this.lstCastCompleteItem3.SelectedValue)) {
			oCompItemSeqList.Add(this.lstCastCompleteItem3.SelectedValue);
			oCompItemCountList.Add(this.txtCastCompleteItemGetCount3.Text);
		}

		pDbSession.ProcedureInArrayParm("pCOMP_ITEM_SEQ_ARR",DbSession.DbType.NUMBER,oCompItemSeqList.ToArray());
		pDbSession.ProcedureInArrayParm("pCOMP_ITEM_COUNT_ARR",DbSession.DbType.NUMBER,oCompItemCountList.ToArray());
	}

	protected void lstItemGetCd_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		oDropDownList.Items.Insert(3,new ListItem("戦利品","3"));
	}
}
