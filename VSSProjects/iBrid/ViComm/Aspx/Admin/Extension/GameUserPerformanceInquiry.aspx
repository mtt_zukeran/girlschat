<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="GameUserPerformanceInquiry.aspx.cs" Inherits="Extension_GameUserPerformanceInquiry"
	Title="ゲームユーザー別稼動状況集計" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="ゲームユーザー別稼動状況集計"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							<asp:Label ID="lblAccessUnit" runat="server" Text="集計開始日"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstFromYYYY" runat="server" Width="60px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstFromMM" runat="server" Width="40px">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstFromDD" runat="server" Width="40px">
							</asp:DropDownList>日
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							<asp:Label ID="Label3" runat="server" Text="集計終了日"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstToYYYY" runat="server" Width="60px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstToMM" runat="server" Width="40px">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstToDD" runat="server" Width="40px">
							</asp:DropDownList>日
						</td>
						<td class="tdHeaderStyle2">
							<asp:Label ID="Label1" runat="server" Text="性別"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:ListBox ID="lstSexCd" runat="server" Rows="1">
								<asp:ListItem Value="1">男性</asp:ListItem>
								<asp:ListItem Value="3">女性</asp:ListItem>
							</asp:ListBox>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
				<asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton"  OnClick="btnCSV_Click" />
			</asp:Panel>
		</fieldset>
		<fieldset>
			<legend>[稼動集計]</legend>
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto">
				<asp:GridView ID="grdPerformance" runat="server" AutoGenerateColumns="False" DataSourceID="dsGameUserPerformance" AllowSorting="True" SkinID="GridViewColor"
					AllowPaging="True" PageSize="100" OnSorting="grdPerformance_Sorting" OnDataBound="grdPerformance_DataBound">
					<Columns>
						<asp:TemplateField HeaderText="会員ID">
							<ItemTemplate>
								<asp:HyperLink ID="lblTxLoginId" runat="server" NavigateUrl='<%# GetViewUrl(Eval("SEX_CD"),Eval("SITE_CD"),Eval("LOGIN_ID")) %>' Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ﾊﾝﾄﾞﾙ名">
							<ItemStyle HorizontalAlign="Left" Width="200px" />
							<ItemTemplate>
								<asp:Label runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("GAME_HANDLE_NM")) %>' ID="lblHandleNm" />
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ﾊﾞﾄﾙ勝利数" SortExpression="BATTLE_WIN_COUNT">
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("BATTLE_WIN_COUNT", "{0:N0}") %>'></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="獲得親密ﾎﾟｲﾝﾄ" SortExpression="FRIENDLY_POINT">
							<ItemTemplate>
								<asp:Label ID="Label4" runat="server" Text='<%# Eval("FRIENDLY_POINT", "{0:N0}") %>'></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="獲得経験値" SortExpression="EXP">
							<ItemTemplate>
								<asp:Label ID="Label5" runat="server" Text='<%# Eval("EXP", "{0:N0}") %>'></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="獲得報酬" SortExpression="PAYMENT">
							<ItemTemplate>
								<asp:Label ID="Label6" runat="server" Text='<%# Eval("PAYMENT", "{0:N0}") %>'></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
				</asp:GridView>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsGameUserPerformance" runat="server" SelectMethod="GetPageCollection" TypeName="GameUserPerformance" SelectCountMethod="GetPageCount"
		OnSelecting="dsGameUserPerformance_Selecting" EnablePaging="True">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
			<asp:Parameter Name="pSexCd" Type="String" />
			<asp:Parameter Name="pOrder" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
</asp:Content>
