﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RealTimeReport.aspx.cs" Inherits="Extension_RealTimeReport" Title="リアルタイムレポート"
    ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="リアルタイムレポート"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset>
                <legend>[設定]</legend>
                <table border="0" style="width: 640px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            サイトコード
                        </td>
                        <td class="tdDataStyle" colspan="3">
                            <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="180px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            集計日
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstYYYY" runat="server" Width="54px">
                            </asp:DropDownList>年
                            <asp:DropDownList ID="lstMM" runat="server" Width="43px">
                            </asp:DropDownList>月
                            <asp:DropDownList ID="lstDD" runat="server" Width="43px">
                            </asp:DropDownList>日
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    CausesValidation="False" />
                <asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" CausesValidation="False" OnClick="btnCSV_Click" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[リアルタイムレポート]</legend>
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="552px">
                    <asp:GridView ID="grdRealTimeReport" runat="server" AutoGenerateColumns="False" DataSourceID="dsRealTimeReport"
                        AllowSorting="true" SkinID="GridViewColor" ShowFooter="True" OnRowDataBound="grdRealTimeReport_RowDataBound">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Label ID="lblReportHour" runat="server" Text='<%# Eval("REPORT_HOUR") + "時" %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                                <FooterTemplate>
                                    <asp:Label ID="lblReportHourSum" runat="server" Text="合計"></asp:Label>
                                </FooterTemplate>
                                <FooterStyle HorizontalAlign="Center" Font-Bold="true" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ﾛｸﾞｲﾝ数">
                                <ItemTemplate>
                                    <asp:Label ID="lblLoginCount" runat="server" Text='<%# Eval("LOGIN_COUNT", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                                <FooterTemplate>
                                    <asp:Label ID="lblLoginCountSum" runat="server"></asp:Label>
                                </FooterTemplate>
                                <FooterStyle HorizontalAlign="Right" Width="80px" Font-Bold="true" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="累計ﾛｸﾞｲﾝ数">
                                <ItemTemplate>
                                    <asp:Label ID="lblLoginCountHourSum" runat="server" Text='<%# Eval("LOGIN_COUNT_HOUR_SUM", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                                <FooterTemplate>
                                    <asp:Label ID="lblLoginCountHourSumSum" runat="server" Text="-"></asp:Label>
                                </FooterTemplate>
                                <FooterStyle HorizontalAlign="Right" Font-Bold="true" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="消費円">
                                <ItemTemplate>
                                    <asp:Label ID="lblUsedGamePoint" runat="server" Text='<%# Eval("USED_GAME_POINT", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                                <FooterTemplate>
                                    <asp:Label ID="lblUsedGamePointSum" runat="server"></asp:Label>
                                </FooterTemplate>
                                <FooterStyle HorizontalAlign="Right" Font-Bold="true" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="消費PT/G">
                                <ItemTemplate>
                                    <asp:Label ID="lblUsedPoint" runat="server" Text='<%# Eval("USED_POINT", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                                <FooterTemplate>
                                    <asp:Label ID="lblUsedPointSum" runat="server"></asp:Label>
                                </FooterTemplate>
                                <FooterStyle HorizontalAlign="Right" Font-Bold="true" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ﾊﾞﾄﾙ回数">
                                <ItemTemplate>
                                    <asp:Label ID="lblBattleCount" runat="server" Text='<%# Eval("BATTLE_COUNT", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                                <FooterTemplate>
                                    <asp:Label ID="lblBattleCountSum" runat="server"></asp:Label>
                                </FooterTemplate>
                                <FooterStyle HorizontalAlign="Right" Font-Bold="true" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ﾅﾝﾊﾟ回数">
                                <ItemTemplate>
                                    <asp:Label ID="lblMissionCount" runat="server" Text='<%# Eval("MISSION_COUNT", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                                <FooterTemplate>
                                    <asp:Label ID="lblMissionCountSum" runat="server"></asp:Label>
                                </FooterTemplate>
                                <FooterStyle HorizontalAlign="Right" Font-Bold="true" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="お宝獲得回数">
                                <ItemTemplate>
                                    <asp:Label ID="lblTreasureGetCount" runat="server" Text='<%# Eval("TREASURE_GET_COUNT", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                                <FooterTemplate>
                                    <asp:Label ID="lblTreasureGetCountSum" runat="server"></asp:Label>
                                </FooterTemplate>
                                <FooterStyle HorizontalAlign="Right" Font-Bold="true" />
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle ForeColor="Black" BackColor="LightYellow" Font-Bold="true" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsRealTimeReport" runat="server" SelectMethod="GetList"
        OnSelecting="dsRealTimeReport_Selecting" TypeName="RealTimeReport">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSeekSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:Parameter Name="pReportDay" Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
