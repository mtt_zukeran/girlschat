﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RichinoRankList.aspx.cs" Inherits="Extension_RichinoRankList" Title="リッチーノランク設定"
    ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="リッチーノランク設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlMainte">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel runat="server" ID="pnlKey">
                    <fieldset class="fieldset-inner">
                        <table border="0" style="width: 640px" class="tableStyle">
                            <tr>
                                <td class="tdHeaderStyle2">
                                    サイトコード
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                        DataValueField="SITE_CD" Width="180px">
                                    </asp:DropDownList>
                                </td>
                                <td class="tdHeaderStyle2">
                                    リッチーノランク
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtRichinoRank" runat="server" MaxLength="1" Width="20px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vdrRichinoRank" runat="server" ErrorMessage="リッチーノランクを入力して下さい。"
                                        ControlToValidate="txtRichinoRank" ValidationGroup="Key">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9"
                                        TargetControlID="vdrRichinoRank" HighlightCssClass="validatorCallout" />
                                    <asp:RegularExpressionValidator ID="vdeRichinoRank" runat="server" ErrorMessage="リッチーノランクは1桁の英字大文字A～Hで入力して下さい。"
                                        ValidationExpression="[A-H]" ControlToValidate="txtRichinoRank" ValidationGroup="Key">*</asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10"
                                        TargetControlID="vdeRichinoRank" HighlightCssClass="validatorCallout" />
                                </td>
                            </tr>
                        </table>
                        <asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click"
                            ValidationGroup="Key" />
                    </fieldset>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlDtl">
                    <fieldset class="fieldset-inner">
                        <legend>[リッチーノランク内容]</legend>
                        <table border="0" style="width: 640px" class="tableStyle">
                            <tr>
                                <td class="tdHeaderStyle">
                                    リッチーノランク名称
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtRichinoRankNm" runat="server" MaxLength="30" Width="200px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vdrRichinoRankNm" runat="server" ErrorMessage="リッチーノランク名称を入力して下さい。"
                                        ControlToValidate="txtRichinoRankNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8"
                                        TargetControlID="vdrRichinoRankNm" HighlightCssClass="validatorCallout" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    最低入金額
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtRichinoMinReceiptAmt" runat="server" MaxLength="8" Width="60px"></asp:TextBox>&nbsp;
                                    <asp:RangeValidator ID="vdrRichinoMinReceiptAmt" runat="server" ControlToValidate="txtRichinoMinReceiptAmt"
                                        ErrorMessage="最低入金額を1以上の数値で入力して下さい。" MaximumValue="99999999" MinimumValue="1"
                                        ValidationGroup="Detail">*</asp:RangeValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2"
                                        TargetControlID="vdrRichinoMinReceiptAmt" HighlightCssClass="validatorCallout" />
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" Enabled="true"
                                        FilterType="Numbers" TargetControlID="txtRichinoMinReceiptAmt" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    ﾕｰｻﾞｰﾗﾝｸｷｰﾌﾟ金額
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtRichinoRankKeepAmt" runat="server" MaxLength="8" Width="60px"></asp:TextBox>&nbsp;
                                    <asp:RequiredFieldValidator ID="vdrRichinoRankKeepAmt" runat="server" ErrorMessage="ﾕｰｻﾞｰﾗﾝｸｷｰﾌﾟ金額を入力して下さい。"
                                        ControlToValidate="txtRichinoRankKeepAmt" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdrRichinoRankKeepAmt">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:CompareValidator ID="vdcRichinoRankKeepAmt" runat="server" ErrorMessage="最低入金額以下の値を入力して下さい。"
                                        ControlToCompare="txtRichinoMinReceiptAmt" ControlToValidate="txtRichinoRankKeepAmt"
                                        Type="Integer" Operator="LessThanEqual" ValidationGroup="Detail">
                                        *</asp:CompareValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3"
                                        TargetControlID="vdcRichinoRankKeepAmt" HighlightCssClass="validatorCallout" />
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true"
                                        FilterType="Numbers" TargetControlID="txtRichinoRankKeepAmt" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    特典動画閲覧可能数
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtSpecialMovieCount" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vdrSpecialMovieCount" runat="server" ErrorMessage="特典動画閲覧可能数入力して下さい。"
                                        ControlToValidate="txtSpecialMovieCount" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4"
                                        TargetControlID="vdrSpecialMovieCount" HighlightCssClass="validatorCallout" />
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="true"
                                        FilterType="Numbers" TargetControlID="txtSpecialMovieCount" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    特典画像閲覧可能数
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtSpecialPicCount" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vdrSpecialPicCount" runat="server" ErrorMessage="特典画像閲覧可能数入力して下さい。"
                                        ControlToValidate="txtSpecialPicCount" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5"
                                        TargetControlID="vdrSpecialPicCount" HighlightCssClass="validatorCallout" />
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" Enabled="true"
                                        FilterType="Numbers" TargetControlID="txtSpecialPicCount" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    メールおねだり送信可能回数
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtMailRequestLimitCount" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vdrMailRequestLmitCount" runat="server" ErrorMessage="メールおねだり送信可能数入力して下さい。"
                                        ControlToValidate="txtMailRequestLimitCount" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6"
                                        TargetControlID="vdrMailRequestLmitCount" HighlightCssClass="validatorCallout" />
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" Enabled="true"
                                        FilterType="Numbers" TargetControlID="txtMailRequestLimitCount" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    つぶやき投稿制限回数
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtManTweetDailyCount" runat="server" MaxLength="2" Width="30px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vdrManTweetDailyCount" runat="server" ErrorMessage="つぶやき投稿制限回数を入力して下さい。"
                                        ControlToValidate="txtMailRequestLimitCount" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7"
                                        TargetControlID="vdrManTweetDailyCount" HighlightCssClass="validatorCallout" />
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" Enabled="true"
                                        FilterType="Numbers" TargetControlID="txtManTweetDailyCount" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle2">
                                    つぶやき投稿間隔(分)
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtManTweetIntervalMin" runat="server" MaxLength="3" Width="30px"></asp:TextBox>分
                                    <asp:RequiredFieldValidator ID="vdrManTweetIntervalMin" runat="server" ErrorMessage="つぶやき投稿間隔(分)を入力して下さい。"
                                        ControlToValidate="txtMailRequestLimitCount" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11"
                                        TargetControlID="vdrManTweetIntervalMin" HighlightCssClass="validatorCallout" />
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" Enabled="true"
                                        FilterType="Numbers" TargetControlID="txtManTweetIntervalMin" />
                                </td>
                            </tr>
                        </table>
                        <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click"
                            ValidationGroup="Detail" />
                        <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click"
                            ValidationGroup="Key" />
                        <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click"
                            CausesValidation="False" />
                    </fieldset>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <fieldset>
            <legend>[リッチーノランク一覧]</legend>
            <table border="0" style="width: 600px" class="tableStyle">
                <tr>
                    <td class="tdHeaderStyle2">
                        サイトコード
                    </td>
                    <td class="tdDataStyle">
                        <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                            DataValueField="SITE_CD" Width="240px">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
            <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                CausesValidation="False" />
            <asp:Button runat="server" ID="btnRegist" Text="ランク追加" CssClass="seekbutton" CausesValidation="False"
                OnClick="btnRegist_Click" />
            <br />
            <br />
            <asp:GridView ID="grdRichinoRank" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                DataSourceID="dsRichinoRank" AllowSorting="True" SkinID="GridViewColor">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkRichinoRank" runat="server" Text='<%# string.Format("{0} {1}",Eval("RICHINO_RANK"),Eval("RICHINO_RANK_NM")) %>'
                                CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("RICHINO_RANK"))  %>'
                                OnCommand="lnkRichinoRank_Command" CausesValidation="False">
                            </asp:LinkButton>
                        </ItemTemplate>
                        <HeaderTemplate>
                            リッチーノランク
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="RICHINO_MIN_RECEIPT_AMT" DataFormatString="{0:#,##0}"
                        HeaderText="最低入金額">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="RICHINO_RANK_KEEP_AMT" DataFormatString="{0:#,##0}" HeaderText="ﾕｰｻﾞｰﾗﾝｸｷｰﾌﾟ金額">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="SPECIAL_MOVIE_COUNT" DataFormatString="{0:#,##0}" HeaderText="特典動画閲覧可能数">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="SPECIAL_PIC_COUNT" DataFormatString="{0:#,##0}" HeaderText="特典画像閲覧可能数">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="メールおねだり<br/>送信可能数">
                        <ItemTemplate>
                            <asp:Label ID="lblMailRequestLimitCount" runat="server" Text='<%# Eval("MAIL_REQUEST_LIMIT_COUNT", "{0:N0}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="つぶやき<br/>投稿制限回数">
                        <ItemTemplate>
                            <asp:Label ID="lblManTweetDailyCount" runat="server" Text='<%# Eval("MAN_TWEET_DAILY_COUNT", "{0:N0}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="つぶやき<br/>投稿間隔(分)">
                        <ItemTemplate>
                            <asp:Label ID="lblManTweetIntervalMin" runat="server" Text='<%# Eval("MAN_TWEET_INTERVAL_MIN", "{0:N0}") %>'></asp:Label>分
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}"
                        HtmlEncode="False">
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                </Columns>
                <PagerSettings Mode="NumericFirstLast" />
            </asp:GridView>
            <asp:Panel runat="server" ID="pnlCount">
                <a class="reccount">Record Count
                    <%# GetRecCount() %>
                </a>
                <br />
                <a class="reccount">Current viewing page
                    <%= grdRichinoRank.PageIndex + 1%>
                    of
                    <%= grdRichinoRank.PageCount%>
                </a>
            </asp:Panel>
        </fieldset>
        <br />
    </div>
    <asp:ObjectDataSource ID="dsRichinoRank" runat="server" SelectMethod="GetPageCollection"
        TypeName="RichinoRank" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelected="dsRichinoRank_Selected">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSeekSiteCd" DbType="String" PropertyName="SelectedValue"
                Name="pSiteCd" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete"
        ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
