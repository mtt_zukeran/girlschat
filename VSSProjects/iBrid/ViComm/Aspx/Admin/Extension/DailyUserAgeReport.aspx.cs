﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 日別ユーザ年齢別レポート

--	Progaram ID		: DailyUserAgeReport
--
--  Creation Date	: 2011.08.10
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using iBridCommLib;
using System.Drawing;
using ViComm;

public partial class Extension_DailyUserAgeReport : System.Web.UI.Page {
	protected string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		private set {
			this.ViewState["SexCd"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		if (!this.IsPostBack) {
			this.SexCd = this.Request.QueryString["sexcd"];
			if (ViCommConst.MAN.Equals(this.SexCd)) {
				this.lblPgmTitle.Text = "男性" + this.lblPgmTitle.Text;
			} else {
				this.lblPgmTitle.Text = "女性" + this.lblPgmTitle.Text;
			}
			this.Title = this.lblPgmTitle.Text;

			this.InitPage();

			this.GetList();
			this.pnlInfo.Visible = true;
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		this.GetList();
		this.pnlInfo.Visible = true;
	}

	protected void dsDailyUserAgeReport_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pReportDayFrom"] = string.Format("{0}/{1}/{2}",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue);
		e.InputParameters["pReportDayTo"] = string.Format("{0}/{1}/{2}",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,this.lstToDD.SelectedValue);
	}

	protected void grdDailyUserAgeReport_DataBound(object sender,EventArgs e) {
		if (this.grdDailyUserAgeReport.Rows.Count > 1) {
			this.grdDailyUserAgeReport.Rows[this.grdDailyUserAgeReport.Rows.Count - 1].Font.Bold = true;
			this.grdDailyUserAgeReport.Rows[this.grdDailyUserAgeReport.Rows.Count - 1].ForeColor = Color.Black;
			this.grdDailyUserAgeReport.Rows[this.grdDailyUserAgeReport.Rows.Count - 1].BackColor = Color.LightYellow;
			this.grdDailyUserAgeReport.Rows[this.grdDailyUserAgeReport.Rows.Count - 2].Font.Bold = true;
			this.grdDailyUserAgeReport.Rows[this.grdDailyUserAgeReport.Rows.Count - 2].ForeColor = Color.Black;
			this.grdDailyUserAgeReport.Rows[this.grdDailyUserAgeReport.Rows.Count - 2].BackColor = Color.LightYellow;
		}
	}

	protected void grdDailyUserAgeReport_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			if (DataBinder.Eval(e.Row.DataItem,"REPORT_DAY").Equals("シェア率")) {
				Label lblCountAll = e.Row.FindControl("lblCountAll") as Label;
				Label lblCount18 = e.Row.FindControl("lblCount18") as Label;
				Label lblCount20 = e.Row.FindControl("lblCount20") as Label;
				Label lblCount25 = e.Row.FindControl("lblCount25") as Label;
				Label lblCount30 = e.Row.FindControl("lblCount30") as Label;
				Label lblCountOther = e.Row.FindControl("lblCountOther") as Label;
				Label lblCountUniqueAll = e.Row.FindControl("lblCountUniqueAll") as Label;
				Label lblCountUnique18 = e.Row.FindControl("lblCountUnique18") as Label;
				Label lblCountUnique20 = e.Row.FindControl("lblCountUnique20") as Label;
				Label lblCountUnique25 = e.Row.FindControl("lblCountUnique25") as Label;
				Label lblCountUnique30 = e.Row.FindControl("lblCountUnique30") as Label;
				Label lblCountUniqueOther = e.Row.FindControl("lblCountUniqueOther") as Label;

				lblCountAll.Text = string.Concat(lblCountAll.Text,"%");
				lblCount18.Text = string.Concat(lblCount18.Text,"%");
				lblCount20.Text = string.Concat(lblCount20.Text,"%");
				lblCount25.Text = string.Concat(lblCount25.Text,"%");
				lblCount30.Text = string.Concat(lblCount30.Text,"%");
				lblCountOther.Text = string.Concat(lblCountOther.Text,"%");
				lblCountUniqueAll.Text = string.Concat(lblCountUniqueAll.Text,"%");
				lblCountUnique18.Text = string.Concat(lblCountUnique18.Text,"%");
				lblCountUnique20.Text = string.Concat(lblCountUnique20.Text,"%");
				lblCountUnique25.Text = string.Concat(lblCountUnique25.Text,"%");
				lblCountUnique30.Text = string.Concat(lblCountUnique30.Text,"%");
				lblCountUniqueOther.Text = string.Concat(lblCountUniqueOther.Text,"%");
			}
		} 
	}

	private void InitPage() {
		this.grdDailyUserAgeReport.DataSourceID = string.Empty;
		this.pnlKey.DataBind();
		this.pnlInfo.Visible = false;
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		SysPrograms.SetupFromToDay(this.lstFromYYYY,this.lstFromMM,this.lstFromDD,this.lstToYYYY,this.lstToMM,this.lstToDD,false);
		this.lstFromYYYY.SelectedValue = DateTime.Today.ToString("yyyy");
		this.lstFromMM.SelectedValue = DateTime.Today.ToString("MM");
		this.lstFromDD.SelectedValue = DateTime.Today.ToString("dd");
		this.lstToYYYY.SelectedValue = DateTime.Today.ToString("yyyy");
		this.lstToMM.SelectedValue = DateTime.Today.ToString("MM");
		this.lstToDD.SelectedValue = DateTime.Today.ToString("dd");
	}

	private void GetList() {
		if (this.rdoReportType.SelectedIndex == 0) {
			this.dsDailyUserAgeReport.SelectMethod = "GetListUsedPointReport";
		} else {
			this.dsDailyUserAgeReport.SelectMethod = "GetListPageViewReport";
		}
		this.grdDailyUserAgeReport.DataSourceID = "dsDailyUserAgeReport";
		this.grdDailyUserAgeReport.DataBind();
		this.lblNotice.DataBind();
	}

	protected string GetHeaderMark(object pHeaderText) {
		return string.Format("{0}{1}",ViCommConst.MAN.Equals(this.SexCd) ? "m" : "f",pHeaderText);
	}

	protected string GetNoticeMark() {
		return this.rdoReportType.SelectedIndex == 0 
			? "上段：消費ポイント、下段：課金UU | m=男性、f=女性、18=18-19歳、20=20-24歳、25=25-29歳、30=30歳以上" 
			: "上段：PV、下段：UU | m=男性、f=女性、18=18-19歳、20=20-24歳、25=25-29歳、30=30歳以上";
	}

	protected void btnCSV_Click(object sender,EventArgs e) {

		if (grdDailyUserAgeReport.Rows.Count == 0) {
			return;
		}

		DataTable oCsvData;
		if (this.rdoReportType.SelectedIndex == 0) {
			using (DailyUserAgeReport oDailyUserAgeReport = new DailyUserAgeReport()) {
				using (DataSet oDataSet = oDailyUserAgeReport.GetListUsedPointReport(
					this.lstSeekSiteCd.SelectedValue,
					this.SexCd,
					string.Format("{0}/{1}/{2}",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue),
					string.Format("{0}/{1}/{2}",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,this.lstToDD.SelectedValue))) {

					if (oDataSet.Tables[0].Rows.Count > 0) {
						oCsvData = oDataSet.Tables[0];
					} else {
						return;
					}
				}
			}
		} else {
			using (DailyUserAgeReport oDailyUserAgeReport = new DailyUserAgeReport()) {
				using (DataSet oDataSet = oDailyUserAgeReport.GetListPageViewReport(
					this.lstSeekSiteCd.SelectedValue,
					this.SexCd,
					string.Format("{0}/{1}/{2}",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue),
					string.Format("{0}/{1}/{2}",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,this.lstToDD.SelectedValue))) {

					if (oDataSet.Tables[0].Rows.Count > 0) {
						oCsvData = oDataSet.Tables[0];
					} else {
						return;
					}
				}
			}
		}

		//ヘッダ作成
		string sHeader = null;
		if (ViCommConst.MAN.Equals(this.SexCd)) {
			sHeader = "年月日,合計,m18,m20,m25,m30,mOther";
		} else {
			sHeader = "年月日,合計,f18,f20,f25,f30,fOther";
		}
		//CSV出力 
		Response.ContentType = "application/download";
		Response.AppendHeader("Content-Disposition","attachment;filename=DailyUserAgeReport.csv");
		Response.ContentEncoding = System.Text.Encoding.GetEncoding("Shift_JIS");

		Response.Write(sHeader + "\r\n");
		foreach (DataRow oCsvRow in oCsvData.Rows) {
			string sData =
				SetCsvString(oCsvRow["REPORT_DAY"].ToString()) + "," +
				SetCsvString(oCsvRow["COUNT_ALL"].ToString()) + "," +
				SetCsvString(oCsvRow["COUNT_18"].ToString()) + "," +
				SetCsvString(oCsvRow["COUNT_20"].ToString()) + "," +
				SetCsvString(oCsvRow["COUNT_25"].ToString()) + "," +
				SetCsvString(oCsvRow["COUNT_30"].ToString()) + "," +
				SetCsvString(oCsvRow["COUNT_OTHER"].ToString());
				
			Response.Write(sData + "\r\n");

			sData =
				SetCsvString(oCsvRow["REPORT_DAY"].ToString()) + "," +
				SetCsvString(oCsvRow["COUNT_UNIQUE_ALL"].ToString()) + "," +
				SetCsvString(oCsvRow["COUNT_UNIQUE_18"].ToString()) + "," +
				SetCsvString(oCsvRow["COUNT_UNIQUE_20"].ToString()) + "," +
				SetCsvString(oCsvRow["COUNT_UNIQUE_25"].ToString()) + "," +
				SetCsvString(oCsvRow["COUNT_UNIQUE_30"].ToString()) + "," +
				SetCsvString(oCsvRow["COUNT_UNIQUE_OTHER"].ToString());

			Response.Write(sData + "\r\n");
			
		}
		Response.End();
	}

	private string SetCsvString(string pData) {
		if (pData.IndexOf('"') > -1 ||
			pData.IndexOf(',') > -1 ||
			pData.IndexOf('\r') > -1 ||
			pData.IndexOf('\n') > -1 ||
			pData.StartsWith(" ") || pData.StartsWith("\t") ||
			pData.EndsWith(" ") || pData.EndsWith("\t")) {
			if (pData.IndexOf('"') > -1) {
				//"を""とする
				pData = pData.Replace("\"","\"\"");
			}
			if (pData.IndexOf('\r') > -1 || pData.IndexOf('\n') > -1) {
				//改行しない 
				pData = pData.Replace("\r","");
				pData = pData.Replace("\n","");
			}
			pData = "\"" + pData + "\"";
		}
		return pData;
	}
}