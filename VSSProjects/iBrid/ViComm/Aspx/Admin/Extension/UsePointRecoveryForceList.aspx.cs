﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ポイント使用部隊回復設定
--	Progaram ID		: UsePointRecoveryForceList
--
--  Creation Date	: 2012.01.25
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using iBridCommLib;
using System.Drawing;
using ViComm;

public partial class Extension_UsePointRecoveryForceList:System.Web.UI.Page {
	string recCount;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		if (!this.IsPostBack) {
			this.InitPage();
		}
	}

	protected void lnkSiteCd_Command(object sender,CommandEventArgs e) {
		string[] sArgumentArray = iBridUtil.GetStringValue(e.CommandArgument).Split(',');

		this.SiteCd = sArgumentArray[0];
		this.txtRecoveryForcePerPoint.Text = sArgumentArray[1];
		this.txtUsePointRecoveryForce.Text = sArgumentArray[2];
		int iMask = int.Parse(sArgumentArray[3]);
		foreach (ListItem oItem in this.chkRecoveryForce.Items) {
			oItem.Selected = (int.Parse(oItem.Value) & iMask) > 0;
		}
		this.pnlMainte.Visible = true;
		this.pnlDtl.Visible = true;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		int iMask = 0;
		foreach (ListItem oItem in this.chkRecoveryForce.Items) {
			iMask += oItem.Selected ? int.Parse(oItem.Value) : 0;
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_RECOVERY_FORCE_POINT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			db.ProcedureInParm("pUSE_POINT_RECOVERY_FORCE",DbSession.DbType.NUMBER,this.txtUsePointRecoveryForce.Text);
			db.ProcedureInParm("pRECOVERY_FORCE_PER_POINT",DbSession.DbType.NUMBER,this.txtRecoveryForcePerPoint.Text);
			db.ProcedureInParm("pRECOVERY_FORCE_MASK",DbSession.DbType.NUMBER,iMask);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		this.InitPage();
	}

	protected void dsSocialGame_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		this.pnlMainte.Visible = false;
		this.pnlDtl.Visible = false;
		this.ClearFields();
		this.GetList();
	}

	private void ClearFields() {
		this.pnlDtl.DataBind();
		this.txtRecoveryForcePerPoint.Text = string.Empty;
		this.txtUsePointRecoveryForce.Text = string.Empty;
	}

	private void GetList() {
		this.grdSocialGame.DataBind();
		this.pnlCount.DataBind();
	}

	protected string GetRecCount() {
		return this.recCount;
	}
}