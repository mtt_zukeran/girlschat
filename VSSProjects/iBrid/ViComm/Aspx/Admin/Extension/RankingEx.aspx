<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RankingEx.aspx.cs" Inherits="Extension_RankingEx" Title="Untitled Page"
    ValidateRequest="false" %>

<%@ Import Namespace="System" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="足あとランキング"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <fieldset class="fieldset">
            <legend>[検索条件]</legend>
            <asp:Panel runat="server" ID="pnlKey">
                <table border="0" style="width: 820px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            サイト
                        </td>
                        <td class="tdDataStyle" colspan="3">
                            <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" Enabled="false"
                                DataValueField="SITE_CD" Width="180px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            集計期間
                        </td>
                        <td class="tdDataStyle" style="width: 300px">
                            <asp:Label ID="lblDate" runat="server"></asp:Label>
                        </td>
                        <td class="tdHeaderStyle2">
                            備考
                        </td>
                        <td class="tdDataStyle">
                            <asp:Label ID="lblRemarks" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" />
                <asp:Button runat="server" ID="btnBack" Text="戻る" CssClass="seekbutton" OnClick="btnBack_Click" />
            </asp:Panel>
        </fieldset>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[ランキング]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%#GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%=grdRankingEx.PageIndex + 1%>
                        of
                        <%=grdRankingEx.PageCount%>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="500px">
                    <asp:GridView ID="grdRankingEx" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        EnableViewState="true" DataSourceID="dsRankingEx" AllowSorting="True" SkinID="GridViewColor"
                        OnRowDataBound="grdRankingEx_RowDataBound">
                        <Columns>
                            <asp:BoundField DataField="RNUM" HeaderText="順位" ItemStyle-HorizontalAlign="Right" />
                            <asp:TemplateField HeaderText="ログインID">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# GetViewUrl(Eval("SITE_CD"), Eval("LOGIN_ID")) %>'
                                        Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="HANDLE_NM" HeaderText="ハンドル名" />
                            <asp:BoundField DataField="SUMMARY_COUNT" HeaderText="集計数" ItemStyle-HorizontalAlign="Right" />
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <br />
    </div>
    <asp:ObjectDataSource ID="dsRankingEx" runat="server" SelectMethod="GetPageCollection"
        TypeName="RankingEx" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelected="dsRankingEx_Selected">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:QueryStringParameter Name="pRankingCtlSeq" QueryStringField="seq" Type="String" />
            <asp:QueryStringParameter Name="pSummaryType" QueryStringField="summarytype" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site">
    </asp:ObjectDataSource>
</asp:Content>
