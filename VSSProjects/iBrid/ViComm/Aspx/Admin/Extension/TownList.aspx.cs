﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 街一覧

--	Progaram ID		: TownList
--
--  Creation Date	: 2011.07.19
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.IO;

public partial class Extension_TownList : System.Web.UI.Page {
	protected string[] DummyArray = new string[3];
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		set {
			this.ViewState["SexCd"] = value;
		}
	}

	private string StageSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["StageSeq"]);
		}
		set {
			this.ViewState["StageSeq"] = value;
		}
	}

	private string TownSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["TownSeq"]);
		}
		set {
			this.ViewState["TownSeq"] = value;
		}
	}

	private string GameItemCategoryType {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemCategoryType"]);
		}
		set {
			this.ViewState["GameItemCategoryType"] = value;
		}
	}

	private string GameItemCategoryType2 {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemCategoryType2"]);
		}
		set {
			this.ViewState["GameItemCategoryType2"] = value;
		}
	}

	private string GameItemCategoryType3 {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemCategoryType3"]);
		}
		set {
			this.ViewState["GameItemCategoryType3"] = value;
		}
	}

	private string GameItemCategoryTypeUse {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemCategoryTypeUse"]);
		}
		set {
			this.ViewState["GameItemCategoryTypeUse"] = value;
		}
	}

	private string GameItemCategoryTypeUse2 {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemCategoryTypeUse2"]);
		}
		set {
			this.ViewState["GameItemCategoryTypeUse2"] = value;
		}
	}

	private string GameItemCategoryTypeUse3 {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemCategoryTypeUse3"]);
		}
		set {
			this.ViewState["GameItemCategoryTypeUse3"] = value;
		}
	}

	private string CreateFlag {
		get {
			return iBridUtil.GetStringValue(this.ViewState["CreateFlag"]);
		}
		set {
			this.ViewState["CreateFlag"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	private string GameItemGetCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemGetCd"]);
		}
		set {
			this.ViewState["GameItemGetCd"] = value;
		}
	}

	private string GameItemGetCd2 {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemGetCd2"]);
		}
		set {
			this.ViewState["GameItemGetCd2"] = value;
		}
	}

	private string GameItemGetCd3 {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemGetCd3"]);
		}
		set {
			this.ViewState["GameItemGetCd3"] = value;
		}
	}

	private string GameItemGetCdUse {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemGetCdUse"]);
		}
		set {
			this.ViewState["GameItemGetCdUse"] = value;
		}
	}

	private string GameItemGetCdUse2 {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemGetCdUse2"]);
		}
		set {
			this.ViewState["GameItemGetCdUse2"] = value;
		}
	}

	private string GameItemGetCdUse3 {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemGetCdUse3"]);
		}
		set {
			this.ViewState["GameItemGetCdUse3"] = value;
		}
	}
	
	private string GameItemPresent {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemPresent"]);
		}
		set {
			this.ViewState["GameItemPresent"] = value;
		}
	}

	private string GameItemPresent2 {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemPresent2"]);
		}
		set {
			this.ViewState["GameItemPresent2"] = value;
		}
	}	

	private string GameItemPresent3 {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemPresent3"]);
		}
		set {
			this.ViewState["GameItemPresent3"] = value;
		}
	}

	private string GameItemPresentUse {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemPresentUse"]);
		}
		set {
			this.ViewState["GameItemPresentUse"] = value;
		}
	}

	private string GameItemPresentUse2 {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemPresentUse2"]);
		}
		set {
			this.ViewState["GameItemPresentUse2"] = value;
		}
	}

	private string GameItemPresentUse3 {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemPresentUse3"]);
		}
		set {
			this.ViewState["GameItemPresentUse3"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();

			this.SiteCd = iBridUtil.GetStringValue(this.Request.QueryString["sitecd"]);
			this.StageSeq = iBridUtil.GetStringValue(this.Request.QueryString["stageseq"]);
			this.SexCd = iBridUtil.GetStringValue(this.Request.QueryString["sexcd"]);
			this.CreateFlag = iBridUtil.GetStringValue(this.Request.QueryString["create"]);

			if (!string.IsNullOrEmpty(this.SiteCd) && !string.IsNullOrEmpty(this.StageSeq)) {
				this.lstSiteCd.SelectedValue = this.SiteCd;
				this.lstStageSeq.SelectedValue = this.StageSeq;

				if (this.CreateFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					this.lstLevel.DataBind();
					this.TownSeq = string.Empty;
					this.pnlKey.Enabled = false;
					this.pnlTownInfo.Visible = true;
				}
				else {
					this.GetList();
				}
			}
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
		this.StageSeq = string.Empty;
		this.TownSeq = string.Empty;
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnBack_Click(object sender,EventArgs e) {
		this.Response.Redirect(string.Concat("~/Extension/StageList.aspx?sexcd=",this.SexCd));
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.UpdateData(false);
		this.pnlTownInfo.Visible = false;
		this.pnlKey.Enabled = true;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		this.UpdateData(true);
		this.pnlTownInfo.Visible = false;
		this.pnlKey.Enabled = true;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.ClearFileds();
		this.pnlTownInfo.Visible = false;
		this.pnlKey.Enabled = true;
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.StageSeq = this.lstStageSeq.SelectedValue;
		this.TownSeq = string.Empty;
		this.CreateFlag = ViCommConst.FLAG_ON_STR;
		this.pnlKey.Enabled = false;
		this.pnlTownInfo.Visible = true;
		this.lstLevel.DataBind();
	}

	protected void lnkEdit_OnCommand(object sender,CommandEventArgs e) {

		lstItemPresent1.SelectedValue = null;
		lstItemPresent2.SelectedValue = null;
		lstItemPresent3.SelectedValue = null;
		lstItemPresentUse1.SelectedValue = null;
		lstItemPresentUse2.SelectedValue = null;
		lstItemPresentUse3.SelectedValue = null;

		this.GameItemGetCd = null;
		this.GameItemGetCd2 = null;
		this.GameItemGetCd3 = null;
		this.GameItemGetCdUse = null;
		this.GameItemGetCdUse2 = null;
		this.GameItemGetCdUse3 = null;

		this.GameItemPresent = null;
		this.GameItemPresent2 = null;
		this.GameItemPresent3 = null;
		this.GameItemPresentUse = null;
		this.GameItemPresentUse2 = null;
		this.GameItemPresentUse3 = null;
		
		if (!this.IsValid) {
			return;
		}
		int iIndex = int.Parse(iBridUtil.GetStringValue(e.CommandArgument));

		GridViewRow oRow = this.grdTown.Rows[iIndex];
		HiddenField oStageSeqHiddenField = oRow.FindControl("hdnStageSeq") as HiddenField;
		HiddenField oTownSeqHiddenFiled = oRow.FindControl("hdnTownSeq") as HiddenField;

		this.StageSeq = oStageSeqHiddenField.Value;
		this.TownSeq = oTownSeqHiddenFiled.Value;
		this.CreateFlag = string.Empty;
		this.pnlKey.Enabled = false;
		this.lstLevel.DataBind();
		this.GetData();

		this.pnlTownInfo.Visible = true;
	}

	protected void dsTown_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	protected void lstLevel_DataBound(object sender,EventArgs e) {
		DropDownList oDropDwonList = sender as DropDownList;
		if (oDropDwonList != null) {
			if (oDropDwonList.Items.Count > 0) {
				ListItem oLastListItem = oDropDwonList.Items[oDropDwonList.Items.Count - 1];
				if (ViCommConst.FLAG_ON_STR.Equals(this.CreateFlag)) {
					oDropDwonList.Items.Add(new ListItem("最後",(int.Parse(oLastListItem.Value) + 1).ToString()));
				}
			}
			else {
				oDropDwonList.Items.Add(new ListItem("最初","1"));
			}
			oDropDwonList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		}
	}

	protected void lstGameItemCategory_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		switch (oDropDownList.ID) {
			case "lstGameItemCategoryGet1":
				this.GameItemCategoryType = oDropDownList.SelectedValue;
				this.lstGameItemGet1.DataBind();
				break;
			case "lstGameItemCategoryGet2":
				this.GameItemCategoryType2 = oDropDownList.SelectedValue;
				this.lstGameItemGet2.DataBind();
				break;
			case "lstGameItemCategoryGet3":
				this.GameItemCategoryType3 = oDropDownList.SelectedValue;
				this.lstGameItemGet3.DataBind();
				break;
			case "lstGameItemCategoryUse1":
				this.GameItemCategoryTypeUse = oDropDownList.SelectedValue;
				this.lstGameItemUse1.DataBind();
				break;
			case "lstGameItemCategoryUse2":
				this.GameItemCategoryTypeUse2 = oDropDownList.SelectedValue;
				this.lstGameItemUse2.DataBind();
				break;
			case "lstGameItemCategoryUse3":
				this.GameItemCategoryTypeUse3 = oDropDownList.SelectedValue;
				this.lstGameItemUse3.DataBind();
				break;
		}
	}

	protected void lstItemGetCd_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		switch (oDropDownList.ID) {
			case "lstItemGetCd1":
				this.GameItemGetCd = oDropDownList.SelectedValue;
				this.lstGameItemGet1.DataBind();
				break;
			case "lstItemGetCd2":
				this.GameItemGetCd2 = oDropDownList.SelectedValue;
				this.lstGameItemGet2.DataBind();
				break;
			case "lstItemGetCd3":
				this.GameItemGetCd3 = oDropDownList.SelectedValue;
				this.lstGameItemGet3.DataBind();
				break;
			case "lstItemGetCdUse1":
				this.GameItemGetCdUse = oDropDownList.SelectedValue;
				this.lstGameItemUse1.DataBind();
				break;
			case "lstItemGetCdUse2":
				this.GameItemGetCdUse2 = oDropDownList.SelectedValue;
				this.lstGameItemUse2.DataBind();
				break;
			case "lstItemGetCdUse3":
				this.GameItemGetCdUse3 = oDropDownList.SelectedValue;
				this.lstGameItemUse3.DataBind();
				break;
		}
	}

	protected void lstItemPresent_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		switch (oDropDownList.ID) {
			case "lstItemPresent1":
				this.GameItemPresent = oDropDownList.SelectedValue;
				this.lstGameItemGet1.DataBind();
				break;
			case "lstItemPresent2":
				this.GameItemPresent2 = oDropDownList.SelectedValue;
				this.lstGameItemGet2.DataBind();
				break;
			case "lstItemPresent3":
				this.GameItemPresent3 = oDropDownList.SelectedValue;
				this.lstGameItemGet3.DataBind();
				break;
			case "lstItemPresentUse1":
				this.GameItemPresentUse = oDropDownList.SelectedValue;
				this.lstGameItemUse1.DataBind();
				break;
			case "lstItemPresentUse2":
				this.GameItemPresentUse2 = oDropDownList.SelectedValue;
				this.lstGameItemUse2.DataBind();
				break;
			case "lstItemPresentUse3":
				this.GameItemPresentUse3 = oDropDownList.SelectedValue;
				this.lstGameItemUse3.DataBind();
				break;
		}
	}
	
	protected void lstSiteCd_SelectedIndexChanged(object sender,EventArgs e) {
		this.lstStageSeq.DataBind();
	}

	protected void dsGameItem_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pGameItemCategoryType"] = this.GameItemCategoryType;
		e.InputParameters["pGameItemGetCd"] = this.GameItemGetCd;
		e.InputParameters["pPresentFlag"] = this.GameItemPresent;
	}

	protected void dsGameItem2_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pGameItemCategoryType"] = this.GameItemCategoryType2;
		e.InputParameters["pGameItemGetCd"] = this.GameItemGetCd2;
		e.InputParameters["pPresentFlag"] = this.GameItemPresent2;
	}

	protected void dsGameItem3_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pGameItemCategoryType"] = this.GameItemCategoryType3;
		e.InputParameters["pGameItemGetCd"] = this.GameItemGetCd3;
		e.InputParameters["pPresentFlag"] = this.GameItemPresent3;
	}

	protected void dsGameItemUse_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pGameItemCategoryType"] = this.GameItemCategoryTypeUse;
		e.InputParameters["pGameItemGetCd"] = this.GameItemGetCdUse;
		e.InputParameters["pPresentFlag"] = this.GameItemPresentUse;
	}

	protected void dsGameItemUse2_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pGameItemCategoryType"] = this.GameItemCategoryTypeUse2;
		e.InputParameters["pGameItemGetCd"] = this.GameItemGetCdUse2;
		e.InputParameters["pPresentFlag"] = this.GameItemPresentUse2;
	}

	protected void dsGameItemUse3_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pGameItemCategoryType"] = this.GameItemCategoryTypeUse3;
		e.InputParameters["pGameItemGetCd"] = this.GameItemGetCdUse3;
		e.InputParameters["pPresentFlag"] = this.GameItemPresentUse3;
	}
	
	protected void dsTownLevel_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pStageSeq"] = this.StageSeq;
	}

	protected void vdcGameItemGet1_ServerValidate(object source,ServerValidateEventArgs args) {
		if (string.IsNullOrEmpty(this.lstGameItemGet1.SelectedValue)) {
			return;
		}

		if (string.IsNullOrEmpty(this.txtGameItemGetCount1.Text)) {
			args.IsValid = false;
			this.vdcGameItemGet1.ErrorMessage = "個数を入力してください。";
		}
		if (string.IsNullOrEmpty(this.txtGameItemGetDropRate1.Text)) {
			args.IsValid = false;
			this.vdcGameItemGet1.ErrorMessage = "ﾄﾞﾛｯﾌﾟ率を入力してください。";
		}
	}

	protected void vdcGameItemGet2_ServerValidate(object source,ServerValidateEventArgs args) {
		if (string.IsNullOrEmpty(this.lstGameItemGet2.SelectedValue)) {
			return;
		}

		if (string.IsNullOrEmpty(this.txtGameItemGetCount2.Text)) {
			args.IsValid = false;
			this.vdcGameItemGet2.ErrorMessage = "個数を入力してください。";
		}
		if (string.IsNullOrEmpty(this.txtGameItemGetDropRate2.Text)) {
			args.IsValid = false;
			this.vdcGameItemGet2.ErrorMessage = "ﾄﾞﾛｯﾌﾟ率を入力してください。";
		}
	}

	protected void vdcGameItemGet3_ServerValidate(object source,ServerValidateEventArgs args) {
		if (string.IsNullOrEmpty(this.lstGameItemGet3.SelectedValue)) {
			return;
		}

		if (string.IsNullOrEmpty(this.txtGameItemGetCount3.Text)) {
			args.IsValid = false;
			this.vdcGameItemGet3.ErrorMessage = "個数を入力してください。";
		}
		if (string.IsNullOrEmpty(this.txtGameItemGetDropRate3.Text)) {
			args.IsValid = false;
			this.vdcGameItemGet3.ErrorMessage = "ﾄﾞﾛｯﾌﾟ率を入力してください。";
		}
	}

	protected void vdcGameItemGetDropRate_ServerValidate(object source,ServerValidateEventArgs args) {
		int iRate = 0;
		if (!string.IsNullOrEmpty(this.lstGameItemGet1.SelectedValue) && !string.IsNullOrEmpty(this.txtGameItemGetDropRate1.Text)) {
			iRate += int.Parse(this.txtGameItemGetDropRate1.Text);
		}
		if (!string.IsNullOrEmpty(this.lstGameItemGet2.SelectedValue) && !string.IsNullOrEmpty(this.txtGameItemGetDropRate2.Text)) {
			iRate += int.Parse(this.txtGameItemGetDropRate2.Text);
		}
		if (!string.IsNullOrEmpty(this.lstGameItemGet3.SelectedValue) && !string.IsNullOrEmpty(this.txtGameItemGetDropRate3.Text)) {
			iRate += int.Parse(this.txtGameItemGetDropRate3.Text);
		}

		if (iRate > 100) {
			args.IsValid = false;
			this.vdcGameItemGetDropRate.ErrorMessage = "ﾄﾞﾛｯﾌﾟ率の合計が100％以下になるよう設定してください。";
		}
	}

	protected void vdcGameItemUse1_ServerValidate(object source,ServerValidateEventArgs args) {
		if (string.IsNullOrEmpty(this.lstGameItemUse1.SelectedValue)) {
			return;
		}

		if (string.IsNullOrEmpty(this.txtGameItemUseCount1.Text)) {
			args.IsValid = false;
			this.vdcGameItemUse1.ErrorMessage = "個数を入力してください。";
		}

		using(GameItem oGameItem = new GameItem()){
			if (!oGameItem.IsExists(this.SiteCd, this.lstGameItemUse1.SelectedValue,this.lstStageSeq.SelectedValue)) {
				args.IsValid = false;
				this.vdcGameItemUse1.ErrorMessage = "選ばれたアイテムは入手できない可能性があるため選択できません。";
			}
		}
	}

	protected void vdcGameItemUse2_ServerValidate(object source,ServerValidateEventArgs args) {
		if (string.IsNullOrEmpty(this.lstGameItemUse2.SelectedValue)) {
			return;
		}

		if (string.IsNullOrEmpty(this.txtGameItemUseCount2.Text)) {
			args.IsValid = false;
			this.vdcGameItemUse2.ErrorMessage = "個数を入力してください。";
		}

		using (GameItem oGameItem = new GameItem()) {
			if (!oGameItem.IsExists(this.SiteCd, this.lstGameItemUse2.SelectedValue, this.lstStageSeq.SelectedValue)) {
				args.IsValid = false;
				this.vdcGameItemUse2.ErrorMessage = "選ばれたアイテムは入手できない可能性があるため選択できません。";
			}
		}
	}

	protected void vdcGameItemUse3_ServerValidate(object source,ServerValidateEventArgs args) {
		if (string.IsNullOrEmpty(this.lstGameItemUse3.SelectedValue)) {
			return;
		}

		if (string.IsNullOrEmpty(this.txtGameItemUseCount3.Text)) {
			args.IsValid = false;
			this.vdcGameItemUse3.ErrorMessage = "個数を入力してください。";
		}

		using (GameItem oGameItem = new GameItem()) {
			if (!oGameItem.IsExists(this.SiteCd, this.lstGameItemUse3.SelectedValue, this.lstStageSeq.SelectedValue)) {
				args.IsValid = false;
				this.vdcGameItemUse3.ErrorMessage = "選ばれたアイテムは入手できない可能性があるため選択できません。";
			}
		}
	}

	private void InitPage() {
		this.pnlInfo.Visible = true;
		this.pnlTownInfo.Visible = false;

		this.StageSeq = string.Empty;
		this.TownSeq = string.Empty;
		this.pnlKey.Enabled = true;
		this.lstSiteCd.DataBind();
		this.lstStageSeq.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void ClearFileds() {
		this.lblTownId.Text = string.Empty;
		this.txtExp.Text = string.Empty;
		this.txtLossForceCount.Text = string.Empty;
		this.txtTargetCount.Text = string.Empty;
		this.txtIncomeMax.Text = string.Empty;
		this.txtIncomeMin.Text = string.Empty;
		//this.txtTownLevel.Text = string.Empty;
		this.txtTownNm.Text = string.Empty;
		this.txtTownNmExtra.Text = string.Empty;
		this.txtGameItemGetCount1.Text = string.Empty;
		this.txtGameItemGetCount2.Text = string.Empty;
		this.txtGameItemGetCount3.Text = string.Empty;
		this.txtGameItemGetDropRate1.Text = string.Empty;
		this.txtGameItemGetDropRate2.Text = string.Empty;
		this.txtGameItemGetDropRate3.Text = string.Empty;
		this.txtGameItemUseCount1.Text = string.Empty;
		this.txtGameItemUseCount2.Text = string.Empty;
		this.txtGameItemUseCount3.Text = string.Empty;
		this.pnlGetItem.DataBind();
		this.pnlUseItem.DataBind();

		this.GameItemCategoryType = null;
		this.GameItemCategoryType2 = null;
		this.GameItemCategoryType3 = null;
		this.GameItemCategoryTypeUse = null;
		this.GameItemCategoryTypeUse2 = null;
		this.GameItemCategoryTypeUse3 = null;
		this.GameItemGetCd = null;
		this.GameItemGetCd2 = null;
		this.GameItemGetCd3 = null;
		this.GameItemGetCdUse = null;
		this.GameItemGetCdUse2 = null;
		this.GameItemGetCdUse3 = null;
		this.GameItemPresent = null;
		this.GameItemPresent2 = null;
		this.GameItemPresent3 = null;
		this.GameItemPresentUse = null;
		this.GameItemPresentUse2 = null;
		this.GameItemPresentUse3 = null;
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.StageSeq = string.Empty;
		this.TownSeq = string.Empty;

		this.pnlGetItem.DataBind();
		this.pnlUseItem.DataBind();

		this.grdTown.PageIndex = 0;
		this.grdTown.DataSourceID = "dsTown";
		this.grdTown.DataBind();
		this.pnlCount.DataBind();
	}

	private void GetData() {
		this.ClearFileds();

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("TOWN_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pSTAGE_SEQ",DbSession.DbType.VARCHAR2,this.StageSeq);
			oDbSession.ProcedureInParm("pTOWN_SEQ",DbSession.DbType.VARCHAR2,this.TownSeq);
			oDbSession.ProcedureOutParm("pTOWN_NM",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTOWN_NM_EXTRA",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTOWN_LEVEL",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSEX_CD",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pEXP",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pLOSS_FORCE_COUNT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pINCOME_MIN",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pINCOME_MAX",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTARGET_COUNT",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutArrayParm("pGET_ITEM_SEQ_ARR",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutArrayParm("pGET_ITEM_COUNT_ARR",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutArrayParm("pGET_ITEM_CATEGORY_TYPE_ARR",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutArrayParm("pGET_ITEM_DROP_RATE_ARR",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutArrayParm("pUSE_ITEM_SEQ_ARR",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutArrayParm("pUSE_ITEM_COUNT_ARR",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutArrayParm("pUSE_ITEM_CATEGORY_TYPE_ARR",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.lblTownId.Text = oDbSession.GetStringValue("pTOWN_SEQ");
			this.txtTownNm.Text = oDbSession.GetStringValue("pTOWN_NM");
			this.txtTownNmExtra.Text = oDbSession.GetStringValue("pTOWN_NM_EXTRA");
			this.lstLevel.SelectedValue = oDbSession.GetStringValue("pTOWN_LEVEL");
			//this.txtTownLevel.Text = oDbSession.GetStringValue("pSTAGE_LEVEL");
			this.txtLossForceCount.Text = oDbSession.GetStringValue("pLOSS_FORCE_COUNT");
			this.txtIncomeMin.Text = oDbSession.GetStringValue("pINCOME_MIN");
			this.txtIncomeMax.Text = oDbSession.GetStringValue("pINCOME_MAX");
			this.txtTargetCount.Text = oDbSession.GetStringValue("pTARGET_COUNT");
			this.txtExp.Text = oDbSession.GetStringValue("pEXP");
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");

			this.lstGameItemCategoryGet1.SelectedValue = oDbSession.GetArryIntValue("pGET_ITEM_CATEGORY_TYPE_ARR",0);
			this.GameItemCategoryType = this.lstGameItemCategoryGet1.SelectedValue;
			this.lstGameItemGet1.DataBind();
			this.lstGameItemCategoryGet2.SelectedValue = oDbSession.GetArryIntValue("pGET_ITEM_CATEGORY_TYPE_ARR",1);
			this.GameItemCategoryType2 = this.lstGameItemCategoryGet2.SelectedValue;
			this.lstGameItemGet2.DataBind();
			this.lstGameItemCategoryGet3.SelectedValue = oDbSession.GetArryIntValue("pGET_ITEM_CATEGORY_TYPE_ARR",2);
			this.GameItemCategoryType3 = this.lstGameItemCategoryGet3.SelectedValue;
			this.lstGameItemGet3.DataBind();

			this.lstGameItemGet1.SelectedValue = oDbSession.GetArryIntValue("pGET_ITEM_SEQ_ARR",0);
			this.lstGameItemGet2.SelectedValue = oDbSession.GetArryIntValue("pGET_ITEM_SEQ_ARR",1);
			this.lstGameItemGet3.SelectedValue = oDbSession.GetArryIntValue("pGET_ITEM_SEQ_ARR",2);
			this.txtGameItemGetCount1.Text = oDbSession.GetArryIntValue("pGET_ITEM_COUNT_ARR",0);
			this.txtGameItemGetCount2.Text = oDbSession.GetArryIntValue("pGET_ITEM_COUNT_ARR",1);
			this.txtGameItemGetCount3.Text = oDbSession.GetArryIntValue("pGET_ITEM_COUNT_ARR",2);
			this.txtGameItemGetDropRate1.Text = oDbSession.GetArryIntValue("pGET_ITEM_DROP_RATE_ARR",0);
			this.txtGameItemGetDropRate2.Text = oDbSession.GetArryIntValue("pGET_ITEM_DROP_RATE_ARR",1);
			this.txtGameItemGetDropRate3.Text = oDbSession.GetArryIntValue("pGET_ITEM_DROP_RATE_ARR",2);

			this.lstGameItemCategoryUse1.SelectedValue = oDbSession.GetArryIntValue("pUSE_ITEM_CATEGORY_TYPE_ARR",0);
			this.GameItemCategoryTypeUse = this.lstGameItemCategoryUse1.SelectedValue;
			this.lstGameItemUse1.DataBind();
			this.lstGameItemCategoryUse2.SelectedValue = oDbSession.GetArryIntValue("pUSE_ITEM_CATEGORY_TYPE_ARR",1);
			this.GameItemCategoryTypeUse2 = this.lstGameItemCategoryUse2.SelectedValue;
			this.lstGameItemUse2.DataBind();
			this.lstGameItemCategoryUse3.SelectedValue = oDbSession.GetArryIntValue("pUSE_ITEM_CATEGORY_TYPE_ARR",2);
			this.GameItemCategoryTypeUse3 = this.lstGameItemCategoryUse3.SelectedValue;
			this.lstGameItemUse3.DataBind();

			this.lstGameItemUse1.SelectedValue = oDbSession.GetArryIntValue("pUSE_ITEM_SEQ_ARR",0);
			this.lstGameItemUse2.SelectedValue = oDbSession.GetArryIntValue("pUSE_ITEM_SEQ_ARR",1);
			this.lstGameItemUse3.SelectedValue = oDbSession.GetArryIntValue("pUSE_ITEM_SEQ_ARR",2);
			this.txtGameItemUseCount1.Text = oDbSession.GetArryIntValue("pUSE_ITEM_COUNT_ARR",0);
			this.txtGameItemUseCount2.Text = oDbSession.GetArryIntValue("pUSE_ITEM_COUNT_ARR",1);
			this.txtGameItemUseCount3.Text = oDbSession.GetArryIntValue("pUSE_ITEM_COUNT_ARR",2);
		}
	}

	private void UpdateData(bool pDelFlag) {
		List<string> oGetItemSeqList = new List<string>();
		List<string> oGetItemCountList = new List<string>();
		List<string> oGetItemDropRateList = new List<string>();
		if (!string.IsNullOrEmpty(this.lstGameItemGet1.SelectedValue)) {
			oGetItemSeqList.Add(this.lstGameItemGet1.SelectedValue);
			oGetItemCountList.Add(this.txtGameItemGetCount1.Text);
			oGetItemDropRateList.Add(this.txtGameItemGetDropRate1.Text);
		}
		if (!string.IsNullOrEmpty(this.lstGameItemGet2.SelectedValue)) {
			oGetItemSeqList.Add(this.lstGameItemGet2.SelectedValue);
			oGetItemCountList.Add(this.txtGameItemGetCount2.Text);
			oGetItemDropRateList.Add(this.txtGameItemGetDropRate2.Text);
		}
		if (!string.IsNullOrEmpty(this.lstGameItemGet3.SelectedValue)) {
			oGetItemSeqList.Add(this.lstGameItemGet3.SelectedValue);
			oGetItemCountList.Add(this.txtGameItemGetCount3.Text);
			oGetItemDropRateList.Add(this.txtGameItemGetDropRate3.Text);
		}

		List<string> oUseItemSeqList = new List<string>();
		List<string> oUseItemCountList = new List<string>();
		if (!string.IsNullOrEmpty(this.lstGameItemUse1.SelectedValue)) {
			oUseItemSeqList.Add(this.lstGameItemUse1.SelectedValue);
			oUseItemCountList.Add(this.txtGameItemUseCount1.Text);
		}
		if (!string.IsNullOrEmpty(this.lstGameItemUse2.SelectedValue)) {
			oUseItemSeqList.Add(this.lstGameItemUse2.SelectedValue);
			oUseItemCountList.Add(this.txtGameItemUseCount2.Text);
		}
		if (!string.IsNullOrEmpty(this.lstGameItemUse3.SelectedValue)) {
			oUseItemSeqList.Add(this.lstGameItemUse3.SelectedValue);
			oUseItemCountList.Add(this.txtGameItemUseCount3.Text);
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("TOWN_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pSTAGE_SEQ",DbSession.DbType.VARCHAR2,this.StageSeq);
			oDbSession.ProcedureBothParm("pTOWN_SEQ",DbSession.DbType.VARCHAR2,this.TownSeq);
			oDbSession.ProcedureInParm("pTOWN_NM",DbSession.DbType.VARCHAR2,this.txtTownNm.Text);
			oDbSession.ProcedureInParm("pTOWN_NM_EXTRA",DbSession.DbType.VARCHAR2,this.txtTownNmExtra.Text);
			oDbSession.ProcedureInParm("pTOWN_LEVEL",DbSession.DbType.VARCHAR2,this.lstLevel.SelectedValue);
			oDbSession.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,this.SexCd);
			oDbSession.ProcedureInParm("pEXP",DbSession.DbType.VARCHAR2,this.txtExp.Text);
			oDbSession.ProcedureInParm("pLOSS_FORCE_COUNT",DbSession.DbType.VARCHAR2,this.txtLossForceCount.Text);
			oDbSession.ProcedureInParm("pINCOME_MIN",DbSession.DbType.VARCHAR2,this.txtIncomeMin.Text);
			oDbSession.ProcedureInParm("pINCOME_MAX",DbSession.DbType.VARCHAR2,this.txtIncomeMax.Text);
			oDbSession.ProcedureInParm("pTARGET_COUNT",DbSession.DbType.VARCHAR2,this.txtTargetCount.Text);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.VARCHAR2,this.RevisionNo);
			oDbSession.ProcedureInArrayParm("pGET_ITEM_SEQ_ARR",DbSession.DbType.NUMBER,oGetItemSeqList.ToArray());
			oDbSession.ProcedureInArrayParm("pGET_ITEM_COUNT_ARR",DbSession.DbType.NUMBER,oGetItemCountList.ToArray());
			oDbSession.ProcedureInArrayParm("pGET_ITEM_DROP_RATE_ARR",DbSession.DbType.NUMBER,oGetItemDropRateList.ToArray());
			oDbSession.ProcedureInArrayParm("pUSE_ITEM_SEQ_ARR",DbSession.DbType.NUMBER,oUseItemSeqList.ToArray());
			oDbSession.ProcedureInArrayParm("pUSE_ITEM_COUNT_ARR",DbSession.DbType.NUMBER,oUseItemCountList.ToArray());
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDelFlag ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.ClearFileds();
		this.GetList();
	}

	protected void lstItemGetCd_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		oDropDownList.Items.Insert(3,new ListItem("戦利品","3"));
	}
}
