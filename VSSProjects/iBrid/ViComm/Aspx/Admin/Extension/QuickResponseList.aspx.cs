﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 10分以内の返事一覧
--	Progaram ID		: QuickResponseList
--
--  Creation Date	: 2011.06.03
--  Creater			: iBrid
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using iBridCommLib;
using ViComm;

public partial class Extension_QuickResponseList : System.Web.UI.Page {
	#region Sorting

	private string SortExpression {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortExpression"]);
		}
		set {
			this.ViewState["SortExpression"] = value;
		}
	}

	private string SortDirect {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortDirect"]);
		}
		set {
			this.ViewState["SortDirect"] = value;
		}
	}
	#endregion

	private string recCount = string.Empty;

	protected void Page_Load(object sender, EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();

			this.pnlInfo.Visible = true;
			this.GetList();
		}

		this.lblMessage.Visible = false;
	}

	protected void btnListSeek_Click(object sender, EventArgs e) {
		this.pnlInfo.Visible = true;
		this.GetList();
	}

	protected void btnClear_Click(object sender, EventArgs e) {
		this.InitPage();
	}

	protected void lnkExit_Command(object sender, CommandEventArgs e) {
		if (!e.CommandName.Equals("EXIT")) {
			return;
		}

		int iRowIndex = int.Parse(e.CommandArgument.ToString());
		IDictionary oDataKeys = this.grdQuickResponse.DataKeys[iRowIndex].Values;

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("QUICK_RESPONSE_ROOM");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.lstSeekSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pUSER_SEQ", DbSession.DbType.VARCHAR2, oDataKeys["USER_SEQ"]);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO", DbSession.DbType.VARCHAR2, oDataKeys["USER_CHAR_NO"]);
			oDbSession.ProcedureInParm("pIN_OUT_TYPE", DbSession.DbType.VARCHAR2, ViCommConst.QuickResponse.OUT);
			oDbSession.ProcedureOutParm("pRESULT", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.lblMessage.Visible = true;
		this.GetList();
	}

	protected void dsQuickResponse_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pLoginId"] = this.txtLoginId.Text.TrimEnd();
		e.InputParameters["pSortExpression"] = this.SortExpression;
		e.InputParameters["pSortDirection"] = this.SortDirect;
	}

	protected void dsQuickResponse_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void grdQuickResponse_RowDataBound(object sender, GridViewRowEventArgs e) {
		if (e.Row.RowType != DataControlRowType.DataRow) {
			return;
		}

		if ((e.Row.FindControl("lblExit") as Label).Text.EndsWith("退室済")) {
			e.Row.BackColor = Color.Gainsboro;
		}
	}

	protected void grdQuickResponse_Sorting(object sender, GridViewSortEventArgs e) {
		e.Cancel = true;

		if (this.SortExpression.Equals(e.SortExpression)) {
			if (this.SortDirect.Equals("ASC")) {
				this.SortDirect = "DESC";
			} else if (this.SortDirect.Equals("DESC")) {
				this.SortDirect = "ASC";
			}
		} else {
			this.SortDirect = "ASC";
		}

		this.SortExpression = e.SortExpression;

		this.GetList();
	}

	private void InitPage() {
		this.recCount = "0";
		this.txtLoginId.Text = string.Empty;
		this.grdQuickResponse.DataSourceID = string.Empty;
		this.pnlInfo.Visible = false;
		this.lstSeekSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		this.lstExitStatus.SelectedIndex = 0;
		this.SortExpression = string.Empty;
		this.SortDirect = string.Empty;
	}

	protected string GetRecCount() {
		return recCount;
	}

	private void GetList() {
		this.grdQuickResponse.PageIndex = 0;

		this.grdQuickResponse.DataSourceID = "dsQuickResponse";
		this.grdQuickResponse.DataBind();
		this.pnlCount.DataBind();
	}

	protected bool GetExitVisible(object pExitStatus) {
		if (pExitStatus.ToString().EndsWith("退室済")) {
			return false;
		}
		return true;
	}
}
