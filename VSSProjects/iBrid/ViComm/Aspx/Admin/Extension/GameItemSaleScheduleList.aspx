﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="GameItemSaleScheduleList.aspx.cs" Inherits="Extension_GameItemSaleScheduleList"
    Title="アイテムセールスケジュール" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="アイテムセールスケジュール"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 600px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle2">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton" ValidationGroup="Create"
                    OnClick="btnCreate_Click" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" Visible="false" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlGameItemSaleScheduleInfo">
            <fieldset class="fieldset">
                <legend>[セール情報]</legend>
                <table border="0" style="width: 600px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            セールコメント
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtSaleComment" runat="server" Height="30px" MaxLength="1024" Rows="3"
                                TextMode="MultiLine" Width="300px"></asp:TextBox>
                            <br />
                            <asp:Label ID="lblErrorMessageSaleComment" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            セール予告コメント
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtSaleNoticeComment" runat="server" Height="30px" MaxLength="1024" Rows="3"
                                TextMode="MultiLine" Width="300px"></asp:TextBox>
                            <br />
                            <asp:Label ID="lblErrorMessageSaleNoticeCommnet" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            セール開始日時
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtSaleStartDate" runat="server" MaxLength="16" Width="120px"></asp:TextBox>
                            <ajaxToolkit:MaskedEditExtender ID="mskSaleStartDate" runat="server" MaskType="DateTime"
                                Mask="9999/99/99 99:99:99" UserDateFormat="YearMonthDay" UserTimeFormat="TwentyFourHour"
                                ClearMaskOnLostFocus="true" TargetControlID="txtSaleStartDate">
                            </ajaxToolkit:MaskedEditExtender>
                            <br />
                            <asp:Label ID="lblErrorMessageSaleStart" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            セール終了日時
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtSaleEndDate" runat="server" MaxLength="16" Width="120px"></asp:TextBox>
                            <ajaxToolkit:MaskedEditExtender ID="mskSaleEndDate" runat="server" MaskType="DateTime"
                                Mask="9999/99/99 99:99:99" UserDateFormat="YearMonthDay" UserTimeFormat="TwentyFourHour"
                                ClearMaskOnLostFocus="true" TargetControlID="txtSaleEndDate">
                            </ajaxToolkit:MaskedEditExtender>
                            <br />
                            <asp:Label ID="lblErrorMessageSaleEnd" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            備考
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtRemarks" runat="server" Height="30px" MaxLength="1024" Rows="3"
                                TextMode="MultiLine" Width="300px"></asp:TextBox>
                            <br />
                            <asp:Label ID="lblErrorMessageRemarks" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="pnlCommand" runat="server">
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Update"
                        OnClick="btnUpdate_Click" />
                    <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" ValidationGroup="Delete"
                        OnClick="btnDelete_Click" />
                    <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" CausesValidation="False"
                        Visible="true" OnClick="btnCancel_Click" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[スケジュール]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdGameItemSaleSchedule.PageIndex + 1 %>
                        of
                        <%= grdGameItemSaleSchedule.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="600px">
                    <asp:GridView ID="grdGameItemSaleSchedule" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        PageSize="30" EnableSortingAndPagingCallbacks="false" DataSourceID="dsGameItemSaleSchedule"
                        SkinID="GridViewColor" Font-Size="small" AllowSorting="true">
                        <Columns>
                            <asp:TemplateField HeaderText="セールID">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%# Eval("SALE_SCHEDULE_SEQ") %>'
                                        Text='<%# Eval("SALE_SCHEDULE_SEQ") %>' OnCommand="lnkEdit_Command"></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="SALE_COMMENT" HeaderText="セールコメント">
                                <ItemStyle HorizontalAlign="Left" Width="250px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="SALE_NOTICE_COMMENT" HeaderText="セール予告コメント">
                                <ItemStyle HorizontalAlign="Left" Width="250px" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="セール開始日時">
                                <ItemTemplate>
                                    <asp:Label ID="lblSaleStartDate" runat="server" Text='<%# Eval("SALE_START_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="セール終了日時">
                                <ItemTemplate>
                                    <asp:Label ID="lblSaleEndDate" runat="server" Text='<%# Eval("SALE_END_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="REMARKS" HeaderText="備考">
                                <ItemStyle HorizontalAlign="Left" Width="150px" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkTownList" runat="server" NavigateUrl='<%# string.Format("~/Extension/GameItemSaleList.aspx?sitecd={0}&salescheduleseq={1}&sexcd={2}", Eval("SITE_CD"), Eval("SALE_SCHEDULE_SEQ"), Eval("SEX_CD"))%>'
                                        Text="アイテム一覧"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsGameItemSaleSchedule" runat="server" SelectMethod="GetPageCollection"
        ConvertNullToDBNull="false" SortParameterName="" TypeName="GameItemSaleSchedule" SelectCountMethod="GetPageCount"
        EnablePaging="True" OnSelected="dsGameItemSaleSchedule_Selected">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="セール情報を更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete"
        ConfirmText="セール情報を削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>
