﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="GameLoginBonusDayList.aspx.cs" Inherits="Extension_GameLoginBonusDayList"
    Title="日別ログインボーナスアイテム一覧" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="日別ログインボーナスアイテム一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 400px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle2">
                                サイトコード
                            </td>
                            <td class="tdDataStyle" style="width: 200px">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton" ValidationGroup="Create"
                    OnClick="btnCreate_Click" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" Visible="false" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlItemInfo">
            <fieldset class="fieldset">
                <legend>[ボーナスアイテム情報]</legend>
                <table border="0" style="width: 650px" class="tableStyle">
                    <tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                ボーナス開始日
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstFromYYYY" runat="server" Width="60px" DataSource='<%# YearArray %>'>
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstFromMM" runat="server" Width="40px" DataSource='<%# MonthArray %>'>
                                </asp:DropDownList>月
                                <asp:DropDownList ID="lstFromDD" runat="server" Width="40px" DataSource='<%# DayArray %>'>
                                </asp:DropDownList>日
                                <asp:DropDownList ID="lstFromHH" runat="server" Width="40px" DataSource='<%# HourArray %>'>
                                </asp:DropDownList>時
                                <asp:DropDownList ID="lstFromMI" runat="server" DataSource='<%# MinuteArray %>'>
                                </asp:DropDownList>分
                                <asp:Label ID="lblErrorMessageFrom" runat="server" Text="" Visible="false" ForeColor="red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                ボーナス終了日
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstToYYYY" runat="server" Width="60px" DataSource='<%# YearArray %>'>
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstToMM" runat="server" Width="40px" DataSource='<%# MonthArray %>'>
                                </asp:DropDownList>月
                                <asp:DropDownList ID="lstToDD" runat="server" Width="40px" DataSource='<%# DayArray %>'>
                                </asp:DropDownList>日
                                <asp:DropDownList ID="lstToHH" runat="server" Width="40px" DataSource='<%# HourArray %>'>
                                </asp:DropDownList>時
                                <asp:DropDownList ID="lstToMI" runat="server" DataSource='<%# MinuteArray %>'>
                                </asp:DropDownList>分
                                <asp:Label ID="lblErrorMessageTo" runat="server" Text="" Visible="false" ForeColor="red"></asp:Label>
                            </td>
                        </tr>
                        <td class="tdHeaderStyle">
                            ボーナスアイテム
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstGameItemCategory" runat="server" DataSourceID="dsGameItemCategory"
                                OnDataBound="lst_DataBound" OnSelectedIndexChanged="lstGameItemCategory_IndexChanged"
                                DataTextField="GAME_ITEM_CATEGORY_NM" DataValueField="GAME_ITEM_CATEGORY_TYPE"
                                Width="170px" AutoPostBack="True">
                            </asp:DropDownList>
                            <asp:DropDownList ID="lstItemGetCd" runat="server" DataSourceID="dsItemGet" DataTextField="CODE_NM"
								DataValueField="CODE" OnDataBound="lstItemGetCd_DataBound" OnSelectedIndexChanged="lstItemGetCd_IndexChanged" AutoPostBack="True">
							</asp:DropDownList>
							<asp:DropDownList ID="lstItemPresent" runat="server" OnSelectedIndexChanged="lstItemPresent_IndexChanged" AutoPostBack="True">
								<asp:ListItem Value=""></asp:ListItem>
								<asp:ListItem Value="0">通常</asp:ListItem>
								<asp:ListItem Value="1">ﾌﾟﾚｾﾞﾝﾄ</asp:ListItem>
							</asp:DropDownList>
                            <asp:DropDownList ID="lstGameItem" runat="server" DataSourceID="dsGameItem" DataTextField="GAME_ITEM_NM"
                                OnDataBound="lst_DataBound" DataValueField="GAME_ITEM_SEQ" Width="170px">
                            </asp:DropDownList>
                            <br />
                            <asp:Label ID="lblErrorMessageItem" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            個数
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtItemCount" runat="server" MaxLength="3" Width="50px"></asp:TextBox>
                            <br />
                            <asp:Label ID="lblErrorMessageCount" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            ゲームポイント
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtGamePoint" runat="server" MaxLength="10" Width="50px"></asp:TextBox>
                            <br />
                            <asp:Label ID="lblErrorMessageGamePoint" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="pnlCommand" runat="server">
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Update"
                        OnClick="btnUpdate_Click" />
                    <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" ValidationGroup="Delete"
                        OnClick="btnDelete_Click" />
                    <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" CausesValidation="False"
                        Visible="true" OnClick="btnCancel_Click" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[ボーナスアイテム一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdGameLoginBonusDay.PageIndex + 1 %>
                        of
                        <%= grdGameLoginBonusDay.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="600px">
                    <asp:GridView ID="grdGameLoginBonusDay" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        PageSize="30" EnableSortingAndPagingCallbacks="false" DataSourceID="dsGameLoginBonusDay"
                        SkinID="GridViewColor" AllowSorting="true">
                        <Columns>

                            <asp:BoundField DataField="GAME_LOGIN_BONUS_START_DATE" HeaderText="開始日時" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}"
                                HtmlEncode="False">
                                <ItemStyle HorizontalAlign="Center" Wrap="false" />
                            </asp:BoundField>
                            <asp:BoundField DataField="GAME_LOGIN_BONUS_END_DATE" HeaderText="終了日時" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}"
                                HtmlEncode="False">
                                <ItemStyle HorizontalAlign="Center" Wrap="false" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="アイテムSEQ">
                                <ItemTemplate>
                                    <asp:Label ID="lblGameItemSeq" runat="server" Text='<%# Eval("GAME_ITEM_SEQ") %>'></asp:Label>
                                    <asp:HiddenField ID="hdnGameItemSeq" runat="server" Value='<%# Eval("GAME_ITEM_SEQ") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="アイテム名">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%# Container.DataItemIndex %>'
                                        OnCommand="lnkEdit_Command" Text='<%# CheckGamePointOnly(Eval("GAME_ITEM_NM")) %>'></asp:LinkButton>
                                    <asp:HiddenField ID="hdnGameLoginBonusSeq" runat="server" Value='<%# Eval("GAME_LOGIN_BONUS_SEQ") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="ITEM_COUNT" HeaderText="個数" DataFormatString="{0:N0}">
                                <ItemStyle HorizontalAlign="right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="GAME_POINT" HeaderText="ｹﾞｰﾑﾎﾟｲﾝﾄ" DataFormatString="{0:N0}">
                                <ItemStyle HorizontalAlign="right" />
                            </asp:BoundField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
            <br />
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsGameLoginBonusDay" runat="server" ConvertNullToDBNull="false"
        EnablePaging="True" OnSelected="dsGameLoginBonusDay_Selected" SelectCountMethod="GetPageCount"
        SelectMethod="GetPageCollection" SortParameterName="" TypeName="GameLoginBonusDay">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItemCategory" runat="server" SelectMethod="GetList"
        TypeName="GameItemCategory">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItem" runat="server" SelectMethod="GetListByCategoryType"
        TypeName="GameItem" OnSelecting="dsGameItem_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
            <asp:Parameter Name="pGameItemCategoryType" Type="String" DefaultValue="" />
            <asp:Parameter Name="pGameItemGetCd" Type="String" DefaultValue="" />
            <asp:Parameter Name="pPresentFlag" Type="String" DefaultValue="" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsItemGet" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="A2" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtItemCount">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="ボーナスアイテム情報を更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete"
        ConfirmText="ボーナスアイテム情報を削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>
