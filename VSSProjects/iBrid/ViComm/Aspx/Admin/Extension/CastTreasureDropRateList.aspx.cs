﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 抽選獲得アイテム一覧

--	Progaram ID		: TownList
--
--  Creation Date	: 2011.07.25
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Ratelain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using ViComm.Extension.Pwild;
using iBridCommLib;

public partial class Extension_CastTreasureDropRateList : System.Web.UI.Page {
	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string StageGroupType {
		get {
			return iBridUtil.GetStringValue(this.ViewState["StageGroupType"]);
		}
		set {
			this.ViewState["StageGroupType"] = value;
		}
	}

	private string GameCharacterType {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameCharacterType"]);
		}
		set {
			this.ViewState["GameCharacterType"] = value;
		}
	}

	private List<string> GameCharacterTypeList {
		get {
			return this.ViewState["GameCharacterTypeList"] as List<string>;
		}
		set {
			this.ViewState["GameCharacterTypeList"] = value;
		}
	}

	private List<string> CastTreasureSeqList {
		get {
			return this.ViewState["CastTreasureSeqList"] as List<string>;
		}
		set {
			this.ViewState["CastTreasureSeqList"] = value;
		}
	}

	private List<string> DropRateList {
		get {
			return this.ViewState["DropRateList"] as List<string>;
		}
		set {
			this.ViewState["DropRateList"] = value;
		}
	}

	private List<string> RevisionNoList {
		get {
			return this.ViewState["RevisionNoList"] as List<string>;
		}
		set {
			this.ViewState["RevisionNoList"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();

			this.SiteCd = iBridUtil.GetStringValue(this.Request.QueryString["sitecd"]);
			this.StageGroupType = iBridUtil.GetStringValue(this.Request.QueryString["stagegrouptype"]);
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
	}

	protected void btnBack_Click(object sender,EventArgs e) {
		this.Response.Redirect(string.Concat("~/Extension/CastTreasureList.aspx"));
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		this.UpdateData();
	}

	protected void dsCastTreasureDropRate_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pGameCharacterType"] = this.GameCharacterType;
	}

	protected void rptCharacterType_DataBinding(object sender,EventArgs e) {
		Repeater oRepeater = sender as Repeater;
		if (oRepeater != null) {
			switch (oRepeater.ID) {
				case "rptCharacterType1":
					this.GameCharacterType = PwViCommConst.GameCharacterType.TYPE01;
					break;
				case "rptCharacterType2":
					this.GameCharacterType = PwViCommConst.GameCharacterType.TYPE02;
					break;
				case "rptCharacterType3":
					this.GameCharacterType = PwViCommConst.GameCharacterType.TYPE03;
					break;
				default:
					return;
			}
		}
	}

	private bool IsCorrectInput() {
		this.lblErrorMessageRate.Visible = false;

		foreach (Repeater oRepeater in new Repeater[] { this.rptCharacterType1,this.rptCharacterType2,this.rptCharacterType3 }) {
			int iRate = 0;
			int iRateSum = 0;

			foreach (RepeaterItem oRepeaterItem in oRepeater.Items) {
				TextBox txtDropRate = oRepeaterItem.FindControl("txtDropRate") as TextBox;

				if (int.TryParse(txtDropRate.Text,out iRate)) {
					if (iRate < 0 || 100 < iRate) {
						this.lblErrorMessageRate.Text = "ドロップ率は0～100の範囲で入力してください。";
						this.lblErrorMessageRate.Visible = true;
						return false;
					}
				} else {
					txtDropRate.Text = "0";
				}

				iRateSum += iRate;

				if (iRateSum > 100) {
					this.lblErrorMessageRate.Text = "同一ｹﾞｰﾑｷｬﾗｸﾀｰで、ﾄﾞﾛｯﾌﾟ率の合計が100％以下になるよう設定してください。";
					this.lblErrorMessageRate.Visible = true;
					return false;
				}

				HiddenField hdnCastTreasureSeq = oRepeaterItem.FindControl("hdnCastTreasureSeq") as HiddenField;
				HiddenField hdnGameCharacterType = oRepeaterItem.FindControl("hdnGameCharacterType") as HiddenField;
				HiddenField hdnRevisionNo = oRepeaterItem.FindControl("hdnRevisionNo") as HiddenField;

				this.CastTreasureSeqList.Add(hdnCastTreasureSeq.Value);
				this.DropRateList.Add(txtDropRate.Text);
				this.GameCharacterTypeList.Add(hdnGameCharacterType.Value);
				this.RevisionNoList.Add(hdnRevisionNo.Value);
			}
		}

		return true;
	}

	private void InitPage() {
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void GetList() {
		this.pnlInfo.DataBind();
	}

	private void UpdateData() {
		this.CastTreasureSeqList = new List<string>();
		this.DropRateList = new List<string>();
		this.GameCharacterTypeList = new List<string>();
		this.RevisionNoList = new List<string>();

		if (!this.IsCorrectInput()) {
			return;
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("CAST_TREASURE_DROP_RATE_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pSTAGE_GROUP_TYPE", DbSession.DbType.VARCHAR2, this.StageGroupType);
			oDbSession.ProcedureInArrayParm("pCAST_TREASURE_SEQ",DbSession.DbType.VARCHAR2,this.CastTreasureSeqList.ToArray());
			oDbSession.ProcedureInArrayParm("pGAME_CHARACTER_TYPE",DbSession.DbType.VARCHAR2,this.GameCharacterTypeList.ToArray());
			oDbSession.ProcedureInArrayParm("pDROP_RATE",DbSession.DbType.NUMBER,this.DropRateList.ToArray());
			oDbSession.ProcedureInArrayParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNoList.ToArray());
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.GetList();
	}
}