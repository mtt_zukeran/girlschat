﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ソーシャルゲーム基本設定
--	Progaram ID		: SocialGameList
--
--  Creation Date	: 2011.07.21
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using iBridCommLib;
using System.Drawing;
using ViComm;

public partial class Extension_SocialGameLimitList:System.Web.UI.Page {
	string recCount;

	protected string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		private set {
			this.ViewState["SiteCd"] = value;
		}
	}

	protected string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		private set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	protected string Rowid {
		get {
			return iBridUtil.GetStringValue(this.ViewState["Rowid"]);
		}
		private set {
			this.ViewState["Rowid"] = value;
		}
	}

    protected string ManLimitStageSeq
    {
        get {
            return iBridUtil.GetStringValue(this.ViewState["ManLimitStageSeq"]);
        }
        private set {
            this.ViewState["ManLimitStageSeq"] = value;
        }
    }

    protected string CastLimitStageSeq {
        get {
            return iBridUtil.GetStringValue(this.ViewState["CastLimitStageSeq"]);
        }
        private set {
            this.ViewState["CastLimitStageSeq"] = value;
        }
    }
	protected void Page_Load(object sender,EventArgs e) {
		if (!this.IsPostBack) {
			this.InitPage();
		}
	}

	protected void lnkSiteCd_Command(object sender,CommandEventArgs e) {
		this.SiteCd = e.CommandArgument.ToString();
		this.pnlMainte.Visible = true;
		this.pnlDtl.Visible = true;
		this.GetData();
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlMainte.Visible = false;
		this.pnlDtl.Visible = false;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.UpdateData(ViCommConst.FLAG_OFF);
		this.InitPage();
	}

    protected void dsSocialGameLimit_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

    protected void dsManLimitStage_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
        e.InputParameters[0] = this.SiteCd;
        e.InputParameters[1] = ViCommConst.MAN;
    }

    protected void dsManLimitStage_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
        if (!string.IsNullOrEmpty(this.ManLimitStageSeq)) {
            this.ModifyLimitStageListBox(e.ReturnValue as DataSet, this.SiteCd, this.ManLimitStageSeq, ViCommConst.MAN);
        }
    }

    protected void dsCastLimitStage_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
        e.InputParameters[0] = this.SiteCd;
        e.InputParameters[1] = ViCommConst.OPERATOR;
    }

    protected void dsCastLimitStage_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
        if (!string.IsNullOrEmpty(this.CastLimitStageSeq)) {
            this.ModifyLimitStageListBox(e.ReturnValue as DataSet, this.SiteCd, this.CastLimitStageSeq, ViCommConst.OPERATOR);
        }
    }

    protected void ModifyLimitStageListBox(DataSet pDS, string pSiteCd , string pLimitStageSeq, string pSexCd) {
        using (Stage oStage = new Stage()) {
            DataSet oLimitStage = oStage.GetOne(pSiteCd, pLimitStageSeq, pSexCd);
            DataSet ds = pDS;

            int iLimitLevel = 0;
            if (oLimitStage.Tables[0].Rows.Count > 0) {
                int.TryParse(oLimitStage.Tables[0].Rows[0]["STAGE_LEVEL"].ToString(), out iLimitLevel);

                DataRow[] oRemoveRows = ds.Tables[0].Select(string.Format("STAGE_LEVEL < {0}", iLimitLevel));
                foreach (DataRow dr in oRemoveRows) {
                    ds.Tables[0].Rows.Remove(dr);
                }
            }
        }
    
    }

	private void InitPage() {
		this.pnlMainte.Visible = false;
		this.pnlDtl.Visible = false;
		this.ClearFields();
		this.GetList();
	}

	private void ClearFields() {
		this.pnlDtl.DataBind();
        this.lstManLimitStage.SelectedIndex = 0;
        this.lstCastLimitStage.SelectedIndex = 0;
	}

	private void GetData() {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("SOCIAL_GAME_LIMIT_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
            oDbSession.ProcedureOutParm("pMAN_LIMIT_STAGE_SEQ", DbSession.DbType.VARCHAR2);
            oDbSession.ProcedureOutParm("pCAST_LIMIT_STAGE_SEQ", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pROWID",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pROW_COUNT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
			this.Rowid = oDbSession.GetStringValue("pROWID");

            if (int.Parse(oDbSession.GetStringValue("pROW_COUNT")) > 0) {
                this.ManLimitStageSeq = oDbSession.GetStringValue("pMAN_LIMIT_STAGE_SEQ");
                this.CastLimitStageSeq = oDbSession.GetStringValue("pCAST_LIMIT_STAGE_SEQ");

				this.pnlDtl.DataBind();

                //this.lstManLimitStage.SelectedValue = this.ManLimitStageSeq;
                //this.lstCastLimitStage.SelectedValue = this.CastLimitStageSeq;

			} else {
				this.ClearFields();
			}
		}
	}

	private void GetList() {
		this.grdSocialGameLimit.DataBind();
		this.pnlCount.DataBind();
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void UpdateData(int pDelFlag) {
		using (DbSession oDbSession = new DbSession()) {
            
			oDbSession.PrepareProcedure("SOCIAL_GAME_LIMIT_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
            oDbSession.ProcedureInParm("pMAN_LIMIT_STAGE_SEQ", DbSession.DbType.VARCHAR2, this.lstManLimitStage.SelectedValue);
            oDbSession.ProcedureInParm("pCAST_LIMIT_STAGE_SEQ", DbSession.DbType.VARCHAR2, this.lstCastLimitStage.SelectedValue);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pROWID",DbSession.DbType.VARCHAR2,this.Rowid);
			oDbSession.ProcedureInParm("pDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
            /**/
		}
	}
}