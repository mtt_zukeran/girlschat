﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="GameCharacterMainte.aspx.cs" Inherits="Extension_GameCharacterMainte" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="ゲームキャラクター情報設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			$NO_TRANS_START;
			<asp:Panel runat="server" ID="pnlDtl">
				<fieldset class="fieldset-inner">
	                    <asp:Panel ID="pnlGameCharacter" runat="server" Visible="false">
						[ゲームキャラクター]
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									ハンドル名
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtGameHandleNm" runat="server" MaxLength="20" Width="150px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									未割り当て部隊
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUnassignedForce" runat="server" MaxLength="5" Width="150px"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server"
                                        Enabled="true" FilterType="Numbers" TargetControlID="txtUnassignedForce" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									連携ポイント
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCooperationPoint" runat="server" MaxLength="8" Width="150px"  ></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server"
                                        Enabled="true" FilterType="Numbers" TargetControlID="txtCooperationPoint" />
								</td>
							</tr>
						</table>
					</asp:Panel>
					<table border="0" style="width: 640px">
						<tr>
							<td>
								<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
								<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
							</td>
						</tr>
					</table>
				</fieldset>
			</asp:Panel>
			$NO_TRANS_END;
		</asp:Panel>
	</div>
</asp:Content>