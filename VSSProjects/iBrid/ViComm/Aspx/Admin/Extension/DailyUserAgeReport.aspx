﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="DailyUserAgeReport.aspx.cs" Inherits="Extension_DailyUserAgeReport"
    Title="年齢別レポート" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="年齢別レポート"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset>
                <legend>[設定]</legend>
                <table border="0" style="width: 640px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            サイトコード
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="180px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            集計期間
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstFromYYYY" runat="server" Width="54px">
                            </asp:DropDownList>年
                            <asp:DropDownList ID="lstFromMM" runat="server" Width="43px">
                            </asp:DropDownList>月
                            <asp:DropDownList ID="lstFromDD" runat="server" Width="43px">
                            </asp:DropDownList>日～
                            <asp:DropDownList ID="lstToYYYY" runat="server" Width="54px">
                            </asp:DropDownList>年
                            <asp:DropDownList ID="lstToMM" runat="server" Width="43px">
                            </asp:DropDownList>月
                            <asp:DropDownList ID="lstToDD" runat="server" Width="43px">
                            </asp:DropDownList>日
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            レポート種別
                        </td>
                        <td class="tdDataStyle">
                            <asp:RadioButtonList ID="rdoReportType" runat="server" RepeatDirection="horizontal">
                                <asp:ListItem Text="ポイント消費" Value="1" Selected="true"></asp:ListItem>
                                <asp:ListItem Text="PV数" Value="2"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    CausesValidation="False" />
                <asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" CausesValidation="False" OnClick="btnCSV_Click" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[年齢別レポート]</legend>
                <asp:Label ID="lblNotice" runat="server" Text='<%# GetNoticeMark() %>'></asp:Label>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="552px">
                    <asp:GridView ID="grdDailyUserAgeReport" runat="server" AutoGenerateColumns="False"
                        DataSourceID="dsDailyUserAgeReport" AllowSorting="true" SkinID="GridViewColor"
                        OnDataBound="grdDailyUserAgeReport_DataBound" OnRowDataBound="grdDailyUserAgeReport_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="年月日">
                                <ItemTemplate>
                                    <asp:Label ID="lblRegistDay" runat="server" Text='<%# Eval("REPORT_DAY") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="合計">
                                <ItemTemplate>
                                    <asp:Label ID="lblCountAll" runat="server" Text='<%# Eval("COUNT_ALL", "{0:N0}") %>'></asp:Label><br />
                                    <asp:Label ID="lblCountUniqueAll" runat="server" Text='<%# Eval("COUNT_UNIQUE_ALL", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHeader18" runat="server" Text='<%# GetHeaderMark(18) %>'></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblCount18" runat="server" Text='<%# Eval("COUNT_18", "{0:N0}") %>'></asp:Label><br />
                                    <asp:Label ID="lblCountUnique18" runat="server" Text='<%# Eval("COUNT_UNIQUE_18", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHeader20" runat="server" Text='<%# GetHeaderMark(20) %>'></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblCount20" runat="server" Text='<%# Eval("COUNT_20", "{0:N0}") %>'></asp:Label><br />
                                    <asp:Label ID="lblCountUnique20" runat="server" Text='<%# Eval("COUNT_UNIQUE_20", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHeader25" runat="server" Text='<%# GetHeaderMark(25) %>'></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblCount25" runat="server" Text='<%# Eval("COUNT_25", "{0:N0}") %>'></asp:Label><br />
                                    <asp:Label ID="lblCountUnique25" runat="server" Text='<%# Eval("COUNT_UNIQUE_25", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHeader30" runat="server" Text='<%# GetHeaderMark(30) %>'></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblCount30" runat="server" Text='<%# Eval("COUNT_30", "{0:N0}") %>'></asp:Label><br />
                                    <asp:Label ID="lblCountUnique30" runat="server" Text='<%# Eval("COUNT_UNIQUE_30", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHeaderOther" runat="server" Text='<%# GetHeaderMark("Other") %>'></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblCountOther" runat="server" Text='<%# Eval("COUNT_OTHER", "{0:N0}") %>'></asp:Label><br />
                                    <asp:Label ID="lblCountUniqueOther" runat="server" Text='<%# Eval("COUNT_UNIQUE_OTHER", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="80px" />
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle ForeColor="Black" BackColor="LightYellow" Font-Bold="true" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsDailyUserAgeReport" runat="server" SelectMethod="GetListReport"
        OnSelecting="dsDailyUserAgeReport_Selecting" TypeName="DailyUserAgeReport">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSeekSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
            <asp:Parameter Name="pReportDayFrom" Type="String" />
            <asp:Parameter Name="pReportDayTo" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
