<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SocialGameLimitList.aspx.cs" Inherits="Extension_SocialGameLimitList"
	Title="ゲームリミット設定" ValidateRequest="false" %>

<%@ Import Namespace="System.Data" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="ゲームリミット設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset>
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[ゲームリミット設定]</legend>
						<table border="0" style="width: 800px; margin-bottom: 10px" class="tableStyle">
						    <tr>
                                <td class="tdHeaderStyle">
						            男性最終ｽﾃｰｼﾞ
						        </td>
                                <td class="tdDataStyle">
	                                <asp:DropDownList ID="lstManLimitStage" runat="server" DataSourceID="dsManLimitStage" DataTextField="STAGE_DISPLAY"
                                        DataValueField="STAGE_SEQ" Width="170px" AutoPostBack="True">
                                    </asp:DropDownList>
	                            </td>
						    </tr>
							<tr>
                                <td class="tdHeaderStyle2">
						            女性最終ｽﾃｰｼﾞ
						        </td>
                                <td class="tdDataStyle">
	                                <asp:DropDownList ID="lstCastLimitStage" runat="server" DataSourceID="dsCastLimitStage" DataTextField="STAGE_DISPLAY"
                                        DataValueField="STAGE_SEQ" Width="170px" AutoPostBack="True">
                                    </asp:DropDownList>
	                            </td>
							</tr>
						</table>
						<asp:Button ID="btnUpdate" runat="server" CausesValidation="True" ValidationGroup="Update" CssClass="seekbutton" OnClick="btnUpdate_Click" Text="更新" OnClientClick="return confirm('更新を行いますか？');" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<asp:Panel ID="pnlList" runat="server" ScrollBars="auto">
			<fieldset>
				<legend>[サイト一覧]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count
						<%# GetRecCount() %>
					</a>
					<br />
					<a class="reccount">Current viewing page
						<%= grdSocialGameLimit.PageIndex + 1 %>
						of
						<%= grdSocialGameLimit.PageCount%>
					</a>
				</asp:Panel>
				&nbsp;
				<asp:Panel ID="pnlGrid" runat="server">
					<asp:GridView ID="grdSocialGameLimit" runat="server" DataSourceID="dsSocialGameLimit" AutoGenerateColumns="False" AllowPaging="true" AllowSorting="true" SkinID="GridViewColor">
						<Columns>
							<asp:TemplateField HeaderText="サイトコード">
								<ItemTemplate>
									<asp:LinkButton ID="lnkSiteCd" runat="server" Text='<%# Eval("SITE_CD") %>' CommandArgument='<%# Eval("SITE_CD") %>' OnCommand="lnkSiteCd_Command" CausesValidation="False">
									</asp:LinkButton>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Left" />
							</asp:TemplateField>
							
							<asp:BoundField DataField="UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsSocialGameLimit" runat="server" SelectMethod="GetPageCollection" SelectCountMethod="GetPageCount" EnablePaging="true" OnSelected="dsSocialGameLimit_Selected"
		TypeName="SocialGameLimit"></asp:ObjectDataSource>
		
	<asp:ObjectDataSource ID="dsManLimitStage" runat="server" SelectMethod="GetLevelList" OnSelected="dsManLimitStage_Selected"
		OnSelecting="dsManLimitStage_Selecting" TypeName="Stage">
		<SelectParameters>
		    <asp:Parameter Name="pSiteCd" Type="String" />
		    <asp:Parameter Name="pSexCd"  Type="String" />
		</SelectParameters>
    </asp:ObjectDataSource>
    
	<asp:ObjectDataSource ID="dsCastLimitStage" runat="server" SelectMethod="GetLevelList" OnSelected="dsCastLimitStage_Selected"
		OnSelecting="dsCastLimitStage_Selecting" TypeName="Stage">
		<SelectParameters>
		    <asp:Parameter Name="pSiteCd" Type="String" />
		    <asp:Parameter Name="pSexCd"  Type="String" />
		</SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
