﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 10分以内の返信・サマリー
--	Progaram ID		: QuickResponseDetail
--
--  Creation Date	: 2011.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using iBridCommLib;
using ViComm;

public partial class Extension_QuickResponseDetail:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			FirstLoad();
		}
	}

	private void FirstLoad() {
		lstSiteCd.DataBind();
		SysPrograms.SetupDay(lstYYYY,lstMM,lstDD,false);
		lstHH.Items.Clear();
		for (int i = 0;i <= 23;i++) {
			lstHH.Items.Add(new ListItem(string.Format("{0:D2}",i)));
		}
		ClearField();
	}

	private void ClearField() {
		lstSiteCd.SelectedIndex = 0;
		DateTime dtTo = DateTime.Now;
		lstYYYY.SelectedValue = dtTo.ToString("yyyy");
		lstMM.SelectedValue = dtTo.ToString("MM");
		lstDD.SelectedValue = dtTo.ToString("dd");
		lstHH.SelectedValue = dtTo.ToString("HH");

		if (!iBridUtil.GetStringValue(Request.QueryString["site"]).Equals(string.Empty)) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Request.QueryString["site"]);
		}
		string sDay = iBridUtil.GetStringValue(Request.QueryString["day"]);
		if (sDay.Length == 10) {
			lstYYYY.SelectedValue = sDay.Substring(0,4);
			lstMM.SelectedValue = sDay.Substring(5,2);
			lstDD.SelectedValue = sDay.Substring(8,2);
		}
		if (!iBridUtil.GetStringValue(Request.QueryString["hour"]).Equals(string.Empty)) {
			lstHH.SelectedValue = iBridUtil.GetStringValue(Request.QueryString["hour"]);
		}

		GetList();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		ClearField();
	}

	protected string GetUserLink(object pSiteCd,object pLoginId,object pSexCd) {
		if (pSexCd.Equals(ViCommConst.MAN)) {
			return string.Format("../Man/ManView.aspx?site={0}&manloginid={1}",pSiteCd,pLoginId);
		} else {
			return string.Format("../Cast/CastView.aspx?loginid={0}",pLoginId);
		}
	}


	private void GetList() {
		pnlInfo.Visible = true;
		grdQuickResMail.PageIndex = 0;
		grdQuickResMail.DataSourceID = "dsQuickResMail";
		grdQuickResMail.DataBind();
		pnlCount.DataBind();
		if (grdQuickResMail.Rows.Count > 0) {
			AddHeader();
		}
	}

	protected void dsQuickResMail_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = string.Format("{0}/{1}/{2}",lstYYYY.SelectedValue,lstMM.SelectedValue,lstDD.SelectedValue);
		e.InputParameters[2] = lstHH.SelectedValue;
	}

	protected void dsQuickResMail_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void AddHeader() {

		GridViewRow row = new GridViewRow(-1,-1,DataControlRowType.Header,DataControlRowState.Normal);
		TableCell cell;
		cell = new TableCell();
		cell.ColumnSpan = 1;
		cell.RowSpan = 1;
		cell.Text = "";
		row.Cells.Add(cell);

		cell = new TableCell();
		cell.ColumnSpan = 2;
		cell.Text = "男性会員";
		cell.HorizontalAlign = HorizontalAlign.Center;
		row.Cells.Add(cell);

		cell = new TableCell();
		cell.ColumnSpan = 2;
		cell.Text = "出演者";
		cell.HorizontalAlign = HorizontalAlign.Center;
		row.Cells.Add(cell);

		grdQuickResMail.Controls[0].Controls.AddAt(0,row);
	}

	protected string GetRecCount() {
		return recCount;
	}
}
