<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="MailDeBingoMainte.aspx.cs" Inherits="Extension_MailDeBingoMainte" Title="メールdeビンゴ開催設定"
    ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="メールdeビンゴ開催設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlMainte">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel runat="server" ID="pnlDtl">
                    <table border="0" style="width: 820px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    Enabled="false" DataValueField="SITE_CD" Width="180px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                備考
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtRemarks" runat="server" MaxLength="1000" Width="650px" Rows="3"
                                    TextMode="MultiLine"></asp:TextBox>
                                <asp:CustomValidator ID="vdcRemarks" runat="server" ControlToValidate="txtRemarks" 
                                    ErrorMessage='<%# string.Format("備考は{0}文字以内で入力してください",txtRemarks.MaxLength) %>' ValidationGroup="Detail" OnServerValidate="tdcRemarks_ServerValidate">*</asp:CustomValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender2"
                                    TargetControlID="vdcRemarks" HighlightCssClass="validatorCallout" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                ジャックポット
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtJackpot" runat="server"  MaxLength="7" ></asp:TextBox>
                                円
                                <asp:RangeValidator ID="vdrJackpot" runat="server" ControlToValidate="txtJackpot"
                                    ErrorMessage="ジャックポット金額を0円以上の数値で入力して下さい。" MaximumValue="9999999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender1"
                                    TargetControlID="vdrJackpot" HighlightCssClass="validatorCallout" />
                                    
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtJackpot" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                ビンゴ開始日
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstFromYYYY" runat="server" Width="60px" DataSource='<%# YearArray %>'>
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstFromMM" runat="server" Width="40px" DataSource='<%# MonthArray %>'>
                                </asp:DropDownList>月
                                <asp:DropDownList ID="lstFromDD" runat="server" Width="40px" DataSource='<%# DayArray %>'>
                                </asp:DropDownList>日
                                <asp:DropDownList ID="lstFromHH" runat="server" Width="40px" DataSource='<%# HourArray %>'>
                                </asp:DropDownList>時
                                <asp:DropDownList ID="lstFromMI" runat="server" DataSource='<%# MinuteArray %>'>
                                </asp:DropDownList>分
                                <asp:Label ID="lblErrorMessageFrom" runat="server" Text="" Visible="false" ForeColor="red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                ビンゴ終了日
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstToYYYY" runat="server" Width="60px" DataSource='<%# YearArray %>'>
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstToMM" runat="server" Width="40px" DataSource='<%# MonthArray %>'>
                                </asp:DropDownList>月
                                <asp:DropDownList ID="lstToDD" runat="server" Width="40px" DataSource='<%# DayArray %>'>
                                </asp:DropDownList>日
                                <asp:DropDownList ID="lstToHH" runat="server" Width="40px" DataSource='<%# HourArray %>'>
                                </asp:DropDownList>時
                                <asp:DropDownList ID="lstToMI" runat="server" DataSource='<%# MinuteArray %>'>
                                </asp:DropDownList>分
                                <asp:Label ID="lblErrorMessageTo" runat="server" Text="" Visible="false" ForeColor="red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle2">
                                一発ビンゴ
                            </td>
                            <td class="tdDataStyle">
								<asp:RadioButton ID="rdoBingoBallFlagOff" runat="server" GroupName="BingoBallFlag" Text="無効"></asp:RadioButton>
								<asp:RadioButton ID="rdoBingoBallFlagOn" runat="server" GroupName="BingoBallFlag" Text="有効"></asp:RadioButton>
                            </td>
                        </tr>
                    </table>
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click"
                        ValidationGroup="Detail" />
                    <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click"
                        ValidationGroup="Key" />
                    <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click"
                        CausesValidation="False" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <fieldset class="fieldset">
            <legend>[検索条件]</legend>
            <fieldset class="fieldset-inner">
                <table border="0" style="width: 800px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle2">
                            サイトコード
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="180px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    CausesValidation="False" />
                <asp:Button runat="server" ID="btnRegist" Text="追加" CssClass="seekbutton" CausesValidation="False"
                    OnClick="btnRegist_Click" />
            </fieldset>
            <fieldset class="fieldset-inner">
                <legend>[設定一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%#GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%=grdMailDeBingo.PageIndex + 1%>
                        of
                        <%=grdMailDeBingo.PageCount%>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel runat="server" ID="pnlGrid">
                    <asp:GridView ID="grdMailDeBingo" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        DataSourceID="dsMailDeBingo" AllowSorting="True" SkinID="GridViewColor" DataKeyNames="SITE_CD,BINGO_TERM_SEQ">
                        <Columns>
                            <asp:TemplateField HeaderText="ビンゴ開始日時">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkStartDate" runat="server" CausesValidation="False" CommandArgument='<%# Container.DataItemIndex %>'
                                        OnCommand="lnkStartDate_Command" Text='<%# Eval("BINGO_START_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>' >
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="BINGO_END_DATE" HeaderText="ビンゴ終了日時" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}"
                                HtmlEncode="False">
                                <ItemStyle HorizontalAlign="Center" Wrap="false" />
                            </asp:BoundField>
                            <asp:BoundField DataField="JACKPOT" HeaderText="ジャックポット">
                                <ItemStyle HorizontalAlign="right" Wrap="true" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="一発ﾋﾞﾝｺﾞ">
                                <ItemTemplate>
                                    <asp:Label ID="lblBingoBallFlag" runat="server" Text='<%# GetBingoBallFlagStr(Eval("BINGO_BALL_FLAG")) %>' ForeColor='<%# GetBingoBallFlagForeColor(Eval("BINGO_BALL_FLAG")) %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Wrap="false" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="備考">
                                <ItemTemplate>
                                    <asp:Label ID="lblRemarks" runat="server" Text='<%# SubStringRemarks(Eval("REMARKS")) %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Wrap="true"  />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkPrizeGet" runat="server" NavigateUrl='<%# string.Format("~/Extension/MailDeBingoPrizeGetList.aspx?sexcd={0}&seq={1}&sitecd={2}",Eval("SEX_CD"),Eval("BINGO_TERM_SEQ"),Eval("SITE_CD")) %>'
                                        Text='<%# GetPrizeGetNm(Eval("SEX_CD")) %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkCreateCard" runat="server" NavigateUrl='<%# string.Format("~/Extension/BingoCardView.aspx?sexcd={0}&seq={1}&sitecd={2}",Eval("SEX_CD"),Eval("BINGO_TERM_SEQ"),Eval("SITE_CD")) %>'
                                        Text="カード作成"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>

                </asp:Panel>
            </fieldset>
        </fieldset>
        <br />
    </div>
    <asp:ObjectDataSource ID="dsMailDeBingo" runat="server" EnablePaging="True" OnSelected="dsMailDeBingo_Selected"
        SelectCountMethod="GetPageCount" SelectMethod="GetPageCollection" TypeName="MailDeBingo">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete"
        ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
