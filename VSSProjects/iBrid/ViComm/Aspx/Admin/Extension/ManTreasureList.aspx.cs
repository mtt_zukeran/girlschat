﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 男性用お宝一覧
--	Progaram ID		: ManTreasureList
--
--  Creation Date	: 2011.07.22
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater				Update Explain
  2010/06/29	伊藤和明			検索項目に日付を追加

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Extension_ManTreasureList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
			GetList();
		}
	}

	protected void dsManTreasure_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void FirstLoad() {
		grdManTreasure.PageSize = 100;
		grdManTreasure.DataSourceID = "";
		DataBind();
		if (Session["SiteCd"].ToString().Equals("")) {
			lstSiteCd.Items.Insert(0,new ListItem("",""));
		}
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["site"]);
		if (!sSiteCd.Equals(string.Empty)) {
			lstSiteCd.SelectedValue = sSiteCd;
		}
		lstSiteCd.DataSourceID = "";

		if (string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["return"]))) {
			InitSession();
		} else {
			SetSearch();
		}
	}

	private void InitPage() {
		grdManTreasure.DataSourceID = "";
		DataBind();
		recCount = "0";
		// 未承認＆未チェック
		if (iBridUtil.GetStringValue(Request.QueryString["uncheck"]).Equals("1")) {
			chkPublish.Checked = false;
			chkNonPublish.Checked = true;
			chkChecked.Checked = false;
			chkUnChecked.Checked = true;
		}
	}

	private void ClearField() {
		lstSiteCd.SelectedIndex = 0;
		recCount = "0";
		lstManTreasureAttr.SelectedIndex = 0;
	}

	protected string GetRecCount() {
		return recCount;
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid)
			return;
		SetSession();
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitSession();
		this.Response.Redirect(this.Request.Url.PathAndQuery);
	}

	private void GetList() {
		grdManTreasure.DataSourceID = "dsManTreasure";
		grdManTreasure.PageIndex = 0;
		grdManTreasure.DataBind();
		pnlCount.DataBind();
	}

	protected void dsManTreasure_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = txtDisplayDay.Text;
		e.InputParameters[2] = chkPublish.Checked;
		e.InputParameters[3] = chkNonPublish.Checked;
		e.InputParameters[4] = txtLoginId.Text;
		e.InputParameters[5] = lstManTreasureAttr.SelectedValue;
		// チェック状態
		string sChecked = string.Empty;
		if (chkChecked.Checked && !chkUnChecked.Checked) {
			sChecked = ViCommConst.FLAG_ON_STR;
		} else if (!chkChecked.Checked && chkUnChecked.Checked) {
			sChecked = ViCommConst.FLAG_OFF_STR;
		}
		e.InputParameters[6] = sChecked;
	}

	protected void dsManTreasureAttr_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	private void InitSession() {
		Session["txtDisplayDay"] = null;
		Session["chkPublish"] = null;
		Session["chkNonPublish"] = null;
		Session["txtLoginId"] = null;
		Session["lstManTreasureAttr"] = null;
	}

	private void SetSession() {
		Session["txtDisplayDay"] = txtDisplayDay.Text;
		if (chkPublish.Checked == true) {
			Session["chkPublish"] = "1";
		} else {
			Session["chkPublish"] = null;
		}
		if (chkNonPublish.Checked == true) {
			Session["chkNonPublish"] = "1";
		} else {
			Session["chkNonPublish"] = null;
		}
		Session["txtLoginId"] = txtLoginId.Text;
		Session["lstManTreasureAttr"] = lstManTreasureAttr.SelectedValue;
	}

	private void SetSearch() {
		if (!iBridUtil.GetStringValue(Session["txtDisplayDay"]).Equals(string.Empty)) {
			txtDisplayDay.Text = Session["txtDisplayDay"].ToString();
		}
		if (!iBridUtil.GetStringValue(Session["chkPublish"]).Equals(string.Empty)) {
			chkPublish.Checked = true;
		}
		if (!iBridUtil.GetStringValue(Session["chkNonPublish"]).Equals(string.Empty)) {
			chkNonPublish.Checked = true;
		}
		if (!iBridUtil.GetStringValue(Session["txtLoginId"]).Equals(string.Empty)) {
			txtLoginId.Text = Session["txtLoginId"].ToString();
		}
		if (!iBridUtil.GetStringValue(Session["lstManTreasureAttr"]).Equals(string.Empty)) {
			lstManTreasureAttr.SelectedValue = Session["lstManTreasureAttr"].ToString();
		}
	}
}
