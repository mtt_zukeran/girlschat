﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="BlogArticleList.aspx.cs" Inherits="Extension_BlogArticleList" Title="ブログ記事操作"
    ValidateRequest="false" %>
<%@ Import Namespace="ViComm" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="ブログ記事操作"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 800px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td class="tdHeaderStyle">
                                会員ID
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtLoginId" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                作成日時From
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstFromYYYY" runat="server" Width="60px">
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstFromMM" runat="server" Width="40px">
                                </asp:DropDownList>月
                                <asp:DropDownList ID="lstFromDD" runat="server" Width="40px">
                                </asp:DropDownList>日
                                <asp:DropDownList ID="lstFromHH" runat="server" Width="40px">
                                </asp:DropDownList>時
                                <asp:DropDownList ID="lstFromMI" runat="server" Width="40px">
                                </asp:DropDownList>分
                            </td>
                            <td class="tdHeaderStyle">
                                公開
                            </td>
                            <td class="tdDataStyle">
                                <asp:RadioButton ID="rdoPublishedAll" runat="server" Checked="True" GroupName="Published"
                                    Text="両方"></asp:RadioButton>
                                <asp:RadioButton ID="rdoPublishedOK" runat="server" GroupName="Published" Text="公開中">
                                </asp:RadioButton>
                                <asp:RadioButton ID="rdoPublishedNG" runat="server" GroupName="Published" Text="非表示">
                                </asp:RadioButton>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                作成日時To
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstToYYYY" runat="server" Width="60px">
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstToMM" runat="server" Width="40px">
                                </asp:DropDownList>月
                                <asp:DropDownList ID="lstToDD" runat="server" Width="40px">
                                </asp:DropDownList>日
                                <asp:DropDownList ID="lstToHH" runat="server" Width="40px">
                                </asp:DropDownList>時
                                <asp:DropDownList ID="lstToMI" runat="server" Width="40px">
                                </asp:DropDownList>分
                            </td>
                            <td class="tdHeaderStyle">
                                オススメ
                            </td>
                            <td class="tdDataStyle">
                                <asp:RadioButton ID="rdoPickupAll" runat="server" Checked="True" GroupName="Pickup"
                                    Text="両方"></asp:RadioButton>
                                <asp:RadioButton ID="rdoPickupOn" runat="server" GroupName="Pickup" Text="オススメ"></asp:RadioButton>
                                <asp:RadioButton ID="rdoPickupOff" runat="server" GroupName="Pickup" Text="非オススメ"></asp:RadioButton>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                キーワード<br />
                                (空白区切り/全てを含む)
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtKeyword" runat="server" Width="300px"></asp:TextBox>
                            </td>
                            <td class="tdHeaderStyle">
                                添付
                            </td>
                            <td class="tdDataStyle">
                                <asp:CheckBox ID="chkPic" runat="server" Text="画像" />
                                <asp:CheckBox ID="chkMovie" runat="server" Text="動画" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle2">
                                チェック状態
                            </td>
                            <td class="tdDataStyle">
								<asp:CheckBox ID="chkChecked" runat="server" Text="チェック済" />
								<asp:CheckBox ID="chkUnChecked" runat="server" Text="未チェック" />
                            </td>
                            <td class="tdHeaderStyle2">
                            </td>
                            <td class="tdDataStyle">
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" />
                <asp:CustomValidator ID="vdcFromTo" runat="server" ErrorMessage="" OnServerValidate="vdcFromTo_ServerValidate"
                    ValidationGroup="Key"></asp:CustomValidator>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[記事一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdBlogArticle.PageIndex + 1 %>
                        of
                        <%= grdBlogArticle.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="443px">
                    <asp:GridView ID="grdBlogArticle" runat="server" AllowPaging="True" AutoGenerateColumns="False" OnSorting="grdBlogArticle_Sorting" EnableSortingAndPagingCallbacks="false"
                        DataSourceID="dsBlogArticle" SkinID="GridViewColor" AllowSorting="true" DataKeyNames="LOGIN_ID,SITE_CD,USER_SEQ,USER_CHAR_NO,BLOG_SEQ,BLOG_ARTICLE_SEQ" OnRowDataBound="grdBlogArticle_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="ﾊﾝﾄﾞﾙ名<br>会員ID" SortExpression="LOGIN_ID">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblHandleNm" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}",Eval("LOGIN_ID"),"WantedApplicantList.aspx") %>'
                                        Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:HyperLink><br />
                                    <asp:Label ID="lblLoginId" runat="server" Text='<%# Eval("LOGIN_ID") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="120px" HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="内容">
                                <ItemTemplate>
                                    <asp:Image ID="imgObj" runat="server" ImageUrl='<%# Eval("SMALL_PHOTO_IMG_PATH", "../{0}") %>'
                                        ImageAlign="left" Visible='<%# GetImageVisible(Eval("BLOG_FILE_TYPE")) %>' />
                                    <asp:HyperLink ID="lnkBlogArticleView" runat="server" NavigateUrl='<%# GetBlogArticleViewLink(Container.DataItem) %>'
                                        Text='<%# GetDisplayBlogArticleBody(Eval("BLOG_ARTICLE_BODY1")) %>' Width="400px"
                                        CssClass="Warp"></asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="作成日時" SortExpression="REGIST_DATE">
                                <ItemTemplate>
                                    <asp:Label ID="lblRegistDate" runat="server" Text='<%# Eval("REGIST_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>'></asp:Label>
                                    <asp:HiddenField ID="hdrPublishStartDate" runat="server" Value='<%# Eval("PUBLISH_START_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="READING_COUNT" SortExpression="READING_COUNT" HeaderText="閲覧数" DataFormatString="{0:#,##0}">
                                <ItemStyle HorizontalAlign="right" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="コメント数">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkBlogCommentList" runat="server" NavigateUrl='<%# string.Format("BlogCommentList.aspx?loginid={0}&blog_article_seq={1}",Eval("LOGIN_ID"),Eval("BLOG_ARTICLE_SEQ")) %>'
                                        Text='<%# Eval("COMMENT_COUNT") %>' Width="50px"
                                        CssClass="Warp"></asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="right" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="PRIORITY" SortExpression="PRIORITY" HeaderText="オススメ順">
                                <ItemStyle HorizontalAlign="center" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="操作">
                                <ItemTemplate>
									<asp:Label ID="lblDelFlag" runat="server" Text="ﾕｰｻﾞｰ削除<br />" Visible='<%# GetDelFlagVisible(Eval("DEL_FLAG")) %>'></asp:Label>
                                    <asp:LinkButton ID="lnkSetPublish" runat="server" CommandArgument='<%# Container.DataItemIndex %>'
                                        Text='<%# GetSetPublishLinkText(Eval("BLOG_ARTICLE_STATUS")) %>' OnCommand="lnkSetPublish_OnCommand"></asp:LinkButton>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="lnkSetDraft" runat="server" CommandArgument='<%# Container.DataItemIndex %>' Visible='<%# GetDraftVisible(Eval("BLOG_ARTICLE_STATUS")) %>'
                                        Text="[下書きに戻す]" OnCommand="lnkSetDraft_OnCommand"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
            <br />
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsBlogArticle" runat="server" SelectMethod="GetPageCollection" ConvertNullToDBNull="true" SortParameterName=""
        TypeName="BlogArticle" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelected="dsBlogArticle_Selected"
        OnSelecting="dsBlogArticle_Selecting">
        <SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
</asp:Content>
