<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ManTreasureMainte.aspx.cs" Inherits="Extension_ManTreasureMainte"
	Title="男性用お宝認証" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="男性用お宝認証"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[男性用お宝認証]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					$NO_TRANS_START;
					<table border="0" style="width: 640px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle">
								ｻｲﾄ／SEQ／ｷｬﾗｸﾀｰNo.
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblSiteCd" runat="server" Text="Label"></asp:Label>-
								<asp:Label ID="lblLoginId" runat="server" Text="Label"></asp:Label>-
								<asp:Label ID="lblUserSeq" runat="server" Text="Label" Visible="false"></asp:Label>
								<asp:Label ID="lblUserCharNo" runat="server" Text="Label"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								ﾊﾝﾄﾞﾙ名/ｹﾞｰﾑﾊﾝﾄﾞﾙ名
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblHandleNm" runat="server" Text="Label"></asp:Label>&nbsp;/&nbsp
								<asp:Label ID="lblGameHandleNm" runat="server" Text="Label"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								出現日
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtDisplayDay" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
								<asp:RangeValidator ID="vdeDisplayDay" runat="server" ControlToValidate="txtDisplayDay" ErrorMessage="出現日を正しく入力して下さい。" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
									Type="Date" ValidationGroup="Detail" Display="Dynamic">*</asp:RangeValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								属性
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstManTreasureAttr" runat="server" DataSourceID="dsManTreasureAttr" DataTextField="CAST_GAME_PIC_ATTR_NM" DataValueField="CAST_GAME_PIC_ATTR_SEQ">
								</asp:DropDownList>
								<asp:RequiredFieldValidator ID="vdrManTreasureAttr" runat="server" ErrorMessage="属性を選択して下さい。" ControlToValidate="lstManTreasureAttr" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								ｺﾒﾝﾄ
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtPicDoc" runat="server" Width="500px" TextMode="MultiLine" Height="100px"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								画像
							</td>
							<td class="tdDataStyle">
								<asp:Image ID="imgPic" runat="server" BorderStyle="none" BorderWidth="0px">
								</asp:Image>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								認証ｽﾃｰﾀｽ
							</td>
							<td class="tdDataStyle">
								<asp:RadioButton ID="rdoOK" runat="server" GroupName="PublishType" Text="認証済">
								</asp:RadioButton>
								<asp:RadioButton ID="rdoNG" runat="server" GroupName="PublishType" Text="未認証">
								</asp:RadioButton>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle2">
								チェック状態
							</td>
							<td class="tdDataStyle">
								<asp:RadioButton ID="rdoChecked" runat="server" GroupName="CheckedType" Text="チェック済">
								</asp:RadioButton>
								<asp:RadioButton ID="rdoUnChecked" runat="server" GroupName="CheckedType" Text="未チェック">
								</asp:RadioButton>
							</td>
						</tr>
					</table>
					$NO_TRANS_END;
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
					<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
					<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsManTreasureAttr" runat="server" SelectMethod="GetList" TypeName="ManTreasureAttr" OnSelecting="dsManTreasureAttr_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:MaskedEditExtender ID="mskDisplayDay" runat="server" TargetControlID="txtDisplayDay" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay"
		ClearMaskOnLostFocus="true">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrManTreasureAttr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdeDisplayDay" HighlightCssClass="validatorCallout" />
</asp:Content>
