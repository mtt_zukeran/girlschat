﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 抽選一覧

--	Progaram ID		: LotteryList
--
--  Creation Date	: 2011.07.19
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.IO;

public partial class Extension_LotteryList : System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		set {
			this.ViewState["SexCd"] = value;
		}
	}

	private string LotterySeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["LotterySeq"]);
		}
		set {
			this.ViewState["LotterySeq"] = value;
		}
	}

	private string GameItemCategoryType {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemCategoryType"]);
		}
		set {
			this.ViewState["GameItemCategoryType"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	private string GameItemGetCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemGetCd"]);
		}
		set {
			this.ViewState["GameItemGetCd"] = value;
		}
	}

	private string GameItemPresent {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemPresent"]);
		}
		set {
			this.ViewState["GameItemPresent"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();
			this.SexCd = iBridUtil.GetStringValue(this.Request.QueryString["sexcd"]);
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
		this.LotterySeq = string.Empty;
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		if (!this.IsCorrectDate()) {
			return;
		}
		this.UpdateData(false);
		this.pnlKey.Enabled = true;
		this.pnlLotteryInfo.Visible = false;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		this.UpdateData(true);
		this.pnlKey.Enabled = true;
		this.pnlLotteryInfo.Visible = false;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.ClearFileds();
		this.pnlKey.Enabled = true;
		this.pnlLotteryInfo.Visible = false;
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.pnlKey.Enabled = false;
		this.pnlLotteryInfo.Visible = true;
	}

	protected void lnkLotteryId_Command(object sender,CommandEventArgs e) {
		
		this.GameItemGetCd = null;
		this.GameItemPresent = null;
		
		if (!this.IsValid) {
			return;
		}
		this.LotterySeq = iBridUtil.GetStringValue(e.CommandArgument);
		this.GetData();
		this.pnlKey.Enabled = false;
		this.pnlLotteryInfo.Visible = true;
	}

	protected void dsLottery_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	protected void lstGameItemCategory_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		this.GameItemCategoryType = oDropDownList.SelectedValue;
		this.lstGameItem.DataBind();
	}

	protected void lstItemGetCd_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		this.GameItemGetCd = oDropDownList.SelectedValue;
		this.lstGameItem.DataBind();
	}

	protected void lstItemPresent_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		this.GameItemPresent = oDropDownList.SelectedValue;
		this.lstGameItem.DataBind();
	}

	protected void dsGameItem_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pGameItemCategoryType"] = this.GameItemCategoryType;
		e.InputParameters["pGameItemGetCd"] = this.GameItemGetCd;
		e.InputParameters["pPresentFlag"] = this.GameItemPresent;
	}

	protected void vdcGameItemGet_ServerValidate(object source,ServerValidateEventArgs args) {
		if (string.IsNullOrEmpty(this.lstGameItem.SelectedValue)) {
			args.IsValid = false;
			this.vdcGameItemGet.ErrorMessage = "ｺﾝﾌﾟﾘｰﾄ時獲得ｱｲﾃﾑを選択してください。";
		}
	}

	private void InitPage() {
		this.pnlInfo.Visible = true;
		this.pnlLotteryInfo.Visible = false;

		this.LotterySeq = string.Empty;
		this.pnlKey.Enabled = true;
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		this.ClearFileds();
	}

	private void ClearFileds() {
		this.lblLotteryId.Text = string.Empty;
		this.txtLotteryNm.Text = string.Empty;
		this.txtTicketNm.Text = string.Empty;
		this.txtPublishStartDate.Text = string.Empty;
		this.txtPublishEndDate.Text = string.Empty;
		this.lstGameItemCategory.DataBind();
		this.lstGameItem.DataBind();
		this.chkPublishFlag.Checked = true;

		this.lblErrorMessagePublishEnd.Visible = false;
		this.lblErrorMessagePublishStart.Visible = false;
		this.lblErrorMessagePublishEnd.Text = string.Empty;
		this.lblErrorMessagePublishStart.Text = string.Empty;

		this.lstItemGetCd.SelectedValue = null;
		this.lstItemPresent.SelectedValue = null;
		this.GameItemGetCd = null;
		this.GameItemPresent = null;
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.LotterySeq = string.Empty;

		this.grdLottery.PageIndex = 0;
		this.grdLottery.DataSourceID = "dsLottery";
		this.grdLottery.DataBind();
		this.pnlCount.DataBind();
	}

	private void GetData() {
		this.ClearFileds();

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("LOTTERY_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pLOTTERY_SEQ",DbSession.DbType.VARCHAR2,this.LotterySeq);
			oDbSession.ProcedureOutParm("pLOTTERY_NM",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTICKET_NM",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSEX_CD",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pCOMPLETE_GET_ITEM_SEQ",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pCOMPLETE_GET_ITEM_CATEGORY",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPUBLISH_START_DATE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPUBLISH_END_DATE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPUBLISH_FLAG",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.lblLotteryId.Text = oDbSession.GetStringValue("pLOTTERY_SEQ");
			this.txtLotteryNm.Text = oDbSession.GetStringValue("pLOTTERY_NM");
			this.txtTicketNm.Text = oDbSession.GetStringValue("pTICKET_NM");
			this.txtPublishStartDate.Text = oDbSession.GetStringValue("pPUBLISH_START_DATE");
			this.txtPublishEndDate.Text = oDbSession.GetStringValue("pPUBLISH_END_DATE");
			this.chkPublishFlag.Checked = ViCommConst.FLAG_ON_STR.Equals(oDbSession.GetStringValue("pPUBLISH_FLAG"));
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");

			this.lstGameItemCategory.SelectedValue = oDbSession.GetStringValue("pCOMPLETE_GET_ITEM_CATEGORY");
			this.GameItemCategoryType = this.lstGameItemCategory.SelectedValue;
			this.lstGameItem.DataBind();
			this.lstGameItem.SelectedValue = oDbSession.GetStringValue("pCOMPLETE_GET_ITEM_SEQ");
		}
	}

	protected string GetPublishFlagMark(object pPublishFlag) {
		string sPublishFlag = iBridUtil.GetStringValue(pPublishFlag);

		return ViCommConst.FLAG_ON_STR.Equals(sPublishFlag) ? "公開中" : "非公開中";
	}

	private bool IsCorrectDate() {
		this.lblErrorMessagePublishEnd.Visible = false;
		this.lblErrorMessagePublishStart.Visible = false;
		this.lblErrorMessagePublishEnd.Text = string.Empty;
		this.lblErrorMessagePublishStart.Text = string.Empty;

		DateTime? dtPublishStart = SysPrograms.TryParseOrDafult(this.txtPublishStartDate.Text,(DateTime?)null);
		DateTime? dtPublishEnd = SysPrograms.TryParseOrDafult(this.txtPublishEndDate.Text,(DateTime?)null);

		if (!dtPublishStart.HasValue) {
			this.lblErrorMessagePublishStart.Visible = true;
			this.lblErrorMessagePublishStart.Text = "公開開始日時を正しく入力してください。";
			return false;
		}
		if (!dtPublishEnd.HasValue) {
			this.lblErrorMessagePublishEnd.Visible = true;
			this.lblErrorMessagePublishEnd.Text = "公開終了日時を正しく入力してください。";
			return false;
		}
		if (dtPublishEnd < dtPublishStart) {
			this.lblErrorMessagePublishEnd.Visible = true;
			this.lblErrorMessagePublishEnd.Text = "公開期間の大小関係が不正です。";
			return false;
		}
		
		/*
		bool bResult;
		using (Lottery oLottery = new Lottery()) {
			bResult = oLottery.IsDuplicateDate(this.SiteCd,this.LotterySeq,this.SexCd,dtPublishStart.Value,dtPublishEnd.Value);
			if (bResult) {
				this.lblErrorMessagePublishEnd.Visible = true;
				this.lblErrorMessagePublishEnd.Text = "公開期間が他の抽選設定と重複しています。";
				return false;
			}
		}
		*/

		return true;
	}

	private void UpdateData(bool pDelFlag) {
		DateTime? dtPublishStart = SysPrograms.TryParseOrDafult(this.txtPublishStartDate.Text,(DateTime?)null);
		DateTime? dtPublishEnd = SysPrograms.TryParseOrDafult(this.txtPublishEndDate.Text,(DateTime?)null);
		if (dtPublishEnd.HasValue) {
			dtPublishEnd = (DateTime?)dtPublishEnd.Value.AddSeconds(-1);
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("LOTTERY_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureBothParm("pLOTTERY_SEQ",DbSession.DbType.VARCHAR2,this.LotterySeq);
			oDbSession.ProcedureInParm("pLOTTERY_NM",DbSession.DbType.VARCHAR2,this.txtLotteryNm.Text);
			oDbSession.ProcedureInParm("pTICKET_NM",DbSession.DbType.VARCHAR2,this.txtTicketNm.Text);
			oDbSession.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,this.SexCd);
			oDbSession.ProcedureInParm("pCOMPLETE_GET_ITEM_SEQ",DbSession.DbType.VARCHAR2,this.lstGameItem.SelectedValue);
			oDbSession.ProcedureInParm("pPUBLISH_START_DATE",DbSession.DbType.DATE,dtPublishStart);
			oDbSession.ProcedureInParm("pPUBLISH_END_DATE",DbSession.DbType.DATE,dtPublishEnd);
			oDbSession.ProcedureInParm("pPUBLISH_FLAG",DbSession.DbType.VARCHAR2,this.chkPublishFlag.Checked ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.ClearFileds();
		this.GetList();
	}

	protected void lstItemGetCd_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		oDropDownList.Items.Insert(3,new ListItem("戦利品","3"));
	}
}
