﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: この娘を探せ・ヒント設定
--	Progaram ID		: WantedHintMainte
--
--  Creation Date	: 2011.03.23
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using System.Collections.Generic;

public partial class Extension_WantedHintMainte : System.Web.UI.Page {
	private enum Priorities {
		First = 1,
		Second,
		Third,
	}

	private const int DAYS_IN_WEEK = 7;

	protected string[] DummyWeekHintArray {
		get {
			return new string[DAYS_IN_WEEK];
		}
	}

	protected DataSet WantedHintData {
		get {
			return this.ViewState["WANTED_HINT"] as DataSet;
		}
		set {
			this.ViewState["WANTED_HINT"] = value;
		}
	}

	protected void Page_Load(object sender, EventArgs e) {
		if (!this.IsPostBack) {
			this.InitPage();

			this.GetList();
			this.pnlList.Visible = true;
		}
	}

	protected void btnListSeek_Click(object sender, EventArgs e) {
		this.GetList();
		this.pnlList.Visible = true;
	}

	protected void btnClear_Click(object sender, EventArgs e) {
		this.InitPage();
	}

	protected void btnUpdate_Click(object sender, EventArgs e) {
		if (this.IsValid) {
			this.UpateData();
		}
	}

	protected void lstCastAttrType_DataBound(object sender, EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList != null) {
			oDropDownList.Items.Insert(0, new ListItem(string.Empty, string.Empty));
		}
	}

	protected void vdcAll_ServerValidate(object source, ServerValidateEventArgs e) {
		if (!this.IsValid) {
			return;
		}

		for (int iDayCd = 1; iDayCd <= DAYS_IN_WEEK; iDayCd++) {
			List<string> oCastAttrTypeList = new List<string>();

			foreach (int iPriority in Enum.GetValues(typeof(Priorities))) {
				string sCastAttrTypeSeq = this.GetCastAttrTypeList(iDayCd.ToString(), iPriority.ToString()).SelectedValue;

				if (string.IsNullOrEmpty(sCastAttrTypeSeq)) {
					this.vdcAll.ErrorMessage = "ヒントを設定してください。";
					e.IsValid = false;
					return;
				} 
				if (oCastAttrTypeList.Contains(sCastAttrTypeSeq)) {
					this.vdcAll.ErrorMessage = "同じ曜日で重複したヒントが設定されています。";
					e.IsValid = false;
					return;
				} else {
					oCastAttrTypeList.Add(sCastAttrTypeSeq);
				}
			}
		}
	}

	private void InitPage() {
		this.DataBind();
		this.lstSeekSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		this.pnlList.Visible = false;
	}

	private DropDownList GetCastAttrTypeList(string pDayCd, string pPriority) {
		Priorities ePriority = (Priorities)int.Parse(pPriority);

		Repeater oRepeter;
		switch (ePriority) {
			case Priorities.First:
				oRepeter = this.rptHint1;
				break;
			case Priorities.Second:
				oRepeter = this.rptHint2;
				break;
			case Priorities.Third:
				oRepeter = this.rptHint3;
				break;
			default:
				return null;
		}

		int iColumnIndex = int.Parse(pDayCd) - 1;
		DropDownList oDropDownList = oRepeter.Items[iColumnIndex].FindControl("lstCastAttrType") as DropDownList;

		return oDropDownList;
	}

	private void GetList() {
		using (WantedHint oWantedHint = new WantedHint()) {
			this.WantedHintData = oWantedHint.GetList(this.lstSeekSiteCd.SelectedValue);
		}

		foreach (DataRow oDataRow in this.WantedHintData.Tables[0].Rows) {
			DropDownList lstCastAttrType = this.GetCastAttrTypeList(iBridUtil.GetStringValue(oDataRow["DAY_CD"]), iBridUtil.GetStringValue(oDataRow["PRIORITY"]));
			if (lstCastAttrType != null) {
				lstCastAttrType.SelectedValue = iBridUtil.GetStringValue(oDataRow["CAST_ATTR_TYPE_SEQ"]);
			}
		}
	}

	private void UpateData() {
		DataTable oWantedHintTable = this.WantedHintData.Tables[0].Copy();
		oWantedHintTable.PrimaryKey = new DataColumn[] { oWantedHintTable.Columns["SITE_CD"], oWantedHintTable.Columns["DAY_CD"], oWantedHintTable.Columns["PRIORITY"] };

		List<string> oDayCdList = new List<string>();
		List<string> oPriorityList = new List<string>();
		List<string> oCastAttrTypeSeqList = new List<string>();
		List<string> oRevisionNoList = new List<string>();

		for (int iDayCd = 1; iDayCd <= DAYS_IN_WEEK; iDayCd++) {
			foreach (int iPriority in Enum.GetValues(typeof(Priorities))) {
				oDayCdList.Add(iDayCd.ToString());
				oPriorityList.Add(iPriority.ToString());
				oCastAttrTypeSeqList.Add(this.GetCastAttrTypeList(iDayCd.ToString(), iPriority.ToString()).SelectedValue);

				DataRow oFindRow = oWantedHintTable.Rows.Find(new object[] { this.lstSeekSiteCd.SelectedValue, iDayCd.ToString(), iPriority.ToString() });
				if (oFindRow == null) {
					oRevisionNoList.Add(null);
				} else {
					oRevisionNoList.Add(iBridUtil.GetStringValue(oFindRow["REVISION_NO"]));
				}
			}
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("WANTED_HINT_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.lstSeekSiteCd.SelectedValue);
			oDbSession.ProcedureInArrayParm("pDAY_CD", DbSession.DbType.VARCHAR2, oDayCdList.ToArray());
			oDbSession.ProcedureInArrayParm("pPRIORITY", DbSession.DbType.VARCHAR2, oPriorityList.ToArray());
			oDbSession.ProcedureInArrayParm("pCAST_ATTR_TYPE_SEQ", DbSession.DbType.VARCHAR2, oCastAttrTypeSeqList.ToArray());
			oDbSession.ProcedureInArrayParm("pREVISION_NO", DbSession.DbType.VARCHAR2, oRevisionNoList.ToArray());
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.GetList();
	}

}
