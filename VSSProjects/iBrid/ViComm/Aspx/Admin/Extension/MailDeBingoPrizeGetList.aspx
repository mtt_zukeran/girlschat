<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MailDeBingoPrizeGetList.aspx.cs" Inherits="Extension_MailDeBingoPrizeGetList"
	Title="Untitled Page" ValidateRequest="false" %>

<%@ Import Namespace="System" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="[derS@Üàl¾Ò"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[õð]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 820px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							TCg
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" Enabled="false" DataValueField="SITE_CD" Width="180px">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle">
							OCID
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtLoginId" runat="server" Width="80px" MaxLength="11"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							WvúÔ
						</td>
						<td class="tdDataStyle" style="width: 300px">
							<asp:Label ID="lblDate" runat="server"></asp:Label>
						</td>
						<td class="tdHeaderStyle">
							Gg[l
						</td>
						<td class="tdDataStyle">
							<asp:Label ID="lblEntryCount" runat="server"></asp:Label>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							JACKPOT
						</td>
						<td class="tdDataStyle" style="width: 300px">
							<asp:Label ID="lblJackpot" runat="server"></asp:Label>
						</td>
						<td class="tdHeaderStyle2">
							vt^pt(àz)
						</td>
						<td class="tdDataStyle">
							<asp:Label ID="lblPrize" runat="server"></asp:Label>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="õ" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnBack" Text="ßé" CssClass="seekbutton" OnClick="btnBack_Click" />
				<asp:Button runat="server" ID="btnCsv" Text="bruoÍ" CssClass="seekbutton" OnClick="btnCSV_Click" />
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[Üàl¾Òê]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count
						<%#GetRecCount() %>
					</a>
					<br />
					<a class="reccount">Current viewing page
						<%=grdBingoEntry.PageIndex + 1%>
						of
						<%=grdBingoEntry.PageCount%>
					</a>
				</asp:Panel>
				&nbsp;
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="500px">
					<asp:GridView ID="grdBingoEntry" runat="server" AllowPaging="True" AutoGenerateColumns="False" EnableViewState="true" DataSourceID="dsBingoEntry" AllowSorting="True"
						SkinID="GridViewColor" OnSorting="grdBingoEntry_Sorting">
						<Columns>
							<asp:TemplateField HeaderText="Û¸Þ²ÝID" SortExpression="LOGIN_ID">
								<ItemTemplate>
									<asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# GetViewUrl(Eval("SITE_CD"), Eval("LOGIN_ID"), Eval("SEX_CD"),Eval("BINGO_ENTRY_SEQ")) %>'
										Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="HANDLE_NM" HeaderText="ÊÝÄÞÙ¼" />
							<asp:TemplateField HeaderText="ËÞÝºÞ¶°ÄÞ" SortExpression="BINGO_CARD_NO">
								<ItemTemplate>
									<asp:Label ID="lblBingoCardNo" runat="server" Text='<%# string.Format("{0}Ú",Eval("BINGO_CARD_NO")) %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="BINGO_ELECTION_POINT" HeaderText="t^pt" ItemStyle-HorizontalAlign="Right" SortExpression="BINGO_ELECTION_POINT" />
							<asp:TemplateField HeaderText="ptt^" SortExpression="BINGO_POINT_PAYMENT_FLAG">
								<ItemTemplate>
									<asp:Label ID="lblBingoPointPayment" runat="server" Text='<%# Eval("BINGO_POINT_PAYMENT_FLAG").ToString().Equals("1") ? "Ï" : "¢" %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
                            <asp:BoundField DataField="BINGO_APPLICATION_DATE" HeaderText="®¬\¿ú" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False" SortExpression="BINGO_APPLICATION_DATE">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="ê­ËÞÝºÞ">
								<ItemTemplate>
									<asp:Label ID="lblBingoBallFlag" runat="server" Text='<%# Eval("BINGO_BALL_FLAG").ToString().Equals("1") ? "ê­ËÞÝºÞ" : "" %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsBingoEntry" runat="server" SelectMethod="GetPageCollection" TypeName="BingoEntry" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsBingoEntry_Selected" OnSelecting="dsBingoEntry_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pBingoTermSeq" Type="String" />
			<asp:Parameter Name="pLoginId" Type="String" />
			<asp:Parameter Name="pSexCd" Type="String" />
            <asp:Parameter Name="pSortExpression" Type="String" />
			<asp:Parameter Name="pSortDirection" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
</asp:Content>
