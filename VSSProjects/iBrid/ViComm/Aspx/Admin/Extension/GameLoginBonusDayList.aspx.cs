﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 日別ｹﾞｰﾑﾛｸﾞｲﾝﾎﾞｰﾅｽ一覧

--	Progaram ID		: GameLoginBonusDayList
--
--  Creation Date	: 2011.07.25
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Ratelain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.IO;

public partial class Extension_GameLoginBonusDayList : System.Web.UI.Page {
	protected static readonly string[] MinuteArray = new string[] { "00","15","30","45" };
	protected static readonly string[] HourArray = new string[] { "00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23" };
	protected static readonly string[] DayArray = new string[] { "01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31" };
	protected static readonly string[] MonthArray = new string[] { "01","02","03","04","05","06","07","08","09","10","11","12" };
	protected string[] YearArray;
	protected string[] DummyArray = new string[3];
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		set {
			this.ViewState["SexCd"] = value;
		}
	}

	private string GameLoginBonusSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameLoginBonusSeq"]);
		}
		set {
			this.ViewState["GameLoginBonusSeq"] = value;
		}
	}

	private string GameItemSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemSeq"]);
		}
		set {
			this.ViewState["GameItemSeq"] = value;
		}
	}

	private string GameItemCategoryType {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemCategoryType"]);
		}
		set {
			this.ViewState["GameItemCategoryType"] = value;
		}
	}

	private string CreateFlag {
		get {
			return iBridUtil.GetStringValue(this.ViewState["CreateFlag"]);
		}
		set {
			this.ViewState["CreateFlag"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	private string GameItemGetCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemGetCd"]);
		}
		set {
			this.ViewState["GameItemGetCd"] = value;
		}
	}

	private string GameItemPresent {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemPresent"]);
		}
		set {
			this.ViewState["GameItemPresent"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.FirstLoad();
			this.InitPage();

			this.SiteCd = iBridUtil.GetStringValue(this.Request.QueryString["sitecd"]);
			this.SexCd = iBridUtil.GetStringValue(this.Request.QueryString["sexcd"]);
			this.CreateFlag = iBridUtil.GetStringValue(this.Request.QueryString["create"]);

			if (!string.IsNullOrEmpty(this.SiteCd) && !string.IsNullOrEmpty(this.GameLoginBonusSeq)) {
				this.lstSiteCd.SelectedValue = this.SiteCd;

				if (this.CreateFlag.Equals(ViCommConst.FLAG_ON_STR)) {
					this.GameItemSeq = string.Empty;
					this.pnlKey.Enabled = false;
					this.pnlItemInfo.Visible = true;
				} else {
					this.GetList();
				}
			}
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
		this.GameItemSeq = string.Empty;
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		if (!this.IsCorrectInput()) {
			return;
		}
		if (!this.UpdateData(false)) {
			return;
		}
		this.pnlItemInfo.Visible = false;
		this.pnlKey.Enabled = true;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		this.UpdateData(true);
		this.pnlItemInfo.Visible = false;
		this.pnlKey.Enabled = true;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlItemInfo.Visible = false;
		this.pnlKey.Enabled = true;
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		this.GameLoginBonusSeq = string.Empty;
		this.ClearFileds();
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.GameItemSeq = string.Empty;
		this.CreateFlag = ViCommConst.FLAG_ON_STR;
		this.pnlKey.Enabled = false;
		this.pnlItemInfo.Visible = true;
		this.ClearFileds();
	}

	protected void lnkEdit_Command(object sender,CommandEventArgs e) {
		
		this.GameItemGetCd = null;
		this.GameItemPresent = null;
		
		if (!this.IsValid) {
			return;
		}
		this.SiteCd = this.lstSiteCd.SelectedValue;
		int iIndex = int.Parse(iBridUtil.GetStringValue(e.CommandArgument));

		GridViewRow oRow = this.grdGameLoginBonusDay.Rows[iIndex];
		HiddenField oGameLoginBonusSeqHiddenField = oRow.FindControl("hdnGameLoginBonusSeq") as HiddenField;
		HiddenField oGameItemSeqHiddenFiled = oRow.FindControl("hdnGameItemSeq") as HiddenField;

		this.GameLoginBonusSeq = oGameLoginBonusSeqHiddenField.Value;
		this.GameItemSeq = oGameItemSeqHiddenFiled.Value;
		this.CreateFlag = string.Empty;
		this.pnlKey.Enabled = false;
		this.GetData();

		this.pnlItemInfo.Visible = true;
	}

	protected void dsGameLoginBonusDay_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsGameItem_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pGameItemCategoryType"] = this.GameItemCategoryType;
		e.InputParameters["pGameItemGetCd"] = this.GameItemGetCd;
		e.InputParameters["pPresentFlag"] = this.GameItemPresent;
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	protected void lstGameItemCategory_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		this.GameItemCategoryType = oDropDownList.SelectedValue;
		this.lstGameItem.DataBind();
	}

	protected void lstItemGetCd_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		this.GameItemGetCd = oDropDownList.SelectedValue;
		this.lstGameItem.DataBind();
	}

	protected void lstItemPresent_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		this.GameItemPresent = oDropDownList.SelectedValue;
		this.lstGameItem.DataBind();
	}

	private void InitPage() {
		this.pnlInfo.Visible = true;
		this.pnlItemInfo.Visible = false;

		this.GameLoginBonusSeq = string.Empty;
		this.GameItemSeq = string.Empty;
		this.pnlKey.Enabled = true;
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void FirstLoad() {
		this.YearArray = new string[] { DateTime.Today.AddYears(-1).ToString("yyyy"),DateTime.Today.ToString("yyyy"),DateTime.Today.AddYears(1).ToString("yyyy") };
		this.pnlItemInfo.DataBind();
	}
	private void ClearFileds() {
		this.txtItemCount.Text = string.Empty;
		this.txtGamePoint.Text = string.Empty;
		this.lstGameItemCategory.DataBind();
		this.lstGameItem.DataBind();
		this.lblErrorMessageItem.Visible = false;
		this.lblErrorMessageCount.Visible = false;
		this.lblErrorMessageGamePoint.Visible = false;
		this.lblErrorMessageFrom.Visible = false;
		this.lblErrorMessageTo.Visible = false;
		this.lstFromYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstFromMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstFromDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstFromHH.SelectedValue = DateTime.Now.ToString("HH");
		this.lstToYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstToMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstToDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstToHH.SelectedValue = DateTime.Now.ToString("HH");
		this.lstItemGetCd.SelectedValue = null;
		this.lstItemPresent.SelectedValue = null;
		this.GameItemGetCd = null;
		this.GameItemPresent = null;
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	protected string CheckGamePointOnly(object pGameItemNm) {
		if (iBridUtil.GetStringValue(pGameItemNm).Equals(string.Empty)) {
			return "ｱｲﾃﾑなし";
		} else {
			return pGameItemNm.ToString();
		}
	}

	private void GetList() {
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.GameItemSeq = string.Empty;

		this.grdGameLoginBonusDay.PageIndex = 0;
		this.grdGameLoginBonusDay.DataSourceID = "dsGameLoginBonusDay";
		this.grdGameLoginBonusDay.DataBind();
		this.pnlCount.DataBind();
	}

	private void GetData() {
		this.ClearFileds();

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("GAME_LOGIN_BONUS_DAY_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pGAME_LOGIN_BONUS_SEQ",DbSession.DbType.VARCHAR2,this.GameLoginBonusSeq);
			oDbSession.ProcedureOutParm("pGAME_LOGIN_BONUS_START_DATE",DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pGAME_LOGIN_BONUS_END_DATE",DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pSEX_CD",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pGAME_ITEM_SEQ",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pGAME_ITEM_CATEGORY_TYPE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pITEM_COUNT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pGAME_POINT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			string sFromDate = string.Format("{0:yyyy/MM/dd HH:mm:ss}",oDbSession.GetDateTimeValue("pGAME_LOGIN_BONUS_START_DATE"));
			DateTime? oToDate = oDbSession.GetDateTimeValue("pGAME_LOGIN_BONUS_END_DATE");
			if (oToDate != null) {
				oToDate = oToDate.Value.AddSeconds(1);
			}
			string sToDate = string.Format("{0:yyyy/MM/dd HH:mm:ss}",oToDate);
			if (!string.IsNullOrEmpty(sFromDate)) {
				this.lstFromYYYY.SelectedValue = sFromDate.Substring(0,4);
				this.lstFromMM.SelectedValue = sFromDate.Substring(5,2);
				this.lstFromDD.SelectedValue = sFromDate.Substring(8,2);
				this.lstFromHH.SelectedValue = sFromDate.Substring(11,2);
				if (this.lstFromMI.Items.FindByValue(sFromDate.Substring(14,2)) != null) {
					this.lstFromMI.SelectedValue = sFromDate.Substring(14,2);
				}
				/*変更できないと不便らしい
								if (DateTime.Parse(sFromDate) < DateTime.Now) {
									this.lstFromYYYY.Enabled = false;
									this.lstFromMM.Enabled = false;
									this.lstFromDD.Enabled = false;
									this.lstFromHH.Enabled = false;
									this.lstFromMI.Enabled = false;
								}
				 */
			}

			if (!string.IsNullOrEmpty(sToDate)) {
				this.lstToYYYY.SelectedValue = sToDate.Substring(0,4);
				this.lstToMM.SelectedValue = sToDate.Substring(5,2);
				this.lstToDD.SelectedValue = sToDate.Substring(8,2);
				this.lstToHH.SelectedValue = sToDate.Substring(11,2);
				if (this.lstToMI.Items.FindByValue(sToDate.Substring(14,2)) != null) {
					this.lstToMI.SelectedValue = sToDate.Substring(14,2);
				}
				/*
								if (DateTime.Parse(sToDate) < DateTime.Now) {
									this.lstToYYYY.Enabled = false;
									this.lstToMM.Enabled = false;
									this.lstToDD.Enabled = false;
									this.lstToHH.Enabled = false;
									this.lstToMI.Enabled = false;
								}
				 */
			}
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
			this.txtItemCount.Text = oDbSession.GetStringValue("pITEM_COUNT");
			this.txtGamePoint.Text = oDbSession.GetStringValue("pGAME_POINT");
			this.lstGameItem.DataBind();
			if (!oDbSession.GetStringValue("pGAME_ITEM_CATEGORY_TYPE").Equals(string.Empty)) {
				this.lstGameItemCategory.SelectedValue = oDbSession.GetStringValue("pGAME_ITEM_CATEGORY_TYPE");
				this.GameItemCategoryType = this.lstGameItemCategory.SelectedValue;
				this.lstGameItem.SelectedValue = oDbSession.GetStringValue("pGAME_ITEM_SEQ");
			}
		}
	}

	private bool UpdateData(bool pDeleteFlag) {
		DateTime dtFrom = new DateTime();
		DateTime dtTo = new DateTime();
		if (!pDeleteFlag) {
			if (!this.CheckDate(out dtFrom,out dtTo)) {
				return false;
			}
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("GAME_LOGIN_BONUS_DAY_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureBothParm("pGAME_LOGIN_BONUS_SEQ",DbSession.DbType.VARCHAR2,this.GameLoginBonusSeq);
			oDbSession.ProcedureInParm("pGAME_LOGIN_BONUS_START_DATE",DbSession.DbType.DATE,dtFrom);
			oDbSession.ProcedureInParm("pGAME_LOGIN_BONUS_END_DATE",DbSession.DbType.DATE,dtTo);
			oDbSession.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,this.SexCd);
			oDbSession.ProcedureInParm("pGAME_ITEM_SEQ",DbSession.DbType.VARCHAR2,this.lstGameItem.SelectedValue);
			oDbSession.ProcedureInParm("pITEM_COUNT",DbSession.DbType.NUMBER,this.txtItemCount.Text);
			oDbSession.ProcedureInParm("pGAME_POINT",DbSession.DbType.NUMBER,this.txtGamePoint.Text);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,pDeleteFlag ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.ClearFileds();
		this.GetList();
		return true;
	}

	private bool IsCorrectInput() {
		this.lblErrorMessageItem.Visible = false;
		this.lblErrorMessageCount.Visible = false;
		this.lblErrorMessageGamePoint.Visible = false;

		bool bResult = true;


		if (string.IsNullOrEmpty(this.lstGameItem.SelectedValue) && string.IsNullOrEmpty(this.txtGamePoint.Text)) {
			this.lblErrorMessageItem.Text = "ボーナスアイテムもしくはゲームポイントを選択してください。";
			this.lblErrorMessageItem.Visible = true;
			bResult = false;
		}

		if (string.IsNullOrEmpty(this.txtItemCount.Text)) {
			this.txtItemCount.Text = "0";
		}
		if (string.IsNullOrEmpty(this.txtGamePoint.Text)) {
			this.txtGamePoint.Text = "0";
		}
		/*
				using (GameLoginBonusDay oGameLoginBonus = new GameLoginBonusDay()) {
					if (ViCommConst.FLAG_ON_STR.Equals(this.CreateFlag) && oGameLoginBonus.IsDupulicateItem(this.SiteCd,this.GameLoginBonusDay,this.lstGameItem.SelectedValue)) {
						this.lblErrorMessageItem.Text = "ボーナスアイテムが重複しています。";
						this.lblErrorMessageItem.Visible = true;
						bResult = false;
					}
				}
		*/

		return bResult;
	}
	private bool CheckDate(out DateTime dtFrom,out DateTime dtTo) {

		dtFrom = new DateTime();
		dtTo = new DateTime();

		this.lblErrorMessageFrom.Visible = false;
		this.lblErrorMessageTo.Visible = false;
		if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:00",
			this.lstFromYYYY.Text,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue,this.lstFromHH.SelectedValue,this.lstFromMI.SelectedValue),out dtFrom)) {
			this.lblErrorMessageFrom.Text = "日時を正しく入力してください";
			this.lblErrorMessageFrom.Visible = true;
			return false;
		}
		if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:00",
			this.lstToYYYY.Text,this.lstToMM.SelectedValue,this.lstToDD.SelectedValue,this.lstToHH.SelectedValue,this.lstToMI.SelectedValue),out dtTo)) {
			this.lblErrorMessageTo.Text = "日時を正しく入力してください";
			this.lblErrorMessageTo.Visible = true;
			return false;
		} else {
			dtTo = dtTo.AddSeconds(-1);
		}
		if (dtFrom > dtTo) {
			this.lblErrorMessageTo.Text = "日時の大小関係を正しく入力してください";
			this.lblErrorMessageTo.Visible = true;
			return false;
		}

		return true;
	}

	protected void lstItemGetCd_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		oDropDownList.Items.Insert(3,new ListItem("戦利品","3"));
	}

}
