﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: サイト構成拡張設定
--	Progaram ID		: SiteManagementExList
--
--  Creation Date	: 2011.03.29
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using iBridCommLib;
using System.Drawing;
using ViComm;

public partial class Extension_SiteManagementExList:System.Web.UI.Page {
	string recCount;

	protected string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		private set {
			this.ViewState["SiteCd"] = value;
		}
	}

	protected string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		private set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	protected string Rowid {
		get {
			return iBridUtil.GetStringValue(this.ViewState["Rowid"]);
		}
		private set {
			this.ViewState["Rowid"] = value;
		}
	}

	protected List<string> HourList {
		get {
			return this.ViewState["HourList"] as List<string>;
		}
		set {
			this.ViewState["HourList"] = value;
		}
	}

	protected List<string> MinuteList {
		get {
			return this.ViewState["MinuteList"] as List<string>;
		}
		set {
			this.ViewState["MinuteList"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		if (!this.IsPostBack) {
			this.CreateLists();
			this.InitPage();
		}
	}

	protected void lnkSiteCd_Command(object sender,CommandEventArgs e) {
		this.SiteCd = e.CommandArgument.ToString();
		this.pnlMainte.Visible = true;
		this.pnlDtl.Visible = true;
		this.pnlDtlTransfer.Visible = false;

		int iCompanyMask = int.Parse(Session["CompanyMask"].ToString());
		this.plcSiteEx.Visible = true;
		this.plcSocialGame.Visible = true;

		SetMailTemplate();
		this.GetData();
	}

	protected void lnkTransfer_Command(object sender,CommandEventArgs e) {
		this.SiteCd = e.CommandArgument.ToString();
		this.pnlMainte.Visible = true;
		this.pnlDtl.Visible = false;
		this.pnlDtlTransfer.Visible = true;
		this.GetData();
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlMainte.Visible = false;
		this.pnlDtl.Visible = false;
		this.pnlDtlTransfer.Visible = false;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		
		if (!this.IsCorrectDate()) {
			return;
		}

		this.UpdateData(ViCommConst.FLAG_OFF);
		this.InitPage();
	}

	protected void dsSiteManagementEx_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		this.pnlMainte.Visible = false;
		this.pnlDtl.Visible = false;
		this.pnlDtlTransfer.Visible = false;
		this.ClearFields();
		this.GetList();
	}

	private void ClearFields() {
		this.pnlDtl.DataBind();
		this.lstFirstHintAnnounceHour.SelectedIndex = 0;
		this.lstFirstHintAnnounceMinute.SelectedIndex = 0;
		this.lstSecondHintAnnounceHour.SelectedIndex = 0;
		this.lstSecondHintAnnounceMinute.SelectedIndex = 0;
		this.lstThirdHintAnnounceHour.SelectedIndex = 0;
		this.lstThirdHintAnnounceMinute.SelectedIndex = 0;
		this.txtBountyPoint.Text = string.Empty;
		this.txtBuyPointMailCommDays.Text = string.Empty;
		this.txtLoveListProfileDays.Text = string.Empty;
		this.txtDocomoMoneyTransferTEL.Text = string.Empty;
		this.txtDocomoMoneyTransferNM.Text = string.Empty;
		this.txtMailRequestOpenStartDate.Text = string.Empty;
		this.txtMailRequestOpenEndDate.Text = string.Empty;
		this.txtRegistCastAgeMin.Text = "0";
	}

	private void GetData() {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("SITE_MANAGEMENT_EX_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureOutParm("pWANTED_BOUNTY_POINT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pWANTED_MONTHLY_BOUNTY_POINT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pBUY_POINT_MAIL_COMM_DAYS",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pLOVE_LIST_PROFILE_DAYS",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pDOCOMO_MONEY_TRANSFER_TEL",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pDOCOMO_MONEY_TRANSFER_NM",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pCAST_FAVORITE_POINT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pCAST_REFUSE_POINT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pTP_MAN_MAIL_MOVIE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTP_MAN_MAIL_PIC",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTP_MAN_PF_PIC",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTP_MAN_TEL_FAIL_VOICE_TV",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTP_MAN_TEL_FAIL_TV_VOICE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTP_MAN_WAITING_CAST_WITH_TALK",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTP_MAN_TALK_BUSY",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTP_MAN_OUTSIDE_TEMP_REGIST",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTP_CAST_MAIL_MOVIE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTP_CAST_MAIL_PIC",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTP_CAST_BBS_MOVIE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTP_CAST_BBS_PIC",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTP_CAST_PF_MOVIE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTP_CAST_PF_PIC",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTP_CAST_TEL_FAIL_VOICE_TV",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTP_CAST_TEL_FAIL_TV_VOICE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTP_CAST_TALK_BUSY",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTP_CAST_SOCIAL_GAME_MOVIE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTP_CAST_SOCIAL_GAME_PIC",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pMAN_ADD_BINGO_JACKPOT_MIN",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pMAN_ADD_BINGO_JACKPOT_MAX",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pMAN_JACKPOT_DENOMINATOR",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pCAST_ADD_BINGO_JACKPOT_MIN",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pCAST_ADD_BINGO_JACKPOT_MAX",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pCAST_JACKPOT_DENOMINATOR",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pBBS_BINGO_JACKPOT_MIN",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pBBS_BINGO_JACKPOT_MAX",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pBBS_BINGO_JACKPOT_DENOMINATOR",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pREGIST_CAST_AGE_MIN",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pTX_MAIL_WAIT_START_PREV_MIN",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pTX_MAIL_LOGIN_START_PREV_MIN",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pMAIL_REQUEST_OPEN_START_DATE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pMAIL_REQUEST_OPEN_END_DATE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pFIRST_HINT_ANNOUNCE_TIME",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSECOND_HINT_ANNOUNCE_TIME",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTHIRD_HINT_ANNOUNCE_TIME",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pLATEST_GCAPP_VER_IOS",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pLATEST_GCAPP_VER_ANDROID",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pFIRST_SETTLE_ENABLE_DAYS",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREMINDER_SMS_DOC",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pROWID",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pROW_COUNT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
			this.Rowid = oDbSession.GetStringValue("pROWID");

			if (int.Parse(oDbSession.GetStringValue("pROW_COUNT")) > 0) {
				string sFirstHintAnnounceTime = oDbSession.GetStringValue("pFIRST_HINT_ANNOUNCE_TIME");
				string sSecondHintAnnounceTime = oDbSession.GetStringValue("pSECOND_HINT_ANNOUNCE_TIME");
				string sThirdHintAnnounceTime = oDbSession.GetStringValue("pTHIRD_HINT_ANNOUNCE_TIME");

				this.pnlDtl.DataBind();
				this.lstFirstHintAnnounceHour.SelectedValue = this.GetHour(sFirstHintAnnounceTime);
				this.lstFirstHintAnnounceMinute.SelectedValue = this.GetMinute(sFirstHintAnnounceTime);
				this.lstSecondHintAnnounceHour.SelectedValue = this.GetHour(sSecondHintAnnounceTime);
				this.lstSecondHintAnnounceMinute.SelectedValue = this.GetMinute(sSecondHintAnnounceTime);
				this.lstThirdHintAnnounceHour.SelectedValue = this.GetHour(sThirdHintAnnounceTime);
				this.lstThirdHintAnnounceMinute.SelectedValue = this.GetMinute(sThirdHintAnnounceTime);
				this.txtBountyPoint.Text = oDbSession.GetStringValue("pWANTED_BOUNTY_POINT");
				this.txtMonthlyBountyPoint.Text = oDbSession.GetStringValue("pWANTED_MONTHLY_BOUNTY_POINT");
				this.txtBuyPointMailCommDays.Text = oDbSession.GetIntValue("pBUY_POINT_MAIL_COMM_DAYS").ToString();
				this.txtLoveListProfileDays.Text = oDbSession.GetStringValue("pLOVE_LIST_PROFILE_DAYS");
				this.txtCastFavoritePoint.Text = oDbSession.GetStringValue("pCAST_FAVORITE_POINT");
				this.txtCastRefusePoint.Text = oDbSession.GetStringValue("pCAST_REFUSE_POINT");
				this.txtManAddBingoJackpotMin.Text = oDbSession.GetStringValue("pMAN_ADD_BINGO_JACKPOT_MIN");
				this.txtManAddBingoJackpotMax.Text = oDbSession.GetStringValue("pMAN_ADD_BINGO_JACKPOT_MAX");
				this.txtManBingoJackpotDenominator.Text = oDbSession.GetStringValue("pMAN_JACKPOT_DENOMINATOR");
				this.txtCastAddBingoJackpotMin.Text = oDbSession.GetStringValue("pCAST_ADD_BINGO_JACKPOT_MIN");
				this.txtCastAddBingoJackpotMax.Text = oDbSession.GetStringValue("pCAST_ADD_BINGO_JACKPOT_MAX");
				this.txtCastBingoJackpotDenominator.Text = oDbSession.GetStringValue("pCAST_JACKPOT_DENOMINATOR");
				this.txtBbsBingoJackpotMin.Text = oDbSession.GetStringValue("pBBS_BINGO_JACKPOT_MIN");
				this.txtBbsBingoJackpotMax.Text = oDbSession.GetStringValue("pBBS_BINGO_JACKPOT_MAX");
				this.txtBbsBingoJackpotDenominator.Text = oDbSession.GetStringValue("pBBS_BINGO_JACKPOT_DENOMINATOR");
				this.txtDocomoMoneyTransferTEL.Text = oDbSession.GetStringValue("pDOCOMO_MONEY_TRANSFER_TEL");
				this.txtDocomoMoneyTransferNM.Text = oDbSession.GetStringValue("pDOCOMO_MONEY_TRANSFER_NM");
				this.txtRegistCastAgeMin.Text = oDbSession.GetStringValue("pREGIST_CAST_AGE_MIN");
				this.txtTxMailWaitStartPrevMin.Text = oDbSession.GetStringValue("pTX_MAIL_WAIT_START_PREV_MIN");
				this.txtTxMailLoginStartPrevMin.Text = oDbSession.GetStringValue("pTX_MAIL_LOGIN_START_PREV_MIN");
				this.txtMailRequestOpenStartDate.Text = oDbSession.GetStringValue("pMAIL_REQUEST_OPEN_START_DATE");
				this.txtMailRequestOpenEndDate.Text = oDbSession.GetStringValue("pMAIL_REQUEST_OPEN_END_DATE");
				this.txtLatestGcappVeriOS.Text = oDbSession.GetStringValue("pLATEST_GCAPP_VER_IOS");
				this.txtLatestGcappVerAndroid.Text = oDbSession.GetStringValue("pLATEST_GCAPP_VER_ANDROID");
				this.txtFirstSettleEnableDays.Text = oDbSession.GetStringValue("pFIRST_SETTLE_ENABLE_DAYS");
				this.txtReminderSmsDoc.Text = oDbSession.GetStringValue("pREMINDER_SMS_DOC");

				lstTpManMailMovie.SelectedIndex = lstTpManMailMovie.Items.IndexOf(lstTpManMailMovie.Items.FindByValue(oDbSession.GetStringValue("pTP_MAN_MAIL_MOVIE")));
				lstTpManMailPic.SelectedIndex = lstTpManMailPic.Items.IndexOf(lstTpManMailPic.Items.FindByValue(oDbSession.GetStringValue("pTP_MAN_MAIL_PIC")));
				lstTpManPfPic.SelectedIndex = lstTpManPfPic.Items.IndexOf(lstTpManPfPic.Items.FindByValue(oDbSession.GetStringValue("pTP_MAN_PF_PIC")));
				lstTpManTelFailVoiceTv.SelectedIndex = lstTpManTelFailVoiceTv.Items.IndexOf(lstTpManTelFailVoiceTv.Items.FindByValue(oDbSession.GetStringValue("pTP_MAN_TEL_FAIL_VOICE_TV")));
				lstTpManTelFailTvVoice.SelectedIndex = lstTpManTelFailTvVoice.Items.IndexOf(lstTpManTelFailTvVoice.Items.FindByValue(oDbSession.GetStringValue("pTP_MAN_TEL_FAIL_TV_VOICE")));
				lstTpManWaitingCastWithTalk.SelectedIndex = lstTpManWaitingCastWithTalk.Items.IndexOf(lstTpManWaitingCastWithTalk.Items.FindByValue(oDbSession.GetStringValue("pTP_MAN_WAITING_CAST_WITH_TALK")));
				lstTpManTalkBusy.SelectedIndex = lstTpManTalkBusy.Items.IndexOf(lstTpManTalkBusy.Items.FindByValue(oDbSession.GetStringValue("pTP_MAN_TALK_BUSY")));
				lstTpManOutsideTempRegist.SelectedIndex = lstTpManOutsideTempRegist.Items.IndexOf(lstTpManOutsideTempRegist.Items.FindByValue(oDbSession.GetStringValue("pTP_MAN_OUTSIDE_TEMP_REGIST")));
				lstTpCastMailMovie.SelectedIndex = lstTpCastMailMovie.Items.IndexOf(lstTpCastMailMovie.Items.FindByValue(oDbSession.GetStringValue("pTP_CAST_MAIL_MOVIE")));
				lstTpCastMailPic.SelectedIndex = lstTpCastMailPic.Items.IndexOf(lstTpCastMailPic.Items.FindByValue(oDbSession.GetStringValue("pTP_CAST_MAIL_PIC")));
				lstTpCastBbsMovie.SelectedIndex = lstTpCastBbsMovie.Items.IndexOf(lstTpCastBbsMovie.Items.FindByValue(oDbSession.GetStringValue("pTP_CAST_BBS_MOVIE")));
				lstTpCastBbsPic.SelectedIndex = lstTpCastBbsPic.Items.IndexOf(lstTpCastBbsPic.Items.FindByValue(oDbSession.GetStringValue("pTP_CAST_BBS_PIC")));
				lstTpCastPfMovie.SelectedIndex = lstTpCastPfMovie.Items.IndexOf(lstTpCastPfMovie.Items.FindByValue(oDbSession.GetStringValue("pTP_CAST_PF_MOVIE")));
				lstTpCastPfPic.SelectedIndex = lstTpCastPfPic.Items.IndexOf(lstTpCastPfPic.Items.FindByValue(oDbSession.GetStringValue("pTP_CAST_PF_PIC")));
				lstTpCastTelFailVoiceTv.SelectedIndex = lstTpCastTelFailVoiceTv.Items.IndexOf(lstTpCastTelFailVoiceTv.Items.FindByValue(oDbSession.GetStringValue("pTP_CAST_TEL_FAIL_VOICE_TV")));
				lstTpCastTelFailTvVoice.SelectedIndex = lstTpCastTelFailTvVoice.Items.IndexOf(lstTpCastTelFailTvVoice.Items.FindByValue(oDbSession.GetStringValue("pTP_CAST_TEL_FAIL_TV_VOICE")));
				lstTpCastTalkBusy.SelectedIndex = lstTpCastTalkBusy.Items.IndexOf(lstTpCastTalkBusy.Items.FindByValue(oDbSession.GetStringValue("pTP_CAST_TALK_BUSY")));
				lstTpCastSocialGameMovie.SelectedIndex = lstTpCastSocialGameMovie.Items.IndexOf(lstTpCastSocialGameMovie.Items.FindByValue(oDbSession.GetStringValue("pTP_CAST_SOCIAL_GAME_MOVIE")));
				lstTpCastSocialGamePic.SelectedIndex = lstTpCastSocialGamePic.Items.IndexOf(lstTpCastSocialGamePic.Items.FindByValue(oDbSession.GetStringValue("pTP_CAST_SOCIAL_GAME_PIC")));
			} else {
				this.ClearFields();
			}
		}

		using (Site oSite = new Site()) {
			if (oSite.GetOne(this.SiteCd)) {
				this.lblSiteNm.Text = oSite.siteNm;
				this.lblSiteNm1.Text = oSite.siteNm;
			}
		}
	}

	private string GetHour(object pTime) {
		DateTime oDateTime;
		if (!DateTime.TryParse(string.Format("1900/01/01 {0}",pTime),out oDateTime)) {
			return "00";
		}

		return oDateTime.ToString("HH");
	}

	private string GetMinute(object pTime) {
		DateTime oDateTime;
		if (!DateTime.TryParse(string.Format("1900/01/01 {0}",pTime),out oDateTime)) {
			return "00";
		}

		return oDateTime.ToString("mm");
	}

	private void GetList() {
		this.grdSiteManagementEx.DataBind();
		this.pnlCount.DataBind();
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private string GetTimeValue(DropDownList pHourList,DropDownList pMinuteList) {
		return string.Format("{0}:{1}:00",pHourList.SelectedValue,pMinuteList.SelectedValue);
	}

	private void UpdateData(int pDelFlag) {
		DateTime? dtMailRequestOpenStartDate = SysPrograms.TryParseOrDafult(this.txtMailRequestOpenStartDate.Text,(DateTime?)null);
		DateTime? dtMailRequestOpenEndDate = SysPrograms.TryParseOrDafult(this.txtMailRequestOpenEndDate.Text,(DateTime?)null);
		
		if (this.pnlDtl.Visible) {
			using (DbSession oDbSession = new DbSession()) {
				oDbSession.PrepareProcedure("SITE_MANAGEMENT_EX_MAINTE");
				oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
				oDbSession.ProcedureInParm("pWANTED_BOUNTY_POINT",DbSession.DbType.NUMBER,this.txtBountyPoint.Text.Trim());
				oDbSession.ProcedureInParm("pWANTED_MONTHLY_BOUNTY_POINT",DbSession.DbType.NUMBER,this.txtMonthlyBountyPoint.Text.Trim());
				oDbSession.ProcedureInParm("pBUY_POINT_MAIL_COMM_DAYS",DbSession.DbType.NUMBER,int.Parse(this.txtBuyPointMailCommDays.Text.Trim()));
				oDbSession.ProcedureInParm("pLOVE_LIST_PROFILE_DAYS",DbSession.DbType.NUMBER,txtLoveListProfileDays.Text);
				oDbSession.ProcedureInParm("pDOCOMO_MONEY_TRANSFER_TEL",DbSession.DbType.VARCHAR2,string.Empty);
				oDbSession.ProcedureInParm("pDOCOMO_MONEY_TRANSFER_NM",DbSession.DbType.VARCHAR2,string.Empty);
				oDbSession.ProcedureInParm("pCAST_FAVORITE_POINT",DbSession.DbType.NUMBER,txtCastFavoritePoint.Text);
				oDbSession.ProcedureInParm("pCAST_REFUSE_POINT",DbSession.DbType.NUMBER,txtCastRefusePoint.Text);
				oDbSession.ProcedureInParm("pTP_MAN_MAIL_MOVIE",DbSession.DbType.VARCHAR2,lstTpManMailMovie.SelectedValue);
				oDbSession.ProcedureInParm("pTP_MAN_MAIL_PIC",DbSession.DbType.VARCHAR2,lstTpManMailPic.SelectedValue);
				oDbSession.ProcedureInParm("pTP_MAN_PF_PIC",DbSession.DbType.VARCHAR2,lstTpManPfPic.SelectedValue);
				oDbSession.ProcedureInParm("pTP_MAN_TEL_FAIL_VOICE_TV",DbSession.DbType.VARCHAR2,lstTpManTelFailVoiceTv.SelectedValue);
				oDbSession.ProcedureInParm("pTP_MAN_TEL_FAIL_TV_VOICE",DbSession.DbType.VARCHAR2,lstTpManTelFailTvVoice.SelectedValue);
				oDbSession.ProcedureInParm("pTP_MAN_WAITING_CAST_WITH_TALK",DbSession.DbType.VARCHAR2,lstTpManWaitingCastWithTalk.SelectedValue);
				oDbSession.ProcedureInParm("pTP_MAN_TALK_BUSY",DbSession.DbType.VARCHAR2,lstTpManTalkBusy.SelectedValue);
				oDbSession.ProcedureInParm("pTP_MAN_OUTSIDE_TEMP_REGIST",DbSession.DbType.VARCHAR2,lstTpManOutsideTempRegist.SelectedValue);
				oDbSession.ProcedureInParm("pTP_CAST_MAIL_MOVIE",DbSession.DbType.VARCHAR2,lstTpCastMailMovie.SelectedValue);
				oDbSession.ProcedureInParm("pTP_CAST_MAIL_PIC",DbSession.DbType.VARCHAR2,lstTpCastMailPic.SelectedValue);
				oDbSession.ProcedureInParm("pTP_CAST_BBS_MOVIE",DbSession.DbType.VARCHAR2,lstTpCastBbsMovie.SelectedValue);
				oDbSession.ProcedureInParm("pTP_CAST_BBS_PIC",DbSession.DbType.VARCHAR2,lstTpCastBbsPic.SelectedValue);
				oDbSession.ProcedureInParm("pTP_CAST_PF_MOVIE",DbSession.DbType.VARCHAR2,lstTpCastPfMovie.SelectedValue);
				oDbSession.ProcedureInParm("pTP_CAST_PF_PIC",DbSession.DbType.VARCHAR2,lstTpCastPfPic.SelectedValue);
				oDbSession.ProcedureInParm("pTP_CAST_TEL_FAIL_VOICE_TV",DbSession.DbType.VARCHAR2,lstTpCastTelFailVoiceTv.SelectedValue);
				oDbSession.ProcedureInParm("pTP_CAST_TEL_FAIL_TV_VOICE",DbSession.DbType.VARCHAR2,lstTpCastTelFailTvVoice.SelectedValue);
				oDbSession.ProcedureInParm("pTP_CAST_TALK_BUSY",DbSession.DbType.VARCHAR2,lstTpCastTalkBusy.SelectedValue);
				oDbSession.ProcedureInParm("pTP_CAST_SOCIAL_GAME_MOVIE",DbSession.DbType.VARCHAR2,lstTpCastSocialGameMovie.SelectedValue);
				oDbSession.ProcedureInParm("pTP_CAST_SOCIAL_GAME_PIC",DbSession.DbType.VARCHAR2,lstTpCastSocialGamePic.SelectedValue);
				oDbSession.ProcedureInParm("pMAN_ADD_BINGO_JACKPOT_MIN",DbSession.DbType.NUMBER,this.txtManAddBingoJackpotMin.Text);
				oDbSession.ProcedureInParm("pMAN_ADD_BINGO_JACKPOT_MAX",DbSession.DbType.NUMBER,this.txtManAddBingoJackpotMax.Text);
				oDbSession.ProcedureInParm("pMAN_JACKPOT_DENOMINATOR",DbSession.DbType.NUMBER,this.txtManBingoJackpotDenominator.Text);
				oDbSession.ProcedureInParm("pCAST_ADD_BINGO_JACKPOT_MIN",DbSession.DbType.NUMBER,this.txtCastAddBingoJackpotMin.Text);
				oDbSession.ProcedureInParm("pCAST_ADD_BINGO_JACKPOT_MAX",DbSession.DbType.NUMBER,this.txtCastAddBingoJackpotMax.Text);
				oDbSession.ProcedureInParm("pCAST_JACKPOT_DENOMINATOR",DbSession.DbType.NUMBER,this.txtCastBingoJackpotDenominator.Text);
				oDbSession.ProcedureInParm("pBBS_BINGO_JACKPOT_MIN",DbSession.DbType.NUMBER,this.txtBbsBingoJackpotMin.Text);
				oDbSession.ProcedureInParm("pBBS_BINGO_JACKPOT_MAX",DbSession.DbType.NUMBER,this.txtBbsBingoJackpotMax.Text);
				oDbSession.ProcedureInParm("pBBS_BINGO_JACKPOT_DENOMINATOR",DbSession.DbType.NUMBER,this.txtBbsBingoJackpotDenominator.Text);
				oDbSession.ProcedureInParm("pREGIST_CAST_AGE_MIN",DbSession.DbType.NUMBER,int.Parse(txtRegistCastAgeMin.Text));
				oDbSession.ProcedureInParm("pTX_MAIL_WAIT_START_PREV_MIN",DbSession.DbType.NUMBER,int.Parse(txtTxMailWaitStartPrevMin.Text));
				oDbSession.ProcedureInParm("pTX_MAIL_LOGIN_START_PREV_MIN",DbSession.DbType.NUMBER,int.Parse(txtTxMailLoginStartPrevMin.Text));
				oDbSession.ProcedureInParm("pMAIL_REQUEST_OPEN_START_DATE",DbSession.DbType.DATE,dtMailRequestOpenStartDate);
				oDbSession.ProcedureInParm("pMAIL_REQUEST_OPEN_END_DATE",DbSession.DbType.DATE,dtMailRequestOpenEndDate);
				oDbSession.ProcedureInParm("pFIRST_HINT_ANNOUNCE_TIME",DbSession.DbType.VARCHAR2,this.GetTimeValue(this.lstFirstHintAnnounceHour,this.lstFirstHintAnnounceMinute));
				oDbSession.ProcedureInParm("pSECOND_HINT_ANNOUNCE_TIME",DbSession.DbType.VARCHAR2,this.GetTimeValue(this.lstSecondHintAnnounceHour,this.lstSecondHintAnnounceMinute));
				oDbSession.ProcedureInParm("pTHIRD_HINT_ANNOUNCE_TIME",DbSession.DbType.VARCHAR2,this.GetTimeValue(this.lstThirdHintAnnounceHour,this.lstThirdHintAnnounceMinute));
				oDbSession.ProcedureInParm("pLATEST_GCAPP_VER_IOS",DbSession.DbType.VARCHAR2,this.txtLatestGcappVeriOS.Text.Trim());
				oDbSession.ProcedureInParm("pLATEST_GCAPP_VER_ANDROID",DbSession.DbType.VARCHAR2,this.txtLatestGcappVerAndroid.Text.Trim());
				oDbSession.ProcedureInParm("pFIRST_SETTLE_ENABLE_DAYS",DbSession.DbType.NUMBER,txtFirstSettleEnableDays.Text);
				oDbSession.ProcedureInParm("pREMINDER_SMS_DOC",DbSession.DbType.VARCHAR2,txtReminderSmsDoc.Text);
				oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
				oDbSession.ProcedureInParm("pROWID",DbSession.DbType.VARCHAR2,this.Rowid);
				oDbSession.ProcedureInParm("pDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
				oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
				oDbSession.ExecuteProcedure();
			}
		} else if (this.pnlDtlTransfer.Visible) {
			using (DbSession oDbSession = new DbSession()) {
				oDbSession.PrepareProcedure("DOCOMO_MONEY_TRANSFER_UPDATE");
				oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
				oDbSession.ProcedureInParm("pDOCOMO_MONEY_TRANSFER_TEL",DbSession.DbType.VARCHAR2,txtDocomoMoneyTransferTEL.Text);
				oDbSession.ProcedureInParm("pDOCOMO_MONEY_TRANSFER_NM",DbSession.DbType.VARCHAR2,txtDocomoMoneyTransferNM.Text);
				oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
				oDbSession.ProcedureInParm("pROWID",DbSession.DbType.VARCHAR2,this.Rowid);
				oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
				oDbSession.ExecuteProcedure();
			}
		}
	}

	private void CreateLists() {
		List<string> oHourList = new List<string>();
		List<string> oMinuteList = new List<string>();

		for (int iIndex = 0;iIndex < 60;iIndex++) {
			string sValue = string.Format("{0:00}",iIndex);
			if (iIndex < 24) {
				oHourList.Add(sValue);
			}
			oMinuteList.Add(sValue);
		}

		this.HourList = oHourList;
		this.MinuteList = oMinuteList;
	}


	private void SetMailTemplate() {
		lstTpManMailMovie.Items.Clear();
		lstTpManMailPic.Items.Clear();
		lstTpManPfPic.Items.Clear();
		lstTpManTelFailVoiceTv.Items.Clear();
		lstTpManTelFailTvVoice.Items.Clear();
		lstTpManWaitingCastWithTalk.Items.Clear();
		lstTpManTalkBusy.Items.Clear();
		lstTpManOutsideTempRegist.Items.Clear();
		lstTpCastMailMovie.Items.Clear();
		lstTpCastMailPic.Items.Clear();
		lstTpCastBbsMovie.Items.Clear();
		lstTpCastBbsPic.Items.Clear();
		lstTpCastPfMovie.Items.Clear();
		lstTpCastPfPic.Items.Clear();
		lstTpCastTelFailVoiceTv.Items.Clear();
		lstTpCastTelFailTvVoice.Items.Clear();
		lstTpCastTalkBusy.Items.Clear();
		lstTpCastSocialGameMovie.Items.Clear();
		lstTpCastSocialGamePic.Items.Clear();

		using (MailTemplate oMail = new MailTemplate()) {
			DataSet ds = oMail.GetNewVerList(this.SiteCd,null);
			lstTpManMailMovie.Items.Add(new ListItem("指定なし",""));
			lstTpManMailPic.Items.Add(new ListItem("指定なし",""));
			lstTpManPfPic.Items.Add(new ListItem("指定なし",""));
			lstTpManTelFailVoiceTv.Items.Add(new ListItem("指定なし",""));
			lstTpManTelFailTvVoice.Items.Add(new ListItem("指定なし",""));
			lstTpManWaitingCastWithTalk.Items.Add(new ListItem("指定なし",""));
			lstTpManTalkBusy.Items.Add(new ListItem("指定なし",""));
			lstTpManOutsideTempRegist.Items.Add(new ListItem("指定なし",""));
			lstTpCastMailMovie.Items.Add(new ListItem("指定なし",""));
			lstTpCastMailPic.Items.Add(new ListItem("指定なし",""));
			lstTpCastBbsMovie.Items.Add(new ListItem("指定なし",""));
			lstTpCastBbsPic.Items.Add(new ListItem("指定なし",""));
			lstTpCastPfMovie.Items.Add(new ListItem("指定なし",""));
			lstTpCastPfPic.Items.Add(new ListItem("指定なし",""));
			lstTpCastTelFailVoiceTv.Items.Add(new ListItem("指定なし",""));
			lstTpCastTelFailTvVoice.Items.Add(new ListItem("指定なし",""));
			lstTpCastTalkBusy.Items.Add(new ListItem("指定なし",""));
			lstTpCastSocialGameMovie.Items.Add(new ListItem("指定なし",""));
			lstTpCastSocialGamePic.Items.Add(new ListItem("指定なし",""));

			string sMailType;
			
			for (int i = 0;i < ds.Tables[0].Rows.Count;i++) {
				sMailType = ds.Tables[0].Rows[i]["MAIL_TEMPLATE_TYPE"].ToString();
				switch (ds.Tables[0].Rows[i]["SEX_CD"].ToString()) {
					case ViCommConst.UsableSexCd.MAN:
						if (sMailType.Equals(ViCommConst.MAIL_TP_TALK_END_REASON)) {
							lstTpManTelFailVoiceTv.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpManTelFailTvVoice.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpManWaitingCastWithTalk.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpManTalkBusy.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
						} else if (sMailType.Equals(ViCommConst.MAIL_TP_COMPLITE_ATTACHED)) {
							lstTpManMailMovie.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpManMailPic.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpManPfPic.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
						} else if (sMailType.Equals(ViCommConst.MAIL_TP_REGIST) || sMailType.Equals(ViCommConst.MAIL_TP_REGIST_AUTO)) {
							lstTpManOutsideTempRegist.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
						}
						break;
					case ViCommConst.UsableSexCd.WOMAN:
						if (sMailType.Equals(ViCommConst.MAIL_TP_TALK_END_REASON)) {
							lstTpCastTelFailVoiceTv.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpCastTelFailTvVoice.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpCastTalkBusy.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
						} else if (sMailType.Equals(ViCommConst.MAIL_TP_COMPLITE_ATTACHED)) {
							lstTpCastMailMovie.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpCastMailPic.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpCastBbsMovie.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpCastBbsPic.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpCastPfMovie.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpCastPfPic.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpCastSocialGameMovie.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpCastSocialGamePic.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
						}
						break;
					default:
						if (sMailType.Equals(ViCommConst.MAIL_TP_TALK_END_REASON)) {
							lstTpManTelFailVoiceTv.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpManTelFailTvVoice.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpManWaitingCastWithTalk.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpManTalkBusy.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpCastTelFailVoiceTv.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpCastTelFailTvVoice.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpCastTalkBusy.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
						} else if (sMailType.Equals(ViCommConst.MAIL_TP_COMPLITE_ATTACHED)) {
							lstTpManMailMovie.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpManMailPic.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpManPfPic.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpCastMailMovie.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpCastMailPic.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpCastBbsMovie.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpCastBbsPic.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpCastPfMovie.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpCastPfPic.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpCastSocialGameMovie.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
							lstTpCastSocialGamePic.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
						} else if (sMailType.Equals(ViCommConst.MAIL_TP_REGIST) || sMailType.Equals(ViCommConst.MAIL_TP_REGIST_AUTO)) {
							lstTpManOutsideTempRegist.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
						}
						break;
				}
			}
		}
	}

	private bool IsCorrectDate() {
		this.lblErrorMessageMailRequestOpenTerm.Visible = false;
		this.lblErrorMessageMailRequestOpenTerm.Text = string.Empty;

		DateTime? dtMailRequestOpenStartDate = SysPrograms.TryParseOrDafult(this.txtMailRequestOpenStartDate.Text,(DateTime?)null);
		DateTime? dtMailRequestOpenEndDate = SysPrograms.TryParseOrDafult(this.txtMailRequestOpenEndDate.Text,(DateTime?)null);

		if (!dtMailRequestOpenStartDate.HasValue) {
			this.lblErrorMessageMailRequestOpenTerm.Visible = true;
			this.lblErrorMessageMailRequestOpenTerm.Text = "メールおねだり機能全開放開始日時を正しく入力してください。";
			return false;
		}
		if (!dtMailRequestOpenEndDate.HasValue) {
			this.lblErrorMessageMailRequestOpenTerm.Visible = true;
			this.lblErrorMessageMailRequestOpenTerm.Text = "メールおねだり機能全開放終了日時を正しく入力してください。";
			return false;
		}
		if (dtMailRequestOpenEndDate < dtMailRequestOpenStartDate) {
			this.lblErrorMessageMailRequestOpenTerm.Visible = true;
			this.lblErrorMessageMailRequestOpenTerm.Text = "メールおねだり機能全開放期間の大小関係が不正です。";
			return false;
		}

		return true;
	}
}