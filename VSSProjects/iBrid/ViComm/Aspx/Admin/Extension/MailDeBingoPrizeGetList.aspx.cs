﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メールdeビンゴ　賞金獲得者
--	Progaram ID		: MailDeBingoPrizeGetList
--
--  Creation Date	: 2011.07.06
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using iBridCommLib;
using ViComm;
using System.Data;

public partial class Extension_MailDeBingoPrizeGetList:Page {
	private Stream filter;

	#region Sorting

	private string SortExpression {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortExpression"]);
		}
		set {
			this.ViewState["SortExpression"] = value;
		}
	}

	private string SortDirect {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortDirect"]);
		}
		set {
			this.ViewState["SortDirect"] = value;
		}
	}
	#endregion

	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		filter = Response.Filter;
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!IsPostBack) {
			FirstLoad();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		this.GetList();
	}

	protected void btnBack_Click(object sender,EventArgs e) {
		this.Response.Redirect(string.Format("~/Extension/MailDeBingoMainte.aspx?sexcd={0}",ViewState["SEX_CD"].ToString()));
	}

	protected void dsBingoEntry_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsBingoEntry_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = ViewState["SEQ"].ToString();
		e.InputParameters[2] = txtLoginId.Text;
		e.InputParameters[3] = ViewState["SEX_CD"].ToString();
		e.InputParameters[4] = this.SortExpression;
		e.InputParameters[5] = this.SortDirect;
	}

	protected void grdBingoEntry_Sorting(object sender,GridViewSortEventArgs e) {
		if (this.SortExpression.Equals(e.SortExpression)) {
			if (this.SortDirect.Equals("ASC")) {
				this.SortDirect = "DESC";
			} else if (this.SortDirect.Equals("DESC")) {
				this.SortDirect = "ASC";
			}
		} else {
			this.SortDirect = "ASC";
		}

		this.SortExpression = e.SortExpression;

		e.Cancel = true;
		this.GetList();
	}

	private void GetList() {
		this.recCount = "0";
		this.grdBingoEntry.PageIndex = 0;
		this.grdBingoEntry.DataSourceID = "dsBingoEntry";
		this.grdBingoEntry.DataBind();
		this.pnlInfo.Visible = true;
		this.pnlCount.DataBind();
		this.SetRemarks(ViewState["SEQ"].ToString());
	}

	protected string GetViewUrl(object pSiteCd,object pLoignId,object pSexCd,object pBingoEntrySeq) {
		if (pSexCd.ToString().Equals(ViCommConst.MAN)) {
			return string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}&bingoentryseq={2}",pSiteCd,pLoignId,pBingoEntrySeq);
		} else {
			return string.Format("~/Cast/CastView.aspx?loginid={0}&bingoentryseq={1}",pLoignId,pBingoEntrySeq);
		}
	}

	protected string GetRecCount() {
		return recCount;
	}

	private void SetRemarks(string pBingoTermSeq) {
		int iEntryCount,iElectionAmt,iElectionPoint;

		using (BingoEntry oBingoEntry = new BingoEntry()) {
			iEntryCount = oBingoEntry.GetEntryCount(lstSiteCd.SelectedValue,ViewState["SEQ"].ToString());
			oBingoEntry.GetSumElection(lstSiteCd.SelectedValue,ViewState["SEQ"].ToString(),out iElectionAmt,out iElectionPoint);
		}
		using (MailDeBingo oMailDeBingo = new MailDeBingo()) {
			oMailDeBingo.GetOne(pBingoTermSeq);
			lblDate.Text = string.Format("{0} ～ {1}",oMailDeBingo.bingoStartDate,oMailDeBingo.bingoEndDate);
			lblJackpot.Text = oMailDeBingo.jackpot.ToString();
		}
		lblEntryCount.Text = iEntryCount.ToString();
		lblPrize.Text = string.Format("{0} pt ({1} 円)",iElectionPoint.ToString(),iElectionAmt.ToString());

	}

	private void FirstLoad() {
		grdBingoEntry.PageSize = 100;
		if (iBridUtil.GetStringValue(Request.QueryString["sexcd"]).Equals(ViCommConst.MAN)) {
			lblPgmTitle.Text = "メールdeビンゴ 男性賞金獲得者";
		} else {
			lblPgmTitle.Text = "メールdeビンゴ 女性賞金獲得者";
		}
		ViewState["SEX_CD"] = iBridUtil.GetStringValue(Request.QueryString["sexcd"]);
		ViewState["SEQ"] = iBridUtil.GetStringValue(Request.QueryString["seq"]);
		lstSiteCd.DataBind();
		this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		this.SetRemarks(ViewState["SEQ"].ToString());
		this.GetList();
	}

	/// <summary>
	/// CSV出力ボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnCSV_Click(object sender,EventArgs e) {
		BingoEntry oBingoEntry = new BingoEntry();
		DateTime dt = DateTime.Today;

		// 出力データ検索
		DataSet ds = oBingoEntry.GetCSVList(
			lstSiteCd.SelectedValue
			,ViewState["SEQ"].ToString()
			,txtLoginId.Text
			,ViewState["SEX_CD"].ToString()
			,this.SortExpression
			,this.SortDirect
		);

		Response.Filter = filter;
		Response.ContentType = "application/download";
		Response.AddHeader("Content-Disposition","attachment;filename=MailDeBingoPrizeGetList.csv");
		Response.ContentEncoding = Encoding.GetEncoding("Shift_JIS");

		// タイトル行
		Response.Write(string.Format(
			"\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\"\r\n"
			,"ログインID"
			,"ハンドル名"
			,"ビンゴカード"
			,"挑戦回数"
			,"付与pt"
			,"pt付与"
			,"完成申請日"
			,"一発ビンゴ"
		));

		foreach (DataRow dr in ds.Tables[0].Rows) {
			if (dr["BINGO_APPLICATION_DATE"] != DBNull.Value) {
				dt = DateTime.Parse(dr["BINGO_APPLICATION_DATE"].ToString());
			}

			// データ行
			Response.Write(string.Format(
				"\"{0}\",\"{1}\",\"{2}枚目\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\"\r\n"
				,iBridUtil.GetStringValue(dr["LOGIN_ID"])
				,iBridUtil.GetStringValue(dr["HANDLE_NM"])
				,iBridUtil.GetStringValue(dr["BINGO_CARD_NO"])
				,iBridUtil.GetStringValue(dr["BINGO_CHARANGE_COUNT"])
				,iBridUtil.GetStringValue(dr["BINGO_ELECTION_POINT"])
				,iBridUtil.GetStringValue(dr["BINGO_POINT_PAYMENT_FLAG"]).Equals("1") ? "済" : "未"
				,(dr["BINGO_APPLICATION_DATE"] != DBNull.Value) ? dt.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty
				,iBridUtil.GetStringValue(dr["BINGO_BALL_FLAG"]).Equals("1") ? "一発ﾋﾞﾝｺﾞ" : ""
			));
		}

		Response.End();
	}

}
