﻿
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Extension_StageClearPointList:System.Web.UI.Page {
	protected string recCount;

	protected string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		set {
			this.ViewState["RevisionNo"] = value;
		}
	}
	protected string RowID {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RowID"]);
		}
		set {
			this.ViewState["RowID"] = value;
		}
	}

	protected void Page_Load(object sender, EventArgs e) {
		if(!IsPostBack) {
			InitPage();
		}
	}

	protected void FirstLoad() {
		InitPage();
	}

	protected void InitPage() {
		ClearFirld();
		DataBind();
	}

	protected void ClearFirld() {
		pnlMainte.Visible = false;
		txtManClearFromRegistDays.Text = string.Empty;
		txtCastClearFromRegistDays.Text = string.Empty;
		txtManStageClearPoint.Text = string.Empty;
		txtCastStageClearPoint.Text = string.Empty;
	}

	private void GetData() {
		this.ClearFirld();

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("STAGE_CLEAR_POINT_GET");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.hdnSiteCd.Value);
			oDbSession.ProcedureOutParm("pMAN_CLEAR_FROM_REGIST_DAYS", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pCAST_CLEAR_FROM_REGIST_DAYS", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pMAN_CLR_PT_TARGET_STAGE_SEQ", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pCAST_CLR_PT_TARGET_STAGE_SEQ", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pMAN_STAGE_CLEAR_POINT", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pCAST_STAGE_CLEAR_POINT", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pREVISION_NO", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pROWID", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pROW_COUNT", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();


			this.txtManClearFromRegistDays.Text = oDbSession.GetStringValue("pMAN_CLEAR_FROM_REGIST_DAYS");
			this.txtCastClearFromRegistDays.Text = oDbSession.GetStringValue("pCAST_CLEAR_FROM_REGIST_DAYS");
			this.lstManClearPointTargetStageSeq.SelectedValue = oDbSession.GetStringValue("pMAN_CLR_PT_TARGET_STAGE_SEQ");
			this.lstCastClearPointTargetStageSeq.SelectedValue = oDbSession.GetStringValue("pCAST_CLR_PT_TARGET_STAGE_SEQ");
			this.txtManStageClearPoint.Text = oDbSession.GetStringValue("pMAN_STAGE_CLEAR_POINT");
			this.txtCastStageClearPoint.Text = oDbSession.GetStringValue("pCAST_STAGE_CLEAR_POINT");

			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
			this.RowID = oDbSession.GetStringValue("pROWID");

		}
	}

	private void UpdateData(bool pDelFlag) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("STAGE_CLEAR_POINT_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.hdnSiteCd.Value);
			oDbSession.ProcedureInParm("pMAN_CLEAR_FROM_REGIST_DAYS", DbSession.DbType.NUMBER, int.Parse(this.txtManClearFromRegistDays.Text));
			oDbSession.ProcedureInParm("pCAST_CLEAR_FROM_REGIST_DAYS", DbSession.DbType.NUMBER, int.Parse(this.txtCastClearFromRegistDays.Text));
			oDbSession.ProcedureInParm("pMAN_CLR_PT_TARGET_STAGE_SEQ", DbSession.DbType.VARCHAR2, lstManClearPointTargetStageSeq.SelectedValue);
			oDbSession.ProcedureInParm("pCAST_CLR_PT_TARGET_STAGE_SEQ", DbSession.DbType.VARCHAR2, lstCastClearPointTargetStageSeq.SelectedValue);
			oDbSession.ProcedureInParm("pMAN_STAGE_CLEAR_POINT", DbSession.DbType.NUMBER, int.Parse(txtManStageClearPoint.Text));
			oDbSession.ProcedureInParm("pCAST_STAGE_CLEAR_POINT", DbSession.DbType.NUMBER, int.Parse(txtCastStageClearPoint.Text));
			oDbSession.ProcedureInParm("pREVISION_NO", DbSession.DbType.NUMBER, this.RevisionNo);
			oDbSession.ProcedureInParm("pROWID", DbSession.DbType.VARCHAR2, this.RowID);
			oDbSession.ProcedureInParm("pDEL_FLAG", DbSession.DbType.NUMBER, pDelFlag ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}
	}

	protected string GetRecCount() {
		return this.recCount;
	}


	# region === Click Event ===
	protected void btnUpdate_Click(object sender, EventArgs e) {
		this.UpdateData(false);
	}
	protected void btnCancel_Click(object sender, EventArgs e) {
		this.ClearFirld();
	}
	protected void lnkSiteCd_Command(object sender, CommandEventArgs e) {
		this.hdnSiteCd.Value = e.CommandArgument.ToString();
		DataBind();
		GetData();
		pnlMainte.Visible = true;
	}
	protected void lst_DataBound(object sender, EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0, new ListItem(string.Empty, string.Empty));
	}
	protected void dsStageClearPoint_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}
	# endregion

}
