﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ステージ一覧

--	Progaram ID		: GameItemList
--
--  Creation Date	: 2011.07.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.IO;
using System.Drawing;
using ViComm.Extension.Pwild;

public partial class Extension_GameItemList : System.Web.UI.Page {
	protected string[] DummyArray = new string[3];
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		set {
			this.ViewState["SexCd"] = value;
		}
	}

	private string GameItemSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemSeq"]);
		}
		set {
			this.ViewState["GameItemSeq"] = value;
		}
	}

	private string GameItemCategoryTypePresent {
		get {
			return iBridUtil.GetStringValue(this.ViewState["GameItemCategoryTypePresent"]);
		}
		set {
			this.ViewState["GameItemCategoryTypePresent"] = value;
		}
    }

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		
		if (!this.IsPostBack) {
			this.InitPage();
			this.SexCd = iBridUtil.GetStringValue(this.Request.QueryString["sexcd"]);
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
		this.GameItemSeq = string.Empty;
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.UpdateData(false);
		this.pnlGameItemInfo.Visible = false;
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		this.UpdateData(true);
		this.pnlGameItemInfo.Visible = false;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.GameItemSeq = string.Empty;
		this.ClearFileds();
		this.pnlKey.Enabled = true;
		this.pnlGameItemInfo.Visible = false;
	}

	protected void btnCreate_Click(object sender,EventArgs e) {
		this.ClearFileds();
		this.pnlKey.Enabled = false;
		this.pnlGameItemInfo.Visible = true;
	}

	protected void lnkEdit_Command(object sender,CommandEventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.GameItemSeq = iBridUtil.GetStringValue(e.CommandArgument);
		this.GetData();
		this.pnlKey.Enabled = false;
		this.pnlGameItemInfo.Visible = true;
	}

	protected void lnkSetPublish_Command(object sender,CommandEventArgs e) {
		int iRowIndex = int.Parse(e.CommandArgument.ToString());

		GridViewRow oRow = this.grdGameItem.Rows[iRowIndex];
		HiddenField oPublishFlagHiddenFiled = oRow.FindControl("hdnPublishFlag") as HiddenField;
		LinkButton oSetPublishLinkButton = oRow.FindControl("lnkSetPublish") as LinkButton;
		Label oGameItemSeqLabel = oRow.FindControl("lblGameItemSeq") as Label;

		string sPublishFlag = ViCommConst.FLAG_ON_STR.Equals(oPublishFlagHiddenFiled.Value) ? ViCommConst.FLAG_OFF_STR : ViCommConst.FLAG_ON_STR;

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("UPDATE_GAME_ITEM_PUBLISH_FLAG");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pGAME_ITEM_SEQ",DbSession.DbType.NUMBER,oGameItemSeqLabel.Text);
			oDbSession.ProcedureInParm("pPUBLISH_FLAG",DbSession.DbType.NUMBER,sPublishFlag);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.GetList();
	}
	
	protected void dsGameItemPresent_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		GameItem oGameItem = new GameItem();
		DataSet oDS = oGameItem.GetOneByGameItemSeq(this.SiteCd,GameItemSeq);
		string sExceptPresentGameItemSeq = string.Empty;
		if(oDS.Tables[0].Rows.Count > 0) {
			sExceptPresentGameItemSeq = oDS.Tables[0].Rows[0]["PRESENT_GAME_ITEM_SEQ"].ToString();
		}
		e.InputParameters["pGameItemCategoryType"] = this.GameItemCategoryTypePresent;
		e.InputParameters["pSexCd"] = ViCommConst.MAN.Equals(this.SexCd) ? ViCommConst.OPERATOR : ViCommConst.MAN;
        e.InputParameters["pPresentFlag"] = ViCommConst.FLAG_OFF_STR;
		e.InputParameters["pExceptPresentGameItemSeq"] = sExceptPresentGameItemSeq;
		e.InputParameters["pSelectGameItemFlag"] = string.IsNullOrEmpty(GameItemSeq) ? ViCommConst.FLAG_OFF_STR : ViCommConst.FLAG_ON_STR;
	}

	protected void dsGameItem_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsGameItem_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {

		e.InputParameters["pGameItemName"] = this.txtGameItemName.Text.TrimEnd();
		e.InputParameters["pGameItemSeq"] = this.txtGameItemSeq.Text.TrimEnd();

		if (this.chkSearchPrizeOfWar.Checked) {
			e.InputParameters["pGameItemGetCd"] = "3";
		}else {
			e.InputParameters["pGameItemGetCd"] = this.lstSearchItemGetCd.SelectedValue;
		}		
		e.InputParameters["pGameItemCategoryType"] = this.lstSearchGameItemCategory.SelectedValue;
		if (this.chkSearchPublishFlag.Checked) {
			e.InputParameters["pPublishFlag"] = ViCommConst.FLAG_ON_STR;
		} else {
			e.InputParameters["pPublishFlag"] = ViCommConst.FLAG_OFF_STR;
		}
		if (this.chkSearchPresentFlag.Checked) {
			e.InputParameters["pPresentFlag"] = ViCommConst.FLAG_ON_STR;
		} else {
			e.InputParameters["pPresentFlag"] = ViCommConst.FLAG_OFF_STR;
		}
		
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	protected void lstGameItemCategory_IndexChanged(object sender,EventArgs e) {
		this.GameItemCategoryTypePresent = this.lstGameItemCategoryPresent.SelectedValue;
		this.lstGameItemPresent.DataBind();
	}
	
	protected void chkPresentFlag_CheckedChanged(object sender,EventArgs e) {
		this.SetPresentItemCondition();
	}

	protected void vdcGameItemPresent_ServerValidate(object source,ServerValidateEventArgs args) {
		if (!this.chkPresentFlag.Checked) {
			this.vdcGameItemPresent.ErrorMessage = string.Empty;
			return;
		}

        if (string.IsNullOrEmpty(this.lstGameItemPresent.SelectedValue)) {
            args.IsValid = false;
            this.vdcGameItemPresent.ErrorMessage = "プレゼントアイテムを選択してください。";
        }
	}
    protected void vdcPresentFlag_ServerValidate(object source, ServerValidateEventArgs args) {
        if (!this.chkPresentFlag.Checked) {
            return;
        } else {
            using (GameItem oGameItem = new GameItem()) {
				string sSexCd = this.SexCd.Equals(ViCommConst.MAN) ? ViCommConst.MAN : ViCommConst.OPERATOR ;
				if (oGameItem.CheckGameItemPresented(this.SiteCd,sSexCd,this.lstGameItemPresent.SelectedValue,this.GameItemSeq)) {
                    args.IsValid = false;
                    this.vdcPresentFlag.ErrorMessage = "既にプレゼントアイテムとして選択されているため、プレゼント設定はできません。";
                }
            }
        
        }
    }

	private void InitPage() {
		this.pnlInfo.Visible = true;
		this.pnlGameItemInfo.Visible = false;
		
		this.GameItemSeq = string.Empty;
		this.pnlKey.Enabled = true;
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		this.lstStageSeq.DataBind();
		this.lstItemGetCd.DataBind();
		this.lstGameItemCategory.DataBind();
		this.lstGameItemCategoryPresent.DataBind();
		this.lstGameItemPresent.DataBind();

		this.txtGameItemName.Text = string.Empty;
		this.txtGameItemSeq.Text = string.Empty;
		this.lstSearchItemGetCd.SelectedIndex = -1;
		this.chkSearchPrizeOfWar.Checked = false;
		this.lstSearchGameItemCategory.SelectedIndex = -1;
		this.chkSearchPublishFlag.Checked = true;
		this.chkSearchPresentFlag.Checked = false;
	}

	private void ClearFileds() {
		this.chkPresentFlag.Checked = false;
		this.chkPublishFlag.Checked = false;
		this.lblGameItemId.Text = string.Empty;
		this.lstGameItemCategory.SelectedIndex = 0;
		this.lstGameItemCategoryPresent.SelectedIndex = 0;
		this.lstGameItemPresent.SelectedIndex = 0;
		this.lstItemGetCd.SelectedIndex = 0;
		this.lstStageSeq.SelectedIndex = 0;
		this.txtGameItemNm.Text = string.Empty;
		this.txtAttackPower.Text = string.Empty;
		this.txtDefencePower.Text = string.Empty;
		this.txtEndurance.Text = string.Empty;
		this.txtPrice.Text = string.Empty;
		this.txtDescription.Text = string.Empty;
		this.txtRemarks.Text = string.Empty;
		this.vdcGameItemPresent.ErrorMessage = string.Empty;
		this.SetPresentItemCondition();
		this.pnlKey.Enabled = true;
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.SiteCd = this.lstSiteCd.SelectedValue;
		this.GameItemSeq = string.Empty;

		this.grdGameItem.PageIndex = 0;
		this.grdGameItem.DataSourceID = "dsGameItem";
		this.grdGameItem.DataBind();
		this.pnlCount.DataBind();
	}

	private void GetData() {
		this.ClearFileds();

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("GAME_ITEM_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pGAME_ITEM_SEQ",DbSession.DbType.VARCHAR2,this.GameItemSeq);
			oDbSession.ProcedureOutParm("pSEX_CD",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pGAME_ITEM_NM",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pATTACK_POWER",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pDEFENCE_POWER",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pENDURANCE",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pDESCRIPTION",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pGAME_ITEM_GET_CD",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPRICE",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pPUBLISH_FLAG",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTAGE_SEQ",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pGAME_ITEM_CATEGORY_TYPE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPRESENT_FLAG",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPRESENT_GAME_ITEM_SEQ",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPRESENT_ITEM_CATEGORY_TYPE",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREMARKS",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.lblGameItemId.Text = oDbSession.GetStringValue("pGAME_ITEM_SEQ");
			this.txtGameItemNm.Text = oDbSession.GetStringValue("pGAME_ITEM_NM");
			this.txtAttackPower.Text = oDbSession.GetStringValue("pATTACK_POWER");
			this.txtDefencePower.Text = oDbSession.GetStringValue("pDEFENCE_POWER");
			this.txtEndurance.Text = oDbSession.GetStringValue("pENDURANCE");
			this.txtPrice.Text = oDbSession.GetStringValue("pPRICE");
			this.txtDescription.Text = oDbSession.GetStringValue("pDESCRIPTION");
			this.txtRemarks.Text = oDbSession.GetStringValue("pREMARKS");
			this.lstStageSeq.SelectedValue = oDbSession.GetStringValue("pSTAGE_SEQ");
			this.lstGameItemCategory.SelectedValue = oDbSession.GetStringValue("pGAME_ITEM_CATEGORY_TYPE");
			this.lstItemGetCd.SelectedValue = oDbSession.GetStringValue("pGAME_ITEM_GET_CD");
			this.chkPresentFlag.Checked = ViCommConst.FLAG_ON_STR.Equals(oDbSession.GetStringValue("pPRESENT_FLAG")) ? true : false;
			this.chkPublishFlag.Checked = ViCommConst.FLAG_ON_STR.Equals(oDbSession.GetStringValue("pPUBLISH_FLAG")) ? true : false;
			this.lstGameItemCategoryPresent.SelectedValue = oDbSession.GetStringValue("pPRESENT_ITEM_CATEGORY_TYPE");
			this.GameItemCategoryTypePresent = this.lstGameItemCategoryPresent.SelectedValue;
			this.lstGameItemPresent.DataBind();
			this.lstGameItemPresent.SelectedValue = oDbSession.GetStringValue("pPRESENT_GAME_ITEM_SEQ");
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");

			this.SetPresentItemCondition();
		}
	}

	protected string GetImagePath(object pGameItemSeq) {
		return string.Format("../{0}/{1}/{2}/{3}/{4}/{5}{6}",
			"Image",
			this.SiteCd,
			"game",
			ViCommConst.MAN.Equals(this.SexCd) ? ViCommConst.MAN_DIRECTORY.Replace(@"\",string.Empty) : ViCommConst.OPERATOR_DIRECTORY.Replace(@"\",string.Empty),
			ViCommConst.ITEM_IMAGE_DIRECTORY.Replace(@"\",string.Empty),
			iBridUtil.GetStringValue(pGameItemSeq).PadLeft(15,'0'),
			PwViCommConst.GAME_PIC_FOODER);
	}

	protected string GetPublishFlagMark(object pPublishFlag) {
		string sPublishFlag = iBridUtil.GetStringValue(pPublishFlag);

		return ViCommConst.FLAG_ON_STR.Equals(sPublishFlag) ? "公開" : "非公開";
	}

	private void UpdateData(bool pDelFlag) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("GAME_ITEM_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureBothParm("pGAME_ITEM_SEQ",DbSession.DbType.VARCHAR2,this.GameItemSeq);
			oDbSession.ProcedureInParm("pGAME_ITEM_NM",DbSession.DbType.VARCHAR2,this.txtGameItemNm.Text.TrimEnd());
			oDbSession.ProcedureInParm("pSEX_CD",DbSession.DbType.NUMBER,this.SexCd);
			oDbSession.ProcedureInParm("pATTACK_POWER",DbSession.DbType.NUMBER,string.IsNullOrEmpty(this.txtAttackPower.Text) ? "0" : this.txtAttackPower.Text);
			oDbSession.ProcedureInParm("pDEFENCE_POWER",DbSession.DbType.NUMBER,string.IsNullOrEmpty(this.txtDefencePower.Text) ? "0" : this.txtDefencePower.Text);
			oDbSession.ProcedureInParm("pENDURANCE",DbSession.DbType.NUMBER,string.IsNullOrEmpty(this.txtEndurance.Text) ? "0" : this.txtEndurance.Text);
			oDbSession.ProcedureInParm("pDESCRIPTION",DbSession.DbType.VARCHAR2,this.txtDescription.Text.TrimEnd());
			oDbSession.ProcedureInParm("pGAME_ITEM_GET_CD",DbSession.DbType.VARCHAR2,this.lstItemGetCd.SelectedValue);
			oDbSession.ProcedureInParm("pPRICE",DbSession.DbType.NUMBER,string.IsNullOrEmpty(this.txtPrice.Text) ? "0" : this.txtPrice.Text);
			oDbSession.ProcedureInParm("pPUBLISH_FLAG",DbSession.DbType.VARCHAR2,this.chkPublishFlag.Checked ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR);
			oDbSession.ProcedureInParm("pSTAGE_SEQ",DbSession.DbType.VARCHAR2,this.lstStageSeq.SelectedValue);
			oDbSession.ProcedureInParm("pGAME_ITEM_CATEGORY_TYPE",DbSession.DbType.VARCHAR2,this.lstGameItemCategory.SelectedValue);
			oDbSession.ProcedureInParm("pPRESENT_FLAG",DbSession.DbType.VARCHAR2,this.chkPresentFlag.Checked ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR);
			oDbSession.ProcedureInParm("pPRESENT_GAME_ITEM_SEQ",DbSession.DbType.VARCHAR2,this.chkPresentFlag.Checked ? this.lstGameItemPresent.SelectedValue : string.Empty);
			oDbSession.ProcedureInParm("pREMARKS",DbSession.DbType.VARCHAR2,this.txtRemarks.Text);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.VARCHAR2,this.RevisionNo);
			oDbSession.ProcedureInParm("pDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.ClearFileds();
		this.GetList();
	}

	private void SetPresentItemCondition() {
		if (this.chkPresentFlag.Checked) {
			this.lstGameItemCategoryPresent.Enabled = true;
			this.lstGameItemPresent.Enabled = true;
			this.lstGameItemCategoryPresent.BackColor = Color.Empty;
			this.lstGameItemPresent.BackColor = Color.Empty;
		}
		else {
			this.lstGameItemCategoryPresent.Enabled = false;
			this.lstGameItemPresent.Enabled = false;
			this.lstGameItemCategoryPresent.SelectedIndex = 0;
			this.lstGameItemPresent.SelectedIndex = 0;
			this.lstGameItemCategoryPresent.BackColor = SystemColors.Control;
			this.lstGameItemPresent.BackColor = SystemColors.Control;
		}
	}
}
