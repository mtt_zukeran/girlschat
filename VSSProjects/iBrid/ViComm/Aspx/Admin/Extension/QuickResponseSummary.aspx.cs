﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 10分以内の返信・サマリー
--	Progaram ID		: QuickResponseSummary
--
--  Creation Date	: 2011.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using iBridCommLib;
using ViComm;

public partial class Extension_QuickResponseSummary:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			FirstLoad();
		}
	}

	private void FirstLoad() {
		lstSiteCd.DataBind();

		SysPrograms.SetupDay(lstYYYY,lstMM,lstDD,false);
		ClearField();
	}

	private void ClearField() {
		lstSiteCd.SelectedIndex = 0;
		DateTime dtTo = DateTime.Now;
		lstYYYY.SelectedValue = dtTo.ToString("yyyy");
		lstMM.SelectedValue = dtTo.ToString("MM");
		lstDD.SelectedValue = dtTo.ToString("dd");
		GetList();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		ClearField();
	}

	private void GetList() {
		pnlInfo.Visible = true;
		grdQuickResSummary.PageIndex = 0;
		grdQuickResSummary.DataSourceID = "dsQuickResReport";
		grdQuickResSummary.DataBind();
	}

	protected void dsQuickResReport_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = string.Format("{0}/{1}/{2}",lstYYYY.SelectedValue,lstMM.SelectedValue,lstDD.SelectedValue);
	}

	protected string GetDetailLink(object pSiteCd,object pReportDay,object pReportHour) {
		return string.Format("QuickResponseDetail.aspx?site={0}&day={1}&hour={2}",pSiteCd,pReportDay,pReportHour);
	}
}
