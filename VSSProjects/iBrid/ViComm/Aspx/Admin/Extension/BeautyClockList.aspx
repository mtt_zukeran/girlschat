﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BeautyClockList.aspx.cs" Inherits="Extension_BeautyClockList" 
	Title="美人時計設定" ValidateRequest="false" %>
<%@ Import Namespace="System.Data" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="美人時計設定"></asp:Label>
</asp:Content>

<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>
					<%# DisplayWordUtil.Replace("[時間一覧]") %>
				</legend>
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle2" style="height: 24px">
							サイトコード
						</td>
						<td class="tdDataStyle" style="height: 24px">
							<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
							</asp:DropDownList>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
				<asp:Button runat="server" ID="btnInitSeek" Text="初期化" CssClass="seekbutton" OnClick="btnInit_Click" CausesValidation="False" Visible="False" />
				<br />
				<br />
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="550px">
					<table class="DataWebControlStyle" cellspacing="0" cellpadding="20" rules="all" border="1" id="ctl00_HolderContent_grdTimeList" style="width:1930px;border-collapse:collapse;">
					<asp:Repeater id="rptTimeList" runat="server" EnableViewState="false">
						<ItemTemplate>
							<tr class="RowStyle">							
								<asp:Repeater id="rptMinuteList" EnableViewState="false" runat="server" DataSource='<%# DataBinder.Eval(Container.DataItem, "TIME_ROW") %>' >
									<ItemTemplate>
										<td align="right"  bgcolor="<%# DataBinder.Eval(Container.DataItem, "CELL_COLOR")%>" >
										<asp:HyperLink runat="server" NavigateUrl='<%# GetBeautyClockViewUrl(DataBinder.Eval(Container.DataItem, "ASSIGNED_TIME")) %>' Text='<%# DataBinder.Eval(Container.DataItem, "TIME") %>'  Enabled='<%# DataBinder.Eval(Container.DataItem, "LINK_ENABLE") %>'></asp:HyperLink>
										<asp:HyperLink runat="server" NavigateUrl='<%# GetBeautyClockRegistUrl(DataBinder.Eval(Container.DataItem, "ASSIGNED_TIME")) %>' Text='登録'></asp:HyperLink></td>
									</ItemTemplate>
								</asp:Repeater>
							</tr>
						</ItemTemplate>
					</asp:Repeater>
					</table>				
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	</div>
</asp:Content>