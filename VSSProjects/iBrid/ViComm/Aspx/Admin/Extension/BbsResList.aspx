﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="BbsResList.aspx.cs" Inherits="Extension_BbsResList" Title="掲示板レス一覧"
    ValidateRequest="false" %>

<%@ Import Namespace="System" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="掲示板レス一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[検索条件]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 800px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle2">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    Enabled="false" DataValueField="SITE_CD" Width="180px">
                                </asp:DropDownList>
                            </td>
                            <td class="tdHeaderStyle2">
                                Paging Off
                            </td>
                            <td class="tdDataStyle">
                                <asp:CheckBox ID="chkPagingOff" runat="server" />
                            </td>
                        </tr>
                    </table>
                    <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                        CausesValidation="False" />
                    <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[掲示板レス一覧] </legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdBbsRes.PageIndex + 1 %>
                        of
                        <%= grdBbsRes.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="506px">
                    <asp:GridView ID="grdBbsRes" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        EnableViewState="true" DataSourceID="dsBbsRes" AllowSorting="True" SkinID="GridView"
                        OnRowDataBound="grdBbsRes_RowDataBound" DataKeyNames="SITE_CD,USER_SEQ,USER_CHAR_NO,BBS_THREAD_SEQ,BBS_THREAD_SUB_SEQ">
                        <Columns>
                            <asp:BoundField HeaderText="コメントNO" SortExpression="BBS_THREAD_SUB_SEQ" DataField="BBS_THREAD_SUB_SEQ">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="作成日時">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblCreateDate" Text='<%# Eval("CREATE_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>'></asp:Label>
                                    <br />
                                    <asp:LinkButton ID="lnkUpdateDelFlag" runat="server" CommandArgument='<%# Container.DataItemIndex %>'
                                        CommandName="DELETE" OnClientClick='<%# GetOnClientClick(Eval("DEL_FLAG")) %>'
                                        OnCommand="lnkUpdateDelFlag_Command" Text='<%# GetDelFlagMark(Eval("DEL_FLAG")) %>'></asp:LinkButton>
                                    <asp:HiddenField ID="hdnDelFlag" runat="server" Value='<%# Eval("DEL_FLAG") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="書込者">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}",Eval("RES_LOGIN_ID"),"BbsResList.aspx") %>'
                                        Text='<%# Eval("RES_LOGIN_ID") %>'></asp:HyperLink>
                                    <br />
                                    <asp:Label ID="lblHandleNm" runat="server" Text='<%# GetHandleNmMark(Eval("RES_HANDLE_NM"), Eval("ANONYMOUS_FLAG")) %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="内容">
                                <ItemTemplate>
                                    <asp:Label ID="lblResDoc" runat="server" Text='<%# string.Concat(Eval("BBS_RES_DOC1"), Eval("BBS_RES_DOC2"), Eval("BBS_RES_DOC3"), Eval("BBS_RES_DOC4")) %>'
                                        CssClass="Warp" Width="650px"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
            <br />
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsBbsRes" runat="server" SelectMethod="GetPageCollection"
        TypeName="BbsRes" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelected="dsBbsRes_Selected">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSeekSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:QueryStringParameter DefaultValue="" Name="pUserSeq" QueryStringField="userseq"
                Type="String" />
            <asp:QueryStringParameter DefaultValue="" Name="pUserCharNo" QueryStringField="usercharno"
                Type="String" />
            <asp:QueryStringParameter DefaultValue="" Name="pBbsThreadSeq" QueryStringField="bbsthreadseq"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
</asp:Content>
