﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SiteManagementExList.aspx.cs" Inherits="Extension_SiteManagementExList"
	Title="サイト構成拡張設定" ValidateRequest="false" %>

<%@ Import Namespace="System.Data" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="サイト構成拡張設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset>
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[サイト内容]</legend>
						<asp:PlaceHolder ID="plcSiteEx" runat="server" Visible="false">
						<table border="0" style="width: 760px; margin-bottom: 10px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle" style="width:220px">
									サイト名
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblSiteNm" runat="server"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ｱｲﾄﾞﾙ出演者登録年齢下限
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtRegistCastAgeMin" runat="server" MaxLength="2" Width="30px"></asp:TextBox>歳以上のみ登録可
									<asp:RequiredFieldValidator ID="vdeRegistCastAgeMin" runat="server" ErrorMessage="ｱｲﾄﾞﾙ出演者登録年齢下限を正しく入力して下さい" ControlToValidate="txtRegistCastAgeMin" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender33" TargetControlID="vdeRegistCastAgeMin" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtRegistCastAgeMin" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									この娘を探せ 第一ﾋﾝﾄ発表時刻(初期値)
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstFirstHintAnnounceHour" runat="server" DataSource='<%# HourList %>'>
									</asp:DropDownList>時
									<asp:DropDownList ID="lstFirstHintAnnounceMinute" runat="server" DataSource='<%# MinuteList %>'>
									</asp:DropDownList>分
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									この娘を探せ 第二ﾋﾝﾄ発表時刻(初期値)
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstSecondHintAnnounceHour" runat="server" DataSource='<%# HourList %>'>
									</asp:DropDownList>時
									<asp:DropDownList ID="lstSecondHintAnnounceMinute" runat="server" DataSource='<%# MinuteList %>'>
									</asp:DropDownList>分
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									この娘を探せ 第三ﾋﾝﾄ発表時刻(初期値)
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstThirdHintAnnounceHour" runat="server" DataSource='<%# HourList %>'>
									</asp:DropDownList>時
									<asp:DropDownList ID="lstThirdHintAnnounceMinute" runat="server" DataSource='<%# MinuteList %>'>
									</asp:DropDownList>分
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									この娘を探せ 懸賞ﾎﾟｲﾝﾄ(初期値)
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtBountyPoint" runat="server"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdeBountyPoint" runat="server" ErrorMessage="懸賞ポイントを正しく入力して下さい" ControlToValidate="txtBountyPoint" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdeBountyPoint" HighlightCssClass="validatorCallout" />
									<asp:RangeValidator ID="vdrBountyPoint" runat="server" ErrorMessage="懸賞ポイントを正しく入力して下さい" ControlToValidate="txtBountyPoint" MaximumValue="999999" MinimumValue="1"
										Type="Integer" ValidationGroup="Update" Display="Dynamic">*</asp:RangeValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrBountyPoint" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtBountyPoint" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									この娘を探せ 月間懸賞ﾎﾟｲﾝﾄ(初期値)
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtMonthlyBountyPoint" runat="server"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdeMonthlyBountyPoint" runat="server" ErrorMessage="月間懸賞ポイントを正しく入力して下さい" ControlToValidate="txtMonthlyBountyPoint" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender24" TargetControlID="vdeMonthlyBountyPoint" HighlightCssClass="validatorCallout" />
									<asp:RangeValidator ID="vdrMonthlyBountyPoint" runat="server" ErrorMessage="月間懸賞ポイントを正しく入力して下さい" ControlToValidate="txtMonthlyBountyPoint" MaximumValue="9999999" MinimumValue="1"
										Type="Integer" ValidationGroup="Update" Display="Dynamic">*</asp:RangeValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender25" TargetControlID="vdrMonthlyBountyPoint" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtMonthlyBountyPoint" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾎﾟｲﾝﾄ購入お知らせ設定								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtBuyPointMailCommDays" runat="server" MaxLength="1" Width="15px"></asp:TextBox>日以内に接触のあった男性を対象にする
									<asp:RegularExpressionValidator ID="vdeBuyPointMailCommDays" runat="server" ErrorMessage="日付は1桁の数値で入力してください" ValidationExpression="[0-9]" ControlToValidate="txtBuyPointMailCommDays"
										ValidationGroup="Update">*</asp:RegularExpressionValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeBuyPointMailCommDays" HighlightCssClass="validatorCallout" />
									<asp:RequiredFieldValidator ID="vdrBuyPointMailCommDays" runat="server" ErrorMessage="日付を入力してください" ControlToValidate="txtBuyPointMailCommDays" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrBuyPointMailCommDays" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtBuyPointMailCommDays" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾗﾌﾞﾘｽﾄ対象ﾌﾟﾛﾌｨｰﾙ閲覧期間
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLoveListProfileDays" runat="server" MaxLength="2" Width="15px"></asp:TextBox>日以内にプロフィールが参照された場合、ラブリストの対象とする
									<asp:RequiredFieldValidator ID="vdrLoveListProfileDays" runat="server" ErrorMessage="ラブリスト対象プロフィール閲覧期間を入力してください。" ControlToValidate="txtLoveListProfileDays"
										ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdrLoveListProfileDays" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtLoveListProfileDays" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									(女性)お気に入り登録報酬ﾎﾟｲﾝﾄ								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCastFavoritePoint" runat="Server" MaxLength="4" Width="30px"></asp:TextBox>
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtCastFavoritePoint">
									</ajaxToolkit:FilteredTextBoxExtender>
									<asp:RegularExpressionValidator ID="vdeCastFavoritePoint" runat="server" ControlToValidate="txtCastFavoritePoint" ErrorMessage="お気に入られ報酬ポイントを正しく入力して下さい"
										ValidationExpression="0|[1-9]\d{0,3}" ValidationGroup="Update">*</asp:RegularExpressionValidator>
									<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCallout" TargetControlID="vdeCastFavoritePoint">
									</ajaxToolkit:ValidatorCalloutExtender>
									<asp:RequiredFieldValidator ID="vdrCastFavoritePoint" runat="server" ErrorMessage="お気に入られ報酬ポイントを正しく入力して下さい" ControlToValidate="txtCastFavoritePoint" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8" TargetControlID="vdrCastFavoritePoint" HighlightCssClass="validatorCallout" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									(女性)拒否登録減算ﾎﾟｲﾝﾄ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCastRefusePoint" runat="Server" MaxLength="4" Width="30px" Enabled="false"></asp:TextBox>ポイントを拒否登録時に減算する
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtCastRefusePoint">
									</ajaxToolkit:FilteredTextBoxExtender>
									<asp:RegularExpressionValidator ID="vdeCastRefusePoint" runat="server" ControlToValidate="txtCastRefusePoint" ErrorMessage="拒否登録減算ポイントを正しく入力して下さい" ValidationExpression="0|[1-9]\d{0,3}"
										ValidationGroup="Update">*</asp:RegularExpressionValidator>
									<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCallout" TargetControlID="vdeCastRefusePoint">
									</ajaxToolkit:ValidatorCalloutExtender>
									<asp:RequiredFieldValidator ID="vdrCastRefusePoint" runat="server" ControlToValidate="txtCastRefusePoint" ErrorMessage="拒否登録減算ポイントを正しく入力して下さい" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCallout" TargetControlID="vdrCastRefusePoint">
									</ajaxToolkit:ValidatorCalloutExtender>
								</td>
							</tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    (男性)ﾒｰﾙdeﾋﾞﾝｺﾞ JACKPOT加算金額                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtManAddBingoJackpotMin" runat="Server" MaxLength="3" Width="30px"></asp:TextBox>～
                                    <asp:TextBox ID="txtManAddBingoJackpotMax" runat="Server" MaxLength="3" Width="30px"></asp:TextBox>の間でランダムに加算する
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server"
                                        Enabled="true" FilterType="Numbers" TargetControlID="txtManAddBingoJackpotMin">
                                    </ajaxToolkit:FilteredTextBoxExtender>
                                    <asp:RegularExpressionValidator ID="vdeManAddBingoJackpotMin" runat="server" ControlToValidate="txtManAddBingoJackpotMin"
                                        ErrorMessage="最小ジャックポット加算金額を正しく入力して下さい" ValidationExpression="0|[1-9]\d{0,3}"
                                        ValidationGroup="Update">*</asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdeManAddBingoJackpotMin">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:RequiredFieldValidator ID="vdrManAddBingoJackpotMin" runat="server" ControlToValidate="txtManAddBingoJackpotMin"
                                        ErrorMessage="最小ジャックポット加算金額を正しく入力して下さい" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdrManAddBingoJackpotMin">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server"
                                        Enabled="true" FilterType="Numbers" TargetControlID="txtManAddBingoJackpotMax">
                                    </ajaxToolkit:FilteredTextBoxExtender>
                                    <asp:RegularExpressionValidator ID="vdeManAddBingoJackpotMax" runat="server" ControlToValidate="txtManAddBingoJackpotMax"
                                        ErrorMessage="最大ジャックポット加算金額を正しく入力して下さい" ValidationExpression="0|[1-9]\d{0,3}"
                                        ValidationGroup="Update">*</asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdeManAddBingoJackpotMax">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:RequiredFieldValidator ID="vdrManAddBingoJackpotMax" runat="server" ControlToValidate="txtManAddBingoJackpotMax"
                                        ErrorMessage="最大ジャックポット加算金額を正しく入力して下さい" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdrManAddBingoJackpotMax">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:CompareValidator ID="vdcManAddBingoJackpot" runat="server" ControlToValidate="txtManAddBingoJackpotMax" ControlToCompare="txtManAddBingoJackpotMin" Operator="GreaterThanEqual" Type="Integer"
                                        ErrorMessage="ジャックポット加算金額の大小関係が不正です" ValidationGroup="Update">*</asp:CompareValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdcManAddBingoJackpot">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    (男性)ﾒｰﾙdeﾋﾞﾝｺﾞ JACKPOT分割率                                </td>
                                <td class="tdDataStyle">
                                    ビンゴ達成時に、ジャックポットの<asp:TextBox ID="txtManBingoJackpotDenominator" runat="server" MaxLength="3" Width="20"></asp:TextBox>分の1を当選額とする
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server"
                                        Enabled="true" FilterType="Numbers" TargetControlID="txtManBingoJackpotDenominator">
                                    </ajaxToolkit:FilteredTextBoxExtender>
                                    <asp:RegularExpressionValidator ID="vdeManBingoJackpotDenominator" runat="server"
                                        ControlToValidate="txtManBingoJackpotDenominator" ErrorMessage="ジャックポット分割率は1以上の整数を入力して下さい"
                                        ValidationExpression="[1-9]\d{0,2}" ValidationGroup="Update">*</asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender20" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdeManBingoJackpotDenominator">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:RequiredFieldValidator ID="vdrManBingoJackpotDenominator" runat="server" ControlToValidate="txtManBingoJackpotDenominator"
                                        ErrorMessage="ジャックポット分割率は1以上の整数を入力して下さい" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender21" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdrManBingoJackpotDenominator">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    (女性)ﾒｰﾙdeﾋﾞﾝｺﾞ JACKPOT加算金額                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtCastAddBingoJackpotMin" runat="Server" MaxLength="3" Width="30px"></asp:TextBox>～
                                    <asp:TextBox ID="txtCastAddBingoJackpotMax" runat="Server" MaxLength="3" Width="30px"></asp:TextBox>の間でランダムに加算する
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server"
                                        Enabled="true" FilterType="Numbers" TargetControlID="txtCastAddBingoJackpotMin">
                                    </ajaxToolkit:FilteredTextBoxExtender>
                                    <asp:RegularExpressionValidator ID="vdeCastAddBingoJackpotMin" runat="server" ControlToValidate="txtCastAddBingoJackpotMin"
                                        ErrorMessage="最小ジャックポット加算金額を正しく入力して下さい" ValidationExpression="0|[1-9]\d{0,3}"
                                        ValidationGroup="Update">*</asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdeCastAddBingoJackpotMin">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:RequiredFieldValidator ID="vdrCastAddBingoJackpotMin" runat="server" ControlToValidate="txtCastAddBingoJackpotMin"
                                        ErrorMessage="最小ジャックポット加算金額を正しく入力して下さい" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdrCastAddBingoJackpotMin">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server"
                                        Enabled="true" FilterType="Numbers" TargetControlID="txtCastAddBingoJackpotMax">
                                    </ajaxToolkit:FilteredTextBoxExtender>
                                    <asp:RegularExpressionValidator ID="vdeCastAddBingoJackpotMax" runat="server" ControlToValidate="txtCastAddBingoJackpotMax"
                                        ErrorMessage="最大ジャックポット加算金額を正しく入力して下さい" ValidationExpression="0|[1-9]\d{0,3}"
                                        ValidationGroup="Update">*</asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdeCastAddBingoJackpotMax">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:RequiredFieldValidator ID="vdrCastAddBingoJackpotMax" runat="server" ControlToValidate="txtCastAddBingoJackpotMax"
                                        ErrorMessage="最大ジャックポット加算金額を正しく入力して下さい" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender17" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdrCastAddBingoJackpotMax">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:CompareValidator ID="vdcCastAddBingoJackpot" runat="server" ControlToValidate="txtCastAddBingoJackpotMax" ControlToCompare="txtCastAddBingoJackpotMin" Operator="GreaterThanEqual" Type="Integer"
                                        ErrorMessage="ジャックポット加算金額の大小関係が不正です" ValidationGroup="Update">*</asp:CompareValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdcCastAddBingoJackpot">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    (女性)ﾒｰﾙdeﾋﾞﾝｺﾞ JACKPOT分割率                                </td>
                                <td class="tdDataStyle">
                                    ビンゴ達成時に、ジャックポットの<asp:TextBox ID="txtCastBingoJackpotDenominator" runat="server" MaxLength="3" Width="20"></asp:TextBox>分の1を当選額とする
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server"
                                        Enabled="true" FilterType="Numbers" TargetControlID="txtCastBingoJackpotDenominator">
                                    </ajaxToolkit:FilteredTextBoxExtender>
                                    <asp:RegularExpressionValidator ID="vdeCastBingoJackpotDenominator" runat="server"
                                        ControlToValidate="txtCastBingoJackpotDenominator" ErrorMessage="ジャックポット分割率は1以上の整数を入力して下さい"
                                        ValidationExpression="[1-9]\d{0,2}" ValidationGroup="Update">*</asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdeCastBingoJackpotDenominator">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:RequiredFieldValidator ID="vdrCastBingoJackpotDenominator" runat="server" ControlToValidate="txtCastBingoJackpotDenominator"
                                        ErrorMessage="ジャックポット分割率は1以上の整数を入力して下さい" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender23" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdrCastBingoJackpotDenominator">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    お宝deﾋﾞﾝｺﾞ JACKPOT加算金額
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtBbsBingoJackpotMin" runat="Server" MaxLength="3" Width="30px"></asp:TextBox>～
                                    <asp:TextBox ID="txtBbsBingoJackpotMax" runat="Server" MaxLength="3" Width="30px"></asp:TextBox>の間でランダムに加算する
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server"
                                        Enabled="true" FilterType="Numbers" TargetControlID="txtBbsBingoJackpotMin">
                                    </ajaxToolkit:FilteredTextBoxExtender>
                                    <asp:RegularExpressionValidator ID="vdeBbsBingoJackpotMin" runat="server" ControlToValidate="txtBbsBingoJackpotMin"
                                        ErrorMessage="最小ジャックポット加算金額を正しく入力して下さい" ValidationExpression="0|[1-9]\d{0,3}"
                                        ValidationGroup="Update">*</asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender26" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdeBbsBingoJackpotMin">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:RequiredFieldValidator ID="vdrBbsBingoJackpotMin" runat="server" ControlToValidate="txtBbsBingoJackpotMin"
                                        ErrorMessage="最小ジャックポット加算金額を正しく入力して下さい" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender27" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdrBbsBingoJackpotMin">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server"
                                        Enabled="true" FilterType="Numbers" TargetControlID="txtBbsBingoJackpotMax">
                                    </ajaxToolkit:FilteredTextBoxExtender>
                                    <asp:RegularExpressionValidator ID="vdeBbsBingoJackpotMax" runat="server" ControlToValidate="txtBbsBingoJackpotMax"
                                        ErrorMessage="最大ジャックポット加算金額を正しく入力して下さい" ValidationExpression="0|[1-9]\d{0,3}"
                                        ValidationGroup="Update">*</asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender28" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdeBbsBingoJackpotMax">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:RequiredFieldValidator ID="vdrBbsBingoJackpotMax" runat="server" ControlToValidate="txtBbsBingoJackpotMax"
                                        ErrorMessage="最大ジャックポット加算金額を正しく入力して下さい" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender29" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdrBbsBingoJackpotMax">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:CompareValidator ID="vdcBbsBingoJackpot" runat="server" ControlToValidate="txtBbsBingoJackpotMax" ControlToCompare="txtBbsBingoJackpotMin" Operator="GreaterThanEqual" Type="Integer"
                                        ErrorMessage="ジャックポット加算金額の大小関係が不正です" ValidationGroup="Update">*</asp:CompareValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender30" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdcBbsBingoJackpot">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    お宝deﾋﾞﾝｺﾞ JACKPOT分割率
                                </td>
                                <td class="tdDataStyle">
                                    ビンゴ達成時に、ジャックポットの<asp:TextBox ID="txtBbsBingoJackpotDenominator" runat="server" MaxLength="3" Width="20"></asp:TextBox>分の1を当選額とする
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server"
                                        Enabled="true" FilterType="Numbers" TargetControlID="txtBbsBingoJackpotDenominator">
                                    </ajaxToolkit:FilteredTextBoxExtender>
                                    <asp:RegularExpressionValidator ID="vdeBbsBingoJackpotDenominator" runat="server"
                                        ControlToValidate="txtBbsBingoJackpotDenominator" ErrorMessage="ジャックポット分割率は1以上の整数を入力して下さい"
                                        ValidationExpression="[1-9]\d{0,2}" ValidationGroup="Update">*</asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender31" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdeBbsBingoJackpotDenominator">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:RequiredFieldValidator ID="vdrBbsBingoJackpotDenominator" runat="server" ControlToValidate="txtBbsBingoJackpotDenominator"
                                        ErrorMessage="ジャックポット分割率は1以上の整数を入力して下さい" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender32" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdrBbsBingoJackpotDenominator">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                </td>
                            </tr>
                            <tr>
								<td class="tdHeaderStyle">
									待機予約時間通知ﾒｰﾙ送信時間
								</td>
								<td class="tdDataStyle">
									待機予約時間の<asp:TextBox ID="txtTxMailWaitStartPrevMin" runat="server" MaxLength="3" Width="30px"></asp:TextBox>分前に通知メールを送信する
									<asp:RequiredFieldValidator ID="vdeTxMailWaitStartPrevMin" runat="server" ErrorMessage="待機予約時間通知メール送信時間を正しく入力して下さい" ControlToValidate="txtTxMailWaitStartPrevMin" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender34" TargetControlID="vdeTxMailWaitStartPrevMin" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtTxMailWaitStartPrevMin" />
								</td>
							</tr>
                            <tr>
								<td class="tdHeaderStyle">
									ﾛｸﾞｲﾝ予定時間通知ﾒｰﾙ送信時間
								</td>
								<td class="tdDataStyle">
									ログイン予定時間の<asp:TextBox ID="txtTxMailLoginStartPrevMin" runat="server" MaxLength="3" Width="30px"></asp:TextBox>分前に通知メールを送信する
									<asp:RequiredFieldValidator ID="vdeTxMailLoginStartPrevMin" runat="server" ErrorMessage="ログイン予定時間通知メール送信時間を正しく入力して下さい" ControlToValidate="txtTxMailLoginStartPrevMin" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender35" TargetControlID="vdeTxMailLoginStartPrevMin" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtTxMailLoginStartPrevMin" />
								</td>
							</tr>
                            <tr>
								<td class="tdHeaderStyle">
									ﾒｰﾙおねだり機能全開放期間
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtMailRequestOpenStartDate" runat="server" MaxLength="16" Width="120px"></asp:TextBox>
									<ajaxToolkit:MaskedEditExtender ID="mskMailRequestOpenStartDate" runat="server" MaskType="DateTime"
										Mask="9999/99/99 99:99:99" UserDateFormat="YearMonthDay" UserTimeFormat="TwentyFourHour"
										ClearMaskOnLostFocus="true" TargetControlID="txtMailRequestOpenStartDate">
									</ajaxToolkit:MaskedEditExtender>
									～
									<asp:TextBox ID="txtMailRequestOpenEndDate" runat="server" MaxLength="16" Width="120px"></asp:TextBox>
									<ajaxToolkit:MaskedEditExtender ID="mskMailRequestOpenEndDate" runat="server" MaskType="DateTime"
										Mask="9999/99/99 99:99:99" UserDateFormat="YearMonthDay" UserTimeFormat="TwentyFourHour"
										ClearMaskOnLostFocus="true" TargetControlID="txtMailRequestOpenEndDate">
									</ajaxToolkit:MaskedEditExtender>
									<br />
									<asp:Label ID="lblErrorMessageMailRequestOpenTerm" runat="server" ForeColor="red" Visible="false"></asp:Label>
								</td>
							</tr>
                            <tr>
								<td class="tdHeaderStyle">
									最新GCｱﾌﾟﾘVer(iOS)
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLatestGcappVeriOS" runat="server" MaxLength="12" Width="80px"></asp:TextBox>
								</td>
							</tr>
                            <tr>
								<td class="tdHeaderStyle">
									最新GCｱﾌﾟﾘVer(Android)
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLatestGcappVerAndroid" runat="server" MaxLength="12" Width="80px"></asp:TextBox>
								</td>
							</tr>
                            <tr>
								<td class="tdHeaderStyle">
									初回購入特典有効日数
								</td>
								<td class="tdDataStyle">
									登録から<asp:TextBox ID="txtFirstSettleEnableDays" runat="server" MaxLength="3" Width="30px"></asp:TextBox>日後まで有効
									<asp:RequiredFieldValidator ID="vdeFirstSettleEnableDays" runat="server" ErrorMessage="初回購入特典有効日数を正しく入力して下さい" ControlToValidate="txtFirstSettleEnableDays" ValidationGroup="Update">*</asp:RequiredFieldValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender36" TargetControlID="vdeFirstSettleEnableDays" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtFirstSettleEnableDays" />
								</td>
							</tr>
                            <tr>
								<td class="tdHeaderStyle2">
									ログインパスワード送信SMS本文
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtReminderSmsDoc" runat="server" Columns="60" MaxLength="70" Rows="4" TextMode="MultiLine" Width="497px"></asp:TextBox>
								</td>
							</tr>
                        </table>
						</asp:PlaceHolder>
						「男性会員向け自動送信メール設定」						<table border="0" style="width: 760px; margin-bottom: 10px" class="tableStyle">
							<tr>
								<td class="tdHeaderWidthDynamicStyle">
									ﾒｰﾙ添付動画受付完了
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstTpManMailMovie" runat="server" Width="500px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderWidthDynamicStyle">
									ﾒｰﾙ添付写真受付完了
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstTpManMailPic" runat="server" Width="500px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderWidthDynamicStyle">
									ﾌﾟﾛﾌｨｰﾙ写真受付完了
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstTpManPfPic" runat="server" Width="500px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderWidthDynamicStyle">
									発信種別相違(音声選択TV発信)
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstTpManTelFailVoiceTv" runat="server" Width="500px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderWidthDynamicStyle">
									発信種別相違(TV選択音声発信)
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstTpManTelFailTvVoice" runat="server" Width="500px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderWidthDynamicStyle">
									相手通話中
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstTpManTalkBusy" runat="server" Width="500px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderWidthDynamicStyle">
									待機開始通知(通話履歴あり)
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstTpManWaitingCastWithTalk" runat="server" Width="500px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderWidthDynamicStyle2">
									外部登録時仮登録受付
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstTpManOutsideTempRegist" runat="server" Width="500px">
									</asp:DropDownList>
								</td>
							</tr>
						</table>
						「出演者向け自動送信メール設定」
						<table border="0" style="width: 760px; margin-bottom: 10px" class="tableStyle">
							<tr>
								<td class="tdHeaderWidthDynamicStyle">
									ﾒｰﾙ添付動画受付完了
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstTpCastMailMovie" runat="server" Width="500px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderWidthDynamicStyle">
									ﾒｰﾙ添付写真受付完了
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstTpCastMailPic" runat="server" Width="500px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderWidthDynamicStyle">
									ﾌﾟﾛﾌｨｰﾙ・ｷﾞｬﾗﾘｰ写真受付完了
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstTpCastPfPic" runat="server" Width="500px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderWidthDynamicStyle">
									ﾌﾟﾛﾌｨｰﾙ動画受付完了
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstTpCastPfMovie" runat="server" Width="500px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderWidthDynamicStyle">
									掲示板動画受付完了
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstTpCastBbsMovie" runat="server" Width="500px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderWidthDynamicStyle">
									掲示板写真受付完了
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstTpCastBbsPic" runat="server" Width="500px">
									</asp:DropDownList>
								</td>
							</tr>
							<asp:PlaceHolder ID="plcSocialGame" runat="server" Visible="false">
							<tr>
								<td class="tdHeaderWidthDynamicStyle">
									ｿｰｼｬﾙｹﾞｰﾑ動画受付完了
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstTpCastSocialGameMovie" runat="server" Width="500px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderWidthDynamicStyle">
									ｿｰｼｬﾙｹﾞｰﾑ写真受付完了
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstTpCastSocialGamePic" runat="server" Width="500px">
									</asp:DropDownList>
								</td>
							</tr>
							</asp:PlaceHolder>
							<tr>
								<td class="tdHeaderWidthDynamicStyle">
									発信種別相違(音声選択TV発信)
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstTpCastTelFailVoiceTv" runat="server" Width="500px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderWidthDynamicStyle">
									発信種別相違(TV選択音声発信)
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstTpCastTelFailTvVoice" runat="server" Width="500px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderWidthDynamicStyle2">
									相手通話中
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstTpCastTalkBusy" runat="server" Width="500px">
									</asp:DropDownList>
								</td>
							</tr>
						</table>
						<asp:Button ID="btnUpdate" runat="server" CausesValidation="True" ValidationGroup="Update" CssClass="seekbutton" OnClick="btnUpdate_Click" Text="更新" OnClientClick="return confirm('更新を行いますか？');" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtlTransfer">
					<fieldset class="fieldset-inner">
						<legend>[サイト内容]</legend>
						<asp:Label ID="lblSiteCd" runat="server" Text="" Visible="false"></asp:Label>
						<table border="0" style="width: 760px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									サイト名
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblSiteNm1" runat="server"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									送金先番号
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtDocomoMoneyTransferTEL" runat="Server" MaxLength="11"></asp:TextBox>
									<ajaxToolkit:FilteredTextBoxExtender ID="xvdnDocomoMoneyTransferTEL" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtDocomoMoneyTransferTEL" />
									<asp:RegularExpressionValidator ID="vdeDocomoMoneyTransferTEL" runat="server" ErrorMessage="送金先番号は11桁の数値で入力してください" ValidationExpression="[0-9]{11}" ControlToValidate="txtDocomoMoneyTransferTEL"
										ValidationGroup="UpdateTransfer">*</asp:RegularExpressionValidator>
									<ajaxToolkit:ValidatorCalloutExtender ID="xvdc_vdeDocomoMoneyTransferTEL" runat="Server" TargetControlID="vdeDocomoMoneyTransferTEL" HighlightCssClass="validatorCallout" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									送金先名義
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtDocomoMoneyTransferNM" runat="Server" MaxLength="2"></asp:TextBox>
									<asp:RegularExpressionValidator ID="vdeDocomoMoneyTransferNM" runat="server" ErrorMessage="送金先名義は全角カタカナ２文字で登録してください" ValidationExpression="[\u30A0-\u30FF]{2}"
										ControlToValidate="txtDocomoMoneyTransferNM" ValidationGroup="UpdateTransfer">*</asp:RegularExpressionValidator>
									<ajaxToolkit:ValidatorCalloutExtender ID="xvdc_vdeDocomoMoneyTransferNM" runat="Server" TargetControlID="vdeDocomoMoneyTransferNM" HighlightCssClass="validatorCallout" />
								</td>
							</tr>
						</table>
						<asp:Button ID="btnUpdateTransfer" runat="server" CausesValidation="True" ValidationGroup="UpdateTransfer" CssClass="seekbutton" OnClick="btnUpdate_Click"
							Text="更新" OnClientClick="return confirm('更新を行いますか？');" />
						<asp:Button runat="server" ID="btnCancel1" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<asp:Panel ID="pnlList" runat="server" ScrollBars="auto">
			<fieldset>
				<legend>[サイト一覧]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count
						<%# GetRecCount() %>
					</a>
					<br />
					<a class="reccount">Current viewing page
						<%= grdSiteManagementEx.PageIndex + 1 %>
						of
						<%= grdSiteManagementEx.PageCount %>
					</a>
				</asp:Panel>
				&nbsp;
				<asp:Panel ID="pnlGrid" runat="server">
					<asp:GridView ID="grdSiteManagementEx" runat="server" DataSourceID="dsSiteManagementEx" AutoGenerateColumns="False" AllowPaging="true" AllowSorting="true"
						SkinID="GridViewColor">
						<Columns>
							<asp:TemplateField HeaderText="サイトコード">
								<ItemTemplate>
									<asp:LinkButton ID="lnkSiteCd" runat="server" Text='<%# Eval("SITE_CD") %>' CommandArgument='<%# Eval("SITE_CD") %>' OnCommand="lnkSiteCd_Command" CausesValidation="False">
									</asp:LinkButton>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Left" />
							</asp:TemplateField>
							<asp:BoundField DataField="UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
							<%--<asp:TemplateField HeaderText="">
								<ItemTemplate>
									<asp:LinkButton ID="lnkTransfer" runat="server" CausesValidation="False" CommandArgument='<%# Eval("SITE_CD") %>' OnCommand="lnkTransfer_Command" Text="送金先情報管理">
									</asp:LinkButton>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Left" />
							</asp:TemplateField>--%>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsSiteManagementEx" runat="server" SelectMethod="GetPageCollection" SelectCountMethod="GetPageCount" EnablePaging="true" OnSelected="dsSiteManagementEx_Selected"
		TypeName="SiteManagementEx"></asp:ObjectDataSource>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server" Enabled="true" FilterType="Numbers,Custom" ValidChars="." TargetControlID="txtLatestGcappVeriOS" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" Enabled="true" FilterType="Numbers,Custom" ValidChars="." TargetControlID="txtLatestGcappVerAndroid" />
</asp:Content>
