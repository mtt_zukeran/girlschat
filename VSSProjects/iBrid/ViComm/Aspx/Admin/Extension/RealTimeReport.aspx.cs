﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: リアルタイムレポート

--	Progaram ID		: RealTimeReport
--
--  Creation Date	: 2011.08.10
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using iBridCommLib;
using System.Drawing;
using ViComm;

public partial class Extension_RealTimeReport : System.Web.UI.Page {
	private int iLoginCountSum = 0;
	private int iUsedGamePointSum = 0;
	private int iUsedPointSum = 0;
	private int iBattleCountSum = 0;
	private int iMissionCountSum = 0;
	private int iTreasureGetCountSum = 0;

	protected string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SexCd"]);
		}
		private set {
			this.ViewState["SexCd"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		if (!this.IsPostBack) {
			this.SexCd = this.Request.QueryString["sexcd"];
			if (ViCommConst.MAN.Equals(this.SexCd)) {
				this.lblPgmTitle.Text = "男性" + this.lblPgmTitle.Text;
			} else {
				this.lblPgmTitle.Text = "女性" + this.lblPgmTitle.Text;
			}
			this.Title = this.lblPgmTitle.Text;

			this.InitPage();

			this.GetList();
			this.pnlInfo.Visible = true;
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		this.GetList();
		this.pnlInfo.Visible = true;
	}

	protected void dsRealTimeReport_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pReportDay"] = string.Format("{0}/{1}/{2}", this.lstYYYY.SelectedValue,this.lstMM.SelectedValue,this.lstDD.SelectedValue);
	}

	protected void grdRealTimeReport_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			iLoginCountSum += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"LOGIN_COUNT"));
			iUsedGamePointSum += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"USED_GAME_POINT"));
			iUsedPointSum += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"USED_POINT"));
			iBattleCountSum += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"BATTLE_COUNT"));
			iMissionCountSum += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"MISSION_COUNT"));
			iTreasureGetCountSum += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TREASURE_GET_COUNT"));

		} else if (e.Row.RowType == DataControlRowType.Footer) {
			Label lblLoginCountSum = e.Row.FindControl("lblLoginCountSum") as Label;
			Label lblUsedGamePointSum = e.Row.FindControl("lblUsedGamePointSum") as Label;
			Label lblUsedPointSum = e.Row.FindControl("lblUsedPointSum") as Label;
			Label lblBattleCountSum = e.Row.FindControl("lblBattleCountSum") as Label;
			Label lblMissionCountSum = e.Row.FindControl("lblMissionCountSum") as Label;
			Label lblTreasureGetCountSum = e.Row.FindControl("lblTreasureGetCountSum") as Label;

			lblLoginCountSum.Text = iLoginCountSum.ToString("N0");
			lblUsedGamePointSum.Text = iUsedGamePointSum.ToString("N0");
			lblUsedPointSum.Text = iUsedPointSum.ToString("N0");
			lblBattleCountSum.Text = iBattleCountSum.ToString("N0");
			lblMissionCountSum.Text = iMissionCountSum.ToString("N0");
			lblTreasureGetCountSum.Text = iTreasureGetCountSum.ToString("N0");
		}
	}

	private void InitPage() {
		this.grdRealTimeReport.DataSourceID = string.Empty;
		this.pnlKey.DataBind();
		this.pnlInfo.Visible = false;
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		SysPrograms.SetupDay(lstYYYY,lstMM,lstDD,false);
		this.lstYYYY.SelectedValue = DateTime.Today.ToString("yyyy");
		this.lstMM.SelectedValue = DateTime.Today.ToString("MM");
		this.lstDD.SelectedValue = DateTime.Today.ToString("dd");
	}

	private void GetList() {
		this.grdRealTimeReport.DataSourceID = "dsRealTimeReport";
		this.grdRealTimeReport.DataBind();
	}
	
	protected void btnCSV_Click(object sender,EventArgs e) {

		if (grdRealTimeReport.Rows.Count == 0) {
			return;
		}

		DataTable oCsvData;
		using (RealTimeReport oRealTimeReport = new RealTimeReport()) {
			using (DataSet oDataSet = oRealTimeReport.GetList(
				this.lstSeekSiteCd.SelectedValue,
				string.Format("{0}/{1}/{2}",this.lstYYYY.SelectedValue,this.lstMM.SelectedValue,this.lstDD.SelectedValue),
				this.SexCd)) {

				if (oDataSet.Tables[0].Rows.Count > 0) {
					oCsvData = oDataSet.Tables[0];
				} else {
					return;
				}
			}
		}

		//ヘッダ作成
		string sHeader = "時間,ﾛｸﾞｲﾝ数,累計ﾛｸﾞｲﾝ数,消費円,消費PT/G,ﾊﾞﾄﾙ回数,ﾅﾝﾊﾟ回数,お宝獲得回数";
		
		//CSV出力 
		Response.ContentType = "application/download";
		Response.AppendHeader("Content-Disposition","attachment;filename=RealTimeReport.csv");
		Response.ContentEncoding = System.Text.Encoding.GetEncoding("Shift_JIS");

		Response.Write(sHeader + "\r\n");
		foreach (DataRow oCsvRow in oCsvData.Rows) {
			string sData =
				SetCsvString(oCsvRow["REPORT_HOUR"].ToString()) + "時," +
				SetCsvString(oCsvRow["LOGIN_COUNT"].ToString()) + "," +
				SetCsvString(oCsvRow["LOGIN_COUNT_HOUR_SUM"].ToString()) + "," +
				SetCsvString(oCsvRow["USED_GAME_POINT"].ToString()) + "," +
				SetCsvString(oCsvRow["USED_POINT"].ToString()) + "," +
				SetCsvString(oCsvRow["BATTLE_COUNT"].ToString()) + "," +
				SetCsvString(oCsvRow["MISSION_COUNT"].ToString()) + "," +
				SetCsvString(oCsvRow["TREASURE_GET_COUNT"].ToString());

			Response.Write(sData + "\r\n");
		}
		Response.End();
	}
	
	private string SetCsvString(string pData) {
		if (pData.IndexOf('"') > -1 ||
			pData.IndexOf(',') > -1 ||
			pData.IndexOf('\r') > -1 ||
			pData.IndexOf('\n') > -1 ||
			pData.StartsWith(" ") || pData.StartsWith("\t") ||
			pData.EndsWith(" ") || pData.EndsWith("\t")) {
			if (pData.IndexOf('"') > -1) {
				//"を""とする
				pData = pData.Replace("\"","\"\"");
			}
			if (pData.IndexOf('\r') > -1 || pData.IndexOf('\n') > -1) {
				//改行しない 
				pData = pData.Replace("\r","");
				pData = pData.Replace("\n","");
			}
			pData = "\"" + pData + "\"";
		}
		return pData;
	}
}