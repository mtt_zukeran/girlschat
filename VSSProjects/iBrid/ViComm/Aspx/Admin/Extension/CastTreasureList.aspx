﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastTreasureList.aspx.cs" Inherits="Extension_CastTreasureList"
	Title="女性用お宝一覧" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="女性用お宝一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlKey">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel ID="pnlSeekCondition" runat="server">
					<table border="0" style="width: 800px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle2">
								サイトコード
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px" AutoPostBack="True"
									OnSelectedIndexChanged="lstSiteCd_SelectedIndexChanged">
								</asp:DropDownList>
							</td>
							<td class="tdHeaderStyle2">
								ステージグループ
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstSeekStageGroupType" runat="server" DataSourceID="dsStageGroup" OnDataBound="lst_DataBound" ValidationGroup="Create" DataTextField="STAGE_GROUP_NM"
									DataValueField="STAGE_GROUP_TYPE" Width="170px">
								</asp:DropDownList>
							</td>
						</tr>
					</table>
				</asp:Panel>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Key" CausesValidation="True" />
				<asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton" ValidationGroup="Create" OnClick="btnCreate_Click" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" CausesValidation="False" />
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlCastTreasureInfo">
			<fieldset class="fieldset">
				<legend>[お宝情報]</legend>
				<table border="0" style="width: 600px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							ステージグループ
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstStageGroupType" runat="server" DataSourceID="dsStageGroup" ValidationGroup="Create" DataTextField="STAGE_GROUP_NM" DataValueField="STAGE_GROUP_TYPE"
								Width="170px">
							</asp:DropDownList>
							<% // 必須チェック %>
							<asp:RequiredFieldValidator ID="vdrStageGroupType" runat="server" ErrorMessage="ステージグループを選択してください。" ControlToValidate="lstStageGroupType" ValidationGroup="Create">*</asp:RequiredFieldValidator>
							<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" TargetControlID="vdrStageGroupType" HighlightCssClass="validatorCallout" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							お宝ID(SEQ)
						</td>
						<td class="tdDataStyle">
							<asp:Label ID="lblCastTreasureId" runat="server"></asp:Label>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							お宝名
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtCastTreasureNm" runat="server" MaxLength="20" Width="200px"></asp:TextBox>
							<% // 必須チェック %>
							<asp:RequiredFieldValidator ID="vdrCastTreasureNm" runat="server" ErrorMessage="お宝名を入力してください。" ControlToValidate="txtCastTreasureNm" ValidationGroup="Update">*</asp:RequiredFieldValidator>
							<ajaxToolkit:ValidatorCalloutExtender ID="vdeCastTreasureNm" runat="Server" TargetControlID="vdrCastTreasureNm" HighlightCssClass="validatorCallout" />
						</td>
					</tr>
				</table>
				<asp:Panel ID="pnlCommand" runat="server">
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Update" OnClick="btnUpdate_Click" />
					<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" ValidationGroup="Delete" OnClick="btnDelete_Click" />
					<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" CausesValidation="False" Visible="true" OnClick="btnCancel_Click" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[お宝一覧]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count
						<%# GetRecCount() %>
					</a>
					<br />
					<a class="reccount">Current viewing page
						<%= grdCastTreasure.PageIndex + 1 %>
						of
						<%= grdCastTreasure.PageCount %>
					</a>
				</asp:Panel>
				&nbsp;
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="600px">
					<asp:GridView ID="grdCastTreasure" runat="server" AllowPaging="True" AutoGenerateColumns="False" PageSize="30" EnableSortingAndPagingCallbacks="false"
						DataSourceID="dsCastTreasure" SkinID="GridViewColor" AllowSorting="true" DataKeyNames="SITE_CD,STAGE_GROUP_TYPE,CAST_TREASURE_SEQ">
						<Columns>
							<asp:TemplateField HeaderText="お宝SEQ">
								<ItemTemplate>
									<asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%# Container.DataItemIndex %>' Text='<%# Eval("CAST_TREASURE_SEQ") %>' OnCommand="lnkEdit_Command"></asp:LinkButton>
									<asp:HiddenField ID="hdnStageGroupType" runat="server" Value='<%# Eval("STAGE_GROUP_TYPE") %>' />
									<asp:HiddenField ID="hdnCastTreasureSeq" runat="server" Value='<%# Eval("CAST_TREASURE_SEQ") %>' />
								</ItemTemplate>
								<ItemStyle HorizontalAlign="left" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="お宝名">
								<ItemTemplate>
									<asp:Label ID="lblCastTreasureNm" runat="server" Text='<%# Eval("CAST_TREASURE_NM") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="left" />
							</asp:TemplateField>
							<asp:BoundField DataField="STAGE_GROUP_NM" HeaderText="ステージグループ">
								<ItemStyle HorizontalAlign="left" />
							</asp:BoundField>
							<asp:BoundField DataField="TOTAL_CAST_POSSESSION_COUNT" DataFormatString="{0:N0}" HeaderText="累計所持数">
								<ItemStyle HorizontalAlign="right" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="">
								<ItemTemplate>
									<asp:HyperLink ID="lnkDropRateList" runat="server" NavigateUrl='<%# string.Format("~/Extension/CastTreasureDropRateList.aspx?sitecd={0}&stagegrouptype={1}&casttreasureseq={2}", Eval("SITE_CD"), Eval("STAGE_GROUP_TYPE"), Eval("CAST_TREASURE_SEQ"))%>'
										Text="ドロップ率一覧"></asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
			<br />
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsCastTreasure" runat="server" ConvertNullToDBNull="false" EnablePaging="True" OnSelected="dsCastTreasure_Selected" SelectCountMethod="GetPageCount"
		SelectMethod="GetPageCollection" SortParameterName="" TypeName="CastTreasure">
		<SelectParameters>
			<asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstSeekStageGroupType" Name="pStageGroupType" PropertyName="SelectedValue" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsStageGroup" runat="server" SelectMethod="GetList" TypeName="StageGroup">
		<SelectParameters>
			<asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue" Type="String" />
			<asp:Parameter Name="pSexCd" DefaultValue="3" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="お宝情報を更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="お宝情報を削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>
