<%@ Import Namespace="ViComm" %>

<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="AcceptWithdrawalList.aspx.cs" Inherits="Extension_AcceptWithdrawalList"
    Title="退会申請履歴" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="退会申請履歴"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[検索]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 1100px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                サイト
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px">
                                </asp:DropDownList>
                            </td>
                            <td class="tdHeaderStyle">
                                理由区分
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstWithdrawalReason" runat="server" DataSourceID="" DataTextField="CODE_NM"
                                    DataValueField="CODE" Width="170px">
                                </asp:DropDownList>
                            </td>
                            <td class="tdHeaderStyle">
                                理由詳細
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtWithdrawalReasonDoc" runat="server" Width="170px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                ログインID
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtLoginId" runat="server" MaxLength="8" Width="88px"></asp:TextBox>
                            </td>
                            <td class="tdHeaderStyle2">
                                申請状態
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstWithdrawalStatus" runat="server" DataSourceID="dsWithdrawalStatus"
                                    DataTextField="CODE_NM" DataValueField="CODE">
                                </asp:DropDownList>
                            </td>
                            <td class="tdHeaderStyle">
                                備考
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtRemarks" runat="server" Width="170px" MaxLength="240"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle2">
                                申請日時
                            </td>
                            <td class="tdDataStyle" colspan="3">
                                <asp:DropDownList ID="lstFromYYYY" runat="server" Width="60px">
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstFromMM" runat="server" Width="40px">
                                </asp:DropDownList>月
                                <asp:DropDownList ID="lstFromDD" runat="server" Width="40px">
                                </asp:DropDownList>日&nbsp;〜&nbsp;
                                <asp:DropDownList ID="lstToYYYY" runat="server" Width="60px">
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstToMM" runat="server" Width="40px">
                                </asp:DropDownList>月
                                <asp:DropDownList ID="lstToDD" runat="server" Width="40px">
                                </asp:DropDownList>日
                                <asp:CustomValidator ID="vdcFromToDate" runat="server" ErrorMessage="" OnServerValidate="vdcFromToDate_SeverValidate"
                                    ValidationGroup="Key"></asp:CustomValidator>
                            </td>
                            <td class="tdHeaderStyle">
                                友達紹介で登録のみ
                            </td>
                            <td class="tdDataStyle">
                                <asp:CheckBox ID="chkIntroduceFriend" runat="server" Text="" Checked="true" />
                                <asp:CheckBox ID="chkUnConfirmed" runat="server" Text="" Checked="true" Visible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDataStyle" colspan="4">
                            </td>
                            <td class="tdHeaderStyle2">
                                手動対応分のみ
                            </td>
                            <td class="tdDataStyle">
                                <asp:CheckBox ID="chkManualFlag" runat="server" Text="" Checked="true" />
                            </td>
                        </tr>
                    </table>
                    <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                        ValidationGroup="Key" />
                    <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
                    <asp:Button ID="btnRegist" runat="server" CssClass="seekbutton" OnClick="btnRegist_Click"
                        Text="追加" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlWithdrawal" HorizontalAlign="Left" Visible="false"
            Width="550px">
            <table border="0">
                <tr>
                    <td>
                        <fieldset class="fieldset-inner">
                            <legend>[退会申請]</legend>
                            <table border="0" class="tableStyleAutoSize">
                                <tr>
                                    <td class="tdHeaderStyle">
                                        サイト
                                    </td>
                                    <td class="tdDataStyle">
                                        <asp:DropDownList ID="lstRegSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                            DataValueField="SITE_CD" Width="170px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdHeaderStyle">
                                        ログインID
                                    </td>
                                    <td class="tdDataStyle">
                                        <asp:TextBox ID="txtRegLoginId" runat="server" MaxLength="8" Width="60px"></asp:TextBox>
                                        <asp:CustomValidator ID="vdcLoginId" runat="server" ControlToValidate="txtRegLoginId"
                                            ErrorMessage="該当するユーザが存在しません。" OnServerValidate="vdcLoginId_ServerValidate" ValidationGroup="Update"
                                            Display="Dynamic" ValidateEmptyText="True">*</asp:CustomValidator>
                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
                                            TargetControlID="vdcLoginId" HighlightCssClass="validatorCallout" />
                                        <asp:Label ID="lblHandleNm" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdHeaderStyle">
                                        申請日時
                                    </td>
                                    <td class="tdDataStyle">
                                        <%--<asp:TextBox ID="txtRegWithdrawalDate" runat="server" Text="" />--%>
                                        <asp:DropDownList ID="lstRegYYYY" runat="server" Width="60px" DataSource='<%# YearArray %>'>
                                        </asp:DropDownList>年
                                        <asp:DropDownList ID="lstRegMM" runat="server" Width="40px" DataSource='<%# MonthArray %>'>
                                        </asp:DropDownList>月
                                        <asp:DropDownList ID="lstRegDD" runat="server" Width="40px" DataSource='<%# DayArray %>'>
                                        </asp:DropDownList>日
                                        <asp:DropDownList ID="lstRegHH" runat="server" Width="40px" DataSource='<%# HourArray %>'>
                                        </asp:DropDownList>時
                                        <asp:DropDownList ID="lstRegMI" runat="server" Width="40px" DataSource='<%# MinuteArray %>'>
                                        </asp:DropDownList>分 00秒
                                        <br />
                                        <asp:CustomValidator ID="vdcRegDate" runat="server" ErrorMessage="" OnServerValidate="vdcRegDate_SeverValidate"
                                            ValidationGroup="Update"></asp:CustomValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdHeaderStyle">
                                        理由区分
                                    </td>
                                    <td class="tdDataStyle">
                                        <asp:DropDownList ID="lstRegWithdrawalReason" runat="server" DataSourceID="" DataTextField="CODE_NM"
                                            DataValueField="CODE" Width="170px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdHeaderStyle">
                                        理由詳細
                                    </td>
                                    <td class="tdDataStyle">
                                        <asp:TextBox ID="txtRegWithdrawalReasonDoc" runat="server" Width="320px" TextMode="MultiLine"
                                            Height="100px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdHeaderStyle2">
                                        備考
                                    </td>
                                    <td class="tdDataStyle">
                                        <asp:TextBox ID="txtRegWithdrawalRemarks" runat="server" Width="320px" TextMode="MultiLine"
                                            Height="100px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <asp:Button runat="server" ID="btnUpdate" Text="登録" CssClass="seekbutton" ValidationGroup="Update"
                                OnClick="btnUpdate_Click" />
                            <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" CausesValidation="false"
                                OnClick="btnCancel_Click" />
                        </fieldset>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[退会申請一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%#GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%=grdAcceptWithdrawal.PageIndex + 1%>
                        of
                        <%=grdAcceptWithdrawal.PageCount%>
                    </a>
                    <br />
                </asp:Panel>
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto">
                    <asp:GridView ID="grdAcceptWithdrawal" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        DataSourceID="dsAcceptWithdrawal" SkinID="GridViewColor" OnRowDataBound="grdAcceptWithdrawal_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="日時">
                                <HeaderStyle Wrap="false" />
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblUpdateDate" Text='<%# Eval("UPDATE_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Wrap="false" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ﾛｸﾞｲﾝID">
                                <HeaderStyle Wrap="false" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# GetUserLink(Eval("SITE_CD"),Eval("LOGIN_ID"),Eval("WITHDRAWAL_SEQ")) %>'
                                        Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Wrap="false" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ﾊﾝﾄﾞﾙﾈｰﾑ">
                                <HeaderStyle Wrap="false" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkHandleNm" runat="server" NavigateUrl='<%# GetUserLinkDispUsedLog(Eval("SITE_CD"),Eval("LOGIN_ID"),Eval("AD_CD"),Eval("WITHDRAWAL_SEQ")) %>'
										Text='<%# ViCommPrograms.DefHandleName(ViCommPrograms.DefHandleName(Eval("HANDLE_NM"))) %>'>
									</asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Wrap="false" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="登録日">
                                <HeaderStyle Wrap="false" />
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblRegistDate" Text='<%# Eval("REGIST_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Wrap="false" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="最終利用日">
                                <HeaderStyle Wrap="false" />
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblLastPointUsedDate" Text='<%# Eval("LAST_POINT_USED_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Wrap="false" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="TOTAL_RECEIPT_AMT" HeaderText="入金累計額">
                                <HeaderStyle Wrap="false" />
                                <ItemStyle HorizontalAlign="Right" Wrap="false" />
                            </asp:BoundField>
                            <asp:BoundField DataField="TOTAL_RECEIPT_COUNT" HeaderText="入金回数">
                                <HeaderStyle Wrap="false" />
                                <ItemStyle HorizontalAlign="Right" Wrap="false" />
                            </asp:BoundField>
                            <asp:BoundField DataField="TOTAL_PAYMENT_AMT" HeaderText="累計報酬額">
                                <HeaderStyle Wrap="false" />
                                <ItemStyle HorizontalAlign="Right" Wrap="false" />
                            </asp:BoundField>
                            <asp:BoundField DataField="WITHDRAWAL_STATUS_NM" HeaderText="申請状態">
                                <HeaderStyle Wrap="false" />
                                <ItemStyle HorizontalAlign="Left" Wrap="false" />
                            </asp:BoundField>
                            <asp:BoundField DataField="WITHDRAWAL_REASON_NM" HeaderText="退会理由区分">
                                <HeaderStyle Wrap="false" />
                                <ItemStyle HorizontalAlign="Left" Wrap="false" />
                            </asp:BoundField>
                            <asp:BoundField DataField="WITHDRAWAL_REASON_DOC" HeaderText="退会理由">
                                <ItemStyle HorizontalAlign="Left" Wrap="true" />
                            </asp:BoundField>
                            <asp:BoundField DataField="REMARKS" HeaderText="備考">
                                <ItemStyle HorizontalAlign="Left" Wrap="true" />
                            </asp:BoundField>
                            <asp:BoundField DataField="AD_CD" HeaderText="広告ｺｰﾄﾞ">
                                <ItemStyle HorizontalAlign="Left" Wrap="true" />
                            </asp:BoundField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
            <br />
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsWithdrawalStatus" runat="server" SelectMethod="GetList"
        TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="03" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsWithdrawalReasonMan" runat="server" SelectMethod="GetList"
        TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="04" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsWithdrawalReasonCast" runat="server" SelectMethod="GetList"
        TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="05" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsAcceptWithdrawal" runat="server" SelectMethod="GetPageCollection"
        TypeName="Withdrawal" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelected="dsAcceptWithdrawal_Selected"
        OnSelecting="dsAcceptWithdrawal_Selecting">
        <SelectParameters>
            <asp:Parameter Name="pSiteCd" Type="String" />
            <asp:Parameter Name="pLoginId" Type="String" />
            <asp:Parameter Name="pWithdrawalStatus" Type="String" />
            <asp:Parameter Name="pWithdrawalReasonCd" Type="String" />
            <asp:Parameter Name="pWithdrawalReasonDoc" Type="String" />
            <asp:Parameter Name="pRemarks" Type="String" />
            <asp:Parameter Name="pDayFrom" Type="String" />
            <asp:Parameter Name="pDayTo" Type="String" />
            <asp:Parameter Name="pSexCd" Type="String" />
            <asp:Parameter Name="pIntroduceFriendFlag" Type="String" />
            <asp:Parameter Name="pUnConfirmed" Type="String" />
            <asp:Parameter Name="pManualFlag" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="退会申請を登録しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
