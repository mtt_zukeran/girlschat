﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="QuickResponseList.aspx.cs" Inherits="Extension_QuickResponseList" Title="10分以内の返信一覧"
    ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="System" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="10分以内の返信一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[検索条件]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 640px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="180px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                <%= DisplayWordUtil.Replace("出演者ID") %>
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtLoginId" runat="server" Width="80px" MaxLength="12"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle2">
                                表示設定
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstExitStatus" runat="server" DataValueField="SITE_CD" Width="180px">
                                    <asp:ListItem Selected="true" Text="全て" Value=""></asp:ListItem>
                                    <asp:ListItem Text="入室中のみ" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="退室済のみ" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                    <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                        CausesValidation="False" />
                    <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>
                    <%= DisplayWordUtil.Replace("[出演者一覧]") %>
                </legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdQuickResponse.PageIndex + 1 %>
                        of
                        <%= grdQuickResponse.PageCount %>
                    </a>
                </asp:Panel>
                <br />
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="483px">
                    <asp:Label ID="lblMessage" runat="server" text="強制退室が完了しました" ForeColor="red" Visible="false"></asp:Label>
                    <asp:GridView ID="grdQuickResponse" runat="server" AllowPaging="True" AutoGenerateColumns="False" PageSize="30"
                        EnableViewState="true" DataSourceID="dsQuickResponse" AllowSorting="True" SkinID="GridView"
                        DataKeyNames="SITE_CD,USER_SEQ,USER_CHAR_NO,QUICK_RES_SEQ" OnRowDataBound="grdQuickResponse_RowDataBound" OnSorting="grdQuickResponse_Sorting">
                        <Columns>
                            <asp:TemplateField HeaderText="出演者ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblLoginId" runat="server" Text='<%# Eval("LOGIN_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ﾊﾝﾄﾞﾙﾈｰﾑ">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblHandleNm" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}",Eval("LOGIN_ID"),"QuickResponseList.aspx") %>'
                                        Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="QUICK_RES_IN_TIME" DataFormatString="{0:MM/dd HH:mm}"
                                HeaderText="入室" SortExpression="QUICK_RES_IN_TIME">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="QUICK_RES_OUT_TIME" DataFormatString="{0:MM/dd HH:mm}"
                                HeaderText="退室" SortExpression="QUICK_RES_OUT_TIME">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="QUICK_RES_OUT_SCH_TIME" DataFormatString="{0:MM/dd HH:mm}"
                                HeaderText="自動退室" SortExpression="QUICK_RES_OUT_SCH_TIME">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnRegTargetFlag" runat="server" Value='<%# Eval("QUICK_RES_FLAG") %>' />
                                    <asp:LinkButton ID="lnkExit" runat="server" CommandArgument='<%# Container.DataItemIndex %>'
                                        CommandName="EXIT" OnClientClick="" OnCommand="lnkExit_Command" Text='<% #Eval("EXIT_STATUS") %>' Visible='<%# GetExitVisible(Eval("EXIT_STATUS")) %>'></asp:LinkButton>
                                    <asp:Label ID="lblExit" runat="server" Text='<%# Eval("EXIT_STATUS") %>'
                                        Visible='<%# !GetExitVisible(Eval("EXIT_STATUS")) %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
            <br />
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsQuickResponse" runat="server" SelectMethod="GetPageCollection"
        TypeName="QuickResInoutHis" SelectCountMethod="GetPageCount" EnablePaging="True"
        OnSelected="dsQuickResponse_Selected" OnSelecting="dsQuickResponse_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSeekSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:Parameter Name="pLoginId" Type="String" />
            <asp:Parameter Name="pSortExpression" Type="String" />
            <asp:Parameter Name="pSortDirection" Type="String" />
            <asp:ControlParameter ControlID="lstExitStatus" Name="pExitStatus" PropertyName="SelectedValue"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
</asp:Content>
