﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 男性お宝認証
--	Progaram ID		: ManTreasureMainte
--
--  Creation Date	: 2011.07.22
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class Extension_ManTreasureMainte:System.Web.UI.Page {
	private Stream filter;

	protected void Page_Load(object sender,EventArgs e) {
		filter = Response.Filter;
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		lblSiteCd.Text = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		lblUserSeq.Text = iBridUtil.GetStringValue(Request.QueryString["userseq"]);
		lblUserCharNo.Text = iBridUtil.GetStringValue(Request.QueryString["usercharno"]);
		ViewState["SITE_CD"] = lblSiteCd.Text;
		ViewState["USER_SEQ"] = lblUserSeq.Text;
		ViewState["USER_CHAR_NO"] = lblUserCharNo.Text;
		ViewState["PIC_SEQ"] = iBridUtil.GetStringValue(Request.QueryString["picseq"]);
		ViewState["RETURN"] = iBridUtil.GetStringValue(Request.QueryString["return"]);

		using (Cast oCast = new Cast()) {
			string sId = string.Empty;
			oCast.GetValue(lblUserSeq.Text,"LOGIN_ID",ref sId);
			lblLoginId.Text = sId;
		}
		using (CastCharacter oCastCharacter = new CastCharacter()) {
			string sHandleNm = string.Empty;
			oCastCharacter.GetValue(lblSiteCd.Text,lblUserSeq.Text,lblUserCharNo.Text,"HANDLE_NM",ref sHandleNm);
			lblHandleNm.Text = sHandleNm;
		}
		using (GameCharacter oGameCharacter = new GameCharacter()) {
			string sGameHandleNm = string.Empty;
			oGameCharacter.GetValue(lblSiteCd.Text,lblUserSeq.Text,lblUserCharNo.Text,"GAME_HANDLE_NM",ref sGameHandleNm);
			lblGameHandleNm.Text = sGameHandleNm;
		}
		// チェック済みに変更
		using (ManTreasure oManTresure = new ManTreasure()) {
			oManTresure.AccessManTresureMainte(
				ViewState["SITE_CD"].ToString(),
				ViewState["USER_SEQ"].ToString(),
				ViewState["USER_CHAR_NO"].ToString(),
				ViewState["PIC_SEQ"].ToString()
			);
		}
		ClearField();
		DataBind();
		GetData();
	}

	private void ClearField() {
		txtPicDoc.Text = string.Empty;
		txtDisplayDay.Text = string.Empty;
		rdoOK.Checked = true;
		rdoNG.Checked = false;
		// チェック状態
		rdoChecked.Checked = true;
		rdoUnChecked.Checked = false;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		ReturnToCall();
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {

			db.PrepareProcedure("MAN_TREASURE_GET");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,ViewState["SITE_CD"].ToString());
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,ViewState["USER_SEQ"].ToString());
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViewState["USER_CHAR_NO"].ToString());
			db.ProcedureInParm("pCAST_GAME_PIC_SEQ",DbSession.DbType.VARCHAR2,ViewState["PIC_SEQ"].ToString());
			db.ProcedureOutParm("pCAST_GAME_PIC_ATTR_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pDISPLAY_DAY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pPIC_DOC",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pPUBLISH_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pCHECKED_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtDisplayDay.Text = db.GetStringValue("pDISPLAY_DAY");
				txtPicDoc.Text = db.GetStringValue("pPIC_DOC");
				if (db.GetStringValue("pPUBLISH_FLAG").Equals(ViCommConst.FLAG_ON_STR)) {
					rdoOK.Checked = true;
					rdoNG.Checked = false;
				} else {
					rdoOK.Checked = false;
					rdoNG.Checked = true;
				}
				// チェック状態
				if (db.GetStringValue("pCHECKED_FLAG").Equals(ViCommConst.FLAG_ON_STR)) {
					rdoChecked.Checked = true;
					rdoUnChecked.Checked = false;
				} else {
					rdoChecked.Checked = false;
					rdoUnChecked.Checked = true;
				}
				lstManTreasureAttr.DataSourceID = "dsManTreasureAttr";
				lstManTreasureAttr.DataBind();
				lstManTreasureAttr.DataSourceID = string.Empty;
				lstManTreasureAttr.SelectedValue = db.GetStringValue("pCAST_GAME_PIC_ATTR_SEQ");
				using (ManTreasure oManTreasure = new ManTreasure()) {
					string sValue = string.Empty;
					if (oManTreasure.GetValue(ViewState["SITE_CD"].ToString(),ViewState["USER_SEQ"].ToString(),ViewState["USER_CHAR_NO"].ToString(),ViewState["PIC_SEQ"].ToString(),"OBJ_PHOTO_IMG_PATH",ref sValue)) {
						imgPic.ImageUrl = string.Format("../{0}",sValue);
					}
				}
			} else {
				ClearField();
			}
		}
		pnlDtl.Visible = true;
	}

	private void UpdateData(int pDelFlag) {
		int iPublishFlag = ViCommConst.FLAG_OFF;
		int iCheckedFlag = ViCommConst.FLAG_OFF;

		if (pDelFlag == 0) {
			if (rdoOK.Checked) {
				iPublishFlag = ViCommConst.FLAG_ON;
			} else {
				iPublishFlag = ViCommConst.FLAG_OFF;
			}
			// チェック状態
			if (rdoChecked.Checked) {
				iCheckedFlag = ViCommConst.FLAG_ON;
			} else {
				iCheckedFlag = ViCommConst.FLAG_OFF;
			}
		}
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MAN_TREASURE_MAINTE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,ViewState["SITE_CD"].ToString());
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,ViewState["USER_SEQ"].ToString());
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViewState["USER_CHAR_NO"].ToString());
			db.ProcedureInParm("pCAST_GAME_PIC_SEQ",DbSession.DbType.VARCHAR2,ViewState["PIC_SEQ"].ToString());
			db.ProcedureInParm("pCAST_GAME_PIC_ATTR_SEQ",DbSession.DbType.VARCHAR2,lstManTreasureAttr.SelectedValue);
			db.ProcedureInParm("pDISPLAY_DAY",DbSession.DbType.VARCHAR2,txtDisplayDay.Text);
			db.ProcedureInParm("pPIC_DOC",DbSession.DbType.VARCHAR2,txtPicDoc.Text);
			db.ProcedureInParm("pPUBLISH_FLAG",DbSession.DbType.NUMBER,iPublishFlag);
			db.ProcedureInParm("pCHECKED_FLAG",DbSession.DbType.NUMBER,iCheckedFlag);
			db.ProcedureInParm("pROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("pDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		if (iPublishFlag == ViCommConst.FLAG_ON) {
			using (DbSession oDbSession = new DbSession()) {
				oDbSession.PrepareProcedure("ADD_CAST_GAME_POINT");
				oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,ViewState["SITE_CD"].ToString());
				oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,ViewState["USER_SEQ"].ToString());
				oDbSession.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViewState["USER_CHAR_NO"].ToString());
				oDbSession.ProcedureInParm("pOBJ_SEQ",DbSession.DbType.VARCHAR2,ViewState["PIC_SEQ"].ToString());
				oDbSession.ProcedureInParm("pOBJ_TYPE",DbSession.DbType.VARCHAR2,PwViCommConst.CastFile.PIC);
				oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
				oDbSession.ExecuteProcedure();
			}
		}

		bool bTxNotPublicNotice;
		using (ManageCompany oManageCompany = new ManageCompany()) {
			bTxNotPublicNotice = oManageCompany.IsAvailableService(ViCommConst.RELEASE_CAST_OBJ_NON_PUBLIC_INFO,2);
		}

		if (bTxNotPublicNotice && pDelFlag == ViCommConst.FLAG_ON) {
			Server.Transfer(string.Format("../CastAdmin/TxCastObjNonPublicMail.aspx?site={0}&userseq={1}&usercharNo={2}&mailtype={3}&return={4}",ViewState["SITE_CD"].ToString(),ViewState["USER_SEQ"].ToString(),ViewState["USER_CHAR_NO"].ToString(),ViCommConst.MAIL_TP_CAST_GAME_PIC_NP,"ManTreasureList"));
		} else {
			ReturnToCall();
		}
	}

	private void ReturnToCall() {
		if (ViewState["RETURN"].ToString().Equals("ManTreasureList")) {
			Server.Transfer(string.Format("../Extension/ManTreasureList.aspx?site={0}&return=1",ViewState["SITE_CD"].ToString()));
		} else {
			Server.Transfer(string.Format("../Cast/CastView.aspx?loginid={0}",lblLoginId.Text));
		}
	}

	protected void dsManTreasureAttr_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = ViewState["SITE_CD"].ToString();
	}
}
