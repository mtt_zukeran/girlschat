﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="LotteryList.aspx.cs" Inherits="Extension_LotteryList" Title="抽選一覧"
    ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="抽選一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 600px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle2">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    Enabled="false" DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnCreate" Text="追加" CssClass="seekbutton" ValidationGroup="Create"
                    OnClick="btnCreate_Click" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" Visible="false" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlLotteryInfo">
            <fieldset class="fieldset">
                <legend>[抽選情報]</legend>
                <table border="0" style="width: 650px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            抽選ID(SEQ)
                        </td>
                        <td class="tdDataStyle">
                            <asp:Label ID="lblLotteryId" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            抽選名
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtLotteryNm" runat="server" MaxLength="100" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            チケット名
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtTicketNm" runat="server" MaxLength="100" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            ｺﾝﾌﾟﾘｰﾄ時獲得ｱｲﾃﾑ
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstGameItemCategory" runat="server" DataSourceID="dsGameItemCategory"
                                OnDataBound="lst_DataBound" OnSelectedIndexChanged="lstGameItemCategory_IndexChanged"
                                DataTextField="GAME_ITEM_CATEGORY_NM" DataValueField="GAME_ITEM_CATEGORY_TYPE"
                                Width="170px" AutoPostBack="True">
                            </asp:DropDownList>
                            <asp:DropDownList ID="lstItemGetCd" runat="server" DataSourceID="dsItemGet" DataTextField="CODE_NM"
								DataValueField="CODE" OnDataBound="lstItemGetCd_DataBound" OnSelectedIndexChanged="lstItemGetCd_IndexChanged" AutoPostBack="True">
							</asp:DropDownList>
							<asp:DropDownList ID="lstItemPresent" runat="server" OnSelectedIndexChanged="lstItemPresent_IndexChanged" AutoPostBack="True">
								<asp:ListItem Value=""></asp:ListItem>
								<asp:ListItem Value="0">通常</asp:ListItem>
								<asp:ListItem Value="1">ﾌﾟﾚｾﾞﾝﾄ</asp:ListItem>
							</asp:DropDownList>
                            <asp:DropDownList ID="lstGameItem" runat="server" DataSourceID="dsGameItem" DataTextField="GAME_ITEM_NM"
                                OnDataBound="lst_DataBound" DataValueField="GAME_ITEM_SEQ" Width="170px">
                            </asp:DropDownList>
                            <asp:CustomValidator ID="vdcGameItemGet" runat="server" ErrorMessage="" OnServerValidate="vdcGameItemGet_ServerValidate"
                                ValidationGroup="Update"></asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            公開開始日時
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtPublishStartDate" runat="server" MaxLength="16" Width="120px"></asp:TextBox>
                            <ajaxToolkit:MaskedEditExtender ID="mskPublishStartDate" runat="server" MaskType="DateTime"
                                Mask="9999/99/99 99:99:99" UserDateFormat="YearMonthDay" UserTimeFormat="TwentyFourHour"
                                ClearMaskOnLostFocus="true" TargetControlID="txtPublishStartDate">
                            </ajaxToolkit:MaskedEditExtender>
                            <br />
                            <asp:Label ID="lblErrorMessagePublishStart" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            公開終了日時
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtPublishEndDate" runat="server" MaxLength="16" Width="120px"></asp:TextBox>
                            <ajaxToolkit:MaskedEditExtender ID="mskPublishEndDate" runat="server" MaskType="DateTime"
                                Mask="9999/99/99 99:99:99" UserDateFormat="YearMonthDay" UserTimeFormat="TwentyFourHour"
                                ClearMaskOnLostFocus="true" TargetControlID="txtPublishEndDate">
                            </ajaxToolkit:MaskedEditExtender>
                            <br />
                            <asp:Label ID="lblErrorMessagePublishEnd" runat="server" ForeColor="red" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            公開フラグ
                        </td>
                        <td class="tdDataStyle">
                            <asp:CheckBox ID="chkPublishFlag" runat="server" />
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="pnlCommand" runat="server">
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Update"
                        OnClick="btnUpdate_Click" />
                    <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" ValidationGroup="Delete"
                        OnClick="btnDelete_Click" />
                    <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" CausesValidation="False"
                        Visible="true" OnClick="btnCancel_Click" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[抽選一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdLottery.PageIndex + 1 %>
                        of
                        <%= grdLottery.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="600px">
                    <asp:GridView ID="grdLottery" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        PageSize="30" EnableSortingAndPagingCallbacks="false" DataSourceID="dsLottery"
                        SkinID="GridViewColor" Font-Size="small" AllowSorting="true" DataKeyNames="SITE_CD,LOTTERY_SEQ">
                        <Columns>
                            <asp:TemplateField HeaderText="抽選ID">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkLotteryId" runat="server" CommandArgument='<%# Eval("LOTTERY_SEQ") %>'
                                        Text='<%# Eval("LOTTERY_SEQ") %>' OnCommand="lnkLotteryId_Command"></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="抽選名">
                                <ItemTemplate>
                                    <asp:Label ID="lblLotteryNm" runat="server" Text='<%# string.IsNullOrEmpty(Eval("LOTTERY_NM","{0}")) ? "(未設定)" : Eval("LOTTERY_NM") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="チケット名">
                                <ItemTemplate>
                                    <asp:Label ID="lblTicketNm" runat="server" Text='<%# string.IsNullOrEmpty(Eval("TICKET_NM","{0}")) ? "(未設定)" : Eval("TICKET_NM") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="GAME_ITEM_NM" HeaderText="ｺﾝﾌﾟﾘｰﾄ時獲得ｱｲﾃﾑ">
                                <ItemStyle HorizontalAlign="center" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="公開開始日時">
                                <ItemTemplate>
                                    <asp:Label ID="lblPublishStartDate" runat="server" Text='<%# Eval("PUBLISH_START_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="公開終了日時">
                                <ItemTemplate>
                                    <asp:Label ID="lblPublishEndDate" runat="server" Text='<%# Eval("PUBLISH_END_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="公開">
                                <ItemTemplate>
                                    <asp:Label ID="lblPublishFlag" runat="server" Text='<%# GetPublishFlagMark(Eval("PUBLISH_FLAG")) %>'></asp:Label>
                                    <%--<asp:LinkButton ID="lnkSetPublish" runat="server" Text='<%# GetPublishFlagMark(Eval("PUBLISH_FLAG")) %>'
                                        OnCommand="lnkSetPublish_Command" CommandArgument='<%# Container.DataItemIndex %>'>
                                    </asp:LinkButton>--%>
                                    <asp:HiddenField ID="hdnPublishFlag" runat="server" Value='<%# Eval("PUBLISH_FLAG") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkTownList" runat="server" NavigateUrl='<%# string.Format("~/Extension/LotteryGetItemList.aspx?sitecd={0}&lotteryseq={1}&sexcd={2}", Eval("SITE_CD"), Eval("LOTTERY_SEQ"), Eval("SEX_CD"))%>'
                                        Text="アイテム一覧"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsLottery" runat="server" SelectMethod="GetPageCollection"
        ConvertNullToDBNull="true" SortParameterName="" TypeName="Lottery" SelectCountMethod="GetPageCount"
        EnablePaging="True" OnSelected="dsLottery_Selected">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItemCategory" runat="server" SelectMethod="GetList"
        TypeName="GameItemCategory">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsGameItem" runat="server" SelectMethod="GetListByCategoryType"
        TypeName="GameItem" OnSelecting="dsGameItem_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" PropertyName="SelectedValue" Name="pSiteCd"
                Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
            <asp:Parameter Name="pGameItemCategoryType" Type="String" DefaultValue="" />
            <asp:Parameter Name="pGameItemGetCd" Type="String" DefaultValue="" />
            <asp:Parameter Name="pPresentFlag" Type="String" DefaultValue="" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsItemGet" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="A2" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="抽選情報を更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete"
        ConfirmText="抽選情報を削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>
