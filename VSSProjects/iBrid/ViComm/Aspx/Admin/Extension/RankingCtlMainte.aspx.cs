﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ランキングタイマー設定
--	Progaram ID		: RankingCtlMainte
--
--  Creation Date	: 2011.04.05
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ this.Update History ]
/*------------------------------------------------------------------------

  Date        this.Updater    this.Update Explain

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;
using System.Windows.Forms;

public partial class Extension_RankingCtlMainte : System.Web.UI.Page {
	protected static readonly string[] MinuteArray = new string[] { "00", "15", "30", "45" };
	protected static readonly string[] HourArray = new string[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" };
	protected static readonly string[] DayArray = new string[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" };
	protected static readonly string[] MonthArray = new string[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" };
	protected string[] YearArray;

	private string SiteCd {
		get { return iBridUtil.GetStringValue(this.ViewState["SiteCd"]); }
		set { this.ViewState["SiteCd"] = value; }
	}

	private string SummaryType {
		get { return iBridUtil.GetStringValue(this.ViewState["SummaryType"]); }
		set { this.ViewState["SummaryType"] = value; }
	}

	private string RankingCtlSeq {
		get { return iBridUtil.GetStringValue(this.ViewState["RANKING_CTL_SEQ"]); }
		set { this.ViewState["RANKING_CTL_SEQ"] = value; }
	}

	private string RevisionNo {
		get { return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]); }
		set { this.ViewState["RevisionNo"] = value; }
	}

	private string Rowid {
		get { return iBridUtil.GetStringValue(this.ViewState["Rowid"]); }
		set { this.ViewState["Rowid"] = value; }
	}

	private string recCount = "";

	protected void Page_Load(object sender, EventArgs e) {
		if (!IsPostBack) {
			this.SummaryType = this.Request.QueryString["summarytype"];
			this.SiteCd = Request.QueryString["sitecd"];
			InitPage();
			FirstLoad();
		}

		switch (this.SummaryType) {
			case ViCommConst.ExRanking.EX_RANKING_MARKING:
				this.lblPgmTitle.Text = "足あとランキングタイマー設定";
				this.Title = "足あとランキングタイマー設定";
				break;
			case ViCommConst.ExRanking.EX_RANKING_FAVORIT_MAN:
				this.lblPgmTitle.Text = "モテキング設定";
				this.Title = "モテキング設定";
				this.btnRegist.Visible = false;
				break;
			case ViCommConst.ExRanking.EX_RANKING_FAVORIT_WOMAN:
				this.lblPgmTitle.Text = "モテコン設定";
				this.Title = "モテコン設定";
				this.btnRegist.Visible = false;
				break;
		}
	}

	protected void btnSeek_Click(object sender, EventArgs e) {
		this.RankingCtlSeq = string.Empty;
		this.GetData();
	}

	protected void btnRegist_Click(object sender, EventArgs e) {
		InitPage();
		this.pnlMainte.Visible = true;
		this.lstSiteCd.SelectedValue = this.lstSeekSiteCd.SelectedValue;

		this.RankingCtlSeq = string.Empty;
		this.GetData();
	}

	protected void btnUpdate_Click(object sender, EventArgs e) {
		if (IsValid) {
			this.UpdateData(ViCommConst.FLAG_OFF);
		}
	}

	protected void btnDelete_Click(object sender, EventArgs e) {
		if (IsValid) {
			this.UpdateData(ViCommConst.FLAG_ON);
		}
	}

	protected void btnCancel_Click(object sender, EventArgs e) {
		this.InitPage();
	}

	protected void btnListSeek_Click(object sender, EventArgs e) {
		this.lstSiteCd.SelectedValue = this.lstSeekSiteCd.SelectedValue;
		this.GetList();
	}

	protected void dsRankingCtl_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void lnkStartDate_Command(object sender, CommandEventArgs e) {
		int iRowIndex = int.Parse(e.CommandArgument.ToString());
		IDictionary oDataKeys = this.grdRankingCtl.DataKeys[iRowIndex].Values;
		this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(oDataKeys["SITE_CD"]);
		this.RankingCtlSeq = iBridUtil.GetStringValue(oDataKeys["RANKING_CTL_SEQ"]);
		this.GetData();
	}

	private void FirstLoad() {
		this.grdRankingCtl.PageSize = 100;
		this.YearArray = new string[] { DateTime.Today.ToString("yyyy"), DateTime.Today.AddYears(1).ToString("yyyy") };

		this.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		if (!string.IsNullOrEmpty(this.SiteCd)) {
			this.lstSeekSiteCd.SelectedValue = this.SiteCd;
		}
		this.GetList();
	}

	private void InitPage() {
		this.ClearField();
		this.pnlMainte.Visible = false;
	}

	private void ClearField() {
		this.recCount = "0";
		this.txtRemarks.Text = string.Empty;

		this.lstFromYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstFromMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstFromDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstFromHH.SelectedValue = DateTime.Now.ToString("HH");
		this.lstToYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstToMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstToDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstToHH.SelectedValue = DateTime.Now.ToString("HH");

		this.lstFromMI.SelectedIndex = 0;
		this.lstToMI.SelectedIndex = 0;
	}

	protected bool GetLinkButtonVisible() {
		if (ViCommConst.ExRanking.EX_RANKING_MARKING.Equals(this.SummaryType)) {
			return true;
		}

		return false;		
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.grdRankingCtl.PageIndex = 0;
		this.grdRankingCtl.DataSourceID = "dsRankingCtl";
		this.grdRankingCtl.DataBind();
		this.pnlCount.DataBind();
	}

	private void GetData() {
		this.lblErrorMessageFrom.Visible = false;
		this.lblErrorMessageTo.Visible = false;
		this.lstFromYYYY.Enabled = true;
		this.lstFromMM.Enabled = true;
		this.lstFromDD.Enabled = true;
		this.lstFromHH.Enabled = true;
		this.lstFromMI.Enabled = true;
		this.lstToYYYY.Enabled = true;
		this.lstToMM.Enabled = true;
		this.lstToDD.Enabled = true;
		this.lstToHH.Enabled = true;
		this.lstToMI.Enabled = true;

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("RANKING_CTL_GET");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pSUMMARY_TYPE", DbSession.DbType.VARCHAR2, this.SummaryType);
			oDbSession.ProcedureInParm("pRANKING_CTL_SEQ", DbSession.DbType.VARCHAR2, this.RankingCtlSeq);
			oDbSession.ProcedureOutParm("pSUMMARY_START_DATE", DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pSUMMARY_END_DATE", DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pREMARKS", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pROWID", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREVISION_NO", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pRECORD_COUNT", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.RevisionNo = oDbSession.GetStringValue("PREVISION_NO");
			this.Rowid = oDbSession.GetStringValue("PROWID");

			if (int.Parse(oDbSession.GetStringValue("PRECORD_COUNT")) > 0) {
				string sFromDate = string.Format("{0:yyyy/MM/dd HH:mm:ss}", oDbSession.GetDateTimeValue("pSUMMARY_START_DATE"));
				string sToDate = string.Format("{0:yyyy/MM/dd HH:mm:ss}", oDbSession.GetDateTimeValue("pSUMMARY_END_DATE"));
				if (!string.IsNullOrEmpty(sFromDate)) {
					this.lstFromYYYY.SelectedValue = sFromDate.Substring(0, 4);
					this.lstFromMM.SelectedValue = sFromDate.Substring(5, 2);
					this.lstFromDD.SelectedValue = sFromDate.Substring(8, 2);
					this.lstFromHH.SelectedValue = sFromDate.Substring(11, 2);
					if (this.lstFromMI.Items.FindByValue(sFromDate.Substring(14, 2)) != null) {
						this.lstFromMI.SelectedValue = sFromDate.Substring(14, 2);
					}
					
					if (DateTime.Parse(sFromDate) < DateTime.Now) {
						this.lstFromYYYY.Enabled = false;
						this.lstFromMM.Enabled = false;
						this.lstFromDD.Enabled = false;
						this.lstFromHH.Enabled = false;
						this.lstFromMI.Enabled = false;
					}
				}

				if (!string.IsNullOrEmpty(sToDate)) {
					this.lstToYYYY.SelectedValue = sToDate.Substring(0, 4);
					this.lstToMM.SelectedValue = sToDate.Substring(5, 2);
					this.lstToDD.SelectedValue = sToDate.Substring(8, 2);
					this.lstToHH.SelectedValue = sToDate.Substring(11, 2);
					if (this.lstToMI.Items.FindByValue(sToDate.Substring(14, 2)) != null) {
						this.lstToMI.SelectedValue = sToDate.Substring(14, 2);
					}

					if (DateTime.Parse(sToDate) < DateTime.Now) {
						this.lstToYYYY.Enabled = false;
						this.lstToMM.Enabled = false;
						this.lstToDD.Enabled = false;
						this.lstToHH.Enabled = false;
						this.lstToMI.Enabled = false;
					}
				}
				this.txtRemarks.Text = oDbSession.GetStringValue("pREMARKS");
			}
		}

		this.pnlMainte.Visible = true;
		this.pnlDtl.Visible = true;
		this.lstSeekSiteCd.SelectedValue = this.lstSiteCd.SelectedValue;
	}

	private bool CheckDate(out DateTime dtFrom, out DateTime dtTo) {
		dtFrom = new DateTime();
		dtTo = new DateTime();
		this.lblErrorMessageFrom.Visible = false;
		this.lblErrorMessageTo.Visible = false;
		if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:00",
			this.lstFromYYYY.Text, this.lstFromMM.SelectedValue, this.lstFromDD.SelectedValue, this.lstFromHH.SelectedValue, this.lstFromMI.SelectedValue), out dtFrom)) {
			this.lblErrorMessageFrom.Text = "日時を正しく入力してください";
			this.lblErrorMessageFrom.Visible = true;
			return false;
		}
		if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:00",
			this.lstToYYYY.Text, this.lstToMM.SelectedValue, this.lstToDD.SelectedValue, this.lstToHH.SelectedValue, this.lstToMI.SelectedValue), out dtTo)) {
			this.lblErrorMessageTo.Text = "日時を正しく入力してください";
			this.lblErrorMessageTo.Visible = true;
			return false;
		} else {
			dtTo = dtTo.AddSeconds(-1);
		}
		if (dtFrom > dtTo) {
			this.lblErrorMessageTo.Text = "日時の大小関係を正しく入力してください";
			this.lblErrorMessageTo.Visible = true;
			return false;
		}
		using (RankingCtl oRankingCtl = new RankingCtl()) {
			if (oRankingCtl.IsDuplicateDate(this.lstSiteCd.SelectedValue, this.SummaryType, dtFrom, dtTo, this.RankingCtlSeq)) {
				this.lblErrorMessageTo.Text = "日時が他のランキングタイマー設定と重複しています";
				this.lblErrorMessageTo.Visible = true;
				return false;
			}
		}
		return true;
	}

	private void UpdateData(int pDelFlag) {
		DateTime dtFrom = new DateTime();
		DateTime dtTo = new DateTime();
		if (pDelFlag == 0) {
			if (!this.CheckDate(out dtFrom, out dtTo)) {
				return;
			}
		}
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("RANKING_CTL_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pSUMMARY_TYPE", DbSession.DbType.VARCHAR2, this.SummaryType);
			oDbSession.ProcedureInParm("pRANKING_CTL_SEQ", DbSession.DbType.VARCHAR2, this.RankingCtlSeq);
			oDbSession.ProcedureInParm("pSUMMARY_START_DATE", DbSession.DbType.DATE, dtFrom);
			oDbSession.ProcedureInParm("pSUMMARY_END_DATE", DbSession.DbType.DATE, dtTo);
			oDbSession.ProcedureInParm("pREMARKS", DbSession.DbType.VARCHAR2, this.txtRemarks.Text.Trim());
			oDbSession.ProcedureInParm("pROWID", DbSession.DbType.VARCHAR2, this.Rowid);
			oDbSession.ProcedureInParm("pREVISION_NO", DbSession.DbType.NUMBER, decimal.Parse(this.RevisionNo));
			oDbSession.ProcedureInParm("pDEL_FLAG", DbSession.DbType.NUMBER, pDelFlag);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}
		this.InitPage();
		this.GetList();
		this.lstSeekSiteCd.SelectedValue = this.lstSiteCd.SelectedValue;
	}
}
