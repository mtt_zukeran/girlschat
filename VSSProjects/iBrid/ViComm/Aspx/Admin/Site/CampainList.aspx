﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CampainList.aspx.cs" Inherits="Site_CampainList" Title="割引キャンペーン開始設定"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="割引キャンペーン開始設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									サイトコード
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblSiteCd" runat="server" Text=""></asp:Label>
									<asp:Label ID="lblSiteNm" runat="server" Text=""></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									キャンペーンコード
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCampainCd" runat="server" Text="" />
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[ｷｬﾝﾍﾟｰﾝ設定]</legend>
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									ｷｬﾝﾍﾟｰﾝ内容
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCampainDesc" runat="server" Text="" MaxLength="160" Width="350px" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ｷｬﾝﾍﾟｰﾝ適用開始日時
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstFromYYYY" runat="server" Width="60px" />年
									<asp:DropDownList ID="lstFromMM" runat="server" Width="40px" />月
									<asp:DropDownList ID="lstFromDD" runat="server" Width="40px" />日
									<asp:Literal ID="Listeral1" runat="server" Text="&nbsp;" Mode="Encode" />
									<asp:DropDownList ID="lstFromHH" runat="server" Width="40px" />時
									<asp:DropDownList ID="lstFromMI" runat="server" Width="40px" />分
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ｷｬﾝﾍﾟｰﾝ適用終了日時
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstToYYYY" runat="server" Width="60px" />年
									<asp:DropDownList ID="lstToMM" runat="server" Width="40px" />月
									<asp:DropDownList ID="lstToDD" runat="server" Width="40px" />日
									<asp:Literal ID="Literal1" runat="server" Text="&nbsp;" Mode="encode" />
									<asp:DropDownList ID="lstToHH" runat="server" Width="40px" />時
									<asp:DropDownList ID="lstToMI" runat="server" Width="40px" />分
								</td>
							</tr>
						</table>
						<asp:CustomValidator ID="vdcAll" runat="server" ErrorMessage="" OnServerValidate="vdcAll_ServerValidate" ValidationGroup="Detail"></asp:CustomValidator>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" Visible="false" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[キャンペーン設定一覧]</legend>
			<table border="0" style="width: 640px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle2">
						サイトコード
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
						</asp:DropDownList>
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<br />
			<br />
			<asp:GridView ID="grdCampain" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCampain" AllowSorting="True" SkinID="GridView"
				OnRowDataBound="grdCampain_RowDataBound">
				<Columns>
					<asp:TemplateField HeaderText="ｷｬﾝﾍﾟｰﾝｺｰﾄﾞ">
						<ItemTemplate>
							<asp:HyperLink ID="lnkCampainCd" runat="server" Text='<%# Eval("CAMPAIN_CD")%>' NavigateUrl='<%# string.Format("~/Site/CampainList.aspx?sitecd={0}&campaincd={1}",Eval("SITE_CD"),Eval("CAMPAIN_CD"))%>'></asp:HyperLink>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Center" />
					</asp:TemplateField>
					<asp:BoundField DataField="CAMPAIN_DESC" HeaderText="ｷｬﾝﾍﾟｰﾝ内容">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
					<asp:TemplateField HeaderText="適用開始日時">
						<ItemTemplate>
							<asp:Label ID="lblStartDate" runat="server" Text='<%# Eval("APPLICATION_START_DATE", "{0:yyyy/MM/dd HH:mm}") %>'></asp:Label>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Center" />
					</asp:TemplateField>
					<asp:TemplateField HeaderText="適用終了日時">
						<ItemTemplate>
							<asp:Label ID="lblEndDate" runat="server" Text='<%# Eval("APPLICATION_END_DATE", "{0:yyyy/MM/dd HH:mm}") %>'></asp:Label>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Center" />
					</asp:TemplateField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%# GetRecCount() %>
				</a>
				<br />
				<a class="reccount">Current viewing page
					<%=grdCampain.PageIndex + 1%>
					of
					<%=grdCampain.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsCampain" runat="server" SelectMethod="GetPageCollection" TypeName="Campain" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsCampain_Selected">
		<SelectParameters>
			<asp:ControlParameter Name="pSiteCd" ControlID="lstSeekSiteCd" PropertyName="SelectedValue" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
