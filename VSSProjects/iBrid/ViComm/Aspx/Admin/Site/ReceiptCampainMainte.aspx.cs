﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 入金キャンペーン設定
--	Progaram ID		: ReceiptCampainMainte
--
--  Creation Date	: 2011.01.28
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using System.Text;
using System.Drawing;
using System.Collections.Generic;

public partial class Site_ReceiptCampainMainte:System.Web.UI.Page {

	string recCount = string.Empty;

	protected string RecordCount {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RecordCount"]);
		}
		private set {
			this.ViewState["RecordCount"] = value;
		}
	}

	protected List<string> CampainApplicationFlagList {
		get {
			return this.ViewState["CampainApplicationFlagList"] as List<string>;
		}
		private set {
			this.ViewState["CampainApplicationFlagList"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		if (!this.IsPostBack) {
			this.InitPage();
			this.GetList();
			this.SetValue();
			this.pnlKey.Enabled = false;
			this.CampainApplicationFlagList = null;
		}
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			this.UpdateData();
			this.GetList();
			this.RestoreCampainApplicationFlags();
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.InitPage();
		this.pnlKey.Enabled = true;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		this.GetList();
		this.SetValue();
		this.pnlKey.Enabled = false;
		this.CampainApplicationFlagList = null;
	}

	protected void dsReceiptCampain_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void grdReceiptCampain_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			DateTime dtFrom;
			DateTime dtTo;
			if (!DateTime.TryParse(DataBinder.Eval(e.Row.DataItem,"APPLICATION_START_DATE").ToString(),out dtFrom)) {
				dtFrom = DateTime.MinValue;
			}
			if (!DateTime.TryParse(DataBinder.Eval(e.Row.DataItem,"APPLICATION_END_DATE").ToString(),out dtTo)) {
				dtTo = DateTime.MinValue;
			}

			string sFlag = DataBinder.Eval(e.Row.DataItem,"CAMPAIN_APPLICATION_FLAG").ToString();

			if (dtFrom <= DateTime.Now && dtTo >= DateTime.Now && sFlag.Equals("1")) {
				for (int i = 1;i < e.Row.Cells.Count;i++) {
					e.Row.Cells[i].BackColor = Color.LavenderBlush;
				}
			}
		}
	}

	protected void vdcAll_ServerValidate(object source,ServerValidateEventArgs args) {
		if (this.IsValid) {
			DateTime dtFrom;
			DateTime dtTo;
			if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:00:00",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue,this.lstFromHH.SelectedValue),out dtFrom)) {
				dtFrom = DateTime.MinValue;
			}
			if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:00:00",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,this.lstToDD.SelectedValue,this.lstToHH.SelectedValue),out dtTo)) {
				dtTo = DateTime.MinValue;
			}
			if (dtFrom >= dtTo) {
				this.vdcAll.Text = "適用日の大小関係が不正です。";
				args.IsValid = false;
			}
		}
	}

	private void InitPage() {
		this.grdReceiptCampain.DataSourceID = string.Empty;
		this.pnlMainte.Visible = false;

		if (!this.IsPostBack) {
			this.lstSeekSiteCd.DataBind();

			if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals(string.Empty)) {
				this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}

			SysPrograms.SetupFromToDayTime(this.lstFromYYYY,this.lstFromMM,this.lstFromDD,this.lstFromHH,this.lstToYYYY,this.lstToMM,this.lstToDD,this.lstToHH,false);
		}

		this.lstFromYYYY.SelectedIndex = 0;
		this.lstToYYYY.SelectedIndex = 0;
		this.lstFromMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstToMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstFromDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstToDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstFromHH.SelectedIndex = 0;
		this.lstToHH.SelectedIndex = 0;
	}

	protected string GetRecCount() {
		this.RecordCount = this.recCount;
		return this.recCount;
	}

	private void GetList() {
		this.pnlMainte.Visible = true;
		this.grdReceiptCampain.DataSourceID = "dsReceiptCampain";
		this.grdReceiptCampain.PageIndex = 0;
		this.grdReceiptCampain.DataBind();
		this.pnlCount.DataBind();
	}

	private void UpdateData() {
		DateTime dtFrom;
		DateTime dtTo;

		if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:00:00",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue,this.lstFromHH.SelectedValue),out dtFrom)) {
			dtFrom = DateTime.MinValue;
		}
		if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:00:00",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,this.lstToDD.SelectedValue,this.lstToHH.SelectedValue),out dtTo)) {
			dtTo = DateTime.MinValue;
		}

		List<string> oChargeTypeList = new List<string>();
		this.CampainApplicationFlagList = new List<string>();

		foreach (GridViewRow oGridViewRow in this.grdReceiptCampain.Rows) {
			Label oLabel = oGridViewRow.Cells[1].FindControl("lblSettleType") as Label;
			if (oLabel != null) {
				oChargeTypeList.Add(oLabel.Text);
			}

			CheckBox oCheckBox = oGridViewRow.Cells[0].FindControl("chkCampainApplicationFlag") as CheckBox;
			if (oCheckBox != null) {
				if (oCheckBox.Checked) {
					this.CampainApplicationFlagList.Add("1");
				} else {
					this.CampainApplicationFlagList.Add("0");
				}
			}
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("RECEIPT_CAMPAIN_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,lstSeekSiteCd.SelectedValue);
			oDbSession.ProcedureInArrayParm("pCHARGE_TYPE",DbSession.DbType.VARCHAR2,oChargeTypeList.ToArray());
			oDbSession.ProcedureInArrayParm("pCAMPAIN_APPLICATION_FLAG",DbSession.DbType.VARCHAR2,this.CampainApplicationFlagList.ToArray());
			oDbSession.ProcedureInParm("pCHARGE_TYPE_COUNT",DbSession.DbType.NUMBER,this.RecordCount);
			oDbSession.ProcedureInParm("pAPPLICATION_START_DATE",DbSession.DbType.DATE,dtFrom);
			oDbSession.ProcedureInParm("pAPPLICATION_END_DATE",DbSession.DbType.DATE,dtTo);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}
	}

	private void RestoreCampainApplicationFlags() {
		if (this.CampainApplicationFlagList == null) {
			return;
		}

		for (int i = 0;i < this.CampainApplicationFlagList.Count;i++) {
			CheckBox oCheckBox = this.grdReceiptCampain.Rows[i].Cells[0].FindControl("chkCampainApplicationFlag") as CheckBox;
			if (oCheckBox != null) {
				if (this.CampainApplicationFlagList[i].Equals("1")) {
					oCheckBox.Checked = true;
				} else {
					oCheckBox.Checked = false;
				}
			}
		}
	}

	private void SetValue() {
		if (this.grdReceiptCampain.Rows.Count == 0) {
			this.btnUpdate.Enabled = false;
			this.pnlDtl.Visible = false;
			return;
		} else {
			this.btnUpdate.Enabled = true;
			this.pnlDtl.Visible = true;
		}

		GridViewRow oGridViewRow = this.grdReceiptCampain.Rows[0];

		DateTime dtFrom;
		Label oLabelFrom = oGridViewRow.Cells[2].FindControl("lblStartDate") as Label;
		if (oLabelFrom != null) {
			if (DateTime.TryParse(oLabelFrom.Text,out dtFrom)) {
				this.lstFromYYYY.SelectedValue = dtFrom.ToString("yyyy");
				this.lstFromMM.SelectedValue = dtFrom.ToString("MM");
				this.lstFromDD.SelectedValue = dtFrom.ToString("dd");
				this.lstFromHH.SelectedValue = dtFrom.ToString("HH");
			}
		}

		DateTime dtTo;
		Label oLabelTo = oGridViewRow.Cells[3].FindControl("lblEndDate") as Label;
		if (oLabelTo != null) {
			if (DateTime.TryParse(oLabelTo.Text,out dtTo)) {
				this.lstToYYYY.SelectedValue = dtTo.ToString("yyyy");
				this.lstToMM.SelectedValue = dtTo.ToString("MM");
				this.lstToDD.SelectedValue = dtTo.ToString("dd");
				this.lstToHH.SelectedValue = dtTo.ToString("HH");
			}
		}
	}
}
