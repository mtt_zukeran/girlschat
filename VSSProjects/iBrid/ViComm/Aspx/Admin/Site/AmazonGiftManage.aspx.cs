﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: Amazonギフト券インポート＆一覧
--	Progaram ID		: AmazonGiftManage
--
--  Creation Date	: 2017.03.17
--  Creater			: M&TT Zukeran
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Site_AmazonGiftManage:System.Web.UI.Page {
	const string GIFT_CODE_TYPE_AMAZON = "1";

	const string CSV_FILE_NM = "AmazonGiftManage.csv";
	const string CSV_ENCODING = "Shift-JIS";
	private char[] CSV_SEPARATION = {'\t',','};

	// 登録結果
	const string UPLOAD_RESULT_OK = "0";	// 正常終了
	const string UPLOAD_RESULT_DUP = "1";	// 重複登録

	// 合計消費枚数
	private int iTotalCount;

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		// アップロードフォームを非表示
		pnlUpload.Visible = false;
		lblErrorMsg.Text = string.Empty;
		lblCompleteMsg.Visible = false;

		// 残枚数を非表示
		pnlRemainingAll.Visible = false;
		pnlRemainingExpiration.Visible = false;
		grdRemainingExpiration.DataSourceID = string.Empty;

		// 履歴を非表示
		pnlList.Visible = false;
		grdData.DataSourceID = string.Empty;

		if (!IsPostBack) {
			// 日付リストの選択肢を設定
			SysPrograms.SetupYear(lstYYYY);
			SysPrograms.SetMonthList(lstMM);
		}

		// 日付リストの初期値設定
		lstYYYY.SelectedIndex = 0;
		lstMM.SelectedValue = DateTime.Now.ToString("MM");

		// 合計消費枚数を初期化
		this.iTotalCount = 0;
	}

	/// <summary>
	/// 閉じるボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnClose_Click(object sender,EventArgs e) {
		pnlUpload.Visible = false;
	}

	/// <summary>
	/// アップロードボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnUpload_Click(object sender,EventArgs e) {
		if (!IsValid) {
			return;
		}
		string sInstallDir;
		string sResult;

		lblErrorMsg.Text = string.Empty;
		lblCompleteMsg.Visible = false;

		using (Sys oSys = new Sys()) {
			oSys.GetValue("INSTALL_DIR",out sInstallDir);
		}
		string sFileNm = string.Format("{0}\\Text\\{1}",sInstallDir,CSV_FILE_NM);

		uplCsv.SaveAs(sFileNm);

		string sLine;
		int iSeq;
		int iAmount;
		DateTime oDateTime = new DateTime();
		using (StreamReader sr = new StreamReader(sFileNm,Encoding.GetEncoding(CSV_ENCODING))) {
			while ((sLine = sr.ReadLine()) != null) {
				sLine = sLine.Replace("\"","").Replace("'","");
				string[] sValues = sLine.Split(CSV_SEPARATION);

				// ヘッダー行をスキップ
				if (!int.TryParse(sValues[0].ToString(),out iSeq)) {
					continue;
				}

				// 金額の数値変換
				if (!int.TryParse(sValues[2].ToString(),out iAmount)) {
					lblErrorMsg.Text = "<br />金額に数値以外が入力されています。<br />" + sLine;
					break;
				}

				// 有効期限の日付変換
				if (!DateTime.TryParse(sValues[3],out oDateTime)) {
					lblErrorMsg.Text = "<br />有効期限に不正な日付が入力されています。<br />" + sLine;
					break;
				}

				using (DbSession db = new DbSession()) {
					try{
						db.PrepareProcedure("REGIST_GIFT_CODE");
						db.ProcedureInParm("pGIFT_CODE_TYPE",DbSession.DbType.NUMBER,GIFT_CODE_TYPE_AMAZON);
						db.ProcedureInParm("pGIFT_CODE",DbSession.DbType.VARCHAR2,sValues[1]);
						db.ProcedureInParm("pAMOUNT",DbSession.DbType.NUMBER,iAmount);
						db.ProcedureInParm("pEXPIRATION_DATE",DbSession.DbType.VARCHAR2,oDateTime.ToString("yyyy-MM-dd"));
						db.ProcedureInParm("pSERIAL_CODE",DbSession.DbType.VARCHAR2,sValues[4]);
						db.ProcedureInParm("pCREATE_ADMIN_ID",DbSession.DbType.VARCHAR2,SessionObjs.AdminId);
						db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
						db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
						db.ExecuteProcedure();

						// 登録結果
						sResult = db.GetStringValue("pRESULT");
						if (!sResult.Equals(UPLOAD_RESULT_OK)) {
							if (sResult.Equals(UPLOAD_RESULT_DUP)) {
								sResult = "既に登録されています。";
							}
							lblErrorMsg.Text = "<br />" + sResult + "<br />" + sLine;
							break;
						}
					} catch (Exception oException) {
						// DBエラー
						sResult = db.GetStringValue("pRESULT");
						lblErrorMsg.Text = "<br />" + sResult + "<br />" + sLine;
						break;
					}
				}
			}
		}

		// アップロード完了
		if (string.IsNullOrEmpty(lblErrorMsg.Text)) {
			lblCompleteMsg.Visible = true;
		}
	}

	/// <summary>
	/// 検索ボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (IsValid) {
			// 残枚数の表示
			pnlRemaining.Visible = true;
			grdRemainingExpiration.DataSourceID = string.Empty;
			if (rdoRemainingNum.SelectedValue.Equals(GiftCode.DispRemainingNum.Hide)) {
				// 非表示
				pnlRemainingAll.Visible = false;
				pnlRemainingExpiration.Visible = false;
			} else if (rdoRemainingNum.SelectedValue.Equals(GiftCode.DispRemainingNum.Expiration)) {
				// 有効期限別
				pnlRemainingAll.Visible = false;
				pnlRemainingExpiration.Visible = true;

				// 残枚数の取得&表示
				grdRemainingExpiration.DataSourceID = "dsGiftCodeRemaining";
				grdRemainingExpiration.DataBind();
			}
			else {
				// 一括
				pnlRemainingAll.Visible = true;
				pnlRemainingExpiration.Visible = false;

				// 残枚数の取得条件を設定
				GiftCode.SearchCondition oSearchConditions = new GiftCode.SearchCondition();
				oSearchConditions.GiftCodeType = GiftCode.GiftCodeType.Amazon;
				oSearchConditions.DispRemainingNum = rdoRemainingNum.SelectedValue;

				// 残枚数の取得
				GiftCode oGiftCode = new GiftCode();
				DataSet oDataSet = oGiftCode.GetRemainingCnt(oSearchConditions);
				string sCnt = oDataSet.Tables[0].Rows[0][0].ToString();
				lblRemainingCntAll.Text = string.IsNullOrEmpty(sCnt) ? "0" : sCnt;
			}

			// 合計消費枚数を初期化
			this.iTotalCount = 0;

			// 履歴の取得&表示
			pnlList.Visible = true;
			grdData.DataSourceID = "dsGiftCodeList";
			grdData.DataBind();
		}
	}

	/// <summary>
	/// クリアボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	/// <summary>
	/// ギフト券登録ボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnDispUpload_Click(object sender,EventArgs e) {
		pnlUpload.Visible = true;
		lblErrorMsg.Text = string.Empty;
		lblCompleteMsg.Visible = false;
	}

	/// <summary>
	/// データ設定処理
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void grdData_RowDataBound(object sender,GridViewRowEventArgs e) {
		// データが存在しない場合
		if (grdData.Controls.Count <= 0) {
			return;
		}

		// ヘッダー
		if (e.Row.RowType == DataControlRowType.Header) {
		}
			// データ行
		else if (e.Row.RowType == DataControlRowType.DataRow) {
			this.iTotalCount += int.Parse(DataBinder.Eval(e.Row.DataItem,"PAYMENT_CNT").ToString());
		}
			// フッター
		else if (e.Row.RowType == DataControlRowType.Footer) {
			e.Row.Cells[2].Text = this.iTotalCount.ToString();
		}
	}

	/// <summary>
	/// 検索前処理 (残枚数表示)
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void dsGiftCodeRemaining_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		GiftCode.SearchCondition oSearchConditions = new GiftCode.SearchCondition();
		oSearchConditions.DispRemainingNum = rdoRemainingNum.SelectedValue;
		oSearchConditions.GiftCodeType = GiftCode.GiftCodeType.Amazon;
		e.InputParameters["pSearchCondition"] = oSearchConditions;
	}

	/// <summary>
	/// 検索前処理 (履歴)
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void dsGiftCodeList_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		GiftCode.SearchCondition oSearchConditions = new GiftCode.SearchCondition();
		oSearchConditions.Year = lstYYYY.SelectedValue;
		oSearchConditions.Month = lstMM.SelectedValue;
		oSearchConditions.GiftCodeType = GiftCode.GiftCodeType.Amazon;
		e.InputParameters["pSearchCondition"] = oSearchConditions;
	}

	/// <summary>
	/// 一括の残枚数を取得
	/// </summary>
	public void GetAllRemainingCnt() {
		GiftCode.SearchCondition oSearchConditions = new GiftCode.SearchCondition();
		oSearchConditions.GiftCodeType = GiftCode.GiftCodeType.Amazon;
		oSearchConditions.DispRemainingNum = rdoRemainingNum.SelectedValue;

		GiftCode oGiftCode = new GiftCode();
		DataSet oDataSet = oGiftCode.GetRemainingCnt(oSearchConditions);
		string sCnt = oDataSet.Tables[0].Rows[0][0].ToString();
		lblRemainingCntAll.Text = string.IsNullOrEmpty(sCnt) ? "0" : sCnt;
	}
}
