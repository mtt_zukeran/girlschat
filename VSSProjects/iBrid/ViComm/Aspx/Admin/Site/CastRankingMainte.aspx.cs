﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ランキングメンテナンス
--	Progaram ID		: CastRankingMainte
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Site_CastRankingMainte:System.Web.UI.Page {

	private const int MAX_RANK_LIST = 20;

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		ClearField();
		pnlKey.Visible = true;
		pnlKey.Enabled = true;
		pnlDtl.Visible = false;
		pnlInfo.Visible = false;
		lstSiteCd.DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		DataBind();

		this.btnGetInfo.Text = DisplayWordUtil.Replace(this.btnGetInfo.Text);
		DisplayWordUtil.ReplaceValidatorErrorMessage(this.Page.Validators);

	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnGetRanking_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnGetInfo_Click(object sender,EventArgs e) {
	}

	private void ClearField() {
		lstSiteCd.SelectedIndex = 0;
		lstRankingType.SelectedIndex = 0;
	}

	protected void vdcLoginId_ServerValidate(object source,ServerValidateEventArgs args) {
		bool bMultCharFlag = false;
		CustomValidator vdcLoginId = (CustomValidator)source as CustomValidator;


		using (Site oSite = new Site()) {
			if (oSite.GetOne(lstSiteCd.SelectedValue)) {
				if (oSite.multiCharFlag == 1) {
					bMultCharFlag = true;
				}
			}
		}
		if (!bMultCharFlag) {
			args.IsValid = GetCastInfo(args.Value,int.Parse(vdcLoginId.ControlToValidate.Substring(10,2)));
		} else {
			using (CastCharacter oCastCharacter = new CastCharacter()) {
				TextBox txtLoginId = (TextBox)pnlDtl.FindControl(string.Format("txtLoginID{0:D2}",vdcLoginId.ControlToValidate.Substring(10,2))) as TextBox;
				args.IsValid = oCastCharacter.IsExistLoginId(lstSiteCd.SelectedValue,txtLoginId.Text,"");
			}
		}
	}

	protected void Duplicate_ServerValidate(object source,ServerValidateEventArgs args) {
		TextBox txtLoginId;
		TextBox txtUserCharNo;
		Hashtable oTable = new Hashtable();
		int iRankingCreateCount;

		using (SiteManagement oSiteManagement = new SiteManagement()) {
			oSiteManagement.GetOne(lstSiteCd.SelectedValue);
			iRankingCreateCount = oSiteManagement.rankingCreateCount;
		}
		args.IsValid = true;

		for (int i = 0;i < iRankingCreateCount;i++) {
			txtLoginId = (TextBox)pnlDtl.FindControl(string.Format("txtLoginID{0:D2}",i)) as TextBox;
			txtUserCharNo = (TextBox)pnlDtl.FindControl(string.Format("txtUserCharNo{0:D2}",i)) as TextBox;
			if (txtLoginId.Text.Equals("") || txtUserCharNo.Text.Equals("")) {
				continue;
			}
			if (oTable.ContainsKey(txtLoginId.Text + txtUserCharNo.Text)) {
				args.IsValid = false;
				break;
			} else {
				oTable.Add(txtLoginId.Text + txtUserCharNo.Text,null);
			}
		}

	}

	private void GetData() {
		SetRankingCreateCount();
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("RANKING_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PACT_CATEGORY_SEQ",DbSession.DbType.VARCHAR2,lstActCategorySeq.SelectedValue);
			db.ProcedureInParm("PRANKING_TYPE",DbSession.DbType.VARCHAR2,lstRankingType.SelectedValue);
			db.ProcedureOutArrayParm("PLOGIN_ID",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PTALK_MIN",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			db.conn.Close();

			int iRecordCount = MAX_RANK_LIST;
			btnUpdate.Visible = false;
			if (iRecordCount >= db.GetIntValue("PRECORD_COUNT")) {
				iRecordCount = db.GetIntValue("PRECORD_COUNT");
				btnUpdate.Visible = true;
			}
			for (int i = 0;i < iRecordCount;i++) {
				TextBox txtLoginId = (TextBox)pnlDtl.FindControl(string.Format("txtLoginID{0:D2}",i)) as TextBox;
				TextBox txtUserCharNo = (TextBox)pnlDtl.FindControl(string.Format("txtUserCharNo{0:D2}",i)) as TextBox;
				Label lblMin = (Label)pnlDtl.FindControl(string.Format("lblMin{0:D2}",i)) as Label;
				txtLoginId.Text = db.GetArryStringValue("PLOGIN_ID",i);
				txtUserCharNo.Text = db.GetArryStringValue("PUSER_CHAR_NO",i);
				lblMin.Text = db.GetArryStringValue("PTALK_MIN",i) + GetRankingUnit(lstRankingType.SelectedValue);

				GetCastInfo(txtLoginId.Text,i);
			}
			for (int i = iRecordCount;i < MAX_RANK_LIST;i++) {
				ClearCastInfo(i,true);
			}
		}
		lblRankingTitle.Text = GetRankingTypeName(lstRankingType.SelectedValue);
		pnlMainte.Visible = true;
		pnlInfo.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;
		using (Site oSite = new Site()) {
			if (oSite.GetOne(lstSiteCd.SelectedValue)) {
				if (oSite.multiCharFlag == 1) {
					trHeader.Cells[2].Visible = true;
					for (int i = 0;i < MAX_RANK_LIST;i++) {
						HtmlTableRow trRank = (HtmlTableRow)pnlDtl.FindControl(string.Format("trRank{0:D2}",i)) as HtmlTableRow;
						trRank.Cells[2].Visible = true;
					}
				} else {
					trHeader.Cells[2].Visible = false;
					for (int i = 0;i < MAX_RANK_LIST;i++) {
						HtmlTableRow trRank = (HtmlTableRow)pnlDtl.FindControl(string.Format("trRank{0:D2}",i)) as HtmlTableRow;
						TextBox txtUserCharNo = (TextBox)pnlDtl.FindControl(string.Format("txtUserCharNo{0:D2}",i)) as TextBox;
						trRank.Cells[2].Visible = false;
						txtUserCharNo.Text = ViCommConst.MAIN_CHAR_NO;
					}
				}
			}
		}
	}



	private void UpdateData(int pDelFlag) {
		string[] sLoginId = new string[MAX_RANK_LIST];
		string[] sUserCharNo = new string[MAX_RANK_LIST];

		TextBox txtLoginId;
		TextBox txtUserCharNo;

		for (int i = 0;i < MAX_RANK_LIST;i++) {
			txtLoginId = (TextBox)pnlDtl.FindControl(string.Format("txtLoginID{0:D2}",i)) as TextBox;
			txtUserCharNo = (TextBox)pnlDtl.FindControl(string.Format("txtUserCharNo{0:D2}",i)) as TextBox;
			sLoginId[i] = txtLoginId.Text;
			sUserCharNo[i] = txtUserCharNo.Text;
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("RANKING_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PACT_CATEGORY_SEQ",DbSession.DbType.VARCHAR2,lstActCategorySeq.SelectedValue);
			db.ProcedureInParm("PRANKING_TYPE",DbSession.DbType.VARCHAR2,lstRankingType.SelectedValue);
			db.ProcedureInArrayParm("PLOGIN_ID",DbSession.DbType.VARCHAR2,MAX_RANK_LIST,sLoginId);
			db.ProcedureInArrayParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,MAX_RANK_LIST,sUserCharNo);
			db.ProcedureInParm("PRECORD_COUNT",DbSession.DbType.NUMBER,MAX_RANK_LIST);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		InitPage();
	}

	private bool GetCastInfo(string pLoginId,int pPos) {
		TextBox txtUserCharNo = (TextBox)pnlDtl.FindControl(string.Format("txtUserCharNo{0:D2}",pPos)) as TextBox;
		HyperLink lnkCastNm = (HyperLink)pnlDtl.FindControl(string.Format("lnkCastNm{0:D2}",pPos)) as HyperLink;
		Label lblHandleNm = (Label)pnlDtl.FindControl(string.Format("lblHandleNm{0:D2}",pPos)) as Label;
		Image imgPIC = (Image)pnlDtl.FindControl(string.Format("imgPIC{0:D2}",pPos)) as Image;
		Label lblProfileMovie = (Label)pnlDtl.FindControl(string.Format("lblProfileMovie{0:D2}",pPos)) as Label;
		Label lblTalkMovie = (Label)pnlDtl.FindControl(string.Format("lblTalkMovie{0:D2}",pPos)) as Label;
		Label lblMonitorEnable = (Label)pnlDtl.FindControl(string.Format("lblMonitorEnable{0:D2}",pPos)) as Label;
		bool bRet = false;

		using (CastCharacter oCharcter = new CastCharacter()) {
			DataSet ds = oCharcter.GetOne(lstSiteCd.SelectedValue,pLoginId,txtUserCharNo.Text);
			if (ds.Tables[0].Rows.Count > 0) {
				DataRow dr = ds.Tables[0].Rows[0];
				if (dr["ACT_CATEGORY_SEQ"].ToString().Equals(lstActCategorySeq.SelectedValue)) {

					lnkCastNm.Text = dr["CAST_NM"].ToString();
					lnkCastNm.NavigateUrl = string.Format("~/Cast/CastView.aspx?loginid={0}&return=CastRankingMainte.aspx",pLoginId);
					lblHandleNm.Text = dr["HANDLE_NM"].ToString();
					lblProfileMovie.Text = dr["PROFILE_MOVIE_CNT"].ToString() + "件";
					lblTalkMovie.Text = dr["TALK_MOVIE_CNT"].ToString() + "件";
					if (dr["MONITOR_ENABLE_FLAG"].ToString().Equals("1")) {
						lblMonitorEnable.Text = "○";
					} else {
						lblMonitorEnable.Text = "";
					}

					imgPIC.ImageUrl = "../" + dr["SMALL_PHOTO_IMG_PATH"].ToString();
					imgPIC.Visible = true;
					bRet = true;
				}
			} else {
				ClearCastInfo(pPos,false);
			}
		}
		return bRet;
	}

	private void ClearCastInfo(int pPos,bool pClearId) {
		TextBox txtLoginId = (TextBox)pnlDtl.FindControl(string.Format("txtLoginID{0:D2}",pPos)) as TextBox;
		Label lblMin = (Label)pnlDtl.FindControl(string.Format("lblMin{0:D2}",pPos)) as Label;
		HyperLink lnkCastNm = (HyperLink)pnlDtl.FindControl(string.Format("lnkCastNm{0:D2}",pPos)) as HyperLink;
		Label lblHandleNm = (Label)pnlDtl.FindControl(string.Format("lblHandleNm{0:D2}",pPos)) as Label;
		Image imgPIC = (Image)pnlDtl.FindControl(string.Format("imgPIC{0:D2}",pPos)) as Image;
		Label lblProfileMovie = (Label)pnlDtl.FindControl(string.Format("lblProfileMovie{0:D2}",pPos)) as Label;
		Label lblTalkMovie = (Label)pnlDtl.FindControl(string.Format("lblTalkMovie{0:D2}",pPos)) as Label;
		Label lblMonitorEnable = (Label)pnlDtl.FindControl(string.Format("lblMonitorEnable{0:D2}",pPos)) as Label;
		if (pClearId) {
			txtLoginId.Text = "";
		}
		lblMin.Text = "";
		lnkCastNm.Text = "";
		lnkCastNm.NavigateUrl = "";
		lblHandleNm.Text = "";
		lblProfileMovie.Text = "";
		lblTalkMovie.Text = "";
		lblMonitorEnable.Text = "";
		imgPIC.Visible = false;
	}

	protected void dsActCategory_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
	}

	protected void lstSiteCd_SelectedIndexChanged(object sender,EventArgs e) {
		lstActCategorySeq.DataBind();
	}


	private string GetRankingUnit(string type) {
		string ret = string.Empty;
		if (ViCommConst.RANKING_TALK_WEEKLY.Equals(type) || ViCommConst.RANKING_TALK_MONTHLY.Equals(type)) {
			ret = "分";
		}
		if (ViCommConst.RANKING_MAIL_WEEKLY.Equals(type) || ViCommConst.RANKING_MAIL_MONTHLY.Equals(type)) {
			ret = "件";
		}
		return ret;
	}

	private string GetRankingTypeName(string type) {
		string ret = string.Empty;
		if (ViCommConst.RANKING_TALK_WEEKLY.Equals(type) || ViCommConst.RANKING_TALK_MONTHLY.Equals(type)) {
			ret = "会話";
		}
		if (ViCommConst.RANKING_MAIL_WEEKLY.Equals(type) || ViCommConst.RANKING_MAIL_MONTHLY.Equals(type)) {
			ret = "受信ﾒｰﾙ";
		}
		return ret;
	}

	private void SetRankingCreateCount() {
		int iRankingCreateCount = 0;
		using (SiteManagement oSiteManagement = new SiteManagement()) {
			oSiteManagement.GetOne(lstSiteCd.SelectedValue);
			iRankingCreateCount = oSiteManagement.rankingCreateCount;
		}
		for (int i = 0;i < MAX_RANK_LIST;i++) {
			CustomValidator vdcLoginID = (CustomValidator)pnlDtl.FindControl(string.Format("vdcLoginID{0:D2}",i)) as CustomValidator;
			RequiredFieldValidator vdrLoginID = (RequiredFieldValidator)pnlDtl.FindControl(string.Format("vdrLoginID{0:D2}",i)) as RequiredFieldValidator;
			HtmlTableRow trRank = (HtmlTableRow)pnlDtl.FindControl(string.Format("trRank{0:D2}",i)) as HtmlTableRow;
			if (i < iRankingCreateCount) {
				trRank.Visible = true;
				vdcLoginID.Enabled = true;
				vdrLoginID.Enabled = true;
			} else {
				trRank.Visible = false;
				vdcLoginID.Enabled = false;
				vdrLoginID.Enabled = false;
			}
		}
	}
	protected void vdcUserCharNo_ServerValidate(object source,ServerValidateEventArgs args) {
		string sPos;
		CustomValidator vdcUserCharNo = (CustomValidator)source as CustomValidator;
		sPos = vdcUserCharNo.ControlToValidate.Substring(13,2);
		TextBox txtLoginId = (TextBox)pnlDtl.FindControl(string.Format("txtLoginID{0:D2}",sPos));
		args.IsValid = GetCastInfo(txtLoginId.Text,int.Parse(sPos));
	}
}
