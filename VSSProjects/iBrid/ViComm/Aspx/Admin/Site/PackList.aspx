﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PackList.aspx.cs" Inherits="System_PackList" Title="パック設定"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="パック設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 800px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									サイトコード
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="140px">
									</asp:DropDownList>
								</td>
								<td class="tdHeaderStyle2">
									決済種別
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstSettleType" runat="server" DataSourceID="dsSettleType" DataTextField="CODE_NM" DataValueField="CODE" Width="150px">
									</asp:DropDownList>
								</td>
								<td class="tdHeaderStyle2">
									決済金額
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtSalesAmt" runat="server" MaxLength="8" Width="60px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrSalesAmt" runat="server" ErrorMessage="決済金額を入力して下さい。" ControlToValidate="txtSalesAmt" ValidationGroup="Key">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeSalesAmt" runat="server" ErrorMessage="決済金額は数字8桁以内で入力して下さい。" ValidationExpression="^\d{1,8}" ControlToValidate="txtSalesAmt"
										ValidationGroup="Key">*</asp:RegularExpressionValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnSeekCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[パック設定]</legend>
						<table border="0" style="width: 800px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									サイト名
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblSiteNm" runat="server" Text=""></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									決済種別
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblSettleType" runat="server" Text=""></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									決済金額
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblSalesAmt" runat="server"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									備考
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtRemarks" runat="server" MaxLength="256" Width="650px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									初回購入のみ
									
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkFirstTimeFlag" runat="server"></asp:CheckBox>
								</td>
							</tr>
							<tr runat="server" id="trCommoditiesCd">
								<td class="tdHeaderStyle2">
									商品コード
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCommoditiesCd" runat="server" MaxLength="8" Width="60px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrCommoditiesCd" runat="server" ErrorMessage="商品コードを入力して下さい。" ControlToValidate="txtCommoditiesCd" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[設定一覧]</legend>
			<table border="0" style="width: 300px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle2">
						サイトコード
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="120px">
						</asp:DropDownList>
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnRegist" Text="追加" CssClass="seekbutton" CausesValidation="False" OnClick="btnRegist_Click" />
			<br />
			<br />
			<asp:GridView ID="grdPack" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsPack" AllowSorting="True" SkinID="GridViewColor">
				<Columns>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:Label ID="txtSettleType" runat="server" CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("SETTLE_TYPE")) %>' Text='<%# Bind("CODE_NM") %>'>
							</asp:Label>
						</ItemTemplate>
						<HeaderTemplate>
							決済種別
						</HeaderTemplate>
					</asp:TemplateField>
					<asp:TemplateField ItemStyle-HorizontalAlign="Right">
						<ItemTemplate>
							<asp:LinkButton ID="lnkSalesAmt" runat="server" CausesValidation="False" CommandArgument='<%# string.Format("{0}:{1}:{2}",Eval("SITE_CD"),Eval("SETTLE_TYPE"),Eval("SALES_AMT")) %>'
								OnCommand="lnkSalesAmt_Command" Text='<%# AddComma(Eval("SALES_AMT")) %>'>
							</asp:LinkButton>
						</ItemTemplate>
						<HeaderTemplate>
							販売金額
						</HeaderTemplate>
					</asp:TemplateField>
					<asp:BoundField DataField="COMMODITIES_CD" HeaderText="商品ｺｰﾄﾞ"></asp:BoundField>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:Label ID="txtFirstTimeFlg" runat="server"  Text='<%# GetFlagMark(Eval("FIRST_TIME_FLAG")) %>'>
							</asp:Label>
						</ItemTemplate>
						<HeaderTemplate>
							初回購入
						</HeaderTemplate>
					</asp:TemplateField>
					<asp:BoundField DataField="UPDATE_DATE" HeaderText="更新日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br />
				<a class="reccount">Current viewing page
					<%=grdPack.PageIndex + 1%>
					of
					<%=grdPack.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsPack" runat="server" SelectMethod="GetPageCollection" TypeName="Pack" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsPack_Selected" OnSelecting="dsPack_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSettleType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="91" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrSalesAmt" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeSalesAmt" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdrCommoditiesCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtSalesAmt" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" Enabled="true" FilterType="UppercaseLetters,LowercaseLetters,Numbers"
		TargetControlID="txtCommoditiesCd" />
</asp:Content>
