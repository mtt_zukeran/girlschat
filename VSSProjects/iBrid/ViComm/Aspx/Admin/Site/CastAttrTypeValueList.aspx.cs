﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者属性値メンテナンス
--	Progaram ID		: CastAttrTypeValueList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;

public partial class Site_CastAttrTypeValueList:System.Web.UI.Page {
	private string recCount = "";
	private string groupCategoryCd = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {

			string sSiteCd = iBridCommLib.iBridUtil.GetStringValue(Request.QueryString["site"]);
			string sCastAttrTypeSeq = iBridCommLib.iBridUtil.GetStringValue(Request.QueryString["castattrtypeseq"]);
			string sCastAttrSeq = iBridCommLib.iBridUtil.GetStringValue(Request.QueryString["castattrseq"]);
			InitPage();

			if ((!sSiteCd.Equals("")) && (!sCastAttrTypeSeq.Equals("")) && (!sCastAttrSeq.Equals(""))) {
				lstSiteCd.SelectedValue = sSiteCd;
				lstSeekSiteCd.SelectedValue = sSiteCd;

				lstCastAttrTypeSeq.DataBind();
				lstCastAttrTypeSeq.SelectedValue = sCastAttrTypeSeq;
				lblCastAttrSeq.Text = sCastAttrSeq;

				GetList();
				GetData();
			}
		}
	}

	protected void dsCastAttrTypeValue_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		grdValue.PageSize = 999;
		lstSiteCd.SelectedIndex = 0;
		lblCastAttrSeq.Text = "";
		ClearField();
		pnlMainte.Visible = false;
		lstSeekSiteCd.DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		DataBind();

		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);

	}

	private void ClearField() {
		groupCategoryCd = "";
		txtCastAttrNm.Text = "";
		txtPriority.Text = "";
		recCount = "0";
		txtItemCd.Text = "";
		chkOmitSeekContionFlag.Checked = true;
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		lstSiteCd.SelectedIndex = lstSeekSiteCd.SelectedIndex;
		lstCastAttrTypeSeq.DataBind();
		lblCastAttrSeq.Text = "0";

		GetData();
		GetList();
		pnlKey.Enabled = true;
		lstSiteCd.Enabled = false;
	}
	
	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		} else {
			GetList();
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		} else {
			GetList();
		}
	}

	protected void lstCastAttrTypeSeq_SelectedIndexChanged(object sender,EventArgs e) {
		SetGroupCd();
		GetList();
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void grdValue_DataBound(object sender,EventArgs e) {
		JoinCells(grdValue,0);
	}

	private void GetList() {
		grdValue.PageIndex = 0;
		grdValue.DataBind();
		pnlCount.DataBind();
	}


	private void GetData() {
		string sGroupCd = "";
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_ATTR_TYPE_VALUE_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PCAST_ATTR_TYPE_SEQ",DbSession.DbType.NUMBER,lstCastAttrTypeSeq.SelectedValue);
			db.ProcedureInParm("PCAST_ATTR_SEQ",DbSession.DbType.NUMBER,lblCastAttrSeq.Text);
			db.ProcedureOutParm("PCAST_ATTR_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("POMIT_SEEK_CONTION_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PPRIORITY",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PGROUPING_CATEGORY_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PGROUPING_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PITEM_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");
			groupCategoryCd = db.GetStringValue("PGROUPING_CATEGORY_CD");
			sGroupCd = db.GetStringValue("PGROUPING_CD");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				lstSiteCd.SelectedValue = db.GetStringValue("PSITE_CD");
				lstCastAttrTypeSeq.SelectedValue = db.GetStringValue("PCAST_ATTR_TYPE_SEQ");
				txtCastAttrNm.Text = db.GetStringValue("PCAST_ATTR_NM");
				txtPriority.Text = db.GetStringValue("PPRIORITY");
				txtItemCd.Text = db.GetStringValue("PITEM_CD");
				chkOmitSeekContionFlag.Checked = db.GetStringValue("POMIT_SEEK_CONTION_FLAG").Equals("0");
			} else {
				ClearField();
			}
		}

		SetGroupCd();

		if (!groupCategoryCd.Equals("")) {
			lstGroupCd.DataBind();
			if ((lstGroupCd.Items.Count > 0) && (!sGroupCd.Equals(""))) {
				lstGroupCd.SelectedValue = sGroupCd;
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;

	}

	private void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_ATTR_TYPE_VALUE_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PCAST_ATTR_TYPE_SEQ",DbSession.DbType.NUMBER,lstCastAttrTypeSeq.SelectedValue);
			db.ProcedureInParm("PCAST_ATTR_SEQ",DbSession.DbType.NUMBER,lblCastAttrSeq.Text);
			db.ProcedureInParm("PCAST_ATTR_NM",DbSession.DbType.VARCHAR2,txtCastAttrNm.Text);
			db.ProcedureInParm("PPRIORITY",DbSession.DbType.NUMBER,txtPriority.Text);
			db.ProcedureInParm("PGROUPING_CD",DbSession.DbType.VARCHAR2,lstGroupCd.SelectedValue);
			db.ProcedureInParm("PITEM_CD",DbSession.DbType.VARCHAR2,txtItemCd.Text);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("POMIT_SEEK_CONTION_FLAG",DbSession.DbType.NUMBER,!chkOmitSeekContionFlag.Checked);
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		int iIdx = lstSiteCd.SelectedIndex;
		InitPage();
		lstSeekSiteCd.SelectedIndex = iIdx;
		GetList();
	}

	private void JoinCells(GridView pGrd,int pCol) {
		int iRow = pGrd.Rows.Count;
		int iBaseidx = 0;

		while (iBaseidx < iRow) {
			int iNextIdx = iBaseidx + 1;
			TableCell celBase = pGrd.Rows[iBaseidx].Cells[pCol];

			while (iNextIdx < iRow) {

				TableCell celNext = pGrd.Rows[iNextIdx].Cells[pCol];

				if (GetText(celBase).Equals(GetText(celNext))) {
					if (celBase.RowSpan == 0) {
						celBase.RowSpan = 2;
					} else {
						celBase.RowSpan++;
					}
					pGrd.Rows[iNextIdx].Cells.Remove(celNext);
					iNextIdx++;
				} else {
					break;
				}
			}
			iBaseidx = iNextIdx;
		}
	}

	private string GetText(TableCell tc) {
		Label dblc =
		  (Label)tc.Controls[1];
		return dblc.Text;
	}

	protected void dsCastAttrType_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
	}

	protected void dsCastAttrTypeValue_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
	}

	protected void dsGroupCd_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = groupCategoryCd;
	}

	private void SetGroupCd() {
		lstGroupCd.Items.Clear();
		using (CastAttrType oCastAttrType = new CastAttrType()) {
			if (oCastAttrType.GetOne(lstSiteCd.SelectedValue,lstCastAttrTypeSeq.SelectedValue)) {
				groupCategoryCd = oCastAttrType.groupingCategoryCd;
				lstGroupCd.DataBind();
			}
		}
	}
}
