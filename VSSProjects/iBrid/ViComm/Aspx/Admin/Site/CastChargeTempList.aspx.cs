﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: キャスト課金一時保存メンテナンス
--	Progaram ID		: CastChargeTempList
--
--  Creation Date	: 2012.05.30
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------
  Date        Updater    Update Explain
-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Site_CastChargeTempList : System.Web.UI.Page {
	private string recCount = "";
	private int MAX_CHARGE_COUNT = 35;
	private string actCategorySeq = "";

	private string CastChargeTempCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["CastChargeTempCd"]);
		}
		set {
			this.ViewState["CastChargeTempCd"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			FirstLoad();
			InitPage();

			if (iBridUtil.GetStringValue(Request.QueryString["edit"]).Equals(ViCommConst.FLAG_ON_STR)) {
				GetData();
			}
		}
	}

	private void FirstLoad() {
		grdCastChargeTemp.PageSize = 999;
		lblSiteCd.Text = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		lblSiteNm.Text = iBridUtil.GetStringValue(Request.QueryString["sitenm"]);
		lblActCategoryNm.Text = iBridUtil.GetStringValue(Request.QueryString["actcategorynm"]);
		actCategorySeq = iBridUtil.GetStringValue(Request.QueryString["actcategoryseq"]);
		ViewState["actCategorySeq"] = actCategorySeq;
		this.CastChargeTempCd = iBridUtil.GetStringValue(Request.QueryString["tempcd"]);
		//lblCastChargeTempNm.Text = iBridUtil.GetStringValue(Request.QueryString["tempnm"]);
		lstSeekCastChargeTempCd.SelectedValue = this.CastChargeTempCd;
		DataBind();
	}

	private void InitPage() {
		pnlDtl.Visible = false;
		ClearField();
		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);
	}

	private void ClearField() {
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
		GetList();
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		pnlDtl.Visible = false;
		GetList();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	private void GetList() {
		pnlKey.Enabled = true;
		grdCastChargeTemp.PageIndex = 0;
		grdCastChargeTemp.DataBind();
		pnlCount.DataBind();
	}

	protected void dsCastChargeTemp_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsCastChargeTemp_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
		e.InputParameters[1] = this.lstSeekCastChargeTempCd.SelectedValue;
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_CHARGE_TEMP_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lblSiteCd.Text);
			db.ProcedureInParm("PACT_CATEGORY_SEQ",DbSession.DbType.VARCHAR2,actCategorySeq);
			db.ProcedureInParm("PCAST_CHARGE_TEMP_CD",DbSession.DbType.VARCHAR2,this.CastChargeTempCd);
			db.ProcedureOutArrayParm("PCHARGE_TYPE",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutArrayParm("PCHARGE_TYPE_NM",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutArrayParm("PCHARGE_POINT_NORMAL",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutArrayParm("PCHARGE_POINT_NO_RECEIPT",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutArrayParm("PCHARGE_POINT_NORMAL2",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutArrayParm("PCHARGE_POINT_NO_RECEIPT2",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutArrayParm("PCHARGE_POINT_NORMAL3",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutArrayParm("PCHARGE_POINT_NO_RECEIPT3",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutArrayParm("PCHARGE_POINT_NORMAL4",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutArrayParm("PCHARGE_POINT_NO_RECEIPT4",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutArrayParm("PCHARGE_POINT_NORMAL5",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutArrayParm("PCHARGE_POINT_NO_RECEIPT5",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutArrayParm("PCHARGE_UNIT_SEC",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutArrayParm("PFREE_DIAL_FEE",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutArrayParm("pCAST_USE_TALK_APP_ADD_POINT",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutArrayParm("pMAN_USE_TALK_APP_ADD_POINT",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutArrayParm("PCHARGE_ROWID",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutArrayParm("PCHARGE_REVISION_NO",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutParm("PCHARGE_RECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["CHARGE_RECORD_COUNT"] = db.GetIntValue("PCHARGE_RECORD_COUNT");

			for (int i = 0;i < db.GetIntValue("PCHARGE_RECORD_COUNT");i++) {
				Label lblChargeTypeNm = (Label)plcHolder.FindControl(string.Format("lblChargeTypeNm{0}",i)) as Label;
				TextBox txtChargeUnitSec = (TextBox)plcHolder.FindControl(string.Format("txtChargeUnitSec{0}",i)) as TextBox;
				TextBox txtChargePointNormal = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNormal{0}",i)) as TextBox;
				TextBox txtChargePointNoReceipt = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNoReceipt{0}",i)) as TextBox;
				TextBox txtChargePointNormalB = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNormalB{0}",i)) as TextBox;
				TextBox txtChargePointNoReceiptB = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNoReceiptB{0}",i)) as TextBox;
				TextBox txtChargePointNormalC = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNormalC{0}",i)) as TextBox;
				TextBox txtChargePointNoReceiptC = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNoReceiptC{0}",i)) as TextBox;
				TextBox txtChargePointNormalD = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNormalD{0}",i)) as TextBox;
				TextBox txtChargePointNoReceiptD = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNoReceiptD{0}",i)) as TextBox;
				TextBox txtChargePointNormalE = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNormalE{0}",i)) as TextBox;
				TextBox txtChargePointNoReceiptE = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNoReceiptE{0}",i)) as TextBox;
				TextBox txtFreeDialFee = (TextBox)plcHolder.FindControl(string.Format("txtFreeDialFee{0}",i)) as TextBox;
				TextBox txtCastUseTalkAppAddPoint = (TextBox)plcHolder.FindControl(string.Format("txtCastUseTalkAppAddPoint{0}",i)) as TextBox;
				TextBox txtManUseTalkAppAddPoint = (TextBox)plcHolder.FindControl(string.Format("txtManUseTalkAppAddPoint{0}",i)) as TextBox;

				RequiredFieldValidator vdrFreeDialFee = (RequiredFieldValidator)plcHolder.FindControl(string.Format("vdrFreeDialFee{0}",i)) as RequiredFieldValidator;
				RegularExpressionValidator vdeFreeDialFee = (RegularExpressionValidator)plcHolder.FindControl(string.Format("vdeFreeDialFee{0}",i)) as RegularExpressionValidator;

				RequiredFieldValidator vdrCastUseTalkAppAddPoint = (RequiredFieldValidator)plcHolder.FindControl(string.Format("vdrCastUseTalkAppAddPoint{0}",i)) as RequiredFieldValidator;
				RegularExpressionValidator vdeCastUseTalkAppAddPoint = (RegularExpressionValidator)plcHolder.FindControl(string.Format("vdeCastUseTalkAppAddPoint{0}",i)) as RegularExpressionValidator;

				RequiredFieldValidator vdrManUseTalkAppAddPoint = (RequiredFieldValidator)plcHolder.FindControl(string.Format("vdrManUseTalkAppAddPoint{0}",i)) as RequiredFieldValidator;
				RegularExpressionValidator vdeManUseTalkAppAddPoint = (RegularExpressionValidator)plcHolder.FindControl(string.Format("vdeManUseTalkAppAddPoint{0}",i)) as RegularExpressionValidator;

				lblChargeTypeNm.Text = db.GetArryStringValue("PCHARGE_TYPE_NM",i);
				txtChargeUnitSec.Text = db.GetArryStringValue("PCHARGE_UNIT_SEC",i);
				txtChargePointNormal.Text = db.GetArryStringValue("PCHARGE_POINT_NORMAL",i);
				txtChargePointNoReceipt.Text = db.GetArryStringValue("PCHARGE_POINT_NO_RECEIPT",i);
				txtChargePointNormalB.Text = db.GetArryStringValue("PCHARGE_POINT_NORMAL2",i);
				txtChargePointNoReceiptB.Text = db.GetArryStringValue("PCHARGE_POINT_NO_RECEIPT2",i);
				txtChargePointNormalC.Text = db.GetArryStringValue("PCHARGE_POINT_NORMAL3",i);
				txtChargePointNoReceiptC.Text = db.GetArryStringValue("PCHARGE_POINT_NO_RECEIPT3",i);
				txtChargePointNormalD.Text = db.GetArryStringValue("PCHARGE_POINT_NORMAL4",i);
				txtChargePointNoReceiptD.Text = db.GetArryStringValue("PCHARGE_POINT_NO_RECEIPT4",i);
				txtChargePointNormalE.Text = db.GetArryStringValue("PCHARGE_POINT_NORMAL5",i);
				txtChargePointNoReceiptE.Text = db.GetArryStringValue("PCHARGE_POINT_NO_RECEIPT5",i);
				txtFreeDialFee.Text = db.GetArryStringValue("PFREE_DIAL_FEE",i);
				txtCastUseTalkAppAddPoint.Text = db.GetArryStringValue("pCAST_USE_TALK_APP_ADD_POINT",i);
				txtManUseTalkAppAddPoint.Text = db.GetArryStringValue("pMAN_USE_TALK_APP_ADD_POINT",i);

				if (db.GetArryStringValue("PCHARGE_TYPE",i).CompareTo(ViCommConst.CHARGE_CAST_ACTION) < 0) {
					txtFreeDialFee.Visible = false;
					vdrFreeDialFee.Enabled = false;
					vdeFreeDialFee.Enabled = false;
				}
				switch (db.GetArryStringValue("PCHARGE_TYPE",i)) {
					case ViCommConst.CHARGE_TALK_PUBLIC:
					case ViCommConst.CHARGE_TALK_WSHOT:
					case ViCommConst.CHARGE_TALK_VOICE_WSHOT:
					case ViCommConst.CHARGE_TALK_VOICE_PUBLIC:
					case ViCommConst.CHARGE_CAST_TALK_PUBLIC:
					case ViCommConst.CHARGE_CAST_TALK_WSHOT:
					case ViCommConst.CHARGE_CAST_TALK_VOICE_WSHOT:
					case ViCommConst.CHARGE_CAST_TALK_VOICE_PUBLIC:
						txtCastUseTalkAppAddPoint.Visible = true;
						txtManUseTalkAppAddPoint.Visible = true;
						vdrCastUseTalkAppAddPoint.Enabled = true;
						vdeCastUseTalkAppAddPoint.Enabled = true;
						vdrManUseTalkAppAddPoint.Enabled = true;
						vdeManUseTalkAppAddPoint.Enabled = true;
						break;
					default:
						txtCastUseTalkAppAddPoint.Visible = false;
						txtManUseTalkAppAddPoint.Visible = false;
						vdrCastUseTalkAppAddPoint.Enabled = false;
						vdeCastUseTalkAppAddPoint.Enabled = false;
						vdrManUseTalkAppAddPoint.Enabled = false;
						vdeManUseTalkAppAddPoint.Enabled = false;
						break;
				}

				ViewState["CHARGE_ROWID" + i.ToString()] = db.GetArryStringValue("PCHARGE_ROWID",i);
				ViewState["CHARGE_REVISION_NO" + i.ToString()] = db.GetArryStringValue("PCHARGE_REVISION_NO",i);
				ViewState["CHARGE_TYPE" + i.ToString()] = db.GetArryStringValue("PCHARGE_TYPE",i);

			}
			for (int i = db.GetIntValue("PCHARGE_RECORD_COUNT");i < MAX_CHARGE_COUNT;i++) {
				Label lblChargeTypeNm = (Label)plcHolder.FindControl(string.Format("lblChargeTypeNm{0}",i)) as Label;
				TextBox txtChargeUnitSec = (TextBox)plcHolder.FindControl(string.Format("txtChargeUnitSec{0}",i)) as TextBox;
				TextBox txtChargePointNormal = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNormal{0}",i)) as TextBox;
				TextBox txtChargePointNoReceipt = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNoReceipt{0}",i)) as TextBox;
				TextBox txtChargePointNormalB = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNormalB{0}",i)) as TextBox;
				TextBox txtChargePointNoReceiptB = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNoReceiptB{0}",i)) as TextBox;
				TextBox txtChargePointNormalC = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNormalC{0}",i)) as TextBox;
				TextBox txtChargePointNoReceiptC = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNoReceiptC{0}",i)) as TextBox;
				TextBox txtChargePointNormalD = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNormalD{0}",i)) as TextBox;
				TextBox txtChargePointNoReceiptD = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNoReceiptD{0}",i)) as TextBox;
				TextBox txtChargePointNormalE = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNormalE{0}",i)) as TextBox;
				TextBox txtChargePointNoReceiptE = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNoReceiptE{0}",i)) as TextBox;
				TextBox txtFreeDialFee = (TextBox)plcHolder.FindControl(string.Format("txtFreeDialFee{0}",i)) as TextBox;
				TextBox txtCastUseTalkAppAddPoint = (TextBox)plcHolder.FindControl(string.Format("txtCastUseTalkAppAddPoint{0}",i)) as TextBox;
				TextBox txtManUseTalkAppAddPoint = (TextBox)plcHolder.FindControl(string.Format("txtManUseTalkAppAddPoint{0}",i)) as TextBox;

				RequiredFieldValidator vdrChargeUnitSec = (RequiredFieldValidator)plcHolder.FindControl(string.Format("vdrChargeUnitSec{0}",i)) as RequiredFieldValidator;
				RegularExpressionValidator vdeChargeUnitSec = (RegularExpressionValidator)plcHolder.FindControl(string.Format("vdeChargeUnitSec{0}",i)) as RegularExpressionValidator;

				RequiredFieldValidator vdrChargePointNormal = (RequiredFieldValidator)plcHolder.FindControl(string.Format("vdrChargePointNormal{0}",i)) as RequiredFieldValidator;
				RegularExpressionValidator vdeChargePointNormal = (RegularExpressionValidator)plcHolder.FindControl(string.Format("vdeChargePointNormal{0}",i)) as RegularExpressionValidator;

				RequiredFieldValidator vdrChargePointNoReceipt = (RequiredFieldValidator)plcHolder.FindControl(string.Format("vdrChargePointNoReceipt{0}",i)) as RequiredFieldValidator;
				RegularExpressionValidator vdeChargePointNoReceipt = (RegularExpressionValidator)plcHolder.FindControl(string.Format("vdeChargePointNoReceipt{0}",i)) as RegularExpressionValidator;

				RequiredFieldValidator vdrChargePointNormalB = (RequiredFieldValidator)plcHolder.FindControl(string.Format("vdrChargePointNormalB{0}",i)) as RequiredFieldValidator;
				RegularExpressionValidator vdeChargePointNormalB = (RegularExpressionValidator)plcHolder.FindControl(string.Format("vdeChargePointNormalB{0}",i)) as RegularExpressionValidator;

				RequiredFieldValidator vdrChargePointNoReceiptB = (RequiredFieldValidator)plcHolder.FindControl(string.Format("vdrChargePointNoReceiptB{0}",i)) as RequiredFieldValidator;
				RegularExpressionValidator vdeChargePointNoReceiptB = (RegularExpressionValidator)plcHolder.FindControl(string.Format("vdeChargePointNoReceiptB{0}",i)) as RegularExpressionValidator;

				RequiredFieldValidator vdrChargePointNormalC = (RequiredFieldValidator)plcHolder.FindControl(string.Format("vdrChargePointNormalC{0}",i)) as RequiredFieldValidator;
				RegularExpressionValidator vdeChargePointNormalC = (RegularExpressionValidator)plcHolder.FindControl(string.Format("vdeChargePointNormalC{0}",i)) as RegularExpressionValidator;

				RequiredFieldValidator vdrChargePointNoReceiptC = (RequiredFieldValidator)plcHolder.FindControl(string.Format("vdrChargePointNoReceiptC{0}",i)) as RequiredFieldValidator;
				RegularExpressionValidator vdeChargePointNoReceiptC = (RegularExpressionValidator)plcHolder.FindControl(string.Format("vdeChargePointNoReceiptC{0}",i)) as RegularExpressionValidator;

				RequiredFieldValidator vdrChargePointNormalD = (RequiredFieldValidator)plcHolder.FindControl(string.Format("vdrChargePointNormalD{0}",i)) as RequiredFieldValidator;
				RegularExpressionValidator vdeChargePointNormalD = (RegularExpressionValidator)plcHolder.FindControl(string.Format("vdeChargePointNormalD{0}",i)) as RegularExpressionValidator;

				RequiredFieldValidator vdrChargePointNoReceiptD = (RequiredFieldValidator)plcHolder.FindControl(string.Format("vdrChargePointNoReceiptD{0}",i)) as RequiredFieldValidator;
				RegularExpressionValidator vdeChargePointNoReceiptD = (RegularExpressionValidator)plcHolder.FindControl(string.Format("vdeChargePointNoReceiptD{0}",i)) as RegularExpressionValidator;

				RequiredFieldValidator vdrChargePointNormalE = (RequiredFieldValidator)plcHolder.FindControl(string.Format("vdrChargePointNormalE{0}",i)) as RequiredFieldValidator;
				RegularExpressionValidator vdeChargePointNormalE = (RegularExpressionValidator)plcHolder.FindControl(string.Format("vdeChargePointNormalE{0}",i)) as RegularExpressionValidator;

				RequiredFieldValidator vdrChargePointNoReceiptE = (RequiredFieldValidator)plcHolder.FindControl(string.Format("vdrChargePointNoReceiptE{0}",i)) as RequiredFieldValidator;
				RegularExpressionValidator vdeChargePointNoReceiptE = (RegularExpressionValidator)plcHolder.FindControl(string.Format("vdeChargePointNoReceiptE{0}",i)) as RegularExpressionValidator;

				RequiredFieldValidator vdrFreeDialFee = (RequiredFieldValidator)plcHolder.FindControl(string.Format("vdrFreeDialFee{0}",i)) as RequiredFieldValidator;
				RegularExpressionValidator vdeFreeDialFee = (RegularExpressionValidator)plcHolder.FindControl(string.Format("vdeFreeDialFee{0}",i)) as RegularExpressionValidator;

				RequiredFieldValidator vdrCastUseTalkAppAddPoint = (RequiredFieldValidator)plcHolder.FindControl(string.Format("vdrCastUseTalkAppAddPoint{0}",i)) as RequiredFieldValidator;
				RegularExpressionValidator vdeCastUseTalkAppAddPoint = (RegularExpressionValidator)plcHolder.FindControl(string.Format("vdeCastUseTalkAppAddPoint{0}",i)) as RegularExpressionValidator;

				RequiredFieldValidator vdrManUseTalkAppAddPoint = (RequiredFieldValidator)plcHolder.FindControl(string.Format("vdrManUseTalkAppAddPoint{0}",i)) as RequiredFieldValidator;
				RegularExpressionValidator vdeManUseTalkAppAddPoint = (RegularExpressionValidator)plcHolder.FindControl(string.Format("vdeManUseTalkAppAddPoint{0}",i)) as RegularExpressionValidator;

				lblChargeTypeNm.Visible = false;
				txtChargeUnitSec.Visible = false;
				txtChargePointNormal.Visible = false;
				txtChargePointNoReceipt.Visible = false;
				txtChargePointNormalB.Visible = false;
				txtChargePointNoReceiptB.Visible = false;
				txtChargePointNormalC.Visible = false;
				txtChargePointNoReceiptC.Visible = false;
				txtChargePointNormalD.Visible = false;
				txtChargePointNoReceiptD.Visible = false;
				txtChargePointNormalE.Visible = false;
				txtChargePointNoReceiptE.Visible = false;
				txtFreeDialFee.Visible = false;
				txtCastUseTalkAppAddPoint.Visible = false;
				txtManUseTalkAppAddPoint.Visible = false;

				vdrChargeUnitSec.Enabled = false;
				vdeChargeUnitSec.Enabled = false;
				vdrChargePointNormal.Enabled = false;
				vdeChargePointNormal.Enabled = false;
				vdrChargePointNoReceipt.Enabled = false;
				vdeChargePointNoReceipt.Enabled = false;
				vdrChargePointNormalB.Enabled = false;
				vdeChargePointNormalB.Enabled = false;
				vdrChargePointNoReceiptB.Enabled = false;
				vdeChargePointNoReceiptB.Enabled = false;
				vdrChargePointNormalC.Enabled = false;
				vdeChargePointNormalC.Enabled = false;
				vdrChargePointNoReceiptC.Enabled = false;
				vdeChargePointNoReceiptC.Enabled = false;
				vdrChargePointNormalD.Enabled = false;
				vdeChargePointNormalD.Enabled = false;
				vdrChargePointNoReceiptD.Enabled = false;
				vdeChargePointNoReceiptD.Enabled = false;
				vdrChargePointNormalE.Enabled = false;
				vdeChargePointNormalE.Enabled = false;
				vdrChargePointNoReceiptE.Enabled = false;
				vdeChargePointNoReceiptE.Enabled = false;
				vdrFreeDialFee.Enabled = false;
				vdeFreeDialFee.Enabled = false;
				vdrCastUseTalkAppAddPoint.Enabled = false;
				vdeCastUseTalkAppAddPoint.Enabled = false;
				vdrManUseTalkAppAddPoint.Enabled = false;
				vdeManUseTalkAppAddPoint.Enabled = false;
			}
		}
		pnlKey.Enabled = false;
		pnlDtl.Visible = true;
	}

	private void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_CHARGE_TEMP_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lblSiteCd.Text);
			db.ProcedureInParm("PACT_CATEGORY_SEQ",DbSession.DbType.VARCHAR2,ViewState["actCategorySeq"].ToString());
			db.ProcedureInParm("PCAST_CHARGE_TEMP_CD",DbSession.DbType.VARCHAR2,this.CastChargeTempCd);

			int iRecCount = int.Parse(ViewState["CHARGE_RECORD_COUNT"].ToString());

			string[] sChargeType = new string[MAX_CHARGE_COUNT];
			string[] sChargeUnitSec = new string[MAX_CHARGE_COUNT];
			string[] sChargePointNormal = new string[MAX_CHARGE_COUNT];
			string[] sChargePointNoReceipt = new string[MAX_CHARGE_COUNT];
			string[] sChargePointNormalB = new string[MAX_CHARGE_COUNT];
			string[] sChargePointNoReceiptB = new string[MAX_CHARGE_COUNT];
			string[] sChargePointNormalC = new string[MAX_CHARGE_COUNT];
			string[] sChargePointNoReceiptC = new string[MAX_CHARGE_COUNT];
			string[] sChargePointNormalD = new string[MAX_CHARGE_COUNT];
			string[] sChargePointNoReceiptD = new string[MAX_CHARGE_COUNT];
			string[] sChargePointNormalE = new string[MAX_CHARGE_COUNT];
			string[] sChargePointNoReceiptE = new string[MAX_CHARGE_COUNT];
			string[] sFreeDialFee = new string[MAX_CHARGE_COUNT];
			string[] sCastUseTalkAppAddPoint = new string[MAX_CHARGE_COUNT];
			string[] sManUseTalkAppAddPoint = new string[MAX_CHARGE_COUNT];

			string[] sRevisionNo = new string[MAX_CHARGE_COUNT];
			string[] sRowID = new string[MAX_CHARGE_COUNT];

			for (int i = 0;i < iRecCount;i++) {
				TextBox txtChargeUnitSec = (TextBox)plcHolder.FindControl(string.Format("txtChargeUnitSec{0}",i)) as TextBox;
				TextBox txtChargePointNormal = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNormal{0}",i)) as TextBox;
				TextBox txtChargePointNoReceipt = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNoReceipt{0}",i)) as TextBox;
				TextBox txtChargePointNormalB = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNormalB{0}",i)) as TextBox;
				TextBox txtChargePointNoReceiptB = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNoReceiptB{0}",i)) as TextBox;
				TextBox txtChargePointNormalC = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNormalC{0}",i)) as TextBox;
				TextBox txtChargePointNoReceiptC = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNoReceiptC{0}",i)) as TextBox;
				TextBox txtChargePointNormalD = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNormalD{0}",i)) as TextBox;
				TextBox txtChargePointNoReceiptD = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNoReceiptD{0}",i)) as TextBox;
				TextBox txtChargePointNormalE = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNormalE{0}",i)) as TextBox;
				TextBox txtChargePointNoReceiptE = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNoReceiptE{0}",i)) as TextBox;
				TextBox txtFreeDialFee = (TextBox)plcHolder.FindControl(string.Format("txtFreeDialFee{0}",i)) as TextBox;
				TextBox txtCastUseTalkAppAddPoint = (TextBox)plcHolder.FindControl(string.Format("txtCastUseTalkAppAddPoint{0}",i)) as TextBox;
				TextBox txtManUseTalkAppAddPoint = (TextBox)plcHolder.FindControl(string.Format("txtManUseTalkAppAddPoint{0}",i)) as TextBox;

				sChargeUnitSec[i] = txtChargeUnitSec.Text;
				sChargePointNormal[i] = txtChargePointNormal.Text;
				sChargePointNoReceipt[i] = txtChargePointNoReceipt.Text;
				sChargePointNormalB[i] = txtChargePointNormalB.Text;
				sChargePointNoReceiptB[i] = txtChargePointNoReceiptB.Text;
				sChargePointNormalC[i] = txtChargePointNormalC.Text;
				sChargePointNoReceiptC[i] = txtChargePointNoReceiptC.Text;
				sChargePointNormalD[i] = txtChargePointNormalD.Text;
				sChargePointNoReceiptD[i] = txtChargePointNoReceiptD.Text;
				sChargePointNormalE[i] = txtChargePointNormalE.Text;
				sChargePointNoReceiptE[i] = txtChargePointNoReceiptE.Text;
				sFreeDialFee[i] = txtFreeDialFee.Text;
				sCastUseTalkAppAddPoint[i] = txtCastUseTalkAppAddPoint.Text;
				sManUseTalkAppAddPoint[i] = txtManUseTalkAppAddPoint.Text;

				sChargeType[i] = (string)ViewState["CHARGE_TYPE" + i.ToString()];
				sRevisionNo[i] = (string)ViewState["CHARGE_REVISION_NO" + i.ToString()];
				sRowID[i] = (string)ViewState["CHARGE_ROWID" + i.ToString()];
			}

			db.ProcedureInArrayParm("pCHARGE_TYPE",DbSession.DbType.VARCHAR2,iRecCount,sChargeType);
			db.ProcedureInArrayParm("pCHARGE_POINT_NORMAL",DbSession.DbType.VARCHAR2,iRecCount,sChargePointNormal);
			db.ProcedureInArrayParm("pCHARGE_POINT_NO_RECEIPT",DbSession.DbType.VARCHAR2,iRecCount,sChargePointNoReceipt);
			db.ProcedureInArrayParm("pCHARGE_POINT_NORMAL2",DbSession.DbType.VARCHAR2,iRecCount,sChargePointNormalB);
			db.ProcedureInArrayParm("pCHARGE_POINT_NO_RECEIPT2",DbSession.DbType.VARCHAR2,iRecCount,sChargePointNoReceiptB);
			db.ProcedureInArrayParm("pCHARGE_POINT_NORMAL3",DbSession.DbType.VARCHAR2,iRecCount,sChargePointNormalC);
			db.ProcedureInArrayParm("pCHARGE_POINT_NO_RECEIPT3",DbSession.DbType.VARCHAR2,iRecCount,sChargePointNoReceiptC);
			db.ProcedureInArrayParm("pCHARGE_POINT_NORMAL4",DbSession.DbType.VARCHAR2,iRecCount,sChargePointNormalD);
			db.ProcedureInArrayParm("pCHARGE_POINT_NO_RECEIPT4",DbSession.DbType.VARCHAR2,iRecCount,sChargePointNoReceiptD);
			db.ProcedureInArrayParm("pCHARGE_POINT_NORMAL5",DbSession.DbType.VARCHAR2,iRecCount,sChargePointNormalE);
			db.ProcedureInArrayParm("pCHARGE_POINT_NO_RECEIPT5",DbSession.DbType.VARCHAR2,iRecCount,sChargePointNoReceiptE);
			db.ProcedureInArrayParm("pCHARGE_UNIT_SEC",DbSession.DbType.VARCHAR2,iRecCount,sChargeUnitSec);
			db.ProcedureInArrayParm("pFREE_DIAL_FEE",DbSession.DbType.VARCHAR2,iRecCount,sFreeDialFee);
			db.ProcedureInArrayParm("pCAST_USE_TALK_APP_ADD_POINT",DbSession.DbType.VARCHAR2,iRecCount,sCastUseTalkAppAddPoint);
			db.ProcedureInArrayParm("pMAN_USE_TALK_APP_ADD_POINT",DbSession.DbType.VARCHAR2,iRecCount,sManUseTalkAppAddPoint);
			db.ProcedureInArrayParm("pCHARGE_ROWID",DbSession.DbType.VARCHAR2,iRecCount,sRowID);
			db.ProcedureInArrayParm("pCHARGE_REVISION_NO",DbSession.DbType.VARCHAR2,iRecCount,sRevisionNo);
			db.ProcedureInParm("pCHARGE_RECORD_COUNT",DbSession.DbType.NUMBER,iRecCount);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();
			pnlDtl.Visible = false;
		}
		InitPage();
		GetList();
		lstSeekSiteCd.SelectedValue = lblSiteCd.Text;
	}

	protected void grdCastChargeTemp_DataBound(object sender,EventArgs e) {
		JoinCells(grdCastChargeTemp,1);
		JoinCells(grdCastChargeTemp,0);

		GridViewRow gRow = new GridViewRow(-1,-1,DataControlRowType.Header,DataControlRowState.Normal);

		TableCell tbHead = new TableCell();
		TableCell tbHeadA = new TableCell();
		TableCell tbHeadB = new TableCell();
		TableCell tbHeadC = new TableCell();
		TableCell tbHeadD = new TableCell();
		TableCell tbHeadE = new TableCell();
		TableCell tbFreeDial = new TableCell();
		TableCell tbTalkApp = new TableCell();

		tbHead.ColumnSpan = 3;
		tbHead.Text = "※「単位」列を除き、金額の値を表します";
		tbHead.Font.Bold = false;
		tbHead.HorizontalAlign = HorizontalAlign.Right;
		tbHeadA.ColumnSpan = 2;
		tbHeadA.Text = "RANK A";
		tbHeadB.ColumnSpan = 2;
		tbHeadB.Text = "RANK B";
		tbHeadC.ColumnSpan = 2;
		tbHeadC.Text = "RANK C";
		tbHeadD.ColumnSpan = 2;
		tbHeadD.Text = "RANK D";
		tbHeadE.ColumnSpan = 2;
		tbHeadE.Text = "RANK E";
		tbFreeDial.ColumnSpan = 1;
		tbFreeDial.Text = "FreeDail";
		tbTalkApp.ColumnSpan = 2;
		tbTalkApp.Text = "TalkApp";

		gRow.RowType = DataControlRowType.Header;
		gRow.Cells.Add(tbHead);
		gRow.Cells.Add(tbHeadA);
		gRow.Cells.Add(tbHeadB);
		gRow.Cells.Add(tbHeadC);
		gRow.Cells.Add(tbHeadD);
		gRow.Cells.Add(tbHeadE);
		gRow.Cells.Add(tbFreeDial);
		gRow.Cells.Add(tbTalkApp);

		if (grdCastChargeTemp.Rows.Count > 0) {
			grdCastChargeTemp.Controls[0].Controls.AddAt(0,gRow);
		}
	}

	protected void JoinCells(GridView pGrd,int pCol) {
		int iRow = pGrd.Rows.Count;
		int iBaseidx = 0;

		while (iBaseidx < iRow) {
			int iNextIdx = iBaseidx + 1;
			TableCell celBase = pGrd.Rows[iBaseidx].Cells[pCol];

			while (iNextIdx < iRow) {

				TableCell celNext = pGrd.Rows[iNextIdx].Cells[pCol];

				if (GetText(celBase).Equals(GetText(celNext))) {
					if (celBase.RowSpan == 0) {
						celBase.RowSpan = 2;
					} else {
						celBase.RowSpan++;
					}
					pGrd.Rows[iNextIdx].Cells.Remove(celNext);
					iNextIdx++;
				} else {
					break;
				}
			}
			iBaseidx = iNextIdx;
		}
	}
	private string GetText(TableCell tc) {
		HyperLink dblc =
		  (HyperLink)tc.Controls[1];
		return dblc.Text;
	}
}