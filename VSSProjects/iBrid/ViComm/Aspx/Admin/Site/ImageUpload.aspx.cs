﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: サイト構成情報メンテナンス
--	Progaram ID		: ImageUpload
--
--  Creation Date	: 2010.03.11
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Site_ImageUpload:System.Web.UI.Page {
	private string recCount = "";
	private string fileNmUploadPic;

	protected void Page_Load(object sender,EventArgs e) {
		int iCompare = string.Compare(Session["AdminType"].ToString(),ViCommConst.RIGHT_SITE_MANAGER);
		if (iCompare < 0) {
			throw new ApplicationException("ViComm権限違反");
		}

		if (!IsPostBack) {
			InitPage();
			string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["site"]);
			if (sSiteCd.Equals("") == false) {
				lblSiteCd.Text = sSiteCd;
				GetData();
			}
		}
	}

	protected void dsSiteManagement_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		grdSiteManagement.PageSize = int.Parse(Session["PageSize"].ToString());
		grdSiteManagement.PageIndex = 0;
		pnlMainte.Visible = false;

		DataBind();
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnUploadPic_Click(object sender,EventArgs e) {
		if (uldUploadPic.HasFile) {
			SetupDir();

			using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
				uldUploadPic.SaveAs(fileNmUploadPic);
			}
			Response.Redirect(string.Format("ImageUpload.aspx?site={0}",lblSiteCd.Text));
		}
	}

	protected void lnkSiteCd_Command(object sender,CommandEventArgs e) {
		lblSiteCd.Text = e.CommandArgument.ToString();
		GetData();
	}

	private void GetData() {
		using (Site oSite = new Site()) {
			if (oSite.GetOne(lblSiteCd.Text)) {
				lblSiteNm.Text = oSite.siteNm;
			}
		}

		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		SetupImage();
	}

	private void SetupDir() {
		string sWebPhisicalDir = "";

		using (Site oSite = new Site()) {
			oSite.GetValue(lblSiteCd.Text,"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
		}

		string sDirectry = sWebPhisicalDir + ViCommConst.IMAGE_DIRECTRY + "\\" + lblSiteCd.Text + "\\";
		fileNmUploadPic = sDirectry + System.IO.Path.GetFileName(uldUploadPic.FileName);
	}

	private void SetupImage() {
		SetupDir();
		btnUploadPic.Attributes["onclick"] = "return confirm('アップロードを行いますか？');";
	}
}
