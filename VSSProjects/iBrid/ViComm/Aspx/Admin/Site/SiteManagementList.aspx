<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SiteManagementList.aspx.cs" Inherits="Site_SiteManagementList"
	Title="サイト構成設定" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="サイト構成設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[サイト内容]</legend>
						<asp:Label ID="lblSiteCd" runat="server" Text="" Visible="false"></asp:Label>
						<table border="0" style="width: 760px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									ｻｲﾄ名
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblSiteNm" runat="server" Text="Label"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ｶﾗｰﾊﾟﾀｰﾝ
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstWebFaceSeq" runat="server" Width="206px" DataSourceID="dsWebFace" DataTextField="HTML_FACE_NAME" DataValueField="WEB_FACE_SEQ">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ｻﾎﾟｰﾄﾒｰﾙｱﾄﾞﾚｽ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtSupportEmailAddr" runat="server" MaxLength="60" Width="200px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrSupportEmailAddr" runat="server" ErrorMessage="ｻﾎﾟｰﾄﾒｰﾙｱﾄﾞﾚｽを入力して下さい。" ControlToValidate="txtSupportEmailAddr" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeSupportEmailAddr" runat="server" ControlToValidate="txtSupportEmailAddr" Display="Dynamic" ErrorMessage="ｻﾎﾟｰﾄﾒｰﾙｱﾄﾞﾚｽを正しく入力して下さい。"
										ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ｲﾝﾌｫﾒｰﾙｱﾄﾞﾚｽ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtInfoEMailAddr" runat="server" MaxLength="60" Width="200px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrInfoEMailAddr" runat="server" ControlToValidate="txtInfoEMailAddr" Display="Dynamic" ErrorMessage="ｲﾝﾌｫﾒｰﾙｱﾄﾞﾚｽを入力して下さい。"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeInfoEMailAddr" runat="server" ControlToValidate="txtInfoEMailAddr" Display="Dynamic" ErrorMessage="ｲﾝﾌｫﾒｰﾙｱﾄﾞﾚｽを正しく入力して下さい。"
										ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									違反報告ﾒｰﾙｱﾄﾞﾚｽ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtReportEMailAddr" runat="server" MaxLength="60" Width="200px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrReportEMailAddr" runat="server" ControlToValidate="txtReportEMailAddr" Display="Dynamic" ErrorMessage="違反報告ﾒｰﾙｱﾄﾞﾚｽを入力して下さい。"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeReportEMailAddr" runat="server" ControlToValidate="txtReportEMailAddr" Display="Dynamic" ErrorMessage="違反報告ﾒｰﾙｱﾄﾞﾚｽを正しく入力して下さい。"
										ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("出演者登録報告")%>
									<br />
									ﾒｰﾙｱﾄﾞﾚｽ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCastRegistReportEMailAddr" runat="server" MaxLength="60" Width="200px"></asp:TextBox>
									<asp:RegularExpressionValidator ID="vdeCastRegistReportEMailAddr" runat="server" ControlToValidate="txtCastRegistReportEMailAddr" Display="Dynamic" ErrorMessage="出演者登録報告ﾒｰﾙｱﾄﾞﾚｽを正しく入力して下さい。"
										ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<asp:PlaceHolder ID="plcModifyAccount" runat="server">
								<tr>
									<td class="tdHeaderStyle">
										出演者口座変更通知<br />
										ﾒｰﾙｱﾄﾞﾚｽ
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastModifyAccountEMailAddr" runat="server" MaxLength="60" Width="200px"></asp:TextBox>
										<asp:RegularExpressionValidator ID="vdeCastModifyAccountEMailAddr" runat="server" ControlToValidate="txtCastModifyAccountEMailAddr" Display="Dynamic" ErrorMessage="出演者口座変更通知ﾒｰﾙｱﾄﾞﾚｽを正しく入力して下さい。"
											ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
							</asp:PlaceHolder>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("男性会員ﾌｧｲﾙｱｯﾌﾟﾛｰﾄﾞ")%>
									<br />
									通知ﾒｰﾙｱﾄﾞﾚｽ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtManUploadEmailAddr" runat="server" MaxLength="60" Width="200px"></asp:TextBox>
									<asp:RegularExpressionValidator ID="vdrManUploadEmailAddr" runat="server" ControlToValidate="txtManUploadEmailAddr" Display="Dynamic" ErrorMessage="男性会員ﾌｧｲﾙｱｯﾌﾟﾛｰﾄﾞ通知ﾒｰﾙｱﾄﾞﾚｽを正しく入力して下さい。"
										ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("女性会員ﾌｧｲﾙｱｯﾌﾟﾛｰﾄﾞ")%>
									<br />
									通知ﾒｰﾙｱﾄﾞﾚｽ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCastUploadEmailAddr" runat="server" MaxLength="60" Width="200px"></asp:TextBox>
									<asp:RegularExpressionValidator ID="vdrCastUploadEmailAddr" runat="server" ControlToValidate="txtCastUploadEmailAddr" Display="Dynamic" ErrorMessage="女性会員ﾌｧｲﾙｱｯﾌﾟﾛｰﾄﾞ通知ﾒｰﾙｱﾄﾞﾚｽを正しく入力して下さい。"
										ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<asp:PlaceHolder ID="plcAgentRegistEmailAddr" runat="server">
								<tr>
									<td class="tdHeaderStyle">
										ｴｰｼﾞｪﾝﾄ登録
										<br />
										通知ﾒｰﾙｱﾄﾞﾚｽ
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtAgentRegistEmailAddr" runat="server" MaxLength="60" Width="200px"></asp:TextBox>
										<asp:RegularExpressionValidator ID="vdrAgentRegistEmailAddr" runat="server" ControlToValidate="txtAgentRegistEmailAddr" Display="Dynamic" ErrorMessage="ｴｰｼﾞｪﾝﾄ登録通知ﾒｰﾙｱﾄﾞﾚｽを正しく入力して下さい。"
											ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
							</asp:PlaceHolder>
							<asp:PlaceHolder ID="plcVisible8" runat="server">
								<tr>
									<td class="tdHeaderStyle">
										ﾒｰﾙ埋め込み用URL
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtMailEmbedDomain" runat="server" MaxLength="60" Width="200px"></asp:TextBox>
									</td>
								</tr>
							</asp:PlaceHolder>
							<tr>
								<td class="tdHeaderStyle">
									ｻﾎﾟｰﾄ電話番号
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtSupportTel" runat="server" MaxLength="11" Width="100px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrSupportTel" runat="server" ControlToValidate="txtSupportTel" Display="Dynamic" ErrorMessage="ｻﾎﾟｰﾄ電話番号を入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeSupportTel" runat="server" ControlToValidate="txtSupportTel" Display="Dynamic" ErrorMessage="ｻﾎﾟｰﾄ電話番号を正しく入力して下さい。"
										ValidationExpression="(0\d{9,10})" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									男性会員登録時<br />
									ｻｰﾋﾞｽﾎﾟｲﾝﾄ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtRegistServicePoint" runat="server" MaxLength="4" Width="30px"></asp:TextBox>Pt
									<asp:RequiredFieldValidator ID="vdrRegistServicePoint" runat="server" ErrorMessage="男性会員登録時ｻｰﾋﾞｽﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtRegistServicePoint"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeRegistServicePoint" runat="server" ErrorMessage="男性会員登録時ｻｰﾋﾞｽﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
										ControlToValidate="txtRegistServicePoint" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									出演者登録時<br />
									ｻｰﾋﾞｽﾎﾟｲﾝﾄ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtRegistCastBonusPoint" runat="server" MaxLength="5" Width="30px"></asp:TextBox>Pt
									<asp:RequiredFieldValidator ID="vdrRegistCastBonusPoint" runat="server" ErrorMessage="出演者登録時ｻｰﾋﾞｽﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtRegistCastBonusPoint"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeRegistCastBonusPoint" runat="server" ErrorMessage="出演者登録時ｻｰﾋﾞｽﾎﾟｲﾝﾄは1〜5桁の数字で入力して下さい。" ValidationExpression="\d{1,5}"
										ControlToValidate="txtRegistCastBonusPoint" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("男性会員紹介")%>
									<br />
									報酬発生条件
								</td>
								<td class="tdDataStyle">
									紹介先側が<asp:DropDownList ID="lstManIntroPointGetTiming" runat="server" DataSourceID="dsManIntroPointGetTiming" DataTextField="CODE_NM" DataValueField="CODE">
									</asp:DropDownList>に
									<br />
									紹介元に<asp:TextBox ID="txtManIntroducerPoint" runat="server" MaxLength="6" Width="50px"></asp:TextBox>Pt追加する。
									<asp:RequiredFieldValidator ID="vdrManIntroducerPoint" runat="server" ErrorMessage="紹介元報酬ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManIntroducerPoint" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeManIntroducerPoint" runat="server" ErrorMessage="紹介元報酬ﾎﾟｲﾝﾄは1〜6桁の数字で入力して下さい。" ValidationExpression="\d{1,6}" ControlToValidate="txtManIntroducerPoint"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									<br />
									紹介先に<asp:TextBox ID="txtManIntroPoint" runat="server" MaxLength="6" Width="50px"></asp:TextBox>Pt追加する。
									<asp:RequiredFieldValidator ID="vdrManIntroPoint" runat="server" ErrorMessage="紹介先報酬ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManIntroPoint" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeManIntroPoint" runat="server" ErrorMessage="紹介先報酬ﾎﾟｲﾝﾄは1〜6桁の数字で入力して下さい。" ValidationExpression="\d{1,6}" ControlToValidate="txtManIntroPoint"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("出演者紹介")%>
									<br />
									報酬発生条件
								</td>
								<td class="tdDataStyle">
									<asp:RadioButton ID="rdoCastIntroPointGetTiming3" runat="server" Text="" GroupName="CastIntroPointGetTiming">
									</asp:RadioButton>
									紹介先出演者登録時<br />
									<asp:RadioButton ID="rdoCastIntroPointGetTiming2" runat="server" Text="" GroupName="CastIntroPointGetTiming">
									</asp:RadioButton>
									紹介先出演者が初めて着信を受けた場合<br />
									<asp:RadioButton ID="rdoCastIntroPointGetTiming1" runat="server" Text="" GroupName="CastIntroPointGetTiming">
									</asp:RadioButton>
									紹介先出演者の累計報酬が<asp:TextBox ID="txtCastMinimumPayment" runat="server" MaxLength="5" Width="50px"></asp:TextBox>
									円に達した場合
									<asp:RequiredFieldValidator ID="vdrCastMinimumPayment" runat="server" ErrorMessage="報酬発生最低額を入力して下さい。" ControlToValidate="txtCastMinimumPayment" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeCastMinimumPayment" runat="server" ErrorMessage="報酬発生最低額は1〜5桁の数字で入力して下さい。" ValidationExpression="\d{1,5}" ControlToValidate="txtCastMinimumPayment"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									<br />
									紹介元に<asp:TextBox ID="txtCastIntroducerPoint" runat="server" MaxLength="6" Width="50px"></asp:TextBox>Pt支払う。
									<asp:RequiredFieldValidator ID="vdrCastIntroducerPoint" runat="server" ErrorMessage="紹介元報酬ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastIntroducerPoint" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeCastIntroducerPoint" runat="server" ErrorMessage="紹介元報酬ﾎﾟｲﾝﾄは1〜6桁の数字で入力して下さい。" ValidationExpression="\d{1,6}" ControlToValidate="txtCastIntroducerPoint"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									<br />
									紹介先に<asp:TextBox ID="txtCastIntroPoint" runat="server" MaxLength="6" Width="50px"></asp:TextBox>Pt支払う。
									<asp:RequiredFieldValidator ID="vdrCastIntroPoint" runat="server" ErrorMessage="紹介先報酬ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastIntroPoint" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeCastIntroPoint" runat="server" ErrorMessage="紹介先報酬ﾎﾟｲﾝﾄは1〜6桁の数字で入力して下さい。" ValidationExpression="\d{1,6}" ControlToValidate="txtCastIntroPoint"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("男性会員再登録")%>
									<br />
									可能入金額
								</td>
								<td class="tdDataStyle">
									入金累計額が<asp:TextBox ID="txtManNewRegistOkAmt" runat="server" MaxLength="10" Width="60px"></asp:TextBox>円を超えている場合、退会後に新規での登録を可能とする。
									<asp:RequiredFieldValidator ID="vdrManNewRegistOkAmt" runat="server" ErrorMessage="男性会員再登録可能入金額を入力して下さい。" ControlToValidate="txtManNewRegistOkAmt" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("男性会員退会時に")%>
									<br />
									関連ﾃﾞｰﾀを削除する
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkManSecessionDelDataFlag" runat="server" Text="男性会員が退会した時に、掲示板の書込みを削除する。" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("男性会員自動ﾛｸﾞｲﾝ時に")%>
									<br />
									退会者専用画面を利用
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkManAutoLoginResignedFlag" runat="server" Text="退会状態の男性会員が自動ﾛｸﾞｲﾝした際に、専用の画面を表示させる。" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("出演者再登録")%>
									<br />
									可能累計報酬額
								</td>
								<td class="tdDataStyle">
									報酬累計額が<asp:TextBox ID="txtCastNewRegistOkAmt" runat="server" MaxLength="10" Width="60px"></asp:TextBox>円を超えている場合、退会後に新規での登録を可能とする。
									<asp:RequiredFieldValidator ID="vdrCastNewRegistOkAmt" runat="server" ErrorMessage="出演者再登録可能入金額を入力して下さい。" ControlToValidate="txtCastNewRegistOkAmt" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("出演者退会時に")%>
									<br />
									関連ﾃﾞｰﾀを削除する
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkCastSecessionDelDataFlag" runat="server" Text="出演者が退会した時に、掲示板・画像掲示板・動画掲示板の書込みを削除する。" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("出演者自動ﾛｸﾞｲﾝ時に")%>
									<br />
									退会者専用画面を利用
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkCastAutoLoginResignedFlag" runat="server" Text="退会状態の出演者が自動ﾛｸﾞｲﾝした際に、専用の画面を表示させる。" />
								</td>
							</tr>
							<tr id="trMeasuredRateServicePoint" runat="server">
								<td class="tdHeaderStyle">
									<label id="lblMeasuredRateServicePoint" runat="server">
										ｸﾚｼﾞｯﾄ従量制<br />
										初回登録ｻｰﾋﾞｽPt</label>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtMeasuredRateServicePoint" runat="server" MaxLength="6" Width="50px"></asp:TextBox>Pt
									<asp:RequiredFieldValidator ID="vdrMeasuredRateServicePoint" runat="server" ErrorMessage="ｸﾚｼﾞｯﾄ従量制初回登録ｻｰﾋﾞｽﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtMeasuredRateServicePoint"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeMeasuredRateServicePoint" runat="server" ErrorMessage="ｸﾚｼﾞｯﾄ従量制初回登録ｻｰﾋﾞｽﾎﾟｲﾝﾄは1〜6桁の数字で入力して下さい。" ValidationExpression="\d{1,6}"
										ControlToValidate="txtMeasuredRateServicePoint" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr id="trMeasuredRateLimitPoint" runat="server">
								<td class="tdHeaderStyle">
									ｸﾚｼﾞｯﾄ従量制初回限度Pt
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtMeasuredRateLimitPoint" runat="server" MaxLength="6" Width="50px"></asp:TextBox>Pt
									<asp:RequiredFieldValidator ID="vdrMeasuredRateLimitPoint" runat="server" ErrorMessage="ｸﾚｼﾞｯﾄ従量制初回限度ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtMeasuredRateLimitPoint"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeMeasuredRateLimitPoint" runat="server" ErrorMessage="ｸﾚｼﾞｯﾄ従量制初回限度ﾎﾟｲﾝﾄは1〜6桁の数字で入力して下さい。" ValidationExpression="\d{1,6}"
										ControlToValidate="txtMeasuredRateLimitPoint" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr id="trZeroSettleServicePoint" runat="server">
								<td class="tdHeaderStyle">
									0円決済初回ｻｰﾋﾞｽPt
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtZeroSettleServicePoint" runat="server" MaxLength="6" Width="50px"></asp:TextBox>Pt
									<asp:RequiredFieldValidator ID="vdrZeroSettleServicePoint" runat="server" ErrorMessage="0円決済初回ｻｰﾋﾞｽﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtZeroSettleServicePoint"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeZeroSettleServicePoint" runat="server" ErrorMessage="0円決済初回ｻｰﾋﾞｽﾎﾟｲﾝﾄは1〜6桁の数字で入力して下さい。" ValidationExpression="\d{1,6}"
										ControlToValidate="txtZeroSettleServicePoint" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									会話招待ﾒｰﾙ有効時間
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtInviteMailAvaHour" runat="server" MaxLength="3" Width="30px"></asp:TextBox>時間
									<asp:RequiredFieldValidator ID="vdrInviteMailAvaHour" runat="server" ErrorMessage="会話招待ﾒｰﾙ有効時間を入力して下さい。" ControlToValidate="txtInviteMailAvaHour" ValidationGroup="Detail"
										Display="Dynamic">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeInviteMailAvaHour" runat="server" ErrorMessage="会話招待ﾒｰﾙ有効時間は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtInviteMailAvaHour"
										ValidationGroup="Detail" Display="Dynamic">*</asp:RegularExpressionValidator>
									<asp:CustomValidator ID="vdcInviteMailAvaHour" runat="server" ErrorMessage="会話招待ﾒｰﾙ有効時間を会話招待ﾒｰﾙ再送不可時間より大きくすることはできません。" ValidationGroup="Detail" OnServerValidate="vdcInviteMailAvaHour_ServerValidate"
										Display="Dynamic" ControlToValidate="txtInviteMailAvaHour">*</asp:CustomValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									会話招待ﾒｰﾙ再送不可時間
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtReInviteMailNAHour" runat="server" MaxLength="3" Width="30px"></asp:TextBox>時間
									<asp:RequiredFieldValidator ID="vdrReInviteMailNAHour" runat="server" ErrorMessage="会話招待ﾒｰﾙ再送不可時間を入力して下さい。" ControlToValidate="txtReInviteMailNAHour" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeReInviteMailNAHour" runat="server" ErrorMessage="会話招待ﾒｰﾙ再送不可時間は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}"
										ControlToValidate="txtReInviteMailNAHour" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									一括ﾒｰﾙ再送不可時間
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtReBatchMailNAHour" runat="server" MaxLength="2" Width="30px"></asp:TextBox>時間
									<asp:RequiredFieldValidator ID="vdrReBatchMailNAHour" runat="server" ErrorMessage="一括ﾒｰﾙ再送不可時間を入力して下さい。" ControlToValidate="txtReBatchMailNAHour" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeReBatchMailNAHour" runat="server" ErrorMessage="一括ﾒｰﾙ再送不可時間は1〜2桁の数字で入力して下さい。" ValidationExpression="\d{1,2}" ControlToValidate="txtReBatchMailNAHour"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									出演者ﾒｰﾙ送信条件
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkTxMailRefMarking" runat="server" Text="出演者に足あとをつけた男性会員へのみﾒｰﾙを送信させる。" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									出演者=>男性へのﾒｰﾙ<br />
									再送不可時間
								</td>
								<td class="tdDataStyle">
									出演者から男性へのﾒｰﾙを<asp:TextBox ID="txtAproachMailIntervalHour" runat="server" MaxLength="3" Width="30px"></asp:TextBox>時間再送できない。
									<asp:RequiredFieldValidator ID="vdrAproachMailIntervalHour" runat="server" ErrorMessage="出演者=>男性へのﾒｰﾙ再送可能時間を入力して下さい。" ControlToValidate="txtAproachMailIntervalHour"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeAproachMailIntervalHour" runat="server" ErrorMessage="出演者=>男性へのﾒｰﾙ再送可能時間は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}"
										ControlToValidate="txtAproachMailIntervalHour" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾛｸﾞｲﾝﾒｰﾙ再送不可時間
								</td>
								<td class="tdDataStyle">
									ﾛｸﾞｲﾝﾒｰﾙを<asp:TextBox ID="txtLoginMailIntervalHour" runat="server" MaxLength="3" Width="30px"></asp:TextBox>時間再送しない。
									<asp:RequiredFieldValidator ID="vdrLoginMailIntervalHour" runat="server" ErrorMessage="ﾛｸﾞｲﾝﾒｰﾙ再送可能時間を入力して下さい。" ControlToValidate="txtLoginMailIntervalHour"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeLoginMailIntervalHour" runat="server" ErrorMessage="ﾛｸﾞｲﾝﾒｰﾙ再送可能時間は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}"
										ControlToValidate="txtLoginMailIntervalHour" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾛｸﾞｱｳﾄﾒｰﾙ再送不可時間
								</td>
								<td class="tdDataStyle">
									ﾛｸﾞｱｳﾄﾒｰﾙを<asp:TextBox ID="txtLogoutMailIntervalHour" runat="server" MaxLength="3" Width="30px"></asp:TextBox>時間再送しない。
									<asp:RequiredFieldValidator ID="vdrLogoutMailIntervalHour" runat="server" ErrorMessage="ﾛｸﾞｱｳﾄﾒｰﾙ再送可能時間を入力して下さい。" ControlToValidate="txtLogoutMailIntervalHour"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeLogoutMailIntervalHour" runat="server" ErrorMessage="ﾛｸﾞｱｳﾄﾒｰﾙ再送可能時間は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}"
										ControlToValidate="txtLogoutMailIntervalHour" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾛｸﾞｱｳﾄﾒｰﾙ対象会話秒数
								</td>
								<td class="tdDataStyle">
									出演者が待機開始〜待機終了までに<asp:TextBox ID="txtLogoutMailValidTalkSec" runat="server" MaxLength="3" Width="30px"></asp:TextBox>秒以上通話した会員にﾛｸﾞｱｳﾄﾒｰﾙを送信する。
									<asp:RequiredFieldValidator ID="vdrLogoutMailValidTalkSec" runat="server" ErrorMessage="ﾛｸﾞｱｳﾄﾒｰﾙ対象会話秒数を入力して下さい。" ControlToValidate="txtLogoutMailValidTalkSec"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeLogoutMailValidTalkSec" runat="server" ErrorMessage="ﾛｸﾞｱｳﾄﾒｰﾙ対象会話秒数は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}"
										ControlToValidate="txtLogoutMailValidTalkSec" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾀﾞﾐｰ待機時ﾛｸﾞｲﾝﾒｰﾙ送信
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkDummyTalkTxLoginMailFlag" runat="server" Text="ﾀﾞﾐｰ待機時にもﾛｸﾞｲﾝﾒｰﾙを送信する。" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾛｸﾞｲﾝ通知ﾒｰﾙ<br />
									(気に入られている)
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkLoginMailNotSendFlag" runat="server" Text="出演者をお気に入り登録している男性会員にはﾛｸﾞｲﾝ通知ﾒｰﾙを送信しない。" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾛｸﾞｲﾝ通知ﾒｰﾙ<br />
									(気にいっている)
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkLoginMail2NotSendFlag" runat="server" Text="出演者がお気に入り登録している男性会員にはﾛｸﾞｲﾝ通知ﾒｰﾙを送信しない。" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾛｸﾞｱｳﾄ通知ﾒｰﾙ
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkLogoutMailNotSendFlag" runat="server" Text="出演者が待機終了した際、会話した男性会員にﾛｸﾞｱｳﾄ通知ﾒｰﾙを送信しない。" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾒｰﾙﾃﾝﾌﾟﾚｰﾄ<br />
									<%= DisplayWordUtil.Replace("項番8出演者属性")%>
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstMailCastAttrTypeSeq1" runat="server" Width="206px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾒｰﾙﾃﾝﾌﾟﾚｰﾄ<br />
									<%= DisplayWordUtil.Replace("項番9出演者属性")%>
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstMailCastAttrTypeSeq2" runat="server" Width="206px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("出演者PC")%>
									<br />
									<%= DisplayWordUtil.Replace("男性会員検索表示1")%>
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstInqManTypeSeq1" runat="server" Width="206px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("出演者PC")%>
									<br />
									<%= DisplayWordUtil.Replace("男性会員検索表示2")%>
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstInqManTypeSeq2" runat="server" Width="206px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("出演者PC")%>
									<br />
									<%= DisplayWordUtil.Replace("男性会員検索表示3")%>
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstInqManTypeSeq3" runat="server" Width="206px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("出演者PC")%>
									<br />
									<%= DisplayWordUtil.Replace("男性会員検索表示4")%>
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstInqManTypeSeq4" runat="server" Width="206px">
									</asp:DropDownList>
								</td>
							</tr>
							<asp:PlaceHolder ID="plcInqAdminManTypeSeq" runat="server">
								<tr>
									<td class="tdHeaderStyle">
										管理画面<br />
										<%= DisplayWordUtil.Replace("男性会員検索表示1")%>
									</td>
									<td class="tdDataStyle">
										<asp:DropDownList ID="lstInqAdminManTypeSeq1" runat="server" Width="206px">
										</asp:DropDownList>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										管理画面<br />
										<%= DisplayWordUtil.Replace("男性会員検索表示2")%>
									</td>
									<td class="tdDataStyle">
										<asp:DropDownList ID="lstInqAdminManTypeSeq2" runat="server" Width="206px">
										</asp:DropDownList>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										管理画面<br />
										<%= DisplayWordUtil.Replace("男性会員検索表示3")%>
									</td>
									<td class="tdDataStyle">
										<asp:DropDownList ID="lstInqAdminManTypeSeq3" runat="server" Width="206px">
										</asp:DropDownList>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										管理画面<br />
										<%= DisplayWordUtil.Replace("男性会員検索表示4")%>
									</td>
									<td class="tdDataStyle">
										<asp:DropDownList ID="lstInqAdminManTypeSeq4" runat="server" Width="206px">
										</asp:DropDownList>
									</td>
								</tr>
							</asp:PlaceHolder>
							<asp:PlaceHolder ID="plcInqAdminCastTypeSeq" runat="server">
								<tr>
									<td class="tdHeaderStyle">
										管理画面<br />
										<%= DisplayWordUtil.Replace("出演者検索表示1")%>
									</td>
									<td class="tdDataStyle">
										<asp:DropDownList ID="lstInqAdminCastTypeSeq1" runat="server" Width="206px">
										</asp:DropDownList>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										管理画面<br />
										<%= DisplayWordUtil.Replace("出演者検索表示2")%>
									</td>
									<td class="tdDataStyle">
										<asp:DropDownList ID="lstInqAdminCastTypeSeq2" runat="server" Width="206px">
										</asp:DropDownList>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										管理画面<br />
										<%= DisplayWordUtil.Replace("出演者検索表示3")%>
									</td>
									<td class="tdDataStyle">
										<asp:DropDownList ID="lstInqAdminCastTypeSeq3" runat="server" Width="206px">
										</asp:DropDownList>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										管理画面<br />
										<%= DisplayWordUtil.Replace("出演者検索表示4")%>
									</td>
									<td class="tdDataStyle">
										<asp:DropDownList ID="lstInqAdminCastTypeSeq4" runat="server" Width="206px">
										</asp:DropDownList>
									</td>
								</tr>
							</asp:PlaceHolder>
							<tr>
								<td class="tdHeaderStyle">
									出演者PC画面にて
									<br />
									全男性会員検索時条件
								</td>
								<td class="tdDataStyle">
									出演者PC画面にて全会員表示時に、会員登録後<asp:TextBox ID="txtFindManElapasedDays" runat="server" MaxLength="3" Width="30px"></asp:TextBox>日までの会員を表示する。<br />
									<asp:RequiredFieldValidator ID="vdrFindManElapasedDays" runat="server" ErrorMessage="男性会員全体検索適合条件を入力して下さい。" ControlToValidate="txtFindManElapasedDays"
										ValidationGroup="Detail" Display="Dynamic">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeFindManElapasedDays" runat="server" ErrorMessage="男性会員全体検索適合条件は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}"
										ControlToValidate="txtFindManElapasedDays" ValidationGroup="Detail" Display="Dynamic">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<asp:PlaceHolder ID="plcVisible5" runat="server">
								<tr>
									<td class="tdHeaderStyle">
										会話招待ﾒｰﾙ<br />
										ｱﾌﾟﾛｰﾁﾒｰﾙ<br />
										1日の受信限度
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCmMailRxLimit" runat="server" MaxLength="3" Width="30px"></asp:TextBox>件
										<asp:RequiredFieldValidator ID="vdrCmMailRxLimit" runat="server" ErrorMessage="1日の受信限度を入力して下さい。" ControlToValidate="txtCmMailRxLimit" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCmMailRxLimit" runat="server" ErrorMessage="1日の受信限度は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtCmMailRxLimit"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
							</asp:PlaceHolder>
							<tr>
								<td class="tdHeaderStyle">
									招待ﾒｰﾙ出演者報酬ON
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkInviteTalkCastChargeFlag" runat="server" Text="招待ﾒｰﾙの通話報酬を、出演者に与える。" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("出演者による")%>
									<br />
									ﾓﾆﾀｰ可否の選択
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkCastCanSelectMonitorFlag" runat="server" Text="出演者の待機開始時にﾓﾆﾀｰ機能利用の可否を選択させる。" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									音声ﾓﾆﾀｰ利用
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkSupportVoiceMonitorFlag" runat="server" Text="音声ﾓﾆﾀｰ機能を利用する。" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("出演者による")%>
									<br />
									通話種別の選択
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkCastCanSelectConnectType" runat="server" Text="出演者の待機開始時に通話種別を選択させる。" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("出演者新人日数")%>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtNewFaceDays" runat="server" MaxLength="2" Width="30px"></asp:TextBox>日
									<asp:RequiredFieldValidator ID="vdrNewFaceDays" runat="server" ErrorMessage="出演者新人日数を入力して下さい。" ControlToValidate="txtNewFaceDays" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeNewFaceDays" runat="server" ErrorMessage="出演者新人日数は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,2}" ControlToValidate="txtNewFaceDays"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("男性会員新人日数") %>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtManNewFaceDays" runat="server" MaxLength="2" Width="30px"></asp:TextBox>日
									<asp:RequiredFieldValidator ID="vdrManNewFaceDays" runat="server" ErrorMessage="男性会員新人日数を入力して下さい。" ControlToValidate="txtManNewFaceDays" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeManNewFaceDays" runat="server" ErrorMessage="男性会員新人日数は1〜2桁の数字で入力して下さい。" ValidationExpression="\d{1,2}" ControlToValidate="txtManNewFaceDays"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<asp:PlaceHolder runat="server" ID="plcDownloadVoice">
								<tr>
									<td class="tdHeaderStyle">
										着ﾎﾞｲｽ新着日数
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtVoiceNewDays" runat="server" MaxLength="2" Width="30px"></asp:TextBox>日
										<asp:RequiredFieldValidator ID="vdrVoiceNewDays" runat="server" ErrorMessage="着ﾎﾞｲｽ新着日数を入力して下さい。" ControlToValidate="txtVoiceNewDays" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeVoiceNewDays" runat="server" ErrorMessage="着ﾎﾞｲｽ新着日数は1〜2桁の数字で入力して下さい。" ValidationExpression="\d{1,2}" ControlToValidate="txtVoiceNewDays"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
							</asp:PlaceHolder>
							<asp:PlaceHolder runat="server" ID="plcDownloadProduct">
								<tr>
									<td class="tdHeaderStyle">
										商品新着日数
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtProductNewDays" runat="server" MaxLength="2" Width="30px"></asp:TextBox>日
										<asp:RequiredFieldValidator ID="vdrProductNewDays" runat="server" ErrorMessage="商品新着日数を入力して下さい。" ControlToValidate="txtProductNewDays" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeProductNewDays" runat="server" ErrorMessage="商品新着日数は1〜2桁の数字で入力して下さい。" ValidationExpression="\d{1,2}" ControlToValidate="txtProductNewDays"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
							</asp:PlaceHolder>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("ﾌﾟﾛﾌｨｰﾙ動画新着時間")%>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtProfileMovieNewHours" runat="server" MaxLength="3" Width="30px"></asp:TextBox>時間
									<asp:RequiredFieldValidator ID="vdrProfileMovieNewHours" runat="server" ErrorMessage="ﾌﾟﾛﾌｨｰﾙ動画新着時間を入力して下さい。" ControlToValidate="txtProfileMovieNewHours"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RangeValidator ID="vdaProfileMovieNewHours" runat="server" ErrorMessage="ﾌﾟﾛﾌｨｰﾙ動画新着時間は1〜999の間で入力してください。" ControlToValidate="txtProfileMovieNewHours"
										MaximumValue="999" MinimumValue="1" ValidationGroup="Detail" Type="Integer">*</asp:RangeValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("掲示板書込新着時間")%>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtBbsNewHours" runat="server" MaxLength="3" Width="30px"></asp:TextBox>時間
									<asp:RequiredFieldValidator ID="vdrBbsNewHours" runat="server" ErrorMessage="掲示板書込新着時間を入力して下さい。" ControlToValidate="txtBbsNewHours" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RangeValidator ID="vdaBbsNewHours" runat="server" ErrorMessage="掲示板書込新着時間は1〜999の間で入力してください。" ControlToValidate="txtBbsNewHours" MaximumValue="999"
										MinimumValue="1" ValidationGroup="Detail" Type="Integer">*</asp:RangeValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("掲示板画像書込新着時間")%>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtPicBbsNewHours" runat="server" MaxLength="3" Width="30px"></asp:TextBox>時間
									<asp:RequiredFieldValidator ID="vdrPicBbsNewHours" runat="server" ErrorMessage="掲示板画像書込新着時間を入力して下さい。" ControlToValidate="txtPicBbsNewHours" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RangeValidator ID="vdaPicBbsNewHours" runat="server" ErrorMessage="掲示板画像書込新着時間は1〜999の間で入力してください。" ControlToValidate="txtPicBbsNewHours" MaximumValue="999"
										MinimumValue="1" ValidationGroup="Detail" Type="Integer">*</asp:RangeValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("掲示板動画書込新着時間")%>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtMovieBbsNewHours" runat="server" MaxLength="3" Width="30px"></asp:TextBox>時間
									<asp:RequiredFieldValidator ID="vdrMovieBbsNewHours" runat="server" ErrorMessage="掲示板動画書込新着時間を入力して下さい。" ControlToValidate="txtMovieBbsNewHours" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RangeValidator ID="vdaMovieBbsNewHours" runat="server" ErrorMessage="掲示板動画書込新着時間は1〜999の間で入力してください。" ControlToValidate="txtMovieBbsNewHours" MaximumValue="999"
										MinimumValue="1" ValidationGroup="Detail" Type="Integer">*</asp:RangeValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("受信ﾒｰﾙ新着時間")%>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtRxMailNewHours" runat="server" MaxLength="3" Width="30px"></asp:TextBox>時間
									<asp:RequiredFieldValidator ID="vdrRxMailNewHours" runat="server" ErrorMessage="受信ﾒｰﾙ新着時間を入力して下さい。" ControlToValidate="txtRxMailNewHours" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RangeValidator ID="vdaRxMailNewHours" runat="server" ErrorMessage="受信ﾒｰﾙ新着時間は1〜999の間で入力してください。" ControlToValidate="txtRxMailNewHours" MaximumValue="999"
										MinimumValue="1" ValidationGroup="Detail" Type="Integer">*</asp:RangeValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									やきもち設定ｵﾌﾗｲﾝ表示<br />
									遅延分数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtJealousyEffectDelayMin" runat="server" MaxLength="3" Width="30px"></asp:TextBox>分
									<asp:RequiredFieldValidator ID="vdrJealousyEffectDelayMin" runat="server" ErrorMessage="やきもち設定ｵﾌﾗｲﾝ表示遅延分数を入力してください。" ControlToValidate="txtRxMailNewHours"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									掲示板重複課金設定
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkBbsDupChargeFlag" runat="server" Text="掲示板利用時に重複課金をする。">
									</asp:CheckBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									添付ﾒｰﾙ閲覧<br />
									重複課金設定
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkAttachedMailDupChargeFlag" runat="server" Text="添付ﾒｰﾙ閲覧時に重複課金をする。">
									</asp:CheckBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									男性会員<br />
									添付ﾒｰﾙを使用
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkUseAttachedMailFlag" runat="server" Text="男性会員がﾒｰﾙ送信時に添付ﾒｰﾙを使用する。">
									</asp:CheckBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("男性会員新規登録時に") %>
									<br />
									ﾊﾝﾄﾞﾙ名を入力
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkRegistHandleNmInputFlag" runat="server" Text="男性会員新規登録時にﾊﾝﾄﾞﾙ名を入力させる。">
									</asp:CheckBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("男性会員新規登録時に") %>
									<br />
									誕生日を入力
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkRegistBirthdayInputFlag" runat="server" Text="男性会員新規登録時に誕生日を入力させる。">
									</asp:CheckBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("出演者新規登録時に")%>
									<br />
									規約に同意させる
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkCastRegistNeedAgreeFlag" runat="server" Text="出演者新規登録時に規約に同意させる。">
									</asp:CheckBox>
								</td>
							</tr>
							<asp:PlaceHolder ID="plcVisible1" runat="server">
								<tr>
									<td class="tdHeaderStyle">
										ﾌﾟﾛﾌｨｰﾙ画面<br />
										<%= DisplayWordUtil.Replace("ﾍﾟｰｼﾞ制御(男性会員)") %>
									</td>
									<td class="tdDataStyle">
										<asp:CheckBox ID="chkProfilePageingFlag" runat="server" Text="出演者のﾌﾟﾛﾌｨｰﾙ画面でﾍﾟｰｼﾞ制御を利用する。">
										</asp:CheckBox>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										前ﾍﾟｰｼﾞｶﾞｲﾀﾞﾝｽ
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtProfilePreviousGuidance" runat="server" MaxLength="128" Width="500px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrProfilePreviousGuidance" runat="server" ControlToValidate="txtProfilePreviousGuidance" ErrorMessage="ﾌﾟﾛﾌｨｰﾙ画面前ﾍﾟｰｼﾞｶﾞｲﾀﾞﾝｽを入力して下さい。"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										次ﾍﾟｰｼﾞｶﾞｲﾀﾞﾝｽ
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtProfileNextGuidance" runat="server" MaxLength="128" Width="500px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrProfileNextGuidance" runat="server" ControlToValidate="txtProfileNextGuidance" ErrorMessage="ﾌﾟﾛﾌｨｰﾙ画面次ﾍﾟｰｼﾞｶﾞｲﾀﾞﾝｽを入力して下さい。"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										ﾌﾟﾛﾌｨｰﾙ画面<br />
										<%= DisplayWordUtil.Replace("ﾍﾟｰｼﾞ制御(出演者)")%>
									</td>
									<td class="tdDataStyle">
										<asp:CheckBox ID="chkProfilePageingFlag2" runat="server" Text="男性会員のﾌﾟﾛﾌｨｰﾙ画面でﾍﾟｰｼﾞ制御を利用する。">
										</asp:CheckBox>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										前ﾍﾟｰｼﾞｶﾞｲﾀﾞﾝｽ
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtProfilePreviousGuidance2" runat="server" MaxLength="128" Width="500px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrProfilePreviousGuidance2" runat="server" ControlToValidate="txtProfilePreviousGuidance2" ErrorMessage="ﾌﾟﾛﾌｨｰﾙ画面前ﾍﾟｰｼﾞｶﾞｲﾀﾞﾝｽを入力して下さい。"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										次ﾍﾟｰｼﾞｶﾞｲﾀﾞﾝｽ
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtProfileNextGuidance2" runat="server" MaxLength="128" Width="500px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrProfileNextGuidance2" runat="server" ControlToValidate="txtProfileNextGuidance2" ErrorMessage="ﾌﾟﾛﾌｨｰﾙ画面次ﾍﾟｰｼﾞｶﾞｲﾀﾞﾝｽを入力して下さい。"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										前ﾍﾟｰｼﾞｱｸｾｽｷｰ
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtPreviousAccessKey" runat="server" MaxLength="1" Width="20px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrPreviousAccessKey" runat="server" ControlToValidate="txtPreviousAccessKey" ErrorMessage="前ﾍﾟｰｼﾞｱｸｾｽｷｰを入力してください。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										次ﾍﾟｰｼﾞｱｸｾｽｷｰ
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtNextAccessKey" runat="server" MaxLength="1" Width="20px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrNextAccessKey" runat="server" ControlToValidate="txtNextAccessKey" ErrorMessage="次ﾍﾟｰｼﾞｱｸｾｽｷｰを入力してください。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
								</tr>
							</asp:PlaceHolder>
							<tr>
								<td class="tdHeaderStyle">
									ﾗﾝｷﾝｸﾞ作成件数
								</td>
								<td class="tdDataStyle">
									ﾗﾝｷﾝｸﾞを
									<asp:TextBox ID="txtRankingCreateCount" runat="server" MaxLength="2" Width="30px"></asp:TextBox>位まで作成する。<br />
									上記数値が有効になるﾗﾝｷﾝｸﾞは、「週間通話、月間通話、週間ﾒｰﾙ、月間ﾒｰﾙ、週間動画、月間動画、日記週間、日記月間、お気に入り、商品」
									<asp:RequiredFieldValidator ID="vdrRankingCreateCount" runat="server" ControlToValidate="txtRankingCreateCount" ErrorMessage="ﾗﾝｷﾝｸﾞ作成件数を入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RangeValidator ID="vdaRankingCreateCount" runat="server" ControlToValidate="txtRankingCreateCount" ErrorMessage="ﾗﾝｷﾝｸﾞ作成件数は1〜99の間で入力してください。" MaximumValue="99"
										MinimumValue="0" ValidationGroup="Detail" Type="Integer">*</asp:RangeValidator></td>
							</tr>
							<asp:PlaceHolder ID="plcVisible6" runat="server">
								<tr>
									<td class="tdHeaderStyle">
										ｻﾌﾞｷｬﾗｸﾀｰ<br />
										ﾛｸﾞｲﾝ最短分数
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtOnlineAdjustMinFrom" runat="server" MaxLength="2" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrOnlineAdjustMinFrom" runat="server" ErrorMessage="ｻﾌﾞｷｬﾗｸﾀｰﾛｸﾞｲﾝ最短分数を入力してください。" ControlToValidate="txtOnlineAdjustMinFrom"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										ｻﾌﾞｷｬﾗｸﾀｰ<br />
										ﾛｸﾞｲﾝ最長分数
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtOnlineAdjustMinTo" runat="server" MaxLength="2" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrOnlineAdjustMinTo" runat="server" ErrorMessage="ｻﾌﾞｷｬﾗｸﾀｰﾛｸﾞｲﾝ最長分数を入力してください。" ControlToValidate="txtOnlineAdjustMinTo"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										<%= DisplayWordUtil.Replace("ﾛｸﾞｲﾝ済み男性会員") %>
										<br />
									</td>
									<td class="tdDataStyle">
										最終操作から<asp:TextBox ID="txtManOnlineMin" runat="server" MaxLength="3" Width="40px"></asp:TextBox>分後にｵﾌﾗｲﾝに変更する。
										<asp:RequiredFieldValidator ID="vdrManOnlineMin" runat="server" ErrorMessage="男性会員最終操作から何分後にｵﾌﾗｲﾝにするかを入力してください。" ControlToValidate="txtManOnlineMin" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
								</tr>
							</asp:PlaceHolder>
							<tr>
								<td class="tdHeaderStyle">
									2SHOTﾁｬｯﾄ<br />
									開始時ｶﾞｲﾀﾞﾝｽ
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtWShotTalkStartGuidance" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾊﾟｰﾃｨﾁｬｯﾄ<br />
									開始時ｶﾞｲﾀﾞﾝｽ
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtPublicTalkStartGuidance" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									入金完了時<br />
									ﾒｰﾙﾃﾝﾌﾟﾚｰﾄ番号
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:DropDownList ID="lstMailTemplateCash1" runat="server" Width="309px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr id="trMailTemplateCash2" runat="Server">
								<td class="tdHeaderStyle">
									入金不足時<br />
									ﾒｰﾙﾃﾝﾌﾟﾚｰﾄ番号
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:DropDownList ID="lstMailTemplateCash2" runat="server" Width="309px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("会員用・<br />友達紹介ﾎﾟｲﾝﾄ追加時")%>
									<br />
									ﾒｰﾙﾃﾝﾌﾟﾚｰﾄ番号
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:DropDownList ID="lstMailTemplateIntro" runat="server" Width="309px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("出演者用・<br />友達紹介ﾎﾟｲﾝﾄ追加時")%>
									<br />
									ﾒｰﾙﾃﾝﾌﾟﾚｰﾄ番号
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:DropDownList ID="lstCastIntroPointMailNo" runat="server" Width="309px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("出演者ｱﾄﾞﾚｽ登録済") %>
									<br />
									ﾒｰﾙﾃﾝﾌﾟﾚｰﾄ番号
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:DropDownList ID="lstMailTemplateCastReg" runat="server" Width="309px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("出演者ｱﾄﾞﾚｽ仮登録済") %>
									<br />
									ﾒｰﾙﾃﾝﾌﾟﾚｰﾄ番号
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:DropDownList ID="lstMailTemplateCastPreReg" runat="server" Width="309px">
									</asp:DropDownList>
								</td>
							</tr>
							<asp:PlaceHolder ID="plcVisible2" runat="server">
								<tr>
									<td class="tdHeaderStyle">
										<%= DisplayWordUtil.Replace("出演者ｱﾄﾞﾚｽ男性登録済") %>
										<br />
										ﾒｰﾙﾃﾝﾌﾟﾚｰﾄ番号
									</td>
									<td class="tdDataStyle" colspan="4">
										<asp:DropDownList ID="lstMailTemplateCastManReg" runat="server" Width="309px">
										</asp:DropDownList>
									</td>
								</tr>
							</asp:PlaceHolder>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("会員会話終了時に") %>
									<br />
									切断理由通知ﾒｰﾙを送信
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkManTalkEndSendFlag" runat="server" Text="会話終了時、男性会員に切断理由通知ﾒｰﾙを送信する。">
									</asp:CheckBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("会員用・出演者側切断") %>
									<br />
									ﾒｰﾙﾃﾝﾌﾟﾚｰﾄ番号
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:DropDownList ID="lstManTalkCastDiscMailNo" runat="server" Width="309px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("会員用・男性会員側切断")%>
									<br />
									ﾒｰﾙﾃﾝﾌﾟﾚｰﾄ番号
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:DropDownList ID="lstManTalkManDiscMailNo" runat="server" Width="309px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("会員用・ﾎﾟｲﾝﾄ切れ切断")%>
									<br />
									ﾒｰﾙﾃﾝﾌﾟﾚｰﾄ番号
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:DropDownList ID="lstManTalkPointDiscMailNo" runat="server" Width="309px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("会員用・その他切断")%>
									<br />
									ﾒｰﾙﾃﾝﾌﾟﾚｰﾄ番号
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:DropDownList ID="lstManTalkSysDiscMailNo" runat="server" Width="309px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("出演者会話終了時に") %>
									<br />
									切断理由通知ﾒｰﾙを送信
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkCastTalkEndSendFlag" runat="server" Text="会話終了時、出演者に切断理由通知ﾒｰﾙを送信する。">
									</asp:CheckBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("出演者用・出演者側切断")%>
									<br />
									ﾒｰﾙﾃﾝﾌﾟﾚｰﾄ番号
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:DropDownList ID="lstCastTalkCastDiscMailNo" runat="server" Width="309px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("出演者用・男性会員側切断")%>
									<br />
									ﾒｰﾙﾃﾝﾌﾟﾚｰﾄ番号
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:DropDownList ID="lstCastTalkManDiscMailNo" runat="server" Width="309px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("出演者用・ﾎﾟｲﾝﾄ切れ切断")%>
									<br />
									ﾒｰﾙﾃﾝﾌﾟﾚｰﾄ番号
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:DropDownList ID="lstCastTalkPointDiscMailNo" runat="server" Width="309px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("出演者用・その他切断")%>
									<br />
									ﾒｰﾙﾃﾝﾌﾟﾚｰﾄ番号
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:DropDownList ID="lstCastTalkSysDiscMailNo" runat="server" Width="309px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									男性会員登録完了時<br />
									販促ﾒｰﾙﾃﾝﾌﾟﾚｰﾄ
								</td>
								<td class="tdDataStyle" colspan="4">
									男性会員登録完了から<asp:DropDownList ID="lstRegistDMMailTiming" runat="server">
										<asp:ListItem>10</asp:ListItem>
										<asp:ListItem>20</asp:ListItem>
										<asp:ListItem>30</asp:ListItem>
										<asp:ListItem>40</asp:ListItem>
										<asp:ListItem>50</asp:ListItem>
										<asp:ListItem>60</asp:ListItem>
										<asp:ListItem>70</asp:ListItem>
										<asp:ListItem>80</asp:ListItem>
										<asp:ListItem>90</asp:ListItem>
									</asp:DropDownList>分後に<asp:DropDownList ID="lstMailTemplateRegistDM" runat="server" Width="309px">
									</asp:DropDownList>
									を送信する。</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									男性会員登録完了時<br />
									販促ﾒｰﾙﾃﾝﾌﾟﾚｰﾄBﾀｲﾌﾟ
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:DropDownList ID="lstMailTemplateRegistDM2" runat="server" Width="309px">
									</asp:DropDownList>
									を送信する。</td>
							</tr>
							<asp:PlaceHolder ID="plcVisible3" runat="server">
								<tr>
									<td class="tdHeaderStyle">
										MAQIAｱﾌﾘ登録完了時<br />
										販促ﾒｰﾙﾃﾝﾌﾟﾚｰﾄ
									</td>
									<td class="tdDataStyle" colspan="4">
										通常登録完了から<asp:DropDownList ID="lstMaqiaDMMailTiming" runat="server">
											<asp:ListItem>10</asp:ListItem>
											<asp:ListItem>20</asp:ListItem>
											<asp:ListItem>30</asp:ListItem>
											<asp:ListItem>40</asp:ListItem>
											<asp:ListItem>50</asp:ListItem>
											<asp:ListItem>60</asp:ListItem>
											<asp:ListItem>70</asp:ListItem>
											<asp:ListItem>80</asp:ListItem>
											<asp:ListItem>90</asp:ListItem>
										</asp:DropDownList>分後に<asp:DropDownList ID="lstMailTemplateRegistMaqiaDM" runat="server" Width="309px">
										</asp:DropDownList>
										を送信する。</td>
								</tr>
							</asp:PlaceHolder>
							<!--PWILD以外で 「通話したことのある女性がTEL待ちしたときに通知する」 が有効の場合表示-->
							<!--PWILDは拡張機能管理で設定-->
							<asp:PlaceHolder ID="plcVisible9" runat="server">
								<tr>
									<td class="tdHeaderStyle">
										<%= DisplayWordUtil.Replace("出演者(通話履歴有り)")%>
										<br />
										待機通知ﾒｰﾙﾃﾝﾌﾟﾚｰﾄ
									</td>
									<td class="tdDataStyle" colspan="4">
										<asp:DropDownList ID="lstTpManWaitingCastWithTalk" runat="server" Width="309px">
										</asp:DropDownList>
									</td>
								</tr>
							</asp:PlaceHolder>
							<!--ﾃﾞﾌｫﾙﾄで出演者の状態に関わらず受信するに変更　2010/09/10-->
							<tr id="trCastOnlineMailRxFlag" runat="server">
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("出演者がｵﾝﾗｲﾝの時も")%>
									<br />
									携帯でﾒｰﾙを受信する。
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkCastOnlineMailRxFlag" runat="server">
									</asp:CheckBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									TV会話開始時<br />
									無課金秒数
								</td>
								<td class="tdDataStyle">
									TV会話開始から<asp:TextBox ID="txtTalkStartNotChargeSec" runat="server" MaxLength="2" Width="30px"></asp:TextBox>秒以内に切断した場合、課金しない。
									<asp:RequiredFieldValidator ID="vdrTalkStartNotChargeSec" runat="server" ErrorMessage="会話開始から何秒以内に切断した場合課金しないかを入力してください。" ControlToValidate="txtTalkStartNotChargeSec"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									音声会話開始時<br />
									無課金秒数
								</td>
								<td class="tdDataStyle">
									音声会話開始から<asp:TextBox ID="txtTalk2StartNotChargeSec" runat="server" MaxLength="2" Width="30px"></asp:TextBox>秒以内に切断した場合、課金しない。
									<asp:RequiredFieldValidator ID="vdrTalk2StartNotChargeSec" runat="server" ErrorMessage="会話開始から何秒以内に切断した場合課金しないかを入力してください。" ControlToValidate="txtTalk2StartNotChargeSec"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									PCﾘﾀﾞｲﾚｸﾄ先URL
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtPcRedirectUrl" runat="server" Width="420px" MaxLength="256"></asp:TextBox>
									<asp:RegularExpressionValidator ID="vdePcRedirectUrl" runat="server" ErrorMessage="PCﾘﾀﾞｲﾚｸﾄ先URLを正しく入力して下さい。" ValidationExpression="(\S+)://([^:/]+)(:(\d+))?(/[^#\s]*)(#(\S+))?"
										ControlToValidate="txtPcRedirectUrl" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("携帯男性会員") %>
									<br />
									明細表示件数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtMobileManDetailListCount" runat="server" MaxLength="2" Width="30px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrMobileManDetailListCount" runat="server" ErrorMessage="携帯男性会員明細表示件数を入力してください。" ControlToValidate="txtMobileManDetailListCount"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("携帯男性会員") %>
									<br />
									一覧表示件数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtMobileManListCount" runat="server" MaxLength="2" Width="30px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrMobileManListCount" runat="server" ErrorMessage="携帯男性会員一覧表示件数を入力してください。" ControlToValidate="txtMobileManListCount" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("携帯出演者")%>
									<br />
									明細表示件数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtMobileWomanDetailListCount" runat="server" MaxLength="2" Width="30px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrMobileWomanDetailListCount" runat="server" ErrorMessage="携帯出演者明細表示件数を入力してください。" ControlToValidate="txtMobileWomanDetailListCount"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("携帯出演者")%>
									<br />
									一覧表示件数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtMobileWomanListCount" runat="server" MaxLength="2" Width="30px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrMobileWomanListCount" runat="server" ErrorMessage="携帯出演者一覧表示件数を入力してください。" ControlToValidate="txtMobileWomanListCount"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<asp:PlaceHolder ID="plcVisible7" runat="server">
								<tr>
									<td class="tdHeaderStyle">
										ﾋﾟｯｸｱｯﾌﾟ<br />
										自動取得件数
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtPickupAutoSetCount" runat="server" MaxLength="2" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrPickupAutoSetCount" runat="server" ErrorMessage="ﾋﾟｯｸｱｯﾌﾟ自動取得件数を入力してください。" ControlToValidate="txtPickupAutoSetCount"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RangeValidator ID="vdaPickupAutoSetCount" runat="server" ErrorMessage="ﾋﾟｯｸｱｯﾌﾟ自動取得件数は0〜20の間で入力してください。" ControlToValidate="txtPickupAutoSetCount"
											MaximumValue="20" MinimumValue="0" ValidationGroup="Detail" Type="Integer">*</asp:RangeValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										ﾋﾟｯｸｱｯﾌﾟ<br />
										自動再実行可能分
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtPickupAutoIntervalMin" runat="server" MaxLength="2" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrPickupAutoIntervalMin" runat="server" ErrorMessage="ﾋﾟｯｸｱｯﾌﾟ自動再実行可能分数を入力してください。" ControlToValidate="txtPickupAutoIntervalMin"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RangeValidator ID="vdaPickupAutoIntervalMin" runat="server" ErrorMessage="ﾋﾟｯｸｱｯﾌﾟ自動再実行可能分数は5〜99の間で入力してください。" ControlToValidate="txtPickupAutoIntervalMin"
											MaximumValue="99" MinimumValue="5" ValidationGroup="Detail" Type="Integer">*</asp:RangeValidator>
									</td>
								</tr>
							</asp:PlaceHolder>
							<tr>
								<td class="tdHeaderStyle">
									画像掲示板で<br />
									属性を利用
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkBbsPicAttrFlag" runat="server" Text="画像掲示板で設定した属性を利用する。" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									動画掲示板で<br />
									属性を利用
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkBbsMovieAttrFlag" runat="server" Text="動画掲示板で設定した属性を利用する。" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									画像・動画の<br />
									属性変更を許可する。
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkSupportChgObjCatFlag" runat="server" Text="ｱｯﾌﾟﾛｰﾄﾞ後に画像・動画の属性変更を許可する。" />
								</td>
							</tr>
							<asp:PlaceHolder runat="server" ID="plcNewFriendIntro">
								<tr>
									<td class="tdHeaderStyle">
										男性=>出演者<br />
										紹介報酬P
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtIntroPointManToCast" runat="server" MaxLength="5" Width="50px"></asp:TextBox>&nbsp;&nbsp;紹介元が男性・紹介先が出演者の時に紹介もとの男性に入るﾎﾟｲﾝﾄを入力する。
										<asp:RequiredFieldValidator ID="vdrIntroPointManToCast" runat="server" ErrorMessage="男性=>出演者への紹介報酬ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtIntroPointManToCast"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										出演者=>男性<br />
										紹介報酬P
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtIntroPointCastToMan" runat="server" MaxLength="5" Width="50px"></asp:TextBox>&nbsp;&nbsp;紹介元が出演者・紹介先が男性の時に紹介もとの出演者に入る報酬額を入力する。
										<asp:RequiredFieldValidator ID="vdrIntroPointCastToMan" runat="server" ErrorMessage="出演者=>男性への紹介報酬額を入力して下さい。" ControlToValidate="txtIntroPointCastToMan"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										男性=>出演者<br />
										報酬発生額
									</td>
									<td class="tdDataStyle">
										紹介元が男性・紹介先が出演者の時に出演者の報酬累計額が<asp:TextBox ID="txtIntroManToCastMinPoint" runat="server" MaxLength="5" Width="50px"></asp:TextBox>を超えたら男性にﾎﾟｲﾝﾄを追加する。
										<asp:RequiredFieldValidator ID="vdrIntroManToCastMinPoint" runat="server" ErrorMessage="男性=>出演者への報酬発生額を入力して下さい。" ControlToValidate="txtIntroManToCastMinPoint"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										紹介元のｷｯｸﾊﾞｯｸ<br />
										ﾚｰﾄを利用
									</td>
									<td class="tdDataStyle">
										<asp:CheckBox ID="chkIntroducerRateOnFlag" runat="server" Text="ﾎﾟｲﾝﾄｷｯｸﾊﾞｯｸ時に紹介元のﾚｰﾄを利用する。" />
									</td>
								</tr>
							</asp:PlaceHolder>
							<asp:PlaceHolder ID="plcVisible4" runat="server">
								<tr>
									<td class="tdHeaderStyle">
										出演者<br />
										ﾘｯﾁｰﾉ検索可能条件
									</td>
									<td class="tdDataStyle">
										精算回数<asp:TextBox ID="txtCastRichinoSeekPaymentCnt" runat="server" MaxLength="5" Width="50px"></asp:TextBox>回以上
										<asp:RequiredFieldValidator ID="vdrCastRichinoSeekPaymentCnt" runat="server" ErrorMessage="ﾘｯﾁｰﾉ検索可能精算回数を入力して下さい。" ControlToValidate="txtCastRichinoSeekPaymentCnt"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>または&nbsp; 累計報酬金額<asp:TextBox ID="txtCastRichinoSeekPaymentAmt" runat="server" MaxLength="8" Width="80px"></asp:TextBox>円以上
										<asp:RequiredFieldValidator ID="vdrCastRichinoSeekPaymentAmt" runat="server" ErrorMessage="ﾘｯﾁｰﾉ検索可能累計報酬金額を入力して下さい。" ControlToValidate="txtCastRichinoSeekPaymentAmt"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
								</tr>
							</asp:PlaceHolder>
							<tr>
								<td class="tdHeaderStyle">
									HTMLﾍﾟｰｼﾞﾀｲﾄﾙ
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtPageTitle" runat="server" Width="420px" MaxLength="60"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrPageTitle" runat="server" ErrorMessage="HTMLﾍﾟｰｼﾞﾀｲﾄﾙを入力して下さい。" ControlToValidate="txtPageTitle" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									HTMLﾍﾟｰｼﾞｷｰﾜｰﾄﾞ
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtPageKeyword" runat="server" Width="420px" MaxLength="60"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrPageKeyword" runat="server" ErrorMessage="HTMLﾍﾟｰｼﾞｷｰﾜｰﾄﾞを入力して下さい。" ControlToValidate="txtPageKeyword" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									HTMLﾍﾟｰｼﾞ説明
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtPageDescription" runat="server" Width="420px" MaxLength="60"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrPageDescription" runat="server" ErrorMessage="HTMLﾍﾟｰｼﾞ説明を入力して下さい。" ControlToValidate="txtPageDescription" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									出演者掲示板書込上限
								</td>
								<td class="tdDataStyle" colspan="4">
									1日<asp:TextBox ID="txtCastWriteBbsLimit" runat="server" Width="50px" MaxLength="3" Style="text-align: right;"></asp:TextBox>回まで（未入力の場合制限なし）
									<asp:RegularExpressionValidator ID="vdeCastWriteBbsLimit" runat="server" ErrorMessage="出演者掲示板書込上限は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtCastWriteBbsLimit"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									出演者掲示板書込<br />
									不可時間
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtCastWriteBbsIntervalMin" runat="server" Width="50px" MaxLength="5" Style="text-align: right;"></asp:TextBox>分
									<asp:RequiredFieldValidator ID="vdrCastWriteBbsIntervalMin" runat="server" ErrorMessage="出演者掲示板書込間隔を入力して下さい。" ControlToValidate="txtCastWriteBbsIntervalMin"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeCastWriteBbsIntervalMin" runat="server" ErrorMessage="出演者掲示板書込間隔は1〜5桁の数字で入力して下さい。" ValidationExpression="\d{1,5}"
										ControlToValidate="txtCastWriteBbsIntervalMin" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									入金無男性会員<br />
									掲示板書込上限
								</td>
								<td class="tdDataStyle" colspan="4">
									1日<asp:TextBox ID="txtManWriteBbsLimit" runat="server" Width="50px" MaxLength="3" Style="text-align: right;"></asp:TextBox>回まで（未入力の場合制限なし）
									<asp:RegularExpressionValidator ID="vdeManWriteBbsLimit" runat="server" ErrorMessage="入金無男性会員掲示板書込上限は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtManWriteBbsLimit"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									入金無男性会員<br />
									掲示板書込不可時間
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtManWriteBbsIntervalMin" runat="server" Width="50px" MaxLength="5" Style="text-align: right;"></asp:TextBox>分
									<asp:RequiredFieldValidator ID="vdrManWriteBbsIntervalMin" runat="server" ErrorMessage="入金無男性会員掲示板書込間隔を入力して下さい。" ControlToValidate="txtManWriteBbsIntervalMin"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeManWriteBbsIntervalMin" runat="server" ErrorMessage="入金無男性会員掲示板書込間隔は1〜5桁の数字で入力して下さい。" ValidationExpression="\d{1,5}"
										ControlToValidate="txtManWriteBbsIntervalMin" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									入金有男性会員<br />
									掲示板書込上限
								</td>
								<td class="tdDataStyle" colspan="4">
									1日<asp:TextBox ID="txtPayManWriteBbsLimit" runat="server" Width="50px" MaxLength="3" Style="text-align: right;"></asp:TextBox>回まで（未入力の場合制限なし）
									<asp:RegularExpressionValidator ID="vdePayManWriteBbsLimit" runat="server" ErrorMessage="入金有男性会員掲示板書込上限は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}"
										ControlToValidate="txtPayManWriteBbsLimit" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									入金有男性会員<br />
									掲示板書込不可時間
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtPayManWriteBbsIntervalMin" runat="server" Width="50px" MaxLength="5" Style="text-align: right;"></asp:TextBox>分
									<asp:RequiredFieldValidator ID="vdrPayManWriteBbsIntervalMin" runat="server" ErrorMessage="入金有男性会員掲示板書込間隔を入力して下さい。" ControlToValidate="txtPayManWriteBbsIntervalMin"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdePayManWriteBbsIntervalMin" runat="server" ErrorMessage="入金有男性会員掲示板書込間隔は1〜5桁の数字で入力して下さい。" ValidationExpression="\d{1,5}"
										ControlToValidate="txtPayManWriteBbsIntervalMin" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									出演者別ｻｲﾄ登録時<br />
									ｷｬﾗｸﾀｰ自動作成
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkCastAutoRegistCharFlag" runat="server" Text="出演者が別ｻｲﾄに登録した際に、ﾁｪｯｸが入っているｻｲﾄにｷｬﾗｸﾀｰを自動作成する。" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									男性会員未入力時<br />
									ﾊﾝﾄﾞﾙ名
								</td>
								<td class="tdDataStyle">
									男性会員の登録時にﾊﾝﾄﾞﾙ名を取得しない場合、<asp:TextBox ID="txtManDefaultHandleNm" runat="server" Width="100px" MaxLength="10"></asp:TextBox>をﾃﾞﾌｫﾙﾄのﾊﾝﾄﾞﾙ名とする。
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ｲﾝﾎﾟｰﾄ出演者<br />
									初回ﾛｸﾞｲﾝ時ｱｸｼｮﾝ
								</td>
								<td class="tdDataStyle">
									ｲﾝﾎﾟｰﾄ出演者が初回ﾛｸﾞｲﾝした時、<asp:DropDownList ID="lstImpCastFirstLoginAct" runat="server" DataSourceID="dsImpFirstLoginAct" DataTextField="CODE_NM" DataValueField="CODE">
									</asp:DropDownList>へ移動する。
								</td>
							</tr>
							<tr>
								<!-- 下のtd classに注意 -->
								<td runat="server" class="tdHeaderStyle" id="tdEndHeader1">
									ｲﾝﾎﾟｰﾄ男性<br />
									初回ﾛｸﾞｲﾝ時ｱｸｼｮﾝ
								</td>
								<td runat="server" class="tdHeaderStyle2" id="tdEndHeader2">
									ｲﾝﾎﾟｰﾄ男性<br />
									初回ﾛｸﾞｲﾝ時ｱｸｼｮﾝ
								</td>
								<td class="tdDataStyle">
									ｲﾝﾎﾟｰﾄ男性が初回ﾛｸﾞｲﾝした時、<asp:DropDownList ID="lstImpManFirstLoginAct" runat="server" DataSourceID="dsImpFirstLoginAct" DataTextField="CODE_NM" DataValueField="CODE">
									</asp:DropDownList>へ移動する。
								</td>
							</tr>
							<asp:PlaceHolder ID="plcOldMainteArea" runat="server">
								<tr>
									<td class="tdHeaderStyle">
										ｵﾌﾗｲﾝｶﾞｲﾀﾞﾝｽ
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtOfflineGuidance" runat="server" Columns="60" MaxLength="100" Rows="2" TextMode="MultiLine" Width="497px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrOfflineGuidance" runat="server" ControlToValidate="txtOfflineGuidance" ErrorMessage="ｵﾌﾗｲﾝｶﾞｲﾀﾞﾝｽを入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										ﾛｸﾞｲﾝ済みｶﾞｲﾀﾞﾝｽ
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtLoginedGuidance" runat="server" Columns="60" MaxLength="100" Rows="2" TextMode="MultiLine" Width="497px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrLoginedGuidance" runat="server" ControlToValidate="txtLoginedGuidance" ErrorMessage="ﾛｸﾞｲﾝ済みｶﾞｲﾀﾞﾝｽを入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										待機中ｶﾞｲﾀﾞﾝｽ
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtWaittingGuidance" runat="server" Columns="60" MaxLength="100" Rows="2" TextMode="MultiLine" Width="497px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrWaittingGuidance" runat="server" ControlToValidate="txtWaittingGuidance" ErrorMessage="待機中ｶﾞｲﾀﾞﾝｽを入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										呼び出し中ｶﾞｲﾀﾞﾝｽ
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCallingGuidance" runat="server" Columns="60" MaxLength="100" Rows="2" TextMode="MultiLine" Width="497px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCallingGuidance" runat="server" ControlToValidate="txtCallingGuidance" ErrorMessage="呼び出し中ｶﾞｲﾀﾞﾝｽを入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										2SHOTﾁｬｯﾄ中ｶﾞｲﾀﾞﾝｽ
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtTalkingWShotGuidance" runat="server" Columns="60" MaxLength="100" Rows="2" TextMode="MultiLine" Width="497px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrTalkingWShotGuidance" runat="server" ControlToValidate="txtTalkingWShotGuidance" ErrorMessage="2SHOT中ｶﾞｲﾀﾞﾝｽを入力して下さい。"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										ﾊﾟｰﾃｨﾁｬｯﾄ中ｶﾞｲﾀﾞﾝｽ
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtTalkingPartyGuidance" runat="server" Columns="60" MaxLength="100" Rows="2" TextMode="MultiLine" Width="497px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrTalkingPartyGuidance" runat="server" ControlToValidate="txtTalkingPartyGuidance" ErrorMessage="ﾊﾟｰﾃｨﾁｬｯﾄ中ｶﾞｲﾀﾞﾝｽを入力して下さい。"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										TV電話OKｶﾞｲﾀﾞﾝｽ
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtConnectTypeVideoGuidance" runat="server" Columns="60" MaxLength="100" Rows="2" TextMode="MultiLine" Width="497px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrConnectTypeVideoGuidance" runat="server" ControlToValidate="txtConnectTypeVideoGuidance" ErrorMessage="TV電話OKｶﾞｲﾀﾞﾝｽを入力して下さい。"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										TV電話/音声OK<br />
										ｶﾞｲﾀﾞﾝｽ
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtConnectTypeBothGuidance" runat="server" Columns="60" MaxLength="100" Rows="2" TextMode="MultiLine" Width="497px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrConnectTypeBothGuidance" runat="server" ControlToValidate="txtConnectTypeBothGuidance" ErrorMessage="TV電話/音声OKｶﾞｲﾀﾞﾝｽを入力して下さい。"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										音声OKｶﾞｲﾀﾞﾝｽ
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtConnectTypeVoiceGuidance" runat="server" Columns="60" MaxLength="100" Rows="2" TextMode="MultiLine" Width="497px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrConnectTypeVoiceGuidance" runat="server" ControlToValidate="txtConnectTypeVoiceGuidance" ErrorMessage="音声OKｶﾞｲﾀﾞﾝｽを入力して下さい。"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										ﾍｯﾀﾞｰﾀｸﾞに追加
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtHeaderHtmlDoc" runat="server" Columns="60" MaxLength="1000" Rows="4" TextMode="MultiLine" Width="497px"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										ﾌｯﾀｰ文章
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFooterHtmlDoc" runat="server" Columns="60" MaxLength="1000" Rows="4" TextMode="MultiLine" Width="497px"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										<%= DisplayWordUtil.Replace("出演者新人ﾏｰｸ")%>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtNewFaceMark" runat="server" Columns="60" MaxLength="100" Rows="2" TextMode="MultiLine" Width="497px"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										返信ﾒｰﾙ利用設定
									</td>
									<td class="tdDataStyle">
										<asp:CheckBox ID="chkNoUseReturnMailFlag" runat="server" Text="返信時に返信ﾒｰﾙを使用せず、通常送信ﾒｰﾙを使用する。">
										</asp:CheckBox>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										ﾎﾟｲﾝﾄﾒｰﾙ成果集計日数
									</td>
									<td class="tdDataStyle">
										ｻｰﾋﾞｽﾎﾟｲﾝﾄ付ﾒｰﾙを送信してから<asp:TextBox ID="txtOutcomeCountDays" runat="server" MaxLength="3" Width="30px"></asp:TextBox>日間を集計の対象とする。
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										ﾄｯﾌﾟﾍﾟｰｼﾞ<br />
										<%= DisplayWordUtil.Replace("出演者表示条件")%>
									</td>
									<td class="tdDataStyle">
										<asp:DropDownList ID="lstTopPageSeekMode" runat="server" DataSourceID="dsTopPageSeekMode" DataTextField="CODE_NM" DataValueField="CODE" Width="180px">
										</asp:DropDownList>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										広告ｺｰﾄﾞ未指定時<br />
										年齢認証要求
									</td>
									<td class="tdDataStyle">
										<asp:CheckBox ID="chkNonAdRegistReqAgeCertFlag" runat="server" Text="男性会員登録時に広告ｺｰﾄﾞが未指定の場合、身分証を要求する。">
										</asp:CheckBox>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										ｻｲﾄHTML文章性別ﾁｪｯｸ
									</td>
									<td class="tdDataStyle">
										<asp:CheckBox ID="chkSiteHtmlDocSexCheckFlag" runat="server" Text="" />
									</td>
								</tr>
							</asp:PlaceHolder>
							<asp:PlaceHolder ID="plcMashupArea" runat="server">
								<tr>
									<td class="tdHeaderStyle2">
										PF会話通知区分
									</td>
									<td class="tdDataStyle">
										<asp:RadioButton ID="rdoNotifyTvTalk" runat="server" Text="TV電話のみ更新&nbsp" GroupName="NotifyTalk">
										</asp:RadioButton>
										<asp:RadioButton ID="rdoNotifyVoiceTalk" runat="server" Text="音声電話のみ更新&nbsp" GroupName="NotifyTalk">
										</asp:RadioButton>
										<asp:RadioButton ID="rdoNotifyBothTalk" runat="server" Text="全て更新&nbsp" GroupName="NotifyTalk">
										</asp:RadioButton>
									</td>
								</tr>
							</asp:PlaceHolder>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
					<fieldset class="fieldset-inner">
						<legend>[サイト画像]</legend>
						<table border="0" style="width: 640px">
							<div align="center">
								<asp:Label ID="lblStatus" runat="server" Text="※画像をアップロードした後も表示画像が変わらない場合は「F5」ボタンを押して画面を更新してください。"></asp:Label>
							</div>
							<div>
								<asp:Image ID="imgCastNoPic" runat="server" /><br />
								<asp:FileUpload ID="uldCastNoPic" runat="server" />&nbsp;<asp:Button ID="btnCastNoPic" runat="server" Text="写真なし出演者画像アップロード" Width="250px" OnClick="btnCastNoPic_Click"
									ValidationGroup="CastNoPic" />
								<asp:RequiredFieldValidator ID="vdrCastNoPic" runat="server" ControlToValidate="uldCastNoPic" ErrorMessage="アップロードファイルが入力されていません。" ValidationGroup="CastNoPic">*</asp:RequiredFieldValidator>
								<asp:RegularExpressionValidator ID="vdeCastNoPic" runat="server" ErrorMessage="アップロードファイルの拡張子はGifにしてください。" ValidationExpression=".*.(g|G)(i|I)(f|F)$" ControlToValidate="uldCastNoPic"
									ValidationGroup="CastNoPic">*</asp:RegularExpressionValidator>
							</div>
							<hr />
							<div>
								<asp:Image ID="imgCastNoPicSmall" runat="server" /><br />
								<asp:FileUpload ID="uldCastNoPicSmall" runat="server" />&nbsp;<asp:Button ID="btnCastNoPicSmall" runat="server" Text="写真なし出演者画像(小)アップロード" Width="250px"
									OnClick="btnCastNoPicSmall_Click" ValidationGroup="CastNoPicSmall" />
								<asp:RequiredFieldValidator ID="vdrCastNoPicSmall" runat="server" ControlToValidate="uldCastNoPicSmall" ErrorMessage="アップロードファイルが入力されていません。" ValidationGroup="CastNoPicSmall">*</asp:RequiredFieldValidator>
								<asp:RegularExpressionValidator ID="vdeCastNoPicSmall" runat="server" ErrorMessage="アップロードファイルの拡張子はGifにしてください。" ValidationExpression=".*.(g|G)(i|I)(f|F)$"
									ControlToValidate="uldCastNoPicSmall" ValidationGroup="CastNoPicSmall">*</asp:RegularExpressionValidator>
							</div>
							<hr />
							<div>
								<asp:Image ID="imgManNoPic" runat="server" /><br />
								<asp:FileUpload ID="uldManNoPic" runat="server" />&nbsp;<asp:Button ID="btnManNoPic" runat="server" Text="写真なし男性画像アップロード" Width="250px" OnClick="btnManNoPic_Click"
									ValidationGroup="ManNoPic" />
								<asp:RequiredFieldValidator ID="vdrManNoPic" runat="server" ControlToValidate="uldManNoPic" ErrorMessage="アップロードファイルが入力されていません。" ValidationGroup="ManNoPic">*</asp:RequiredFieldValidator>
								<asp:RegularExpressionValidator ID="vdeManNoPic" runat="server" ErrorMessage="アップロードファイルの拡張子はGifにしてください。" ValidationExpression=".*.(g|G)(i|I)(f|F)$" ControlToValidate="uldManNoPic"
									ValidationGroup="ManNoPic">*</asp:RegularExpressionValidator>
							</div>
						</table>
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[サイト一覧]</legend>
			<asp:GridView ID="grdSiteManagement" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsSiteManagement" AllowSorting="True"
				SkinID="GridViewColor">
				<Columns>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkSiteCd" runat="server" Text='<%# Eval("SITE_CD") %>' CommandArgument='<%# Eval("SITE_CD") %>' OnCommand="lnkSiteCd_Command" CausesValidation="False">
							</asp:LinkButton>
							<asp:Label ID="lblConnect" runat="server" Text='<%# Bind("SITE_NM") %>'></asp:Label>
						</ItemTemplate>
						<HeaderTemplate>
							サイトコード
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:BoundField DataField="REGIST_SERVICE_POINT" HeaderText="登録P">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:BoundField DataField="SUPPORT_EMAIL_ADDR" HeaderText="サポートメール" />
					<asp:BoundField DataField="INFO_EMAIL_ADDR" HeaderText="インフォメール" />
					<asp:BoundField DataField="SUPPORT_TEL" HeaderText="サポート電話" />
					<asp:BoundField DataField="MANAGEMENT_UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<a class="reccount">Record Count
				<%#GetRecCount() %>
			</a>
			<br />
			<a class="reccount">Current viewing page
				<%=grdSiteManagement.PageIndex + 1%>
				of
				<%=grdSiteManagement.PageCount%>
			</a>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsSiteManagement" runat="server" SelectMethod="GetPageCollection" TypeName="SiteManagement" SelectCountMethod="GetPageCount"
		EnablePaging="True" OnSelected="dsSiteManagement_Selected"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsWebFace" runat="server" SelectMethod="GetList" TypeName="WebFace"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsTopPageSeekMode" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="89" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsManIntroPointGetTiming" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="96" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsImpFirstLoginAct" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="08" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastRegistReportTiming" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="20" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdrSupportEmailAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdeSupportEmailAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdrInfoEMailAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vdeInfoEMailAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender69" TargetControlID="vdrReportEMailAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender70" TargetControlID="vdeReportEMailAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender84" TargetControlID="vdeCastRegistReportEMailAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender114" TargetControlID="vdrManUploadEmailAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender123" TargetControlID="vdrCastUploadEmailAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender124" TargetControlID="vdrAgentRegistEmailAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8" TargetControlID="vdrSupportTel" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9" TargetControlID="vdeSupportTel" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender17" TargetControlID="vdrCastNoPic" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender15" TargetControlID="vdeCastNoPic" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender18" TargetControlID="vdrCastNoPicSmall" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender16" TargetControlID="vdeCastNoPicSmall" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender19" TargetControlID="vdrManNoPic" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender20" TargetControlID="vdeManNoPic" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrRegistServicePoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeRegistServicePoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender115" TargetControlID="vdrRegistCastBonusPoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender116" TargetControlID="vdeRegistCastBonusPoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrInviteMailAvaHour" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10" TargetControlID="vdeInviteMailAvaHour" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender27" TargetControlID="vdcInviteMailAvaHour" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11" TargetControlID="vdrReInviteMailNAHour" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender12" TargetControlID="vdeReInviteMailNAHour" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender75" TargetControlID="vdrReBatchMailNAHour" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender76" TargetControlID="vdeReBatchMailNAHour" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender13" TargetControlID="vdrFindManElapasedDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender14" TargetControlID="vdeFindManElapasedDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender22" TargetControlID="vdrCmMailRxLimit" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender23" TargetControlID="vdeCmMailRxLimit" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender24" TargetControlID="vdrNewFaceDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender25" TargetControlID="vdeNewFaceDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender63" TargetControlID="vdrManNewFaceDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender64" TargetControlID="vdeManNewFaceDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender85" TargetControlID="vdrVoiceNewDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender86" TargetControlID="vdeVoiceNewDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender94" TargetControlID="vdrProfileMovieNewHours" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender95" TargetControlID="vdaProfileMovieNewHours" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender96" TargetControlID="vdrBbsNewHours" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender97" TargetControlID="vdaBbsNewHours" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender98" TargetControlID="vdrPicBbsNewHours" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender99" TargetControlID="vdaPicBbsNewHours" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender100" TargetControlID="vdrMovieBbsNewHours" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender101" TargetControlID="vdaMovieBbsNewHours" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender102" TargetControlID="vdrRxMailNewHours" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender103" TargetControlID="vdaRxMailNewHours" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender26" TargetControlID="vdrLoginMailIntervalHour" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender28" TargetControlID="vdeLoginMailIntervalHour" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender29" TargetControlID="vdrLogoutMailIntervalHour" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender30" TargetControlID="vdeLogoutMailIntervalHour" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender31" TargetControlID="vdrAproachMailIntervalHour" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender32" TargetControlID="vdeAproachMailIntervalHour" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender33" TargetControlID="vdrLogoutMailValidTalkSec" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender34" TargetControlID="vdeLogoutMailValidTalkSec" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender35" TargetControlID="vdeCastMinimumPayment" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender36" TargetControlID="vdrCastMinimumPayment" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender37" TargetControlID="vdeManIntroducerPoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender38" TargetControlID="vdrManIntroducerPoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender121" TargetControlID="vdeManIntroPoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender122" TargetControlID="vdrManIntroPoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender91" TargetControlID="vdrCastIntroducerPoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender90" TargetControlID="vdeCastIntroducerPoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender44" TargetControlID="vdrCastIntroPoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender39" TargetControlID="vdeCastIntroPoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender57" TargetControlID="vdrMeasuredRateServicePoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender58" TargetControlID="vdeMeasuredRateServicePoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender59" TargetControlID="vdrMeasuredRateLimitPoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender60" TargetControlID="vdeMeasuredRateLimitPoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender81" TargetControlID="vdrZeroSettleServicePoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender82" TargetControlID="vdeZeroSettleServicePoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender40" TargetControlID="vdrProfilePreviousGuidance" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender41" TargetControlID="vdrProfileNextGuidance" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender61" TargetControlID="vdrPreviousAccessKey" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender62" TargetControlID="vdrNextAccessKey" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender42" TargetControlID="vdrRankingCreateCount" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender43" TargetControlID="vdaRankingCreateCount" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender50" TargetControlID="vdrOnlineAdjustMinFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender51" TargetControlID="vdrOnlineAdjustMinTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender52" TargetControlID="vdrManOnlineMin" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender45" TargetControlID="vdrOfflineGuidance" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender21" TargetControlID="vdrLoginedGuidance" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender48" TargetControlID="vdrWaittingGuidance" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender46" TargetControlID="vdrTalkingWShotGuidance" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender47" TargetControlID="vdrTalkingPartyGuidance" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender49" TargetControlID="vdrCallingGuidance" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender66" TargetControlID="vdrConnectTypeVideoGuidance" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender67" TargetControlID="vdrConnectTypeBothGuidance" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender68" TargetControlID="vdrConnectTypeVoiceGuidance" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender65" TargetControlID="vdrTalkStartNotChargeSec" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender117" TargetControlID="vdrTalk2StartNotChargeSec" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender53" TargetControlID="vdePcRedirectUrl" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender71" TargetControlID="vdrMobileManDetailListCount" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender72" TargetControlID="vdrMobileManListCount" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender73" TargetControlID="vdrMobileWomanDetailListCount" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender74" TargetControlID="vdrMobileWomanListCount" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender77" TargetControlID="vdrPickupAutoSetCount" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender78" TargetControlID="vdaPickupAutoSetCount" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender79" TargetControlID="vdrPickupAutoIntervalMin" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender80" TargetControlID="vdaPickupAutoIntervalMin" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender87" TargetControlID="vdrIntroPointManToCast" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender88" TargetControlID="vdrIntroPointCastToMan" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender89" TargetControlID="vdrIntroManToCastMinPoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender54" TargetControlID="vdrPageTitle" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender55" TargetControlID="vdrPageKeyword" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender56" TargetControlID="vdrPageDescription" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender83" TargetControlID="vdeCastModifyAccountEMailAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender92" TargetControlID="vdrCastRichinoSeekPaymentCnt" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender93" TargetControlID="vdrCastRichinoSeekPaymentAmt" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender104" TargetControlID="vdrManNewRegistOkAmt" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender105" TargetControlID="vdrCastNewRegistOkAmt" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender106" TargetControlID="vdeCastWriteBbsLimit" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender107" TargetControlID="vdrCastWriteBbsIntervalMin" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender108" TargetControlID="vdeCastWriteBbsIntervalMin" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender109" TargetControlID="vdeManWriteBbsLimit" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender110" TargetControlID="vdrManWriteBbsIntervalMin" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender111" TargetControlID="vdeManWriteBbsIntervalMin" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender118" TargetControlID="vdePayManWriteBbsLimit" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender119" TargetControlID="vdrPayManWriteBbsIntervalMin" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender120" TargetControlID="vdePayManWriteBbsIntervalMin" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender112" TargetControlID="vdrProductNewDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender113" TargetControlID="vdeProductNewDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender29" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtRegistCastBonusPoint" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtNewFaceDays" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtManNewFaceDays" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtProfileMovieNewHours" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtBbsNewHours" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPicBbsNewHours" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtMovieBbsNewHours" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtRxMailNewHours" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtRankingCreateCount" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtOnlineAdjustMinFrom" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtOnlineAdjustMinTo" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtTalkStartNotChargeSec" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtManOnlineMin" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtMobileManDetailListCount" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtMobileManListCount" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtMobileWomanDetailListCount" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtMobileWomanListCount" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPickupAutoSetCount" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtIntroPointManToCast" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtIntroPointCastToMan" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtIntroManToCastMinPoint" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtCastIntroducerPoint" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtCastIntroPoint" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender23" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtManNewRegistOkAmt" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender24" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtCastNewRegistOkAmt" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender25" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtCastWriteBbsLimit" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender26" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtCastWriteBbsIntervalMin" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender27" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtManWriteBbsLimit" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender28" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtManWriteBbsIntervalMin" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender30" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPayManWriteBbsLimit" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender31" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPayManWriteBbsIntervalMin" />
</asp:Content>
