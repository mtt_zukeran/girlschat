<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastPicAttrTypeList.aspx.cs" Inherits="Site_CastPicAttrTypeList"
	Title="出演者写真属性区分設定" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="出演者写真属性区分設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									サイトコード
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
									</asp:DropDownList>
									<asp:Label ID="lblCastPicAttrTypeSeq" runat="server" Text="Label" Visible="false"></asp:Label>
								</td>
							</tr>
						</table>
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[属性区分内容]</legend>
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									属性区分名
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCastPicAttrTypeNm" runat="server" MaxLength="30" Width="200px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrCastPicAttrTypeNm" runat="server" ErrorMessage="属性区分名を入力して下さい。" ControlToValidate="txtCastPicAttrTypeNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									表示順位
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtPriority" runat="server" MaxLength="3" Width="60px"></asp:TextBox>&nbsp;
									<asp:RequiredFieldValidator ID="vdrPriority" runat="server" ErrorMessage="優先順位を入力して下さい。" ControlToValidate="txtPriority" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									アイテムNo.
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtItemNo" runat="server" MaxLength="2" Width="40px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrItemNo" runat="server" ErrorMessage="アイテムNo.を入力して下さい。" ControlToValidate="txtItemNo" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeItemNo" runat="server" ErrorMessage="アイテムNo.は01〜99の間で入力して下さい。" ValidationExpression="([0-1][1-9]|10|20|99)" ControlToValidate="txtItemNo"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[属性区分一覧]</legend>
			<table border="0" style="width: 640px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle2">
						サイトコード
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
						</asp:DropDownList>
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnRegist" Text="属性区分追加" CssClass="seekbutton" CausesValidation="False" OnClick="btnRegist_Click" />
			<br />
			<br />
			<asp:GridView ID="grdCastPicAttrType" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCastPicAttrType" AllowSorting="True"
				SkinID="GridViewColor">
				<Columns>
					<asp:TemplateField HeaderText="アイテムNo.">
						<ItemTemplate>
							<%# Eval("ITEM_NO") %>
							(
								<%# Eval("CAST_PIC_ATTR_TYPE_SEQ") %>								
							)
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField DataField="PRIORITY" HeaderText="表示順位">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkCastPicAttrTypeSeq" runat="server" Text='<%# Eval("CAST_PIC_ATTR_TYPE_NM") %>' CommandArgument='<%# Eval("CAST_PIC_ATTR_TYPE_SEQ") %>'
								OnCommand="lnkCastPicAttrTypeSeq_Command" CausesValidation="False">
							</asp:LinkButton>
						</ItemTemplate>
						<HeaderTemplate>
							属性区分名
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br>
				<a class="reccount">Current viewing page
					<%=grdCastPicAttrType.PageIndex + 1%>
					of
					<%=grdCastPicAttrType.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsCastPicAttrType" runat="server" SelectMethod="GetPageCollection" TypeName="CastPicAttrType" SelectCountMethod="GetPageCount"
		EnablePaging="True" OnSelected="dsCastPicAttrType_Selected" OnSelecting="dsCastPicAttrType_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPriority" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrCastPicAttrTypeNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdrPriority" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrItemNo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdeItemNo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
