<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UserViewList.aspx.cs" Inherits="Site_UserViewList" Title="ユーザービュー設定"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="ユーザービュー設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<asp:Literal ID="ltlUserColor" runat="server"></asp:Literal>
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<asp:Label ID="lblHtmlDocSeq" runat="server" Text="" Visible="false"></asp:Label>
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 1100px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									サイトコード
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
									</asp:DropDownList>
								</td>
								<td class="tdHeaderStyle">
									画面名称
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstProgramId" runat="server" DataSourceID="dsProgram" DataTextField="PROGRAM_NM" DataValueField="PROGRAM_ID" Width="400px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									端末種別
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstUserAgentType" runat="server" DataSourceID="dsUserAgentType" DataTextField="CODE_NM" DataValueField="CODE" Width="180px">
									</asp:DropDownList>
								</td>
								<td class="tdHeaderStyle">
									広告グループ
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstAdGroupCd" runat="server" DataSourceID="dsAdGroup" DataTextField="AD_GROUP_NM" DataValueField="AD_GROUP_CD" Width="180px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									ユーザーランク
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstUserRank" runat="server" DataSourceID="dsUserRank" DataTextField="USER_RANK_NM" DataValueField="USER_RANK" Width="180px">
									</asp:DropDownList>
								</td>
								<td class="tdHeaderStyle2">
									カテゴリ
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstActCategorySeq" runat="server" DataSourceID="dsActCategory" DataTextField="ACT_CATEGORY_NM" DataValueField="ACT_CATEGORY_SEQ"
										Width="180px">
									</asp:DropDownList>
								</td>
								<td class="tdHeaderSmallStyle3">
									順序番号
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtHtmlDocSubSeq" runat="server" Width="24px" MaxLength="2"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrHtmlDocSubSeq" runat="server" ErrorMessage="順序番号を入力して下さい。" ControlToValidate="txtHtmlDocSubSeq" ValidationGroup="Key">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeHtmlDocSubSeq" runat="server" ErrorMessage="順序番号は数字で入力して下さい。" ValidationExpression="\d{1,2}" ControlToValidate="txtHtmlDocSubSeq"
										ValidationGroup="Key">*</asp:RegularExpressionValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnSeekCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnSeekCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
				<table border="0">
					<tr>
						<td valign="top">
							<asp:Panel runat="server" ID="pnlSelect" Height="580px" Width="308px" ScrollBars="Auto">
								<fieldset >
									<legend>[アウトルック]</legend>
									<asp:GridView ID="grdSiteHtmlDoc" runat="server" AllowPaging="False" AutoGenerateColumns="False" DataSourceID="dsSiteHtmlDoc" SkinID="GridView" Width="230px"
										ShowHeader="False">
										<Columns>
											<asp:TemplateField>
												<ItemTemplate>
													<asp:LinkButton ID="lnkDoc" runat="server" CommandArgument='<%# Eval("HTML_DOC_SUB_SEQ") %>' Text='<%# string.Format("{0:D2} {1}",Eval("HTML_DOC_SUB_SEQ"),Eval("HTML_DOC_TITLE")) %>'
														OnCommand="lnkDoc_Command" Font-Bold="True" Font-Underline="True"></asp:LinkButton>
													<br />
													<asp:Literal ID="lblHtmlDoc1" runat="server" Text='<%# ReplaceForm(string.Format("<table class=\"userColor\" ><tr><td width=\"220px\">{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}</tr></td></table>",Eval("HTML_DOC1"), Eval("HTML_DOC2"), Eval("HTML_DOC3"), Eval("HTML_DOC4"), Eval("HTML_DOC5"), Eval("HTML_DOC6"),Eval("HTML_DOC7"), Eval("HTML_DOC8") ,Eval("HTML_DOC9"), Eval("HTML_DOC10"))) %>'></asp:Literal>
												</ItemTemplate>
												<ItemStyle HorizontalAlign="Left" />
											</asp:TemplateField>
										</Columns>
									</asp:GridView>
								</fieldset>
							</asp:Panel>
						</td>
						<td valign="top">
							<asp:Panel runat="server" ID="pnlDtl" Width="905px">
								<fieldset class="fieldset-inner">
									<legend>[ユーザービュー内容]</legend>
									<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seektopbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
									<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seektopbutton" OnClick="btnCancel_Click" CausesValidation="False" />
									<asp:LinkButton runat="server" ID="lnkVariableSample" OnClick="lnkVariableSample_Click">変数表</asp:LinkButton>&nbsp;
									<asp:LinkButton ID="lnkPictographSample" runat="server" OnClick="lnkPictographSample_Click">絵文字一覧</asp:LinkButton>
									<asp:Button runat="server" ID="btnDelete" Text="削除" OnClick="btnDelete_Click" ValidationGroup="Key" CssClass="delBtnStyle" />
									<asp:Label runat="server" ID="lblErrorInputSizeOver" Text="本文の入力文字数が制限値を超えています。" ForeColor="red" Visible="false" CssClass="f_clear"></asp:Label>
									<br clear="all" />
									$NO_TRANS_START;
									<table border="0" style="width: 880px" class="tableStyle">
										<tr>
											<td class="tdHeaderStyle" nowrap="nowrap">
												HTML名
											</td>
											<td class="tdDataStyle" colspan="3">
												<asp:TextBox ID="txtHtmlDocTitle" runat="server"></asp:TextBox>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderStyle" nowrap="nowrap">
												ﾀｲﾄﾙ
											</td>
											<td class="tdDataStyle" colspan="3">
												<asp:TextBox ID="txtPageTile" runat="server" Width="500px" MaxLength="100"></asp:TextBox>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderStyle" nowrap="nowrap">
												ｷｰﾜｰﾄﾞ
											</td>
											<td class="tdDataStyle" colspan="3">
												<asp:TextBox ID="txtPageKeyword" runat="server" Width="500px" MaxLength="100"></asp:TextBox>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderStyle" nowrap="nowrap">
												説明
											</td>
											<td class="tdDataStyle" colspan="3">
												<asp:TextBox ID="txtPageDescription" runat="server" Width="500px" MaxLength="100"></asp:TextBox>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderStyle" nowrap="nowrap">
												CSS名1
											</td>
											<td class="tdDataStyle">
												<asp:TextBox ID="txtCssFileNm1" runat="server" Width="300px" MaxLength="64"></asp:TextBox>
											</td>
                                            <td class="tdHeaderStyle" nowrap="nowrap">
                                                JavaScript名1
                                            </td>
                                            <td class="tdDataStyle">
                                                <asp:TextBox ID="txtJavascriptFileNm1" runat="server" MaxLength="64" Width="300px"></asp:TextBox>
                                            </td>
										</tr>
										<tr>
											<td class="tdHeaderStyle" nowrap="nowrap">
												CSS名2
											</td>
											<td class="tdDataStyle">
												<asp:TextBox ID="txtCssFileNm2" runat="server" Width="300px" MaxLength="64"></asp:TextBox>
											</td>
                                            <td class="tdHeaderStyle" nowrap="nowrap">
                                                JavaScript名2
                                            </td>
                                            <td class="tdDataStyle">
                                                <asp:TextBox ID="txtJavascriptFileNm2" runat="server" MaxLength="64" Width="300px"></asp:TextBox>
                                            </td>
										</tr>
 										<tr>
											<td class="tdHeaderStyle" nowrap="nowrap">
												CSS名3
											</td>
											<td class="tdDataStyle">
												<asp:TextBox ID="txtCssFileNm3" runat="server" Width="300px" MaxLength="64"></asp:TextBox>
											</td>
                                            <td class="tdHeaderStyle" nowrap="nowrap">
                                                JavaScript名3
                                            </td>
                                            <td class="tdDataStyle">
                                                <asp:TextBox ID="txtJavascriptFileNm3" runat="server" MaxLength="64" Width="300px"></asp:TextBox>
                                            </td>
										</tr>
										<tr>
											<td class="tdHeaderStyle" nowrap="nowrap">
												CSS名4
											</td>
											<td class="tdDataStyle">
												<asp:TextBox ID="txtCssFileNm4" runat="server" Width="300px" MaxLength="64"></asp:TextBox>
											</td>
                                            <td class="tdHeaderStyle" nowrap="nowrap">
                                                JavaScript名4
                                            </td>
                                            <td class="tdDataStyle">
                                                <asp:TextBox ID="txtJavascriptFileNm4" runat="server" MaxLength="64" Width="300px"></asp:TextBox>
                                            </td>
										</tr>
										<tr>
											<td class="tdHeaderStyle" nowrap="nowrap">
												CSS名5
											</td>
											<td class="tdDataStyle">
												<asp:TextBox ID="txtCssFileNm5" runat="server" Width="300px" MaxLength="64"></asp:TextBox>
											</td>
                                            <td class="tdHeaderStyle2" nowrap="nowrap">
                                                JavaScript名5
                                            </td>
                                            <td class="tdDataStyle">
                                                <asp:TextBox ID="txtJavascriptFileNm5" runat="server" MaxLength="64" Width="300px"></asp:TextBox>
                                            </td>
										</tr>
										<asp:PlaceHolder ID="plcIndex" runat="server">
											<tr>
												<td class="tdHeaderStyle" nowrap="nowrap">
													INDEX
												</td>
												<td class="tdDataStyle" colspan="3">
													<asp:DropDownList ID="lstTableIndex" runat="server" DataSourceID="dsTableIndex" DataTextField="CODE_NM" DataValueField="CODE" Width="250px">
													</asp:DropDownList>
												</td>
											</tr>
										</asp:PlaceHolder>
										<asp:Panel ID="pnlExImport" runat="server">
										<tr>
											<td class="tdHeaderStyle">
												外部読込
											</td>
											<td class="tdDataStyle" colspan="3">
												<asp:CheckBox ID="chkProtectImageFlag" Text="画像保護" runat="server" />
												&nbsp;
												<asp:CheckBox ID="chkEmojiToolFlag" Text="絵文字ツール" runat="server" />
											</td>
										</tr>
										</asp:Panel>
										<tr>
											<td class="tdHeaderStyle">
												ｸﾛｰﾗｰ対策
											</td>
											<td class="tdDataStyle" colspan="3">
												<asp:CheckBox ID="chkNoIndex" Text="noindex" runat="server" />
												&nbsp;
												<asp:CheckBox ID="chkNoFollow" Text="nofollow" runat="server" />
												&nbsp;
												<asp:CheckBox ID="chkNoArchive" Text="noarchive" runat="server" />
											</td>
										</tr>
										<tr>
											<td class="tdHeaderStyle2" nowrap="nowrap">
												HTML<br />
												文章
											</td>
											<td class="tdDataStyle" colspan="3">
												<pin:pinEdit ID="txtHtmlDoc" runat="server" UserMode="HTML" DisplayStyle="Style5" DocumentView="True" HtmlMode="Body" Ruler="False" ReturnMode="BR" SpellMode="Inline"
													ToolbarConfiguration="ToolbarDesigner" Toolbar="T09SE10116012136267;T22232427282930313233343536;B53SE54SE55" FormatMode="Classic" BorderWidth="1px"
													Height="500px" Width="780px" DocumentWidth="230" Font-Bold="False" FontSizeType="px" RelativeBaseImageUrl="/" RelativeBaseLinkUrl="/" RelativeImageRoot="/">
												</pin:pinEdit>
												<asp:TextBox ID="txtHtmlDocText" runat="server" TextMode="MultiLine" Height="500px" Width="780px" Visible="false"></asp:TextBox>
											</td>
										</tr>
									</table>
									$NO_TRANS_END;
									<asp:Button runat="server" ID="btnUpdate2" Text="更新" CssClass="seektopbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
									<asp:Button runat="server" ID="btnCancel2" Text="キャンセル" CssClass="seektopbutton" OnClick="btnCancel_Click" CausesValidation="False" />
									<asp:Button runat="server" ID="btnDelete2" Text="削除" CssClass="delBtnStyle" OnClick="btnDelete_Click" ValidationGroup="Key" />
								</fieldset>
							</asp:Panel>
						</td>
					</tr>
				</table>
			</fieldset>
		</asp:Panel>
		<fieldset class="f_clear">
			<legend>[ユーザービュー一覧]</legend>
			<table border="0" style="width:700px; margin-bottom:3px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle">
						サイトコード
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD">
						</asp:DropDownList>
					</td>
					<td class="tdHeaderStyle">
						広告グループ
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekAdGroupCd" runat="server" DataSourceID="dsSeekAdGroup" DataTextField="AD_GROUP_NM" DataValueField="AD_GROUP_CD">
						</asp:DropDownList>
					</td>
				</tr>
				<tr>
					<td class="tdHeaderStyle">
						端末種別
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekUserAgentType" runat="server" DataSourceID="dsUserAgentType" DataTextField="CODE_NM" DataValueField="CODE">
						</asp:DropDownList>
					</td>
					<td class="tdHeaderStyle2">
						カテゴリ
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekActCategorySeq" runat="server" DataSourceID="dsSeekActCategory" DataTextField="ACT_CATEGORY_NM" DataValueField="ACT_CATEGORY_SEQ">
						</asp:DropDownList>
					</td>
				</tr>
				<tr>
					<td class="tdHeaderStyle2">
						プログラムID
					</td>
					<td class="tdDataStyle" colspan="3">
						<asp:TextBox ID="txtSeekProgramId" runat="server" Text="" Width="300px"></asp:TextBox>
						※部分一致
					</td>
				</tr>
			</table>
			$NO_TRANS_START;
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnRegist" Text="ユーザービュー追加" CssClass="seekbutton" CausesValidation="False" OnClick="btnRegist_Click" />
			<asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" CausesValidation="False" OnClick="btnCSV_Click" />
			<br />
			<br />
			<asp:GridView ID="grdUserView" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsUserView" AllowSorting="True" SkinID="GridViewColor" OnSorting="grdUserView_Sorting">
				<Columns>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkUserView" runat="server" Text='<%# ShortProgramNm(Eval("PROGRAM_NM")) %>' CommandArgument='<%# string.Format("{0}:{1}:{2}:{3}:{4}:{5}:{6}",Eval("SITE_CD"),Eval("PROGRAM_ID"),Eval("USER_AGENT_TYPE"),Eval("AD_GROUP_CD"),Eval("USER_RANK"),Eval("ACT_CATEGORY_SEQ"),Eval("HTML_DOC_SEQ"))  %>'
								OnCommand="lnkUserView_Command" CausesValidation="False">
							</asp:LinkButton>
						</ItemTemplate>
						<HeaderTemplate>
							画面名称
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:BoundField DataField="PROGRAM_ID" HeaderText="プログラムID" SortExpression="PROGRAM_ID">
						<ItemStyle HorizontalAlign="Left" />
					</asp:BoundField>
					<asp:BoundField DataField="USER_AGENT_TYPE_NM" HeaderText="端末種別">
						<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
					</asp:BoundField>
					<asp:BoundField DataField="AD_GROUP_NM" HeaderText="広告グループ">
						<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
					</asp:BoundField>
					<asp:BoundField DataField="TABLE_INDEX_NM" HeaderText="INDEX">
						<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
					</asp:BoundField>
					<asp:BoundField DataField="UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False" SortExpression="UPDATE_DATE">
						<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
			</asp:GridView>
			$NO_TRANS_END;
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br />
				<a class="reccount">Current viewing page
					<%=grdUserView.PageIndex + 1%>
					of
					<%=grdUserView.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSiteHtmlDoc" runat="server" SelectMethod="GetListByDocSeq" TypeName="SiteHtmlDoc" OnSelecting="dsSiteHtmlDoc_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pHtmlDocSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsProgram" runat="server" SelectMethod="GetList" TypeName="Program">
		<SelectParameters>
			<asp:QueryStringParameter Name="pProgramRoot" QueryStringField="pgmroot" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsAdGroup" runat="server" SelectMethod="GetListIncDefault" TypeName="AdGroup" OnSelecting="dsAdGroup_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSeekAdGroup" runat="server" SelectMethod="GetListIncDefault" TypeName="AdGroup" OnSelecting="dsSeekAdGroup_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUserRank" runat="server" SelectMethod="GetListIncDefault" TypeName="UserRank" OnSelecting="dsUserRank_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsActCategory" runat="server" SelectMethod="GetListIncDefault" TypeName="ActCategory" OnSelecting="dsActCategory_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSeekActCategory" runat="server" SelectMethod="GetListIncDefault" TypeName="ActCategory" OnSelecting="dsSeekActCategory_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsTableIndex" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="39" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUserAgentType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="06" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUserView" runat="server" SelectMethod="GetPageCollection" TypeName="UserView" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsUserView_Selected" OnSelecting="dsUserView_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pProgramRoot" Type="String" />
			<asp:Parameter Name="pProgramId" Type="String" />
			<asp:Parameter Name="pUserAgentType" Type="String" />
			<asp:Parameter Name="pUserRank" Type="String" />
			<asp:Parameter Name="pAdGroupCd" Type="String" />
			<asp:Parameter Name="pActCategorySeq" Type="String" />
			<asp:Parameter Name="pSortExpression" Type="String" />
			<asp:Parameter Name="pSortDirection" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender3" runat="server" TargetControlID="btnUpdate2" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender4" runat="server" TargetControlID="btnDelete2" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrHtmlDocSubSeq" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeHtmlDocSubSeq" HighlightCssClass="validatorCallout" />
</asp:Content>
