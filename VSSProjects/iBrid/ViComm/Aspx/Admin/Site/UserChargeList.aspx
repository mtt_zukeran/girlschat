<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UserChargeList.aspx.cs" Inherits="Site_UserChargeList" Title="ユーザー課金設定"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="ユーザー課金設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									サイトコード
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblSiteCd" runat="server" Text=""></asp:Label>
									<asp:Label ID="lblSiteNm" runat="server" Text=""></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ユーザーランク
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblUserRank" runat="server" Text=""></asp:Label>
									<asp:Label ID="lblUserRankNm" runat="server" Text=""></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									カテゴリ
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstActCategorySeq" runat="server" Width="206px" DataSourceID="dsActCategory" DataTextField="ACT_CATEGORY_NM" DataValueField="ACT_CATEGORY_SEQ">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("キャストランク") %>
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstCastRank" runat="server" DataSourceID="dsCodeDtl" DataTextField="CODE_NM" DataValueField="CODE">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									キャンペーン区分
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstCampainCd" runat="server" DataSourceID="dsCampainCd" DataTextField="CAMPAIN_DESC" DataValueField="CAMPAIN_CD">
									</asp:DropDownList>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnUserRank" Text="ユーザーランク設定へ" CssClass="seekbutton" CausesValidation="false" PostBackUrl='<%# string.Format("UserRankList.aspx?sitecd={0}",lblSiteCd.Text)%>' />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[課金内容]</legend>
						<asp:PlaceHolder ID="plcHolder" runat="server">
							<table border="0" style="width: 720px" class="tableStyle">
								<tr align="center">
									<td class="tdHeaderSmallStyle">
									</td>
									<td class="tdHeaderSmallStyle">
										単位
									</td>
									<td class="tdHeaderSmallStyle">
										通常
									</td>
									<td class="tdHeaderSmallStyle">
										新規
									</td>
									<td class="tdHeaderSmallStyle">
										未入金
									</td>
									<td class="tdHeaderSmallStyle">
										未利用
									</td>
									<td class="tdHeaderSmallStyle">
										ﾌﾘｰﾀﾞｲｱﾙ
									</td>
									<asp:PlaceHolder ID="plcWhitePlan" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdHeaderSmallStyle" >
										ﾎﾜｲﾄﾌﾟﾗﾝ
									</td>
									</asp:PlaceHolder>
									<td class="tdHeaderSmallStyle">
										出演者<br />通話ｱﾌﾟﾘ<br />利用時
									</td>
									<td class="tdHeaderSmallStyle">
										男性<br />通話ｱﾌﾟﾘ<br />利用時
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm0" runat="server" Text="課金0" Width="140px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec0" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec0" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec0" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec0" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec0"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal0" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal0" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal0" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal0" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal0"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew0" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew0" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew0" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew0" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew0"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt0" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt0" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt0"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt0" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoReceipt0"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse0" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse0" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse0" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse0" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse0"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial0" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial0" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial0"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial0" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial0" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder0" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan0" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan0" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan0"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan0" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan0" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint0" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint0" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint0"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint0" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint0" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint0" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint0" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint0"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint0" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint0" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm1" runat="server" Text="課金1" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec1" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec1" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec1" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec1" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec1"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal1" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal1" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal1" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal1" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal1"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew1" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew1" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew1" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew1" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew1"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt1" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt1" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt1"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt1" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoReceipt1"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse1" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse1" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse1" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse1" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse1"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial1" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial1" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial1"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial1" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial1" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder1" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan1" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan1" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan1"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan1" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan1" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint1" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint1" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint1"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint1" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint1" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint1" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint1" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint1"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint1" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint1" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm2" runat="server" Text="課金2" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec2" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec2" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec2" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec2" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec2"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal2" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal2" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal2" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal2" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal2"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew2" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew2" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew2" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew2" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew2"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt2" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt2" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt2"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt2" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoReceipt2"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse2" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse2" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse2" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse2" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse2"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial2" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial2" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial2"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial2" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial2" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder2" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan2" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan2" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan2"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan2" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan2" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint2" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint2" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint2"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint2" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint2" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint2" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint2" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint2"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint2" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint2" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm3" runat="server" Text="課金3" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec3" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec3" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec3" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec3" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec3"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal3" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal3" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal3" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal3" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal3"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew3" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew3" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew3" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew3" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew3"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt3" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt3" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt3"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt3" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoReceipt3"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse3" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse3" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse3" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse3" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse3"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial3" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial3" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial3"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial3" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial3" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder3" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan3" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan3" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan3"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan3" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan3" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint3" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint3" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint3"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint3" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint3" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint3" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint3" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint3"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint3" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint3" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm4" runat="server" Text="課金4" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec4" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec4" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec4" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec4" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec4"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal4" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal4" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal4" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal4" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal4"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew4" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew4" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew4" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew4" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew4"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt4" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt4" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt4"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt4" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoReceipt4"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse4" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse4" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse4" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse4" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse4"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial4" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial4" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial4"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial4" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial4" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder4" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan4" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan4" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan4"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan4" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan4" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint4" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint4" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint4"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint4" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint4" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint4" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint4" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint4"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint4" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint4" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm5" runat="server" Text="課金5" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec5" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec5" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec5" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec5" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec5"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal5" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal5" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal5" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal5" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal5"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew5" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew5" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew5" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew5" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew5"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt5" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt5" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt5"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt5" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoReceipt5"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse5" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse5" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse5" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse5" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse5"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial5" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial5" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial5"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial5" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial5" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder5" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan5" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan5" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan5"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan5" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan5" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint5" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint5" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint5"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint5" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint5" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint5" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint5" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint5"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint5" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint5" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
                                <tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm6" runat="server" Text="課金6" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec6" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec6" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec6" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec6" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec6"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal6" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal6" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal6" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal6" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal6"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew6" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew6" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew6" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew6" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew6"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt6" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt6" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt6"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt6" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoReceipt6"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse6" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse6" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse6" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse6" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse6"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial6" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial6" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial6"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial6" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial6" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder6" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan6" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan6" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan6"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan6" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan6" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint6" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint6" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint6"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint6" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint6" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint6" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint6" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint6"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint6" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint6" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm7" runat="server" Text="課金7" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec7" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec7" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec7" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec7" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec7"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal7" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal7" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal7" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal7" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal7"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew7" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew7" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew7" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew7" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew7"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt7" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt7" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt7"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt7" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoReceipt7"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse7" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse7" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse7" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse7" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse7"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial7" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial7" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial7"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial7" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial7" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder7" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan7" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan7" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan7"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan7" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan7" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint7" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint7" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint7"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint7" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint7" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint7" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint7" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint7"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint7" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint7" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm8" runat="server" Text="課金8" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec8" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec8" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec8" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec8" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec8"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal8" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal8" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal8" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal8" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal8"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew8" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew8" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew8" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew8" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew8"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt8" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt8" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt8"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt8" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoReceipt8"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse8" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse8" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse8" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse8" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse8"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial8" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial8" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial8"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial8" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial8" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder8" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan8" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan8" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan8"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan8" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan8" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint8" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint8" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint8"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint8" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint8" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint8" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint8" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint8"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint8" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint8" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm9" runat="server" Text="課金9" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec9" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec9" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec9" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec9" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec9"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal9" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal9" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal9" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal9" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal9"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew9" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew9" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew9" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew9" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew9"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt9" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt9" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt9"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt9" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoReceipt9"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse9" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse9" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse9" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse9" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse9"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial9" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial9" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial9"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial9" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial9" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder9" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan9" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan9" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan9"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan9" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan9" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint9" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint9" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint9"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint9" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint9" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint9" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint9" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint9"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint9" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint9" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm10" runat="server" Text="課金10" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec10" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec10" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec10" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec10" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec10"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal10" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal10" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal10" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal10" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal10"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew10" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew10" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew10" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew10" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew10"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt10" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt10" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt10"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt10" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt10" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse10" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse10" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse10" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse10" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse10"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial10" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial10" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial10"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial10" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial10" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder10" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan10" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan10" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan10"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan10" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan10" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint10" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint10" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint10"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint10" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint10" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint10" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint10" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint10"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint10" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint10" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm11" runat="server" Text="課金11" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec11" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec11" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec11" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec11" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec11"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal11" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal11" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal11" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal11" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal11"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew11" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew11" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew11" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew11" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew11"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt11" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt11" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt11"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt11" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt11" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse11" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse11" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse11" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse11" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse11"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial11" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial11" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial11"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial11" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial11" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder11" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan11" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan11" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan11"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan11" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan11" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint11" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint11" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint11"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint11" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint11" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint11" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint11" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint11"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint11" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint11" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm12" runat="server" Text="課金12" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec12" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec12" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec12" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec12" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec12"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal12" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal12" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal12" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal12" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal12"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew12" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew12" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew12" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew12" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew12"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt12" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt12" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt12"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt12" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt12" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse12" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse12" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse12" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse12" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse12"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial12" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial12" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial12"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial12" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial12" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder12" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan12" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan12" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan12"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan12" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan12" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint12" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint12" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint12"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint12" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint12" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint12" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint12" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint12"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint12" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint12" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm13" runat="server" Text="課金13" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec13" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec13" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec13" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec13" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec13"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal13" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal13" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal13" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal13" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal13"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew13" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew13" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew13" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew13" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew13"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt13" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt13" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt13"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt13" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt13" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse13" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse13" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse13" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse13" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse13"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial13" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial13" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial13"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial13" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial13" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder13" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan13" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan13" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan13"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan13" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan13" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint13" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint13" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint13"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint13" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint13" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint13" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint13" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint13"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint13" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint13" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm14" runat="server" Text="課金14" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec14" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec14" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec14" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec14" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec14"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal14" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal14" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal14" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal14" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal14"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew14" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew14" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew14" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew14" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew14"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt14" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt14" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt14"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt14" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt14" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse14" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse14" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse14" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse14" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse14"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial14" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial14" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial14"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial14" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial14" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder14" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan14" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan14" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan14"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan14" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan14" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint14" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint14" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint14"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint14" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint14" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint14" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint14" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint14"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint14" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint14" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm15" runat="server" Text="課金15" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec15" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec15" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec15" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec15" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec15"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal15" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal15" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal15" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal15" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal15"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew15" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew15" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew15" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew15" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew15"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt15" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt15" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt15"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt15" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt15" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse15" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse15" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse15" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse15" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse15"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial15" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial15" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial15"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial15" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial15" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder15" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan15" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan15" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan15"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan15" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan15" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint15" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint15" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint15"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint15" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint15" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint15" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint15" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint15"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint15" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint15" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm16" runat="server" Text="課金16" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec16" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec16" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec16" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec16" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec16"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal16" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal16" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal16" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal16" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal16"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew16" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew16" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew16" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew16" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew16"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt16" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt16" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt16"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt16" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt16" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse16" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse16" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse16" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse16" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse16"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial16" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial16" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial16"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial16" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial16" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder16" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan16" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan16" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan16"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan16" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan16" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint16" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint16" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint16"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint16" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint16" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint16" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint16" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint16"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint16" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint16" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm17" runat="server" Text="課金17" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec17" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec17" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec17" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec17" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec17"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal17" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal17" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal17" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal17" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal17"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew17" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew17" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew17" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew17" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew17"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt17" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt17" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt17"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt17" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt17" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse17" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse17" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse17" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse17" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse17"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial17" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial17" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial17"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial17" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial17" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder17" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan17" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan17" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan17"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan17" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan17" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint17" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint17" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint17"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint17" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint17" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint17" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint17" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint17"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint17" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint17" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm18" runat="server" Text="課金18" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec18" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec18" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec18" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec18" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec18"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal18" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal18" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal18" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal18" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal18"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew18" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew18" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew18" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew18" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew18"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt18" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt18" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt18"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt18" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt18" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse18" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse18" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse18" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse18" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse18"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial18" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial18" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial18"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial18" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial18" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder18" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan18" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan18" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan18"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan18" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan18" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint18" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint18" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint18"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint18" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint18" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint18" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint18" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint18"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint18" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint18" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle">
										<asp:Label ID="lblChargeTypeNm19" runat="server" Text="課金19" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec19" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec19" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec19" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec19" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec19"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal19" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal19" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal19" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal19" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal19"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew19" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew19" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew19" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew19" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew19"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt19" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt19" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt19"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt19" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt19" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse19" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse19" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse19" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse19" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse19"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial19" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial19" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial19"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial19" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial19" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder19" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan19" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan19" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan19"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan19" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan19" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint19" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint19" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint19"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint19" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint19" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint19" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint19" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint19"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint19" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint19" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle">
										<asp:Label ID="lblChargeTypeNm20" runat="server" Text="課金20" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec20" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec20" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec20" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec20" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec20"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal20" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal20" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal20" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal20" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal20"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew20" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew20" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew20" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew20" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew20"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt20" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt20" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt20"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt20" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt20" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse20" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse20" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse20" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse20" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse20"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial20" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial20" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial20"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial20" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial20" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder20" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan20" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan20" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan20"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan20" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan20" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint20" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint20" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint20"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint20" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint20" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint20" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint20" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint20"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint20" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint20" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle">
										<asp:Label ID="lblChargeTypeNm21" runat="server" Text="課金21" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec21" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec21" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec21" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec21" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec21"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal21" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal21" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal21" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal21" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal21"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew21" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew21" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew21" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew21" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew21"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt21" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt21" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt21"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt21" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt21" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse21" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse21" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse21" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse21" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse21"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial21" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial21" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial21"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial21" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial21" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder21" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan21" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan21" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan21"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan21" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan21" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint21" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint21" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint21"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint21" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint21" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint21" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint21" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint21"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint21" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint21" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle">
										<asp:Label ID="lblChargeTypeNm22" runat="server" Text="課金22" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec22" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec22" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec22" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec22" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec22"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal22" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal22" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal22" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal22" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal22"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew22" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew22" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew22" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew22" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew22"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt22" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt22" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt22"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt22" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt22" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse22" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse22" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse22" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse22" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse22"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial22" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial22" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial22"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial22" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial22" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder22" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan22" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan22" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan22"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan22" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan22" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint22" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint22" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint22"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint22" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint22" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint22" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint22" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint22"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint22" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint22" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle">
										<asp:Label ID="lblChargeTypeNm23" runat="server" Text="課金23" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec23" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec23" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec23" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec23" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec23"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal23" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal23" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal23" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal23" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal23"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew23" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew23" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew23" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew23" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew23"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt23" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt23" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt23"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt23" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt23" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse23" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse23" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse23" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse23" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse23"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial23" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial23" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial23"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial23" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial23" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder23" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan23" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan23" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan23"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan23" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan23" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint23" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint23" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint23"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint23" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint23" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint23" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint23" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint23"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint23" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint23" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle">
										<asp:Label ID="lblChargeTypeNm24" runat="server" Text="課金24" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec24" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec24" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec24" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec24" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec24"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal24" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal24" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal24" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal24" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal24"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew24" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew24" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew24" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew24" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew24"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt24" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt24" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt24"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt24" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt24" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse24" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse24" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse24" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse24" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse24"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial24" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial24" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial24"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial24" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial24" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder24" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan24" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan24" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan24"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan24" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan24" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint24" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint24" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint24"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint24" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint24" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint24" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint24" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint24"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint24" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint24" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle">
										<asp:Label ID="lblChargeTypeNm25" runat="server" Text="課金25" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec25" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec25" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec25" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec25" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec25"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal25" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal25" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal25" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal25" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal25"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew25" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew25" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew25" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew25" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew25"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt25" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt25" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt25"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt25" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt25" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse25" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse25" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse25" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse25" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse25"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial25" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial25" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial25"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial25" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial25" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder25" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan25" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan25" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan25"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan25" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan25" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint25" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint25" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint25"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint25" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint25" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint25" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint25" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint25"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint25" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint25" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle">
										<asp:Label ID="lblChargeTypeNm26" runat="server" Text="課金26" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec26" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec26" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec26" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec26" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec26"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal26" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal26" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal26" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal26" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal26"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew26" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew26" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew26" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew26" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew26"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt26" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt26" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt26"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt26" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt26" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse26" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse26" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse26" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse26" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse26"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial26" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial26" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial26"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial26" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial26" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder26" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan26" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan26" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan26"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan26" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan26" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint26" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint26" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint26"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint26" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint26" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint26" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint26" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint26"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint26" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint26" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle">
										<asp:Label ID="lblChargeTypeNm27" runat="server" Text="課金27" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec27" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec27" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec27" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec27" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec27"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal27" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal27" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal27" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal27" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal27"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew27" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew27" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew27" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew27" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew27"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt27" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt27" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt27"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt27" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt27" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse27" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse27" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse27" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse27" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse27"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial27" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial27" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial27"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial27" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial27" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder27" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan27" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan27" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan27"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan27" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan27" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint27" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint27" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint27"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint27" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint27" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint27" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint27" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint27"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint27" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint27" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle">
										<asp:Label ID="lblChargeTypeNm28" runat="server" Text="課金28" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec28" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec28" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec28" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec28" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec28"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal28" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal28" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal28" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal28" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal28"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew28" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew28" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew28" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew28" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew28"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt28" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt28" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt28"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt28" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt28" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse28" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse28" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse28" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse28" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse28"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial28" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial28" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial28"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial28" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial28" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder28" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan28" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan28" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan28"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan28" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan28" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint28" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint28" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint28"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint28" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint28" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint28" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint28" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint28"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint28" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint28" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle">
										<asp:Label ID="lblChargeTypeNm29" runat="server" Text="課金29" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec29" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec29" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec29" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec29" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec29"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal29" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal29" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal29" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal29" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal29"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew29" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew29" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew29" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew29" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew29"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt29" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt29" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt29"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt29" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt29" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse29" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse29" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse29" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse29" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse29"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial29" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial29" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial29"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial29" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial29" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder29" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan29" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan29" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan29"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan29" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan29" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint29" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint29" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint29"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint29" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint29" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint29" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint29" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint29"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint29" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint29" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle">
										<asp:Label ID="lblChargeTypeNm30" runat="server" Text="課金30" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec30" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec30" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec30" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec30" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec30"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal30" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal30" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal30" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal30" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal30"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew30" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew30" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew30" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew30" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew30"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt30" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt30" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt30"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt30" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt30" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse30" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse30" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse30" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse30" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse30"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial30" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial30" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial30"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial30" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial30" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder30" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan30" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan30" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan30"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan30" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan30" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint30" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint30" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint30"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint30" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint30" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint30" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint30" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint30"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint30" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint30" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle">
										<asp:Label ID="lblChargeTypeNm31" runat="server" Text="課金31" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec31" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec31" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec31" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec31" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec31"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal31" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal31" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal31" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal31" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal31"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew31" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew31" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew31" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew31" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew31"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt31" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt31" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt31"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt31" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt31" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse31" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse31" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse31" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse31" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse31"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial31" runat="server" MaxLength="4" Width="31px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial31" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial31"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial31" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial31" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder31" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan31" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan31" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan31"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan31" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan31" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint31" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint31" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint31"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint31" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint31" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint31" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint31" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint31"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint31" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint31" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle">
										<asp:Label ID="lblChargeTypeNm32" runat="server" Text="課金32" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec32" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec32" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec32" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec32" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec32"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal32" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal32" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal32" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal32" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal32"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew32" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew32" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew32" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew32" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew32"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt32" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt32" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt32"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt32" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt32" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse32" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse32" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse32" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse32" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse32"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial32" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial32" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial32"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial32" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial32" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder32" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan32" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan32" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan32"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan32" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan32" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint32" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint32" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint32"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint32" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint32" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint32" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint32" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint32"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint32" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint32" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle">
										<asp:Label ID="lblChargeTypeNm33" runat="server" Text="課金33" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec33" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec33" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec33" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec33" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec33"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal33" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal33" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal33" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal33" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal33"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew33" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew33" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew33" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew33" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew33"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt33" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt33" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt33"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt33" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt33" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse33" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse33" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse33" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse33" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse33"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial33" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial33" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial33"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial33" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial33" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder33" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan33" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan33" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan33"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan33" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan33" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint33" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint33" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint33"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint33" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint33" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint33" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint33" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint33"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint33" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint33" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle">
										<asp:Label ID="lblChargeTypeNm34" runat="server" Text="課金34" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec34" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec34" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec34" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec34" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec34"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal34" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal34" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal34" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal34" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal34"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew34" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew34" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew34" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew34" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew34"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt34" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt34" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt34"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt34" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt34" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse34" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse34" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse34" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse34" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse34"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial34" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial34" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial34"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial34" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial34" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder34" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan34" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan34" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan34"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan34" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan34" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint34" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint34" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint34"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint34" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint34" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint34" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint34" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint34"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint34" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint34" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle">
										<asp:Label ID="lblChargeTypeNm35" runat="server" Text="課金35" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec35" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec35" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec35" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec35" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec35"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal35" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal35" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal35" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal35" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal35"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew35" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew35" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew35" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew35" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew35"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt35" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt35" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt35"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt35" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt35" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse35" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse35" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse35" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse35" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse35"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial35" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial35" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial35"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial35" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial35" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder35" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan35" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan35" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan35"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan35" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan35" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint35" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint35" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint35"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint35" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint35" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint35" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint35" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint35"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint35" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint35" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle">
										<asp:Label ID="lblChargeTypeNm36" runat="server" Text="課金36" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec36" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec36" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec36" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec36" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec36"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal36" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal36" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal36" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal36" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal36"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew36" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew36" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew36" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew36" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew36"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt36" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt36" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt36"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt36" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt36" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse36" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse36" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse36" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse36" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse36"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial36" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial36" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial36"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial36" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial36" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder36" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan36" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan36" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan36"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan36" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan36" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint36" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint36" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint36"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint36" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint36" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint36" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint36" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint36"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint36" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint36" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle">
										<asp:Label ID="lblChargeTypeNm37" runat="server" Text="課金37" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec37" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec37" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec37" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec37" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec37"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal37" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal37" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal37" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal37" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal37"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew37" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew37" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew37" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew37" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew37"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt37" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt37" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt37"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt37" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt37" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse37" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse37" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse37" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse37" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse37"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial37" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial37" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial37"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial37" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial37" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder37" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan37" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan37" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan37"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan37" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan37" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint37" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint37" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint37"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint37" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint37" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint37" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint37" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint37"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint37" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint37" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle">
										<asp:Label ID="lblChargeTypeNm38" runat="server" Text="課金38" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec38" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec38" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec38" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec38" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec38"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal38" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal38" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal38" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal38" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal38"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew38" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew38" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew38" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew38" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew38"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt38" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt38" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt38"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt38" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt38" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse38" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse38" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse38" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse38" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse38"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial38" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial38" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial38"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial38" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial38" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder38" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan38" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan38" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan38"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan38" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan38" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint38" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint38" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint38"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint38" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint38" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint38" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint38" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint38"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint38" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint38" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle">
										<asp:Label ID="lblChargeTypeNm39" runat="server" Text="課金39" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec39" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec39" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec39" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec39" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec39"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal39" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal39" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal39" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal39" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal39"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew39" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew39" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew39" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew39" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew39"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt39" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt39" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt39"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt39" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt39" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse39" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse39" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse39" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse39" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse39"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial39" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial39" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial39"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial39" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial39" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder39" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan39" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan39" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan39"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan39" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan39" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint39" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint39" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint39"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint39" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint39" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint39" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint39" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint39"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint39" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint39" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle">
										<asp:Label ID="lblChargeTypeNm40" runat="server" Text="課金40" Width="120px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeUnitSec40" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec40" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec40" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec40" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec40"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNormal40" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal40" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal40" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal40" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal40"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNew40" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNew40" runat="server" ErrorMessage="新規会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNew40" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNew40" runat="server" ErrorMessage="新規会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNew40"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoReceipt40" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt40" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt40"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt40" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt40" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointNoUse40" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoUse40" runat="server" ErrorMessage="未利用会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoUse40" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoUse40" runat="server" ErrorMessage="未利用会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoUse40"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointFreeDial40" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointFreeDial40" runat="server" ErrorMessage="フリーダイアル利用ポイントを入力して下さい。" ControlToValidate="txtChargePointFreeDial40"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointFreeDial40" runat="server" ErrorMessage="フリーダイアル利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointFreeDial40" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<asp:PlaceHolder ID="PlaceHolder40" runat="server" Visible ="<%# IsWhitePlan() %>">
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargePointWhitePlan40" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointWhitePlan40" runat="server" ErrorMessage="ホワイトプラン利用ポイントを入力して下さい。" ControlToValidate="txtChargePointWhitePlan40"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointWhitePlan40" runat="server" ErrorMessage="ホワイトプラン利用ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointWhitePlan40" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									</asp:PlaceHolder>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeCastTalkAppPoint40" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeCastTalkAppPoint40" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeCastTalkAppPoint40"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeCastTalkAppPoint40" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeCastTalkAppPoint40" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center">
										<asp:TextBox ID="txtChargeManTalkAppPoint40" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeManTalkAppPoint40" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeManTalkAppPoint40"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeManTalkAppPoint40" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時課金ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}"
											ControlToValidate="txtChargeManTalkAppPoint40" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
							</table>
						</asp:PlaceHolder>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[ユーザーランク一覧]</legend>
			<table border="0" style="width: 640px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle">
						サイトコード
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px" AutoPostBack="True" OnSelectedIndexChanged="lstSeekSiteCd_SelectedIndexChanged">
						</asp:DropDownList>
					</td>
				</tr>
				<tr>
					<td class="tdHeaderStyle">
						<%= DisplayWordUtil.Replace("キャストランク") %>
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekCastRank" runat="server" Width="180px" DataSourceID="dsCodeDtl" DataTextField="CODE_NM" DataValueField="CODE" AutoPostBack="True" OnSelectedIndexChanged="lstSeekCastRank_SelectedIndexChanged">
						</asp:DropDownList>
					</td>
				</tr>
				<tr>
					<td class="tdHeaderStyle2">
						キャンペーン区分
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekCampainCd" runat="server" DataSourceID="dsCampainCd" DataTextField="CAMPAIN_DESC" DataValueField="CAMPAIN_CD" AutoPostBack="True" OnSelectedIndexChanged="lstSeekCampainCd_SelectedIndexChanged">
						</asp:DropDownList>
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" CausesValidation="False" OnClick="btnCSV_Click" />
			<span style="color: #ff0033">*CSV出力時にはｻｲﾄ別,ｷｬﾝﾍﾟｰﾝ別/全ﾕｰｻﾞｰﾗﾝｸ/全ｷｬｽﾄﾗﾝｸの課金ﾃﾞｰﾀが出力されます。</span><br />
			<br />
			<asp:GridView ID="grdUserCharge" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsUserCharge" AllowSorting="True" SkinID="GridView"
				OnDataBound="grdUserCharge_DataBound" OnRowDataBound="grdUserCharge_RowDataBound">
				<Columns>
					<asp:TemplateField HeaderText="ｶﾃｺﾞﾘ">
						<ItemTemplate>
							<asp:HyperLink ID="lnkCategory" runat="server" Text='<%# Eval("ACT_CATEGORY_NM")%>' NavigateUrl='<%# string.Format("~/Site/ActCategoryList.aspx?sitecd={0}",Eval("SITE_CD"))%>'></asp:HyperLink>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:TemplateField HeaderText="ﾕｰｻﾞｰﾗﾝｸ">
						<ItemTemplate>
							<asp:HyperLink ID="lnkCategory" runat="server" Text='<%# Eval("USER_RANK_NM")%>' NavigateUrl='<%# string.Format("~/Site/UserRankList.aspx?sitecd={0}",Eval("SITE_CD"))%>'></asp:HyperLink>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:TemplateField HeaderText="ｺｰﾅｰ">
						<ItemTemplate>
							<asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Eval("CHARGE_TYPE_NM")%>' NavigateUrl='<%# string.Format("~/Site/UserChargeList.aspx?sitecd={0}&userrank={1}&sitenm={2}&userranknm={3}&actcategoryseq={4}&castrank={5}&campaincd={6}",Eval("SITE_CD"),Eval("USER_RANK"),Eval("SITE_NM"),Eval("USER_RANK_NM"),Eval("ACT_CATEGORY_SEQ"),Eval("CAST_RANK"),Eval("CAMPAIN_CD"))%>'></asp:HyperLink>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField DataField="CHARGE_UNIT_SEC" HeaderText="単位">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:BoundField DataField="CHARGE_POINT_NORMAL" HeaderText="通常会員">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:BoundField DataField="CHARGE_POINT_NEW" HeaderText="新規会員">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:BoundField DataField="CHARGE_POINT_NO_RECEIPT" HeaderText="未入金会員">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:BoundField DataField="CHARGE_POINT_NO_USE" HeaderText="未利用会員">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:BoundField DataField="CHARGE_POINT_FREE_DIAL" HeaderText="ﾌﾘｰﾀﾞｲｱﾙ">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:BoundField DataField="CHARGE_POINT_WHITE_PLAN" HeaderText="ﾎﾜｲﾄﾌﾟﾗﾝ">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:BoundField DataField="CHARGE_CAST_TALK_APP_POINT" HeaderText="出演者通話ｱﾌﾟﾘ利用" HeaderStyle-Wrap="false">
						<ItemStyle HorizontalAlign="Right"/>
					</asp:BoundField>
					<asp:BoundField DataField="CHARGE_MAN_TALK_APP_POINT" HeaderText="男性通話ｱﾌﾟﾘ利用" HeaderStyle-Wrap="false">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>						
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br />
				<a class="reccount">Current viewing page
					<%=grdUserCharge.PageIndex + 1%>
					of
					<%=grdUserCharge.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsActCategory" runat="server" SelectMethod="GetList" TypeName="ActCategory">
		<SelectParameters>
			<asp:QueryStringParameter Name="pSiteCd" QueryStringField="sitecd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUserCharge" runat="server" SelectMethod="GetPageCollection" TypeName="UserCharge" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsUserCharge_Selected" OnSelecting="dsUserCharge_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pCastRank" Type="String" />
			<asp:Parameter Name="pCampainCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCodeDtl" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="42" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCampainCd" runat="server" SelectMethod="GetList" TypeName="Campain">
		<SelectParameters>
			<asp:QueryStringParameter Name="pSiteCd" QueryStringField="sitecd" Type="String" />
			<asp:Parameter DefaultValue="1" Name="pCampainType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
