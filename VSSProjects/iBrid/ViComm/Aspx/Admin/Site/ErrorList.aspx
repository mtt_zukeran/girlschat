﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ErrorList.aspx.cs" Inherits="Site_ErrorList" Title="エラーメッセージ管理" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="エラーメッセージ管理"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[エラーメッセージ情報]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					<asp:Label ID="lblErrorCd" runat="server" Text="" Visible="false"></asp:Label>
					<table border="0" style="width: 800px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle2">
								エラー詳細
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtErrorDtl" runat="server" MaxLength="3000" Width="600px" Rows="6" TextMode="MultiLine"></asp:TextBox>
								<asp:RequiredFieldValidator ID="vdrErrorDtl" runat="server" ErrorMessage="エラー詳細を入力して下さい。" ControlToValidate="txtErrorDtl" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								<asp:CustomValidator ID="vdcErrorDtl" runat="server" ControlToValidate="txtErrorDtl" ErrorMessage="文字数を減らしてください。" OnServerValidate="vdcErrorDtl_ServerValidate"
									ValidationGroup="Detail">*</asp:CustomValidator>
							</td>
						</tr>
					</table>
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
					<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[エラーメッセージ管理]</legend>
			<table border="0" style="width: 640px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle2">
						サイトコード
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
						</asp:DropDownList>
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<br />
			<br />
			<asp:GridView ID="grdError" runat="server" AllowPaging="True" DataSourceID="dsError" AllowSorting="True" SkinID="GridViewColor" AutoGenerateColumns="False">
				<Columns>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkErrorCd" runat="server" Text='<%# Eval("ERROR_CD") %>' CommandArgument='<%# Eval("ERROR_CD") %>' OnCommand="lnkErrorCd_Command"
								CausesValidation="False"></asp:LinkButton>
						</ItemTemplate>
						<HeaderTemplate>
							コード
						</HeaderTemplate>
					</asp:TemplateField>
					<asp:BoundField DataField="ERROR_DTL" HeaderText="エラー詳細">
						<ItemStyle HorizontalAlign="Left" Width="700px" />
					</asp:BoundField>
					<asp:BoundField DataField="UPDATE_DATE" HeaderText="更新日付" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%# GetRecCount() %>
				</a>
				<br>
				<a class="reccount">Current viewing page
					<%=grdError.PageIndex + 1%>
					of
					<%=grdError.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsError" runat="server" SelectMethod="GetPageCollection" SelectCountMethod="GetPageCount" OnSelected="dsError_Selected" EnablePaging="True"
		TypeName="Error" OnSelecting="dsError_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrErrorDtl" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdcErrorDtl" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
