<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UnauthCastInviteScheduleList.aspx.cs" Inherits="Site_UnauthCastInviteScheduleList"
	Title="未認証キャスト勧誘スケジュール" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="未認証キャスト勧誘スケジュール"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey" Width="350px">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 300px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									サイト名
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblSiteCd" runat="server" Text="" Visible="false"></asp:Label>
									<asp:Label ID="lblSiteNm" runat="server" Text=""></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									勧誘レベル
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblInviteLevel" runat="server" Text=""></asp:Label>
									<asp:Label ID="lblInviteLevelNm" runat="server" Text=""></asp:Label>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnInviteLevel" Text="勧誘レベル設定へ" CssClass="seekbutton" CausesValidation="false" PostBackUrl='<%# string.Format("UnauthCastInviteLevelList.aspx?sitecd={0}",lblSiteCd.Text)%>' />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl" Width="740px">
					<fieldset class="fieldset-inner">
						<legend>[スケジュール内容]</legend>
						<asp:PlaceHolder ID="plcHolder" runat="server">
							<table border="1" style="width: 700px" class="tableStyle">
								<tr align="center">
									<td class="HeaderStyle" align="center">
										勧誘回数<br />
										／日
									</td>
									<td class="HeaderStyle" align="center">
										勧誘開始<br />
										時間
									</td>
									<td class="HeaderStyle" align="center">
										勧誘終了<br />
										時間
									</td>
									<td class="HeaderStyle" align="center">
										ﾒｰﾙﾃﾝﾌﾟﾚｰﾄ
									</td>
									<td class="HeaderStyle" align="center">
										最終実行日時
									</td>
								</tr>
								<tr>
									<td class="tdDataStyle" align="right">
										<asp:Label ID="lblInviteCountPerDay1" runat="server" Width="70px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:TextBox ID="txtInviteStartTime1" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
										<asp:RegularExpressionValidator ID="vdeInviteStartTime1" runat="server" ErrorMessage="開始時刻は「HH:MM」形式で入力して下さい。" ValidationExpression="^([0-1][0-9]|[2][0-3]):[0-5][0-9]"
											ControlToValidate="txtInviteStartTime1" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:TextBox ID="txtInviteEndTime1" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
										<asp:RegularExpressionValidator ID="vdeInviteEndTime1" runat="server" ErrorMessage="終了時刻は「HH:MM」形式で入力して下さい。" ValidationExpression="^([0-1][0-9]|[2][0-3]):[0-5][0-9]"
											ControlToValidate="txtInviteEndTime1" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
										<asp:CompareValidator ID="vdcInviteEndTime1" runat="server" ControlToCompare="txtInviteStartTime1" ControlToValidate="txtInviteEndTime1" ErrorMessage="終了時刻＞開始時刻となっています。"
											Operator="GreaterThanEqual" ValidationGroup="Detail">*</asp:CompareValidator>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:DropDownList ID="lstMailTemplateNo1" runat="server" DataSourceID="dsMailTemplate" DataTextField="TEMPLATE_NM" DataValueField="MAIL_TEMPLATE_NO" Width="309px">
										</asp:DropDownList>
										<asp:CustomValidator ID="vdcMailTemplateNo1" runat="server" ErrorMessage="メールテンプレートが未選択です。" OnServerValidate="vdcMailTemplateNo_ServerValidate" ValidationGroup="Detail">*</asp:CustomValidator>
										<asp:RequiredFieldValidator ID="vdrMailTemplateNo1" runat="server" ErrorMessage="メールテンプレートが未選択です。" ControlToValidate="lstMailTemplateNo1" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:Label ID="lblInviteLastExecDate11" runat="server" Width="130px"></asp:Label>
									</td>
								</tr>
								<tr>
									<td class="tdDataStyle" align="right">
										<asp:Label ID="lblInviteCountPerDay2" runat="server"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:TextBox ID="txtInviteStartTime2" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
										<asp:RegularExpressionValidator ID="vdeInviteStartTime2" runat="server" ErrorMessage="開始時刻は「HH:MM」形式で入力して下さい。" ValidationExpression="^([0-1][0-9]|[2][0-3]):[0-5][0-9]"
											ControlToValidate="txtInviteStartTime2" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:TextBox ID="txtInviteEndTime2" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
										<asp:RegularExpressionValidator ID="vdeInviteEndTime2" runat="server" ErrorMessage="終了時刻は「HH:MM」形式で入力して下さい。" ValidationExpression="^([0-1][0-9]|[2][0-3]):[0-5][0-9]"
											ControlToValidate="txtInviteEndTime2" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
										<asp:CompareValidator ID="vdcInviteEndTime2" runat="server" ControlToCompare="txtInviteStartTime2" ControlToValidate="txtInviteEndTime2" ErrorMessage="終了時刻＞開始時刻となっています。"
											Operator="GreaterThanEqual" ValidationGroup="Detail">*</asp:CompareValidator>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:DropDownList ID="lstMailTemplateNo2" runat="server" DataSourceID="dsMailTemplate" DataTextField="TEMPLATE_NM" DataValueField="MAIL_TEMPLATE_NO" Width="309px">
										</asp:DropDownList>
										<asp:CustomValidator ID="vdcMailTemplateNo2" runat="server" ErrorMessage="メールテンプレートが未選択です。" OnServerValidate="vdcMailTemplateNo_ServerValidate" ValidationGroup="Detail">*</asp:CustomValidator>
										<asp:RequiredFieldValidator ID="vdrMailTemplateNo2" runat="server" ErrorMessage="メールテンプレートが未選択です。" ControlToValidate="lstMailTemplateNo2" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:Label ID="lblInviteLastExecDate12" runat="server" Width="130px"></asp:Label>
									</td>
								</tr>
								<tr>
									<td class="tdDataStyle" align="right">
										<asp:Label ID="lblInviteCountPerDay3" runat="server"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:TextBox ID="txtInviteStartTime3" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
										<asp:RegularExpressionValidator ID="vdeInviteStartTime3" runat="server" ErrorMessage="開始時刻は「HH:MM」形式で入力して下さい。" ValidationExpression="^([0-1][0-9]|[2][0-3]):[0-5][0-9]"
											ControlToValidate="txtInviteStartTime3" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:TextBox ID="txtInviteEndTime3" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
										<asp:RegularExpressionValidator ID="vdeInviteEndTime3" runat="server" ErrorMessage="終了時刻は「HH:MM」形式で入力して下さい。" ValidationExpression="^([0-1][0-9]|[2][0-3]):[0-5][0-9]"
											ControlToValidate="txtInviteEndTime3" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
										<asp:CompareValidator ID="vdcInviteEndTime3" runat="server" ControlToCompare="txtInviteStartTime3" ControlToValidate="txtInviteEndTime3" ErrorMessage="終了時刻＞開始時刻となっています。"
											Operator="GreaterThanEqual" ValidationGroup="Detail">*</asp:CompareValidator>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:DropDownList ID="lstMailTemplateNo3" runat="server" DataSourceID="dsMailTemplate" DataTextField="TEMPLATE_NM" DataValueField="MAIL_TEMPLATE_NO" Width="309px">
										</asp:DropDownList>
										<asp:CustomValidator ID="vdcMailTemplateNo3" runat="server" ErrorMessage="メールテンプレートが未選択です。" OnServerValidate="vdcMailTemplateNo_ServerValidate" ValidationGroup="Detail">*</asp:CustomValidator>
										<asp:RequiredFieldValidator ID="vdrMailTemplateNo3" runat="server" ErrorMessage="メールテンプレートが未選択です。" ControlToValidate="lstMailTemplateNo3" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:Label ID="lblInviteLastExecDate13" runat="server" Width="130px"></asp:Label>
									</td>
								</tr>
								<tr>
									<td class="tdDataStyle" align="right">
										<asp:Label ID="lblInviteCountPerDay4" runat="server"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:TextBox ID="txtInviteStartTime4" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
										<asp:RegularExpressionValidator ID="vdeInviteStartTime4" runat="server" ErrorMessage="開始時刻は「HH:MM」形式で入力して下さい。" ValidationExpression="^([0-1][0-9]|[2][0-3]):[0-5][0-9]"
											ControlToValidate="txtInviteStartTime4" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:TextBox ID="txtInviteEndTime4" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
										<asp:RegularExpressionValidator ID="vdeInviteEndTime4" runat="server" ErrorMessage="終了時刻は「HH:MM」形式で入力して下さい。" ValidationExpression="^([0-1][0-9]|[2][0-3]):[0-5][0-9]"
											ControlToValidate="txtInviteEndTime4" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
										<asp:CompareValidator ID="vdcInviteEndTime4" runat="server" ControlToCompare="txtInviteStartTime4" ControlToValidate="txtInviteEndTime4" ErrorMessage="終了時刻＞開始時刻となっています。"
											Operator="GreaterThanEqual" ValidationGroup="Detail">*</asp:CompareValidator>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:DropDownList ID="lstMailTemplateNo4" runat="server" DataSourceID="dsMailTemplate" DataTextField="TEMPLATE_NM" DataValueField="MAIL_TEMPLATE_NO" Width="309px">
										</asp:DropDownList>
										<asp:CustomValidator ID="vdcMailTemplateNo4" runat="server" ErrorMessage="メールテンプレートが未選択です。" OnServerValidate="vdcMailTemplateNo_ServerValidate" ValidationGroup="Detail">*</asp:CustomValidator>
										<asp:RequiredFieldValidator ID="vdrMailTemplateNo4" runat="server" ErrorMessage="メールテンプレートが未選択です。" ControlToValidate="lstMailTemplateNo4" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:Label ID="lblInviteLastExecDate14" runat="server" Width="130px"></asp:Label>
									</td>
								</tr>
								<tr>
									<td class="tdDataStyle" align="right">
										<asp:Label ID="lblInviteCountPerDay5" runat="server"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:TextBox ID="txtInviteStartTime5" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
										<asp:RegularExpressionValidator ID="vdeInviteStartTime5" runat="server" ErrorMessage="開始時刻は「HH:MM」形式で入力して下さい。" ValidationExpression="^([0-1][0-9]|[2][0-3]):[0-5][0-9]"
											ControlToValidate="txtInviteStartTime5" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:TextBox ID="txtInviteEndTime5" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
										<asp:RegularExpressionValidator ID="vdeInviteEndTime5" runat="server" ErrorMessage="終了時刻は「HH:MM」形式で入力して下さい。" ValidationExpression="^([0-1][0-9]|[2][0-3]):[0-5][0-9]"
											ControlToValidate="txtInviteEndTime5" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
										<asp:CompareValidator ID="vdcInviteEndTime5" runat="server" ControlToCompare="txtInviteStartTime5" ControlToValidate="txtInviteEndTime5" ErrorMessage="終了時刻＞開始時刻となっています。"
											Operator="GreaterThanEqual" ValidationGroup="Detail">*</asp:CompareValidator>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:DropDownList ID="lstMailTemplateNo5" runat="server" DataSourceID="dsMailTemplate" DataTextField="TEMPLATE_NM" DataValueField="MAIL_TEMPLATE_NO" Width="309px">
										</asp:DropDownList>
										<asp:CustomValidator ID="vdcMailTemplateNo5" runat="server" ErrorMessage="メールテンプレートが未選択です。" OnServerValidate="vdcMailTemplateNo_ServerValidate" ValidationGroup="Detail">*</asp:CustomValidator>
										<asp:RequiredFieldValidator ID="vdrMailTemplateNo5" runat="server" ErrorMessage="メールテンプレートが未選択です。" ControlToValidate="lstMailTemplateNo5" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:Label ID="lblInviteLastExecDate15" runat="server" Width="130px"></asp:Label>
									</td>
								</tr>
							</table>
						</asp:PlaceHolder>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[スケジュール一覧]</legend>
			<table border="0" style="width: 750px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle2">
						サイト名
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
						</asp:DropDownList>
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<br />
			<br />
			<asp:GridView ID="grdInviteSchedule" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsInviteSchedule" AllowSorting="True"
				SkinID="GridViewColor" OnDataBound="grdInviteSchedule_DataBound">
				<Columns>
					<asp:TemplateField HeaderText="勧誘<BR/>レベル">
						<ItemTemplate>
							<asp:HyperLink ID="lnkInviteLevel" runat="server" Text='<%# Eval("INVITE_LEVEL")%>' NavigateUrl='<%# string.Format("~/Site/UnauthCastInviteScheduleList.aspx?sitecd={0}&invitelevel={1}",Eval("SITE_CD"),Eval("INVITE_LEVEL"))%>'></asp:HyperLink>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Center" />
					</asp:TemplateField>
					<asp:TemplateField HeaderText="勧誘回数<BR/>／日">
						<ItemTemplate>
							<asp:Label ID="lnkInviteSchedule" runat="server" Text='<%# Eval("INVITE_COUNT_PER_DAY")%>'></asp:Label>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Right" />
					</asp:TemplateField>
					<asp:TemplateField HeaderText="勧誘<BR/>開始日">
						<ItemTemplate>
							<asp:Label ID="lnkInviteSchedule" runat="server" Text='<%# Eval("INVITE_START_TIME")%>'></asp:Label>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Center" />
					</asp:TemplateField>
					<asp:TemplateField HeaderText="勧誘<BR/>終了日">
						<ItemTemplate>
							<asp:Label ID="lnkInviteSchedule" runat="server" Text='<%# Eval("INVITE_END_TIME")%>'></asp:Label>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Center" />
					</asp:TemplateField>
					<asp:BoundField DataField="TEMPLATE_NM" HeaderText="ﾒｰﾙﾃﾝﾌﾟﾚｰﾄ">
						<ItemStyle HorizontalAlign="Left" />
					</asp:BoundField>
					<asp:BoundField DataField="Invite_LAST_EXEC_DATE" HeaderText="最終実行日時">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br />
				<a class="reccount">Current viewing page
					<%=grdInviteSchedule.PageIndex + 1%>
					of
					<%=grdInviteSchedule.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsInviteSchedule" runat="server" SelectMethod="GetPageCollection" TypeName="UnauthCastInviteSchedule" SelectCountMethod="GetPageCount"
		EnablePaging="True" OnSelected="dsInviteSchedule_Selected" OnSelecting="dsInviteSchedule_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pInviteLevel" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsMailTemplate" runat="server" SelectMethod="GetListByTemplateType" TypeName="MailTemplate" OnSelecting="dsMailTemplate_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pMailTemplateType" Type="String" />
			<asp:Parameter DefaultValue="2" Name="pSexCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdeInviteStartTime1" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeInviteStartTime2" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdeInviteStartTime3" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdeInviteStartTime4" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdeInviteStartTime5" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdeInviteEndTime1" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vdeInviteEndTime2" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8" TargetControlID="vdeInviteEndTime3" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9" TargetControlID="vdeInviteEndTime4" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10" TargetControlID="vdeInviteEndTime5" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11" TargetControlID="vdcInviteEndTime1" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender12" TargetControlID="vdcInviteEndTime2" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender13" TargetControlID="vdcInviteEndTime3" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender14" TargetControlID="vdcInviteEndTime4" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender15" TargetControlID="vdcInviteEndTime5" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender16" TargetControlID="vdrMailTemplateNo1" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender17" TargetControlID="vdrMailTemplateNo2" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender18" TargetControlID="vdrMailTemplateNo3" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender19" TargetControlID="vdrMailTemplateNo4" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender20" TargetControlID="vdrMailTemplateNo5" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
