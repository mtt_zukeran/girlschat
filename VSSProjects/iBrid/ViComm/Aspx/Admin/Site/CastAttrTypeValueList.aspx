<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastAttrTypeValueList.aspx.cs" Inherits="Site_CastAttrTypeValueList"
	Title="出演者属性値設定" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="出演者属性値設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									サイトコード
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD">
									</asp:DropDownList>
								</td>
								<td class="tdHeaderStyle2">
									属性区分
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstCastAttrTypeSeq" runat="server" DataSourceID="dsCastAttrType" DataTextField="CAST_ATTR_TYPE_NM" DataValueField="CAST_ATTR_TYPE_SEQ"
										AutoPostBack="true" OnSelectedIndexChanged="lstCastAttrTypeSeq_SelectedIndexChanged">
									</asp:DropDownList>
									<asp:Label ID="lblCastAttrSeq" runat="server" Text="" Visible="false"></asp:Label>
								</td>
							</tr>
						</table>
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[属性値内容]</legend>
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									属性値
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCastAttrNm" runat="server" MaxLength="160" Width="400px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrCastAttrNm" runat="server" ErrorMessage="属性値を入力して下さい。" ControlToValidate="txtCastAttrNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									表示順位
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtPriority" runat="server" MaxLength="3" Width="60px"></asp:TextBox>&nbsp;
									<asp:RequiredFieldValidator ID="vdrPriority" runat="server" ErrorMessage="優先順位を入力して下さい。" ControlToValidate="txtPriority" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									グループコード
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstGroupCd" runat="server" Width="206px" DataSourceID="dsGroupCd" DataTextField="CODE_NM" DataValueField="CODE">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									検索可
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkOmitSeekContionFlag" runat="server" Checked="true">
									</asp:CheckBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									アイテムCD
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtItemCd" runat="server" MaxLength="3" Width="60px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrItemCd" runat="server" ControlToValidate="txtItemCd" ErrorMessage="*" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[属性値一覧]</legend>
			<table border="0" style="width: 640px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle2">
						サイトコード
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
						</asp:DropDownList>
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnRegist" Text="属性値追加" CssClass="seekbutton" CausesValidation="False" OnClick="btnRegist_Click" />
			<br />
			<br />
			<asp:GridView ID="grdValue" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCastAttrTypeValue" AllowSorting="True" SkinID="GridView"
				OnDataBound="grdValue_DataBound">
				<Columns>
					<asp:TemplateField HeaderText="属性区分">
						<ItemTemplate>
							<asp:Label ID="lblCastAttrTypeNm" runat="server" Text='<%# Bind("CAST_ATTR_TYPE_NM") %>'></asp:Label>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:BoundField DataField="ITEM_CD" HeaderText="アイテムCD">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
					<asp:BoundField DataField="PRIORITY" HeaderText="表示順位">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:HyperLink ID="lnkCastAttrSeq" runat="server" NavigateUrl='<%# string.Format("~/Site/CastAttrTypeValueList.aspx?site={0}&castattrtypeseq={1}&castattrseq={2}",Eval("SITE_CD"),Eval("CAST_ATTR_TYPE_SEQ"),Eval("CAST_ATTR_SEQ"))%>'><%#Eval("CAST_ATTR_NM")%></asp:HyperLink>
						</ItemTemplate>
						<HeaderTemplate>
							属性区分名
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Left" Wrap="true"/>
					</asp:TemplateField>
					<asp:BoundField DataField="GROUPING_NM" HeaderText="グループ">
						<ItemStyle HorizontalAlign="Left" />
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br />
				<a class="reccount">Current viewing page
					<%=grdValue.PageIndex + 1%>
					of
					<%=grdValue.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsCastAttrTypeValue" runat="server" SelectMethod="GetPageCollection" TypeName="CastAttrTypeValue" SelectCountMethod="GetPageCount"
		EnablePaging="True" OnSelected="dsCastAttrTypeValue_Selected" OnSelecting="dsCastAttrTypeValue_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastAttrType" runat="server" SelectMethod="GetList" TypeName="CastAttrType" OnSelecting="dsCastAttrType_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsGroupCd" runat="server" SelectMethod="GetList" TypeName="CodeDtl" OnSelecting="dsGroupCd_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="Server" Enabled="true" FilterType="Numbers" TargetControlID="txtPriority" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="Server" Enabled="true" FilterType="Numbers,UppercaseLetters,LowercaseLetters"
		TargetControlID="txtItemCd" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrCastAttrNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdrPriority" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行いますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
