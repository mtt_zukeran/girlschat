﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者写真属性区分メンテナンス
--	Progaram ID		: CastPicAttrTypeList
--
--  Creation Date	: 2010.05.17
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;

public partial class Site_CastPicAttrTypeList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_SITE_OWNER)) {
			throw new ApplicationException("ViComm権限違反");
		}
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
	}

	protected void dsCastPicAttrType_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void FirstLoad() {
		grdCastPicAttrType.PageSize = 999;
	}

	private void InitPage() {
		lstSiteCd.SelectedIndex = 0;
		lblCastPicAttrTypeSeq.Text = "";
		ClearField();
		pnlMainte.Visible = false;
		lstSeekSiteCd.DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		DataBind();

		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);

	}

	private void ClearField() {
		txtCastPicAttrTypeNm.Text = "";
		txtPriority.Text = "";
		txtItemNo.Text = "";
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		lstSiteCd.SelectedIndex = lstSeekSiteCd.SelectedIndex;
		lblCastPicAttrTypeSeq.Text = "0";
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void lnkCastPicAttrTypeSeq_Command(object sender,CommandEventArgs e) {
		lblCastPicAttrTypeSeq.Text = e.CommandArgument.ToString();
		GetData();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	private void GetList() {
		grdCastPicAttrType.PageIndex = 0;
		grdCastPicAttrType.DataBind();
		pnlCount.DataBind();
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_PIC_ATTR_TYPE_GET");
			db.ProcedureInParm("PCAST_PIC_ATTR_TYPE_SEQ",DbSession.DbType.NUMBER,lblCastPicAttrTypeSeq.Text);
			db.ProcedureOutParm("PSITE_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_PIC_ATTR_TYPE_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPRIORITY",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PITEM_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				lstSiteCd.SelectedValue = db.GetStringValue("PSITE_CD");
				txtCastPicAttrTypeNm.Text = db.GetStringValue("PCAST_PIC_ATTR_TYPE_NM");
				txtPriority.Text = db.GetStringValue("PPRIORITY");
				txtItemNo.Text = db.GetStringValue("PITEM_NO");
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;

	}

	private void UpdateData(int pDelFlag) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_PIC_ATTR_TYPE_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PCAST_PIC_ATTR_TYPE_SEQ",DbSession.DbType.NUMBER,lblCastPicAttrTypeSeq.Text);
			db.ProcedureInParm("PCAST_PIC_ATTR_TYPE_NM",DbSession.DbType.VARCHAR2,txtCastPicAttrTypeNm.Text);
			db.ProcedureInParm("PPRIORITY",DbSession.DbType.NUMBER,txtPriority.Text);
			db.ProcedureInParm("PITEM_NO",DbSession.DbType.VARCHAR2,txtItemNo.Text);;
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		int iIdx = lstSiteCd.SelectedIndex;
		InitPage();
		lstSeekSiteCd.SelectedIndex = iIdx;
		GetList();
	}

	protected void dsCastPicAttrType_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
	}
}
