﻿/*************************************************************************
--	System			: ViComm System
--	Sub System Name	: Admin
--	Title			: メッセージ一覧
--	Progaram ID		: MessageList
--
--  Creation Date	: 2010.07.05
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using iBridCommLib;
using ViComm;

public partial class System_MessageList:System.Web.UI.Page {
	private string recCount = "";
	private Stream filter;

	protected void Page_Load(object sender,EventArgs e) {
		filter = Response.Filter;
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);

		if (!IsPostBack) {
			SysPrograms.SetupFromToDay(lstFromYYYY,lstFromMM,lstFromDD,lstToYYYY,lstToMM,lstToDD,true);
			DateTime sDayFrom = DateTime.Today;
			DateTime sDayTo = DateTime.Today;
			lstFromYYYY.SelectedValue = sDayFrom.ToString("yyyy");
			lstToYYYY.SelectedValue = sDayTo.ToString("yyyy");
			lstFromMM.SelectedValue = sDayFrom.ToString("MM");
			lstToMM.SelectedValue = sDayTo.ToString("MM");
			lstFromDD.SelectedValue = sDayFrom.ToString("dd");
			lstToDD.SelectedValue = sDayTo.ToString("dd");

			ViewState["SEX_CD"] = iBridUtil.GetStringValue(Request.QueryString["sexcd"]);
			InitPage();
		}
	}

	protected void dsMesage_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		DisplayWordUtil.ReplaceGridColumnHeader(this.grdMessage);
		if (ViewState["SEX_CD"].ToString().Equals(ViCommConst.MAN)) {
			lblPgmTitle.Text = DisplayWordUtil.Replace("男性メッセージ管理");
			lblHandleNmHeader.Text = DisplayWordUtil.Replace("会員名");
			lblPartnerHandleNmHeader.Text = DisplayWordUtil.Replace("出演者名");
			grdMessage.Columns[2].Visible = false;
			grdMessage.Columns[3].Visible = false;
		} else {
			lblPgmTitle.Text = DisplayWordUtil.Replace("出演者メッセージ管理");
			lblHandleNmHeader.Text = DisplayWordUtil.Replace("出演者名");
			lblPartnerHandleNmHeader.Text = DisplayWordUtil.Replace("会員名");
			grdMessage.Columns[0].Visible = false;
			grdMessage.Columns[1].Visible = false;
		}
		grdMessage.PageSize = int.Parse(Session["PageSize"].ToString());
		grdMessage.DataSourceID = "";
		ClearField();
		pnlSeek.Visible = true;
		pnlInfo.Visible = false;
		pnlMainte.Visible = false;
		DataBind();
		if (!iBridUtil.GetStringValue(Request.QueryString["sitecd"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		} else if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void ClearField() {
		txtMessageDoc.Text = "";
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
		GetList();
	}

	protected void lnkMessageDoc_Command(object sender,CommandEventArgs e) {
		string[] sKey = e.CommandArgument.ToString().Split(':');
		if (sKey.Length >= 3) {
			lblMessageSeq.Text = sKey[0];
			lblHandleNm.Text = sKey[1];
			lblPartnerHandleNm.Text = sKey[2];
			GetData();
			pnlSeek.Visible = false;
		}
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void dsMessage_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = ViewState["SEX_CD"].ToString();
		e.InputParameters[2] = string.Format("{0}/{1}/{2}",lstFromYYYY.SelectedValue,lstFromMM.SelectedValue,lstFromDD.SelectedValue);
		e.InputParameters[3] = string.Format("{0}/{1}/{2}",lstToYYYY.SelectedValue,lstToMM.SelectedValue,lstToDD.SelectedValue);
	}

	private void GetList() {
		grdMessage.DataSourceID = "dsMessage";
		grdMessage.PageIndex = 0;
		grdMessage.DataBind();
		pnlCount.DataBind();
		pnlInfo.Visible = true;
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MESSAGE_GET");
			db.ProcedureInParm("PMESSAGE_SEQ",DbSession.DbType.VARCHAR2,lblMessageSeq.Text);
			db.ProcedureOutParm("PSITE_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PUSER_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPARTNER_USER_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPARTNER_USER_CHAR_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMESSAGE_DOC",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCREATE_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PUPDATE_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				lblCreateDate.Text = db.GetStringValue("PCREATE_DATE");
				lblUpdateDate.Text = db.GetStringValue("PUPDATE_DATE");
				txtMessageDoc.Text = db.GetStringValue("PMESSAGE_DOC");
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
	}

	private void UpdateData(int pDelFlag) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MESSAGE_MAINTE");
			db.ProcedureInParm("PMESSAGE_SEQ",DbSession.DbType.VARCHAR2,lblMessageSeq.Text);
			db.ProcedureInParm("PMESSAGE_DOC",DbSession.DbType.VARCHAR2,txtMessageDoc.Text);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,ViewState["REVISION_NO"].ToString());
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		InitPage();
		GetList();
	}

}
