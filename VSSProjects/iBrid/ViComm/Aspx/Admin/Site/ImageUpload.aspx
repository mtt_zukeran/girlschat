<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ImageUpload.aspx.cs" Inherits="Site_ImageUpload"
	Title="画像アップロード" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="画像アップロード"></asp:Label>
</asp:Content>

<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[サイト画像]</legend>
						<asp:Label ID="lblSiteCd" runat="server" Text="" Visible="false"></asp:Label>
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									サイト名
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblSiteNm" runat="server" Text="Label"></asp:Label>
								</td>
							</tr>
						</table>
						<table border="0" style="width: 640px">
							<div>
								<asp:FileUpload ID="uldUploadPic" runat="server" />&nbsp;<asp:Button ID="btnUploadPic" runat="server" Text="アップロード" Width="100px" OnClick="btnUploadPic_Click"
									ValidationGroup="UploadPic" />
								<asp:RequiredFieldValidator ID="vdrUploadPic" runat="server" ControlToValidate="uldUploadPic" ErrorMessage="アップロードファイルが入力されていません。" ValidationGroup="UploadPic">*</asp:RequiredFieldValidator>
							</div>
						</table>
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[サイト一覧]</legend>
			<asp:GridView ID="grdSiteManagement" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsSiteManagement" AllowSorting="True"
				SkinID="GridViewColor">
				<Columns>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkSiteCd" runat="server" Text='<%# Eval("SITE_CD") %>' CommandArgument='<%# Eval("SITE_CD") %>' OnCommand="lnkSiteCd_Command" CausesValidation="False">
							</asp:LinkButton>
							<asp:Label ID="lblConnect" runat="server" Text='<%# Bind("SITE_NM") %>'></asp:Label>
						</ItemTemplate>
						<HeaderTemplate>
							サイトコード
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:BoundField DataField="REGIST_SERVICE_POINT" HeaderText="登録Ｐ">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:BoundField DataField="SUPPORT_EMAIL_ADDR" HeaderText="サポートメール" />
					<asp:BoundField DataField="INFO_EMAIL_ADDR" HeaderText="インフォメール" />
					<asp:BoundField DataField="SUPPORT_TEL" HeaderText="サポート電話" />
					<asp:BoundField DataField="MANAGEMENT_UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<a class="reccount">Record Count
				<%#GetRecCount() %>
			</a>
			<br>
			<a class="reccount">Current viewing page
				<%=grdSiteManagement.PageIndex + 1%>
				of
				<%=grdSiteManagement.PageCount%>
			</a>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsSiteManagement" runat="server" SelectMethod="GetPageCollection" TypeName="SiteManagement" SelectCountMethod="GetPageCount"
		EnablePaging="True" OnSelected="dsSiteManagement_Selected"></asp:ObjectDataSource>
</asp:Content>
