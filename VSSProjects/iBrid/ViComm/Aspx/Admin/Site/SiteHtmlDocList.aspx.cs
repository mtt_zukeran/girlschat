﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: サイト構成HTML文章メンテナンス
--	Progaram ID		: SiteHtmlDocList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using iBridCommLib;
using ViComm;

public partial class Site_SiteHtmlDocList:System.Web.UI.Page {
	#region Sorting

	private string SortExpression {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortExpression"]);
		}
		set {
			this.ViewState["SortExpression"] = value;
		}
	}

	private string SortDirection {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortDirection"]);
		}
		set {
			this.ViewState["SortDirection"] = value;
		}
	}
	#endregion
	private string recCount = "";
	private Stream filter;

	private bool DisablePinEdit {
		get {
			return (bool)this.ViewState["DisablePinEdit"];
		}
		set {
			this.ViewState["DisablePinEdit"] = value;
		}
	}

	private string HtmlDoc {
		get {
			if (this.DisablePinEdit) {
				return this.txtHtmlDocText.Text;
			} else {
				return this.txtHtmlDoc.Text;
			}
		}
		set {
			if (this.DisablePinEdit) {
				this.txtHtmlDocText.Text = value;
			} else {
				this.txtHtmlDoc.Text = value;
			}
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		txtHtmlDoc.EditorDirectory = string.Format("http://{0}:{1}/pinEdit/editor/",Request.UrlReferrer.Host,Request.UrlReferrer.Port);
		txtHtmlDoc.Autofocus = true;
		txtSeekHtmlDocType.Style.Add("ime-mode","disabled");
		filter = Response.Filter;
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!IsPostBack) {
			FirstLoad();
			InitPage();

			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["direct"])) && iBridUtil.GetStringValue(Request.QueryString["direct"]).Equals(ViCommConst.FLAG_ON_STR)) {
				string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
				string sHtmlDocType = iBridUtil.GetStringValue(Request.QueryString["htmldoctype"]);
				string sUserAgentType = iBridUtil.GetStringValue(Request.QueryString["useragenttype"]);
				
				DataSet oDS;
				using (SiteHtmlDoc oSiteHtmlDoc = new SiteHtmlDoc()) {
					oDS = oSiteHtmlDoc.GetDataByDocType(sSiteCd,sHtmlDocType,sUserAgentType);
				}
				
				if (oDS.Tables[0].Rows.Count > 0) {
					lstSiteCd.SelectedValue = sSiteCd;
					lblHtmlDocSeq.Text = oDS.Tables[0].Rows[0]["HTML_DOC_SEQ"].ToString();
					txtHtmlDocType.Text = sHtmlDocType;
					lstUserAgentType.SelectedValue = oDS.Tables[0].Rows[0]["USER_AGENT_TYPE"].ToString();
					GetData();
				}
			}
		}
	}

	protected void dsSiteHtmlDoc_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void FirstLoad() {
		grdSiteHtmlDoc.PageSize = 999;
		grdSiteHtmlDoc.DataSourceID = "";
		DataBind();
		lstSeekUserAgentType.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		lstSeekUserAgentType.SelectedIndex = 0;
		lstSeekUserAgentType.DataSourceID = string.Empty;

		lstSiteCd.DataSourceID = string.Empty;
		lstSeekSiteCd.DataSourceID = string.Empty;
		lstTableIndex.DataSourceID = string.Empty;

		lstWebFaceSeq.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		lstWebFaceSeq.SelectedIndex = 0;
		lstWebFaceSeq.DataSourceID = string.Empty;
		using (ManageCompany oManageCompany = new ManageCompany()) {
			this.plcSample.Visible = oManageCompany.IsAvailableService(ViCommConst.RELEASE_NEW_VARIABLE);
			this.DisablePinEdit = oManageCompany.IsAvailableService(ViCommConst.RELEASE_DISABLE_PINEDIT,2);
			this.txtHtmlDoc.Visible = !this.DisablePinEdit;
			this.txtHtmlDocText.Visible = this.DisablePinEdit;
		}
	}

	private void InitPage() {
		txtHtmlDocType.Text = "";
		ClearField();
		pnlMainte.Visible = false;
		grdOutLook.DataSourceID = "";
		lstSeekSiteCd.DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		DataBind();
		int iCompare = string.Compare(Session["AdminType"].ToString(),ViCommConst.RIGHT_SITE_OWNER);
		btnCSV.Visible = (iCompare >= 0);
	}

	private void ClearField() {
		txtPageTile.Text = string.Empty;
		txtPageKeyword.Text = string.Empty;
		txtPageDescription.Text = string.Empty;
		txtCssFileNm1.Text = string.Empty;
		txtCssFileNm2.Text = string.Empty;
		txtCssFileNm3.Text = string.Empty;
		txtCssFileNm4.Text = string.Empty;
		txtCssFileNm5.Text = string.Empty;
		txtJavascriptFileNm1.Text = string.Empty;
		txtJavascriptFileNm2.Text = string.Empty;
		txtJavascriptFileNm3.Text = string.Empty;
		txtJavascriptFileNm4.Text = string.Empty;
		txtJavascriptFileNm5.Text = string.Empty;
		txtCanonical.Text = string.Empty;
		chkProtectImageFlag.Checked = true;
		chkEmojiToolFlag.Checked = true;
		txtHtmlDocTypeNm.Text = string.Empty;
		lblHtmlDocSeq.Text = string.Empty;
		this.HtmlDoc = string.Empty;
		recCount = "0";
		SortExpression = string.Empty;
		SortDirection = string.Empty;
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		if (IsValid) {
			using (SiteHtmlDoc oSiteHtmlDoc = new SiteHtmlDoc()) {
				lblHtmlDocSeq.Text = oSiteHtmlDoc.GetDocSeq(lstSiteCd.SelectedValue,lstUserAgentType.SelectedValue,txtHtmlDocType.Text);
			}
			GetData();
		}
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		lstSiteCd.SelectedIndex = lstSeekSiteCd.SelectedIndex;
		if (!lstSeekUserAgentType.SelectedValue.Equals("")) {
			lstUserAgentType.SelectedValue = lstSeekUserAgentType.SelectedValue;
		}
		ClearField();
		pnlMainte.Visible = true;
		pnlKey.Enabled = true;
		pnlDtl.Visible = false;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void lnkHtmlDocSeq_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		lstSiteCd.SelectedValue = sKeys[0];
		lblHtmlDocSeq.Text = sKeys[1];
		txtHtmlDocType.Text = sKeys[2];
		txtHtmlDocTypeNm.Text = sKeys[3];
		lstUserAgentType.SelectedValue = sKeys[4];
		GetData();
	}

	protected void lnkDoc_Command(object sender,CommandEventArgs e) {
		GetData();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected string EditHtmlDoc(object pHtmlDoc) {
		return pHtmlDoc.ToString();
	}


	private void GetList() {
		grdSiteHtmlDoc.PageIndex = 0;
		grdSiteHtmlDoc.DataSourceID = "dsSiteHtmlDoc";
		grdSiteHtmlDoc.DataBind();
		pnlCount.DataBind();
	}

	private void GetData() {
		SessionObjs userObjs = (SessionObjs)Session["objs"];
		userObjs.site.GetOne(lstSiteCd.SelectedValue);

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SITE_HTML_DOC_GET");
			db.ProcedureInParm("PHTML_DOC_SEQ",DbSession.DbType.VARCHAR2,lblHtmlDocSeq.Text);
			db.ProcedureInParm("PHTML_DOC_SUB_SEQ",DbSession.DbType.NUMBER,1);
			db.ProcedureOutParm("PPAGE_TITLE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPAGE_KEYWORD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPAGE_DESCRIPTION",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PTABLE_INDEX",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PWEB_FACE_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSEX_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHTML_DOC_TITLE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutArrayParm("PHTML_DOC",DbSession.DbType.VARCHAR2,SysConst.MAX_HTML_BLOCKS);
			db.ProcedureOutParm("PEND_PUB_DAY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PNON_DISP_IN_MAIL_BOX",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PNON_DISP_IN_DECOMAIL",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PUSER_AGENT_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCSS_FILE_NM1",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pCSS_FILE_NM2",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pCSS_FILE_NM3",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pCSS_FILE_NM4",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pCSS_FILE_NM5",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pJAVASCRIPT_FILE_NM1",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pJAVASCRIPT_FILE_NM2",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pJAVASCRIPT_FILE_NM3",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pJAVASCRIPT_FILE_NM4",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pJAVASCRIPT_FILE_NM5",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pCANONICAL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pPROTECT_IMAGE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pEMOJI_TOOL_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pNOINDEX_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pNOFOLLOW_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pNOARCHIVE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PNA_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID_MANAGE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO_MANAGE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID_DOC",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO_DOC",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();

			ViewState["REVISION_NO_MANAGE"] = db.GetStringValue("PREVISION_NO_MANAGE");
			ViewState["ROWID_MANAGE"] = db.GetStringValue("PROWID_MANAGE");

			ViewState["REVISION_NO_DOC"] = db.GetStringValue("PREVISION_NO_DOC");
			ViewState["ROWID_DOC"] = db.GetStringValue("PROWID_DOC");

			this.HtmlDoc = "";
			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtPageTile.Text = db.GetStringValue("PPAGE_TITLE");
				txtPageKeyword.Text = db.GetStringValue("PPAGE_KEYWORD");
				txtPageDescription.Text = db.GetStringValue("PPAGE_DESCRIPTION");
				txtHtmlDocTypeNm.Text = db.GetStringValue("PHTML_DOC_TITLE");
				txtCssFileNm1.Text = db.GetStringValue("PCSS_FILE_NM1");
				txtCssFileNm2.Text = db.GetStringValue("PCSS_FILE_NM2");
				txtCssFileNm3.Text = db.GetStringValue("PCSS_FILE_NM3");
				txtCssFileNm4.Text = db.GetStringValue("PCSS_FILE_NM4");
				txtCssFileNm5.Text = db.GetStringValue("PCSS_FILE_NM5");
				txtJavascriptFileNm1.Text = db.GetStringValue("pJAVASCRIPT_FILE_NM1");
				txtJavascriptFileNm2.Text = db.GetStringValue("pJAVASCRIPT_FILE_NM2");
				txtJavascriptFileNm3.Text = db.GetStringValue("pJAVASCRIPT_FILE_NM3");
				txtJavascriptFileNm4.Text = db.GetStringValue("pJAVASCRIPT_FILE_NM4");
				txtJavascriptFileNm5.Text = db.GetStringValue("pJAVASCRIPT_FILE_NM5");
				txtCanonical.Text = db.GetStringValue("pCANONICAL");
				chkProtectImageFlag.Checked = db.GetIntValue("pPROTECT_IMAGE_FLAG").Equals(ViCommConst.FLAG_ON);
				chkEmojiToolFlag.Checked = db.GetIntValue("pEMOJI_TOOL_FLAG").Equals(ViCommConst.FLAG_ON);
				lstSexCd.SelectedValue = db.GetStringValue("PSEX_CD");
				lstWebFaceSeq.SelectedValue = db.GetStringValue("PWEB_FACE_SEQ");

				for (int i = 0;i < SysConst.MAX_HTML_BLOCKS;i++) {
					this.HtmlDoc = this.HtmlDoc + db.GetArryStringValue("PHTML_DOC",i);
				}
				for (int i = 0;i < lstTableIndex.Items.Count;i++) {
					if (int.Parse(lstTableIndex.Items[i].Value) == int.Parse(db.GetStringValue("PTABLE_INDEX"))) {
						lstTableIndex.SelectedIndex = i;
					}
				}
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;
		SetColor();
		grdOutLook.DataSourceID = "dsOutLook";
		grdOutLook.DataBind();
		lblErrorInputSizeOver.Visible= false;
		pnlExtImport.Visible = !lstUserAgentType.SelectedValue.Equals(ViCommConst.DEVICE_3G);
	}

	private void UpdateData(int pDelFlag) {

		string[] sDoc;
		int iDocCount;
		
		lblErrorInputSizeOver.Visible = false;
		if (this.DisablePinEdit) {
			SysPrograms.SeparateSiteHtmlText(HttpUtility.HtmlDecode(this.HtmlDoc),SysConst.MAX_HTML_BLOCKS + 1,out sDoc,out iDocCount);
		} else {
			SysPrograms.SeparateSiteHtml(HttpUtility.HtmlDecode(this.HtmlDoc),SysConst.MAX_HTML_BLOCKS + 1,out sDoc,out iDocCount);
		}

		if ((iDocCount > SysConst.MAX_HTML_BLOCKS) && (pDelFlag == 0)) {
			lblErrorInputSizeOver.Visible = true;
			return;
		}else{
			Array.Resize<string>(ref sDoc,SysConst.MAX_HTML_BLOCKS);
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SITE_HTML_DOC_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PHTML_DOC_SUB_SEQ",DbSession.DbType.NUMBER,1);
			db.ProcedureInParm("PHTML_DOC_GROUP",DbSession.DbType.VARCHAR2,ViCommConst.DOC_GRP_NORMAL);
			db.ProcedureInParm("PHTML_DOC_TYPE",DbSession.DbType.VARCHAR2,txtHtmlDocType.Text);
			db.ProcedureInParm("PSTART_PUB_DAY",DbSession.DbType.VARCHAR2,ViCommConst.DEFUALT_PUB_DAY);
			db.ProcedureInParm("PHTML_DOC_TITLE",DbSession.DbType.VARCHAR2,txtHtmlDocTypeNm.Text);
			db.ProcedureInArrayParm("PHTML_DOC",DbSession.DbType.VARCHAR2,iDocCount,sDoc);
			db.ProcedureInParm("PHTML_DOC_COUNT",DbSession.DbType.NUMBER,iDocCount);
			db.ProcedureInParm("PPAGE_TITLE",DbSession.DbType.VARCHAR2,txtPageTile.Text);
			db.ProcedureInParm("PPAGE_KEYWORD",DbSession.DbType.VARCHAR2,txtPageKeyword.Text);
			db.ProcedureInParm("PPAGE_DESCRIPTION",DbSession.DbType.VARCHAR2,txtPageDescription.Text);
			db.ProcedureInParm("PTABLE_INDEX",DbSession.DbType.NUMBER,int.Parse(lstTableIndex.SelectedValue));
			db.ProcedureInParm("PWEB_FACE_SEQ",DbSession.DbType.VARCHAR2,lstWebFaceSeq.SelectedValue);
			db.ProcedureInParm("PSEX_CD",DbSession.DbType.VARCHAR2,lstSexCd.SelectedValue);
			db.ProcedureInParm("PEND_PUB_DAY",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PNON_DISP_IN_MAIL_BOX",DbSession.DbType.NUMBER,0);
			db.ProcedureInParm("PNON_DISP_IN_DECOMAIL",DbSession.DbType.NUMBER,0);
			db.ProcedureInParm("PUSER_AGENT_TYPE",DbSession.DbType.VARCHAR2,lstUserAgentType.SelectedValue);
			db.ProcedureInParm("PCSS_FILE_NM1",DbSession.DbType.VARCHAR2,txtCssFileNm1.Text);
			db.ProcedureInParm("pCSS_FILE_NM2",DbSession.DbType.VARCHAR2,txtCssFileNm2.Text);
			db.ProcedureInParm("pCSS_FILE_NM3",DbSession.DbType.VARCHAR2,txtCssFileNm3.Text);
			db.ProcedureInParm("pCSS_FILE_NM4",DbSession.DbType.VARCHAR2,txtCssFileNm4.Text);
			db.ProcedureInParm("pCSS_FILE_NM5",DbSession.DbType.VARCHAR2,txtCssFileNm5.Text);
			db.ProcedureInParm("pJAVASCRIPT_FILE_NM1",DbSession.DbType.VARCHAR2,txtJavascriptFileNm1.Text);
			db.ProcedureInParm("pJAVASCRIPT_FILE_NM2",DbSession.DbType.VARCHAR2,txtJavascriptFileNm2.Text);
			db.ProcedureInParm("pJAVASCRIPT_FILE_NM3",DbSession.DbType.VARCHAR2,txtJavascriptFileNm3.Text);
			db.ProcedureInParm("pJAVASCRIPT_FILE_NM4",DbSession.DbType.VARCHAR2,txtJavascriptFileNm4.Text);
			db.ProcedureInParm("pJAVASCRIPT_FILE_NM5",DbSession.DbType.VARCHAR2,txtJavascriptFileNm5.Text);
			db.ProcedureInParm("pCANONICAL",DbSession.DbType.VARCHAR2,txtCanonical.Text);
			db.ProcedureInParm("pPROTECT_IMAGE_FLAG",DbSession.DbType.NUMBER,chkProtectImageFlag.Checked ? 1 : 0);
			db.ProcedureInParm("pEMOJI_TOOL_FLAG",DbSession.DbType.NUMBER,chkEmojiToolFlag.Checked ? 1 : 0);
			db.ProcedureInParm("pNOINDEX_FLAG",DbSession.DbType.NUMBER,0);
			db.ProcedureInParm("pNOFOLLOW_FLAG",DbSession.DbType.NUMBER,0);
			db.ProcedureInParm("pNOARCHIVE_FLAG",DbSession.DbType.NUMBER,0);
			db.ProcedureInParm("PNA_FLAG",DbSession.DbType.NUMBER,0);
			db.ProcedureInParm("PROWID_MANAGE",DbSession.DbType.VARCHAR2,ViewState["ROWID_MANAGE"].ToString());
			db.ProcedureInParm("PREVISION_NO_MANAGE",DbSession.DbType.NUMBER,ViewState["REVISION_NO_MANAGE"].ToString());
			db.ProcedureInParm("PROWID_DOC",DbSession.DbType.VARCHAR2,ViewState["ROWID_DOC"].ToString());
			db.ProcedureInParm("PREVISION_NO_DOC",DbSession.DbType.NUMBER,ViewState["REVISION_NO_DOC"].ToString());
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PHTML_DOC_SEQ",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			lblHtmlDocSeq.Text = db.GetStringValue("PHTML_DOC_SEQ");
		}
		if (pDelFlag != 0) {
			int iIdx = lstSiteCd.SelectedIndex;
			InitPage();
			lstSeekSiteCd.SelectedIndex = iIdx;
		} else {
			GetData();
			pnlDtl.Visible = false;
			GetList();
		}
	}

	protected void dsSiteHtmlDoc_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		SessionObjs userObjs = (SessionObjs)Session["objs"];
		userObjs.site.GetOne(lstSeekSiteCd.SelectedValue);
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
		e.InputParameters[1] = lstSeekUserAgentType.SelectedValue;
		e.InputParameters[2] = txtSeekHtmlDocType.Text;
		e.InputParameters[3] = SortExpression;
		e.InputParameters[4] = SortDirection;
	}

	protected void dsOutLook_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lblHtmlDocSeq.Text;
	}

	private void SetColor() {
		string sForeColor;
		string sBackColor;
		string sLinkColor;

		if (lstWebFaceSeq.SelectedValue.Equals(string.Empty)) {
			using (Site oSite = new Site()) {
				oSite.GetOne(lstSiteCd.SelectedValue);
				sForeColor = oSite.colorChar;
				sBackColor = oSite.colorBack;
				sLinkColor = oSite.colorLink;
			}
		} else {
			using (WebFace oWebFace = new WebFace()) {
				oWebFace.GetOne(lstWebFaceSeq.SelectedValue);
				sForeColor = oWebFace.colorChar;
				sBackColor = oWebFace.colorBack;
				sLinkColor = oWebFace.colorLink;
			}
		}

		ltlUserColor.Text = "<style type=\"text/css\">\r\n" +
							"<!--\r\n" +
							".userColor { background-color: " + sBackColor + ";}\r\n" +
							".userColor TD{ color: " + sForeColor + ";}\r\n" +
							".userColor TD A{color: " + sLinkColor + ";font-weight: bold;text-decoration: none;background-color: inherit;}\r\n" +
							"-->\r\n" +
							"</style>";
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		Response.Filter = filter;
		Response.AddHeader("Content-Disposition",string.Format("attachment;filename=SITE_HTML_DOC_{0}.CSV",lstSeekSiteCd.SelectedValue));
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		using (SiteHtmlDoc oDoc = new SiteHtmlDoc()) {
			DataSet ds = oDoc.GetCsvData(lstSeekSiteCd.SelectedValue,lstSeekUserAgentType.SelectedValue,txtSeekHtmlDocType.Text);
			SetCsvData(ds);
			Response.End();
		}
	}

	public void SetCsvData(DataSet pDataSet) {

		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");
		string sDtl = "";

		DataRow dr = null;
		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];

			sDtl = string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}\t{16}\t{17}\t{18}\t{19}\t{20}\t{21}\t{22}\t{23}\t{24}\t{25}\t{26}\t{27}\t{28}\t{29}\t{30}\t{31}\t{32}\t{33}\t{34}\t{35}\t{36}\t{37}\t{38}",
							dr["SITE_CD"].ToString(),
							dr["HTML_DOC_GROUP"].ToString(),
							dr["HTML_DOC_TYPE"].ToString(),
							dr["PAGE_TITLE"].ToString(),
							dr["PAGE_KEYWORD"].ToString(),
							dr["PAGE_DESCRIPTION"].ToString(),
							dr["TABLE_INDEX"].ToString(),
							dr["SEX_CD"].ToString(),
							dr["NA_FLAG"].ToString(),
							dr["WEB_FACE_SEQ"].ToString(),
							dr["USER_AGENT_TYPE"].ToString(),
							dr["CSS_FILE_NM1"].ToString(),
							dr["CSS_FILE_NM2"].ToString(),
							dr["CSS_FILE_NM3"].ToString(),
							dr["CSS_FILE_NM4"].ToString(),
							dr["CSS_FILE_NM5"].ToString(),
							dr["JAVASCRIPT_FILE_NM1"].ToString(),
							dr["JAVASCRIPT_FILE_NM2"].ToString(),
							dr["JAVASCRIPT_FILE_NM3"].ToString(),
							dr["JAVASCRIPT_FILE_NM4"].ToString(),
							dr["JAVASCRIPT_FILE_NM5"].ToString(),
							dr["HTML_DOC_SUB_SEQ"].ToString(),
							dr["HTML_DOC_TITLE"].ToString(),
							dr["HTML_DOC1"].ToString(),
							dr["HTML_DOC2"].ToString(),
							dr["HTML_DOC3"].ToString(),
							dr["HTML_DOC4"].ToString(),
							dr["HTML_DOC5"].ToString(),
							dr["HTML_DOC6"].ToString(),
							dr["HTML_DOC7"].ToString(),
							dr["HTML_DOC8"].ToString(),
							dr["HTML_DOC9"].ToString(),
							dr["HTML_DOC10"].ToString(),
							dr["HTML_DOC11"].ToString(),
							dr["HTML_DOC12"].ToString(),
							dr["HTML_DOC13"].ToString(),
							dr["HTML_DOC14"].ToString(),
							dr["HTML_DOC15"].ToString(),
							dr["HTML_DOC16"].ToString(),
							dr["HTML_DOC17"].ToString(),
							dr["HTML_DOC18"].ToString(),
							dr["HTML_DOC19"].ToString(),
							dr["HTML_DOC20"].ToString());
			sDtl = sDtl.Replace("\r","");
			sDtl = sDtl.Replace("\n","");
			sDtl += "\r\n";
			Response.BinaryWrite(encoding.GetBytes(sDtl));
		}
	}

	protected string ReplaceForm(object pHtmlDoc) {
		return Regex.Replace(pHtmlDoc.ToString(),@"<\/?[fF][oO][rR][mM].*?>",string.Empty,RegexOptions.Compiled);
	}

	protected void lnkVariableSample_Click(object sender,CommandEventArgs e) {
		string sURL = "";
		txtHtmlDoc.Autofocus = false;
		sURL = string.Format("{0}/Site/VariableSample.aspx?usetype={1}",Session["Root"].ToString(),e.CommandArgument.ToString());
		string sScripts = string.Format("<script language=\"javascript\">window.open('{0}','','width=1060,height=900,resizable=yes,directories=no,scrollbars=yes' , false);</script>",sURL);
		ClientScript.RegisterStartupScript(Page.GetType(),"OpenNewWindow",sScripts);
	}

	protected void lnkPictographSample_Click(object sender,EventArgs e) {
		txtHtmlDoc.Autofocus = false;
		string sURL = string.Concat(Session["Root"],"/Site/PictographSample.aspx");
		string sScripts = string.Format("<script language=\"javascript\">window.open('{0}','','width=1060,height=900,resizable=yes,directories=no,scrollbars=yes' , false);</script>",sURL);
		ClientScript.RegisterStartupScript(Page.GetType(),"OpenNewWindow",sScripts);
	}

	protected void grdSiteHtmlDoc_Sorting(object sender,GridViewSortEventArgs e) {
		if (this.SortExpression.Equals(e.SortExpression)) {
			if (this.SortDirection.Equals("ASC")) {
				this.SortDirection = "DESC";
			} else if (this.SortDirection.Equals("DESC")) {
				this.SortDirection = "ASC";
			}
		} else {
			this.SortDirection = "ASC";
		}

		this.SortExpression = e.SortExpression;

		e.Cancel = true;
		this.GetList();
	}
}
