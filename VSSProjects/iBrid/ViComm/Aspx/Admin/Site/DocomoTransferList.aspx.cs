﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ドコモケータイ送金設定
--	Progaram ID		: DocomoTransferList
--
--  Creation Date	: 2011.09.27
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Configuration;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using iBridCommLib;
using ViComm;

public partial class Site_DocomoTransferList : System.Web.UI.Page {

	protected string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		private set {
			this.ViewState["SiteCd"] = value;
		}
	}

	protected string DocomoMoneyTransferTel {
		get {
			return iBridUtil.GetStringValue(this.ViewState["DocomoMoneyTransferTel"]);
		}
		private set {
			this.ViewState["DocomoMoneyTransferTel"] = value;
		}
	}

	protected string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		private set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		if (!this.IsPostBack) {
			this.InitPage();
			this.GetList();
			this.pnlList.Visible = true;
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		this.GetList();
		this.pnlList.Visible = true;
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		this.ClearFields();
		this.SetSiteNm();
		this.pnlMainte.Visible = true;
		this.txtDocomoMoneyTransferTel.Enabled = true;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlMainte.Visible = false;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.DocomoMoneyTransferTel = this.txtDocomoMoneyTransferTel.Text.Trim();
		this.UpdateData(ViCommConst.FLAG_OFF);
		this.pnlMainte.Visible = false;
		this.GetList();
	}

	protected void btnActivate_Command(object sender,CommandEventArgs e) {
		string sDocomoMoneyTransferTel = iBridUtil.GetStringValue(e.CommandArgument);

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("ACTIVATE_DOCOMO_TRANSFER");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pDOCOMO_MONEY_TRANSFER_TEL",DbSession.DbType.VARCHAR2,sDocomoMoneyTransferTel);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.GetList();
	}

	protected void btnUpdate_Command(object sender,CommandEventArgs e) {
		string sDocomoMoneyTransferTel = iBridUtil.GetStringValue(e.CommandArgument);

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("DOCOMO_TRANSFER_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pDOCOMO_MONEY_TRANSFER_TEL",DbSession.DbType.VARCHAR2,sDocomoMoneyTransferTel);
			oDbSession.ProcedureOutParm("pDOCOMO_MONEY_TRANSFER_NM",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREMARKS",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.txtDocomoMoneyTransferTel.Text = oDbSession.GetStringValue("pDOCOMO_MONEY_TRANSFER_TEL");
			this.txtDocomoMoneyTransferNm.Text = oDbSession.GetStringValue("pDOCOMO_MONEY_TRANSFER_NM");
			this.txtRemarks.Text = oDbSession.GetStringValue("pREMARKS");
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
		}

		this.SetSiteNm();
		this.pnlMainte.Visible = true;
		this.txtDocomoMoneyTransferTel.Enabled = false;
	}

	protected void btnDelete_Command(object sender,CommandEventArgs e) {
		string[] sArguemnts = iBridUtil.GetStringValue(e.CommandArgument).Split(':');
		this.DocomoMoneyTransferTel = sArguemnts[0];
		this.RevisionNo = sArguemnts[1];

		this.UpdateData(ViCommConst.FLAG_ON);
		this.GetList();
	}

	protected void grdDocomoTransfer_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType != DataControlRowType.DataRow) {
			return;
		}

		e.Row.BackColor = ViCommConst.FLAG_ON_STR.Equals(DataBinder.Eval(e.Row.DataItem,"USED_FLAG","{0}")) ? Color.LightSkyBlue : Color.Empty;
	}

	protected void vdcRegist_ServerValidate(object source,ServerValidateEventArgs args) {
		if (!this.txtDocomoMoneyTransferTel.Enabled) {
			return;
		}

		using (DocomoTransfer oDocomoTransfer = new DocomoTransfer()) {
			args.IsValid = !oDocomoTransfer.IsDuplicate(this.SiteCd,this.txtDocomoMoneyTransferTel.Text.Trim());
		}

		if (!args.IsValid) {
			this.vdcRegist.ErrorMessage = "送金先番号が重複しています。";
		} else {
			this.vdcRegist.ErrorMessage = string.Empty;
		}
	}

	private void InitPage() {
		this.lstSeekSiteCd.DataBind();
		this.DocomoMoneyTransferTel = string.Empty;
		this.grdDocomoTransfer.DataSourceID = string.Empty;
		this.pnlList.Visible = false;
		this.pnlMainte.Visible = false;
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			this.SiteCd = this.lstSeekSiteCd.SelectedValue;
		}
	}

	private void ClearFields() {
		this.txtDocomoMoneyTransferTel.Text = string.Empty;
		this.txtDocomoMoneyTransferNm.Text = string.Empty;
		this.txtRemarks.Text = string.Empty;
	}

	private void SetSiteNm() {
		using (Site oSite = new Site()) {
			if (oSite.GetOne(this.SiteCd)) {
				this.lblSiteNm.Text = oSite.siteNm;
			}
		}
	}

	protected string GetRemarksMark(object pRemarks) {
		string sRemarks = iBridUtil.GetStringValue(pRemarks);
		if (sRemarks.Length > 40) {
			return string.Concat(SysPrograms.Substring(sRemarks,39),"…");
		} else {
			return sRemarks;
		}
	}

	private void GetList() {
		this.SiteCd = this.lstSeekSiteCd.SelectedValue;
		this.DocomoMoneyTransferTel = string.Empty;
		this.grdDocomoTransfer.DataSourceID = "dsDocomoTransfer";
		this.grdDocomoTransfer.DataBind();
	}

	private void UpdateData(int pDelFlag) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("DOCOMO_TRANSFER_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureInParm("pDOCOMO_MONEY_TRANSFER_TEL",DbSession.DbType.VARCHAR2,this.DocomoMoneyTransferTel);
			oDbSession.ProcedureInParm("pDOCOMO_MONEY_TRANSFER_NM",DbSession.DbType.VARCHAR2,this.txtDocomoMoneyTransferNm.Text.Trim());
			oDbSession.ProcedureInParm("pREMARKS",DbSession.DbType.VARCHAR2,this.txtRemarks.Text.Trim());
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			oDbSession.ProcedureInParm("pDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}
	}
}
