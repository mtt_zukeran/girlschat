<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AdminReportList.aspx.cs" Inherits="Site_AdminReportList" Title="管理者連絡"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="管理者連絡"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<asp:Label ID="lblDocSeq" runat="server" Text="" Visible="false"></asp:Label>
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					<table border="0" style="width:640px" class="tableStyle">
						<tr>
							<td class="tdHeaderSmallStyle2">
								サイトコード
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
								</asp:DropDownList>
							</td>
						</tr>
					</table>
					<fieldset class="fieldset-inner">
						<legend>[連絡事項]</legend>
						<table border="0" style="width: 720px" class="tableStyle">
							<tr>
								<td class="tdHeaderSmallStyle">
									連絡日
								</td>
								<td class="tdDataStyle" >
									<asp:TextBox ID="txtStartPubDay" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrStartPubDay" runat="server" ErrorMessage="連絡日を入力して下さい。" ControlToValidate="txtStartPubDay" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RangeValidator ID="vdeStartPubDay" runat="server" ControlToValidate="txtStartPubDay" ErrorMessage="連絡日を正しく入力して下さい。" MaximumValue="2099/12/31" MinimumValue="1900/01/01"
										Type="Date" SetFocusOnError="True" ValidationGroup="Detail">*</asp:RangeValidator>
								</td>
								<td class="tdHeaderSmallStyle2">
									時間
								</td>
								<td class="tdDataStyle" >
									<asp:TextBox ID="txtReportHourMin" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
									<asp:RegularExpressionValidator ID="vdrReportHourMin" runat="server" ControlToValidate="txtReportHourMin"
										ErrorMessage="時間を正しく入力して下さい。" ValidationExpression="^([0-1][0-9]|[2][0-3]):([0-5][0-9])$"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdHeaderSmallStyle2">
									タイトル
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtDocTitle" runat="server" MaxLength="80" Width="280px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderSmallStyle2">
									連絡内容
								</td>
								<td class="tdDataStyle" colspan="5">
									<pin:pinEdit ID="txtHtmlDoc" runat="server" UserMode="HTML" DisplayStyle="Style5" DocumentView="True" HtmlMode="Body" Ruler="False" ReturnMode="BR" SpellMode="Inline"
										ToolbarConfiguration="ToolbarDesigner" Toolbar="T09SE10116012136267;T22232427282930313233343536;B53SE54SE55" FormatMode="Advanced" Optimizer="True"
										BorderWidth="1px" Height="500px" Width="600px" DocumentWidth="230" Font-Bold="False" FontSizeType="px" RelativeBaseImageUrl="/" RelativeBaseLinkUrl="/">
									</pin:pinEdit>
									<asp:TextBox ID="txtHtmlDocText" runat="server" TextMode="MultiLine" Height="500px" Width="600px" Visible="false"></asp:TextBox>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[連絡事項一覧]</legend>
			<table border="0" style="width:640px" class="tableStyle">
				<tr>
					<td class="tdHeaderSmallStyle2">
						サイトコード
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
						</asp:DropDownList>
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnRegist" Text="連絡事項追加" CssClass="seekbutton" CausesValidation="False" OnClick="btnRegist_Click" />
			<br />
			<br />
			<asp:GridView ID="grdAdminReport" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsAdminReport" AllowSorting="True" SkinID="GridView"
				ShowHeader="False" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" CellPadding="4" GridLines="Horizontal">
				<Columns>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkHtmlDocType" runat="server" Text='<%# Eval("START_PUB_DAY") %>' CommandArgument='<%#Eval("DOC_SEQ")%>' OnCommand="lnkDocSeq_Command"
								CausesValidation="False" Font-Size="Medium"></asp:LinkButton>
							<asp:Label Font-Bold="true" ID="lblReportHourMin" runat="server" Text='<%# Eval("REPORT_HOUR_MIN")%>'></asp:Label>
							<asp:Label Font-Bold="true" ID="lblDocTitle" runat="server" Text='<%# Eval("DOC_TITLE")%>'></asp:Label>
							<br />
							<asp:Literal ID="lblHtmlDoc" runat="server" Text='<%# GetHtmlDoc()%>'></asp:Literal>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
				</Columns>
				<FooterStyle BackColor="White" ForeColor="#333333" />
				<RowStyle BackColor="White" ForeColor="#333333" />
				<PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
				<SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
				<HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
				<AlternatingRowStyle BackColor="#EFF3FB" />
			</asp:GridView>
			&nbsp;
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br>
				<a class="reccount">Current viewing page
					<%=grdAdminReport.PageIndex + 1%>
					of
					<%=grdAdminReport.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsAdminReport" runat="server" SelectMethod="GetPageCollection" TypeName="AdminReport" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsAdminReport_Selected" OnSelecting="dsAdminReport_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pSexCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrStartPubDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeStartPubDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrReportHourMin" HighlightCssClass="validatorCallout" />

	<ajaxToolkit:MaskedEditExtender ID="mskStartPubDay" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtStartPubDay">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskReportHourMin" runat="server" MaskType="Time" Mask="99:99" UserTimeFormat="TwentyFourHour" ClearMaskOnLostFocus="true"
		TargetControlID="txtReportHourMin">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
