﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 管理者連絡
--	Progaram ID		: AdminReportList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Site_AdminReportList:System.Web.UI.Page {
	private string recCount = "";

	private bool DisablePinEdit {
		get {
			return (bool)this.ViewState["DisablePinEdit"];
		}
		set {
			this.ViewState["DisablePinEdit"] = value;
		}
	}

	private string HtmlDoc {
		get {
			if (this.DisablePinEdit) {
				return this.txtHtmlDocText.Text;
			} else {
				return this.txtHtmlDoc.Text;
			}
		}
		set {
			if (this.DisablePinEdit) {
				this.txtHtmlDocText.Text = value;
			} else {
				this.txtHtmlDoc.Text = value;
			}
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		txtHtmlDoc.EditorDirectory = string.Format("http://{0}:{1}/pinEdit/editor/",Request.UrlReferrer.Host,Request.UrlReferrer.Port);

		if (!IsPostBack) {
			lblProductionSeq.Text = Request.QueryString["productionseq"];
			ViewState["ProductionSeq"] = lblProductionSeq.Text;
			using (Production oProduction = new Production()) {
				string sProductionNm = "";
				oProduction.GetValue(lblProductionSeq.Text,"PRODUCTION_NM",ref sProductionNm);
				lblPgmTitle.Text = sProductionNm + "業務連絡";
			}

			using (ManageCompany oManageCompany = new ManageCompany()) {
				this.DisablePinEdit = oManageCompany.IsAvailableService(ViCommConst.RELEASE_DISABLE_PINEDIT,2);
				this.txtHtmlDoc.Visible = !this.DisablePinEdit;
				this.txtHtmlDocText.Visible = this.DisablePinEdit;
			}

			InitPage();
		}
	}

	protected void dsBusinessReport_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		grdBusinessReport.PageSize = int.Parse(Session["PageSize"].ToString());
		ClearField();
		pnlMainte.Visible = false;
		DataBind();
	}

	private void ClearField() {
		lstSiteCd.SelectedIndex = -1;
		txtStartPubDay.Text = "";
		txtDocTitle.Text = "";
		this.HtmlDoc = "";
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		lblDocSeq.Text = "0";
		GetData();
		txtStartPubDay.Text = DateTime.Now.ToString("yyyy/MM/dd");
		pnlKey.Enabled = true;
		lstSiteCd.Enabled = false;
		lstSiteCd.SelectedIndex = lstSeekSiteCd.SelectedIndex;
	}

	protected void lnkDocSeq_Command(object sender,CommandEventArgs e) {
		lblDocSeq.Text = e.CommandArgument.ToString();
		GetData();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	private void GetList() {
		grdBusinessReport.PageIndex = 0;
		grdBusinessReport.DataBind();
		pnlCount.DataBind();
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("BUSINESS_REPORT_GET");
			db.ProcedureInParm("PDOC_SEQ",DbSession.DbType.NUMBER,lblDocSeq.Text);
			db.ProcedureOutParm("PPRODUCTION_SEQ",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSITE_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PDOC_TITLE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTART_PUB_DAY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHTML_DOC",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				lstSiteCd.SelectedValue = db.GetStringValue("PSITE_CD");
				txtDocTitle.Text = db.GetStringValue("PDOC_TITLE");
				this.HtmlDoc = db.GetStringValue("PHTML_DOC");
				txtStartPubDay.Text = db.GetStringValue("PSTART_PUB_DAY");
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;
	}

	private void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("BUSINESS_REPORT_MAINTE");
			db.ProcedureInParm("PPRODUCTION_SEQ",DbSession.DbType.NUMBER,lblProductionSeq.Text);
			db.ProcedureInParm("PDOC_SEQ",DbSession.DbType.NUMBER,lblDocSeq.Text);
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PDOC_TITLE",DbSession.DbType.VARCHAR2,txtDocTitle.Text);
			db.ProcedureInParm("PSTART_PUB_DAY",DbSession.DbType.VARCHAR2,txtStartPubDay.Text);
			db.ProcedureInParm("PHTML_DOC",DbSession.DbType.VARCHAR2,HttpUtility.HtmlDecode(this.HtmlDoc));
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,ViewState["REVISION_NO"].ToString());
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		int iIdx = lstSiteCd.SelectedIndex;
		InitPage();
		lstSeekSiteCd.SelectedIndex = iIdx;
		GetList();
	}
	protected void dsBusinessReport_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["ProductionSeq"]);
		e.InputParameters[1] = lstSeekSiteCd.SelectedValue;
	}
}
