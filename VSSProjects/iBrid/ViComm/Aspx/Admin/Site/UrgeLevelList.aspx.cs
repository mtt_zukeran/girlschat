﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 督促レベル
--	Progaram ID		: UserLevel
--
--  Creation Date	: 2009.12.11
--  Creater			: iBrid(T.Tokunaga)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Site_UrgeLevelList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_SITE_OWNER)) {
			throw new ApplicationException("ViComm権限違反");
		}
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
	}

	private void FirstLoad() {
		grdUrgeLevel.PageSize = 999;
		string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		if (string.IsNullOrEmpty(sSiteCd)) {
			if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
				lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}
		} else {
			lstSeekSiteCd.SelectedValue = sSiteCd;
		}
		DataBind();
	}

	private void InitPage() {
		txtUrgeLevel.Text = string.Empty;
		ClearField();
		pnlMainte.Visible = false;
	}

	private void ClearField() {
		txtDelayDays.Text = "";
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	private void GetList() {
		grdUrgeLevel.PageIndex = 0;
		grdUrgeLevel.DataBind();
		pnlCount.DataBind();
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		pnlMainte.Visible = true;
		pnlKey.Enabled = true;
		pnlDtl.Visible = false;
		lstSiteCd.SelectedIndex = lstSeekSiteCd.SelectedIndex;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void lnkUrgeLevel_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		lstSiteCd.SelectedValue = sKeys[0];
		txtUrgeLevel.Text = sKeys[1];
        txtDelayDays.Text = sKeys[2];
		GetData();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void dsUrgeLevel_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("URGE_LEVEL_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PURGE_LEVEL",DbSession.DbType.NUMBER,int.Parse(txtUrgeLevel.Text));
			db.ProcedureOutParm("PDELAY_DAYS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

            if(string.IsNullOrEmpty(txtDelayDays.Text)){
                if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				    txtDelayDays.Text = db.GetStringValue("PDELAY_DAYS");
			    } else {
				    ClearField();
			    }
            }
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		lstSeekSiteCd.SelectedIndex = lstSiteCd.SelectedIndex;
		pnlKey.Enabled = false;
	}

	private void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("URGE_LEVEL_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
            db.ProcedureInParm("PURGE_LEVEL",DbSession.DbType.NUMBER,int.Parse(txtUrgeLevel.Text));
            db.ProcedureInParm("PDELAY_DAYS",DbSession.DbType.NUMBER,int.Parse(txtDelayDays.Text));
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		int iIdx = lstSiteCd.SelectedIndex;
		InitPage();
		GetList();
		lstSeekSiteCd.SelectedIndex = iIdx;
	}

	protected void dsUrgeLevel_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
	}

}
