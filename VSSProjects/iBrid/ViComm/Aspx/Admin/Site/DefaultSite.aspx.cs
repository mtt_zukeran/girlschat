﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;

public partial class Site_DefaultSite:System.Web.UI.Page {

	protected void Page_Load(object sender, EventArgs e) {
		DataBind();

		switch (Session["AdminType"].ToString()) {
			case ViCommConst.RIGHT_SITE_MANAGER:
			case ViCommConst.RIGHT_STAFF:
				Response.Redirect("~/Site/SiteHtmlDocList.aspx");
				break;
			default:
				Response.Redirect("~/Site/SiteManagementList.aspx");
				break;
		}
	}
}
