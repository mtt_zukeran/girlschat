﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: サイト構成情報メンテナンス
--	Progaram ID		: SiteManagementList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Web.UI.WebControls;
using iBridCommLib;
using ViComm;

public partial class Site_SiteManagementList:System.Web.UI.Page {
	private string recCount = "";

	private string webNmCastNoPic;
	private string webNmCastNoPicSmall;
	private string webNmManNoPic;

	private string fileNmCastNoPic;
	private string fileNmCastNoPicSmall;
	private string fileNmManNoPic;

	protected void Page_Load(object sender,EventArgs e) {

		if (!Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_SITE_OWNER)) {
			throw new ApplicationException("ViComm権限違反");
		}

		if (!IsPostBack) {
			InitPage();
			string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["site"]);
			if (sSiteCd.Equals("") == false) {
				lblSiteCd.Text = sSiteCd;
				GetData();
			}
		}
	}

	protected void dsSiteManagement_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		grdSiteManagement.PageSize = int.Parse(Session["PageSize"].ToString());
		grdSiteManagement.PageIndex = 0;
		ClearField();

		{
			trMeasuredRateLimitPoint.Visible = false;
		}
		{
			trMeasuredRateServicePoint.Visible = false;
		}
		{
			lstMailTemplateCash2.Visible = false;
			trMailTemplateCash2.Visible = false;
		}
		this.plcAgentRegistEmailAddr.Visible = false;
		this.plcMashupArea.Visible = false;

		pnlMainte.Visible = false;

		// Available Serive?
		using (ManageCompany oCompany = new ManageCompany()) {
			plcModifyAccount.Visible = oCompany.IsAvailableService(ViCommConst.RELEASE_VALIDATE_ACCOUNT_INFO);
		}
		using (ManageCompany oCompany = new ManageCompany()) {
			plcNewFriendIntro.Visible = oCompany.IsAvailableService(ViCommConst.RELEASE_KICK_BACK_INTRO_SYSTEM);
		}
		using (ManageCompany oCompany = new ManageCompany()) {
			plcDownloadVoice.Visible = oCompany.IsAvailableService(ViCommConst.RELEASE_SALE_VOICE);
		}
		using (ManageCompany oCompany = new ManageCompany()) {
			plcInqAdminCastTypeSeq.Visible = oCompany.IsAvailableService(ViCommConst.RELEASE_ATTR_INQUIRY);
			plcInqAdminManTypeSeq.Visible = oCompany.IsAvailableService(ViCommConst.RELEASE_ATTR_INQUIRY);
		}
		DataBind();

		this.vdeCastRegistReportEMailAddr.ErrorMessage = DisplayWordUtil.Replace(this.vdeCastRegistReportEMailAddr.ErrorMessage);
		this.vdrMobileWomanListCount.ErrorMessage = DisplayWordUtil.Replace(this.vdrMobileWomanListCount.ErrorMessage);
		this.vdrMobileWomanDetailListCount.ErrorMessage = DisplayWordUtil.Replace(this.vdrMobileWomanDetailListCount.ErrorMessage);
		this.vdrCastIntroducerPoint.ErrorMessage = DisplayWordUtil.Replace(this.vdrCastIntroducerPoint.ErrorMessage);
		this.vdeCastIntroducerPoint.ErrorMessage = DisplayWordUtil.Replace(this.vdeCastIntroducerPoint.ErrorMessage);
		this.vdrCastIntroPoint.ErrorMessage = DisplayWordUtil.Replace(this.vdrCastIntroPoint.ErrorMessage);
		this.vdeCastIntroPoint.ErrorMessage = DisplayWordUtil.Replace(this.vdeCastIntroPoint.ErrorMessage);
		this.vdrManNewRegistOkAmt.ErrorMessage = DisplayWordUtil.Replace(this.vdrManNewRegistOkAmt.ErrorMessage);
		this.vdrCastNewRegistOkAmt.ErrorMessage = DisplayWordUtil.Replace(this.vdrCastNewRegistOkAmt.ErrorMessage);
		this.vdrNewFaceDays.ErrorMessage = DisplayWordUtil.Replace(this.vdrNewFaceDays.ErrorMessage);
		this.vdeNewFaceDays.ErrorMessage = DisplayWordUtil.Replace(this.vdeNewFaceDays.ErrorMessage);
		this.btnCastNoPic.Text = DisplayWordUtil.Replace(this.btnCastNoPic.Text);
		this.btnCastNoPicSmall.Text = DisplayWordUtil.Replace(this.btnCastNoPicSmall.Text);
		this.vdrManIntroducerPoint.ErrorMessage = DisplayWordUtil.Replace(this.vdrManIntroducerPoint.ErrorMessage);
		this.vdeManIntroducerPoint.ErrorMessage = DisplayWordUtil.Replace(this.vdeManIntroducerPoint.ErrorMessage);
		this.vdrFindManElapasedDays.ErrorMessage = DisplayWordUtil.Replace(this.vdrFindManElapasedDays.ErrorMessage);
		this.vdeFindManElapasedDays.ErrorMessage = DisplayWordUtil.Replace(this.vdeFindManElapasedDays.ErrorMessage);
		this.vdrManNewFaceDays.ErrorMessage = DisplayWordUtil.Replace(this.vdrManNewFaceDays.ErrorMessage);
		this.vdeManNewFaceDays.ErrorMessage = DisplayWordUtil.Replace(this.vdeManNewFaceDays.ErrorMessage);
		this.vdrProfileMovieNewHours.ErrorMessage = DisplayWordUtil.Replace(this.vdrProfileMovieNewHours.ErrorMessage);
		this.vdaProfileMovieNewHours.ErrorMessage = DisplayWordUtil.Replace(this.vdaProfileMovieNewHours.ErrorMessage);
		this.vdrBbsNewHours.ErrorMessage = DisplayWordUtil.Replace(this.vdrBbsNewHours.ErrorMessage);
		this.vdaBbsNewHours.ErrorMessage = DisplayWordUtil.Replace(this.vdaBbsNewHours.ErrorMessage);
		this.vdrPicBbsNewHours.ErrorMessage = DisplayWordUtil.Replace(this.vdrPicBbsNewHours.ErrorMessage);
		this.vdaPicBbsNewHours.ErrorMessage = DisplayWordUtil.Replace(this.vdaPicBbsNewHours.ErrorMessage);
		this.vdrMovieBbsNewHours.ErrorMessage = DisplayWordUtil.Replace(this.vdrMovieBbsNewHours.ErrorMessage);
		this.vdaMovieBbsNewHours.ErrorMessage = DisplayWordUtil.Replace(this.vdaMovieBbsNewHours.ErrorMessage);
		this.vdrRxMailNewHours.ErrorMessage = DisplayWordUtil.Replace(this.vdrRxMailNewHours.ErrorMessage);
		this.vdaRxMailNewHours.ErrorMessage = DisplayWordUtil.Replace(this.vdaRxMailNewHours.ErrorMessage);
		this.chkRegistHandleNmInputFlag.Text = DisplayWordUtil.Replace(this.chkRegistHandleNmInputFlag.Text);
		this.chkRegistBirthdayInputFlag.Text = DisplayWordUtil.Replace(this.chkRegistBirthdayInputFlag.Text);
		this.chkCastRegistNeedAgreeFlag.Text = DisplayWordUtil.Replace(this.chkCastRegistNeedAgreeFlag.Text);
		this.vdrManOnlineMin.ErrorMessage = DisplayWordUtil.Replace(this.vdrManOnlineMin.ErrorMessage);
		this.vdrMobileManDetailListCount.ErrorMessage = DisplayWordUtil.Replace(this.vdrMobileManDetailListCount.ErrorMessage);
		this.vdrMobileManListCount.ErrorMessage = DisplayWordUtil.Replace(this.vdrMobileManListCount.ErrorMessage);
		this.btnManNoPic.Text = DisplayWordUtil.Replace(this.btnManNoPic.Text);
		//ﾃﾞﾌｫﾙﾄをｷｬｽﾄのｵﾝﾗｲﾝ状態に関わらず受信するに変更
		trCastOnlineMailRxFlag.Visible = false;
		PlcVisible();
	}

	private void ClearField() {
		txtCmMailRxLimit.Text = "";
		txtFindManElapasedDays.Text = "";
		txtInfoEMailAddr.Text = "";
		txtReportEMailAddr.Text = "";
		txtInviteMailAvaHour.Text = "";
		txtNewFaceDays.Text = "";
		txtNewFaceMark.Text = "";
		txtManNewFaceDays.Text = "";
		txtVoiceNewDays.Text = "0";
		txtProfileMovieNewHours.Text = "";
		txtBbsNewHours.Text = "";
		txtPicBbsNewHours.Text = "";
		txtMovieBbsNewHours.Text = "";
		txtRxMailNewHours.Text = "";
		txtRegistServicePoint.Text = "";
		txtRegistCastBonusPoint.Text = "";
		lstManIntroPointGetTiming.SelectedIndex = 0;
		txtManIntroducerPoint.Text = "";
		txtManIntroPoint.Text = "";
		txtCastIntroducerPoint.Text = "";
		txtCastIntroPoint.Text = "";
		txtManNewRegistOkAmt.Text = "";
		txtCastNewRegistOkAmt.Text = "";
		txtMeasuredRateServicePoint.Text = "0";
		txtMeasuredRateLimitPoint.Text = "0";
		txtZeroSettleServicePoint.Text = "";
		txtReInviteMailNAHour.Text = "";
		txtReBatchMailNAHour.Text = "";
		txtSupportEmailAddr.Text = "";
		txtCastRegistReportEMailAddr.Text = "";
		txtCastModifyAccountEMailAddr.Text = "";
		txtManUploadEmailAddr.Text = "";
		txtCastUploadEmailAddr.Text = "";
		txtAgentRegistEmailAddr.Text = "";
		txtSupportTel.Text = "";
		lstWebFaceSeq.SelectedIndex = 0;
		lstTopPageSeekMode.SelectedIndex = 0;
		txtOfflineGuidance.Text = "";
		txtLoginedGuidance.Text = "";
		txtWaittingGuidance.Text = "";
		txtCallingGuidance.Text = "";
		txtTalkingWShotGuidance.Text = "";
		txtTalkingPartyGuidance.Text = "";
		txtConnectTypeVideoGuidance.Text = "";
		txtConnectTypeBothGuidance.Text = "";
		txtConnectTypeVoiceGuidance.Text = "";
		txtLoginMailIntervalHour.Text = "";
		txtLogoutMailIntervalHour.Text = "";
		txtLogoutMailValidTalkSec.Text = "";
		txtAproachMailIntervalHour.Text = "";
		txtHeaderHtmlDoc.Text = "";
		txtFooterHtmlDoc.Text = "";
		chkTxMailRefMarking.Checked = false;
		chkInviteTalkCastChargeFlag.Checked = false;
		chkBbsDupChargeFlag.Checked = false;
		chkAttachedMailDupChargeFlag.Checked = false;
		chkUseAttachedMailFlag.Checked = false;
		chkRegistHandleNmInputFlag.Checked = false;
		chkRegistBirthdayInputFlag.Checked = false;
		chkCastRegistNeedAgreeFlag.Checked = false;
		chkProfilePageingFlag.Checked = false;
		chkProfilePageingFlag2.Checked = false;
		chkCastCanSelectMonitorFlag.Checked = false;
		chkCastCanSelectConnectType.Checked = false;
		chkCastOnlineMailRxFlag.Checked = true;
		chkNonAdRegistReqAgeCertFlag.Checked = false;
		chkBbsPicAttrFlag.Checked = false;
		chkBbsMovieAttrFlag.Checked = false;
		chkSupportChgObjCatFlag.Checked = false;
		chkSupportVoiceMonitorFlag.Checked = false;
		chkIntroducerRateOnFlag.Checked = false;
		chkCastAutoRegistCharFlag.Checked = false;
		rdoNotifyBothTalk.Checked = false;
		rdoNotifyTvTalk.Checked = false;
		rdoNotifyVoiceTalk.Checked = false;
		txtProfilePreviousGuidance.Text = "";
		txtProfileNextGuidance.Text = "";
		txtProfilePreviousGuidance2.Text = "";
		txtProfileNextGuidance2.Text = "";
		txtPreviousAccessKey.Text = "";
		txtNextAccessKey.Text = "";
		txtRankingCreateCount.Text = "";
		txtMailEmbedDomain.Text = "";
		txtOnlineAdjustMinFrom.Text = "";
		txtOnlineAdjustMinTo.Text = "";
		txtManOnlineMin.Text = "";
		txtWShotTalkStartGuidance.Text = "";
		txtPublicTalkStartGuidance.Text = "";
		txtTalkStartNotChargeSec.Text = "";
		txtTalk2StartNotChargeSec.Text = "";
		txtPcRedirectUrl.Text = "";
		txtPageTitle.Text = "";
		txtPageKeyword.Text = "";
		txtPageDescription.Text = "";
		txtMobileManDetailListCount.Text = "";
		txtMobileManListCount.Text = "";
		txtMobileWomanDetailListCount.Text = "";
		txtMobileWomanListCount.Text = "";
		txtPickupAutoSetCount.Text = "";
		txtCastRichinoSeekPaymentCnt.Text = "0";
		txtCastRichinoSeekPaymentAmt.Text = "0";
		lstMaqiaDMMailTiming.SelectedIndex = 0;
		lstRegistDMMailTiming.SelectedIndex = 0;
		txtIntroPointManToCast.Text = "0";
		txtIntroPointCastToMan.Text = "0";
		txtIntroManToCastMinPoint.Text = "0";
		chkDummyTalkTxLoginMailFlag.Checked = false;
		chkLoginMailNotSendFlag.Checked = false;
		chkLoginMail2NotSendFlag.Checked = false;
		chkLogoutMailNotSendFlag.Checked = false;
		txtManDefaultHandleNm.Text = "";
		rdoCastIntroPointGetTiming1.Checked = false;
		rdoCastIntroPointGetTiming2.Checked = false;
		rdoCastIntroPointGetTiming3.Checked = false;
		lstImpCastFirstLoginAct.SelectedIndex = 0;
		lstImpManFirstLoginAct.SelectedIndex = 0;
		chkManAutoLoginResignedFlag.Checked = false;
		chkManSecessionDelDataFlag.Checked = false;
		chkCastAutoLoginResignedFlag.Checked = false;
		chkCastSecessionDelDataFlag.Checked = false;
		txtJealousyEffectDelayMin.Text = "0";
		txtOutcomeCountDays.Text = "0";
	}

	private void SetMailCastAttrTypeSeq() {
		lstMailCastAttrTypeSeq1.Items.Clear();
		lstMailCastAttrTypeSeq2.Items.Clear();

		using (CastAttrType oCastAttrType = new CastAttrType()) {
			DataSet ds = oCastAttrType.GetList(lblSiteCd.Text);
			lstMailCastAttrTypeSeq1.Items.Add(new ListItem("指定なし",""));
			lstMailCastAttrTypeSeq2.Items.Add(new ListItem("指定なし",""));
			for (int i = 0;i < ds.Tables[0].Rows.Count;i++) {
				lstMailCastAttrTypeSeq1.Items.Add(new ListItem(ds.Tables[0].Rows[i]["CAST_ATTR_TYPE_NM"].ToString(),ds.Tables[0].Rows[i]["CAST_ATTR_TYPE_SEQ"].ToString()));
				lstMailCastAttrTypeSeq2.Items.Add(new ListItem(ds.Tables[0].Rows[i]["CAST_ATTR_TYPE_NM"].ToString(),ds.Tables[0].Rows[i]["CAST_ATTR_TYPE_SEQ"].ToString()));
			}
		}
	}

	private void SetInqManTypeSeq() {
		lstInqManTypeSeq1.Items.Clear();
		lstInqManTypeSeq2.Items.Clear();
		lstInqManTypeSeq3.Items.Clear();
		lstInqManTypeSeq4.Items.Clear();

		lstInqAdminManTypeSeq1.Items.Clear();
		lstInqAdminManTypeSeq2.Items.Clear();
		lstInqAdminManTypeSeq3.Items.Clear();
		lstInqAdminManTypeSeq4.Items.Clear();

		lstInqAdminCastTypeSeq1.Items.Clear();
		lstInqAdminCastTypeSeq2.Items.Clear();
		lstInqAdminCastTypeSeq3.Items.Clear();
		lstInqAdminCastTypeSeq4.Items.Clear();

		using (UserManAttrType oUserManAttrType = new UserManAttrType()) {
			DataSet ds = oUserManAttrType.GetList(lblSiteCd.Text);
			lstInqManTypeSeq1.Items.Add(new ListItem("指定なし",""));
			lstInqManTypeSeq2.Items.Add(new ListItem("指定なし",""));
			lstInqManTypeSeq3.Items.Add(new ListItem("指定なし",""));
			lstInqManTypeSeq4.Items.Add(new ListItem("指定なし",""));
			lstInqAdminManTypeSeq1.Items.Add(new ListItem("指定なし",""));
			lstInqAdminManTypeSeq2.Items.Add(new ListItem("指定なし",""));
			lstInqAdminManTypeSeq3.Items.Add(new ListItem("指定なし",""));
			lstInqAdminManTypeSeq4.Items.Add(new ListItem("指定なし",""));
			for (int i = 0;i < ds.Tables[0].Rows.Count;i++) {
				if (ds.Tables[0].Rows[i]["INPUT_TYPE"].ToString().Equals(ViCommConst.INPUT_TYPE_LIST)) {
					lstInqManTypeSeq1.Items.Add(new ListItem(ds.Tables[0].Rows[i]["MAN_ATTR_TYPE_NM"].ToString(),ds.Tables[0].Rows[i]["MAN_ATTR_TYPE_SEQ"].ToString()));
					lstInqManTypeSeq2.Items.Add(new ListItem(ds.Tables[0].Rows[i]["MAN_ATTR_TYPE_NM"].ToString(),ds.Tables[0].Rows[i]["MAN_ATTR_TYPE_SEQ"].ToString()));
					lstInqManTypeSeq3.Items.Add(new ListItem(ds.Tables[0].Rows[i]["MAN_ATTR_TYPE_NM"].ToString(),ds.Tables[0].Rows[i]["MAN_ATTR_TYPE_SEQ"].ToString()));
					lstInqManTypeSeq4.Items.Add(new ListItem(ds.Tables[0].Rows[i]["MAN_ATTR_TYPE_NM"].ToString(),ds.Tables[0].Rows[i]["MAN_ATTR_TYPE_SEQ"].ToString()));
					lstInqAdminManTypeSeq1.Items.Add(new ListItem(ds.Tables[0].Rows[i]["MAN_ATTR_TYPE_NM"].ToString(),ds.Tables[0].Rows[i]["MAN_ATTR_TYPE_SEQ"].ToString()));
					lstInqAdminManTypeSeq2.Items.Add(new ListItem(ds.Tables[0].Rows[i]["MAN_ATTR_TYPE_NM"].ToString(),ds.Tables[0].Rows[i]["MAN_ATTR_TYPE_SEQ"].ToString()));
					lstInqAdminManTypeSeq3.Items.Add(new ListItem(ds.Tables[0].Rows[i]["MAN_ATTR_TYPE_NM"].ToString(),ds.Tables[0].Rows[i]["MAN_ATTR_TYPE_SEQ"].ToString()));
					lstInqAdminManTypeSeq4.Items.Add(new ListItem(ds.Tables[0].Rows[i]["MAN_ATTR_TYPE_NM"].ToString(),ds.Tables[0].Rows[i]["MAN_ATTR_TYPE_SEQ"].ToString()));
				}
			}
		}
		using (CastAttrType oCastAttrType = new CastAttrType()) {
			DataSet ds = oCastAttrType.GetList(lblSiteCd.Text);
			lstInqAdminCastTypeSeq1.Items.Add(new ListItem("指定なし",""));
			lstInqAdminCastTypeSeq2.Items.Add(new ListItem("指定なし",""));
			lstInqAdminCastTypeSeq3.Items.Add(new ListItem("指定なし",""));
			lstInqAdminCastTypeSeq4.Items.Add(new ListItem("指定なし",""));
			for (int i = 0;i < ds.Tables[0].Rows.Count;i++) {
				if (ds.Tables[0].Rows[i]["INPUT_TYPE"].ToString().Equals(ViCommConst.INPUT_TYPE_LIST)) {
					lstInqAdminCastTypeSeq1.Items.Add(new ListItem(ds.Tables[0].Rows[i]["CAST_ATTR_TYPE_NM"].ToString(),ds.Tables[0].Rows[i]["CAST_ATTR_TYPE_SEQ"].ToString()));
					lstInqAdminCastTypeSeq2.Items.Add(new ListItem(ds.Tables[0].Rows[i]["CAST_ATTR_TYPE_NM"].ToString(),ds.Tables[0].Rows[i]["CAST_ATTR_TYPE_SEQ"].ToString()));
					lstInqAdminCastTypeSeq3.Items.Add(new ListItem(ds.Tables[0].Rows[i]["CAST_ATTR_TYPE_NM"].ToString(),ds.Tables[0].Rows[i]["CAST_ATTR_TYPE_SEQ"].ToString()));
					lstInqAdminCastTypeSeq4.Items.Add(new ListItem(ds.Tables[0].Rows[i]["CAST_ATTR_TYPE_NM"].ToString(),ds.Tables[0].Rows[i]["CAST_ATTR_TYPE_SEQ"].ToString()));
				}
			}
		}
	}

	private void SetMailTemplate() {
		lstMailTemplateCash1.Items.Clear();
		lstMailTemplateCash2.Items.Clear();
		lstMailTemplateIntro.Items.Clear();
		lstMailTemplateRegistDM.Items.Clear();
		lstMailTemplateRegistDM2.Items.Clear();
		lstMailTemplateRegistMaqiaDM.Items.Clear();
		lstMailTemplateCastReg.Items.Clear();
		lstMailTemplateCastPreReg.Items.Clear();
		lstMailTemplateCastManReg.Items.Clear();
		lstManTalkCastDiscMailNo.Items.Clear();
		lstManTalkManDiscMailNo.Items.Clear();
		lstManTalkPointDiscMailNo.Items.Clear();
		lstManTalkSysDiscMailNo.Items.Clear();
		lstCastTalkCastDiscMailNo.Items.Clear();
		lstCastTalkManDiscMailNo.Items.Clear();
		lstCastTalkPointDiscMailNo.Items.Clear();
		lstCastTalkSysDiscMailNo.Items.Clear();
		lstCastIntroPointMailNo.Items.Clear();
		lstTpManWaitingCastWithTalk.Items.Clear();

		using (MailTemplate oMail = new MailTemplate()) {
			DataSet ds = oMail.GetNewVerList(lblSiteCd.Text,null);
			lstMailTemplateCash1.Items.Add(new ListItem("指定なし",""));
			lstMailTemplateCash2.Items.Add(new ListItem("指定なし",""));
			lstMailTemplateIntro.Items.Add(new ListItem("指定なし",""));
			lstMailTemplateRegistDM.Items.Add(new ListItem("指定なし",""));
			lstMailTemplateRegistDM2.Items.Add(new ListItem("指定なし",""));
			lstMailTemplateRegistMaqiaDM.Items.Add(new ListItem("指定なし",""));
			lstMailTemplateCastReg.Items.Add(new ListItem("指定なし",""));
			lstMailTemplateCastPreReg.Items.Add(new ListItem("指定なし",""));
			lstMailTemplateCastManReg.Items.Add(new ListItem("指定なし",""));
			lstManTalkCastDiscMailNo.Items.Add(new ListItem("指定なし",""));
			lstManTalkManDiscMailNo.Items.Add(new ListItem("指定なし",""));
			lstManTalkPointDiscMailNo.Items.Add(new ListItem("指定なし",""));
			lstManTalkSysDiscMailNo.Items.Add(new ListItem("指定なし",""));
			lstCastTalkCastDiscMailNo.Items.Add(new ListItem("指定なし",""));
			lstCastTalkManDiscMailNo.Items.Add(new ListItem("指定なし",""));
			lstCastTalkPointDiscMailNo.Items.Add(new ListItem("指定なし",""));
			lstCastTalkSysDiscMailNo.Items.Add(new ListItem("指定なし",""));
			lstCastIntroPointMailNo.Items.Add(new ListItem("指定なし",""));
			lstTpManWaitingCastWithTalk.Items.Add(new ListItem("指定なし",""));
			for (int i = 0;i < ds.Tables[0].Rows.Count;i++) {
				switch (ds.Tables[0].Rows[i]["SEX_CD"].ToString()) {
					case ViCommConst.UsableSexCd.MAN:
						lstMailTemplateCash1.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
						lstMailTemplateCash2.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
						lstMailTemplateIntro.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
						lstMailTemplateRegistDM.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
						lstMailTemplateRegistDM2.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
						lstMailTemplateRegistMaqiaDM.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
						break;
					case ViCommConst.UsableSexCd.WOMAN:
						lstMailTemplateCastReg.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
						lstMailTemplateCastPreReg.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
						lstMailTemplateCastManReg.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
						lstCastIntroPointMailNo.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
						break;
					default:
						lstMailTemplateCash1.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
						lstMailTemplateCash2.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
						lstMailTemplateIntro.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
						lstMailTemplateRegistDM.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
						lstMailTemplateRegistDM2.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
						lstMailTemplateRegistMaqiaDM.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
						lstMailTemplateCastReg.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
						lstMailTemplateCastPreReg.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
						lstMailTemplateCastManReg.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
						lstCastIntroPointMailNo.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
						break;
				}

				if (ds.Tables[0].Rows[i]["MAIL_TEMPLATE_TYPE"].ToString().Equals(ViCommConst.MAIL_TP_INFO)) {
					lstManTalkCastDiscMailNo.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
					lstManTalkManDiscMailNo.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
					lstManTalkPointDiscMailNo.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
					lstManTalkSysDiscMailNo.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
				} else if (ds.Tables[0].Rows[i]["MAIL_TEMPLATE_TYPE"].ToString().Equals(ViCommConst.MAIL_TP_CAST_MAIL_MAGAZINE)) {
					lstCastTalkCastDiscMailNo.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
					lstCastTalkManDiscMailNo.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
					lstCastTalkPointDiscMailNo.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
					lstCastTalkSysDiscMailNo.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
				} else if (ds.Tables[0].Rows[i]["MAIL_TEMPLATE_TYPE"].ToString().Equals(ViCommConst.MAIL_TP_NOTIFY_LOGIN) &&
					ViCommConst.UsableSexCd.MAN.Equals(ds.Tables[0].Rows[i]["SEX_CD"].ToString())) {
					lstTpManWaitingCastWithTalk.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
				}
			}
		}
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void lnkSiteManagementCd_Command(object sender,CommandEventArgs e) {
		lblSiteCd.Text = e.CommandArgument.ToString();
		GetData();
	}

	protected void btnCastNoPic_Click(object sender,EventArgs e) {
		if (uldCastNoPic.HasFile) {
			SetupDir();

			using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
				uldCastNoPic.SaveAs(fileNmCastNoPic);
			}
			Response.Redirect(string.Format("SiteManagementList.aspx?site={0}",lblSiteCd.Text));
		}
	}

	protected void btnCastNoPicSmall_Click(object sender,EventArgs e) {
		if (uldCastNoPicSmall.HasFile) {
			SetupDir();
			using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
				uldCastNoPicSmall.SaveAs(fileNmCastNoPicSmall);
			}
			Response.Redirect(string.Format("SiteManagementList.aspx?site={0}",lblSiteCd.Text));
		}
	}

	protected void btnManNoPic_Click(object sender,EventArgs e) {
		if (uldManNoPic.HasFile) {
			SetupDir();
			using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
				uldManNoPic.SaveAs(fileNmManNoPic);
			}
			Response.Redirect(string.Format("SiteManagementList.aspx?site={0}",lblSiteCd.Text));
		}
	}

	protected void lnkSiteCd_Command(object sender,CommandEventArgs e) {
		lblSiteCd.Text = e.CommandArgument.ToString();
		if (lblSiteCd.Text.Equals("") == false) {
			SetMailCastAttrTypeSeq();
			SetInqManTypeSeq();
			SetMailTemplate();
		}
		GetData();
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SITE_MANAGEMENT_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lblSiteCd.Text);
			db.ProcedureOutParm("PWSHOT_TALK_START_GUIDANCE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPUB_TALK_START_GUIDANCE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPC_REDIRECT_URL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPAGE_TITLE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPAGE_KEYWORD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPAGE_DESCRIPTION",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREGIST_SERVICE_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PREGIST_CAST_BONUS_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PINVITE_MAIL_AVA_HOUR",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRE_INVITE_MAIL_NA_HOUR",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRE_BATCH_MAIL_NA_HOUR",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PTX_MAIL_REF_MARKING",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PLOGIN_MAIL_INTERVAL_HOUR",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PLOGOUT_MAIL_INTERVAL_HOUR",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PLOGOUT_MAIL_VALID_TALK_SEC",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PAPROACH_MAIL_INTERVAL_HOUR",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PNEW_FACE_DAYS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PNEW_FACE_MARK",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMAN_NEW_FACE_DAYS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PVOICE_NEW_DAYS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PPRODUCT_NEW_DAYS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PPROFILE_MOVIE_NEW_HOURS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PBBS_NEW_HOURS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PPIC_BBS_NEW_HOURS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMOVIE_BBS_NEW_HOURS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRX_MAIL_NEW_HOURS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PFIND_MAN_ELAPASED_DAYS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCM_MAIL_RX_LIMIT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSUPPORT_EMAIL_ADDR",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PINFO_EMAIL_ADDR",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREPORT_EMAIL_ADDR",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_REGIST_REPORT_EMAIL_ADDR",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_MODIFY_ACC_EMAIL_ADDR",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMAN_UPLOAD_EMAIL_ADDR",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_UPLOAD_EMAIL_ADDR",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PAGENT_REGIST_EMAIL_ADDR",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSUPPORT_TEL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PWEB_FACE_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHEADER_HTML_DOC",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PFOOTER_HTML_DOC",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMAIL_CAST_ATTR_TYPE_SEQ1",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMAIL_CAST_ATTR_TYPE_SEQ2",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PINQ_MAN_TYPE_SEQ1",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PINQ_MAN_TYPE_SEQ2",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PINQ_MAN_TYPE_SEQ3",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PINQ_MAN_TYPE_SEQ4",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PADMIN_INQ_MAN_TYPE_SEQ1",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PADMIN_INQ_MAN_TYPE_SEQ2",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PADMIN_INQ_MAN_TYPE_SEQ3",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PADMIN_INQ_MAN_TYPE_SEQ4",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PADMIN_INQ_CAST_TYPE_SEQ1",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PADMIN_INQ_CAST_TYPE_SEQ2",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PADMIN_INQ_CAST_TYPE_SEQ3",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PADMIN_INQ_CAST_TYPE_SEQ4",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PINVITE_TALK_CAST_CHARGE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_MINIMUM_PAYMENT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PNO_USE_RETURN_MAIL_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PBBS_DUP_CHARGE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PATTACHED_MAIL_DUP_CHARGE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PUSE_ATTACHED_MAIL_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PREGIST_HANDLE_NM_INPUT_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PREGIST_BIRTHDAY_INPUT_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_REGIST_NEED_AGREE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMAN_INTRO_POINT_GET_TIMING",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMAN_INTRODUCER_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMAN_INTRO_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_INTRO_POINT_GET_TIMING",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_INTRODUCER_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_INTRO_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMEASURED_RATE_SERVICE_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMEASURED_RATE_LIMIT_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PZERO_SETTLE_SERVICE_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PTOP_PAGE_SEEK_MODE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("POFFLINE_GUIDANCE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PLOGINED_GUIDANCE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PWAITING_GUIDANCE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCALLING_GUIDANCE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PTALKING_WSHOT_GUIDANCE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PTALKING_PARTY_GUIDANCE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCONNECT_TYPE_VIDEO_GUIDANCE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCONNECT_TYPE_BOTH_GUIDANCE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCONNECT_TYPE_VOICE_GUIDANCE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPROFILE_PAGEING_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PPROFILE_PREVIOUS_GUIDANCE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPROFILE_NEXT_GUIDANCE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPROFILE_PAGEING_FLAG2",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PPROFILE_PREVIOUS_GUIDANCE2",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPROFILE_NEXT_GUIDANCE2",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPREVIOUS_ACCESS_KEY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PNEXT_ACCESS_KEY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PRANKING_CREATE_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_CAN_SELECT_MONITOR_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_CAN_SELECT_CONNECT_TYPE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMAIL_EMBED_DOMAIN",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PONLINE_ADJUST_MIN_FROM",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PONLINE_ADJUST_MIN_TO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMAN_ONLINE_MIN",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCASH_MAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCASH_SHORT_MAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PINTRO_POINT_MAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREGIST_DM_MAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREGIST_DM_MAIL_TEMPLATE_NO2",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMAQIA_DM_MAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_REG_MAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_PREREG_MAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_MAN_MAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREGIST_DM_MAIL_TIMING",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMAQIA_DM_MAIL_TIMING",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_ONLINE_MAIL_RX_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PTALK_START_NOT_CHARGE_SEC",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PTALK2_START_NOT_CHARGE_SEC",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("NON_AD_REGIST_REQ_AGECERT_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMOBILE_MAN_DETAIL_LIST_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMOBILE_MAN_LIST_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMOBILE_WOMAN_DETAIL_LIST_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMOBILE_WOMAN_LIST_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PPICKUP_AUTO_SET_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PPICKUP_AUTO_INTERVAL_MIN",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PBBS_PIC_ATTR_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PBBS_MOVIE_ATTR_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSUPPORT_VOICE_MONITOR_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PINTRO_POINT_MAN_TO_CAST",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PINTRO_POINT_CAST_TO_MAN",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PINTRO_MAN_TO_CAST_MIN_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PINTRODUCER_RATE_ON_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_RICHINO_SEEK_PAYMENT_CNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_RICHINO_SEEK_PAYMENT_AMT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSUPPORT_CHG_OBJ_CAT_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMAN_NEW_REGIST_OK_AMT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_NEW_REGIST_OK_AMT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_WRITE_BBS_LIMIT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_WRITE_BBS_INTERVAL_MIN",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMAN_WRITE_BBS_LIMIT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMAN_WRITE_BBS_INTERVAL_MIN",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PPAY_MAN_WRITE_BBS_LIMIT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PPAYMAN_WRITE_BBS_INTERVAL_MIN",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PLOGIN_MAIL_NOT_SEND_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PLOGIN_MAIL2_NOT_SEND_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PLOGOUT_MAIL_NOT_SEND_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMAN_TALK_END_SEND_MAIL_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMAN_TALK_CAST_DISC_MAIL_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMAN_TALK_MAN_DISC_MAIL_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMAN_TALK_POINT_DISC_MAIL_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMAN_TALK_SYS_DISC_MAIL_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_TALK_END_SEND_MAIL_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_TALK_CAST_DISC_MAIL_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_TALK_MAN_DISC_MAIL_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_TALK_POINT_DISC_MAIL_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_TALK_SYS_DISC_MAIL_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_AUTO_REGIST_CHAR_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMAN_DEFAULT_HANDLE_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PIMP_CAST_FIRST_LOGIN_ACT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PIMP_MAN_FIRST_LOGIN_ACT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PDUMMY_TALK_TX_LOGIN_MAIL_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_INTRO_POINT_MAIL_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMAN_AUTO_LOGIN_RESIGNED_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMAN_SECESSION_DEL_DATA_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_AUTO_LOGIN_RESIGNED_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_SECESSION_DEL_DATA_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pSITE_HTML_DOC_SEX_CHECK_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pJEALOUSY_EFFECT_DELAY_MIN",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pOUTCOME_COUNT_DAYS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PPF_TALK_NOTIFY_MASK",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID_SITE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO_SITE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID_MANAGEMENT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO_MANAGEMENT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();

			ViewState["REVISION_NO_SITE"] = db.GetStringValue("PREVISION_NO_SITE");
			ViewState["ROWID_SITE"] = db.GetStringValue("PROWID_SITE");
			ViewState["REVISION_NO_MANAGEMENT"] = db.GetStringValue("PREVISION_NO_MANAGEMENT");
			ViewState["ROWID_MANAGEMENT"] = db.GetStringValue("PROWID_MANAGEMENT");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtCastMinimumPayment.Text = db.GetStringValue("PCAST_MINIMUM_PAYMENT");
				txtRegistServicePoint.Text = db.GetStringValue("PREGIST_SERVICE_POINT");
				txtRegistCastBonusPoint.Text = db.GetStringValue("PREGIST_CAST_BONUS_POINT");
				lstManIntroPointGetTiming.SelectedValue = db.GetStringValue("PMAN_INTRO_POINT_GET_TIMING");
				txtManIntroducerPoint.Text = db.GetStringValue("PMAN_INTRODUCER_POINT");
				txtManIntroPoint.Text = db.GetStringValue("PMAN_INTRO_POINT");
				lstRegistDMMailTiming.SelectedValue = db.GetStringValue("PREGIST_DM_MAIL_TIMING");
				lstMaqiaDMMailTiming.SelectedValue = db.GetStringValue("PMAQIA_DM_MAIL_TIMING");
				txtCastIntroducerPoint.Text = db.GetStringValue("PCAST_INTRODUCER_POINT");
				txtCastIntroPoint.Text = db.GetStringValue("PCAST_INTRO_POINT");
				txtManNewRegistOkAmt.Text = db.GetStringValue("PMAN_NEW_REGIST_OK_AMT");
				txtCastNewRegistOkAmt.Text = db.GetStringValue("PCAST_NEW_REGIST_OK_AMT");
				txtMeasuredRateServicePoint.Text = db.GetStringValue("PMEASURED_RATE_SERVICE_POINT");
				txtMeasuredRateLimitPoint.Text = db.GetStringValue("PMEASURED_RATE_LIMIT_POINT");
				txtZeroSettleServicePoint.Text = db.GetStringValue("PZERO_SETTLE_SERVICE_POINT");
				txtInviteMailAvaHour.Text = db.GetStringValue("PINVITE_MAIL_AVA_HOUR");
				txtLoginMailIntervalHour.Text = db.GetStringValue("PLOGIN_MAIL_INTERVAL_HOUR");
				txtLogoutMailIntervalHour.Text = db.GetStringValue("PLOGOUT_MAIL_INTERVAL_HOUR");
				txtLogoutMailValidTalkSec.Text = db.GetStringValue("PLOGOUT_MAIL_VALID_TALK_SEC");
				txtAproachMailIntervalHour.Text = db.GetStringValue("PAPROACH_MAIL_INTERVAL_HOUR");
				txtReInviteMailNAHour.Text = db.GetStringValue("PRE_INVITE_MAIL_NA_HOUR");
				txtReBatchMailNAHour.Text = db.GetStringValue("PRE_BATCH_MAIL_NA_HOUR");
				chkTxMailRefMarking.Checked = db.GetStringValue("PTX_MAIL_REF_MARKING").Equals("1");
				chkInviteTalkCastChargeFlag.Checked = db.GetStringValue("PINVITE_TALK_CAST_CHARGE_FLAG").Equals("1");
				chkCastCanSelectMonitorFlag.Checked = db.GetStringValue("PCAST_CAN_SELECT_MONITOR_FLAG").Equals("1");
				chkCastCanSelectConnectType.Checked = db.GetStringValue("PCAST_CAN_SELECT_CONNECT_TYPE").Equals("1");
				txtNewFaceDays.Text = db.GetStringValue("PNEW_FACE_DAYS");
				txtNewFaceMark.Text = db.GetStringValue("PNEW_FACE_MARK");
				txtManNewFaceDays.Text = db.GetStringValue("PMAN_NEW_FACE_DAYS");
				txtVoiceNewDays.Text = db.GetStringValue("PVOICE_NEW_DAYS");
				txtProductNewDays.Text = db.GetStringValue("PPRODUCT_NEW_DAYS");
				txtProfileMovieNewHours.Text = db.GetStringValue("PPROFILE_MOVIE_NEW_HOURS");
				txtBbsNewHours.Text = db.GetStringValue("PBBS_NEW_HOURS");
				txtPicBbsNewHours.Text = db.GetStringValue("PPIC_BBS_NEW_HOURS");
				txtMovieBbsNewHours.Text = db.GetStringValue("PMOVIE_BBS_NEW_HOURS");
				txtRxMailNewHours.Text = db.GetStringValue("PRX_MAIL_NEW_HOURS");
				txtFindManElapasedDays.Text = db.GetStringValue("PFIND_MAN_ELAPASED_DAYS");
				txtCmMailRxLimit.Text = db.GetStringValue("PCM_MAIL_RX_LIMIT");
				txtSupportEmailAddr.Text = db.GetStringValue("PSUPPORT_EMAIL_ADDR");
				txtInfoEMailAddr.Text = db.GetStringValue("PINFO_EMAIL_ADDR");
				txtReportEMailAddr.Text = db.GetStringValue("PREPORT_EMAIL_ADDR");
				txtCastRegistReportEMailAddr.Text = db.GetStringValue("PCAST_REGIST_REPORT_EMAIL_ADDR");
				txtCastModifyAccountEMailAddr.Text = db.GetStringValue("PCAST_MODIFY_ACC_EMAIL_ADDR");
				txtManUploadEmailAddr.Text = db.GetStringValue("PMAN_UPLOAD_EMAIL_ADDR");
				txtCastUploadEmailAddr.Text = db.GetStringValue("PCAST_UPLOAD_EMAIL_ADDR");
				txtAgentRegistEmailAddr.Text = db.GetStringValue("PAGENT_REGIST_EMAIL_ADDR");
				txtSupportTel.Text = db.GetStringValue("PSUPPORT_TEL");
				lstWebFaceSeq.SelectedValue = db.GetStringValue("PWEB_FACE_SEQ");
				lstTopPageSeekMode.SelectedValue = db.GetStringValue("PTOP_PAGE_SEEK_MODE");
				txtOfflineGuidance.Text = db.GetStringValue("POFFLINE_GUIDANCE");
				txtLoginedGuidance.Text = db.GetStringValue("PLOGINED_GUIDANCE");
				txtWaittingGuidance.Text = db.GetStringValue("PWAITING_GUIDANCE");
				txtCallingGuidance.Text = db.GetStringValue("PCALLING_GUIDANCE");
				txtTalkingWShotGuidance.Text = db.GetStringValue("PTALKING_WSHOT_GUIDANCE");
				txtTalkingPartyGuidance.Text = db.GetStringValue("PTALKING_PARTY_GUIDANCE");
				txtConnectTypeVideoGuidance.Text = db.GetStringValue("PCONNECT_TYPE_VIDEO_GUIDANCE");
				txtConnectTypeBothGuidance.Text = db.GetStringValue("PCONNECT_TYPE_BOTH_GUIDANCE");
				txtConnectTypeVoiceGuidance.Text = db.GetStringValue("PCONNECT_TYPE_VOICE_GUIDANCE");
				txtHeaderHtmlDoc.Text = db.GetStringValue("PHEADER_HTML_DOC");
				txtFooterHtmlDoc.Text = db.GetStringValue("PFOOTER_HTML_DOC");
				chkNoUseReturnMailFlag.Checked = db.GetStringValue("PNO_USE_RETURN_MAIL_FLAG").Equals("1");
				chkBbsDupChargeFlag.Checked = db.GetStringValue("PBBS_DUP_CHARGE_FLAG").Equals("1");
				chkAttachedMailDupChargeFlag.Checked = db.GetStringValue("PATTACHED_MAIL_DUP_CHARGE_FLAG").Equals("1");
				chkUseAttachedMailFlag.Checked = db.GetStringValue("PUSE_ATTACHED_MAIL_FLAG").Equals("1");
				chkRegistHandleNmInputFlag.Checked = db.GetStringValue("PREGIST_HANDLE_NM_INPUT_FLAG").Equals("1");
				chkRegistBirthdayInputFlag.Checked = db.GetStringValue("PREGIST_BIRTHDAY_INPUT_FLAG").Equals("1");
				chkCastRegistNeedAgreeFlag.Checked = db.GetStringValue("PCAST_REGIST_NEED_AGREE_FLAG").Equals("1");
				chkProfilePageingFlag.Checked = db.GetStringValue("PPROFILE_PAGEING_FLAG").Equals("1");
				chkProfilePageingFlag2.Checked = db.GetStringValue("PPROFILE_PAGEING_FLAG2").Equals("1");
				chkCastOnlineMailRxFlag.Checked = db.GetStringValue("PCAST_ONLINE_MAIL_RX_FLAG").Equals("1");
				chkSupportVoiceMonitorFlag.Checked = db.GetStringValue("PSUPPORT_VOICE_MONITOR_FLAG").Equals("1");
				txtProfilePreviousGuidance.Text = db.GetStringValue("PPROFILE_PREVIOUS_GUIDANCE");
				txtProfileNextGuidance.Text = db.GetStringValue("PPROFILE_NEXT_GUIDANCE");
				txtProfilePreviousGuidance2.Text = db.GetStringValue("PPROFILE_PREVIOUS_GUIDANCE2");
				txtProfileNextGuidance2.Text = db.GetStringValue("PPROFILE_NEXT_GUIDANCE2");
				txtPreviousAccessKey.Text = db.GetStringValue("PPREVIOUS_ACCESS_KEY");
				txtNextAccessKey.Text = db.GetStringValue("PNEXT_ACCESS_KEY");
				txtRankingCreateCount.Text = db.GetStringValue("PRANKING_CREATE_COUNT");
				txtMailEmbedDomain.Text = db.GetStringValue("PMAIL_EMBED_DOMAIN");
				txtOnlineAdjustMinFrom.Text = db.GetStringValue("PONLINE_ADJUST_MIN_FROM");
				txtOnlineAdjustMinTo.Text = db.GetStringValue("PONLINE_ADJUST_MIN_TO");
				txtManOnlineMin.Text = db.GetStringValue("PMAN_ONLINE_MIN");
				txtWShotTalkStartGuidance.Text = db.GetStringValue("PWSHOT_TALK_START_GUIDANCE");
				txtPublicTalkStartGuidance.Text = db.GetStringValue("PPUB_TALK_START_GUIDANCE");
				txtTalkStartNotChargeSec.Text = db.GetStringValue("PTALK_START_NOT_CHARGE_SEC");
				txtTalk2StartNotChargeSec.Text = db.GetStringValue("PTALK2_START_NOT_CHARGE_SEC");
				chkNonAdRegistReqAgeCertFlag.Checked = db.GetStringValue("NON_AD_REGIST_REQ_AGECERT_FLAG").Equals("1");
				chkBbsPicAttrFlag.Checked = db.GetStringValue("PBBS_PIC_ATTR_FLAG").Equals("1");
				chkBbsMovieAttrFlag.Checked = db.GetStringValue("PBBS_MOVIE_ATTR_FLAG").Equals("1");
				chkSupportChgObjCatFlag.Checked = db.GetStringValue("PSUPPORT_CHG_OBJ_CAT_FLAG").Equals("1");

				rdoNotifyTvTalk.Checked = ((db.GetIntValue("PPF_TALK_NOTIFY_MASK") & ViCommConst.NOTIFY_TALK_TV) > 0);
				rdoNotifyVoiceTalk.Checked = ((db.GetIntValue("PPF_TALK_NOTIFY_MASK") & ViCommConst.NOTIFY_TALK_VOICE) > 0);
				rdoNotifyBothTalk.Checked = ((db.GetIntValue("PPF_TALK_NOTIFY_MASK") & ViCommConst.NOTIFY_TALK_BOTH) > 0);

				txtPcRedirectUrl.Text = db.GetStringValue("PPC_REDIRECT_URL");
				txtPageTitle.Text = db.GetStringValue("PPAGE_TITLE");
				txtPageKeyword.Text = db.GetStringValue("PPAGE_KEYWORD");
				txtPageDescription.Text = db.GetStringValue("PPAGE_DESCRIPTION");

				txtMobileManDetailListCount.Text = db.GetStringValue("PMOBILE_MAN_DETAIL_LIST_COUNT");
				txtMobileManListCount.Text = db.GetStringValue("PMOBILE_MAN_LIST_COUNT");
				txtMobileWomanDetailListCount.Text = db.GetStringValue("PMOBILE_WOMAN_DETAIL_LIST_COUNT");
				txtMobileWomanListCount.Text = db.GetStringValue("PMOBILE_WOMAN_LIST_COUNT");

				txtPickupAutoSetCount.Text = db.GetStringValue("PPICKUP_AUTO_SET_COUNT");
				txtPickupAutoIntervalMin.Text = db.GetStringValue("PPICKUP_AUTO_INTERVAL_MIN");

				txtIntroPointManToCast.Text = db.GetStringValue("PINTRO_POINT_MAN_TO_CAST");
				txtIntroPointCastToMan.Text = db.GetStringValue("PINTRO_POINT_CAST_TO_MAN");
				txtIntroManToCastMinPoint.Text = db.GetStringValue("PINTRO_MAN_TO_CAST_MIN_POINT");

				chkIntroducerRateOnFlag.Checked = db.GetStringValue("PINTRODUCER_RATE_ON_FLAG").Equals("1");

				txtCastRichinoSeekPaymentCnt.Text = db.GetStringValue("PCAST_RICHINO_SEEK_PAYMENT_CNT");
				txtCastRichinoSeekPaymentAmt.Text = db.GetStringValue("PCAST_RICHINO_SEEK_PAYMENT_AMT");

				if (db.GetIntValue("PCAST_INTRO_POINT_GET_TIMING") == 0) {
					rdoCastIntroPointGetTiming1.Checked = true;
				} else if (db.GetIntValue("PCAST_INTRO_POINT_GET_TIMING") == 1) {
					rdoCastIntroPointGetTiming3.Checked = true;
				} else {
					rdoCastIntroPointGetTiming2.Checked = true;
				}

				txtCastWriteBbsLimit.Text = db.GetStringValue("PCAST_WRITE_BBS_LIMIT");
				if (txtCastWriteBbsLimit.Text.Equals("-1"))
					txtCastWriteBbsLimit.Text = string.Empty;
				txtCastWriteBbsIntervalMin.Text = db.GetStringValue("PCAST_WRITE_BBS_INTERVAL_MIN");

				txtManWriteBbsLimit.Text = db.GetStringValue("PMAN_WRITE_BBS_LIMIT");
				if (txtManWriteBbsLimit.Text.Equals("-1"))
					txtManWriteBbsLimit.Text = string.Empty;
				txtManWriteBbsIntervalMin.Text = db.GetStringValue("PMAN_WRITE_BBS_INTERVAL_MIN");

				txtPayManWriteBbsLimit.Text = db.GetStringValue("PPAY_MAN_WRITE_BBS_LIMIT");
				if (txtPayManWriteBbsLimit.Text.Equals("-1"))
					txtPayManWriteBbsLimit.Text = string.Empty;
				txtPayManWriteBbsIntervalMin.Text = db.GetStringValue("PPAYMAN_WRITE_BBS_INTERVAL_MIN");

				for (int i = 0;i < lstMailCastAttrTypeSeq1.Items.Count;i++) {
					if (lstMailCastAttrTypeSeq1.Items[i].Value == db.GetStringValue("PMAIL_CAST_ATTR_TYPE_SEQ1")) {
						lstMailCastAttrTypeSeq1.SelectedIndex = i;
					}
				}
				for (int i = 0;i < lstMailCastAttrTypeSeq2.Items.Count;i++) {
					if (lstMailCastAttrTypeSeq2.Items[i].Value == db.GetStringValue("PMAIL_CAST_ATTR_TYPE_SEQ2")) {
						lstMailCastAttrTypeSeq2.SelectedIndex = i;
					}
				}
				for (int i = 0;i < lstInqManTypeSeq1.Items.Count;i++) {
					if (lstInqManTypeSeq1.Items[i].Value == db.GetStringValue("PINQ_MAN_TYPE_SEQ1")) {
						lstInqManTypeSeq1.SelectedIndex = i;
					}
				}
				for (int i = 0;i < lstInqManTypeSeq2.Items.Count;i++) {
					if (lstInqManTypeSeq2.Items[i].Value == db.GetStringValue("PINQ_MAN_TYPE_SEQ2")) {
						lstInqManTypeSeq2.SelectedIndex = i;
					}
				}
				for (int i = 0;i < lstInqManTypeSeq3.Items.Count;i++) {
					if (lstInqManTypeSeq3.Items[i].Value == db.GetStringValue("PINQ_MAN_TYPE_SEQ3")) {
						lstInqManTypeSeq3.SelectedIndex = i;
					}
				}
				for (int i = 0;i < lstInqManTypeSeq4.Items.Count;i++) {
					if (lstInqManTypeSeq4.Items[i].Value == db.GetStringValue("PINQ_MAN_TYPE_SEQ4")) {
						lstInqManTypeSeq4.SelectedIndex = i;
					}
				}
				for (int i = 0;i < lstInqAdminManTypeSeq1.Items.Count;i++) {
					if (lstInqAdminManTypeSeq1.Items[i].Value == db.GetStringValue("PADMIN_INQ_MAN_TYPE_SEQ1")) {
						lstInqAdminManTypeSeq1.SelectedIndex = i;
					}
				}
				for (int i = 0;i < lstInqAdminManTypeSeq2.Items.Count;i++) {
					if (lstInqAdminManTypeSeq2.Items[i].Value == db.GetStringValue("PADMIN_INQ_MAN_TYPE_SEQ2")) {
						lstInqAdminManTypeSeq2.SelectedIndex = i;
					}
				}
				for (int i = 0;i < lstInqAdminManTypeSeq3.Items.Count;i++) {
					if (lstInqAdminManTypeSeq3.Items[i].Value == db.GetStringValue("PADMIN_INQ_MAN_TYPE_SEQ3")) {
						lstInqAdminManTypeSeq3.SelectedIndex = i;
					}
				}
				for (int i = 0;i < lstInqAdminManTypeSeq4.Items.Count;i++) {
					if (lstInqAdminManTypeSeq4.Items[i].Value == db.GetStringValue("PADMIN_INQ_MAN_TYPE_SEQ4")) {
						lstInqAdminManTypeSeq4.SelectedIndex = i;
					}
				}
				for (int i = 0;i < lstInqAdminCastTypeSeq1.Items.Count;i++) {
					if (lstInqAdminCastTypeSeq1.Items[i].Value == db.GetStringValue("PADMIN_INQ_CAST_TYPE_SEQ1")) {
						lstInqAdminCastTypeSeq1.SelectedIndex = i;
					}
				}
				for (int i = 0;i < lstInqAdminCastTypeSeq2.Items.Count;i++) {
					if (lstInqAdminCastTypeSeq2.Items[i].Value == db.GetStringValue("PADMIN_INQ_CAST_TYPE_SEQ2")) {
						lstInqAdminCastTypeSeq2.SelectedIndex = i;
					}
				}
				for (int i = 0;i < lstInqAdminCastTypeSeq3.Items.Count;i++) {
					if (lstInqAdminCastTypeSeq3.Items[i].Value == db.GetStringValue("PADMIN_INQ_CAST_TYPE_SEQ3")) {
						lstInqAdminCastTypeSeq3.SelectedIndex = i;
					}
				}
				for (int i = 0;i < lstInqAdminCastTypeSeq4.Items.Count;i++) {
					if (lstInqAdminCastTypeSeq4.Items[i].Value == db.GetStringValue("PADMIN_INQ_CAST_TYPE_SEQ4")) {
						lstInqAdminCastTypeSeq4.SelectedIndex = i;
					}
				}

				this.lstMailTemplateCash1.SelectedIndex = this.lstMailTemplateCash1.Items.IndexOf(this.lstMailTemplateCash1.Items.FindByValue(db.GetStringValue("PCASH_MAIL_TEMPLATE_NO")));
				this.lstMailTemplateCash2.SelectedIndex = this.lstMailTemplateCash2.Items.IndexOf(this.lstMailTemplateCash2.Items.FindByValue(db.GetStringValue("PCASH_SHORT_MAIL_TEMPLATE_NO")));
				this.lstMailTemplateIntro.SelectedIndex = this.lstMailTemplateIntro.Items.IndexOf(this.lstMailTemplateIntro.Items.FindByValue(db.GetStringValue("PINTRO_POINT_MAIL_TEMPLATE_NO")));
				this.lstMailTemplateRegistDM.SelectedIndex = this.lstMailTemplateRegistDM.Items.IndexOf(this.lstMailTemplateRegistDM.Items.FindByValue(db.GetStringValue("PREGIST_DM_MAIL_TEMPLATE_NO")));
				this.lstMailTemplateRegistDM2.SelectedIndex = this.lstMailTemplateRegistDM2.Items.IndexOf(this.lstMailTemplateRegistDM2.Items.FindByValue(db.GetStringValue("PREGIST_DM_MAIL_TEMPLATE_NO2")));
				this.lstMailTemplateRegistMaqiaDM.SelectedIndex = this.lstMailTemplateRegistMaqiaDM.Items.IndexOf(this.lstMailTemplateRegistMaqiaDM.Items.FindByValue(db.GetStringValue("PMAQIA_DM_MAIL_TEMPLATE_NO")));
				this.lstMailTemplateCastReg.SelectedIndex = this.lstMailTemplateCastReg.Items.IndexOf(this.lstMailTemplateCastReg.Items.FindByValue(db.GetStringValue("PCAST_REG_MAIL_TEMPLATE_NO")));
				this.lstMailTemplateCastPreReg.SelectedIndex = this.lstMailTemplateCastPreReg.Items.IndexOf(this.lstMailTemplateCastPreReg.Items.FindByValue(db.GetStringValue("PCAST_PREREG_MAIL_TEMPLATE_NO")));
				this.lstMailTemplateCastManReg.SelectedIndex = this.lstMailTemplateCastManReg.Items.IndexOf(this.lstMailTemplateCastManReg.Items.FindByValue(db.GetStringValue("PCAST_MAN_MAIL_TEMPLATE_NO")));
				this.lstCastIntroPointMailNo.SelectedIndex = this.lstCastIntroPointMailNo.Items.IndexOf(this.lstCastIntroPointMailNo.Items.FindByValue(db.GetStringValue("PCAST_INTRO_POINT_MAIL_NO")));

				chkDummyTalkTxLoginMailFlag.Checked = db.GetStringValue("PDUMMY_TALK_TX_LOGIN_MAIL_FLAG").Equals("1");
				chkLoginMailNotSendFlag.Checked = db.GetStringValue("PLOGIN_MAIL_NOT_SEND_FLAG").Equals("1");
				chkLoginMail2NotSendFlag.Checked = db.GetStringValue("PLOGIN_MAIL2_NOT_SEND_FLAG").Equals("1");
				chkLogoutMailNotSendFlag.Checked = db.GetStringValue("PLOGOUT_MAIL_NOT_SEND_FLAG").Equals("1");

				chkManTalkEndSendFlag.Checked = db.GetStringValue("PMAN_TALK_END_SEND_MAIL_FLAG").Equals("1");
				chkCastTalkEndSendFlag.Checked = db.GetStringValue("PCAST_TALK_END_SEND_MAIL_FLAG").Equals("1");

				this.lstManTalkCastDiscMailNo.SelectedIndex = this.lstManTalkCastDiscMailNo.Items.IndexOf(this.lstManTalkCastDiscMailNo.Items.FindByValue(db.GetStringValue("PMAN_TALK_CAST_DISC_MAIL_NO")));
				this.lstManTalkManDiscMailNo.SelectedIndex = this.lstManTalkManDiscMailNo.Items.IndexOf(this.lstManTalkManDiscMailNo.Items.FindByValue(db.GetStringValue("PMAN_TALK_MAN_DISC_MAIL_NO")));
				this.lstManTalkPointDiscMailNo.SelectedIndex = this.lstManTalkPointDiscMailNo.Items.IndexOf(this.lstManTalkPointDiscMailNo.Items.FindByValue(db.GetStringValue("PMAN_TALK_POINT_DISC_MAIL_NO")));
				this.lstManTalkSysDiscMailNo.SelectedIndex = this.lstManTalkSysDiscMailNo.Items.IndexOf(this.lstManTalkSysDiscMailNo.Items.FindByValue(db.GetStringValue("PMAN_TALK_SYS_DISC_MAIL_NO")));

				this.lstCastTalkCastDiscMailNo.SelectedIndex = this.lstCastTalkCastDiscMailNo.Items.IndexOf(this.lstCastTalkCastDiscMailNo.Items.FindByValue(db.GetStringValue("PCAST_TALK_CAST_DISC_MAIL_NO")));
				this.lstCastTalkManDiscMailNo.SelectedIndex = this.lstCastTalkManDiscMailNo.Items.IndexOf(this.lstCastTalkManDiscMailNo.Items.FindByValue(db.GetStringValue("PCAST_TALK_MAN_DISC_MAIL_NO")));
				this.lstCastTalkPointDiscMailNo.SelectedIndex = this.lstCastTalkPointDiscMailNo.Items.IndexOf(this.lstCastTalkPointDiscMailNo.Items.FindByValue(db.GetStringValue("PCAST_TALK_POINT_DISC_MAIL_NO")));
				this.lstCastTalkSysDiscMailNo.SelectedIndex = this.lstCastTalkSysDiscMailNo.Items.IndexOf(this.lstCastTalkSysDiscMailNo.Items.FindByValue(db.GetStringValue("PCAST_TALK_SYS_DISC_MAIL_NO")));

				chkCastAutoRegistCharFlag.Checked = db.GetStringValue("PCAST_AUTO_REGIST_CHAR_FLAG").Equals("1");
				txtManDefaultHandleNm.Text = db.GetStringValue("PMAN_DEFAULT_HANDLE_NM");

				for (int i = 0;i < lstImpCastFirstLoginAct.Items.Count;i++) {
					if (lstImpCastFirstLoginAct.Items[i].Value == db.GetStringValue("PIMP_CAST_FIRST_LOGIN_ACT")) {
						lstImpCastFirstLoginAct.SelectedIndex = i;
					}
				}
				for (int i = 0;i < lstImpManFirstLoginAct.Items.Count;i++) {
					if (lstImpManFirstLoginAct.Items[i].Value == db.GetStringValue("PIMP_MAN_FIRST_LOGIN_ACT")) {
						lstImpManFirstLoginAct.SelectedIndex = i;
					}
				}

				chkManAutoLoginResignedFlag.Checked = db.GetStringValue("PMAN_AUTO_LOGIN_RESIGNED_FLAG").Equals("1");
				chkManSecessionDelDataFlag.Checked = db.GetStringValue("PMAN_SECESSION_DEL_DATA_FLAG").Equals("1");
				chkCastAutoLoginResignedFlag.Checked = db.GetStringValue("PCAST_AUTO_LOGIN_RESIGNED_FLAG").Equals("1");
				chkCastSecessionDelDataFlag.Checked = db.GetStringValue("PCAST_SECESSION_DEL_DATA_FLAG").Equals("1");
				chkSiteHtmlDocSexCheckFlag.Checked = ViCommConst.FLAG_ON_STR.Equals(db.GetStringValue("pSITE_HTML_DOC_SEX_CHECK_FLAG"));
				txtJealousyEffectDelayMin.Text = db.GetStringValue("PJEALOUSY_EFFECT_DELAY_MIN");
				txtOutcomeCountDays.Text = db.GetStringValue("pOUTCOME_COUNT_DAYS");

				using (SiteManagementEx oSiteManagementEx = new SiteManagementEx()) {
					string sTpManWaiingCastWithTalk = oSiteManagementEx.GetTpManWaiingCastWithTalk(lblSiteCd.Text);
					if (this.lstTpManWaitingCastWithTalk.Items.FindByValue(sTpManWaiingCastWithTalk) != null) {
						this.lstTpManWaitingCastWithTalk.SelectedValue = sTpManWaiingCastWithTalk;
					}
				}
			} else {
				ClearField();
			}
		}
		using (Site oSite = new Site()) {
			if (oSite.GetOne(lblSiteCd.Text)) {
				lblSiteNm.Text = oSite.siteNm;
			}
		}

		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		SetupImage();
	}

	private void UpdateData(int pDelFlag) {
		int iNotifyTalkMask = 0;
		if (rdoNotifyTvTalk.Checked) {
			iNotifyTalkMask += ViCommConst.NOTIFY_TALK_TV;
		}
		if (rdoNotifyVoiceTalk.Checked) {
			iNotifyTalkMask += ViCommConst.NOTIFY_TALK_VOICE;
		}
		if (rdoNotifyBothTalk.Checked) {
			iNotifyTalkMask += ViCommConst.NOTIFY_TALK_BOTH;
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SITE_MANAGEMENT_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lblSiteCd.Text);
			db.ProcedureInParm("PWSHOT_TALK_START_GUIDANCE",DbSession.DbType.VARCHAR2,txtWShotTalkStartGuidance.Text);
			db.ProcedureInParm("PPUB_TALK_START_GUIDANCE",DbSession.DbType.VARCHAR2,txtPublicTalkStartGuidance.Text);
			db.ProcedureInParm("PPC_REDIRECT_URL",DbSession.DbType.VARCHAR2,txtPcRedirectUrl.Text);
			db.ProcedureInParm("PPAGE_TITLE",DbSession.DbType.VARCHAR2,txtPageTitle.Text);
			db.ProcedureInParm("PPAGE_KEYWORD",DbSession.DbType.VARCHAR2,txtPageKeyword.Text);
			db.ProcedureInParm("PPAGE_DESCRIPTION",DbSession.DbType.VARCHAR2,txtPageDescription.Text);
			db.ProcedureInParm("PREGIST_SERVICE_POINT",DbSession.DbType.NUMBER,int.Parse(txtRegistServicePoint.Text));
			db.ProcedureInParm("PREGIST_CAST_BONUS_POINT",DbSession.DbType.NUMBER,int.Parse(txtRegistCastBonusPoint.Text));
			db.ProcedureInParm("PINVITE_MAIL_AVA_HOUR",DbSession.DbType.NUMBER,int.Parse(txtInviteMailAvaHour.Text));
			db.ProcedureInParm("PRE_INVITE_MAIL_NA_HOUR",DbSession.DbType.NUMBER,int.Parse(txtReInviteMailNAHour.Text));
			db.ProcedureInParm("PRE_BATCH_MAIL_NA_HOUR",DbSession.DbType.NUMBER,int.Parse(txtReBatchMailNAHour.Text));
			db.ProcedureInParm("PTX_MAIL_REF_MARKING",DbSession.DbType.NUMBER,chkTxMailRefMarking.Checked);
			db.ProcedureInParm("PLOGIN_MAIL_INTERVAL_HOUR",DbSession.DbType.NUMBER,int.Parse(txtLoginMailIntervalHour.Text));
			db.ProcedureInParm("PLOGOUT_MAIL_INTERVAL_HOUR",DbSession.DbType.NUMBER,int.Parse(txtLogoutMailIntervalHour.Text));
			db.ProcedureInParm("PLOGOUT_MAIL_VALID_TALK_SEC",DbSession.DbType.NUMBER,int.Parse(txtLogoutMailValidTalkSec.Text));
			db.ProcedureInParm("PAPROACH_MAIL_INTERVAL_HOUR",DbSession.DbType.NUMBER,int.Parse(txtAproachMailIntervalHour.Text));
			db.ProcedureInParm("PNEW_FACE_DAYS",DbSession.DbType.NUMBER,int.Parse(txtNewFaceDays.Text));
			db.ProcedureInParm("PNEW_FACE_MARK",DbSession.DbType.VARCHAR2,txtNewFaceMark.Text);
			db.ProcedureInParm("PMAN_NEW_FACE_DAYS",DbSession.DbType.NUMBER,int.Parse(txtManNewFaceDays.Text));
			db.ProcedureInParm("PVOICE_NEW_DAYS",DbSession.DbType.NUMBER,int.Parse(txtVoiceNewDays.Text));
			db.ProcedureInParm("PPRODUCT_NEW_DAYS",DbSession.DbType.NUMBER,int.Parse(txtProductNewDays.Text));
			db.ProcedureInParm("PPROFILE_MOVIE_NEW_HOURS",DbSession.DbType.NUMBER,int.Parse(txtProfileMovieNewHours.Text));
			db.ProcedureInParm("PBBS_NEW_HOURS",DbSession.DbType.NUMBER,int.Parse(txtBbsNewHours.Text));
			db.ProcedureInParm("PPIC_BBS_NEW_HOURS",DbSession.DbType.NUMBER,int.Parse(txtPicBbsNewHours.Text));
			db.ProcedureInParm("PMOVIE_BBS_NEW_HOURS",DbSession.DbType.NUMBER,int.Parse(txtMovieBbsNewHours.Text));
			db.ProcedureInParm("PRX_MAIL_NEW_HOURS",DbSession.DbType.NUMBER,int.Parse(txtRxMailNewHours.Text));
			db.ProcedureInParm("PFIND_MAN_ELAPASED_DAYS",DbSession.DbType.NUMBER,int.Parse(txtFindManElapasedDays.Text));
			db.ProcedureInParm("PCM_MAIL_RX_LIMIT",DbSession.DbType.NUMBER,int.Parse(txtCmMailRxLimit.Text));
			db.ProcedureInParm("PSUPPORT_EMAIL_ADDR",DbSession.DbType.VARCHAR2,txtSupportEmailAddr.Text);
			db.ProcedureInParm("PINFO_EMAIL_ADDR",DbSession.DbType.VARCHAR2,txtInfoEMailAddr.Text);
			db.ProcedureInParm("PREPORT_EMAIL_ADDR",DbSession.DbType.VARCHAR2,txtReportEMailAddr.Text);
			db.ProcedureInParm("PCAST_REGIST_REPORT_EMAIL_ADDR",DbSession.DbType.VARCHAR2,txtCastRegistReportEMailAddr.Text);
			db.ProcedureInParm("PCAST_MODIFY_ACC_EMAIL_ADDR",DbSession.DbType.VARCHAR2,txtCastModifyAccountEMailAddr.Text);
			db.ProcedureInParm("PMAN_UPLOAD_EMAIL_ADDR",DbSession.DbType.VARCHAR2,txtManUploadEmailAddr.Text);
			db.ProcedureInParm("PCAST_UPLOAD_EMAIL_ADDR",DbSession.DbType.VARCHAR2,txtCastUploadEmailAddr.Text);
			db.ProcedureInParm("PAGENT_REGIST_EMAIL_ADDR",DbSession.DbType.VARCHAR2,txtAgentRegistEmailAddr.Text);
			db.ProcedureInParm("PSUPPORT_TEL",DbSession.DbType.VARCHAR2,txtSupportTel.Text);
			db.ProcedureInParm("PWEB_FACE_SEQ",DbSession.DbType.VARCHAR2,lstWebFaceSeq.SelectedValue);
			db.ProcedureInParm("PHEADER_HTML_DOC",DbSession.DbType.VARCHAR2,txtHeaderHtmlDoc.Text);
			db.ProcedureInParm("PFOOTER_HTML_DOC",DbSession.DbType.VARCHAR2,txtFooterHtmlDoc.Text);
			db.ProcedureInParm("PMAIL_CAST_ATTR_TYPE_SEQ1",DbSession.DbType.VARCHAR2,lstMailCastAttrTypeSeq1.SelectedValue);
			db.ProcedureInParm("PMAIL_CAST_ATTR_TYPE_SEQ2",DbSession.DbType.VARCHAR2,lstMailCastAttrTypeSeq2.SelectedValue);
			db.ProcedureInParm("PINQ_MAN_TYPE_SEQ1",DbSession.DbType.VARCHAR2,lstInqManTypeSeq1.SelectedValue);
			db.ProcedureInParm("PINQ_MAN_TYPE_SEQ2",DbSession.DbType.VARCHAR2,lstInqManTypeSeq2.SelectedValue);
			db.ProcedureInParm("PINQ_MAN_TYPE_SEQ3",DbSession.DbType.VARCHAR2,lstInqManTypeSeq3.SelectedValue);
			db.ProcedureInParm("PINQ_MAN_TYPE_SEQ4",DbSession.DbType.VARCHAR2,lstInqManTypeSeq4.SelectedValue);
			db.ProcedureInParm("PADMIN_INQ_MAN_TYPE_SEQ1",DbSession.DbType.VARCHAR2,lstInqAdminManTypeSeq1.SelectedValue);
			db.ProcedureInParm("PADMIN_INQ_MAN_TYPE_SEQ2",DbSession.DbType.VARCHAR2,lstInqAdminManTypeSeq2.SelectedValue);
			db.ProcedureInParm("PADMIN_INQ_MAN_TYPE_SEQ3",DbSession.DbType.VARCHAR2,lstInqAdminManTypeSeq3.SelectedValue);
			db.ProcedureInParm("PADMIN_INQ_MAN_TYPE_SEQ4",DbSession.DbType.VARCHAR2,lstInqAdminManTypeSeq4.SelectedValue);
			db.ProcedureInParm("PADMIN_INQ_CAST_TYPE_SEQ1",DbSession.DbType.VARCHAR2,lstInqAdminCastTypeSeq1.SelectedValue);
			db.ProcedureInParm("PADMIN_INQ_CAST_TYPE_SEQ2",DbSession.DbType.VARCHAR2,lstInqAdminCastTypeSeq2.SelectedValue);
			db.ProcedureInParm("PADMIN_INQ_CAST_TYPE_SEQ3",DbSession.DbType.VARCHAR2,lstInqAdminCastTypeSeq3.SelectedValue);
			db.ProcedureInParm("PADMIN_INQ_CAST_TYPE_SEQ4",DbSession.DbType.VARCHAR2,lstInqAdminCastTypeSeq4.SelectedValue);
			db.ProcedureInParm("PINVITE_TALK_CAST_CHARGE_FLAG",DbSession.DbType.NUMBER,chkInviteTalkCastChargeFlag.Checked);
			db.ProcedureInParm("PCAST_MINIMUM_PAYMENT",DbSession.DbType.NUMBER,int.Parse(txtCastMinimumPayment.Text));
			db.ProcedureInParm("PNO_USE_RETURN_MAIL_FLAG",DbSession.DbType.NUMBER,chkNoUseReturnMailFlag.Checked);
			db.ProcedureInParm("PBBS_DUP_CHARGE_FLAG",DbSession.DbType.NUMBER,chkBbsDupChargeFlag.Checked);
			db.ProcedureInParm("PATTACHED_MAIL_DUP_CHARGE_FLAG",DbSession.DbType.NUMBER,chkAttachedMailDupChargeFlag.Checked);
			db.ProcedureInParm("PUSE_ATTACHED_MAIL_FLAG",DbSession.DbType.NUMBER,chkUseAttachedMailFlag.Checked);
			db.ProcedureInParm("PREGIST_HANDLE_NM_INPUT_FLAG",DbSession.DbType.NUMBER,chkRegistHandleNmInputFlag.Checked);
			db.ProcedureInParm("PREGIST_BIRTHDAY_INPUT_FLAG",DbSession.DbType.NUMBER,chkRegistBirthdayInputFlag.Checked);
			db.ProcedureInParm("PCAST_REGIST_NEED_AGREE_FLAG",DbSession.DbType.NUMBER,chkCastRegistNeedAgreeFlag.Checked);
			db.ProcedureInParm("PMAN_INTRO_POINT_GET_TIMING",DbSession.DbType.NUMBER,lstManIntroPointGetTiming.SelectedValue);
			db.ProcedureInParm("PMAN_INTRODUCER_POINT",DbSession.DbType.NUMBER,int.Parse(txtManIntroducerPoint.Text));
			db.ProcedureInParm("PMAN_INTRO_POINT",DbSession.DbType.NUMBER,int.Parse(txtManIntroPoint.Text));
			if (rdoCastIntroPointGetTiming1.Checked) {
				db.ProcedureInParm("PCAST_INTRO_POINT_GET_TIMING",DbSession.DbType.NUMBER,0);
			} else if (rdoCastIntroPointGetTiming2.Checked) {
				db.ProcedureInParm("PCAST_INTRO_POINT_GET_TIMING",DbSession.DbType.NUMBER,2);
			} else {
				db.ProcedureInParm("PCAST_INTRO_POINT_GET_TIMING",DbSession.DbType.NUMBER,1);
			}
			db.ProcedureInParm("PCAST_INTRODUCER_POINT",DbSession.DbType.NUMBER,int.Parse(txtCastIntroducerPoint.Text));
			db.ProcedureInParm("PCAST_INTRO_POINT",DbSession.DbType.NUMBER,int.Parse(txtCastIntroPoint.Text));
			db.ProcedureInParm("PMEASURED_RATE_SERVICE_POINT",DbSession.DbType.NUMBER,int.Parse(txtMeasuredRateServicePoint.Text));
			db.ProcedureInParm("PMEASURED_RATE_LIMIT_POINT",DbSession.DbType.NUMBER,int.Parse(txtMeasuredRateLimitPoint.Text));
			db.ProcedureInParm("PZERO_SETTLE_SERVICE_POINT",DbSession.DbType.NUMBER,int.Parse(txtZeroSettleServicePoint.Text));
			db.ProcedureInParm("PTOP_PAGE_SEEK_MODE",DbSession.DbType.NUMBER,int.Parse(lstTopPageSeekMode.SelectedValue));
			db.ProcedureInParm("POFFLINE_GUIDANCE",DbSession.DbType.VARCHAR2,txtOfflineGuidance.Text);
			db.ProcedureInParm("PLOGINED_GUIDANCE",DbSession.DbType.VARCHAR2,txtLoginedGuidance.Text);
			db.ProcedureInParm("PWAITING_GUIDANCE",DbSession.DbType.VARCHAR2,txtWaittingGuidance.Text);
			db.ProcedureInParm("PCALLING_GUIDANCE",DbSession.DbType.VARCHAR2,txtCallingGuidance.Text);
			db.ProcedureInParm("PTALKING_WSHOT_GUIDANCE",DbSession.DbType.VARCHAR2,txtTalkingWShotGuidance.Text);
			db.ProcedureInParm("PTALKING_PARTY_GUIDANCE",DbSession.DbType.VARCHAR2,txtTalkingPartyGuidance.Text);
			db.ProcedureInParm("PCONNECT_TYPE_VIDEO_GUIDANCE",DbSession.DbType.VARCHAR2,txtConnectTypeVideoGuidance.Text);
			db.ProcedureInParm("PCONNECT_TYPE_BOTH_GUIDANCE",DbSession.DbType.VARCHAR2,txtConnectTypeBothGuidance.Text);
			db.ProcedureInParm("PCONNECT_TYPE_VOICE_GUIDANCE",DbSession.DbType.VARCHAR2,txtConnectTypeVoiceGuidance.Text);
			db.ProcedureInParm("PPROFILE_PAGEING_FLAG",DbSession.DbType.NUMBER,chkProfilePageingFlag.Checked);
			db.ProcedureInParm("PPROFILE_PREVIOUS_GUIDANCE",DbSession.DbType.VARCHAR2,txtProfilePreviousGuidance.Text);
			db.ProcedureInParm("PPROFILE_NEXT_GUIDANCE",DbSession.DbType.VARCHAR2,txtProfileNextGuidance.Text);
			db.ProcedureInParm("PPROFILE_PAGEING_FLAG2",DbSession.DbType.NUMBER,chkProfilePageingFlag2.Checked);
			db.ProcedureInParm("PPROFILE_PREVIOUS_GUIDANCE2",DbSession.DbType.VARCHAR2,txtProfilePreviousGuidance2.Text);
			db.ProcedureInParm("PPROFILE_NEXT_GUIDANCE2",DbSession.DbType.VARCHAR2,txtProfileNextGuidance2.Text);
			db.ProcedureInParm("PPREVIOUS_ACCESS_KEY",DbSession.DbType.VARCHAR2,txtPreviousAccessKey.Text);
			db.ProcedureInParm("PNEXT_ACCESS_KEY",DbSession.DbType.VARCHAR2,txtNextAccessKey.Text);
			db.ProcedureInParm("PRANKING_CREATE_COUNT",DbSession.DbType.NUMBER,int.Parse(txtRankingCreateCount.Text));
			db.ProcedureInParm("PCAST_CAN_SELECT_MONITOR_FLAG",DbSession.DbType.NUMBER,chkCastCanSelectMonitorFlag.Checked);
			db.ProcedureInParm("PCAST_CAN_SELECT_CONNECT_TYPE",DbSession.DbType.NUMBER,chkCastCanSelectConnectType.Checked);
			db.ProcedureInParm("PMAIL_EMBED_DOMAIN",DbSession.DbType.VARCHAR2,txtMailEmbedDomain.Text);
			db.ProcedureInParm("PONLINE_ADJUST_MIN_FROM",DbSession.DbType.NUMBER,int.Parse(txtOnlineAdjustMinFrom.Text));
			db.ProcedureInParm("PONLINE_ADJUST_MIN_TO",DbSession.DbType.NUMBER,int.Parse(txtOnlineAdjustMinTo.Text));
			db.ProcedureInParm("PMAN_ONLINE_MIN",DbSession.DbType.NUMBER,int.Parse(txtManOnlineMin.Text));
			db.ProcedureInParm("PCASH_MAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,lstMailTemplateCash1.SelectedValue);
			db.ProcedureInParm("PCASH_SHORT_MAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,lstMailTemplateCash2.SelectedValue);
			db.ProcedureInParm("PINTRO_POINT_MAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,lstMailTemplateIntro.SelectedValue);
			db.ProcedureInParm("PREGIST_DM_MAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,lstMailTemplateRegistDM.SelectedValue);
			db.ProcedureInParm("PREGIST_DM_MAIL_TEMPLATE_NO2",DbSession.DbType.VARCHAR2,lstMailTemplateRegistDM2.SelectedValue);
			db.ProcedureInParm("PMAQIA_DM_MAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,lstMailTemplateRegistMaqiaDM.SelectedValue);
			db.ProcedureInParm("PCAST_REG_MAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,lstMailTemplateCastReg.SelectedValue);
			db.ProcedureInParm("PCAST_PREREG_MAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,lstMailTemplateCastPreReg.SelectedValue);
			db.ProcedureInParm("PCAST_MAN_MAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,lstMailTemplateCastManReg.SelectedValue);
			db.ProcedureInParm("PREGIST_DM_MAIL_TIMING",DbSession.DbType.NUMBER,int.Parse(lstRegistDMMailTiming.SelectedValue));
			db.ProcedureInParm("PMAQIA_DM_MAIL_TIMING",DbSession.DbType.NUMBER,int.Parse(lstMaqiaDMMailTiming.SelectedValue));
			db.ProcedureInParm("PCAST_ONLINE_MAIL_RX_FLAG",DbSession.DbType.NUMBER,chkCastOnlineMailRxFlag.Checked);
			db.ProcedureInParm("PTALK_START_NOT_CHARGE_SEC",DbSession.DbType.NUMBER,txtTalkStartNotChargeSec.Text);
			db.ProcedureInParm("PTALK2_START_NOT_CHARGE_SEC",DbSession.DbType.NUMBER,txtTalk2StartNotChargeSec.Text);
			db.ProcedureInParm("NON_AD_REGIST_REQ_AGECERT_FLAG",DbSession.DbType.NUMBER,chkNonAdRegistReqAgeCertFlag.Checked);
			db.ProcedureInParm("PMOBILE_MAN_DETAIL_LIST_COUNT",DbSession.DbType.NUMBER,int.Parse(txtMobileManDetailListCount.Text));
			db.ProcedureInParm("PMOBILE_MAN_LIST_COUNT",DbSession.DbType.NUMBER,int.Parse(txtMobileManListCount.Text));
			db.ProcedureInParm("PMOBILE_WOMAN_DETAIL_LIST_COUNT",DbSession.DbType.NUMBER,int.Parse(txtMobileWomanDetailListCount.Text));
			db.ProcedureInParm("PMOBILE_WOMAN_LIST_COUNT",DbSession.DbType.NUMBER,int.Parse(txtMobileWomanListCount.Text));
			db.ProcedureInParm("PPICKUP_AUTO_SET_COUNT",DbSession.DbType.NUMBER,int.Parse(txtPickupAutoSetCount.Text));
			db.ProcedureInParm("PPICKUP_AUTO_INTERVAL_MIN",DbSession.DbType.NUMBER,int.Parse(txtPickupAutoIntervalMin.Text));
			db.ProcedureInParm("PBBS_PIC_ATTR_FLAG",DbSession.DbType.NUMBER,chkBbsPicAttrFlag.Checked);
			db.ProcedureInParm("PBBS_MOVIE_ATTR_FLAG",DbSession.DbType.NUMBER,chkBbsMovieAttrFlag.Checked);
			db.ProcedureInParm("PSUPPORT_VOICE_MONITOR_FLAG",DbSession.DbType.NUMBER,chkSupportVoiceMonitorFlag.Checked);
			db.ProcedureInParm("PINTRO_POINT_MAN_TO_CAST",DbSession.DbType.NUMBER,int.Parse(txtIntroPointManToCast.Text));
			db.ProcedureInParm("PINTRO_POINT_CAST_TO_MAN",DbSession.DbType.NUMBER,int.Parse(txtIntroPointCastToMan.Text));
			db.ProcedureInParm("PINTRO_MAN_TO_CAST_MIN_POINT",DbSession.DbType.NUMBER,int.Parse(txtIntroManToCastMinPoint.Text));
			db.ProcedureInParm("PINTRODUCER_RATE_ON_FLAG",DbSession.DbType.NUMBER,chkIntroducerRateOnFlag.Checked);
			db.ProcedureInParm("PCAST_RICHINO_SEEK_PAYMENT_CNT",DbSession.DbType.NUMBER,int.Parse(txtCastRichinoSeekPaymentCnt.Text));
			db.ProcedureInParm("PCAST_RICHINO_SEEK_PAYMENT_AMT",DbSession.DbType.NUMBER,int.Parse(txtCastRichinoSeekPaymentAmt.Text));
			db.ProcedureInParm("PSUPPORT_CHG_OBJ_CAT_FLAG",DbSession.DbType.NUMBER,chkSupportChgObjCatFlag.Checked);
			db.ProcedureInParm("PMAN_NEW_REGIST_OK_AMT",DbSession.DbType.NUMBER,decimal.Parse(txtManNewRegistOkAmt.Text));
			db.ProcedureInParm("PCAST_NEW_REGIST_OK_AMT",DbSession.DbType.NUMBER,decimal.Parse(txtCastNewRegistOkAmt.Text));
			db.ProcedureInParm("PCAST_WRITE_BBS_LIMIT",DbSession.DbType.NUMBER,txtCastWriteBbsLimit.Text.Equals(string.Empty) ? "-1" : txtCastWriteBbsLimit.Text);
			db.ProcedureInParm("PCAST_WRITE_BBS_INTERVAL_MIN",DbSession.DbType.NUMBER,txtCastWriteBbsIntervalMin.Text);
			db.ProcedureInParm("PMAN_WRITE_BBS_LIMIT",DbSession.DbType.NUMBER,txtManWriteBbsLimit.Text.Equals(string.Empty) ? "-1" : txtManWriteBbsLimit.Text);
			db.ProcedureInParm("PMAN_WRITE_BBS_INTERVAL_MIN",DbSession.DbType.NUMBER,txtManWriteBbsIntervalMin.Text);
			db.ProcedureInParm("PPAY_MAN_WRITE_BBS_LIMIT",DbSession.DbType.NUMBER,txtPayManWriteBbsLimit.Text.Equals(string.Empty) ? "-1" : txtPayManWriteBbsLimit.Text);
			db.ProcedureInParm("PPAYMAN_WRITE_BBS_INTERVAL_MIN",DbSession.DbType.NUMBER,txtPayManWriteBbsIntervalMin.Text);
			db.ProcedureInParm("PLOGIN_MAIL_NOT_SEND_FLAG",DbSession.DbType.NUMBER,chkLoginMailNotSendFlag.Checked);
			db.ProcedureInParm("PLOGIN_MAIL2_NOT_SEND_FLAG",DbSession.DbType.NUMBER,chkLoginMail2NotSendFlag.Checked);
			db.ProcedureInParm("PLOGOUT_MAIL_NOT_SEND_FLAG",DbSession.DbType.NUMBER,chkLogoutMailNotSendFlag.Checked);
			db.ProcedureInParm("PMAN_TALK_END_SEND_MAIL_FLAG",DbSession.DbType.NUMBER,chkManTalkEndSendFlag.Checked);
			db.ProcedureInParm("PMAN_TALK_CAST_DISC_MAIL_NO",DbSession.DbType.VARCHAR2,lstManTalkCastDiscMailNo.SelectedValue);
			db.ProcedureInParm("PMAN_TALK_MAN_DISC_MAIL_NO",DbSession.DbType.VARCHAR2,lstManTalkManDiscMailNo.SelectedValue);
			db.ProcedureInParm("PMAN_TALK_POINT_DISC_MAIL_NO",DbSession.DbType.VARCHAR2,lstManTalkPointDiscMailNo.SelectedValue);
			db.ProcedureInParm("PMAN_TALK_SYS_DISC_MAIL_NO",DbSession.DbType.VARCHAR2,lstManTalkSysDiscMailNo.SelectedValue);
			db.ProcedureInParm("PCAST_TALK_END_SEND_MAIL_FLAG",DbSession.DbType.NUMBER,chkCastTalkEndSendFlag.Checked);
			db.ProcedureInParm("PCAST_TALK_CAST_DISC_MAIL_NO",DbSession.DbType.VARCHAR2,lstCastTalkCastDiscMailNo.SelectedValue);
			db.ProcedureInParm("PCAST_TALK_MAN_DISC_MAIL_NO",DbSession.DbType.VARCHAR2,lstCastTalkManDiscMailNo.SelectedValue);
			db.ProcedureInParm("PCAST_TALK_POINT_DISC_MAIL_NO",DbSession.DbType.VARCHAR2,lstCastTalkPointDiscMailNo.SelectedValue);
			db.ProcedureInParm("PCAST_TALK_SYS_DISC_MAIL_NO",DbSession.DbType.VARCHAR2,lstCastTalkSysDiscMailNo.SelectedValue);
			db.ProcedureInParm("PCAST_AUTO_REGIST_CHAR_FLAG",DbSession.DbType.NUMBER,chkCastAutoRegistCharFlag.Checked);
			db.ProcedureInParm("PMAN_DEFAULT_HANDLE_NM",DbSession.DbType.VARCHAR2,txtManDefaultHandleNm.Text);
			db.ProcedureInParm("PIMP_CAST_FIRST_LOGIN_ACT",DbSession.DbType.VARCHAR2,lstImpCastFirstLoginAct.SelectedValue);
			db.ProcedureInParm("PIMP_MAN_FIRST_LOGIN_ACT",DbSession.DbType.VARCHAR2,lstImpManFirstLoginAct.SelectedValue);
			db.ProcedureInParm("PDUMMY_TALK_TX_LOGIN_MAIL_FLAG",DbSession.DbType.NUMBER,chkDummyTalkTxLoginMailFlag.Checked);
			db.ProcedureInParm("PCAST_INTRO_POINT_MAIL_NO",DbSession.DbType.VARCHAR2,lstCastIntroPointMailNo.SelectedValue);
			db.ProcedureInParm("PMAN_AUTO_LOGIN_RESIGNED_FLAG",DbSession.DbType.VARCHAR2,chkManAutoLoginResignedFlag.Checked);
			db.ProcedureInParm("PMAN_SECESSION_DEL_DATA_FLAG",DbSession.DbType.VARCHAR2,chkManSecessionDelDataFlag.Checked);
			db.ProcedureInParm("PCAST_AUTO_LOGIN_RESIGNED_FLAG",DbSession.DbType.VARCHAR2,chkCastAutoLoginResignedFlag.Checked);
			db.ProcedureInParm("PCAST_SECESSION_DEL_DATA_FLAG",DbSession.DbType.VARCHAR2,chkCastSecessionDelDataFlag.Checked);
			db.ProcedureInParm("pSITE_HTML_DOC_SEX_CHECK_FLAG",DbSession.DbType.NUMBER,chkSiteHtmlDocSexCheckFlag.Checked);
			db.ProcedureInParm("pJEALOUSY_EFFECT_DELAY_MIN",DbSession.DbType.NUMBER,txtJealousyEffectDelayMin.Text);
			db.ProcedureInParm("pOUTCOME_COUNT_DAYS",DbSession.DbType.NUMBER,txtOutcomeCountDays.Text);
			db.ProcedureInParm("PF_TALK_NOTIFY_MASK",DbSession.DbType.NUMBER,iNotifyTalkMask);
			db.ProcedureInParm("PROWID_SITE",DbSession.DbType.VARCHAR2,ViewState["ROWID_SITE"].ToString());
			db.ProcedureInParm("PREVISION_NO_SITE",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO_SITE"].ToString()));
			db.ProcedureInParm("PROWID_MANAGEMENT",DbSession.DbType.VARCHAR2,ViewState["ROWID_MANAGEMENT"].ToString());
			db.ProcedureInParm("PREVISION_NO_MANAGEMENT",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO_MANAGEMENT"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,0);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		InitPage();
	}

	private void SetupDir() {
		string sWebPhisicalDir = "";

		using (Site oSite = new Site()) {
			oSite.GetValue(lblSiteCd.Text,"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
		}

		string sDirectry = sWebPhisicalDir + ViCommConst.IMAGE_DIRECTRY + "\\" + lblSiteCd.Text + "\\";
		string sWebPath = ConfigurationManager.AppSettings["Root"] + ViCommConst.IMAGE_WEB_PATH + "/" + lblSiteCd.Text + "/";

		webNmCastNoPic = sWebPath + ViCommConst.IMG_FILE_NM_CAST_NOPIC;
		webNmCastNoPicSmall = sWebPath + ViCommConst.IMG_FILE_NM_CAST_NOPICSMALL;
		webNmManNoPic = sWebPath + ViCommConst.IMG_FILE_NM_MAN_NOPIC;

		fileNmCastNoPic = sDirectry + ViCommConst.IMG_FILE_NM_CAST_NOPIC;
		fileNmCastNoPicSmall = sDirectry + ViCommConst.IMG_FILE_NM_CAST_NOPICSMALL;
		fileNmManNoPic = sDirectry + ViCommConst.IMG_FILE_NM_MAN_NOPIC;
	}

	private void SetupImage() {

		SetupDir();

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
			if (System.IO.File.Exists(fileNmCastNoPic)) {
				imgCastNoPic.ImageUrl = webNmCastNoPic;
				imgCastNoPic.Visible = true;
			} else {
				imgCastNoPic.Visible = false;
			}

			if (System.IO.File.Exists(fileNmCastNoPicSmall)) {
				imgCastNoPicSmall.ImageUrl = webNmCastNoPicSmall;
				imgCastNoPicSmall.Visible = true;
			} else {
				imgCastNoPicSmall.Visible = false;
			}

			if (System.IO.File.Exists(fileNmManNoPic)) {
				imgManNoPic.ImageUrl = webNmManNoPic;
				imgManNoPic.Visible = true;
			} else {
				imgManNoPic.Visible = false;
			}
		}
		btnCastNoPic.Attributes["onclick"] = "return confirm('アップロードを行いますか？');";
		btnCastNoPicSmall.Attributes["onclick"] = "return confirm('アップロードを行いますか？');";
		btnManNoPic.Attributes["onclick"] = "return confirm('アップロードを行いますか？');";
	}

	protected void vdcInviteMailAvaHour_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid) {
			if (int.Parse(txtInviteMailAvaHour.Text) > int.Parse(txtReInviteMailNAHour.Text)) {
				args.IsValid = false;
			}
		}
	}
	protected void dsMailTemplate_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lblSiteCd.Text;
	}
	private void PlcVisible() {
		string sOldAreaVisibleFlag = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["oldAreaVisible"]);
		if (sOldAreaVisibleFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			plcOldMainteArea.Visible = false;
			tdEndHeader1.Visible = false;
			tdEndHeader2.Visible = true;
		} else {
			plcOldMainteArea.Visible = true;
			tdEndHeader1.Visible = true;
			tdEndHeader2.Visible = false;
		}
		plcVisible1.Visible = false;
		plcVisible2.Visible = false;
		plcVisible3.Visible = false;
		plcVisible4.Visible = false;
		plcVisible5.Visible = false;
		plcVisible6.Visible = false;
		plcVisible7.Visible = false;
		plcVisible8.Visible = false;
		plcVisible9.Visible = false;
	}
}
