﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ListUserDefPoint.aspx.cs" Inherits="Site_ListUserDefPoint"
	Title="ユーザー定義ポイント設定" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="ユーザー定義ポイント設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 400px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									ｻｲﾄｺｰﾄﾞ
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstKeySiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									ﾎﾟｲﾝﾄ追加識別
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtKeyAddPointId" runat="server" MaxLength="32" Width="150px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrKeyAddPointId" runat="server" ErrorMessage="ﾎﾟｲﾝﾄ追加識別を入力して下さい。" ControlToValidate="txtKeyAddPointId" ValidationGroup="Key">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeKeyAddPointId" runat="server" ErrorMessage="ﾎﾟｲﾝﾄ追加識別は英数字で入力して下さい。" ValidationExpression="[-_a-zA-Z0-9]*" ControlToValidate="txtKeyAddPointId"
										ValidationGroup="Key">*</asp:RegularExpressionValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnKeySeek" Text="検索" CssClass="seekbutton" OnClick="btnKeySeek_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnKeyCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[ﾕｰｻﾞｰ定義ﾎﾟｲﾝﾄ内容]</legend>
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									増減ﾎﾟｲﾝﾄ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtAddPoint" runat="server" MaxLength="5" Width="50px"></asp:TextBox>pt
									<asp:RegularExpressionValidator ID="vdeAddPoint" runat="server" ControlToValidate="txtAddPoint"
                                        ErrorMessage="増減ﾎﾟｲﾝﾄは数字と+-で入力して下さい。" ValidationExpression="[-\+0-9]*" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									<asp:CustomValidator ID="vdcAddPointMinMax" runat="server" ErrorMessage="増減ﾎﾟｲﾝﾄは-9999～9999の間で入力してください" ControlToValidate="txtAddPoint" ValidationGroup="Detail"
										OnServerValidate="vdcAddPointMinMax_ServerValidate">*</asp:CustomValidator>
								</td>
							</tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    ﾎﾟｲﾝﾄ名称
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtDefPointNm" runat="server" MaxLength="200" Width="200px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vdeDefPointNm" runat="server" ErrorMessage="ﾎﾟｲﾝﾄ名称を入力して下さい。"
                                        ControlToValidate="txtDefPointNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5"
                                        TargetControlID="vdeDefPointNm" HighlightCssClass="validatorCallout" />
                                </td>
                            </tr>
							<tr>
								<td class="tdHeaderStyle">
									追加ﾀｲﾐﾝｸﾞ
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstApplyCountType" runat="server" DataSourceID="dsApplyCountType" DataTextField="CODE_NM" DataValueField="CODE" Width="170px">
									</asp:DropDownList>
								</td>
							</tr>
                            <tr>
								<td class="tdHeaderStyle">
									適用時刻
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstApplyStartHH" runat="server">
										<asp:ListItem Text="" Value="null"></asp:ListItem>
										<asp:ListItem>00</asp:ListItem>
										<asp:ListItem>01</asp:ListItem>
										<asp:ListItem>02</asp:ListItem>
										<asp:ListItem>03</asp:ListItem>
										<asp:ListItem>04</asp:ListItem>
										<asp:ListItem>05</asp:ListItem>
										<asp:ListItem>06</asp:ListItem>
										<asp:ListItem>07</asp:ListItem>
										<asp:ListItem>08</asp:ListItem>
										<asp:ListItem>09</asp:ListItem>
										<asp:ListItem>10</asp:ListItem>
										<asp:ListItem>11</asp:ListItem>
										<asp:ListItem>12</asp:ListItem>
										<asp:ListItem>13</asp:ListItem>
										<asp:ListItem>14</asp:ListItem>
										<asp:ListItem>15</asp:ListItem>
										<asp:ListItem>16</asp:ListItem>
										<asp:ListItem>17</asp:ListItem>
										<asp:ListItem>18</asp:ListItem>
										<asp:ListItem>19</asp:ListItem>
										<asp:ListItem>20</asp:ListItem>
										<asp:ListItem>21</asp:ListItem>
										<asp:ListItem>22</asp:ListItem>
										<asp:ListItem>23</asp:ListItem>
									</asp:DropDownList>
									<asp:DropDownList ID="lstApplyStartMM" runat="server">
										<asp:ListItem Text="" Value="null"></asp:ListItem>
										<asp:ListItem>00</asp:ListItem>
										<asp:ListItem>15</asp:ListItem>
										<asp:ListItem>30</asp:ListItem>
										<asp:ListItem>45</asp:ListItem>
									</asp:DropDownList>
									～
									<asp:DropDownList ID="lstApplyEndHH" runat="server">
										<asp:ListItem Text="" Value="null"></asp:ListItem>
										<asp:ListItem>00</asp:ListItem>
										<asp:ListItem>01</asp:ListItem>
										<asp:ListItem>02</asp:ListItem>
										<asp:ListItem>03</asp:ListItem>
										<asp:ListItem>04</asp:ListItem>
										<asp:ListItem>05</asp:ListItem>
										<asp:ListItem>06</asp:ListItem>
										<asp:ListItem>07</asp:ListItem>
										<asp:ListItem>08</asp:ListItem>
										<asp:ListItem>09</asp:ListItem>
										<asp:ListItem>10</asp:ListItem>
										<asp:ListItem>11</asp:ListItem>
										<asp:ListItem>12</asp:ListItem>
										<asp:ListItem>13</asp:ListItem>
										<asp:ListItem>14</asp:ListItem>
										<asp:ListItem>15</asp:ListItem>
										<asp:ListItem>16</asp:ListItem>
										<asp:ListItem>17</asp:ListItem>
										<asp:ListItem>18</asp:ListItem>
										<asp:ListItem>19</asp:ListItem>
										<asp:ListItem>20</asp:ListItem>
										<asp:ListItem>21</asp:ListItem>
										<asp:ListItem>22</asp:ListItem>
										<asp:ListItem>23</asp:ListItem>
									</asp:DropDownList>
									<asp:DropDownList ID="lstApplyEndMM" runat="server">
										<asp:ListItem Text="" Value="null"></asp:ListItem>
										<asp:ListItem>00</asp:ListItem>
										<asp:ListItem>15</asp:ListItem>
										<asp:ListItem>30</asp:ListItem>
										<asp:ListItem>45</asp:ListItem>
									</asp:DropDownList>
									<asp:CustomValidator ID="vdcApplyTime" runat="server" ErrorMessage="" ControlToValidate="" OnServerValidate="vdcApplyTime_ServerValidate" ValidationGroup="Detail">*</asp:CustomValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									備考
								</td>
								<td class="tdDataStyle" colspan="5">
									<asp:TextBox ID="txtRemarks" runat="server" MaxLength="300" Width="400px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									利用不可
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkNaFlag" runat="server">
									</asp:CheckBox>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[ユーザー定義ﾎﾟｲﾝﾄ一覧]</legend>
			<table border="0" style="width: 320px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle">
						ｻｲﾄｺｰﾄﾞ
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px">
						</asp:DropDownList>
					</td>
				</tr>
				<tr>
					<td class="tdHeaderStyle2">
						ﾎﾟｲﾝﾄ追加識別
					</td>
					<td class="tdDataStyle">
						<asp:TextBox ID="txtAddPointId" runat="server" MaxLength="32" Width="150px"></asp:TextBox>
						<asp:RegularExpressionValidator ID="vdeAddPointId" runat="server" ErrorMessage="ﾎﾟｲﾝﾄ追加識別は英数字で入力して下さい。" ValidationExpression="[-_a-zA-Z0-9]*" ControlToValidate="txtAddPointId"
							ValidationGroup="Seek">*</asp:RegularExpressionValidator>
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" CausesValidation="True" ValidationGroup="Seek" />
			<asp:Button runat="server" ID="btnRegist" Text="追加" CssClass="seekbutton" CausesValidation="False" OnClick="btnRegist_Click" />
			<br />
			<br />
			<asp:GridView ID="grdUserDefPoint" runat="server" DataSourceID="dsUserDefPoint" OnRowDataBound="grdUserDefPoint_RowDataBound" AllowPaging="True" AutoGenerateColumns="False"
				AllowSorting="True" SkinID="GridViewColor" PageSize="30">
				<Columns>
					<asp:TemplateField HeaderText="ﾎﾟｲﾝﾄ追加識別">
						<ItemTemplate>
							<asp:LinkButton ID="lnkAddPointId" runat="server" Text='<%# string.Format("{0}",Eval("ADD_POINT_ID"))%>' CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("ADD_POINT_ID"))  %>'
								OnCommand="lnkAddPointId_Command" CausesValidation="False"></asp:LinkButton>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Left" />
						<HeaderStyle HorizontalAlign="Center" />
					</asp:TemplateField>
                    <asp:BoundField DataField="DEF_POINT_NM" HeaderText="ﾎﾟｲﾝﾄ名称">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ADD_POINT" HeaderText="増減ﾎﾟｲﾝﾄ" DataFormatString="{0:N0}">
						<HeaderStyle HorizontalAlign="Center" />
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:BoundField DataField="APPLY_COUNT_TYPE_NM" HeaderText="追加ﾀｲﾐﾝｸﾞ">
						<HeaderStyle HorizontalAlign="Center" />
					</asp:BoundField>
					<asp:TemplateField HeaderText="適用時刻">
						<ItemTemplate>
							<asp:Label runat="server" Text='<%#string.Format("{0}～{1}",Eval("APPLY_START_TIME"),Eval("APPLY_END_TIME")) %>' Width="100px"></asp:Label>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Center" />
						<HeaderStyle HorizontalAlign="Center" />
					</asp:TemplateField>
					<asp:BoundField DataField="REMARKS" HeaderText="備考">
						<HeaderStyle HorizontalAlign="Center" />
					</asp:BoundField>
					<asp:TemplateField HeaderText="利用不可">
						<ItemTemplate>
							<asp:Label runat="server" Text='<%# string.Format("{0}",CheckNaFlag(Eval("NA_FLAG"))) %>'></asp:Label>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Center" />
						<HeaderStyle HorizontalAlign="Center" />
					</asp:TemplateField>
				</Columns>
			</asp:GridView>
			
			<a class="reccount">Record Count
				<%# string.Format("{0}",GetRecCount()) %>
			</a>
			<br>
			<a class="reccount">Current viewing page
				<%=grdUserDefPoint.PageIndex + 1%>
				of
				<%=grdUserDefPoint.PageCount%>
			</a>
			<div class="button">
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUserDefPoint" runat="server" SelectMethod="GetPageCollection" TypeName="UserDefPoint" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsUserDefPoint_Selecting" OnSelected="dsUserDefPoint_Selected">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pAddPointId" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<% /* */%>
	<asp:ObjectDataSource ID="dsApplyCountType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="24" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdeAddPoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdcApplyTime" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrKeyAddPointId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdeKeyAddPointId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdeAddPointId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vdcAddPointMinMax" HighlightCssClass="validatorCallout" />
</asp:Content>
