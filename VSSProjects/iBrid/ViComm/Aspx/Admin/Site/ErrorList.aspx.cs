﻿/*************************************************************************
--	System			: ViComm Admin
--	Sub System Name	: Admin
--	Title			: エラーメッセージ管理
--	Progaram ID		: Site_ErrorList
--
--  Creation Date	: 2010.05.27
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;

public partial class Site_ErrorList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			FirstLoad();
			InitPage();

		}
	}

	private void InitPage() {
		lblErrorCd.Text = "";
		ClearField();
		pnlMainte.Visible = false;
		GetList();
	}

	private void FirstLoad() {
		grdError.PageSize = 50;
		grdError.PageIndex = 0;
		lstSeekSiteCd.DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void ClearField() {
		txtErrorDtl.Text = "";
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData();
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void lnkErrorCd_Command(object sender,CommandEventArgs e) {
		lblErrorCd.Text = e.CommandArgument.ToString();
		GetData();
	}

	protected void vdcErrorDtl_ServerValidate(object source,ServerValidateEventArgs args) {
		if (System.Text.Encoding.GetEncoding(65001).GetByteCount(txtErrorDtl.Text) <= txtErrorDtl.MaxLength) {
			args.IsValid = true;
		} else {
			args.IsValid = false;
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ERROR_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSeekSiteCd.SelectedValue);
			db.ProcedureInParm("PERROR_CD",DbSession.DbType.VARCHAR2,lblErrorCd.Text);
			db.ProcedureOutParm("PERROR_DTL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PUPDATE_DATE",DbSession.DbType.DATE);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				lblErrorCd.Text = db.GetStringValue("PERROR_CD");
				txtErrorDtl.Text = db.GetStringValue("PERROR_DTL");
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
	}

	private void UpdateData() {
		string[] sDoc;
		int iDocCount;

		SysPrograms.SeparateHtml(HttpUtility.HtmlDecode(txtErrorDtl.Text),1,out sDoc,out iDocCount);

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ERROR_MAINTE");
			db.ProcedureInParm("PERROR_DTL",DbSession.DbType.VARCHAR2,sDoc[0]);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		InitPage();
	}

	private void GetList() {
		grdError.PageIndex = 0;
		grdError.DataBind();
		pnlCount.DataBind();
	}



	protected void dsError_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
	}

	protected void dsError_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}
}