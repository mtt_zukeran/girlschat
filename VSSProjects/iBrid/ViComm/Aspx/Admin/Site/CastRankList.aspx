<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastRankList.aspx.cs" Inherits="Site_CastRankList" Title="キャストランク設定"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="キャストランク設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									キャストランク
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUserRank" runat="server" MaxLength="1" Width="20px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrUserRank" runat="server" ErrorMessage="キャストランクを入力して下さい。" ControlToValidate="txtUserRank" ValidationGroup="Key">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeUserRank" runat="server" ErrorMessage="キャストランクは1桁の英字大文字A〜Eで入力して下さい。" ValidationExpression="[A-E]" ControlToValidate="txtUserRank"
										ValidationGroup="Key">*</asp:RegularExpressionValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[キャストランク内容]</legend>
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									キャストランク名称
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUserRankNm" runat="server" MaxLength="30" Width="200px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrUserRankNm" runat="server" ErrorMessage="キャストランク名称を入力して下さい。" ControlToValidate="txtUserRankNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									単価／１ﾎﾟｲﾝﾄあたり
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtPointPrice" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrPointPrice" runat="server" ErrorMessage="単価／１ﾎﾟｲﾝﾄあたりを入力して下さい。" ControlToValidate="txtPointPrice" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RangeValidator ID="vdaPointPrice" runat="server" ErrorMessage="単価／１ﾎﾟｲﾝﾄあたりを1〜999で入力して下さい。" ControlToValidate="txtPointPrice" MaximumValue="999"
										MinimumValue="1" Type="Integer" ValidationGroup="Detail" Display="Dynamic">*</asp:RangeValidator>
								</td>
							</tr>
							<asp:PlaceHolder runat="server" ID="plcIntroPointRate">
								<tr>
									<td class="tdHeaderStyle">
										友達紹介ｷｯｸﾊﾞｯｸ率
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtIntroPointRate" runat="server" MaxLength="3" Width="30px"></asp:TextBox>％&nbsp;
										<asp:RequiredFieldValidator ID="vdrIntroPointRate" runat="server" ErrorMessage="友達紹介ｷｯｸﾊﾞｯｸ率を入力して下さい。" ControlToValidate="txtIntroPointRate" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
								</tr>
							</asp:PlaceHolder>
							<tr>
								<td class="tdHeaderStyle2">
									BINGOﾎﾞｰﾙ出現率
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtBingoBallRate" runat="server" MaxLength="4" Width="40px"></asp:TextBox>％&nbsp;小数点第1位まで入力可
									<asp:CustomValidator ID="vdcBingoBallRate" runat="server" ErrorMessage="" ValidationGroup="Detail" OnServerValidate="vdcBingoBallRate_ServerValidate"></asp:CustomValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[キャストランク一覧]</legend>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" Visible="false" />
			<asp:Button runat="server" ID="btnRegist" Text="ランク追加" CssClass="seekbutton" CausesValidation="False" OnClick="btnRegist_Click" />
			<br />
			<br />
			<asp:GridView ID="grdCastRank" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCastRank" AllowSorting="True" SkinID="GridViewColor">
				<Columns>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkUserRank" runat="server" Text='<%# string.Format("{0} {1}",Eval("USER_RANK"),Eval("USER_RANK_NM")) %>' CommandArgument='<%# string.Format("{0}",Eval("USER_RANK"))  %>'
								OnCommand="lnkUserRank_Command" CausesValidation="False">
							</asp:LinkButton>
						</ItemTemplate>
						<HeaderTemplate>
							キャストランク
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:BoundField DataField="UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:Label ID="lblIntroPointRate" runat="server" Text='<%# string.Format("{0}%",Eval("INTRO_POINT_RATE"))%>'></asp:Label>
						</ItemTemplate>
						<HeaderTemplate>
							友達紹介<br>
							ｷｯｸﾊﾞｯｸ率
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Right" />
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:Label ID="lblBingoBallRate" runat="server" Text='<%# string.Format("{0}%",Eval("BINGO_BALL_RATE"))%>'></asp:Label>
						</ItemTemplate>
						<HeaderTemplate>
							BINGOﾎﾞｰﾙ<br>
							出現率
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Right" />
					</asp:TemplateField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br>
				<a class="reccount">Current viewing page
					<%=grdCastRank.PageIndex + 1%>
					of
					<%=grdCastRank.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsCastRank" runat="server" SelectMethod="GetPageCollection" TypeName="CastRank" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsCastRank_Selected" OnSelecting="dsCastRank_Selecting">
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtIntroPointRate" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrUserRank" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeUserRank" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrUserRankNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdrIntroPointRate" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdrPointPrice" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdaPointPrice" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
