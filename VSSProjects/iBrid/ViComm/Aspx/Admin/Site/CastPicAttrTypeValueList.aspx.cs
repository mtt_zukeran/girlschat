﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者写真属性値メンテナンス
--	Progaram ID		: CastPicAttrTypeValueList
--
--  Creation Date	: 2010.05.17
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Site_CastPicAttrTypeValueList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {

			string sSiteCd = iBridCommLib.iBridUtil.GetStringValue(Request.QueryString["site"]);
			string sCastPicAttrTypeSeq = iBridCommLib.iBridUtil.GetStringValue(Request.QueryString["castpicattrtypeseq"]);
			string sCastPicAttrSeq = iBridCommLib.iBridUtil.GetStringValue(Request.QueryString["castpicattrseq"]);
			InitPage();

			if ((!sSiteCd.Equals("")) && (!sCastPicAttrTypeSeq.Equals("")) && (!sCastPicAttrSeq.Equals(""))) {
				lstSiteCd.SelectedValue = sSiteCd;
				lstSeekSiteCd.SelectedValue = sSiteCd;

				lstCastPicAttrTypeSeq.DataBind();
				lstCastPicAttrTypeSeq.SelectedValue = sCastPicAttrTypeSeq;
				lblCastPicAttrSeq.Text = sCastPicAttrSeq;

				GetList();
				GetData();
			}
		}
	}

	protected void dsCastPicAttrTypeValue_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		grdValue.PageSize = 999;
		lstSiteCd.SelectedIndex = 0;
		lblCastPicAttrSeq.Text = "";
		ClearField();
		pnlMainte.Visible = false;
		lstSeekSiteCd.DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		DataBind();

		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);

	}

	private void ClearField() {
		txtCastPicAttrNm.Text = "";
		txtPriority.Text = "";
		recCount = "0";
		chkPosterDelNaFlag.Checked = false;
		chkUseIndividualPayFlag.Checked = false;
		txtPostPayAmt.Text = string.Empty;
		txtViewPayAmt.Text = string.Empty;
		chkUseIndividualChargeFlag.Checked = false;
		txtViewerChargePoint.Text = string.Empty;
		txtUploadLimitCount.Text = string.Empty;
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		lstSiteCd.SelectedIndex = lstSeekSiteCd.SelectedIndex;
		lstCastPicAttrTypeSeq.DataBind();
		lblCastPicAttrSeq.Text = "0";

		GetData();
		GetList();
		pnlKey.Enabled = true;
		lstSiteCd.Enabled = false;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		} else {
			GetList();
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		} else {
			GetList();
		}
	}

	protected void lstCastPicAttrTypeSeq_SelectedIndexChanged(object sender,EventArgs e) {
		GetList();
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void grdValue_DataBound(object sender,EventArgs e) {
		JoinCells(grdValue,0);
	}

	private void GetList() {
		grdValue.PageIndex = 0;
		grdValue.DataBind();
		pnlCount.DataBind();
	}


	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_PIC_ATTR_TYPE_VALUE_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PCAST_PIC_ATTR_TYPE_SEQ",DbSession.DbType.NUMBER,lstCastPicAttrTypeSeq.SelectedValue);
			db.ProcedureInParm("PCAST_PIC_ATTR_SEQ",DbSession.DbType.NUMBER,lblCastPicAttrSeq.Text);
			db.ProcedureOutParm("PCAST_PIC_ATTR_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPRIORITY",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pPOSTER_DEL_NA_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pUSE_INDIVIDUAL_PAY_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pPOST_PAY_AMT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pVIEW_PAY_AMT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pUSE_INDIVIDUAL_CHARGE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pVIEWER_CHARGE_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pUPLOAD_LIMIT_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				lstSiteCd.SelectedValue = db.GetStringValue("PSITE_CD");
				lstCastPicAttrTypeSeq.SelectedValue = db.GetStringValue("PCAST_PIC_ATTR_TYPE_SEQ");
				txtCastPicAttrNm.Text = db.GetStringValue("PCAST_PIC_ATTR_NM");
				txtPriority.Text = db.GetStringValue("PPRIORITY");
				chkPosterDelNaFlag.Checked = db.GetStringValue("pPOSTER_DEL_NA_FLAG").Equals(ViCommConst.FLAG_ON_STR);
				chkUseIndividualPayFlag.Checked = db.GetStringValue("pUSE_INDIVIDUAL_PAY_FLAG").Equals(ViCommConst.FLAG_ON_STR);
				txtPostPayAmt.Text = db.GetStringValue("pPOST_PAY_AMT");
				txtViewPayAmt.Text = db.GetStringValue("pVIEW_PAY_AMT");
				chkUseIndividualChargeFlag.Checked = db.GetStringValue("pUSE_INDIVIDUAL_CHARGE_FLAG").Equals(ViCommConst.FLAG_ON_STR);
				txtViewerChargePoint.Text = db.GetStringValue("pVIEWER_CHARGE_POINT");
				txtUploadLimitCount.Text = db.GetStringValue("pUPLOAD_LIMIT_COUNT");
			} else {
				ClearField();
			}
		}

		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;

	}

	private void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_PIC_ATTR_TYPE_VALUE_MNT");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PCAST_PIC_ATTR_TYPE_SEQ",DbSession.DbType.NUMBER,lstCastPicAttrTypeSeq.SelectedValue);
			db.ProcedureInParm("PCAST_PIC_ATTR_SEQ",DbSession.DbType.NUMBER,lblCastPicAttrSeq.Text);
			db.ProcedureInParm("PCAST_PIC_ATTR_NM",DbSession.DbType.VARCHAR2,txtCastPicAttrNm.Text);
			db.ProcedureInParm("PPRIORITY",DbSession.DbType.NUMBER,txtPriority.Text);
			db.ProcedureInParm("pPOSTER_DEL_NA_FLAG",DbSession.DbType.NUMBER,chkPosterDelNaFlag.Checked ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF);
			db.ProcedureInParm("pUSE_INDIVIDUAL_PAY_FLAG",DbSession.DbType.NUMBER,chkUseIndividualPayFlag.Checked ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF);
			db.ProcedureInParm("pPOST_PAY_AMT",DbSession.DbType.NUMBER,0);
			db.ProcedureInParm("pVIEW_PAY_AMT",DbSession.DbType.NUMBER,txtViewPayAmt.Text);
			db.ProcedureInParm("pUSE_INDIVIDUAL_CHARGE_FLAG",DbSession.DbType.NUMBER,chkUseIndividualChargeFlag.Checked ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF);
			db.ProcedureInParm("pVIEWER_CHARGE_POINT",DbSession.DbType.NUMBER,txtViewerChargePoint.Text);
			db.ProcedureInParm("pUPLOAD_LIMIT_COUNT",DbSession.DbType.NUMBER,txtUploadLimitCount.Text);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		int iIdx = lstSiteCd.SelectedIndex;
		InitPage();
		lstSeekSiteCd.SelectedIndex = iIdx;
		GetList();
	}

	private void JoinCells(GridView pGrd,int pCol) {
		int iRow = pGrd.Rows.Count;
		int iBaseidx = 0;

		while (iBaseidx < iRow) {
			int iNextIdx = iBaseidx + 1;
			TableCell celBase = pGrd.Rows[iBaseidx].Cells[pCol];

			while (iNextIdx < iRow) {

				TableCell celNext = pGrd.Rows[iNextIdx].Cells[pCol];

				if (GetText(celBase).Equals(GetText(celNext))) {
					if (celBase.RowSpan == 0) {
						celBase.RowSpan = 2;
					} else {
						celBase.RowSpan++;
					}
					pGrd.Rows[iNextIdx].Cells.Remove(celNext);
					iNextIdx++;
				} else {
					break;
				}
			}
			iBaseidx = iNextIdx;
		}
	}

	private string GetText(TableCell tc) {
		Label dblc =
		  (Label)tc.Controls[1];
		return dblc.Text;
	}

	protected void dsCastPicAttrType_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
	}

	protected void dsCastPicAttrTypeValue_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
	}
}
