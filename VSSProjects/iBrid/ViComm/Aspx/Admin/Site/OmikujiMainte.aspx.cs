﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: おみくじ設定
--	Progaram ID		: OmikujiMainte
--
--  Creation Date	: 2011.09.09
--  Creater			: iBrid
--
**************************************************************************/

// [ this.Update History ]
/*------------------------------------------------------------------------

  Date        this.Updater    this.Update Explain

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;
using System.Windows.Forms;

public partial class Site_OmikujiMainte : System.Web.UI.Page
{

    private string SexCd {
        get { return iBridUtil.GetStringValue(this.ViewState["SexCd"]); }
        set { this.ViewState["SexCd"] = value; }
    }

    private string RevisionNo {
        get { return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]); }
        set { this.ViewState["RevisionNo"] = value; }
    }

    private string Rowid {
        get { return iBridUtil.GetStringValue(this.ViewState["Rowid"]); }
        set { this.ViewState["Rowid"] = value; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
			this.SexCd = Request.QueryString["sexcd"];
            this.InitPage();
            this.FirstLoad();
        }
    }

    protected void FirstLoad() {

        if (iBridUtil.GetStringValue(this.SexCd).Equals(ViCommConst.MAN)) {
            this.lblPgmTitle.Text = "男性" + lblPgmTitle.Text;
            this.Title = "男性" + this.Title;
        } else {
            this.lblPgmTitle.Text = "女性" + lblPgmTitle.Text;
            this.Title = "女性" + this.Title;
        }
        this.DataBind();
        if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
            this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
            this.lstSeekSiteCd.SelectedValue = this.lstSiteCd.SelectedValue;
        }

    }

    private void InitPage()
    {
        this.ClearField();
        this.pnlMainte.Visible = false;
        this.lblErrorMessage.Visible = false;
    }

    private void ClearField() {
        this.txtOmikujiPointNormal1.Text = "0";
        this.txtOmikujiPointNormalRate1.Text = "0";
        this.txtOmikujiPointNoReceipt1.Text = "0";
        this.txtOmikujiPointNoReceiptRate1.Text = "0";

        this.txtOmikujiPointNormal2.Text = "0";
        this.txtOmikujiPointNormalRate2.Text = "0";
        this.txtOmikujiPointNoReceipt2.Text = "0";
        this.txtOmikujiPointNoReceiptRate2.Text = "0";

        this.txtOmikujiPointNormal3.Text = "0";
        this.txtOmikujiPointNormalRate3.Text = "0";
        this.txtOmikujiPointNoReceipt3.Text = "0";
        this.txtOmikujiPointNoReceiptRate3.Text = "0";

        this.txtOmikujiPointNormal4.Text = "0";
        this.txtOmikujiPointNormalRate4.Text = "0";
        this.txtOmikujiPointNoReceipt4.Text = "0";
        this.txtOmikujiPointNoReceiptRate4.Text = "0";

        this.txtOmikujiPointNormal5.Text = "0";
        this.txtOmikujiPointNormalRate5.Text = "0";
        this.txtOmikujiPointNoReceipt5.Text = "0";
        this.txtOmikujiPointNoReceiptRate5.Text = "0";

		if (!this.SexCd.Equals(ViCommConst.MAN)) {
			this.plcManHeader.Visible = false;
			this.plcWomanHeader.Visible = true;
			this.plcNoReciptContents1.Visible = false;
			this.plcNoReciptContents2.Visible = false;
			this.plcNoReciptContents3.Visible = false;
			this.plcNoReciptContents4.Visible = false;
			this.plcNoReciptContents5.Visible = false;
		}
    }

    protected void GetData() {

        using (DbSession oDbSession = new DbSession())
        {
            oDbSession.PrepareProcedure("OMIKUJI_GET");
            oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.lstSiteCd.SelectedValue);
            oDbSession.ProcedureInParm("pSEX_CD", DbSession.DbType.VARCHAR2, this.SexCd);

            oDbSession.ProcedureOutParm("pOMIKUJI_POINT_NORMAL1", DbSession.DbType.NUMBER);
            oDbSession.ProcedureOutParm("pOMIKUJI_POINT_NORMAL_RATE1", DbSession.DbType.NUMBER);
            oDbSession.ProcedureOutParm("pOMIKUJI_POINT_NO_RECEIPT1", DbSession.DbType.NUMBER);
            oDbSession.ProcedureOutParm("pOMIKUJI_POINT_NO_RCPT_RATE1", DbSession.DbType.NUMBER);

            oDbSession.ProcedureOutParm("pOMIKUJI_POINT_NORMAL2", DbSession.DbType.NUMBER);
            oDbSession.ProcedureOutParm("pOMIKUJI_POINT_NORMAL_RATE2", DbSession.DbType.NUMBER);
            oDbSession.ProcedureOutParm("pOMIKUJI_POINT_NO_RECEIPT2", DbSession.DbType.NUMBER);
            oDbSession.ProcedureOutParm("pOMIKUJI_POINT_NO_RCPT_RATE2", DbSession.DbType.NUMBER);

            oDbSession.ProcedureOutParm("pOMIKUJI_POINT_NORMAL3", DbSession.DbType.NUMBER);
            oDbSession.ProcedureOutParm("pOMIKUJI_POINT_NORMAL_RATE3", DbSession.DbType.NUMBER);
            oDbSession.ProcedureOutParm("pOMIKUJI_POINT_NO_RECEIPT3", DbSession.DbType.NUMBER);
            oDbSession.ProcedureOutParm("pOMIKUJI_POINT_NO_RCPT_RATE3", DbSession.DbType.NUMBER);

            oDbSession.ProcedureOutParm("pOMIKUJI_POINT_NORMAL4", DbSession.DbType.NUMBER);
            oDbSession.ProcedureOutParm("pOMIKUJI_POINT_NORMAL_RATE4", DbSession.DbType.NUMBER);
            oDbSession.ProcedureOutParm("pOMIKUJI_POINT_NO_RECEIPT4", DbSession.DbType.NUMBER);
            oDbSession.ProcedureOutParm("pOMIKUJI_POINT_NO_RCPT_RATE4", DbSession.DbType.NUMBER);

            oDbSession.ProcedureOutParm("pOMIKUJI_POINT_NORMAL5", DbSession.DbType.NUMBER);
            oDbSession.ProcedureOutParm("pOMIKUJI_POINT_NORMAL_RATE5", DbSession.DbType.NUMBER);
            oDbSession.ProcedureOutParm("pOMIKUJI_POINT_NO_RECEIPT5", DbSession.DbType.NUMBER);
            oDbSession.ProcedureOutParm("pOMIKUJI_POINT_NO_RCPT_RATE5", DbSession.DbType.NUMBER);
            
            oDbSession.ProcedureOutParm("pROWID", DbSession.DbType.VARCHAR2);
            oDbSession.ProcedureOutParm("pREVISION_NO", DbSession.DbType.NUMBER);
            oDbSession.ProcedureOutParm("pRECORD_COUNT", DbSession.DbType.NUMBER);
            oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
            oDbSession.ExecuteProcedure();

            this.RevisionNo = oDbSession.GetStringValue("PREVISION_NO");
            this.Rowid = oDbSession.GetStringValue("PROWID");

            if (oDbSession.GetIntValue("PRECORD_COUNT") > 0) {
                this.txtOmikujiPointNormal1.Text = iBridUtil.GetStringValue(oDbSession.GetIntValue("pOMIKUJI_POINT_NORMAL1"));
                this.txtOmikujiPointNormalRate1.Text = iBridUtil.GetStringValue(oDbSession.GetIntValue("pOMIKUJI_POINT_NORMAL_RATE1"));
                this.txtOmikujiPointNoReceipt1.Text = iBridUtil.GetStringValue(oDbSession.GetIntValue("pOMIKUJI_POINT_NO_RECEIPT1"));
                this.txtOmikujiPointNoReceiptRate1.Text = iBridUtil.GetStringValue(oDbSession.GetIntValue("pOMIKUJI_POINT_NO_RCPT_RATE1"));

                this.txtOmikujiPointNormal2.Text = iBridUtil.GetStringValue(oDbSession.GetIntValue("pOMIKUJI_POINT_NORMAL2"));
                this.txtOmikujiPointNormalRate2.Text = iBridUtil.GetStringValue(oDbSession.GetIntValue("pOMIKUJI_POINT_NORMAL_RATE2"));
                this.txtOmikujiPointNoReceipt2.Text = iBridUtil.GetStringValue(oDbSession.GetIntValue("pOMIKUJI_POINT_NO_RECEIPT2"));
                this.txtOmikujiPointNoReceiptRate2.Text = iBridUtil.GetStringValue(oDbSession.GetIntValue("pOMIKUJI_POINT_NO_RCPT_RATE2"));

                this.txtOmikujiPointNormal3.Text = iBridUtil.GetStringValue(oDbSession.GetIntValue("pOMIKUJI_POINT_NORMAL3"));
                this.txtOmikujiPointNormalRate3.Text = iBridUtil.GetStringValue(oDbSession.GetIntValue("pOMIKUJI_POINT_NORMAL_RATE3"));
                this.txtOmikujiPointNoReceipt3.Text = iBridUtil.GetStringValue(oDbSession.GetIntValue("pOMIKUJI_POINT_NO_RECEIPT3"));
                this.txtOmikujiPointNoReceiptRate3.Text = iBridUtil.GetStringValue(oDbSession.GetIntValue("pOMIKUJI_POINT_NO_RCPT_RATE3"));

                this.txtOmikujiPointNormal4.Text = iBridUtil.GetStringValue(oDbSession.GetIntValue("pOMIKUJI_POINT_NORMAL4"));
                this.txtOmikujiPointNormalRate4.Text = iBridUtil.GetStringValue(oDbSession.GetIntValue("pOMIKUJI_POINT_NORMAL_RATE4"));
                this.txtOmikujiPointNoReceipt4.Text = iBridUtil.GetStringValue(oDbSession.GetIntValue("pOMIKUJI_POINT_NO_RECEIPT4"));
                this.txtOmikujiPointNoReceiptRate4.Text = iBridUtil.GetStringValue(oDbSession.GetIntValue("pOMIKUJI_POINT_NO_RCPT_RATE4"));

                this.txtOmikujiPointNormal5.Text = iBridUtil.GetStringValue(oDbSession.GetIntValue("pOMIKUJI_POINT_NORMAL5"));
                this.txtOmikujiPointNormalRate5.Text = iBridUtil.GetStringValue(oDbSession.GetIntValue("pOMIKUJI_POINT_NORMAL_RATE5"));
                this.txtOmikujiPointNoReceipt5.Text = iBridUtil.GetStringValue(oDbSession.GetIntValue("pOMIKUJI_POINT_NO_RECEIPT5"));
                this.txtOmikujiPointNoReceiptRate5.Text = iBridUtil.GetStringValue(oDbSession.GetIntValue("pOMIKUJI_POINT_NO_RCPT_RATE5"));
            } else {
                this.ClearField();
            }
        }
        this.pnlMainte.Visible = true;
        this.pnlDtl.Visible = true;
    }

    protected void UpdateData() {
		if (this.SexCd.Equals(ViCommConst.MAN)) {
			if (!this.CheckRate()) {
				return;
			}
		} else {
			if (!this.CheckRateWoman()) {
				return;
			}

		}

        using (DbSession oDbSession = new DbSession())
        {
            oDbSession.PrepareProcedure("OMIKUJI_MAINTE");
            oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.lstSiteCd.SelectedValue);
            oDbSession.ProcedureInParm("pSEX_CD", DbSession.DbType.VARCHAR2, this.SexCd);

            oDbSession.ProcedureInParm("pOMIKUJI_POINT_NORMAL1", DbSession.DbType.NUMBER, int.Parse(txtOmikujiPointNormal1.Text));
            oDbSession.ProcedureInParm("pOMIKUJI_POINT_NORMAL_RATE1", DbSession.DbType.NUMBER, int.Parse(txtOmikujiPointNormalRate1.Text));
            oDbSession.ProcedureInParm("pOMIKUJI_POINT_NO_RECEIPT1", DbSession.DbType.NUMBER, int.Parse(txtOmikujiPointNoReceipt1.Text));
            oDbSession.ProcedureInParm("pOMIKUJI_POINT_NO_RCPT_RATE1", DbSession.DbType.NUMBER, int.Parse(txtOmikujiPointNoReceiptRate1.Text));

            oDbSession.ProcedureInParm("pOMIKUJI_POINT_NORMAL2", DbSession.DbType.NUMBER, int.Parse(txtOmikujiPointNormal2.Text));
            oDbSession.ProcedureInParm("pOMIKUJI_POINT_NORMAL_RATE2", DbSession.DbType.NUMBER, int.Parse(txtOmikujiPointNormalRate2.Text));
            oDbSession.ProcedureInParm("pOMIKUJI_POINT_NO_RECEIPT2", DbSession.DbType.NUMBER, int.Parse(txtOmikujiPointNoReceipt2.Text));
            oDbSession.ProcedureInParm("pOMIKUJI_POINT_NO_RCPT_RATE2", DbSession.DbType.NUMBER, int.Parse(txtOmikujiPointNoReceiptRate2.Text));

            oDbSession.ProcedureInParm("pOMIKUJI_POINT_NORMAL3", DbSession.DbType.NUMBER, int.Parse(txtOmikujiPointNormal3.Text));
            oDbSession.ProcedureInParm("pOMIKUJI_POINT_NORMAL_RATE3", DbSession.DbType.NUMBER, int.Parse(txtOmikujiPointNormalRate3.Text));
            oDbSession.ProcedureInParm("pOMIKUJI_POINT_NO_RECEIPT3", DbSession.DbType.NUMBER, int.Parse(txtOmikujiPointNoReceipt3.Text));
            oDbSession.ProcedureInParm("pOMIKUJI_POINT_NO_RCPT_RATE3", DbSession.DbType.NUMBER, int.Parse(txtOmikujiPointNoReceiptRate3.Text));

            oDbSession.ProcedureInParm("pOMIKUJI_POINT_NORMAL4", DbSession.DbType.NUMBER, int.Parse(txtOmikujiPointNormal4.Text));
            oDbSession.ProcedureInParm("pOMIKUJI_POINT_NORMAL_RATE4", DbSession.DbType.NUMBER, int.Parse(txtOmikujiPointNormalRate4.Text));
            oDbSession.ProcedureInParm("pOMIKUJI_POINT_NO_RECEIPT4", DbSession.DbType.NUMBER, int.Parse(txtOmikujiPointNoReceipt4.Text));
            oDbSession.ProcedureInParm("pOMIKUJI_POINT_NO_RCPT_RATE4", DbSession.DbType.NUMBER, int.Parse(txtOmikujiPointNoReceiptRate4.Text));

            oDbSession.ProcedureInParm("pOMIKUJI_POINT_NORMAL5", DbSession.DbType.NUMBER, int.Parse(txtOmikujiPointNormal5.Text));
            oDbSession.ProcedureInParm("pOMIKUJI_POINT_NORMAL_RATE5", DbSession.DbType.NUMBER, int.Parse(txtOmikujiPointNormalRate5.Text));
            oDbSession.ProcedureInParm("pOMIKUJI_POINT_NO_RECEIPT5", DbSession.DbType.NUMBER, int.Parse(txtOmikujiPointNoReceipt5.Text));
            oDbSession.ProcedureInParm("pOMIKUJI_POINT_NO_RCPT_RATE5", DbSession.DbType.NUMBER, int.Parse(txtOmikujiPointNoReceiptRate5.Text));

            oDbSession.ProcedureInParm("pROWID", DbSession.DbType.VARCHAR2, this.Rowid);
            oDbSession.ProcedureInParm("pREVISION_NO", DbSession.DbType.NUMBER, decimal.Parse(this.RevisionNo));
            oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
            oDbSession.ExecuteProcedure();
        }
        this.InitPage();

    }

	protected bool CheckRateWoman() {
		lblErrorMessage.Visible = false;

		int iNormalRate = int.Parse(this.txtOmikujiPointNormalRate1.Text) +
							int.Parse(this.txtOmikujiPointNormalRate2.Text) +
							int.Parse(this.txtOmikujiPointNormalRate3.Text) +
							int.Parse(this.txtOmikujiPointNormalRate4.Text) +
							int.Parse(this.txtOmikujiPointNormalRate5.Text);

		if (iNormalRate != 100) {
			lblErrorMessage.Text = "確率合計が100%になるように入力してください。";
			lblErrorMessage.Visible = true;
			return false;
		}

		return true;
	}

    protected bool CheckRate() { 
        lblErrorMessage.Visible= false;
        string sErrorTmpMsg = string.Empty;

        int iNormalRate =   int.Parse(this.txtOmikujiPointNormalRate1.Text) +
                            int.Parse(this.txtOmikujiPointNormalRate2.Text) +
                            int.Parse(this.txtOmikujiPointNormalRate3.Text) +
                            int.Parse(this.txtOmikujiPointNormalRate4.Text) +
                            int.Parse(this.txtOmikujiPointNormalRate5.Text);
        if(iNormalRate != 100){
            sErrorTmpMsg = "課金あり";
        }

        int iNoReceiptRate =    int.Parse(this.txtOmikujiPointNoReceiptRate1.Text) +
                                int.Parse(this.txtOmikujiPointNoReceiptRate2.Text) +
                                int.Parse(this.txtOmikujiPointNoReceiptRate3.Text) +
                                int.Parse(this.txtOmikujiPointNoReceiptRate4.Text) +
                                int.Parse(this.txtOmikujiPointNoReceiptRate5.Text);
        if (iNoReceiptRate != 100) {
            if (sErrorTmpMsg.Equals(string.Empty)) {
                sErrorTmpMsg = "課金なし";
            }else{
                sErrorTmpMsg = string.Format("{0}、課金なし", sErrorTmpMsg);
            }
        }

        if (!sErrorTmpMsg.Equals(string.Empty)) {
            lblErrorMessage.Text = string.Format("{0}の確率合計が100%になるように入力してください。",sErrorTmpMsg);
            lblErrorMessage.Visible = true;
            return false;
        }

        return true;
    }

    # region === Click Event ===
    protected void btnUpdate_Click(object sender, EventArgs e) {
        if (IsValid) {
            this.UpdateData();
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e) {
        this.InitPage();
    }
    protected void btnSeek_Click(object sender, EventArgs e) {
        this.lstSiteCd.SelectedValue = this.lstSeekSiteCd.SelectedValue;
        this.InitPage();
        this.GetData();
    }
    # endregion

}
