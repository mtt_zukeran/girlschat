﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CastChargeTempConditionList.aspx.cs" Inherits="Site_CastChargeTempConditionList"
    Title="出演者課金設定パターン" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="出演者課金設定パターン"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlMainte">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel runat="server" ID="pnlKey">
                    <fieldset class="fieldset-inner">
                        <table border="0" style="width: 640px" class="tableStyle">
                            <tr>
                                <td class="tdHeaderStyle">
                                    サイトコード
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                        DataValueField="SITE_CD" Width="206px" AutoPostBack="True" OnSelectedIndexChanged="lstSiteCd_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    カテゴリ
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstActCategorySeq" runat="server" Width="206px" DataSourceID="dsActCategory"
                                        DataTextField="ACT_CATEGORY_NM" DataValueField="ACT_CATEGORY_SEQ">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle2">
                                    設定コード
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstCastChargeTempCd" runat="server" Width="206px" DataSourceID="dsCastChargeTempCd"
                                        DataTextField="CODE_NM" DataValueField="CODE" OnDataBound="lst_DataBound">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="vdrCastChargeTempCd" runat="server" ErrorMessage="設定コードを選択して下さい。"
                                        ControlToValidate="lstCastChargeTempCd" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
                                        TargetControlID="vdrCastChargeTempCd" HighlightCssClass="validatorCallout" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlDtl">
                    <fieldset class="fieldset-inner">
                        <legend>[パターン内容]</legend>
                        <table border="0" style="width: 640px; margin-bottom: 4px" class="tableStyle">
                            <tr>
                                <td class="tdHeaderStyle2">
                                    パターン名
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtCastChargeTempCdNm" runat="server" MaxLength="80" Width="200px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vdrCastChargeTempCdNm" runat="server" ErrorMessage="パターン名を入力して下さい。"
                                        ControlToValidate="txtCastChargeTempCdNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2"
                                        TargetControlID="vdrCastChargeTempCdNm" HighlightCssClass="validatorCallout" />
                                </td>
                            </tr>
                        </table>
                        <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click"
                            ValidationGroup="Detail" />
                        <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click"
                            ValidationGroup="Key" />
                        <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click"
                            CausesValidation="False" />
                    </fieldset>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[検索条件]</legend>
                <table border="0" style="width: 640px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle2">
                            サイトコード
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="180px" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    CausesValidation="False" />
                <asp:Button runat="server" ID="btnRegist" Text="追加" CssClass="seekbutton" CausesValidation="False"
                    OnClick="btnRegist_Click" />
                <%--<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />--%>
                <asp:Button ID="btnBack" runat="server" Text="戻る" CssClass="seekbutton" OnClick="btnBack_Click" />
            </fieldset>
            <fieldset>
                <legend>[パターン一覧]</legend>
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="502px">
                    <asp:Label ID="lblNaError" runat="server" Text="複数の条件を有効にすることは出来ません。" ForeColor="red"></asp:Label>
                    <asp:GridView ID="grdCastChargeTempCondition" runat="server" AutoGenerateColumns="False"
                        DataSourceID="dsCastChargeTempCondition" AllowPaging="true" AllowSorting="true"
                        SkinID="GridViewColor">
                        <Columns>
                            <asp:TemplateField HeaderText="設定ｺｰﾄﾞ">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkCastChargeTempCd" runat="server" OnCommand="lnkCastChargeTempCd_Command"
                                        CommandArgument='<%# Eval("CAST_CHARGE_TEMP_CD") %>' Text='<%# Eval("CAST_CHARGE_TEMP_CD") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="パターン">
                                <ItemTemplate>
                                    <%--<asp:HyperLink ID="lnkCastChargeTempCdNm" runat="server" NavigateUrl='<%# string.Format("~/Site/CastChargeTempList.aspx?sitecd={0}&sitenm={1}&actcategoryseq={2}&actcategorynm={3}&tempcd={4}&tempnm={5}",Eval("SITE_CD"),Eval("SITE_NM"),Eval("ACT_CATEGORY_SEQ"),Eval("ACT_CATEGORY_NM"),Eval("CAST_CHARGE_TEMP_CD"),Eval("CAST_CHARGE_TEMP_NM")) %>'
                                        Text='<%# Eval("CAST_CHARGE_TEMP_CD_NM") %>' />--%>
                                    <asp:LinkButton ID="lnkCastChargeTempCdNm" runat="server" OnCommand="lnkCastChargeTemp_Command"
                                        CommandArgument='<%# string.Format("~/Site/CastChargeTempList.aspx?sitecd={0}&sitenm={1}&tempcd={2}&tempcdnm={3}",Eval("SITE_CD"),Eval("SITE_NM"),Eval("CAST_CHARGE_TEMP_CD"),Eval("CAST_CHARGE_TEMP_CD_NM"))%>'
                                        Text='<%# Eval("CAST_CHARGE_TEMP_CD_NM") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="150px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="有効">
                                <ItemTemplate>
                                    <asp:Label ID="lblNaFlag" runat="server" Text='<%# ViCommConst.FLAG_OFF_STR.Equals(Eval("NA_FLAG","{0}")) ? "○" : string.Empty %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="APPLICATION_DATE" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}"
                                HeaderText="適用日時">
                                <ItemStyle HorizontalAlign="Center" Width="140px" />
                            </asp:BoundField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Button ID="btnActivate" runat="server" CausesValidation="false" CommandArgument='<%# string.Format("{0},0",Eval("CAST_CHARGE_TEMP_CD")) %>'
                                        OnClientClick='return confirm("この条件を有効にしますか？");' OnCommand="btnActivate_Command"
                                        Text="有効" Visible='<%# ViCommConst.FLAG_ON_STR.Equals(Eval("NA_FLAG","{0}")) %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Button ID="btnNotActivate" runat="server" CausesValidation="false" CommandArgument='<%# string.Format("{0},1",Eval("CAST_CHARGE_TEMP_CD")) %>'
                                        OnClientClick='return confirm("この条件を無効にしますか？");' OnCommand="btnActivate_Command"
                                        Text="解除" Visible='<%# ViCommConst.FLAG_OFF_STR.Equals(Eval("NA_FLAG","{0}")) %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsActCategory" runat="server" SelectMethod="GetList" TypeName="ActCategory">
        <SelectParameters>
            <asp:QueryStringParameter Name="pSiteCd" QueryStringField="sitecd" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsCastChargeTempCondition" runat="server" SelectMethod="GetList"
        TypeName="CastChargeTempCondition">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSeekSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsCastChargeTempCd" runat="server" SelectMethod="GetList"
        TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="A6" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
