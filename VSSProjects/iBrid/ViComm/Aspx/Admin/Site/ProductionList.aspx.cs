﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: プロダクション情報メンテナンス
--	Progaram ID		: ProductionList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;

public partial class Site_ProductionList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_SITE_OWNER)) {
			throw new ApplicationException("ViComm権限違反");
		}
		if (!IsPostBack) {
			InitPage();
		}
	}

	protected void dsProduction_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		grdProduction.PageSize = int.Parse(Session["PageSize"].ToString());
		grdProduction.PageIndex = 0;
		lblProductionSeq.Text = "";
		ClearField();
		pnlMainte.Visible = false;
		DataBind();
	}

	private void ClearField() {
		txtProductionNm.Text = "";
		txtTel.Text = "";
		txtEmailAddr.Text = "";
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		GetData();
	}

	protected void lnkProductionSeq_Command(object sender,CommandEventArgs e) {
		lblProductionSeq.Text = e.CommandArgument.ToString();
		GetData();
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("PRODUCTION_GET");
			db.ProcedureInParm("PPRODUCTION_SEQ",DbSession.DbType.VARCHAR2,lblProductionSeq.Text);
			db.ProcedureOutParm("PPRODUCTION_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PTEL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PEMAIL_ADDR",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtProductionNm.Text = db.GetStringValue("PPRODUCTION_NM");
				txtTel.Text = db.GetStringValue("PTEL");
				txtEmailAddr.Text = db.GetStringValue("PEMAIL_ADDR");
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
	}

	private void UpdateData(int pDelFlag) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("PRODUCTION_MAINTE");
			db.ProcedureInParm("PPRODUCTION_SEQ",DbSession.DbType.VARCHAR2,lblProductionSeq.Text);
			db.ProcedureInParm("PPRODUCTION_NM",DbSession.DbType.VARCHAR2,txtProductionNm.Text);
			db.ProcedureInParm("PTEL",DbSession.DbType.VARCHAR2,txtTel.Text);
			db.ProcedureInParm("PEMAIL_ADDR",DbSession.DbType.VARCHAR2,txtEmailAddr.Text);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		InitPage();
	}
}
