﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ColorSample.aspx.cs" Inherits="System_ColorSample" Title="カラーサンプル" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<table border="1" cellpadding="0" cellspacing="0" bgcolor="#99BBCC" align="center">
			<tr>
				<td>
					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td height="20" valign="middle" style="font-size: 12px;">
								カラー見本表
							</td>
						</tr>
						<tr>
							<td bgcolor="#FFFFFF">
								<table border="0" cellpadding="" cellspacing="0" bgcolor="#FFFFFF">
									<tr>
										<td colspan="6">
										</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#000000">■</font>#000000
										</td>
										<td width="78" style="font-size: 12px;">
											<font color="#003300">■</font>#003300
										</td>
										<td width="78" style="font-size: 12px;">
											<font color="#006600">■</font>#006600
										</td>
										<td width="78" style="font-size: 12px;">
											<font color="#009900">■</font>#009900
										</td>
										<td width="78" style="font-size: 12px;">
											<font color="#00CC00">■</font>#00CC00
										</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#00FF00">■</font>#00FF00
										</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#000033">■</font>#000033</td>
										<td width="78" style="font-size: 12px;">
											<font color="#003333">■</font>#003333</td>
										<td width="78" style="font-size: 12px;">
											<font color="#006633">■</font>#006633</td>
										<td width="78" style="font-size: 12px;">
											<font color="#009933">■</font>#009933</td>
										<td width="78" style="font-size: 12px;">
											<font color="#00CC33">■</font>#00CC33</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#00FF33">■</font>#00FF33</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#000066">■</font>#000066</td>
										<td width="78" style="font-size: 12px;">
											<font color="#003366">■</font>#003366</td>
										<td width="78" style="font-size: 12px;">
											<font color="#006666">■</font>#006666</td>
										<td width="78" style="font-size: 12px;">
											<font color="#009966">■</font>#009966</td>
										<td width="78" style="font-size: 12px;">
											<font color="#00CC66">■</font>#00CC66</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#00FF66">■</font>#00FF66</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#000099">■</font>#000099</td>
										<td width="78" style="font-size: 12px;">
											<font color="#003399">■</font>#003399</td>
										<td width="78" style="font-size: 12px;">
											<font color="#006699">■</font>#006699</td>
										<td width="78" style="font-size: 12px;">
											<font color="#009999">■</font>#009999</td>
										<td width="78" style="font-size: 12px;">
											<font color="#00CC99">■</font>#00CC99</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#00FF99">■</font>#00FF99</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#0000CC">■</font>#0000CC</td>
										<td width="78" style="font-size: 12px;">
											<font color="#0033CC">■</font>#0033CC</td>
										<td width="78" style="font-size: 12px;">
											<font color="#0066CC">■</font>#0066CC</td>
										<td width="78" style="font-size: 12px;">
											<font color="#0099CC">■</font>#0099CC</td>
										<td width="78" style="font-size: 12px;">
											<font color="#00CCCC">■</font>#00CCCC</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#00FFCC">■</font>#00FFCC</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#0000FF">■</font>#0000FF</td>
										<td width="78" style="font-size: 12px;">
											<font color="#0033FF">■</font>#0033FF</td>
										<td width="78" style="font-size: 12px;">
											<font color="#0066FF">■</font>#0066FF</td>
										<td width="78" style="font-size: 12px;">
											<font color="#0099FF">■</font>#0099FF</td>
										<td width="78" style="font-size: 12px;">
											<font color="#00CCFF">■</font>#00CCFF</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#00FFFF">■</font>#00FFFF</td>
									</tr>
									<tr>
										<td width="468" colspan="6" height="2">
										</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#330000">■</font>#330000</td>
										<td width="78" style="font-size: 12px;">
											<font color="#333300">■</font>#333300</td>
										<td width="78" style="font-size: 12px;">
											<font color="#336600">■</font>#336600</td>
										<td width="78" style="font-size: 12px;">
											<font color="#339900">■</font>#339900</td>
										<td width="78" style="font-size: 12px;">
											<font color="#33CC00">■</font>#33CC00</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#33FF00">■</font>#33FF00</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#330033">■</font>#330033</td>
										<td width="78" style="font-size: 12px;">
											<font color="#333333">■</font>#333333</td>
										<td width="78" style="font-size: 12px;">
											<font color="#336633">■</font>#336633</td>
										<td width="78" style="font-size: 12px;">
											<font color="#339933">■</font>#339933</td>
										<td width="78" style="font-size: 12px;">
											<font color="#33CC33">■</font>#33CC33</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#33FF33">■</font>#33FF33</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#330066">■</font>#330066</td>
										<td width="78" style="font-size: 12px;">
											<font color="#333366">■</font>#333366</td>
										<td width="78" style="font-size: 12px;">
											<font color="#336666">■</font>#336666</td>
										<td width="78" style="font-size: 12px;">
											<font color="#339966">■</font>#339966</td>
										<td width="78" style="font-size: 12px;">
											<font color="#33CC66">■</font>#33CC66</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#33FF66">■</font>#33FF66</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#330099">■</font>#330099</td>
										<td width="78" style="font-size: 12px;">
											<font color="#333399">■</font>#333399</td>
										<td width="78" style="font-size: 12px;">
											<font color="#336699">■</font>#336699</td>
										<td width="78" style="font-size: 12px;">
											<font color="#339999">■</font>#339999</td>
										<td width="78" style="font-size: 12px;">
											<font color="#33CC99">■</font>#33CC99</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#33FF99">■</font>#33FF99
										</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#3300CC">■</font>#3300CC</td>
										<td width="78" style="font-size: 12px;">
											<font color="#3333CC">■</font>#3333CC</td>
										<td width="78" style="font-size: 12px;">
											<font color="#3366CC">■</font>#3366CC</td>
										<td width="78" style="font-size: 12px;">
											<font color="#3399CC">■</font>#3399CC</td>
										<td width="78" style="font-size: 12px;">
											<font color="#33CCCC">■</font>#33CCCC</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#33FFCC">■</font>#33FFCC</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#3300FF">■</font>#3300FF</td>
										<td width="78" style="font-size: 12px;">
											<font color="#3333FF">■</font>#3333FF</td>
										<td width="78" style="font-size: 12px;">
											<font color="#3366FF">■</font>#3366FF</td>
										<td width="78" style="font-size: 12px;">
											<font color="#3399FF">■</font>#3399FF</td>
										<td width="78" style="font-size: 12px;">
											<font color="#33CCFF">■</font>#33CCFF</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#33FFFF">■</font>#33FFFF</td>
									</tr>
									<tr>
										<td width="468" colspan="6">
											<img src="ima_mp/common/shim.gif" width="468" height="5">
										</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#660000">■</font>#660000</td>
										<td width="78" style="font-size: 12px;">
											<font color="#663300">■</font>#663300</td>
										<td width="78" style="font-size: 12px;">
											<font color="#666600">■</font>#666600</td>
										<td width="78" style="font-size: 12px;">
											<font color="#669900">■</font>#669900</td>
										<td width="78" style="font-size: 12px;">
											<font color="#66CC00">■</font>#66CC00</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#66FF00">■</font>#66FF00</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#660033">■</font>#660033</td>
										<td width="78" style="font-size: 12px;">
											<font color="#663333">■</font>#663333</td>
										<td width="78" style="font-size: 12px;">
											<font color="#666633">■</font>#666633</td>
										<td width="78" style="font-size: 12px;">
											<font color="#669933">■</font>#669933</td>
										<td width="78" style="font-size: 12px;">
											<font color="#66CC33">■</font>#66CC33</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#66FF33">■</font>#66FF33</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="##660066">■</font>#660066</td>
										<td width="78" style="font-size: 12px;">
											<font color="##663366">■</font>#663366</td>
										<td width="78" style="font-size: 12px;">
											<font color="##666666">■</font>#666666</td>
										<td width="78" style="font-size: 12px;">
											<font color="##669966">■</font>#669966</td>
										<td width="78" style="font-size: 12px;">
											<font color="##66CC66">■</font>#66CC66</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="##66FF66">■</font>#66FF66</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#660099">■</font>#660099</td>
										<td width="78" style="font-size: 12px;">
											<font color="#663399">■</font>#663399</td>
										<td width="78" style="font-size: 12px;">
											<font color="#666699">■</font>#666699</td>
										<td width="78" style="font-size: 12px;">
											<font color="#669999">■</font>#669999</td>
										<td width="78" style="font-size: 12px;">
											<font color="#66CC99">■</font>#66CC99</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#66FF99">■</font>#66FF99</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#6600CC">■</font>#6600CC</td>
										<td width="78" style="font-size: 12px;">
											<font color="#6633CC">■</font>#6633CC</td>
										<td width="78" style="font-size: 12px;">
											<font color="#6666CC">■</font>#6666CC</td>
										<td width="78" style="font-size: 12px;">
											<font color="#6699CC">■</font>#6699CC</td>
										<td width="78" style="font-size: 12px;">
											<font color="#66CCCC">■</font>#66CCCC</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#66FFCC">■</font>#66FFCC</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#6600FF">■</font>#6600FF</td>
										<td width="78" style="font-size: 12px;">
											<font color="#6633FF">■</font>#6633FF</td>
										<td width="78" style="font-size: 12px;">
											<font color="#6666FF">■</font>#6666FF</td>
										<td width="78" style="font-size: 12px;">
											<font color="#6699FF">■</font>#6699FF</td>
										<td width="78" style="font-size: 12px;">
											<font color="#66CCFF">■</font>#66CCFF</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#66FFFF">■</font>#66FFFF</td>
									</tr>
									<tr>
										<td width="468" colspan="6">
											<img src="ima_mp/common/shim.gif" width="468" height="5">
										</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#990000">■</font>#990000</td>
										<td width="78" style="font-size: 12px;">
											<font color="#993300">■</font>#993300</td>
										<td width="78" style="font-size: 12px;">
											<font color="#996600">■</font>#996600</td>
										<td width="78" style="font-size: 12px;">
											<font color="#999900">■</font>#999900</td>
										<td width="78" style="font-size: 12px;">
											<font color="#99CC00">■</font>#99CC00</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#99FF00">■</font>#99FF00</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#990033">■</font>#990033</td>
										<td width="78" style="font-size: 12px;">
											<font color="#993333">■</font>#993333</td>
										<td width="78" style="font-size: 12px;">
											<font color="#996633">■</font>#996633</td>
										<td width="78" style="font-size: 12px;">
											<font color="#999933">■</font>#999933</td>
										<td width="78" style="font-size: 12px;">
											<font color="#99CC33">■</font>#99CC33</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#99FF33">■</font>#99FF33</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#990066">■</font>#990066</td>
										<td width="78" style="font-size: 12px;">
											<font color="#993366">■</font>#993366</td>
										<td width="78" style="font-size: 12px;">
											<font color="#996666">■</font>#996666</td>
										<td width="78" style="font-size: 12px;">
											<font color="#999966">■</font>#999966</td>
										<td width="78" style="font-size: 12px;">
											<font color="#99CC66">■</font>#99CC66</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#99FF66">■</font>#99FF66</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#990099">■</font>#990099</td>
										<td width="78" style="font-size: 12px;">
											<font color="#993399">■</font>#993399</td>
										<td width="78" style="font-size: 12px;">
											<font color="#996699">■</font>#996699</td>
										<td width="78" style="font-size: 12px;">
											<font color="#999999">■</font>#999999</td>
										<td width="78" style="font-size: 12px;">
											<font color="#99CC99">■</font>#99CC99</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#99FF99">■</font>#99FF99</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#9900CC">■</font>#9900CC</td>
										<td width="78" style="font-size: 12px;">
											<font color="#99CCCC">■</font>#9933CC</td>
										<td width="78" style="font-size: 12px;">
											<font color="#9966CC">■</font>#9966CC</td>
										<td width="78" style="font-size: 12px;">
											<font color="#9999CC">■</font>#9999CC</td>
										<td width="78" style="font-size: 12px;">
											<font color="#99CCCC">■</font>#99CCCC</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#99FFCC">■</font>#99FFCC</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#9900FF">■</font>#9900FF</td>
										<td width="78" style="font-size: 12px;">
											<font color="#9933FF">■</font>#9933FF</td>
										<td width="78" style="font-size: 12px;">
											<font color="#9966FF">■</font>#9966FF</td>
										<td width="78" style="font-size: 12px;">
											<font color="#9999FF">■</font>#9999FF</td>
										<td width="78" style="font-size: 12px;">
											<font color="#99CCFF">■</font>#99CCFF</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#99FFFF">■</font>#99FFFF</td>
									</tr>
									<tr>
										<td width="468" colspan="6">
											<img src="ima_mp/common/shim.gif" width="468" height="5">
										</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#CC0000">■</font>#CC0000</td>
										<td width="78" style="font-size: 12px;">
											<font color="#CC3300">■</font>#CC3300</td>
										<td width="78" style="font-size: 12px;">
											<font color="#CC6600">■</font>#CC6600</td>
										<td width="78" style="font-size: 12px;">
											<font color="#CC9900">■</font>#CC9900</td>
										<td width="78" style="font-size: 12px;">
											<font color="#CCCC00">■</font>#CCCC00</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#CCFF00">■</font>#CCFF00</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#CC0033">■</font>#CC0033</td>
										<td width="78" style="font-size: 12px;">
											<font color="#CC3333">■</font>#CC3333</td>
										<td width="78" style="font-size: 12px;">
											<font color="#CC6633">■</font>#CC6633</td>
										<td width="78" style="font-size: 12px;">
											<font color="#CC9933">■</font>#CC9933</td>
										<td width="78" style="font-size: 12px;">
											<font color="#CCCC33">■</font>#CCCC33</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#CCFF33">■</font>#CCFF33</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#CC0066">■</font>#CC0066</td>
										<td width="78" style="font-size: 12px;">
											<font color="#CC3366">■</font>#CC3366</td>
										<td width="78" style="font-size: 12px;">
											<font color="#CC6666">■</font>#CC6666</td>
										<td width="78" style="font-size: 12px;">
											<font color="#CC9966">■</font>#CC9966</td>
										<td width="78" style="font-size: 12px;">
											<font color="#CCCC66">■</font>#CCCC66</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#CCFF66">■</font>#CCFF66</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#CC0099">■</font>#CC0099</td>
										<td width="78" style="font-size: 12px;">
											<font color="#CC3399">■</font>#CC3399</td>
										<td width="78" style="font-size: 12px;">
											<font color="#CC6699">■</font>#CC6699</td>
										<td width="78" style="font-size: 12px;">
											<font color="#CC9999">■</font>#CC9999</td>
										<td width="78" style="font-size: 12px;">
											<font color="#CCCC99">■</font>#CCCC99</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#CCFF99">■</font>#CCFF99</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#CC00CC">■</font>#CC00CC</td>
										<td width="78" style="font-size: 12px;">
											<font color="#CC33CC">■</font>#CC33CC</td>
										<td width="78" style="font-size: 12px;">
											<font color="#CC66CC">■</font>#CC66CC</td>
										<td width="78" style="font-size: 12px;">
											<font color="#CC99CC">■</font>#CC99CC</td>
										<td width="78" style="font-size: 12px;">
											<font color="#CCCCCC">■</font>#CCCCCC</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#CCFFCC">■</font>#CCFFCC</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#CC00FF">■</font>#CC00FF</td>
										<td width="78" style="font-size: 12px;">
											<font color="#CC33FF">■</font>#CC33FF</td>
										<td width="78" style="font-size: 12px;">
											<font color="#CC66FF">■</font>#CC66FF</td>
										<td width="78" style="font-size: 12px;">
											<font color="#CC99FF">■</font>#CC99FF</td>
										<td width="78" style="font-size: 12px;">
											<font color="#CCCCFF">■</font>#CCCCFF</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#CCFFFF">■</font>#CCFFFF</td>
									</tr>
									<tr>
										<td width="468" colspan="6">
											<img src="ima_mp/common/shim.gif" width="468" height="5">
										</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#FF0000">■</font>#FF0000</td>
										<td width="78" style="font-size: 12px;">
											<font color="#FF3300">■</font>#FF3300</td>
										<td width="78" style="font-size: 12px;">
											<font color="#FF6600">■</font>#FF6600</td>
										<td width="78" style="font-size: 12px;">
											<font color="#FF9900">■</font>#FF9900</td>
										<td width="78" style="font-size: 12px;">
											<font color="#FFCC00">■</font>#FFCC00</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#FFFF00">■</font>#FFFF00</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#FF0033">■</font>#FF0033</td>
										<td width="78" style="font-size: 12px;">
											<font color="#FF3333">■</font>#FF3333</td>
										<td width="78" style="font-size: 12px;">
											<font color="#FF6633">■</font>#FF6633</td>
										<td width="78" style="font-size: 12px;">
											<font color="#FF9933">■</font>#FF9933</td>
										<td width="78" style="font-size: 12px;">
											<font color="#FFCC33">■</font>#FFCC33</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#FFFF33">■</font>#FFFF33</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#FF0066">■</font>#FF0066</td>
										<td width="78" style="font-size: 12px;">
											<font color="#FF3366">■</font>#FF3366</td>
										<td width="78" style="font-size: 12px;">
											<font color="#FF6666">■</font>#FF6666</td>
										<td width="78" style="font-size: 12px;">
											<font color="#FF9966">■</font>#FF9966</td>
										<td width="78" style="font-size: 12px;">
											<font color="#FFCC66">■</font>#FFCC66</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#FFFF66">■</font>#FFFF66</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#FF0099">■</font>#FF0099</td>
										<td width="78" style="font-size: 12px;">
											<font color="#FF3366">■</font>#FF3399</td>
										<td width="78" style="font-size: 12px;">
											<font color="#FF6699">■</font>#FF6699</td>
										<td width="78" style="font-size: 12px;">
											<font color="#FF9999">■</font>#FF9999</td>
										<td width="78" style="font-size: 12px;">
											<font color="#FFCC66">■</font>#FFCC99</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#FFFF99">■</font>#FFFF99</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#FF00CC">■</font>#FF00CC</td>
										<td width="78" style="font-size: 12px;">
											<font color="#FF33CC">■</font>#FF33CC</td>
										<td width="78" style="font-size: 12px;">
											<font color="#FF66CC">■</font>#FF66CC</td>
										<td width="78" style="font-size: 12px;">
											<font color="#FF99CC">■</font>#FF99CC</td>
										<td width="78" style="font-size: 12px;">
											<font color="#FFCCCC">■</font>#FFCCCC</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#FFFFCC">■</font>#FFFFCC</td>
									</tr>
									<tr>
										<td width="78" style="font-size: 12px;">
											<font color="#FF00FF">■</font>#FF00FF</td>
										<td width="78" style="font-size: 12px;">
											<font color="#FF33FF">■</font>#FF33FF</td>
										<td width="78" style="font-size: 12px;">
											<font color="#FF66FF">■</font>#FF66FF</td>
										<td width="78" style="font-size: 12px;">
											<font color="#FF99FF">■</font>#FF99FF</td>
										<td width="78" style="font-size: 12px;">
											<font color="#FFCCFF">■</font>#FFCCFF</td>
										<td style="font-size: 12px; width: 78px;">
											<font color="#FFFFFF">■</font>#FFFFFF</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
</asp:Content>
