﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using MobileLib.Pictograph;
using iBridCommLib;
using ViComm;

public partial class Site_PictographSample : System.Web.UI.Page {
	private DataTable DataSource {
		get { return this.ViewState["DataSource"] as DataTable; }
		set { this.ViewState["DataSource"] = value; }
	}

	protected void Page_Load(object sender, EventArgs e) {
		//Response.BufferOutput = false;
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender, EventArgs e) {
		this.GetList();
	}

	protected void dsPictograph_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pDocomoFlag"] = this.chkDocomo.Checked;
		e.InputParameters["pKddiFlag"] = this.chkKddi.Checked;
		e.InputParameters["pSoftbankFlag"] = this.chkSoftBank.Checked;
	}

	private void GetList() {
		this.grdPictograph.PageIndex = 0;
		this.grdPictograph.DataBind();
	}

	protected string GetAvailableMark(object pMaqiaCode, object pCarrierCode) {
		if (string.IsNullOrEmpty(iBridUtil.GetStringValue(pMaqiaCode))) {
			return string.Empty;
		}
		return string.IsNullOrEmpty(iBridUtil.GetStringValue(pCarrierCode)) ? "×" : "○";
	}
}
