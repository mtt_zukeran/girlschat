<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastRankingMainte.aspx.cs" Inherits="Site_CastRankingMainte"
	Title="ランキング修正" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="ランキング修正"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[検索条件]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<table border="0" style="width: 640px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle">
								サイトコード
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px" AutoPostBack="True"
									OnSelectedIndexChanged="lstSiteCd_SelectedIndexChanged">
								</asp:DropDownList>
							</td>
							<td class="tdHeaderStyle2">
								ランキングタイプ
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstRankingType" runat="server" DataSourceID="dsRankType" DataTextField="CODE_NM" DataValueField="CODE" Width="100px">
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle2">
								カテゴリ
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstActCategorySeq" runat="server" DataSourceID="dsActCategory" DataTextField="ACT_CATEGORY_NM" DataValueField="ACT_CATEGORY_SEQ"
									Width="180px">
								</asp:DropDownList>
							</td>
						</tr>
					</table>
					<asp:Button runat="server" ID="btnGetRanking" Text="ランキング取得" CssClass="seekbutton" OnClick="btnGetRanking_Click" />
				</asp:Panel>
			</fieldset>
			<asp:Panel runat="server" ID="pnlInfo">
				<fieldset class="fieldset">
					<legend>[ランキング]</legend>
					<asp:Panel runat="server" ID="pnlDtl" ScrollBars="auto" Height="500px">
						<asp:Button runat="server" ID="btnGetInfo" Text="出演者情報取得" CssClass="seekbutton" OnClick="btnGetInfo_Click" ValidationGroup="Detail" visible="false"/>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
						<asp:CustomValidator ID="vdcDuplicate" runat="server" ErrorMessage="出演者ＩＤが重複しています。" ValidationGroup="Detail" OnServerValidate="Duplicate_ServerValidate"></asp:CustomValidator>
						<br />
						<br />
						<table border="0" class="tableStyle" runat="server" ID="tblRanking">
							<tr runat="server" id="trHeader">
								<td class="tdHeaderSmallStyle" style="width: 35px">
									順位
								</td>
								<td class="tdHeaderSmallStyle">
									ID
								</td>
								<td class="tdHeaderSmallStyle" style="width: 35px">
									ｷｬﾗｸﾀｰNo
								</td>
								<td class="tdHeaderSmallStyle">
									<%= DisplayWordUtil.Replace("出演者名") %>
								</td>
								<td class="tdHeaderSmallStyle">
									ﾊﾝﾄﾞﾙ名
								</td>
								<td class="tdHeaderSmallStyle" align="center">
									写真
								</td>
								<td class="tdHeaderSmallStyle" align="center">
									<asp:Label ID="lblRankingTitle" runat="server" Text="Label"></asp:Label>
								</td>
								<td class="tdHeaderSmallStyle" align="center">
									ﾌﾟﾛﾌ
								</td>
								<td class="tdHeaderSmallStyle" align="center">
									ﾀﾞｲｼﾞｪｽﾄ
								</td>
								<td class="tdHeaderSmallStyle" align="center" width="40px">
									ﾓﾆﾀｰ
								</td>
							</tr>
							<tr runat="server" id="trRank00">
								<td class="tdDataStyle" style="width: 35px" align="center">
									1位
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLoginID00" runat="server" Width="70px"></asp:TextBox>
									<asp:CustomValidator ID="vdcLoginID00" runat="server" ControlToValidate="txtLoginID00" ErrorMessage="この出演者ＩＤは存在しません。" OnServerValidate="vdcLoginId_ServerValidate"
										ValidationGroup="Detail">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrLoginID00" runat="server" ErrorMessage="出演者ＩＤを入力して下さい。" ControlToValidate="txtLoginID00" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUserCharNo00" runat="server" MaxLength="2" Width="35px"></asp:TextBox>
									<asp:CustomValidator ID="vdcUserCharNo00" runat="server" ControlToValidate="txtUserCharNo00" Display="Dynamic" ErrorMessage="入力したｷｬﾗｸﾀｰ番号は存在しません。" ValidationGroup="Detail"
										OnServerValidate="vdcUserCharNo_ServerValidate">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrUserCharNo00" runat="server" ErrorMessage="ｷｬﾗｸﾀｰ番号を入力してください。" ControlToValidate="txtUserCharNo00" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeUserCharNo00" runat="server" ErrorMessage="ｷｬﾀｸﾀｰ番号は00〜99の間で入力してください" ControlToValidate="txtUserCharNo00" ValidationGroup="Detail"
										ValidationExpression="(\d\d)">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdDataStyle">
									<asp:HyperLink ID="lnkCastNm00" runat="server" Width="130px"></asp:HyperLink>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblHandleNm00" runat="server" Text="Label" Width="140px"></asp:Label>
								</td>
								<td class="tdDataStyle">
									<asp:Image ID="imgPIC00" runat="server" Width="40%" />
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblMin00" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblProfileMovie00" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblTalkMovie00" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="center">
									<asp:Label ID="lblMonitorEnable00" runat="server" Text="Label" Width="40px"></asp:Label>
								</td>
							</tr>
							<tr runat="server" id="trRank01">
								<td class="tdDataStyle" style="width: 35px" align="center">
									2位
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLoginID01" runat="server" Width="70px"></asp:TextBox>
									<asp:CustomValidator ID="vdcLoginID01" runat="server" ControlToValidate="txtLoginID01" ErrorMessage="この出演者ＩＤは存在しません。" OnServerValidate="vdcLoginId_ServerValidate"
										ValidationGroup="Detail">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrLoginID01" runat="server" ErrorMessage="出演者ＩＤを入力して下さい。" ControlToValidate="txtLoginID01" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUserCharNo01" runat="server" MaxLength="2" Width="35px"></asp:TextBox>
									<asp:CustomValidator ID="vdcUserCharNo01" runat="server" ControlToValidate="txtUserCharNo01" Display="Dynamic" ErrorMessage="入力したｷｬﾗｸﾀｰ番号は存在しません。" ValidationGroup="Detail"
										OnServerValidate="vdcUserCharNo_ServerValidate">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrUserCharNo01" runat="server" ErrorMessage="ｷｬﾗｸﾀｰ番号を入力してください。" ControlToValidate="txtUserCharNo01" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeUserCharNo01" runat="server" ErrorMessage="ｷｬﾀｸﾀｰ番号は00〜99の間で入力してください" ControlToValidate="txtUserCharNo01" ValidationGroup="Detail"
										ValidationExpression="(\d\d)">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdDataStyle">
									<asp:HyperLink ID="lnkCastNm01" runat="server" Width="130px"></asp:HyperLink>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblHandleNm01" runat="server" Text="Label" Width="140px"></asp:Label>
								</td>
								<td class="tdDataStyle">
									<asp:Image ID="imgPIC01" runat="server" Width="40%" />
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblMin01" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblProfileMovie01" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblTalkMovie01" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="center">
									<asp:Label ID="lblMonitorEnable01" runat="server" Text="Label" Width="40px"></asp:Label>
								</td>
							</tr>
							<tr runat="server" id="trRank02">
								<td class="tdDataStyle" style="width: 35px" align="center">
									3位
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLoginID02" runat="server" Width="70px"></asp:TextBox>
									<asp:CustomValidator ID="vdcLoginID02" runat="server" ControlToValidate="txtLoginID02" ErrorMessage="この出演者ＩＤは存在しません。" OnServerValidate="vdcLoginId_ServerValidate"
										ValidationGroup="Detail">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrLoginID02" runat="server" ErrorMessage="出演者ＩＤを入力して下さい。" ControlToValidate="txtLoginID02" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUserCharNo02" runat="server" MaxLength="2" Width="35px"></asp:TextBox>
									<asp:CustomValidator ID="vdcUserCharNo02" runat="server" ControlToValidate="txtUserCharNo02" Display="Dynamic" ErrorMessage="入力したｷｬﾗｸﾀｰ番号は存在しません。" ValidationGroup="Detail"
										OnServerValidate="vdcUserCharNo_ServerValidate">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrUserCharNo02" runat="server" ErrorMessage="ｷｬﾗｸﾀｰ番号を入力してください。" ControlToValidate="txtUserCharNo02" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeUserCharNo02" runat="server" ErrorMessage="ｷｬﾀｸﾀｰ番号は00〜99の間で入力してください" ControlToValidate="txtUserCharNo02" ValidationGroup="Detail"
										ValidationExpression="(\d\d)">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdDataStyle">
									<asp:HyperLink ID="lnkCastNm02" runat="server" Width="130px"></asp:HyperLink>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblHandleNm02" runat="server" Text="Label" Width="140px"></asp:Label>
								</td>
								<td class="tdDataStyle">
									<asp:Image ID="imgPIC02" runat="server" Width="40%" />
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblMin02" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblProfileMovie02" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblTalkMovie02" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="center">
									<asp:Label ID="lblMonitorEnable02" runat="server" Text="Label" Width="40px"></asp:Label>
								</td>
							</tr>
							<tr runat="server" id="trRank03">
								<td class="tdDataStyle" style="width: 35px" align="center">
									4位
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLoginID03" runat="server" Width="70px"></asp:TextBox>
									<asp:CustomValidator ID="vdcLoginID03" runat="server" ControlToValidate="txtLoginID03" ErrorMessage="この出演者ＩＤは存在しません。" OnServerValidate="vdcLoginId_ServerValidate"
										ValidationGroup="Detail">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrLoginID03" runat="server" ErrorMessage="出演者ＩＤを入力して下さい。" ControlToValidate="txtLoginID03" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUserCharNo03" runat="server" MaxLength="2" Width="35px"></asp:TextBox>
									<asp:CustomValidator ID="vdcUserCharNo03" runat="server" ControlToValidate="txtUserCharNo03" Display="Dynamic" ErrorMessage="入力したｷｬﾗｸﾀｰ番号は存在しません。" ValidationGroup="Detail"
										OnServerValidate="vdcUserCharNo_ServerValidate">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrUserCharNo03" runat="server" ErrorMessage="ｷｬﾗｸﾀｰ番号を入力してください。" ControlToValidate="txtUserCharNo03" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeUserCharNo03" runat="server" ErrorMessage="ｷｬﾀｸﾀｰ番号は00〜99の間で入力してください" ControlToValidate="txtUserCharNo03" ValidationGroup="Detail"
										ValidationExpression="(\d\d)">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdDataStyle">
									<asp:HyperLink ID="lnkCastNm03" runat="server" Width="130px"></asp:HyperLink>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblHandleNm03" runat="server" Text="Label" Width="140px"></asp:Label>
								</td>
								<td class="tdDataStyle">
									<asp:Image ID="imgPIC03" runat="server" Width="40%" />
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblMin03" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblProfileMovie03" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblTalkMovie03" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="center">
									<asp:Label ID="lblMonitorEnable03" runat="server" Text="Label" Width="40px"></asp:Label>
								</td>
							</tr>
							<tr runat="server" id="trRank04">
								<td class="tdDataStyle" style="width: 35px" align="center">
									5位
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLoginID04" runat="server" Width="70px"></asp:TextBox>
									<asp:CustomValidator ID="vdcLoginID04" runat="server" ControlToValidate="txtLoginID04" ErrorMessage="この出演者ＩＤは存在しません。" OnServerValidate="vdcLoginId_ServerValidate"
										ValidationGroup="Detail">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrLoginID04" runat="server" ErrorMessage="出演者ＩＤを入力して下さい。" ControlToValidate="txtLoginID04" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUserCharNo04" runat="server" MaxLength="2" Width="35px"></asp:TextBox>
									<asp:CustomValidator ID="vdcUserCharNo04" runat="server" ControlToValidate="txtUserCharNo04" Display="Dynamic" ErrorMessage="入力したｷｬﾗｸﾀｰ番号は存在しません。" ValidationGroup="Detail"
										OnServerValidate="vdcUserCharNo_ServerValidate">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrUserCharNo04" runat="server" ErrorMessage="ｷｬﾗｸﾀｰ番号を入力してください。" ControlToValidate="txtUserCharNo04" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeUserCharNo04" runat="server" ErrorMessage="ｷｬﾀｸﾀｰ番号は00〜99の間で入力してください" ControlToValidate="txtUserCharNo04" ValidationGroup="Detail"
										ValidationExpression="(\d\d)">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdDataStyle">
									<asp:HyperLink ID="lnkCastNm04" runat="server" Width="130px"></asp:HyperLink>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblHandleNm04" runat="server" Text="Label" Width="140px"></asp:Label>
								</td>
								<td class="tdDataStyle">
									<asp:Image ID="imgPIC04" runat="server" Width="40%" />
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblMin04" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblProfileMovie04" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblTalkMovie04" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="center">
									<asp:Label ID="lblMonitorEnable04" runat="server" Text="Label" Width="40px"></asp:Label>
								</td>
							</tr>
							<tr runat="server" id="trRank05">
								<td class="tdDataStyle" style="width: 35px" align="center">
									6位
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLoginID05" runat="server" Width="70px"></asp:TextBox>
									<asp:CustomValidator ID="vdcLoginID05" runat="server" ControlToValidate="txtLoginID05" ErrorMessage="この出演者ＩＤは存在しません。" OnServerValidate="vdcLoginId_ServerValidate"
										ValidationGroup="Detail">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrLoginID05" runat="server" ErrorMessage="出演者ＩＤを入力して下さい。" ControlToValidate="txtLoginID05" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUserCharNo05" runat="server" MaxLength="2" Width="35px"></asp:TextBox>
									<asp:CustomValidator ID="vdcUserCharNo05" runat="server" ControlToValidate="txtUserCharNo05" Display="Dynamic" ErrorMessage="入力したｷｬﾗｸﾀｰ番号は存在しません。" ValidationGroup="Detail"
										OnServerValidate="vdcUserCharNo_ServerValidate">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrUserCharNo05" runat="server" ErrorMessage="ｷｬﾗｸﾀｰ番号を入力してください。" ControlToValidate="txtUserCharNo05" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeUserCharNo05" runat="server" ErrorMessage="ｷｬﾀｸﾀｰ番号は00〜99の間で入力してください" ControlToValidate="txtUserCharNo05" ValidationGroup="Detail"
										ValidationExpression="(\d\d)">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdDataStyle">
									<asp:HyperLink ID="lnkCastNm05" runat="server" Width="130px"></asp:HyperLink>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblHandleNm05" runat="server" Text="Label" Width="140px"></asp:Label>
								</td>
								<td class="tdDataStyle">
									<asp:Image ID="imgPIC05" runat="server" Width="40%" />
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblMin05" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblProfileMovie05" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblTalkMovie05" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="center">
									<asp:Label ID="lblMonitorEnable05" runat="server" Text="Label" Width="40px"></asp:Label>
								</td>
							</tr>
							<tr runat="server" id="trRank06">
								<td class="tdDataStyle" style="width: 35px" align="center">
									7位
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLoginID06" runat="server" Width="70px"></asp:TextBox>
									<asp:CustomValidator ID="vdcLoginID06" runat="server" ControlToValidate="txtLoginID06" ErrorMessage="この出演者ＩＤは存在しません。" OnServerValidate="vdcLoginId_ServerValidate"
										ValidationGroup="Detail">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrLoginID06" runat="server" ErrorMessage="出演者ＩＤを入力して下さい。" ControlToValidate="txtLoginID06" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUserCharNo06" runat="server" MaxLength="2" Width="35px"></asp:TextBox>
									<asp:CustomValidator ID="vdcUserCharNo06" runat="server" ControlToValidate="txtUserCharNo06" Display="Dynamic" ErrorMessage="入力したｷｬﾗｸﾀｰ番号は存在しません。" ValidationGroup="Detail"
										OnServerValidate="vdcUserCharNo_ServerValidate">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrUserCharNo06" runat="server" ErrorMessage="ｷｬﾗｸﾀｰ番号を入力してください。" ControlToValidate="txtUserCharNo06" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeUserCharNo06" runat="server" ErrorMessage="ｷｬﾀｸﾀｰ番号は00〜99の間で入力してください" ControlToValidate="txtUserCharNo06" ValidationGroup="Detail"
										ValidationExpression="(\d\d)">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdDataStyle">
									<asp:HyperLink ID="lnkCastNm06" runat="server" Width="130px"></asp:HyperLink>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblHandleNm06" runat="server" Text="Label" Width="140px"></asp:Label>
								</td>
								<td class="tdDataStyle">
									<asp:Image ID="imgPIC06" runat="server" Width="40%" />
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblMin06" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblProfileMovie06" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblTalkMovie06" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="center">
									<asp:Label ID="lblMonitorEnable06" runat="server" Text="Label" Width="40px"></asp:Label>
								</td>
							</tr>
							<tr runat="server" id="trRank07">
								<td class="tdDataStyle" style="width: 35px" align="center">
									8位
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLoginID07" runat="server" Width="70px"></asp:TextBox>
									<asp:CustomValidator ID="vdcLoginID07" runat="server" ControlToValidate="txtLoginID07" ErrorMessage="この出演者ＩＤは存在しません。" OnServerValidate="vdcLoginId_ServerValidate"
										ValidationGroup="Detail">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrLoginID07" runat="server" ErrorMessage="出演者ＩＤを入力して下さい。" ControlToValidate="txtLoginID07" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUserCharNo07" runat="server" MaxLength="2" Width="35px"></asp:TextBox>
									<asp:CustomValidator ID="vdcUserCharNo07" runat="server" ControlToValidate="txtUserCharNo07" Display="Dynamic" ErrorMessage="入力したｷｬﾗｸﾀｰ番号は存在しません。" ValidationGroup="Detail"
										OnServerValidate="vdcUserCharNo_ServerValidate">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrUserCharNo07" runat="server" ErrorMessage="ｷｬﾗｸﾀｰ番号を入力してください。" ControlToValidate="txtUserCharNo07" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeUserCharNo07" runat="server" ErrorMessage="ｷｬﾀｸﾀｰ番号は00〜99の間で入力してください" ControlToValidate="txtUserCharNo07" ValidationGroup="Detail"
										ValidationExpression="(\d\d)">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdDataStyle">
									<asp:HyperLink ID="lnkCastNm07" runat="server" Width="130px"></asp:HyperLink>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblHandleNm07" runat="server" Text="Label" Width="140px"></asp:Label>
								</td>
								<td class="tdDataStyle">
									<asp:Image ID="imgPIC07" runat="server" Width="40%" />
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblMin07" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblProfileMovie07" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblTalkMovie07" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="center">
									<asp:Label ID="lblMonitorEnable07" runat="server" Text="Label" Width="40px"></asp:Label>
								</td>
							</tr>
							<tr runat="server" id="trRank08">
								<td class="tdDataStyle" style="width: 35px" align="center">
									9位
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLoginID08" runat="server" Width="70px"></asp:TextBox>
									<asp:CustomValidator ID="vdcLoginID08" runat="server" ControlToValidate="txtLoginID08" ErrorMessage="この出演者ＩＤは存在しません。" OnServerValidate="vdcLoginId_ServerValidate"
										ValidationGroup="Detail">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrLoginID08" runat="server" ErrorMessage="出演者ＩＤを入力して下さい。" ControlToValidate="txtLoginID08" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUserCharNo08" runat="server" MaxLength="2" Width="35px"></asp:TextBox>
									<asp:CustomValidator ID="vdcUserCharNo08" runat="server" ControlToValidate="txtUserCharNo08" Display="Dynamic" ErrorMessage="入力したｷｬﾗｸﾀｰ番号は存在しません。" ValidationGroup="Detail"
										OnServerValidate="vdcUserCharNo_ServerValidate">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrUserCharNo08" runat="server" ErrorMessage="ｷｬﾗｸﾀｰ番号を入力してください。" ControlToValidate="txtUserCharNo08" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeUserCharNo08" runat="server" ErrorMessage="ｷｬﾀｸﾀｰ番号は00〜99の間で入力してください" ControlToValidate="txtUserCharNo08" ValidationGroup="Detail"
										ValidationExpression="(\d\d)">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdDataStyle">
									<asp:HyperLink ID="lnkCastNm08" runat="server" Width="130px"></asp:HyperLink>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblHandleNm08" runat="server" Text="Label" Width="140px"></asp:Label>
								</td>
								<td class="tdDataStyle">
									<asp:Image ID="imgPIC08" runat="server" Width="40%" />
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblMin08" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblProfileMovie08" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblTalkMovie08" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="center">
									<asp:Label ID="lblMonitorEnable08" runat="server" Text="Label" Width="40px"></asp:Label>
								</td>
							</tr>
							<tr runat="server" id="trRank09">
								<td class="tdDataStyle" style="width: 35px" align="center">
									10位
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLoginID09" runat="server" Width="70px"></asp:TextBox>
									<asp:CustomValidator ID="vdcLoginID09" runat="server" ControlToValidate="txtLoginID09" ErrorMessage="この出演者ＩＤは存在しません。" OnServerValidate="vdcLoginId_ServerValidate"
										ValidationGroup="Detail">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrLoginID09" runat="server" ErrorMessage="出演者ＩＤを入力して下さい。" ControlToValidate="txtLoginID09" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUserCharNo09" runat="server" MaxLength="2" Width="35px"></asp:TextBox>
									<asp:CustomValidator ID="vdcUserCharNo09" runat="server" ControlToValidate="txtUserCharNo09" Display="Dynamic" ErrorMessage="入力したｷｬﾗｸﾀｰ番号は存在しません。" ValidationGroup="Detail"
										OnServerValidate="vdcUserCharNo_ServerValidate">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrUserCharNo09" runat="server" ErrorMessage="ｷｬﾗｸﾀｰ番号を入力してください。" ControlToValidate="txtUserCharNo09" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeUserCharNo09" runat="server" ErrorMessage="ｷｬﾀｸﾀｰ番号は00〜99の間で入力してください" ControlToValidate="txtUserCharNo09" ValidationGroup="Detail"
										ValidationExpression="(\d\d)">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdDataStyle">
									<asp:HyperLink ID="lnkCastNm09" runat="server" Width="130px"></asp:HyperLink>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblHandleNm09" runat="server" Text="Label" Width="140px"></asp:Label>
								</td>
								<td class="tdDataStyle">
									<asp:Image ID="imgPIC09" runat="server" Width="40%" />
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblMin09" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblProfileMovie09" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblTalkMovie09" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="center">
									<asp:Label ID="lblMonitorEnable09" runat="server" Text="Label" Width="40px"></asp:Label>
								</td>
							</tr>
							<tr runat="server" id="trRank10">
								<td class="tdDataStyle" style="width: 35px" align="center">
									11位
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLoginID10" runat="server" Width="70px"></asp:TextBox>
									<asp:CustomValidator ID="vdcLoginID10" runat="server" ControlToValidate="txtLoginID10" ErrorMessage="この出演者ＩＤは存在しません。" OnServerValidate="vdcLoginId_ServerValidate"
										ValidationGroup="Detail">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrLoginID10" runat="server" ErrorMessage="出演者ＩＤを入力して下さい。" ControlToValidate="txtLoginID10" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUserCharNo10" runat="server" MaxLength="2" Width="35px"></asp:TextBox>
									<asp:CustomValidator ID="vdcUserCharNo10" runat="server" ControlToValidate="txtUserCharNo10" Display="Dynamic" ErrorMessage="入力したｷｬﾗｸﾀｰ番号は存在しません。" ValidationGroup="Detail"
										OnServerValidate="vdcUserCharNo_ServerValidate">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrUserCharNo10" runat="server" ErrorMessage="ｷｬﾗｸﾀｰ番号を入力してください。" ControlToValidate="txtUserCharNo10" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeUserCharNo10" runat="server" ErrorMessage="ｷｬﾀｸﾀｰ番号は00〜99の間で入力してください" ControlToValidate="txtUserCharNo10" ValidationGroup="Detail"
										ValidationExpression="(\d\d)">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdDataStyle">
									<asp:HyperLink ID="lnkCastNm10" runat="server" Width="130px"></asp:HyperLink>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblHandleNm10" runat="server" Text="Label" Width="140px"></asp:Label>
								</td>
								<td class="tdDataStyle">
									<asp:Image ID="imgPIC10" runat="server" Width="40%" />
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblMin10" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblProfileMovie10" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblTalkMovie10" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="center">
									<asp:Label ID="lblMonitorEnable10" runat="server" Text="Label" Width="40px"></asp:Label>
								</td>
							</tr>
							<tr runat="server" id="trRank11">
								<td class="tdDataStyle" style="width: 35px" align="center">
									12位
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLoginID11" runat="server" Width="70px"></asp:TextBox>
									<asp:CustomValidator ID="vdcLoginID11" runat="server" ControlToValidate="txtLoginID11" ErrorMessage="この出演者ＩＤは存在しません。" OnServerValidate="vdcLoginId_ServerValidate"
										ValidationGroup="Detail">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrLoginID11" runat="server" ErrorMessage="出演者ＩＤを入力して下さい。" ControlToValidate="txtLoginID11" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUserCharNo11" runat="server" MaxLength="2" Width="35px"></asp:TextBox>
									<asp:CustomValidator ID="vdcUserCharNo11" runat="server" ControlToValidate="txtUserCharNo11" Display="Dynamic" ErrorMessage="入力したｷｬﾗｸﾀｰ番号は存在しません。" ValidationGroup="Detail"
										OnServerValidate="vdcUserCharNo_ServerValidate">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrUserCharNo11" runat="server" ErrorMessage="ｷｬﾗｸﾀｰ番号を入力してください。" ControlToValidate="txtUserCharNo11" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeUserCharNo11" runat="server" ErrorMessage="ｷｬﾀｸﾀｰ番号は00〜99の間で入力してください" ControlToValidate="txtUserCharNo11" ValidationGroup="Detail"
										ValidationExpression="(\d\d)">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdDataStyle">
									<asp:HyperLink ID="lnkCastNm11" runat="server" Width="130px"></asp:HyperLink>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblHandleNm11" runat="server" Text="Label" Width="140px"></asp:Label>
								</td>
								<td class="tdDataStyle">
									<asp:Image ID="imgPIC11" runat="server" Width="40%" />
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblMin11" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblProfileMovie11" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblTalkMovie11" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="center">
									<asp:Label ID="lblMonitorEnable11" runat="server" Text="Label" Width="40px"></asp:Label>
								</td>
							</tr>
							<tr runat="server" id="trRank12">
								<td class="tdDataStyle" style="width: 35px" align="center">
									13位
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLoginID12" runat="server" Width="70px"></asp:TextBox>
									<asp:CustomValidator ID="vdcLoginID12" runat="server" ControlToValidate="txtLoginID12" ErrorMessage="この出演者ＩＤは存在しません。" OnServerValidate="vdcLoginId_ServerValidate"
										ValidationGroup="Detail">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrLoginID12" runat="server" ErrorMessage="出演者ＩＤを入力して下さい。" ControlToValidate="txtLoginID12" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUserCharNo12" runat="server" MaxLength="2" Width="35px"></asp:TextBox>
									<asp:CustomValidator ID="vdcUserCharNo12" runat="server" ControlToValidate="txtUserCharNo12" Display="Dynamic" ErrorMessage="入力したｷｬﾗｸﾀｰ番号は存在しません。" ValidationGroup="Detail"
										OnServerValidate="vdcUserCharNo_ServerValidate">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrUserCharNo12" runat="server" ErrorMessage="ｷｬﾗｸﾀｰ番号を入力してください。" ControlToValidate="txtUserCharNo12" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeUserCharNo12" runat="server" ErrorMessage="ｷｬﾀｸﾀｰ番号は00〜99の間で入力してください" ControlToValidate="txtUserCharNo12" ValidationGroup="Detail"
										ValidationExpression="(\d\d)">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdDataStyle">
									<asp:HyperLink ID="lnkCastNm12" runat="server" Width="130px"></asp:HyperLink>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblHandleNm12" runat="server" Text="Label" Width="140px"></asp:Label>
								</td>
								<td class="tdDataStyle">
									<asp:Image ID="imgPIC12" runat="server" Width="40%" />
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblMin12" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblProfileMovie12" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblTalkMovie12" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="center">
									<asp:Label ID="lblMonitorEnable12" runat="server" Text="Label" Width="40px"></asp:Label>
								</td>
							</tr>
							<tr runat="server" id="trRank13">
								<td class="tdDataStyle" style="width: 35px" align="center">
									14位
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLoginID13" runat="server" Width="70px"></asp:TextBox>
									<asp:CustomValidator ID="vdcLoginID13" runat="server" ControlToValidate="txtLoginID13" ErrorMessage="この出演者ＩＤは存在しません。" OnServerValidate="vdcLoginId_ServerValidate"
										ValidationGroup="Detail">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrLoginID13" runat="server" ErrorMessage="出演者ＩＤを入力して下さい。" ControlToValidate="txtLoginID13" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUserCharNo13" runat="server" MaxLength="2" Width="35px"></asp:TextBox>
									<asp:CustomValidator ID="vdcUserCharNo13" runat="server" ControlToValidate="txtUserCharNo13" Display="Dynamic" ErrorMessage="入力したｷｬﾗｸﾀｰ番号は存在しません。" ValidationGroup="Detail"
										OnServerValidate="vdcUserCharNo_ServerValidate">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrUserCharNo13" runat="server" ErrorMessage="ｷｬﾗｸﾀｰ番号を入力してください。" ControlToValidate="txtUserCharNo13" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeUserCharNo13" runat="server" ErrorMessage="ｷｬﾀｸﾀｰ番号は00〜99の間で入力してください" ControlToValidate="txtUserCharNo13" ValidationGroup="Detail"
										ValidationExpression="(\d\d)">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdDataStyle">
									<asp:HyperLink ID="lnkCastNm13" runat="server" Width="130px"></asp:HyperLink>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblHandleNm13" runat="server" Text="Label" Width="140px"></asp:Label>
								</td>
								<td class="tdDataStyle">
									<asp:Image ID="imgPIC13" runat="server" Width="40%" />
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblMin13" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblProfileMovie13" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblTalkMovie13" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="center">
									<asp:Label ID="lblMonitorEnable13" runat="server" Text="Label" Width="40px"></asp:Label>
								</td>
							</tr>
							<tr runat="server" id="trRank14">
								<td class="tdDataStyle" style="width: 35px" align="center">
									15位
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLoginID14" runat="server" Width="70px"></asp:TextBox>
									<asp:CustomValidator ID="vdcLoginID14" runat="server" ControlToValidate="txtLoginID14" ErrorMessage="この出演者ＩＤは存在しません。" OnServerValidate="vdcLoginId_ServerValidate"
										ValidationGroup="Detail">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrLoginID14" runat="server" ErrorMessage="出演者ＩＤを入力して下さい。" ControlToValidate="txtLoginID14" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUserCharNo14" runat="server" MaxLength="2" Width="35px"></asp:TextBox>
									<asp:CustomValidator ID="vdcUserCharNo14" runat="server" ControlToValidate="txtUserCharNo14" Display="Dynamic" ErrorMessage="入力したｷｬﾗｸﾀｰ番号は存在しません。" ValidationGroup="Detail"
										OnServerValidate="vdcUserCharNo_ServerValidate">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrUserCharNo14" runat="server" ErrorMessage="ｷｬﾗｸﾀｰ番号を入力してください。" ControlToValidate="txtUserCharNo14" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeUserCharNo14" runat="server" ErrorMessage="ｷｬﾀｸﾀｰ番号は00〜99の間で入力してください" ControlToValidate="txtUserCharNo14" ValidationGroup="Detail"
										ValidationExpression="(\d\d)">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdDataStyle">
									<asp:HyperLink ID="lnkCastNm14" runat="server" Width="130px"></asp:HyperLink>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblHandleNm14" runat="server" Text="Label" Width="140px"></asp:Label>
								</td>
								<td class="tdDataStyle">
									<asp:Image ID="imgPIC14" runat="server" Width="40%" />
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblMin14" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblProfileMovie14" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblTalkMovie14" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="center">
									<asp:Label ID="lblMonitorEnable14" runat="server" Text="Label" Width="40px"></asp:Label>
								</td>
							</tr>
							<tr runat="server" id="trRank15">
								<td class="tdDataStyle" style="width: 35px" align="center">
									16位
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLoginID15" runat="server" Width="70px"></asp:TextBox>
									<asp:CustomValidator ID="vdcLoginID15" runat="server" ControlToValidate="txtLoginID15" ErrorMessage="この出演者ＩＤは存在しません。" OnServerValidate="vdcLoginId_ServerValidate"
										ValidationGroup="Detail">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrLoginID15" runat="server" ErrorMessage="出演者ＩＤを入力して下さい。" ControlToValidate="txtLoginID15" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUserCharNo15" runat="server" MaxLength="2" Width="35px"></asp:TextBox>
									<asp:CustomValidator ID="vdcUserCharNo15" runat="server" ControlToValidate="txtUserCharNo15" Display="Dynamic" ErrorMessage="入力したｷｬﾗｸﾀｰ番号は存在しません。" ValidationGroup="Detail"
										OnServerValidate="vdcUserCharNo_ServerValidate">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrUserCharNo15" runat="server" ErrorMessage="ｷｬﾗｸﾀｰ番号を入力してください。" ControlToValidate="txtUserCharNo15" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeUserCharNo15" runat="server" ErrorMessage="ｷｬﾀｸﾀｰ番号は00〜99の間で入力してください" ControlToValidate="txtUserCharNo15" ValidationGroup="Detail"
										ValidationExpression="(\d\d)">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdDataStyle">
									<asp:HyperLink ID="lnkCastNm15" runat="server" Width="130px"></asp:HyperLink>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblHandleNm15" runat="server" Text="Label" Width="140px"></asp:Label>
								</td>
								<td class="tdDataStyle">
									<asp:Image ID="imgPIC15" runat="server" Width="40%" />
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblMin15" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblProfileMovie15" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblTalkMovie15" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="center">
									<asp:Label ID="lblMonitorEnable15" runat="server" Text="Label" Width="40px"></asp:Label>
								</td>
							</tr>
							<tr runat="server" id="trRank16">
								<td class="tdDataStyle" style="width: 35px" align="center">
									17位
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLoginID16" runat="server" Width="70px"></asp:TextBox>
									<asp:CustomValidator ID="vdcLoginID16" runat="server" ControlToValidate="txtLoginID16" ErrorMessage="この出演者ＩＤは存在しません。" OnServerValidate="vdcLoginId_ServerValidate"
										ValidationGroup="Detail">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrLoginID16" runat="server" ErrorMessage="出演者ＩＤを入力して下さい。" ControlToValidate="txtLoginID16" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUserCharNo16" runat="server" MaxLength="2" Width="35px"></asp:TextBox>
									<asp:CustomValidator ID="vdcUserCharNo16" runat="server" ControlToValidate="txtUserCharNo16" Display="Dynamic" ErrorMessage="入力したｷｬﾗｸﾀｰ番号は存在しません。" ValidationGroup="Detail"
										OnServerValidate="vdcUserCharNo_ServerValidate">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrUserCharNo16" runat="server" ErrorMessage="ｷｬﾗｸﾀｰ番号を入力してください。" ControlToValidate="txtUserCharNo16" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeUserCharNo16" runat="server" ErrorMessage="ｷｬﾀｸﾀｰ番号は00〜99の間で入力してください" ControlToValidate="txtUserCharNo16" ValidationGroup="Detail"
										ValidationExpression="(\d\d)">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdDataStyle">
									<asp:HyperLink ID="lnkCastNm16" runat="server" Width="130px"></asp:HyperLink>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblHandleNm16" runat="server" Text="Label" Width="140px"></asp:Label>
								</td>
								<td class="tdDataStyle">
									<asp:Image ID="imgPIC16" runat="server" Width="40%" />
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblMin16" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblProfileMovie16" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblTalkMovie16" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="center">
									<asp:Label ID="lblMonitorEnable16" runat="server" Text="Label" Width="40px"></asp:Label>
								</td>
							</tr>
							<tr runat="server" id="trRank17">
								<td class="tdDataStyle" style="width: 35px" align="center">
									18位
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLoginID17" runat="server" Width="70px"></asp:TextBox>
									<asp:CustomValidator ID="vdcLoginID17" runat="server" ControlToValidate="txtLoginID17" ErrorMessage="この出演者ＩＤは存在しません。" OnServerValidate="vdcLoginId_ServerValidate"
										ValidationGroup="Detail">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrLoginID17" runat="server" ErrorMessage="出演者ＩＤを入力して下さい。" ControlToValidate="txtLoginID17" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUserCharNo17" runat="server" MaxLength="2" Width="35px"></asp:TextBox>
									<asp:CustomValidator ID="vdcUserCharNo17" runat="server" ControlToValidate="txtUserCharNo17" Display="Dynamic" ErrorMessage="入力したｷｬﾗｸﾀｰ番号は存在しません。" ValidationGroup="Detail"
										OnServerValidate="vdcUserCharNo_ServerValidate">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrUserCharNo17" runat="server" ErrorMessage="ｷｬﾗｸﾀｰ番号を入力してください。" ControlToValidate="txtUserCharNo17" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeUserCharNo17" runat="server" ErrorMessage="ｷｬﾀｸﾀｰ番号は00〜99の間で入力してください" ControlToValidate="txtUserCharNo17" ValidationGroup="Detail"
										ValidationExpression="(\d\d)">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdDataStyle">
									<asp:HyperLink ID="lnkCastNm17" runat="server" Width="130px"></asp:HyperLink>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblHandleNm17" runat="server" Text="Label" Width="140px"></asp:Label>
								</td>
								<td class="tdDataStyle">
									<asp:Image ID="imgPIC17" runat="server" Width="40%" />
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblMin17" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblProfileMovie17" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblTalkMovie17" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="center">
									<asp:Label ID="lblMonitorEnable17" runat="server" Text="Label" Width="40px"></asp:Label>
								</td>
							</tr>
							<tr runat="server" id="trRank18">
								<td class="tdDataStyle" style="width: 35px" align="center">
									19位
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLoginID18" runat="server" Width="70px"></asp:TextBox>
									<asp:CustomValidator ID="vdcLoginID18" runat="server" ControlToValidate="txtLoginID18" ErrorMessage="この出演者ＩＤは存在しません。" OnServerValidate="vdcLoginId_ServerValidate"
										ValidationGroup="Detail">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrLoginID18" runat="server" ErrorMessage="出演者ＩＤを入力して下さい。" ControlToValidate="txtLoginID18" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUserCharNo18" runat="server" MaxLength="2" Width="35px"></asp:TextBox>
									<asp:CustomValidator ID="vdcUserCharNo18" runat="server" ControlToValidate="txtUserCharNo18" Display="Dynamic" ErrorMessage="入力したｷｬﾗｸﾀｰ番号は存在しません。" ValidationGroup="Detail"
										OnServerValidate="vdcUserCharNo_ServerValidate">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrUserCharNo18" runat="server" ErrorMessage="ｷｬﾗｸﾀｰ番号を入力してください。" ControlToValidate="txtUserCharNo18" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeUserCharNo18" runat="server" ErrorMessage="ｷｬﾀｸﾀｰ番号は00〜99の間で入力してください" ControlToValidate="txtUserCharNo18" ValidationGroup="Detail"
										ValidationExpression="(\d\d)">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdDataStyle">
									<asp:HyperLink ID="lnkCastNm18" runat="server" Width="130px"></asp:HyperLink>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblHandleNm18" runat="server" Text="Label" Width="140px"></asp:Label>
								</td>
								<td class="tdDataStyle">
									<asp:Image ID="imgPIC18" runat="server" Width="40%" />
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblMin18" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblProfileMovie18" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblTalkMovie18" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="center">
									<asp:Label ID="lblMonitorEnable18" runat="server" Text="Label" Width="40px"></asp:Label>
								</td>
							</tr>
							<tr runat="server" id="trRank19">
								<td class="tdDataStyle" style="width: 35px" align="center">
									20位
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLoginID19" runat="server" Width="70px"></asp:TextBox>
									<asp:CustomValidator ID="vdcLoginID19" runat="server" ControlToValidate="txtLoginID19" ErrorMessage="この出演者ＩＤは存在しません。" OnServerValidate="vdcLoginId_ServerValidate"
										ValidationGroup="Detail">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrLoginID19" runat="server" ErrorMessage="出演者ＩＤを入力して下さい。" ControlToValidate="txtLoginID19" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUserCharNo19" runat="server" MaxLength="2" Width="35px"></asp:TextBox>
									<asp:CustomValidator ID="vdcUserCharNo19" runat="server" ControlToValidate="txtUserCharNo19" Display="Dynamic" ErrorMessage="入力したｷｬﾗｸﾀｰ番号は存在しません。" ValidationGroup="Detail"
										OnServerValidate="vdcUserCharNo_ServerValidate">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrUserCharNo19" runat="server" ErrorMessage="ｷｬﾗｸﾀｰ番号を入力してください。" ControlToValidate="txtUserCharNo19" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeUserCharNo19" runat="server" ErrorMessage="ｷｬﾀｸﾀｰ番号は00〜99の間で入力してください" ControlToValidate="txtUserCharNo19" ValidationGroup="Detail"
										ValidationExpression="(\d\d)">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdDataStyle">
									<asp:HyperLink ID="lnkCastNm19" runat="server" Width="130px"></asp:HyperLink>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblHandleNm19" runat="server" Text="Label" Width="140px"></asp:Label>
								</td>
								<td class="tdDataStyle">
									<asp:Image ID="imgPIC19" runat="server" Width="40%" />
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblMin19" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblProfileMovie19" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="right">
									<asp:Label ID="lblTalkMovie19" runat="server" Text="Label" Width="60px"></asp:Label>
								</td>
								<td class="tdDataStyle" align="center">
									<asp:Label ID="lblMonitorEnable19" runat="server" Text="Label" Width="40px"></asp:Label>
								</td>
							</tr>
						</table>
					</asp:Panel>
				</fieldset>
			</asp:Panel>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsRankType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="63" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsActCategory" runat="server" SelectMethod="GetList" TypeName="ActCategory" OnSelecting="dsActCategory_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdcLoginID00" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdcLoginID01" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdcLoginID02" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdcLoginID03" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdcLoginID04" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdcLoginID05" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vdcLoginID06" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8" TargetControlID="vdcLoginID07" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9" TargetControlID="vdcLoginID08" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10" TargetControlID="vdcLoginID09" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender21" TargetControlID="vdcLoginID10" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender22" TargetControlID="vdcLoginID11" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender23" TargetControlID="vdcLoginID12" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender24" TargetControlID="vdcLoginID13" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender25" TargetControlID="vdcLoginID14" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender26" TargetControlID="vdcLoginID15" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender27" TargetControlID="vdcLoginID16" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender28" TargetControlID="vdcLoginID17" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender29" TargetControlID="vdcLoginID18" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender30" TargetControlID="vdcLoginID19" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11" TargetControlID="vdrLoginID00" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender12" TargetControlID="vdrLoginID01" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender13" TargetControlID="vdrLoginID02" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender14" TargetControlID="vdrLoginID03" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender15" TargetControlID="vdrLoginID04" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender16" TargetControlID="vdrLoginID05" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender17" TargetControlID="vdrLoginID06" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender18" TargetControlID="vdrLoginID07" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender19" TargetControlID="vdrLoginID08" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender20" TargetControlID="vdrLoginID09" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender31" TargetControlID="vdrLoginID10" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender32" TargetControlID="vdrLoginID11" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender33" TargetControlID="vdrLoginID12" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender34" TargetControlID="vdrLoginID13" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender35" TargetControlID="vdrLoginID14" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender36" TargetControlID="vdrLoginID15" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender37" TargetControlID="vdrLoginID16" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender38" TargetControlID="vdrLoginID17" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender39" TargetControlID="vdrLoginID18" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender40" TargetControlID="vdrLoginID19" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender41" TargetControlID="vdrUserCharNo00" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender42" TargetControlID="vdrUserCharNo01" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender43" TargetControlID="vdrUserCharNo02" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender44" TargetControlID="vdrUserCharNo03" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender45" TargetControlID="vdrUserCharNo04" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender46" TargetControlID="vdrUserCharNo05" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender47" TargetControlID="vdrUserCharNo06" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender48" TargetControlID="vdrUserCharNo07" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender49" TargetControlID="vdrUserCharNo08" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender50" TargetControlID="vdrUserCharNo09" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender51" TargetControlID="vdrUserCharNo10" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender52" TargetControlID="vdrUserCharNo11" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender53" TargetControlID="vdrUserCharNo12" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender54" TargetControlID="vdrUserCharNo13" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender55" TargetControlID="vdrUserCharNo14" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender56" TargetControlID="vdrUserCharNo15" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender57" TargetControlID="vdrUserCharNo16" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender58" TargetControlID="vdrUserCharNo17" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender59" TargetControlID="vdrUserCharNo18" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender60" TargetControlID="vdrUserCharNo19" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender61" TargetControlID="vdeUserCharNo00" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender62" TargetControlID="vdeUserCharNo01" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender63" TargetControlID="vdeUserCharNo02" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender64" TargetControlID="vdeUserCharNo03" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender65" TargetControlID="vdeUserCharNo04" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender66" TargetControlID="vdeUserCharNo05" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender67" TargetControlID="vdeUserCharNo06" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender68" TargetControlID="vdeUserCharNo07" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender69" TargetControlID="vdeUserCharNo08" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender70" TargetControlID="vdeUserCharNo09" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender71" TargetControlID="vdeUserCharNo10" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender72" TargetControlID="vdeUserCharNo11" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender73" TargetControlID="vdeUserCharNo12" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender74" TargetControlID="vdeUserCharNo13" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender75" TargetControlID="vdeUserCharNo14" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender76" TargetControlID="vdeUserCharNo15" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender77" TargetControlID="vdeUserCharNo16" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender78" TargetControlID="vdeUserCharNo17" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender79" TargetControlID="vdeUserCharNo18" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender80" TargetControlID="vdeUserCharNo19" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender81" TargetControlID="vdcUserCharNo00" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender82" TargetControlID="vdcUserCharNo01" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender83" TargetControlID="vdcUserCharNo02" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender84" TargetControlID="vdcUserCharNo03" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender85" TargetControlID="vdcUserCharNo04" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender86" TargetControlID="vdcUserCharNo05" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender87" TargetControlID="vdcUserCharNo06" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender88" TargetControlID="vdcUserCharNo07" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender89" TargetControlID="vdcUserCharNo08" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender90" TargetControlID="vdcUserCharNo09" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender91" TargetControlID="vdcUserCharNo10" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender92" TargetControlID="vdcUserCharNo11" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender93" TargetControlID="vdcUserCharNo12" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender94" TargetControlID="vdcUserCharNo13" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender95" TargetControlID="vdcUserCharNo14" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender96" TargetControlID="vdcUserCharNo15" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender97" TargetControlID="vdcUserCharNo16" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender98" TargetControlID="vdcUserCharNo17" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender99" TargetControlID="vdcUserCharNo18" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender100" TargetControlID="vdcUserCharNo19" HighlightCssClass="validatorCallout" />
</asp:Content>
