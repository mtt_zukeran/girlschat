<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ProductionList.aspx.cs" Inherits="Site_ProductionList" Title="プロダクション設定" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="プロダクション設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[プロダクション情報]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					<asp:Label ID="lblProductionSeq" runat="server" Text="" Visible="false"></asp:Label>
					<table border="0" style="width: 640px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle">
								プロダクション名
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtProductionNm" runat="server" MaxLength="50" Width="400px"></asp:TextBox>
								<asp:RequiredFieldValidator ID="vdrProductionNm" runat="server" ErrorMessage="プロダクション名を入力して下さい。" ControlToValidate="txtProductionNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								電話番号
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtTel" runat="server" MaxLength="11" Width="110px"></asp:TextBox>
								<asp:RequiredFieldValidator ID="vdrTel" runat="server" ErrorMessage="電話番号を入力して下さい。" ControlToValidate="txtTel" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								<asp:RegularExpressionValidator ID="vdeTel" runat="server" ErrorMessage="電話番号をを正しく入力して下さい。" ValidationExpression="(0\d{9,10})" ControlToValidate="txtTel" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle2">
								メールアドレス
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtEmailAddr" runat="server" MaxLength="100" Width="200px"></asp:TextBox>
								<asp:RequiredFieldValidator ID="vdrEmailAddr" runat="server" ErrorMessage="メールアドレスを入力して下さい。" ControlToValidate="txtEmailAddr" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								<asp:RegularExpressionValidator ID="vdeEmailAddr" runat="server" ErrorMessage="メールアドレスをを正しく入力して下さい。" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtEmailAddr"
									ValidationGroup="Detail">*</asp:RegularExpressionValidator>
							</td>
						</tr>
					</table>
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
					<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
					<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[プロダクション一覧]</legend>
			<asp:GridView ID="grdProduction" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsProduction" AllowSorting="True" SkinID="GridViewColor">
				<Columns>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkProductionSeq" runat="server" Text='<%# Eval("PRODUCTION_NM") %>' CommandArgument='<%# Eval("PRODUCTION_SEQ") %>' OnCommand="lnkProductionSeq_Command" CausesValidation="False">
							</asp:LinkButton>
						</ItemTemplate>
						<HeaderTemplate>
							プロダクション
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:BoundField DataField="TEL" HeaderText="電話番号"></asp:BoundField>
					<asp:BoundField DataField="EMAIL_ADDR" HeaderText="メールアドレス"></asp:BoundField>
					<asp:BoundField DataField="UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:HyperLink ID="lnkBusinessReport" runat="server" NavigateUrl='<%# string.Format("~/Site/ProductionReportList.aspx?productionseq={0}",Eval("PRODUCTION_SEQ"))%>'>編集</asp:HyperLink>
						</ItemTemplate>
						<HeaderTemplate>
							業務連絡
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Center" />
					</asp:TemplateField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<a class="reccount">Record Count
				<%#GetRecCount() %>
			</a>
			<br>
			<a class="reccount">Current viewing page
				<%=grdProduction.PageIndex + 1%>
				of
				<%=grdProduction.PageCount%>
			</a>
			<div class="button">
				<asp:Button ID="btnRegist" runat="server" Text="プロダクション追加" OnClick="btnRegist_Click" />
			</div>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsProduction" runat="server" SelectMethod="GetPageCollection" TypeName="Production" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelected="dsProduction_Selected"></asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrProductionNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrTel" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdeTel" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdrEmailAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdeEmailAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
