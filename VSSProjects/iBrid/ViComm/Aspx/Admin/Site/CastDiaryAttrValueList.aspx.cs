﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者日記属性区分メンテナンス
--	Progaram ID		: CastDiaryAttrValueList
--
--  Creation Date	: 2011.08.29
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;

public partial class Site_CastDiaryAttrValueList : System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_SITE_OWNER)) {
			throw new ApplicationException("ViComm権限違反");
		}
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
	}

    protected void dsCastDiaryAttrValue_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void FirstLoad() {
        grdCastDiaryAttrValue.PageSize = 999;
	}

	private void InitPage() {
		lstSiteCd.SelectedIndex = 0;
        lblCastDiaryAttrSeq.Text = "";
		ClearField();
		pnlMainte.Visible = false;
		lstSeekSiteCd.DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		DataBind();

		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);

	}

	private void ClearField() {
        txtCastDiaryAttrNm.Text = string.Empty;
		txtPriority.Text = string.Empty;
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		lstSiteCd.SelectedIndex = lstSeekSiteCd.SelectedIndex;
        lblCastDiaryAttrSeq.Text = "0";
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

    protected void lnkCastDiaryAttrSeq_Command(object sender, CommandEventArgs e) {
        lblCastDiaryAttrSeq.Text = e.CommandArgument.ToString();
		GetData();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	private void GetList() {
        grdCastDiaryAttrValue.PageIndex = 0;
        grdCastDiaryAttrValue.DataBind();
		pnlCount.DataBind();
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_DIARY_ATTR_VALUE_GET");
            db.ProcedureInParm("PCAST_DIARY_ATTR_SEQ", DbSession.DbType.NUMBER, lblCastDiaryAttrSeq.Text);
			db.ProcedureOutParm("PSITE_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_DIARY_ATTR_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPRIORITY",DbSession.DbType.NUMBER);
            db.ProcedureOutParm("PROWID", DbSession.DbType.VARCHAR2);
            db.ProcedureOutParm("PREVISION_NO", DbSession.DbType.NUMBER);
            db.ProcedureOutParm("PRECORD_COUNT", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				lstSiteCd.SelectedValue = db.GetStringValue("PSITE_CD");
                txtCastDiaryAttrNm.Text = db.GetStringValue("PCAST_DIARY_ATTR_NM");
				txtPriority.Text = db.GetStringValue("PPRIORITY");
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;

	}

	private void UpdateData(int pDelFlag) {
		
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_DIARY_ATTR_VALUE_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
            db.ProcedureInParm("PCAST_DIARY_ATTR_SEQ", DbSession.DbType.NUMBER, lblCastDiaryAttrSeq.Text);
            db.ProcedureInParm("PCAST_DIARY_ATTR_NM", DbSession.DbType.VARCHAR2, txtCastDiaryAttrNm.Text);
			db.ProcedureInParm("PPRIORITY",DbSession.DbType.NUMBER,txtPriority.Text);
            db.ProcedureInParm("PROWID", DbSession.DbType.VARCHAR2, ViewState["ROWID"]);
            db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		int iIdx = lstSiteCd.SelectedIndex;
		InitPage();
		lstSeekSiteCd.SelectedIndex = iIdx;
		GetList();
	}

    protected void dsCastDiaryAttrValue_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
	}
}
