﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="UserChargeAddConditionList.aspx.cs" Inherits="Site_UserChargeAddConditionList"
    Title="ユーザー課金追加設定適応条件" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="ユーザー課金追加設定適応条件"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlMainte">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel runat="server" ID="pnlKey">
                    <fieldset class="fieldset-inner">
                        <table border="0" style="width: 640px" class="tableStyle">
                            <tr>
                                <td class="tdHeaderStyle">
                                    サイトコード
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                        DataValueField="SITE_CD" Width="206px" AutoPostBack="True" OnSelectedIndexChanged="lstSiteCd_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle2">
                                    設定コード
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstUserChargeAddCd" runat="server" Width="206px" DataSourceID="dsUserChargeAddCd"
                                        DataTextField="CODE_NM" DataValueField="CODE" OnDataBound="lst_DataBound">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="vdrUserChargeAddCd" runat="server" ErrorMessage="設定コードを選択して下さい。"
                                        ControlToValidate="lstUserChargeAddCd" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
                                        TargetControlID="vdrUserChargeAddCd" HighlightCssClass="validatorCallout" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlDtl">
                    <fieldset class="fieldset-inner">
                        <legend>[条件内容]</legend>
                        <table border="0" style="width: 640px; margin-bottom: 4px" class="tableStyle">
                            <tr>
                                <td class="tdHeaderStyle2" style="width: 200px">
                                    条件名
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtUserChargeAddCdNm" runat="server" MaxLength="80" Width="200px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vdrUserChargeAddCdNm" runat="server" ErrorMessage="条件名を入力して下さい。"
                                        ControlToValidate="txtUserChargeAddCdNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2"
                                        TargetControlID="vdrUserChargeAddCdNm" HighlightCssClass="validatorCallout" />
                                </td>
                            </tr>
                        </table>
                        <table border="0" style="width: 640px; margin-bottom: 4px" class="tableStyle">
                            <tr>
                                <td class="tdHeaderStyle" style="width: 200px">
                                    最終入金開始日時
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtLastReceiptDateStart" runat="server" MaxLength="16" Width="120px"></asp:TextBox>
                                    <ajaxToolkit:MaskedEditExtender ID="mskLastReceiptDateStart" runat="server" MaskType="DateTime"
                                        Mask="9999/99/99 99:99:99" UserDateFormat="YearMonthDay" UserTimeFormat="TwentyFourHour"
                                        ClearMaskOnLostFocus="true" TargetControlID="txtLastReceiptDateStart">
                                    </ajaxToolkit:MaskedEditExtender>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    最終入金終了日時
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtLastReceiptDateEnd" runat="server" MaxLength="16" Width="120px"></asp:TextBox>
                                    <ajaxToolkit:MaskedEditExtender ID="mskLastReceiptDateEnd" runat="server" MaskType="DateTime"
                                        Mask="9999/99/99 99:99:99" UserDateFormat="YearMonthDay" UserTimeFormat="TwentyFourHour"
                                        ClearMaskOnLostFocus="true" TargetControlID="txtLastReceiptDateEnd">
                                    </ajaxToolkit:MaskedEditExtender>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle2">
                                    最終入金条件利用ﾌﾗｸﾞ
                                </td>
                                <td class="tdDataStyle">
                                    <asp:CheckBox ID="chkLastReceiptUseFlag" runat="server" />
                                </td>
                            </tr>
                        </table>
                        <table border="0" style="width: 640px; margin-bottom: 4px" class="tableStyle">
                            <tr>
                                <td class="tdHeaderStyle" style="width: 200px">
                                    入金総額集計開始日時
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtReceiptSumStartDate" runat="server" MaxLength="16" Width="120px"></asp:TextBox>
                                    <ajaxToolkit:MaskedEditExtender ID="mskReceiptSumStartDate" runat="server" MaskType="DateTime"
                                        Mask="9999/99/99 99:99:99" UserDateFormat="YearMonthDay" UserTimeFormat="TwentyFourHour"
                                        ClearMaskOnLostFocus="true" TargetControlID="txtReceiptSumStartDate">
                                    </ajaxToolkit:MaskedEditExtender>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    入金総額最小(日付指定)
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtReceiptSumSpecifiedMin" runat="server" MaxLength="8" Width="100px"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                        Enabled="true" FilterType="Numbers" TargetControlID="txtReceiptSumSpecifiedMin" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    入金総額最大(日付指定)
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtReceiptSumSpecifiedMax" runat="server" MaxLength="8" Width="100px"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                                        Enabled="true" FilterType="Numbers" TargetControlID="txtReceiptSumSpecifiedMax" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    ﾎﾟｲﾝﾄｱﾌﾘを含む(日付指定)
                                </td>
                                <td class="tdDataStyle">
                                    <asp:CheckBox ID="chkAfContainSpecifiedFlag" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle2">
                                    入金総額条件利用ﾌﾗｸﾞ(日付指定)
                                </td>
                                <td class="tdDataStyle">
                                    <asp:CheckBox ID="chkReceiptSumSpecifyUseFlag" runat="server" />
                                </td>
                            </tr>
                        </table>
                        <table border="0" style="width: 640px; margin-bottom: 4px" class="tableStyle">
                            <tr>
                                <td class="tdHeaderStyle" style="width: 200px">
                                    入金総額最小
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtReceiptSumMin" runat="server" MaxLength="8" Width="100px"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
                                        Enabled="true" FilterType="Numbers" TargetControlID="txtReceiptSumMin" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    入金総額最大
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtReceiptSumMax" runat="server" MaxLength="8" Width="100px"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
                                        Enabled="true" FilterType="Numbers" TargetControlID="txtReceiptSumMax" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    ﾎﾟｲﾝﾄｱﾌﾘを含む
                                </td>
                                <td class="tdDataStyle">
                                    <asp:CheckBox ID="chkAfContainFlag" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle2">
                                    入金総額条件利用ﾌﾗｸﾞ
                                </td>
                                <td class="tdDataStyle">
                                    <asp:CheckBox ID="chkReceiptSumUseFlag" runat="server" />
                                </td>
                            </tr>
                        </table>
                        <asp:CustomValidator ID="vdcAll" runat="server" ErrorMessage="" OnServerValidate="vdcAll_ServerValidate"
                            ValidationGroup="Detail"></asp:CustomValidator>
                        <asp:Panel ID="pnlButton" runat="server">
                            <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click"
                                ValidationGroup="Detail" />
                            <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click"
                                ValidationGroup="Key" />
                            <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click"
                                CausesValidation="False" />
                        </asp:Panel>
                    </fieldset>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[検索条件]</legend>
                <table border="0" style="width: 640px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle2">
                            サイトコード
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="180px" AutoPostBack="True" OnSelectedIndexChanged="lstSeekSiteCd_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    CausesValidation="False" />
                <asp:Button runat="server" ID="btnRegist" Text="追加" CssClass="seekbutton" CausesValidation="False"
                    OnClick="btnRegist_Click" />
                <%--<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />--%>
                <asp:Button ID="btnBack" runat="server" Text="戻る" CssClass="seekbutton" OnClick="btnBack_Click" />
            </fieldset>
            <fieldset>
                <legend>[条件一覧]</legend>
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="502px">
                    <asp:Label ID="lblNotice" runat="server" Text="※金額が 0 、日付が空 の場合は指定無しとなります。"></asp:Label>
                    <asp:Label ID="lblNaError" runat="server" Text="<br>複数の条件を有効にすることは出来ません。" ForeColor="red"></asp:Label>
                    <asp:Label ID="lblUseFlagError" runat="server" ForeColor="red" Text="<br>利用ﾌﾗｸﾞが全てOFFになっている条件を有効にすることは出来ません。"></asp:Label>
                    <asp:GridView ID="grdUserChargeAddCondition" runat="server" AutoGenerateColumns="False"
                        DataSourceID="dsUserChargeAddCondition" AllowPaging="true" AllowSorting="true"
                        Font-Size="X-Small" SkinID="GridViewColor">
                        <Columns>
                            <asp:TemplateField HeaderText="設定ｺｰﾄﾞ">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkUserChargeAddCd" runat="server" OnCommand="lnkUserChargeAddCd_Command"
                                        CommandArgument='<%# Eval("USER_CHARGE_ADD_CD") %>' Text='<%# Eval("USER_CHARGE_ADD_CD") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="条件名">
                                <ItemTemplate>
                                    <%--<asp:HyperLink ID="lnkUserChargeAddCondition" runat="server" NavigateUrl='<%# string.Format("~/Site/UserChargeAddList.aspx?sitecd={0}&sitenm={1}&actcategoryseq={2}&actcategorynm={3}&tempcd={4}&tempnm={5}",Eval("SITE_CD"),Eval("SITE_NM"),Eval("ACT_CATEGORY_SEQ"),Eval("ACT_CATEGORY_NM"),Eval("CAST_CHARGE_TEMP_CD"),Eval("CAST_CHARGE_TEMP_NM")) %>'
                                        Text='<%# Eval("USER_CHARGE_ADD_CD_NM") %>' />--%>
                                    <%--<asp:HyperLink ID="lnkUserChargeAddCondition" runat="server" NavigateUrl='<%# string.Format("~/Site/UserChargeAddConditionList.aspx?sitecd={0}&userrank={1}&sitenm={2}&userranknm={3}",Eval("SITE_CD"),Eval("USER_RANK"),Eval("SITE_NM"),Eval("USER_RANK_NM"))%>'
                                        Text='<%# Eval("USER_CHARGE_ADD_CD_NM") %>' />--%>
                                    <asp:LinkButton ID="lnkUserChargeAdd" runat="server" OnCommand="lnkUserChargeAdd_Command"
                                        CommandArgument='<%# string.Format("~/Site/UserChargeAddList.aspx?sitecd={0}&sitenm={1}&tempcd={2}&tempcdnm={3}",Eval("SITE_CD"),Eval("SITE_NM"),Eval("USER_CHARGE_ADD_CD"),Eval("USER_CHARGE_ADD_CD_NM"))%>'
                                        Text='<%# Eval("USER_CHARGE_ADD_CD_NM") %>' />
                                    <%--<asp:HyperLink ID="lnkUserChargeAddCondition" runat="server" NavigateUrl='<%# string.Format("~/Site/UserChargeAddList.aspx?sitecd={0}&sitenm={1}",Eval("SITE_CD"),Eval("SITE_NM"))%>'
                                        Text='<%# Eval("USER_CHARGE_ADD_CD_NM") %>' />--%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="150px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="最終入金日時">
                                <ItemTemplate>
                                    <asp:Label ID="lblLastReceiptDate" runat="server" Text='<%# string.Format("{0:yy/MM/dd HH:mm} - {1:yy/MM/dd HH:mm}",Eval("LAST_RECEIPT_DATE_START"),Eval("LAST_RECEIPT_DATE_END")) %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <%--                            <asp:BoundField DataField="LAST_RECEIPT_DATE_START" HeaderText="最終入金開始日時" DataFormatString="{0:yy/MM/dd HH:mm:ss}">
                                <ItemStyle HorizontalAlign="Center" Width="140px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="LAST_RECEIPT_DATE_END" HeaderText="最終入金終了日時" DataFormatString="{0:yy/MM/dd HH:mm:ss}">
                                <ItemStyle HorizontalAlign="Center" Width="140px" />
                            </asp:BoundField>
--%>
                            <asp:TemplateField HeaderText="最終入金<br>利用ﾌﾗｸﾞ">
                                <ItemTemplate>
                                    <asp:Label ID="lblLastReceiptUseFlag" runat="server" Text='<%# ViCommConst.FLAG_ON_STR.Equals(Eval("LAST_RECEIPT_USE_FLAG","{0}")) ? "○" : string.Empty %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="RECEIPT_SUM_START_DATE" HeaderText="入金総額(日付)開始" DataFormatString="{0:yy/MM/dd HH:mm}">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="入金総額(日付)">
                                <ItemTemplate>
                                    <asp:Label ID="lblReceiptSumSpecified" runat="server" Text='<%# string.Format("{0:N0} - {1:N0}",Eval("RECEIPT_SUM_SPECIFIED_MIN"),Eval("RECEIPT_SUM_SPECIFIED_MAX")) %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <%--                           <asp:BoundField DataField="RECEIPT_SUM_SPECIFIED_MIN" HeaderText="総額集計最小" DataFormatString="{0:N0}">
                                <ItemStyle HorizontalAlign="Right" Width="140px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="RECEIPT_SUM_SPECIFIED_MAX" HeaderText="総額集計最大" DataFormatString="{0:N0}">
                                <ItemStyle HorizontalAlign="Right" Width="140px" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="ﾎﾟｲﾝﾄｱﾌﾘ">
                                <ItemTemplate>
                                    <asp:Label ID="lblAfContainSpecifiedFlag" runat="server" Text='<%# ViCommConst.FLAG_ON_STR.Equals(Eval("AF_CONTAIN_SPECIFIED_FLAG","{0}")) ? "○" : string.Empty %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
--%>
                            <asp:TemplateField HeaderText="入金総額(日付)<br>利用ﾌﾗｸﾞ">
                                <ItemTemplate>
                                    <asp:Label ID="lblReceiptSumSpecifyFlag" runat="server" Text='<%# ViCommConst.FLAG_ON_STR.Equals(Eval("RECEIPT_SUM_SPECIFIED_USE_FLAG","{0}")) ? "○" : string.Empty %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="入金総額">
                                <ItemTemplate>
                                    <asp:Label ID="lblReceiptSum" runat="server" Text='<%# string.Format("{0:N0} - {1:N0}",Eval("RECEIPT_SUM_MIN"),Eval("RECEIPT_SUM_MAX")) %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <%--                            <asp:BoundField DataField="RECEIPT_SUM_MIN" HeaderText="入金総額最小" DataFormatString="{0:N0}">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="RECEIPT_SUM_MAX" HeaderText="入金集計最大" DataFormatString="{0:N0}">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="ﾎﾟｲﾝﾄｱﾌﾘ利用">
                                <ItemTemplate>
                                    <asp:Label ID="lblAfContainFlag" runat="server" Text='<%# ViCommConst.FLAG_ON_STR.Equals(Eval("AF_CONTAIN_FLAG","{0}")) ? "○" : string.Empty %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
--%>
                            <asp:TemplateField HeaderText="入金総額<br>利用ﾌﾗｸﾞ">
                                <ItemTemplate>
                                    <asp:Label ID="lblReceiptSumFlag" runat="server" Text='<%# ViCommConst.FLAG_ON_STR.Equals(Eval("RECEIPT_SUM_USE_FLAG","{0}")) ? "○" : string.Empty %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="有効">
                                <ItemTemplate>
                                    <asp:Label ID="lblNaFlag" runat="server" Text='<%# ViCommConst.FLAG_OFF_STR.Equals(Eval("NA_FLAG","{0}")) ? "○" : string.Empty %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="APPLICATION_DATE" DataFormatString="{0:yy/MM/dd HH:mm:ss}"
                                HeaderText="適用日時">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Button ID="btnActivate" runat="server" CausesValidation="false" CommandArgument='<%# string.Format("{0},0",Eval("USER_CHARGE_ADD_CD")) %>'
                                        OnClientClick='return confirm("この条件を有効にしますか？");' OnCommand="btnActivate_Command"
                                        Text="有効" Visible='<%# ViCommConst.FLAG_ON_STR.Equals(Eval("NA_FLAG","{0}")) %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Button ID="btnNotActivate" runat="server" CausesValidation="false" CommandArgument='<%# string.Format("{0},1",Eval("USER_CHARGE_ADD_CD")) %>'
                                        OnClientClick='return confirm("この条件を無効にしますか？");' OnCommand="btnActivate_Command"
                                        Text="解除" Visible='<%# ViCommConst.FLAG_OFF_STR.Equals(Eval("NA_FLAG","{0}")) %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsActCategory" runat="server" SelectMethod="GetList" TypeName="ActCategory">
        <SelectParameters>
            <asp:QueryStringParameter Name="pSiteCd" QueryStringField="sitecd" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsUserChargeAddCondition" runat="server" SelectMethod="GetList"
        TypeName="UserChargeAddCondition">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSeekSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsUserChargeAddCd" runat="server" SelectMethod="GetList"
        TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="A5" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete"
        ConfirmText="削除を行ないますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
