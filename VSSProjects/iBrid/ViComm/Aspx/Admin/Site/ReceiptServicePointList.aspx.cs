﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 入金残高サービスポイント
--	Progaram ID		: ReceiptServicePointList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Site_ReceiptServicePointList:System.Web.UI.Page {
	private string recCount = "";
	private string settleType = "";
	private string receiptAmt;

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
	}

	private void FirstLoad() {
		grdReceiptServicePoint.PageSize = 999;
		lblSiteCd.Text = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		lblSiteNm.Text = iBridUtil.GetStringValue(Request.QueryString["sitenm"]);
		lblUserRank.Text = iBridUtil.GetStringValue(Request.QueryString["userrank"]);
		lblUserRankNm.Text = iBridUtil.GetStringValue(Request.QueryString["userranknm"]);
		settleType = iBridUtil.GetStringValue(Request.QueryString["settle"]);
		receiptAmt = iBridUtil.GetStringValue(Request.QueryString["amt"]);
		if (lstSettleType.Items.Count > 0) {
			lstSettleType.SelectedIndex = 0;
		}
		DataBind();
	}

	private void InitPage() {
		pnlDtl.Visible = false;
		ClearField();
		if (!IsPostBack) {
			SysPrograms.SetupFromToDayTime(lstFromYYYY,lstFromMM,lstFromDD,lstFromHH,lstToYYYY,lstToMM,lstToDD,lstToHH,false);
			lstFromYYYY.SelectedIndex = 0;
			lstToYYYY.SelectedIndex = 0;
			lstFromMM.SelectedValue = DateTime.Now.ToString("MM");
			lstToMM.SelectedValue = DateTime.Now.ToString("MM");
			lstFromDD.SelectedValue = DateTime.Now.ToString("dd");
			lstToDD.SelectedValue = DateTime.Now.ToString("dd");
			lstFromHH.SelectedIndex = 0;
			lstToHH.SelectedIndex = 0;
		}
		if (!settleType.Equals("")) {
			lstSettleType.SelectedValue = settleType;
			txtReceiptAmt.Text = receiptAmt;
			GetData();
		}
	}

	private void ClearField() {
		txtAddRate.Text = "";
		txtAddRate0.Text = "";
		txtAddRate1.Text = "";
		txtAddRate2.Text = "";
		txtAddRateCampain.Text = "";
		vdcAll.Text = "";
		chkPaymentDelayFlag.Checked = false;
		chkAddPointFlag.Checked = false;
		chkCampainApplicatonFlag.Checked = false;
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetList();
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
		GetList();
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		pnlDtl.Visible = false;
		GetList();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	private void GetList() {
		grdReceiptServicePoint.PageIndex = 0;
		grdReceiptServicePoint.DataBind();
		pnlCount.DataBind();
		pnlKey.Enabled = true;
	}

	protected void dsReceiptServicePoint_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsReceiptServicePoint_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
	}

	protected void vdcAll_ServerValidate(object source,ServerValidateEventArgs args) {
		if (IsValid) {
			if (chkCampainApplicatonFlag.Checked) {
				DateTime dtFrom,dtTo;
				if (!DateTime.TryParse(lstFromYYYY.Text + "/" + lstFromMM.SelectedValue + "/" + lstFromDD.SelectedValue + " " + lstFromHH.SelectedValue + ":00:00",out dtFrom)) {
					dtFrom = DateTime.MinValue;
				}
				if (!DateTime.TryParse(lstToYYYY.Text + "/" + lstToMM.SelectedValue + "/" + lstToDD.SelectedValue + " " + lstToHH.SelectedValue + ":00:00",out dtTo)) {
					dtTo = DateTime.MinValue;
				}
				if (dtFrom >= dtTo) {
					vdcAll.Text = "適用日の大小関係が不正です。";
					args.IsValid = false;
				}
				if (args.IsValid) {
					int iRate;
					int.TryParse(txtAddRateCampain.Text, out iRate);
					if (iRate == 0) {
						vdcAll.Text = "加算率(ｷｬﾝﾍﾟｰﾝ) が０に設定されています。";
						args.IsValid = false;
					}
				}
			}
		}
	}


	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("RECEIPT_SERVICE_POINT_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lblSiteCd.Text);
			db.ProcedureInParm("PUSER_RANK",DbSession.DbType.VARCHAR2,lblUserRank.Text);
			db.ProcedureInParm("PSETTLE_TYPE",DbSession.DbType.VARCHAR2,lstSettleType.SelectedValue);
			db.ProcedureInParm("PRECEIPT_AMT",DbSession.DbType.NUMBER,int.Parse(txtReceiptAmt.Text));
			db.ProcedureOutParm("PADD_RATE0",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PADD_RATE1",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PADD_RATE2",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PADD_RATE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PADD_RATE_CAMPAIN",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PAPPLICATION_START_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PAPPLICATION_END_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAMPAIN_APPLICATION_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PPAYMENT_DELAY_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PADD_POINT_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtAddRate.Text = db.GetStringValue("PADD_RATE");
				txtAddRate0.Text = db.GetStringValue("PADD_RATE0");
				txtAddRate1.Text = db.GetStringValue("PADD_RATE1");
				txtAddRate2.Text = db.GetStringValue("PADD_RATE2");
				txtAddRateCampain.Text = db.GetStringValue("PADD_RATE_CAMPAIN");

				string sFromDay = db.GetStringValue("PAPPLICATION_START_DATE");
				string sToDay = db.GetStringValue("PAPPLICATION_END_DATE");

				if (!sFromDay.Equals(string.Empty)) {
					lstFromYYYY.SelectedValue = sFromDay.Substring(0,4);
					lstFromMM.SelectedValue = sFromDay.Substring(4,2);
					lstFromDD.SelectedValue = sFromDay.Substring(6,2);
					lstFromHH.SelectedValue = sFromDay.Substring(8,2);
				}

				if (!sToDay.Equals(string.Empty)) {
					lstToYYYY.SelectedValue = sToDay.Substring(0,4);
					lstToMM.SelectedValue = sToDay.Substring(4,2);
					lstToDD.SelectedValue = sToDay.Substring(6,2);
					lstToHH.SelectedValue = sToDay.Substring(8,2);
				}

				chkPaymentDelayFlag.Checked = (db.GetIntValue("PPAYMENT_DELAY_FLAG") != 0);
				chkAddPointFlag.Checked = (db.GetIntValue("PADD_POINT_FLAG") != 0);
				chkCampainApplicatonFlag.Checked = (db.GetIntValue("PCAMPAIN_APPLICATION_FLAG") != 0);
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;
	}

	private void UpdateData(int pDelFlag) {
		DateTime dtFrom,dtTo;

		if (!DateTime.TryParse(lstFromYYYY.Text + "/" + lstFromMM.SelectedValue + "/" + lstFromDD.SelectedValue + " " + lstFromHH.SelectedValue + ":00:00",out dtFrom)) {
			dtFrom = DateTime.MinValue;
		}
		if (!DateTime.TryParse(lstToYYYY.Text + "/" + lstToMM.SelectedValue + "/" + lstToDD.SelectedValue + " " + lstToHH.SelectedValue + ":00:00",out dtTo)) {
			dtTo = DateTime.MinValue;
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("RECEIPT_SERVICE_POINT_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lblSiteCd.Text);
			db.ProcedureInParm("PUSER_RANK",DbSession.DbType.VARCHAR2,lblUserRank.Text);
			db.ProcedureInParm("PSETTLE_TYPE",DbSession.DbType.VARCHAR2,lstSettleType.SelectedValue);
			db.ProcedureInParm("PRECEIPT_AMT",DbSession.DbType.NUMBER,int.Parse(txtReceiptAmt.Text));
			db.ProcedureInParm("PADD_RATE0",DbSession.DbType.NUMBER,int.Parse(txtAddRate0.Text));
			db.ProcedureInParm("PADD_RATE1",DbSession.DbType.NUMBER,int.Parse(txtAddRate1.Text));
			db.ProcedureInParm("PADD_RATE2",DbSession.DbType.NUMBER,int.Parse(txtAddRate2.Text));
			db.ProcedureInParm("PADD_RATE",DbSession.DbType.NUMBER,int.Parse(txtAddRate.Text));
			db.ProcedureInParm("PADD_RATE_CAMPAIN",DbSession.DbType.NUMBER,int.Parse(txtAddRateCampain.Text));
			db.ProcedureInParm("PAPPLICATION_START_DATE",DbSession.DbType.DATE,dtFrom);
			db.ProcedureInParm("PAPPLICATION_END_DATE",DbSession.DbType.DATE,dtTo);
			db.ProcedureInParm("PCAMPAIN_APPLICATION_FLAG",DbSession.DbType.NUMBER,chkCampainApplicatonFlag.Checked);
			db.ProcedureInParm("PPAYMENT_DELAY_FLAG",DbSession.DbType.NUMBER,chkPaymentDelayFlag.Checked);
			db.ProcedureInParm("PADD_POINT_FLAG",DbSession.DbType.NUMBER,chkAddPointFlag.Checked);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,ViewState["REVISION_NO"].ToString());
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		InitPage();
		GetList();
	}

	protected void grdReceiptServicePoint_DataBound(object sender,EventArgs e) {
		SysPrograms.JoinLabelCells(grdReceiptServicePoint,1);
		SysPrograms.JoinLinkCells(grdReceiptServicePoint,0);
	}

	protected void grdReceiptServicePoint_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			if (DataBinder.Eval(e.Row.DataItem,"CAMPAIN_APPLICATION_FLAG").ToString().Equals(ViCommConst.FLAG_ON_STR)) {
				DateTime dtFrom,dtTo;
				if (!DateTime.TryParse(DataBinder.Eval(e.Row.DataItem,"APPLICATION_START_DATE").ToString(),out dtFrom)) {
					dtFrom = DateTime.MinValue;
				}
				if (!DateTime.TryParse(DataBinder.Eval(e.Row.DataItem,"APPLICATION_END_DATE").ToString(),out dtTo)) {
					dtTo = DateTime.MinValue;
				}

				if (dtFrom <= DateTime.Now && dtTo >= DateTime.Now) {
					for (int i = 2;i < 11;i++) {
						e.Row.Cells[i].BackColor = Color.LavenderBlush;
					}
				}
			}
		}
	}


	protected void btnCSV_Click(object sender,EventArgs e) {
		Response.AddHeader("Content-Disposition","attachment;filename=RECEIPT_SERVICE_POINT.CSV");
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		using (ReceiptServicePoint oService = new ReceiptServicePoint()) {
			DataSet ds = oService.GetCsvData(lstSeekSiteCd.SelectedValue);
			SetCsvData(ds);
			Response.End();
		}
	}

	public void SetCsvData(DataSet pDataSet) {

		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");
		string sDtl = "";
		DataRow dr = null;

		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];
			sDtl = string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}",
							dr["SITE_CD"].ToString(),
							dr["USER_RANK"].ToString(),
							dr["SETTLE_TYPE"].ToString(),
							dr["RECEIPT_AMT"].ToString(),
							dr["ADD_RATE0"].ToString(),
							dr["ADD_RATE1"].ToString(),
							dr["ADD_RATE2"].ToString(),
							dr["ADD_RATE"].ToString(),
							dr["ADD_RATE_CAMPAIN"].ToString(),
							dr["APPLICATION_START_DATE"].ToString(),
							dr["APPLICATION_END_DATE"].ToString(),
							dr["CAMPAIN_APPLICATION_FLAG"].ToString(),
							dr["PAYMENT_DELAY_FLAG"].ToString(),
							dr["ADD_POINT_FLAG"].ToString());
			sDtl = sDtl.Replace("\r","");
			sDtl = sDtl.Replace("\n","");
			sDtl += "\r\n";
			Response.BinaryWrite(encoding.GetBytes(sDtl));
		}
	}

	public string GetAddPointRule(object pAddPointFlag) {
		string sAddPointFlag = pAddPointFlag.ToString();
		if (sAddPointFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
			return "加算率";
		} else {
			return "加算ﾎﾟｲﾝﾄ";
		}
	}

}
