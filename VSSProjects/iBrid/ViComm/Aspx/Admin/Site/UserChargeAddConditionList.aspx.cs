﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 会員課金追加設定適応条件
--	Progaram ID		: UserChargeAddConditionList
--
--  Creation Date	: 2012.05.30
--  Creater			: iBrid
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;
using System.Text;

public partial class Site_UserChargeAddConditionList : System.Web.UI.Page {

	protected string SiteCd {
		get { return iBridUtil.GetStringValue(this.ViewState["SiteCd"]); }
		private set { this.ViewState["SiteCd"] = value; }
	}
	protected string UserChargeAddCd {
		get { return iBridUtil.GetStringValue(this.ViewState["UserChargeAddCd"]); }
		private set { this.ViewState["UserChargeAddCd"] = value; }
	}
	protected string NaFlag {
		get { return iBridUtil.GetStringValue(this.ViewState["NaFlag"]); }
		private set { this.ViewState["NaFlag"] = value; }
	}
	protected string ApplicationDate {
		get { return iBridUtil.GetStringValue(this.ViewState["ApplicationDate"]); }
		private set { this.ViewState["ApplicationDate"] = value; }
	}
	protected string RevisionNo {
		get { return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]); }
		private set { this.ViewState["RevisionNo"] = value; }
	}
	protected string UserRank {
		get { return iBridUtil.GetStringValue(this.ViewState["UserRank"]); }
		private set { this.ViewState["UserRank"] = value; }
	}
	protected string UserRankNm {
		get { return iBridUtil.GetStringValue(this.ViewState["UserRankNm"]); }
		private set { this.ViewState["UserRankNm"] = value; }
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		this.UserRank = iBridUtil.GetStringValue(Request.QueryString["userrank"]);
		this.UserRankNm = iBridUtil.GetStringValue(Request.QueryString["userranknm"]);

		if (!this.IsPostBack) {
			this.InitPage();
		}
	}

	protected void lstSeekSiteCd_SelectedIndexChanged(object sender,EventArgs e) {
	}

	protected void lstSiteCd_SelectedIndexChanged(object sender,EventArgs e) {
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (sender == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	protected void btnClear_Click(object sender,EventArgs e) {
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlMainte.Visible = false;
	}

	protected void btnBack_Click(object sender,EventArgs e) {
		this.Response.Redirect("~/Site/UserRankList.aspx");
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		this.SiteCd = this.lstSeekSiteCd.SelectedValue;
		this.GetList();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		this.lstSiteCd.DataBind();
		this.lstSiteCd.SelectedValue = this.lstSeekSiteCd.SelectedValue;
		this.ClearField();
		this.pnlMainte.Visible = true;
		this.pnlKey.Enabled = true;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.Update(ViCommConst.FLAG_OFF);
		this.pnlMainte.Visible = false;
		this.GetList();
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		this.Update(ViCommConst.FLAG_ON);
		this.pnlMainte.Visible = false;
		this.GetList();
	}

	protected void btnActivate_Command(object sender,CommandEventArgs e) {
		this.lblNaError.Visible = false;
		this.lblUseFlagError.Visible = false;

		string[] sArgs = iBridUtil.GetStringValue(e.CommandArgument).Split(',');

		if (ViCommConst.FLAG_OFF_STR.Equals(sArgs[1])) {
			int iNaCount = 0;
			int iUseFlag = 0;
			using (UserChargeAddCondition oCondition = new UserChargeAddCondition()) {
				iNaCount = oCondition.GetNaCount(this.SiteCd);
				iUseFlag = oCondition.GetUseFlagCount(this.SiteCd,sArgs[0]);
			}
			if (iNaCount > 0) {
				this.lblNaError.Visible = true;
				return;
			}
			if (iUseFlag == 0) {
				this.lblUseFlagError.Visible = true;
				return;
			}
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_USER_CHARGE_ADD_COND");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			db.ProcedureInParm("pUSER_CHARGE_ADD_CD",DbSession.DbType.VARCHAR2,sArgs[0]);
			db.ProcedureInParm("pNA_FLAG",DbSession.DbType.NUMBER,sArgs[1]);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		this.GetList();
	}

	protected void lnkUserChargeAddCd_Command(object sender,CommandEventArgs e) {
		string sArg = iBridUtil.GetStringValue(e.CommandArgument);

		this.ClearField();
		this.lstUserChargeAddCd.SelectedValue = sArg;
		this.GetData(this.SiteCd,sArg);
		this.pnlMainte.Visible = true;
		this.pnlKey.Enabled = false;
	}

	protected void lnkUserChargeAdd_Command(object sender,CommandEventArgs e) {
		string sArg = iBridUtil.GetStringValue(e.CommandArgument);

		this.Response.Redirect(string.Format("{0}&userrank={1}&userranknm={2}",sArg,this.UserRank,this.UserRankNm));
	}

	protected void vdcAll_ServerValidate(object source,ServerValidateEventArgs args) {
		StringBuilder oErrorMessage = new StringBuilder();

		DateTime? dtLastReceiptStart = SysPrograms.TryParseOrDafult(this.txtLastReceiptDateStart.Text,DateTime.MinValue);
		DateTime? dtLastReceiptEnd = SysPrograms.TryParseOrDafult(this.txtLastReceiptDateEnd.Text,DateTime.Parse("2050/12/31"));

		if (dtLastReceiptStart > dtLastReceiptEnd) {
			args.IsValid = false;
			oErrorMessage.Append("最終入金日時の大小関係が不正です。");
		}

		int iReceiptSumSpecifiedMin = int.MinValue;
		int iReceiptSumSpecifiedMax = int.MaxValue;
		if (int.TryParse(this.txtReceiptSumSpecifiedMin.Text.Trim(),out iReceiptSumSpecifiedMin)) {
			if (iReceiptSumSpecifiedMin == 0) {
				iReceiptSumSpecifiedMin = int.MinValue;
			}
		}
		if (int.TryParse(this.txtReceiptSumSpecifiedMax.Text.Trim(),out iReceiptSumSpecifiedMax)) {
			if (iReceiptSumSpecifiedMax == 0) {
				iReceiptSumSpecifiedMax = int.MaxValue;
			}
		}

		if (iReceiptSumSpecifiedMax < iReceiptSumSpecifiedMin) {
			args.IsValid = false;
			oErrorMessage.AppendFormat("{0}入金総額(日付指定)の大小関係が不正です。",oErrorMessage.Length > 0 ? "<br>" : string.Empty);
		}

		int iReceiptSumMin = int.MinValue;
		int iReceiptSumMax = int.MaxValue;
		if (int.TryParse(this.txtReceiptSumMin.Text.Trim(),out iReceiptSumMin)) {
			if (iReceiptSumMin == 0) {
				iReceiptSumMin = int.MinValue;
			}
		}
		if (int.TryParse(this.txtReceiptSumMax.Text.Trim(),out iReceiptSumMax)) {
			if (iReceiptSumMax == 0) {
				iReceiptSumMax = int.MaxValue;
			}
		}
		if (iReceiptSumMax < iReceiptSumMin) {
			args.IsValid = false;
			oErrorMessage.AppendFormat("{0}入金総額の大小関係が不正です。",oErrorMessage.Length > 0 ? "<br>" : string.Empty);
		}

		this.vdcAll.ErrorMessage = oErrorMessage.ToString();
	}

	private void ClearField() {
		this.lstUserChargeAddCd.SelectedValue = string.Empty;
		this.txtUserChargeAddCdNm.Text = string.Empty;
		this.txtLastReceiptDateStart.Text = string.Empty;
		this.txtLastReceiptDateEnd.Text = string.Empty;
		this.chkLastReceiptUseFlag.Checked = false;
		this.txtReceiptSumStartDate.Text = string.Empty;
		this.txtReceiptSumSpecifiedMin.Text = string.Empty;
		this.txtReceiptSumSpecifiedMax.Text = string.Empty;
		this.chkAfContainSpecifiedFlag.Checked = false;
		this.chkReceiptSumSpecifyUseFlag.Checked = false;
		this.txtReceiptSumMin.Text = string.Empty;
		this.txtReceiptSumMax.Text = string.Empty;
		this.chkAfContainFlag.Checked = false;
		this.chkReceiptSumUseFlag.Checked = false;

		this.NaFlag = string.Empty;
		this.ApplicationDate = string.Empty;
		this.RevisionNo = string.Empty;

		this.lblNaError.Visible = false;
		this.lblUseFlagError.Visible = false;
	}

	private void InitPage() {
		this.pnlMainte.Visible = false;

		this.lstSeekSiteCd.DataBind();
		this.lstUserChargeAddCd.DataBind();

		if (!string.IsNullOrEmpty(this.SiteCd)) {
			lstSeekSiteCd.SelectedValue = this.SiteCd;
		} else if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		this.ClearField();
	}

	private void GetList() {
		this.lblNaError.Visible = false;
		this.lblUseFlagError.Visible = false;
		this.grdUserChargeAddCondition.DataBind();
	}

	private void GetData(string pSiteCd,string pUserChargeAddCd) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("USER_CHARGE_ADD_CONDITION_GET");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_CHARGE_ADD_CD",DbSession.DbType.VARCHAR2,pUserChargeAddCd);
			db.ProcedureOutParm("pUSER_CHARGE_ADD_CD_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pLAST_RECEIPT_DATE_START",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pLAST_RECEIPT_DATE_END",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pLAST_RECEIPT_USE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pRECEIPT_SUM_START_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRECEIPT_SUM_SPECIFIED_MIN",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pRECEIPT_SUM_SPECIFIED_MAX",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pAF_CONTAIN_SPECIFIED_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pRECEIPT_SUM_SPECIFY_USE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pRECEIPT_SUM_MIN",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pRECEIPT_SUM_MAX",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pAF_CONTAIN_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pRECEIPT_SUM_USE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pNA_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pAPPLICATION_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			if (db.GetIntValue("pRECORD_COUNT") > 0) {
				this.txtUserChargeAddCdNm.Text = db.GetStringValue("pUSER_CHARGE_ADD_CD_NM");
				this.txtLastReceiptDateStart.Text = db.GetStringValue("pLAST_RECEIPT_DATE_START");
				this.txtLastReceiptDateEnd.Text = db.GetStringValue("pLAST_RECEIPT_DATE_END");
				this.chkLastReceiptUseFlag.Checked = db.GetStringValue("pLAST_RECEIPT_USE_FLAG").Equals(ViCommConst.FLAG_ON_STR);
				this.txtReceiptSumStartDate.Text = db.GetStringValue("pRECEIPT_SUM_START_DATE");
				this.txtReceiptSumSpecifiedMin.Text = db.GetStringValue("pRECEIPT_SUM_SPECIFIED_MIN");
				this.txtReceiptSumSpecifiedMax.Text = db.GetStringValue("pRECEIPT_SUM_SPECIFIED_MAX");
				this.chkAfContainSpecifiedFlag.Checked = db.GetStringValue("pAF_CONTAIN_SPECIFIED_FLAG").Equals(ViCommConst.FLAG_ON_STR);
				this.chkReceiptSumSpecifyUseFlag.Checked = db.GetStringValue("pRECEIPT_SUM_SPECIFY_USE_FLAG").Equals(ViCommConst.FLAG_ON_STR);
				this.txtReceiptSumMin.Text = db.GetStringValue("pRECEIPT_SUM_MIN");
				this.txtReceiptSumMax.Text = db.GetStringValue("pRECEIPT_SUM_MAX");
				this.chkAfContainFlag.Checked = db.GetStringValue("pAF_CONTAIN_FLAG").Equals(ViCommConst.FLAG_ON_STR);
				this.chkReceiptSumUseFlag.Checked = db.GetStringValue("pRECEIPT_SUM_USE_FLAG").Equals(ViCommConst.FLAG_ON_STR);

				this.NaFlag = db.GetStringValue("pNA_FLAG");
				this.ApplicationDate = db.GetStringValue("pAPPLICATION_DATE");
				this.RevisionNo = db.GetStringValue("pREVISION_NO");
			}
		}
	}

	private void Update(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("USER_CHARGE_ADD_CONDITION_MNT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			db.ProcedureInParm("pUSER_CHARGE_ADD_CD",DbSession.DbType.VARCHAR2,this.lstUserChargeAddCd.SelectedValue);
			db.ProcedureInParm("pUSER_CHARGE_ADD_CD_NM",DbSession.DbType.VARCHAR2,this.txtUserChargeAddCdNm.Text.Trim());
			db.ProcedureInParm("pLAST_RECEIPT_DATE_START",DbSession.DbType.VARCHAR2,this.txtLastReceiptDateStart.Text.Trim());
			db.ProcedureInParm("pLAST_RECEIPT_DATE_END",DbSession.DbType.VARCHAR2,this.txtLastReceiptDateEnd.Text.Trim());
			db.ProcedureInParm("pLAST_RECEIPT_USE_FLAG",DbSession.DbType.NUMBER,this.chkLastReceiptUseFlag.Checked ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF);
			db.ProcedureInParm("pRECEIPT_SUM_START_DATE",DbSession.DbType.VARCHAR2,this.txtReceiptSumStartDate.Text.Trim());
			db.ProcedureInParm("pRECEIPT_SUM_SPECIFIED_MIN",DbSession.DbType.NUMBER,this.txtReceiptSumSpecifiedMin.Text.Trim());
			db.ProcedureInParm("pRECEIPT_SUM_SPECIFIED_MAX",DbSession.DbType.NUMBER,this.txtReceiptSumSpecifiedMax.Text.Trim());
			db.ProcedureInParm("pAF_CONTAIN_SPECIFIED_FLAG",DbSession.DbType.NUMBER,this.chkAfContainSpecifiedFlag.Checked ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF);
			db.ProcedureInParm("pRECEIPT_SUM_SPECIFIED_USE_FLAG",DbSession.DbType.NUMBER,this.chkReceiptSumSpecifyUseFlag.Checked ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF);
			db.ProcedureInParm("pRECEIPT_SUM_MIN",DbSession.DbType.NUMBER,this.txtReceiptSumMin.Text.Trim());
			db.ProcedureInParm("pRECEIPT_SUM_MAX",DbSession.DbType.NUMBER,this.txtReceiptSumMax.Text.Trim());
			db.ProcedureInParm("pAF_CONTAIN_FLAG",DbSession.DbType.NUMBER,this.chkAfContainFlag.Checked ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF);
			db.ProcedureInParm("pRECEIPT_SUM_USE_FLAG",DbSession.DbType.NUMBER,this.chkReceiptSumUseFlag.Checked ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF);
			db.ProcedureInParm("pNA_FLAG",DbSession.DbType.NUMBER,this.NaFlag);
			db.ProcedureInParm("pAPPLICATION_DATE",DbSession.DbType.VARCHAR2,this.ApplicationDate);
			db.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			db.ProcedureInParm("pDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

	}
}
