﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ユーザービューメンテナンス
--	Progaram ID		: UserViewList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using iBridCommLib;
using ViComm;

public partial class Site_UserViewList:System.Web.UI.Page {
	#region Sorting

	private string SortExpression {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortExpression"]);
		}
		set {
			this.ViewState["SortExpression"] = value;
		}
	}

	private string SortDirection {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortDirection"]);
		}
		set {
			this.ViewState["SortDirection"] = value;
		}
	}
	#endregion
	
	private string recCount = "";
	private Stream filter;

	private bool DisablePinEdit {
		get {
			return (bool)this.ViewState["DisablePinEdit"];
		}
		set {
			this.ViewState["DisablePinEdit"] = value;
		}
	}

	private string HtmlDoc {
		get {
			if (this.DisablePinEdit) {
				return this.txtHtmlDocText.Text;
			} else {
				return this.txtHtmlDoc.Text;
			}
		}
		set {
			if (this.DisablePinEdit) {
				this.txtHtmlDocText.Text = value;
			} else {
				this.txtHtmlDoc.Text = value;
			}
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		filter = Response.Filter;
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		txtHtmlDoc.EditorDirectory = string.Format("http://{0}:{1}/pinEdit/editor/",Request.UrlReferrer.Host,Request.UrlReferrer.Port);
		txtHtmlDoc.Autofocus = true;
		txtSeekProgramId.Style.Add("ime-mode","disabled");

		if (!IsPostBack) {
			FirstLoad();
			InitPage();

			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["direct"])) && iBridUtil.GetStringValue(Request.QueryString["direct"]).Equals(ViCommConst.FLAG_ON_STR)) {
				string sProgramId = iBridUtil.GetStringValue(Request.QueryString["pgmid"]);
				string sUserAgentType = iBridUtil.GetStringValue(Request.QueryString["useragenttype"]);
				string sHtmlDocSeq = string.Empty;

				DataSet oDS;
				using (UserView oUserView = new UserView()) {
					oDS = oUserView.GetOneByProgramId(lstSeekSiteCd.SelectedValue,iBridUtil.GetStringValue(ViewState["PROGRAM_ROOT"]),sProgramId,sUserAgentType);
				}

				if (oDS.Tables[0].Rows.Count > 0) {
					sHtmlDocSeq = oDS.Tables[0].Rows[0]["HTML_DOC_SEQ"].ToString();
					sUserAgentType = oDS.Tables[0].Rows[0]["USER_AGENT_TYPE"].ToString();
					MainteDirect(sHtmlDocSeq,sProgramId,sUserAgentType);
				}
			}
		}
	}

	private void FirstLoad() {

		grdUserView.PageSize = 999;

		ViewState["PROGRAM_ROOT"] = iBridUtil.GetStringValue(Request.QueryString["pgmroot"]);
		this.Title = string.Format("{0}({1})",this.Title,iBridUtil.GetStringValue(ViewState["PROGRAM_ROOT"]).Contains("woman") ? "女性" : "男性");
		this.lblPgmTitle.Text = string.Format("{0}({1})",this.lblPgmTitle.Text,iBridUtil.GetStringValue(ViewState["PROGRAM_ROOT"]).Contains("woman") ? "女性" : "男性");

		string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		if (sSiteCd.Equals("")) {
			if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
				lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}
		} else {
			lstSeekSiteCd.SelectedValue = sSiteCd;
		}
		grdUserView.DataSourceID = "";
		DataBind();

		lstSeekUserAgentType.DataBind();
		lstSeekUserAgentType.Items.Insert(0,new ListItem("",""));
		lstSeekUserAgentType.DataSourceID = "";
		lstSeekUserAgentType.SelectedIndex = 0;

		lstSeekActCategorySeq.DataBind();
		lstSeekActCategorySeq.Items.Insert(0,new ListItem("",""));
		lstSeekActCategorySeq.DataSourceID = "";
		lstSeekActCategorySeq.SelectedIndex = 0;

		lstSeekAdGroupCd.DataBind();
		lstSeekAdGroupCd.Items.Insert(0, new ListItem(string.Empty, string.Empty));
		lstSeekAdGroupCd.DataSourceID = string.Empty;
		lstSeekAdGroupCd.SelectedIndex = 0;

		lstTableIndex.DataSourceID = "";

		using (ManageCompany oManageCompany = new ManageCompany()) {
			this.lnkVariableSample.Visible = oManageCompany.IsAvailableService(ViCommConst.RELEASE_NEW_VARIABLE);
			this.DisablePinEdit = oManageCompany.IsAvailableService(ViCommConst.RELEASE_DISABLE_PINEDIT,2);
			this.txtHtmlDoc.Visible = !this.DisablePinEdit;
			this.txtHtmlDocText.Visible = this.DisablePinEdit;
		}
	}

	private void InitPage() {
		lstSiteCd.Enabled = true;
		txtHtmlDocSubSeq.Text = "";
		lblHtmlDocSeq.Text = "";
		ClearField();
		pnlMainte.Visible = false;
		pnlSelect.Visible = false;
		grdSiteHtmlDoc.DataSourceID = "";
		int iCompare = string.Compare(Session["AdminType"].ToString(),ViCommConst.RIGHT_SITE_OWNER);
		btnCSV.Visible = (iCompare >= 0);
	}

	private void ClearField() {
		txtPageTile.Text = string.Empty;
		txtPageKeyword.Text = string.Empty;
		txtPageDescription.Text = string.Empty;
		txtCssFileNm1.Text = string.Empty;
		txtCssFileNm2.Text = string.Empty;
		txtCssFileNm3.Text = string.Empty;
		txtCssFileNm4.Text = string.Empty;
		txtCssFileNm5.Text = string.Empty;
		txtJavascriptFileNm1.Text = string.Empty;
		txtJavascriptFileNm2.Text = string.Empty;
		txtJavascriptFileNm3.Text = string.Empty;
		txtJavascriptFileNm4.Text = string.Empty;
		txtJavascriptFileNm5.Text = string.Empty;
		txtHtmlDocTitle.Text = string.Empty;
		chkProtectImageFlag.Checked = true;
		chkEmojiToolFlag.Checked = true;
		chkNoIndex.Checked = false;
		chkNoFollow.Checked = false;
		chkNoArchive.Checked = false;
		this.HtmlDoc = string.Empty;
		recCount = "0";
		lblErrorInputSizeOver.Visible = false;
		SortExpression = string.Empty;
		SortDirection = string.Empty;
	}

	protected string GetRecCount() {
		return recCount;
	}

	private void GetList() {
		grdUserView.PageIndex = 0;
		grdUserView.DataSourceID = "dsUserView";
		grdUserView.DataBind();
		pnlCount.DataBind();
		lblErrorInputSizeOver.Visible = false;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		pnlMainte.Visible = true;
		pnlKey.Enabled = true;
		pnlDtl.Visible = false;
		lstSiteCd.SelectedIndex = lstSeekSiteCd.SelectedIndex;
		lstSiteCd.Enabled = false;

		lstUserAgentType.DataBind();
		lstActCategorySeq.DataBind();
		lstAdGroupCd.DataBind();
		lstUserRank.DataBind();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			if (UpdateData(0)) {
				lblErrorInputSizeOver.Visible = false;
			} else {
				lblErrorInputSizeOver.Visible = true;
			}
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		EndEdit();
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnSeekCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	private void GetData() {
		SessionObjs userObjs = (SessionObjs)Session["objs"];
		userObjs.site.GetOne(lstSiteCd.SelectedValue);

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("USER_VIEW_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PPROGRAM_ROOT",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["PROGRAM_ROOT"]));
			db.ProcedureInParm("PPROGRAM_ID",DbSession.DbType.VARCHAR2,lstProgramId.SelectedValue);
			db.ProcedureInParm("PUSER_AGENT_TYPE",DbSession.DbType.VARCHAR2,lstUserAgentType.SelectedValue);
			db.ProcedureInParm("PAD_GROUP_CD",DbSession.DbType.VARCHAR2,lstAdGroupCd.SelectedValue);
			db.ProcedureInParm("PUSER_RANK",DbSession.DbType.VARCHAR2,lstUserRank.SelectedValue);
			db.ProcedureInParm("PACT_CATEGORY_SEQ",DbSession.DbType.NUMBER,lstActCategorySeq.SelectedValue);
			db.ProcedureInParm("PHTML_DOC_SUB_SEQ",DbSession.DbType.NUMBER,int.Parse(txtHtmlDocSubSeq.Text));
			db.ProcedureOutParm("PMIN_DOC_SUB_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPAGE_TITLE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPAGE_KEYWORD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPAGE_DESCRIPTION",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PTABLE_INDEX",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCSS_FILE_NM1",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pCSS_FILE_NM2",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pCSS_FILE_NM3",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pCSS_FILE_NM4",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pCSS_FILE_NM5",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pJAVASCRIPT_FILE_NM1",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pJAVASCRIPT_FILE_NM2",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pJAVASCRIPT_FILE_NM3",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pJAVASCRIPT_FILE_NM4",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pJAVASCRIPT_FILE_NM5",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pPROTECT_IMAGE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pEMOJI_TOOL_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pNOINDEX_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pNOFOLLOW_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pNOARCHIVE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PHTML_DOC_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHTML_DOC_TITLE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutArrayParm("PHTML_DOC",DbSession.DbType.VARCHAR2,SysConst.MAX_HTML_BLOCKS);
			db.ProcedureOutParm("PROWID_VIEW",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO_VIEW",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID_MANAGE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO_MANAGE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID_DOC",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO_DOC",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();

			ViewState["REVISION_NO_VIEW"] = db.GetStringValue("PREVISION_NO_VIEW");
			ViewState["ROWID_VIEW"] = db.GetStringValue("PROWID_VIEW");

			ViewState["REVISION_NO_MANAGE"] = db.GetStringValue("PREVISION_NO_MANAGE");
			ViewState["ROWID_MANAGE"] = db.GetStringValue("PROWID_MANAGE");

			ViewState["REVISION_NO_DOC"] = db.GetStringValue("PREVISION_NO_DOC");
			ViewState["ROWID_DOC"] = db.GetStringValue("PROWID_DOC");

			lblHtmlDocSeq.Text = db.GetStringValue("PHTML_DOC_SEQ");

			this.HtmlDoc = string.Empty;
			txtPageTile.Text = string.Empty;
			txtPageKeyword.Text = string.Empty;
			txtPageDescription.Text = string.Empty;
			txtCssFileNm1.Text = string.Empty;
			txtCssFileNm2.Text = string.Empty;
			txtCssFileNm3.Text = string.Empty;
			txtCssFileNm4.Text = string.Empty;
			txtCssFileNm5.Text = string.Empty;
			txtJavascriptFileNm1.Text = string.Empty;
			txtJavascriptFileNm2.Text = string.Empty;
			txtJavascriptFileNm3.Text = string.Empty;
			txtJavascriptFileNm4.Text = string.Empty;
			txtJavascriptFileNm5.Text = string.Empty;
			chkProtectImageFlag.Checked = true;
			chkEmojiToolFlag.Checked = true;
			chkNoIndex.Checked = false;
			chkNoFollow.Checked = false;
			chkNoArchive.Checked = false;

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtPageTile.Text = db.GetStringValue("PPAGE_TITLE");
				txtPageKeyword.Text = db.GetStringValue("PPAGE_KEYWORD");
				txtPageDescription.Text = db.GetStringValue("PPAGE_DESCRIPTION");
				txtHtmlDocTitle.Text = db.GetStringValue("PHTML_DOC_TITLE");
				txtCssFileNm1.Text = db.GetStringValue("PCSS_FILE_NM1");
				txtCssFileNm2.Text = db.GetStringValue("PCSS_FILE_NM2");
				txtCssFileNm3.Text = db.GetStringValue("PCSS_FILE_NM3");
				txtCssFileNm4.Text = db.GetStringValue("PCSS_FILE_NM4");
				txtCssFileNm5.Text = db.GetStringValue("PCSS_FILE_NM5");
				txtJavascriptFileNm1.Text = db.GetStringValue("pJAVASCRIPT_FILE_NM1");
				txtJavascriptFileNm2.Text = db.GetStringValue("pJAVASCRIPT_FILE_NM2");
				txtJavascriptFileNm3.Text = db.GetStringValue("pJAVASCRIPT_FILE_NM3");
				txtJavascriptFileNm4.Text = db.GetStringValue("pJAVASCRIPT_FILE_NM4");
				txtJavascriptFileNm5.Text = db.GetStringValue("pJAVASCRIPT_FILE_NM5");
				chkProtectImageFlag.Checked = db.GetIntValue("pPROTECT_IMAGE_FLAG").Equals(ViCommConst.FLAG_ON);
				chkEmojiToolFlag.Checked = db.GetIntValue("pEMOJI_TOOL_FLAG").Equals(ViCommConst.FLAG_ON);
				chkNoIndex.Checked = db.GetIntValue("pNOINDEX_FLAG").Equals(ViCommConst.FLAG_ON);
				chkNoFollow.Checked = db.GetIntValue("pNOFOLLOW_FLAG").Equals(ViCommConst.FLAG_ON);
				chkNoArchive.Checked = db.GetIntValue("pNOARCHIVE_FLAG").Equals(ViCommConst.FLAG_ON);

				for (int i = 0;i < SysConst.MAX_HTML_BLOCKS;i++) {
					this.HtmlDoc = this.HtmlDoc + db.GetArryStringValue("PHTML_DOC",i);
				}

				for (int i = 0;i < lstTableIndex.Items.Count;i++) {
					if (int.Parse(lstTableIndex.Items[i].Value) == int.Parse(db.GetStringValue("PTABLE_INDEX"))) {
						lstTableIndex.SelectedIndex = i;
					}
				}

				if ((db.GetStringValue("PMIN_DOC_SUB_SEQ").Equals("")) || (db.GetStringValue("PMIN_DOC_SUB_SEQ").Equals(txtHtmlDocSubSeq.Text))) {
					txtPageTile.Enabled = true;
					txtPageKeyword.Enabled = true;
					txtPageDescription.Enabled = true;
					txtCssFileNm1.Enabled = true;
					txtCssFileNm2.Enabled = true;
					txtCssFileNm3.Enabled = true;
					txtCssFileNm4.Enabled = true;
					txtCssFileNm5.Enabled = true;
					txtJavascriptFileNm1.Enabled = true;
					txtJavascriptFileNm2.Enabled = true;
					txtJavascriptFileNm3.Enabled = true;
					txtJavascriptFileNm4.Enabled = true;
					txtJavascriptFileNm5.Enabled = true;
					lstTableIndex.Enabled = true;
					chkProtectImageFlag.Enabled = true;
					chkEmojiToolFlag.Enabled = true;
					chkNoIndex.Enabled = true;
					chkNoFollow.Enabled = true;
					chkNoArchive.Enabled = true;
				} else {
					txtPageTile.Enabled = false;
					txtPageKeyword.Enabled = false;
					txtPageDescription.Enabled = false;
					txtCssFileNm1.Enabled = false;
					txtCssFileNm2.Enabled = false;
					txtCssFileNm3.Enabled = false;
					txtCssFileNm4.Enabled = false;
					txtCssFileNm5.Enabled = false;
					txtJavascriptFileNm1.Enabled = false;
					txtJavascriptFileNm2.Enabled = false;
					txtJavascriptFileNm3.Enabled = false;
					txtJavascriptFileNm4.Enabled = false;
					txtJavascriptFileNm5.Enabled = false;
					lstTableIndex.Enabled = false;
					chkProtectImageFlag.Enabled = false;
					chkEmojiToolFlag.Enabled = false;
					chkNoIndex.Enabled = false;
					chkNoFollow.Enabled = false;
					chkNoArchive.Enabled = false;
				}
			} else {
				ClearField();
			}
		}

		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		lstSeekSiteCd.SelectedIndex = lstSiteCd.SelectedIndex;
		pnlKey.Enabled = false;
		pnlSelect.Visible = true;
		pnlExImport.Visible = !lstUserAgentType.SelectedValue.Equals(ViCommConst.DEVICE_3G);
		SetColor();
		grdSiteHtmlDoc.DataSourceID = "dsSiteHtmlDoc";
		grdSiteHtmlDoc.DataBind();
	}

	private bool UpdateData(int pDelFlag) {
		string[] sDoc;
		int iDocCount;

		if (this.DisablePinEdit) {
			SysPrograms.SeparateSiteHtmlText(HttpUtility.HtmlDecode(this.HtmlDoc),SysConst.MAX_HTML_BLOCKS,out sDoc,out iDocCount);
		} else {
			SysPrograms.SeparateSiteHtml(HttpUtility.HtmlDecode(this.HtmlDoc),SysConst.MAX_HTML_BLOCKS,out sDoc,out iDocCount);
		}

		if ((iDocCount > 10) && (pDelFlag == 0)) {
			return false;
		}
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("USER_VIEW_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PPROGRAM_ROOT",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["PROGRAM_ROOT"]));
			db.ProcedureInParm("PPROGRAM_ID",DbSession.DbType.VARCHAR2,lstProgramId.SelectedValue);
			db.ProcedureInParm("PUSER_AGENT_TYPE",DbSession.DbType.VARCHAR2,lstUserAgentType.SelectedValue);
			db.ProcedureInParm("PAD_GROUP_CD",DbSession.DbType.VARCHAR2,lstAdGroupCd.SelectedValue);
			db.ProcedureInParm("PUSER_RANK",DbSession.DbType.VARCHAR2,lstUserRank.SelectedValue);
			db.ProcedureInParm("PACT_CATEGORY_SEQ",DbSession.DbType.NUMBER,lstActCategorySeq.SelectedValue);
			db.ProcedureInParm("PPAGE_TITLE",DbSession.DbType.VARCHAR2,txtPageTile.Text);
			db.ProcedureInParm("PPAGE_KEYWORD",DbSession.DbType.VARCHAR2,txtPageKeyword.Text);
			db.ProcedureInParm("PPAGE_DESCRIPTION",DbSession.DbType.VARCHAR2,txtPageDescription.Text);
			db.ProcedureInParm("PTABLE_INDEX",DbSession.DbType.NUMBER,int.Parse(lstTableIndex.SelectedValue));
			db.ProcedureInParm("PCSS_FILE_NM1",DbSession.DbType.VARCHAR2,txtCssFileNm1.Text);
			db.ProcedureInParm("pCSS_FILE_NM2",DbSession.DbType.VARCHAR2,txtCssFileNm2.Text);
			db.ProcedureInParm("pCSS_FILE_NM3",DbSession.DbType.VARCHAR2,txtCssFileNm3.Text);
			db.ProcedureInParm("pCSS_FILE_NM4",DbSession.DbType.VARCHAR2,txtCssFileNm4.Text);
			db.ProcedureInParm("pCSS_FILE_NM5",DbSession.DbType.VARCHAR2,txtCssFileNm5.Text);
			db.ProcedureInParm("pJAVASCRIPT_FILE_NM1",DbSession.DbType.VARCHAR2,txtJavascriptFileNm1.Text);
			db.ProcedureInParm("pJAVASCRIPT_FILE_NM2",DbSession.DbType.VARCHAR2,txtJavascriptFileNm2.Text);
			db.ProcedureInParm("pJAVASCRIPT_FILE_NM3",DbSession.DbType.VARCHAR2,txtJavascriptFileNm3.Text);
			db.ProcedureInParm("pJAVASCRIPT_FILE_NM4",DbSession.DbType.VARCHAR2,txtJavascriptFileNm4.Text);
			db.ProcedureInParm("pJAVASCRIPT_FILE_NM5",DbSession.DbType.VARCHAR2,txtJavascriptFileNm5.Text);
			db.ProcedureInParm("pPROTECT_IMAGE_FLAG",DbSession.DbType.NUMBER,chkProtectImageFlag.Checked ? 1 : 0);
			db.ProcedureInParm("pEMOJI_TOOL_FLAG",DbSession.DbType.NUMBER,chkEmojiToolFlag.Checked ? 1 : 0);
			db.ProcedureInParm("pNOINDEX_FLAG",DbSession.DbType.NUMBER,chkNoIndex.Checked ? 1 : 0);
			db.ProcedureInParm("pNOFOLLOW_FLAG",DbSession.DbType.NUMBER,chkNoFollow.Checked ? 1 : 0);
			db.ProcedureInParm("pNOARCHIVE_FLAG",DbSession.DbType.NUMBER,chkNoArchive.Checked ? 1 : 0);
			db.ProcedureInParm("PHTML_DOC_SUB_SEQ",DbSession.DbType.NUMBER,int.Parse(txtHtmlDocSubSeq.Text));
			db.ProcedureInParm("PHTML_DOC_TITLE",DbSession.DbType.VARCHAR2,txtHtmlDocTitle.Text);
			db.ProcedureInArrayParm("PHTML_DOC",DbSession.DbType.VARCHAR2,iDocCount,sDoc);
			db.ProcedureInParm("PHTML_DOC_COUNT",DbSession.DbType.NUMBER,iDocCount);
			db.ProcedureInParm("PROWID_VIEW",DbSession.DbType.VARCHAR2,ViewState["ROWID_VIEW"].ToString());
			db.ProcedureInParm("PREVISION_NO_VIEW",DbSession.DbType.NUMBER,ViewState["REVISION_NO_VIEW"].ToString());
			db.ProcedureInParm("PROWID_MANAGE",DbSession.DbType.VARCHAR2,ViewState["ROWID_MANAGE"].ToString());
			db.ProcedureInParm("PREVISION_NO_MANAGE",DbSession.DbType.NUMBER,ViewState["REVISION_NO_MANAGE"].ToString());
			db.ProcedureInParm("PROWID_DOC",DbSession.DbType.VARCHAR2,ViewState["ROWID_DOC"].ToString());
			db.ProcedureInParm("PREVISION_NO_DOC",DbSession.DbType.NUMBER,ViewState["REVISION_NO_DOC"].ToString());
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PHTML_DOC_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			lblHtmlDocSeq.Text = db.GetStringValue("PHTML_DOC_SEQ");
		}
		EndEdit();

		return true;
	}

	protected void lnkUserView_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		lstSiteCd.SelectedValue = sKeys[0];
		lstUserAgentType.DataBind();
		lstActCategorySeq.DataBind();
		lstAdGroupCd.DataBind();
		lstUserRank.DataBind();

		lstProgramId.SelectedValue = sKeys[1];
		lstUserAgentType.SelectedValue = sKeys[2];
		lstAdGroupCd.SelectedValue = sKeys[3];
		lstUserRank.SelectedValue = sKeys[4];
		lstActCategorySeq.SelectedValue = sKeys[5];
		lblHtmlDocSeq.Text = sKeys[6];
		lstSiteCd.Enabled = false;
		pnlMainte.Visible = true;
		pnlSelect.Visible = true;
		pnlDtl.Visible = false;
		SetColor();

		SessionObjs userObjs = (SessionObjs)Session["objs"];
		userObjs.site.GetOne(lstSiteCd.SelectedValue);

		grdSiteHtmlDoc.DataSourceID = "dsSiteHtmlDoc";
		grdSiteHtmlDoc.DataBind();
	}
	
	private void MainteDirect(string pHTMLDocSeq,string pProgramId,string pUserAgentTypeSeq) {
		lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		lstUserAgentType.DataBind();
		lstActCategorySeq.DataBind();
		lstAdGroupCd.DataBind();
		lstUserRank.DataBind();

		lstProgramId.SelectedValue = pProgramId;
		lstUserAgentType.SelectedValue = pUserAgentTypeSeq;
		lstAdGroupCd.SelectedIndex = 0;
		lstUserRank.SelectedIndex = 0;
		lstActCategorySeq.SelectedIndex = 0;
		lblHtmlDocSeq.Text = pHTMLDocSeq;
		lstSiteCd.Enabled = false;
		pnlMainte.Visible = true;
		pnlSelect.Visible = true;
		pnlDtl.Visible = false;
		SetColor();

		SessionObjs userObjs = (SessionObjs)Session["objs"];
		userObjs.site.GetOne(lstSiteCd.SelectedValue);

		grdSiteHtmlDoc.DataSourceID = "dsSiteHtmlDoc";
		grdSiteHtmlDoc.DataBind();
	}

	protected void lnkDoc_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		txtHtmlDocSubSeq.Text = sKeys[0];
		GetData();
	}

	protected void lnkEdit_Command(object sender,CommandEventArgs e) {

	}
	protected void dsSiteHtmlDoc_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lblHtmlDocSeq.Text;
	}

	protected void dsAdGroup_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
	}

	protected void dsSeekAdGroup_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
	}

	protected void dsUserRank_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
	}

	protected void dsActCategory_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
	}

	protected void dsSeekActCategory_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
	}

	protected void dsUserView_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsUserView_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
		e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["PROGRAM_ROOT"]);
		e.InputParameters[2] = txtSeekProgramId.Text;
		e.InputParameters[3] = lstSeekUserAgentType.SelectedValue;
		e.InputParameters[4] = string.Empty;
		e.InputParameters[5] = lstSeekAdGroupCd.SelectedValue;
		e.InputParameters[6] = lstSeekActCategorySeq.SelectedValue;
		e.InputParameters[7] = SortExpression;
		e.InputParameters[8] = SortDirection;
	}

	private void EndEdit() {
		ClearField();
		pnlDtl.Visible = false;
		pnlKey.Enabled = true;
		lstSiteCd.Enabled = false;
		grdSiteHtmlDoc.DataSourceID = "dsSiteHtmlDoc";
		grdSiteHtmlDoc.DataBind();
		grdUserView.DataBind();
	}

	protected string ShortProgramNm(object pName) {
		string sName = pName.ToString();
		if (sName.Length > 14) {
			return sName.Substring(0,14);
		} else {
			return pName.ToString();
		}
	}

	private void SetColor() {
		string sForeColor;
		string sBackColor;
		string sLinkColor;

		if (int.Parse(lstActCategorySeq.SelectedValue) != ViCommConst.DEFAULT_ACT_CATEGORY_SEQ) {
			using (ActCategory oCategory = new ActCategory()) {
				oCategory.GetOne(lstSiteCd.SelectedValue,lstActCategorySeq.SelectedValue);
				sForeColor = oCategory.colorChar;
				sBackColor = oCategory.colorBack;
				sLinkColor = oCategory.colorLink;
			}
		} else if (lstUserRank.SelectedValue != ViCommConst.DEFAUL_USER_RANK) {
			using (UserRank oUserRank = new UserRank()) {
				oUserRank.GetOne(lstSiteCd.SelectedValue,lstUserRank.SelectedValue);
				sForeColor = oUserRank.colorChar;
				sBackColor = oUserRank.colorBack;
				sLinkColor = oUserRank.colorLink;
			}

		} else if (lstAdGroupCd.SelectedValue != ViCommConst.DEFAUL_AD_GROUP_CD) {
			using (AdGroup oAdGroup = new AdGroup()) {
				oAdGroup.GetOne(lstSiteCd.SelectedValue,lstAdGroupCd.SelectedValue);
				sForeColor = oAdGroup.colorChar;
				sBackColor = oAdGroup.colorBack;
				sLinkColor = oAdGroup.colorLink;
			}

		} else {
			using (Site oSite = new Site()) {
				oSite.GetOne(lstSiteCd.SelectedValue);
				sForeColor = oSite.colorChar;
				sBackColor = oSite.colorBack;
				sLinkColor = oSite.colorLink;
			}
		}

		ltlUserColor.Text = "<style type=\"text/css\">\r\n" +
							"<!--\r\n" +
							".userColor { background-color: " + sBackColor + ";}\r\n" +
							".userColor TD{ color: " + sForeColor + ";}\r\n" +
							".userColor TD A{color: " + sLinkColor + ";font-weight: bold;text-decoration: none;background-color: inherit;}\r\n" +
							"-->\r\n" +
							"</style>";
	}


	protected void btnCSV_Click(object sender,EventArgs e) {
		string sFileNm;
		if (iBridUtil.GetStringValue(ViewState["PROGRAM_ROOT"]).Equals("/ViComm/woman")) {
			sFileNm = string.Format("USER_VIEW_WOMAN_{0}.CSV",lstSeekSiteCd.SelectedValue);
		} else {
			sFileNm = string.Format("USER_VIEW_MAN_{0}.CSV",lstSeekSiteCd.SelectedValue);
		}

		Response.Filter = filter;
		Response.AddHeader("Content-Disposition",string.Format("attachment;filename={0}",sFileNm));
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		using (UserView oView = new UserView()) {
			DataSet ds = oView.GetCsvData(
								lstSeekSiteCd.SelectedValue,
								iBridUtil.GetStringValue(ViewState["PROGRAM_ROOT"]),
								txtSeekProgramId.Text,
								lstSeekUserAgentType.SelectedValue,
								"",
								lstSeekAdGroupCd.SelectedValue,
								lstSeekActCategorySeq.SelectedValue);
			SetCsvData(ds);
			Response.End();
		}
	}

	public void SetCsvData(DataSet pDataSet) {

		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");
		string sDtl = "";

		DataRow dr = null;
		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];
			sDtl = string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}\t{16}\t{17}\t{18}\t{19}\t{20}\t{21}\t{22}\t{23}\t{24}\t{25}\t{26}\t{27}\t{28}\t{29}\t{30}\t{31}\t{32}\t{33}",
							dr["SITE_CD"].ToString(),
							dr["PROGRAM_ROOT"].ToString(),
							dr["PROGRAM_ID"].ToString(),
							dr["USER_AGENT_TYPE"].ToString(),
							dr["AD_GROUP_CD"].ToString(),
							dr["USER_RANK"].ToString(),
							dr["CATEGORY_INDEX"].ToString(),
							dr["VIEW_KEY_MASK"].ToString(),
							dr["PAGE_TITLE"].ToString(),
							dr["PAGE_KEYWORD"].ToString(),
							dr["PAGE_DESCRIPTION"].ToString(),
							dr["TABLE_INDEX"].ToString(),
							dr["CSS_FILE_NM1"].ToString(),
							dr["CSS_FILE_NM2"].ToString(),
							dr["CSS_FILE_NM3"].ToString(),
							dr["CSS_FILE_NM4"].ToString(),
							dr["CSS_FILE_NM5"].ToString(),
							dr["JAVASCRIPT_FILE_NM1"].ToString(),
							dr["JAVASCRIPT_FILE_NM2"].ToString(),
							dr["JAVASCRIPT_FILE_NM3"].ToString(),
							dr["JAVASCRIPT_FILE_NM4"].ToString(),
							dr["JAVASCRIPT_FILE_NM5"].ToString(),
							dr["HTML_DOC_SUB_SEQ"].ToString(),
							dr["HTML_DOC_TITLE"].ToString(),
							dr["HTML_DOC1"].ToString(),
							dr["HTML_DOC2"].ToString(),
							dr["HTML_DOC3"].ToString(),
							dr["HTML_DOC4"].ToString(),
							dr["HTML_DOC5"].ToString(),
							dr["HTML_DOC6"].ToString(),
							dr["HTML_DOC7"].ToString(),
							dr["HTML_DOC8"].ToString(),
							dr["HTML_DOC9"].ToString(),
							dr["HTML_DOC10"].ToString());
			sDtl = sDtl.Replace("\r","");
			sDtl = sDtl.Replace("\n","");
			sDtl += "\r\n";
			Response.BinaryWrite(encoding.GetBytes(sDtl));
		}
	}
	protected string ReplaceForm(object pHtmlDoc) {
		return Regex.Replace(pHtmlDoc.ToString(),@"<\/?[fF][oO][rR][mM].*?>",string.Empty,RegexOptions.Compiled);
	}
	protected void lnkVariableSample_Click(object sender,EventArgs e) {
		string sURL = "";
		txtHtmlDoc.Autofocus = false;
		if (iBridUtil.GetStringValue(ViewState["PROGRAM_ROOT"]).Equals("/ViComm/woman")) {
			sURL = string.Format("{0}/Site/VariableSample.aspx?usetype={1}",Session["Root"].ToString(),ViCommConst.UsableSexCd.WOMAN);
		} else {
			sURL = string.Format("{0}/Site/VariableSample.aspx?usetype={1}",Session["Root"].ToString(),ViCommConst.UsableSexCd.MAN);
		}
		string sScripts = string.Format("<script language=\"javascript\">window.open('{0}','_new','width=1060,height=900,resizable=yes,directories=no,scrollbars=yes' , false);</script>",sURL);
		ClientScript.RegisterStartupScript(Page.GetType(),"OpenNewWindow",sScripts);
	}

	protected void lnkPictographSample_Click(object sender,EventArgs e) {
		txtHtmlDoc.Autofocus = false;
		string sURL = string.Concat(Session["Root"],"/Site/PictographSample.aspx");
		string sScripts = string.Format("<script language=\"javascript\">window.open('{0}','','width=1060,height=900,resizable=yes,directories=no,scrollbars=yes' , false);</script>",sURL);
		ClientScript.RegisterStartupScript(Page.GetType(),"OpenNewWindow",sScripts);
	}

	protected void grdUserView_Sorting(object sender,GridViewSortEventArgs e) {
		if (this.SortExpression.Equals(e.SortExpression)) {
			if (this.SortDirection.Equals("ASC")) {
				this.SortDirection = "DESC";
			} else if (this.SortDirection.Equals("DESC")) {
				this.SortDirection = "ASC";
			}
		} else {
			this.SortDirection = "ASC";
		}

		this.SortExpression = e.SortExpression;

		e.Cancel = true;
		this.GetList();
	}
}
