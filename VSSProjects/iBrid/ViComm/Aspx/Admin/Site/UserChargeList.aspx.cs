﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ユーザー課金メンテナンス
--	Progaram ID		: UserChargeList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Site_UserChargeList:System.Web.UI.Page {
	private string recCount = "";
	private int MAX_CHARGE_COUNT = 41;
	private string actCategorySeq = "";
	private string castRank = "";
	private string campainCd = "";
	private int WHITE_PLAN_COL = 9;
	private bool bWhitePlanFlag;

	protected void Page_Load(object sender,EventArgs e) {
		bWhitePlanFlag = GetWhitePlanFlag();
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
	}

	private void FirstLoad() {
		grdUserCharge.PageSize = 999;
		lblSiteCd.Text = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		lblSiteNm.Text = iBridUtil.GetStringValue(Request.QueryString["sitenm"]);
		lblUserRank.Text = iBridUtil.GetStringValue(Request.QueryString["userrank"]);
		lblUserRankNm.Text = iBridUtil.GetStringValue(Request.QueryString["userranknm"]);
		actCategorySeq = iBridUtil.GetStringValue(Request.QueryString["actcategoryseq"]);
		castRank = iBridUtil.GetStringValue(Request.QueryString["castrank"]);
		campainCd = iBridUtil.GetStringValue(Request.QueryString["campaincd"]);

		if (lstActCategorySeq.Items.Count > 0) {
			lstActCategorySeq.SelectedIndex = 0;
		}
		if (lstCastRank.Items.Count > 0) {
			lstCastRank.SelectedIndex = 0;
		}
		DataBind();
	}

	private void InitPage() {
		pnlDtl.Visible = false;
		ClearField();
		if (actCategorySeq.Equals(string.Empty) == false) {
			lstActCategorySeq.SelectedValue = actCategorySeq;
		}
		if (castRank.Equals(string.Empty) == false) {
			lstCastRank.SelectedValue = castRank;
			lstSeekCastRank.SelectedValue = castRank;
			grdUserCharge.DataBind();
		}
		if (campainCd.Equals(string.Empty) == false) {
			lstCampainCd.SelectedValue = campainCd;
		}
		if (!actCategorySeq.Equals(string.Empty) || !castRank.Equals(string.Empty) || !campainCd.Equals(string.Empty)) {
			GetData();
		}
	}

	private void ClearField() {
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetList();
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
		GetList();
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		pnlDtl.Visible = false;
		GetList();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void lstSeekCampainCd_SelectedIndexChanged(object sender,EventArgs e) {
		GetList();
	}

	protected void lstSeekCastRank_SelectedIndexChanged(object sender,EventArgs e) {
		GetList();
	}

	protected void lstSeekSiteCd_SelectedIndexChanged(object sender,EventArgs e) {
		GetList();
	}

	private void GetList() {
		pnlKey.Enabled = true;
		grdUserCharge.PageIndex = 0;
		grdUserCharge.DataBind();
		pnlCount.DataBind();
	}

	protected void dsUserCharge_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsUserCharge_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
		e.InputParameters[1] = lstSeekCastRank.SelectedValue;
		e.InputParameters[2] = lstSeekCampainCd.SelectedValue;
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("USER_CHARGE_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lblSiteCd.Text);
			db.ProcedureInParm("PUSER_RANK",DbSession.DbType.VARCHAR2,lblUserRank.Text);
			db.ProcedureInParm("PACT_CATEGORY_SEQ",DbSession.DbType.VARCHAR2,lstActCategorySeq.SelectedValue);
			db.ProcedureInParm("PCAST_RANK",DbSession.DbType.VARCHAR2,lstCastRank.SelectedValue);
			db.ProcedureInParm("PCAMPAIN_CD",DbSession.DbType.VARCHAR2,lstCampainCd.SelectedValue);
			db.ProcedureOutArrayParm("PCHARGE_TYPE",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutArrayParm("PCHARGE_TYPE_NM",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutArrayParm("PCHARGE_POINT_NORMAL",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutArrayParm("PCHARGE_POINT_NEW",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutArrayParm("PCHARGE_POINT_NO_RECEIPT",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutArrayParm("PCHARGE_POINT_NO_USE",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutArrayParm("PCHARGE_POINT_FREE_DIAL",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutArrayParm("PCHARGE_POINT_WHITE_PLAN",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutArrayParm("PCHARGE_CAST_TALK_APP_POINT",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutArrayParm("PCHARGE_MAN_TALK_APP_POINT",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutArrayParm("PCHARGE_UNIT_SEC",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutArrayParm("PCHARGE_ROWID",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutArrayParm("PCHARGE_REVISION_NO",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
			db.ProcedureOutParm("PCHARGE_RECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["CHARGE_RECORD_COUNT"] = db.GetIntValue("PCHARGE_RECORD_COUNT");

			//bool bWhitePlanFlag = IsWhitePlan();

			for (int i = 0;i < db.GetIntValue("PCHARGE_RECORD_COUNT");i++) {
				Label lblChargeTypeNm = (Label)plcHolder.FindControl(string.Format("lblChargeTypeNm{0}",i)) as Label;
				TextBox txtChargeUnitSec = (TextBox)plcHolder.FindControl(string.Format("txtChargeUnitSec{0}",i)) as TextBox;
				TextBox txtChargePointNormal = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNormal{0}",i)) as TextBox;
				TextBox txtChargePointNew = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNew{0}",i)) as TextBox;
				TextBox txtChargePointNoReceipt = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNoReceipt{0}",i)) as TextBox;
				TextBox txtChargePointNoUse = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNoUse{0}",i)) as TextBox;
				TextBox txtChargePointFreeDial = (TextBox)plcHolder.FindControl(string.Format("txtChargePointFreeDial{0}",i)) as TextBox;
				TextBox txtChargePointWhitePlan = (TextBox)plcHolder.FindControl(string.Format("txtChargePointWhitePlan{0}",i)) as TextBox;
				TextBox txtChargeCastTalkAppPoint = (TextBox)plcHolder.FindControl(string.Format("txtChargeCastTalkAppPoint{0}",i)) as TextBox;
				TextBox txtChargeManTalkAppPoint = (TextBox)plcHolder.FindControl(string.Format("txtChargeManTalkAppPoint{0}",i)) as TextBox;

				RequiredFieldValidator vdrChargeCastTalkAppPoint = (RequiredFieldValidator)plcHolder.FindControl(string.Format("vdrChargeCastTalkAppPoint{0}",i)) as RequiredFieldValidator;
				RegularExpressionValidator vdeChargeCastTalkAppPoint = (RegularExpressionValidator)plcHolder.FindControl(string.Format("vdeChargeCastTalkAppPoint{0}",i)) as RegularExpressionValidator;

				RequiredFieldValidator vdrChargeManTalkAppPoint = (RequiredFieldValidator)plcHolder.FindControl(string.Format("vdrChargeManTalkAppPoint{0}",i)) as RequiredFieldValidator;
				RegularExpressionValidator vdeChargeManTalkAppPoint = (RegularExpressionValidator)plcHolder.FindControl(string.Format("vdeChargeManTalkAppPoint{0}",i)) as RegularExpressionValidator;

				db.ProcedureOutArrayParm("PCHARGE_POINT_FREE_DIAL",DbSession.DbType.VARCHAR2,MAX_CHARGE_COUNT);
				lblChargeTypeNm.Text = db.GetArryStringValue("PCHARGE_TYPE_NM",i);
				txtChargeUnitSec.Text = db.GetArryStringValue("PCHARGE_UNIT_SEC",i);
				txtChargePointNormal.Text = db.GetArryStringValue("PCHARGE_POINT_NORMAL",i);
				txtChargePointNew.Text = db.GetArryStringValue("PCHARGE_POINT_NEW",i);
				txtChargePointNoReceipt.Text = db.GetArryStringValue("PCHARGE_POINT_NO_RECEIPT",i);
				txtChargePointNoUse.Text = db.GetArryStringValue("PCHARGE_POINT_NO_USE",i);
				txtChargePointFreeDial.Text = db.GetArryStringValue("PCHARGE_POINT_FREE_DIAL",i);
				txtChargePointWhitePlan.Text = db.GetArryStringValue("PCHARGE_POINT_WHITE_PLAN",i);
				txtChargeCastTalkAppPoint.Text = db.GetArryStringValue("PCHARGE_CAST_TALK_APP_POINT",i);
				txtChargeManTalkAppPoint.Text = db.GetArryStringValue("PCHARGE_MAN_TALK_APP_POINT",i);

				ViewState["CHARGE_ROWID" + i.ToString()] = db.GetArryStringValue("PCHARGE_ROWID",i);
				ViewState["CHARGE_REVISION_NO" + i.ToString()] = db.GetArryStringValue("PCHARGE_REVISION_NO",i);
				ViewState["CHARGE_TYPE" + i.ToString()] = db.GetArryStringValue("PCHARGE_TYPE",i);


				switch (db.GetArryStringValue("PCHARGE_TYPE",i)) {
					case ViCommConst.CHARGE_TALK_PUBLIC:
					case ViCommConst.CHARGE_TALK_WSHOT:
					case ViCommConst.CHARGE_TALK_VOICE_WSHOT:
					case ViCommConst.CHARGE_TALK_VOICE_PUBLIC:
					case ViCommConst.CHARGE_CAST_TALK_PUBLIC:
					case ViCommConst.CHARGE_CAST_TALK_WSHOT:
					case ViCommConst.CHARGE_CAST_TALK_VOICE_WSHOT:
					case ViCommConst.CHARGE_CAST_TALK_VOICE_PUBLIC:
						txtChargeCastTalkAppPoint.Visible = true;
						txtChargeManTalkAppPoint.Visible = true;
						vdrChargeCastTalkAppPoint.Enabled = true;
						vdeChargeCastTalkAppPoint.Enabled = true;
						vdrChargeManTalkAppPoint.Enabled = true;
						vdeChargeManTalkAppPoint.Enabled = true;
						break;
					default:
						txtChargeCastTalkAppPoint.Visible = false;
						txtChargeManTalkAppPoint.Visible = false;
						vdrChargeCastTalkAppPoint.Enabled = false;
						vdeChargeCastTalkAppPoint.Enabled = false;
						vdrChargeManTalkAppPoint.Enabled = false;
						vdeChargeManTalkAppPoint.Enabled = false;
						break;
				}
				if (!bWhitePlanFlag) {
					txtChargePointWhitePlan.Visible = bWhitePlanFlag;
				}
			}

			for (int i = db.GetIntValue("PCHARGE_RECORD_COUNT");i < MAX_CHARGE_COUNT;i++) {
				Label lblChargeTypeNm = (Label)plcHolder.FindControl(string.Format("lblChargeTypeNm{0}",i)) as Label;
				TextBox txtChargeUnitSec = (TextBox)plcHolder.FindControl(string.Format("txtChargeUnitSec{0}",i)) as TextBox;
				TextBox txtChargePointNormal = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNormal{0}",i)) as TextBox;
				TextBox txtChargePointNew = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNew{0}",i)) as TextBox;
				TextBox txtChargePointNoReceipt = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNoReceipt{0}",i)) as TextBox;
				TextBox txtChargePointNoUse = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNoUse{0}",i)) as TextBox;
				TextBox txtChargePointFreeDial = (TextBox)plcHolder.FindControl(string.Format("txtChargePointFreeDial{0}",i)) as TextBox;
				TextBox txtChargePointWhitePlan = (TextBox)plcHolder.FindControl(string.Format("txtChargePointWhitePlan{0}",i)) as TextBox;
				TextBox txtChargeCastTalkAppPoint = (TextBox)plcHolder.FindControl(string.Format("txtChargeCastTalkAppPoint{0}",i)) as TextBox;
				TextBox txtChargeManTalkAppPoint = (TextBox)plcHolder.FindControl(string.Format("txtChargeManTalkAppPoint{0}",i)) as TextBox;

				RequiredFieldValidator vdrChargeUnitSec = (RequiredFieldValidator)plcHolder.FindControl(string.Format("vdrChargeUnitSec{0}",i)) as RequiredFieldValidator;
				RegularExpressionValidator vdeChargeUnitSec = (RegularExpressionValidator)plcHolder.FindControl(string.Format("vdeChargeUnitSec{0}",i)) as RegularExpressionValidator;

				RequiredFieldValidator vdrChargePointNormal = (RequiredFieldValidator)plcHolder.FindControl(string.Format("vdrChargePointNormal{0}",i)) as RequiredFieldValidator;
				RegularExpressionValidator vdeChargePointNormal = (RegularExpressionValidator)plcHolder.FindControl(string.Format("vdeChargePointNormal{0}",i)) as RegularExpressionValidator;

				RequiredFieldValidator vdrChargePointNew = (RequiredFieldValidator)plcHolder.FindControl(string.Format("vdrChargePointNew{0}",i)) as RequiredFieldValidator;
				RegularExpressionValidator vdeChargePointNew = (RegularExpressionValidator)plcHolder.FindControl(string.Format("vdeChargePointNew{0}",i)) as RegularExpressionValidator;

				RequiredFieldValidator vdrChargePointNoReceipt = (RequiredFieldValidator)plcHolder.FindControl(string.Format("vdrChargePointNoReceipt{0}",i)) as RequiredFieldValidator;
				RegularExpressionValidator vdeChargePointNoReceipt = (RegularExpressionValidator)plcHolder.FindControl(string.Format("vdeChargePointNoReceipt{0}",i)) as RegularExpressionValidator;

				RequiredFieldValidator vdrChargePointNoUse = (RequiredFieldValidator)plcHolder.FindControl(string.Format("vdrChargePointNoUse{0}",i)) as RequiredFieldValidator;
				RegularExpressionValidator vdeChargePointNoUse = (RegularExpressionValidator)plcHolder.FindControl(string.Format("vdeChargePointNoUse{0}",i)) as RegularExpressionValidator;

				RequiredFieldValidator vdrChargePointFreeDial = (RequiredFieldValidator)plcHolder.FindControl(string.Format("vdrChargePointFreeDial{0}",i)) as RequiredFieldValidator;
				RegularExpressionValidator vdeChargePointFreeDial = (RegularExpressionValidator)plcHolder.FindControl(string.Format("vdeChargePointFreeDial{0}",i)) as RegularExpressionValidator;

				RequiredFieldValidator vdrChargePointWhitePlan = (RequiredFieldValidator)plcHolder.FindControl(string.Format("vdrChargePointWhitePlan{0}",i)) as RequiredFieldValidator;
				RegularExpressionValidator vdeChargePointWhitePlan = (RegularExpressionValidator)plcHolder.FindControl(string.Format("vdeChargePointWhitePlan{0}",i)) as RegularExpressionValidator;

				RequiredFieldValidator vdrChargeCastTalkAppPoint = (RequiredFieldValidator)plcHolder.FindControl(string.Format("vdrChargeCastTalkAppPoint{0}",i)) as RequiredFieldValidator;
				RegularExpressionValidator vdeChargeCastTalkAppPoint = (RegularExpressionValidator)plcHolder.FindControl(string.Format("vdeChargeCastTalkAppPoint{0}",i)) as RegularExpressionValidator;

				RequiredFieldValidator vdrChargeManTalkAppPoint = (RequiredFieldValidator)plcHolder.FindControl(string.Format("vdrChargeManTalkAppPoint{0}",i)) as RequiredFieldValidator;
				RegularExpressionValidator vdeChargeManTalkAppPoint = (RegularExpressionValidator)plcHolder.FindControl(string.Format("vdeChargeManTalkAppPoint{0}",i)) as RegularExpressionValidator;

				lblChargeTypeNm.Visible = false;
				txtChargeUnitSec.Visible = false;
				txtChargePointNormal.Visible = false;
				txtChargePointNew.Visible = false;
				txtChargePointNoReceipt.Visible = false;
				txtChargePointNoUse.Visible = false;
				txtChargePointFreeDial.Visible = false;
				txtChargePointWhitePlan.Visible = false;
				txtChargeCastTalkAppPoint.Visible = false;
				txtChargeManTalkAppPoint.Visible = false;

				vdrChargeUnitSec.Enabled = false;
				vdeChargeUnitSec.Enabled = false;

				vdrChargePointNormal.Enabled = false;
				vdeChargePointNormal.Enabled = false;

				vdrChargePointNew.Enabled = false;
				vdeChargePointNew.Enabled = false;

				vdrChargePointNoReceipt.Enabled = false;
				vdeChargePointNoReceipt.Enabled = false;

				vdrChargePointNoUse.Enabled = false;
				vdeChargePointNoUse.Enabled = false;

				vdrChargePointFreeDial.Enabled = false;
				vdeChargePointFreeDial.Enabled = false;

				vdrChargePointWhitePlan.Enabled = false;
				vdeChargePointWhitePlan.Enabled = false;

				vdrChargeCastTalkAppPoint.Enabled = false;
				vdeChargeCastTalkAppPoint.Enabled = false;

				vdrChargeManTalkAppPoint.Enabled = false;
				vdeChargeManTalkAppPoint.Enabled = false;

			}
		}
		pnlKey.Enabled = false;
		pnlDtl.Visible = true;
	}

	private void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("USER_CHARGE_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lblSiteCd.Text);
			db.ProcedureInParm("PUSER_RANK",DbSession.DbType.VARCHAR2,lblUserRank.Text);
			db.ProcedureInParm("PACT_CATEGORY_SEQ",DbSession.DbType.VARCHAR2,lstActCategorySeq.SelectedValue);
			db.ProcedureInParm("PCAST_RANK",DbSession.DbType.VARCHAR2,lstCastRank.SelectedValue);
			db.ProcedureInParm("PCAMPAIN_CD",DbSession.DbType.VARCHAR2,lstCampainCd.SelectedValue);

			int iRecCount = int.Parse(ViewState["CHARGE_RECORD_COUNT"].ToString());

			string[] sChargeType = new string[MAX_CHARGE_COUNT];
			string[] sChargeUnitSec = new string[MAX_CHARGE_COUNT];
			string[] sChargePointNormal = new string[MAX_CHARGE_COUNT];
			string[] sChargePointNew = new string[MAX_CHARGE_COUNT];
			string[] sChargePointNoReceipt = new string[MAX_CHARGE_COUNT];
			string[] sChargePointNoUse = new string[MAX_CHARGE_COUNT];
			string[] sChargePointFreeDial = new string[MAX_CHARGE_COUNT];
			string[] sChargePointWhitePlan = new string[MAX_CHARGE_COUNT];
			string[] sChargeCastTalkAppPoint = new string[MAX_CHARGE_COUNT];
			string[] sChargeManTalkAppPoint = new string[MAX_CHARGE_COUNT];
			string[] sRevisionNo = new string[MAX_CHARGE_COUNT];
			string[] sRowID = new string[MAX_CHARGE_COUNT];

			for (int i = 0;i < iRecCount;i++) {
				TextBox txtChargeUnitSec = (TextBox)plcHolder.FindControl(string.Format("txtChargeUnitSec{0}",i)) as TextBox;
				TextBox txtChargePointNormal = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNormal{0}",i)) as TextBox;
				TextBox txtChargePointNew = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNew{0}",i)) as TextBox;
				TextBox txtChargePointNoReceipt = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNoReceipt{0}",i)) as TextBox;
				TextBox txtChargePointNoUse = (TextBox)plcHolder.FindControl(string.Format("txtChargePointNoUse{0}",i)) as TextBox;
				TextBox txtChargePointFreeDial = (TextBox)plcHolder.FindControl(string.Format("txtChargePointFreeDial{0}",i)) as TextBox;
				TextBox txtChargePointWhitePlan = (TextBox)plcHolder.FindControl(string.Format("txtChargePointWhitePlan{0}",i)) as TextBox;
				TextBox txtChargeCastTalkAppPoint = (TextBox)plcHolder.FindControl(string.Format("txtChargeCastTalkAppPoint{0}",i)) as TextBox;
				TextBox txtChargeManTalkAppPoint = (TextBox)plcHolder.FindControl(string.Format("txtChargeManTalkAppPoint{0}",i)) as TextBox;

				sChargeUnitSec[i] = txtChargeUnitSec.Text;
				sChargePointNormal[i] = txtChargePointNormal.Text;
				sChargePointNew[i] = txtChargePointNew.Text;
				sChargePointNoReceipt[i] = txtChargePointNoReceipt.Text;
				sChargePointNoUse[i] = txtChargePointNoUse.Text;
				sChargePointFreeDial[i] = txtChargePointFreeDial.Text;
				sChargePointWhitePlan[i] = txtChargePointWhitePlan.Text;
				sChargeCastTalkAppPoint[i] = txtChargeCastTalkAppPoint.Text;
				sChargeManTalkAppPoint[i] = txtChargeManTalkAppPoint.Text;

				sChargeType[i] = (string)ViewState["CHARGE_TYPE" + i.ToString()];
				sRevisionNo[i] = (string)ViewState["CHARGE_REVISION_NO" + i.ToString()];
				sRowID[i] = (string)ViewState["CHARGE_ROWID" + i.ToString()];
			}

			db.ProcedureInArrayParm("PCHARGE_TYPE",DbSession.DbType.VARCHAR2,iRecCount,sChargeType);
			db.ProcedureInArrayParm("PCHARGE_POINT_NORMAL",DbSession.DbType.VARCHAR2,iRecCount,sChargePointNormal);
			db.ProcedureInArrayParm("PCHARGE_POINT_NEW",DbSession.DbType.VARCHAR2,iRecCount,sChargePointNew);
			db.ProcedureInArrayParm("PCHARGE_POINT_NO_RECEIPT",DbSession.DbType.VARCHAR2,iRecCount,sChargePointNoReceipt);
			db.ProcedureInArrayParm("PCHARGE_POINT_NO_USE",DbSession.DbType.VARCHAR2,iRecCount,sChargePointNoUse);
			db.ProcedureInArrayParm("PCHARGE_POINT_FREE_DIAL",DbSession.DbType.VARCHAR2,iRecCount,sChargePointFreeDial);
			db.ProcedureInArrayParm("PCHARGE_POINT_WHITE_PLAN",DbSession.DbType.VARCHAR2,iRecCount,sChargePointWhitePlan);
			db.ProcedureInArrayParm("PCHARGE_CAST_TALK_APP_POINT",DbSession.DbType.VARCHAR2,iRecCount,sChargeCastTalkAppPoint);
			db.ProcedureInArrayParm("PCHARGE_MAN_TALK_APP_POINT",DbSession.DbType.VARCHAR2,iRecCount,sChargeManTalkAppPoint);
			db.ProcedureInArrayParm("PCHARGE_UNIT_SEC",DbSession.DbType.VARCHAR2,iRecCount,sChargeUnitSec);
			db.ProcedureInArrayParm("PCHARGE_ROWID",DbSession.DbType.VARCHAR2,iRecCount,sRowID);
			db.ProcedureInArrayParm("PCHARGE_REVISION_NO",DbSession.DbType.VARCHAR2,iRecCount,sRevisionNo);
			db.ProcedureInParm("PCHARGE_RECORD_COUNT",DbSession.DbType.NUMBER,iRecCount);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();
			pnlDtl.Visible = false;
		}
		InitPage();
		GetList();
		lstSeekSiteCd.SelectedValue = lblSiteCd.Text;
		lstSeekCastRank.SelectedValue = lstCastRank.SelectedValue;
		lstSeekCampainCd.SelectedValue = lstCampainCd.SelectedValue;
	}

	protected void grdUserCharge_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow ||
			e.Row.RowType == DataControlRowType.Header ||
			e.Row.RowType == DataControlRowType.Footer) {
			e.Row.Cells[WHITE_PLAN_COL].Visible = bWhitePlanFlag;
		}

	}

	protected void grdUserCharge_DataBound(object sender,EventArgs e) {
		JoinCells(grdUserCharge,2);
		JoinCells(grdUserCharge,1);
		JoinCells(grdUserCharge,0);
	}

	protected void JoinCells(GridView pGrd,int pCol) {
		int iRow = pGrd.Rows.Count;
		int iBaseidx = 0;

		while (iBaseidx < iRow) {
			int iNextIdx = iBaseidx + 1;
			TableCell celBase = pGrd.Rows[iBaseidx].Cells[pCol];

			while (iNextIdx < iRow) {

				TableCell celNext = pGrd.Rows[iNextIdx].Cells[pCol];

				if (GetText(celBase).Equals(GetText(celNext))) {
					if (celBase.RowSpan == 0) {
						celBase.RowSpan = 2;
					} else {
						celBase.RowSpan++;
					}
					pGrd.Rows[iNextIdx].Cells.Remove(celNext);
					iNextIdx++;
				} else {
					break;
				}
			}
			iBaseidx = iNextIdx;
		}
	}
	private string GetText(TableCell tc) {
		HyperLink dblc =
		  (HyperLink)tc.Controls[1];
		return dblc.Text;
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		Response.AddHeader("Content-Disposition",string.Format("attachment;filename=USER_CHARGE_{0}_CAMPAIN{1}.CSV",lstSeekSiteCd.SelectedValue,lstSeekCampainCd.SelectedValue));
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		using (UserCharge oUserCharge = new UserCharge()) {
			DataSet ds = oUserCharge.GetCsvData(lstSeekSiteCd.SelectedValue,"",lstSeekCampainCd.SelectedValue);
			SetCsvData(ds);
			Response.End();
		}
	}

	public void SetCsvData(DataSet pDataSet) {

		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");
		string sDtl = "";

		DataRow dr = null;
		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];
			if (bWhitePlanFlag) {
				sDtl = string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}",
								dr["SITE_CD"].ToString(),
								dr["USER_RANK"].ToString(),
								dr["CHARGE_TYPE"].ToString(),
								dr["CATEGORY_INDEX"].ToString(),
								dr["CAST_RANK"].ToString(),
								dr["CAMPAIN_CD"].ToString(),
								dr["CHARGE_POINT_NORMAL"].ToString(),
								dr["CHARGE_POINT_NEW"].ToString(),
								dr["CHARGE_POINT_NO_RECEIPT"].ToString(),
								dr["CHARGE_POINT_NO_USE"].ToString(),
								dr["CHARGE_POINT_FREE_DIAL"].ToString(),
								dr["CHARGE_POINT_WHITE_PLAN"].ToString(),
								dr["CHARGE_CAST_TALK_APP_POINT"].ToString(),
								dr["CHARGE_MAN_TALK_APP_POINT"].ToString(),
								dr["CHARGE_UNIT_SEC"].ToString());
			} else {
				sDtl = string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}",
								dr["SITE_CD"].ToString(),
								dr["USER_RANK"].ToString(),
								dr["CHARGE_TYPE"].ToString(),
								dr["CATEGORY_INDEX"].ToString(),
								dr["CAST_RANK"].ToString(),
								dr["CAMPAIN_CD"].ToString(),
								dr["CHARGE_POINT_NORMAL"].ToString(),
								dr["CHARGE_POINT_NEW"].ToString(),
								dr["CHARGE_POINT_NO_RECEIPT"].ToString(),
								dr["CHARGE_POINT_NO_USE"].ToString(),
								dr["CHARGE_POINT_FREE_DIAL"].ToString(),
								dr["CHARGE_CAST_TALK_APP_POINT"].ToString(),
								dr["CHARGE_MAN_TALK_APP_POINT"].ToString(),
								dr["CHARGE_UNIT_SEC"].ToString());
			}
			sDtl = sDtl.Replace("\r","");
			sDtl = sDtl.Replace("\n","");
			sDtl += "\r\n";
			Response.BinaryWrite(encoding.GetBytes(sDtl));
		}
	}

	public bool IsWhitePlan() {

		return bWhitePlanFlag;
	}
	public bool GetWhitePlanFlag() {

		using (ManageCompany oManage = new ManageCompany()) {
			return oManage.IsAvailableService(ViCommConst.RELEASE_ENABLED_WHITE_PLAN);
		}
	}
}
