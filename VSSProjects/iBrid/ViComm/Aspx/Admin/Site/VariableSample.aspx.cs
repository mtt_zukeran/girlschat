﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 変数表

--	Progaram ID		: VariableSample
--
--  Creation Date	: 2010.08.20
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Site_VariableSample:System.Web.UI.Page {

	private const int VARIABLE_DISP_COUNT = 1;		// 1行に表示する変数の数
	private const int MENU_DISP_COUNT = 8;			// 1行に表示するリンクの数

	private DataTable dtVarType;
	private DataTable dtData;

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			ViewState["USABLE_TYPE"] = iBridUtil.GetStringValue(Request.QueryString["usetype"]);
			InitPage();
		}
	}

	private void InitPage() {
		if (!IsPostBack) {
			if (ViewState["USABLE_TYPE"].ToString().Equals(ViCommConst.UsableSexCd.MAN)) {
				this.lblPgmTitle.Text = DisplayWordUtil.Replace("男性用変換表");
			} else {
				this.lblPgmTitle.Text = DisplayWordUtil.Replace("女性用変換表");
			}
			GetList();
		}
	}

	private void GetList() {
		using (Variable oVariable = new Variable()) {
			dtData = oVariable.GetSampleList(ViewState["USABLE_TYPE"].ToString(),null).Tables[0];
		}
		using (CodeDtl oCodeDtl = new CodeDtl()) {
			dtVarType = oCodeDtl.GetList("35").Tables[0];
		}
		SetTable();
		SetMenuTable();
	}

	protected void SetTable() {
		foreach (DataRow dr in dtVarType.Rows) {
			DataRow[] targetRows;
			targetRows = dtData.Select(string.Format("VARIABLE_TYPE = {0}",dr["CODE"]));

			if (targetRows.Length > 0) {
				TableRow trValType = new TableRow();
				TableCell tcValType = new TableCell();
				
				tcValType.ColumnSpan = VARIABLE_DISP_COUNT * 4;
				tcValType.Text = dr["CODE_NM"].ToString() + string.Format("<span id=\"{0}\"></span>",dr["CODE"]);
				tcValType.Font.Bold = true;
				if (ViewState["USABLE_TYPE"].ToString().Equals(ViCommConst.UsableSexCd.MAN)) {
					tcValType.BackColor = Color.LightBlue;
				} else {
					tcValType.BackColor = Color.LightPink;
				}

				trValType.Cells.Add(tcValType);
				tblVariable.Rows.Add(trValType);
			}

			int iRow = 1;
			for (int idx = 0;idx < targetRows.Length;idx += VARIABLE_DISP_COUNT) {
				TableRow trData = new TableRow();

				for (int i = 0;i < VARIABLE_DISP_COUNT;i++) {
					TableCell tcId = new TableCell();
					TableCell tcNm = new TableCell();
					TableCell tcSummary = new TableCell();
					TableCell tcParm = new TableCell();
					if (targetRows.Length > idx + i) {
						tcId.ForeColor = Color.Blue;
						tcId.Text = string.Format("&nbsp;{0}&nbsp;",targetRows[idx + i]["VARIABLE_ID"].ToString());
						tcNm.Text = string.Format("&nbsp;{0}&nbsp;",targetRows[idx + i]["VARIABLE_NM"].ToString());
						tcSummary.Text = string.Format("<div style='width:350px;'>&nbsp;{0}&nbsp;</div>",targetRows[idx + i]["VARIABLE_SUMMARY"].ToString());
						tcParm.Text = string.Format("&nbsp;{0}&nbsp;",targetRows[idx + i]["VARIABLE_PARM"].ToString());

						tcId.Wrap = false;
						tcNm.Wrap = false;
						//tcSummary.Wrap = false;
						tcParm.Wrap = false;

						trData.Cells.Add(tcId);
						trData.Cells.Add(tcNm);
						trData.Cells.Add(tcSummary);
						trData.Cells.Add(tcParm);

					} else {
						tcId.ColumnSpan = (VARIABLE_DISP_COUNT-i) * 4;
						trData.Cells.Add(tcId);
					}
				}
				if (iRow % 2 == 0) {
					trData.CssClass = "tdSampleDataStyle";
				}
				tblVariable.Rows.Add(trData);
				iRow++;
			}
		}
	}

	protected void SetMenuTable() {

		int iCell = 0;
		int iRec = 0;
		for (int idx = 0;idx < dtVarType.Rows.Count;idx += MENU_DISP_COUNT) {
			if (idx != 0) {
				idx += iRec - MENU_DISP_COUNT;
			}

			TableRow trMenu = new TableRow();
			iCell = 0;
			iRec = 0;

			while (iCell < MENU_DISP_COUNT) {
				TableCell tcMenu = new TableCell();
				tcMenu.BorderWidth = 0;

				if (dtVarType.Rows.Count > idx + iRec) {
					DataRow[] targetRows;
					targetRows = dtData.Select(string.Format("VARIABLE_TYPE = {0}",dtVarType.Rows[idx + iRec]["CODE"]));

					if (targetRows.Length > 0) {
						tcMenu.Text = string.Format("<a href=\"#{0}\">{1}</a>",dtVarType.Rows[idx + iRec]["CODE"].ToString(),dtVarType.Rows[idx + iRec]["CODE_NM"].ToString());
						trMenu.Cells.Add(tcMenu);
						iCell++;
					}
					iRec++;
				} else {
					tcMenu.ColumnSpan = (MENU_DISP_COUNT - iCell);
					trMenu.Cells.Add(tcMenu);
					break;
				}
			}
			tblLink.Rows.Add(trMenu);
		}
	}
}
