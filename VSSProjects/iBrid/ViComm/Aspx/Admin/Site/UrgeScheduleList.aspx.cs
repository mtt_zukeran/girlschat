﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 督促スケジュール
--	Progaram ID		: UrgeScheduleList
--
--  Creation Date	: 2009.12.14
--  Creater			: iBrid(T.Tokunaga)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Site_UrgeScheduleListt:System.Web.UI.Page {
	private string recCount = "";
	private int MAX_URGE_COUNT = 5;

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
	}

	private void FirstLoad() {
		grdUrgeSchedule.PageSize = 999;
		lblSiteCd.Text = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		using (Site oSite = new Site()) {
			if (oSite.GetOne(lblSiteCd.Text)) {
				lblSiteNm.Text = oSite.siteNm;
			}
		}
		lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		lblUrgeLevel.Text = iBridUtil.GetStringValue(Request.QueryString["urgelevel"]);
		DataBind();
		lstMailTemplateNo1.Items.Insert(0,new ListItem("",""));
		lstMailTemplateNo2.Items.Insert(0,new ListItem("",""));
		lstMailTemplateNo3.Items.Insert(0,new ListItem("",""));
		lstMailTemplateNo4.Items.Insert(0,new ListItem("",""));
		lstMailTemplateNo5.Items.Insert(0,new ListItem("",""));
	}

	private void InitPage() {
		pnlDtl.Visible = false;
		ClearField();
		GetData();
	}

	private void ClearField() {
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetList();
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
		GetList();
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		pnlDtl.Visible = false;
		GetList();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	private void GetList() {
		pnlKey.Enabled = true;
		grdUrgeSchedule.PageIndex = 0;
		grdUrgeSchedule.DataBind();
		pnlCount.DataBind();
	}

	protected void dsUrgeSchedule_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsUrgeSchedule_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
		e.InputParameters[1] = string.Empty;
	}

	protected void dsUrgeType_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
	}

	protected void dsMailTemplate_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		e.InputParameters[1] = ViCommConst.MAIL_TP_URGE;
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("URGE_SCHEDULE_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lblSiteCd.Text);
			db.ProcedureInParm("PURGE_LEVEL",DbSession.DbType.NUMBER,lblUrgeLevel.Text);
			db.ProcedureInParm("PURGE_RECORD_COUNT",DbSession.DbType.NUMBER,MAX_URGE_COUNT);
			db.ProcedureOutArrayParm("PURGE_COUNT_PER_DAY",DbSession.DbType.VARCHAR2,MAX_URGE_COUNT);
			db.ProcedureOutArrayParm("PURGE_START_TIME",DbSession.DbType.VARCHAR2,MAX_URGE_COUNT);
			db.ProcedureOutArrayParm("PURGE_END_TIME",DbSession.DbType.VARCHAR2,MAX_URGE_COUNT);
			db.ProcedureOutArrayParm("PURGE_TYPE",DbSession.DbType.VARCHAR2,MAX_URGE_COUNT);
			db.ProcedureOutArrayParm("PURGE_LAST_EXEC_DATE",DbSession.DbType.VARCHAR2,MAX_URGE_COUNT);
			db.ProcedureOutArrayParm("PURGE_MAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,MAX_URGE_COUNT);
			db.ProcedureOutArrayParm("PURGE_ROWID",DbSession.DbType.VARCHAR2,MAX_URGE_COUNT);
			db.ProcedureOutArrayParm("PURGE_REVISION_NO",DbSession.DbType.VARCHAR2,MAX_URGE_COUNT);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["PURGE_RECORD_COUNT"] = db.GetIntValue("PURGE_RECORD_COUNT");

			for (int i = 1;i <= MAX_URGE_COUNT;i++) {
				int intArrIndex = i - 1;
				if (db.GetIntValue("PURGE_RECORD_COUNT") > 0) {
					Label lblUrgeCountPerDay = (Label)plcHolder.FindControl(string.Format("lblUrgeCountPerDay{0}",i)) as Label;
					TextBox txtUrgeStartTime = (TextBox)plcHolder.FindControl(string.Format("txtUrgeStartTime{0}",i)) as TextBox;
					TextBox txtUrgeEndTime = (TextBox)plcHolder.FindControl(string.Format("txtUrgeEndTime{0}",i)) as TextBox;
					DropDownList lstUrgeType = (DropDownList)plcHolder.FindControl(string.Format("lstUrgeType{0}",i)) as DropDownList;
					DropDownList lstMailTemplateNo = (DropDownList)plcHolder.FindControl(string.Format("lstMailTemplateNo{0}",i)) as DropDownList;
					Label lblUrgeLastExecDate = (Label)plcHolder.FindControl(string.Format("lblUrgeLastExecDate{0}",i)) as Label;

					lblUrgeCountPerDay.Text = i.ToString();
					txtUrgeStartTime.Text = db.GetArryStringValue("PURGE_START_TIME",intArrIndex);
					txtUrgeEndTime.Text = db.GetArryStringValue("PURGE_END_TIME",intArrIndex);
					if (string.IsNullOrEmpty(db.GetArryStringValue("PURGE_TYPE",intArrIndex))) {
						lstUrgeType.SelectedIndex = 0;
					} else {
						lstUrgeType.SelectedValue = db.GetArryStringValue("PURGE_TYPE",intArrIndex);
					}
					if (string.IsNullOrEmpty(db.GetArryStringValue("PURGE_MAIL_TEMPLATE_NO",intArrIndex))) {
						lstMailTemplateNo.SelectedIndex = -1;
					} else {
						lstMailTemplateNo.SelectedValue = db.GetArryStringValue("PURGE_MAIL_TEMPLATE_NO",intArrIndex);
					}
					lblUrgeLastExecDate.Text = db.GetArryStringValue("PURGE_LAST_EXEC_DATE",intArrIndex);
				}
				ViewState["URGE_ROWID" + intArrIndex.ToString()] = db.GetArryStringValue("PURGE_ROWID",intArrIndex);
				ViewState["PURGE_REVISION_NO" + intArrIndex.ToString()] = db.GetArryStringValue("PURGE_REVISION_NO",intArrIndex);
			}
		}
		pnlKey.Enabled = true;
		pnlDtl.Visible = true;
	}

	private void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {


			string[] sUrgeLevel = new string[MAX_URGE_COUNT];
			string[] sUrgeCount = new string[MAX_URGE_COUNT];
			string[] sUrgeStartTime = new string[MAX_URGE_COUNT];
			string[] sUrgeEndTime = new string[MAX_URGE_COUNT];
			string[] sUrgeType = new string[MAX_URGE_COUNT];
			string[] sMailTemplateNo = new string[MAX_URGE_COUNT];
			string[] sRevisionNo = new string[MAX_URGE_COUNT];
			string[] sRowID = new string[MAX_URGE_COUNT];
			string[] sDelSchedule = new string[MAX_URGE_COUNT];

			for (int i = 0;i < MAX_URGE_COUNT;i++) {

				int intItemIndex = i + 1;
				TextBox txtUrgeStartTime = (TextBox)plcHolder.FindControl(string.Format("txtUrgeStartTime{0}",intItemIndex)) as TextBox;
				TextBox txtUrgeEndTime = (TextBox)plcHolder.FindControl(string.Format("txtUrgeEndTime{0}",intItemIndex)) as TextBox;
				DropDownList lstUrgeType = (DropDownList)plcHolder.FindControl(string.Format("lstUrgeType{0}",intItemIndex)) as DropDownList;
				DropDownList lstMailTemplateNo = (DropDownList)plcHolder.FindControl(string.Format("lstMailTemplateNo{0}",intItemIndex)) as DropDownList;

				sUrgeCount[i] = intItemIndex.ToString();
				if (txtUrgeStartTime.Text.Equals("")) {
					txtUrgeStartTime.Text = "00:00";
				}
				sUrgeStartTime[i] = txtUrgeStartTime.Text;
				if (txtUrgeEndTime.Text.Equals("")) {
					txtUrgeEndTime.Text = "00:00";
				}
				sUrgeEndTime[i] = txtUrgeEndTime.Text;
				sUrgeType[i] = lstUrgeType.SelectedValue;
				sMailTemplateNo[i] = lstMailTemplateNo.SelectedValue;
				sDelSchedule[i] = "0";

				sRowID[i] = ViewState["URGE_ROWID" + i.ToString()].ToString();
				sRevisionNo[i] = ViewState["PURGE_REVISION_NO" + i.ToString()].ToString();
			}

			db.PrepareProcedure("URGE_SCHEDULE_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lblSiteCd.Text);
			db.ProcedureInParm("PURGE_LEVEL",DbSession.DbType.NUMBER,lblUrgeLevel.Text);
			db.ProcedureInParm("PURGE_RECORD_COUNT",DbSession.DbType.NUMBER,MAX_URGE_COUNT);
			db.ProcedureInArrayParm("PURGE_COUNT_PER_DAY",DbSession.DbType.VARCHAR2,MAX_URGE_COUNT,sUrgeCount);
			db.ProcedureInArrayParm("PURGE_START_TIME",DbSession.DbType.VARCHAR2,MAX_URGE_COUNT,sUrgeStartTime);
			db.ProcedureInArrayParm("PURGE_END_TIME",DbSession.DbType.VARCHAR2,MAX_URGE_COUNT,sUrgeEndTime);
			db.ProcedureInArrayParm("PURGE_TYPE",DbSession.DbType.VARCHAR2,MAX_URGE_COUNT,sUrgeType);
			db.ProcedureInArrayParm("PURGE_MAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,MAX_URGE_COUNT,sMailTemplateNo);
			db.ProcedureInArrayParm("PDEL_FLAG",DbSession.DbType.VARCHAR2,MAX_URGE_COUNT,sDelSchedule);
			db.ProcedureInArrayParm("PURGE_ROWID",DbSession.DbType.VARCHAR2,MAX_URGE_COUNT,sRowID);
			db.ProcedureInArrayParm("PURGE_REVISION_NO",DbSession.DbType.VARCHAR2,MAX_URGE_COUNT,sRevisionNo);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();
			pnlDtl.Visible = false;
		}
		InitPage();
		GetList();
		lstSeekSiteCd.SelectedValue = lblSiteCd.Text;
	}

	protected void grdUrgeSchedule_DataBound(object sender,EventArgs e) {
		JoinCells(grdUrgeSchedule,0);
	}

	protected void JoinCells(GridView pGrd,int pCol) {
		int iRow = pGrd.Rows.Count;
		int iBaseidx = 0;

		while (iBaseidx < iRow) {
			int iNextIdx = iBaseidx + 1;
			TableCell celBase = pGrd.Rows[iBaseidx].Cells[pCol];

			while (iNextIdx < iRow) {

				TableCell celNext = pGrd.Rows[iNextIdx].Cells[pCol];

				if (GetText(celBase).Equals(GetText(celNext))) {
					if (celBase.RowSpan == 0) {
						celBase.RowSpan = 2;
					} else {
						celBase.RowSpan++;
					}
					pGrd.Rows[iNextIdx].Cells.Remove(celNext);
					iNextIdx++;
				} else {
					break;
				}
			}
			iBaseidx = iNextIdx;
		}
	}
	private string GetText(TableCell tc) {
		HyperLink dblc =
		  (HyperLink)tc.Controls[1];
		return dblc.Text;
	}

	protected void vdcMailTemplateNo_ServerValidate(object source,ServerValidateEventArgs args) {
		string sPos;
		CustomValidator vdcMailTemplateNo = (CustomValidator)source as CustomValidator;
		sPos = vdcMailTemplateNo.ID.Substring(17,1);
		DropDownList lstUrgeType = (DropDownList)pnlDtl.FindControl(string.Format("lstUrgeType{0}",sPos));
		DropDownList lstMailTemplateNo = (DropDownList)pnlDtl.FindControl(string.Format("lstMailTemplateNo{0}",sPos));
		RequiredFieldValidator vdrMailTemplateNo = (RequiredFieldValidator)pnlDtl.FindControl(string.Format("vdrMailTemplateNo{0}",sPos));
		if (!lstUrgeType.SelectedValue.Equals(ViCommConst.URGE_CALL)) {
			if (lstMailTemplateNo.SelectedValue.Equals("")) {
				vdrMailTemplateNo.Enabled = true;
			} else {
				vdrMailTemplateNo.Enabled = false;
			}
		} else {
			vdrMailTemplateNo.Enabled = false;
		}
	}
}
