﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 男性会員属性区分メンテナンス
--	Progaram ID		: UserManAttrTypeList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;

public partial class Site_UserManAttrTypeList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_SITE_OWNER)) {
			throw new ApplicationException("ViComm権限違反");
		}
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
	}

	protected void dsUserManAttrType_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void FirstLoad() {
		grdManAttrType.PageSize = 999;
		lstGrouping.DataBind();
		lstGrouping.Items.Insert(0,new ListItem("",""));
		lstGrouping.DataSourceID = "";
		lstGrouping.SelectedIndex = 0;
	}

	private void InitPage() {
		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);

		lstSiteCd.SelectedIndex = 0;
		lblManAttrTypeSeq.Text = "";
		ClearField();
		pnlMainte.Visible = false;
		vdrRowCount.Enabled = false;
		vdeRowCount.Enabled = false;
		lstSeekSiteCd.DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		DataBind();
	}

	private void ClearField() {
		txtManAttrTypeNm.Text = string.Empty;
		txtManAttrTypeFindNm.Text = string.Empty;
		txtPriority.Text = string.Empty;
		txtRowCount.Text = string.Empty;
		txtItemNo.Text = string.Empty;
		lstGrouping.SelectedIndex = 0;
		lstInputType.SelectedIndex = 0;
		chkNaFlag.Checked = false;
		chkProfileReqIitemFlag.Checked = false;
		chkOmitSeekContionFlag.Checked = true;
		chkRegistInputFlag.Checked = false;
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		lstSiteCd.SelectedIndex = lstSeekSiteCd.SelectedIndex;
		lblManAttrTypeSeq.Text = "0";
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void lnkUserManAttrTypeSeq_Command(object sender,CommandEventArgs e) {
		lblManAttrTypeSeq.Text = e.CommandArgument.ToString();
		GetData();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	private void GetList() {
		grdManAttrType.PageIndex = 0;
		grdManAttrType.DataBind();
		pnlCount.DataBind();
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("USER_MAN_ATTR_TYPE_GET");
			db.ProcedureInParm("PMAN_ATTR_TYPE_SEQ",DbSession.DbType.NUMBER,lblManAttrTypeSeq.Text);
			db.ProcedureOutParm("PSITE_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMAN_ATTR_TYPE_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMAN_ATTR_TYPE_FIND_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPRIORITY",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PINPUT_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PGROUPING_CATEGORY_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PITEM_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PNA_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PPROFILE_REQ_ITEM_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("POMIT_SEEK_CONTION_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PREGIST_INPUT_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROW_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				lstSiteCd.SelectedValue = db.GetStringValue("PSITE_CD");
				txtManAttrTypeNm.Text = db.GetStringValue("PMAN_ATTR_TYPE_NM");
				txtManAttrTypeFindNm.Text = db.GetStringValue("PMAN_ATTR_TYPE_FIND_NM");
				txtPriority.Text = db.GetStringValue("PPRIORITY");
				txtItemNo.Text = db.GetStringValue("PITEM_NO");
				txtRowCount.Text = db.GetStringValue("PROW_COUNT");
				lstInputType.SelectedValue = db.GetStringValue("PINPUT_TYPE");
				chkNaFlag.Checked = (db.GetStringValue("PNA_FLAG").Equals("0") == false);
				chkProfileReqIitemFlag.Checked = (db.GetStringValue("PPROFILE_REQ_ITEM_FLAG").Equals("1"));
				chkOmitSeekContionFlag.Checked = db.GetStringValue("POMIT_SEEK_CONTION_FLAG").Equals("0");
				chkRegistInputFlag.Checked = db.GetStringValue("PREGIST_INPUT_FLAG").Equals("1");
				if (!db.GetStringValue("PGROUPING_CATEGORY_CD").Equals("")) {
					lstGrouping.SelectedValue = db.GetStringValue("PGROUPING_CATEGORY_CD");
				}
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;

	}

	private void UpdateData(int pDelFlag) {
		int iNaFlag = 0;
		int iProfileReqIitemFlag = 0;

		if (chkNaFlag.Checked) {
			iNaFlag = 1;
		}

		if (chkProfileReqIitemFlag.Checked) {
			iProfileReqIitemFlag = 1;
		}

		if (txtRowCount.Text.Equals(string.Empty)) {
			txtRowCount.Text = "0";
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("USER_MAN_ATTR_TYPE_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PMAN_ATTR_TYPE_SEQ",DbSession.DbType.NUMBER,lblManAttrTypeSeq.Text);
			db.ProcedureInParm("PMAN_ATTR_TYPE_NM",DbSession.DbType.VARCHAR2,txtManAttrTypeNm.Text);
			db.ProcedureInParm("PMAN_ATTR_TYPE_FIND_NM",DbSession.DbType.VARCHAR2,txtManAttrTypeFindNm.Text);
			db.ProcedureInParm("PPRIORITY",DbSession.DbType.NUMBER,txtPriority.Text);
			db.ProcedureInParm("PINPUT_TYPE",DbSession.DbType.VARCHAR2,lstInputType.SelectedValue);
			db.ProcedureInParm("PGROUPING_CATEGORY_CD",DbSession.DbType.VARCHAR2,lstGrouping.SelectedValue);
			db.ProcedureInParm("PITEM_NO",DbSession.DbType.VARCHAR2,txtItemNo.Text);
			db.ProcedureInParm("PNA_FLAG",DbSession.DbType.NUMBER,iNaFlag);
			db.ProcedureInParm("PPROFILE_REQ_ITEM_FLAG",DbSession.DbType.NUMBER,iProfileReqIitemFlag);
			db.ProcedureInParm("POMIT_SEEK_CONTION_FLAG",DbSession.DbType.NUMBER,!chkOmitSeekContionFlag.Checked);
			db.ProcedureInParm("PREGIST_INPUT_FLAG",DbSession.DbType.NUMBER,chkRegistInputFlag.Checked);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			if (lstInputType.SelectedValue == ViCommConst.INPUT_TYPE_TEXT) {
				db.ProcedureInParm("PROW_COUNT",DbSession.DbType.NUMBER,int.Parse(txtRowCount.Text));
			} else {
				db.ProcedureInParm("PROW_COUNT",DbSession.DbType.NUMBER,0);
			}
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		int iIdx = lstSiteCd.SelectedIndex;
		InitPage();
		lstSeekSiteCd.SelectedIndex = iIdx;
		GetList();
	}

	protected void dsUserManAttrType_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
	}
	protected void vdcRowCount_ServerValidate(object source,ServerValidateEventArgs args) {
		if (lstInputType.SelectedValue == ViCommConst.INPUT_TYPE_TEXT) {
			vdrRowCount.Enabled = true;
			vdeRowCount.Enabled = true;
		} else {
			vdrRowCount.Enabled = false;
			vdeRowCount.Enabled = false;
		}
	}
}
