﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PictographSample.aspx.cs"
    Inherits="Site_PictographSample" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>絵文字一覧</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div id="header">
                <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnableScriptGlobalization="True"
                    EnableScriptLocalization="True">
                </ajaxToolkit:ToolkitScriptManager>
            </div>
            <div id="title">
                <asp:Label ID="lblPgmTitle" runat="server" Text="絵文字一覧"></asp:Label>
            </div>
            <div id="contentright">
                <table border="0" style="width: 400px; font-size: small" class="tableStyle" cellpadding="0">
                    <tr>
                        <td class="tdHeaderStyle2">
                            キャリア
                        </td>
                        <td class="tdDataStyle">
                            <asp:CheckBox ID="chkDocomo" runat="server" Text="DoCoMo" />
                            <asp:CheckBox ID="chkKddi" runat="server" Text="Au" />
                            <asp:CheckBox ID="chkSoftBank" runat="server" Text="SoftBank" />
                        </td>
                    </tr>
                </table>
                <asp:Button ID="btnListSeek" runat="server" OnClick="btnListSeek_Click" Text="検索" />
                <asp:Panel ID="pnlGrid" runat="server">
                    <asp:GridView ID="grdPictograph" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        DataSourceID="dsPictograph" EnableViewState="False" PageSize="40" SkinID="GridViewColor">
                        <Columns>
                            <asp:TemplateField HeaderText="コード">
                                <ItemTemplate>
                                    $NO_TRANS_START;
                                    <asp:Label ID="lblCommonCode0" runat="server" Text='<%# Eval("CommonCode0") %>'>
                                    </asp:Label>
                                    $NO_TRANS_END;
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="CommonCode0" HeaderText="絵" />
                            <asp:BoundField DataField="Text0" HeaderText="テキスト表記" />
                            <asp:TemplateField HeaderText="DoCoMo">
                                <ItemTemplate>
                                    <asp:Label ID="lblDocomo0" runat="server" Text='<%# GetAvailableMark(Eval("CommonCode0"), Eval("DocomoShiftJIS0")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="au">
                                <ItemTemplate>
                                    <asp:Label ID="lblAu0" runat="server" Text='<%# GetAvailableMark(Eval("CommonCode0"), Eval("KddiShiftJIS0")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="SoftBank">
                                <ItemTemplate>
                                    <asp:Label ID="lblSoftbank0" runat="server" Text='<%# GetAvailableMark(Eval("CommonCode0"), Eval("SoftbankShiftJIS0")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="80px" />
                            </asp:TemplateField>
<%--                            <asp:TemplateField ItemStyle-BackColor="white" HeaderStyle-BackColor="white"></asp:TemplateField>
                            <asp:TemplateField HeaderText="コード">
                                <ItemTemplate>
                                    $NO_TRANS_START;
                                    <asp:Label ID="lblCommonCode1" runat="server" Text='<%# Eval("CommonCode1") %>'>
                                    </asp:Label>
                                    $NO_TRANS_END;
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="CommonCode1" HeaderText="絵" />
                            <asp:BoundField DataField="Text1" HeaderText="テキスト表記" />
                            <asp:TemplateField HeaderText="DoCoMo">
                                <ItemTemplate>
                                    <asp:Label ID="lblDocomo1" runat="server" Text='<%# GetAvailableMark(Eval("CommonCode1"), Eval("DocomoShiftJIS1")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="au">
                                <ItemTemplate>
                                    <asp:Label ID="lblAu1" runat="server" Text='<%# GetAvailableMark(Eval("CommonCode1"), Eval("KddiShiftJIS1")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="SoftBank">
                                <ItemTemplate>
                                    <asp:Label ID="lblSoftbank1" runat="server" Text='<%# GetAvailableMark(Eval("CommonCode1"), Eval("SoftbankShiftJIS1")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="80px" />
                            </asp:TemplateField>--%>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                    </asp:GridView>
                    <asp:ObjectDataSource ID="dsPictograph" runat="server" EnablePaging="true" SelectCountMethod="GetPageCount"
                        SelectMethod="GetPageCollection" TypeName="Pictograph" OnSelecting="dsPictograph_Selecting">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="1" Name="pColumnCount" Type="Int32" />
                            <asp:Parameter Name="pDocomoFlag" Type="boolean" />
                            <asp:Parameter Name="pKddiFlag" Type="boolean" />
                            <asp:Parameter Name="pSoftbankFlag" Type="boolean" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </asp:Panel>
            </div>
            <div id="footer" style="width: auto;">
                <p>
                    &copy;
                    <asp:Label ID="lblCopyrightYear" runat="server" Text="2011" />
                    <asp:Label ID="lblCopyrightName" runat="server" Text="Powerd by" EnableViewState="false" />
                    <a href="http://www.ibrid.co.jp" target="_top">iBrid Co. Ltd.</a>
                </p>
            </div>
        </div>
    </form>
</body>
</html>
