﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="OmikujiMainte.aspx.cs" Inherits="Site_OmikujiMainte" 
    Title="おみくじ設定" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="おみくじ設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <fieldset class="fieldset">
            <legend>[検索条件]</legend>
            <fieldset class="fieldset-inner">
                <table border="0" style="width: 320px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle2">
                            サイトコード
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="180px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click"
                    CausesValidation="False" />
            </fieldset>
        </fieldset>
        <asp:Panel runat="server" ID="pnlMainte">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel runat="server" ID="pnlDtl">
                    <table border="0" style="width: 320px" class="tableStyle">
                            <tr>
                                <td class="tdHeaderStyle2">
                                    サイトコード
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                        Enabled="false" DataValueField="SITE_CD" Width="180px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                    </table><br />
                    <asp:PlaceHolder ID="plcManHeader" runat="server" Visible="true">
                    <table border="0" style="width: 320px" class="tableStyle">
                    
                        <tr>
                            <td class="tdHeaderSmallStyle" colspan="2" align="center">
                                入金あり
                            </td>
                            <td class="tdHeaderSmallStyle2L" colspan="2" align="center">
                                入金なし
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderSmallStyle2" align="center">
                                おみくじP
                            </td>
                            <td class="tdHeaderSmallStyleL" align="center">
                                確率
                            </td>
                            <td class="tdHeaderSmallStyleL" align="center">
                                おみくじP
                            </td>
                            <td class="tdHeaderSmallStyleL" align="center">
                                確率
                            </td>
                        </tr>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="plcWomanHeader" runat="server" Visible="false">
                    <table border="0" style="width: 160px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderSmallStyle2" align="center">
                                おみくじP
                            </td>
                            <td class="tdHeaderSmallStyleL" align="center">
                                確率
                            </td>
                        </tr>
                    </asp:PlaceHolder>
                        <tr>
                            <td class="tdDataStyle" >
                                <asp:TextBox ID="txtOmikujiPointNormal1" runat="server" MaxLength="5" Width="35" />P
                                <asp:RangeValidator ID="vdrOmikujiPointNormal1" runat="server" ControlToValidate="txtOmikujiPointNormal1"
                                    ErrorMessage="おみくじﾎﾟｲﾝﾄを0P以上の数値で入力して下さい。" MaximumValue="99999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender1"
                                    TargetControlID="vdrOmikujiPointNormal1" HighlightCssClass="validatorCallout" />
                                    
                                <ajaxToolkit:FilteredTextBoxExtender ID="fteOmikujiPointNormal1" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtOmikujiPointNormal1" />
                            </td>
                            <td class="tdDataStyle" >
                                <asp:TextBox ID="txtOmikujiPointNormalRate1" runat="server" MaxLength="3" Width="21" />% 
                                <asp:RangeValidator ID="vdrOmikujiPointNormalRate1" runat="server" ControlToValidate="txtOmikujiPointNormalRate1"
                                    ErrorMessage="おみくじ確立を0%以上の数値で入力して下さい。" MaximumValue="999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender11"
                                    TargetControlID="vdrOmikujiPointNormalRate1" HighlightCssClass="validatorCallout" />
                                
                                <asp:RequiredFieldValidator ID="vdeOmikujiPointNormalRate1" runat="server" ControlToValidate="txtOmikujiPointNormalRate1" 
                                    ErrorMessage="おみくじ確立を数値で入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender17"
                                    TargetControlID="vdeOmikujiPointNormalRate1" HighlightCssClass="validatorCallout" />
                                
                                
                                <ajaxToolkit:FilteredTextBoxExtender ID="fteOmikujiPointNormalRate1" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtOmikujiPointNormalRate1" />
                            </td>
                            <asp:PlaceHolder ID="plcNoReciptContents1" runat="server" Visible="true">
                            <td class="tdDataStyle" >
                                <asp:TextBox ID="txtOmikujiPointNoReceipt1" runat="server" MaxLength="5" Width="35" />P
                                <asp:RangeValidator ID="vdrOmikujiPointNoReceipt1" runat="server" ControlToValidate="txtOmikujiPointNoReceipt1"
                                    ErrorMessage="おみくじﾎﾟｲﾝﾄを0P以上の数値で入力して下さい。" MaximumValue="99999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender2"
                                    TargetControlID="vdrOmikujiPointNoReceipt1" HighlightCssClass="validatorCallout" />
                                    
                                <ajaxToolkit:FilteredTextBoxExtender ID="fteOmikujiPointNoReceipt1" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtOmikujiPointNoReceipt1" />
                            </td>
                            <td class="tdDataStyle" >
                                <asp:TextBox ID="txtOmikujiPointNoReceiptRate1" runat="server" MaxLength="3" Width="21" />%
                                <asp:RangeValidator ID="vdrOmikujiPointNoReceiptRate1" runat="server" ControlToValidate="txtOmikujiPointNoReceiptRate1"
                                    ErrorMessage="おみくじ確立を0%以上の数値で入力して下さい。" MaximumValue="999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender12"
                                    TargetControlID="vdrOmikujiPointNoReceiptRate1" HighlightCssClass="validatorCallout" />
                             
                                <asp:RequiredFieldValidator ID="vdeOmikujiPointNoReceiptRate1" runat="server" ControlToValidate="txtOmikujiPointNoReceiptRate1" 
                                    ErrorMessage="おみくじ確立を数値で入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender18"
                                    TargetControlID="vdeOmikujiPointNoReceiptRate1" HighlightCssClass="validatorCallout" />
                             
                                <ajaxToolkit:FilteredTextBoxExtender ID="fteOmikujiPointNoReceiptRate1" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtOmikujiPointNoReceiptRate1" />
                            </td>
                            </asp:PlaceHolder>
                        </tr>
                        <tr>
                            <td class="tdDataStyle" >
                                <asp:TextBox ID="txtOmikujiPointNormal2" runat="server" MaxLength="5" Width="35" />P
                                <asp:RangeValidator ID="vdrOmikujiPointNormal2" runat="server" ControlToValidate="txtOmikujiPointNormal2"
                                    ErrorMessage="おみくじﾎﾟｲﾝﾄを0P以上の数値で入力して下さい。" MaximumValue="99999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender3"
                                    TargetControlID="vdrOmikujiPointNormal2" HighlightCssClass="validatorCallout" />
                                    
                                <ajaxToolkit:FilteredTextBoxExtender ID="fteOmikujiPointNormal2" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtOmikujiPointNormal2" />
                            </td>
                            <td class="tdDataStyle" >
                                <asp:TextBox ID="txtOmikujiPointNormalRate2" runat="server" MaxLength="3" Width="21" />%
                                <asp:RangeValidator ID="vdrOmikujiPointNormalRate2" runat="server" ControlToValidate="txtOmikujiPointNormalRate2"
                                    ErrorMessage="おみくじ確立を0%以上の数値で入力して下さい。" MaximumValue="999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender22"
                                    TargetControlID="vdrOmikujiPointNormalRate2" HighlightCssClass="validatorCallout" />

                                <asp:RequiredFieldValidator ID="vdeOmikujiPointNormalRate2" runat="server" ControlToValidate="txtOmikujiPointNormalRate2" 
                                    ErrorMessage="おみくじ確立を数値で入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender19"
                                    TargetControlID="vdeOmikujiPointNormalRate2" HighlightCssClass="validatorCallout" />
                                    
                                <ajaxToolkit:FilteredTextBoxExtender ID="fteOmikujiPointNormalRate2" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtOmikujiPointNormalRate2" />
                            </td>
                            <asp:PlaceHolder ID="plcNoReciptContents2" runat="server" Visible="true">
                            <td class="tdDataStyle" >
                                <asp:TextBox ID="txtOmikujiPointNoReceipt2" runat="server" MaxLength="5" Width="35" />P
                                <asp:RangeValidator ID="vdrOmikujiPointNoReceipt2" runat="server" ControlToValidate="txtOmikujiPointNoReceipt2"
                                    ErrorMessage="おみくじﾎﾟｲﾝﾄを0P以上の数値で入力して下さい。" MaximumValue="99999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender4"
                                    TargetControlID="vdrOmikujiPointNoReceipt2" HighlightCssClass="validatorCallout" />
                                    
                                <ajaxToolkit:FilteredTextBoxExtender ID="fteOmikujiPointNoReceipt2" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtOmikujiPointNoReceipt2" />
                            </td>
                            <td class="tdDataStyle" >
                                <asp:TextBox ID="txtOmikujiPointNoReceiptRate2" runat="server" MaxLength="3" Width="21" />%
                                <asp:RangeValidator ID="vdrOmikujiPointNoReceiptRate2" runat="server" ControlToValidate="txtOmikujiPointNoReceiptRate2"
                                    ErrorMessage="おみくじ確立を0%以上の数値で入力して下さい。" MaximumValue="999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender13"
                                    TargetControlID="vdrOmikujiPointNoReceiptRate2" HighlightCssClass="validatorCallout" />

                                <asp:RequiredFieldValidator ID="vdeOmikujiPointNoReceiptRate2" runat="server" ControlToValidate="txtOmikujiPointNoReceiptRate2" 
                                    ErrorMessage="おみくじ確立を数値で入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender20"
                                    TargetControlID="vdeOmikujiPointNoReceiptRate2" HighlightCssClass="validatorCallout" />

                                <ajaxToolkit:FilteredTextBoxExtender ID="fteOmikujiPointNoReceiptRate2" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtOmikujiPointNoReceiptRate2" />
                            </td>
                            </asp:PlaceHolder>
                        </tr>
                        <tr>
                            <td class="tdDataStyle" >
                                <asp:TextBox ID="txtOmikujiPointNormal3" runat="server" MaxLength="5" Width="35" />P
                                <asp:RangeValidator ID="vdrOmikujiPointNormal3" runat="server" ControlToValidate="txtOmikujiPointNormal3"
                                    ErrorMessage="おみくじﾎﾟｲﾝﾄを0P以上の数値で入力して下さい。" MaximumValue="99999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender5"
                                    TargetControlID="vdrOmikujiPointNormal3" HighlightCssClass="validatorCallout" />
                                    
                                <ajaxToolkit:FilteredTextBoxExtender ID="fteOmikujiPointNormal3" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtOmikujiPointNormal3" />
                            </td>
                            <td class="tdDataStyle" >
                                <asp:TextBox ID="txtOmikujiPointNormalRate3" runat="server" MaxLength="3" Width="21" />%
                                <asp:RangeValidator ID="vdrOmikujiPointNormalRate3" runat="server" ControlToValidate="txtOmikujiPointNormalRate3"
                                    ErrorMessage="おみくじ確立を0%以上の数値で入力して下さい。" MaximumValue="999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender33"
                                    TargetControlID="vdrOmikujiPointNormalRate3" HighlightCssClass="validatorCallout" />
                                    
                                <asp:RequiredFieldValidator ID="vdeOmikujiPointNormalRate3" runat="server" ControlToValidate="txtOmikujiPointNormalRate3" 
                                    ErrorMessage="おみくじ確立を数値で入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender21"
                                    TargetControlID="vdeOmikujiPointNormalRate3" HighlightCssClass="validatorCallout" />
                                    
                                <ajaxToolkit:FilteredTextBoxExtender ID="fteOmikujiPointNormalRate3" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtOmikujiPointNormalRate3" />
                            </td>
                            <asp:PlaceHolder ID="plcNoReciptContents3" runat="server" Visible="true">
                            <td class="tdDataStyle" >
                                <asp:TextBox ID="txtOmikujiPointNoReceipt3" runat="server" MaxLength="5" Width="35" />P
                                <asp:RangeValidator ID="vdrOmikujiPointNoReceipt3" runat="server" ControlToValidate="txtOmikujiPointNoReceipt3"
                                    ErrorMessage="おみくじﾎﾟｲﾝﾄを0P以上の数値で入力して下さい。" MaximumValue="99999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender6"
                                    TargetControlID="vdrOmikujiPointNoReceipt3" HighlightCssClass="validatorCallout" />
                                    
                                <ajaxToolkit:FilteredTextBoxExtender ID="fteOmikujiPointNoReceipt3" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtOmikujiPointNoReceipt3" />
                            </td>
                            <td class="tdDataStyle" >
                                <asp:TextBox ID="txtOmikujiPointNoReceiptRate3" runat="server" MaxLength="3" Width="21" />%
                                <asp:RangeValidator ID="vdrOmikujiPointNoReceiptRate3" runat="server" ControlToValidate="txtOmikujiPointNoReceiptRate3"
                                    ErrorMessage="おみくじ確立を0%以上の数値で入力して下さい。" MaximumValue="999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender14"
                                    TargetControlID="vdrOmikujiPointNoReceiptRate3" HighlightCssClass="validatorCallout" />
                                    
                                <asp:RequiredFieldValidator ID="vdeOmikujiPointNoReceiptRate3" runat="server" ControlToValidate="txtOmikujiPointNoReceiptRate3" 
                                    ErrorMessage="おみくじ確立を数値で入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender23"
                                    TargetControlID="vdeOmikujiPointNoReceiptRate3" HighlightCssClass="validatorCallout" />
                                    
                                <ajaxToolkit:FilteredTextBoxExtender ID="fteOmikujiPointNoReceiptRate3" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtOmikujiPointNoReceiptRate3" />
                            </td>
                            </asp:PlaceHolder>
                        </tr>
                        <tr>
                            <td class="tdDataStyle" >
                                <asp:TextBox ID="txtOmikujiPointNormal4" runat="server" MaxLength="5" Width="35" />P
                                <asp:RangeValidator ID="vdrOmikujiPointNormal4" runat="server" ControlToValidate="txtOmikujiPointNormal4"
                                    ErrorMessage="おみくじﾎﾟｲﾝﾄを0P以上の数値で入力して下さい。" MaximumValue="99999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender7"
                                    TargetControlID="vdrOmikujiPointNormal4" HighlightCssClass="validatorCallout" />
                                    
                                <ajaxToolkit:FilteredTextBoxExtender ID="fteOmikujiPointNormal4" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtOmikujiPointNormal4" />
                            </td>
                            <td class="tdDataStyle" >
                                <asp:TextBox ID="txtOmikujiPointNormalRate4" runat="server" MaxLength="3" Width="21" />%
                                <asp:RangeValidator ID="vdrOmikujiPointNormalRate4" runat="server" ControlToValidate="txtOmikujiPointNormalRate4"
                                    ErrorMessage="おみくじ確立を0%以上の数値で入力して下さい。" MaximumValue="999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender44"
                                    TargetControlID="vdrOmikujiPointNormalRate4" HighlightCssClass="validatorCallout" />

                                <asp:RequiredFieldValidator ID="vdeOmikujiPointNormalRate4" runat="server" ControlToValidate="txtOmikujiPointNormalRate4" 
                                    ErrorMessage="おみくじ確立を数値で入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender24"
                                    TargetControlID="vdeOmikujiPointNormalRate4" HighlightCssClass="validatorCallout" />
                                    
                                <ajaxToolkit:FilteredTextBoxExtender ID="fteOmikujiPointNormalRate4" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtOmikujiPointNormalRate4" />
                            </td>
                            <asp:PlaceHolder ID="plcNoReciptContents4" runat="server" Visible="true">
                            <td class="tdDataStyle" >
                                <asp:TextBox ID="txtOmikujiPointNoReceipt4" runat="server" MaxLength="5" Width="35" />P
                                <asp:RangeValidator ID="vdrOmikujiPointNoReceipt4" runat="server" ControlToValidate="txtOmikujiPointNoReceipt4"
                                    ErrorMessage="おみくじﾎﾟｲﾝﾄを0P以上の数値で入力して下さい。" MaximumValue="99999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender8"
                                    TargetControlID="vdrOmikujiPointNoReceipt4" HighlightCssClass="validatorCallout" />
                                    
                                <ajaxToolkit:FilteredTextBoxExtender ID="fteOmikujiPointNoReceipt4" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtOmikujiPointNoReceipt4" />
                            </td>
                            <td class="tdDataStyle" >
                                <asp:TextBox ID="txtOmikujiPointNoReceiptRate4" runat="server" MaxLength="3" Width="21" />%
                                <asp:RangeValidator ID="vdrOmikujiPointNoReceiptRate4" runat="server" ControlToValidate="txtOmikujiPointNoReceiptRate4"
                                    ErrorMessage="おみくじ確立を0%以上の数値で入力して下さい。" MaximumValue="999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender15"
                                    TargetControlID="vdrOmikujiPointNoReceiptRate4" HighlightCssClass="validatorCallout" />
                                    
                                <asp:RequiredFieldValidator ID="vdeOmikujiPointNoReceiptRate4" runat="server" ControlToValidate="txtOmikujiPointNoReceiptRate4" 
                                    ErrorMessage="おみくじ確立を数値で入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender25"
                                    TargetControlID="vdeOmikujiPointNoReceiptRate4" HighlightCssClass="validatorCallout" />
                                    
                                <ajaxToolkit:FilteredTextBoxExtender ID="fteOmikujiPointNoReceiptRate4" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtOmikujiPointNoReceiptRate4" />
                            </td>
                            </asp:PlaceHolder>
                        </tr>
                        <tr>
                            <td class="tdDataStyle" >
                                <asp:TextBox ID="txtOmikujiPointNormal5" runat="server" MaxLength="5" Width="35" />P
                                <asp:RangeValidator ID="vdrOmikujiPointNormal5" runat="server" ControlToValidate="txtOmikujiPointNormal5"
                                    ErrorMessage="おみくじﾎﾟｲﾝﾄを0P以上の数値で入力して下さい。" MaximumValue="99999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender9"
                                    TargetControlID="vdrOmikujiPointNormal5" HighlightCssClass="validatorCallout" />
                                    
                                <ajaxToolkit:FilteredTextBoxExtender ID="fteOmikujiPointNormal5" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtOmikujiPointNormal5" />
                            </td>
                            <td class="tdDataStyle" >
                                <asp:TextBox ID="txtOmikujiPointNormalRate5" runat="server" MaxLength="3" Width="21" />%
                                <asp:RangeValidator ID="vdrOmikujiPointNormalRate5" runat="server" ControlToValidate="txtOmikujiPointNormalRate5"
                                    ErrorMessage="おみくじ確立を0%以上の数値で入力して下さい。" MaximumValue="999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender55"
                                    TargetControlID="vdrOmikujiPointNormalRate5" HighlightCssClass="validatorCallout" />
                                    
                                <asp:RequiredFieldValidator ID="vdeOmikujiPointNormalRate5" runat="server" ControlToValidate="txtOmikujiPointNormalRate5" 
                                    ErrorMessage="おみくじ確立を数値で入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender26"
                                    TargetControlID="vdeOmikujiPointNormalRate5" HighlightCssClass="validatorCallout" />
                                    
                                <ajaxToolkit:FilteredTextBoxExtender ID="fteOmikujiPointNormalRate5" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtOmikujiPointNormalRate5" />
                            </td>
                            <asp:PlaceHolder ID="plcNoReciptContents5" runat="server" Visible="true">
                            <td class="tdDataStyle" >
                                <asp:TextBox ID="txtOmikujiPointNoReceipt5" runat="server" MaxLength="5" Width="35" />P
                                <asp:RangeValidator ID="vdrOmikujiPointNoReceipt5" runat="server" ControlToValidate="txtOmikujiPointNoReceipt5"
                                    ErrorMessage="おみくじﾎﾟｲﾝﾄを0P以上の数値で入力して下さい。" MaximumValue="99999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender10"
                                    TargetControlID="vdrOmikujiPointNoReceipt5" HighlightCssClass="validatorCallout" />
                                    
                                <ajaxToolkit:FilteredTextBoxExtender ID="fteOmikujiPointNoReceipt5" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtOmikujiPointNoReceipt5" />
                            </td>
                            <td class="tdDataStyle" >
                                <asp:TextBox ID="txtOmikujiPointNoReceiptRate5" runat="server" MaxLength="3" Width="21" />%
                                <asp:RangeValidator ID="vdrOmikujiPointNoReceiptRate5" runat="server" ControlToValidate="txtOmikujiPointNoReceiptRate5"
                                    ErrorMessage="おみくじ確立を0%以上の数値で入力して下さい。" MaximumValue="999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender16"
                                    TargetControlID="vdrOmikujiPointNoReceiptRate5" HighlightCssClass="validatorCallout" />

                                <asp:RequiredFieldValidator ID="vdeOmikujiPointNoReceiptRate5" runat="server" ControlToValidate="txtOmikujiPointNoReceiptRate5" 
                                    ErrorMessage="おみくじ確立を数値で入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender27"
                                    TargetControlID="vdeOmikujiPointNoReceiptRate5" HighlightCssClass="validatorCallout" />
                                    
                                <ajaxToolkit:FilteredTextBoxExtender ID="fteOmikujiPointNoReceiptRate5" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtOmikujiPointNoReceiptRate5" />
                            </td>
                            </asp:PlaceHolder>
                        </tr>
                    </table>
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click"
                        ValidationGroup="Detail" />
                    <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click"
                        CausesValidation="False" />
                    <asp:Label runat="server" ID="lblErrorMessage" ForeColor="red" Visible="false"></asp:Label>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
</asp:Content>