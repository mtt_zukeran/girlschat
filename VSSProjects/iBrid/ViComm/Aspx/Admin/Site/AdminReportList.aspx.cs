﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 管理者連絡
--	Progaram ID		: AdminReportList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Site_AdminReportList:System.Web.UI.Page {
	private string recCount = "";

	private bool DisablePinEdit {
		get {
			return (bool)this.ViewState["DisablePinEdit"];
		}
		set {
			this.ViewState["DisablePinEdit"] = value;
		}
	}

	private string HtmlDoc {
		get {
			if (this.DisablePinEdit) {
				return this.txtHtmlDocText.Text;
			} else {
				return this.txtHtmlDoc.Text;
			}
		}
		set {
			if (this.DisablePinEdit) {
				this.txtHtmlDocText.Text = value;
			} else {
				this.txtHtmlDoc.Text = value;
			}
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		txtHtmlDoc.EditorDirectory = string.Format("http://{0}:{1}/pinEdit/editor/",Request.UrlReferrer.Host,Request.UrlReferrer.Port);
		if (!IsPostBack) {
			using (ManageCompany oManageCompany = new ManageCompany()) {
				this.DisablePinEdit = oManageCompany.IsAvailableService(ViCommConst.RELEASE_DISABLE_PINEDIT,2);
				this.txtHtmlDoc.Visible = !this.DisablePinEdit;
				this.txtHtmlDocText.Visible = this.DisablePinEdit;
			}
			InitPage();
		}
	}

	protected void dsAdminReport_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		grdAdminReport.PageSize = int.Parse(Session["PageSize"].ToString());
		ClearField();
		pnlMainte.Visible = false;
		lstSeekSiteCd.DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		DataBind();
	}

	private void ClearField() {
		lstSiteCd.SelectedIndex = -1;
		txtDocTitle.Text = "";
		this.HtmlDoc = "";
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		lblDocSeq.Text = "0";
		GetData();
		txtStartPubDay.Text = DateTime.Now.ToString("yyyy/MM/dd");
		txtReportHourMin.Text = string.Empty;
		lstSiteCd.Enabled = false;
		lstSiteCd.SelectedIndex = lstSeekSiteCd.SelectedIndex;
	}

	protected void lnkDocSeq_Command(object sender,CommandEventArgs e) {
		lblDocSeq.Text = e.CommandArgument.ToString();
		GetData();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void dsAdminReport_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
		e.InputParameters[1] = ViCommConst.OPERATOR;
	}

	private void GetList() {
		grdAdminReport.PageIndex = 0;
		grdAdminReport.DataBind();
		pnlCount.DataBind();
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ADMIN_REPORT_GET");
			db.ProcedureInParm("PDOC_SEQ",DbSession.DbType.VARCHAR2,lblDocSeq.Text);
			db.ProcedureOutParm("PSITE_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PDOC_TITLE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTART_PUB_DAY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREPORT_HOUR_MIN", DbSession.DbType.VARCHAR2);
			db.ProcedureOutArrayParm("PHTML_DOC",DbSession.DbType.VARCHAR2,SysConst.MAX_HTML_BLOCKS);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				lstSiteCd.SelectedValue = db.GetStringValue("PSITE_CD");
				txtDocTitle.Text = db.GetStringValue("PDOC_TITLE");
				txtStartPubDay.Text = db.GetStringValue("PSTART_PUB_DAY");
				txtReportHourMin.Text = db.GetStringValue("PREPORT_HOUR_MIN");
				for (int i = 0;i < ViCommConst.ADMIN_REPORT_HTML_DOC_MAX ;i++) {
					this.HtmlDoc = this.HtmlDoc + db.GetArryStringValue("PHTML_DOC",i);
				}
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
	}

	private void UpdateData(int pDelFlag) {
		string[] sDoc;
		int iDocCount;

		SysPrograms.SeparateHtml(HttpUtility.HtmlDecode(this.HtmlDoc),ViCommConst.ADMIN_REPORT_HTML_DOC_MAX,out sDoc,out iDocCount);

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ADMIN_REPORT_MAINTE");
			db.ProcedureInParm("PDOC_SEQ",DbSession.DbType.VARCHAR2,lblDocSeq.Text);
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PDOC_TITLE",DbSession.DbType.VARCHAR2,txtDocTitle.Text);
			db.ProcedureInParm("PSTART_PUB_DAY",DbSession.DbType.VARCHAR2,txtStartPubDay.Text);
			db.ProcedureInParm("PREPORT_HOUR_MIN", DbSession.DbType.VARCHAR2, txtReportHourMin.Text);
			db.ProcedureInArrayParm("PHTML_DOC",DbSession.DbType.VARCHAR2,iDocCount,sDoc);
			db.ProcedureInParm("PHTML_DOC_COUNT",DbSession.DbType.NUMBER,iDocCount);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,ViewState["REVISION_NO"].ToString());
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureInParm("PSEX_CD",DbSession.DbType.NUMBER,ViCommConst.OPERATOR);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		int iIdx = lstSiteCd.SelectedIndex;
		InitPage();
		lstSeekSiteCd.SelectedIndex = iIdx;
		GetList();
	}
	
	protected string GetHtmlDoc( ) {
		string sHtmlDoc = "";
		
		sHtmlDoc += Eval("HTML_DOC1");
		sHtmlDoc += Eval("HTML_DOC2");
		sHtmlDoc += Eval("HTML_DOC3");
		sHtmlDoc += Eval("HTML_DOC4");
		sHtmlDoc += Eval("HTML_DOC5");
		
		return sHtmlDoc;
	}
}
