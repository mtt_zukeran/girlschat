﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者カテゴリーメンテナンス
--	Progaram ID		: ActCategoryList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Site_ActCategoryList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_SITE_OWNER)) {
			throw new ApplicationException("ViComm権限違反");
		}
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
	}

	private void FirstLoad() {
		grdActCategory.PageSize = 999;
		string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		if (sSiteCd.Equals("")) {
			if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
				lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			} else {
				lstSeekSiteCd.SelectedIndex = 0;
			}
		} else {
			lstSeekSiteCd.SelectedValue = sSiteCd;
		}
		DataBind();
		lstWebFaceSeq.DataSourceID = "";
		lstWebFaceSeq.SelectedIndex = 0;
	}

	private void InitPage() {
		lblActCategorySeq.Text = "";
		ClearField();
		pnlMainte.Visible = false;

		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);
		DisplayWordUtil.ReplaceGridColumnHeader(this.grdActCategory);
	}

	private void ClearField() {
		txtActCategoryNm.Text = "";
		txtPriority.Text = "";
		chkNaFlag.Checked = false;
		chkDisplayFlag.Checked = false;
		txtHtmlDoc.Text = "";
		if (lstWebFaceSeq.Items.Count > 0) {
			lstWebFaceSeq.SelectedIndex = 0;
		}
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		lstSiteCd.SelectedIndex = lstSeekSiteCd.SelectedIndex;
		lblActCategorySeq.Text = "0";
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void lnkActCategorySeq_Command(object sender,CommandEventArgs e) {
		lblActCategorySeq.Text = e.CommandArgument.ToString();
		GetData();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	private void GetList() {
		grdActCategory.PageIndex = 0;
		grdActCategory.DataBind();
		pnlCount.DataBind();
	}

	protected void dsActCategory_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ACT_CATEGORY_GET");
			db.ProcedureInParm("PACT_CATEGORY_SEQ",DbSession.DbType.NUMBER,lblActCategorySeq.Text);
			db.ProcedureOutParm("PSITE_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PACT_CATEGORY_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPRIORITY",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PDISPLAY_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PHTML_DOC",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PWEB_FACE_SEQ",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PNA_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				lstSiteCd.SelectedValue = db.GetStringValue("PSITE_CD");
				txtActCategoryNm.Text = db.GetStringValue("PACT_CATEGORY_NM");
				txtPriority.Text = db.GetStringValue("PPRIORITY");
				txtHtmlDoc.Text = db.GetStringValue("PHTML_DOC");
				chkNaFlag.Checked = (db.GetStringValue("PNA_FLAG").Equals("0") == false);
				chkDisplayFlag.Checked = (db.GetStringValue("PDISPLAY_FLAG").Equals("0") == false);
				if (!db.GetStringValue("PWEB_FACE_SEQ").Equals("")) {
					lstWebFaceSeq.SelectedValue = db.GetStringValue("PWEB_FACE_SEQ");
				}
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;

	}

	private void UpdateData(int pDelFlag) {
		int iNaFlag = 0;
		int iDisplayFlag = 0;

		if (chkNaFlag.Checked) {
			iNaFlag = 1;
		}

		if (chkDisplayFlag.Checked) {
			iDisplayFlag = 1;
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ACT_CATEGORY_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PACT_CATEGORY_SEQ",DbSession.DbType.NUMBER,lblActCategorySeq.Text);
			db.ProcedureInParm("PACT_CATEGORY_NM",DbSession.DbType.VARCHAR2,txtActCategoryNm.Text);
			db.ProcedureInParm("PPRIORITY",DbSession.DbType.NUMBER,txtPriority.Text);
			db.ProcedureInParm("PDISPLAY_FLAG",DbSession.DbType.NUMBER,iDisplayFlag);
			db.ProcedureInParm("PHTML_DOC",DbSession.DbType.VARCHAR2,txtHtmlDoc.Text);
			db.ProcedureInParm("PWEB_FACE_SEQ",DbSession.DbType.VARCHAR2,lstWebFaceSeq.SelectedValue);
			db.ProcedureInParm("PNA_FLAG",DbSession.DbType.NUMBER,iNaFlag);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		int iIdx = lstSiteCd.SelectedIndex;
		InitPage();
		lstSeekSiteCd.SelectedIndex = iIdx;
		GetList();
	}

	protected void dsActCategory_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
	}

	protected bool IsEnabledExpandedCharge() {
		using (ManageCompany oCompany = new ManageCompany()) {
			return oCompany.IsAvailableService(ViCommConst.RELEASE_ENABLE_EXPANDED_CHARGE,2);
		}
	}
}
