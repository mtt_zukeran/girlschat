﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="PointReductionMainte.aspx.cs" Inherits="Site_PointReductionMainte" 
    Title="ポイント還元設定" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="ポイント還元設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
            <fieldset class="fieldset-inner">
                <legend>[検索条件]</legend>
                <table border="0" style="width: 800px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle2">
                            サイトコード
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="180px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    CausesValidation="False" />
            </fieldset>
        <asp:Panel runat="server" ID="pnlMainte" Visible="false">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel runat="server" ID="pnlDtl">
                    <table border="0" style="width: 820px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    Enabled="false" DataValueField="SITE_CD" Width="180px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                ポイント還元適応開始日時
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstFromYYYY" runat="server" Width="60px" DataSource='<%# YearArray %>'>
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstFromMM" runat="server" Width="40px" DataSource='<%# MonthArray %>'>
                                </asp:DropDownList>月
                                <asp:DropDownList ID="lstFromDD" runat="server" Width="40px" DataSource='<%# DayArray %>'>
                                </asp:DropDownList>日
                                <asp:DropDownList ID="lstFromHH" runat="server" Width="40px" DataSource='<%# HourArray %>'>
                                </asp:DropDownList>時
                                <asp:Label ID="lblErrorMessageFrom" runat="server" Text="" Visible="false" ForeColor="red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                ポイント還元適応終了日時
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstToYYYY" runat="server" Width="60px" DataSource='<%# YearArray %>'>
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstToMM" runat="server" Width="40px" DataSource='<%# MonthArray %>'>
                                </asp:DropDownList>月
                                <asp:DropDownList ID="lstToDD" runat="server" Width="40px" DataSource='<%# DayArray %>'>
                                </asp:DropDownList>日
                                <asp:DropDownList ID="lstToHH" runat="server" Width="40px" DataSource='<%# HourArray %>'>
                                </asp:DropDownList>時
                                <asp:Label ID="lblErrorMessageTo" runat="server" Text="" Visible="false" ForeColor="red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                ポイント付与方式
                            </td>
                            <td class="tdDataStyle">
                                <asp:RadioButton ID="rdoInputValue" runat="server" GroupName="PointReductionType" Text="入力値" Checked="true" />
                                <asp:RadioButton ID="rdoAddRate" runat="server" GroupName="PointReductionType" Text="加算率" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle2">
                                メール種別
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstMailTemplateNo" runat="server" Width="309px" DataSourceID="dsMailTemplate"
                                    DataTextField="TEMPLATE_NM" DataValueField="MAIL_TEMPLATE_NO" OnDataBound="lstMailTemplate_DataBound">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <table border="0" style="width: 270px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderSmallStyle2">
                                還元対象金額
                            </td>
                            <td class="tdHeaderSmallStyleL">
                                加算ポイント
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtReceiptSumAmt1" runat="server" MaxLength="10" Width="70" />円以上
                                <asp:RangeValidator ID="vdrReceiptSumAmt1" runat="server" ControlToValidate="txtReceiptSumAmt1"
                                    ErrorMessage="金額を0円以上の数値で入力して下さい。" MaximumValue="9999999999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender1"
                                    TargetControlID="vdrReceiptSumAmt1" HighlightCssClass="validatorCallout" />
                                    
                                <ajaxToolkit:FilteredTextBoxExtender ID="fteReceiptSumAmt1" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtReceiptSumAmt1" />
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtReductionPoint1" runat="server" MaxLength="10" Width="70" />
                                <asp:RangeValidator ID="vdrReductionPoint1" runat="server" ControlToValidate="txtReductionPoint1"
                                    ErrorMessage="加算ポイントを0以上の数値で入力して下さい。" MaximumValue="9999999999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender2"
                                    TargetControlID="vdrReductionPoint1" HighlightCssClass="validatorCallout" />
                                    
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtReductionPoint1" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtReceiptSumAmt2" runat="server" MaxLength="10" Width="70" />円以上
                                <asp:RangeValidator ID="vdrReceiptSumAmt2" runat="server" ControlToValidate="txtReceiptSumAmt2"
                                    ErrorMessage="金額を0円以上の数値で入力して下さい。" MaximumValue="9999999999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender3"
                                    TargetControlID="vdrReceiptSumAmt2" HighlightCssClass="validatorCallout" />
                                    
                                <ajaxToolkit:FilteredTextBoxExtender ID="fteReceiptSumAmt2" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtReceiptSumAmt2" />
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtReductionPoint2" runat="server" MaxLength="10" Width="70" />
                                <asp:RangeValidator ID="vdrReductionPoint2" runat="server" ControlToValidate="txtReductionPoint2"
                                    ErrorMessage="加算ポイントを0以上の数値で入力して下さい。" MaximumValue="9999999999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender4"
                                    TargetControlID="vdrReductionPoint2" HighlightCssClass="validatorCallout" />
                                    
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtReductionPoint2" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtReceiptSumAmt3" runat="server" MaxLength="10" Width="70" />円以上
                                <asp:RangeValidator ID="vdrReceiptSumAmt3" runat="server" ControlToValidate="txtReceiptSumAmt3"
                                    ErrorMessage="金額を0円以上の数値で入力して下さい。" MaximumValue="9999999999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender5"
                                    TargetControlID="vdrReceiptSumAmt3" HighlightCssClass="validatorCallout" />
                                    
                                <ajaxToolkit:FilteredTextBoxExtender ID="fteReceiptSumAmt3" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtReceiptSumAmt3" />
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtReductionPoint3" runat="server" MaxLength="10" Width="70" />
                                <asp:RangeValidator ID="vdrReductionPoint3" runat="server" ControlToValidate="txtReductionPoint3"
                                    ErrorMessage="加算ポイントを0以上の数値で入力して下さい。" MaximumValue="9999999999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender6"
                                    TargetControlID="vdrReductionPoint3" HighlightCssClass="validatorCallout" />
                                    
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtReductionPoint3" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtReceiptSumAmt4" runat="server" MaxLength="10" Width="70" />円以上
                                <asp:RangeValidator ID="vdrReceiptSumAmt4" runat="server" ControlToValidate="txtReceiptSumAmt4"
                                    ErrorMessage="金額を0円以上の数値で入力して下さい。" MaximumValue="9999999999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender7"
                                    TargetControlID="vdrReceiptSumAmt4" HighlightCssClass="validatorCallout" />
                                    
                                <ajaxToolkit:FilteredTextBoxExtender ID="fteReceiptSumAmt4" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtReceiptSumAmt4" />
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtReductionPoint4" runat="server" MaxLength="10" Width="70" />
                                <asp:RangeValidator ID="vdrReductionPoint4" runat="server" ControlToValidate="txtReductionPoint4"
                                    ErrorMessage="加算ポイントを0以上の数値で入力して下さい。" MaximumValue="9999999999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender8"
                                    TargetControlID="vdrReductionPoint4" HighlightCssClass="validatorCallout" />
                                    
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtReductionPoint4" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtReceiptSumAmt5" runat="server" MaxLength="10" Width="70" />円以上
                                <asp:RangeValidator ID="vdrReceiptSumAmt5" runat="server" ControlToValidate="txtReceiptSumAmt5"
                                    ErrorMessage="金額を0円以上の数値で入力して下さい。" MaximumValue="9999999999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender9"
                                    TargetControlID="vdrReceiptSumAmt5" HighlightCssClass="validatorCallout" />
                                    
                                <ajaxToolkit:FilteredTextBoxExtender ID="fteReceiptSumAmt5" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtReceiptSumAmt5" />
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtReductionPoint5" runat="server" MaxLength="10" Width="70" />
                                <asp:RangeValidator ID="vdrReductionPoint5" runat="server" ControlToValidate="txtReductionPoint5"
                                    ErrorMessage="加算ポイントを0以上の数値で入力して下さい。" MaximumValue="9999999999" MinimumValue="0"
                                    ValidationGroup="Detail" >*</asp:RangeValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender10"
                                    TargetControlID="vdrReductionPoint5" HighlightCssClass="validatorCallout" />
                                    
                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" Enabled="true"
                                    FilterType="Numbers"  TargetControlID="txtReductionPoint5" />
                            </td>
                        </tr>
                    </table>
                    
                    
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click"
                        ValidationGroup="Detail" />
                    <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click"
                        CausesValidation="False" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
     </div>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsMailTemplate" runat="server" SelectMethod="GetListByTemplateType" TypeName="MailTemplate">
		<SelectParameters>
			<asp:ControlParameter ControlID="lstSeekSiteCd" Name="pSiteCd" PropertyName="SelectedValue" Type="String" />
			<asp:Parameter DefaultValue="43" Name="pMailTemplateType" Type="String" />
			<asp:Parameter DefaultValue="1" Name="pSexCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
</asp:Content>