﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: パック設定
--	Progaram ID		: PackList
--
--  Creation Date	: 2009.12.11
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  2010/12/06  ttakahashi パック設定に初回購入のみ(FIRST_TIME_FLAG)を追加

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;
using System.Windows.Forms;

public partial class System_PackList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
	}

	private void FirstLoad() {
		grdPack.PageSize = 999;
		string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		if (string.IsNullOrEmpty(sSiteCd)) {
			lstSeekSiteCd.SelectedIndex = 0;
		} else {
			lstSeekSiteCd.SelectedValue = sSiteCd;
		}
		lstSeekSiteCd.DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		DataBind();

		lstSiteCd.DataSourceID = string.Empty;
		lstSettleType.DataSourceID = string.Empty;
		txtSalesAmt.Text = string.Empty;

		lstSiteCd.SelectedIndex = 0;
		lstSettleType.SelectedIndex = 0;
	}

	private void InitPage() {
		ClearField();
		pnlMainte.Visible = false;
	}

	protected string AddComma(object salesAmt) {
		return string.Format("{0:#,0}",Convert.ToInt32(salesAmt));
	}

	private void ClearField() {
		recCount = "0";
		lblSettleType.Text = "";
		lblSalesAmt.Text = "";
		txtRemarks.Text = "";
		txtCommoditiesCd.Text = "";
	}

	protected string GetRecCount() {
		return recCount;
	}

	private void GetList() {
		grdPack.PageIndex = 0;
		grdPack.DataBind();
		pnlCount.DataBind();
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		lblSalesAmt.Text = AddComma(txtSalesAmt.Text);
		GetData();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		pnlMainte.Visible = true;
		pnlKey.Enabled = true;
		pnlDtl.Visible = false;
		lstSiteCd.Enabled = false;
		lstSiteCd.SelectedIndex = lstSeekSiteCd.SelectedIndex;
		txtSalesAmt.Text = string.Empty;
		lblSalesAmt.Text = string.Empty;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void lnkPack_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		lstSiteCd.SelectedValue = sKeys[0];
		GetData();
	}

	protected void lnkSalesAmt_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		lstSiteCd.SelectedValue = sKeys[0];
		lstSettleType.SelectedValue = sKeys[1];
		txtSalesAmt.Text = sKeys[2];
		GetData();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
		lstSiteCd.SelectedValue = lstSeekSiteCd.SelectedValue;
	}

	protected void dsPack_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsPack_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
	}

	private void GetData() {

		using (DbSession db = new DbSession()) {

			db.PrepareProcedure("PACK_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PSETTLE_TYPE",DbSession.DbType.VARCHAR2,lstSettleType.SelectedValue);
			db.ProcedureInParm("PSALES_AMT",DbSession.DbType.NUMBER,int.Parse(txtSalesAmt.Text));
			db.ProcedureOutParm("PREMARKS",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCOMMODITIES_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
            db.ProcedureOutParm("PFIRST_TIME_FLAG", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				lblSalesAmt.Text = AddComma(db.GetStringValue("PSALES_AMT"));
				txtRemarks.Text = db.GetStringValue("PREMARKS");
                chkFirstTimeFlag.Checked = db.GetIntValue("PFIRST_TIME_FLAG") > 0 ? true : false;
                txtCommoditiesCd.Text = db.GetStringValue("PCOMMODITIES_CD");
			} else if (string.IsNullOrEmpty(txtSalesAmt.Text)) {
				ClearField();
			}

			using (Site oSite = new Site()) {
				if (oSite.GetOne(lstSiteCd.SelectedValue)) {
					lblSiteNm.Text = oSite.siteNm;
				}
			}

			using (CodeDtl oCodeDtl = new CodeDtl()) {
				if (oCodeDtl.GetOne("91",lstSettleType.SelectedValue)) {
					lblSettleType.Text = oCodeDtl.codeNm;
				}
			}
		}
		if (lstSettleType.SelectedValue.Equals(ViCommConst.SETTLE_S_MONEY) ||
			lstSettleType.SelectedValue.Equals(ViCommConst.SETTLE_G_MONEY) ||
			lstSettleType.SelectedValue.Equals(ViCommConst.SETTLE_ECO_CHIP) ||
			lstSettleType.SelectedValue.Equals(ViCommConst.SETTLE_CVS_DIRECT)) {
			trCommoditiesCd.Visible = true;
		} else {
			trCommoditiesCd.Visible = false;
		}

		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		lstSeekSiteCd.SelectedIndex = lstSiteCd.SelectedIndex;
		pnlKey.Enabled = false;
	}

	private void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("PACK_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PSETTLE_TYPE",DbSession.DbType.VARCHAR2,lstSettleType.SelectedValue);
			db.ProcedureInParm("PSALES_AMT",DbSession.DbType.NUMBER,int.Parse(txtSalesAmt.Text));
			db.ProcedureInParm("PREMARKS",DbSession.DbType.VARCHAR2,txtRemarks.Text);
			db.ProcedureInParm("PCOMMODITIES_CD",DbSession.DbType.VARCHAR2,txtCommoditiesCd.Text);
            db.ProcedureInParm("PFIRST_TIME_FLAG", DbSession.DbType.NUMBER, chkFirstTimeFlag.Checked ? 1 : 0);
            db.ProcedureInParm("PROWID", DbSession.DbType.VARCHAR2, ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		int iIdx = lstSiteCd.SelectedIndex;
		InitPage();
		GetList();
		lstSeekSiteCd.SelectedIndex = iIdx;
	}
	protected string GetFlagMark(object pValue) {
		if (pValue.ToString().Equals(ViCommConst.FLAG_ON_STR)) {
			return "○";
		}
		return string.Empty;
	}
}
