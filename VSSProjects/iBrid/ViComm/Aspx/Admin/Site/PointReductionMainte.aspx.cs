﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ポイント還元設定
--	Progaram ID		: PointReductionMainte
--
--  Creation Date	: 2011.09.09
--  Creater			: iBrid
--
**************************************************************************/

// [ this.Update History ]
/*------------------------------------------------------------------------

  Date        this.Updater    this.Update Explain

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public partial class Site_PointReductionMainte : System.Web.UI.Page {
    protected static readonly string[] HourArray = new string[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" };
    protected static readonly string[] DayArray = new string[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" };
    protected static readonly string[] MonthArray = new string[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" };
    protected string[] YearArray;

    private string SiteCd {
        get { return iBridUtil.GetStringValue(this.ViewState["SiteCd"]); }
        set { this.ViewState["SiteCd"] = value; }
    }
    private string RevisionNo {
        get { return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]); }
        set { this.ViewState["RevisionNo"] = value; }
    }

    private string Rowid {
        get { return iBridUtil.GetStringValue(this.ViewState["Rowid"]); }
        set { this.ViewState["Rowid"] = value; }
    }



    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            this.FirstLoad();
        }
    }
    protected void FirstLoad() {
        this.RevisionNo = "0";
        this.YearArray = new string[] {DateTime.Today.AddYears(-1).ToString("yyyy"), DateTime.Today.ToString("yyyy"), DateTime.Today.AddYears(1).ToString("yyyy") };
        this.DataBind();
        this.InitPage();
    }

    protected void InitPage() {
        this.ClearFirld();
        this.pnlMainte.Visible = false;
		this.lstMailTemplateNo.DataBind();
    }

    protected void ClearFirld() {

        this.lstFromYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
        this.lstFromMM.SelectedValue = DateTime.Now.ToString("MM");
        this.lstFromDD.SelectedValue = DateTime.Now.ToString("dd");
        this.lstFromHH.SelectedValue = DateTime.Now.ToString("HH");
        this.lstToYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
        this.lstToMM.SelectedValue = DateTime.Now.ToString("MM");
        this.lstToDD.SelectedValue = DateTime.Now.ToString("dd");
        this.lstToHH.SelectedValue = DateTime.Now.ToString("HH");

        this.txtReceiptSumAmt1.Text = string.Empty;
        this.txtReceiptSumAmt2.Text = string.Empty;
        this.txtReceiptSumAmt3.Text = string.Empty;
        this.txtReceiptSumAmt4.Text = string.Empty;
        this.txtReceiptSumAmt5.Text = string.Empty;

        this.txtReductionPoint1.Text = string.Empty;
        this.txtReductionPoint2.Text = string.Empty;
        this.txtReductionPoint3.Text = string.Empty;
        this.txtReductionPoint4.Text = string.Empty;
        this.txtReductionPoint5.Text = string.Empty;

        this.rdoInputValue.Checked = true;
        this.rdoAddRate.Checked = false;
    }

    protected void GetData() {
        this.lblErrorMessageFrom.Visible = false;
        this.lblErrorMessageTo.Visible = false;


        using (DbSession oDbSession = new DbSession()) {
            oDbSession.PrepareProcedure("POINT_REDUCTION_GET");
            oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.lstSiteCd.SelectedValue);
            oDbSession.ProcedureOutParm("pAPPLICATION_START_DATE", DbSession.DbType.DATE);
            oDbSession.ProcedureOutParm("pAPPLICATION_END_DATE", DbSession.DbType.DATE);
            oDbSession.ProcedureOutParm("pPOINT_REDUCTION_TYPE", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pMAIL_TEMPLATE_NO", DbSession.DbType.VARCHAR2);
            oDbSession.ProcedureOutArrayParm("pRECEIPT_SUM_AMT_ARR", DbSession.DbType.NUMBER);
            oDbSession.ProcedureOutArrayParm("pREDUCTION_POINT_ARR", DbSession.DbType.NUMBER);
            oDbSession.ProcedureOutParm("pROWID", DbSession.DbType.VARCHAR2);
            oDbSession.ProcedureOutParm("pREVISION_NO", DbSession.DbType.NUMBER);
            oDbSession.ProcedureOutParm("pRECORD_COUNT", DbSession.DbType.NUMBER);
            oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
            oDbSession.ExecuteProcedure();

            this.RevisionNo = oDbSession.GetStringValue("PREVISION_NO");
            this.Rowid = oDbSession.GetStringValue("PROWID");

            if (int.Parse(oDbSession.GetStringValue("PRECORD_COUNT")) > 0) {

                string sFromDate = string.Format("{0:yyyy/MM/dd HH:mm:ss}", oDbSession.GetDateTimeValue("pAPPLICATION_START_DATE"));
                string sToDate = string.Format("{0:yyyy/MM/dd HH:mm:ss}", oDbSession.GetDateTimeValue("pAPPLICATION_END_DATE"));
                if (!string.IsNullOrEmpty(sFromDate)) {
					if ((int.Parse(DateTime.Today.AddYears(-1).ToString("yyyy")) <= int.Parse(sFromDate.Substring(0, 4)))) {
						this.lstFromYYYY.SelectedValue = sFromDate.Substring(0, 4);
						this.lstFromMM.SelectedValue = sFromDate.Substring(5, 2);
						this.lstFromDD.SelectedValue = sFromDate.Substring(8, 2);
						this.lstFromHH.SelectedValue = sFromDate.Substring(11, 2);
					}

                }

                if (!string.IsNullOrEmpty(sToDate)) {
					if ((int.Parse(DateTime.Today.AddYears(-1).ToString("yyyy")) <= int.Parse(sToDate.Substring(0, 4)))) {
						this.lstToYYYY.SelectedValue = sToDate.Substring(0, 4);
						this.lstToMM.SelectedValue = sToDate.Substring(5, 2);
						this.lstToDD.SelectedValue = sToDate.Substring(8, 2);
						this.lstToHH.SelectedValue = sToDate.Substring(11, 2);
					}
                }

                this.rdoInputValue.Checked = oDbSession.GetStringValue("pPOINT_REDUCTION_TYPE").Equals(ViCommConst.PointReductionType.InputValue);
                this.rdoAddRate.Checked = oDbSession.GetStringValue("pPOINT_REDUCTION_TYPE").Equals(ViCommConst.PointReductionType.AddRate);
				this.lstMailTemplateNo.SelectedValue = oDbSession.GetStringValue("pMAIL_TEMPLATE_NO");

                int iReductionPointLength = oDbSession.GetArrySize("pREDUCTION_POINT_ARR");

                this.SetPointReduction(txtReceiptSumAmt1, txtReductionPoint1, 0, oDbSession, iReductionPointLength);
                this.SetPointReduction(txtReceiptSumAmt2, txtReductionPoint2, 1, oDbSession, iReductionPointLength);
                this.SetPointReduction(txtReceiptSumAmt3, txtReductionPoint3, 2, oDbSession, iReductionPointLength);
                this.SetPointReduction(txtReceiptSumAmt4, txtReductionPoint4, 3, oDbSession, iReductionPointLength);
                this.SetPointReduction(txtReceiptSumAmt5, txtReductionPoint5, 4, oDbSession, iReductionPointLength);
            }
        }
    }

    protected void UpdateData() {
        DateTime dtFrom;
        DateTime dtTo;
        if (!this.CheckDate(out dtFrom, out dtTo)) {
            return;
        }

        List<string> oReceiptSumAmtArr = new List<string>();
        List<string> oReductionPointArr = new List<string>();

        GetPointReduction(this.txtReceiptSumAmt1.Text, this.txtReductionPoint1.Text, ref oReceiptSumAmtArr, ref oReductionPointArr);
        GetPointReduction(this.txtReceiptSumAmt2.Text, this.txtReductionPoint2.Text, ref oReceiptSumAmtArr, ref oReductionPointArr);
        GetPointReduction(this.txtReceiptSumAmt3.Text, this.txtReductionPoint3.Text, ref oReceiptSumAmtArr, ref oReductionPointArr);
        GetPointReduction(this.txtReceiptSumAmt4.Text, this.txtReductionPoint4.Text, ref oReceiptSumAmtArr, ref oReductionPointArr);
        GetPointReduction(this.txtReceiptSumAmt5.Text, this.txtReductionPoint5.Text, ref oReceiptSumAmtArr, ref oReductionPointArr);
        

        string sPointReductionType = (rdoInputValue.Checked) ? ViCommConst.PointReductionType.InputValue : ViCommConst.PointReductionType.AddRate;


        using (DbSession oDbSession = new DbSession())
        {
            oDbSession.PrepareProcedure("POINT_REDUCTION_MAINTE");
            oDbSession.cmd.BindByName = true;
            oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.lstSiteCd.SelectedValue);
            oDbSession.ProcedureInParm("pAPPLICATION_START_DATE", DbSession.DbType.DATE, dtFrom);
            oDbSession.ProcedureInParm("pAPPLICATION_END_DATE", DbSession.DbType.DATE, dtTo);
            oDbSession.ProcedureInParm("pPOINT_REDUCTION_TYPE", DbSession.DbType.VARCHAR2, sPointReductionType);
			oDbSession.ProcedureInParm("pMAIL_TEMPLATE_NO", DbSession.DbType.VARCHAR2, this.lstMailTemplateNo.SelectedValue);
            oDbSession.ProcedureInArrayParm("pRECEIPT_SUM_AMT_ARR", DbSession.DbType.NUMBER, oReceiptSumAmtArr.ToArray());
            oDbSession.ProcedureInArrayParm("pREDUCTION_POINT_ARR", DbSession.DbType.NUMBER, oReductionPointArr.ToArray());
            oDbSession.ProcedureInParm("pROWID", DbSession.DbType.VARCHAR2, this.Rowid);
            oDbSession.ProcedureInParm("pREVISION_NO", DbSession.DbType.NUMBER, decimal.Parse(this.RevisionNo));
            oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
            oDbSession.ExecuteProcedure();
        }
        this.InitPage();
    }

    private bool CheckDate(out DateTime dtFrom, out DateTime dtTo) {

        dtFrom = new DateTime();
        dtTo = new DateTime();
        DateTime dtToTmp = new DateTime();

        this.lblErrorMessageFrom.Visible = false;
        this.lblErrorMessageTo.Visible = false;
        if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:00:00",
            this.lstFromYYYY.Text, this.lstFromMM.SelectedValue, this.lstFromDD.SelectedValue, this.lstFromHH.SelectedValue), out dtFrom)) {
            this.lblErrorMessageFrom.Text = "日時を正しく入力してください";
            this.lblErrorMessageFrom.Visible = true;
            return false;
        }
        if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:00:00",
            this.lstToYYYY.Text, this.lstToMM.SelectedValue, this.lstToDD.SelectedValue, this.lstToHH.SelectedValue), out dtTo)) {
            this.lblErrorMessageTo.Text = "日時を正しく入力してください";
            this.lblErrorMessageTo.Visible = true;
            return false;
        } else {
            dtToTmp = dtTo.AddSeconds(-1);
        }
        if (dtFrom > dtToTmp)
        {
            this.lblErrorMessageTo.Text = "日時の大小関係を正しく入力してください";
            this.lblErrorMessageTo.Visible = true;
            return false;
        }

        return true;
    }

    protected void GetPointReduction(string pReceiptSumAmt, string pPointReduction, ref List<string> pReceiptSumAmtArr, ref List<string> pPointReductionArr) {
        if (!string.IsNullOrEmpty(pReceiptSumAmt) &&
            !string.IsNullOrEmpty(pPointReduction)) {
            if (int.Parse(pReceiptSumAmt) > 0 &&
                int.Parse(pPointReduction) > 0) {
                pReceiptSumAmtArr.Add(pReceiptSumAmt);
                pPointReductionArr.Add(pPointReduction);
            }
        }
    }

    protected void SetPointReduction(TextBox pReceiptSumAmt, TextBox pPointReduction, int pIndex, DbSession pDbsession, int pReductionPointLength) {
        if (pReductionPointLength > pIndex) {
            pReceiptSumAmt.Text = pDbsession.GetArryIntValue("pRECEIPT_SUM_AMT_ARR", pIndex);
            pPointReduction.Text = pDbsession.GetArryIntValue("pREDUCTION_POINT_ARR", pIndex);
        }
    }
    # region === Click Event ===
    protected void btnListSeek_Click(object sender, EventArgs e) {
        this.InitPage();
        pnlMainte.Visible = true;
        this.lstSiteCd.SelectedValue = this.lstSeekSiteCd.SelectedValue;
        this.GetData();
    }
    protected void btnUpdate_Click(object sender, EventArgs e) {
        if (IsValid) {
            this.UpdateData();
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e) {
        this.InitPage();
    }
    # endregion

	protected void lstMailTemplate_DataBound(object sender,EventArgs e) {
		this.lstMailTemplateNo.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}
}

