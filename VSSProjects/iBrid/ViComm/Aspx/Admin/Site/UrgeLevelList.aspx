<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UrgeLevelList.aspx.cs" Inherits="Site_UrgeLevelList" Title="督促レベル設定"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="督促レベル設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey" Width="550px">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 500px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									サイト名
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="120px">
									</asp:DropDownList>
								</td>
								<td class="tdHeaderStyle2">
									レベル
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUrgeLevel" runat="server" MaxLength="1" Width="25px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrUrgeLevel" runat="server" ErrorMessage="督促レベルを入力して下さい。" ControlToValidate="txtUrgeLevel" ValidationGroup="Key">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeUrgeLevel" runat="server" ErrorMessage="督促レベルは数字1桁で入力して下さい。" ValidationExpression="^[1-9]" ControlToValidate="txtUrgeLevel"
										ValidationGroup="Key">*</asp:RegularExpressionValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl" Width="550px">
					<fieldset class="fieldset-inner">
						<legend>[レベル内容]</legend>
						<table border="0" style="width: 300px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									遅延日数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtDelayDays" runat="server" MaxLength="2" Width="30px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrDelayDays" runat="server" ErrorMessage="遅延日数を入力して下さい。" ControlToValidate="txtDelayDays" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeDelayDays" runat="server" ErrorMessage="遅延日数は数字2桁で入力して下さい。" ValidationExpression="^\d{1,2}" ControlToValidate="txtDelayDays"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[レベル一覧]</legend>
			<table border="0" style="width: 300px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle2">
						サイト名
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="120px">
						</asp:DropDownList>
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnRegist" Text="レベル追加" CssClass="seekbutton" CausesValidation="False" OnClick="btnRegist_Click" />
			<br />
			<br />
			<asp:GridView ID="grdUrgeLevel" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsUrgeLevel" AllowSorting="True" SkinID="GridViewColor">
				<Columns>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkUrgeLevel" runat="server" Text='<%# Eval("URGE_LEVEL") %>' CommandArgument='<%# string.Format("{0}:{1}:{2}",Eval("SITE_CD"),Eval("URGE_LEVEL"),Eval("DELAY_DAYS"))  %>'
								OnCommand="lnkUrgeLevel_Command" CausesValidation="False">
							</asp:LinkButton>
						</ItemTemplate>
						<HeaderTemplate>
							レベル
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Right" />
					</asp:TemplateField>
					<asp:BoundField DataField="DELAY_DAYS" HeaderText="遅延日数">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:BoundField DataField="UPDATE_DATE" HeaderText="更新日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:HyperLink ID="lnkUrgeSchedule" runat="server" NavigateUrl='<%# string.Format("~/Site/UrgeScheduleList.aspx?sitecd={0}&urgelevel={1}",Eval("SITE_CD"),Eval("URGE_LEVEL"))%>'>スケジュール設定</asp:HyperLink>
						</ItemTemplate>
						<HeaderTemplate>
							スケジュール設定
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Center" />
					</asp:TemplateField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br />
				<a class="reccount">Current viewing page
					<%=grdUrgeLevel.PageIndex + 1%>
					of
					<%=grdUrgeLevel.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsUrgeLevel" runat="server" SelectMethod="GetPageCollection" TypeName="UrgeLevel" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsUrgeLevel_Selected" OnSelecting="dsUrgeLevel_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsWebFace" runat="server" SelectMethod="GetList" TypeName="WebFace"></asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrUrgeLevel" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeUrgeLevel" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrDelayDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdeDelayDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtUrgeLevel" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtDelayDays" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
