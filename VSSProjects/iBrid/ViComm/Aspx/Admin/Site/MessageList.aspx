<%@ Import Namespace="ViComm" %>
<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MessageList.aspx.cs" Inherits="System_MessageList" Title="変更履歴"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="メッセージ管理"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<asp:Label ID="lblMessageSeq" runat="server" Text="" Visible="false"></asp:Label>
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[変更内容]</legend>
						<table border="0" style="width: 650px" class="tableStyle">
							<tr>
								<td class="tdHeaderSmallStyle">
									作成日
								</td>
								<td class="tdDataStyle" width="225px">
									<asp:Label ID="lblCreateDate" runat="server" Text=""></asp:Label>&nbsp;&nbsp;
								</td>
								<td class="tdHeaderSmallStyle">
									更新日
								</td>
								<td class="tdDataStyle" width="225px">
									<asp:Label ID="lblUpdateDate" runat="server" Text=""></asp:Label>&nbsp;&nbsp;
								</td>
							</tr>
							<tr>
								<td class="tdHeaderSmallStyle">
									<asp:Label ID="lblHandleNmHeader" runat="server" Text=""></asp:Label>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblHandleNm" runat="server" Text=""></asp:Label>&nbsp;&nbsp;
								</td>
								<td class="tdHeaderSmallStyle2">
									<asp:Label ID="lblPartnerHandleNmHeader" runat="server" Text=""></asp:Label>
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblPartnerHandleNm" runat="server" Text=""></asp:Label>&nbsp;&nbsp;
								</td>
							</tr>
							<tr>
								<td class="tdHeaderSmallStyle2">
									メッセージ
								</td>
								<td class="tdDataStyle" colspan="3">
									<asp:TextBox ID="txtMessageDoc" runat="server" Columns="60" MaxLength="1000" Rows="10" TextMode="MultiLine" Width="497px"></asp:TextBox>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlSeek">
			<fieldset>
				<legend>[検索条件]</legend>
				<table border="0" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイトコード
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							作成日
						</td>
						<td class="tdDataStyle" colspan="5">
							<asp:DropDownList ID="lstFromYYYY" runat="server">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstFromMM" runat="server">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstFromDD" runat="server">
							</asp:DropDownList>日〜
							<asp:DropDownList ID="lstToYYYY" runat="server">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstToMM" runat="server">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstToDD" runat="server">
							</asp:DropDownList>日
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" CausesValidation="False" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" CausesValidation="False" />
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[メッセージ一覧]</legend>
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="400px">
					<asp:GridView ID="grdMessage" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsMessage" AllowSorting="True" SkinID="GridViewColor">
						<Columns>
							<asp:TemplateField HeaderText="会員名">
								<ItemTemplate>
									<asp:Label ID="Label1" runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Left" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="出演者名">
								<ItemTemplate>
									<asp:Label ID="Label2" runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("PARTNER_HANDLE_NM")) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Left" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="出演者名">
								<ItemTemplate>
									<asp:Label ID="Label3" runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Left" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="会員名">
								<ItemTemplate>
									<asp:Label ID="Label4" runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("PARTNER_HANDLE_NM")) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Left" />
							</asp:TemplateField>
							<asp:TemplateField>
								<ItemTemplate>
									<asp:LinkButton ID="lnkMessageDoc" runat="server" Text='<%# Eval("MESSAGE_DOC") %>' CommandArgument='<%# string.Format("{0}:{1}:{2}",Eval("MESSAGE_SEQ"),ViCommPrograms.DefHandleName(Eval("HANDLE_NM")),ViCommPrograms.DefHandleName(Eval("PARTNER_HANDLE_NM"))) %>'
										OnCommand="lnkMessageDoc_Command" CausesValidation="False">
									</asp:LinkButton>
								</ItemTemplate>
								<HeaderTemplate>
									メッセージ
								</HeaderTemplate>
								<ItemStyle HorizontalAlign="Left" Width="500px" />
							</asp:TemplateField>
							<asp:BoundField DataField="CREATE_DATE" HeaderText="作成日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
				&nbsp;
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count
						<%#GetRecCount() %>
					</a>
					<br>
					<a class="reccount">Current viewing page
						<%=grdMessage.PageIndex + 1%>
						of
						<%=grdMessage.PageCount%>
					</a>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsMessage" runat="server" SelectMethod="GetPageCollection" TypeName="Message" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsMessage_Selecting" OnSelected="dsMesage_Selected">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pSexCd" Type="String" />
			<asp:Parameter Name="pCreateDateFrom" Type="String" />
			<asp:Parameter Name="pCreateDateTo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
