﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ユーザー定義タグメンテナンス
--	Progaram ID		: UserDefineTagList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using iBridCommLib;
using ViComm;

public partial class Site_UserDefineTagList:System.Web.UI.Page {
	private string recCount = "";
	private readonly int MAX_USER_DEFINE_TAG_BLOCK = 10;

	private bool DisablePinEdit {
		get {
			return (bool)this.ViewState["DisablePinEdit"];
		}
		set {
			this.ViewState["DisablePinEdit"] = value;
		}
	}

	private string HtmlDoc {
		get {
			if (this.DisablePinEdit) {
				return this.txtHtmlDocText.Text;
			} else {
				return this.txtHtmlDoc.Text;
			}
		}
		set {
			if (this.DisablePinEdit) {
				this.txtHtmlDocText.Text = value;
			} else {
				this.txtHtmlDoc.Text = value;
			}
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		txtHtmlDoc.EditorDirectory = string.Format("http://{0}:{1}/pinEdit/editor/",Request.UrlReferrer.Host,Request.UrlReferrer.Port);
		txtHtmlDoc.Autofocus = true;
		txtSeekVariableId.Style.Add("ime-mode","disabled");

		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
	}

	protected void dsUserDefineTag_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void FirstLoad() {
		grdUserDefineTag.PageSize = 999;
		grdUserDefineTag.DataSourceID = "";

		DataBind();
		lstSiteCd.DataSourceID = "";
		lstUserAgentType.DataSourceID = "";
		lstSeekSiteCd.DataSourceID = "";

		lstSeekUserAgentType.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		lstSeekUserAgentType.SelectedIndex = 0;
		lstSeekUserAgentType.DataSourceID = string.Empty;

		lstSCategorySeq.SelectedIndex = 0;

		using (ManageCompany oManageCompany = new ManageCompany()) {
			this.DisablePinEdit = oManageCompany.IsAvailableService(ViCommConst.RELEASE_DISABLE_PINEDIT,2);
			this.txtHtmlDoc.Visible = !this.DisablePinEdit;
			this.txtHtmlDocText.Visible = this.DisablePinEdit;
		}
	}

	private void InitPage() {
		txtVariableId.Text = "";
		ClearField();
		pnlMainte.Visible = false;
		lstSeekSiteCd.DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		DataBind();
		int iCompare = string.Compare(Session["AdminType"].ToString(),ViCommConst.RIGHT_SITE_OWNER);
		btnCSV.Visible = (iCompare >= 0);
	}

	private void ClearField() {
		txtVariableNm.Text = "";
		this.HtmlDoc = "";
		recCount = "0";
		this.lstCategorySeq.SelectedValue = null;
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		if (IsValid) {
			GetData();
		}
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		lstSiteCd.SelectedIndex = lstSeekSiteCd.SelectedIndex;
		if (!lstSeekUserAgentType.SelectedValue.Equals("")) {
			lstUserAgentType.SelectedValue = lstSeekUserAgentType.SelectedValue;
		}
		ClearField();
		pnlMainte.Visible = true;
		pnlKey.Enabled = true;
		pnlDtl.Visible = false;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void lnkVariableId_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		lstSiteCd.SelectedValue = sKeys[0];
		txtVariableId.Text = sKeys[1];
		lstUserAgentType.SelectedValue = sKeys[2];
		GetData();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected string EditHtmlDoc(object pHtmlDoc) {
		return pHtmlDoc.ToString();
	}


	private void GetList() {
		grdUserDefineTag.PageIndex = 0;
		grdUserDefineTag.DataSourceID = "dsUserDefineTag";
		grdUserDefineTag.DataBind();
		pnlCount.DataBind();
	}

	private void GetData() {
		SessionObjs userObjs = (SessionObjs)Session["objs"];
		userObjs.site.GetOne(lstSiteCd.SelectedValue);

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("USER_DEFINE_TAG_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PVARIABLE_ID",DbSession.DbType.VARCHAR2,txtVariableId.Text.TrimEnd());
			db.ProcedureInParm("PUSER_AGENT_TYPE",DbSession.DbType.VARCHAR2,lstUserAgentType.SelectedValue);
			db.ProcedureOutParm("PVARIABLE_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCATEGORY_SEQ",DbSession.DbType.NUMBER);
			db.ProcedureOutArrayParm("PHTML_DOC",DbSession.DbType.VARCHAR2,SysConst.MAX_HTML_BLOCKS);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			this.HtmlDoc = "";
			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtVariableNm.Text = db.GetStringValue("PVARIABLE_NM");
				this.lstCategorySeq.SelectedValue = db.GetStringValue("PCATEGORY_SEQ");
				for (int i = 0;i < SysConst.MAX_HTML_BLOCKS;i++) {
					this.HtmlDoc = this.HtmlDoc + db.GetArryStringValue("PHTML_DOC",i);
				}
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;
		SetColor();
		lblErrorInputSizeOver.Visible = false;
	}

	private void UpdateData(int pDelFlag) {

		string[] sDoc;
		int iDocCount;

		lblErrorInputSizeOver.Visible = false;
		if (this.DisablePinEdit) {
			SysPrograms.SeparateSiteHtmlText(HttpUtility.HtmlDecode(this.HtmlDoc),SysConst.MAX_HTML_BLOCKS,out sDoc,out iDocCount);
		} else {
			SysPrograms.SeparateSiteHtml(HttpUtility.HtmlDecode(this.HtmlDoc),SysConst.MAX_HTML_BLOCKS,out sDoc,out iDocCount);
		}

		if ((iDocCount > this.MAX_USER_DEFINE_TAG_BLOCK) && (pDelFlag == 0)) {
			lblErrorInputSizeOver.Visible = true;
			return;
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("USER_DEFINE_TAG_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PVARIABLE_ID",DbSession.DbType.VARCHAR2,txtVariableId.Text.TrimEnd());
			db.ProcedureInParm("PUSER_AGENT_TYPE",DbSession.DbType.VARCHAR2,lstUserAgentType.SelectedValue);
			db.ProcedureInParm("PVARIABLE_NM",DbSession.DbType.VARCHAR2,txtVariableNm.Text);
			if (!string.IsNullOrEmpty(this.lstCategorySeq.SelectedValue)) {
				db.ProcedureInParm("PCATEGORY_SEQ",DbSession.DbType.NUMBER,this.lstCategorySeq.SelectedValue);
			} else {
				db.ProcedureInParm("PCATEGORY_SEQ",DbSession.DbType.NUMBER,null);
			}
			db.ProcedureInArrayParm("PHTML_DOC",DbSession.DbType.VARCHAR2,iDocCount,sDoc);
			db.ProcedureInParm("PHTML_DOC_COUNT",DbSession.DbType.NUMBER,iDocCount);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,ViewState["REVISION_NO"].ToString());
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		int iIdx = lstSiteCd.SelectedIndex;

		if (pDelFlag == 1) {
			InitPage();
		} else {
			GetData();
			GetList();
		}
		
		lstSeekSiteCd.SelectedIndex = iIdx;
	}

	protected void dsUserDefineTag_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		SessionObjs userObjs = (SessionObjs)Session["objs"];
		userObjs.site.GetOne(lstSeekSiteCd.SelectedValue);
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
		e.InputParameters[1] = lstSeekUserAgentType.SelectedValue;
		e.InputParameters[2] = lstSCategorySeq.SelectedValue;
		e.InputParameters[3] = txtSeekVariableId.Text;
	}

	private void SetColor() {
		string sForeColor;
		string sBackColor;
		string sLinkColor;

		using (Site oSite = new Site()) {
			oSite.GetOne(lstSiteCd.SelectedValue);
			sForeColor = oSite.colorChar;
			sBackColor = oSite.colorBack;
			sLinkColor = oSite.colorLink;
		}

		ltlUserColor.Text = "<style type=\"text/css\">\r\n" +
							"<!--\r\n" +
							".userColor { background-color: " + sBackColor + ";}\r\n" +
							".userColor TD{ color: " + sForeColor + ";}\r\n" +
							".userColor TD A{color: " + sLinkColor + ";font-weight: bold;text-decoration: none;background-color: inherit;}\r\n" +
							"-->\r\n" +
							"</style>";
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		Response.AddHeader("Content-Disposition",string.Format("attachment;filename=USER_DEFINE_TAG_{0}.CSV",lstSeekSiteCd.SelectedValue));
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		using (UserDefineTag oDoc = new UserDefineTag()) {
			DataSet ds = oDoc.GetCsvData(lstSeekSiteCd.SelectedValue,lstSeekUserAgentType.SelectedValue,lstSCategorySeq.SelectedValue,txtSeekVariableId.Text);
			SetCsvData(ds);
			Response.End();
		}
	}

	public void SetCsvData(DataSet pDataSet) {

		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");
		string sDtl = "";

		DataRow dr = null;
		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];

			sDtl = string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}",
							dr["SITE_CD"].ToString(),
							dr["VARIABLE_ID"].ToString(),
							dr["USER_AGENT_TYPE"].ToString(),
							dr["VARIABLE_NM"].ToString(),
							dr["HTML_DOC1"].ToString(),
							dr["HTML_DOC2"].ToString(),
							dr["HTML_DOC3"].ToString(),
							dr["HTML_DOC4"].ToString(),
							dr["HTML_DOC5"].ToString(),
							dr["HTML_DOC6"].ToString(),
							dr["HTML_DOC7"].ToString(),
							dr["HTML_DOC8"].ToString(),
							dr["HTML_DOC9"].ToString(),
							dr["HTML_DOC10"].ToString()
							);
			sDtl = sDtl.Replace("\r","");
			sDtl = sDtl.Replace("\n","");
			sDtl += "\r\n";
			Response.BinaryWrite(encoding.GetBytes(sDtl));
		}
	}

	protected string ReplaceForm(object pHtmlDoc) {
		return Regex.Replace(pHtmlDoc.ToString(),@"<\/?[fF][oO][rR][mM].*?>",string.Empty,RegexOptions.Compiled);
	}

	protected void vdcVariableId_ServerValidate(object source,ServerValidateEventArgs args) {
		if (IsValid) {
			if ((args.IsValid = txtVariableId.Text.StartsWith("$"))) {
				args.IsValid = (txtVariableId.Text.Length > 1);
			}
		}
	}

}
