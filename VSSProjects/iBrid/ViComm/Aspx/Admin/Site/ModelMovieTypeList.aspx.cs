﻿using System;
/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 携帯機種別対応動画ファイル種別管理
--	Progaram ID		: ModelMovieTypeList
--
--  Creation Date	: 2010.08.10
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;

public partial class Site_ModelMovieTypeList : System.Web.UI.Page
{
    private string recCount = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitPage();
        }
    }

    private void InitPage()
    {
        grdModelMovieType.PageSize = 20;
        grdModelMovieType.PageIndex = 0;
        ClearField();
        pnlMainte.Visible = false;
        pnlMainte.Enabled = false;
        lstCarrier.DataBind();
        lstMovieFileType.DataBind();
        DataBind();
    }

    private void ClearField()
    {
        lblModeMovieTypeSeq.Text = string.Empty;

        lstCarrier.SelectedIndex = -1;
        rdoTargetUseTypeDownload.Checked = true;
        rdoTargetUseTypeStreaming.Checked = false;
        txtMobileModel.Text = "";
        lstMovieFileType.SelectedIndex = -1;
    }

    protected string GetRecCount()
    {
        return recCount;
    }

    private void GetData()
    {
        using (DbSession db = new DbSession())
        {
            db.PrepareProcedure("MODEL_MOVIE_TYPE_GET");
            db.ProcedureInParm("PMODE_MOVIE_TYPE_SEQ", DbSession.DbType.VARCHAR2, lblModeMovieTypeSeq.Text);
            db.ProcedureOutParm("PMOVIE_FILE_TYPE", DbSession.DbType.VARCHAR2);
            db.ProcedureOutParm("PMOBILE_CARRIER_CD", DbSession.DbType.VARCHAR2);
            db.ProcedureOutParm("PTARGET_USE_TYPE", DbSession.DbType.VARCHAR2);
            db.ProcedureOutParm("PMOBILE_MODEL", DbSession.DbType.VARCHAR2);
            db.ProcedureOutParm("PMOBILE_MODEL_LOWER", DbSession.DbType.VARCHAR2);
            db.ProcedureOutParm("PROWID", DbSession.DbType.VARCHAR2);
            db.ProcedureOutParm("PREVISION_NO", DbSession.DbType.NUMBER);
            db.ProcedureOutParm("PRECORD_COUNT", DbSession.DbType.NUMBER);
            db.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);
            db.ExecuteProcedure();

            ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
            ViewState["ROWID"] = db.GetStringValue("PROWID");

            if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0)
            {

                lstCarrier.SelectedValue = db.GetStringValue("PMOBILE_CARRIER_CD");

                if(db.GetStringValue("PTARGET_USE_TYPE").Equals("s"))
                {
                        rdoTargetUseTypeDownload.Checked = false;
                        rdoTargetUseTypeStreaming.Checked = true;
                }
                else
                {
                        rdoTargetUseTypeDownload.Checked = true;
                        rdoTargetUseTypeStreaming.Checked = false;
                }
                
                string mobileModel = db.GetStringValue("PMOBILE_MODEL");
                if(mobileModel.Equals("__default__"))
                {
                    txtMobileModel.Text = "既定";
                    lstCarrier.Enabled = false;
                    rdoTargetUseTypeDownload.Enabled = false;
                    rdoTargetUseTypeStreaming.Enabled = false;
                    txtMobileModel.Enabled = false;
                    btnDelete.Enabled = false;
                }
                else
                {
                    txtMobileModel.Text = mobileModel;
                    lstCarrier.Enabled = true;
                    rdoTargetUseTypeDownload.Enabled = true;
                    rdoTargetUseTypeStreaming.Enabled = true;
                    txtMobileModel.Enabled = true;
                    btnDelete.Enabled = true;
                }
                lstMovieFileType.SelectedValue = db.GetStringValue("PMOVIE_FILE_TYPE");
            }
            else
            {
                ClearField();
            }
        }
        pnlMainte.Enabled = true;
        pnlMainte.Visible = true;
    }

    private void UpdateData(int pDelFlag)
    {
        string targetUseType = "";
        string mobileModel = "";

        if (rdoTargetUseTypeStreaming.Checked)
        {
            targetUseType = "s";
        }
        else
        {
            targetUseType = "d";
        }

        if (txtMobileModel.Text.Equals("既定"))
        {
            mobileModel = "__default__";
        }
        else
        {
            mobileModel = txtMobileModel.Text;
        }

        using (DbSession db = new DbSession())
        {
            db.PrepareProcedure("MODEL_MOVIE_TYPE_MAINTE");
            db.ProcedureInParm("PMODE_MOVIE_TYPE_SEQ", DbSession.DbType.VARCHAR2, lblModeMovieTypeSeq.Text);
            db.ProcedureInParm("PMOVIE_FILE_TYPE", DbSession.DbType.VARCHAR2, lstMovieFileType.SelectedValue);
            db.ProcedureInParm("PMOBILE_CARRIER_CD", DbSession.DbType.VARCHAR2, lstCarrier.SelectedValue);
            db.ProcedureInParm("PTARGET_USE_TYPE", DbSession.DbType.VARCHAR2, targetUseType);
            db.ProcedureInParm("PMOBILE_MODEL", DbSession.DbType.VARCHAR2, mobileModel);
            db.ProcedureInParm("PMOBILE_MODEL_LOWER", DbSession.DbType.VARCHAR2, mobileModel.ToLower());
            db.ProcedureInParm("PROWID", DbSession.DbType.VARCHAR2, ViewState["ROWID"].ToString());
            db.ProcedureInParm("PREVISION_NO", DbSession.DbType.NUMBER, decimal.Parse(ViewState["REVISION_NO"].ToString()));
            db.ProcedureInParm("PDEL_FLAG", DbSession.DbType.NUMBER, pDelFlag);
            db.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);
            db.ExecuteProcedure();
        }
        InitPage();
    }

    protected void btnRegister_Click(object sender, EventArgs e)
    {
        InitPage();
        pnlMainte.Visible = true;
        pnlMainte.Enabled = true;
        lstCarrier.Enabled = true;
        rdoTargetUseTypeDownload.Enabled = true;
        rdoTargetUseTypeStreaming.Enabled = true;
        txtMobileModel.Enabled = true;
        btnDelete.Enabled = true;
        GetData();
    }

    protected void lnkMobileModel_Command(object sender, CommandEventArgs e)
    {
        lblModeMovieTypeSeq.Text = e.CommandArgument.ToString();
        GetData();
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            UpdateData(0);
        }
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            UpdateData(1);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        InitPage();
    }

    protected void dsModelMovieType_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.ReturnValue != null)
        {
            recCount = e.ReturnValue.ToString();
        }
    }

}
