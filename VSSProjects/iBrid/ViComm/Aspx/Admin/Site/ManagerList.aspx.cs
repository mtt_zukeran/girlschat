﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 担当メンテナンス
--	Progaram ID		: ManagerList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;

public partial class Site_ManagerList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_SITE_OWNER)) {
			throw new ApplicationException("ViComm権限違反");
		}
		if (!IsPostBack) {
			InitPage();
		}
	}

	protected void dsManager_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		grdManager.PageSize = 999;
		grdManager.PageIndex = 0;
		lstProductionSeq.SelectedIndex = 0;
		lblManagerSeq.Text = "";
		ClearField();
		pnlMainte.Visible = false;
		DataBind();
	}

	private void ClearField() {
		txtManagerNm.Text = "";
		txtEmailAddr.Text = "";
		txtTel.Text = "";
		txtLoginId.Text = "";
		txtLoginPassword.Text = "";
		lblRegistDay.Text = "";
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		pnlMainte.Visible = true;
		pnlKey.Enabled = true;
		pnlDtl.Visible = false;
		GetData();
		pnlKey.Enabled = true;
	}

	protected void lnkSupplementCd_Command(object sender,CommandEventArgs e) {
		string sKey = e.CommandArgument.ToString();
		int idx = sKey.IndexOf(":");
		if (idx >= 0) {
			lstProductionSeq.SelectedValue = sKey.Substring(0,idx);
			lblManagerSeq.Text = sKey.Substring(idx + 1,sKey.Length - idx - 1);
			GetData();
		}
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MANAGER_GET");
			db.ProcedureInParm("PPRODUCTION_SEQ",DbSession.DbType.VARCHAR2,lstProductionSeq.SelectedValue);
			db.ProcedureInParm("PMANAGER_SEQ",DbSession.DbType.VARCHAR2,lblManagerSeq.Text);
			db.ProcedureOutParm("PLOGIN_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PLOGIN_PASSWORD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMANAGER_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PTEL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PEMAIL_ADDR",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREGIST_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtManagerNm.Text = db.GetStringValue("PMANAGER_NM");
				txtTel.Text = db.GetStringValue("PTEL");
				txtEmailAddr.Text = db.GetStringValue("PEMAIL_ADDR");
				lblRegistDay.Text = db.GetStringValue("PREGIST_DATE");
				txtLoginId.Text = db.GetStringValue("PLOGIN_ID");
				txtLoginPassword.Text = db.GetStringValue("PLOGIN_PASSWORD");
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;
	}

	private void UpdateData(int pDelFlag) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MANAGER_MAINTE");
			db.ProcedureInParm("PPRODUCTION_SEQ",DbSession.DbType.VARCHAR2,lstProductionSeq.SelectedValue);
			db.ProcedureInParm("PMANAGER_SEQ",DbSession.DbType.VARCHAR2,lblManagerSeq.Text);
			db.ProcedureInParm("PLOGIN_ID",DbSession.DbType.VARCHAR2,txtLoginId.Text);
			db.ProcedureInParm("PLOGIN_PASSWORD",DbSession.DbType.VARCHAR2,txtLoginPassword.Text);
			db.ProcedureInParm("PMANAGER_NM",DbSession.DbType.VARCHAR2,txtManagerNm.Text);
			db.ProcedureInParm("PTEL",DbSession.DbType.VARCHAR2,txtTel.Text);
			db.ProcedureInParm("PEMAIL_ADDR",DbSession.DbType.VARCHAR2,txtEmailAddr.Text);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		int iIdx = lstProductionSeq.SelectedIndex;
		InitPage();
	}
	protected void vdcLoginId_ServerValidate(object source,ServerValidateEventArgs args) {
	/*
		using (Manager oManager = new Manager()) {
			args.IsValid = !oManager.IsExistLoginId(txtLoginId.Text);
		}
		if (args.IsValid == true) {
	*/		using (Admin oAdmin = new Admin()) {
				args.IsValid = !oAdmin.IsExistLoginId(txtLoginId.Text);
			}
	//	}
	}
}
