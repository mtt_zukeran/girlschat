<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SiteHtmlDocList.aspx.cs" Inherits="Site_SiteHtmlDocList" Title="サイト構成HTML文章設定"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="サイト構成HTML文章設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<asp:Literal ID="ltlUserColor" runat="server"></asp:Literal>
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<asp:Label ID="lblHtmlDocSeq" runat="server" Text="" Visible="false"></asp:Label>
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									サイトコード
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD">
									</asp:DropDownList>
								</td>
								<td class="tdHeaderStyle2">
									端末種別
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstUserAgentType" runat="server" DataSourceID="dsUserAgentType" DataTextField="CODE_NM" DataValueField="CODE">
									</asp:DropDownList>
								</td>
								<td class="tdHeaderStyle2">
									HTML文章コード
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtHtmlDocType" runat="server" MaxLength="4" Width="40px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrHtmlDocType" runat="server" ErrorMessage="HTML文章コードを入力して下さい。" ControlToValidate="txtHtmlDocType" ValidationGroup="Key">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeHtmlDocType" runat="server" ErrorMessage="HTML文章コードは3〜4桁の数字で入力して下さい。" ValidationExpression="\d{3,4}" ControlToValidate="txtHtmlDocType"
										ValidationGroup="Key">*</asp:RegularExpressionValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="Button1" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
				<table border="0">
					<tr>
						<td valign="top">
							<asp:Panel runat="server" ID="pnlSelect" Height="580px" Width="304px" ScrollBars="Auto">
								<fieldset class="fieldset-inner">
									<legend>[アウトルック]</legend>
									<asp:GridView ID="grdOutLook" runat="server" AllowPaging="False" AutoGenerateColumns="False" DataSourceID="dsOutLook" SkinID="GridView" Width="230px" ShowHeader="False">
										<Columns>
											<asp:TemplateField>
												<ItemTemplate>
													<asp:LinkButton ID="lnkDoc" runat="server" CommandArgument='<%# Eval("HTML_DOC_SUB_SEQ") %>' Text='<%# string.Format("{0:D2} {1}",Eval("HTML_DOC_SUB_SEQ"),Eval("HTML_DOC_TITLE")) %>'
														Font-Bold="True" Font-Underline="True" OnCommand="lnkDoc_Command"></asp:LinkButton>
													<br />
													<asp:Literal ID="lblHtmlDoc1" runat="server" Text='<%# ReplaceForm(string.Format("<table class=\"userColor\" ><tr><td width=\"220px\">{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}{11}{12}{13}{14}{15}{16}{17}{18}{19}</tr></td></table>",Eval("HTML_DOC1"), Eval("HTML_DOC2"), Eval("HTML_DOC3"), Eval("HTML_DOC4"), Eval("HTML_DOC5"), Eval("HTML_DOC6"),Eval("HTML_DOC7"), Eval("HTML_DOC8") ,Eval("HTML_DOC9"), Eval("HTML_DOC10"),Eval("HTML_DOC11"), Eval("HTML_DOC12"), Eval("HTML_DOC13"), Eval("HTML_DOC14"), Eval("HTML_DOC15"), Eval("HTML_DOC16"),Eval("HTML_DOC17"), Eval("HTML_DOC18") ,Eval("HTML_DOC19"), Eval("HTML_DOC20")))%>'></asp:Literal>
												</ItemTemplate>
												<ItemStyle HorizontalAlign="Left" />
											</asp:TemplateField>
										</Columns>
									</asp:GridView>
								</fieldset>
							</asp:Panel>
						</td>
						<td valign="top">
							<asp:Panel runat="server" ID="pnlDtl" Width="901px">
								<fieldset class="fieldset-inner">
									<legend>[文章内容]</legend>
									<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seektopbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
									<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seektopbutton" OnClick="btnCancel_Click" CausesValidation="False" />
									<asp:PlaceHolder ID="plcSample" runat="server">
										<asp:LinkButton ID="lnkVariableSampleMan" runat="server" OnCommand="lnkVariableSample_Click" CommandArgument="1">
											<%= DisplayWordUtil.Replace("男性用変数表") %>
										</asp:LinkButton>&nbsp;
										<asp:LinkButton ID="lnkVariableSampleWoman" runat="server" OnCommand="lnkVariableSample_Click" CommandArgument="2">
											<%= DisplayWordUtil.Replace("女性用変数表") %>
										</asp:LinkButton>&nbsp;
										<asp:LinkButton ID="lnkPictographSample" runat="server" OnClick="lnkPictographSample_Click">
											絵文字一覧
										</asp:LinkButton>
									</asp:PlaceHolder>
									<asp:Button runat="server" ID="btnDelete" Text="削除" OnClick="btnDelete_Click" ValidationGroup="Key" CssClass="delBtnStyle" />
									<asp:Label runat="server" ID="lblErrorInputSizeOver" Text="本文の入力文字数が制限値を超えています。" ForeColor="red" Visible="false" CssClass="f_clear"></asp:Label>
									<br clear="all" />
									$NO_TRANS_START;
									<table border="0" style="width: 870px" class="tableStyle f_clear">
										<tr>
											<td class="tdHeaderStyle" nowrap="nowrap">
												文章名
											</td>
											<td class="tdDataStyle" colspan="3">
												<asp:TextBox ID="txtHtmlDocTypeNm" runat="server" MaxLength="50" Width="400px"></asp:TextBox>
												<asp:RequiredFieldValidator ID="vdrHtmlDocTypeNm" runat="server" ErrorMessage="HTML文章タイトルを入力して下さい。" ControlToValidate="txtHtmlDocTypeNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
												<asp:RegularExpressionValidator ID="vdeHtmlDocTypeNm" runat="server" ErrorMessage="HTML文章タイトルは30文字以下で入力して下さい。" ValidationExpression=".{1,30}" ControlToValidate="txtHtmlDocTypeNm"
									            ValidationGroup="Detail">*</asp:RegularExpressionValidator>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderStyle" nowrap="nowrap">
												ﾀｲﾄﾙ
											</td>
											<td class="tdDataStyle" colspan="3">
												<asp:TextBox ID="txtPageTile" runat="server" Width="500px" MaxLength="100"></asp:TextBox>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderStyle" nowrap="nowrap">
												ｷｰﾜｰﾄﾞ
											</td>
											<td class="tdDataStyle" colspan="3">
												<asp:TextBox ID="txtPageKeyword" runat="server" Width="500px" MaxLength="100"></asp:TextBox>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderStyle" nowrap="nowrap">
												説明
											</td>
											<td class="tdDataStyle" colspan="3">
												<asp:TextBox ID="txtPageDescription" runat="server" Width="500px" MaxLength="100"></asp:TextBox>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderStyle" nowrap="nowrap">
												利用性別
											</td>
											<td class="tdDataStyle" colspan="3">
												<asp:DropDownList ID="lstSexCd" runat="server" DataSourceID="dsSexCd" DataTextField="CODE_NM" DataValueField="CODE" Width="100px">
												</asp:DropDownList>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderStyle" nowrap="nowrap">
												INDEX
											</td>
											<td class="tdDataStyle" colspan="3">
												<asp:DropDownList ID="lstTableIndex" runat="server" DataSourceID="dsTableIndex" DataTextField="CODE_NM" DataValueField="CODE">
												</asp:DropDownList>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderStyle" nowrap="nowrap">
												ｶﾗｰﾊﾟﾀｰﾝ
											</td>
											<td class="tdDataStyle" colspan="3">
												<asp:DropDownList ID="lstWebFaceSeq" runat="server" Width="206px" DataSourceID="dsWebFace" DataTextField="HTML_FACE_NAME" DataValueField="WEB_FACE_SEQ">
												</asp:DropDownList>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderStyle" nowrap="nowrap">
												CSS名1
											</td>
											<td class="tdDataStyle">
												<asp:TextBox ID="txtCssFileNm1" runat="server" Width="300px" MaxLength="64"></asp:TextBox>
											</td>
                                            <td class="tdHeaderStyle" nowrap="nowrap">
                                                JavaScript名1
                                            </td>
                                            <td class="tdDataStyle">
                                                <asp:TextBox ID="txtJavascriptFileNm1" runat="server" MaxLength="64" Width="300px"></asp:TextBox>
                                            </td>
										</tr>
										<tr>
											<td class="tdHeaderStyle" nowrap="nowrap">
												CSS名2
											</td>
											<td class="tdDataStyle">
												<asp:TextBox ID="txtCssFileNm2" runat="server" Width="300px" MaxLength="64"></asp:TextBox>
											</td>
                                            <td class="tdHeaderStyle" nowrap="nowrap">
                                                JavaScript名2
                                            </td>
                                            <td class="tdDataStyle">
                                                <asp:TextBox ID="txtJavascriptFileNm2" runat="server" MaxLength="64" Width="300px"></asp:TextBox>
                                            </td>
										</tr>
										<tr>
											<td class="tdHeaderStyle" nowrap="nowrap">
												CSS名3
											</td>
											<td class="tdDataStyle">
												<asp:TextBox ID="txtCssFileNm3" runat="server" Width="300px" MaxLength="64"></asp:TextBox>
											</td>
                                            <td class="tdHeaderStyle" nowrap="nowrap">
                                                JavaScript名3
                                            </td>
                                            <td class="tdDataStyle">
                                                <asp:TextBox ID="txtJavascriptFileNm3" runat="server" MaxLength="64" Width="300px"></asp:TextBox>
                                            </td>
										</tr>
										<tr>
											<td class="tdHeaderStyle" nowrap="nowrap">
												CSS名4
											</td>
											<td class="tdDataStyle">
												<asp:TextBox ID="txtCssFileNm4" runat="server" Width="300px" MaxLength="64"></asp:TextBox>
											</td>
                                            <td class="tdHeaderStyle" nowrap="nowrap">
                                                JavaScript名4
                                            </td>
                                            <td class="tdDataStyle">
                                                <asp:TextBox ID="txtJavascriptFileNm4" runat="server" MaxLength="64" Width="300px"></asp:TextBox>
                                            </td>
										</tr>
										<tr>
											<td class="tdHeaderStyle" nowrap="nowrap">
												CSS名5
											</td>
											<td class="tdDataStyle">
												<asp:TextBox ID="txtCssFileNm5" runat="server" Width="300px" MaxLength="64"></asp:TextBox>
											</td>
                                            <td class="tdHeaderStyle2" nowrap="nowrap">
                                                JavaScript名5
                                            </td>
                                            <td class="tdDataStyle">
                                                <asp:TextBox ID="txtJavascriptFileNm5" runat="server" MaxLength="64" Width="300px"></asp:TextBox>
                                            </td>
										</tr>
										<tr>
											<td class="tdHeaderStyle" nowrap="nowrap">
												canonical
											</td>
											<td class="tdDataStyle" colspan="3">
												<asp:TextBox ID="txtCanonical" runat="server" MaxLength="256" Width="500px"></asp:TextBox>
											</td>
										</tr>
										<asp:Panel runat="server" ID="pnlExtImport">
										<tr>
											<td class="tdHeaderStyle" nowrap="nowrap">
												外部読込
											</td>
											<td class="tdDataStyle" colspan="3">
												<asp:CheckBox ID="chkProtectImageFlag" Text="画像保護" runat="server" />
												&nbsp;
												<asp:CheckBox ID="chkEmojiToolFlag" Text="絵文字ツール" runat="server" />
											</td>
										</tr>
										</asp:Panel>
                                        <tr>
											<td class="tdHeaderStyle2" nowrap="nowrap">
												文章
											</td>
											<td class="tdDataStyle" colspan="3">
												<pin:pinEdit ID="txtHtmlDoc" runat="server" UserMode="HTML" DisplayStyle="Style5" DocumentView="True" HtmlMode="Body" Ruler="False" ReturnMode="BR" SpellMode="Inline"
													ToolbarConfiguration="ToolbarDesigner" Toolbar="T09SE10116012136267;T22232427282930313233343536;B53SE54SE55" FormatMode="Classic" BorderWidth="1px"
													Height="500px" Width="770px" DocumentWidth="230" Font-Bold="False" FontSizeType="px" RelativeBaseImageUrl="/" RelativeBaseLinkUrl="/" RelativeImageRoot="/">
												</pin:pinEdit>
												<asp:TextBox ID="txtHtmlDocText" runat="server" TextMode="multiLine" Height="500px" Width="770px" Visible="false"></asp:TextBox>
											</td>
										</tr>
									</table>
									$NO_TRANS_END;
									<asp:Button runat="server" ID="btnUpdate2" Text="更新" CssClass="seektopbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
									<asp:Button runat="server" ID="btnCancel2" Text="キャンセル" CssClass="seektopbutton" OnClick="btnCancel_Click" CausesValidation="False" />
									<asp:Button runat="server" ID="btnDelete2" Text="削除" CssClass="delBtnStyle" OnClick="btnDelete_Click" ValidationGroup="Key" />
								</fieldset>
							</asp:Panel>
						</td>
					</tr>
				</table>
			</fieldset>
		</asp:Panel>
		<fieldset class="f_clear">
			<legend>[サイト構成HTML文章一覧]</legend>
			<table border="0" style="width:600px; margin-bottom:3px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle">
						サイトコード
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD">
						</asp:DropDownList>
					</td>
					<td class="tdHeaderStyle2">
						端末種別
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekUserAgentType" runat="server" DataSourceID="dsUserAgentType" DataTextField="CODE_NM" DataValueField="CODE">
						</asp:DropDownList>
					</td>
				</tr>
				<tr>
					<td class="tdHeaderStyle2">
						HTML文章コード
					</td>
					<td class="tdDataStyle" colspan="3">
						<asp:TextBox ID="txtSeekHtmlDocType" runat="server" Text="" Width="40px" MaxLength="4" />
						※部分一致
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnRegist" Text="文章追加" CssClass="seekbutton" OnClick="btnRegist_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" CausesValidation="False" OnClick="btnCSV_Click" /><br />
			<br />
			$NO_TRANS_START;
			<asp:GridView ID="grdSiteHtmlDoc" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsSiteHtmlDoc" AllowSorting="True" SkinID="GridViewColor" OnSorting="grdSiteHtmlDoc_Sorting">
				<Columns>
					<asp:TemplateField HeaderText="文章種別" SortExpression="HTML_DOC_TYPE">
						<ItemTemplate>
							<asp:LinkButton ID="lnkHtmlDocSeq" runat="server" Text='<%# string.Format("{0} {1}",Eval("HTML_DOC_TYPE"),Eval("HTML_DOC_TYPE_NM")) %>' CommandArgument='<%# string.Format("{0}:{1}:{2}:{3}:{4}",Eval("SITE_CD"),Eval("HTML_DOC_SEQ"),Eval("HTML_DOC_TYPE"),Eval("HTML_DOC_TYPE_NM"),Eval("USER_AGENT_TYPE")) %>'
								OnCommand="lnkHtmlDocSeq_Command" CausesValidation="False">
							</asp:LinkButton>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Left" Width="300px" />
					</asp:TemplateField>
					<asp:BoundField DataField="USER_AGENT_TYPE_NM" HeaderText="端末種別">
						<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
					</asp:BoundField>
					<asp:BoundField DataField="DOC" HeaderText="HTML文章">
						<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
					</asp:BoundField>
					<asp:BoundField DataField="UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False" SortExpression="UPDATE_DATE">
						<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
					</asp:BoundField>
					<asp:BoundField DataField="TABLE_INDEX_NM" HeaderText="INDEX">
						<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
					</asp:BoundField>
					<asp:BoundField DataField="SEX_NM" HeaderText="性別">
						<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
			</asp:GridView>
			$NO_TRANS_END; &nbsp;
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br />
				<a class="reccount">Current viewing page
					<%=grdSiteHtmlDoc.PageIndex + 1%>
					of
					<%=grdSiteHtmlDoc.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsSiteHtmlDoc" runat="server" SelectMethod="GetPageCollection" TypeName="SiteHtmlDoc" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsSiteHtmlDoc_Selected" OnSelecting="dsSiteHtmlDoc_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserAgentType" Type="String" />
			<asp:Parameter Name="pHtmlDocType" Type="String" />
            <asp:Parameter Name="pSortExpression" Type="String" />
            <asp:Parameter Name="pSortDirection" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsOutLook" runat="server" SelectMethod="GetListByDocSeq" TypeName="SiteHtmlDoc" OnSelecting="dsOutLook_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pHtmlDocSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsTableIndex" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="39" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSexCd" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="36" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUserAgentType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="06" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsWebFace" runat="server" SelectMethod="GetList" TypeName="WebFace"></asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrHtmlDocType" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeHtmlDocType" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrHtmlDocTypeNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdeHtmlDocTypeNm" HighlightCssClass="validatorCallout" />	
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender3" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnUpdate2" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender4" runat="server" TargetControlID="btnDelete2" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtSeekHtmlDocType" />
</asp:Content>
