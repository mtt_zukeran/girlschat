﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ユーザーランクメンテナンス
--	Progaram ID		: UserRankList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Site_UserRankList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_SITE_OWNER)) {
			throw new ApplicationException("ViComm権限違反");
		}
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
	}

	private void FirstLoad() {
		grdUserRank.PageSize = 999;
		string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		if (sSiteCd.Equals("")) {
			lstSeekSiteCd.SelectedIndex = 0;
		} else {
			lstSeekSiteCd.SelectedValue = sSiteCd;
		}
		lstSeekSiteCd.DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		DataBind();
		lstWebFaceSeq.DataSourceID = "";
		lstWebFaceSeq.SelectedIndex = 0;
		int iCompanyMask = int.Parse(Session["CompanyMask"].ToString());

		using (ManageCompany oCompany = new ManageCompany()) {
			if (oCompany.IsAvailableService(ViCommConst.RELEASE_KICK_BACK_INTRO_SYSTEM)) {
				plcIntroPointRate.Visible = true;
				grdUserRank.Columns[7].Visible = true;
			} else {
				plcIntroPointRate.Visible = false;
				grdUserRank.Columns[7].Visible = false;
			}
		}
		using (ManageCompany oCompany = new ManageCompany()) {
			if (oCompany.IsAvailableService(ViCommConst.RELEASE_RICHINO)) {
				plcUserRankKeepAmt.Visible = true;
				grdUserRank.Columns[8].Visible = true;
			} else {
				plcUserRankKeepAmt.Visible = false;
				grdUserRank.Columns[8].Visible = false;
			}
		}
	}

	private void InitPage() {
		txtUserRank.Text = "";
		ClearField();
		pnlMainte.Visible = false;
	}

	private void ClearField() {
		txtUserRankNm.Text = "";
		txtTotalReceiptAmt.Text = "";
		txtLimitPoint.Text = "";
		if (lstWebFaceSeq.Items.Count > 0) {
			lstWebFaceSeq.SelectedIndex = 0;
		}
		txtIntroPointRate.Text = "0";
		txtUserRankKeepAmt.Text = "0";
		txtBingoBallRate.Text = "0";
		chkPointAffiliateRankFlag.Checked = false;
		vdcPointAffiliateUserRankFlag.Text = "";
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	private void GetList() {
		grdUserRank.PageIndex = 0;
		grdUserRank.DataBind();
		pnlCount.DataBind();
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		pnlMainte.Visible = true;
		pnlKey.Enabled = true;
		pnlDtl.Visible = false;
		lstSiteCd.SelectedIndex = lstSeekSiteCd.SelectedIndex;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void lnkUserRank_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		lstSiteCd.SelectedValue = sKeys[0];
		txtUserRank.Text = sKeys[1];
		GetData();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void dsUserRank_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void vdcPointAffiliateUserRankFlag_ServerValidate(object source,ServerValidateEventArgs args) {
		if (IsValid) {
			if (chkPointAffiliateRankFlag.Checked) {
				if (int.Parse(txtTotalReceiptAmt.Text) != 0) {
					args.IsValid = false;
				} else {
					using (UserRank oRank = new UserRank()) {
						args.IsValid = (oRank.IsExistPointAffliateUserRank(lstSiteCd.SelectedValue,txtUserRank.Text) == false);
					}
				}
			}
			if (args.IsValid == false) {
				vdcPointAffiliateUserRankFlag.Text = "<br>入金累計額が0でないか、既に他のﾗﾝｸに使用されています。";
			}
		}
	}

	protected void vdcBingoBallRate_ServerValidate(object source,ServerValidateEventArgs args) {
		if (IsValid) {

			if (txtBingoBallRate.Text.Equals("")) {
				args.IsValid = false;
				vdcBingoBallRate.Text = "BINGOﾎﾞｰﾙ出現率を入力して下さい。";
			} else {
				double dBingoBallRate;
				bool bParse = double.TryParse(txtBingoBallRate.Text,out dBingoBallRate);

				if (bParse == false) {
					args.IsValid = false;
					vdcBingoBallRate.Text = "数値が正しくありません";
				} else if (dBingoBallRate > 100) {
					args.IsValid = false;
					vdcBingoBallRate.Text = "数値が正しくありません";
				} else if (dBingoBallRate < 0) {
					args.IsValid = false;
					vdcBingoBallRate.Text = "数値が正しくありません";
				}
			}
		}
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("USER_RANK_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PUSER_RANK",DbSession.DbType.VARCHAR2,txtUserRank.Text);
			db.ProcedureOutParm("PUSER_RANK_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PTOTAL_RECEIPT_AMT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PLIMIT_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PWEB_FACE_SEQ",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PINTRO_POINT_RATE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PUSER_RANK_KEEP_AMT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PPOINT_AFFILIATE_RANK_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PBINGO_BALL_RATE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtUserRankNm.Text = db.GetStringValue("PUSER_RANK_NM");
				txtTotalReceiptAmt.Text = db.GetStringValue("PTOTAL_RECEIPT_AMT");
				txtLimitPoint.Text = db.GetStringValue("PLIMIT_POINT");
				if (!db.GetStringValue("PWEB_FACE_SEQ").Equals("")) {
					lstWebFaceSeq.SelectedValue = db.GetStringValue("PWEB_FACE_SEQ");
				}
				txtIntroPointRate.Text = db.GetStringValue("PINTRO_POINT_RATE");
				txtUserRankKeepAmt.Text = db.GetStringValue("PUSER_RANK_KEEP_AMT");
				chkPointAffiliateRankFlag.Checked = (db.GetIntValue("PPOINT_AFFILIATE_RANK_FLAG") != 0);
				txtBingoBallRate.Text = db.GetDecimalValue("PBINGO_BALL_RATE").ToString();
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		lstSeekSiteCd.SelectedIndex = lstSiteCd.SelectedIndex;
		pnlKey.Enabled = false;
	}

	private void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("USER_RANK_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PUSER_RANK",DbSession.DbType.VARCHAR2,txtUserRank.Text);
			db.ProcedureInParm("PUSER_RANK_NM",DbSession.DbType.VARCHAR2,txtUserRankNm.Text);
			db.ProcedureInParm("PTOTAL_RECEIPT_AMT",DbSession.DbType.NUMBER,txtTotalReceiptAmt.Text);
			db.ProcedureInParm("PLIMIT_POINT",DbSession.DbType.NUMBER,txtLimitPoint.Text);
			db.ProcedureInParm("PWEB_FACE_SEQ",DbSession.DbType.VARCHAR2,lstWebFaceSeq.SelectedValue);
			db.ProcedureInParm("PINTRO_POINT_RATE",DbSession.DbType.NUMBER,txtIntroPointRate.Text);
			db.ProcedureInParm("PUSER_RANK_KEEP_AMT",DbSession.DbType.NUMBER,txtUserRankKeepAmt.Text);
			db.ProcedureInParm("PPOINT_AFFILIATE_RANK_FLAG",DbSession.DbType.NUMBER,chkPointAffiliateRankFlag.Checked);
			db.ProcedureInParm("PBINGO_BALL_RATE",DbSession.DbType.NUMBER,txtBingoBallRate.Text);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		int iIdx = lstSiteCd.SelectedIndex;
		InitPage();
		GetList();
		lstSeekSiteCd.SelectedIndex = iIdx;
	}

	protected void dsUserRank_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
	}

	protected bool IsEnabledExpandedCharge() {
		using (ManageCompany oCompany = new ManageCompany()) {
			return oCompany.IsAvailableService(ViCommConst.RELEASE_ENABLE_EXPANDED_CHARGE,2);			
		}
	}
}
