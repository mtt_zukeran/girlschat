﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: サイト補足区分メンテナンス
--	Progaram ID		: SiteSupplementList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;

public partial class Site_SiteSupplementList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_SITE_OWNER)) {
			throw new ApplicationException("ViComm権限違反");
		}
		if (!IsPostBack) {
			InitPage();
		}
	}

	protected void dsSiteSupplement_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		grdSiteSupplement.PageSize = 999;
		lstSiteCd.SelectedIndex = 0;
		lblSupplementCd.Text = "";
		ClearField();
		pnlMainte.Visible = false;
		lstSiteCd.DataBind();
		lstSeekSiteCd.DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		//DataBind();
	}

	private void ClearField() {
		txtSupplementValue.Text = "";
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void lnkSupplementCd_Command(object sender,CommandEventArgs e) {
		string sKey = e.CommandArgument.ToString();
		int idx = sKey.IndexOf(":");
		if (idx >= 0) {
			lstSiteCd.SelectedValue = sKey.Substring(0,idx);
			lblSupplementCd.Text = sKey.Substring(idx + 1,sKey.Length - idx - 1);
			GetData();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}


	private void GetList() {
		grdSiteSupplement.DataBind();
		pnlCount.DataBind();
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SITE_SUPPLEMENT_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PSUPPLEMENT_CD",DbSession.DbType.VARCHAR2,lblSupplementCd.Text);
			db.ProcedureOutParm("PSUPPLEMENT_VALUE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSUPPLEMENT_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtSupplementValue.Text = db.GetStringValue("PSUPPLEMENT_VALUE");
				lblSupplementNm.Text = db.GetStringValue("PSUPPLEMENT_NM");
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;

	}

	private void UpdateData(int pDelFlag) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SITE_SUPPLEMENT_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PSUPPLEMENT_CD",DbSession.DbType.VARCHAR2,lblSupplementCd.Text);
			db.ProcedureInParm("PSUPPLEMENT_VALUE",DbSession.DbType.VARCHAR2,txtSupplementValue.Text);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,0);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		int iIdx = lstSiteCd.SelectedIndex;
		InitPage();
		lstSeekSiteCd.SelectedIndex = iIdx;
		GetList();
	}

	protected void dsSiteSupplement_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
	}
}
