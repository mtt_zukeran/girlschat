﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: Ｗｅｂデザイン設定
--	Progaram ID		: WebFaceList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;

public partial class Site_WebFaceList:System.Web.UI.Page {
	private string recCount = string.Empty;

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
	}

	private void InitPage() {
		ClearField();
		pnlMainte.Visible = false;
		DataBind();
	}

	private void FirstLoad() {
		grdWebFace.PageSize = int.Parse(Session["PageSize"].ToString());
		grdWebFace.PageIndex = 0;
		lblWebFaceSeq.Text = iBridUtil.GetStringValue(Request.QueryString["webfaceseq"]);
		DataBind();
		if (!lblWebFaceSeq.Text.Equals(string.Empty)) {
			GetData();
		}
	}

	private void ClearField() {
		lblWebFaceSeq.Text = string.Empty;
		txtHtmlFaceNm.Text = string.Empty;

		txtColorBack.Text = string.Empty;
		txtColorChar.Text = string.Empty;
		txtColorIndex.Text = string.Empty;
		txtColorLine.Text = string.Empty;
		txtColorALink.Text = string.Empty;
		txtColorVLink.Text = string.Empty;
		txtSizeChar.Text = string.Empty;
		txtSizeLine.Text = string.Empty;
		txtColorBackWoman.Text = string.Empty;
		txtColorCharWoman.Text = string.Empty;
		txtColorIndexWoman.Text = string.Empty;
		txtColorLineWoman.Text = string.Empty;
		txtColorALinkWoman.Text = string.Empty;
		txtColorVLinkWoman.Text = string.Empty;
		txtSizeCharWoman.Text = string.Empty;
		txtSizeLineWoman.Text = string.Empty;
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		GetData();
	}

	protected void lnkWebFaceSeq_Command(object sender,CommandEventArgs e) {
		lblWebFaceSeq.Text = e.CommandArgument.ToString();
		GetData();
	}

	protected void dsWebFace_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("WEB_FACE_GET");
			db.ProcedureInParm("PWEB_FACE_SEQ",DbSession.DbType.VARCHAR2,lblWebFaceSeq.Text);
			db.ProcedureOutParm("PHTML_FACE_NAME",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCOLOR_LINE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCOLOR_INDEX",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCOLOR_CHAR",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCOLOR_BACK",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCOLOR_LINK",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCOLOR_ALINK",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCOLOR_VLINK",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSIZE_LINE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSIZE_CHAR",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCOLOR_LINE_WOMAN", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCOLOR_INDEX_WOMAN", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCOLOR_CHAR_WOMAN", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCOLOR_BACK_WOMAN", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCOLOR_LINK_WOMAN", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCOLOR_ALINK_WOMAN", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCOLOR_VLINK_WOMAN", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSIZE_LINE_WOMAN", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSIZE_CHAR_WOMAN", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtHtmlFaceNm.Text = db.GetStringValue("PHTML_FACE_NAME");

				txtColorBack.Text = db.GetStringValue("PCOLOR_BACK");
				txtColorChar.Text = db.GetStringValue("PCOLOR_CHAR");
				txtColorIndex.Text = db.GetStringValue("PCOLOR_INDEX");
				txtColorLine.Text = db.GetStringValue("PCOLOR_LINE");
				txtColorLink.Text = db.GetStringValue("PCOLOR_LINK");
				txtColorALink.Text = db.GetStringValue("PCOLOR_ALINK");
				txtColorVLink.Text = db.GetStringValue("PCOLOR_VLINK");
				txtSizeChar.Text = db.GetStringValue("PSIZE_CHAR");
				txtSizeLine.Text = db.GetStringValue("PSIZE_LINE");
				txtColorBackWoman.Text = db.GetStringValue("PCOLOR_BACK_WOMAN");
				txtColorCharWoman.Text = db.GetStringValue("PCOLOR_CHAR_WOMAN");
				txtColorIndexWoman.Text = db.GetStringValue("PCOLOR_INDEX_WOMAN");
				txtColorLineWoman.Text = db.GetStringValue("PCOLOR_LINE_WOMAN");
				txtColorLinkWoman.Text = db.GetStringValue("PCOLOR_LINK_WOMAN");
				txtColorALinkWoman.Text = db.GetStringValue("PCOLOR_ALINK_WOMAN");
				txtColorVLinkWoman.Text = db.GetStringValue("PCOLOR_VLINK_WOMAN");
				txtSizeCharWoman.Text = db.GetStringValue("PSIZE_CHAR_WOMAN");
				txtSizeLineWoman.Text = db.GetStringValue("PSIZE_LINE_WOMAN");
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
	}

	private void UpdateData(int pDelFlag) {
		if(txtColorIndex.Text == string.Empty){
			txtColorIndex.Text = "#000000";
		}
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("WEB_FACE_MAINTE");
			db.ProcedureInParm("PHTML_FACE_NAME",DbSession.DbType.VARCHAR2,txtHtmlFaceNm.Text);
			db.ProcedureInParm("PCOLOR_LINE", DbSession.DbType.VARCHAR2, txtColorLine.Text);
			db.ProcedureInParm("PCOLOR_INDEX", DbSession.DbType.VARCHAR2, txtColorIndex.Text);
			db.ProcedureInParm("PCOLOR_CHAR", DbSession.DbType.VARCHAR2, txtColorChar.Text);
			db.ProcedureInParm("PCOLOR_BACK", DbSession.DbType.VARCHAR2, txtColorBack.Text);
			db.ProcedureInParm("PCOLOR_LINK", DbSession.DbType.VARCHAR2, txtColorLink.Text);
			db.ProcedureInParm("PCOLOR_ALINK", DbSession.DbType.VARCHAR2, txtColorALink.Text);
			db.ProcedureInParm("PCOLOR_VLINK", DbSession.DbType.VARCHAR2, txtColorVLink.Text);
			db.ProcedureInParm("PSIZE_LINE", DbSession.DbType.VARCHAR2, int.Parse(txtSizeLine.Text));
			db.ProcedureInParm("PSIZE_CHAR", DbSession.DbType.VARCHAR2, int.Parse(txtSizeChar.Text));
			db.ProcedureInParm("PCOLOR_LINE_WOMAN", DbSession.DbType.VARCHAR2, txtColorLineWoman.Text);
			db.ProcedureInParm("PCOLOR_INDEX_WOMAN", DbSession.DbType.VARCHAR2, txtColorIndexWoman.Text);
			db.ProcedureInParm("PCOLOR_CHAR_WOMAN", DbSession.DbType.VARCHAR2, txtColorCharWoman.Text);
			db.ProcedureInParm("PCOLOR_BACK_WOMAN", DbSession.DbType.VARCHAR2, txtColorBackWoman.Text);
			db.ProcedureInParm("PCOLOR_LINK_WOMAN", DbSession.DbType.VARCHAR2, txtColorLinkWoman.Text);
			db.ProcedureInParm("PCOLOR_ALINK_WOMAN", DbSession.DbType.VARCHAR2, txtColorALinkWoman.Text);
			db.ProcedureInParm("PCOLOR_VLINK_WOMAN", DbSession.DbType.VARCHAR2, txtColorVLinkWoman.Text);
			db.ProcedureInParm("PSIZE_LINE_WOMAN", DbSession.DbType.VARCHAR2, int.Parse(txtSizeLineWoman.Text));
			db.ProcedureInParm("PSIZE_CHAR_WOMAN", DbSession.DbType.VARCHAR2, int.Parse(txtSizeCharWoman.Text));
			db.ProcedureInParm("PROWID", DbSession.DbType.VARCHAR2, ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		InitPage();
	}
	protected void lnkColorSample_Click(object sender,EventArgs e) {
		Response.Redirect("~/Site/ColorSample.aspx");
	}
}
