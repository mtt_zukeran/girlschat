<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UserDefineTagList.aspx.cs" Inherits="Site_UserDefineTagList"
	Title="ユーザー定義タグ設定" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="ユーザー定義タグ設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<asp:Literal ID="ltlUserColor" runat="server"></asp:Literal>
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 840px" class="tableStyle">
							<tr>
								<td class="tdHeaderSmallStyle2">
									サイトコード
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
									</asp:DropDownList>
								</td>
								<td class="tdHeaderSmallStyle2">
									端末種別
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstUserAgentType" runat="server" DataSourceID="dsUserAgentType" DataTextField="CODE_NM" DataValueField="CODE" Width="90px">
									</asp:DropDownList>
								</td>
								<td class="tdHeaderSmallStyle2">
									変数ID
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtVariableId" runat="server" MaxLength="40" Width="200px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrVariableId" runat="server" ErrorMessage="変数IDを入力して下さい。" ControlToValidate="txtVariableId" ValidationGroup="Key">*</asp:RequiredFieldValidator>
									<asp:CustomValidator ID="vdcVariableId" runat="server" ErrorMessage="変数は$で開始されている2文字以上の英数字をしてください。" ControlToValidate="txtVariableId" OnServerValidate="vdcVariableId_ServerValidate"
										ValidationGroup="Key">*</asp:CustomValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="Button1" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
				<table border="0">
					<tr>
						<td valign="top">
							<asp:Panel runat="server" ID="pnlDtl">
								<fieldset class="fieldset-inner">
									<legend>[変数内容]</legend>
									<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seektopbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
									<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seektopbutton" OnClick="btnCancel_Click" CausesValidation="False" />
									<asp:Button runat="server" ID="btnDelete" Text="削除" OnClick="btnDelete_Click" ValidationGroup="Key" CssClass="delBtnStyle" />
									<asp:Label runat="server" ID="lblErrorInputSizeOver" Text="本文の入力文字数が制限値を超えています。" ForeColor="red" Visible="false" CssClass="f_clear"></asp:Label>
									<br clear="all" />
									<table border="0" class="f_clear">
										<tr>
											<td class="tdHeaderVerySmallStyle">
												変数概要
											</td>
											<td class="tdDataStyle">
												<asp:TextBox ID="txtVariableNm" runat="server" MaxLength="100" Width="400px"></asp:TextBox>
												<asp:RequiredFieldValidator ID="vdrVariableNm" runat="server" ErrorMessage="変数概要を入力して下さい。" ControlToValidate="txtVariableNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderVerySmallStyle">
												カテゴリ
											</td>
											<td class="tdDataStyle">
												<asp:ListBox ID="lstCategorySeq" runat="server" Rows="1">
													<asp:ListItem Value="">未設定</asp:ListItem>
													<asp:ListItem Value="1">ｶﾞｰﾙｽﾞﾁｬｯﾄ</asp:ListItem>
													<asp:ListItem Value="2">美女ｺﾚ</asp:ListItem>
													<asp:ListItem Value="3">共通</asp:ListItem>
													<asp:ListItem Value="4">その他</asp:ListItem>
												</asp:ListBox>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderVerySmallStyle">
												HTML文章
											</td>
											<td class="tdDataStyle">
												<pin:pinEdit ID="txtHtmlDoc" runat="server" UserMode="HTML" DisplayStyle="Style5" DocumentView="True" HtmlMode="Body" Ruler="False" ReturnMode="BR" SpellMode="Inline"
													ToolbarConfiguration="ToolbarDesigner" Toolbar="T09SE10116012136267;T22232427282930313233343536;B53SE54SE55" FormatMode="Classic" BorderWidth="1px"
													Height="500px" Width="900px" DocumentWidth="230" Font-Bold="False" FontSizeType="px" RelativeBaseImageUrl="/" RelativeBaseLinkUrl="/" RelativeImageRoot="/"
													TextMode="HTMLBody" XHTMLType="XHTML_10_TRANS">
												</pin:pinEdit>
												<asp:TextBox ID="txtHtmlDocText" runat="server" TextMode="MultiLine" Height="500px" Width="900px" Visible="false"></asp:TextBox>
											</td>
										</tr>
									</table>
									<asp:Button runat="server" ID="btnUpdate2" Text="更新" CssClass="seektopbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
									<asp:Button runat="server" ID="btnCancel2" Text="キャンセル" CssClass="seektopbutton" OnClick="btnCancel_Click" CausesValidation="False" />
									<asp:Button runat="server" ID="btnDelete2" Text="削除" OnClick="btnDelete_Click" ValidationGroup="Key" CssClass="delBtnStyle" />
								</fieldset>
							</asp:Panel>
						</td>
					</tr>
				</table>
			</fieldset>
		</asp:Panel>
		<fieldset class="f_clear">
			<legend>[ユーザー定義タグ一覧]</legend>
			<table border="0" style="width: 800px; margin-bottom:3px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle">
						サイトコード
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD">
						</asp:DropDownList>
					</td>
					<td class="tdHeaderStyle2">
						端末種別
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekUserAgentType" runat="server" DataSourceID="dsUserAgentType" DataTextField="CODE_NM" DataValueField="CODE">
						</asp:DropDownList>
					</td>
					<td class="tdHeaderStyle2">
						カテゴリ
					</td>
					<td class="tdDataStyle">
						<asp:ListBox ID="lstSCategorySeq" runat="server" Rows="1">
							<asp:ListItem Value="" Text=""></asp:ListItem>
							<asp:ListItem Value="null">未設定</asp:ListItem>
							<asp:ListItem Value="1">ｶﾞｰﾙｽﾞﾁｬｯﾄ</asp:ListItem>
							<asp:ListItem Value="2">美女ｺﾚ</asp:ListItem>
							<asp:ListItem Value="3">共通</asp:ListItem>
							<asp:ListItem Value="4">その他</asp:ListItem>
						</asp:ListBox>
					</td>
				</tr>
				<tr>
					<td class="tdHeaderStyle2">
						変数ID
					</td>
					<td class="tdDataStyle" colspan="5">
						<asp:TextBox ID="txtSeekVariableId" runat="server" Text="" Width="300px"></asp:TextBox>
						※部分一致
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnRegist" Text="変数追加" CssClass="seekbutton" OnClick="btnRegist_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" CausesValidation="False" OnClick="btnCSV_Click" /><br />
			<br />
			<asp:GridView ID="grdUserDefineTag" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsUserDefineTag" AllowSorting="True" SkinID="GridViewColor">
				<Columns>
					<asp:TemplateField>
						<ItemTemplate><asp:LinkButton ID="lnkVariableId" runat="server" Text='<%# Eval("VARIABLE_ID") %>' CommandArgument='<%# string.Format("{0}:{1}:{2}",Eval("SITE_CD"),Eval("VARIABLE_ID"),Eval("USER_AGENT_TYPE")) %>' OnCommand="lnkVariableId_Command" CausesValidation="False"></asp:LinkButton></ItemTemplate>
						<HeaderTemplate>
							変数ID
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
					</asp:TemplateField>
					<asp:BoundField DataField="USER_AGENT_TYPE_NM" HeaderText="端末種別">
						<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
					</asp:BoundField>
					<asp:BoundField DataField="VARIABLE_NM" HeaderText="変数概要">
						<ItemStyle HorizontalAlign="Left" Width="200px" Font-Size="X-Small" />
					</asp:BoundField>
					<asp:BoundField DataField="DOC" HeaderText="HTML文章">
						<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
					</asp:BoundField>
					<asp:BoundField DataField="CATEGORY_NM" HeaderText="カテゴリ">
						<ItemStyle HorizontalAlign="Left" Font-Size="X-Small"/>
					</asp:BoundField>
					<asp:BoundField DataField="UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
						<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
			</asp:GridView>
			&nbsp;
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br>
				<a class="reccount">Current viewing page
					<%=grdUserDefineTag.PageIndex + 1%>
					of
					<%=grdUserDefineTag.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsUserDefineTag" runat="server" SelectMethod="GetPageCollection" TypeName="UserDefineTag" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsUserDefineTag_Selected" OnSelecting="dsUserDefineTag_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserAgentType" Type="String" />
			<asp:Parameter Name="pCategorySeq" Type="String" />
			<asp:Parameter Name="pVariableId" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUserAgentType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="06" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrVariableId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrVariableNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdcVariableId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender3" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnUpdate2" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender4" runat="server" TargetControlID="btnDelete2" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
