﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: キャストランクメンテナンス
--	Progaram ID		: CastRankList
--
--  Creation Date	: 2010.08.05
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Site_CastRankList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_SITE_OWNER)) {
			throw new ApplicationException("ViComm権限違反");
		}
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
	}

	private void FirstLoad() {
		grdCastRank.PageSize = 999;
		DataBind();

		using (ManageCompany oCompany = new ManageCompany()) {
			if (oCompany.IsAvailableService(ViCommConst.RELEASE_KICK_BACK_INTRO_SYSTEM)) {
			    plcIntroPointRate.Visible = true;
			    grdCastRank.Columns[2].Visible = true;
			} else {
				plcIntroPointRate.Visible = false;
				grdCastRank.Columns[2].Visible = false;
			}
		}
	}

	private void InitPage() {
		txtUserRank.Text = "";
		ClearField();
		pnlMainte.Visible = false;
	}

	private void ClearField() {
		txtUserRankNm.Text = "";
		txtIntroPointRate.Text = "0";
		txtPointPrice.Text = "0";
		txtBingoBallRate.Text = "0";
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	private void GetList() {
		grdCastRank.PageIndex = 0;
		grdCastRank.DataBind();
		pnlCount.DataBind();
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		pnlMainte.Visible = true;
		pnlKey.Enabled = true;
		pnlDtl.Visible = false;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void lnkUserRank_Command(object sender,CommandEventArgs e) {
		txtUserRank.Text = e.CommandArgument.ToString();
		GetData();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void dsCastRank_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void vdcBingoBallRate_ServerValidate(object source,ServerValidateEventArgs args) {
		if (IsValid) {

			if (txtBingoBallRate.Text.Equals("")) {
				args.IsValid = false;
				vdcBingoBallRate.Text = "BINGOﾎﾞｰﾙ出現率を入力して下さい。";
			} else {
				double dBingoBallRate;
				bool bParse = double.TryParse(txtBingoBallRate.Text,out dBingoBallRate);

				if (bParse == false) {
					args.IsValid = false;
					vdcBingoBallRate.Text = "数値が正しくありません";
				} else if (dBingoBallRate > 100) {
					args.IsValid = false;
					vdcBingoBallRate.Text = "数値が正しくありません";
				} else if (dBingoBallRate < 0) {
					args.IsValid = false;
					vdcBingoBallRate.Text = "数値が正しくありません";
				}
			}
		}
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_RANK_GET");
			db.ProcedureInParm("PUSER_RANK",DbSession.DbType.VARCHAR2,txtUserRank.Text);
			db.ProcedureOutParm("PUSER_RANK_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PINTRO_POINT_RATE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PPOINT_PRICE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PBINGO_BALL_RATE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtUserRankNm.Text = db.GetStringValue("PUSER_RANK_NM");
				txtIntroPointRate.Text = db.GetStringValue("PINTRO_POINT_RATE");
				txtPointPrice.Text = db.GetStringValue("PPOINT_PRICE");
				txtBingoBallRate.Text = db.GetDecimalValue("PBINGO_BALL_RATE").ToString();
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;
	}

	private void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_RANK_MAINTE");
			db.ProcedureInParm("PUSER_RANK",DbSession.DbType.VARCHAR2,txtUserRank.Text);
			db.ProcedureInParm("PUSER_RANK_NM",DbSession.DbType.VARCHAR2,txtUserRankNm.Text);
			db.ProcedureInParm("PINTRO_POINT_RATE",DbSession.DbType.NUMBER,txtIntroPointRate.Text);
			db.ProcedureInParm("PPOINT_PRICE", DbSession.DbType.NUMBER, txtPointPrice.Text);
			db.ProcedureInParm("PBINGO_BALL_RATE",DbSession.DbType.NUMBER,txtBingoBallRate.Text);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		InitPage();
		GetList();

	}

	protected void dsCastRank_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
	}

}
