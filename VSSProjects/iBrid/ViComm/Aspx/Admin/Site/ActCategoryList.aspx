<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ActCategoryList.aspx.cs" Inherits="Site_ActCategoryList" Title="出演者カテゴリー設定"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="出演者カテゴリー設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									サイトコード
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
									</asp:DropDownList>
									<asp:Label ID="lblActCategorySeq" runat="server" Text="Label" Visible="false"></asp:Label>
								</td>
							</tr>
						</table>
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[カテゴリ内容]</legend>
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									カテゴリ名
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtActCategoryNm" runat="server" MaxLength="30" Width="200px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrActCategoryNm" runat="server" ErrorMessage="カテゴリ名を入力して下さい。" ControlToValidate="txtActCategoryNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									表示順位
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtPriority" runat="server" MaxLength="3" Width="60px"></asp:TextBox>&nbsp;
									<asp:RequiredFieldValidator ID="vdrPriority" runat="server" ErrorMessage="優先順位を入力して下さい。" ControlToValidate="txtPriority" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									サブタイトル
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtHtmlDoc" runat="server" MaxLength="200" Width="200px"></asp:TextBox>&nbsp;
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									サブタイトル表示
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkDisplayFlag" runat="server" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									カラーパターン
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstWebFaceSeq" runat="server" Width="206px" DataSourceID="dsWebFace" DataTextField="HTML_FACE_NAME" DataValueField="WEB_FACE_SEQ">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									利用不可
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkNaFlag" runat="server" />
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[カテゴリ一覧]</legend>
			<table border="0" style="width: 640px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle2">
						サイトコード
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
						</asp:DropDownList>
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnRegist" Text="カテゴリー追加" CssClass="seekbutton" CausesValidation="False" OnClick="btnRegist_Click" />
			<br />
			<br />
			<asp:GridView ID="grdActCategory" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsActCategory" AllowSorting="True" SkinID="GridViewColor">
				<Columns>
					<asp:BoundField DataField="PRIORITY" HeaderText="表示順位">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkActCategorySeq" runat="server" Text='<%# Eval("ACT_CATEGORY_NM") %>' CommandArgument='<%# Eval("ACT_CATEGORY_SEQ") %>' OnCommand="lnkActCategorySeq_Command"
								CausesValidation="False">
							</asp:LinkButton>
						</ItemTemplate>
						<HeaderTemplate>
							カテゴリー名
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:BoundField DataField="ACT_COUNT" HeaderText="出演者数">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:BoundField DataField="HTML_DOC" HeaderText="サブタイトル" />
					<asp:BoundField DataField="DISPLAY_MARK" HeaderText="サブ表示" />
					<asp:BoundField DataField="NA_MARK" HeaderText="利用">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:HyperLink ID="lnkUserCharge" runat="server" NavigateUrl='<%# string.Format("~/Site/CastChargeList.aspx?sitecd={0}&sitenm={1}&actcategoryseq={2}&actcategorynm={3}",Eval("SITE_CD"),Eval("SITE_NM"),Eval("ACT_CATEGORY_SEQ"),Eval("ACT_CATEGORY_NM"))%>'>課金設定</asp:HyperLink>
							<asp:HyperLink ID="lnkUserChargeTemp" runat="server" NavigateUrl='<%# string.Format("~/Site/CastChargeTempConditionList.aspx?sitecd={0}&actcategoryseq={1}&actcategorynm={2}",Eval("SITE_CD"),Eval("ACT_CATEGORY_SEQ"),Eval("ACT_CATEGORY_NM"))%>' Visible='<%# IsEnabledExpandedCharge() %>'>追加課金設定</asp:HyperLink>
						</ItemTemplate>
						<HeaderTemplate>
							課金設定
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Center" />
					</asp:TemplateField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br />
				<a class="reccount">Current viewing page
					<%=grdActCategory.PageIndex + 1%>
					of
					<%=grdActCategory.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsActCategory" runat="server" SelectMethod="GetPageCollection" TypeName="ActCategory" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsActCategory_Selected" OnSelecting="dsActCategory_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsWebFace" runat="server" SelectMethod="GetList" TypeName="WebFace"></asp:ObjectDataSource>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPriority" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrActCategoryNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdrPriority" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
