﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 割引キャンペーン開始設定
--	Progaram ID		: CampainList
--
--  Creation Date	: 2011.01.19
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using System.Text;
using System.Drawing;

public partial class Site_CampainList : System.Web.UI.Page {

	private string recCount = string.Empty;

	protected string SiteCd {
		get { return iBridUtil.GetStringValue(this.ViewState["SiteCd"]); }
		private set { this.ViewState["SiteCd"] = value; }
	}

	protected string CampainCd {
		get { return iBridUtil.GetStringValue(this.ViewState["CampainCd"]); }
		private set { this.ViewState["CampainCd"] = value; }
	}

	protected string Rowid {
		get { return iBridUtil.GetStringValue(this.ViewState["Rowid"]); }
		private set { this.ViewState["Rowid"] = value; }
	}

	protected string RevisionNo {
		get { return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]); }
		private set { this.ViewState["RevisionNo"] = value; }
	}

	protected string CampainType {
		get { return iBridUtil.GetStringValue(this.ViewState["CampainType"]); }
		private set { this.ViewState["CampainType"] = value; }
	}

	protected void Page_Load(object sender, EventArgs e) {
		if (!this.IsPostBack) {
			this.SiteCd = iBridUtil.GetStringValue(this.Request.QueryString["sitecd"]);
			this.CampainCd = iBridUtil.GetStringValue(this.Request.QueryString["campaincd"]);

			this.InitPage();
		}
	}

	protected void btnUpdate_Click(object sender, EventArgs e) {
		if (this.IsValid) {
			this.UpdateData(0);
			this.pnlMainte.Visible = false;
		}
	}

	protected void btnDelete_Click(object sender, EventArgs e) {
		if (this.IsValid) {
			this.UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender, EventArgs e) {
		this.pnlMainte.Visible = false;
		this.GetList();
	}

	protected void btnSeek_Click(object sender, EventArgs e) {
		this.pnlDtl.Visible = true;
		this.GetList();
		this.GetData();
	}

	protected void btnListSeek_Click(object sender, EventArgs e) {
		this.GetList();
	}

	protected void grdCampain_RowDataBound(object sender, GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			DateTime dtFrom, dtTo;
			if (!DateTime.TryParse(DataBinder.Eval(e.Row.DataItem, "APPLICATION_START_DATE").ToString(), out dtFrom)) {
				dtFrom = DateTime.MinValue;
			}
			if (!DateTime.TryParse(DataBinder.Eval(e.Row.DataItem, "APPLICATION_END_DATE").ToString(), out dtTo)) {
				dtTo = DateTime.MinValue;
			}

			if (dtFrom <= DateTime.Now && dtTo >= DateTime.Now) {
				foreach (TableCell oCell in e.Row.Cells) {
					oCell.BackColor = Color.LavenderBlush;
				}
			}
		}
	}

	protected void dsCampain_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void vdcAll_ServerValidate(object source, ServerValidateEventArgs args) {
		if (this.IsValid) {
			if (args.IsValid) {
				if (Encoding.GetEncoding(932).GetByteCount(this.txtCampainDesc.Text) > 160) {
					this.vdcAll.Text = "ｷｬﾝﾍﾟｰﾝ内容を入力して下さい。";
					args.IsValid = false;
				}
			}
			if (args.IsValid) {
				DateTime dtFrom, dtTo;
				if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:00", this.lstFromYYYY.SelectedValue, this.lstFromMM.SelectedValue, this.lstFromDD.SelectedValue, this.lstFromHH.SelectedValue, this.lstFromMI.SelectedValue), out dtFrom)) {
					dtFrom = DateTime.MinValue;
				}
				if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:00", this.lstToYYYY.SelectedValue, this.lstToMM.SelectedValue, this.lstToDD.SelectedValue, this.lstToHH.SelectedValue, this.lstToMI.SelectedValue), out dtTo)) {
					dtFrom = DateTime.MinValue;
				}
				if (dtFrom >= dtTo) {
					this.vdcAll.Text = "ｷｬﾝﾍﾟｰﾝ適用日の大小関係に誤りがあります。";
					args.IsValid = false;
				}

				using (Campain oCampain = new Campain()) {
					using (DataSet oDataSet = oCampain.GetCheckList(this.lstSeekSiteCd.SelectedValue)) {
						if (oDataSet.Tables[0].Rows.Count == 0) {
							return;
						}
						DataTable oDataTable = oDataSet.Tables[0];

						foreach (DataRow oDataRow in oDataTable.Select(string.Format("SITE_CD<>'{0}' OR CAMPAIN_CD<>'{1}'", this.SiteCd, this.CampainCd))) {
							DateTime dtExistingFrom;
							DateTime dtExistingTo;

							if (!DateTime.TryParse(iBridUtil.GetStringValue(oDataRow["APPLICATION_START_DATE"]), out dtExistingFrom)) {
								dtExistingFrom = DateTime.MinValue;
							}
							if (!DateTime.TryParse(iBridUtil.GetStringValue( oDataRow["APPLICATION_END_DATE"]), out dtExistingTo)) {
								dtExistingTo = DateTime.MinValue;
							}

							if (!(dtTo <= dtExistingFrom || dtExistingTo <= dtFrom)) {
								this.vdcAll.Text = "複数のｷｬﾝﾍﾟｰﾝ期間が重複しています。";
								args.IsValid = false;
								break;
							}
						}
					}
				}
			}
		}
	}

	private void InitPage() {
		this.pnlMainte.Visible = false;

		this.lstSeekSiteCd.DataBind();

		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals(string.Empty)) {
			this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		this.txtCampainDesc.Text = string.Empty;
		this.txtCampainCd.Text = string.Empty;

		if (!this.IsPostBack) {
			SysPrograms.SetupFromToDayTime(this.lstFromYYYY, this.lstFromMM, this.lstFromDD, this.lstFromHH, this.lstToYYYY, this.lstToMM, this.lstToDD, this.lstToHH, false);
			this.lstFromYYYY.SelectedIndex = 0;
			this.lstToYYYY.SelectedIndex = 0;
			this.lstFromMM.SelectedValue = DateTime.Now.ToString("MM");
			this.lstToMM.SelectedValue = DateTime.Now.ToString("MM");
			this.lstFromDD.SelectedValue = DateTime.Now.ToString("dd");
			this.lstToDD.SelectedValue = DateTime.Now.ToString("dd");
			this.lstFromHH.SelectedIndex = 0;
			this.lstToHH.SelectedIndex = 0;

			for (int i = 0; i < 60; i += 15) {
				this.lstFromMI.Items.Add(new ListItem(string.Format("{0:D2}", i)));
				this.lstToMI.Items.Add(new ListItem(string.Format("{0:D2}", i)));
			}
		}

		if (!string.IsNullOrEmpty(this.SiteCd) && !string.IsNullOrEmpty(this.CampainCd)) {
			this.lstSeekSiteCd.SelectedValue = this.SiteCd;
			this.lblSiteCd.Text = this.lstSeekSiteCd.SelectedValue;
			this.lblSiteNm.Text = this.lstSeekSiteCd.SelectedItem.Text;
			this.txtCampainCd.Text = this.CampainCd;
			this.GetList();
			this.GetData();
		} else {
			this.GetList();
		}
	}

	protected string GetRecCount() {
		return recCount;
	}

	private void GetList() {
		this.grdCampain.PageIndex = 0;
		this.grdCampain.DataBind();
		this.pnlCount.DataBind();
	}

	private void GetData() {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("CAMPAIN_GET");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.lstSeekSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pCAMPAIN_CD", DbSession.DbType.VARCHAR2, this.txtCampainCd.Text.TrimEnd());
			oDbSession.ProcedureOutParm("pCAMPAIN_TYPE", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pCAMPAIN_DESC", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pAPPLICATION_START_DATE", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pAPPLICATION_END_DATE", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pROWID", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREVISION_NO", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pRECORD_COUNT", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
			this.Rowid = oDbSession.GetStringValue("pROWID");
			this.CampainType = oDbSession.GetStringValue("pCAMPAIN_TYPE");

			if (int.Parse(oDbSession.GetStringValue("pRECORD_COUNT")) > 0) {
				this.txtCampainDesc.Text = oDbSession.GetStringValue("pCAMPAIN_DESC");
				string sFromDay = oDbSession.GetStringValue("pAPPLICATION_START_DATE");
				string sToDay = oDbSession.GetStringValue("pAPPLICATION_END_DATE");

				if (!sFromDay.Equals(string.Empty)) {
					this.lstFromYYYY.SelectedValue = sFromDay.Substring(0, 4);
					this.lstFromMM.SelectedValue = sFromDay.Substring(4, 2);
					this.lstFromDD.SelectedValue = sFromDay.Substring(6, 2);
					this.lstFromHH.SelectedValue = sFromDay.Substring(8, 2);
					this.lstFromMI.SelectedValue = sFromDay.Substring(10, 2);
				}

				if (!sToDay.Equals(string.Empty)) {
					this.lstToYYYY.SelectedValue = sToDay.Substring(0, 4);
					this.lstToMM.SelectedValue = sToDay.Substring(4, 2);
					this.lstToDD.SelectedValue = sToDay.Substring(6, 2);
					this.lstToHH.SelectedValue = sToDay.Substring(8, 2);
					this.lstToMI.SelectedValue = sToDay.Substring(10, 2);
				}
			}
		}
		this.pnlMainte.Visible = true;
		this.pnlDtl.Visible = true;
		this.pnlKey.Enabled = false;
	}

	private void UpdateData(int pDelFlag) {
		DateTime dtFrom, dtTo;

		if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:00", this.lstFromYYYY.SelectedValue, this.lstFromMM.SelectedValue, this.lstFromDD.SelectedValue, this.lstFromHH.SelectedValue, this.lstFromMI.SelectedValue), out dtFrom)) {
			dtFrom = DateTime.MinValue;
		}
		if (!DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:00", this.lstToYYYY.SelectedValue, this.lstToMM.SelectedValue, this.lstToDD.SelectedValue, this.lstToHH.SelectedValue, this.lstToMI.SelectedValue), out dtTo)) {
			dtTo = DateTime.MinValue;
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("CAMPAIN_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.SiteCd);
			oDbSession.ProcedureInParm("pCAMPAIN_CD", DbSession.DbType.VARCHAR2, this.txtCampainCd.Text.TrimEnd());
			oDbSession.ProcedureInParm("pCAMPAIN_TYPE", DbSession.DbType.VARCHAR2, this.CampainType);
			oDbSession.ProcedureInParm("pCAMPAIN_DESC", DbSession.DbType.VARCHAR2, this.txtCampainDesc.Text.TrimEnd());
			oDbSession.ProcedureInParm("pAPPLICATION_START_DATE", DbSession.DbType.DATE, dtFrom);
			oDbSession.ProcedureInParm("pAPPLICATION_END_DATE", DbSession.DbType.DATE, dtTo);
			oDbSession.ProcedureInParm("pROWID", DbSession.DbType.VARCHAR2, this.Rowid);
			oDbSession.ProcedureInParm("pREVISION_NO", DbSession.DbType.NUMBER, this.RevisionNo);
			oDbSession.ProcedureInParm("pDEL_FLAG", DbSession.DbType.NUMBER, pDelFlag);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.InitPage();
		this.GetList();
	}

}
