<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="WebFaceList.aspx.cs" Inherits="Site_WebFaceList" Title="Ｗｅｂデザイン設定" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="ＷＥＢデザイン設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[ＷＥＢデザイン内容]</legend>
						<asp:Label ID="lblWebFaceSeq" runat="server" Text="" Visible="false"></asp:Label>
						<table border="0" style="width: 700px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									デザイン名称
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtHtmlFaceNm" runat="server" MaxLength="30" Width="200px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrHtmlFaceNm" runat="server" ErrorMessage="デザイン名称を入力して下さい。" ControlToValidate="txtHtmlFaceNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
						</table>

						<table style="width: 500px;border-style:none;" class="tableStyle">
							<tr>
								<td></td>
								<td align="center">[男性会員向け]</td>
								<td align="center">[女性会員向け]</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									線色
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtColorLine" runat="server" MaxLength="7" Width="70px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrColorLine" runat="server" ControlToValidate="txtColorLine" Display="Dynamic" ErrorMessage="線色を入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeColorLine" runat="server" ErrorMessage="#で始まる6桁のカラーコードを入力して下さい。" ValidationExpression="#[a-fA-F0-9]{6}$" ControlToValidate="txtColorLine"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									<asp:LinkButton ID="lnkColorSample" runat="server" OnClick="lnkColorSample_Click">色見本</asp:LinkButton>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtColorLineWoman" runat="server" MaxLength="7" Width="70px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrColorLineWoman" runat="server" ControlToValidate="txtColorLineWoman" Display="Dynamic" ErrorMessage="線色を入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeColorLineWoman" runat="server" ErrorMessage="#で始まる6桁のカラーコードを入力して下さい。" ValidationExpression="#[a-fA-F0-9]{6}$" ControlToValidate="txtColorLineWoman"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr id="Tr1" runat="server" Visible="false">
								<td class="tdHeaderStyle">
									INDEX色
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtColorIndex" runat="server" MaxLength="7" Width="70px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrColorIndex" runat="server" ControlToValidate="txtColorIndex" Display="Dynamic" ErrorMessage="INDEX文字色を入力して下さい。"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeColorIndex" runat="server" ErrorMessage="#で始まる6桁のカラーコードを入力して下さい。" ValidationExpression="#[a-fA-F0-9]{6}$" ControlToValidate="txtColorIndex"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtColorIndexWoman" runat="server" MaxLength="7" Width="70px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrColorIndexWoman" runat="server" ControlToValidate="txtColorIndexWoman" Display="Dynamic" ErrorMessage="INDEX文字色を入力して下さい。"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeColorIndexWoman" runat="server" ErrorMessage="#で始まる6桁のカラーコードを入力して下さい。" ValidationExpression="#[a-fA-F0-9]{6}$" ControlToValidate="txtColorIndexWoman"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									テキスト色
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtColorChar" runat="server" MaxLength="7" Width="70px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrColorChar" runat="server" ControlToValidate="txtColorChar" Display="Dynamic" ErrorMessage="テキスト色を入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeColorChar" runat="server" ErrorMessage="#で始まる6桁のカラーコードを入力して下さい。" ValidationExpression="#[a-fA-F0-9]{6}$" ControlToValidate="txtColorChar"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtColorCharWoman" runat="server" MaxLength="7" Width="70px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrColorCharWoman" runat="server" ControlToValidate="txtColorCharWoman" Display="Dynamic" ErrorMessage="テキスト色を入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeColorCharWoman" runat="server" ErrorMessage="#で始まる6桁のカラーコードを入力して下さい。" ValidationExpression="#[a-fA-F0-9]{6}$" ControlToValidate="txtColorCharWoman"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									リンク文字色
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtColorLink" runat="server" MaxLength="7" Width="70px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrColorLink" runat="server" ControlToValidate="txtColorLink" Display="Dynamic" ErrorMessage="リンク文字色を入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeColorLink" runat="server" ErrorMessage="#で始まる6桁のカラーコードを入力して下さい。" ValidationExpression="#[a-fA-F0-9]{6}$" ControlToValidate="txtColorLink"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtColorLinkWoman" runat="server" MaxLength="7" Width="70px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrColorLinkWoman" runat="server" ControlToValidate="txtColorLinkWoman" Display="Dynamic" ErrorMessage="リンク文字色を入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeColorLinkWoman" runat="server" ErrorMessage="#で始まる6桁のカラーコードを入力して下さい。" ValidationExpression="#[a-fA-F0-9]{6}$" ControlToValidate="txtColorLinkWoman"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									リンク選択時文字色
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtColorALink" runat="server" MaxLength="7" Width="70px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrColorALink" runat="server" ControlToValidate="txtColorALink" Display="Dynamic" ErrorMessage="リンク選択時文字色を入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeColorALink" runat="server" ErrorMessage="#で始まる6桁のカラーコードを入力して下さい。" ValidationExpression="#[a-fA-F0-9]{6}$" ControlToValidate="txtColorALink"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtColorALinkWoman" runat="server" MaxLength="7" Width="70px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrColorALinkWoman" runat="server" ControlToValidate="txtColorALinkWoman" Display="Dynamic" ErrorMessage="リンク選択時文字色を入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeColorALinkWoman" runat="server" ErrorMessage="#で始まる6桁のカラーコードを入力して下さい。" ValidationExpression="#[a-fA-F0-9]{6}$" ControlToValidate="txtColorALinkWoman"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									リンク選択済み文字色
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtColorVLink" runat="server" MaxLength="7" Width="70px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrColorVLink" runat="server" ControlToValidate="txtColorVLink" Display="Dynamic" ErrorMessage="リンク選択済み文字色を入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeColorVLink" runat="server" ErrorMessage="#で始まる6桁のカラーコードを入力して下さい。" ValidationExpression="#[a-fA-F0-9]{6}$" ControlToValidate="txtColorVLink"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtColorVLinkWoman" runat="server" MaxLength="7" Width="70px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrColorVLinkWoman" runat="server" ControlToValidate="txtColorVLinkWoman" Display="Dynamic" ErrorMessage="リンク選択済み文字色を入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeColorVLinkWoman" runat="server" ErrorMessage="#で始まる6桁のカラーコードを入力して下さい。" ValidationExpression="#[a-fA-F0-9]{6}$" ControlToValidate="txtColorVLinkWoman"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									背景色
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtColorBack" runat="server" MaxLength="7" Width="70px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrColorBack" runat="server" ControlToValidate="txtColorBack" Display="Dynamic" ErrorMessage="背景色を入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeColorBack" runat="server" ErrorMessage="#で始まる6桁のカラーコードを入力して下さい。" ValidationExpression="#[a-fA-F0-9]{6}$" ControlToValidate="txtColorBack"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtColorBackWoman" runat="server" MaxLength="7" Width="70px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrColorBackWoman" runat="server" ControlToValidate="txtColorBackWoman" Display="Dynamic" ErrorMessage="背景色を入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeColorBackWoman" runat="server" ErrorMessage="#で始まる6桁のカラーコードを入力して下さい。" ValidationExpression="#[a-fA-F0-9]{6}$" ControlToValidate="txtColorBackWoman"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									文字サイズ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtSizeChar" runat="server" MaxLength="2" Width="20px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrSizeChar" runat="server" ControlToValidate="txtSizeChar" Display="Dynamic" ErrorMessage="文字サイズを入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeSizeChar" runat="server" ErrorMessage="文字サイズは1〜2桁の数字で入力して下さい。" ValidationExpression="\d{1,2}" ControlToValidate="txtSizeChar"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtSizeCharWoman" runat="server" MaxLength="2" Width="20px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrSizeCharWoman" runat="server" ControlToValidate="txtSizeCharWoman" Display="Dynamic" ErrorMessage="文字サイズを入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeSizeCharWoman" runat="server" ErrorMessage="文字サイズは1〜2桁の数字で入力して下さい。" ValidationExpression="\d{1,2}" ControlToValidate="txtSizeCharWoman"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									線サイズ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtSizeLine" runat="server" MaxLength="2" Width="20px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrSizeLine" runat="server" ControlToValidate="txtSizeLine" Display="Dynamic" ErrorMessage="線サイズを入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeSizeLine" runat="server" ErrorMessage="線サイズは1〜2桁の数字で入力して下さい。" ValidationExpression="\d{1,2}" ControlToValidate="txtSizeLine"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtSizeLineWoman" runat="server" MaxLength="2" Width="20px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrSizeLineWoman" runat="server" ControlToValidate="txtSizeLineWoman" Display="Dynamic" ErrorMessage="線サイズを入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeSizeLineWoman" runat="server" ErrorMessage="線サイズは1〜2桁の数字で入力して下さい。" ValidationExpression="\d{1,2}" ControlToValidate="txtSizeLineWoman"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
						</table>
							
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[ＷＥＢデザイン一覧]</legend>
			<asp:GridView ID="grdWebFace" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsWebFace" SkinID="GridViewColor">
				<Columns>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkWebFaceSeq" runat="server" Text='<%# Eval("HTML_FACE_NAME") %>' CommandArgument='<%# Eval("WEB_FACE_SEQ") %>' OnCommand="lnkWebFaceSeq_Command"
								CausesValidation="False"></asp:LinkButton>
						</ItemTemplate>
						<HeaderTemplate>
							デザイン名称
						</HeaderTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="線色(男/女)">
						<ItemTemplate>
							<%# Eval("COLOR_LINE") %>
							<br />
							<%# Eval("COLOR_LINE_WOMAN") %>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="INDEX文字色(男/女)" Visible="false">
						<ItemTemplate>
							<%# Eval("COLOR_INDEX")%>
							<br />
							<%# Eval("COLOR_INDEX_WOMAN")%>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="文字色(男/女)">
						<ItemTemplate>
							<%# Eval("COLOR_CHAR")%>
							<br />
							<%# Eval("COLOR_CHAR_WOMAN")%>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="リンク文字色(男/女)">
						<ItemTemplate>
							<%# Eval("COLOR_LINK")%>
							<br />
							<%# Eval("COLOR_LINK_WOMAN")%>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="背景色(男/女)">
						<ItemTemplate>
							<%# Eval("COLOR_BACK")%>
							<br />
							<%# Eval("COLOR_BACK_WOMAN")%>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="文字サイズ(男/女)">
						<ItemTemplate>
							<%# Eval("SIZE_CHAR")%>
							<br />
							<%# Eval("SIZE_CHAR_WOMAN")%>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="線サイズ(男/女)">
						<ItemTemplate>
							<%# Eval("SIZE_LINE")%>
							<br />
							<%# Eval("SIZE_LINE_WOMAN")%>
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<a class="reccount">Record Count
				<%#GetRecCount() %>
			</a>
			<br>
			<a class="reccount">Current viewing page
				<%=grdWebFace.PageIndex + 1%>
				of
				<%=grdWebFace.PageCount%>
			</a>
			<div class="button">
				<asp:Button ID="btnRegist" runat="server" Text="ＷＥＢデザイン追加" OnClick="btnRegist_Click" />
			</div>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsWebFace" runat="server" SelectMethod="GetPageCollection" TypeName="WebFace" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsWebFace_Selected"></asp:ObjectDataSource>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrColorLine" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeColorLine" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrColorIndex" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdeColorIndex" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdrColorChar" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdeColorChar" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vdrColorLink" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8" TargetControlID="vdeColorLink" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender15" TargetControlID="vdrColorALink" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender16" TargetControlID="vdeColorALink" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender17" TargetControlID="vdrColorVLink" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender18" TargetControlID="vdeColorVLink" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9" TargetControlID="vdrColorBack" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10" TargetControlID="vdeColorBack" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11" TargetControlID="vdrSizeLine" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender12" TargetControlID="vdeSizeLine" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender13" TargetControlID="vdrSizeChar" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender14" TargetControlID="vdeSizeChar" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender19" TargetControlID="vdrColorLineWoman" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender20" TargetControlID="vdeColorLineWoman" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender21" TargetControlID="vdrColorIndexWoman" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender22" TargetControlID="vdeColorIndexWoman" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender23" TargetControlID="vdrColorCharWoman" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender24" TargetControlID="vdeColorCharWoman" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender25" TargetControlID="vdrColorLinkWoman" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender26" TargetControlID="vdeColorLinkWoman" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender27" TargetControlID="vdrColorALinkWoman" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender28" TargetControlID="vdeColorALinkWoman" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender29" TargetControlID="vdrColorVLinkWoman" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender30" TargetControlID="vdeColorVLinkWoman" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender31" TargetControlID="vdrColorBackWoman" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender32" TargetControlID="vdeColorBackWoman" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender33" TargetControlID="vdrSizeLineWoman" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender34" TargetControlID="vdeSizeLineWoman" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender35" TargetControlID="vdrSizeCharWoman" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender36" TargetControlID="vdeSizeCharWoman" HighlightCssClass="validatorCallout" />	
</asp:Content>
