﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ReceiptCampainMainte.aspx.cs" Inherits="Site_ReceiptCampainMainte"
    Title="入金キャンペーン設定" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="入金キャンペーン設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel ID="pnlKey" runat="server">
            <fieldset>
                <legend>[入金一覧]</legend>
                <table border="0" style="width: 640px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle2">
                            サイトコード
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="180px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    CausesValidation="False" />
            </fieldset>
        </asp:Panel>
        <br />
        <asp:Panel runat="server" ID="pnlMainte">
            <fieldset>
                <legend>[キャンペーン設定]</legend>
                <table>
                    <tr>
                        <td>
                            <asp:GridView ID="grdReceiptCampain" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                DataSourceID="dsReceiptCampain" OnRowDataBound="grdReceiptCampain_RowDataBound"
                                AllowSorting="True" SkinID="GridView">
                                <Columns>
                                    <asp:TemplateField HeaderText="ｷｬﾝﾍﾟｰﾝ適用">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkCampainApplicationFlag" runat="server" Checked="true" />
                                            <asp:Label ID="lblCampainApplicationFlag" runat="server" Text='<%# Eval("CAMPAIN_APPLICATION_FLAG") %>'
                                                Visible="false" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="入金種別">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSettleTypeNm" runat="server" Text='<%# Eval("SETTLE_TYPE_NM") %>' />
                                            <asp:Label ID="lblSettleType" runat="server" Text='<%# Eval("SETTLE_TYPE") %>' Visible="false" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="適用開始日時">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStartDate" runat="server" Text='<%# Eval("APPLICATION_START_DATE", "{0:yyyy/MM/dd HH:mm}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="適用終了日時">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEndDate" runat="server" Text='<%# Eval("APPLICATION_END_DATE", "{0:yyyy/MM/dd HH:mm}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                                <PagerSettings Mode="NumericFirstLast" />
                            </asp:GridView>
                            <asp:Panel runat="server" ID="pnlCount">
                                <a class="reccount">Record Count
                                    <%#GetRecCount() %>
                                </a>
                                <br />
                                <a class="reccount">Current viewing page
                                    <%=grdReceiptCampain.PageIndex + 1%>
                                    of
                                    <%=grdReceiptCampain.PageCount%>
                                </a>
                            </asp:Panel>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td valign="top">
                            <asp:Panel ID="pnlDtl" runat="server">
                                <table border="0" style="width: 640px" class="tableStyle">
                                    <tr>
                                        <td class="tdHeaderStyle">
                                            ｷｬﾝﾍﾟｰﾝ適用開始日時
                                        </td>
                                        <td class="tdDataStyle">
                                            <asp:DropDownList ID="lstFromYYYY" runat="server" Width="60px">
                                            </asp:DropDownList>年
                                            <asp:DropDownList ID="lstFromMM" runat="server" Width="40px">
                                            </asp:DropDownList>月
                                            <asp:DropDownList ID="lstFromDD" runat="server" Width="40px">
                                            </asp:DropDownList>日
                                            <asp:DropDownList ID="lstFromHH" runat="server" Width="40px">
                                            </asp:DropDownList>時00分から</td>
                                    </tr>
                                    <tr>
                                        <td class="tdHeaderStyle2">
                                            ｷｬﾝﾍﾟｰﾝ適用終了日時
                                        </td>
                                        <td class="tdDataStyle">
                                            <asp:DropDownList ID="lstToYYYY" runat="server" Width="60px">
                                            </asp:DropDownList>年
                                            <asp:DropDownList ID="lstToMM" runat="server" Width="40px">
                                            </asp:DropDownList>月
                                            <asp:DropDownList ID="lstToDD" runat="server" Width="40px">
                                            </asp:DropDownList>日
                                            <asp:DropDownList ID="lstToHH" runat="server" Width="40px">
                                            </asp:DropDownList>時00分まで
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:CustomValidator ID="vdcAll" runat="server" ErrorMessage="" OnServerValidate="vdcAll_ServerValidate"
                                ValidationGroup="Detail"></asp:CustomValidator>
                            <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click"
                                ValidationGroup="Detail" />
                            <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click"
                                CausesValidation="False" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </asp:Panel>
        <br />
    </div>
    <asp:ObjectDataSource ID="dsReceiptCampain" runat="server" SelectMethod="GetPageCollection"
        TypeName="ReceiptCampain" SelectCountMethod="GetPageCount" EnablePaging="True"
        OnSelected="dsReceiptCampain_Selected">
        <SelectParameters>
            <asp:ControlParameter Name="pSiteCd" ControlID="lstSeekSiteCd" PropertyName="SelectedValue"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
