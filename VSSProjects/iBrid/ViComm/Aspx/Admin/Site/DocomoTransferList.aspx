﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="DocomoTransferList.aspx.cs" Inherits="Site_DocomoTransferList" Title="ドコモケータイ送金設定"
    ValidateRequest="false" %>

<%@ Import Namespace="System.Data" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="ドコモケータイ送金設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlMainte">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel runat="server" ID="pnlDtl">
                    <table border="0" style="width: 800px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                サイト名
                            </td>
                            <td class="tdDataStyle">
                                <asp:Label ID="lblSiteNm" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                送金先番号
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtDocomoMoneyTransferTel" runat="Server" MaxLength="13"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="xvdnDocomoMoneyTransferTel" runat="server"
                                    Enabled="true" FilterType="Numbers" TargetControlID="txtDocomoMoneyTransferTel" />
                                <asp:RegularExpressionValidator ID="vdeDocomoMoneyTransferTel" runat="server" ErrorMessage="送金先番号は携帯電話番号またはドコモ口座番号を入力してください"
                                    ValidationExpression="[0-9]{13}|[0-9]{11}" ControlToValidate="txtDocomoMoneyTransferTel"
                                    ValidationGroup="UpdateTransfer">*</asp:RegularExpressionValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="xvdc_vdeDocomoMoneyTransferTel" runat="Server"
                                    TargetControlID="vdeDocomoMoneyTransferTel" HighlightCssClass="validatorCallout" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                送金先名義
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtDocomoMoneyTransferNm" runat="Server" MaxLength="2"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="vdeDocomoMoneyTransferNm" runat="server" ControlToValidate="txtDocomoMoneyTransferNm"
                                    ErrorMessage="送金先名義はカタカナ２文字で登録してください" ValidationExpression="([\u30A0-\u30FF]|[｡-ﾟ]){2}"
                                    ValidationGroup="Detail">*</asp:RegularExpressionValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="xvdc_vdeDocomoMoneyTransferNm" runat="Server"
                                    TargetControlID="vdeDocomoMoneyTransferNm" HighlightCssClass="validatorCallout" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle2">
                                備考
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox runat="server" ID="txtRemarks" Width="500px" Rows="4" MaxLength="1024"
                                    TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click"
                        ValidationGroup="Detail" />
                    <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click"
                        CausesValidation="False" />
                    <asp:CustomValidator ID="vdcRegist" runat="server" ErrorMessage="" OnServerValidate="vdcRegist_ServerValidate"
                        ValidationGroup="Detail"></asp:CustomValidator>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset>
                <legend>[検索]</legend>
                <table border="0" style="width: 640px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle2">
                            サイトコード
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="180px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <asp:Button ID="btnListSeek" runat="server" CausesValidation="False" CssClass="seekbutton"
                    OnClick="btnListSeek_Click" Text="一覧検索" />
                <asp:Button ID="btnRegist" runat="server" CausesValidation="False" CssClass="seekbutton"
                    OnClick="btnRegist_Click" Text="追加" />
            </fieldset>
        </asp:Panel>
        <asp:Panel ID="pnlList" runat="server" ScrollBars="auto">
            <fieldset>
                <legend>[送金一覧]</legend>
                <asp:Literal ID="litNotice" runat="server" Text="※背景色が<font color='lightskyblue'>青色</font>のデータが現在表示中のデータ"></asp:Literal>
                <asp:GridView ID="grdDocomoTransfer" runat="server" AllowSorting="true" AutoGenerateColumns="False" DataSourceID="dsDocomoTransfer"
                    SkinID="GridViewColor" OnRowDataBound="grdDocomoTransfer_RowDataBound" DataKeyNames="SITE_CD,DOCOMO_MONEY_TRANSFER_TEL">
                    <Columns>
                        <asp:TemplateField HeaderText="送金先番号">
                            <ItemTemplate>
                                <asp:Label ID="lblDocomoMoneyTransferTel" runat="server" Text='<%# Eval("DOCOMO_MONEY_TRANSFER_TEL") %>' />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="送金先名義">
                            <ItemTemplate>
                                <asp:Label ID="lblDocomoMoneyTransferNm" runat="server" Text='<%# Eval("DOCOMO_MONEY_TRANSFER_NM") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="反映日時" SortExpression="APPLICATION_DATE">
                            <ItemTemplate>
                                <asp:Label ID="lblApplicationDate" runat="server" Text='<%# Eval("APPLICATION_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="更新日時" SortExpression="UPDATE_DATE">
                            <ItemTemplate>
                                <asp:Label ID="lblUpdateDate" runat="server" Text='<%# Eval("UPDATE_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="備考">
                            <ItemTemplate>
                                <asp:Label ID="lblRemarks" runat="server" Text='<%# GetRemarksMark(Eval("REMARKS")) %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnUpdate" runat="server" CommandArgument='<%# Eval("DOCOMO_MONEY_TRANSFER_TEL") %>'
                                    OnCommand="btnUpdate_Command" Text="内容変更" ValidationGroup="Update" />
                                <asp:Button ID="btnDelete" runat="server" CausesValidation="false" CommandArgument='<%# string.Format("{0}:{1}",Eval("DOCOMO_MONEY_TRANSFER_TEL"),Eval("REVISION_NO")) %>'
                                    OnClientClick='return confirm("削除を行ないますか？");' OnCommand="btnDelete_Command" Text="削除" />
                                <asp:Button ID="btnActivate" runat="server" CausesValidation="false" CommandArgument='<%# Eval("DOCOMO_MONEY_TRANSFER_TEL") %>'
                                    OnClientClick='return confirm("ドコモ送金ページ反映を行ないますか？");' OnCommand="btnActivate_Command"
                                    Text="ドコモ送金ページ反映" />
                                <asp:HiddenField ID="hdrRevisionNo" runat="server" Value='<%# Eval("REVISION_NO") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsDocomoTransfer" runat="server" SelectMethod="GetList"
        TypeName="DocomoTransfer">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSeekSiteCd" DbType="String" PropertyName="SelectedValue"
                Name="pSiteCd" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
