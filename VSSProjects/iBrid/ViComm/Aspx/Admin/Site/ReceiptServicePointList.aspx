<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReceiptServicePointList.aspx.cs" Inherits="Site_ReceiptServicePointList"
	Title="入金残高サービスポイント設定" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="入金残高サービスポイント設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									サイトコード
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblSiteCd" runat="server" Text=""></asp:Label>
									<asp:Label ID="lblSiteNm" runat="server" Text=""></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ユーザーランク
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblUserRank" runat="server" Text=""></asp:Label>
									<asp:Label ID="lblUserRankNm" runat="server" Text=""></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									決済種別
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstSettleType" runat="server" DataSourceID="dsCodeDtl" DataTextField="CODE_NM" DataValueField="CODE" Width="180px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									入金残高
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtReceiptAmt" runat="server" MaxLength="6" Width="50px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrReceiptAmt" runat="server" ErrorMessage="入金残高を入力して下さい。" ControlToValidate="txtReceiptAmt" ValidationGroup="Key">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeReceiptAmt" runat="server" ErrorMessage="入金残高は1〜6桁の数字で入力して下さい。" ValidationExpression="\d{1,6}" ControlToValidate="txtReceiptAmt"
										ValidationGroup="Key">*</asp:RegularExpressionValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnUserRank" Text="ユーザーランク設定へ" CssClass="seekbutton" CausesValidation="false" PostBackUrl='<%# string.Format("UserRankList.aspx?sitecd={0}",lblSiteCd.Text)%>' />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[サービスポイント設定]</legend>
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									加算率(入金１回目)
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtAddRate0" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrAddRate0" runat="server" ErrorMessage="加算率(入金１回目)を入力して下さい。" ControlToValidate="txtAddRate0" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeAddRate0" runat="server" ErrorMessage="加算率は1〜5桁の数字で入力して下さい。" ValidationExpression="\d{1,5}" ControlToValidate="txtAddRate0"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									加算率(入金２回目)
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtAddRate1" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrAddRate1" runat="server" ErrorMessage="加算率(入金２回目)を入力して下さい。" ControlToValidate="txtAddRate1" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeAddRate1" runat="server" ErrorMessage="加算率は1〜5桁の数字で入力して下さい。" ValidationExpression="\d{1,5}" ControlToValidate="txtAddRate1"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									加算率(入金３回目)
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtAddRate2" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrAddRate2" runat="server" ErrorMessage="加算率(入金３回目)を入力して下さい。" ControlToValidate="txtAddRate2" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeAddRate2" runat="server" ErrorMessage="加算率は1〜5桁の数字で入力して下さい。" ValidationExpression="\d{1,5}" ControlToValidate="txtAddRate2"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									加算率(その他)
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtAddRate" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrAddRate" runat="server" ErrorMessage="加算率(その他)を入力して下さい。" ControlToValidate="txtAddRate" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeAddRate" runat="server" ErrorMessage="加算率は1〜5桁の数字で入力して下さい。" ValidationExpression="\d{1,5}" ControlToValidate="txtAddRate"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									支払遅延
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkPaymentDelayFlag" runat="server" Text="支払遅延時にも加算する。" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									加算方法
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkAddPointFlag" runat="server" Text="加算率ではなく入力値を加算する。" />
								</td>
							</tr>
						</table>
						[ｷｬﾝﾍﾟｰﾝ設定]<br />
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									ｷｬﾝﾍﾟｰﾝ適用
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkCampainApplicatonFlag" runat="server" Text="下記の日時でｷｬﾝﾍﾟｰﾝを適用します。" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ｷｬﾝﾍﾟｰﾝ適用開始日時
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstFromYYYY" runat="server" Width="60px">
									</asp:DropDownList>年
									<asp:DropDownList ID="lstFromMM" runat="server" Width="40px">
									</asp:DropDownList>月
									<asp:DropDownList ID="lstFromDD" runat="server" Width="40px">
									</asp:DropDownList>日
									<asp:DropDownList ID="lstFromHH" runat="server" Width="40px">
									</asp:DropDownList>時00分から</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ｷｬﾝﾍﾟｰﾝ適用終了日時
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstToYYYY" runat="server" Width="60px">
									</asp:DropDownList>年
									<asp:DropDownList ID="lstToMM" runat="server" Width="40px">
									</asp:DropDownList>月
									<asp:DropDownList ID="lstToDD" runat="server" Width="40px">
									</asp:DropDownList>日
									<asp:DropDownList ID="lstToHH" runat="server" Width="40px">
									</asp:DropDownList>時00分まで
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									加算率(ｷｬﾝﾍﾟｰﾝ)
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtAddRateCampain" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
									<span style="color: #0000ff">*入金初回以外でｷｬﾝﾍﾟｰﾝ中の場合、加算率+ｷｬﾝﾍﾟｰﾝ加算率になります。</span>
									<asp:RequiredFieldValidator ID="vdrAddRateCampain" runat="server" ErrorMessage="加算率(ｷｬﾝﾍﾟｰﾝ)を入力して下さい。" ControlToValidate="txtAddRateCampain" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeAddRateCampain" runat="server" ErrorMessage="加算率は1〜5桁の数字で入力して下さい。" ValidationExpression="\d{1,5}" ControlToValidate="txtAddRateCampain"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
						</table>
						<asp:CustomValidator ID="vdcAll" runat="server" ErrorMessage="" OnServerValidate="vdcAll_ServerValidate" ValidationGroup="Detail"></asp:CustomValidator>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[ユーザーランク一覧]</legend>
			<table border="0" style="width: 640px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle2">
						サイトコード
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
						</asp:DropDownList>
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" CausesValidation="False" OnClick="btnCSV_Click" />
			<br />
			<br />
			<asp:GridView ID="grdReceiptServicePoint" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsReceiptServicePoint" AllowSorting="True"
				SkinID="GridView" OnDataBound="grdReceiptServicePoint_DataBound" OnRowDataBound="grdReceiptServicePoint_RowDataBound">
				<Columns>
					<asp:TemplateField HeaderText="ランク">
						<ItemTemplate>
							<asp:HyperLink ID="lnkUserRank" runat="server" Text='<%# Eval("USER_RANK_NM")%>' NavigateUrl='<%# string.Format("~/Site/UserRankList.aspx?sitecd={0}",Eval("SITE_CD"))%>'></asp:HyperLink>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:TemplateField HeaderText="決済種別">
						<ItemTemplate>
							<asp:Label ID="lblSettleType" runat="server" Text='<%# Eval("SETTLE_TYPE_NM") %>'></asp:Label>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:TemplateField HeaderText="決済金額">
						<ItemTemplate>
							<asp:HyperLink ID="lnkSettleAmt" runat="server" Text='<%# Eval("RECEIPT_AMT")%>' NavigateUrl='<%# string.Format("~/Site/ReceiptServicePointList.aspx?sitecd={0}&userrank={1}&sitenm={2}&userranknm={3}&settle={4}&amt={5}",Eval("SITE_CD"),Eval("USER_RANK"),Eval("SITE_NM"),Eval("USER_RANK_NM"),Eval("SETTLE_TYPE"),Eval("RECEIPT_AMT"))%>'></asp:HyperLink>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Right" />
					</asp:TemplateField>
					<asp:BoundField DataField="ADD_RATE0" HeaderText="加算率(入金1)">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:BoundField DataField="ADD_RATE1" HeaderText="加算率(入金2)">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:BoundField DataField="ADD_RATE2" HeaderText="加算率(入金3)">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:BoundField DataField="ADD_RATE" HeaderText="加算率(その他)">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:BoundField DataField="ADD_RATE_CAMPAIN" HeaderText="加算率(ｷｬﾝﾍﾟｰﾝ)">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:TemplateField HeaderText="適用開始日時">
						<ItemTemplate>
							<asp:Label ID="Label1" runat="server" Text='<%#Eval("CAMPAIN_APPLICATION_FLAG").ToString().Equals("1") ? Eval("APPLICATION_START_DATE", "{0:yyyy/MM/dd HH:mm}") :""%>'></asp:Label>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Center" />
					</asp:TemplateField>
					<asp:TemplateField HeaderText="適用開始終了">
						<ItemTemplate>
							<asp:Label ID="Label2" runat="server" Text='<%# Eval("CAMPAIN_APPLICATION_FLAG").ToString().Equals("1") ? Eval("APPLICATION_END_DATE", "{0:yyyy/MM/dd HH:mm}") :"" %>'></asp:Label>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Center" />
					</asp:TemplateField>
					<asp:TemplateField HeaderText="加算方式">
						<ItemTemplate>
							<asp:Label runat="server" Text='<%# GetAddPointRule(Eval("ADD_POINT_FLAG")) %>'></asp:Label>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Center" />
					</asp:TemplateField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br>
				<a class="reccount">Current viewing page
					<%=grdReceiptServicePoint.PageIndex + 1%>
					of
					<%=grdReceiptServicePoint.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsCodeDtl" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="74" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsReceiptServicePoint" runat="server" SelectMethod="GetPageCollection" TypeName="ReceiptServicePoint" SelectCountMethod="GetPageCount"
		EnablePaging="True" OnSelected="dsReceiptServicePoint_Selected" OnSelecting="dsReceiptServicePoint_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtReceiptAmt" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtAddRate0" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtAddRate1" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtAddRate2" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtAddRate" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9" TargetControlID="vdrReceiptAmt" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10" TargetControlID="vdeReceiptAmt" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrAddRate0" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeAddRate0" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrAddRate1" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdeAddRate1" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdrAddRate2" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdeAddRate2" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vdrAddRate" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8" TargetControlID="vdeAddRate" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11" TargetControlID="vdrAddRateCampain" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender12" TargetControlID="vdeAddRateCampain" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
