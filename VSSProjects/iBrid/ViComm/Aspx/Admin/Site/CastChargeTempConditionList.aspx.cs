﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: キャスト課金一時保存パターン
--	Progaram ID		: CastChargeTempConditionList
--
--  Creation Date	: 2012.05.30
--  Creater			: iBrid
--
**************************************************************************/using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Site_CastChargeTempConditionList : System.Web.UI.Page {

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string ActCategorySeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["ActCategorySeq"]);
		}
		set {
			this.ViewState["ActCategorySeq"] = value;
		}
	}
	private string ActCategorySeqNm {
		get {
			return iBridUtil.GetStringValue(this.ViewState["ActCategorySeqNm"]);
		}
		set {
			this.ViewState["ActCategorySeqNm"] = value;
		}
	}
	protected string NaFlag {
		get { return iBridUtil.GetStringValue(this.ViewState["NaFlag"]); }
		private set { this.ViewState["NaFlag"] = value; }
	}
	protected string ApplicationDate {
		get { return iBridUtil.GetStringValue(this.ViewState["ApplicationDate"]); }
		private set { this.ViewState["ApplicationDate"] = value; }
	}
	protected string RevisionNo {
		get { return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]); }
		private set { this.ViewState["RevisionNo"] = value; }
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		this.ActCategorySeq = iBridUtil.GetStringValue(Request.QueryString["actcategoryseq"]);
		this.ActCategorySeqNm = iBridUtil.GetStringValue(Request.QueryString["actcategorynm"]);

		if (!this.IsPostBack) {
			this.InitPage();
		}
	}

	protected void lstSeekSiteCd_SelectedIndexChanged(object sender,EventArgs e) {
	}

	protected void lstSiteCd_SelectedIndexChanged(object sender,EventArgs e) {
		this.lstActCategorySeq.DataBind();
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (sender == null) {
			return;
		}

		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	protected void btnClear_Click(object sender,EventArgs e) {
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlMainte.Visible = false;
	}

	protected void btnBack_Click(object sender,EventArgs e) {
		this.Response.Redirect("~/Site/ActCategoryList.aspx");
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		this.SiteCd = this.lstSeekSiteCd.SelectedValue;
		this.GetList();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		this.lstSiteCd.DataBind();
		this.lstSiteCd.SelectedValue = this.lstSeekSiteCd.SelectedValue;
		this.ClearField();
		this.pnlMainte.Visible = true;
		this.pnlKey.Enabled = true;
	}

	protected void btnActivate_Command(object sender,CommandEventArgs e) {
		string[] sArgs = iBridUtil.GetStringValue(e.CommandArgument).Split(',');

		if (ViCommConst.FLAG_OFF_STR.Equals(sArgs[1])) {
			int iNaCount = 0;
			using (CastChargeTempCondition oCondition = new CastChargeTempCondition()) {
				iNaCount = oCondition.GetNaCount(this.SiteCd);
			}
			if (iNaCount > 0) {
				this.lblNaError.Visible = true;
				return;
			} else {
				this.lblNaError.Visible = false;
			}
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_CAST_CHARGE_TEMP_COND");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			db.ProcedureInParm("pCAST_CHARGE_TEMP_CD",DbSession.DbType.VARCHAR2,sArgs[0]);
			db.ProcedureInParm("pNA_FLAG",DbSession.DbType.NUMBER,sArgs[1]);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		this.GetList();
	}

	protected void lnkCastChargeTempCd_Command(object sender,CommandEventArgs e) {
		string sArg = iBridUtil.GetStringValue(e.CommandArgument);

		this.ClearField();
		this.lstCastChargeTempCd.SelectedValue = sArg;
		this.GetData(this.SiteCd,sArg);
		this.pnlMainte.Visible = true;
		this.pnlKey.Enabled = false;
	}

	protected void lnkCastChargeTemp_Command(object sender,CommandEventArgs e) {
		string sArg = iBridUtil.GetStringValue(e.CommandArgument);

		this.Response.Redirect(string.Format("{0}&actcategoryseq={1}&actcategorynm={2}",sArg,this.ActCategorySeq,this.ActCategorySeqNm));
		//this.Response.Redirect(string.Format("{0}",sArg));
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		this.Update(ViCommConst.FLAG_OFF);
		this.pnlMainte.Visible = false;
		this.GetList();
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		this.Update(ViCommConst.FLAG_ON);
		this.pnlMainte.Visible = false;
		this.GetList();
	}

	private void InitPage() {
		this.pnlMainte.Visible = false;

		this.lstSeekSiteCd.DataBind();
		this.lstCastChargeTempCd.DataBind();

		if (!string.IsNullOrEmpty(this.SiteCd)) {
			lstSeekSiteCd.SelectedValue = this.SiteCd;
		} else if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		this.ClearField();
	}

	private void ClearField() {
		this.txtCastChargeTempCdNm.Text = string.Empty;

		this.NaFlag = string.Empty;
		this.ApplicationDate = string.Empty;
		this.RevisionNo = string.Empty;

		this.lblNaError.Visible = false;
	}

	private void GetList() {
		this.lblNaError.Visible = false;
		this.grdCastChargeTempCondition.DataBind();
	}

	private void GetData(string pSiteCd,string pCastChargeTempCd) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_CHARGE_TEMP_CONDITION_GET");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pCAST_CHARGE_TEMP_CD",DbSession.DbType.VARCHAR2,pCastChargeTempCd);
			db.ProcedureOutParm("pCAST_CHARGE_TEMP_CD_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pNA_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pAPPLICATION_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			if (db.GetIntValue("pRECORD_COUNT") > 0) {
				this.txtCastChargeTempCdNm.Text = db.GetStringValue("pCAST_CHARGE_TEMP_CD_NM");

				this.NaFlag = db.GetStringValue("pNA_FLAG");
				this.ApplicationDate = db.GetStringValue("pAPPLICATION_DATE");
				this.RevisionNo = db.GetStringValue("pREVISION_NO");
			}
		}
	}

	private void Update(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_CHARGE_TEMP_CONDITION_MNT");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			db.ProcedureInParm("pCAST_CHARGE_TEMP_CD",DbSession.DbType.VARCHAR2,this.lstCastChargeTempCd.SelectedValue);
			db.ProcedureInParm("pCAST_CHARGE_TEMP_CD_NM",DbSession.DbType.VARCHAR2,this.txtCastChargeTempCdNm.Text.Trim());
			db.ProcedureInParm("pNA_FLAG",DbSession.DbType.NUMBER,this.NaFlag);
			db.ProcedureInParm("pAPPLICATION_DATE",DbSession.DbType.VARCHAR2,this.ApplicationDate);
			db.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			db.ProcedureInParm("pDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}
