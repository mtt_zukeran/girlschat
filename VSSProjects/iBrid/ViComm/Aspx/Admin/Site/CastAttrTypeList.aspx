<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastAttrTypeList.aspx.cs" Inherits="Site_CastAttrTypeList"
	Title="出演者属性区分設定" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="出演者属性区分設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									サイトコード
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
									</asp:DropDownList>
									<asp:Label ID="lblCastAttrTypeSeq" runat="server" Text="Label" Visible="false"></asp:Label>
								</td>
							</tr>
						</table>
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[属性区分内容]</legend>
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									属性区分名
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCastAttrTypeNm" runat="server" MaxLength="30" Width="200px"></asp:TextBox>&nbsp;*&nbsp;ﾌﾟﾛﾌｨｰﾙ設定時のﾀｲﾄﾙに使用されます。
									<asp:RequiredFieldValidator ID="vdrCastAttrTypeNm" runat="server" ErrorMessage="属性区分名を入力して下さい。" ControlToValidate="txtCastAttrTypeNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									詳細検索時タイトル
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCastAttrTypeFindNm" runat="server" MaxLength="30" Width="200px"></asp:TextBox>&nbsp;*&nbsp;詳細検索時のﾀｲﾄﾙに使用されます。
									<asp:RequiredFieldValidator ID="vdrCastAttrTypeFindNm" runat="server" ErrorMessage="属性区分検索時表示名を入力して下さい。" ControlToValidate="txtCastAttrTypeFindNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									表示順位
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtPriority" runat="server" MaxLength="3" Width="60px"></asp:TextBox>&nbsp;
									<asp:RequiredFieldValidator ID="vdrPriority" runat="server" ErrorMessage="優先順位を入力して下さい。" ControlToValidate="txtPriority" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									入力種別
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstInputType" runat="server" Width="206px" DataSourceID="dsInputType" DataTextField="CODE_NM" DataValueField="CODE">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									行数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtRowCount" runat="server" MaxLength="2" Width="50px"></asp:TextBox>&nbsp;
									<asp:CustomValidator ID="vdcRowCount" runat="server" ErrorMessage="*" OnServerValidate="vdcRowCount_ServerValidate" ValidationGroup="Detail"></asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdeRowCount" runat="server" ControlToValidate="txtRowCount" ErrorMessage="行数を入力して下さい。" ValidationGroup="Detail" Enabled="False">*</asp:RequiredFieldValidator>
									<asp:RangeValidator ID="vdrRowCount" runat="server" ErrorMessage="行数は1〜99の間で入力してください" MaximumValue="99" MinimumValue="1" ValidationGroup="Detail" ControlToValidate="txtRowCount"
										Enabled="False">*</asp:RangeValidator></td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									グルーピング
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstGrouping" runat="server" Width="206px" DataSourceID="dsGrouping" DataTextField="CODE_NM" DataValueField="CODE">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									利用不可
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkNaFlag" runat="server" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									アイテムNo.
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtItemNo" runat="server" MaxLength="2" Width="40px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrItemNo" runat="server" ErrorMessage="アイテムNo.を入力して下さい。" ControlToValidate="txtItemNo" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeItemNo" runat="server" ErrorMessage="アイテムNo.は01〜20の間で入力して下さい。" ValidationExpression="([0-1]\d|20)" ControlToValidate="txtItemNo"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									プロフィール設定完了<br />
									必須項目
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkProfileReqIitemFlag" runat="server" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									検索可
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkOmitSeekContionFlag" runat="server" />
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[属性区分一覧]</legend>
			<table border="0" style="width: 640px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle2">
						サイトコード
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
						</asp:DropDownList>
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnRegist" Text="属性区分追加" CssClass="seekbutton" CausesValidation="False" OnClick="btnRegist_Click" />
			<br />
			<br />
			<asp:GridView ID="grdCastAttrType" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCastAttrType" AllowSorting="True" SkinID="GridViewColor">
				<Columns>
					<asp:BoundField DataField="ITEM_NO" HeaderText="アイテムNo.">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
					<asp:BoundField DataField="PRIORITY" HeaderText="表示順位">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkCastAttrTypeSeq" runat="server" Text='<%# Eval("CAST_ATTR_TYPE_NM") %>' CommandArgument='<%# Eval("CAST_ATTR_TYPE_SEQ") %>' OnCommand="lnkCastAttrTypeSeq_Command"
								CausesValidation="False">
							</asp:LinkButton>
						</ItemTemplate>
						<HeaderTemplate>
							属性区分名
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:BoundField DataField="CAST_ATTR_TYPE_FIND_NM" HeaderText="属性区分検索時表示名">
						<ItemStyle HorizontalAlign="Left" />
					</asp:BoundField>
					<asp:BoundField DataField="CODE_NM" HeaderText="入力種別">
						<ItemStyle HorizontalAlign="Left" />
					</asp:BoundField>
					<asp:BoundField DataField="NA_MARK" HeaderText="利用">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
					<asp:BoundField DataField="PROFILE_REQ_ITEM_MARK" HeaderText="必須">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br>
				<a class="reccount">Current viewing page
					<%=grdCastAttrType.PageIndex + 1%>
					of
					<%=grdCastAttrType.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsCastAttrType" runat="server" SelectMethod="GetPageCollection" TypeName="CastAttrType" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsCastAttrType_Selected" OnSelecting="dsCastAttrType_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsGrouping" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="80" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsInputType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="47" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtRowCount" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPriority" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrCastAttrTypeNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vdrCastAttrTypeFindNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdrPriority" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrItemNo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdeItemNo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender5" TargetControlID="vdrRowCount" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender6" TargetControlID="vdeRowCount" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
