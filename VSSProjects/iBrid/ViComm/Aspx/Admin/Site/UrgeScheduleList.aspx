<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UrgeScheduleList.aspx.cs" Inherits="Site_UrgeScheduleListt"
	Title="督促スケジュール" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="督促スケジュール"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey" Width="350px">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 300px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									サイト名
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblSiteCd" runat="server" Text="" Visible="false"></asp:Label>
									<asp:Label ID="lblSiteNm" runat="server" Text=""></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									督促レベル
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblUrgeLevel" runat="server" Text=""></asp:Label>
									<asp:Label ID="lblUrgeLevelNm" runat="server" Text=""></asp:Label>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUrgeLevel" Text="督促レベル設定へ" CssClass="seekbutton" CausesValidation="false" PostBackUrl='<%# string.Format("UrgeLevelList.aspx?sitecd={0}",lblSiteCd.Text)%>' />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl" Width="790px">
					<fieldset class="fieldset-inner">
						<legend>[スケジュール内容]</legend>
						<asp:PlaceHolder ID="plcHolder" runat="server">
							<table border="1" style="width: 750px" class="tableStyle">
								<tr align="center">
									<td class="HeaderStyle" align="center">
										督促回数<br />
										／日
									</td>
									<td class="HeaderStyle" align="center">
										督促開始<br />
										時間
									</td>
									<td class="HeaderStyle" align="center">
										督促終了<br />
										時間
									</td>
									<td class="HeaderStyle" align="center">
										督促種別
									</td>
									<td class="HeaderStyle" align="center">
										ﾒｰﾙﾃﾝﾌﾟﾚｰﾄ
									</td>
									<td class="HeaderStyle" align="center">
										最終実行日時
									</td>
								</tr>
								<tr>
									<td class="tdDataStyle" align="right">
										<asp:Label ID="lblUrgeCountPerDay1" runat="server" Width="70px"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:TextBox ID="txtUrgeStartTime1" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
										<asp:RegularExpressionValidator ID="vdeUrgeStartTime1" runat="server" ErrorMessage="開始時刻は「HH:MM」形式で入力して下さい。" ValidationExpression="^([0-1][0-9]|[2][0-3]):[0-5][0-9]"
											ControlToValidate="txtUrgeStartTime1" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:TextBox ID="txtUrgeEndTime1" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
										<asp:RegularExpressionValidator ID="vdeUrgeEndTime1" runat="server" ErrorMessage="終了時刻は「HH:MM」形式で入力して下さい。" ValidationExpression="^([0-1][0-9]|[2][0-3]):[0-5][0-9]"
											ControlToValidate="txtUrgeEndTime1" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
										<asp:CompareValidator ID="vdcUrgeEndTime1" runat="server" ControlToCompare="txtUrgeStartTime1" ControlToValidate="txtUrgeEndTime1" ErrorMessage="終了時刻＞開始時刻となっています。"
											Operator="GreaterThanEqual" ValidationGroup="Detail">*</asp:CompareValidator>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:DropDownList ID="lstUrgeType1" runat="server" DataSourceID="dsUrgeType" DataTextField="CODE_NM" DataValueField="CODE" Width="70px">
										</asp:DropDownList>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:DropDownList ID="lstMailTemplateNo1" runat="server" DataSourceID="dsMailTemplate" DataTextField="TEMPLATE_NM" DataValueField="MAIL_TEMPLATE_NO" Width="309px">
										</asp:DropDownList>
										<asp:CustomValidator ID="vdcMailTemplateNo1" runat="server" ErrorMessage="メールテンプレートが未選択です。" OnServerValidate="vdcMailTemplateNo_ServerValidate" ValidationGroup="Detail">*</asp:CustomValidator>
										<asp:RequiredFieldValidator ID="vdrMailTemplateNo1" runat="server" ErrorMessage="メールテンプレートが未選択です。" ControlToValidate="lstMailTemplateNo1" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
									<td class="tdDataStyle">
										<asp:Label ID="lblUrgeLastExecDate1" runat="server" Width="130px"></asp:Label>
									</td>
								</tr>
								<tr>
									<td class="tdDataStyle" align="right">
										<asp:Label ID="lblUrgeCountPerDay2" runat="server"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:TextBox ID="txtUrgeStartTime2" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
										<asp:RegularExpressionValidator ID="vdeUrgeStartTime2" runat="server" ErrorMessage="開始時刻は「HH:MM」形式で入力して下さい。" ValidationExpression="^([0-1][0-9]|[2][0-3]):[0-5][0-9]"
											ControlToValidate="txtUrgeStartTime2" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:TextBox ID="txtUrgeEndTime2" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
										<asp:RegularExpressionValidator ID="vdeUrgeEndTime2" runat="server" ErrorMessage="終了時刻は「HH:MM」形式で入力して下さい。" ValidationExpression="^([0-1][0-9]|[2][0-3]):[0-5][0-9]"
											ControlToValidate="txtUrgeEndTime2" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
										<asp:CompareValidator ID="vdcUrgeEndTime2" runat="server" ControlToCompare="txtUrgeStartTime2" ControlToValidate="txtUrgeEndTime2" ErrorMessage="終了時刻＞開始時刻となっています。"
											Operator="GreaterThanEqual" ValidationGroup="Detail">*</asp:CompareValidator>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:DropDownList ID="lstUrgeType2" runat="server" DataSourceID="dsUrgeType" DataTextField="CODE_NM" DataValueField="CODE" Width="70px">
										</asp:DropDownList>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:DropDownList ID="lstMailTemplateNo2" runat="server" DataSourceID="dsMailTemplate" DataTextField="TEMPLATE_NM" DataValueField="MAIL_TEMPLATE_NO" Width="309px">
										</asp:DropDownList>
										<asp:CustomValidator ID="vdcMailTemplateNo2" runat="server" ErrorMessage="メールテンプレートが未選択です。" OnServerValidate="vdcMailTemplateNo_ServerValidate" ValidationGroup="Detail">*</asp:CustomValidator>
										<asp:RequiredFieldValidator ID="vdrMailTemplateNo2" runat="server" ErrorMessage="メールテンプレートが未選択です。" ControlToValidate="lstMailTemplateNo2" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
									<td class="tdDataStyle">
										<asp:Label ID="lblUrgeLastExecDate2" runat="server" Width="130px"></asp:Label>
									</td>
								</tr>
								<tr>
									<td class="tdDataStyle" align="right">
										<asp:Label ID="lblUrgeCountPerDay3" runat="server"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:TextBox ID="txtUrgeStartTime3" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
										<asp:RegularExpressionValidator ID="vdeUrgeStartTime3" runat="server" ErrorMessage="開始時刻は「HH:MM」形式で入力して下さい。" ValidationExpression="^([0-1][0-9]|[2][0-3]):[0-5][0-9]"
											ControlToValidate="txtUrgeStartTime3" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:TextBox ID="txtUrgeEndTime3" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
										<asp:RegularExpressionValidator ID="vdeUrgeEndTime3" runat="server" ErrorMessage="終了時刻は「HH:MM」形式で入力して下さい。" ValidationExpression="^([0-1][0-9]|[2][0-3]):[0-5][0-9]"
											ControlToValidate="txtUrgeEndTime3" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
										<asp:CompareValidator ID="vdcUrgeEndTime3" runat="server" ControlToCompare="txtUrgeStartTime3" ControlToValidate="txtUrgeEndTime3" ErrorMessage="終了時刻＞開始時刻となっています。"
											Operator="GreaterThanEqual" ValidationGroup="Detail">*</asp:CompareValidator>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:DropDownList ID="lstUrgeType3" runat="server" DataSourceID="dsUrgeType" DataTextField="CODE_NM" DataValueField="CODE" Width="70px">
										</asp:DropDownList>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:DropDownList ID="lstMailTemplateNo3" runat="server" DataSourceID="dsMailTemplate" DataTextField="TEMPLATE_NM" DataValueField="MAIL_TEMPLATE_NO" Width="309px">
										</asp:DropDownList>
										<asp:CustomValidator ID="vdcMailTemplateNo3" runat="server" ErrorMessage="メールテンプレートが未選択です。" OnServerValidate="vdcMailTemplateNo_ServerValidate" ValidationGroup="Detail">*</asp:CustomValidator>
										<asp:RequiredFieldValidator ID="vdrMailTemplateNo3" runat="server" ErrorMessage="メールテンプレートが未選択です。" ControlToValidate="lstMailTemplateNo3" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
									<td class="tdDataStyle">
										<asp:Label ID="lblUrgeLastExecDate3" runat="server" Width="130px"></asp:Label>
									</td>
								</tr>
								<tr>
									<td class="tdDataStyle" align="right">
										<asp:Label ID="lblUrgeCountPerDay4" runat="server"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:TextBox ID="txtUrgeStartTime4" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
										<asp:RegularExpressionValidator ID="vdeUrgeStartTime4" runat="server" ErrorMessage="開始時刻は「HH:MM」形式で入力して下さい。" ValidationExpression="^([0-1][0-9]|[2][0-3]):[0-5][0-9]"
											ControlToValidate="txtUrgeStartTime4" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:TextBox ID="txtUrgeEndTime4" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
										<asp:RegularExpressionValidator ID="vdeUrgeEndTime4" runat="server" ErrorMessage="終了時刻は「HH:MM」形式で入力して下さい。" ValidationExpression="^([0-1][0-9]|[2][0-3]):[0-5][0-9]"
											ControlToValidate="txtUrgeEndTime4" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
										<asp:CompareValidator ID="vdcUrgeEndTime4" runat="server" ControlToCompare="txtUrgeStartTime4" ControlToValidate="txtUrgeEndTime4" ErrorMessage="終了時刻＞開始時刻となっています。"
											Operator="GreaterThanEqual" ValidationGroup="Detail">*</asp:CompareValidator>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:DropDownList ID="lstUrgeType4" runat="server" DataSourceID="dsUrgeType" DataTextField="CODE_NM" DataValueField="CODE" Width="70px">
										</asp:DropDownList>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:DropDownList ID="lstMailTemplateNo4" runat="server" DataSourceID="dsMailTemplate" DataTextField="TEMPLATE_NM" DataValueField="MAIL_TEMPLATE_NO" Width="309px">
										</asp:DropDownList>
										<asp:CustomValidator ID="vdcMailTemplateNo4" runat="server" ErrorMessage="メールテンプレートが未選択です。" OnServerValidate="vdcMailTemplateNo_ServerValidate" ValidationGroup="Detail">*</asp:CustomValidator>
										<asp:RequiredFieldValidator ID="vdrMailTemplateNo4" runat="server" ErrorMessage="メールテンプレートが未選択です。" ControlToValidate="lstMailTemplateNo4" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
									<td class="tdDataStyle">
										<asp:Label ID="lblUrgeLastExecDate4" runat="server" Width="130px"></asp:Label>
									</td>
								</tr>
								<tr>
									<td class="tdDataStyle" align="right">
										<asp:Label ID="lblUrgeCountPerDay5" runat="server"></asp:Label>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:TextBox ID="txtUrgeStartTime5" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
										<asp:RegularExpressionValidator ID="vdeUrgeStartTime5" runat="server" ErrorMessage="開始時刻は「HH:MM」形式で入力して下さい。" ValidationExpression="^([0-1][0-9]|[2][0-3]):[0-5][0-9]"
											ControlToValidate="txtUrgeStartTime5" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:TextBox ID="txtUrgeEndTime5" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
										<asp:RegularExpressionValidator ID="vdeUrgeEndTime5" runat="server" ErrorMessage="終了時刻は「HH:MM」形式で入力して下さい。" ValidationExpression="^([0-1][0-9]|[2][0-3]):[0-5][0-9]"
											ControlToValidate="txtUrgeEndTime5" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
										<asp:CompareValidator ID="vdcUrgeEndTime5" runat="server" ControlToCompare="txtUrgeStartTime5" ControlToValidate="txtUrgeEndTime5" ErrorMessage="終了時刻＞開始時刻となっています。"
											Operator="GreaterThanEqual" ValidationGroup="Detail">*</asp:CompareValidator>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:DropDownList ID="lstUrgeType5" runat="server" DataSourceID="dsUrgeType" DataTextField="CODE_NM" DataValueField="CODE" Width="70px">
										</asp:DropDownList>
									</td>
									<td class="tdDataStyle" align="center" valign="middle">
										<asp:DropDownList ID="lstMailTemplateNo5" runat="server" DataSourceID="dsMailTemplate" DataTextField="TEMPLATE_NM" DataValueField="MAIL_TEMPLATE_NO" Width="309px">
										</asp:DropDownList>
										<asp:CustomValidator ID="vdcMailTemplateNo5" runat="server" ErrorMessage="メールテンプレートが未選択です。" OnServerValidate="vdcMailTemplateNo_ServerValidate" ValidationGroup="Detail">*</asp:CustomValidator>
										<asp:RequiredFieldValidator ID="vdrMailTemplateNo5" runat="server" ErrorMessage="メールテンプレートが未選択です。" ControlToValidate="lstMailTemplateNo5" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
									<td class="tdDataStyle">
										<asp:Label ID="lblUrgeLastExecDate5" runat="server" Width="130px"></asp:Label>
									</td>
								</tr>
							</table>
						</asp:PlaceHolder>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[スケジュール一覧]</legend>
			<table border="0" style="width: 750px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle2">
						サイト名
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
						</asp:DropDownList>
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<br />
			<br />
			<asp:GridView ID="grdUrgeSchedule" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsUrgeSchedule" AllowSorting="True" SkinID="GridViewColor"
				OnDataBound="grdUrgeSchedule_DataBound">
				<Columns>
					<asp:TemplateField HeaderText="督促<BR/>レベル">
						<ItemTemplate>
							<asp:HyperLink ID="lnkUrgeLevel" runat="server" Text='<%# Eval("URGE_LEVEL")%>' NavigateUrl='<%# string.Format("~/Site/UrgeScheduleList.aspx?sitecd={0}&urgelevel={1}",Eval("SITE_CD"),Eval("URGE_LEVEL"))%>'></asp:HyperLink>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Center" />
					</asp:TemplateField>
					<asp:TemplateField HeaderText="督促回数<BR/>／日">
						<ItemTemplate>
							<asp:Label ID="lnkUrgeSchedule" runat="server" Text='<%# Eval("URGE_COUNT_PER_DAY")%>'></asp:Label>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Right" />
					</asp:TemplateField>
					<asp:TemplateField HeaderText="督促<BR/>開始日">
						<ItemTemplate>
							<asp:Label ID="lnkUrgeSchedule" runat="server" Text='<%# Eval("URGE_START_TIME")%>'></asp:Label>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Center" />
					</asp:TemplateField>
					<asp:TemplateField HeaderText="督促<BR/>終了日">
						<ItemTemplate>
							<asp:Label ID="lnkUrgeSchedule" runat="server" Text='<%# Eval("URGE_END_TIME")%>'></asp:Label>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Center" />
					</asp:TemplateField>
					<asp:TemplateField HeaderText="督促<BR/>種別">
						<ItemTemplate>
							<asp:Label ID="lnkUrgeSchedule" runat="server" Text='<%# Eval("URGE_TYPE_NM")%>'></asp:Label>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Center" />
					</asp:TemplateField>
					<asp:BoundField DataField="TEMPLATE_NM" HeaderText="ﾒｰﾙﾃﾝﾌﾟﾚｰﾄ">
						<ItemStyle HorizontalAlign="Left" />
					</asp:BoundField>
					<asp:BoundField DataField="URGE_LAST_EXEC_DATE" HeaderText="最終実行日時">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br />
				<a class="reccount">Current viewing page
					<%=grdUrgeSchedule.PageIndex + 1%>
					of
					<%=grdUrgeSchedule.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsUrgeSchedule" runat="server" SelectMethod="GetPageCollection" TypeName="UrgeSchedule" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsUrgeSchedule_Selected" OnSelecting="dsUrgeSchedule_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUrgeLevel" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsMailTemplate" runat="server" SelectMethod="GetListByTemplateType" TypeName="MailTemplate" OnSelecting="dsMailTemplate_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pMailTemplateType" Type="String" />
			<asp:Parameter DefaultValue="1" Name="pSexCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUrgeType" runat="server" SelectMethod="GetUrgeType" TypeName="UrgeSchedule"></asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdeUrgeStartTime1" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeUrgeEndTime1" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11" TargetControlID="vdcUrgeEndTime1" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender17" TargetControlID="vdrMailTemplateNo1" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdeUrgeStartTime2" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdeUrgeEndTime2" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender12" TargetControlID="vdcUrgeEndTime2" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender18" TargetControlID="vdrMailTemplateNo2" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdeUrgeStartTime3" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdeUrgeEndTime3" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender13" TargetControlID="vdcUrgeEndTime3" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender19" TargetControlID="vdrMailTemplateNo3" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vdeUrgeStartTime4" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8" TargetControlID="vdeUrgeEndTime4" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender14" TargetControlID="vdcUrgeEndTime4" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender20" TargetControlID="vdrMailTemplateNo4" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9" TargetControlID="vdeUrgeStartTime5" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10" TargetControlID="vdeUrgeEndTime5" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender15" TargetControlID="vdcUrgeEndTime5" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender16" TargetControlID="vdrMailTemplateNo5" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
