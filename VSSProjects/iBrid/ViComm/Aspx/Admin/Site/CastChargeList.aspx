<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastChargeList.aspx.cs" Inherits="Site_CastChargeList" Title="出演者課金設定"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="出演者課金設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 850px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									サイトコード
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblSiteCd" runat="server" Text=""></asp:Label>
									<asp:Label ID="lblSiteNm" runat="server" Text=""></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									カテゴリー
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblActCategoryNm" runat="server" Text=""></asp:Label>
								</td>
							</tr>
						</table>
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[課金内容]</legend>
						<asp:PlaceHolder ID="plcHolder" runat="server">
							<table border="0" style="width: 800px" class="tableStyle">
								<tr>
                                    <td class="tdHeaderSmallStyle" colspan="2" style="width: 200px" valign="bottom" align="right">
                                        <asp:Label ID="lblNotice" runat="server" Text="※「単位」列を除き、金額の値を表します"></asp:Label>
                                    </td>
									<td class="tdHeaderSmallStyle2L" align="center" colspan="2" style="width: 100px">
										RANK A</td>
									<td class="tdHeaderSmallStyle2L" align="center" colspan="2" style="width: 100px">
										RANK B</td>
									<td class="tdHeaderSmallStyle2L" align="center" colspan="2" style="width: 100px">
										RANK C</td>
									<td class="tdHeaderSmallStyle2L" align="center" colspan="2" style="width: 100px">
										RANK D</td>
									<td class="tdHeaderSmallStyle2L" align="center" colspan="2" style="width: 100px">
										RANK E</td>
									<td class="tdHeaderSmallStyle2L" align="center" style="width: 50px">
										Free<br />
										ﾀﾞｲｱﾙ</td>
                                    <td align="center" class="tdHeaderSmallStyle2L" colspan="2" style="width: 100px">
                                        talk app</td>
                                </tr>
								<tr align="center">
									<td class="tdHeaderSmallStyle" style="width: 200px">
									</td>
									<td class="tdHeaderSmallStyleL" style="width: 50px">
										単位
									</td>
									<td class="tdHeaderSmallStyleL" style="width: 50px">
										通常
									</td>
									<td class="tdHeaderSmallStyleL" style="width: 50px">
										未入金
									</td>
									<td class="tdHeaderSmallStyleL" style="width: 50px">
										通常
									</td>
									<td class="tdHeaderSmallStyleL" style="width: 50px">
										未入金
									</td>
									<td class="tdHeaderSmallStyleL" style="width: 50px">
										通常
									</td>
									<td class="tdHeaderSmallStyleL" style="width: 50px">
										未入金
									</td>
									<td class="tdHeaderSmallStyleL" style="width: 50px">
										通常
									</td>
									<td class="tdHeaderSmallStyleL" style="width: 50px">
										未入金
									</td>
									<td class="tdHeaderSmallStyleL" style="width: 50px">
										通常
									</td>
									<td class="tdHeaderSmallStyleL" style="width: 50px">
										未入金
									</td>
									<td class="tdHeaderSmallStyleL" style="width: 50px">
										減算<br />金額
									</td>
                                    <td class="tdHeaderSmallStyleL" style="width: 50px">
                                        出演者<br />通話ｱﾌﾟﾘ<br />利用時
                                    </td>
                                    <td class="tdHeaderSmallStyleL" style="width: 50px">
                                        男性<br />通話ｱﾌﾟﾘ<br />利用時
                                    </td>
                                </tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm0" runat="server" Text="課金0" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec0" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec0" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec0" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec0" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec0"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal0" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal0" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal0" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal0" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal0"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt0" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt0" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt0"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt0" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoReceipt0"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB0" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB0" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB0" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB0" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB0"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB0" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB0" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB0"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB0" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB0" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC0" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC0" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC0" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC0" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC0"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC0" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC0" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC0"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC0" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC0" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD0" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD0" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD0" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD0" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD0"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD0" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD0" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD0"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD0" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD0" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE0" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE0" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE0" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE0" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE0"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE0" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE0" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE0"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE0" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE0" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee0" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee0" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee0" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee0" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee0"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint0" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint0" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint0" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint0" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint0"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint0" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint0" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint0" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint0" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint0"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm1" runat="server" Text="課金1" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec1" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec1" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec1" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec1" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec1"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal1" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal1" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal1" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal1" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal1"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt1" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt1" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt1"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt1" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoReceipt1"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB1" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB1" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB1" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB1" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB1"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB1" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB1" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB1"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB1" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB1" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC1" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC1" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC1" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC1" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC1"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC1" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC1" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC1"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC1" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC1" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD1" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD1" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD1" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD1" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD1"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD1" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD1" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD1"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD1" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD1" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE1" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE1" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE1" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE1" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE1"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE1" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE1" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE1"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE1" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE1" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee1" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee1" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee1" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee1" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee1"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint1" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint1" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint1" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint1" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint1"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint1" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint1" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint1" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint1" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint1"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm2" runat="server" Text="課金2" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec2" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec2" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec2" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec2" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec2"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal2" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal2" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal2" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal2" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal2"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt2" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt2" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt2"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt2" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoReceipt2"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB2" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB2" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB2" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB2" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB2"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB2" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB2" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB2"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB2" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB2" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC2" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC2" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC2" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC2" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC2"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC2" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC2" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC2"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC2" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC2" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD2" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD2" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD2" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD2" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD2"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD2" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD2" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD2"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD2" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD2" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE2" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE2" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE2" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE2" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE2"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE2" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE2" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE2"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE2" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE2" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee2" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee2" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee2" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee2" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee2"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint2" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint2" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint2" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint2" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint2"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint2" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint2" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint2" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint2" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint2"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm3" runat="server" Text="課金3" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec3" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec3" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec3" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec3" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec3"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal3" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal3" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal3" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal3" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal3"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt3" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt3" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt3"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt3" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoReceipt3"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB3" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB3" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB3" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB3" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB3"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB3" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB3" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB3"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB3" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB3" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC3" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC3" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC3" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC3" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC3"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC3" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC3" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC3"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC3" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC3" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD3" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD3" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD3" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD3" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD3"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD3" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD3" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD3"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD3" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD3" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE3" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE3" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE3" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE3" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE3"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE3" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE3" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE3"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE3" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE3" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee3" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee3" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee3" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee3" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee3"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint3" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint3" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint3" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint3" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint3"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint3" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint3" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint3" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint3" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint3"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm4" runat="server" Text="課金4" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec4" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec4" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec4" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec4" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec4"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal4" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal4" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal4" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal4" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal4"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt4" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt4" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt4"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt4" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoReceipt4"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB4" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB4" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB4" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB4" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB4"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB4" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB4" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB4"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB4" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB4" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC4" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC4" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC4" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC4" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC4"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC4" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC4" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC4"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC4" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC4" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD4" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD4" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD4" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD4" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD4"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD4" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD4" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD4"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD4" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD4" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE4" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE4" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE4" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE4" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE4"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE4" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE4" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE4"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE4" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE4" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee4" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee4" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee4" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee4" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee4"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint4" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint4" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint4" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint4" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint4"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint4" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint4" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint4" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint4" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint4"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm5" runat="server" Text="課金5" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec5" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec5" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec5" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec5" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec5"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal5" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal5" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal5" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal5" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal5"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt5" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt5" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt5"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt5" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoReceipt5"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB5" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB5" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB5" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB5" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB5"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB5" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB5" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB5"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB5" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB5" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC5" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC5" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC5" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC5" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC5"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC5" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC5" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC5"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC5" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC5" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD5" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD5" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD5" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD5" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD5"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD5" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD5" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD5"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD5" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD5" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE5" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE5" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE5" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE5" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE5"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE5" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE5" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE5"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE5" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE5" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee5" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee5" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee5" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee5" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee5"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint5" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint5" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint5" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint5" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint5"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint5" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint5" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint5" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint5" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint5"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm6" runat="server" Text="課金6" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec6" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec6" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec6" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec6" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec6"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal6" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal6" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal6" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal6" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal6"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt6" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt6" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt6"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt6" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoReceipt6"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB6" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB6" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB6" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB6" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB6"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB6" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB6" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB6"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB6" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB6" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC6" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC6" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC6" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC6" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC6"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC6" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC6" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC6"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC6" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC6" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD6" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD6" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD6" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD6" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD6"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD6" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD6" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD6"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD6" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD6" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE6" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE6" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE6" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE6" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE6"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE6" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE6" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE6"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE6" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE6" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee6" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee6" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee6" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee6" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee6"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint6" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint6" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint6" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint6" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint6"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint6" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint6" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint6" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint6" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint6"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm7" runat="server" Text="課金7" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec7" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec7" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec7" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec7" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec7"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal7" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal7" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal7" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal7" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal7"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt7" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt7" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt7"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt7" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoReceipt7"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB7" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB7" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB7" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB7" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB7"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB7" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB7" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB7"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB7" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB7" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC7" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC7" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC7" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC7" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC7"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC7" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC7" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC7"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC7" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC7" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD7" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD7" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD7" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD7" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD7"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD7" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD7" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD7"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD7" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD7" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE7" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE7" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE7" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE7" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE7"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE7" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE7" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE7"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE7" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE7" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee7" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee7" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee7" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee7" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee7"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint7" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint7" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint7" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint7" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint7"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint7" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint7" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint7" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint7" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint7"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm8" runat="server" Text="課金8" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec8" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec8" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec8" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec8" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec8"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal8" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal8" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal8" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal8" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal8"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt8" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt8" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt8"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt8" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoReceipt8"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB8" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB8" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB8" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB8" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB8"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB8" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB8" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB8"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB8" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB8" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC8" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC8" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC8" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC8" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC8"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC8" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC8" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC8"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC8" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC8" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD8" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD8" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD8" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD8" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD8"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD8" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD8" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD8"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD8" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD8" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE8" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE8" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE8" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE8" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE8"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE8" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE8" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE8"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE8" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE8" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee8" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee8" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee8" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee8" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee8"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint8" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint8" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint8" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint8" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint8"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint8" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint8" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint8" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint8" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint8"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm9" runat="server" Text="課金9" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec9" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec9" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec9" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec9" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec9"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal9" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal9" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal9" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal9" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal9"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt9" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt9" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt9"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt9" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNoReceipt9"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB9" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB9" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB9" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB9" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB9"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB9" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB9" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB9"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB9" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB9" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC9" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC9" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC9" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC9" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC9"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC9" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC9" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC9"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC9" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC9" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD9" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD9" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD9" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD9" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD9"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD9" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD9" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD9"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD9" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD9" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE9" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE9" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE9" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE9" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE9"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE9" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE9" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE9"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE9" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE9" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee9" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee9" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee9" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee9" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee9"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint9" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint9" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint9" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint9" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint9"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint9" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint9" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint9" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint9" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint9"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm10" runat="server" Text="課金10" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec10" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec10" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec10" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec10" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec10"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal10" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal10" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal10" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal10" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal10"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt10" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt10" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt10"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt10" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt10" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB10" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB10" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB10" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB10" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB10"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB10" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB10" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB10"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB10" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB10" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC10" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC10" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC10" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC10" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC10"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC10" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC10" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC10"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC10" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC10" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD10" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD10" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD10" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD10" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD10"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD10" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD10" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD10"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD10" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD10" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE10" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE10" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE10" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE10" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE10"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE10" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE10" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE10"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE10" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE10" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee10" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee10" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee10" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee10" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee10"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint10" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint10" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint10" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint10" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint10"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint10" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint10" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint10" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint10" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint10"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm11" runat="server" Text="課金11" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec11" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec11" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec11" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec11" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec11"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal11" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal11" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal11" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal11" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal11"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt11" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt11" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt11"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt11" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt11" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB11" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB11" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB11" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB11" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB11"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB11" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB11" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB11"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB11" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB11" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC11" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC11" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC11" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC11" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC11"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC11" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC11" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC11"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC11" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC11" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD11" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD11" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD11" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD11" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD11"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD11" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD11" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD11"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD11" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD11" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE11" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE11" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE11" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE11" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE11"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE11" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE11" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE11"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE11" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE11" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee11" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee11" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee11" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee11" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee11"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint11" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint11" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint11" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint11" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint11"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint11" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint11" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint11" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint11" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint11"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm12" runat="server" Text="課金12" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec12" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec12" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec12" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec12" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec12"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal12" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal12" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal12" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal12" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal12"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt12" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt12" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt12"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt12" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt12" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB12" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB12" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB12" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB12" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB12"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB12" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB12" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB12"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB12" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB12" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC12" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC12" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC12" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC12" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC12"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC12" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC12" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC12"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC12" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC12" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD12" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD12" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD12" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD12" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD12"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD12" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD12" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD12"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD12" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD12" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE12" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE12" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE12" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE12" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE12"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE12" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE12" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE12"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE12" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE12" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee12" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee12" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee12" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee12" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee12"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint12" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint12" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint12" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint12" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint12"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint12" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint12" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint12" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint12" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint12"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm13" runat="server" Text="課金13" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec13" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec13" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec13" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec13" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec13"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal13" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal13" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal13" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal13" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal13"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt13" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt13" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt13"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt13" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt13" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB13" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB13" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB13" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB13" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB13"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB13" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB13" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB13"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB13" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB13" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC13" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC13" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC13" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC13" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC13"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC13" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC13" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC13"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC13" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC13" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD13" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD13" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD13" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD13" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD13"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD13" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD13" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD13"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD13" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD13" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE13" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE13" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE13" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE13" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE13"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE13" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE13" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE13"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE13" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE13" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee13" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee13" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee13" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee13" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee13"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint13" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint13" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint13" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint13" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint13"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint13" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint13" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint13" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint13" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint13"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm14" runat="server" Text="課金14" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec14" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec14" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec14" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec14" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec14"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal14" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal14" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal14" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal14" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal14"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt14" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt14" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt14"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt14" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt14" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB14" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB14" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB14" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB14" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB14"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB14" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB14" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB14"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB14" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB14" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC14" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC14" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC14" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC14" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC14"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC14" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC14" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC14"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC14" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC14" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD14" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD14" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD14" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD14" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD14"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD14" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD14" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD14"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD14" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD14" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE14" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE14" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE14" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE14" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE14"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE14" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE14" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE14"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE14" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE14" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee14" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee14" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee14" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee14" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee14"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint14" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint14" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint14" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint14" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint14"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint14" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint14" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint14" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint14" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint14"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm15" runat="server" Text="課金15" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec15" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec15" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec15" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec15" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec15"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal15" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal15" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal15" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal15" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal15"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt15" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt15" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt15"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt15" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt15" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB15" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB15" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB15" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB15" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB15"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB15" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB15" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB15"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB15" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB15" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC15" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC15" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC15" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC15" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC15"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC15" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC15" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC15"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC15" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC15" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD15" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD15" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD15" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD15" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD15"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD15" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD15" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD15"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD15" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD15" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE15" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE15" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE15" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE15" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE15"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE15" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE15" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE15"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE15" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE15" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee15" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee15" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee15" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee15" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee15"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint15" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint15" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint15" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint15" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint15"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint15" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint15" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint15" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint15" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint15"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm16" runat="server" Text="課金16" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec16" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec16" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec16" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec16" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec16"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal16" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal16" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal16" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal16" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal16"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt16" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt16" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt16"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt16" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt16" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB16" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB16" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB16" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB16" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB16"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB16" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB16" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB16"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB16" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB16" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC16" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC16" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC16" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC16" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC16"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC16" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC16" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC16"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC16" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC16" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD16" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD16" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD16" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD16" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD16"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD16" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD16" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD16"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD16" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD16" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE16" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE16" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE16" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE16" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE16"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE16" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE16" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE16"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE16" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE16" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee16" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee16" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee16" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee16" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee16"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint16" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint16" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint16" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint16" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint16"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint16" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint16" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint16" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint16" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint16"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm17" runat="server" Text="課金17" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec17" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec17" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec17" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec17" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec17"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal17" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal17" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal17" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal17" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal17"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt17" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt17" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt17"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt17" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt17" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB17" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB17" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB17" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB17" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB17"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB17" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB17" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB17"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB17" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB17" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC17" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC17" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC17" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC17" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC17"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC17" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC17" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC17"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC17" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC17" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD17" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD17" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD17" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD17" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD17"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD17" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD17" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD17"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD17" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD17" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE17" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE17" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE17" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE17" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE17"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE17" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE17" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE17"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE17" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE17" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee17" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee17" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee17" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee17" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee17"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint17" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint17" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint17" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint17" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint17"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint17" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint17" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint17" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint17" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint17"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm18" runat="server" Text="課金18" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec18" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec18" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec18" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec18" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec18"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal18" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal18" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal18" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal18" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal18"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt18" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt18" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt18"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt18" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt18" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB18" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB18" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB18" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB18" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB18"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB18" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB18" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB18"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB18" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB18" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC18" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC18" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC18" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC18" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC18"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC18" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC18" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC18"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC18" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC18" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD18" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD18" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD18" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD18" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD18"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD18" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD18" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD18"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD18" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD18" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE18" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE18" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE18" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE18" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE18"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE18" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE18" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE18"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE18" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE18" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee18" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee18" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee18" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee18" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee18"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint18" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint18" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint18" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint18" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint18"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint18" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint18" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint18" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint18" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint18"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm19" runat="server" Text="課金19" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec19" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec19" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec19" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec19" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec19"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal19" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal19" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal19" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal19" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal19"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt19" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt19" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt19"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt19" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt19" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB19" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB19" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB19" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB19" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB19"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB19" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB19" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB19"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB19" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB19" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC19" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC19" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC19" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC19" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC19"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC19" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC19" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC19"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC19" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC19" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD19" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD19" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD19" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD19" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD19"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD19" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD19" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD19"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD19" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD19" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE19" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE19" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE19" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE19" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE19"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE19" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE19" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE19"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE19" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE19" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee19" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee19" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee19" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee19" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee19"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint19" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint19" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint19" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint19" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint19"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint19" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint19" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint19" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint19" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint19"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm20" runat="server" Text="課金20" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec20" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec20" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec20" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec20" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec20"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal20" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal20" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal20" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal20" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal20"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt20" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt20" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt20"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt20" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt20" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB20" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB20" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB20" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB20" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB20"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB20" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB20" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB20"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB20" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB20" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC20" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC20" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC20" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC20" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC20"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC20" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC20" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC20"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC20" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC20" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD20" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD20" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD20" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD20" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD20"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD20" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD20" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD20"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD20" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD20" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE20" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE20" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE20" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE20" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE20"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE20" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE20" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE20"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE20" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE20" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee20" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee20" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee20" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee20" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee20"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint20" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint20" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint20" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint20" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint20"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint20" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint20" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint20" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint20" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint20"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm21" runat="server" Text="課金21" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec21" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec21" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec21" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec21" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec21"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal21" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal21" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal21" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal21" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal21"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt21" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt21" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt21"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt21" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt21" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB21" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB21" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB21" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB21" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB21"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB21" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB21" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB21"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB21" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB21" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC21" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC21" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC21" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC21" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC21"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC21" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC21" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC21"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC21" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC21" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD21" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD21" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD21" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD21" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD21"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD21" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD21" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD21"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD21" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD21" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE21" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE21" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE21" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE21" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE21"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE21" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE21" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE21"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE21" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE21" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee21" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee21" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee21" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee21" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee21"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint21" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint21" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint21" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint21" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint21"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint21" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint21" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint21" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint21" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint21"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm22" runat="server" Text="課金22" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec22" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec22" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec22" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec22" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec22"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal22" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal22" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal22" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal22" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal22"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt22" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt22" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt22"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt22" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt22" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB22" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB22" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB22" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB22" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB22"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB22" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB22" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB22"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB22" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB22" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC22" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC22" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC22" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC22" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC22"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC22" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC22" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC22"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC22" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC22" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD22" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD22" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD22" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD22" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD22"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD22" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD22" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD22"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD22" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD22" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE22" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE22" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE22" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE22" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE22"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE22" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE22" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE22"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE22" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE22" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee22" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee22" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee22" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee22" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee22"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint22" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint22" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint22" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint22" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint22"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint22" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint22" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint22" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint22" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint22"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm23" runat="server" Text="課金23" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec23" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec23" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec23" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec23" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec23"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal23" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal23" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal23" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal23" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal23"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt23" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt23" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt23"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt23" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt23" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB23" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB23" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB23" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB23" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB23"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB23" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB23" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB23"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB23" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB23" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC23" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC23" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC23" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC23" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC23"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC23" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC23" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC23"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC23" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC23" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD23" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD23" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD23" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD23" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD23"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD23" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD23" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD23"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD23" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD23" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE23" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE23" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE23" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE23" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE23"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE23" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE23" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE23"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE23" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE23" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee23" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee23" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee23" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee23" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee23"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint23" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint23" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint23" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint23" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint23"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint23" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint23" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint23" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint23" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint23"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm24" runat="server" Text="課金24" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec24" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec24" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec24" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec24" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec24"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal24" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal24" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal24" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal24" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal24"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt24" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt24" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt24"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt24" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt24" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB24" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB24" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB24" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB24" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB24"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB24" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB24" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB24"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB24" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB24" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC24" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC24" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC24" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC24" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC24"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC24" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC24" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC24"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC24" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC24" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD24" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD24" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD24" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD24" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD24"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD24" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD24" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD24"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD24" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD24" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE24" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE24" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE24" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE24" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE24"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE24" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE24" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE24"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE24" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE24" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee24" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee24" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee24" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee24" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee24"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint24" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint24" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint24" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint24" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint24"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint24" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint24" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint24" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint24" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint24"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm25" runat="server" Text="課金25" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec25" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec25" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec25" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec25" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec25"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal25" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal25" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal25" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal25" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal25"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt25" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt25" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt25"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt25" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt25" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB25" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB25" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB25" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB25" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB25"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB25" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB25" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB25"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB25" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB25" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC25" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC25" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC25" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC25" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC25"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC25" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC25" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC25"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC25" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC25" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD25" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD25" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD25" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD25" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD25"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD25" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD25" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD25"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD25" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD25" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE25" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE25" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE25" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE25" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE25"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE25" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE25" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE25"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE25" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE25" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee25" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee25" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee25" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee25" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee25"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint25" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint25" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint25" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint25" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint25"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint25" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint25" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint25" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint25" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint25"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm26" runat="server" Text="課金26" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec26" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec26" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec26" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec26" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec26"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal26" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal26" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal26" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal26" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal26"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt26" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt26" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt26"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt26" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt26" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB26" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB26" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB26" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB26" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB26"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB26" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB26" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB26"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB26" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB26" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC26" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC26" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC26" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC26" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC26"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC26" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC26" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC26"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC26" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC26" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD26" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD26" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD26" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD26" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD26"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD26" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD26" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD26"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD26" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD26" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE26" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE26" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE26" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE26" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE26"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE26" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE26" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE26"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE26" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE26" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee26" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee26" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee26" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee26" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee26"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint26" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint26" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint26" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint26" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint26"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint26" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint26" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint26" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint26" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint26"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm27" runat="server" Text="課金27" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec27" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec27" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec27" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec27" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec27"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal27" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal27" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal27" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal27" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal27"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt27" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt27" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt27"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt27" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt27" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB27" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB27" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB27" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB27" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB27"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB27" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB27" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB27"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB27" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB27" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC27" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC27" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC27" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC27" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC27"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC27" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC27" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC27"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC27" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC27" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD27" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD27" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD27" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD27" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD27"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD27" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD27" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD27"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD27" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD27" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE27" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE27" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE27" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE27" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE27"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE27" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE27" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE27"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE27" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE27" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee27" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee27" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee27" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee27" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee27"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint27" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint27" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint27" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint27" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint27"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint27" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint27" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint27" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint27" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint27"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm28" runat="server" Text="課金28" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec28" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec28" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec28" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec28" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec28"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal28" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal28" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal28" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal28" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal28"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt28" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt28" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt28"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt28" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt28" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB28" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB28" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB28" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB28" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB28"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB28" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB28" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB28"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB28" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB28" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC28" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC28" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC28" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC28" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC28"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC28" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC28" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC28"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC28" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC28" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD28" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD28" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD28" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD28" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD28"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD28" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD28" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD28"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD28" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD28" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE28" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE28" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE28" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE28" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE28"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE28" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE28" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE28"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE28" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE28" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee28" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee28" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee28" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee28" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee28"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint28" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint28" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint28" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint28" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint28"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint28" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint28" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint28" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint28" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint28"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm29" runat="server" Text="課金29" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec29" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec29" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec29" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec29" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec29"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal29" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal29" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal29" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal29" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal29"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt29" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt29" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt29"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt29" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt29" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB29" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB29" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB29" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB29" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB29"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB29" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB29" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB29"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB29" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB29" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC29" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC29" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC29" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC29" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC29"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC29" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC29" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC29"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC29" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC29" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD29" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD29" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD29" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD29" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD29"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD29" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD29" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD29"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD29" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD29" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE29" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE29" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE29" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE29" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE29"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE29" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE29" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE29"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE29" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE29" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee29" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee29" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee29" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee29" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee29"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint29" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint29" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint29" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint29" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint29"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint29" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint29" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint29" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint29" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint29"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm30" runat="server" Text="課金30" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec30" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec30" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec30" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec30" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec30"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal30" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal30" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal30" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal30" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal30"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt30" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt30" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt30"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt30" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt30" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB30" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB30" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB30" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB30" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB30"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB30" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB30" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB30"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB30" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB30" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC30" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC30" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC30" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC30" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC30"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC30" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC30" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC30"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC30" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC30" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD30" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD30" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD30" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD30" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD30"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD30" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD30" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD30"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD30" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD30" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE30" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE30" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE30" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE30" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE30"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE30" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE30" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE30"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE30" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE30" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee30" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee30" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee30" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee30" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee30"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint30" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint30" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint30" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint30" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint30"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint30" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint30" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint30" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint30" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint30"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm31" runat="server" Text="課金31" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec31" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec31" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec31" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec31" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec31"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal31" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal31" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal31" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal31" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal31"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt31" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt31" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt31"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt31" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt31" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB31" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB31" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB31" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB31" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB31"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB31" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB31" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB31"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB31" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB31" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC31" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC31" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC31" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC31" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC31"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC31" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC31" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC31"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC31" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC31" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD31" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD31" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD31" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD31" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD31"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD31" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD31" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD31"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD31" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD31" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE31" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE31" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE31" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE31" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE31"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE31" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE31" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE31"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE31" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE31" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee31" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee31" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee31" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee31" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee31"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint31" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint31" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint31" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint31" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint31"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint31" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint31" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint31" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint31" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint31"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm32" runat="server" Text="課金32" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec32" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec32" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec32" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec32" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec32"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal32" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal32" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal32" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal32" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal32"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt32" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt32" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt32"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt32" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt32" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB32" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB32" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB32" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB32" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB32"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB32" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB32" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB32"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB32" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB32" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC32" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC32" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC32" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC32" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC32"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC32" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC32" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC32"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC32" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC32" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD32" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD32" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD32" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD32" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD32"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD32" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD32" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD32"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD32" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD32" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE32" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE32" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE32" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE32" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE32"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE32" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE32" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE32"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE32" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE32" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee32" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee32" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee32" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee32" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee32"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint32" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint32" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint32" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint32" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint32"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint32" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint32" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint32" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint32" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint32"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm33" runat="server" Text="課金33" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec33" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec33" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec33" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec33" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec33"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal33" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal33" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal33" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal33" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal33"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt33" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt33" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt33"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt33" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt33" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB33" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB33" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB33" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB33" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB33"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB33" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB33" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB33"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB33" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB33" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC33" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC33" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC33" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC33" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC33"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC33" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC33" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC33"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC33" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC33" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD33" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD33" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD33" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD33" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD33"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD33" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD33" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD33"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD33" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD33" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE33" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE33" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE33" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE33" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE33"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE33" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE33" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE33"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE33" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE33" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee33" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee33" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee33" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee33" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee33"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint33" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint33" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint33" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint33" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint33"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint33" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint33" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint33" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint33" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint33"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderWidthDynamicStyle" nowrap="nowrap">
										<asp:Label ID="lblChargeTypeNm34" runat="server" Text="課金34" Width="150px"></asp:Label>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargeUnitSec34" runat="server" MaxLength="3" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargeUnitSec34" runat="server" ErrorMessage="単位を入力して下さい。" ControlToValidate="txtChargeUnitSec34" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargeUnitSec34" runat="server" ErrorMessage="単位は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeUnitSec34"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormal34" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormal34" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormal34" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormal34" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormal34"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceipt34" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceipt34" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceipt34"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceipt34" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceipt34" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalB34" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalB34" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalB34" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalB34" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalB34"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptB34" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptB34" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptB34"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptB34" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptB34" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalC34" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalC34" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalC34" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalC34" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalC34"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptC34" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptC34" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptC34"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptC34" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptC34" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalD34" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalD34" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalD34" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalD34" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalD34"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptD34" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptD34" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptD34"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptD34" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptD34" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNormalE34" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNormalE34" runat="server" ErrorMessage="通常会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNormalE34" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNormalE34" runat="server" ErrorMessage="通常会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtChargePointNormalE34"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtChargePointNoReceiptE34" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrChargePointNoReceiptE34" runat="server" ErrorMessage="未入金会員ポイントを入力して下さい。" ControlToValidate="txtChargePointNoReceiptE34"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeChargePointNoReceiptE34" runat="server" ErrorMessage="未入金会員ポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}"
											ControlToValidate="txtChargePointNoReceiptE34" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtFreeDialFee34" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrFreeDialFee34" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtFreeDialFee34" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeFreeDialFee34" runat="server" ErrorMessage="ﾌﾘｰﾀﾞｲｱﾙ減算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtFreeDialFee34"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCastUseTalkAppAddPoint34" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrCastUseTalkAppAddPoint34" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtCastUseTalkAppAddPoint34" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeCastUseTalkAppAddPoint34" runat="server" ErrorMessage="出演者通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtCastUseTalkAppAddPoint34"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtManUseTalkAppAddPoint34" runat="server" MaxLength="5" Width="30px"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrManUseTalkAppAddPoint34" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtManUseTalkAppAddPoint34" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeManUseTalkAppAddPoint34" runat="server" ErrorMessage="男性通話ｱﾌﾟﾘ利用時加算ﾎﾟｲﾝﾄは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtManUseTalkAppAddPoint34"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
							</table>
						</asp:PlaceHolder>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>
				<%= DisplayWordUtil.Replace("[出演者一覧]") %>
			</legend>
			<table border="0" style="width: 850px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle2" style="height: 24px">
						サイトコード
					</td>
					<td class="tdDataStyle" style="height: 24px">
						<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
						</asp:DropDownList>
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" CausesValidation="False" OnClick="btnCSV_Click" />
			<br />
			<br />
			<asp:GridView ID="grdCastCharge" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCastCharge" AllowSorting="True" SkinID="GridViewColor"
				Width="950px" OnDataBound="grdCastCharge_DataBound">
				<Columns>
					<asp:TemplateField HeaderText="カテゴリー">
						<ItemTemplate>
							<asp:HyperLink ID="lnkCategory" runat="server" Text='<%# Eval("ACT_CATEGORY_NM")%>' NavigateUrl='<%# string.Format("~/Site/ActCategoryList.aspx?sitecd={0}",Eval("SITE_CD"))%>'></asp:HyperLink>
						</ItemTemplate>
						<ItemStyle Width="150px" />
						<HeaderStyle VerticalAlign="Bottom" />
					</asp:TemplateField>
					<asp:TemplateField HeaderText="コーナー">
						<ItemTemplate>
							<asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Eval("CHARGE_TYPE_NM")%>' NavigateUrl='<%# string.Format("~/Site/CastChargeList.aspx?sitecd={0}&sitenm={1}&actcategoryseq={2}&actcategorynm={3}",Eval("SITE_CD"),Eval("SITE_NM"),Eval("ACT_CATEGORY_SEQ"),Eval("ACT_CATEGORY_NM"))%>'></asp:HyperLink>
						</ItemTemplate>
						<ItemStyle Width="200px" />
						<HeaderStyle VerticalAlign="Bottom" />
					</asp:TemplateField>
					<asp:BoundField DataField="CHARGE_UNIT_SEC" HeaderText="単位">
						<ItemStyle HorizontalAlign="Right" Width="40px" />
						<HeaderStyle VerticalAlign="Bottom" />
					</asp:BoundField>
					<asp:BoundField DataField="CHARGE_POINT_NORMAL" HeaderText="通常">
						<ItemStyle HorizontalAlign="Right" Width="40px" />
					</asp:BoundField>
					<asp:BoundField DataField="CHARGE_POINT_NO_RECEIPT" HeaderText="未入金">
						<ItemStyle HorizontalAlign="Right" Width="50px" />
					</asp:BoundField>
					<asp:BoundField DataField="CHARGE_POINT_NORMAL2" HeaderText="通常">
						<ItemStyle HorizontalAlign="Right" Width="40px" />
					</asp:BoundField>
					<asp:BoundField DataField="CHARGE_POINT_NO_RECEIPT2" HeaderText="未入金">
						<ItemStyle HorizontalAlign="Right" Width="50px" />
					</asp:BoundField>
					<asp:BoundField DataField="CHARGE_POINT_NORMAL3" HeaderText="通常">
						<ItemStyle HorizontalAlign="Right" Width="40px" />
					</asp:BoundField>
					<asp:BoundField DataField="CHARGE_POINT_NO_RECEIPT3" HeaderText="未入金">
						<ItemStyle HorizontalAlign="Right" Width="50px" />
					</asp:BoundField>
					<asp:BoundField DataField="CHARGE_POINT_NORMAL4" HeaderText="通常">
						<ItemStyle HorizontalAlign="Right" Width="40px" />
					</asp:BoundField>
					<asp:BoundField DataField="CHARGE_POINT_NO_RECEIPT4" HeaderText="未入金">
						<ItemStyle HorizontalAlign="Right" Width="50px" />
					</asp:BoundField>
					<asp:BoundField DataField="CHARGE_POINT_NORMAL5" HeaderText="通常">
						<ItemStyle HorizontalAlign="Right" Width="40px" />
					</asp:BoundField>
					<asp:BoundField DataField="CHARGE_POINT_NO_RECEIPT5" HeaderText="未入金">
						<ItemStyle HorizontalAlign="Right" Width="50px" />
					</asp:BoundField>
					<asp:BoundField DataField="FREE_DIAL_FEE" HeaderText="減算金額">
						<ItemStyle HorizontalAlign="Right" Width="50px" />
					</asp:BoundField>
					<asp:BoundField DataField="CAST_USE_TALK_APP_ADD_POINT" HeaderText="出演者利用" HeaderStyle-Wrap="false">
						<ItemStyle HorizontalAlign="Right" Width="50px" />
					</asp:BoundField>
					<asp:BoundField DataField="MAN_USE_TALK_APP_ADD_POINT" HeaderText="男性利用" HeaderStyle-Wrap="false">
						<ItemStyle HorizontalAlign="Right" Width="50px" />
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br />
				<a class="reccount">Current viewing page
					<%=grdCastCharge.PageIndex + 1%>
					of
					<%=grdCastCharge.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsActCategory" runat="server" SelectMethod="GetList" TypeName="ActCategory">
		<SelectParameters>
			<asp:QueryStringParameter Name="pSiteCd" QueryStringField="sitecd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastCharge" runat="server" SelectMethod="GetPageCollection" TypeName="CastCharge" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsCastCharge_Selected" OnSelecting="dsCastCharge_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
