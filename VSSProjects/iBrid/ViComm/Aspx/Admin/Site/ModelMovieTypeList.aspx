﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ModelMovieTypeList.aspx.cs" Inherits="Site_ModelMovieTypeList"
 Title="携帯機種別対応動画ファイル種別管理" ValidateRequest="false"  %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
    &nbsp;<asp:Label ID="lblPgmTitle" runat="server" Text="携帯機種別対応動画ファイル種別管理"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" Runat="Server">
	<div class="admincontent">
        <asp:Panel ID="pnlMainte" runat="server">
            <fieldset class="fieldset">
    		    <legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
					    <legend>[携帯機種内容]</legend>
                        <table id="TABLE1" border="0" class="tableStyle" style="width: 640px" onclick="return TABLE1_onclick()">
                            <tr>
                                <td class="tdHeaderStyle">
                                    キャリア</td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstCarrier" runat="server" AutoPostBack="True"
                                    Width="180px" DataSourceID="dsUseTerminalType" DataTextField="CODE_NM" DataValueField="CODE">
                                    </asp:DropDownList>
                                    <asp:Label ID="lblModeMovieTypeSeq" runat="server" Text="Label" Visible="False"></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    用途</td>
                                <td class="tdDataStyle">
                                    <asp:RadioButton ID="rdoTargetUseTypeDownload" runat="server" Text="ﾀﾞｳﾝﾛｰﾄﾞ" Checked="True" GroupName="GroupTargetUseType" />&nbsp;
                                    <asp:RadioButton ID="rdoTargetUseTypeStreaming" runat="server" Text="ｽﾄﾘｰﾐﾝｸﾞ" GroupName="GroupTargetUseType" /></td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle" style="height: 24px">
                                    機種</td>
                                <td class="tdDataStyle" style="height: 24px">
                                    <asp:TextBox ID="txtMobileModel" runat="server" MaxLength="16" Width="118px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vdrMobileModel" runat="server" ControlToValidate="txtMobileModel"
                                        ErrorMessage="機種を入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
                                    </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle2">
                                    ファイル種別</td>
                                <td class="tdDataStyle"><asp:DropDownList ID="lstMovieFileType" runat="server" Width="180px" DataSourceID="dsMovieFileType" DataTextField="MOVIE_FILE_TYPE_NM" DataValueField="MOVIE_FILE_TYPE">
                                </asp:DropDownList></td>
                            </tr>
                        </table>
                        <asp:Button ID="btnUpdate" runat="server" CssClass="seekbutton" Text="更新" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
                        <asp:Button ID="btnDelete" runat="server" CssClass="seekbutton" Text="削除" OnClick="btnDelete_Click" ValidationGroup="delete" />
                        <asp:Button ID="btnCancel" runat="server" CssClass="seekbutton" OnClick="btnCancel_Click" Text="キャンセル" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
        	</fieldset>
        </asp:Panel>
		<fieldset>
		    <legend>[携帯機種一覧]</legend>
            <asp:GridView ID="grdModelMovieType" runat="server" AllowPaging="True" AllowSorting="True"
                AutoGenerateColumns="False" DataSourceID="dsModelMovieType"
                SkinID="GridViewColor">
                <Columns>
                    <asp:BoundField DataField="MOBILE_CARRIER_NM" HeaderText="キャリア" />
                    <asp:BoundField DataField="TARGET_USE_TYPE_NM" HeaderText="用途" />
                    <asp:TemplateField HeaderText="機種">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("MOBILE_MODEL") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            &nbsp;<asp:LinkButton ID="lnkMobileModel" runat="server" CommandArgument='<%# string.Format("{0}",Eval("MODE_MOVIE_TYPE_SEQ")) %>'
                                OnCommand="lnkMobileModel_Command" Text='<%# Eval("MOBILE_MODEL") %>'></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="MOVIE_FILE_TYPE_NM" HeaderText="ファイル種別" />
                </Columns>
                <PagerSettings Mode="NumericFirstLast" />
            </asp:GridView>
            <asp:Panel ID="pnlCount" runat="server">
                <a class="reccount">Record Count
                    <%#GetRecCount() %>
                </a>
                <br />
                <a class="reccount">Current viewing page
                    <%=grdModelMovieType.PageIndex + 1%>
                    of
                    <%=grdModelMovieType.PageCount%>
                </a>
            </asp:Panel>
   			<div class="button">
                <asp:Button ID="btnRegister" runat="server" CssClass="seekbutton" OnClick="btnRegister_Click"
                    Text="新規追加" />
            </div>
		</fieldset>
	</div>
    <asp:ObjectDataSource ID="dsModelMovieType" runat="server" SelectMethod="GetPageCollection"
        TypeName="ModelMovieType" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelected="dsModelMovieType_Selected"> </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsMovieFileType" runat="server" SelectMethod="GetList"
        TypeName="MovieFileType"></asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsUseTerminalType" runat="server" SelectMethod="GetList"
        TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="73" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrMobileModel" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？"
		ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？"
		ConfirmOnFormSubmit="true" />
</asp:Content>

