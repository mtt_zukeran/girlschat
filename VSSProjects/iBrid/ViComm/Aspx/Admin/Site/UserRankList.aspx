<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UserRankList.aspx.cs" Inherits="Site_UserRankList" Title="ユーザーランク設定"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="ユーザーランク設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									サイトコード
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
									</asp:DropDownList>
								</td>
								<td class="tdHeaderStyle2">
									ユーザーランク
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUserRank" runat="server" MaxLength="1" Width="20px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrUserRank" runat="server" ErrorMessage="ユーザーランクを入力して下さい。" ControlToValidate="txtUserRank" ValidationGroup="Key">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeUserRank" runat="server" ErrorMessage="ユーザーランクは1桁の英字大文字A〜Hで入力して下さい。" ValidationExpression="[A-H]" ControlToValidate="txtUserRank"
										ValidationGroup="Key">*</asp:RegularExpressionValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[ユーザーランク内容]</legend>
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									ユーザーランク名称
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUserRankNm" runat="server" MaxLength="30" Width="200px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrUserRankNm" runat="server" ErrorMessage="ユーザーランク名称を入力して下さい。" ControlToValidate="txtUserRankNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									入金累計額
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtTotalReceiptAmt" runat="server" MaxLength="8" Width="60px"></asp:TextBox>&nbsp;
									<asp:RequiredFieldValidator ID="vdrTotalReceiptAmt" runat="server" ErrorMessage="入金累計額を入力して下さい。" ControlToValidate="txtTotalReceiptAmt" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾎﾟｲﾝﾄｱﾌﾘ専用ﾗﾝｸ
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkPointAffiliateRankFlag" runat="server" Text="入金累計額が0円でもﾎﾟｲﾝﾄｱﾌﾘ入金額が0より多きい場合、このﾗﾝｸを使用する">
									</asp:CheckBox><asp:CustomValidator ID="vdcPointAffiliateUserRankFlag" runat="server" ErrorMessage="" ValidationGroup="Detail"
										OnServerValidate="vdcPointAffiliateUserRankFlag_ServerValidate"></asp:CustomValidator>
								</td>
							</tr>
							<asp:PlaceHolder runat="server" ID="plcLimitPoint">
								<tr>
									<td class="tdHeaderStyle">
										後払利用限度ﾎﾟｲﾝﾄ
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtLimitPoint" runat="server" MaxLength="6" Width="40px"></asp:TextBox>&nbsp;
										<asp:RequiredFieldValidator ID="vdrLimitPoint" runat="server" ErrorMessage="後払利用限度を入力して下さい。" ControlToValidate="txtLimitPoint" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
								</tr>
							</asp:PlaceHolder>
							<tr>
								<td class="tdHeaderStyle">
									カラーパターン
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstWebFaceSeq" runat="server" Width="206px" DataSourceID="dsWebFace" DataTextField="HTML_FACE_NAME" DataValueField="WEB_FACE_SEQ">
									</asp:DropDownList>
								</td>
							</tr>
							<asp:PlaceHolder runat="server" ID="plcIntroPointRate">
								<tr>
									<td class="tdHeaderStyle">
										友達紹介ｷｯｸﾊﾞｯｸ率
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtIntroPointRate" runat="server" MaxLength="3" Width="30px"></asp:TextBox>％&nbsp;
										<asp:RequiredFieldValidator ID="vdrIntroPointRate" runat="server" ErrorMessage="友達紹介ｷｯｸﾊﾞｯｸ率を入力して下さい。" ControlToValidate="txtIntroPointRate" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
								</tr>
							</asp:PlaceHolder>
							<asp:PlaceHolder runat="server" ID="plcUserRankKeepAmt">
								<tr>
									<td class="tdHeaderStyle">
										ﾕｰｻﾞｰﾗﾝｸｷｰﾌﾟ金額
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtUserRankKeepAmt" runat="server" MaxLength="8" Width="60px"></asp:TextBox>&nbsp;
										<asp:RequiredFieldValidator ID="vdrUserRankKeepAmt" runat="server" ErrorMessage="ﾕｰｻﾞｰﾗﾝｸｷｰﾌﾟ金額を入力して下さい。" ControlToValidate="txtUserRankKeepAmt" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
								</tr>
							</asp:PlaceHolder>
							<tr>
								<td class="tdHeaderStyle">
									BINGOﾎﾞｰﾙ出現率
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtBingoBallRate" runat="server" MaxLength="4" Width="40px"></asp:TextBox>％&nbsp;小数点第1位まで入力可
									<asp:CustomValidator ID="vdcBingoBallRate" runat="server" ErrorMessage="" ValidationGroup="Detail" OnServerValidate="vdcBingoBallRate_ServerValidate"></asp:CustomValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[ユーザーランク一覧]</legend>
			<table border="0" style="width: 600px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle2">
						サイトコード
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
						</asp:DropDownList>
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnRegist" Text="ランク追加" CssClass="seekbutton" CausesValidation="False" OnClick="btnRegist_Click" />
			<br />
			<br />
			<asp:GridView ID="grdUserRank" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsUserRank" AllowSorting="True" SkinID="GridViewColor">
				<Columns>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkUserRank" runat="server" Text='<%# string.Format("{0} {1}",Eval("USER_RANK"),Eval("USER_RANK_NM")) %>' CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("USER_RANK"))  %>'
								OnCommand="lnkUserRank_Command" CausesValidation="False">
							</asp:LinkButton>
						</ItemTemplate>
						<HeaderTemplate>
							ﾕｰｻﾞｰﾗﾝｸ
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:BoundField DataField="TOTAL_RECEIPT_AMT" HeaderText="入金累計額">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:BoundField DataField="LIMIT_POINT" HeaderText="利用限度ﾎﾟｲﾝﾄ">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:BoundField DataField="HTML_FACE_NAME" HeaderText="カラーパターン">
						<ItemStyle HorizontalAlign="Left" />
					</asp:BoundField>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:Label ID="lblBingoBallRate" runat="server" Text='<%# string.Format("{0}%",Eval("BINGO_BALL_RATE")) %>'></asp:Label>
						</ItemTemplate>
						<HeaderTemplate>
							BINGOﾎﾞｰﾙ<br>
							出現率
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Right" />
					</asp:TemplateField>
					<asp:BoundField DataField="UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:HyperLink ID="lnkUserCharge" runat="server" NavigateUrl='<%# string.Format("~/Site/UserChargeList.aspx?sitecd={0}&userrank={1}&sitenm={2}&userranknm={3}",Eval("SITE_CD"),Eval("USER_RANK"),Eval("SITE_NM"),Eval("USER_RANK_NM"))%>'>課金設定</asp:HyperLink>
                            <asp:HyperLink ID="lnkUserChargeAdd" runat="server" NavigateUrl='<%# string.Format("~/Site/UserChargeAddConditionList.aspx?sitecd={0}&userrank={1}&sitenm={2}&userranknm={3}",Eval("SITE_CD"),Eval("USER_RANK"),Eval("SITE_NM"),Eval("USER_RANK_NM"))%>' Visible='<%# IsEnabledExpandedCharge() %>'>追加課金設定</asp:HyperLink>
						</ItemTemplate>
						<HeaderTemplate>
							課金設定
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Center" />
					</asp:TemplateField>
                    <asp:TemplateField>
						<ItemTemplate>
							<asp:HyperLink ID="lnkReceiptService" runat="server" Visible='<%# Eval("USE_OTHER_SYS_INFO_FLAG").ToString().Equals("0") %>' NavigateUrl='<%# string.Format("~/Site/ReceiptServicePointList.aspx?sitecd={0}&userrank={1}&sitenm={2}&userranknm={3}",Eval("SITE_CD"),Eval("USER_RANK"),Eval("SITE_NM"),Eval("USER_RANK_NM"))%>'>設定</asp:HyperLink>
						</ItemTemplate>
						<HeaderTemplate>
							入金ｻｰﾋﾞｽ<br>
							ﾎﾟｲﾝﾄ設定
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Center" />
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:Label ID="lblIntroPointRate" runat="server" Text='<%# string.Format("{0}%",Eval("INTRO_POINT_RATE"))%>'></asp:Label>
						</ItemTemplate>
						<HeaderTemplate>
							友達紹介<br>
							ｷｯｸﾊﾞｯｸ率
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Right" />
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:Label ID="lblUserRankKeepAmt" runat="server" Text='<%# Eval("USER_RANK_KEEP_AMT")%>'></asp:Label>
						</ItemTemplate>
						<HeaderTemplate>
							ﾕｰｻﾞｰﾗﾝｸ<br>
							ｷｰﾌﾟ金額
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Right" />
					</asp:TemplateField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br />
				<a class="reccount">Current viewing page
					<%=grdUserRank.PageIndex + 1%>
					of
					<%=grdUserRank.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsUserRank" runat="server" SelectMethod="GetPageCollection" TypeName="UserRank" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsUserRank_Selected" OnSelecting="dsUserRank_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsWebFace" runat="server" SelectMethod="GetList" TypeName="WebFace"></asp:ObjectDataSource>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtTotalReceiptAmt" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtLimitPoint" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtIntroPointRate" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrUserRank" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeUserRank" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrUserRankNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdrTotalReceiptAmt" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdrLimitPoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdrIntroPointRate" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vdrUserRankKeepAmt" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
