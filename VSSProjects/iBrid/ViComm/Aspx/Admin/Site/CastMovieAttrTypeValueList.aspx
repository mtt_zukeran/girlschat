<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastMovieAttrTypeValueList.aspx.cs" Inherits="Site_CastMovieAttrTypeValueList"
	Title="出演者動画属性値設定" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="出演者動画属性値設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									サイトコード
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD">
									</asp:DropDownList>
								</td>
								<td class="tdHeaderStyle2">
									属性区分
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstCastMovieAttrTypeSeq" runat="server" DataSourceID="dsCastMovieAttrType" DataTextField="CAST_MOVIE_ATTR_TYPE_NM" DataValueField="CAST_MOVIE_ATTR_TYPE_SEQ"
										AutoPostBack="true" OnSelectedIndexChanged="lstCastMovieAttrTypeSeq_SelectedIndexChanged">
									</asp:DropDownList>
									<asp:Label ID="lblCastMovieAttrSeq" runat="server" Text="" Visible="false"></asp:Label>
								</td>
							</tr>
						</table>
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[属性値内容]</legend>
						<table border="0" style="width: 640px; margin-bottom: 4px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									属性値
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCastMovieAttrNm" runat="server" MaxLength="30" Width="200px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrCastMovieAttrNm" runat="server" ErrorMessage="属性値を入力して下さい。" ControlToValidate="txtCastMovieAttrNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									表示順位
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtPriority" runat="server" MaxLength="3" Width="60px"></asp:TextBox>&nbsp;
									<asp:RequiredFieldValidator ID="vdrPriority" runat="server" ErrorMessage="優先順位を入力して下さい。" ControlToValidate="txtPriority" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
						</table>
						<table border="0" style="width: 640px; margin-bottom: 4px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									投稿者報酬区分
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkUseIndividualPayFlag" runat="server" Text="下記個別設定を使用する" />
								</td>
							</tr>
							<tr id="trPostPayAmt" runat="server" visible="false">
								<td class="tdHeaderStyle">
									投稿時報酬金額
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtPostPayAmt" runat="server" MaxLength="5" Width="60px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrPostPayAmt" runat="server" ErrorMessage="投稿時報酬金額入力して下さい。" ControlToValidate="txtPostPayAmt" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									閲覧時報酬金額
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtViewPayAmt" runat="server" MaxLength="5" Width="60px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrViewPayAmt" runat="server" ControlToValidate="txtViewPayAmt" ErrorMessage="投稿時報酬金額を入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									投稿者削除不可
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkPosterDelNaFlag" runat="server" Text="投稿者による動画の削除を許可しない" />
								</td>
							</tr>
						</table>
						<table border="0" style="width: 640px; margin-bottom: 4px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									閲覧者課金区分
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkUseIndividualChargeFlag" runat="server" Text="下記個別設定を使用する" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									閲覧時課金Pt
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtViewerChargePoint" runat="server" MaxLength="3" Width="60px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrViewerChargePoint" runat="server" ControlToValidate="txtViewerChargePoint" ErrorMessage="投稿時報酬Ptを入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
						</table>
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									ｱｯﾌﾟﾛｰﾄﾞ上限数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUploadLimitCount" runat="server" MaxLength="5" Width="60px"></asp:TextBox>
									<asp:Label ID="Label1" runat="server" Text="※0を指定した場合、無制限" ForeColor="red"></asp:Label>
									<asp:RequiredFieldValidator ID="vdrUploadLimitCount" runat="server" ControlToValidate="txtUploadLimitCount" ErrorMessage="ｱｯﾌﾟﾛｰﾄﾞ上限数を入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[属性値一覧]</legend>
			<table border="0" style="width: 640px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle2">
						サイトコード
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
						</asp:DropDownList>
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnRegist" Text="属性値追加" CssClass="seekbutton" CausesValidation="False" OnClick="btnRegist_Click" />
			<br />
			<br />
			<asp:Label ID="lblUploadLimit" runat="server" Text="※ｱｯﾌﾟﾛｰﾄﾞ上限数：0 は無制限"></asp:Label>
			<asp:GridView ID="grdValue" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCastMovieAttrTypeValue" AllowSorting="True" SkinID="GridView"
				OnDataBound="grdValue_DataBound">
				<Columns>
					<asp:TemplateField HeaderText="属性区分">
						<ItemTemplate>
							<asp:Label ID="lblCastMovieAttrTypeNm" runat="server" Text='<%# Bind("CAST_MOVIE_ATTR_TYPE_NM") %>'></asp:Label>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:TemplateField HeaderText="属性値No.">
						<ItemTemplate>
							<%# Eval("CAST_MOVIE_ATTR_SEQ") %>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:BoundField DataField="PRIORITY" HeaderText="表示順位">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:HyperLink ID="lnkCastMovieAttrSeq" runat="server" NavigateUrl='<%# string.Format("~/Site/CastMovieAttrTypeValueList.aspx?site={0}&castmovieattrtypeseq={1}&castmovieattrseq={2}",Eval("SITE_CD"),Eval("CAST_MOVIE_ATTR_TYPE_SEQ"),Eval("CAST_MOVIE_ATTR_SEQ"))%>'><%#Eval("CAST_MOVIE_ATTR_NM")%></asp:HyperLink>
						</ItemTemplate>
						<HeaderTemplate>
							属性区分名
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:TemplateField HeaderText="投稿者報酬個別設定">
						<ItemTemplate>
							<%# ViComm.ViCommConst.FLAG_ON_STR.Equals(Eval("USE_INDIVIDUAL_PAY_FLAG","{0}")) ? "○" : string.Empty %>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Center" />
					</asp:TemplateField>
					<asp:BoundField DataField="POST_PAY_AMT" HeaderText="投稿時報酬金額" Visible="false">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:BoundField DataField="VIEW_PAY_AMT" HeaderText="閲覧時報酬金額">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:TemplateField HeaderText="投稿者削除不可">
						<ItemTemplate>
							<%# ViComm.ViCommConst.FLAG_ON_STR.Equals(Eval("POSTER_DEL_NA_FLAG","{0}")) ? "○" : string.Empty %>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Center" />
					</asp:TemplateField>
					<asp:TemplateField HeaderText="閲覧者課金個別設定">
						<ItemTemplate>
							<%# ViComm.ViCommConst.FLAG_ON_STR.Equals(Eval("USE_INDIVIDUAL_CHARGE_FLAG","{0}")) ? "○" : string.Empty %>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Center" />
					</asp:TemplateField>
					<asp:BoundField DataField="VIEWER_CHARGE_POINT" HeaderText="閲覧時課金Pt">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:BoundField DataField="UPLOAD_LIMIT_COUNT" HeaderText="ｱｯﾌﾟﾛｰﾄﾞ上限数">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br />
				<a class="reccount">Current viewing page
					<%=grdValue.PageIndex + 1%>
					of
					<%=grdValue.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsCastMovieAttrTypeValue" runat="server" SelectMethod="GetPageCollection" TypeName="CastMovieAttrTypeValue" SelectCountMethod="GetPageCount"
		EnablePaging="True" OnSelected="dsCastMovieAttrTypeValue_Selected" OnSelecting="dsCastMovieAttrTypeValue_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastMovieAttrType" runat="server" SelectMethod="GetList" TypeName="CastMovieAttrType" OnSelecting="dsCastMovieAttrType_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="Server" Enabled="true" FilterType="Numbers" TargetControlID="txtPriority" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="Server" Enabled="true" FilterType="Numbers" TargetControlID="txtPostPayAmt" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="Server" Enabled="true" FilterType="Numbers" TargetControlID="txtViewPayAmt" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="Server" Enabled="true" FilterType="Numbers" TargetControlID="txtViewerChargePoint" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrCastMovieAttrNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrPriority" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrPostPayAmt" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdrViewPayAmt" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdrViewerChargePoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行いますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
