<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VariableSample.aspx.cs" Inherits="Site_VariableSample" Title="変数表" ValidateRequest="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>変数表</title>
</head>
<body>
	<form id="form1" runat="server">
		<div>
			<div id="header">
				<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnableScriptGlobalization="True" EnableScriptLocalization="True">
				</ajaxToolkit:ToolkitScriptManager>
			</div>
			<div id="title">
				<asp:Label ID="lblPgmTitle" runat="server" Text="変数表"></asp:Label>
			</div>
			<div id="contentright">
				<div style="padding-top:5px;" style="width:auto;">
					<asp:Table ID="tblLink" runat="server" CssClass="DataWebControlStyle" CellSpacing="0" CellPadding="20" rules="all" Border="0" Font-Size="X-Small" Width="990px">
					</asp:Table>
					<div style="height: 830px; width: auto; overflow: auto;">
						<asp:Table ID="tblVariable" runat="server" CssClass="DataWebControlStyle" CellSpacing="0" CellPadding="10" rules="all" Border="1" Font-Size="X-Small">
						</asp:Table>
					</div>
				</div>
			</div>
			<div id="footer" style="width:auto;">
				<p>
					&copy;
					<asp:Label ID="lblCopyrightYear" runat="server" Text="2008" />
					<asp:Label ID="lblCopyrightName" runat="server" Text="Powerd by" EnableViewState="false" />
					<a href="http://www.ibrid.co.jp" target="_top">iBrid Co. Ltd.</a>
				</p>
			</div>
		</div>
	</form>
</body>
</html>
