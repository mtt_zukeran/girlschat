﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ユーザー定義ポイント
--	Progaram ID		: ManagerList
--
--  Creation Date	: 2011.03.30
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using iBridCommLib;
using ViComm;


public partial class Site_ListUserDefPoint:System.Web.UI.Page {
	private string sRecCount;
	private readonly Regex oAddPointRegex = new System.Text.RegularExpressions.Regex("[\\-\\+][0-9]+");
	protected void Page_Load(object sender,EventArgs e) {

		if (!IsPostBack) {
		
			lstSiteCd.DataBind();
			if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
				lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}
			lstSiteCd.DataSourceID = string.Empty;
			InitPage();
			DataBind();
		}
	}
	protected void InitPage(){
		ClearField();
		pnlMainte.Visible = false;
		pnlDtl.Visible = true;
		pnlKey.Enabled = true;
	}

	protected void GetData() {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("USER_DEF_POINT_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstKeySiteCd.SelectedValue);
			db.ProcedureInParm("PADD_POINT_ID",DbSession.DbType.VARCHAR2,txtKeyAddPointId.Text);
			db.ProcedureOutParm("PADD_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PAPPLY_COUNT_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PAPPLY_START_TIME",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PAPPLY_END_TIME",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREMARKS",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pDEF_POINT_NM", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PNA_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PPREINSTALL_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["PREINSTALL_FLAG"] = db.GetStringValue("PPREINSTALL_FLAG");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
			
				string[] oApplyStartTime = GetApplyTimeAry(db,"PAPPLY_START_TIME");
				string[] oApplyEndTime = GetApplyTimeAry(db,"PAPPLY_END_TIME");
			
				txtAddPoint.Text = db.GetIntValue("PADD_POINT").ToString();
				lstApplyCountType.SelectedValue = db.GetStringValue("PAPPLY_COUNT_TYPE");
				lstApplyStartHH.SelectedValue = oApplyStartTime[0];
				lstApplyStartMM.SelectedValue = oApplyStartTime[1];
				lstApplyEndHH.SelectedValue = oApplyEndTime[0];
				lstApplyEndMM.SelectedValue = oApplyEndTime[1];
				txtRemarks.Text = db.GetStringValue("PREMARKS");
				txtDefPointNm.Text = db.GetStringValue("pDEF_POINT_NM");
				chkNaFlag.Checked = db.GetIntValue("PNA_FLAG").Equals(ViCommConst.FLAG_ON);

			} else {
				string sBuffer = txtKeyAddPointId.Text;
				ClearField();
				txtKeyAddPointId.Text = sBuffer;
			}

			lstApplyCountType.DataBind();
			if (ViewState["PREINSTALL_FLAG"].ToString().Equals(ViCommConst.FLAG_ON_STR)) {
				lstApplyCountType.Enabled = false;
				lstApplyStartHH.Enabled = false;
				lstApplyStartMM.Enabled = false;
				lstApplyEndHH.Enabled = false;
				lstApplyEndMM.Enabled = false;
				btnDelete.Visible = false;
				txtDefPointNm.Enabled = false;
			} else {
				lstApplyCountType.Enabled = true;
				lstApplyStartHH.Enabled = true;
				lstApplyStartMM.Enabled = true;
				lstApplyEndHH.Enabled = true;
				lstApplyEndMM.Enabled = true;
				btnDelete.Visible = true;
				lstApplyCountType.Items.RemoveAt(0);
				txtDefPointNm.Enabled = true;
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;

	}
	protected void ClearField(){
		txtKeyAddPointId.Text = string.Empty;
		txtAddPointId.Text = string.Empty;
		txtAddPoint.Text = string.Empty;
		lstApplyCountType.SelectedIndex =0;
		lstApplyStartHH.SelectedIndex = 0;
		lstApplyStartMM.SelectedIndex = 0;
		lstApplyEndHH.SelectedIndex = 0;
		lstApplyEndMM.SelectedIndex = 0;
		txtRemarks.Text = string.Empty;
		txtDefPointNm.Text = string.Empty;
		chkNaFlag.Checked = false;
		sRecCount = "0";
	}
	protected void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			int iAddPoint = ParseAddPoint(txtAddPoint.Text);
			string sApplyStartTime = string.Empty;
			if (lstApplyStartHH.SelectedValue != "null" &&
				lstApplyStartMM.SelectedValue != "null") {
				sApplyStartTime = string.Format("{0}:{1}",lstApplyStartHH.SelectedValue,lstApplyStartMM.SelectedValue);
			}

			string sApplyEndTime = string.Empty;
			if (lstApplyEndHH.SelectedValue != "null" &&
				lstApplyEndMM.SelectedValue != "null") {
				sApplyEndTime = string.Format("{0}:{1}",lstApplyEndHH.SelectedValue,lstApplyEndMM.SelectedValue);
			}
			string sPreInstallFlag = ViewState["PREINSTALL_FLAG"].ToString();
			if(string.IsNullOrEmpty(sPreInstallFlag)){
				sPreInstallFlag = "0";
			}
						
			db.PrepareProcedure("USER_DEF_POINT_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstKeySiteCd.SelectedValue);
			db.ProcedureInParm("PADD_POINT_ID",DbSession.DbType.VARCHAR2,txtKeyAddPointId.Text.TrimEnd());
			db.ProcedureInParm("PADD_POINT",DbSession.DbType.NUMBER,iAddPoint);
			db.ProcedureInParm("PAPPLY_COUNT_TYPE",DbSession.DbType.VARCHAR2,lstApplyCountType.SelectedValue);
			db.ProcedureInParm("PAPPLY_START_TIME",DbSession.DbType.VARCHAR2,sApplyStartTime);
			db.ProcedureInParm("PAPPLY_END_TIME",DbSession.DbType.VARCHAR2,sApplyEndTime);
			db.ProcedureInParm("PREMARKS",DbSession.DbType.VARCHAR2,txtRemarks.Text.TrimEnd());
			db.ProcedureInParm("pDEF_POINT_NM", DbSession.DbType.VARCHAR2, txtDefPointNm.Text.TrimEnd());
			db.ProcedureInParm("PNA_FLAG",DbSession.DbType.NUMBER,(chkNaFlag.Checked)?ViCommConst.FLAG_ON:ViCommConst.FLAG_OFF);
			db.ProcedureInParm("PPREINSTALL_FLAG",DbSession.DbType.NUMBER,decimal.Parse(sPreInstallFlag));
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("pDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		InitPage();
		DataBind();
		
	}
	protected int ParseAddPoint(string pAddPoint){
		if(string.IsNullOrEmpty(pAddPoint)){return 0;}
		
		if(pAddPoint[0].Equals('+')||pAddPoint[0].Equals('-')){

			MatchCollection oTmp = oAddPointRegex.Matches(pAddPoint);
			int iRet = 0;
			for(int i = 0; i < oTmp.Count;i++){
				iRet += int.Parse(oTmp[i].Value);
			}			
			return iRet;
		} else {
			return ParseAddPoint(string.Format("{0}{1}","+",pAddPoint));
		}
	}
	protected string[] GetApplyTimeAry(DbSession pDb,string pColmnNm){
		string sApplyTime = pDb.GetStringValue(pColmnNm);
		string[] oApplyTime = null;
		if (!string.IsNullOrEmpty(sApplyTime)) {
			oApplyTime = sApplyTime.Split(':');
		} else {
			oApplyTime = new string[2];
			oApplyTime[0] = "null";
			oApplyTime[1] = "null";
		}
		
		return oApplyTime;
	}
	protected void dsUserDefPoint_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = txtAddPointId.Text.ToString();
	}
	protected void dsUserDefPoint_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			sRecCount = e.ReturnValue.ToString();
		}
	}
	protected void grdUserDefPoint_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			if(DataBinder.Eval(e.Row.DataItem,"NA_FLAG").ToString().Equals(ViCommConst.FLAG_ON_STR)){
				e.Row.BackColor = System.Drawing.Color.LightGray;
			}
		}
	}
	protected void vdcAddPointMinMax_ServerValidate(object source,ServerValidateEventArgs e){
		int iAddPoint = ParseAddPoint(txtAddPoint.Text);
		e.IsValid = (-10000 < iAddPoint && iAddPoint < 10000);
		
	}
	
	protected void vdcApplyTime_ServerValidate(object sourse,ServerValidateEventArgs e){
		if (!((lstApplyStartHH.SelectedValue.Equals("null") && lstApplyStartMM.SelectedValue.Equals("null")) ||
			(!lstApplyStartHH.SelectedValue.Equals("null") && !lstApplyStartMM.SelectedValue.Equals("null")))) {
			((System.Web.UI.WebControls.BaseValidator)(sourse)).ErrorMessage = "適応日時Fromを正しく入力してください";
			((System.Web.UI.WebControls.BaseValidator)(sourse)).ControlToValidate = "lstApplyEndMM";
			e.IsValid = false;
		} else if (!((lstApplyEndHH.SelectedValue.Equals("null") && lstApplyEndMM.SelectedValue.Equals("null")) ||
				(!lstApplyEndHH.SelectedValue.Equals("null") && !lstApplyEndMM.SelectedValue.Equals("null")))) {
			((System.Web.UI.WebControls.BaseValidator)(sourse)).ErrorMessage = "適応日時Toを正しく入力してください";
			((System.Web.UI.WebControls.BaseValidator)(sourse)).ControlToValidate = "lstApplyEndMM";
			e.IsValid = false;

		} else if (!((lstApplyStartHH.SelectedValue.Equals("null") && lstApplyEndHH.SelectedValue.Equals("null")) ||
			(!lstApplyStartHH.SelectedValue.Equals("null") && !lstApplyEndHH.SelectedValue.Equals("null")))) {
			((System.Web.UI.WebControls.BaseValidator)(sourse)).ErrorMessage = "適応日時を指定する場合は、FromとToの両方を入力してください";
			((System.Web.UI.WebControls.BaseValidator)(sourse)).ControlToValidate = "lstApplyEndMM";
			e.IsValid = false;
		} else {

			if (lstApplyStartHH.SelectedValue.Equals("null")) {
				e.IsValid = true;
				return;
			}
			int iStartTime = (int.Parse(lstApplyStartHH.SelectedValue) * 60) + int.Parse(lstApplyStartMM.SelectedValue);
			int iEndTime = (int.Parse(lstApplyEndHH.SelectedValue) * 60) + int.Parse(lstApplyEndMM.SelectedValue);
			if(!(iStartTime <= iEndTime)){
				((System.Web.UI.WebControls.BaseValidator)(sourse)).ErrorMessage = "大小関係が正しくありません";
				((System.Web.UI.WebControls.BaseValidator)(sourse)).ControlToValidate = "lstApplyEndMM";
				e.IsValid = false;
			} else {
				e.IsValid = true;
			}
		}
		
	}
	
	protected void lnkAddPointId_Command(object sender,CommandEventArgs e){
		string[] sKey = e.CommandArgument.ToString().Split(':');
		lstKeySiteCd.SelectedValue = sKey[0];
		txtKeyAddPointId.Text = sKey[1];
		GetData();
	}
	
	
	protected string CheckNaFlag(object pNaFlag){
		string sRetStr = string.Empty;
		if(pNaFlag.ToString().Equals(ViCommConst.FLAG_ON_STR)){
			sRetStr =  "×";
		}
		return sRetStr;
	}

	protected string GetRecCount() {
		return sRecCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		DataBind();
	}
	
	protected void btnKeySeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnUpdate_Click(object sender, EventArgs e){
		if(IsValid){
			UpdateData(ViCommConst.FLAG_OFF);
		}
	}
	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(ViCommConst.FLAG_ON);
		}
	}
	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}
	protected void btnRegist_Click(object sender,EventArgs e) {

		InitPage();
		pnlMainte.Visible = true;
		pnlKey.Enabled = true;
		pnlDtl.Visible = false;
		lstKeySiteCd.Enabled = false;
		lstKeySiteCd.SelectedIndex = lstSiteCd.SelectedIndex;
	}
}
