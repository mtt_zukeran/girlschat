<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ManagerList.aspx.cs" Inherits="Site_ManagerList"
	Title="担当設定" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="担当設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									プロダクションコード
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstProductionSeq" runat="server" DataSourceID="dsProduction" DataTextField="PRODUCTION_NM" DataValueField="PRODUCTION_SEQ"
										Width="180px">
									</asp:DropDownList>
									<asp:Label ID="lblManagerSeq" runat="server" Text="Label" Visible="false"></asp:Label>
								</td>
							</tr>
						</table>
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[担当内容]</legend>
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									担当名
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtManagerNm" runat="server" MaxLength="50" Width="400px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrManagerNm" runat="server" ErrorMessage="担当名を入力して下さい。" ControlToValidate="txtManagerNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ログインＩＤ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLoginId" runat="server" MaxLength="15" Width="160px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrLoginId" runat="server" ErrorMessage="ログインＩＤを入力して下さい。" ControlToValidate="txtLoginId"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeLoginId" runat="server" ErrorMessage="ログインＩＤは4桁以上の英数字で入力して下さい。" ValidationExpression="(\w|[._-]){4,15}"
										ControlToValidate="txtLoginId" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									<asp:CustomValidator ID="vdcLoginId" runat="server" ErrorMessage="ログインIDが重複しています" OnServerValidate="vdcLoginId_ServerValidate" ValidationGroup="Detail"></asp:CustomValidator></td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									パスワード
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLoginPassword" runat="server" MaxLength="12" Width="160px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrLoginPassword" runat="server" ErrorMessage="パスワードを入力して下さい。" ControlToValidate="txtLoginPassword"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeLoginPassword" runat="server" ErrorMessage="パスワードは4桁以上の英数字で入力して下さい。" ValidationExpression="\w{4,12}"
										ControlToValidate="txtLoginPassword" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									電話番号
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtTel" runat="server" MaxLength="11" Width="110px"></asp:TextBox>
									<asp:RegularExpressionValidator ID="vdeTel" runat="server" ErrorMessage="電話番号をを正しく入力して下さい。" ValidationExpression="(0\d{9,10})" ControlToValidate="txtTel"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									メールアドレス
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtEmailAddr" runat="server" MaxLength="100" Width="200px"></asp:TextBox>
									<asp:RegularExpressionValidator ID="vdeEmailAddr" runat="server" ErrorMessage="メールアドレスをを正しく入力して下さい。" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
										ControlToValidate="txtEmailAddr" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									登録日
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblRegistDay" runat="server" Text="Label"></asp:Label>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[担当一覧]</legend>
			<asp:GridView ID="grdManager" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsManager" AllowSorting="True"
				SkinID="GridViewColor">
				<Columns>
					<asp:BoundField DataField="PRODUCTION_NM" HeaderText="プロダクション"></asp:BoundField>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkManager" runat="server" Text='<%# Eval("MANAGER_NM") %>' CommandArgument='<%# string.Format("{0}:{1}",Eval("PRODUCTION_SEQ"),Eval("MANAGER_SEQ")) %>'
								OnCommand="lnkSupplementCd_Command" CausesValidation="False">
							</asp:LinkButton>
						</ItemTemplate>
						<HeaderTemplate>
							担当
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:BoundField DataField="MANAGE_COUNT" HeaderText="担当数">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:BoundField DataField="TEL" HeaderText="電話番号"></asp:BoundField>
					<asp:BoundField DataField="EMAIL_ADDR" HeaderText="メールアドレス"></asp:BoundField>
					<asp:BoundField DataField="UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<a class="reccount">Record Count
				<%#GetRecCount() %>
			</a>
			<br>
			<a class="reccount">Current viewing page
				<%=grdManager.PageIndex + 1%>
				of
				<%=grdManager.PageCount%>
			</a>
			<div class="button">
				<asp:Button ID="btnRegist" runat="server" Text="担当追加" OnClick="btnRegist_Click" />
			</div>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsManager" runat="server" SelectMethod="GetPageCollection" TypeName="Manager" SelectCountMethod="GetPageCount"
		EnablePaging="True" OnSelected="dsManager_Selected"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsProduction" runat="server" SelectMethod="GetList" TypeName="Production"></asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrManagerNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdeTel" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdeEmailAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdrLoginPassword" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeLoginPassword" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdrLoginId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vdeLoginId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？"
		ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？"
		ConfirmOnFormSubmit="true" />
</asp:Content>
