﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" ValidateRequest="false"
	Title="Amazonギフト券管理" CodeFile="AmazonGiftManage.aspx.cs" Inherits="Site_AmazonGiftManage"
%>
<%@ Import Namespace="System.Globalization" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="Amazonギフト券管理"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlUpload">
			<fieldset class="fieldset">
				<legend>[ファイル指定]</legend>
				<div>Amazonギフト券のアップロードを行います。下記のフォームにCSVファイルを指定してください。</div>
				<div>※上から順番に登録を実行していくため、エラー発生後に再アップロードを行う際にはエラー表示された行から上の行を全て削除したCSVをアップロードしてください。</div>
				<asp:FileUpload ID="uplCsv" runat="server" Width="500px" />
				<asp:RequiredFieldValidator ID="valrUpload" runat="server" ControlToValidate="uplCsv" ErrorMessage="アップロードファイルを入力して下さい。" ValidationGroup="Upload" Text="*"></asp:RequiredFieldValidator>
				<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="valrUpload" HighlightCssClass="validatorCallout" />
				<br />
				<asp:Button ID="btnUpload" runat="server" Text="アップロード" ValidationGroup="Upload" Width="150px" OnClick="btnUpload_Click" />
				<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpload" ConfirmText="アップロードを行ないますか？" ConfirmOnFormSubmit="true" />
				<br />
				<asp:Label ID="lblCompleteMsg" runat="server" Text="<br />アップロード完了しました。"></asp:Label>
				<font color="red"><asp:Label ID="lblErrorMsg" runat="server" Text=""></asp:Label></font>
				<br />
				<asp:Button ID="btnClose" runat="server" Text="閉じる" Width="60px" Height="20px" OnClick="btnClose_Click" />
			</fieldset>
			<br />
		</asp:Panel>

		<%-- ============================== --%>
		<%--  Search Condition              --%>
		<%-- ============================== --%>
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							<asp:Label ID="lblDispDate" runat="server" Text="表示月"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstYYYY" runat="server" Width="60px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstMM" runat="server" Width="40px">
							</asp:DropDownList>月
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							残枚数の表示
						</td>
						<td class="tdDataStyle">
							<asp:RadioButtonList id="rdoRemainingNum" runat="server" RepeatDirection="Horizontal">
								<asp:ListItem Text="一括" Value="1" Selected="True" />
								<asp:ListItem Text="有効期限別" Value="2" />
								<asp:ListItem Text="非表示" Value="3" />
							</asp:RadioButtonList>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="True" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
				<asp:Button runat="server" ID="btnDispUpload" Text="ギフト券登録" CssClass="seekbutton" OnClick="btnDispUpload_Click" />
			</asp:Panel>
		</fieldset>

		<%-- ============================== --%>
		<%--  RemainingData                 --%>
		<%-- ============================== --%>
		<asp:Panel runat="server" ID="pnlRemaining" Visible="false">
			<fieldset class="fieldset">
				<legend>[残枚数]</legend>
				<asp:Panel runat="server" ID="pnlRemainingAll" ScrollBars="None">
					<table border="0" style="width: 200px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle2" style="width: 100px">
								<asp:Label ID="Label1" runat="server" Text="残枚数"></asp:Label>
							</td>
							<td class="tdDataStyle" align="right" style="width: 100px">
								<asp:Label ID="lblRemainingCntAll" runat="server" Text=""></asp:Label>
							</td>
						</tr>
					</table>
				</asp:Panel>
				<asp:Panel ID="pnlRemainingExpiration" runat="server" ScrollBars="None">
					<asp:GridView ID="grdRemainingExpiration" runat="server" ShowFooter="False" AllowSorting="True" AutoGenerateColumns="False"
						DataSourceID="" SkinID="GridViewColor" Font-Size="X-Small" EnableViewState="false">
						<Columns>
							<asp:TemplateField HeaderText="有効期限">
								<ItemStyle HorizontalAlign="Center" />
								<ItemTemplate>
									<asp:Label ID="lblRemainingDate" runat="server" Text='<%# Eval("EXPIRATION_DATE", "{0:yyyy/MM/dd}") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="残枚数">
								<ItemStyle HorizontalAlign="Right" />
								<ItemTemplate>
									<asp:Label ID="lblExpirationCount" runat="server" Text='<%# Eval("CNT") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</asp:Panel>

		<%-- ============================== --%>
		<%--  Data                          --%>
		<%-- ============================== --%>
		<asp:Panel runat="server" ID="pnlList" Visible="false">
			<fieldset class="fieldset">
				<legend>[履歴]</legend>
				<asp:Panel ID="pnlData" runat="server" ScrollBars="None">
					<asp:GridView ID="grdData" runat="server" ShowFooter="True" AllowSorting="True" AutoGenerateColumns="False"
						DataSourceID="" OnRowDataBound="grdData_RowDataBound" SkinID="GridViewColor" Font-Size="X-Small" EnableViewState="false">
						<Columns>
							<asp:TemplateField HeaderText="日付">
								<ItemStyle HorizontalAlign="Center" />
								<ItemTemplate>
									<asp:Label ID="lblPaymentDate" runat="server" Text='<%# Eval("DISPLAY_DAY") %>'></asp:Label>日
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Center" />
								<FooterTemplate></FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="曜日">
								<ItemStyle HorizontalAlign="Center" />
								<ItemTemplate>
									<asp:Label ID="lblPaymentWeek" runat="server" Text='<%# Eval("DISPLAY_DAY_OF_WEEK") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Center" />
								<FooterTemplate>
									合計
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="消費枚数">
								<ItemStyle HorizontalAlign="Right" />
								<ItemTemplate>
									<asp:HyperLink ID="lnkPaymentCnt" runat="server" Text='<%# Eval("PAYMENT_CNT") %>' Width="100%"
										NavigateUrl='<%# string.Format(
											"~/StatusManager/PaymentHistorySummary.aspx?pdate={0}&pmethod={1}"
											,Eval("DISPLAY_DATE", "{0:yyyy/MM/dd}")
											,GiftCode.GiftCodeType.Amazon) %>'
									></asp:HyperLink>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<FooterTemplate></FooterTemplate>
							</asp:TemplateField>
						</Columns>
						<FooterStyle ForeColor="Black" BackColor="LightYellow" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
	</div>

	<%-- ============================== --%>
	<%--  Data Source                   --%>
	<%-- ============================== --%>
	<asp:ObjectDataSource ID="dsGiftCodeRemaining" runat="server" TypeName="GiftCode" SelectMethod="GetRemainingCnt" EnablePaging="True"
		OnSelecting="dsGiftCodeRemaining_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSearchCondition" Type="Object" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsGiftCodeList" runat="server" TypeName="GiftCode" SelectMethod="GetPageCollection" EnablePaging="True"
		OnSelecting="dsGiftCodeList_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSearchCondition" Type="Object" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
