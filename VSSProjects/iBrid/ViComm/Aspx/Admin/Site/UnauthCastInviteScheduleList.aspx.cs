﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 未認証キャスト勧誘スケジュール
--	Progaram ID		: UnauthCastInviteScheduleList
--
--  Creation Date	: 2010.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Site_UnauthCastInviteScheduleList:System.Web.UI.Page {
	private string recCount = "";
	private int MAX_INVITE_COUNT = 5;

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
	}

	private void FirstLoad() {
		grdInviteSchedule.PageSize = 999;
		lblSiteCd.Text = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		using (Site oSite = new Site()) {
			if (oSite.GetOne(lblSiteCd.Text)) {
				lblSiteNm.Text = oSite.siteNm;
			}
		}
		lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		lblInviteLevel.Text = iBridUtil.GetStringValue(Request.QueryString["invitelevel"]);
		DataBind();
		lstMailTemplateNo1.Items.Insert(0,new ListItem("",""));
		lstMailTemplateNo2.Items.Insert(0,new ListItem("",""));
		lstMailTemplateNo3.Items.Insert(0,new ListItem("",""));
		lstMailTemplateNo4.Items.Insert(0,new ListItem("",""));
		lstMailTemplateNo5.Items.Insert(0,new ListItem("",""));
	}

	private void InitPage() {
		pnlDtl.Visible = false;
		ClearField();
		GetData();

		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);

	}

	private void ClearField() {
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetList();
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
		GetList();
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		pnlDtl.Visible = false;
		GetList();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	private void GetList() {
		pnlKey.Enabled = true;
		grdInviteSchedule.PageIndex = 0;
		grdInviteSchedule.DataBind();
		pnlCount.DataBind();
	}

	protected void JoinCells(GridView pGrd,int pCol) {
		int iRow = pGrd.Rows.Count;
		int iBaseidx = 0;

		while (iBaseidx < iRow) {
			int iNextIdx = iBaseidx + 1;
			TableCell celBase = pGrd.Rows[iBaseidx].Cells[pCol];

			while (iNextIdx < iRow) {

				TableCell celNext = pGrd.Rows[iNextIdx].Cells[pCol];

				if (GetText(celBase).Equals(GetText(celNext))) {
					if (celBase.RowSpan == 0) {
						celBase.RowSpan = 2;
					} else {
						celBase.RowSpan++;
					}
					pGrd.Rows[iNextIdx].Cells.Remove(celNext);
					iNextIdx++;
				} else {
					break;
				}
			}
			iBaseidx = iNextIdx;
		}
	}

	protected void dsInviteSchedule_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsInviteSchedule_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
		e.InputParameters[1] = string.Empty;
	}

	protected void dsMailTemplate_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		e.InputParameters[1] = ViCommConst.MAIL_TP_UNAUTH_CAST_INVITE;
	}

	protected void grdInviteSchedule_DataBound(object sender,EventArgs e) {
		JoinCells(grdInviteSchedule,0);
	}

	protected void vdcMailTemplateNo_ServerValidate(object source,ServerValidateEventArgs args) {
		string sPos;
		CustomValidator vdcMailTemplateNo = (CustomValidator)source as CustomValidator;
		sPos = vdcMailTemplateNo.ID.Substring(17,1);
		TextBox txtInviteStartTime = (TextBox)plcHolder.FindControl(string.Format("txtInviteStartTime{0}",sPos)) as TextBox;
		TextBox txtInviteEndTime = (TextBox)plcHolder.FindControl(string.Format("txtInviteEndTime{0}",sPos)) as TextBox;
		DropDownList lstMailTemplateNo = (DropDownList)pnlDtl.FindControl(string.Format("lstMailTemplateNo{0}",sPos));
		RequiredFieldValidator vdrMailTemplateNo = (RequiredFieldValidator)pnlDtl.FindControl(string.Format("vdrMailTemplateNo{0}",sPos));
		
		if (lstMailTemplateNo.SelectedValue.Equals("")) {
			if (txtInviteStartTime.Text.Equals("00:00") &&
				txtInviteEndTime.Text.Equals("00:00")) {
				vdrMailTemplateNo.Enabled = false;
			} else {
				vdrMailTemplateNo.Enabled = true;
			}
		} else {
			vdrMailTemplateNo.Enabled = false;
		}
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UNAUTH_CAST_INVITE_SCHE_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lblSiteCd.Text);
			db.ProcedureInParm("PINVITE_LEVEL",DbSession.DbType.NUMBER,lblInviteLevel.Text);
			db.ProcedureInParm("PINVITE_RECORD_COUNT",DbSession.DbType.NUMBER,MAX_INVITE_COUNT);
			db.ProcedureOutArrayParm("PINVITE_COUNT_PER_DAY",DbSession.DbType.VARCHAR2,MAX_INVITE_COUNT);
			db.ProcedureOutArrayParm("PINVITE_START_TIME",DbSession.DbType.VARCHAR2,MAX_INVITE_COUNT);
			db.ProcedureOutArrayParm("PINVITE_END_TIME",DbSession.DbType.VARCHAR2,MAX_INVITE_COUNT);
			db.ProcedureOutArrayParm("PINVITE_LAST_EXEC_DATE",DbSession.DbType.VARCHAR2,MAX_INVITE_COUNT);
			db.ProcedureOutArrayParm("PINVITE_MAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,MAX_INVITE_COUNT);
			db.ProcedureOutArrayParm("PINVITE_ROWID",DbSession.DbType.VARCHAR2,MAX_INVITE_COUNT);
			db.ProcedureOutArrayParm("PINVITE_REVISION_NO",DbSession.DbType.VARCHAR2,MAX_INVITE_COUNT);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["PINVITE_RECORD_COUNT"] = db.GetIntValue("PINVITE_RECORD_COUNT");
			
			for (int i = 1;i <= MAX_INVITE_COUNT;i++) {
				int intArrIndex = i - 1;
				if (db.GetIntValue("PINVITE_RECORD_COUNT") > 0) {
					Label lblInviteCountPerDay = (Label)plcHolder.FindControl(string.Format("lblInviteCountPerDay{0}",i)) as Label;
					TextBox txtInviteStartTime = (TextBox)plcHolder.FindControl(string.Format("txtInviteStartTime{0}",i)) as TextBox;
					TextBox txtInviteEndTime = (TextBox)plcHolder.FindControl(string.Format("txtInviteEndTime{0}",i)) as TextBox;
					DropDownList lstMailTemplateNo = (DropDownList)plcHolder.FindControl(string.Format("lstMailTemplateNo{0}",i)) as DropDownList;
					Label lblInviteLastExecDate1 = (Label)plcHolder.FindControl(string.Format("lblInviteLastExecDate1{0}",i)) as Label;

					lblInviteCountPerDay.Text = i.ToString();
					txtInviteStartTime.Text = db.GetArryStringValue("PINVITE_START_TIME",intArrIndex);
					txtInviteEndTime.Text = db.GetArryStringValue("PINVITE_END_TIME",intArrIndex);

					if (string.IsNullOrEmpty(db.GetArryStringValue("PINVITE_MAIL_TEMPLATE_NO",intArrIndex))) {
						lstMailTemplateNo.SelectedIndex = -1;
					} else {
						lstMailTemplateNo.SelectedValue = db.GetArryStringValue("PINVITE_MAIL_TEMPLATE_NO",intArrIndex);
					}
					
					lblInviteLastExecDate1.Text = db.GetArryStringValue("PINVITE_LAST_EXEC_DATE",intArrIndex);
				}
				ViewState["INVITE_ROWID" + intArrIndex.ToString()] = db.GetArryStringValue("PINVITE_ROWID",intArrIndex);
				ViewState["PINVITE_REVISION_NO" + intArrIndex.ToString()] = db.GetArryStringValue("PINVITE_REVISION_NO",intArrIndex);
			}
		}
		pnlKey.Enabled = true;
		pnlDtl.Visible = true;
	}

	private void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			string[] sInviteLevel = new string[MAX_INVITE_COUNT];
			string[] sInviteCount = new string[MAX_INVITE_COUNT];
			string[] sInviteStartTime = new string[MAX_INVITE_COUNT];
			string[] sInviteEndTime = new string[MAX_INVITE_COUNT];
			string[] sMailTemplateNo = new string[MAX_INVITE_COUNT];
			string[] sRevisionNo = new string[MAX_INVITE_COUNT];
			string[] sRowID = new string[MAX_INVITE_COUNT];
			string[] sDelSchedule = new string[MAX_INVITE_COUNT];
			
			for (int i = 0;i < MAX_INVITE_COUNT;i++) {

				int intItemIndex = i + 1;
				TextBox txtInviteStartTime = (TextBox)plcHolder.FindControl(string.Format("txtInviteStartTime{0}",intItemIndex)) as TextBox;
				TextBox txtInviteEndTime = (TextBox)plcHolder.FindControl(string.Format("txtInviteEndTime{0}",intItemIndex)) as TextBox;
				DropDownList lstMailTemplateNo = (DropDownList)plcHolder.FindControl(string.Format("lstMailTemplateNo{0}",intItemIndex)) as DropDownList;

				sInviteCount[i] = intItemIndex.ToString();
				if (txtInviteStartTime.Text.Equals("")) {
					txtInviteStartTime.Text = "00:00";
				}
				sInviteStartTime[i] = txtInviteStartTime.Text;
				if (txtInviteEndTime.Text.Equals("")) {
					txtInviteEndTime.Text = "00:00";
				}
				sInviteEndTime[i] = txtInviteEndTime.Text;
				sMailTemplateNo[i] = lstMailTemplateNo.SelectedValue;
				sDelSchedule[i] = "0";

				sRowID[i] = ViewState["INVITE_ROWID" + i.ToString()].ToString();
				sRevisionNo[i] = ViewState["PINVITE_REVISION_NO" + i.ToString()].ToString();
			}

			db.PrepareProcedure("UNAUTH_CAST_INVITE_SCHE_MNT");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lblSiteCd.Text);
			db.ProcedureInParm("PINVITE_LEVEL",DbSession.DbType.NUMBER,lblInviteLevel.Text);
			db.ProcedureInParm("PINVITE_RECORD_COUNT",DbSession.DbType.NUMBER,MAX_INVITE_COUNT);
			db.ProcedureInArrayParm("PINVITE_COUNT_PER_DAY",DbSession.DbType.VARCHAR2,MAX_INVITE_COUNT,sInviteCount);
			db.ProcedureInArrayParm("PINVITE_START_TIME",DbSession.DbType.VARCHAR2,MAX_INVITE_COUNT,sInviteStartTime);
			db.ProcedureInArrayParm("PINVITE_END_TIME",DbSession.DbType.VARCHAR2,MAX_INVITE_COUNT,sInviteEndTime);
			db.ProcedureInArrayParm("PINVITE_MAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,MAX_INVITE_COUNT,sMailTemplateNo);
			db.ProcedureInArrayParm("PDEL_FLAG",DbSession.DbType.VARCHAR2,MAX_INVITE_COUNT,sDelSchedule);
			db.ProcedureInArrayParm("PINVITE_ROWID",DbSession.DbType.VARCHAR2,MAX_INVITE_COUNT,sRowID);
			db.ProcedureInArrayParm("PINVITE_REVISION_NO",DbSession.DbType.VARCHAR2,MAX_INVITE_COUNT,sRevisionNo);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();
			pnlDtl.Visible = false;
		}
		InitPage();
		GetList();
		lstSeekSiteCd.SelectedValue = lblSiteCd.Text;
	}

	private string GetText(TableCell tc) {
		HyperLink dblc =
		  (HyperLink)tc.Controls[1];
		return dblc.Text;
	}	
}