﻿
function CreateGridHeader(headerRowsCount, gridView, panelGrid, panelUnderGrid) {
//    var start = new Date();

    var gridViewObj = document.getElementById(gridView);
    var panelGridObj = document.getElementById(panelGrid);
    if (!gridViewObj || !panelGridObj) {
        return false;
    }
    
    var gridViewParentObj = gridViewObj.parentNode;
    
    headerDivObj = panelGridObj.appendChild(document.createElement('div'));
    dataDivObj = panelGridObj.appendChild(document.createElement('div'));
    footerDivObj = panelGridObj.appendChild(document.createElement('div'));
    
    headerDivObj.id = 'HeaderDiv';
    dataDivObj.id = 'DataDiv';
    dataDivObj.onscroll = 'OnScroll();'
    dataDivObj.style.cssText = 'overflow: auto; height: auto;';
    footerDivObj.id = 'FooterDiv';
    
    panelGridObj.removeChild(gridViewParentObj);
    dataDivObj.appendChild(gridViewParentObj);
    
    var panelUnderGridObj = document.getElementById(panelUnderGrid);
    
    headerDivObj.style.borderBottomWidth = '0px';
    dataDivObj.style.paddingTop = '0px';
    dataDivObj.style.paddingBottom = '0px';
    dataDivObj.style.width = '5000px';
    footerDivObj.style.borderTopWidth = '0px';

    var _headerRowsCount = headerRowsCount;
    var _footerRowsCount = 0;
    
    if (gridViewObj.rows[0].className == 'PagerRowStyle') {
        _headerRowsCount += 1;
    }
    if (gridViewObj.rows[gridViewObj.rows.length - 1].className == 'PagerRowStyle') {
        _footerRowsCount += 1;
    }

    // 固定テーブルを作成
    var headerTableObj = CreateFixTableObj(headerDivObj, gridViewObj, 0, _headerRowsCount);
    var footerTableObj = CreateFixTableObj(footerDivObj, gridViewObj, gridViewObj.rows.length - 1, _footerRowsCount);

    // 元のヘッダを非表示
    for (var indexRow = 0; indexRow < _headerRowsCount; indexRow++) {
        gridViewObj.rows[indexRow].style.display = 'none';
    }
    
    // 元のフッタを非表示
    if (_footerRowsCount > 0) {
        gridViewObj.rows[gridViewObj.rows.length - 1].style.display = 'none';
    }
    
    // サイズ調整
    var indexColMax = headerTableObj.rows[_headerRowsCount - 1].cells.length;
    var indexRowMax = gridViewObj.rows.length - _footerRowsCount;
    for (var indexCol = 0; indexCol < indexColMax; indexCol++) {
        var headerCellWidth = headerTableObj.rows[_headerRowsCount - 1].cells[indexCol].clientWidth;
        
        for (var indexRow = _headerRowsCount; indexRow < indexRowMax; indexRow++) {
            var td = gridViewObj.rows[indexRow].cells[indexCol];
            if (td.clientWidth == headerCellWidth) {
                break;
            }
            var style = GetStyle(td);
            var pad = GetPxValue(style.paddingLeft) + GetPxValue(style.paddingRight);
            gridViewObj.rows[indexRow].cells[indexCol].style.width = (headerCellWidth - pad) + 'px';
        }
    }
        
    var tableWidth = GetDivWidth(gridViewObj);
    headerDivObj.style.width = tableWidth + 'px';
    footerDivObj.style.width = tableWidth + 'px';
    dataDivObj.style.width = tableWidth + 'px';
    dataDivObj.style.height = GetDataDivHeight(headerDivObj, dataDivObj, footerDivObj, panelGridObj, panelUnderGridObj) + 'px';

//    var end = new Date();
//    alert(end - start);
    return false;
}

function CreateFixTableObj(divObj, gridViewObj, rowIndex, rowsCount) {
    if (!divObj || rowsCount == 0) {
        return null;
    }
    
    divObj.style.overflow = 'auto';
    divObj.style.overflowX = 'hidden';
    divObj.style.overflowY = 'hidden';

    var fixTableObj = divObj.appendChild(document.createElement('table'));
    fixTableObj.className = gridViewObj.className;
    fixTableObj.style.cssText = gridViewObj.style.cssText;
    fixTableObj.border = '1px';
    fixTableObj.rules = 'all';
    fixTableObj.cellPadding = gridViewObj.cellPadding;
    fixTableObj.cellSpacing = gridViewObj.cellSpacing;

//    var _isExcept = document.URL.match(/DailySalesInquiry/g);

    var indexRowMax = rowIndex + rowsCount;
    for (var indexRow = rowIndex; indexRow < indexRowMax; indexRow++) {
        // ヘッダ行を作成
        var row = fixTableObj.insertRow(-1);
        row.className = gridViewObj.rows[indexRow].className;
        row.style.cssText = gridViewObj.rows[indexRow].style.cssText;
        row.style.fontWeight = 'bold';
        row.style.height = gridViewObj.rows[indexRow].clientHeight + 'px';

        // ヘッダセルを作成
        var indexColMax = gridViewObj.rows[indexRow].cells.length;
        for (var indexCol = 0; indexCol < indexColMax; indexCol++) {
            var td = gridViewObj.rows[indexRow].cells[indexCol];
            var spanTag = row.appendChild(document.createElement('td'));
            
            spanTag.colSpan = td.colSpan;
            spanTag.innerHTML = td.innerHTML;
            spanTag.style.cssText = td.style.cssText;
            
//            if (indexRow != rowIndex + rowsCount - 1 && _isExcept) {
//                continue;
//            }
            var style = GetStyle(td);
            spanTag.style.paddingLeft = style.paddingLeft;
            spanTag.style.paddingRight = style.paddingRight;
            spanTag.style.width = td.clientWidth - (GetPxValue(style.paddingLeft) + GetPxValue(style.paddingRight))  + 'px';
            spanTag.style.height = td.clientHeight + 'px';
        }
    }
    
    return fixTableObj;
}

function GetDivWidth(gridViewObj) {
    var width = gridViewObj.clientWidth;
    
    // 垂直スクロールバー用に加算
    if (GetBrowserVersion() == '8') {
        width += 17;
    } else {
        width += 18;
    }
    
    return width;
}

function GetDataDivHeight(headerDivObj, dataDivObj, footerDivObj, panelGridObj, panelUnderGridObj) {
    var height;
    // 修正前のheight
    var preHeight = headerDivObj.clientHeight + dataDivObj.clientHeight + footerDivObj.clientHeight;
    if (panelUnderGridObj != null) {
        preHeight += panelUnderGridObj.clientHeight;
    }

    if (preHeight > panelGridObj.clientHeight) {
        height = panelGridObj.clientHeight - headerDivObj.offsetHeight;
        height -= footerDivObj.offsetHeight;

        var styleHeader = GetStyle(headerDivObj);
        height -= GetPxValue(styleHeader.marginTop);
        height -= GetPxValue(styleHeader.marginBottom);

        var stylefooter = GetStyle(footerDivObj);
        height -= GetPxValue(stylefooter.marginTop);
        height -= GetPxValue(stylefooter.marginBottom);

        var styleDataDiv = GetStyle(dataDivObj);
        height -= GetPxValue(styleDataDiv.marginTop);
        height -= GetPxValue(styleDataDiv.marginBottom);

        var styleGrid = GetStyle(panelGridObj);
        height -= GetPxValue(styleGrid.paddingTop);
        height -= GetPxValue(styleGrid.paddingBottom);
        
        if (panelUnderGridObj != null) {
            height -= panelUnderGridObj.offsetHeight;

            var styleUnderGrid = GetStyle(panelUnderGridObj);
            height -= GetPxValue(styleUnderGrid.marginTop);
            height -= GetPxValue(styleUnderGrid.marginBottom);
        }
    } else {
        dataDivObj.style.overflow = 'hidden';
        return dataDivObj.clientHeight;
    }

    return height;
}

function GetPxValue(value) {
    return eval(value.replace('px', ''));
}

function GetStyle(element) {
    return element.currentStyle || document.defaultView.getComputedStyle(element, '');
}

function GetBrowserVersion() { // @return Number:
    return window.opera ? (opera.version().replace(/\d$/, "") - 0) // Opera10 shock
                      : parseFloat((/(?:IE |fox\/|ome\/|ion\/)(\d+\.\d)/.
                                   exec(navigator.userAgent) || [,0])[1]);
}

function OnScroll() {
    var dataDiv = document.getElementById('DataDiv');
    var headerDiv= document.getElementById('HeaderDiv');
    headerDiv.scrollLeft = dataDiv.scrollLeft;
    return false;
}


/*
・対象となる画面は、Panel pnlGrid があるもの。

・Pasing が有効の際、table に Cssクラス名 PagerRowStyle の行が作られる。
　この行がある場合、PagerRowStyle の行を固定ヘッダ・フッタに追加する。

・デザイナに設定された幅、またはコード上から TemplateField.ItemStyle.Width に設定された値と
　コード上で GridView.Width に設定された値に乖離がある場合、ヘッダ部とデータ部にズレが生じる。
　また、データ部の幅調整の際、時間がかかる。
　→対処
　　各Widthにふさわしい値を設定し、 GridView.Width に値を設定しない。
　　
・Google Chrome でズレが生じる画面は、表示対象に画像があるもの。
　javascript が動作するまでに画像が表示されない。その為、幅の設定を行った後、画像分の幅が広がりズレが生じる。
　レンダリングエンジンの動作による。
*/