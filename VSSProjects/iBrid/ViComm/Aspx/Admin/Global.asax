﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.Diagnostics" %>

<script RunAt="server">
	void Application_Start(object sender,EventArgs e) {
		DateTime dt = DateTime.Parse("2008/06/11 14:00:28");
	}

		void Application_End(object sender,EventArgs e) {
		}

		void Application_Error(object sender,EventArgs e) {
			string sRoot = ConfigurationManager.AppSettings["Root"].ToString().ToLower();
			string sSource = "ViComm";
			string sLog = "Application";
			
			Exception objErr = Server.GetLastError().GetBaseException();
			string err = "ViComm Administrator Error Caught in Application_Error event\n" +
			"Error in: " + Request.Url.ToString() +
			"\nError Message:" + objErr.Message.ToString() +
			"\nStack Trace:" + objErr.StackTrace.ToString();

			if (!EventLog.SourceExists(sSource)) {
				EventLog.CreateEventSource(sSource,sLog);
			}
			
			EventLog.WriteEntry(sSource,err,EventLogEntryType.Error);
			Server.ClearError();
			string dbErrType = objErr.Message.ToString().Substring(objErr.Message.ToString().Length - 4);
			if (ViComm.ViCommConst.SP_ERR_STATUS_ALREADY_UPDATED.Equals(dbErrType)) {
				Response.Redirect(sRoot + "/error/DbAttention.aspx?&errtype=" + dbErrType);
			}
			else if(ViComm.ViCommConst.SP_ERR_STATUS_HAVE_CHILD_RECORD.Equals(dbErrType)){
				Response.Redirect(sRoot + "/error/DbAttention.aspx?&errtype=" + dbErrType);
			}else {
				Response.Redirect(sRoot + "/error/WebError.aspx?&errtype=" + ViComm.ViCommConst.USER_INFO_INTERNAL_ERROR);
			}
		}

		void Session_Start(object sender,EventArgs e) {
		
			string sRoot = ConfigurationManager.AppSettings["Root"].ToString().ToLower();
			string sUrl = Request.Url.ToString().ToLower();
			if (sUrl.IndexOf(sRoot) > 0) {
				if (sUrl.ToString().IndexOf("loginadmin.aspx") < 0 && sUrl.ToString().IndexOf("weberrorformenu.aspx") < 0) {
					if (Session["site"] == null) {
						if (sUrl.ToString().IndexOf("treemenu") >= 0) {
							Response.Redirect(sRoot + "/error/WebErrorForMenu.aspx?errtype=" + ViComm.ViCommConst.USER_INFO_TIMEOUT);
						} else {
							Response.Redirect(sRoot + "/LoginAdmin.aspx");
						}
					}
				}
			}
			
		}

		void Session_End(object sender,EventArgs e) {
		}
      
</script>

