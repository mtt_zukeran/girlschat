﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ASP広告コードメンテナンス
--	Progaram ID		: AspAdList
--
--  Creation Date	: 2010.09.24
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class AspAdManagea_AspAdList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
	}

	private void FirstLoad() {
		grdAspAd.PageSize = 100;
		grdAspAd.DataSourceID = string.Empty;
		DataBind();

		lstSeekSettleCompany.DataSourceID = string.Empty;
		lstSeekSettleCompany.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		lstSeekSettleCompany.SelectedIndex = 0;

		lstSiteCd.DataSourceID = string.Empty;
		lstSiteCd.SelectedIndex = 0;

		lstSettleCompany.DataSourceID = string.Empty;
		lstSettleCompany.SelectedIndex = 0;

		lstAdCategory.DataSourceID = string.Empty;
		lstAdCategory.SelectedIndex = 0;

		lstAdPublicationTopic.DataSourceID = string.Empty;
		lstAdPublicationTopic.SelectedIndex = 0;

		lstApplySexCd.DataSourceID = string.Empty;
		lstApplySexCd.SelectedIndex = 0;

		lstSeekApplySexCd.DataSourceID = string.Empty;
		lstSeekApplySexCd.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		lstSeekApplySexCd.SelectedIndex = 0;
	}

	private void InitPage() {
		txtAspAdCd.Text = "";
		ClearField();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			lstSiteCd_SelectedIndexChanged(lstSiteCd,null);
		}
		pnlMainte.Visible = false;
		GetList();

		this.vdrAffiliatePoint.ErrorMessage = DisplayWordUtil.Replace(this.vdrAffiliatePoint.ErrorMessage);
		this.vdrAffiliateAmt.ErrorMessage = DisplayWordUtil.Replace(this.vdrAffiliateAmt.ErrorMessage);
		this.vdrWomanAffiliatePoint.ErrorMessage = DisplayWordUtil.Replace(this.vdrWomanAffiliatePoint.ErrorMessage);
		this.vdrWomanAffiliateAmt.ErrorMessage = DisplayWordUtil.Replace(this.vdrWomanAffiliateAmt.ErrorMessage);
	}

	private void ClearField() {
		txtAdNm.Text = string.Empty;
		txtAdDescription.Text = string.Empty;
		txtAdDescriptionSub.Text = string.Empty;
		txtMonthlyFee.Text = string.Empty;
		txtAffiliatePoint.Text = string.Empty;
		txtAffiliateAmt.Text = string.Empty;
		txtWomanAffiliatePoint.Text = string.Empty;
		txtWomanAffiliateAmt.Text = string.Empty;
		txtApplyStartDay.Text = string.Empty;
		txtApplyEndDay.Text = string.Empty;
		txtSelfAdCd.Text = string.Empty;
		chkDocomo.Checked = false;
		chkSoftbank.Checked = false;
		chkAu.Checked = false;
		chkAndroid.Checked = false;
		chkIphone.Checked = false;
		chkNaFlag.Checked = false;
		lstAdCategory.SelectedIndex = 0;
		lstAdPublicationTopic.SelectedIndex = 0;
		lstApplySexCd.SelectedIndex = 0;
		recCount = "0";

		lstSeekSettleCompany.SelectedIndex = 0;
		txtSeekAdCd.Text = string.Empty;
		txtSeekAdNm.Text = string.Empty;
		txtAffiliatePointFrom.Text = string.Empty;
		txtAffiliatePointTo.Text = string.Empty;
		txtMonthlyFeeFrom.Text = string.Empty;
		txtMonthlyFeeTo.Text = string.Empty;
		txtWomanAffiliatePointFrom.Text = string.Empty;
		txtWomanAffiliatePointTo.Text = string.Empty;
		txtApplyStartDayFrom.Text = string.Empty;
		txtApplyStartDayTo.Text = string.Empty;
		lstSeekApplySexCd.SelectedIndex = 0;
		chkSeekDocomo.Checked = false;
		chkSeekAu.Checked = false;
		chkseekSoftbank.Checked = false;
		chkSeekAndroid.Checked = false;
		chkSeekIphone.Checked = false;
		rdoStatus.SelectedIndex = 0;
	}

	protected string GetRecCount() {
		return recCount;
	}

	private void GetList() {
		SetupCondition();
		grdAspAd.DataSourceID = "dsAspAd";
		grdAspAd.PageIndex = 0;
		grdAspAd.DataBind();
		pnlCount.DataBind();
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}
	protected void btnClear_Click(object sender,EventArgs e) {
	
		InitPage();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		pnlMainte.Visible = true;
		pnlKey.Enabled = true;
		pnlDtl.Visible = false;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void lnkAspAdCd_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		lstSiteCd.SelectedValue = sKeys[0];
		lstSettleCompany.SelectedValue = sKeys[1];
		txtAspAdCd.Text = sKeys[3];
		GetData();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void dsAspAd_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsAspAd_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
	
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
		e.InputParameters[1] = lstSeekSettleCompany.SelectedValue;
		e.InputParameters[2] = txtSeekAdCd.Text;
		e.InputParameters[3] = txtSeekAdNm.Text;
		e.InputParameters[4] = txtAffiliatePointFrom.Text;
		e.InputParameters[5] = txtAffiliatePointTo.Text;
		e.InputParameters[6] = txtWomanAffiliatePointFrom.Text;
		e.InputParameters[7] = txtWomanAffiliatePointTo.Text;
		e.InputParameters[8] = txtApplyStartDayFrom.Text;
		e.InputParameters[9] = txtApplyStartDayTo.Text;
		e.InputParameters[10] = txtMonthlyFeeFrom.Text;
		e.InputParameters[11] = txtMonthlyFeeTo.Text;
		e.InputParameters[12] = chkSeekDocomo.Checked ? "1" : "0";
		e.InputParameters[13] = chkseekSoftbank.Checked ? "1" : "0";
		e.InputParameters[14] = chkSeekAu.Checked ? "1" : "0";
		e.InputParameters[15] = chkSeekAndroid.Checked ? "1" : "0";
		e.InputParameters[16] = chkSeekIphone.Checked ? "1" : "0";
		e.InputParameters[17] = lstSeekApplySexCd.SelectedValue;
		e.InputParameters[18] = rdoStatus.SelectedValue;

		
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {

			db.PrepareProcedure("ASP_AD_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PSETTLE_COMPANY_CD",DbSession.DbType.VARCHAR2,lstSettleCompany.SelectedValue);
			db.ProcedureInParm("PSETTLE_TYPE",DbSession.DbType.VARCHAR2,ViCommConst.SETTLE_POINT_AFFILIATE);
			db.ProcedureInParm("PASP_AD_CD",DbSession.DbType.VARCHAR2,txtAspAdCd.Text);
			db.ProcedureOutParm("PAD_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PAD_DESCRIPTION",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PAD_DESCRIPTION_SUB",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMONTHLY_FEE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PAFFILIATE_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PAFFILIATE_AMT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PWOMAN_AFFILIATE_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PWOMAN_AFFILIATE_AMT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PAPPLY_START_DAY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PAPPLY_END_DAY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PAPPLY_SEX_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PAPPLY_DOCOMO_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PAPPLY_SOFTBANK_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PAPPLY_AU_FLAG", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PAPPLY_ANDROID_FLAG", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PAPPLY_IPHONE_FLAG", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PAD_CATEGORY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PAD_PUBLICATION_TOPIC",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSELF_AD_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PNA_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			
			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtAdNm.Text = db.GetStringValue("PAD_NM");
				txtAdDescription.Text = db.GetStringValue("PAD_DESCRIPTION");
				txtAdDescriptionSub.Text = db.GetStringValue("PAD_DESCRIPTION_SUB");
				txtMonthlyFee.Text = db.GetStringValue("PMONTHLY_FEE");
				txtAffiliatePoint.Text = db.GetStringValue("PAFFILIATE_POINT");
				txtAffiliateAmt.Text = db.GetStringValue("PAFFILIATE_AMT");
				txtWomanAffiliatePoint.Text = db.GetStringValue("PWOMAN_AFFILIATE_POINT");
				txtWomanAffiliateAmt.Text = db.GetStringValue("PWOMAN_AFFILIATE_AMT");
				txtApplyStartDay.Text = db.GetStringValue("PAPPLY_START_DAY");
				txtApplyEndDay.Text = db.GetStringValue("PAPPLY_END_DAY");
				txtSelfAdCd.Text = db.GetStringValue("PSELF_AD_CD");

				chkDocomo.Checked = db.GetStringValue("PAPPLY_DOCOMO_FLAG").Equals(ViCommConst.FLAG_ON_STR);
				chkSoftbank.Checked = db.GetStringValue("PAPPLY_SOFTBANK_FLAG").Equals(ViCommConst.FLAG_ON_STR);
				chkAu.Checked = db.GetStringValue("PAPPLY_AU_FLAG").Equals(ViCommConst.FLAG_ON_STR);
				chkAndroid.Checked = db.GetStringValue("PAPPLY_ANDROID_FLAG").Equals(ViCommConst.FLAG_ON_STR);
				chkIphone.Checked = db.GetStringValue("PAPPLY_IPHONE_FLAG").Equals(ViCommConst.FLAG_ON_STR);
				chkNaFlag.Checked = db.GetStringValue("PNA_FLAG").Equals(ViCommConst.FLAG_ON_STR);

				lstAdCategory.SelectedValue = db.GetStringValue("PAD_CATEGORY");
				lstAdPublicationTopic.SelectedValue = db.GetStringValue("PAD_PUBLICATION_TOPIC");
				lstApplySexCd.SelectedValue = db.GetStringValue("PAPPLY_SEX_CD");
			
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;
	}

	private void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ASP_AD_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PSETTLE_COMPANY_CD",DbSession.DbType.VARCHAR2,lstSettleCompany.SelectedValue);
			db.ProcedureInParm("PSETTLE_TYPE",DbSession.DbType.VARCHAR2,ViCommConst.SETTLE_POINT_AFFILIATE);
			db.ProcedureInParm("PASP_AD_CD",DbSession.DbType.VARCHAR2,txtAspAdCd.Text);
			db.ProcedureInParm("PAD_NM",DbSession.DbType.VARCHAR2,txtAdNm.Text);
			db.ProcedureInParm("PAD_DESCRIPTION",DbSession.DbType.VARCHAR2,txtAdDescription.Text);
			db.ProcedureInParm("PAD_DESCRIPTION_SUB",DbSession.DbType.VARCHAR2,txtAdDescriptionSub.Text);
			db.ProcedureInParm("PMONTHLY_FEE",DbSession.DbType.NUMBER,int.Parse(txtMonthlyFee.Text));
			db.ProcedureInParm("PAFFILIATE_POINT",DbSession.DbType.NUMBER,int.Parse(txtAffiliatePoint.Text));
			db.ProcedureInParm("PAFFILIATE_AMT",DbSession.DbType.NUMBER,int.Parse(txtAffiliateAmt.Text));
			db.ProcedureInParm("PWOMAN_AFFILIATE_POINT",DbSession.DbType.NUMBER,int.Parse(txtWomanAffiliatePoint.Text));
			db.ProcedureInParm("PWOMAN_AFFILIATE_AMT",DbSession.DbType.NUMBER,int.Parse(txtWomanAffiliateAmt.Text));
			db.ProcedureInParm("PAPPLY_START_DAY",DbSession.DbType.VARCHAR2,txtApplyStartDay.Text);
			db.ProcedureInParm("PAPPLY_END_DAY",DbSession.DbType.VARCHAR2,string.IsNullOrEmpty(txtApplyEndDay.Text) ? "2099/12/31" : txtApplyEndDay.Text);
			db.ProcedureInParm("PAPPLY_SEX_CD",DbSession.DbType.VARCHAR2,lstApplySexCd.SelectedValue);
			db.ProcedureInParm("PAPPLY_DOCOMO_FLAG",DbSession.DbType.NUMBER,chkDocomo.Checked ? 1 : 0);
			db.ProcedureInParm("PAPPLY_SOFTBANK_FLAG",DbSession.DbType.NUMBER,chkSoftbank.Checked ? 1 : 0);
			db.ProcedureInParm("PAPPLY_AU_FLAG",DbSession.DbType.NUMBER,chkAu.Checked ? 1 : 0);
			db.ProcedureInParm("PAPPLY_ANDROID_FLAG", DbSession.DbType.NUMBER, chkAndroid.Checked ? 1 : 0);
			db.ProcedureInParm("PAPPLY_IPHONE_FLAG", DbSession.DbType.NUMBER, chkIphone.Checked ? 1 : 0);
			db.ProcedureInParm("PAD_CATEGORY",DbSession.DbType.VARCHAR2,lstAdCategory.SelectedValue);
			db.ProcedureInParm("PAD_PUBLICATION_TOPIC",DbSession.DbType.VARCHAR2,lstAdPublicationTopic.SelectedValue);
			db.ProcedureInParm("PSELF_AD_CD",DbSession.DbType.VARCHAR2,txtSelfAdCd.Text);
			db.ProcedureInParm("PNA_FLAG",DbSession.DbType.NUMBER,chkNaFlag.Checked ? 1 : 0);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		InitPage();
		GetList();
	}

	private void SetupCondition() {
		if ((!txtAffiliatePointFrom.Text.Equals("")) || (!txtAffiliatePointTo.Text.Equals(""))) {
			if (txtAffiliatePointFrom.Text.Equals("")) {
				txtAffiliatePointFrom.Text = txtAffiliatePointTo.Text;
			} else if (txtAffiliatePointTo.Text.Equals("")) {
				txtAffiliatePointTo.Text = txtAffiliatePointFrom.Text;
			}
		}

		if ((!txtWomanAffiliatePointFrom.Text.Equals("")) || (!txtWomanAffiliatePointTo.Text.Equals(""))) {
			if (txtWomanAffiliatePointFrom.Text.Equals("")) {
				txtWomanAffiliatePointFrom.Text = txtWomanAffiliatePointTo.Text;
			} else if (txtWomanAffiliatePointTo.Text.Equals("")) {
				txtWomanAffiliatePointTo.Text = txtWomanAffiliatePointFrom.Text;
			}
		}

		if ((!txtApplyStartDayFrom.Text.Equals("")) || (!txtApplyStartDayTo.Text.Equals(""))) {
			if (txtApplyStartDayFrom.Text.Equals("")) {
				txtApplyStartDayFrom.Text = txtApplyStartDayTo.Text;
			} else if (txtApplyStartDayTo.Text.Equals("")) {
				txtApplyStartDayTo.Text = txtApplyStartDayFrom.Text;
			}
		}

		if ((!txtMonthlyFeeFrom.Text.Equals("")) || (!txtMonthlyFeeTo.Text.Equals(""))) {
			if (txtMonthlyFeeFrom.Text.Equals("")) {
				txtMonthlyFeeFrom.Text = txtMonthlyFeeTo.Text;
			} else if (txtMonthlyFeeTo.Text.Equals("")) {
				txtMonthlyFeeTo.Text = txtMonthlyFeeFrom.Text;
			}
		}
	}

	protected void lstSiteCd_SelectedIndexChanged(object sender,EventArgs e) {
		lstSettleCompany.DataSourceID = "dsSiteSettle";
		lstSettleCompany.DataBind();
		lstSettleCompany.SelectedIndex = 0;
	}

	protected void lstSeekSiteCd_SelectedIndexChanged(object sender,EventArgs e) {
		lstSeekSettleCompany.DataSourceID = "dsSiteSettle";
		lstSeekSettleCompany.DataBind();
		lstSeekSettleCompany.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		lstSeekSettleCompany.DataSourceID = "";
		lstSeekSettleCompany.SelectedIndex = 0;
	}

	protected string GetCarrierMark(object CarrierFlag) {
		return CarrierFlag.ToString().Equals(ViCommConst.FLAG_ON_STR) ? "●" : string.Empty;
	}

	protected void vdcSelfAdCd_ServerValidate(object source,ServerValidateEventArgs args) {
		if (string.IsNullOrEmpty(txtSelfAdCd.Text)) {
			args.IsValid = true;
		} else {
			using (Ad oAd = new Ad()) {
				string sAdNm = string.Empty;
				args.IsValid = oAd.IsExist(txtSelfAdCd.Text,ref sAdNm);
			}
		}
	}
}
