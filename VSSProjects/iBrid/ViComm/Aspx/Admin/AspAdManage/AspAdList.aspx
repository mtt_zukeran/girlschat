<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AspAdList.aspx.cs" Inherits="AspAdManagea_AspAdList" Title="広告コード設定"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="広告コード設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 980px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2" style="width: 100px;">
									サイト
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px" AutoPostBack="True"
										OnSelectedIndexChanged="lstSiteCd_SelectedIndexChanged">
									</asp:DropDownList>
								</td>
								<td class="tdHeaderStyle2" style="width: 100px;">
									ASP出稿先
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstSettleCompany" runat="server" DataSourceID="dsSiteSettle" DataTextField="SETTLE_COMPANY_NM" DataValueField="SETTLE_COMPANY_CD"
										Width="280px">
									</asp:DropDownList>
								</td>
								<td class="tdHeaderStyle2" style="width: 100px;">
									ASP広告ｺｰﾄﾞ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtAspAdCd" runat="server" MaxLength="32" Width="100px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrAspAdCd" runat="server" ErrorMessage="ASP広告コードを入力して下さい。" ControlToValidate="txtAspAdCd" ValidationGroup="Key">*</asp:RequiredFieldValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[ASP広告コード内容]</legend>
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									広告名称
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtAdNm" runat="server" MaxLength="80" Width="200px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrAdNm" runat="server" ErrorMessage="ASP広告名称を入力して下さい。" ControlToValidate="txtAdNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									広告説明
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtAdDescription" runat="server" MaxLength="1000" Width="400px" TextMode="MultiLine" Height="50px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									広告説明2<br />
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtAdDescriptionSub" runat="server" MaxLength="1000" Width="400px" TextMode="MultiLine" Height="50px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									月額利用料
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtMonthlyFee" runat="server" MaxLength="5" Width="50px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrMonthlyFee" runat="server" ErrorMessage="月額利用料を入力して下さい。" ControlToValidate="txtMonthlyFee" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("男性成果ポイント")%>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtAffiliatePoint" runat="server" MaxLength="5" Width="60px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrAffiliatePoint" runat="server" ErrorMessage="男性会員成果ポイントを入力して下さい。" ControlToValidate="txtAffiliatePoint" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("男性成果金額")%>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtAffiliateAmt" runat="server" MaxLength="6" Width="60px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrAffiliateAmt" runat="server" ErrorMessage="男性会員成果金額を入力して下さい。" ControlToValidate="txtAffiliateAmt" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("女性成果ポイント")%>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtWomanAffiliatePoint" runat="server" MaxLength="5" Width="60px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrWomanAffiliatePoint" runat="server" ErrorMessage="女性会員成果ポイントを入力して下さい。" ControlToValidate="txtWomanAffiliatePoint" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("女性成果金額")%>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtWomanAffiliateAmt" runat="server" MaxLength="6" Width="60px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrWomanAffiliateAmt" runat="server" ErrorMessage="女性会員成果金額を入力して下さい。" ControlToValidate="txtWomanAffiliateAmt" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									適用開始日
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtApplyStartDay" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrApplyStartDay" runat="server" ErrorMessage="適用開始日を入力して下さい。" ControlToValidate="txtApplyStartDay" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RangeValidator ID="vddApplyStartDay" runat="server" ErrorMessage="適用開始日をを正しく入力して下さい。" ControlToValidate="txtApplyStartDay" MaximumValue="2099/12/31"
										MinimumValue="1990/01/01" Type="Date" ValidationGroup="Detail" Display="Dynamic">*</asp:RangeValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									適用終了日
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtApplyEndDay" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
									<asp:RangeValidator ID="vddApplyEndDay" runat="server" ErrorMessage="適用終了日をを正しく入力して下さい。" ControlToValidate="txtApplyEndDay" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
										Type="Date" ValidationGroup="Detail" Display="Dynamic">*</asp:RangeValidator>
									<asp:CompareValidator ID="vdcApplyDay" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtApplyStartDay" ControlToValidate="txtApplyEndDay"
										Operator="GreaterThanEqual" ValidationGroup="Detail" Display="Dynamic">*</asp:CompareValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									適用性別
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstApplySexCd" runat="server" Width="60px" DataSourceID="dsApplySexCd" DataTextField="CODE_NM" DataValueField="CODE">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									キャリア
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkDocomo" runat="server" Text="ﾄﾞｺﾓ" />
									<asp:CheckBox ID="chkAu" runat="server" Text="au" />
									<asp:CheckBox ID="chkSoftbank" runat="server" Text="ｿﾌﾄﾊﾞﾝｸ" />
									<asp:CheckBox ID="chkAndroid" runat="server" Text="Android" />
									<asp:CheckBox ID="chkIphone" runat="server" Text="iPhone" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									広告カテゴリ
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstAdCategory" runat="server" Width="206px" DataSourceID="dsAdCategory" DataTextField="CODE_NM" DataValueField="CODE">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									広告掲載トピック
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstAdPublicationTopic" runat="server" Width="206px" DataSourceID="dsAdPublicationTopic" DataTextField="CODE_NM" DataValueField="CODE">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									自社広告コード
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtSelfAdCd" runat="server" MaxLength="32" Width="200px"></asp:TextBox>
									<asp:RegularExpressionValidator ID="vdeSelfAdCd" runat="server" ErrorMessage="広告コードは32桁以下の英数字で入力して下さい。" ValidationExpression="[a-zA-Z0-9-\/_]{1,32}" ControlToValidate="txtSelfAdCd"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									<asp:CustomValidator ID="vdcSelfAdCd" runat="server" ControlToValidate="txtAspAdCd" ErrorMessage="広告コードが未登録です。" OnServerValidate="vdcSelfAdCd_ServerValidate"
										ValidationGroup="Detail">*</asp:CustomValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									利用不可フラグ
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkNaFlag" runat="server" />
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[広告コード一覧]</legend>
			<table border="0" style="width: 700px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle">
						サイト
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px" AutoPostBack="True"
							OnSelectedIndexChanged="lstSeekSiteCd_SelectedIndexChanged">
						</asp:DropDownList>
					</td>
					<td class="tdHeaderStyle">
						ASP出稿先
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekSettleCompany" runat="server" DataSourceID="dsSiteSettle" DataTextField="SETTLE_COMPANY_NM" DataValueField="SETTLE_COMPANY_CD"
							Width="280px">
						</asp:DropDownList>
					</td>
				</tr>
				<tr>
					<td class="tdHeaderStyle">
						ASP広告コード
					</td>
					<td class="tdDataStyle">
						<asp:TextBox ID="txtSeekAdCd" runat="server" MaxLength="32" Width="100px"></asp:TextBox>
					</td>
					<td class="tdHeaderStyle">
						ASP広告名称
					</td>
					<td class="tdDataStyle">
						<asp:TextBox ID="txtSeekAdNm" runat="server" MaxLength="80" Width="100px"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<td class="tdHeaderStyle">
						<%= DisplayWordUtil.Replace("男性成果ポイント")%>
					</td>
					<td class="tdDataStyle">
						<asp:TextBox ID="txtAffiliatePointFrom" runat="server" MaxLength="5" Width="50px"></asp:TextBox>
						〜
						<asp:TextBox ID="txtAffiliatePointTo" runat="server" MaxLength="5" Width="50px"></asp:TextBox>
						<asp:CompareValidator ID="vdcAffiliatePoint" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtAffiliatePointFrom" ControlToValidate="txtAffiliatePointTo"
							Operator="GreaterThanEqual" ValidationGroup="Seek" Type="Integer">*</asp:CompareValidator>
					</td>
					<td class="tdHeaderStyle">
						月額利用料
					</td>
					<td class="tdDataStyle">
						<asp:TextBox ID="txtMonthlyFeeFrom" runat="server" MaxLength="5" Width="50px"></asp:TextBox>
						〜
						<asp:TextBox ID="txtMonthlyFeeTo" runat="server" MaxLength="5" Width="50px"></asp:TextBox>
						<asp:CompareValidator ID="vdcMonthlyFee" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtMonthlyFeeFrom" ControlToValidate="txtMonthlyFeeTo"
							Operator="GreaterThanEqual" ValidationGroup="Seek" Type="Integer">*</asp:CompareValidator>
					</td>
				</tr>
				<tr>
					<td class="tdHeaderStyle">
						<%= DisplayWordUtil.Replace("女性成果ポイント")%>
					</td>
					<td class="tdDataStyle">
						<asp:TextBox ID="txtWomanAffiliatePointFrom" runat="server" MaxLength="5" Width="50px"></asp:TextBox>
						〜
						<asp:TextBox ID="txtWomanAffiliatePointTo" runat="server" MaxLength="5" Width="50px"></asp:TextBox>
						<asp:CompareValidator ID="vdcWomanAffiliatePoint" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtWomanAffiliatePointFrom" ControlToValidate="txtWomanAffiliatePointTo"
							Operator="GreaterThanEqual" ValidationGroup="Seek" Type="Integer">*</asp:CompareValidator>
					</td>
					<td class="tdHeaderStyle">
						適用開始日
					</td>
					<td class="tdDataStyle">
						<asp:TextBox ID="txtApplyStartDayFrom" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
						〜
						<asp:TextBox ID="txtApplyStartDayTo" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
						<asp:RangeValidator ID="vdrApplyStartDayFrom" runat="server" ErrorMessage="利用開始日Fromを正しく入力して下さい。" ControlToValidate="txtApplyStartDayFrom" MaximumValue="2099/12/31"
							MinimumValue="1990/01/01" Type="Date" ValidationGroup="Seek" Display="Dynamic">*</asp:RangeValidator>
						<asp:RangeValidator ID="vdrApplyStartDayTo" runat="server" ErrorMessage="利用開始日Toを正しく入力して下さい。" ControlToValidate="txtApplyStartDayTo" MaximumValue="2099/12/31"
							MinimumValue="1990/01/01" Type="Date" ValidationGroup="Seek" Display="Dynamic">*</asp:RangeValidator>
						<asp:CompareValidator ID="vdcApplyStartDay" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtApplyStartDayFrom" ControlToValidate="txtApplyStartDayTo"
							Operator="GreaterThanEqual" ValidationGroup="Seek" Display="Dynamic">*</asp:CompareValidator>
					</td>
				</tr>
				<tr>
					<td class="tdHeaderStyle">
						適用性別
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekApplySexCd" runat="server" Width="60px" DataSourceID="dsApplySexCd" DataTextField="CODE_NM" DataValueField="CODE">
						</asp:DropDownList>
					</td>
					<td class="tdHeaderStyle2">
						キャリア
					</td>
					<td class="tdDataStyle">
						<asp:CheckBox ID="chkSeekDocomo" runat="server" Text="ﾄﾞｺﾓ" />
						<asp:CheckBox ID="chkSeekAu" runat="server" Text="au" />
						<asp:CheckBox ID="chkseekSoftbank" runat="server" Text="ｿﾌﾄﾊﾞﾝｸ" />
						<asp:CheckBox ID="chkSeekAndroid" runat="server" Text="Android" />
						<asp:CheckBox ID="chkSeekIphone" runat="server" Text="iPhone" />
					</td>
				</tr>
				<tr>
					<td class="tdHeaderStyle2">
						状態
					</td>
					<td class="tdDataStyle" colspan="3">
						<asp:RadioButtonList ID="rdoStatus" runat="server" RepeatDirection="Horizontal">
							<asp:ListItem Text="すべて" Value="0" Selected="true"></asp:ListItem>
							<asp:ListItem Text="利用可能のみ" Value="1"></asp:ListItem>
							<asp:ListItem Text="利用不可のみ" Value="2"></asp:ListItem>
						</asp:RadioButtonList>
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Seek" />
			<asp:Button runat="server" ID="btnRegist" Text="ASP広告ｺｰﾄﾞ追加・修正" CssClass="seekbutton" CausesValidation="False" OnClick="btnRegist_Click" />
			<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" CausesValidation="False" OnClick="btnClear_Click" ValidationGroup="Seek"  />
			<br />
			<br />
			<asp:GridView ID="grdAspAd" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsAspAd" AllowSorting="True" SkinID="GridViewColor"
				Font-Size="X-Small">
				<Columns>
					<asp:BoundField DataField="SETTLE_COMPANY_NM" HeaderText="ASP出稿先">
						<ItemStyle HorizontalAlign="Left" />
					</asp:BoundField>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkAspAdCd" runat="server" Text='<%# string.Format("{0} {1}",Eval("ASP_AD_CD"),Eval("AD_NM")) %>' CommandArgument='<%# string.Format("{0}:{1}:{2}:{3}",Eval("SITE_CD"),Eval("SETTLE_COMPANY_CD"),Eval("SETTLE_TYPE"),Eval("ASP_AD_CD"))  %>'
								OnCommand="lnkAspAdCd_Command" CausesValidation="False">
							</asp:LinkButton>
						</ItemTemplate>
						<HeaderTemplate>
							ASP広告
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:BoundField DataField="AFFILIATE_POINT" HeaderText="成果ﾎﾟｲﾝﾄ">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:BoundField DataField="MONTHLY_FEE" HeaderText="月額">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:Label ID="lblApplyDay" runat="server" Text='<%# string.Format("{0}〜{1}",Eval("APPLY_START_DAY"),Eval("APPLY_END_DAY")) %>'></asp:Label>
						</ItemTemplate>
						<HeaderTemplate>
							適用日
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:Label ID="Label1" runat="server" Text='<%# GetCarrierMark(Eval("APPLY_DOCOMO_FLAG")) %>'></asp:Label>
						</ItemTemplate>
						<HeaderTemplate>
							Do
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Center" />
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:Label ID="Label2" runat="server" Text='<%# GetCarrierMark(Eval("APPLY_SOFTBANK_FLAG")) %>'></asp:Label>
						</ItemTemplate>
						<HeaderTemplate>
							Sb
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Center" />
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:Label ID="Label3" runat="server" Text='<%# GetCarrierMark(Eval("APPLY_AU_FLAG")) %>'></asp:Label>
						</ItemTemplate>
						<HeaderTemplate>
							Au
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Center" />
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:Label ID="Label4" runat="server" Text='<%# GetCarrierMark(Eval("APPLY_ANDROID_FLAG")) %>'></asp:Label>
						</ItemTemplate>
						<HeaderTemplate>
							And
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Center" />
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:Label ID="Label5" runat="server" Text='<%# GetCarrierMark(Eval("APPLY_IPHONE_FLAG")) %>'></asp:Label>
						</ItemTemplate>
						<HeaderTemplate>
							Ip
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Center" />
					</asp:TemplateField>
					<asp:BoundField DataField="UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br>
				<a class="reccount">Current viewing page
					<%=grdAspAd.PageIndex + 1%>
					of
					<%=grdAspAd.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSiteSettle" runat="server" SelectMethod="GetList" TypeName="SiteSettle">
		<SelectParameters>
			<asp:ControlParameter ControlID="lstSeekSiteCd" Name="pSiteCd" PropertyName="SelectedValue" Type="String" />
			<asp:Parameter Name="pSettleType" Type="String" DefaultValue="A" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsAdCategory" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="34" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsAdPublicationTopic" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="33" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsApplySexCd" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="36" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsAspAd" runat="server" SelectMethod="GetPageCollection" TypeName="AspAd" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsAspAd_Selected" OnSelecting="dsAspAd_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pSettleCompanyCd" Type="String" />
			<asp:Parameter Name="pAdCd" Type="String" />
			<asp:Parameter Name="pAdNm" Type="String" />
			<asp:Parameter Name="pAffiliatePointFrom" Type="String" />
			<asp:Parameter Name="pAffiliatePointTo" Type="String" />
			<asp:Parameter Name="pWomanAffiliatePointFrom" Type="String" />
			<asp:Parameter Name="pWomanAffiliatePointTo" Type="String" />
			<asp:Parameter Name="pApplyStartDayFrom" Type="String" />
			<asp:Parameter Name="pApplyStartDayTo" Type="String" />
			<asp:Parameter Name="pMonthlyFeeFrom" Type="String" />
			<asp:Parameter Name="pMonthlyFeeTo" Type="String" />
			<asp:Parameter Name="pApplyDocomoFlag" Type="String" />
			<asp:Parameter Name="pApplySoftbankFlag" Type="String" />
			<asp:Parameter Name="pApplyAuFlag" Type="String" />
			<asp:Parameter Name="pApplyAndroidFlag" Type="String" />
			<asp:Parameter Name="pApplyIphoneFlag" Type="String" />
			<asp:Parameter Name="pApplySexCd" Type="String" />
			<asp:Parameter Name="pAspAdStatus" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtMonthlyFee" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum2" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtAffiliatePoint" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum3" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtAffiliatePointFrom" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum4" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtAffiliatePointTo" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum5" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtMonthlyFeeFrom" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum6" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtMonthlyFeeTo" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum8" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtAffiliateAmt" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum9" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtWomanAffiliatePoint" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum10" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtWomanAffiliateAmt" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum11" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtWomanAffiliatePointFrom" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum12" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtWomanAffiliatePointTo" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrAdNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrMonthlyFee" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdrAffiliatePoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdrApplyStartDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vddApplyStartDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8" TargetControlID="vddApplyEndDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9" TargetControlID="vdcAffiliatePoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10" TargetControlID="vdcMonthlyFee" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11" TargetControlID="vdrApplyStartDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender12" TargetControlID="vdrApplyStartDayTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender13" TargetControlID="vdcApplyStartDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender14" TargetControlID="vdcApplyDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender15" TargetControlID="vdrAffiliateAmt" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender16" TargetControlID="vdrWomanAffiliatePoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender17" TargetControlID="vdrWomanAffiliateAmt" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender18" TargetControlID="vdcWomanAffiliatePoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskApplyStartDay" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtApplyStartDay">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskApplyEndDay" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtApplyEndDay">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskApplyStartDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtApplyStartDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskApplyStartDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtApplyStartDayTo">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrAspAdCd" HighlightCssClass="validatorCallout" />
</asp:Content>
