﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="NGMailAddressLift.aspx.cs" Inherits="Import_NGMailAddressLift" Title="NGメールアドレス解除" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="NGメールアドレス解除"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[<%= DisplayWordUtil.Replace("男性") %>側NGメールアドレス解除]</legend>
				<asp:Panel runat="server" ID="pnlMan">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 400px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									更新対象サイト
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px">
									</asp:DropDownList>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnManInvalidMailAddrLift" Text="男性側一括解除" CssClass="seekbutton" OnClick="btnManInvalidMailAddrLift_Click" ValidationGroup="Man" />
					</fieldset>
				</asp:Panel>
			</fieldset>
			<fieldset class="fieldset">
				<legend>[<%= DisplayWordUtil.Replace("女性")%>側NGメールアドレス解除]</legend>
				<asp:Panel runat="server" ID="pnlCast">
					<fieldset class="fieldset-inner">
						<asp:Button runat="server" ID="btnCastInvalidMailAddrLift" Text="女性側一括解除" CssClass="seekbutton" OnClick="btnCastInvalidMailAddrLift_Click" ValidationGroup="Cast" />
					</fieldset>
					<%= DisplayWordUtil.Replace("女性")%>側はサイトの指定はできません。
				</asp:Panel>
			</fieldset>
		</asp:Panel>
	</div>
		<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnManInvalidMailAddrLift" ConfirmText="男性側NGメールアドレス一括解除を行ないますか？" ConfirmOnFormSubmit="true" />
		<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnCastInvalidMailAddrLift" ConfirmText="女性側NGメールアドレス一括解除を行ないますか？" ConfirmOnFormSubmit="true" />
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
</asp:Content>
