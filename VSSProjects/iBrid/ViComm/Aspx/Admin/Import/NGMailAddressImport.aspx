<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="NGMailAddressImport.aspx.cs" Inherits="Import_NGMailAddressImport"
	Title="ＮＧメールアドレス取得" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="ＮＧメールアドレス取得"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[NGメールアドレス更新]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<asp:Label ID="lblStatus1" runat="server" ForeColor="blue" Font-Italic="true" Text="NGメールアドレスのアップロードを行います。更新対象サイトを選択し、下記のフォームにファイルを指定してください。"></asp:Label>
					<table border="0" style="width: 800px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle">
								更新対象サイト
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px">
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle2">
								アップロードファイル
							</td>
							<td class="tdDataStyle">
								<asp:FileUpload ID="uplCsv" runat="server" Width="500px" />
								<asp:RequiredFieldValidator ID="valrUpload" runat="server" ControlToValidate="uplCsv" ErrorMessage="アップロードファイルを入力して下さい。" ValidationGroup="Upload" Text="*"></asp:RequiredFieldValidator>
								<br />
							</td>
						</tr>
					</table>
				</asp:Panel>
				<asp:Button ID="btnUpload" runat="server" Text="アップロード" ValidationGroup="Upload" Width="150px" OnClick="btnUpload_Click" />
			</fieldset>
		</asp:Panel>
		<br />
		<asp:Panel ID="pnlTxtFileData" runat="server">
			<fieldset>
				<asp:Label ID="lblTxtFileName" runat="server"></asp:Label>
				<a class="reccount">
					<asp:Label ID="lblCount" runat="server"></asp:Label>
				</a>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpload" ConfirmText="アップロードを行ないますか？" ConfirmOnFormSubmit="true" />
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
</asp:Content>
