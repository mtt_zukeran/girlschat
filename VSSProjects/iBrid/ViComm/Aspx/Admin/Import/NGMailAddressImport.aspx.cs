﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ＮＧメールアドレスインポート
--	Progaram ID		: NGMailAddressImport
--
--  Creation Date	: 2009.08.12
--  Creater			: iBrid(Y.Inoue)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.IO;
using System.Text;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Import_NGMailAddressImport:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		ClearField();

		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void ClearField() {
		SetTxtFileName("");
		SetCount(0,0);
	}

	protected void btnUpload_Click(object sender,EventArgs e) {
		string sInstallDir;
		using (Sys oSys = new Sys()) {
			oSys.GetValue("INSTALL_DIR",out sInstallDir);
		}
		string sFileNm = string.Format("{0}\\Text\\{1}",sInstallDir,"NGMailAdress.csv");
		uplCsv.SaveAs(sFileNm);
		string sLine;
		int iRecCount = 0;
		int iNgCount = 0;
		using (StreamReader sr = new StreamReader(sFileNm,Encoding.GetEncoding("Shift_JIS"))) {
			while ((sLine = sr.ReadLine()) != null) {
				using (DbSession db = new DbSession()) {
					db.PrepareProcedure("IMPORT_INVALID_MAIL_ADDR");
					db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
					db.ProcedureInParm("PEMAIL_ADDR",DbSession.DbType.VARCHAR2,sLine);
					db.ProcedureOutParm("PNG_COUNT",DbSession.DbType.NUMBER);
					db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
					db.ExecuteProcedure();

					iRecCount += 1;
					iNgCount += db.GetIntValue("PNG_COUNT");
				}
			}
		}

		SetTxtFileName(uplCsv.FileName);
		SetCount(iRecCount,iNgCount);
	}

	private void SetTxtFileName(string pTxtFileName) {
		string sTxtFileName;
		if (pTxtFileName == "") {
			sTxtFileName = "未指定";
		} else {
			sTxtFileName = pTxtFileName;
		}
		lblTxtFileName.Text = string.Format("ファイル名 {0}",sTxtFileName);
	}

	private void SetCount(int pRecCount,int pNgCount) {
		lblCount.Text = string.Format("処理 {0:N0}件　該当 {1:N0}件",pRecCount,pNgCount);
	}
}
