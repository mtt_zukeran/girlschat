﻿using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using ViComm;
using iBridCommLib;

public partial class Import_SelectedIDMailSend:System.Web.UI.Page {
	private string mailTemplateType = "";
	private string sCSVFile = "";
	private int OKIDCount = 0;
	private int NGIDCount = 0;
	private int HavePointIDCount = 0;
	private int NoPointIDCount = 0;

	private string[] sUserID;
	private string[] sUserSeq;

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		pnlKey.Enabled = true;

		btnInfoSetupMail.Visible = false;
		btnApproachSetupMail.Visible = false;
		pnlTxMail.Visible = false;
		grdLoginID.DataSource = null;
		ClearField();
		DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		
		DisplayWordUtil.ReplaceValidatorErrorMessage(this.Page.Validators);
	}

	private void ClearField() {
		lstSiteCd.SelectedIndex = 0;
		chkNoPointUser.Checked = false;
		sUserID = new string[0];
		sUserSeq = new string[0];
		sCSVFile = "";
	}

	protected string SetFilePath() {
		if (sCSVFile.Length == 0) {
			return "未指定";
		} else {
			return sCSVFile;
		}
	}

	protected string GetCount() {
		int iExceptCount = 0;
		if (chkNoPointUser.Checked) {
			iExceptCount = NoPointIDCount;
		}
		return string.Format("対象 {0}件  NG {1}件  除外 {2}件",OKIDCount.ToString(),NGIDCount.ToString(),iExceptCount.ToString());
	}

	protected void btnInputCsv_Click(object sender,EventArgs e) {
		if (IsValid == false) {
			return;
		}

		string line = "";
		ArrayList alUserID = new ArrayList();

		string sInstallDir;
		using (Sys oSys = new Sys()) {
			oSys.GetValue("INSTALL_DIR",out sInstallDir);
		}
		string sFileNm = string.Format("{0}\\Text\\{1}",sInstallDir,"IdToMail.csv");

		uplCsv.SaveAs(sFileNm);

		using (StreamReader sr = new StreamReader(sFileNm,Encoding.GetEncoding("Shift_JIS"))) {

			while ((line = sr.ReadLine()) != null) {
				alUserID.Add(line);
			}
			sUserID = new string[alUserID.Count];
			for (int i = 0;i < alUserID.Count;i++) {
				sUserID[i] = alUserID[i].ToString();
			}
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("LOGINID_TO_MAN_MAIL");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInArrayParm("PLOGIN_ID",DbSession.DbType.VARCHAR2,sUserID.Length,sUserID);
			db.ProcedureInParm("PLOGIN_ID_COUNT",DbSession.DbType.NUMBER,sUserID.Length);
			db.ProcedureOutParm("POK_IDCNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PNG_IDCNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PHAVE_POINTCNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PNO_POINTCNT",DbSession.DbType.NUMBER);
			db.ProcedureOutArrayParm("PUSER_SEQ",DbSession.DbType.NUMBER,sUserID.Length);
			db.ProcedureOutArrayParm("PBAL_POINT",DbSession.DbType.NUMBER,sUserID.Length);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			OKIDCount = db.GetIntValue("POK_IDCNT");
			NGIDCount = db.GetIntValue("PNG_IDCNT");
			HavePointIDCount = db.GetIntValue("PHAVE_POINTCNT");
			NoPointIDCount = db.GetIntValue("PNO_POINTCNT");

			using (DataSet ds = new DataSet())
			using (DataTable dt = new DataTable()) {

				dt.Columns.Add(new DataColumn("LOGIN_ID",System.Type.GetType("System.String")));
				dt.Columns.Add(new DataColumn("USER_SEQ",System.Type.GetType("System.Int32")));
				dt.Columns.Add(new DataColumn("BAL_POINT",System.Type.GetType("System.Int32")));
				dt.Columns.Add(new DataColumn("OK_IDCNT",System.Type.GetType("System.Int32")));
				dt.Columns.Add(new DataColumn("NG_IDCNT",System.Type.GetType("System.Int32")));
				dt.Columns.Add(new DataColumn("NO_POINTCNT",System.Type.GetType("System.Int32")));
				for (int i = 0;i < sUserID.Length;i++) {
					DataRow dr = dt.NewRow();
					dr["LOGIN_ID"] = sUserID[i];
					if (db.GetArryIntValue("PUSER_SEQ",i).Length == 0) {
						dr["USER_SEQ"] = -1;
						dr["BAL_POINT"] = 0;
					} else {
						dr["USER_SEQ"] = db.GetArryIntValue("PUSER_SEQ",i);
						dr["BAL_POINT"] = db.GetArryIntValue("PBAL_POINT",i);
					}
					if (i == 0) {
						dr["OK_IDCNT"] = db.GetIntValue("POK_IDCNT");
						dr["NG_IDCNT"] = db.GetIntValue("PNG_IDCNT");
						dr["NO_POINTCNT"] = db.GetIntValue("PNO_POINTCNT");
					}
					dt.Rows.Add(dr);
				}

				ds.Tables.Add(dt);

				pnlCsvFileData.DataBind();
				grdLoginID.DataSource = ds;
				grdLoginID.DataBind();
			}
		}

		if ((!chkNoPointUser.Checked && OKIDCount > 0) ||
			(chkNoPointUser.Checked && (OKIDCount - NoPointIDCount) > 0)) {
			int iCompare = string.Compare(Session["AdminType"].ToString(),ViCommConst.RIGHT_SITE_MANAGER);
			btnInfoSetupMail.Visible = (iCompare >= 0);
			btnApproachSetupMail.Visible = (iCompare >= 0);
			pnlKey.Enabled = !btnApproachSetupMail.Visible;
		}
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnInfoSetupMail_Click(object sender,EventArgs e) {
		setSetupMail(ViCommConst.MAIL_TP_INFO);
	}
	protected void btnApproachSetupMail_Click(object sender,EventArgs e) {
		setSetupMail(ViCommConst.MAIL_TP_APPROACH_ADMIN_TO_USER);
	}
	protected void btnCancelTx_Click(object sender,EventArgs e) {

		pnlTxMail.Visible = false;
	}
	protected void btnTxMail_Click(object sender,EventArgs e) {
		if (IsValid == false) {
			return;
		}

		string[] sUserSeq;
		int iPointBorder = -1;
		if (chkNoPointUser.Checked) {
			sUserSeq = new string[int.Parse(grdLoginID.Rows[0].Cells[3].Text) - int.Parse(grdLoginID.Rows[0].Cells[5].Text)];
			iPointBorder = 0;
		} else {
			sUserSeq = new string[int.Parse(grdLoginID.Rows[0].Cells[3].Text)];
		}
		int iSeqCnt = 0;
		for (int iRow = 0;iRow < grdLoginID.Rows.Count;iRow++) {
			if (!grdLoginID.Rows[iRow].Cells[1].Text.Equals("-1")
				&& (int.Parse(grdLoginID.Rows[iRow].Cells[2].Text)) > iPointBorder) {
				sUserSeq[iSeqCnt++] = grdLoginID.Rows[iRow].Cells[1].Text;
			}
		}

		string[] sDoc;
		int iDocCount;

		SysPrograms.SeparateHtml("",ViCommConst.MAX_ORG_MAIL_BLOCKS,out sDoc,out iDocCount);

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("TX_ADMIN_TO_MAN_MAIL");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PMAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,lstMailTemplateNo.SelectedValue);
			db.ProcedureInParm("PCAST_LOGIN_ID",DbSession.DbType.VARCHAR2,txtCastLoginId.Text);
			db.ProcedureInParm("PCAST_CHAR_NO",DbSession.DbType.VARCHAR2,txtCastCharNo.Text);
			db.ProcedureInArrayParm("PMAN_USER_SEQ",DbSession.DbType.VARCHAR2,sUserSeq.Length,sUserSeq);
			db.ProcedureInParm("PMAN_USER_COUNT",DbSession.DbType.NUMBER,sUserSeq.Length);
			db.ProcedureInParm("PMAIL_AVA_HOUR",DbSession.DbType.NUMBER,int.Parse(txtMailAvaHour.Text));
			db.ProcedureInParm("PPOINT_TRANSFER_AVA_HOUR",DbSession.DbType.NUMBER,int.Parse(txtPointTransferAvaHour.Text));
			db.ProcedureInParm("PSERVICE_POINT",DbSession.DbType.NUMBER,int.Parse(txtServicePoint.Text));
			db.ProcedureInParm("PORIGINAL_TITLE",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInArrayParm("PORIGINAL_DOC",DbSession.DbType.VARCHAR2,iDocCount,sDoc);
			db.ProcedureInParm("PORIGINAL_DOC_COUNT",DbSession.DbType.NUMBER,iDocCount);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			string str = db.GetStringValue("PSTATUS");
		}

		btnTxMail.Enabled = false;
		lblAttention.Text = "メール送信が完了しました。";
	}

	private void setSetupMail(string pMailTemplateType) {
		pnlKey.Enabled = false;
		pnlTxMail.Visible = true;
		btnTxMail.Enabled = true;
		lblAttention.Text = "メール有効時間/Ｐ振替後有効時間に０を設定すると無期限になります。";
		txtServicePoint.Text = "0";
		txtMailAvaHour.Text = "0";
		txtPointTransferAvaHour.Text = "0";
		txtCastLoginId.Text = "";
		lblCastNm.Text = "";
		mailTemplateType = pMailTemplateType;
		if (pMailTemplateType.Equals(ViCommConst.MAIL_TP_APPROACH_ADMIN_TO_USER)) {
			trCastLoginId.Visible = true;
			using (Site oSite = new Site()) {
				if (oSite.GetMultiCharFlag(lstSiteCd.SelectedValue)) {
					tdHeaderCastCharNo.Visible = true;
					tdDataCastCharNo.Visible = true;
				} else {
					tdHeaderCastCharNo.Visible = false;
					tdDataCastCharNo.Visible = false;
					txtCastCharNo.Text = ViCommConst.MAIN_CHAR_NO;
				}
			}
		} else {
			trCastLoginId.Visible = false;
		}
		lstMailTemplateNo.DataBind();
	}

	protected void vdcCastCharNo_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid) {
			using (CastCharacter oCastCharacter = new CastCharacter()) {
				args.IsValid = oCastCharacter.IsExistUserCharNoByLoginId(lstSiteCd.SelectedValue,txtCastLoginId.Text,txtCastCharNo.Text);
			}
		}
	}
	protected void vdcCastLoginId_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid) {
			using (MailTemplate oTemplate = new MailTemplate()) {
				if (oTemplate.GetOne(lstSiteCd.SelectedValue,lstMailTemplateNo.SelectedValue)) {
					if (oTemplate.mailTemplateType.Equals(ViCommConst.MAIL_TP_APPROACH_ADMIN_TO_USER) == false) {
						txtCastLoginId.Text = "";
						return;
					}
				}
			}

			using (Cast oCast = new Cast()) {
				args.IsValid = oCast.IsExistLoginId(txtCastLoginId.Text);
				if (args.IsValid) {
					lblCastNm.Text = oCast.castNm;
				} else {
					lblCastNm.Text = "";
				}
			}
		}
	}

	protected void dsDMMail_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = mailTemplateType;
	}
}
