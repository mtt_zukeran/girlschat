﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ユーザー課金インポート
--	Progaram ID		: UserChargeImport
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.IO;
using System.Text;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Import_UserChargeImport:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
	}


	protected void btnUpload_Click(object sender,EventArgs e) {
		string sInstallDir;
		using (Sys oSys = new Sys()) {
			oSys.GetValue("INSTALL_DIR",out sInstallDir);
		}
		string sFileNm = string.Format("{0}\\Text\\{1}",sInstallDir,"UserCharge.csv");

		uplCsv.SaveAs(sFileNm);
		string sLine;
		bool bWhitePlanFlag = GetWhitePlanFlag();
		using (StreamReader sr = new StreamReader(sFileNm,Encoding.GetEncoding("Shift_JIS"))) {
			while ((sLine = sr.ReadLine()) != null) {
				string[] sValues = sLine.Split('\t');

				using (DbSession db = new DbSession()) {
 					db.PrepareProcedure("USER_CHARGE_IMPORT");
					db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,sValues[0]);
					db.ProcedureInParm("PUSER_RANK",DbSession.DbType.VARCHAR2,sValues[1]);
					db.ProcedureInParm("PCHARGE_TYPE",DbSession.DbType.VARCHAR2,sValues[2]);
					db.ProcedureInParm("PCATEGORY_INDEX",DbSession.DbType.NUMBER,sValues[3]);
					db.ProcedureInParm("PCAST_RANK",DbSession.DbType.VARCHAR2,sValues[4]);
					db.ProcedureInParm("PCAMPAIN_CD",DbSession.DbType.VARCHAR2,sValues[5]);
					db.ProcedureInParm("PCHARGE_POINT_NORMAL",DbSession.DbType.NUMBER,sValues[6]);
					db.ProcedureInParm("PCHARGE_POINT_NEW",DbSession.DbType.NUMBER,sValues[7]);
					db.ProcedureInParm("PCHARGE_POINT_NO_RECEIPT",DbSession.DbType.NUMBER,sValues[8]);
					db.ProcedureInParm("PCHARGE_POINT_NO_USE",DbSession.DbType.NUMBER,sValues[9]);
					db.ProcedureInParm("PCHARGE_POINT_FREE_DIAL",DbSession.DbType.NUMBER,sValues[10]);
					if(bWhitePlanFlag) {
						db.ProcedureInParm("PCHARGE_POINT_WHITE_PLAN",DbSession.DbType.NUMBER,sValues[11]);
						db.ProcedureInParm("pCHARGE_CAST_TALK_APP_POINT",DbSession.DbType.NUMBER,sValues[12]);
						db.ProcedureInParm("pCHARGE_MAN_TALK_APP_POINT",DbSession.DbType.NUMBER,sValues[13]);
						db.ProcedureInParm("PCHARGE_UNIT_SEC",DbSession.DbType.NUMBER,sValues[14]);
					} else {
						db.ProcedureInParm("PCHARGE_POINT_WHITE_PLAN",DbSession.DbType.NUMBER,0);
						db.ProcedureInParm("pCHARGE_CAST_TALK_APP_POINT",DbSession.DbType.NUMBER,sValues[11]);
						db.ProcedureInParm("pCHARGE_MAN_TALK_APP_POINT",DbSession.DbType.NUMBER,sValues[12]);
						db.ProcedureInParm("PCHARGE_UNIT_SEC",DbSession.DbType.NUMBER,sValues[13]);
					}
					db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
					db.ExecuteProcedure();
				}
			}
		}

	}
	public bool GetWhitePlanFlag() {
		using (ManageCompany oManage = new ManageCompany()) {
			return oManage.IsAvailableService(ViCommConst.RELEASE_ENABLED_WHITE_PLAN);
		}
	}
}
