﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 変数インポート
--	Progaram ID		: VariableImport
--
--  Creation Date	: 2010.08.26
--  Creater			: iBrid(Y.Igarashi)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.IO;
using System.Text;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Import_VariableImport : System.Web.UI.Page
{
    protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
	}

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        string sInstallDir;
        using (Sys oSys = new Sys())
        {
            oSys.GetValue("INSTALL_DIR", out sInstallDir);
        }
        string sFileNm = string.Format("{0}\\Text\\{1}", sInstallDir, "Variable.csv");

        uplCsv.SaveAs(sFileNm);
        string sLine;

        using (StreamReader sr = new StreamReader(sFileNm, Encoding.GetEncoding("Shift_JIS")))
        {
            while ((sLine = sr.ReadLine()) != null)
            {
                string[] sValues = sLine.Split('\t');

                using (DbSession db = new DbSession())
                {
                    db.PrepareProcedure("VARIABLE_IMPORT");
                    db.ProcedureInParm("PVARIABLE_ID", DbSession.DbType.VARCHAR2, sValues[0]);
                    db.ProcedureInParm("PUSABLE_TYPE", DbSession.DbType.VARCHAR2, sValues[1]);
                    db.ProcedureInParm("PVARIABLE_TYPE", DbSession.DbType.VARCHAR2, sValues[2]);
                    db.ProcedureInParm("PVARIABLE_NM", DbSession.DbType.VARCHAR2, sValues[3]);
                    db.ProcedureInParm("PVARIABLE_SUMMARY", DbSession.DbType.VARCHAR2, sValues[4]);
					db.ProcedureInParm("PVARIABLE_PARM",DbSession.DbType.VARCHAR2,sValues[5]);
					db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
                    db.ExecuteProcedure();
                }
            }
        }
	}
}
