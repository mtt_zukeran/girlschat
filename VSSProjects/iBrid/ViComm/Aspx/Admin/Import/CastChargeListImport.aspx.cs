﻿using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using ViComm;
using iBridCommLib;

public partial class Import_CastChargeListImport:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	protected void btnInputTxt_Click(object sender,EventArgs e) {
		if (uplTxt.FileName.Length == 0) {
			return;
		}

		string sResult = "更新できません。";
		string sErrLine = "";
		bool bCheck = true;

		string sInstallDir;
		using (Sys oSys = new Sys()) {
			oSys.GetValue("INSTALL_DIR",out sInstallDir);
		}
		string sFileName = string.Format("{0}\\Text\\{1}",sInstallDir,"CastChargeList.txt");
		uplTxt.SaveAs(sFileName);

		string sLine;
		ArrayList alData = new ArrayList();
		using (StreamReader sr = new StreamReader(sFileName,Encoding.GetEncoding("Shift_JIS"))) {
			while ((sLine = sr.ReadLine()) != null) {
				alData.Add(sLine);
			}
		}

		string[][] sDataList = new string[18][];
		for (int iCnt = 0;iCnt < sDataList.GetLength(0);iCnt++) {
			sDataList[iCnt] = new string[alData.Count];
		}

		for (int iRec = 0;iRec < alData.Count;iRec++) {
			string[] sCol = alData[iRec].ToString().Split('\t');
			if (sCol.Length != sDataList.GetLength(0)) {
				sErrLine = string.Format("フォーマットエラーです。<br /> {0}行目",(iRec + 1));
				bCheck = false;
				break;
			}
			for (int iCnt = 0;iCnt < sDataList.GetLength(0);iCnt++) {
				switch (iCnt) {
					case 0:
						sDataList[iCnt][iRec] = sCol[iCnt];
						break;
					default:
						int result = 0;
						if ((iCnt < sCol.Length) && (int.TryParse(sCol[iCnt],out result))) {
							if (iCnt == 1) {
								sDataList[iCnt][iRec] = int.Parse(sCol[iCnt]).ToString("00");
							} else {
								sDataList[iCnt][iRec] = sCol[iCnt];
							}
						} else {
							sErrLine = string.Format("データエラーです。<br /> {0}行 {1}項目",(iRec + 1),(iCnt + 1));
							bCheck = false;
						}
						break;
				}
				if (!bCheck) {
					break;
				}
			}
		}

		if (bCheck) {
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("CAST_CHARGE_IMPORT");
				db.ProcedureInArrayParm("PSITE_CD",DbSession.DbType.VARCHAR2,alData.Count,sDataList[0]);
				db.ProcedureInArrayParm("PCHARGE_TYPE",DbSession.DbType.VARCHAR2,alData.Count,sDataList[1]);
				db.ProcedureInArrayParm("PACT_CATEGORY_SEQ",DbSession.DbType.NUMBER,alData.Count,sDataList[2]);
				db.ProcedureInArrayParm("PCHARGE_POINT_NORMAL",DbSession.DbType.NUMBER,alData.Count,sDataList[4]);
				db.ProcedureInArrayParm("PCHARGE_POINT_NO_RECEIPT",DbSession.DbType.NUMBER,alData.Count,sDataList[5]);
				db.ProcedureInArrayParm("PCHARGE_POINT_NORMAL2",DbSession.DbType.NUMBER,alData.Count,sDataList[6]);
				db.ProcedureInArrayParm("PCHARGE_POINT_NO_RECEIPT2",DbSession.DbType.NUMBER,alData.Count,sDataList[7]);
				db.ProcedureInArrayParm("PCHARGE_POINT_NORMAL3",DbSession.DbType.NUMBER,alData.Count,sDataList[8]);
				db.ProcedureInArrayParm("PCHARGE_POINT_NO_RECEIPT3",DbSession.DbType.NUMBER,alData.Count,sDataList[9]);
				db.ProcedureInArrayParm("PCHARGE_POINT_NORMAL4",DbSession.DbType.NUMBER,alData.Count,sDataList[10]);
				db.ProcedureInArrayParm("PCHARGE_POINT_NO_RECEIPT4",DbSession.DbType.NUMBER,alData.Count,sDataList[11]);
				db.ProcedureInArrayParm("PCHARGE_POINT_NORMAL5",DbSession.DbType.NUMBER,alData.Count,sDataList[12]);
				db.ProcedureInArrayParm("PCHARGE_POINT_NO_RECEIPT5",DbSession.DbType.NUMBER,alData.Count,sDataList[13]);
				db.ProcedureInArrayParm("PCHARGE_UNIT_SEC",DbSession.DbType.NUMBER,alData.Count,sDataList[14]);
				db.ProcedureInArrayParm("PFREE_DIAL_FEE",DbSession.DbType.NUMBER,alData.Count,sDataList[15]);
				db.ProcedureInArrayParm("pCAST_USE_TALK_APP_ADD_POINT",DbSession.DbType.NUMBER,alData.Count,sDataList[16]);
				db.ProcedureInArrayParm("pMAN_USE_TALK_APP_ADD_POINT",DbSession.DbType.NUMBER,alData.Count,sDataList[17]);
				db.ProcedureOutParm("PRESULTLIST",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();

				if (db.GetStringValue("PSTATUS").Equals("I000")) {
					if (db.GetStringValue("PRESULTLIST").Length == 0) {
						sResult = "更新しました。";
					} else {
						sErrLine = string.Format("該当データがありません。<br />{0}行目",db.GetStringValue("PRESULTLIST"));
					}
				}
			}
		}
		SetTxtFileName(uplTxt.FileName);
		lblReslut.Text = sResult;
		lblErrLine.Text = sErrLine;
	}
	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	private void InitPage() {
		ClearField();

		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);
		this.Label1.Text = DisplayWordUtil.Replace(this.Label1.Text);
		this.btnInputTxt.Text = DisplayWordUtil.Replace(this.btnInputTxt.Text);
		this.ConfirmButtonExtender1.ConfirmText = DisplayWordUtil.Replace(this.ConfirmButtonExtender1.ConfirmText);
	}

	private void ClearField() {
		SetTxtFileName("");
		lblReslut.Text = "";
		lblErrLine.Text = "";

	}

	private void SetTxtFileName(string pTxtFileName) {
		string sTxtFileName;
		if (pTxtFileName == "") {
			sTxtFileName = "未指定";
		} else {
			sTxtFileName = pTxtFileName;
		}
		lblTxtFileName.Text = string.Format("ファイル名 {0}",sTxtFileName);
	}

}
