﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ユーザービューインポート
--	Progaram ID		: UserViewImport
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.IO;
using System.Text;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Import_UserViewImport:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
	}


	protected void btnUpload_Click(object sender,EventArgs e) {
		string sInstallDir;
		using (Sys oSys = new Sys()) {
			oSys.GetValue("INSTALL_DIR",out sInstallDir);
		}
		string sFileNm = string.Format("{0}\\Text\\{1}",sInstallDir,"UserView.csv");

		uplCsv.SaveAs(sFileNm);
		string sLine;
		using (StreamReader sr = new StreamReader(sFileNm,Encoding.GetEncoding("Shift_JIS"))) {
			while ((sLine = sr.ReadLine()) != null) {
				string[] sValues = sLine.Split('\t');
				string[] sDoc;
				int iDocCount;

				string sAllDoc = "";

				for (int i = 24;i < sValues.Length;i++) {
					sAllDoc += sValues[i];
				}
				SysPrograms.SeparateHtml(sAllDoc,SysConst.MAX_HTML_BLOCKS,out sDoc,out iDocCount);

				using (DbSession db = new DbSession()) {
					db.PrepareProcedure("USER_VIEW_IMPORT");
					db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,sValues[0]);
					db.ProcedureInParm("PPROGRAM_ROOT",DbSession.DbType.VARCHAR2,sValues[1]);
					db.ProcedureInParm("PPROGRAM_ID",DbSession.DbType.VARCHAR2,sValues[2]);
					db.ProcedureInParm("PUSER_AGENT_TYPE",DbSession.DbType.VARCHAR2,sValues[3]);
					db.ProcedureInParm("PAD_GROUP_CD",DbSession.DbType.VARCHAR2,sValues[4]);
					db.ProcedureInParm("PUSER_RANK",DbSession.DbType.VARCHAR2,sValues[5]);
					db.ProcedureInParm("PCATEGORY_INDEX",DbSession.DbType.NUMBER,sValues[6]);
					db.ProcedureInParm("PVIEW_KEY_MASK",DbSession.DbType.NUMBER,sValues[7]);
					db.ProcedureInParm("PPAGE_TITLE",DbSession.DbType.VARCHAR2,sValues[8]);
					db.ProcedureInParm("PPAGE_KEYWORD",DbSession.DbType.VARCHAR2,sValues[9]);
					db.ProcedureInParm("PPAGE_DESCRIPTION",DbSession.DbType.VARCHAR2,sValues[10]);
					db.ProcedureInParm("PTABLE_INDEX",DbSession.DbType.NUMBER,sValues[11]);
					db.ProcedureInParm("PCSS_FILE_NM1",DbSession.DbType.VARCHAR2,sValues[12]);
					db.ProcedureInParm("PCSS_FILE_NM2",DbSession.DbType.VARCHAR2,sValues[13]);
					db.ProcedureInParm("PCSS_FILE_NM3",DbSession.DbType.VARCHAR2,sValues[14]);
					db.ProcedureInParm("PCSS_FILE_NM4",DbSession.DbType.VARCHAR2,sValues[15]);
					db.ProcedureInParm("PCSS_FILE_NM5",DbSession.DbType.VARCHAR2,sValues[16]);
					db.ProcedureInParm("pJAVASCRIPT_FILE_NM1",DbSession.DbType.VARCHAR2,sValues[17]);
					db.ProcedureInParm("pJAVASCRIPT_FILE_NM2",DbSession.DbType.VARCHAR2,sValues[18]);
					db.ProcedureInParm("pJAVASCRIPT_FILE_NM3",DbSession.DbType.VARCHAR2,sValues[19]);
					db.ProcedureInParm("pJAVASCRIPT_FILE_NM4",DbSession.DbType.VARCHAR2,sValues[20]);
					db.ProcedureInParm("pJAVASCRIPT_FILE_NM5",DbSession.DbType.VARCHAR2,sValues[21]);
					db.ProcedureInParm("PHTML_DOC_SUB_SEQ",DbSession.DbType.NUMBER,sValues[22]);
					db.ProcedureInParm("PHTML_DOC_TITLE",DbSession.DbType.VARCHAR2,sValues[23]);
					db.ProcedureInArrayParm("PHTML_DOC",DbSession.DbType.VARCHAR2,iDocCount,sDoc);
					db.ProcedureInParm("PHTML_DOC_COUNT",DbSession.DbType.NUMBER,iDocCount);
					db.ProcedureOutParm("PHTML_DOC_SEQ",DbSession.DbType.VARCHAR2);
					db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
					db.ExecuteProcedure();
				}
			}
		}

	}
}
