﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 不達メールアドレス更新

--	Progaram ID		: ImportNgMailAddr
--
--  Creation Date	: 2010.04.12
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using ViComm;
using iBridCommLib;

public partial class Import_ImportNgMailAddr:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	protected void btnInputTxt_Click(object sender,EventArgs e) {

		string sInstallDir;
		using (Sys oSys = new Sys()) {
			oSys.GetValue("INSTALL_DIR",out sInstallDir);
		}
		string sFileName = string.Format("{0}\\Text\\{1}",sInstallDir,"NgMailAddr.txt");
		uplTxt.SaveAs(sFileName);

		string sLine;
		ArrayList alEmailAddr = new ArrayList();
		using (StreamReader sr = new StreamReader(sFileName,Encoding.GetEncoding("Shift_JIS"))) {
			while ((sLine = sr.ReadLine()) != null) {
				alEmailAddr.Add(sLine);
			}
		}
		string[] sEmailAddr = (string[])alEmailAddr.ToArray(typeof(string));

		int iNgCount = 0;
		int iNonExistCount = 0;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("IMPORT_NG_MAIL_ADDR");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PRECORD_COUNT",DbSession.DbType.NUMBER,sEmailAddr.Length);
			db.ProcedureInArrayParm("PEMAIL_ADDR",DbSession.DbType.VARCHAR2,sEmailAddr.Length,sEmailAddr);
			db.ProcedureOutParm("PNG_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PNON_EXIST_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			iNgCount = db.GetIntValue("PNG_COUNT");
			iNonExistCount = db.GetIntValue("PNON_EXIST_COUNT");
		}

		SetTxtFileName(uplTxt.FileName);
		SetCount(alEmailAddr.Count,iNgCount,iNonExistCount);
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	// ページを初期化する。
	private void InitPage() {
		ClearField();
		DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	// 項目を初期化する。
	private void ClearField() {
		SetTxtFileName("");
		SetCount(0,0,0);
	}

	// TXTファイル名を設定する。
	private void SetTxtFileName(string pTxtFileName) {
		string sTxtFileName;
		if (pTxtFileName == "") {
			sTxtFileName = "未指定";
		} else {
			sTxtFileName = pTxtFileName;
		}
		lblTxtFileName.Text = string.Format("ファイル名 {0}",sTxtFileName);
	}

	// 件数を設定する。
	private void SetCount(int pRecordCount,int pNgCount,int pNonExistCount) {
		lblCount.Text = string.Format("処理 {0:N0}件　該当 {1:N0}件　未存在フラグ更新 {2:N0}件",pRecordCount,pNgCount,pNonExistCount);
	}
}
