﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 入金サービスポイントインポート
--	Progaram ID		: ReceiptServicePointImport
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.IO;
using System.Text;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Import_ReceiptServicePointImport:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
	}


	protected void btnUpload_Click(object sender,EventArgs e) {
		string sInstallDir;
		using (Sys oSys = new Sys()) {
			oSys.GetValue("INSTALL_DIR",out sInstallDir);
		}
		string sFileNm = string.Format("{0}\\Text\\{1}",sInstallDir,"ReceitpServicePoint.csv");

		uplCsv.SaveAs(sFileNm);

		string sLine;
		using (StreamReader sr = new StreamReader(sFileNm,Encoding.GetEncoding("Shift_JIS"))) {
			while ((sLine = sr.ReadLine()) != null) {
				string[] sValues = sLine.Split('\t');

				using (DbSession db = new DbSession()) {
					db.PrepareProcedure("RECEIPT_SERVICE_POINT_IMPORT");
					db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,sValues[0]);
					db.ProcedureInParm("PUSER_RANK",DbSession.DbType.VARCHAR2,sValues[1]);
					db.ProcedureInParm("PSETTLE_TYPE",DbSession.DbType.VARCHAR2,sValues[2]);
					db.ProcedureInParm("PRECEIPT_AMT",DbSession.DbType.NUMBER,sValues[3]);
					db.ProcedureInParm("PADD_RATE0",DbSession.DbType.NUMBER,sValues[4]);
					db.ProcedureInParm("PADD_RATE1",DbSession.DbType.NUMBER,sValues[5]);
					db.ProcedureInParm("PADD_RATE2",DbSession.DbType.NUMBER,sValues[6]);
					db.ProcedureInParm("PADD_RATE",DbSession.DbType.NUMBER,sValues[7]);
					db.ProcedureInParm("PADD_RATE_CAMPAIN",DbSession.DbType.NUMBER,sValues[8]);
					db.ProcedureInParm("PAPPLICATION_START_DATE",DbSession.DbType.VARCHAR2,sValues[9]);
					db.ProcedureInParm("PAPPLICATION_END_DATE",DbSession.DbType.VARCHAR2,sValues[10]);
					db.ProcedureInParm("PCAMPAIN_APPLICATION_FLAG",DbSession.DbType.NUMBER,sValues[11]);
					db.ProcedureInParm("PPAYMENT_DELAY_FLAG",DbSession.DbType.NUMBER,sValues[12]);
					db.ProcedureInParm("PADD_POINT_FLAG",DbSession.DbType.NUMBER,sValues[13]);
					db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
					db.ExecuteProcedure();
				}
			}
		}

	}
}
