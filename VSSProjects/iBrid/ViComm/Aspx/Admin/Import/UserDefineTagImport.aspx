<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UserDefineTagImport.aspx.cs" Inherits="Import_UserDefineTagImport" Title="ユーザー定義タグ取得"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="ユーザー定義タグ取得"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[ファイル指定]</legend>
				<asp:Label ID="lblStatus1" runat="server" Text="ユーザー定義タグのアップロードを行います。下記のフォームにファイルを指定してください。"></asp:Label>
				<br />
				<asp:FileUpload ID="uplCsv" runat="server" Width="500px" />
				<asp:RequiredFieldValidator ID="valrUpload" runat="server" ControlToValidate="uplCsv" ErrorMessage="アップロードファイルを入力して下さい。" ValidationGroup="Upload" Text="*"></asp:RequiredFieldValidator>
				<br />
				<asp:Button ID="btnUpload" runat="server" Text="アップロード" ValidationGroup="Upload" Width="150px" OnClick="btnUpload_Click" />
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpload" ConfirmText="アップロードを行ないますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
