﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UserManImport.aspx.cs" Inherits="Import_UserManImport" Title="Untitled Page" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="会員情報インポート"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[ファイル指定]</legend>
				<table border="0" class="tableStyle" style="width: 1100px">
					<tr>
						<td class="tdHeaderStyle2">
							サイトコード
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" Width="170px" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD">
							</asp:DropDownList>
						</td>
					</tr>
					<asp:Repeater ID="rptManAttrType" runat="server" DataSource="<%# new Object[MAN_ATTR_TYPE_COUNT] %>">
						<ItemTemplate>
							<tr>
								<td class="tdHeaderStyle2">
									会員属性<%# Container.ItemIndex + 1 %>
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstManAttrType" runat="server" Width="170px" DataSource="<%# ManAttrTypeDs %>" DataTextField="MAN_ATTR_TYPE_NM" DataValueField="MAN_ATTR_TYPE_SEQ"
										AppendDataBoundItems="true">
										<asp:ListItem Text="指定なし" Value="" />
									</asp:DropDownList>
								</td>
							</tr>
						</ItemTemplate>
					</asp:Repeater>
				</table>
				<asp:Label ID="lblStatus1" runat="server" Text="会員情報のインポートを行います。下記のフォームにファイルを指定してください。"></asp:Label>
				<br />
				<asp:FileUpload ID="uplCsv" runat="server" Width="500px" />
				<asp:RequiredFieldValidator ID="valrUpload" runat="server" ControlToValidate="uplCsv" ErrorMessage="アップロードファイルを入力して下さい。" ValidationGroup="Upload" Text="*"></asp:RequiredFieldValidator>
				<br />
				<asp:Button ID="btnUpload" runat="server" Text="アップロード" ValidationGroup="Upload" Width="150px" OnClick="btnUpload_Click" />
				<br />
				<br />
				タブ(\t)区切りで下記フォーマットのファイルを指定してください。※[会員属性1～5]はリストから選択したものだけを入力してください。<br />
				<font color="red">ﾛｸﾞｲﾝﾊﾟｽﾜｰﾄﾞ&nbsp;&nbsp;&nbsp;&nbsp;ﾒｰﾙｱﾄﾞﾚｽ&nbsp;&nbsp;&nbsp;&nbsp;ﾎﾟｲﾝﾄ&nbsp;&nbsp;&nbsp;&nbsp;広告ｺｰﾄﾞ&nbsp;&nbsp;&nbsp;&nbsp;ﾊﾝﾄﾞﾙ名&nbsp;&nbsp;&nbsp;&nbsp;誕生日&nbsp;&nbsp;&nbsp;&nbsp;[会員属性1]&nbsp;&nbsp;&nbsp;&nbsp;[会員属性2]&nbsp;&nbsp;&nbsp;&nbsp;[会員属性3]&nbsp;&nbsp;&nbsp;&nbsp;[会員属性4]&nbsp;&nbsp;&nbsp;&nbsp;[会員属性5]
				</font>
			</fieldset>
		</asp:Panel>
		<br />
		<fieldset class="fieldset-inner">
			<legend>[実行履歴]</legend>
			<asp:Panel runat="server" ID="pnlHistory">
				<asp:GridView ID="grdHistory" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsHistory" AllowSorting="True" SkinID="GridViewColor"
					Font-Size="X-Small" Width="900px" OnRowCommand="grdHistory_RowCommand">
					<Columns>
						<asp:TemplateField HeaderText="実行履歴ID">
							<ItemTemplate>
								<asp:Label ID="lblSeq" runat="server" Text='<%#Eval("BULK_IMP_MAN_HISTORY_SEQ") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" />
							<HeaderStyle HorizontalAlign="Center" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="開始日時">
							<ItemTemplate>
								<asp:Label ID="lblStart" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "START_DATE", "{0:yyyy/MM/dd HH:MM:dd}") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" />
							<HeaderStyle HorizontalAlign="Center" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="終了日時">
							<ItemTemplate>
								<asp:Label ID="lblEndDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "END_DATE", "{0:yyyy/MM/dd HH:MM:dd}") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" />
							<HeaderStyle HorizontalAlign="Center" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="取込件数">
							<ItemTemplate>
								<asp:Label ID="lblImpCount" runat="server" Text='<%#Eval("IMP_COUNT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" />
							<HeaderStyle HorizontalAlign="Center" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="エラー件数">
							<ItemTemplate>
								<asp:Label ID="lblErrCount" runat="server" Text='<%#Eval("ERR_COUNT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" />
							<HeaderStyle HorizontalAlign="Center" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ステータス">
							<ItemTemplate>
								<asp:LinkButton ID="lnkStatus" runat="server" Text='<%#Eval("BULK_PROC_STATUS_NM") %>' Enabled='<%# IsErrorStatus(Eval("BULK_PROC_STATUS")) %>' CommandArgument='<%#Eval("BULK_IMP_MAN_HISTORY_SEQ") %>'
									CommandName="DOWNLOAD_ERROR_INFO"></asp:LinkButton>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" />
							<HeaderStyle HorizontalAlign="Center" />
						</asp:TemplateField>
					</Columns>
					<PagerSettings Mode="NumericFirstLast" />
				</asp:GridView>
			</asp:Panel>
		</fieldset>
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsHistory" runat="server" TypeName="BulkImpManHistory" SelectMethod="GetList" OnSelecting="dsHistory_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter DefaultValue="1" Name="pSexCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpload" ConfirmText="アップロードを行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="valrUpload" HighlightCssClass="validatorCallout" />
</asp:Content>
