﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastChargeListImport.aspx.cs" Inherits="Import_CastChargeListImport"
	Title="出演者課金設定取得" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="出演者課金設定取得"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[ファイル指定]</legend>
			<asp:Panel runat="server" ID="Panel1">
				<asp:Label ID="Label1" runat="server" Text="出演者課金設定の更新を行います。下記のフォームにファイルを指定してください。"></asp:Label>
				<br />
				<table class="tableStyle" border="0" style="width: 900px">
					<tr>
						<td class="tdDataStyle" colspan="2">
							<asp:FileUpload ID="uplTxt" runat="server" Width="500px" />
							<asp:RequiredFieldValidator ID="rfvTxt" runat="server" ControlToValidate="uplTxt" ErrorMessage="ファイルを入力して下さい。" ValidationGroup="InputTxt" Text="*"></asp:RequiredFieldValidator>
						</td>
					</tr>
				</table>
				<asp:Button ID="btnInputTxt" runat="server" Text="出演者課金設定更新" CssClass="seekbutton" OnClick="btnInputTxt_Click" ValidationGroup="InputTxt" />
				<asp:Button ID="btnClear" runat="server" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" CausesValidation="False" />
			</asp:Panel>
		</fieldset>
		<br />
		<asp:Panel ID="pnlTxtFileData" runat="server">
			<fieldset>
				<asp:Label ID="lblTxtFileName" runat="server"></asp:Label>
				<br />
				<asp:Label ID="lblReslut" runat="server"></asp:Label>
				<br />
				<asp:Label ID="lblErrLine" runat="server"></asp:Label>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" TargetControlID="rfvTxt" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnInputTxt" ConfirmText="出演者課金設定更新を行ないますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
