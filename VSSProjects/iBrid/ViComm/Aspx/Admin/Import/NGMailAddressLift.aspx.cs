﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ＮＧメールアドレス一括解除

--	Progaram ID		: NGMailAddressImport
--
--  Creation Date	: 2009.11.05
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.IO;
using System.Text;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Import_NGMailAddressLift:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
			this.btnManInvalidMailAddrLift.Text = DisplayWordUtil.Replace(this.btnManInvalidMailAddrLift.Text);
			this.btnCastInvalidMailAddrLift.Text = DisplayWordUtil.Replace(this.btnCastInvalidMailAddrLift.Text);
			this.ConfirmButtonExtender1.ConfirmText = DisplayWordUtil.Replace(this.ConfirmButtonExtender1.ConfirmText);
			this.ConfirmButtonExtender2.ConfirmText = DisplayWordUtil.Replace(this.ConfirmButtonExtender2.ConfirmText);
		}
	}

	private void InitPage() {

		DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}


	protected void btnCastInvalidMailAddrLift_Click(object sender,EventArgs e) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_INVALID_MAIL_ADDR_LIFT");
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	protected void btnManInvalidMailAddrLift_Click(object sender,EventArgs e) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MAN_INVALID_MAIL_ADDR_LIFT");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}
