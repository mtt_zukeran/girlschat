﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SelectedIDMailSend.aspx.cs" Inherits="Import_SelectedIDMailSend"
	Title="指定ID一括送信" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="指定ID一括送信"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[ファイル指定]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<asp:Label ID="lblStatus1" runat="server" Text="メール一括送信を行います。下記のフォームにファイルを指定してください。"></asp:Label>
				<br />
				<table border="0" style="width: 900px" class="tableStyle">
					<tr>
						<td class="tdHeaderSmallStyle2" style="width: 135px">
							サイト
						</td>
						<td class="tdDataStyle" style="width: 300px">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdDataStyle" colspan="2">
							<asp:FileUpload ID="uplCsv" runat="server" Width="500px" />
							<asp:RequiredFieldValidator ID="vdrUpload" runat="server" ControlToValidate="uplCsv" ErrorMessage="送信するIDファイルを入力して下さい。" ValidationGroup="Upload" Text="*"></asp:RequiredFieldValidator>
							<asp:CheckBox ID="chkNoPointUser" runat="server" Text="残ポイント０以下は除外する">
							</asp:CheckBox>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnInputCsv" Text="CSV入力" CssClass="seekbutton" OnClick="btnInputCsv_Click" ValidationGroup="Upload" />
			</asp:Panel>
			<asp:Panel runat="server" ID="pnlSubBtn">
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" CausesValidation="False" />
				<asp:Button runat="server" ID="btnInfoSetupMail" Text="お知らせメール送信設定" CssClass="seekbutton" OnClick="btnInfoSetupMail_Click" CausesValidation="False" />
				<asp:Button runat="server" ID="btnApproachSetupMail" Text="アプローチメール送信設定" CssClass="seekbutton" OnClick="btnApproachSetupMail_Click" CausesValidation="False" />
			</asp:Panel>
		</fieldset>
		<br />
		<asp:Panel ID="pnlCsvFileData" runat="server">
			<fieldset>
				<asp:Label ID="lblCsvFile" runat="server" Text='<%# string.Format("ファイル名 {0}", SetFilePath()) %>'></asp:Label>
				<a class="reccount">
					<%#GetCount()%>
				</a>
			</fieldset>
		</asp:Panel>
		<br />
		<asp:Panel runat="server" ID="pnlTxMail">
			<fieldset class="fieldset">
				<legend>[メール送信設定]</legend>
				<table border="0" class="tableStyle">
					<tr>
						<td class="tdHeaderSmallStyle">
							ﾒｰﾙ種別
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstMailTemplateNo" runat="server" Width="309px" DataSourceID="dsDMMail" DataTextField="TEMPLATE_NM" DataValueField="MAIL_TEMPLATE_NO">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderSmallStyle2">
							ｻｰﾋﾞｽP
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtServicePoint" runat="server" MaxLength="4" Width="30px"></asp:TextBox>Ｐ
							<asp:RequiredFieldValidator ID="vdrServicePoint" runat="server" ErrorMessage="サービスポイントを入力して下さい。" ControlToValidate="txtServicePoint" ValidationGroup="TxMail">*</asp:RequiredFieldValidator>
						</td>
						<td class="tdHeaderStyle2">
							ﾒｰﾙ有効時間
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtMailAvaHour" runat="server" MaxLength="3" Width="30px"></asp:TextBox>時間
							<asp:RequiredFieldValidator ID="vdrMailAvaHour" runat="server" ErrorMessage="メール有効時間を入力して下さい。" ControlToValidate="txtMailAvaHour" ValidationGroup="TxMail">*</asp:RequiredFieldValidator>
						</td>
						<td class="tdHeaderStyle2">
							Ｐ振替後有効時間
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtPointTransferAvaHour" runat="server" MaxLength="3" Width="30px"></asp:TextBox>時間
							<asp:RequiredFieldValidator ID="vdrPointTransferAvaHour" runat="server" ErrorMessage="ポイント振替後有効時間を入力して下さい。" ControlToValidate="txtPointTransferAvaHour"
								ValidationGroup="TxMail">*</asp:RequiredFieldValidator>
						</td>
					</tr>
					<tr runat="server" id="trCastLoginId">
						<td class="tdHeaderSmallStyle">
							<%= DisplayWordUtil.Replace("出演者ID") %>
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtCastLoginId" runat="server" MaxLength="8" Width="60px"></asp:TextBox>
							<asp:CustomValidator ID="vdcCastLoginId" runat="server" ControlToValidate="txtCastLoginId" ErrorMessage="出演者ログインＩＤが未登録です。" OnServerValidate="vdcCastLoginId_ServerValidate"
								ValidationGroup="TxMail" Display="Dynamic" ValidateEmptyText="True">*</asp:CustomValidator>
							<asp:Label ID="lblCastNm" runat="server" Text=""></asp:Label>
						</td>
						<td runat="server" id="tdHeaderCastCharNo" class="tdHeaderSmallStyle">
							ｷｬﾗｸﾀｰNo
						</td>
						<td runat="server" id="tdDataCastCharNo" class="tdDataStyle">
							<asp:TextBox ID="txtCastCharNo" runat="server" MaxLength="2" Width="35px"></asp:TextBox>
							<asp:CustomValidator ID="vdcCastCharNo" runat="server" ControlToValidate="txtCastCharNo" ErrorMessage="出演者キャラクターＮｏが未登録です。" ValidationGroup="TxMail" Display="Dynamic"
								ValidateEmptyText="True" OnServerValidate="vdcCastCharNo_ServerValidate">*</asp:CustomValidator>
						</td>
					</tr>
				</table>
				<asp:Label ID="lblAttention" runat="server" Text="メール有効時間/Ｐ振替後有効時間に０を設定すると無期限になります。" Font-Size="Small" ForeColor="Red"></asp:Label><br />
				<asp:Button runat="server" ID="btnTxMail" Text="メール送信" CssClass="seekbutton" OnClick="btnTxMail_Click" ValidationGroup="TxMail" />
				<asp:Button runat="server" ID="btnCancelTx" Text="中止" CssClass="seekbutton" CausesValidation="False" OnClick="btnCancelTx_Click" />
			</fieldset>
		</asp:Panel>
		<asp:GridView ID="grdLoginID" runat="server" AutoGenerateColumns="False" SkinID="GridView" Font-Size="X-Small" Width="600px" Visible="False">
			<Columns>
				<asp:BoundField DataField="LOGIN_ID" HeaderText="ログインID" />
				<asp:BoundField DataField="USER_SEQ" HeaderText="ユーザーSEQ" />
				<asp:BoundField DataField="BAL_POINT" HeaderText="ポイント" />
				<asp:BoundField DataField="OK_IDCNT" HeaderText="OK数" />
				<asp:BoundField DataField="NG_IDCNT" HeaderText="NG数" />
				<asp:BoundField DataField="NO_POINTCNT" HeaderText="NoPoint数" />
			</Columns>
		</asp:GridView>
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsDMMail" runat="server" OnSelecting="dsDMMail_Selecting" SelectMethod="GetListByTemplateType" TypeName="MailTemplate">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pMailTemplateType" Type="String" />
			<asp:Parameter DefaultValue="1" Name="pSexCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrUpload" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrServicePoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrMailAvaHour" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdrPointTransferAvaHour" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdcCastLoginId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdcCastCharNo" HighlightCssClass="validatorCallout" />
</asp:Content>
