﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 登録不可番号インポート
--	Progaram ID		: UnableRegistNoImport
--
--  Creation Date	: 2010.04.05
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.IO;
using System.Text;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Import_UnableRegistNoImport:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
	}


	protected void btnUpload_Click(object sender,EventArgs e) {
		string sInstallDir;
		using (Sys oSys = new Sys()) {
			oSys.GetValue("INSTALL_DIR",out sInstallDir);
		}
		string sFileNm = string.Format("{0}\\Text\\{1}",sInstallDir,"UnableRegistNo.csv");

		uplCsv.SaveAs(sFileNm);
		string sLine;
		using (StreamReader sr = new StreamReader(sFileNm,Encoding.GetEncoding("Shift_JIS"))) {
			while ((sLine = sr.ReadLine()) != null) {
				string[] sValues = sLine.Split('\t');

				using (DbSession db = new DbSession()) {
					db.PrepareProcedure("UNABLE_REGIST_NO_IMPORT");
					db.ProcedureInParm("PNO_TYPE",DbSession.DbType.VARCHAR2,sValues[0]);
					db.ProcedureInParm("PTERMINAL_UNIQUE_ID_OR_TEL",DbSession.DbType.VARCHAR2,sValues[1]);
					db.ProcedureInParm("PREMARKS",DbSession.DbType.VARCHAR2,sValues[2]);
					db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
					db.ExecuteProcedure();
				}
			}
		}
	}
}
