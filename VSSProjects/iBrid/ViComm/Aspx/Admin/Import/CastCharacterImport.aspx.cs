﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者情報インポート
--	Progaram ID		: CastCharacterImport
--
--  Creation Date	: 2010.10.15
--  Creater			: 
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.IO;
using System.Text;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Oracle.DataAccess.Client;

using iBridCommLib;
using ViComm;

/// <summary>
/// 出演者情報インポート画面
/// </summary>
/// <remarks>
/// 出演者情報のインポート機能を提供する画面
/// </remarks>
public partial class Import_CastCharacterImport:System.Web.UI.Page {
	
	#region □■□ 定数等 □■□ ======================================================================================
	
	// 処理対象の属性件数
	protected const int CAST_ATTR_TYPE_COUNT = 5;
	// ファイルセパレーター
	private const char SEPARATOR = '\t';
	// ファイルエンコード
	private static readonly Encoding DefaultEncoding = Encoding.GetEncoding("Shift_JIS");
	
	#endregion ========================================================================================================


	#region □■□ プロパティ □■□ ==================================================================================

	protected DataSet CastAttrTypeDs {
		get { return (DataSet) this.ViewState["CastAttrTypeDs"]; }
		set { this.ViewState["CastAttrTypeDs"] = value; }
	}
	
	protected string MenuSite{
		get { return this.Session["MENU_SITE"] as string ?? string.Empty; }
	}
	
	#endregion ========================================================================================================	
	
	
	#region □■□ イベントハンドラ □■□ ============================================================================
	
	protected void Page_Load(object sender, EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}
	
	/// <summary>
	/// アップロード処理の実行
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnUpload_Click(object sender, EventArgs e) {
		if (!this.IsValid) return;
		
		using(BulkImpManHistory oBulkImpManHistory = new BulkImpManHistory()){
			string sSiteCd = this.lstSiteCd.SelectedValue;
			string sCastAttrSeq01 = GetCastAttrTypeSelectedValue(0);
			string sCastAttrSeq02 = GetCastAttrTypeSelectedValue(1);
			string sCastAttrSeq03 = GetCastAttrTypeSelectedValue(2);
			string sCastAttrSeq04 = GetCastAttrTypeSelectedValue(3);
			string sCastAttrSeq05 = GetCastAttrTypeSelectedValue(4);
			
			// SEQ採番
			decimal dSeq = oBulkImpManHistory.GetSeq();			
			string sFileNm = string.Format("{0}.tsv",iBridUtil.addZero(dSeq.ToString(),ViCommConst.OBJECT_NM_LENGTH));
			string sFilePath = Path.Combine(this.GetTextDir(), sFileNm);
			
			try{
				uplCsv.SaveAs(sFilePath);
				
				// 一括会員取込履歴を作成
				// 男性会員Importを代用
				using (DbSession db = new DbSession()) {
					db.PrepareProcedure("REGIST_BULK_IMP_MAN_HISTORY");
					db.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, sSiteCd);
					db.ProcedureInParm("pBULK_IMP_MAN_HISTORY_SEQ", DbSession.DbType.NUMBER, dSeq);
					db.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,ViCommConst.OPERATOR);
					db.ProcedureInParm("pMAN_ATTR_SEQ01", DbSession.DbType.VARCHAR2, sCastAttrSeq01);
					db.ProcedureInParm("pMAN_ATTR_SEQ02", DbSession.DbType.VARCHAR2, sCastAttrSeq02);
					db.ProcedureInParm("pMAN_ATTR_SEQ03", DbSession.DbType.VARCHAR2, sCastAttrSeq03);
					db.ProcedureInParm("pMAN_ATTR_SEQ04", DbSession.DbType.VARCHAR2, sCastAttrSeq04);
					db.ProcedureInParm("pMAN_ATTR_SEQ05", DbSession.DbType.VARCHAR2, sCastAttrSeq05);
					db.ProcedureInParm("pMAN_ATTR_SEQ06", DbSession.DbType.VARCHAR2, null);// 予備
					db.ProcedureInParm("pMAN_ATTR_SEQ07", DbSession.DbType.VARCHAR2, null);// 予備
					db.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);
					db.ExecuteProcedure();
				}
				
				// 添付テーブルにインポート
				this.Import2Temp(sFilePath,sSiteCd,dSeq);

				using (DbSession db = new DbSession()) {
					db.PrepareProcedure("BULK_IMPORT_CAST");
					db.ProcedureInParm("pBULK_IMP_MAN_HISTORY_SEQ", DbSession.DbType.NUMBER, dSeq);
					db.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);
					db.ExecuteProcedure();
				}
				this.RefreshGrid();

				// 添付テーブルから実テーブルへ反映
			}catch{
				// 一括会員取込履歴を更新(ｽﾃｰﾀｽを失敗に)
				
				throw;
			}finally{			
				if(File.Exists(sFilePath)){
					// ファイル削除が必要になったらコメントはずす
					//File.Delete(sFilePath);
				}
			}

		}

	}

	protected void grdHistory_RowCommand(object sender, GridViewCommandEventArgs e) {
		if (e.CommandName.Equals("DOWNLOAD_ERROR_INFO")) {
			decimal dSeq = decimal.Parse((string)e.CommandArgument);
			
			using(BulkImpManData oBulkImpManData = new BulkImpManData()){
				using(DataSet oErrorDs = oBulkImpManData.GetErrorList(dSeq)){
					StringBuilder oFileBuilder = new StringBuilder();
					
					foreach(DataRow oDr in oErrorDs.Tables[0].Rows){
						oFileBuilder.Append(oDr["TEXT_LINE_NO"]);
						oFileBuilder.Append("\t");
						oFileBuilder.Append(oDr["ERROR_MSG"]);
						oFileBuilder.Append("\t");
						oFileBuilder.Append(oDr["TEXT_DTL"]);
						oFileBuilder.Append("\t");
						oFileBuilder.AppendLine();						
					}

					Response.ClearContent();
					Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.txt",iBridUtil.addZero(dSeq.ToString(),ViCommConst.OBJECT_NM_LENGTH)));
					Response.ContentType = "application/octet-stream-dummy";
					Response.Write(oFileBuilder.ToString());
					Response.End();
				}
			}
		}
	}

	protected void dsHistory_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = this.lstSiteCd.SelectedValue;
	}
	
	#endregion ========================================================================================================
	
	protected bool IsErrorStatus(object pStatus){
		return object.Equals(pStatus,ViCommConst.BulkProcStatus.ERROR) || object.Equals(pStatus,ViCommConst.BulkProcStatus.SUCCESS_WITH_ERROR);
	}
	
	private void Import2Temp(string pFilePath,string pSiteCd, decimal pSeq){
		using (StreamReader oStreamReader = new StreamReader(pFilePath, DefaultEncoding)) {

			// ここは例外的にアプリでトランザクション管理を行う。
			using (DbSession oDbSession = new DbSession()) {
				OracleTransaction oTransaction = null;
				try {
					oDbSession.conn = oDbSession.DbConnect();
					oTransaction = oDbSession.conn.BeginTransaction();

					int iLineNumber = 1;
					string sLine = null;
					while (oStreamReader.Peek() > -1) {
						sLine = oStreamReader.ReadLine();

						// 男性会員Importを代用
						oDbSession.PrepareProcedure("REGIST_BULK_IMP_MAN_DATA");
						oDbSession.ProcedureInParm("pNO_TRAN", DbSession.DbType.NUMBER, ViCommConst.FLAG_ON);
						oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, pSiteCd);
						oDbSession.ProcedureInParm("pBULK_IMP_MAN_HISTORY_SEQ", DbSession.DbType.NUMBER, pSeq);
						oDbSession.ProcedureInParm("pTEXT_LINE_NO", DbSession.DbType.NUMBER, iLineNumber);
						oDbSession.ProcedureInParm("pTEXT_DTL", DbSession.DbType.VARCHAR2, sLine);
						oDbSession.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);
						oDbSession.ExecuteProcedure();

						iLineNumber += 1;
					}
					oTransaction.Commit();
				} catch {
					if (oTransaction != null) {
						oTransaction.Rollback();
					}
					throw;
				} finally {
					oDbSession.conn.Close();
				}
			}
		}
	}
	
	private void RefreshGrid(){
		this.grdHistory.DataBind();
	}
	
	private string GetCastAttrTypeSelectedValue(int pIndex){
		if(pIndex >= this.rptCastAttrType.Items.Count) return null;
		
		DropDownList lstCastAttrType = this.rptCastAttrType.Items[pIndex].FindControl("lstCastAttrType") as DropDownList;
		return (lstCastAttrType == null ) ? null:lstCastAttrType.SelectedValue;
	}
	
	private string GetTextDir(){
		string sInstallDir = null;
		using (Sys oSys = new Sys()) {
			oSys.GetValue("INSTALL_DIR", out sInstallDir);
		}
		return Path.Combine(sInstallDir,"Text");
	}

	private void InitPage() {
		//
		// サイトDropDownList 初期化
		//
		lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(MenuSite)) {
			lstSiteCd.SelectedValue = this.MenuSite;
		}
		//
		// 会員属性DropDownList
		//
		string sSiteCd = this.lstSiteCd.SelectedValue;
		using (CastAttrType oCastAttrType = new CastAttrType()) {
			this.CastAttrTypeDs = oCastAttrType.GetList(sSiteCd);
		}
		this.rptCastAttrType.DataBind();
		
		this.RefreshGrid();
	}


}
