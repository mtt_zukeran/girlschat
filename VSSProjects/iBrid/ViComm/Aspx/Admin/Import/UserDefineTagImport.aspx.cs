﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ユーザー定義タグインポート
--	Progaram ID		: UserDefineTagImport
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.IO;
using System.Text;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Import_UserDefineTagImport:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
	}


	protected void btnUpload_Click(object sender,EventArgs e) {
		string sInstallDir;
		using (Sys oSys = new Sys()) {
			oSys.GetValue("INSTALL_DIR",out sInstallDir);
		}

		string sFileNm = string.Format("{0}\\Text\\{1}",sInstallDir,"UserDefineTag.csv");
		uplCsv.SaveAs(sFileNm);
		string sLine;
		using (StreamReader sr = new StreamReader(sFileNm,Encoding.GetEncoding("Shift_JIS"))) {
			while ((sLine = sr.ReadLine()) != null) {
				string[] sValues = sLine.Split('\t');
				string[] sDoc;
				int iDocCount;

				string sAllDoc = "";

				for (int i = 4;i < sValues.Length;i++) {
					sAllDoc += sValues[i];
				}
				SysPrograms.SeparateHtml(sAllDoc,SysConst.MAX_HTML_BLOCKS,out sDoc,out iDocCount);

				using (DbSession db = new DbSession()) {
					db.PrepareProcedure("USER_DEFINE_TAG_IMPORT");
					db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,sValues[0]);
					db.ProcedureInParm("PVARIABLE_ID",DbSession.DbType.VARCHAR2,sValues[1]);
					db.ProcedureInParm("PUSER_AGENT_TYPE",DbSession.DbType.VARCHAR2,sValues[2]);
					db.ProcedureInParm("PVARIABLE_NM",DbSession.DbType.VARCHAR2,sValues[3]);
					db.ProcedureInArrayParm("PHTML_DOC",DbSession.DbType.VARCHAR2,iDocCount,sDoc);
					db.ProcedureInParm("PHTML_DOC_COUNT",DbSession.DbType.NUMBER,iDocCount);
					db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
					db.ExecuteProcedure();
				}
			}
		}

	}
}
