﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メールテンプレートインポート
--	Progaram ID		: MailTemplateImport
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.IO;
using System.Text;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Import_MailTemplateImport:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
	}


	protected void btnUpload_Click(object sender,EventArgs e) {
		string sInstallDir;
		using (Sys oSys = new Sys()) {
			oSys.GetValue("INSTALL_DIR",out sInstallDir);
		}
		string sFileNm = string.Format("{0}\\Text\\{1}",sInstallDir,"MailTemplate.csv");

		uplCsv.SaveAs(sFileNm);
		string sLine;
		using (StreamReader sr = new StreamReader(sFileNm,Encoding.GetEncoding("Shift_JIS"))) {
			while ((sLine = sr.ReadLine()) != null) {
				string[] sValues = sLine.Split('\t');
				string[] sDoc;
				string[] sTextDoc;
				int iDocCount;
				int iTextDocCount;

				string sAllDoc = string.Empty;
				string sTextAllDoc = string.Empty;

				for (int i = 19;i < sValues.Length;i++) {
					if (i < 19 + SysConst.MAX_HTML_BLOCKS / 2) {
						sAllDoc += sValues[i];
					} else {
						sTextAllDoc += sValues[i].Replace("<br />",Environment.NewLine);
					}
				}
				SysPrograms.SeparateHtml(sAllDoc,SysConst.MAX_HTML_BLOCKS / 2,out sDoc,out iDocCount);
				SysPrograms.SeparateHtml(sTextAllDoc,SysConst.MAX_HTML_BLOCKS / 2,out sTextDoc,out iTextDocCount);

				List<string> oDocList = new List<string>();
				oDocList.AddRange(sDoc);
				oDocList.AddRange(sTextDoc);

				using (DbSession db = new DbSession()) {
					db.PrepareProcedure("MAIL_TEMPLATE_IMPORT");
					db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,sValues[0]);
					db.ProcedureInParm("PMAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,sValues[1]);
					db.ProcedureInParm("PMAIL_TEMPLATE_TYPE",DbSession.DbType.VARCHAR2,sValues[2]);
					db.ProcedureInParm("PCAST_CREATE_ORG_SUB_SEQ",DbSession.DbType.VARCHAR2,sValues[3]);
					db.ProcedureInParm("PMAIL_ATTACHED_OBJ_TYPE",DbSession.DbType.NUMBER,sValues[12]);
					db.ProcedureInParm("PWEB_FACE_SEQ",DbSession.DbType.VARCHAR2,sValues[4]);
					db.ProcedureInParm("PMOBILE_MAIL_FONT_SIZE",DbSession.DbType.VARCHAR2,sValues[5]);
					db.ProcedureInParm("PMOBILE_MAIL_BACK_COLOR",DbSession.DbType.VARCHAR2,sValues[6]);
					db.ProcedureInParm("PMAIL_TITLE",DbSession.DbType.VARCHAR2,sValues[7]);
					db.ProcedureInParm("PEMAIL_ADDR",DbSession.DbType.VARCHAR2,sValues[8]);
					db.ProcedureInParm("PTEMPLATE_NM",DbSession.DbType.VARCHAR2,sValues[9]);
					db.ProcedureInParm("PHTML_DOC_SUB_SEQ",DbSession.DbType.NUMBER,sValues[10]);
					db.ProcedureInParm("PHTML_DOC_TITLE",DbSession.DbType.VARCHAR2,sValues[11]);
					db.ProcedureInArrayParm("PHTML_DOC",DbSession.DbType.VARCHAR2,oDocList.Count,oDocList.ToArray());
					db.ProcedureInParm("PHTML_DOC_COUNT",DbSession.DbType.NUMBER,oDocList.Count);
					db.ProcedureInParm("PNON_DISP_IN_MAIL_BOX",DbSession.DbType.NUMBER,sValues[13]);
					db.ProcedureInParm("PNON_DISP_IN_DECOMAIL",DbSession.DbType.NUMBER,sValues[14]);
					db.ProcedureInParm("PSEX_CD",DbSession.DbType.VARCHAR2,sValues[15]);
					db.ProcedureInParm("PMAIL_ATTACHED_METHOD",DbSession.DbType.VARCHAR2,sValues[16]);
					db.ProcedureInParm("PTEXT_MAIL_FLAG",DbSession.DbType.NUMBER,sValues[17]);
					db.ProcedureInParm("PFORCE_TX_MOBILE_MAIL_FLAG",DbSession.DbType.NUMBER,sValues[18]);
					db.ProcedureOutParm("PHTML_DOC_SEQ",DbSession.DbType.VARCHAR2);
					db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
					db.ExecuteProcedure();
				}
			}
		}
	}
}
