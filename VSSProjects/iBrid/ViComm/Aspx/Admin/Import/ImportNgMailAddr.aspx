﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ImportNgMailAddr.aspx.cs" Inherits="Import_ImportNgMailAddr"
	Title="不達メールアドレス更新" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="不達メールアドレス更新"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[ファイル指定]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<asp:Label ID="lblStatus1" runat="server" Text="不達メールアドレス更新を行います。下記のフォームにファイルを指定してください。"></asp:Label>
				<br />
				<table class="tableStyle" border="0" style="width: 900px">
					<tr>
						<td class="tdHeaderSmallStyle2" style="width: 135px">
							サイト
						</td>
						<td class="tdDataStyle" style="width: 300px">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdDataStyle" colspan="2">
							<asp:FileUpload ID="uplTxt" runat="server" Width="500px" />
							<asp:RequiredFieldValidator ID="rfvTxt" runat="server" ControlToValidate="uplTxt" ErrorMessage="ファイルを入力して下さい。" ValidationGroup="InputTxt" Text="*"></asp:RequiredFieldValidator>
						</td>
					</tr>
				</table>
				<asp:Button ID="btnInputTxt" runat="server" Text="不達メールアドレス更新" CssClass="seekbutton" OnClick="btnInputTxt_Click" ValidationGroup="InputTxt" />
				<asp:Button ID="btnClear" runat="server" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" CausesValidation="False" />
			</asp:Panel>
		</fieldset>
		<br />
		<asp:Panel ID="pnlTxtFileData" runat="server">
			<fieldset>
				<asp:Label ID="lblTxtFileName" runat="server"></asp:Label>
				<a class="reccount">
					<asp:Label ID="lblCount" runat="server"></asp:Label>
				</a>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" TargetControlID="rfvTxt" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnInputTxt" ConfirmText="不達メールアドレス更新を行ないますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
