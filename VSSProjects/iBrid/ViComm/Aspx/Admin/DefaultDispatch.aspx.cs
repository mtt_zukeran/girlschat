﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;

public partial class DefaultDispatch:System.Web.UI.Page {
	protected void Page_Load(object sender,EventArgs e) {
		switch (Session["AdminType"].ToString()) {
			case ViCommConst.RIGHT_SYS_OWNER:
				Response.Redirect("~/System/DefaultSystem.aspx");
				break;
			case ViCommConst.RIGHT_SITE_OWNER:
				Response.Redirect("~/StatusManager/DefaultStatus.aspx");
				break;
			case ViCommConst.RIGHT_SITE_MANAGER:
				Response.Redirect("~/StatusManager/DefaultStatus.aspx");
				break;
			case ViCommConst.RIGHT_STAFF:
				Response.Redirect("~/StatusManager/DefaultStatus.aspx");
				break;
			case ViCommConst.RIGHT_LOCAL_STAFF:
				Response.Redirect("~/Cast/DefaultCast.aspx");
				break;
			case ViCommConst.RIGHT_PRODUCTION:
				Response.Redirect("~/Cast/DefaultCast.aspx");
				break;
			case ViCommConst.RIGHT_AD_MANAGE:
				Response.Redirect("~/AdManage/DefaultAdManage.aspx");
				break;
		}
	}
}
