/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 入金サービスポイント
--	Progaram ID		: ReceiptServicePoint
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class ReceiptServicePoint:DbSession {

	public ReceiptServicePoint() {
	}

	public int GetPageCount(string pSiteCd) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_RECEIPT_SERVICE_POINT01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();


			string sOrder = "ORDER BY  SITE_CD,USER_RANK,SETTLE_TYPE,RECEIPT_AMT ";
			string sSql = "SELECT " +
							"SITE_CD				," +
							"USER_RANK				," +
							"SETTLE_TYPE			," +
							"RECEIPT_AMT			," +
							"ADD_RATE0				," +
							"ADD_RATE1				," +
							"ADD_RATE2				," +
							"ADD_RATE				," +
							"ADD_RATE_CAMPAIN		," +
							"APPLICATION_START_DATE	," +
							"APPLICATION_END_DATE	," +
							"CAMPAIN_APPLICATION_FLAG," +
							"PAYMENT_DELAY_FLAG		," +
							"ADD_POINT_FLAG			," +
							"REVISION_NO			," +
							"UPDATE_DATE			," +
							"USER_RANK_NM			," +
							"SITE_NM				," +
							"SETTLE_TYPE_NM " +
							"FROM(" +
							" SELECT VW_RECEIPT_SERVICE_POINT01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_RECEIPT_SERVICE_POINT01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_RECEIPT_SERVICE_POINT01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	public DataSet GetCsvData(string pSiteCd) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();
			string sOrder = "ORDER BY SITE_CD,USER_RANK,SETTLE_TYPE,RECEIPT_AMT ";
			string sSql = "SELECT " +
							"SITE_CD					," +
							"USER_RANK					," +
							"SETTLE_TYPE				," +
							"RECEIPT_AMT				," +
							"ADD_RATE0					," +
							"ADD_RATE1					," +
							"ADD_RATE2					," +
							"ADD_RATE					," +
							"ADD_RATE_CAMPAIN			," +
							"TO_CHAR(APPLICATION_START_DATE,'YYYYMMDDHH24MISS') AS APPLICATION_START_DATE," +
							"TO_CHAR(APPLICATION_END_DATE,'YYYYMMDDHH24MISS') AS APPLICATION_END_DATE," +
							"CAMPAIN_APPLICATION_FLAG	," +
							"PAYMENT_DELAY_FLAG			," +
							"ADD_POINT_FLAG		" +
							"FROM " +
							 "VW_RECEIPT_SERVICE_POINT01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql = sSql + sWhere;
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_RECEIPT_SERVICE_POINT01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		pWhere = pWhere + " WHERE SITE_CD = :SITE_CD  ";
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}


}
