﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 抽選
--	Progaram ID		: Lottery
--
--  Creation Date	: 2011.07.19
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;


public class Lottery : DbSession {

	public Lottery() {
	}

	public int GetPageCount(string pSiteCd,string pSexCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_LOTTERY		L	,");
		oSqlBuilder.AppendLine("	T_GAME_ITEM		GI	");

		string sWhereClause;
		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pSexCd,out sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		}
		finally {
			conn.Close();
		}
		return iPageCount;
	}

	public int GetPageCount_2(string pSiteCd,string pSexCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT					");
		oSqlBuilder.AppendLine("	COUNT(*)			");
		oSqlBuilder.AppendLine("FROM					");
		oSqlBuilder.AppendLine("	T_LOTTERY_NOT_COMP	");

		string sWhereClause;
		OracleParameter[] oWhereParams = this.CreateWhereNotComp(pSiteCd,pSexCd,out sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pSexCd,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	L.SITE_CD						,");
		oSqlBuilder.AppendLine("	L.LOTTERY_SEQ					,");
		oSqlBuilder.AppendLine("	L.LOTTERY_NM					,");
		oSqlBuilder.AppendLine("	L.TICKET_NM						,");
		oSqlBuilder.AppendLine("	L.SEX_CD						,");
		oSqlBuilder.AppendLine("	L.COMPLETE_GET_ITEM_SEQ			,");
		oSqlBuilder.AppendLine("	GI.GAME_ITEM_NM					,");
		oSqlBuilder.AppendLine("	L.PUBLISH_START_DATE			,");
		oSqlBuilder.AppendLine("	L.PUBLISH_END_DATE				,");
		oSqlBuilder.AppendLine("	L.PUBLISH_FLAG					,");
		oSqlBuilder.AppendLine("	L.REVISION_NO					,");
		oSqlBuilder.AppendLine("	L.UPDATE_DATE					");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_LOTTERY		L	,");
		oSqlBuilder.AppendLine("	T_GAME_ITEM		GI	");

		string sWhereClause;
		string sPagingSql;
		string sSortExpression = "ORDER BY L.SITE_CD, L.PUBLISH_START_DATE DESC";

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pSexCd,out sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		}
		finally {
			conn.Close();
		}
		return oDs;
	}

	public DataSet GetPageCollection_2(string pSiteCd,string pSexCd,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	SITE_CD						,");
		oSqlBuilder.AppendLine("	LOTTERY_SEQ					,");
		oSqlBuilder.AppendLine("	LOTTERY_NM					,");
		oSqlBuilder.AppendLine("	SEX_CD						,");
		oSqlBuilder.AppendLine("	FIRST_BUY_FREE_START_DATE	,");
		oSqlBuilder.AppendLine("	FIRST_BUY_FREE_END_DATE		,");
		oSqlBuilder.AppendLine("	PUBLISH_START_DATE			,");
		oSqlBuilder.AppendLine("	PUBLISH_END_DATE			,");
		oSqlBuilder.AppendLine("	PRICE						,");
		oSqlBuilder.AppendLine("	REVISION_NO					,");
		oSqlBuilder.AppendLine("	UPDATE_DATE					 ");
		oSqlBuilder.AppendLine("FROM							 ");
		oSqlBuilder.AppendLine("	T_LOTTERY_NOT_COMP			 ");

		string sWhereClause;
		string sPagingSql;
		string sSortExpression = "ORDER BY SITE_CD, PUBLISH_START_DATE DESC";

		OracleParameter[] oWhereParams = this.CreateWhereNotComp(pSiteCd,pSexCd,out sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pSexCd,out string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();

		SysPrograms.SqlAppendWhere("L.SITE_CD = GI.SITE_CD AND L.COMPLETE_GET_ITEM_SEQ = GI.GAME_ITEM_SEQ",ref pWhere);

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("L.SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}
		if (!string.IsNullOrEmpty(pSexCd)) {
			SysPrograms.SqlAppendWhere("L.SEX_CD = :SEX_CD",ref pWhere);
			list.Add(new OracleParameter("SEX_CD",pSexCd));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	private OracleParameter[] CreateWhereNotComp(string pSiteCd,string pSexCd,out string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}
		if (!string.IsNullOrEmpty(pSexCd)) {
			SysPrograms.SqlAppendWhere("SEX_CD = :SEX_CD",ref pWhere);
			list.Add(new OracleParameter("SEX_CD",pSexCd));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public bool IsDuplicateDate(string pSiteCd,string pLotterySeq,string pSexCd,DateTime pPublishStartDate,DateTime pPublishEndDate) {
		bool bDuplicate = false;
		try {
			conn = DbConnect();
			DataSet ds;
			StringBuilder sSql = new StringBuilder();
			sSql.AppendLine("SELECT ");
			sSql.AppendLine("   LOTTERY_SEQ ");
			sSql.AppendLine("FROM ");
			sSql.AppendLine("   T_LOTTERY ");
			sSql.AppendLine("WHERE ");
			sSql.AppendLine("	SITE_CD				= :SITE_CD				AND ");
			sSql.AppendLine("	SEX_CD      		= :SEX_CD   			AND ");
			sSql.AppendLine("	((PUBLISH_START_DATE>= :PUBLISH_START_DATE1	AND	");
			sSql.AppendLine("	 PUBLISH_START_DATE <= :PUBLISH_END_DATE1)	OR	");
			sSql.AppendLine("	(PUBLISH_END_DATE	>= :PUBLISH_START_DATE2	AND	");
			sSql.AppendLine("	 PUBLISH_END_DATE	<= :PUBLISH_END_DATE2)	OR	");
			sSql.AppendLine("	(PUBLISH_START_DATE <= :PUBLISH_START_DATE3	AND	");
			sSql.AppendLine("	 PUBLISH_END_DATE	>= :PUBLISH_END_DATE3))	AND	");
			sSql.AppendLine("	LOTTERY_SEQ			!= :LOTTERY_SEQ		");
			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					using (ds = new DataSet()) {
						cmd.Parameters.Add("SITE_CD",pSiteCd);
						cmd.Parameters.Add("SEX_CD",pSexCd);
						cmd.Parameters.Add("PUBLISH_START_DATE1",OracleDbType.Date,pPublishStartDate,ParameterDirection.Input);
						cmd.Parameters.Add("PUBLISH_END_DATE1",OracleDbType.Date,pPublishEndDate,ParameterDirection.Input);
						cmd.Parameters.Add("PUBLISH_START_DATE2",OracleDbType.Date,pPublishStartDate,ParameterDirection.Input);
						cmd.Parameters.Add("PUBLISH_END_DATE2",OracleDbType.Date,pPublishEndDate,ParameterDirection.Input);
						cmd.Parameters.Add("PUBLISH_START_DATE3",OracleDbType.Date,pPublishStartDate,ParameterDirection.Input);
						cmd.Parameters.Add("PUBLISH_END_DATE3",OracleDbType.Date,pPublishEndDate,ParameterDirection.Input);
						cmd.Parameters.Add("LOTTERY_SEQ",pLotterySeq);
						da.Fill(ds);
						if (ds.Tables[0].Rows.Count != 0) {
							bDuplicate = true;
						}
					}
				}
			}
		}
		finally {
			conn.Close();
		}
		return bDuplicate;
	}

	public DataSet GetList(string pSiteCd,string pSexCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	SITE_CD					,	");
		oSqlBuilder.AppendLine("	TICKET_NM				,	");
		oSqlBuilder.AppendLine("	LOTTERY_SEQ					");
		oSqlBuilder.AppendLine("FROM							");
		oSqlBuilder.AppendLine("	T_LOTTERY					");
		oSqlBuilder.AppendLine("WHERE							");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD		AND	");
		oSqlBuilder.AppendLine("	SEX_CD	= :SEX_CD			");
		oSqlBuilder.AppendLine("ORDER BY						");
		oSqlBuilder.AppendLine("	SITE_CD, LOTTERY_SEQ		");

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":SEX_CD",pSexCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}
}
