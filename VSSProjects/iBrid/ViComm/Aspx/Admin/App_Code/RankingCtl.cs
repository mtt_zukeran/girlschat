﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: RankingCtl
--	Title			: ランキング集計設定
--	Progaram ID		: RankingCtl
--
--  Creation Date	: 2011.04.05
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater   Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Text;
using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class RankingCtl:DbSession {

	public RankingCtl() {
	}

	public DataSet GetPageCollection(string pSiteCd, string pSummaryType, int startRowIndex,int maximumRows) {

		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();
			StringBuilder oSql = new StringBuilder();

			string sOrder = "ORDER BY SITE_CD,SUMMARY_START_DATE DESC";
			oSql.AppendLine("SELECT ");
			oSql.AppendLine("	SITE_CD				,	");
			oSql.AppendLine("	SUMMARY_TYPE		,	");
			oSql.AppendLine("	RANKING_CTL_SEQ		,	");
			oSql.AppendLine("	SUMMARY_START_DATE	,	");
			oSql.AppendLine("	SUMMARY_END_DATE	,	");
			oSql.AppendLine("	REMARKS				,	");
			oSql.AppendLine("	UPDATE_DATE				");
			oSql.AppendLine("FROM(");
			oSql.AppendLine(" SELECT T_RANKING_CTL.*, ROW_NUMBER() OVER ( " + sOrder + ") AS RNUM FROM T_RANKING_CTL ");

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pSummaryType,ref sWhere);
			oSql.AppendLine(sWhere);

			oSql.AppendLine(")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ");
			oSql.AppendLine(sOrder);

			using (cmd = CreateSelectCommand(oSql.ToString(),conn)) {
				cmd.BindByName = true;
				for (int i = 0; i < objParms.Length; i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_RANKING_CTL");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd, string pSummaryType, ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		SysPrograms.SqlAppendWhere(" SITE_CD = :SITE_CD ",ref pWhere);
		list.Add(new OracleParameter("SITE_CD",pSiteCd));

		SysPrograms.SqlAppendWhere(" SUMMARY_TYPE = :SUMMARY_TYPE ", ref pWhere);
		list.Add(new OracleParameter("SUMMARY_TYPE", pSummaryType));

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public int GetPageCount(string pSiteCd, string pSummaryType) {

		int iPageCount = 0;
		try {
			conn = DbConnect();
			DataSet ds;
			StringBuilder sSql = new StringBuilder();

			sSql.AppendLine("SELECT ");
			sSql.AppendLine("   COUNT(*) AS ROW_COUNT ");
			sSql.AppendLine("FROM ");
			sSql.AppendLine("   T_RANKING_CTL ");
			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd, pSummaryType,ref sWhere);
			sSql.AppendLine(sWhere);

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					using (ds = new DataSet()) {
						for (int i = 0;i < objParms.Length;i++) {
							cmd.Parameters.Add((OracleParameter)objParms[i]);
						}
						da.Fill(ds);
						iPageCount = Convert.ToInt32(ds.Tables[0].Rows[0]["ROW_COUNT"]);
					}
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public bool IsDuplicateDate(string pSiteCd, string pSummaryType, DateTime pSummaryStartDate, DateTime pSummaryEndDate, string pRankingCtlSeq) {
		bool bDuplicate = false;
		try {
			conn = DbConnect();
			DataSet ds;
			StringBuilder sSql = new StringBuilder();
			sSql.AppendLine("SELECT ");
			sSql.AppendLine("   RANKING_CTL_SEQ ");
			sSql.AppendLine("FROM ");
			sSql.AppendLine("   T_RANKING_CTL ");
			sSql.AppendLine("WHERE ");
			sSql.AppendLine("	SITE_CD				= :SITE_CD				AND ");
			sSql.AppendLine("	SUMMARY_TYPE		= :SUMMARY_TYPE			AND ");
			sSql.AppendLine("	((SUMMARY_START_DATE>= :SUMMARY_START_DATE1	AND	");
			sSql.AppendLine("	 SUMMARY_START_DATE <= :SUMMARY_END_DATE1)	OR	");
			sSql.AppendLine("	(SUMMARY_END_DATE	>= :SUMMARY_START_DATE2	AND	");
			sSql.AppendLine("	 SUMMARY_END_DATE	<= :SUMMARY_END_DATE2)	OR	");
			sSql.AppendLine("	(SUMMARY_START_DATE <= :SUMMARY_START_DATE3	AND	");
			sSql.AppendLine("	 SUMMARY_END_DATE	>= :SUMMARY_END_DATE3))		");
			if (!pRankingCtlSeq.Equals(string.Empty)) {
				sSql.AppendLine(" AND	RANKING_CTL_SEQ		!=:RANKING_CTL_SEQ		");
			}
			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					using (ds = new DataSet()) {
						cmd.Parameters.Add("SITE_CD",pSiteCd);
						cmd.Parameters.Add("SUMMARY_TYPE", pSummaryType);
						cmd.Parameters.Add("SUMMARY_START_DATE1",OracleDbType.Date,pSummaryStartDate,ParameterDirection.Input);
						cmd.Parameters.Add("SUMMARY_END_DATE1",OracleDbType.Date,pSummaryEndDate,ParameterDirection.Input);
						cmd.Parameters.Add("SUMMARY_START_DATE2",OracleDbType.Date,pSummaryStartDate,ParameterDirection.Input);
						cmd.Parameters.Add("SUMMARY_END_DATE2",OracleDbType.Date,pSummaryEndDate,ParameterDirection.Input);
						cmd.Parameters.Add("SUMMARY_START_DATE3",OracleDbType.Date,pSummaryStartDate,ParameterDirection.Input);
						cmd.Parameters.Add("SUMMARY_END_DATE3",OracleDbType.Date,pSummaryEndDate,ParameterDirection.Input);
						if (!string.IsNullOrEmpty(pRankingCtlSeq)) {
							cmd.Parameters.Add("RANKING_CTL_SEQ", pRankingCtlSeq);
						}
						da.Fill(ds);
						if (ds.Tables[0].Rows.Count != 0) {
							bDuplicate = true;
						}
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bDuplicate;
	}

	public DataSet GetList(string pSiteCd, string pSummaryType, string pRankingCtlSeq) {

		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();
			StringBuilder oSql = new StringBuilder();

			string sOrder = "ORDER BY SITE_CD,SUMMARY_START_DATE DESC";
			oSql.AppendLine("SELECT ");
			oSql.AppendLine("	SUMMARY_TYPE		,");
			oSql.AppendLine("	RANKING_CTL_SEQ		,");
			oSql.AppendLine("	REMARKS				,");
			oSql.AppendLine("	SUMMARY_START_DATE	,");
			oSql.AppendLine("	SUMMARY_END_DATE	");
			oSql.AppendLine("FROM ");
			oSql.AppendLine("	T_RANKING_CTL	");

			string sWhere = "";
			List<OracleParameter> oParameterList = new List<OracleParameter>(CreateWhere(pSiteCd, pSummaryType, ref sWhere));
			oSql.AppendLine(sWhere);

			if (!string.IsNullOrEmpty(pRankingCtlSeq)) {
				oSql.AppendLine(" AND RANKING_CTL_SEQ = :RANKING_CTL_SEQ");
				oParameterList.Add(new OracleParameter("RANKING_CTL_SEQ", pRankingCtlSeq));
			}
			oSql.AppendLine(sOrder);

			using (cmd = CreateSelectCommand(oSql.ToString(),conn)) {
				cmd.Parameters.AddRange(oParameterList.ToArray());
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_RANKING_CTL");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


}
