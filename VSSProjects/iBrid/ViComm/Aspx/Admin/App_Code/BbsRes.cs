﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 掲示板レス
--	Progaram ID		: BbsRes
--  Creation Date	: 2011.04.11
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class BbsRes : DbSession {
	public BbsRes() {
	}

	public int GetPageCount(string pSiteCd, string pUserSeq, string pUserCharNo, string pBbsThreadSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_BBS_RES				BR	,");
		oSqlBuilder.AppendLine("	T_BBS_THREAD			BT	,");
		oSqlBuilder.AppendLine("	T_USER					UR	,");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER		CCR	");

		StringBuilder oWhereBuilder = new StringBuilder();
		oWhereBuilder.AppendLine("WHERE	");
		oWhereBuilder.AppendLine("	BR.SITE_CD					= BT.SITE_CD			AND");
		oWhereBuilder.AppendLine("	BR.USER_SEQ					= BT.USER_SEQ			AND");
		oWhereBuilder.AppendLine("	BR.USER_CHAR_NO				= BT.USER_CHAR_NO		AND");
		oWhereBuilder.AppendLine("	BR.BBS_THREAD_SEQ			= BT.BBS_THREAD_SEQ		AND");
		oWhereBuilder.AppendLine("	BR.BBS_RES_USER_SEQ			= UR.USER_SEQ			AND");
		oWhereBuilder.AppendLine("	BR.SITE_CD					= CCR.SITE_CD			AND");
		oWhereBuilder.AppendLine("	BR.BBS_RES_USER_SEQ			= CCR.USER_SEQ			AND");
		oWhereBuilder.AppendLine("	BR.BBS_RES_USER_CHAR_NO		= CCR.USER_CHAR_NO		");

		string sWhereClause = oWhereBuilder.ToString();
		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd, pUserSeq, pUserCharNo, pBbsThreadSeq, ref sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(), conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd, string pUserSeq, string pUserCharNo, string pBbsThreadSeq, int startRowIndex, int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	BR.SITE_CD						,");
		oSqlBuilder.AppendLine("	BR.USER_SEQ						,");
		oSqlBuilder.AppendLine("	BR.USER_CHAR_NO					,");
		oSqlBuilder.AppendLine("	BR.BBS_THREAD_SEQ				,");
		oSqlBuilder.AppendLine("	BR.BBS_THREAD_SUB_SEQ			,");
		oSqlBuilder.AppendLine("	BR.BBS_RES_HANDLE_NM			,");
		oSqlBuilder.AppendLine("	BR.BBS_RES_USER_SEQ				,");
		oSqlBuilder.AppendLine("	BR.BBS_RES_USER_CHAR_NO			,");
		oSqlBuilder.AppendLine("	UR.LOGIN_ID		RES_LOGIN_ID	,");
		oSqlBuilder.AppendLine("	CCR.HANDLE_NM	RES_HANDLE_NM	,");
		oSqlBuilder.AppendLine("	BR.POST_BBS_THREAD_SEQ			,");
		oSqlBuilder.AppendLine("	BR.POST_THREAD_SUB_SEQ			,");
		oSqlBuilder.AppendLine("	BR.DEL_FLAG						,");
		oSqlBuilder.AppendLine("	BR.ANONYMOUS_FLAG				,");
		oSqlBuilder.AppendLine("	BR.BBS_RES_DOC1					,");
		oSqlBuilder.AppendLine("	BR.BBS_RES_DOC2					,");
		oSqlBuilder.AppendLine("	BR.BBS_RES_DOC3					,");
		oSqlBuilder.AppendLine("	BR.BBS_RES_DOC4					,");
		oSqlBuilder.AppendLine("	BR.CREATE_DATE					,");
		oSqlBuilder.AppendLine("	BR.UPDATE_DATE					");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_BBS_RES				BR	,");
		oSqlBuilder.AppendLine("	T_BBS_THREAD			BT	,");
		oSqlBuilder.AppendLine("	T_USER					UR	,");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER		CCR	");

		StringBuilder oWhereBuilder = new StringBuilder();
		oWhereBuilder.AppendLine("WHERE	");
		oWhereBuilder.AppendLine("	BR.SITE_CD					= BT.SITE_CD			AND");
		oWhereBuilder.AppendLine("	BR.USER_SEQ					= BT.USER_SEQ			AND");
		oWhereBuilder.AppendLine("	BR.USER_CHAR_NO				= BT.USER_CHAR_NO		AND");
		oWhereBuilder.AppendLine("	BR.BBS_THREAD_SEQ			= BT.BBS_THREAD_SEQ		AND");
		oWhereBuilder.AppendLine("	BR.BBS_RES_USER_SEQ			= UR.USER_SEQ			AND");
		oWhereBuilder.AppendLine("	BR.SITE_CD					= CCR.SITE_CD			AND");
		oWhereBuilder.AppendLine("	BR.BBS_RES_USER_SEQ			= CCR.USER_SEQ			AND");
		oWhereBuilder.AppendLine("	BR.BBS_RES_USER_CHAR_NO		= CCR.USER_CHAR_NO		");

		string sWhereClause = oWhereBuilder.ToString();
		string sSortExpression = "ORDER BY BR.SITE_CD, BR.CREATE_DATE DESC";
		string sPagingSql;

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd, pUserSeq, pUserCharNo, pBbsThreadSeq, ref sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(), sSortExpression, startRowIndex, maximumRows, out sPagingSql);

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql, conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}

	private OracleParameter[] CreateWhere(string pSiteCd, string pUserSeq, string pUserCharNo, string pBbsThreadSeq, ref string pWhere) {

		List<OracleParameter> oOracleParameterList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("BR.SITE_CD = :SITE_CD", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("SITE_CD", pSiteCd));
		}

		if (!string.IsNullOrEmpty(pUserSeq)) {
			SysPrograms.SqlAppendWhere("BR.USER_SEQ = :USER_SEQ", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("USER_SEQ", pUserSeq));
		}

		if (!string.IsNullOrEmpty(pUserCharNo)) {
			SysPrograms.SqlAppendWhere("BR.USER_CHAR_NO = :USER_CHAR_NO", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("USER_CHAR_NO", pUserCharNo));
		}

		if (!string.IsNullOrEmpty(pBbsThreadSeq)) {
			SysPrograms.SqlAppendWhere("BR.BBS_THREAD_SEQ = :BBS_THREAD_SEQ", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("BBS_THREAD_SEQ", pBbsThreadSeq));
		}

		return oOracleParameterList.ToArray();
	}
}
