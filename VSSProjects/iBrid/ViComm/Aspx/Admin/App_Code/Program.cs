﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: プログラム
--	Progaram ID		: Program
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class Program:DbSession {

	public Program() {
	}

	public int GetPageCount(string pProgramRoot) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_PROGRAM ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}


	public DataSet GetPageCollection(string pProgramRoot,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY PROGRAM_ID";
			string sSql = "SELECT " +
							"PROGRAM_ID	," +
							"PROGRAM_NM," +
							"UPDATE_DATE " +
							"FROM(" +
							" SELECT T_PROGRAM.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM T_PROGRAM  ";

			string sWhere = "";
			OracleParameter[] oParms = CreateWhere(pProgramRoot,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < oParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)oParms[i]);
				}
				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_PROGRAM");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pProgramRoot,ref string pWhere) {
		ArrayList list = new ArrayList();
		pWhere = " WHERE PROGRAM_ROOT = :PROGRAM_ROOT ";
		list.Add(new OracleParameter("PROGRAM_ROOT",pProgramRoot));
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}


	public DataSet GetList(string pProgramRoot) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT PROGRAM_ID,(RPAD(PROGRAM_ID,34,'-') || ' ' || PROGRAM_NM) PROGRAM_NM FROM T_PROGRAM WHERE PROGRAM_ROOT = :PROGRAM_ROOT ORDER BY PROGRAM_ID ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("PROGRAM_ROOT",pProgramRoot);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_PROGRAM");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
}
