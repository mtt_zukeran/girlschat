﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 男性用お宝
--	Progaram ID		: ManTreasure
--
--  Creation Date	: 2011.07.21
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater			Update Explain

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;
using ViComm;

public class ManTreasure:DbSession {
	public string siteCd;
	public string userSeq;
	public string userCharNo;
	public string castNm;
	public string handleNm;
	public string loginId;
	public string castGamePicSeq;
	public string castGameAttrTypeNm;
	public string picTitle;
	public string objPhotoImgPath;
	public string objSmallPhotoImgPath;

	public ManTreasure() {
	}

	public int GetPageCount(string pSiteCd,string pDisplayDay,bool pPublishFlag,bool pNonPublishFlag,string pLoginId,string pCastGamePicAttrSeq) {
		return GetPageCount(pSiteCd,pDisplayDay,pPublishFlag,pNonPublishFlag,pLoginId,pCastGamePicAttrSeq,null);
	}

	public int GetPageCount(string pSiteCd,string pDisplayDay,bool pPublishFlag,bool pNonPublishFlag,string pLoginId,string pCastGamePicAttrSeq,string pCheckedFlag) {
		decimal iPageCount = 0;

		string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_MAN_TREASURE01 ";
		string sWhere = "";

		OracleParameter[] objParms = CreateWhere(pSiteCd,pDisplayDay,pPublishFlag,pNonPublishFlag,pLoginId,pCastGamePicAttrSeq,pCheckedFlag,ref sWhere);
		sSql = sSql + sWhere;

		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				iPageCount = (cmd.ExecuteScalar() as decimal?) ?? 0;
			}
		} finally {
			conn.Close();
		}

		return Convert.ToInt32(iPageCount);
	}
	public DataSet GetPageCollection(string pSiteCd,string pDisplayDay,bool pPublishFlag,bool pNonPublishFlag,string pLoginId,string pCastGamePicAttrSeq,int startRowIndex,int maximumRows) {
		return GetPageCollection(pSiteCd,pDisplayDay,pPublishFlag,pNonPublishFlag,pLoginId,pCastGamePicAttrSeq,null,startRowIndex,maximumRows);
	}
	public DataSet GetPageCollection(string pSiteCd,string pDisplayDay,bool pPublishFlag,bool pNonPublishFlag,string pLoginId,string pCastGamePicAttrSeq,string pCheckedFlag,int startRowIndex,int maximumRows) {

		string sOrder = "ORDER BY SITE_CD,CAST_GAME_PIC_SEQ DESC ";
		string sSql = "SELECT " +
							"*	" +
						"FROM(" +
						" SELECT VW_MAN_TREASURE01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_MAN_TREASURE01 ";

		string sWhere = "";
		OracleParameter[] objParms = CreateWhere(pSiteCd,pDisplayDay,pPublishFlag,pNonPublishFlag,pLoginId,pCastGamePicAttrSeq,pCheckedFlag,ref sWhere);
		sSql = sSql + sWhere;
		sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
		sSql = sSql + sOrder;

		DataSet ds = new DataSet();

		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_MAN_TREASURE01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
	private OracleParameter[] CreateWhere(string pSiteCd,string pDisplayDay,bool pPublishFlag,bool pNonPublishFlag,string pLoginId,string pCastGamePicAttrSeq,string pCheckedFlag,ref string pWhere) {
		ArrayList list = new ArrayList();
		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		if (!string.IsNullOrEmpty(pDisplayDay)) {
			SysPrograms.SqlAppendWhere("DISPLAY_DAY = :DISPLAY_DAY",ref pWhere);
			list.Add(new OracleParameter("DISPLAY_DAY",pDisplayDay));
		}
		if (pPublishFlag && !pNonPublishFlag) {
			SysPrograms.SqlAppendWhere("PUBLISH_FLAG = :PUBLISH_FLAG",ref pWhere);
			list.Add(new OracleParameter("PUBLISH_FLAG",ViCommConst.FLAG_ON));
		} else if (!pPublishFlag && pNonPublishFlag) {
			SysPrograms.SqlAppendWhere("PUBLISH_FLAG = :PUBLISH_FLAG",ref pWhere);
			list.Add(new OracleParameter("PUBLISH_FLAG",ViCommConst.FLAG_OFF));
		}
		if (!string.IsNullOrEmpty(pLoginId)) {
			SysPrograms.SqlAppendWhere("LOGIN_ID = :LOGIN_ID",ref pWhere);
			list.Add(new OracleParameter("LOGIN_ID",pLoginId));
		}
		if (!string.IsNullOrEmpty(pCastGamePicAttrSeq)) {
			SysPrograms.SqlAppendWhere("CAST_GAME_PIC_ATTR_SEQ = :CAST_GAME_PIC_ATTR_SEQ",ref pWhere);
			list.Add(new OracleParameter("CAST_GAME_PIC_ATTR_SEQ",pCastGamePicAttrSeq));
		}
		// チェック済みフラグ
		if (!string.IsNullOrEmpty(pCheckedFlag)) {
			SysPrograms.SqlAppendWhere("CHECKED_FLAG = :CHECKED_FLAG",ref pWhere);
			list.Add(new OracleParameter("CHECKED_FLAG",pCheckedFlag));
		}
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
	
	public bool GetValue(string pSiteCd,string pUserSeq,string pUserCharNo,string pCastGamePicSeq,string pItem,ref string pValue) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect();
			string sSql = "SELECT " +
								"*	" +
							"FROM " +
								"VW_MAN_TREASURE01 " +
							"WHERE " +
								"SITE_CD			= :SITE_CD		AND	" +
								"USER_SEQ			= :USER_SEQ		AND	" +
								"USER_CHAR_NO		= :USER_CHAR_NO	AND	" +
								"CAST_GAME_PIC_SEQ	= :CAST_GAME_PIC_SEQ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				cmd.Parameters.Add("CAST_GAME_PIC_SEQ",pCastGamePicSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_MAN_TREASURE01");
					if (ds.Tables["VW_MAN_TREASURE01"].Rows.Count != 0) {
						dr = ds.Tables["VW_MAN_TREASURE01"].Rows[0];
						pValue = dr[pItem].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public DataSet GetList(string pSiteCd,string pUserSeq,string pUserCharNo) {
		DataSet ds = new DataSet();
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SITE_CD,");
		oSqlBuilder.AppendLine("	USER_SEQ,");
		oSqlBuilder.AppendLine("	USER_CHAR_NO,");
		oSqlBuilder.AppendLine("	CAST_GAME_PIC_SEQ,");
		oSqlBuilder.AppendLine("	UPLOAD_DATE,");
		oSqlBuilder.AppendLine("	PUBLISH_FLAG,");
		oSqlBuilder.AppendLine("	OBJ_PHOTO_IMG_PATH,");
		oSqlBuilder.AppendLine("	OBJ_SMALL_PHOTO_IMG_PATH");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_MAN_TREASURE01");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD		AND");
		oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ		AND");
		oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO");
		oSqlBuilder.AppendLine("ORDER BY");
		oSqlBuilder.AppendLine("	UPLOAD_DATE DESC");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));

		ds = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		return ds;
	}

	public bool GetOne(string pCastGamePicSeq) {
		bool bFind = false;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SITE_CD,");
		oSqlBuilder.AppendLine("	USER_SEQ,");
		oSqlBuilder.AppendLine("	USER_CHAR_NO,");
		oSqlBuilder.AppendLine("	CAST_NM,");
		oSqlBuilder.AppendLine("	HANDLE_NM,");
		oSqlBuilder.AppendLine("	LOGIN_ID,");
		oSqlBuilder.AppendLine("	CAST_GAME_PIC_SEQ,");
		oSqlBuilder.AppendLine("	CAST_GAME_PIC_ATTR_NM,");
		oSqlBuilder.AppendLine("	PIC_TITLE,");
		oSqlBuilder.AppendLine("	OBJ_PHOTO_IMG_PATH,");
		oSqlBuilder.AppendLine("	OBJ_SMALL_PHOTO_IMG_PATH");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_MAN_TREASURE01");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	CAST_GAME_PIC_SEQ = :CAST_GAME_PIC_SEQ");

		oParamList.Add(new OracleParameter(":CAST_GAME_PIC_SEQ",pCastGamePicSeq));

		DataSet ds = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (ds.Tables[0].Rows.Count > 0) {
			bFind = true;
			siteCd = ds.Tables[0].Rows[0]["SITE_CD"].ToString();
			userSeq = ds.Tables[0].Rows[0]["USER_SEQ"].ToString();
			userCharNo = ds.Tables[0].Rows[0]["USER_CHAR_NO"].ToString();
			castNm = ds.Tables[0].Rows[0]["CAST_NM"].ToString();
			handleNm = ds.Tables[0].Rows[0]["HANDLE_NM"].ToString();
			loginId = ds.Tables[0].Rows[0]["LOGIN_ID"].ToString();
			castGamePicSeq = ds.Tables[0].Rows[0]["CAST_GAME_PIC_SEQ"].ToString();
			castGameAttrTypeNm = ds.Tables[0].Rows[0]["CAST_GAME_PIC_ATTR_NM"].ToString();
			picTitle = ds.Tables[0].Rows[0]["PIC_TITLE"].ToString();
			objPhotoImgPath = ds.Tables[0].Rows[0]["OBJ_PHOTO_IMG_PATH"].ToString();
			objSmallPhotoImgPath = ds.Tables[0].Rows[0]["OBJ_SMALL_PHOTO_IMG_PATH"].ToString();
		}

		return bFind;
	}

	/// <summary>
	/// チェック済みフラグをたてる
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pUserSeq"></param>
	/// <param name="pUserCharNo"></param>
	/// <param name="pCastGamePicSeq"></param>
	/// <returns></returns>
	public string AccessManTresureMainte(string pSiteCd,string pUserSeq,string pUserCharNo,string pCastGamePicSeq) {
		string pStatus = string.Empty;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MAN_TREASURE_CHECKED");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pCAST_GAME_PIC_SEQ",DbSession.DbType.VARCHAR2,pCastGamePicSeq);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pStatus = db.GetStringValue("pSTATUS");
		}
		return pStatus;
	}
}
