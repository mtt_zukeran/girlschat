﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 検索条件設定
--	Progaram ID		: SeekCondition
--
--  Creation Date	: 2011.01.31
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Web;
using System.Data;
using System.Data.Common;
using System.Text;

using iBridCommLib;
using ViComm;

public class SeekCondition :DbSession {
	public SeekCondition() {}

	public DataSet GetOne(string pSeekCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine(" SELECT						 ");
		oSqlBuilder.AppendLine("	ADMIN_ID				,");
		oSqlBuilder.AppendLine("	SEEK_CONDITION_SEQ      ,");
		oSqlBuilder.AppendLine("	SEEK_CONDITION_TYPE     ,");
		oSqlBuilder.AppendLine("	SEEK_CONDITION_NM       ,");
		oSqlBuilder.AppendLine("	SEEK_CONDITION1			,");
		oSqlBuilder.AppendLine("	SEEK_CONDITION2			,");
		oSqlBuilder.AppendLine("	SEEK_CONDITION3			,");
		oSqlBuilder.AppendLine("	SEEK_CONDITION4			,");
		oSqlBuilder.AppendLine("	SEEK_CONDITION5			,");
		oSqlBuilder.AppendLine("	SEEK_CONDITION6			,");
		oSqlBuilder.AppendLine("	SEEK_CONDITION7			,");
		oSqlBuilder.AppendLine("	SEEK_CONDITION8			,");
		oSqlBuilder.AppendLine("	SEEK_CONDITION9			,");
		oSqlBuilder.AppendLine("	SEEK_CONDITION10		,");
		oSqlBuilder.AppendLine("	PUBLISH_FLAG            ,");
		oSqlBuilder.AppendLine("	UPDATE_DATE             ,");
		oSqlBuilder.AppendLine("	REVISION_NO              "); 
		oSqlBuilder.AppendLine(" FROM                                                                   ");
		oSqlBuilder.AppendLine(" 	T_SEEK_CONDITION                                                    ");
		oSqlBuilder.AppendLine(" WHERE                                                                  ");
		oSqlBuilder.AppendLine(" 	T_SEEK_CONDITION.SEEK_CONDITION_SEQ = :SEEK_CONDITION_SEQ           ");
		DataSet oDataSet = new DataSet();
		try {
			this.conn = this.DbConnect();

			using (this.cmd = this.CreateSelectCommand(oSqlBuilder.ToString(), this.conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SEEK_CONDITION_SEQ", pSeekCondition);
				using (this.da = new OracleDataAdapter(this.cmd)) {
					this.da.Fill(oDataSet);
				}
			}
		} finally {
			this.conn.Close();
		}
		return oDataSet;
	}

	public DataSet GetListOwn(string pAdminId, string pSeekConditionType) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine(" SELECT																	");
		oSqlBuilder.AppendLine(" 	T_SEEK_CONDITION.ADMIN_ID			,                               ");
		oSqlBuilder.AppendLine(" 	T_SEEK_CONDITION.SEEK_CONDITION_NM	,                               ");
		oSqlBuilder.AppendLine(" 	T_SEEK_CONDITION.SEEK_CONDITION_SEQ                                 ");
		oSqlBuilder.AppendLine(" FROM                                                                   ");
		oSqlBuilder.AppendLine(" 	T_SEEK_CONDITION                                                    ");
		oSqlBuilder.AppendLine(" WHERE                                                                  ");
		oSqlBuilder.AppendLine(" 	T_SEEK_CONDITION.SEEK_CONDITION_TYPE = :SEEK_CONDITION_TYPE AND     ");
		oSqlBuilder.AppendLine(" 	T_SEEK_CONDITION.ADMIN_ID 			 = :ADMIN_ID			        ");
		oSqlBuilder.AppendLine(" ORDER BY                                                               ");
		oSqlBuilder.AppendLine("	ADMIN_ID			,                                               ");
		oSqlBuilder.AppendLine("	SEEK_CONDITION_NM                                                   ");
		DataSet oDataSet = new DataSet();
		try {
			this.conn = this.DbConnect();

			using (this.cmd = this.CreateSelectCommand(oSqlBuilder.ToString(), this.conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":ADMIN_ID", pAdminId);
				cmd.Parameters.Add(":SEEK_CONDITION_TYPE", pSeekConditionType);
				using (this.da = new OracleDataAdapter(this.cmd)) {
					this.da.Fill(oDataSet);
				}
			}
		} finally {
			this.conn.Close();
		}
		return oDataSet;
	}
	public DataSet GetListOther(string pAdminId, string pSeekConditionType) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine(" SELECT																	");
		oSqlBuilder.AppendLine(" 	T_SEEK_CONDITION.ADMIN_ID			,                               ");
		oSqlBuilder.AppendLine(" 	T_SEEK_CONDITION.SEEK_CONDITION_NM	,                               ");
		oSqlBuilder.AppendLine(" 	T_SEEK_CONDITION.SEEK_CONDITION_SEQ                                 ");
		oSqlBuilder.AppendLine(" FROM                                                                   ");
		oSqlBuilder.AppendLine(" 	T_SEEK_CONDITION                                                    ");
		oSqlBuilder.AppendLine(" WHERE                                                                  ");
		oSqlBuilder.AppendLine(" 	T_SEEK_CONDITION.SEEK_CONDITION_TYPE = :SEEK_CONDITION_TYPE AND     ");
		oSqlBuilder.AppendLine("	T_SEEK_CONDITION.PUBLISH_FLAG 		 = :PUBLISH_FLAG		AND     ");
		oSqlBuilder.AppendLine(" 	T_SEEK_CONDITION.ADMIN_ID 			 <> :ADMIN_ID			        ");
		oSqlBuilder.AppendLine(" ORDER BY                                                               ");
		oSqlBuilder.AppendLine("	SEEK_CONDITION_NM                                                   ");
		DataSet oDataSet = new DataSet();
		try {
			this.conn = this.DbConnect();

			using (this.cmd = this.CreateSelectCommand(oSqlBuilder.ToString(), this.conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":ADMIN_ID", pAdminId);
				cmd.Parameters.Add(":PUBLISH_FLAG", ViCommConst.FLAG_ON);
				cmd.Parameters.Add(":SEEK_CONDITION_TYPE", pSeekConditionType);
				using (this.da = new OracleDataAdapter(this.cmd)) {
					this.da.Fill(oDataSet);
				}
			}
		} finally {
			this.conn.Close();
		}
		return oDataSet;
	}
}
