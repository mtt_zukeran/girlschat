﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 一括送信メール
--	Progaram ID		: BatchMail
--
--  Creation Date	: 2011.10.07
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater		Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Text;
using System.Data.Common;
using System.Collections;
using iBridCommLib;
using ViComm;

public class BatchMail : DbSession {
	public BatchMail() {
	}

	public int GetPageCount(string pSiteCd,string pReportDayFrom,string pReportTimeFrom,string pReportDayTo,string pReportTimeTo,string pTxLoginId,string pTxUserCharNo,string pAttachedType,string pSortExpression,string pSortDirection) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	VW_REQ_TX_MAIL02");

		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pReportDayFrom,pReportTimeFrom,pReportDayTo,pReportTimeTo,pTxLoginId,pTxUserCharNo,pAttachedType,ref sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
				return iPageCount;
			}
		} finally {
			conn.Close();
		}
	}

	public DataSet GetPageCollection(string pSiteCd,string pReportDayFrom,string pReportTimeFrom,string pReportDayTo,string pReportTimeTo,string pTxLoginId,string pTxUserCharNo,string pAttachedType,string pSortExpression,string pSortDirection,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	REQUEST_TX_MAIL_SEQ			,");
		oSqlBuilder.AppendLine("	REQUEST_TX_DATE				,");
		oSqlBuilder.AppendLine("	TX_SITE_CD					,");
		oSqlBuilder.AppendLine("	TX_USER_SEQ					,");
		oSqlBuilder.AppendLine("	TX_USER_CHAR_NO				,");
		oSqlBuilder.AppendLine("	TX_HANDLE_NM				,");
		oSqlBuilder.AppendLine("	TX_LOGIN_ID					,");
		oSqlBuilder.AppendLine("	TX_SEX_CD					,");
		oSqlBuilder.AppendLine("	ORIGINAL_TITLE				,");
		oSqlBuilder.AppendLine("	ORIGINAL_DOC1				,");
		oSqlBuilder.AppendLine("	ORIGINAL_DOC2				,");
		oSqlBuilder.AppendLine("	ORIGINAL_DOC3				,");
		oSqlBuilder.AppendLine("	ORIGINAL_DOC4				,");
		oSqlBuilder.AppendLine("	ORIGINAL_DOC5				,");
		oSqlBuilder.AppendLine("	BATCH_MAIL_FLAG				,");
		oSqlBuilder.AppendLine("	MAIL_TYPE					,");
		oSqlBuilder.AppendLine("	MAIL_ATTACHED_METHOD		,");
		oSqlBuilder.AppendLine("	ATTACHED_OBJ_TYPE			,");
		oSqlBuilder.AppendLine("	PIC_SEQ						,");
		oSqlBuilder.AppendLine("	MOVIE_SEQ					,");
		oSqlBuilder.AppendLine("	OBJ_SEQ						,");
		oSqlBuilder.AppendLine("	BATCH_MAIL_FIND_COUNT		,");
		oSqlBuilder.AppendLine("	BATCH_MAIL_SEND_REAL_COUNT	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	VW_REQ_TX_MAIL02			");

		string sWhereClause = string.Empty;
		string sSortExpression = string.Format("ORDER BY {0} REQUEST_TX_DATE DESC, TX_USER_SEQ",
			string.IsNullOrEmpty(pSortExpression) || string.IsNullOrEmpty(pSortDirection) ? string.Empty : string.Format("{0} {1},",pSortExpression,pSortDirection));
		string sPagingSql;

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pReportDayFrom,pReportTimeFrom,pReportDayTo,pReportTimeTo,pTxLoginId,pTxUserCharNo,pAttachedType,ref sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pReportDayFrom,string pReportTimeFrom,string pReportDayTo,string pReportTimeTo,string pTxLoginId,string pTxUserCharNo,string pAttachedType,ref string pWhere) {
		SetDefaultTimeValue(ref pReportTimeFrom,ref pReportTimeTo);
		ArrayList list = new ArrayList();

		SysPrograms.SqlAppendWhere("BATCH_MAIL_FLAG = '1'",ref pWhere);

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("TX_SITE_CD = :TX_SITE_CD",ref pWhere);
			list.Add(new OracleParameter("TX_SITE_CD",pSiteCd));
		}

		SysPrograms.SqlAppendWhere("WAIT_TX_FLAG = :WAIT_TX_FLAG",ref pWhere);
		list.Add(new OracleParameter("WAIT_TX_FLAG",ViCommConst.FLAG_OFF_STR));

		if ((!string.IsNullOrEmpty(pReportDayFrom)) && (!string.IsNullOrEmpty(pReportDayTo))) {
			DateTime dtFrom = DateTime.Parse(pReportDayFrom + " " + pReportTimeFrom + ":00:00");
			DateTime dtTo = DateTime.Parse(pReportDayTo + " " + pReportTimeTo + ":59:59").AddSeconds(1);
			iBridCommLib.SysPrograms.SqlAppendWhere("REQUEST_TX_DATE >= :REQUEST_TX_DATE_FROM AND REQUEST_TX_DATE < :REQUEST_TX_DATE_TO",ref pWhere);
			list.Add(new OracleParameter("REQUEST_TX_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("REQUEST_TX_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (!string.IsNullOrEmpty(pTxLoginId)) {
			SysPrograms.SqlAppendWhere("TX_LOGIN_ID LIKE :TX_LOGIN_ID||'%'",ref pWhere);
			list.Add(new OracleParameter("TX_LOGIN_ID",pTxLoginId));

			if (!string.IsNullOrEmpty(pTxUserCharNo)) {
				SysPrograms.SqlAppendWhere("TX_USER_CHAR_NO = :TX_USER_CHAR_NO",ref pWhere);
				list.Add(new OracleParameter("TX_USER_CHAR_NO",pTxUserCharNo));
			}
		}

		if (!string.IsNullOrEmpty(pAttachedType)) {
			StringBuilder oClauseBuilder = new StringBuilder();

			if (!pWhere.Equals(string.Empty)) {
				oClauseBuilder.Append(" AND ");
			}

			oClauseBuilder.Append("ATTACHED_OBJ_TYPE IN (");
			int iIndex = 1;
			foreach (string sValue in pAttachedType.Split(',')) {
				string sParamName = string.Format(":ATTACHED_OBJ_TYPE{0}",iIndex);
				if (iIndex != 1) {
					oClauseBuilder.Append(",");
				}
				oClauseBuilder.Append(sParamName);
				list.Add(new OracleParameter(sParamName,sValue));
				iIndex += 1;
			}
			oClauseBuilder.Append(") ");

			pWhere += oClauseBuilder.ToString();
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	private void SetDefaultTimeValue(ref string pTimeFrom,ref string pTimeTo) {
		if (string.IsNullOrEmpty(pTimeFrom)) {
			pTimeFrom = "00";
		}
		if (string.IsNullOrEmpty(pTimeTo)) {
			pTimeTo = "23";
		}
	}
}
