﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 男性ユーザーキャラクター拡張
--	Progaram ID		: UserManCharacterEx
--  Creation Date	: 2011.04.18
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Web;
using System.Web.UI;

using iBridCommLib;
using ViComm;

public class UserManCharacterEx : DbSession{
	public UserManCharacterEx() {	}

	public DataSet GetOne(string pSiteCd, string pUserSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	CE.SITE_CD						,");
		oSqlBuilder.AppendLine("	CE.USER_SEQ						,");
		oSqlBuilder.AppendLine("	CE.USER_CHAR_NO					,");
		oSqlBuilder.AppendLine("	CE.BLOG_MAIL_RX_TYPE			,");
		oSqlBuilder.AppendLine("	CE.MONTHLY_FIRST_RICHINO_RANK	,");
		oSqlBuilder.AppendLine("	CE.RICHINO_RANK					,");
        oSqlBuilder.AppendLine("	CE.LAST_RICHINO_RANK_UPD_DATE	,");
        oSqlBuilder.AppendLine("	CE.SITE_USE_STATUS          	,");
		oSqlBuilder.AppendLine("	CE.UPDATE_DATE					,");
		oSqlBuilder.AppendLine("	CE.REVISION_NO					,");
		oSqlBuilder.AppendLine("	RR.RICHINO_RANK_NM				");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER_EX	CE,");
		oSqlBuilder.AppendLine("	T_RICHINO_RANK			RR	");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	CE.SITE_CD			= RR.SITE_CD		(+)	AND");
		oSqlBuilder.AppendLine("	CE.RICHINO_RANK		= RR.RICHINO_RANK	(+)	AND");
		oSqlBuilder.AppendLine("	CE.SITE_CD			= :SITE_CD			AND");
		oSqlBuilder.AppendLine("	CE.USER_SEQ			= :USER_SEQ			AND");
		oSqlBuilder.AppendLine("	CE.USER_CHAR_NO		= :USER_CHAR_NO		");

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(), conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(new OracleParameter("SITE_CD", pSiteCd));
				cmd.Parameters.Add(new OracleParameter("USER_SEQ", pUserSeq));
				cmd.Parameters.Add(new OracleParameter("USER_CHAR_NO", ViCommConst.MAIN_CHAR_NO));

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}
}
