/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 認証IP 
--	Progaram ID		: AuthIP
--
--  Creation Date	: 2011.05.24
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class AuthIP:DbSession {

	public AuthIP() {
	}

	public bool IsExist(string pIP) {
		DataSet ds;
		bool bExist = false;
		try {
			conn = DbConnect("AuthIP.IsExist");

			using (cmd = CreateSelectCommand("SELECT REDIRECT_PC_PAGE_FLAG FROM T_AUTH_IP WHERE IP_ADDR =:IP_ADDR",conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("IP_ADDR",pIP);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_AUTH_IP");
					if (ds.Tables["T_AUTH_IP"].Rows.Count != 0) {
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public int GetPageCount() {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_AUTH_IP ";

			using (cmd = CreateSelectCommand(sSql, conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(int startRowIndex, int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY IP_ADDR";
			string sSql = "SELECT " +
							"IP_ADDR					," +
							"REDIRECT_PC_PAGE_FLAG		," +
							"NA_MOBILE_PAGE_FLAG		 " +
							"FROM(  " +
							" SELECT T_AUTH_IP.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM T_AUTH_IP ";

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql, conn)) {

				cmd.Parameters.Add("FIRST_ROW", startRowIndex);
				cmd.Parameters.Add("LAST_ROW", startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds, "T_AUTH_IP");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
}
