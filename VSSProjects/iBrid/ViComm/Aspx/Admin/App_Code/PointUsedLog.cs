﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: ポイント利用履歴
--	Progaram ID		: PointUsedLog
--  Creation Date	: 2011.08.11
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public class PointUsedLog : DbSession {
	public PointUsedLog() {
	}

	public DataSet GetListReport(string pSiteCd,string pSexCd,string pReportDayFrom,string pReportDayTo,string pPointUsedType,string pGameItemCategoryType) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	PUL.SITE_CD						,");
		oSqlBuilder.AppendLine("	PUL.USER_SEQ					,");
		oSqlBuilder.AppendLine("	PUL.USER_CHAR_NO				,");
		oSqlBuilder.AppendLine("	U.LOGIN_ID						,");
		oSqlBuilder.AppendLine("	C.HANDLE_NM						,");
		oSqlBuilder.AppendLine("	GC.GAME_HANDLE_NM				,");
		oSqlBuilder.AppendLine("	PUL.POINT_USED_REPORT_SEQ		,");
		oSqlBuilder.AppendLine("	PUL.POINT_USED_TYPE				,");
		oSqlBuilder.AppendLine("	CASE							");
		oSqlBuilder.AppendLine("		WHEN POINT_USED_TYPE = '1'	");
		oSqlBuilder.AppendLine("		THEN '課金アイテム'			");
		oSqlBuilder.AppendLine("		WHEN POINT_USED_TYPE = '2'	");
		oSqlBuilder.AppendLine("		THEN 'デート'				");
		oSqlBuilder.AppendLine("		WHEN POINT_USED_TYPE = '3'	");
		oSqlBuilder.AppendLine("		THEN '抽選'					");
		oSqlBuilder.AppendLine("		ELSE 'その他'				");
		oSqlBuilder.AppendLine("	END POINT_USED_TYPE_NM			,");
		oSqlBuilder.AppendLine("	PUL.GAME_ITEM_SEQ				,");
		oSqlBuilder.AppendLine("	PUL.DATE_PLAN_SEQ				,");
		oSqlBuilder.AppendLine("	PUL.LOTTERY_SEQ					,");
		oSqlBuilder.AppendLine("	PUL.BUY_QUANTITY				,");
		switch (pPointUsedType) {
			case PwViCommConst.GameCharge.ITEM:
				oSqlBuilder.AppendLine("	GI.GAME_ITEM_NM					,");
				oSqlBuilder.AppendLine("	GIG.GAME_ITEM_CATEGORY_TYPE		,");
				oSqlBuilder.AppendLine("	GIG.GAME_ITEM_CATEGORY_NM		,");
				oSqlBuilder.AppendLine("	'' DATE_PLAN_NM					,");
				oSqlBuilder.AppendLine("	'' LOTTERY_NM					,");
				break;
			case PwViCommConst.GameCharge.DATE:
				oSqlBuilder.AppendLine("	'' GAME_ITEM_NM					,");
				oSqlBuilder.AppendLine("	'' GAME_ITEM_CATEGORY_TYPE		,");
				oSqlBuilder.AppendLine("	'' GAME_ITEM_CATEGORY_NM		,");
				oSqlBuilder.AppendLine("	DP.DATE_PLAN_NM					,");
				oSqlBuilder.AppendLine("	'' LOTTERY_NM					,");
				break;
			case PwViCommConst.GameCharge.LOTTERY:
				oSqlBuilder.AppendLine("	'' GAME_ITEM_NM					,");
				oSqlBuilder.AppendLine("	'' GAME_ITEM_CATEGORY_TYPE		,");
				oSqlBuilder.AppendLine("	'' GAME_ITEM_CATEGORY_NM		,");
				oSqlBuilder.AppendLine("	'' DATE_PLAN_NM					,");
				oSqlBuilder.AppendLine("	LNC.LOTTERY_NM					,");
				break;
		}
		oSqlBuilder.AppendLine("	PUL.USED_POINT					,");
		oSqlBuilder.AppendLine("	PUL.POINT_USED_DATE");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_POINT_USED_LOG		PUL		,");
		oSqlBuilder.AppendLine("	T_USEr					U		,");
		oSqlBuilder.AppendLine("	T_GAME_CHARACTER		GC		,");
		if (ViCommConst.MAN.Equals(pSexCd)) {
			oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER	C		,");
		} else {
			oSqlBuilder.AppendLine("	T_CAST_CHARACTER		C		,");
		}
		switch (pPointUsedType) {
			case PwViCommConst.GameCharge.ITEM:
				oSqlBuilder.AppendLine("	T_GAME_ITEM				GI		,");
				oSqlBuilder.AppendLine("	T_GAME_ITEM_CATEGORY	GIG		");
				break;
			case PwViCommConst.GameCharge.DATE:
				oSqlBuilder.AppendLine("	T_DATE_PLAN				DP		");
				break;
			case PwViCommConst.GameCharge.LOTTERY:
				oSqlBuilder.AppendLine("	T_LOTTERY_NOT_COMP		LNC		");
				break;
		}

		string sWhereClause;
		string sSortExpression = " ORDER BY PUL.SITE_CD, PUL.POINT_USED_DATE DESC, PUL.USED_POINT ";

		OracleParameter[] oWhereParams = this.CreateWhereReport(pSiteCd,pSexCd,pReportDayFrom,pReportDayTo,pPointUsedType,pGameItemCategoryType,out sWhereClause);
		oSqlBuilder.Append(sWhereClause);
		oSqlBuilder.Append(sSortExpression);

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}

	private OracleParameter[] CreateWhereReport(string pSiteCd,string pSexCd,string pReportDayFrom,string pReportDayTo,string pPointUsedType,string pGameItemCategoryType,out string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();

		SysPrograms.SqlAppendWhere("PUL.SITE_CD = GC.SITE_CD ",ref pWhere);
		SysPrograms.SqlAppendWhere("PUL.USER_SEQ = GC.USER_SEQ (+) ",ref pWhere);
		SysPrograms.SqlAppendWhere("PUL.USER_CHAR_NO = GC.USER_CHAR_NO (+) ",ref pWhere);
		SysPrograms.SqlAppendWhere("PUL.SITE_CD = C.SITE_CD ",ref pWhere);
		SysPrograms.SqlAppendWhere("PUL.USER_SEQ = C.USER_SEQ (+) ",ref pWhere);
		SysPrograms.SqlAppendWhere("PUL.USER_CHAR_NO = C.USER_CHAR_NO (+) ",ref pWhere);
		SysPrograms.SqlAppendWhere("PUL.USER_SEQ = U.USER_SEQ (+) ",ref pWhere);
		if (!string.IsNullOrEmpty(pPointUsedType)) {
			switch (pPointUsedType) {
				case PwViCommConst.GameCharge.ITEM:
					SysPrograms.SqlAppendWhere("PUL.SITE_CD = GI.SITE_CD ",ref pWhere);
					SysPrograms.SqlAppendWhere("PUL.GAME_ITEM_SEQ = GI.GAME_ITEM_SEQ (+) ",ref pWhere);
					SysPrograms.SqlAppendWhere("PUL.SITE_CD = GIG.SITE_CD (+) ",ref pWhere);
					SysPrograms.SqlAppendWhere("GI.GAME_ITEM_CATEGORY_TYPE = GIG.GAME_ITEM_CATEGORY_TYPE ",ref pWhere);
					SysPrograms.SqlAppendWhere("GC.SEX_CD = GI.SEX_CD ",ref pWhere);
					break;
				case PwViCommConst.GameCharge.DATE:
					SysPrograms.SqlAppendWhere("PUL.SITE_CD = DP.SITE_CD ",ref pWhere);
					SysPrograms.SqlAppendWhere("PUL.DATE_PLAN_SEQ = DP.DATE_PLAN_SEQ (+) ",ref pWhere);
					break;
				case PwViCommConst.GameCharge.LOTTERY:
					SysPrograms.SqlAppendWhere("PUL.SITE_CD = LNC.SITE_CD ",ref pWhere);
					SysPrograms.SqlAppendWhere("PUL.LOTTERY_SEQ = LNC.LOTTERY_SEQ (+) ",ref pWhere);
					SysPrograms.SqlAppendWhere("GC.SEX_CD = LNC.SEX_CD ",ref pWhere);
					break;
			}
		}

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("PUL.SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!string.IsNullOrEmpty(pSexCd)) {
			SysPrograms.SqlAppendWhere("GC.SEX_CD = :SEX_CD ",ref pWhere);
			list.Add(new OracleParameter("SEX_CD",pSexCd));
		}

		if (!string.IsNullOrEmpty(pPointUsedType)) {
			SysPrograms.SqlAppendWhere("PUL.POINT_USED_TYPE = :POINT_USED_TYPE ",ref pWhere);
			list.Add(new OracleParameter("POINT_USED_TYPE",pPointUsedType));

			if (!string.IsNullOrEmpty(pGameItemCategoryType) && PwViCommConst.GameCharge.ITEM.Equals(pPointUsedType)) {
				SysPrograms.SqlAppendWhere("GI.GAME_ITEM_CATEGORY_TYPE = :GAME_ITEM_CATEGORY_TYPE ",ref pWhere);
				list.Add(new OracleParameter("GAME_ITEM_CATEGORY_TYPE",pGameItemCategoryType));
			}
		}

		if (!string.IsNullOrEmpty(pReportDayFrom) && !string.IsNullOrEmpty(pReportDayTo)) {
			DateTime dtFrom = DateTime.Parse(pReportDayFrom + " 00:00:00");
			DateTime dtTo = DateTime.Parse(pReportDayTo + " 23:59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere("(PUL.POINT_USED_DATE >= :POINT_USED_DATE_FROM AND PUL.POINT_USED_DATE <= :POINT_USED_DATE_TO)",ref pWhere);
			list.Add(new OracleParameter("POINT_USED_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("POINT_USED_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

}
