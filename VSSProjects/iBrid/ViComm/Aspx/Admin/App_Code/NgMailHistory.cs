/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: NGﾒｰﾙ履歴
--	Progaram ID		: NgMailHistory
--
--  Creation Date	: 2010.04.15
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class NgMailHistory:DbSession {

	public NgMailHistory() {
	}

	public int GetPageCount(string pUserSeq) {

		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = " SELECT COUNT(*) AS ROW_COUNT FROM T_NG_MAIL_HISTORY		" +
							" WHERE													" +
							"	 USER_SEQ = :USER_SEQ								";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":USER_SEQ",pUserSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_NG_MAIL_HISTORY");
					if (ds.Tables[0].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
					}
				}
			}

		} finally {
			conn.Close();
		}

		return iPageCount;
	}

	public DataSet GetPageCollection(string pUserSeq,int startRowIndex,int maximumRows) {

		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = " ORDER BY USER_SEQ, NG_MAIL_DATE DESC ";
			string sSql = " SELECT " +
							"	 NG_MAIL_DATE											," +
							"	 EMAIL_ADDR												," +
							"    ERROR_TYPE                                              " +
							" FROM(														 " +
							"	 SELECT													 " +
							"		 T_NG_MAIL_HISTORY.*,								 " +
							"		 ROW_NUMBER() OVER ( " + sOrder + " ) AS RNUM		 " +
							"	 FROM													 " +
							"		 T_NG_MAIL_HISTORY									 " +
							"	 WHERE													 " +
							"		 USER_SEQ = :USER_SEQ								 ";
			sSql = sSql + " )WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":USER_SEQ",pUserSeq);
				cmd.Parameters.Add(":FIRST_ROW",startRowIndex);
				cmd.Parameters.Add(":LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_NG_MAIL_HISTORY");
				}
			}

		} finally {
			conn.Close();
		}

		return ds;
	}
}
