﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者動画
--	Progaram ID		: CastMovie
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater		Update Explain
  2010/06/29	伊藤和明	検索項目に日付を追加
  2010/07/16	Koyanagi	検索項目に非公開ﾌﾗｸﾞを追加

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class CastMovie:DbSession {

	public string siteCd;
	public string loginId;
	public decimal userSeq;
	public string userCharNo;
	public decimal movieSeq;
	public string movieTitle;
	public int chargePoint;
	public int movieType;
	public string castNm;
	public string handleNm;

	public CastMovie() {
	}

	public int GetPageCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pMovieType) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_CAST_MOVIE01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pMovieType,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}


	public DataSet GetPageCollection(string pSiteCd,string pUserSeq,string pUserCharNo,string pMovieType,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY SITE_CD,USER_SEQ,USER_CHAR_NO,MOVIE_SEQ DESC ";
			string sSql = "SELECT " +
								"SITE_CD					," +
								"USER_SEQ					," +
								"USER_CHAR_NO				," +
								"LOGIN_ID					," +
								"MOVIE_SEQ					," +
								"MOVIE_TITLE				," +
								"MOVIE_DOC					," +
								"CHARGE_POINT				," +
								"OBJ_NOT_APPROVE_FLAG		," +
								"OBJ_NOT_PUBLISH_FLAG		," +
								"NOT_APPROVE_MARK			," +
								"UPLOAD_DATE				," +
								"SAMPLE_MOVIE_SEQ			," +
								"THUMBNAIL_PIC_SEQ			," +
								"CAST_MOVIE_ATTR_TYPE_NM	," +
								"CAST_MOVIE_ATTR_NM			," +
								"MOVIE_TYPE		" +
							"FROM(" +
							" SELECT VW_CAST_MOVIE01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_CAST_MOVIE01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pMovieType,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_MOVIE01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetList(string pSiteCd, string pUserSeq, string pUserCharNo, string pMovieType) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = " ORDER BY SITE_CD,USER_SEQ,USER_CHAR_NO,MOVIE_SEQ DESC ";
			string sSql = "SELECT " +
								"SITE_CD					," +
								"USER_SEQ					," +
								"USER_CHAR_NO				," +
								"LOGIN_ID					," +
								"MOVIE_SEQ					," +
								"MOVIE_TITLE				," +
								"CHARGE_POINT				," +
								"OBJ_NOT_APPROVE_FLAG		," +
								"OBJ_NOT_PUBLISH_FLAG		," +
								"NOT_APPROVE_MARK			," +
								"UPLOAD_DATE				," +
								"SAMPLE_MOVIE_SEQ			," +
								"THUMBNAIL_PIC_SEQ			," +
								"MOVIE_TYPE					," +
								"GET_PHOTO_IMG_PATH(SITE_CD,LOGIN_ID,THUMBNAIL_PIC_SEQ,NULL) AS THUMBNAIL_IMG_PATH " +
							"FROM" +
							"	VW_CAST_MOVIE01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pMovieType,ref sWhere);
			sSql = sSql + sWhere;
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql, conn)) {
				for (int i = 0; i < objParms.Length; i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds, "VW_CAST_MOVIE01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}



	private OracleParameter[] CreateWhere(string pSiteCd,string pUserSeq,string pUserCharNo,string pMovieType,ref string pWhere) {
		ArrayList list = new ArrayList();

		if (!pSiteCd.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!pUserSeq.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhere);
			list.Add(new OracleParameter("USER_SEQ",pUserSeq));
			if (!pUserCharNo.Equals("")) {
				iBridCommLib.SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhere);
				list.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));
			}
		}
		if (!pMovieType.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("MOVIE_TYPE = :MOVIE_TYPE",ref pWhere);
			list.Add(new OracleParameter("MOVIE_TYPE",pMovieType));

			if (pMovieType.Equals(ViCommConst.ATTACHED_MAIL.ToString())) {
				iBridCommLib.SysPrograms.SqlAppendWhere("(EXISTS (SELECT 1 FROM T_CAST_MOVIE WHERE SITE_CD = VW_CAST_MOVIE01.SITE_CD AND USER_SEQ = VW_CAST_MOVIE01.USER_SEQ AND USER_CHAR_NO = VW_CAST_MOVIE01.USER_CHAR_NO AND MOVIE_SEQ = VW_CAST_MOVIE01.MOVIE_SEQ AND MAIL_STOCK_FLAG = 1) OR OBJ_NOT_APPROVE_FLAG = 1)",ref pWhere);
			}
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public bool GetOne(decimal pMovieSeq) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try{
			conn = DbConnect();

			string sSql = "SELECT SITE_CD,USER_SEQ,USER_CHAR_NO,LOGIN_ID,MOVIE_SEQ,MOVIE_TITLE,CHARGE_POINT,MOVIE_TYPE,HANDLE_NM,CAST_NM " +
											"FROM VW_CAST_MOVIE01 WHERE MOVIE_SEQ =:MOVIE_SEQ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("MOVIE_SEQ",pMovieSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_MOVIE01");
					if (ds.Tables["VW_CAST_MOVIE01"].Rows.Count != 0) {
						dr = ds.Tables["VW_CAST_MOVIE01"].Rows[0];
						siteCd = dr["SITE_CD"].ToString();
						userSeq = decimal.Parse(dr["USER_SEQ"].ToString());
						userCharNo = dr["USER_CHAR_NO"].ToString();
						loginId = dr["LOGIN_ID"].ToString();
						movieSeq = decimal.Parse(dr["MOVIE_SEQ"].ToString());
						movieTitle = dr["MOVIE_TITLE"].ToString();
						chargePoint = int.Parse(dr["CHARGE_POINT"].ToString());
						movieType = int.Parse(dr["MOVIE_TYPE"].ToString());
						handleNm = dr["HANDLE_NM"].ToString();
						castNm = dr["CAST_NM"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public decimal GetMovieNo() {
		decimal dMovieNo = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT SEQ_OBJ.NEXTVAL FROM DUAL";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				dMovieNo = (decimal)cmd.ExecuteScalar();
			}
		} finally {
			conn.Close();
		}
		return dMovieNo;
	}

	public int GetApproveManagePageCount(string pSiteCd,string pNotApproveFlag,string pMovieType) {
		return GetApproveManagePageCount(pSiteCd,pNotApproveFlag,null,pMovieType,null,null,null);
	}
	public int GetApproveManagePageCount(string pSiteCd, string pNotApproveFlag, string pNotPublishFlag, string pMovieType, string pUploadDateFrom, string pUploadDateTo, string pLoginId) {
		return GetApproveManagePageCount(pSiteCd, pNotApproveFlag, pNotPublishFlag, pMovieType, pUploadDateFrom, pUploadDateTo, null, null, pLoginId, string.Empty);
	}
	public int GetApproveManagePageCount(string pSiteCd,string pNotApproveFlag,string pNotPublishFlag,string pMovieType,string pUploadDateFrom,string pUploadDateTo,string pCastMovieAttrType,string pCastMovieAttrTypeValue,string pLoginId) {
		return GetApproveManagePageCount(pSiteCd,pNotApproveFlag,pNotPublishFlag,pMovieType,pUploadDateFrom,pUploadDateTo,pCastMovieAttrType,pCastMovieAttrTypeValue,pLoginId,string.Empty);
	}
	public int GetApproveManagePageCount(string pSiteCd, string pNotApproveFlag, string pNotPublishFlag, string pMovieType, string pUploadDateFrom, string pUploadDateTo, string pCastMovieAttrType, string pCastMovieAttrTypeValue, string pLoginId, string pItemNo) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_CAST_MOVIE02 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateApproveManageWhere(pSiteCd, pNotApproveFlag, pNotPublishFlag, pMovieType, pUploadDateFrom, pUploadDateTo, pCastMovieAttrType, pCastMovieAttrTypeValue, pLoginId, pItemNo, ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetApproveManagePageCollection(string pSiteCd,string pNotApproveFlag,string pMovieType,int startRowIndex,int maximumRows) {
		return GetApproveManagePageCollection(pSiteCd,pNotApproveFlag,null,pMovieType,null,null,null,startRowIndex,maximumRows);
	}
	public DataSet GetApproveManagePageCollection(string pSiteCd, string pNotApproveFlag, string pNotPublishFlag, string pMovieType, string pUploadDateFrom, string pUploadDateTo, string pLoginId, int startRowIndex, int maximumRows) {
		return GetApproveManagePageCollection(pSiteCd, pNotApproveFlag, pNotPublishFlag, pMovieType, pUploadDateFrom, pUploadDateTo, null, null, pLoginId, string.Empty, startRowIndex, maximumRows);
	}
	public DataSet GetApproveManagePageCollection(string pSiteCd,string pNotApproveFlag,string pNotPublishFlag,string pMovieType,string pUploadDateFrom,string pUploadDateTo,string pCastMovieAttrType,string pCastMovieAttrTypeValue,string pLoginId,int startRowIndex,int maximumRows) {
		return GetApproveManagePageCollection(pSiteCd,pNotApproveFlag,pNotPublishFlag,pMovieType,pUploadDateFrom,pUploadDateTo,pCastMovieAttrType,pCastMovieAttrTypeValue,pLoginId,string.Empty,startRowIndex,maximumRows);
	}
	public DataSet GetApproveManagePageCollection(string pSiteCd, string pNotApproveFlag, string pNotPublishFlag, string pMovieType, string pUploadDateFrom, string pUploadDateTo, string pCastMovieAttrType, string pCastMovieAttrTypeValue, string pLoginId, string pItemNo, int startRowIndex, int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY SITE_CD,MOVIE_SEQ DESC ";

			string sSql = "SELECT " +
								"SITE_CD				," +
								"SITE_NM				," +
								"USER_SEQ				," +
								"USER_CHAR_NO			," +
								"LOGIN_ID				," +
								"CAST_NM				," +
								"HANDLE_NM				," +
								"EMAIL_ADDR				," +
								"UPLOAD_DATE			," +
								"MOVIE_TITLE			," +
								"MOVIE_DOC				," +
								"MOVIE_SEQ				," +
								"MOVIE_TYPE				," +
								"CAST_MOVIE_ATTR_TYPE_SEQ," +
								"CAST_MOVIE_ATTR_TYPE_NM," +
								"CAST_MOVIE_ATTR_SEQ	," +
								"CAST_MOVIE_ATTR_NM		," +
								"OBJ_NOT_PUBLISH_FLAG	," +
								"CAUTION_FLAG			," +
								"NOT_APPROVE_MARK_ADMIN		" +
							"FROM(" +
							" SELECT VW_CAST_MOVIE02.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_CAST_MOVIE02  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateApproveManageWhere(pSiteCd, pNotApproveFlag, pNotPublishFlag, pMovieType, pUploadDateFrom, pUploadDateTo, pCastMovieAttrType, pCastMovieAttrTypeValue, pLoginId, pItemNo, ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_MOVIE02");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateApproveManageWhere(string pSiteCd, string pNotApproveFlag, string pNotPublishFlag, string pMovieType, string pUploadDateFrom, string pUploadDateTo, string pCastMovieAttrType, string pCastMovieAttrTypeValue, string pLoginId, string pItemNo, ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();

		if (pSiteCd.Equals("") == false) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (pMovieType.Equals("") == false) {
			SysPrograms.SqlAppendWhere("MOVIE_TYPE = :MOVIE_TYPE",ref pWhere);
			list.Add(new OracleParameter("MOVIE_TYPE",pMovieType));
		}

		if ((!string.IsNullOrEmpty(pNotApproveFlag)) || (string.IsNullOrEmpty(pNotPublishFlag))) {
			if (pNotApproveFlag.Equals(ViCommConst.FLAG_ON.ToString()) & pNotPublishFlag.Equals(ViCommConst.FLAG_ON.ToString())) {
				SysPrograms.SqlAppendWhere("(OBJ_NOT_APPROVE_FLAG = :OBJ_NOT_APPROVE_FLAG OR OBJ_NOT_PUBLISH_FLAG = :OBJ_NOT_PUBLISH_FLAG)",ref pWhere);
				list.Add(new OracleParameter("OBJ_NOT_APPROVE_FLAG",pNotApproveFlag));
				list.Add(new OracleParameter("OBJ_NOT_PUBLISH_FLAG",pNotPublishFlag));
			} else {
				if (pNotApproveFlag.Equals("") == false) {
					SysPrograms.SqlAppendWhere("OBJ_NOT_APPROVE_FLAG = :OBJ_NOT_APPROVE_FLAG",ref pWhere);
					list.Add(new OracleParameter("OBJ_NOT_APPROVE_FLAG",pNotApproveFlag));
				}

				// 非公開フラグ
				if (!string.IsNullOrEmpty(pNotPublishFlag)) {
					SysPrograms.SqlAppendWhere("OBJ_NOT_PUBLISH_FLAG = :OBJ_NOT_PUBLISH_FLAG",ref pWhere);
					list.Add(new OracleParameter("OBJ_NOT_PUBLISH_FLAG",pNotPublishFlag));
				}
			}
		}

		// アップロード日時(from)
		if(!string.IsNullOrEmpty(pUploadDateFrom) ){
			SysPrograms.SqlAppendWhere("UPLOAD_DATE_RAW >= :UPLOAD_DATE_FROM", ref pWhere);
			DateTime dtFrom = DateTime.ParseExact(pUploadDateFrom, "yyyy/MM/dd", null, System.Globalization.DateTimeStyles.None).Date;
			list.Add(new OracleParameter("UPLOAD_DATE_FROM", OracleDbType.Date, dtFrom, ParameterDirection.Input));
		}

		// アップロード日時(to)
		if(!string.IsNullOrEmpty(pUploadDateTo)) {
			SysPrograms.SqlAppendWhere("UPLOAD_DATE_RAW < :UPLOAD_DATE_TO", ref pWhere);
			DateTime dtFrom = DateTime.ParseExact(pUploadDateTo, "yyyy/MM/dd", null, System.Globalization.DateTimeStyles.None).Date.AddDays(1);
			list.Add(new OracleParameter("UPLOAD_DATE_TO", OracleDbType.Date, dtFrom, ParameterDirection.Input));
		}

		// ﾛｸﾞｲﾝID
		if (!string.IsNullOrEmpty(pLoginId)) {
			SysPrograms.SqlAppendWhere("LOGIN_ID = :LOGIN_ID", ref pWhere);
			list.Add(new OracleParameter("LOGIN_ID", pLoginId));
		}


		// 動画属性区分
		if (!string.IsNullOrEmpty(pCastMovieAttrType)) {
			SysPrograms.SqlAppendWhere("CAST_MOVIE_ATTR_TYPE_SEQ = :CAST_MOVIE_ATTR_TYPE_SEQ", ref pWhere);
			list.Add(new OracleParameter("CAST_MOVIE_ATTR_TYPE_SEQ", pCastMovieAttrType));
		}

		// 動画属性値
		if (!string.IsNullOrEmpty(pCastMovieAttrTypeValue)) {
			SysPrograms.SqlAppendWhere("CAST_MOVIE_ATTR_SEQ = :CAST_MOVIE_ATTR_SEQ", ref pWhere);
			list.Add(new OracleParameter("CAST_MOVIE_ATTR_SEQ", pCastMovieAttrTypeValue));
		}

		// 企画動画を取捨
		if (string.IsNullOrEmpty(pItemNo)) {
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_CAST_MOVIE WHERE SITE_CD = VW_CAST_MOVIE02.SITE_CD AND USER_SEQ = VW_CAST_MOVIE02.USER_SEQ AND USER_CHAR_NO = VW_CAST_MOVIE02.USER_CHAR_NO AND MOVIE_SEQ = VW_CAST_MOVIE02.MOVIE_SEQ AND PLANNING_TYPE = 0)",ref pWhere);
		} else {
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_CAST_MOVIE WHERE SITE_CD = VW_CAST_MOVIE02.SITE_CD AND USER_SEQ = VW_CAST_MOVIE02.USER_SEQ AND USER_CHAR_NO = VW_CAST_MOVIE02.USER_CHAR_NO AND MOVIE_SEQ = VW_CAST_MOVIE02.MOVIE_SEQ AND PLANNING_TYPE <> 0)",ref pWhere);
		}

		// 画像種別がメール添付用の場合にストック利用可フラグでの絞り込み
		if (!string.IsNullOrEmpty(pMovieType) && pMovieType.Equals(ViCommConst.ATTACHED_MAIL.ToString()) &&
			!string.IsNullOrEmpty(pNotApproveFlag) && pNotApproveFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_CAST_MOVIE WHERE SITE_CD = VW_CAST_MOVIE02.SITE_CD AND USER_SEQ = VW_CAST_MOVIE02.USER_SEQ AND USER_CHAR_NO = VW_CAST_MOVIE02.USER_CHAR_NO AND MOVIE_SEQ = VW_CAST_MOVIE02.MOVIE_SEQ AND MAIL_STOCK_FLAG = 1)",ref pWhere);
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public DataSet GetCsvList(string pSiteCd,string pUserSeq) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT " +
								"MOVIE_SEQ				," +
								"MOVIE_TITLE			," +
								"CHARGE_POINT			," +
								"OBJ_NOT_APPROVE_FLAG	," +
								"MOVIE_TYPE				," +
								"TO_CHAR(UPLOAD_DATE,'YYYYMMDDHH24MISS') AS UPLOAD_DATE " +
							"FROM " +
								"T_CAST_MOVIE " +
							"WHERE " +
								"SITE_CD	=:SITE_CD	AND " +
								"USER_SEQ	=:USER_SEQ " +
							"ORDER BY SITE_CD,USER_SEQ,MOVIE_TYPE,MOVIE_SEQ DESC ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_CAST_MOVIE");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
}
