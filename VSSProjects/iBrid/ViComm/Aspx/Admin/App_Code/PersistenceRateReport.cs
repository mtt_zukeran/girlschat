﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 継続率レポート--	Progaram ID		: PersistenceRateReport
--
--  Creation Date	: 2011.08.11
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;


public class PersistenceRateReport : DbSession {

	public PersistenceRateReport() {
	}

	public int GetPageCount(string pSiteCd,string pSexCd,string pReportDayFrom,string pReportDayTo) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	COUNT(*) ");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_PERSISTENCE_RATE_REPORT	");

		string sWhereClause;
		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pSexCd,pReportDayFrom,pReportDayTo,out sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		}
		finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pSexCd,string pReportDayFrom,string pReportDayTo,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	SITE_CD			,");
		oSqlBuilder.AppendLine("	REGIST_DAY		,");
		oSqlBuilder.AppendLine("	SEX_CD			,");
		oSqlBuilder.AppendLine("	REGIST_COUNT	,");
		oSqlBuilder.AppendLine("	CASE WHEN REGIST_COUNT > 0 AND TO_DATE(REGIST_DAY,'YYYY/MM/DD') +  1 <= TRUNC(SYSDATE,'DD') THEN TRUNC(AFTER_1DAY_LOGIN_COUNT_UNIQUE  / REGIST_COUNT * 100,2) ELSE -1 END  AFTER_1DAY	,");
		oSqlBuilder.AppendLine("	CASE WHEN REGIST_COUNT > 0 AND TO_DATE(REGIST_DAY,'YYYY/MM/DD') +  3 <= TRUNC(SYSDATE,'DD') THEN TRUNC(AFTER_3DAY_LOGIN_COUNT_UNIQUE  / REGIST_COUNT * 100,2) ELSE -1 END  AFTER_3DAY	,");
		oSqlBuilder.AppendLine("	CASE WHEN REGIST_COUNT > 0 AND TO_DATE(REGIST_DAY,'YYYY/MM/DD') +  7 <= TRUNC(SYSDATE,'DD') THEN TRUNC(AFTER_7DAY_LOGIN_COUNT_UNIQUE  / REGIST_COUNT * 100,2) ELSE -1 END  AFTER_7DAY	,");
		oSqlBuilder.AppendLine("	CASE WHEN REGIST_COUNT > 0 AND TO_DATE(REGIST_DAY,'YYYY/MM/DD') + 14 <= TRUNC(SYSDATE,'DD') THEN TRUNC(AFTER_14DAY_LOGIN_COUNT_UNIQUE / REGIST_COUNT * 100,2) ELSE -1 END AFTER_14DAY	,");
		oSqlBuilder.AppendLine("	CASE WHEN REGIST_COUNT > 0 AND TO_DATE(REGIST_DAY,'YYYY/MM/DD') + 30 <= TRUNC(SYSDATE,'DD') THEN TRUNC(AFTER_30DAY_LOGIN_COUNT_UNIQUE / REGIST_COUNT * 100,2) ELSE -1 END AFTER_30DAY	,");
		oSqlBuilder.AppendLine("	CASE WHEN REGIST_COUNT > 0 AND TO_DATE(REGIST_DAY,'YYYY/MM/DD') + 60 <= TRUNC(SYSDATE,'DD') THEN TRUNC(AFTER_60DAY_LOGIN_COUNT_UNIQUE / REGIST_COUNT * 100,2) ELSE -1 END AFTER_60DAY	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_PERSISTENCE_RATE_REPORT	");

		string sWhereClause;
		string sPagingSql;
		string sSortExpression = "ORDER BY SITE_CD, REGIST_DAY	";

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pSexCd,pReportDayFrom,pReportDayTo,out sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		}
		finally {
			conn.Close();
		}
		return oDs;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pSexCd,string pReportDayFrom,string pReportDayTo,out string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!string.IsNullOrEmpty(pSexCd)) {
			SysPrograms.SqlAppendWhere("SEX_CD = :SEX_CD",ref pWhere);
			list.Add(new OracleParameter("SEX_CD",pSexCd));
		}

		if (!string.IsNullOrEmpty(pReportDayFrom) && !string.IsNullOrEmpty(pReportDayTo)) {
			SysPrograms.SqlAppendWhere("REGIST_DAY >= :REGIST_DAY_FROM AND REGIST_DAY <= :REGIST_DAY_TO",ref pWhere);
			list.Add(new OracleParameter("REGIST_DAY_FROM",pReportDayFrom));
			list.Add(new OracleParameter("REGIST_DAY_TO",pReportDayTo));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}
