﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: サイト
--	Progaram ID		: Site
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;
using ViComm;

public class Site:DbSession {

	public string siteCd;
	public string siteNm;
	public string webPhisicalDir;
	public string url;
	public string colorBack;
	public string colorChar;
	public string colorIndex;
	public string colorLink;
	public string colorLine;
	public int sizeLine;
	public string footerHtmlDoc;
	public string mailHost;
	public string supportEmailAddr;
	public string supportTel;
	public string ivpLocCd;
	public string ivpSiteCd;
	public string siteType;
	public int enablePrivteTalkMenuFlag;
	public int useOtherSysInfoFlag;
	public int multiCharFlag;
	public int urgeDays;
	public int receiptSight;
	public string nextbusinesstime;
	public int viewCallCountFlag;
	public string hostNm;
	public string globalIpAddr;
	public string iisSiteIdentifier;
	public int transferFee;

	private Hashtable siteSupplement;

	public Site() {
		siteSupplement = new Hashtable();
	}

	public int GetPageCount() {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_SITE01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY SITE_CD";
			string sSql = "SELECT " +
							"SITE_CD		," +
							"SITE_NM		," +
							"SITE_MARK		," +
							"HOST_NM		," +
							"SUB_HOST_NM	," +
							"URL			," +
							"LOCAL_IP_ADDR	," +
							"LOCAL_PORT		," +
							"PRIORITY		," +
							"REVISION_NO	," +
							"UPDATE_DATE " +
							"FROM(" +
							" SELECT VW_SITE01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_SITE01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_SITE01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(ref string pWhere) {
		ArrayList list = new ArrayList();
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}


	public DataSet GetList() {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSiteCd = iBridUtil.GetStringValue(HttpContext.Current.Session["SiteCd"]);
			string sSql = "SELECT SITE_CD,SITE_NM,SITE_MARK FROM T_SITE WHERE NA_FLAG = 0 ";

			if (!sSiteCd.Equals("")) {
				sSql += " AND SITE_CD =: SITE_CD ";
			}
			sSql += " ORDER BY PRIORITY,SITE_CD ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				if (!sSiteCd.Equals("")) {
					cmd.Parameters.Add("SITE_CD",sSiteCd);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_SITE");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetPreviousVerList() {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT SITE_CD,SITE_NM,SITE_MARK FROM T_SITE WHERE NA_FLAG = 0 AND USE_OTHER_SYS_INFO_FLAG = 1";
			sSql = sSql + " ORDER BY PRIORITY,SITE_CD ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_SITE");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	public DataSet GetNewVerList() {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSiteCd = iBridUtil.GetStringValue(HttpContext.Current.Session["SiteCd"]);
			string sSql = "SELECT SITE_CD,SITE_NM,SITE_MARK FROM T_SITE WHERE NA_FLAG = 0";

			if (!sSiteCd.Equals("")) {
				sSql += " AND SITE_CD = : SITE_CD ";
			}

			sSql = sSql + " ORDER BY PRIORITY,SITE_CD ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				if (!sSiteCd.Equals("")) {
					cmd.Parameters.Add("SITE_CD",sSiteCd);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_SITE");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetAbleToSiteList() {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSiteCd = iBridUtil.GetStringValue(HttpContext.Current.Session["SiteCd"]);

			string sSql = "SELECT DISTINCT " +
							"SITE_CD    ," +
							"SITE_NM    ," +
							"SITE_MARK  ," +
							"PRIORITY   " +
						  "FROM (" +
							"SELECT " +
								"T_SITE.SITE_CD     ," +
								"T_SITE.SITE_NM     ," +
								"T_SITE.SITE_MARK   ," +
								"T_SITE.PRIORITY    " +
							  "FROM " +
								"T_SITE             ," +
								"T_SITE_MANAGEMENT  ," +
								"T_USER_RANK        " +
							  "WHERE " +
								"T_SITE.SITE_CD                     = T_SITE_MANAGEMENT.SITE_CD AND " +
								"T_SITE.SITE_CD                     = T_USER_RANK.SITE_CD       AND " +
								"T_SITE.NA_FLAG                     = 0                         AND " +
								"T_SITE.USE_OTHER_SYS_INFO_FLAG     = 0 ";

			if (!sSiteCd.Equals("")) {
				sSql += " AND T_SITE.SITE_CD = : SITE_CD ";
			}
			sSql = sSql + " ) ORDER BY PRIORITY,SITE_CD ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				if (!sSiteCd.Equals("")) {
					cmd.Parameters.Add("SITE_CD",sSiteCd);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_SITE");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	public bool GetValue(string pSiteCd,string pItem,ref string pValue) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect();
			string sSql = "SELECT " +
								"SITE_CD					," +
								"SITE_NM					," +
								"HOST_NM					," +
								"MAILER_FTP_ID				," +
								"MAILER_FTP_PW				," +
								"TX_HI_MAILER_IP			," +
								"WEB_PHISICAL_DIR			," +
								"TRANSFER_FEE				," +
								"MAIL_DOC_NOT_USE_WEB_FACE_FLAG," +
								"CAST_REGIST_REPORT_TIMING	," +
								"MAN_REGIST_REPORT_TIMING	" +
							"FROM " +
								"T_SITE " +
							"WHERE " +
								"SITE_CD = :SITE_CD ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_SITE");
					if (ds.Tables["T_SITE"].Rows.Count != 0) {
						dr = ds.Tables["T_SITE"].Rows[0];
						pValue = dr[pItem].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public bool GetOne(string pSiteCd) {
		DataSet ds;
		bool bExist = false;
		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand("SELECT * FROM VW_SITE01 WHERE SITE_CD = :SITE_CD",conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				bExist = SetData(ds);
			}
		} finally {
			conn.Close();
		}
		SetSupplement();

		return bExist;
	}

	private bool SetData(DataSet ds) {
		DataRow dr;
		bool bRet = false;
		using (da = new OracleDataAdapter(cmd)) {
			da.Fill(ds,"VW_SITE01");
			if (ds.Tables["VW_SITE01"].Rows.Count != 0) {
				dr = ds.Tables["VW_SITE01"].Rows[0];
				siteCd = dr["SITE_CD"].ToString();
				siteNm = dr["SITE_NM"].ToString();
				webPhisicalDir = dr["WEB_PHISICAL_DIR"].ToString();
				url = dr["URL"].ToString();
				colorBack = dr["COLOR_BACK"].ToString();
				colorChar = dr["COLOR_CHAR"].ToString();
				colorIndex = dr["COLOR_INDEX"].ToString();
				colorLink = dr["COLOR_LINK"].ToString();
				colorLine = dr["COLOR_LINE"].ToString();
				sizeLine = int.Parse(dr["SIZE_LINE"].ToString());
				mailHost = dr["MAIL_HOST"].ToString();
				supportEmailAddr = dr["SUPPORT_EMAIL_ADDR"].ToString();
				supportTel = dr["SUPPORT_TEL"].ToString();
				ivpLocCd = dr["IVP_LOC_CD"].ToString();
				ivpSiteCd = dr["IVP_SITE_CD"].ToString();
				siteType = dr["SITE_TYPE"].ToString();
				enablePrivteTalkMenuFlag = int.Parse(dr["ENABLE_PRIVATE_TALK_MENU_FLAG"].ToString());
				footerHtmlDoc = dr["FOOTER_HTML_DOC"].ToString();
				useOtherSysInfoFlag = int.Parse(dr["USE_OTHER_SYS_INFO_FLAG"].ToString());
				multiCharFlag = int.Parse(dr["MULTI_CHAR_FLAG"].ToString());
				urgeDays = int.Parse(dr["URGE_DAYS"].ToString());
				receiptSight = int.Parse(dr["RECEIPT_SIGHT"].ToString());
				nextbusinesstime = dr["NEXT_BUSINESS_TIME"].ToString();
				viewCallCountFlag = int.Parse(dr["VIEW_CALL_COUNT_FLAG"].ToString());
				hostNm = dr["HOST_NM"].ToString();
				globalIpAddr = dr["GLOBAL_IP_ADDR"].ToString();
				iisSiteIdentifier = dr["IIS_SITE_IDENTIFIER"].ToString();
				transferFee = int.Parse(dr["TRANSFER_FEE"].ToString());
				bRet = true;
			}
		}
		return bRet;
	}

	public void SetSupplement() {
		DataSet ds;
		siteSupplement.Clear();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand("SELECT SUPPLEMENT_CD,SUPPLEMENT_VALUE FROM T_SITE_SUPPLEMENT WHERE SITE_CD = :SITE_CD",conn))
			using (ds = new DataSet())
			using (da = new OracleDataAdapter(cmd)) {
				cmd.Parameters.Add("SITE_CD",siteCd);
				da.Fill(ds,"T_SITE_SUPPLEMENT");
				foreach (DataRow dr in ds.Tables[0].Rows) {
					siteSupplement.Add(dr["SUPPLEMENT_CD"],dr["SUPPLEMENT_VALUE"]);
				}
			}
		} finally {
			conn.Close();
		}
	}

	public bool IsExistPriority(string pSiteCd,string pPriority) {
		DataSet ds;
		bool bExist = false;
		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(" SELECT * FROM T_SITE " +
											" WHERE " +
												" SITE_CD	!= :SITE_CD AND " +
												" PRIORITY	= :PRIORITY ",conn))
			using (ds = new DataSet())
			using (da = new OracleDataAdapter(cmd)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("PRIORITY",pPriority);
				da.Fill(ds,"T_SITE");
				if (ds.Tables["T_SITE"].Rows.Count != 0) {
					bExist = true;
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public string GetSupplement(string pCode) {
		return iBridUtil.GetStringValue(siteSupplement[pCode]);
	}

	public bool GetMultiCharFlag(string pSiteCd) {
		bool bResult = false;
		if (GetOne(pSiteCd)) {
			if (multiCharFlag == 1) {
				bResult = true;
			}
		}
		return bResult;
	}

	public bool IsAvailablePayAfterFlag(string pSiteCd) {
		bool bExist = false;
		try {
			conn = DbConnect();
			DataSet ds = new DataSet();
			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT ");
			sSql.Append("   * ");
			sSql.Append("FROM ");
			sSql.Append("   T_SITE ");
			sSql.Append("WHERE ");
			sSql.Append("   SITE_CD						= :SITE_CD AND ");
			sSql.Append("	AVAILABLE_PAY_AFTER_FLAG	= :AVAILABLE_PAY_AFTER_FLAG ");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (da = new OracleDataAdapter(cmd)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("AVAILABLE_PAY_AFTER_FLAG",ViCommConst.FLAG_ON);
				da.Fill(ds,"T_SITE");
				if (ds.Tables["T_SITE"].Rows.Count != 0) {
					bExist = true;
				}
			}
		} finally {
			conn.Close();
		}

		return bExist;
	}

	public bool IsUsedChargeSettleFlag(string pSiteCd) {
		bool bExist = false;
		try {
			conn = DbConnect();
			DataSet ds = new DataSet();
			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT ");
			sSql.Append("   * ");
			sSql.Append("FROM ");
			sSql.Append("   T_SITE ");
			sSql.Append("WHERE ");
			sSql.Append("   SITE_CD						= :SITE_CD AND ");
			sSql.Append("	USED_CHARGE_SETTLE_FLAG		= :USED_CHARGE_SETTLE_FLAG ");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (da = new OracleDataAdapter(cmd)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USED_CHARGE_SETTLE_FLAG",ViCommConst.FLAG_ON);
				da.Fill(ds,"T_SITE");
				if (ds.Tables["T_SITE"].Rows.Count != 0) {
					bExist = true;
				}
			}
		} finally {
			conn.Close();
		}

		return bExist;
	}
}
