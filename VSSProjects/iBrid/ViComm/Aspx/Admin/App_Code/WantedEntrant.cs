﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: この娘を探せ・男性エントリー(詳細)


--	Progaram ID		: WantedEntrant
--  Creation Date	: 2011.03.23
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class WantedEntrant : DbSession {
	public WantedEntrant() {
	}

	public int GetPageCount(string pSiteCd, string pExecutionDay, string pCaughtFlag, string pPointAcquiredFlag) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_WANTED_ENTRANT		WE	,");
		oSqlBuilder.AppendLine("	T_USER					U	,");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER	UMC");

		StringBuilder oWhereBuilder = new StringBuilder();
		oWhereBuilder.AppendLine("WHERE	");
		oWhereBuilder.AppendLine("	WE.USER_SEQ				= U.USER_SEQ			AND");
		oWhereBuilder.AppendLine("	WE.SITE_CD				= UMC.SITE_CD			AND");
		oWhereBuilder.AppendLine("	WE.USER_SEQ				= UMC.USER_SEQ			AND");
		oWhereBuilder.AppendLine("	WE.USER_CHAR_NO			= UMC.USER_CHAR_NO		");

		string sWhereClause = oWhereBuilder.ToString();
		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd, pExecutionDay, pCaughtFlag, pPointAcquiredFlag, ref sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(), conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}


	public DataSet GetPageCollection(string pSiteCd, string pExecutionDay, string pCaughtFlag, string pPointAcquiredFlag, int startRowIndex, int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	U.LOGIN_ID				,");
		oSqlBuilder.AppendLine("	UMC.HANDLE_NM			,");
		oSqlBuilder.AppendLine("	WE.SITE_CD				,");
		oSqlBuilder.AppendLine("	WE.USER_SEQ				,");
		oSqlBuilder.AppendLine("	WE.USER_CHAR_NO			,");
		oSqlBuilder.AppendLine("	WE.EXECUTION_DAY		,");
		oSqlBuilder.AppendLine("	WE.ENTRY_TIME			,");
		oSqlBuilder.AppendLine("	WE.CAUGHT_TIME			,");
		oSqlBuilder.AppendLine("	WE.POINT_ACQUIRED_TIME	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_WANTED_ENTRANT		WE	,");
		oSqlBuilder.AppendLine("	T_USER					U	,");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER	UMC");

		StringBuilder oWhereBuilder = new StringBuilder();
		oWhereBuilder.AppendLine("WHERE	");
		oWhereBuilder.AppendLine("	WE.USER_SEQ				= U.USER_SEQ			AND");
		oWhereBuilder.AppendLine("	WE.SITE_CD				= UMC.SITE_CD			AND");
		oWhereBuilder.AppendLine("	WE.USER_SEQ				= UMC.USER_SEQ			AND");
		oWhereBuilder.AppendLine("	WE.USER_CHAR_NO			= UMC.USER_CHAR_NO		");

		string sWhereClause = oWhereBuilder.ToString();
		string sSortExpression = "ORDER BY WE.SITE_CD, WE.ENTRY_TIME, U.LOGIN_ID";
		string sPagingSql;

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd, pExecutionDay, pCaughtFlag, pPointAcquiredFlag, ref sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(), sSortExpression, startRowIndex, maximumRows, out sPagingSql);

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql, conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}

	private OracleParameter[] CreateWhere(string pSiteCd, string pExecutionDay, string pCaughtFlag, string pPointAcquiredFlag, ref string pWhere) {
		List<OracleParameter> oOracleParameterList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere(":SITE_CD = WE.SITE_CD", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("SITE_CD", pSiteCd));
		}

		if (!string.IsNullOrEmpty(pExecutionDay)) {
			SysPrograms.SqlAppendWhere(":EXECUTION_DAY = WE.EXECUTION_DAY", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("EXECUTION_DAY", pExecutionDay));
		}

		if (!string.IsNullOrEmpty(pCaughtFlag)) {
			SysPrograms.SqlAppendWhere(":CAUGHT_FLAG = WE.CAUGHT_FLAG", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("CAUGHT_FLAG", pCaughtFlag));
		}

		if (!string.IsNullOrEmpty(pPointAcquiredFlag)) {
			SysPrograms.SqlAppendWhere(":POINT_ACQUIRED_FLAG = WE.POINT_ACQUIRED_FLAG", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("POINT_ACQUIRED_FLAG", pPointAcquiredFlag));
		}

		return oOracleParameterList.ToArray();
	}
}
