﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: OKリスト
--	Progaram ID		: OkPlayList
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;

public class OkPlayList:DbSession {

	public OkPlayList() {
	}

	public DataSet GetList(string pSiteCd) {
		DataSet ds;
		try{
			conn = DbConnect("OkPlayList.GetList");
			ds = new DataSet();

			string sSql = "SELECT OK_PLAY,OK_PLAY_NM FROM T_OK_PLAY_LIST " +
							"WHERE " +
							" SITE_CD	= :SITE_CD " +
							"ORDER BY " +
								"SITE_CD,OK_PLAY ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_OK_PLAY_LIST");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

}