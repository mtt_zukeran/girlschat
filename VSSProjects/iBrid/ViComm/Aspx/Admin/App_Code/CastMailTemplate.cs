/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: キャストメールテンプレート
--	Progaram ID		: CastMailTemplate
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class CastMailTemplate:DbSession {

	public CastMailTemplate() {
	}

	public int GetPageCount(string pSiteCd,string pUserSeq,string pUserCharNo) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_CAST_MAIL_TEMPLATE01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pUserSeq,string pUserCharNo,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY SITE_CD,USER_SEQ,USER_CHAR_NO,MAIL_TEMPLATE_NO ";
			string sSql = "SELECT " +
							"SITE_CD			," +
							"USER_SEQ			," +
							"MAIL_TEMPLATE_NO	," +
							"MAIL_TEMPLATE_TYPE	," +
							"MAIL_TITLE			," +
							"HTML_DOC_SEQ		," +
							"UPDATE_DATE		," +
							"REVISION_NO		," +
							"HTML_DOC_TITLE		," +
							"SUBSTR(HTML_DOC1,1,20) DOC," +
							"HTML_DOC1			," +
							"HTML_DOC2			," +
							"HTML_DOC3			," +
							"HTML_DOC4			," +
							"MAIL_TEMPLATE_TYPE_NM," +
							"SITE_NM	" +
							"FROM(" +
							" SELECT VW_CAST_MAIL_TEMPLATE01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_CAST_MAIL_TEMPLATE01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_MAIL_TEMPLATE01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,string pUserSeq,string pUserCharNo,ref string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD ",ref pWhere);
		list.Add(new OracleParameter("SITE_CD",pSiteCd));

		SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ ",ref pWhere);
		list.Add(new OracleParameter("USER_SEQ",pUserSeq));

		SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO ",ref pWhere);
		list.Add(new OracleParameter("USER_CHAR_NO", pUserCharNo));
		
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

}
