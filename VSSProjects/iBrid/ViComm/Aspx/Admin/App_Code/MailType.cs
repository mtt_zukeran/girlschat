/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メール種別
--	Progaram ID		: MailType
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class MailType:DbSession {

	public MailType() {
	}


	public DataSet GetList() {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT MAIL_TYPE,MAIL_TYPE_NM FROM T_MAIL_TYPE ";
			sSql = sSql + " ORDER BY MAIL_TYPE_NM";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
}
