﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: ｷｬｽﾄ用お宝--	Progaram ID		: CastTreasure
--
--  Creation Date	: 2011.07.25
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;


public class CastTreasure : DbSession {

	public CastTreasure() {
	}

	public int GetPageCount(string pSiteCd,string pStageGroupType) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_CAST_TREASURE		CT,");
		oSqlBuilder.AppendLine("	T_STAGE_GROUP		SG");

		string sWhereClause;
		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd, pStageGroupType, out sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		}
		finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd, string pStageGroupType, int startRowIndex, int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	CT.SITE_CD						,");
		oSqlBuilder.AppendLine("	CT.STAGE_GROUP_TYPE				,");
		oSqlBuilder.AppendLine("	CT.CAST_TREASURE_SEQ			,");
		oSqlBuilder.AppendLine("	CT.CAST_TREASURE_NM				,");
		oSqlBuilder.AppendLine("	CT.TOTAL_CAST_POSSESSION_COUNT	,");
		oSqlBuilder.AppendLine("	SG.STAGE_GROUP_NM				,");
		oSqlBuilder.AppendLine("	CT.REVISION_NO					,");
		oSqlBuilder.AppendLine("	CT.UPDATE_DATE					");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_CAST_TREASURE		CT,");
		oSqlBuilder.AppendLine("	T_STAGE_GROUP		SG");

		string sWhereClause;
		string sPagingSql;
		string sSortExpression = "ORDER BY CT.SITE_CD, CT.CAST_TREASURE_SEQ DESC";

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd, pStageGroupType, out sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		}
		finally {
			conn.Close();
		}
		return oDs;
	}

	private OracleParameter[] CreateWhere(string pSiteCd, string pStageGroupType, out string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();

		SysPrograms.SqlAppendWhere("CT.SITE_CD = SG.SITE_CD AND CT.STAGE_GROUP_TYPE = SG.STAGE_GROUP_TYPE AND SG.SEX_CD = :SEX_CD", ref pWhere);
		list.Add(new OracleParameter("SEX_CD", ViCommConst.OPERATOR));

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("CT.SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!string.IsNullOrEmpty(pStageGroupType)) {
			SysPrograms.SqlAppendWhere("CT.STAGE_GROUP_TYPE = :STAGE_GROUP_TYPE", ref pWhere);
			list.Add(new OracleParameter("STAGE_GROUP_TYPE", pStageGroupType));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public DataSet GetList(string pSiteCd,string pStageGroupType) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	SITE_CD						,");
		oSqlBuilder.AppendLine("	STAGE_GROUP_TYPE			,");
		oSqlBuilder.AppendLine("	CAST_TREASURE_SEQ			,");
		oSqlBuilder.AppendLine("	CAST_TREASURE_NM			");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_CAST_TREASURE		");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD			AND");
		oSqlBuilder.AppendLine("	STAGE_GROUP_TYPE	= :STAGE_GROUP_TYPE	");
		oSqlBuilder.AppendLine("ORDER BY SITE_CD, CAST_TREASURE_SEQ	");


		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(new OracleParameter("SITE_CD",pSiteCd));
				cmd.Parameters.Add(new OracleParameter("STAGE_GROUP_TYPE", pStageGroupType));

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}
}