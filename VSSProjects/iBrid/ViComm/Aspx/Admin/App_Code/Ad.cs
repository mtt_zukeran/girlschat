/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 広告コード
--	Progaram ID		: Ad
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class Ad:DbSession {

	public Ad() {
	}

	public int GetPageCount(string pAdCd,string pAdNm,string pSiteCd,string pAdGroupCd,string pUsedSearch) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(VW_AD02.AD_CD) AS ROW_COUNT FROM VW_AD02 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pAdCd,pAdNm,pSiteCd,pAdGroupCd,pUsedSearch,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pAdCd,string pAdNm,string pSiteCd,string pAdGroupCd,string pUsedSearch,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY AD_CD ";
			string sSql = "SELECT " +
							"AD_CD							," +
							"AD_NM							," +
							"MAN_CONVERSION_FEE				," +
							"WOMAN_CONVERSION_FEE			," +
							"ADMIN_ID						," +
							"ADMIN_PASSWORD					," +
							"CAST_REGIST_REPORT_TIMING_NM	," +
							"MAN_REGIST_REPORT_TIMING_NM	," +
							"UPDATE_DATE					," +
							"USED_FLAG						" +
							"FROM(" +
							" SELECT VW_AD02.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_AD02";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pAdCd,pAdNm,pSiteCd,pAdGroupCd,pUsedSearch,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_AD02");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(string pAdCd,string pAdNm,string pSiteCd,string pAdGroupCd,string pUsedSearch,ref string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();

		if (!pAdCd.Equals("")) {
			SysPrograms.SqlAppendWhere("AD_CD LIKE :AD_CD ||'%'",ref pWhere);
			list.Add(new OracleParameter("AD_CD",pAdCd));
		}

		if (!pAdNm.Equals("")) {
			SysPrograms.SqlAppendWhere("AD_NM LIKE '%'||:AD_NM ||'%'",ref pWhere);
			list.Add(new OracleParameter("AD_NM",pAdNm));
		}

		if (!pAdGroupCd.Equals("")) {
			string sIsExist= "EXISTS (SELECT 1 FROM T_SITE_AD_GROUP WHERE SITE_CD =:SITE_CD AND AD_GROUP_CD =:AD_GROUP_CD AND VW_AD02.AD_CD = T_SITE_AD_GROUP.AD_CD)";
			SysPrograms.SqlAppendWhere(sIsExist,ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
			list.Add(new OracleParameter("AD_GROUP_CD",pAdGroupCd));
		}
		
		if (!pUsedSearch.Equals("")) {
			SysPrograms.SqlAppendWhere("USED_FLAG=:USED_FLAG",ref pWhere);
			list.Add(new OracleParameter("USED_FLAG",pUsedSearch));
		}
		

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}


	public DataSet GetList() {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT AD_CD,AD_NM FROM T_AD ";
			sSql = sSql + " ORDER BY AD_CD";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	public int GetPageCountBySiteGroup(string pSiteCd,string pAdCd,string pAdNm,bool pNoAssign) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();


			string sSql = "SELECT COUNT(*) AS ROW_COUNT " +
						"FROM " +
							"T_AD			," +
							"T_SITE_AD_GROUP," +
							"T_AD_GROUP		," +
							"T_SITE ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhereBySiteGroup(pSiteCd,pAdCd,pAdNm,pNoAssign,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollectionBySiteGroup(string pSiteCd,string pAdCd,string pAdNm,bool pNoAssign,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY T_AD.AD_CD ";


			string sSql = "SELECT * FROM(SELECT " +
							"T_AD.AD_CD						," +
							"T_AD.AD_NM						," +
							"T_SITE_AD_GROUP.UPDATE_DATE	," +
							"T_SITE_AD_GROUP.REVISION_NO	," +
							"T_AD_GROUP.SITE_CD				," +
							"T_AD_GROUP.AD_GROUP_CD			," +
							"T_AD_GROUP.AD_GROUP_NM			," +
							"T_AD_GROUP.WEB_FACE_SEQ		," +
							"T_SITE.SITE_NM					," +
							"ROW_NUMBER() OVER (" + sOrder + ")  AS RNUM " +
						"FROM " +
							"T_AD			," +
							"T_SITE_AD_GROUP," +
							"T_AD_GROUP		," +
							"T_SITE ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhereBySiteGroup(pSiteCd,pAdCd,pAdNm,pNoAssign,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + " ORDER BY AD_CD ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_AD");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhereBySiteGroup(string pSiteCd,string pAdCd,string pAdNm,bool pNoAssign,ref string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();

		SysPrograms.SqlAppendWhere("T_SITE_AD_GROUP.SITE_CD(+)	= :SITE_CD				    ",ref pWhere);
		SysPrograms.SqlAppendWhere("T_AD.AD_CD					= T_SITE_AD_GROUP.AD_CD (+) ",ref pWhere);
		SysPrograms.SqlAppendWhere("T_SITE_AD_GROUP.SITE_CD		= T_AD_GROUP.SITE_CD	(+)	",ref pWhere);
		SysPrograms.SqlAppendWhere("T_SITE_AD_GROUP.AD_GROUP_CD	= T_AD_GROUP.AD_GROUP_CD(+)	",ref pWhere);
		SysPrograms.SqlAppendWhere("T_AD_GROUP.SITE_CD			= T_SITE.SITE_CD		(+) ",ref pWhere);

		list.Add(new OracleParameter("SITE_CD",pSiteCd));

		if (!pAdCd.Equals("")) {
			SysPrograms.SqlAppendWhere("T_AD.AD_CD LIKE :AD_CD||'%' ",ref pWhere);
			list.Add(new OracleParameter("AD_CD",pAdCd));
		}

		if (!pAdNm.Equals("")) {
			SysPrograms.SqlAppendWhere("T_AD.AD_NM LIKE '%'||:AD_NM||'%' ",ref pWhere);
			list.Add(new OracleParameter("AD_NM",pAdNm));
		}

		if (pNoAssign) {
			SysPrograms.SqlAppendWhere("T_SITE_AD_GROUP.AD_GROUP_CD IS NULL",ref pWhere);
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public int GetPageCountByServicePoint(string pSiteCd,string pAdCd,string pAdNm,string pApplyDay,bool pNoAssign) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT " +
						"FROM " +
							"T_AD			," +
							"T_SITE_AD_POINT," +
							"T_SITE ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhereByServicePoint(pSiteCd,pAdCd,pAdNm,pApplyDay,pNoAssign,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollectionByServicePoint(string pSiteCd,string pAdCd,string pAdNm,string pApplyDay,bool pNoAssign,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY T_AD.AD_CD ";

			string sSql = "SELECT * FROM(SELECT " +
							"T_AD.AD_CD							," +
							"T_AD.AD_NM							," +
							"T_SITE_AD_POINT.UPDATE_DATE		," +
							"T_SITE_AD_POINT.REVISION_NO		," +
							"T_SITE_AD_POINT.SITE_CD			," +
							"T_SITE_AD_POINT.REGIST_SERVICE_POINT," +
							"T_SITE_AD_POINT.APPLY_START_DAY	," +
							"T_SITE_AD_POINT.APPLY_END_DAY		," +
							"T_SITE.SITE_NM						," +
							"ROW_NUMBER() OVER (" + sOrder + ")  AS RNUM " +
						"FROM " +
							"T_AD			," +
							"T_SITE_AD_POINT," +
							"T_SITE ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhereByServicePoint(pSiteCd,pAdCd,pAdNm,pApplyDay,pNoAssign,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + "ORDER BY AD_CD";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_AD");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhereByServicePoint(string pSiteCd,string pAdCd,string pAdNm,string pApplyDay,bool pNoAssign,ref string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();

		SysPrograms.SqlAppendWhere("T_SITE_AD_POINT.SITE_CD(+)	= :SITE_CD				    ",ref pWhere);
		SysPrograms.SqlAppendWhere("T_AD.AD_CD					= T_SITE_AD_POINT.AD_CD (+) ",ref pWhere);
		SysPrograms.SqlAppendWhere("T_SITE_AD_POINT.SITE_CD		= T_SITE.SITE_CD		(+) ",ref pWhere);
		list.Add(new OracleParameter("SITE_CD",pSiteCd));

		if (!pAdCd.Equals("")) {
			SysPrograms.SqlAppendWhere("T_AD.AD_CD LIKE :AD_CD ||'%'",ref pWhere);
			list.Add(new OracleParameter("AD_CD",pAdCd));
		}

		if (!pAdNm.Equals("")) {
			SysPrograms.SqlAppendWhere("T_AD.AD_NM LIKE '%'||:AD_NM ||'%'",ref pWhere);
			list.Add(new OracleParameter("AD_NM",pAdNm));
		}

		if (!pApplyDay.Equals("")) {
			SysPrograms.SqlAppendWhere("T_SITE_AD_POINT.APPLY_START_DAY <= :APPLY_START_DAY ",ref pWhere);
			list.Add(new OracleParameter("APPLY_START_DAY",pApplyDay));
			SysPrograms.SqlAppendWhere("T_SITE_AD_POINT.APPLY_END_DAY >= :APPLY_END_DAY ",ref pWhere);
			list.Add(new OracleParameter("APPLY_END_DAY",pApplyDay));
		}

		if (pNoAssign) {
			SysPrograms.SqlAppendWhere("T_SITE_AD_POINT.SITE_CD IS NULL",ref pWhere);
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public int GetPageCountBySiteGroupCd(string pSiteCd,string pAdGroupCd,string pAdNm,string pIssueDateFrom,string pIssueDateTo) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_AD01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhereBySiteGroupCd(pSiteCd,pAdGroupCd,pAdNm,pIssueDateFrom,pIssueDateTo,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollectionBySiteGroupCd(string pSiteCd,string pAdGroupCd,string pAdNm,string pIssueDateFrom,string pIssueDateTo,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY AD_CD ";
			string sSql = "SELECT " +
							"AD_CD			," +
							"AD_NM			," +
							"ISSUE_DATE		," +
							"UPDATE_DATE	," +
							"SITE_CD		," +
							"AD_GROUP_CD	 " +
							"FROM(" +
							" SELECT VW_AD01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_AD01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhereBySiteGroupCd(pSiteCd,pAdGroupCd,pAdNm,pIssueDateFrom,pIssueDateTo,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_AD01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetCsvDataBySiteGroupCd(string pSiteCd,string pAdGroupCd,string pAdNm,string pIssueDateFrom,string pIssueDateTo) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = " ORDER BY AD_CD ";
			string sSql = "SELECT " +
							"AD_CD			," +
							"AD_NM			," +
							"ISSUE_DATE		," +
							"UPDATE_DATE	 " +
						   "FROM " +
							"VW_AD01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhereBySiteGroupCd(pSiteCd,pAdGroupCd,pAdNm,pIssueDateFrom,pIssueDateTo,ref sWhere);
			sSql = sSql + sWhere;
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_AD01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
	private OracleParameter[] CreateWhereBySiteGroupCd(string pSiteCd,string pAdGroupCd,string pAdNm,string pIssueDateFrom,string pIssueDateTo,ref string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();

		if (!pSiteCd.Equals("")) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD ",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!pAdGroupCd.Equals("")) {
			SysPrograms.SqlAppendWhere("AD_GROUP_CD = :AD_GROUP_CD ",ref pWhere);
			list.Add(new OracleParameter("AD_GROUP_CD",pAdGroupCd));
		}

		if (!pAdNm.Equals("")) {
			SysPrograms.SqlAppendWhere("AD_NM = :AD_NM ",ref pWhere);
			list.Add(new OracleParameter("AD_NM",pAdNm));
		}

		if (!pIssueDateFrom.Equals("") && !pIssueDateTo.Equals("")) {
			DateTime dtFrom = DateTime.Parse(pIssueDateFrom + " " + "00:00:00");
			DateTime dtTo = DateTime.Parse(pIssueDateTo + " " + "23:59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere("ISSUE_DATE >= :ISSUE_DATE_FROM AND ISSUE_DATE < :ISSUE_DATE_TO",ref pWhere);
			list.Add(new OracleParameter("ISSUE_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("ISSUE_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public bool IsExist(string pAdCd,ref string pAdNm) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		pAdNm = "";
		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand("SELECT AD_NM FROM T_AD WHERE AD_CD =:AD_CD",conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("AD_CD",pAdCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_AD");
					if (ds.Tables["T_AD"].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						pAdNm = dr["AD_NM"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}

		return bExist;
	}
}
