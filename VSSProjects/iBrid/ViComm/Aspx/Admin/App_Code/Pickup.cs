/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ピックアップ

--	Progaram ID		: Pickup
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;
using ViComm;

public class Pickup : DbSession {

	public Pickup() {
	}
	public DataSet GetListLite(string pSiteCd,string pPickupType) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			List<OracleParameter> oWhereParamList = new List<OracleParameter>();
			string sWhere = string.Empty;

			// WHERE句生成
			oWhereParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
			SysPrograms.SqlAppendWhere(" SITE_CD	= :SITE_CD ",ref sWhere);
			if (!string.IsNullOrEmpty(pPickupType)) {
				oWhereParamList.Add(new OracleParameter(":PICKUP_TYPE",pPickupType));
				SysPrograms.SqlAppendWhere(" PICKUP_TYPE	= :PICKUP_TYPE ",ref sWhere);
			}

			// SQL文生成



			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT									").AppendLine();
			sSql.Append("	SITE_CD				,				").AppendLine();
			sSql.Append("	PICKUP_ID			,				").AppendLine();
			sSql.Append("	PICKUP_TITLE		,				").AppendLine();
			sSql.Append("	UPDATE_DATE			,				").AppendLine();
			sSql.Append("	PICKUP_TYPE			,				").AppendLine();
			sSql.Append("	PICKUP_FLAG			,				").AppendLine();
			sSql.Append("	USE_TOP_PAGE_FLAG					").AppendLine();
			sSql.Append("FROM									").AppendLine();
			sSql.Append("	T_PICKUP							").AppendLine();
			sSql.Append(sWhere);
			sSql.Append("ORDER BY								").AppendLine();
			sSql.Append("	PICKUP_ID							").AppendLine();

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParamList.ToArray());
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetList(string pSiteCd) {
		return GetList(pSiteCd,null);
	}
	public DataSet GetList(string pSiteCd,string pPickupType) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			List<OracleParameter> oWhereParamList = new List<OracleParameter>();
			string sWhere = string.Empty;

			// WHERE句生成
			oWhereParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
			SysPrograms.SqlAppendWhere(" SITE_CD	= :SITE_CD ",ref sWhere);
			if (!string.IsNullOrEmpty(pPickupType)) {
				oWhereParamList.Add(new OracleParameter(":PICKUP_TYPE",pPickupType));
				SysPrograms.SqlAppendWhere(" PICKUP_TYPE	= :PICKUP_TYPE ",ref sWhere);
			}

			// SQL文生成
			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT									").AppendLine();
			sSql.Append("	SITE_CD				,				").AppendLine();
			sSql.Append("	PICKUP_ID			,				").AppendLine();
			sSql.Append("	PICKUP_TITLE		,				").AppendLine();
			sSql.Append("	UPDATE_DATE			,				").AppendLine();
			sSql.Append("	REGIST_COUNT		,				").AppendLine();
			sSql.Append("	REGIST_PIC_COUNT	,				").AppendLine();
			sSql.Append("	REGIST_MOVIE_COUNT	,				").AppendLine();
			sSql.Append("	PICKUP_COUNT		,				").AppendLine();
			sSql.Append("	PICKUP_PIC_COUNT	,				").AppendLine();
			sSql.Append("	PICKUP_MOVIE_COUNT	,				").AppendLine();
			sSql.Append("	PICKUP_TYPE			,				").AppendLine();
			sSql.Append("	PICKUP_TYPE_NM		,				").AppendLine();
			sSql.Append("	PICKUP_FLAG			,				").AppendLine();
			sSql.Append("	USE_TOP_PAGE_FLAG	,				").AppendLine();
			sSql.Append("	CAST_PUBLISH_FLAG					").AppendLine();
			sSql.Append("FROM									").AppendLine();
			sSql.Append("	VW_PICKUP01							").AppendLine();
			sSql.Append(sWhere);
			sSql.Append("ORDER BY								").AppendLine();
			sSql.Append("	PICKUP_ID							").AppendLine();

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParamList.ToArray());
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public bool IsExistOtherUseTopPageFlag(string pSiteCd,string pPickupId) {
		bool bExist = false;
		;
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT												").AppendLine();
			sSql.Append("	PICKUP_ID										").AppendLine();
			sSql.Append("FROM												").AppendLine();
			sSql.Append("	T_PICKUP										").AppendLine();
			sSql.Append("WHERE												").AppendLine();
			sSql.Append("	SITE_CD				= :SITE_CD				AND	").AppendLine();
			sSql.Append("	USE_TOP_PAGE_FLAG	= :USE_TOP_PAGE_FLAG	AND	").AppendLine();
			sSql.Append("	PICKUP_ID			!= :PICKUP_ID				").AppendLine();

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USE_TOP_PAGE_FLAG",ViCommConst.FLAG_ON);
				cmd.Parameters.Add("PICKUP_ID",pPickupId);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_PICKUP");
					if (ds.Tables["T_PICKUP"].Rows.Count != 0) {
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;

	}

	public string GetPickupType(string pSiteCd,string pPickupId) {
		string sType = string.Empty;
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT									").AppendLine();
			sSql.Append("	PICKUP_TYPE							").AppendLine();
			sSql.Append("FROM									").AppendLine();
			sSql.Append("	T_PICKUP							").AppendLine();
			sSql.Append("WHERE									").AppendLine();
			sSql.Append("	SITE_CD		= :SITE_CD			AND	").AppendLine();
			sSql.Append("	PICKUP_ID	= :PICKUP_ID			").AppendLine();

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("PICKUP_ID",pPickupId);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_PICKUP");
					if (ds.Tables["T_PICKUP"].Rows.Count != 0) {
						DataRow dr = ds.Tables["T_PICKUP"].Rows[0];
						sType = dr["PICKUP_TYPE"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
		return sType;

	}
}
