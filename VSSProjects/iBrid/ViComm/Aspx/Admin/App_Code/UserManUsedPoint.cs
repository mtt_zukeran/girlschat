﻿/*************************************************************************
--	System			: ViComm System
--	Sub System Name	: Admin
--	Title			: 変更履歴
--	Progaram ID		: UserManUsedPoint
--
--  Creation Date	: 2010.05.14
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class UserManUsedPoint:DbSession {

	public UserManUsedPoint() {
	}

	public DataSet GetPageCollection(string pSiteCd,string pFromYYYY,string pFromMM,string pFromDD,string pFromHH,string pToYYYY,string pToMM,string pToDD,string pToHH) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sDateFrom = string.Format("{0}/{1}/{2} {3}:00:00",pFromYYYY,pFromMM,pFromDD,pFromHH);
			string sDateTo = string.Format("{0}/{1}/{2}",pToYYYY,pToMM,pToDD);
			if (pToHH.Equals("24")) {
				sDateTo = DateTime.Parse(sDateTo).AddDays(1).ToString("yyyy/MM/dd HH:mm:ss");
			} else {
				sDateTo = string.Format("{0}/{1}/{2} {3}:00:00",pToYYYY,pToMM,pToDD,pToHH);
			}
			DateTime dtFrom = System.DateTime.ParseExact(sDateFrom,"yyyy/MM/dd HH:mm:ss",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);
			DateTime dtTo = System.DateTime.ParseExact(sDateTo,"yyyy/MM/dd HH:mm:ss",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);

			StringBuilder sqlBuilder = new StringBuilder();
			sqlBuilder.Append(" SELECT																						").AppendLine();
			sqlBuilder.Append(" 	T_USER_MAN_CHARACTER.SITE_CD				,											").AppendLine();
			sqlBuilder.Append(" 	T_USER_MAN_CHARACTER.USER_SEQ				,                                           ").AppendLine();
			sqlBuilder.Append(" 	T_USER.LOGIN_ID								,											").AppendLine();
			sqlBuilder.Append(" 	T_USER_MAN_CHARACTER.USER_RANK				,											").AppendLine();
			sqlBuilder.Append(" 	CASE WHEN T_USER_MAN_CHARACTER.USER_RANK = 'A' THEN '振無' ELSE '振有' END AS USER_RANK_NM,").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_USED_LOG.TALK_POINT,0)						AS TALK_POINT					,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_USED_LOG.PUB_TV_TALK_POINT,0)				AS PUB_TV_TALK_POINT			,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_USED_LOG.PRV_TV_TALK_POINT,0)				AS PRV_TV_TALK_POINT			,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_USED_LOG.PRV_VOICE_TALK_POINT,0)			AS PRV_VOICE_TALK_POINT			,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_USED_LOG.VIEW_LIVE_POINT,0)					AS VIEW_LIVE_POINT				,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_USED_LOG.VIEW_TALK_POINT,0)					AS VIEW_TALK_POINT				,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_USED_LOG.VIEW_ONLINE_POINT,0)				AS VIEW_ONLINE_POINT			,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_USED_LOG.PLAY_MOVIE_POINT,0)				AS PLAY_MOVIE_POINT				,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_USED_LOG.PLAY_ROOM_SHOW_POINT,0)			AS PLAY_ROOM_SHOW_POINT			,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_USED_LOG.GPF_TALK_VOICE_POINT,0)			AS GPF_TALK_VOICE_POINT			,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_USED_LOG.PLAY_PF_VOICE_POINT,0)				AS PLAY_PF_VOICE_POINT			,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_USED_LOG.REC_PF_VOICE_POINT,0)				AS REC_PF_VOICE_POINT			,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_USED_LOG.PRV_MESSAGE_RX_POINT,0)			AS PRV_MESSAGE_RX_POINT			,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_USED_LOG.PRV_PROFILE_RX_POINT,0)			AS PRV_PROFILE_RX_POINT			,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_USED_LOG.WIRETAPPING_POINT,0)				AS WIRETAPPING_POINT			,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_USED_LOG.PLAY_PV_MSG_POINT,0)				AS PLAY_PV_MSG_POINT			,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_USED_LOG.PUB_VOICE_TALK_POINT,0)			AS PUB_VOICE_TALK_POINT			,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_USED_LOG.CAST_PUB_TV_TALK_POINT,0)			AS CAST_PUB_TV_TALK_POINT		,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_USED_LOG.CAST_PRV_TV_TALK_POINT,0)			AS CAST_PRV_TV_TALK_POINT		,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_USED_LOG.CAST_PRV_VOICE_TALK_POINT,0)		AS CAST_PRV_VOICE_TALK_POINT	,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_USED_LOG.CAST_PUB_VOICE_TALK_POINT,0)		AS CAST_PUB_VOICE_TALK_POINT	,	").AppendLine();
			
			sqlBuilder.Append(" 	NVL(TMP_WEB_USED_LOG.WEB_TOTAL_POINT,0)				AS WEB_TOTAL_POINT				,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_WEB_USED_LOG.RX_MAIL_POINT,0)				AS RX_MAIL_POINT				,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_WEB_USED_LOG.MAIL_WITH_PIC_POINT,0)			AS MAIL_WITH_PIC_POINT			,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_WEB_USED_LOG.MAIL_WITH_MOVIE_POINT,0)		AS MAIL_WITH_MOVIE_POINT		,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_WEB_USED_LOG.TX_MAIL_POINT,0)				AS TX_MAIL_POINT				,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_WEB_USED_LOG.TX_MAIL_WITH_PIC_POINT,0)		AS TX_MAIL_WITH_PIC_POINT		,   ").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_WEB_USED_LOG.TX_MAIL_WITH_MOVIE_POINT,0)	AS TX_MAIL_WITH_MOVIE_POINT		,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_WEB_USED_LOG.BBS_MOVIE_POINT,0)				AS BBS_MOVIE_POINT				,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_WEB_USED_LOG.BBS_PIC_POINT,0)				AS BBS_PIC_POINT				,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_WEB_USED_LOG.DOWNLOAD_PF_MOVIE_POINT,0)		AS DOWNLOAD_PF_MOVIE_POINT		,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_WEB_USED_LOG.RX_MAIL_WITH_PIC_POINT,0)		AS RX_MAIL_WITH_PIC_POINT		,   ").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_WEB_USED_LOG.RX_MAIL_WITH_MOVIE_POINT,0)	AS RX_MAIL_WITH_MOVIE_POINT		,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_WEB_USED_LOG.REC_PF_MOVIE_POINT,0)			AS REC_PF_MOVIE_POINT			,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_WEB_USED_LOG.WRITE_BBS_POINT,0)				AS WRITE_BBS_POINT				,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_WEB_USED_LOG.DOWNLOAD_MOVIE_POINT,0)		AS DOWNLOAD_MOVIE_POINT			,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_WEB_USED_LOG.DOWNLOAD_VOICE_POINT,0)		AS DOWNLOAD_VOICE_POINT			,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_WEB_USED_LOG.SOCIAL_GAME_POINT,0)			AS SOCIAL_GAME_POINT			,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_WEB_USED_LOG.YAKYUKEN_POINT,0)				AS YAKYUKEN_POINT				,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_WEB_USED_LOG.PRESENT_MAIL_POINT,0)			AS PRESENT_MAIL_POINT			,	").AppendLine();
			sqlBuilder.Append(" 	NVL(TMP_WEB_USED_LOG.TIME_CALL_POINT,0)				AS TIME_CALL_POINT					").AppendLine();
			sqlBuilder.Append(" FROM																					    ").AppendLine();
			sqlBuilder.Append(" 	T_USER_MAN_CHARACTER,																	").AppendLine();
			sqlBuilder.Append(" 	T_USER,																					").AppendLine();
			sqlBuilder.Append(" 	(																						").AppendLine();
			sqlBuilder.Append(" 		SELECT																				").AppendLine();
			sqlBuilder.Append(" 			T_IVP_REQUEST.MAN_USER_SITE_CD	,												").AppendLine();
			sqlBuilder.Append(" 			T_IVP_REQUEST.MAN_USER_SEQ		,												").AppendLine();
			sqlBuilder.Append(" 			SUM(T_USED_LOG.CHARGE_POINT)												AS TALK_POINT					,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_USED_LOG.CHARGE_TYPE,'01',T_USED_LOG.CHARGE_POINT,0))			AS PUB_TV_TALK_POINT			,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_USED_LOG.CHARGE_TYPE,'02',T_USED_LOG.CHARGE_POINT,0))			AS PRV_TV_TALK_POINT			,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_USED_LOG.CHARGE_TYPE,'03',T_USED_LOG.CHARGE_POINT,0))			AS PRV_VOICE_TALK_POINT			,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_USED_LOG.CHARGE_TYPE,'04',T_USED_LOG.CHARGE_POINT,0))			AS VIEW_LIVE_POINT				,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_USED_LOG.CHARGE_TYPE,'05',T_USED_LOG.CHARGE_POINT,0))			AS VIEW_TALK_POINT				,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_USED_LOG.CHARGE_TYPE,'06',T_USED_LOG.CHARGE_POINT,0))			AS VIEW_ONLINE_POINT			,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_USED_LOG.CHARGE_TYPE,'07',T_USED_LOG.CHARGE_POINT,0))			AS PLAY_MOVIE_POINT				,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_USED_LOG.CHARGE_TYPE,'21',T_USED_LOG.CHARGE_POINT,0))			AS PLAY_ROOM_SHOW_POINT			,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_USED_LOG.CHARGE_TYPE,'23',T_USED_LOG.CHARGE_POINT,0))			AS GPF_TALK_VOICE_POINT			,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_USED_LOG.CHARGE_TYPE,'24',T_USED_LOG.CHARGE_POINT,0))			AS PLAY_PF_VOICE_POINT			,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_USED_LOG.CHARGE_TYPE,'25',T_USED_LOG.CHARGE_POINT,0))			AS REC_PF_VOICE_POINT			,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_USED_LOG.CHARGE_TYPE,'26',T_USED_LOG.CHARGE_POINT,0))			AS PRV_MESSAGE_RX_POINT			,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_USED_LOG.CHARGE_TYPE,'27',T_USED_LOG.CHARGE_POINT,0))			AS PRV_PROFILE_RX_POINT			,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_USED_LOG.CHARGE_TYPE,'28',T_USED_LOG.CHARGE_POINT,0))			AS WIRETAPPING_POINT			,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_USED_LOG.CHARGE_TYPE,'29',T_USED_LOG.CHARGE_POINT,0))			AS PLAY_PV_MSG_POINT			,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_USED_LOG.CHARGE_TYPE,'31',T_USED_LOG.CHARGE_POINT,0))			AS PUB_VOICE_TALK_POINT			,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_USED_LOG.CHARGE_TYPE,'51',T_USED_LOG.CHARGE_POINT,0))			AS CAST_PUB_TV_TALK_POINT		,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_USED_LOG.CHARGE_TYPE,'52',T_USED_LOG.CHARGE_POINT,0))			AS CAST_PRV_TV_TALK_POINT		,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_USED_LOG.CHARGE_TYPE,'53',T_USED_LOG.CHARGE_POINT,0))			AS CAST_PRV_VOICE_TALK_POINT	,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_USED_LOG.CHARGE_TYPE,'54',T_USED_LOG.CHARGE_POINT,0))			AS CAST_PUB_VOICE_TALK_POINT	").AppendLine();
			sqlBuilder.Append(" 		FROM																				").AppendLine();
			sqlBuilder.Append(" 			T_IVP_REQUEST	,																").AppendLine();
			sqlBuilder.Append(" 			T_USED_LOG																		").AppendLine();
			sqlBuilder.Append(" 		WHERE																				").AppendLine();
			sqlBuilder.Append(" 			-- T_IVP_REQUEST × T_USED_LOG													").AppendLine();
			sqlBuilder.Append(" 			T_IVP_REQUEST.IVP_REQUEST_SEQ	=	T_USED_LOG.IVP_REQUEST_SEQ			AND		").AppendLine();
			sqlBuilder.Append(" 			T_IVP_REQUEST.MAN_USER_SITE_CD		=	:SITE_CD						AND		").AppendLine();
			sqlBuilder.Append(" 			T_USED_LOG.CHARGE_START_DATE		>=	:CHARGE_START_DATE_FROM			AND		").AppendLine();
			sqlBuilder.Append(" 			T_USED_LOG.CHARGE_START_DATE		<	:CHARGE_START_DATE_TO					").AppendLine();
			sqlBuilder.Append(" 		GROUP BY																			").AppendLine();
			sqlBuilder.Append(" 			T_IVP_REQUEST.MAN_USER_SITE_CD		,											").AppendLine();
			sqlBuilder.Append(" 			T_IVP_REQUEST.MAN_USER_SEQ														").AppendLine();
			sqlBuilder.Append(" 	) TMP_USED_LOG,																			").AppendLine();
			sqlBuilder.Append(" 	(																						").AppendLine();
			sqlBuilder.Append(" 		SELECT																				").AppendLine();
			sqlBuilder.Append(" 			T_WEB_USED_LOG.MAN_USER_SITE_CD		,                                           ").AppendLine();
			sqlBuilder.Append(" 			T_WEB_USED_LOG.MAN_USER_SEQ			,											").AppendLine();
			sqlBuilder.Append(" 			SUM(T_WEB_USED_LOG.CHARGE_POINT)											AS WEB_TOTAL_POINT				,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_WEB_USED_LOG.CHARGE_TYPE,'08',T_WEB_USED_LOG.CHARGE_POINT,0))	AS RX_MAIL_POINT				,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_WEB_USED_LOG.CHARGE_TYPE,'09',T_WEB_USED_LOG.CHARGE_POINT,0))	AS MAIL_WITH_PIC_POINT			,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_WEB_USED_LOG.CHARGE_TYPE,'10',T_WEB_USED_LOG.CHARGE_POINT,0))	AS MAIL_WITH_MOVIE_POINT		,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_WEB_USED_LOG.CHARGE_TYPE,'11',T_WEB_USED_LOG.CHARGE_POINT,0))	AS TX_MAIL_POINT				,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_WEB_USED_LOG.CHARGE_TYPE,'12',T_WEB_USED_LOG.CHARGE_POINT,0))	AS TX_MAIL_WITH_PIC_POINT		,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_WEB_USED_LOG.CHARGE_TYPE,'13',T_WEB_USED_LOG.CHARGE_POINT,0))	AS TX_MAIL_WITH_MOVIE_POINT		,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_WEB_USED_LOG.CHARGE_TYPE,'14',T_WEB_USED_LOG.CHARGE_POINT,0))	AS BBS_MOVIE_POINT				,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_WEB_USED_LOG.CHARGE_TYPE,'15',T_WEB_USED_LOG.CHARGE_POINT,0))	AS BBS_PIC_POINT				,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_WEB_USED_LOG.CHARGE_TYPE,'16',T_WEB_USED_LOG.CHARGE_POINT,0))	AS DOWNLOAD_PF_MOVIE_POINT		,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_WEB_USED_LOG.CHARGE_TYPE,'17',T_WEB_USED_LOG.CHARGE_POINT,0))	AS RX_MAIL_WITH_PIC_POINT		,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_WEB_USED_LOG.CHARGE_TYPE,'18',T_WEB_USED_LOG.CHARGE_POINT,0))	AS RX_MAIL_WITH_MOVIE_POINT		,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_WEB_USED_LOG.CHARGE_TYPE,'22',T_WEB_USED_LOG.CHARGE_POINT,0))	AS REC_PF_MOVIE_POINT			,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_WEB_USED_LOG.CHARGE_TYPE,'30',T_WEB_USED_LOG.CHARGE_POINT,0))	AS WRITE_BBS_POINT				,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_WEB_USED_LOG.CHARGE_TYPE,'32',T_WEB_USED_LOG.CHARGE_POINT,0))	AS DOWNLOAD_MOVIE_POINT			,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_WEB_USED_LOG.CHARGE_TYPE,'33',T_WEB_USED_LOG.CHARGE_POINT,0))	AS TIME_CALL_POINT				,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_WEB_USED_LOG.CHARGE_TYPE,'34',T_WEB_USED_LOG.CHARGE_POINT,0))	AS DOWNLOAD_VOICE_POINT			,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_WEB_USED_LOG.CHARGE_TYPE,'35',T_WEB_USED_LOG.CHARGE_POINT,0))	AS SOCIAL_GAME_POINT			,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_WEB_USED_LOG.CHARGE_TYPE,'36',T_WEB_USED_LOG.CHARGE_POINT,0))	AS YAKYUKEN_POINT				,").AppendLine();
			sqlBuilder.Append(" 			SUM(DECODE(T_WEB_USED_LOG.CHARGE_TYPE,'37',T_WEB_USED_LOG.CHARGE_POINT,0))	AS PRESENT_MAIL_POINT			").AppendLine();
			sqlBuilder.Append(" 		FROM																											").AppendLine();
			sqlBuilder.Append(" 			T_WEB_USED_LOG																								").AppendLine();
			sqlBuilder.Append(" 		WHERE																											").AppendLine();
			sqlBuilder.Append(" 			T_WEB_USED_LOG.MAN_USER_SITE_CD			=	:SITE_CD					AND	").AppendLine();
			sqlBuilder.Append(" 			T_WEB_USED_LOG.CHARGE_START_DATE		>=	:CHARGE_START_DATE_FROM		AND	").AppendLine();
			sqlBuilder.Append(" 			T_WEB_USED_LOG.CHARGE_START_DATE		<	:CHARGE_START_DATE_TO			").AppendLine();
			sqlBuilder.Append(" 		GROUP BY																		").AppendLine();
			sqlBuilder.Append(" 			T_WEB_USED_LOG.MAN_USER_SITE_CD		,										").AppendLine();
			sqlBuilder.Append(" 			T_WEB_USED_LOG.MAN_USER_SEQ													").AppendLine();
			sqlBuilder.Append(" 	) TMP_WEB_USED_LOG																	").AppendLine();
			sqlBuilder.Append(" WHERE																					").AppendLine();
			sqlBuilder.Append(" 	-- T_USER_MAN_CHARACTER × USER_SEQ													").AppendLine();
			sqlBuilder.Append(" 	T_USER_MAN_CHARACTER.USER_SEQ		=	T_USER.USER_SEQ							AND	").AppendLine();
			sqlBuilder.Append(" 	-- T_USER_MAN_CHARACTER × TMP_USED_LOG												").AppendLine();
			sqlBuilder.Append(" 	T_USER_MAN_CHARACTER.SITE_CD		=	TMP_USED_LOG.MAN_USER_SITE_CD		(+)	AND	").AppendLine();
			sqlBuilder.Append(" 	T_USER_MAN_CHARACTER.USER_SEQ		=	TMP_USED_LOG.MAN_USER_SEQ			(+)	AND	").AppendLine();
			sqlBuilder.Append(" 	-- T_USER_MAN_CHARACTER × TMP_WEB_USED_LOG											").AppendLine();
			sqlBuilder.Append(" 	T_USER_MAN_CHARACTER.SITE_CD		=	TMP_WEB_USED_LOG.MAN_USER_SITE_CD	(+)	AND	").AppendLine();
			sqlBuilder.Append(" 	T_USER_MAN_CHARACTER.USER_SEQ		=	TMP_WEB_USED_LOG.MAN_USER_SEQ		(+)	AND	").AppendLine();
			sqlBuilder.Append(" 	T_USER_MAN_CHARACTER.SITE_CD 		=	:SITE_CD								AND	").AppendLine();
			sqlBuilder.Append(" 	( TMP_USED_LOG.TALK_POINT	> 0 OR TMP_WEB_USED_LOG.WEB_TOTAL_POINT > 0)			").AppendLine();
			sqlBuilder.Append(" ORDER BY																				").AppendLine();
			sqlBuilder.Append(" 	LOGIN_ID																			").AppendLine();

			string sSql = sqlBuilder.ToString();

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add(new OracleParameter("CHARGE_START_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
				cmd.Parameters.Add(new OracleParameter("CHARGE_START_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MAN_USED_POINT");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public int GetTotal(string pSiteCd,string pFromYYYY,string pFromMM,string pFromDD,string pFromHH,string pToYYYY,string pToMM,string pToDD,string pToHH) {
		DataSet ds;

		string sDateFrom = string.Format("{0}/{1}/{2} {3}:00:00",pFromYYYY,pFromMM,pFromDD,pFromHH);
		string sDateTo = string.Format("{0}/{1}/{2}",pToYYYY,pToMM,pToDD);
		if (pToHH.Equals("24")) {
			sDateTo = DateTime.Parse(sDateTo).AddDays(1).ToString("yyyy/MM/dd HH:mm:ss");
		} else {
			sDateTo = string.Format("{0}/{1}/{2} {3}:00:00",pToYYYY,pToMM,pToDD,pToHH);
		}
		DateTime dtFrom = System.DateTime.ParseExact(sDateFrom,"yyyy/MM/dd HH:mm:ss",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);
		DateTime dtTo = System.DateTime.ParseExact(sDateTo,"yyyy/MM/dd HH:mm:ss",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);

		StringBuilder sqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		sqlBuilder.AppendLine(" SELECT																					");
		sqlBuilder.AppendLine("		COUNT(*)																			");
		sqlBuilder.AppendLine(" FROM																					");
		sqlBuilder.AppendLine(" 	T_USER_MAN_CHARACTER,																");
		sqlBuilder.AppendLine(" 	T_USER,																				");
		sqlBuilder.AppendLine(" 	(																					");
		sqlBuilder.AppendLine(" 		SELECT																			");
		sqlBuilder.AppendLine(" 			T_IVP_REQUEST.MAN_USER_SITE_CD	,											");
		sqlBuilder.AppendLine(" 			T_IVP_REQUEST.MAN_USER_SEQ		,											");
		sqlBuilder.AppendLine(" 			SUM(T_USED_LOG.CHARGE_POINT) AS TALK_POINT									");
		sqlBuilder.AppendLine(" 		FROM																			");
		sqlBuilder.AppendLine(" 			T_IVP_REQUEST	,															");
		sqlBuilder.AppendLine(" 			T_USED_LOG																	");
		sqlBuilder.AppendLine(" 		WHERE																			");
		sqlBuilder.AppendLine(" 			-- T_IVP_REQUEST × T_USED_LOG												");
		sqlBuilder.AppendLine(" 			T_IVP_REQUEST.IVP_REQUEST_SEQ	=	T_USED_LOG.IVP_REQUEST_SEQ			AND	");
		sqlBuilder.AppendLine(" 			T_IVP_REQUEST.MAN_USER_SITE_CD		=	:SITE_CD						AND	");
		sqlBuilder.AppendLine(" 			T_USED_LOG.CHARGE_START_DATE		>=	:CHARGE_START_DATE_FROM			AND	");
		sqlBuilder.AppendLine(" 			T_USED_LOG.CHARGE_START_DATE		<	:CHARGE_START_DATE_TO				");
		sqlBuilder.AppendLine(" 		GROUP BY																		");
		sqlBuilder.AppendLine(" 			T_IVP_REQUEST.MAN_USER_SITE_CD		,										");
		sqlBuilder.AppendLine(" 			T_IVP_REQUEST.MAN_USER_SEQ													");
		sqlBuilder.AppendLine(" 	) TMP_USED_LOG,																		");
		sqlBuilder.AppendLine(" 	(																					");
		sqlBuilder.AppendLine(" 		SELECT																			");
		sqlBuilder.AppendLine(" 			T_WEB_USED_LOG.MAN_USER_SITE_CD		,                                       ");
		sqlBuilder.AppendLine(" 			T_WEB_USED_LOG.MAN_USER_SEQ			,										");
		sqlBuilder.AppendLine(" 			SUM(T_WEB_USED_LOG.CHARGE_POINT) AS WEB_TOTAL_POINT							");
		sqlBuilder.AppendLine(" 		FROM																			");
		sqlBuilder.AppendLine(" 			T_WEB_USED_LOG																");
		sqlBuilder.AppendLine(" 		WHERE																			");
		sqlBuilder.AppendLine(" 			T_WEB_USED_LOG.MAN_USER_SITE_CD			=	:SITE_CD					AND	");
		sqlBuilder.AppendLine(" 			T_WEB_USED_LOG.CHARGE_START_DATE		>=	:CHARGE_START_DATE_FROM		AND	");
		sqlBuilder.AppendLine(" 			T_WEB_USED_LOG.CHARGE_START_DATE		<	:CHARGE_START_DATE_TO			");
		sqlBuilder.AppendLine(" 		GROUP BY																		");
		sqlBuilder.AppendLine(" 			T_WEB_USED_LOG.MAN_USER_SITE_CD		,										");
		sqlBuilder.AppendLine(" 			T_WEB_USED_LOG.MAN_USER_SEQ													");
		sqlBuilder.AppendLine(" 	) TMP_WEB_USED_LOG																	");
		sqlBuilder.AppendLine(" WHERE																					");
		sqlBuilder.AppendLine(" 	-- T_USER_MAN_CHARACTER × USER_SEQ													");
		sqlBuilder.AppendLine(" 	T_USER_MAN_CHARACTER.USER_SEQ		=	T_USER.USER_SEQ							AND	");
		sqlBuilder.AppendLine(" 	-- T_USER_MAN_CHARACTER × TMP_USED_LOG												");
		sqlBuilder.AppendLine(" 	T_USER_MAN_CHARACTER.SITE_CD		=	TMP_USED_LOG.MAN_USER_SITE_CD		(+)	AND	");
		sqlBuilder.AppendLine(" 	T_USER_MAN_CHARACTER.USER_SEQ		=	TMP_USED_LOG.MAN_USER_SEQ			(+)	AND	");
		sqlBuilder.AppendLine(" 	-- T_USER_MAN_CHARACTER × TMP_WEB_USED_LOG											");
		sqlBuilder.AppendLine(" 	T_USER_MAN_CHARACTER.SITE_CD		=	TMP_WEB_USED_LOG.MAN_USER_SITE_CD	(+)	AND	");
		sqlBuilder.AppendLine(" 	T_USER_MAN_CHARACTER.USER_SEQ		=	TMP_WEB_USED_LOG.MAN_USER_SEQ		(+)	AND	");
		sqlBuilder.AppendLine(" 	T_USER_MAN_CHARACTER.SITE_CD 		=	:SITE_CD								AND	");
		sqlBuilder.AppendLine(" 	( TMP_USED_LOG.TALK_POINT	> 0 OR TMP_WEB_USED_LOG.WEB_TOTAL_POINT > 0)			");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":CHARGE_START_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
		oParamList.Add(new OracleParameter(":CHARGE_START_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));

		int iCount = ExecuteSelectCountQueryBase(sqlBuilder.ToString(),oParamList.ToArray());

		return iCount;
	}

	public DataSet GetDailyTotalPoint(string pSiteCd,string pFromYYYY,string pFromMM,string pFromDD,string pFromHH,string pToYYYY,string pToMM,string pToDD,string pToHH) {
		DataSet oDataSet;

		string sDateFrom = string.Format("{0}/{1}/{2} {3}:00:00",pFromYYYY,pFromMM,pFromDD,pFromHH);
		string sDateTo = string.Format("{0}/{1}/{2}",pToYYYY,pToMM,pToDD);
		if (pToHH.Equals("24")) {
			sDateTo = DateTime.Parse(sDateTo).AddDays(1).ToString("yyyy/MM/dd HH:mm:ss");
		} else {
			sDateTo = string.Format("{0}/{1}/{2} {3}:00:00",pToYYYY,pToMM,pToDD,pToHH);
		}
		DateTime dtFrom = System.DateTime.ParseExact(sDateFrom,"yyyy/MM/dd HH:mm:ss",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);
		DateTime dtTo = System.DateTime.ParseExact(sDateTo,"yyyy/MM/dd HH:mm:ss",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT																						");
		oSqlBuilder.AppendLine("	T_USER.LOGIN_ID																		,	");
		oSqlBuilder.AppendLine("	C.*																						");
		oSqlBuilder.AppendLine("FROM																						");
		oSqlBuilder.AppendLine("	(																						");
		oSqlBuilder.AppendLine("		SELECT																				");
		oSqlBuilder.AppendLine("			MAN_USER_SITE_CD															,	");
		oSqlBuilder.AppendLine("			MAN_USER_SEQ																,	");
		oSqlBuilder.AppendLine("			CHARGE_DAY																	,	");
		oSqlBuilder.AppendLine("			SUM(CHARGE_POINT) AS CHARGE_POINT												");
		oSqlBuilder.AppendLine("		FROM																				");
		oSqlBuilder.AppendLine("			(																				");
		oSqlBuilder.AppendLine("				SELECT																		");
		oSqlBuilder.AppendLine("					T_IVP_REQUEST.MAN_USER_SITE_CD										,	");
		oSqlBuilder.AppendLine("					T_IVP_REQUEST.MAN_USER_SEQ											,	");
		oSqlBuilder.AppendLine("					UL.CHARGE_DAY														,	");
		oSqlBuilder.AppendLine("					SUM(UL.CHARGE_POINT) CHARGE_POINT										");
		oSqlBuilder.AppendLine("				FROM																		");
		oSqlBuilder.AppendLine("					T_IVP_REQUEST														,	");
		oSqlBuilder.AppendLine("					(																		");
		oSqlBuilder.AppendLine("						SELECT																");
		oSqlBuilder.AppendLine("							IVP_REQUEST_SEQ												,	");
		oSqlBuilder.AppendLine("							CHARGE_POINT												,	");
		oSqlBuilder.AppendLine("							TO_CHAR(CHARGE_START_DATE,'YYYY/MM/DD') AS CHARGE_DAY			");
		oSqlBuilder.AppendLine("						FROM																");
		oSqlBuilder.AppendLine("							T_USED_LOG														");
		oSqlBuilder.AppendLine("						WHERE																");
		oSqlBuilder.AppendLine("							CHARGE_START_DATE	>= :CHARGE_START_DATE_FROM				AND	");
		oSqlBuilder.AppendLine("							CHARGE_START_DATE	< :CHARGE_START_DATE_TO					AND	");
		oSqlBuilder.AppendLine("							CHARGE_POINT		> 0											");
		oSqlBuilder.AppendLine("					) UL																	");
		oSqlBuilder.AppendLine("				WHERE																		");
		oSqlBuilder.AppendLine("					T_IVP_REQUEST.IVP_REQUEST_SEQ	= UL.IVP_REQUEST_SEQ				AND	");
		oSqlBuilder.AppendLine("					T_IVP_REQUEST.MAN_USER_SITE_CD	= :SITE_CD								");
		oSqlBuilder.AppendLine("				GROUP BY																	");
		oSqlBuilder.AppendLine("					T_IVP_REQUEST.MAN_USER_SITE_CD										,	");
		oSqlBuilder.AppendLine("					T_IVP_REQUEST.MAN_USER_SEQ											,	");
		oSqlBuilder.AppendLine("					UL.CHARGE_DAY															");
		oSqlBuilder.AppendLine("				UNION ALL																	");
		oSqlBuilder.AppendLine("				SELECT																		");
		oSqlBuilder.AppendLine("					MAN_USER_SITE_CD													,	");
		oSqlBuilder.AppendLine("					MAN_USER_SEQ														,	");
		oSqlBuilder.AppendLine("					TO_CHAR(CHARGE_START_DATE,'YYYY/MM/DD') AS CHARGE_DAY				,	");
		oSqlBuilder.AppendLine("					SUM(CHARGE_POINT) CHARGE_POINT											");
		oSqlBuilder.AppendLine("				FROM																		");
		oSqlBuilder.AppendLine("					T_WEB_USED_LOG															");
		oSqlBuilder.AppendLine("				WHERE																		");
		oSqlBuilder.AppendLine("					MAN_USER_SITE_CD	= :SITE_CD										AND	");
		oSqlBuilder.AppendLine("					CHARGE_START_DATE	>= :CHARGE_START_DATE_FROM						AND	");
		oSqlBuilder.AppendLine("					CHARGE_START_DATE	< :CHARGE_START_DATE_TO							AND	");
		oSqlBuilder.AppendLine("					CHARGE_POINT		> 0													");
		oSqlBuilder.AppendLine("				GROUP BY																	");
		oSqlBuilder.AppendLine("					MAN_USER_SITE_CD													,	");
		oSqlBuilder.AppendLine("					MAN_USER_SEQ														,	");
		oSqlBuilder.AppendLine("					TO_CHAR(CHARGE_START_DATE,'YYYY/MM/DD')									");
		oSqlBuilder.AppendLine("			)																				");
		oSqlBuilder.AppendLine("		GROUP BY																			");
		oSqlBuilder.AppendLine("			MAN_USER_SITE_CD															,	");
		oSqlBuilder.AppendLine("			MAN_USER_SEQ																,	");
		oSqlBuilder.AppendLine("			CHARGE_DAY																		");
		oSqlBuilder.AppendLine("	) C																					,	");
		oSqlBuilder.AppendLine("	T_USER																					");
		oSqlBuilder.AppendLine("WHERE																						");
		oSqlBuilder.AppendLine("	C.MAN_USER_SEQ = T_USER.USER_SEQ														");
		oSqlBuilder.AppendLine("ORDER BY C.MAN_USER_SITE_CD,T_USER.LOGIN_ID,C.CHARGE_DAY									");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":CHARGE_START_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
		oParamList.Add(new OracleParameter(":CHARGE_START_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));

		oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

}
