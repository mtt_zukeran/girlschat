﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者写真
--	Progaram ID		: CastPic
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater			Update Explain
  2009/08/24	iBrid(Y.Inoue)	公開/未認証一覧対応  2010/06/29	伊藤和明		検索項目に日付を追加
  2010/07/16	Koyanagi		検索項目に非公開ﾌﾗｸﾞを追加

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;
using ViComm;

public class CastPic:DbSession {

	public string siteCd;
	public string loginId;
	public string userSeq;
	public string userCharNo;
	public string castNm;
	public string handleNm;
	public string picSeq;
	public string picTitle;
	public string picDoc;
	public int objNotApprovedFlag;
	public int objNotPublishFlag;
	public int picType;
	public string castPicAttrTypeSeq;
	public string castPicAttrSeq;
	public string objPhotImgPath;
	public string objModifyRequestSpec;
	
	public CastPic() {
	}

	public int GetPageCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pPicType) {
		decimal iPageCount = 0;

		string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_CAST_PIC01 ";
		string sWhere = "";

		OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pPicType,ref sWhere);
		sSql = sSql + sWhere;

		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				iPageCount = (cmd.ExecuteScalar() as decimal?) ?? 0;
			}
		} finally {
			conn.Close();
		}

		return Convert.ToInt32(iPageCount);
	}
	public DataSet GetPageCollection(string pSiteCd,string pUserSeq,string pUserCharNo,string pPicType,int startRowIndex,int maximumRows) {

		string sOrder = "ORDER BY SITE_CD,USER_SEQ,USER_CHAR_NO,PIC_SEQ DESC ";
		string sSql = "SELECT " +
							"SITE_CD		            ," +
							"USER_SEQ		            ," +
							"USER_CHAR_NO	            ," +
							"LOGIN_ID		            ," +
							"PIC_SEQ		            ," +
							"PIC_TITLE	                ," +
							"PIC_DOC	                ," +
							"OBJ_NOT_APPROVE_FLAG		," +
							"NOT_APPROVE_MARK	        ," +
							"UPLOAD_DATE	            ," +
							"PIC_TYPE		            ," +
							"CAST_PIC_ATTR_TYPE_NM		," +
							"CAST_PIC_ATTR_NM			," +
							"SMALL_PHOTO_IMG_PATH		," +
							"OBJ_SMALL_PHOTO_IMG_PATH	" +
						"FROM(" +
						" SELECT VW_CAST_PIC01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_CAST_PIC01  ";

		string sWhere = "";
		OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pPicType,ref sWhere);
		sSql = sSql + sWhere;
		sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
		sSql = sSql + sOrder;

		DataSet ds = new DataSet();

		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_PIC01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
	private OracleParameter[] CreateWhere(string pSiteCd,string pUserSeq,string pUserCharNo,string pPicType,ref string pWhere) {
		if (string.IsNullOrEmpty(pSiteCd))
			throw new ArgumentException("pSiteCd");
		if (string.IsNullOrEmpty(pUserSeq))
			throw new ArgumentException("pUserSeq");
		if (string.IsNullOrEmpty(pUserCharNo))
			throw new ArgumentException("pUserCharNo");
		if (string.IsNullOrEmpty(pPicType))
			throw new ArgumentException("pPicType");

		ArrayList list = new ArrayList();

		// ｻｲﾄｺｰﾄﾞ 
		iBridCommLib.SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		// ﾕｰｻﾞｰSEQ 
		iBridCommLib.SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhere);
		list.Add(new OracleParameter("USER_SEQ",pUserSeq));
		// ﾕｰｻﾞｰｷｬﾗｸﾀｰ№ 
		iBridCommLib.SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhere);
		list.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));
		// 写真種別 
		if (pPicType.Equals(ViCommConst.ATTACHED_PROFILE.ToString())) {
			iBridCommLib.SysPrograms.SqlAppendWhere("PIC_TYPE IN (:PIC_TYPE1,:PIC_TYPE2)",ref pWhere);
			list.Add(new OracleParameter("PIC_TYPE1",pPicType));
			list.Add(new OracleParameter("PIC_TYPE2",ViCommConst.ATTACHED_HIDE.ToString()));
		} else {
			iBridCommLib.SysPrograms.SqlAppendWhere("PIC_TYPE = :PIC_TYPE",ref pWhere);
			list.Add(new OracleParameter("PIC_TYPE",pPicType));
		}
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public DataSet GetList(string pSiteCd,string pUserSeq,string pUserCharNo,string pPicType) {
		DataSet ds;
		DataColumn col;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT " +
								"PIC_SEQ					," +
								"OBJ_PHOTO_IMG_PATH			," +
								"PROFILE_PIC_FLAG			," +
								"OBJ_NOT_APPROVE_FLAG		," +
								"OBJ_NOT_PUBLISH_FLAG		," +
								"DISPLAY_POSITION			," +
								"PIC_TITLE					," +
								"PIC_DOC					," +
								"UPLOAD_DATE				," +
								"PIC_TYPE					," +
								"SMALL_PHOTO_IMG_PATH		," +
								"OBJ_SMALL_PHOTO_IMG_PATH	 " +
							"FROM " +
								"VW_CAST_PIC01 " +
							"WHERE " +
								"SITE_CD		= :SITE_CD		AND " +
								"USER_SEQ		= :USER_SEQ		AND " +
								"USER_CHAR_NO	= :USER_CHAR_NO	AND ";
			if (pPicType.Equals(ViCommConst.ATTACHED_PROFILE.ToString())) {
				sSql += "PIC_TYPE IN (:PIC_TYPE1,:PIC_TYPE2) ";
			} else {
				sSql += "PIC_TYPE = :PIC_TYPE ";
			}
			
			if (pPicType.Equals(ViCommConst.ATTACHED_MAIL.ToString())) {
				sSql += " AND (MAIL_STOCK_FLAG = :MAIL_STOCK_FLAG_ON OR OBJ_NOT_APPROVE_FLAG = :OBJ_NOT_APPROVE_FLAG_ON) ";
			}
			
			sSql += "ORDER BY SITE_CD,USER_SEQ,USER_CHAR_NO,DISPLAY_POSITION,PIC_SEQ DESC ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				if (pPicType.Equals(ViCommConst.ATTACHED_PROFILE.ToString())) {
					cmd.Parameters.Add("PIC_TYPE1",pPicType);
					cmd.Parameters.Add("PIC_TYPE2",ViCommConst.ATTACHED_HIDE.ToString());
				} else {
					cmd.Parameters.Add("PIC_TYPE",pPicType);
				}
				
				if (pPicType.Equals(ViCommConst.ATTACHED_MAIL.ToString())) {
					cmd.Parameters.Add("MAIL_STOCK_FLAG_ON",ViCommConst.FLAG_ON_STR);
					cmd.Parameters.Add("OBJ_NOT_APPROVE_FLAG_ON",ViCommConst.FLAG_ON_STR);
				}

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_PIC01");
				}
			}

		} finally {
			conn.Close();
		}

		if (ds.Tables[0].Rows.Count > 0) {
			col = new DataColumn("UP_BTN_FLAG",System.Type.GetType("System.Boolean"));
			ds.Tables[0].Columns.Add(col);

			col = new DataColumn("DOWN_BTN_FLAG",System.Type.GetType("System.Boolean"));
			ds.Tables[0].Columns.Add(col);

			foreach (DataRow dr in ds.Tables[0].Rows) {
				dr["UP_BTN_FLAG"] = true;
				dr["DOWN_BTN_FLAG"] = true;
			}
			DataRow drFisrt = ds.Tables[0].Rows[0];
			drFisrt["UP_BTN_FLAG"] = false;

			DataRow drLast = ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1];
			drLast["DOWN_BTN_FLAG"] = false;
		}
		return ds;
	}
	
	public DataSet GetAllList(string pSiteCd,string pUserSeq,string pUserCharNo,string pWithoutObjSeq) {
		DataSet ds;
		DataColumn col;
		
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT											");
		oSqlBuilder.AppendLine("	SITE_CD									,	");
		oSqlBuilder.AppendLine("	LOGIN_ID								,	");
		oSqlBuilder.AppendLine("	PIC_SEQ									,	");
		oSqlBuilder.AppendLine("	OBJ_PHOTO_IMG_PATH						,	");
		oSqlBuilder.AppendLine("	PROFILE_PIC_FLAG						,	");
		oSqlBuilder.AppendLine("	OBJ_NOT_APPROVE_FLAG					,	");
		oSqlBuilder.AppendLine("	OBJ_NOT_PUBLISH_FLAG					,	");
		oSqlBuilder.AppendLine("	DISPLAY_POSITION						,	");
		oSqlBuilder.AppendLine("	PIC_TITLE								,	");
		oSqlBuilder.AppendLine("	PIC_DOC									,	");
		oSqlBuilder.AppendLine("	UPLOAD_DATE								,	");
		oSqlBuilder.AppendLine("	PIC_TYPE								,	");
		oSqlBuilder.AppendLine("	SMALL_PHOTO_IMG_PATH					,	");
		oSqlBuilder.AppendLine("	OBJ_SMALL_PHOTO_IMG_PATH					");
		oSqlBuilder.AppendLine("FROM											");
		oSqlBuilder.AppendLine("	VW_CAST_PIC01								");
		oSqlBuilder.AppendLine("WHERE											");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	USER_SEQ		= :USER_SEQ				AND	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO	= :USER_CHAR_NO			AND	");
		oSqlBuilder.AppendLine("	PIC_TYPE		IN(:PIC_TYPE_ATTACHED_PROFILE,:PIC_TYPE_ATTACHED_BBS,:PIC_TYPE_ATTACHED_MAIL,:PIC_TYPE_ATTACHED_SOCIAL_GAME,:PIC_TYPE_ATTACHED_YAKYUKEN,:PIC_TYPE_ATTACHED_HIDE)	AND ");
		oSqlBuilder.AppendLine("	NOT(PIC_TYPE = :PIC_TYPE_ATTACHED_MAIL AND MAIL_STOCK_FLAG = :MAIL_STOCK_FLAG_OFF AND OBJ_NOT_APPROVE_FLAG = :OBJ_NOT_APPROVE_FLAG_OFF)");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":PIC_TYPE_ATTACHED_PROFILE",ViCommConst.ATTACHED_PROFILE));
		oParamList.Add(new OracleParameter(":PIC_TYPE_ATTACHED_BBS",ViCommConst.ATTACHED_BBS));
		oParamList.Add(new OracleParameter(":PIC_TYPE_ATTACHED_MAIL",ViCommConst.ATTACHED_MAIL));
		oParamList.Add(new OracleParameter(":PIC_TYPE_ATTACHED_SOCIAL_GAME",ViCommConst.ATTACHED_SOCIAL_GAME));
		oParamList.Add(new OracleParameter(":PIC_TYPE_ATTACHED_YAKYUKEN",ViCommConst.ATTACHED_YAKYUKEN));
		oParamList.Add(new OracleParameter(":PIC_TYPE_ATTACHED_HIDE",ViCommConst.ATTACHED_HIDE));
		oParamList.Add(new OracleParameter(":MAIL_STOCK_FLAG_OFF",ViCommConst.FLAG_OFF_STR));
		oParamList.Add(new OracleParameter(":OBJ_NOT_APPROVE_FLAG_OFF",ViCommConst.FLAG_OFF_STR));
		
		if (!string.IsNullOrEmpty(pWithoutObjSeq)) {
			oSqlBuilder.AppendLine(" AND PIC_SEQ != :WITHOUT_OBJ_SEQ");
			oParamList.Add(new OracleParameter(":WITHOUT_OBJ_SEQ",pWithoutObjSeq));
		}

		oSqlBuilder.AppendLine("ORDER BY SITE_CD,USER_SEQ,USER_CHAR_NO,DISPLAY_POSITION,PIC_SEQ DESC ");
		
		
		ds = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (ds.Tables[0].Rows.Count > 0) {
			col = new DataColumn("UP_BTN_FLAG",System.Type.GetType("System.Boolean"));
			ds.Tables[0].Columns.Add(col);

			col = new DataColumn("DOWN_BTN_FLAG",System.Type.GetType("System.Boolean"));
			ds.Tables[0].Columns.Add(col);

			foreach (DataRow dr in ds.Tables[0].Rows) {
				dr["UP_BTN_FLAG"] = true;
				dr["DOWN_BTN_FLAG"] = true;
			}
			DataRow drFisrt = ds.Tables[0].Rows[0];
			drFisrt["UP_BTN_FLAG"] = false;

			DataRow drLast = ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1];
			drLast["DOWN_BTN_FLAG"] = false;
		}
		return ds;
	}

	public DataSet SeekCastPicResembled(string pSiteCd,string pUserSeq,string pUserCharNo,string pLoginId,string pOriginalPicSeq) {
		
		DataSet oDataSet = GetAllList(pSiteCd,pUserSeq,pUserCharNo,pOriginalPicSeq);
		
		DataSet oResultDS = OpenCvHelper.SeekResembledPicByDataSet(pSiteCd,pLoginId,pOriginalPicSeq,oDataSet);

		return oResultDS;
	}

	public decimal GetPicNo() {

		decimal dPicNo = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT SEQ_OBJ.NEXTVAL FROM DUAL";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				dPicNo = (decimal)cmd.ExecuteScalar();
			}
		} finally {
			conn.Close();
		}

		return dPicNo;
	}

	public bool GetOne(string pPicSeq) {
		return GetOne("","","",pPicSeq);
	}

	public bool GetOne(string pSiteCd,string pUserSeq,string pUserCharNo,string pPicSeq) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect();

			StringBuilder sSql = new StringBuilder();

			sSql.Append("SELECT	");
			sSql.Append("	SITE_CD					,");
			sSql.Append("	USER_SEQ				,");
			sSql.Append("	USER_CHAR_NO			,");
			sSql.Append("	LOGIN_ID				,");
			sSql.Append("	CAST_NM					,");
			sSql.Append("	HANDLE_NM				,");
			sSql.Append("	LOGIN_ID				,");
			sSql.Append("	PIC_SEQ					,");
			sSql.Append("	PIC_TITLE				,");
			sSql.Append("	PIC_DOC					,");
			sSql.Append("	OBJ_NOT_APPROVE_FLAG	,");
			sSql.Append("	OBJ_NOT_PUBLISH_FLAG	,");
			sSql.Append("	PIC_TYPE				,");
			sSql.Append("	CAST_PIC_ATTR_TYPE_SEQ	,");
			sSql.Append("	CAST_PIC_ATTR_SEQ		,");
			sSql.Append("	OBJ_PHOTO_IMG_PATH		,");
			sSql.Append("	OBJ_MODIFY_REQUEST_SPEC	");
			sSql.Append("FROM ");
			sSql.Append("	VW_CAST_PIC01 ");
			sSql.Append("WHERE ");

			if (!pSiteCd.Equals(string.Empty)) {
				sSql.Append("	SITE_CD			=: SITE_CD		AND ");
				sSql.Append("	USER_SEQ		=: USER_SEQ		AND ");
				sSql.Append("	USER_CHAR_NO	=: USER_CHAR_NO	AND ");
			}
			sSql.Append("	PIC_SEQ			=: PIC_SEQ			");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (ds = new DataSet()) {
				if (!pSiteCd.Equals(string.Empty)) {
					cmd.Parameters.Add("SITE_CD",pSiteCd);
					cmd.Parameters.Add("USER_SEQ",pUserSeq);
					cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				}
				cmd.Parameters.Add("PIC_SEQ",pPicSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_PIC01");
					if (ds.Tables["VW_CAST_PIC01"].Rows.Count != 0) {
						dr = ds.Tables["VW_CAST_PIC01"].Rows[0];
						siteCd = dr["SITE_CD"].ToString();
						userSeq = dr["USER_SEQ"].ToString();
						userCharNo = dr["USER_CHAR_NO"].ToString();
						castNm = dr["CAST_NM"].ToString();
						handleNm = dr["HANDLE_NM"].ToString();
						loginId = dr["LOGIN_ID"].ToString();
						picSeq = dr["PIC_SEQ"].ToString();
						picTitle = dr["PIC_TITLE"].ToString();
						picDoc = dr["PIC_DOC"].ToString();
						objNotApprovedFlag = int.Parse(dr["OBJ_NOT_APPROVE_FLAG"].ToString());
						objNotPublishFlag = int.Parse(dr["OBJ_NOT_PUBLISH_FLAG"].ToString());
						picType = int.Parse(dr["PIC_TYPE"].ToString());
						castPicAttrTypeSeq = dr["CAST_PIC_ATTR_TYPE_SEQ"].ToString();
						castPicAttrSeq = dr["CAST_PIC_ATTR_SEQ"].ToString();
						objPhotImgPath = dr["OBJ_PHOTO_IMG_PATH"].ToString();
						objModifyRequestSpec = dr["OBJ_MODIFY_REQUEST_SPEC"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public int GetApproveManagePageCount(string pSiteCd,string pNotApproveFlag,string pPicType) {
		return GetApproveManagePageCount(pSiteCd,pNotApproveFlag,null,pPicType,null,null,null);
	}
	public int GetApproveManagePageCount(string pSiteCd, string pNotApproveFlag, string pNotPublishFlag, string pPicType, string pUploadDateFrom, string pUploadDateTo, string pLoginId) {
		return GetApproveManagePageCount(pSiteCd, pNotApproveFlag, pNotPublishFlag, pPicType, pUploadDateFrom, pUploadDateTo,null,null, pLoginId, string.Empty);
	}
	public int GetApproveManagePageCount(string pSiteCd,string pNotApproveFlag,string pNotPublishFlag,string pPicType,string pUploadDateFrom,string pUploadDateTo,string pCastPicAttrType,string pCastPicAttrTypeValue,string pLoginId) {
		return GetApproveManagePageCount(pSiteCd,pNotApproveFlag,pNotPublishFlag,pPicType,pUploadDateFrom,pUploadDateTo,pCastPicAttrType,pCastPicAttrTypeValue,pLoginId,string.Empty);
	}
	public int GetApproveManagePageCount(string pSiteCd, string pNotApproveFlag, string pNotPublishFlag, string pPicType, string pUploadDateFrom, string pUploadDateTo, string pCastPicAttrType, string pCastPicAttrTypeValue, string pLoginId, string pItemNo)
	{
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_CAST_PIC01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateApproveManageWhere(pSiteCd, pNotApproveFlag, pNotPublishFlag, pPicType, pUploadDateFrom, pUploadDateTo, pCastPicAttrType, pCastPicAttrTypeValue, pLoginId, pItemNo, ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetApproveManagePageCollection(string pSiteCd,string pNotApproveFlag,string pPicType,int startRowIndex,int maximumRows) {
		return this.GetApproveManagePageCollection(pSiteCd,pNotApproveFlag,null,pPicType,null,null,null,startRowIndex,maximumRows);
	}
	public DataSet GetApproveManagePageCollection(string pSiteCd, string pNotApproveFlag, string pNotPublishFlag, string pPicType, string pUploadDateFrom, string pUploadDateTo, string pLoginId, int startRowIndex, int maximumRows) {
		return this.GetApproveManagePageCollection(pSiteCd, pNotApproveFlag, pNotPublishFlag, pPicType, pUploadDateFrom, pUploadDateTo,null,null, pLoginId, string.Empty, startRowIndex, maximumRows);
	}
	public DataSet GetApproveManagePageCollection(string pSiteCd,string pNotApproveFlag,string pNotPublishFlag,string pPicType,string pUploadDateFrom,string pUploadDateTo,string pCastPicAttrType,string pCastPicAttrTypeValue,string pLoginId,int startRowIndex,int maximumRows) {
		return this.GetApproveManagePageCollection(pSiteCd,pNotApproveFlag,pNotPublishFlag,pPicType,pUploadDateFrom,pUploadDateTo,pCastPicAttrType,pCastPicAttrTypeValue,pLoginId,string.Empty,startRowIndex,maximumRows);
	}
	public DataSet GetApproveManagePageCollection(string pSiteCd, string pNotApproveFlag, string pNotPublishFlag, string pPicType, string pUploadDateFrom, string pUploadDateTo, string pCastPicAttrType, string pCastPicAttrTypeValue, string pLoginId, string pItemNo, int startRowIndex, int maximumRows)
	{
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY SITE_CD,PIC_SEQ DESC ";

			string sSql = "SELECT " +
								"SITE_CD					," +
								"(SELECT SITE_NM FROM T_SITE WHERE T_SITE.SITE_CD=INNER.SITE_CD) AS SITE_NM					," +
								"OBJ_SMALL_PHOTO_IMG_PATH	," +
								"OBJ_PHOTO_IMG_PATH			," +
								"USER_SEQ					," +
								"USER_CHAR_NO				," +
								"LOGIN_ID					," +
								"CAST_NM					," +
								"EMAIL_ADDR					," +
								"HANDLE_NM					," +
								"UPLOAD_DATE				," +
								"PIC_TITLE					," +
								"PIC_DOC					," +
								"PIC_SEQ					," +
								"PIC_TYPE					," +
								"CAST_PIC_ATTR_TYPE_SEQ		," +
								"CAST_PIC_ATTR_TYPE_NM		," +
								"CAST_PIC_ATTR_SEQ			," +
								"CAST_PIC_ATTR_NM			," +
								"OBJ_NOT_PUBLISH_FLAG		," +
								"CAUTION_FLAG				," +
								"NOT_APPROVE_MARK_ADMIN		" +
							"FROM(" +
							" SELECT VW_CAST_PIC01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_CAST_PIC01";
			string sWhere = "";
			OracleParameter[] objParms = CreateApproveManageWhere(pSiteCd, pNotApproveFlag, pNotPublishFlag, pPicType, pUploadDateFrom, pUploadDateTo, pCastPicAttrType, pCastPicAttrTypeValue, pLoginId, pItemNo, ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ") INNER WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_PIC01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetAllProfilePic() {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT ");
			sSql.Append("	SITE_CD		,");
			sSql.Append("	USER_SEQ	,");
			sSql.Append("	USER_CHAR_NO,");
			sSql.Append("	LOGIN_ID	,");
			sSql.Append("	PIC_SEQ		");
			sSql.Append("FROM ");
			sSql.Append("	VW_CAST_PIC01 ");
			sSql.Append("WHERE ");
			sSql.Append("	PROFILE_PIC_FLAG =: PROFILE_PIC_FLAG ");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				cmd.Parameters.Add("PROFILE_PIC_FLAG","1");

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_PIC01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
	private OracleParameter[] CreateApproveManageWhere(string pSiteCd,string pNotApproveFlag,string pNotPublishFlag,string pPicType,string pUploadDateFrom,string pUploadDateTo, string pCastPicAttrType, string pCastPicAttrTypeValue, string pLoginId, string pItemNo,ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();

		if (pSiteCd.Equals("") == false) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (pPicType.Equals(ViCommConst.ATTACHED_PROFILE.ToString())) {
			iBridCommLib.SysPrograms.SqlAppendWhere("PIC_TYPE IN (:PIC_TYPE1,:PIC_TYPE2)",ref pWhere);
			list.Add(new OracleParameter("PIC_TYPE1",pPicType));
			list.Add(new OracleParameter("PIC_TYPE2",ViCommConst.ATTACHED_HIDE.ToString()));
		} else {
			iBridCommLib.SysPrograms.SqlAppendWhere("PIC_TYPE = :PIC_TYPE",ref pWhere);
			list.Add(new OracleParameter("PIC_TYPE",pPicType));
		}

		if ((!string.IsNullOrEmpty(pNotApproveFlag)) || (string.IsNullOrEmpty(pNotPublishFlag))) {
			if (pNotApproveFlag.Equals(ViCommConst.FLAG_ON.ToString()) & pNotPublishFlag.Equals(ViCommConst.FLAG_ON.ToString())) {
				SysPrograms.SqlAppendWhere("(OBJ_NOT_APPROVE_FLAG = :OBJ_NOT_APPROVE_FLAG OR OBJ_NOT_PUBLISH_FLAG = :OBJ_NOT_PUBLISH_FLAG)",ref pWhere);
				list.Add(new OracleParameter("OBJ_NOT_APPROVE_FLAG",pNotApproveFlag));
				list.Add(new OracleParameter("OBJ_NOT_PUBLISH_FLAG",pNotPublishFlag));
			} else {
				if (pNotApproveFlag.Equals("") == false) {
					SysPrograms.SqlAppendWhere("OBJ_NOT_APPROVE_FLAG = :OBJ_NOT_APPROVE_FLAG",ref pWhere);
					list.Add(new OracleParameter("OBJ_NOT_APPROVE_FLAG",pNotApproveFlag));
				}

				// 非公開フラグ
				if (!string.IsNullOrEmpty(pNotPublishFlag)) {
					SysPrograms.SqlAppendWhere("OBJ_NOT_PUBLISH_FLAG = :OBJ_NOT_PUBLISH_FLAG",ref pWhere);
					list.Add(new OracleParameter("OBJ_NOT_PUBLISH_FLAG",pNotPublishFlag));
				}
			}
		}

		// アップロード日時(from)
		if (!string.IsNullOrEmpty(pUploadDateFrom)) {
			SysPrograms.SqlAppendWhere("UPLOAD_DATE_RAW >= :UPLOAD_DATE_FROM",ref pWhere);
			DateTime dtFrom = DateTime.ParseExact(pUploadDateFrom,"yyyy/MM/dd",null,System.Globalization.DateTimeStyles.None).Date;
			list.Add(new OracleParameter("UPLOAD_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
		}

		// アップロード日時(to)
		if (!string.IsNullOrEmpty(pUploadDateTo)) {
			SysPrograms.SqlAppendWhere("UPLOAD_DATE_RAW < :UPLOAD_DATE_TO",ref pWhere);
			DateTime dtFrom = DateTime.ParseExact(pUploadDateTo,"yyyy/MM/dd",null,System.Globalization.DateTimeStyles.None).Date.AddDays(1);
			list.Add(new OracleParameter("UPLOAD_DATE_TO",OracleDbType.Date,dtFrom,ParameterDirection.Input));
		}

		// ﾛｸﾞｲﾝID
		if (!string.IsNullOrEmpty(pLoginId)) {
			SysPrograms.SqlAppendWhere("LOGIN_ID = :LOGIN_ID",ref pWhere);
			list.Add(new OracleParameter("LOGIN_ID",pLoginId));
		}


		// 画像属性区分
		if (!string.IsNullOrEmpty(pCastPicAttrType)) {
			SysPrograms.SqlAppendWhere("CAST_PIC_ATTR_TYPE_SEQ = :CAST_PIC_ATTR_TYPE_SEQ", ref pWhere);
			list.Add(new OracleParameter("CAST_PIC_ATTR_TYPE_SEQ", pCastPicAttrType));
		}

		// 画像属性値
		if (!string.IsNullOrEmpty(pCastPicAttrTypeValue)) {
			SysPrograms.SqlAppendWhere("CAST_PIC_ATTR_SEQ = :CAST_PIC_ATTR_SEQ", ref pWhere);
			list.Add(new OracleParameter("CAST_PIC_ATTR_SEQ", pCastPicAttrTypeValue));
		}

		// 企画画像を取捨
		if (string.IsNullOrEmpty(pItemNo)) {
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_CAST_PIC WHERE SITE_CD = VW_CAST_PIC01.SITE_CD AND USER_SEQ = VW_CAST_PIC01.USER_SEQ AND USER_CHAR_NO = VW_CAST_PIC01.USER_CHAR_NO AND PIC_SEQ = VW_CAST_PIC01.PIC_SEQ AND PLANNING_TYPE = 0)",ref pWhere);
		} else {
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_CAST_PIC WHERE SITE_CD = VW_CAST_PIC01.SITE_CD AND USER_SEQ = VW_CAST_PIC01.USER_SEQ AND USER_CHAR_NO = VW_CAST_PIC01.USER_CHAR_NO AND PIC_SEQ = VW_CAST_PIC01.PIC_SEQ AND PLANNING_TYPE <> 0)",ref pWhere);
		}
		
		// 画像種別がメール添付用の場合にストック利用可フラグでの絞り込み
		if (!string.IsNullOrEmpty(pPicType) && pPicType.Equals(ViCommConst.ATTACHED_MAIL.ToString()) &&
			!string.IsNullOrEmpty(pNotApproveFlag) && pNotApproveFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_CAST_PIC WHERE SITE_CD = VW_CAST_PIC01.SITE_CD AND USER_SEQ = VW_CAST_PIC01.USER_SEQ AND USER_CHAR_NO = VW_CAST_PIC01.USER_CHAR_NO AND PIC_SEQ = VW_CAST_PIC01.PIC_SEQ AND MAIL_STOCK_FLAG = 1)",ref pWhere);
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
	public int GetProfilePicPageCount(string pSiteCd, string pUploadDateFrom, string pUploadDateTo, string pNotApproveFlag, string pProfilePicFlag, string pCastPicAttrType, string pCastPicAttrTypeValue, string pLoginId) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_CAST_PIC01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateProfilePicWhere(pSiteCd, pUploadDateFrom, pUploadDateTo, pNotApproveFlag, pProfilePicFlag, pCastPicAttrType, pCastPicAttrTypeValue, pLoginId, ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}
	public DataSet GetProfilePicPageCollection(string pSiteCd, string pUploadDateFrom, string pUploadDateTo, string pNotApproveFlag, string pProfilePicFlag, string pCastPicAttrType, string pCastPicAttrTypeValue, string pLoginId, int startRowIndex, int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY SITE_CD,PIC_SEQ DESC ";

			string sSql = "SELECT " +
								"SITE_CD					," +
								"OBJ_SMALL_PHOTO_IMG_PATH	," +
								"OBJ_PHOTO_IMG_PATH			," +
								"USER_SEQ					," +
								"USER_CHAR_NO				," +
								"LOGIN_ID					," +
								"CAST_NM					," +
								"HANDLE_NM					," +
								"UPLOAD_DATE				," +
								"PIC_SEQ					," +
								"PIC_TYPE					," +
								"PIC_TITLE					," +
								"PIC_DOC					," +
								"PROFILE_PIC_FLAG			," +
								"OBJ_NOT_APPROVE_FLAG		," +
								"OBJ_NOT_PUBLISH_FLAG		" +
							"FROM(" +
							" SELECT VW_CAST_PIC01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_CAST_PIC01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateProfilePicWhere(pSiteCd,pUploadDateFrom,pUploadDateTo,pNotApproveFlag,pProfilePicFlag,pCastPicAttrType,pCastPicAttrTypeValue,pLoginId,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_PIC01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateProfilePicWhere(string pSiteCd, string pUploadDateFrom, string pUploadDateTo, string pNotApproveFlag, string pProfilePicFlag, string pCastPicAttrType, string pCastPicAttrTypeValue, string pLoginId, ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();

		if (pSiteCd.Equals("") == false) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}
		SysPrograms.SqlAppendWhere("PIC_TYPE IN (:PIC_TYPE1,:PIC_TYPE2)",ref pWhere);
		list.Add(new OracleParameter("PIC_TYPE1",ViCommConst.ATTACHED_PROFILE.ToString()));
		list.Add(new OracleParameter("PIC_TYPE2",ViCommConst.ATTACHED_HIDE.ToString()));

		// アップロード日時(from)
		if (!string.IsNullOrEmpty(pUploadDateFrom)) {
			SysPrograms.SqlAppendWhere("UPLOAD_DATE_RAW >= :UPLOAD_DATE_FROM",ref pWhere);
			DateTime dtFrom = DateTime.ParseExact(pUploadDateFrom,"yyyy/MM/dd",null,System.Globalization.DateTimeStyles.None).Date;
			list.Add(new OracleParameter("UPLOAD_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
		}

		// アップロード日時(to)
		if (!string.IsNullOrEmpty(pUploadDateTo)) {
			SysPrograms.SqlAppendWhere("UPLOAD_DATE_RAW < :UPLOAD_DATE_TO",ref pWhere);
			DateTime dtFrom = DateTime.ParseExact(pUploadDateTo,"yyyy/MM/dd",null,System.Globalization.DateTimeStyles.None).Date.AddDays(1);
			list.Add(new OracleParameter("UPLOAD_DATE_TO",OracleDbType.Date,dtFrom,ParameterDirection.Input));
		}

		// 未認証フラグ
		if (!string.IsNullOrEmpty(pNotApproveFlag) && pNotApproveFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			SysPrograms.SqlAppendWhere("OBJ_NOT_APPROVE_FLAG = :OBJ_NOT_APPROVE_FLAG",ref pWhere);
			list.Add(new OracleParameter("OBJ_NOT_APPROVE_FLAG",ViCommConst.FLAG_ON));
		}

		// プロフ写真フラグ
		if (!string.IsNullOrEmpty(pProfilePicFlag) && pProfilePicFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			SysPrograms.SqlAppendWhere("PROFILE_PIC_FLAG = :PROFILE_PIC_FLAG",ref pWhere);
			list.Add(new OracleParameter("PROFILE_PIC_FLAG",ViCommConst.FLAG_ON));
		}

		// 画像属性区分
		if (!string.IsNullOrEmpty(pCastPicAttrType)) {
			SysPrograms.SqlAppendWhere("CAST_PIC_ATTR_TYPE_SEQ = :CAST_PIC_ATTR_TYPE_SEQ", ref pWhere);
			list.Add(new OracleParameter("CAST_PIC_ATTR_TYPE_SEQ", pCastPicAttrType));
		}

		// 画像属性値
		if (!string.IsNullOrEmpty(pCastPicAttrTypeValue)) {
			SysPrograms.SqlAppendWhere("CAST_PIC_ATTR_SEQ = :CAST_PIC_ATTR_SEQ", ref pWhere);
			list.Add(new OracleParameter("CAST_PIC_ATTR_SEQ", pCastPicAttrTypeValue));
		}

		// ﾛｸﾞｲﾝID
		if (!string.IsNullOrEmpty(pLoginId)) {
			SysPrograms.SqlAppendWhere("LOGIN_ID = :LOGIN_ID",ref pWhere);
			list.Add(new OracleParameter("LOGIN_ID",pLoginId));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	/// <summary>
	/// 退会出演者画像一覧を取得
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pLoginId"></param>
	/// <param name="pWithdrawalDateFrom"></param>
	/// <param name="pWithdrawalDateTo"></param>
	/// <param name="pPicType"></param>
	/// <param name="startRowIndex"></param>
	/// <param name="maximumRows"></param>
	/// <returns></returns>
	public DataSet GetWithdrawalPicPageCollection(
		string pSiteCd
		,string pLoginId
		,string pWithdrawalDateFrom
		,string pWithdrawalDateTo
		,string pPicType
		,int startRowIndex
		,int maximumRows
	) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		string sSortExpression = string.Empty;
		string sExecSql = string.Empty;

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	P.*");
		oSqlBuilder.AppendLine("	,U.LOGIN_ID");
		oSqlBuilder.AppendLine("	,CC.HANDLE_NM");
		oSqlBuilder.AppendLine("	,PI.PIC_SEQ");
		oSqlBuilder.AppendLine("	,PI.UPLOAD_DATE");
		oSqlBuilder.AppendLine("	,GET_PHOTO_IMG_PATH (CC.SITE_CD,U.LOGIN_ID,PI.PIC_SEQ) AS PHOTO_IMG_PATH");
		oSqlBuilder.AppendLine("	,GET_SMALL_PHOTO_IMG_PATH (CC.SITE_CD,U.LOGIN_ID,PI.PIC_SEQ) AS SMALL_PHOTO_IMG_PATH");
		this.SetWithdrawalPicTableAndWhere(
			ref oSqlBuilder
			,ref oParamList
			,pSiteCd
			,pLoginId
			,pWithdrawalDateFrom
			,pWithdrawalDateTo
			,pPicType
			,string.Empty
		);
		sSortExpression = "ORDER BY PI.SITE_CD,PI.PIC_SEQ DESC";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		return ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
	}

	/// <summary>
	/// 退会出演者画像一覧の件数を取得
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pLoginId"></param>
	/// <param name="pWithdrawalDateFrom"></param>
	/// <param name="pWithdrawalDateTo"></param>
	/// <param name="pPicType"></param>
	/// <returns></returns>
	public int GetWithdrawalPicPageCount(
		string pSiteCd
		,string pLoginId
		,string pWithdrawalDateFrom
		,string pWithdrawalDateTo
		,string pPicType
	) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT COUNT(*)");
		this.SetWithdrawalPicTableAndWhere(
			ref oSqlBuilder
			,ref oParamList
			,pSiteCd
			,pLoginId
			,pWithdrawalDateFrom
			,pWithdrawalDateTo
			,pPicType
			,string.Empty
		);

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return int.Parse(oDataSet.Tables[0].Rows[0][0].ToString());
	}

	/// <summary>
	/// 退会出演者画像データを取得
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pLoginId"></param>
	/// <param name="pPicSeq"></param>
	/// <returns></returns>
	public DataSet GetOneWithdrawalPic(
		string pSiteCd
		,string pLoginId
		,string pPicSeq
	) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	P.*");
		oSqlBuilder.AppendLine("	,U.LOGIN_ID");
		oSqlBuilder.AppendLine("	,CC.HANDLE_NM");
		oSqlBuilder.AppendLine("	,PI.PIC_SEQ");
		oSqlBuilder.AppendLine("	,PI.UPLOAD_DATE");
		oSqlBuilder.AppendLine("	,GET_PHOTO_IMG_PATH (CC.SITE_CD,U.LOGIN_ID,PI.PIC_SEQ) AS PHOTO_IMG_PATH");
		oSqlBuilder.AppendLine("	,GET_SMALL_PHOTO_IMG_PATH (CC.SITE_CD,U.LOGIN_ID,PI.PIC_SEQ) AS SMALL_PHOTO_IMG_PATH");
		this.SetWithdrawalPicTableAndWhere(
			ref oSqlBuilder
			,ref oParamList
			,pSiteCd
			,pLoginId
			,string.Empty
			,string.Empty
			,string.Empty
			,pPicSeq
		);

		return ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
	}

	/// <summary>
	/// 退会出演者画像取得用のテーブル＆条件設定
	/// </summary>
	/// <param name="pSqlBuilder"></param>
	/// <param name="pParamList"></param>
	/// <param name="pSiteCd"></param>
	/// <param name="pLoginId"></param>
	/// <param name="pWithdrawalDateFrom"></param>
	/// <param name="pWithdrawalDateTo"></param>
	/// <param name="pPicType"></param>
	private void SetWithdrawalPicTableAndWhere(
		ref StringBuilder pSqlBuilder
		,ref List<OracleParameter> pParamList
		,string pSiteCd
		,string pLoginId
		,string pWithdrawalDateFrom
		,string pWithdrawalDateTo
		,string pPicType
		,string pPicSeq
	) {
		string pWhere = "";

		pSqlBuilder.AppendLine("FROM");
		// 退会申請テーブル
		pSqlBuilder.AppendLine("	(SELECT");
		pSqlBuilder.AppendLine("			MAX(UPDATE_DATE) AS WITHDRAWAL_DATE");
		pSqlBuilder.AppendLine("			,SITE_CD");
		pSqlBuilder.AppendLine("			,USER_SEQ");
		pSqlBuilder.AppendLine("			,USER_CHAR_NO");
		pSqlBuilder.AppendLine("		FROM");
		pSqlBuilder.AppendLine("			T_ACCEPT_WITHDRAWAL");
		pWhere = "";
		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
		pParamList.Add(new OracleParameter("SITE_CD",pSiteCd));
		pSqlBuilder.AppendLine(pWhere);
		pSqlBuilder.AppendLine("		GROUP BY");
		pSqlBuilder.AppendLine("			SITE_CD,USER_SEQ,USER_CHAR_NO");
		pSqlBuilder.AppendLine("	) P");
		// ユーザテーブル
		pSqlBuilder.AppendLine("	INNER JOIN T_USER U");
		pSqlBuilder.AppendLine("		ON P.USER_SEQ = U.USER_SEQ");
		pSqlBuilder.AppendLine("		AND U.USER_STATUS = :STATUS_WITHDRAWAL");
		pSqlBuilder.AppendLine("		AND U.SEX_CD = :SEX_CD_CAST");
		pParamList.Add(new OracleParameter("STATUS_WITHDRAWAL",ViCommConst.USER_WOMAN_RESIGNED));
		pParamList.Add(new OracleParameter("SEX_CD_CAST",ViCommConst.OPERATOR));
		// 画像テーブル
		pSqlBuilder.AppendLine("	INNER JOIN T_CAST_PIC PI");
		pSqlBuilder.AppendLine("		ON P.SITE_CD = PI.SITE_CD");
		pSqlBuilder.AppendLine("		AND P.USER_SEQ = PI.USER_SEQ");
		pSqlBuilder.AppendLine("		AND P.USER_CHAR_NO = PI.USER_CHAR_NO");
		// 出演者キャラクターテーブル
		pSqlBuilder.AppendLine("	INNER JOIN T_CAST_CHARACTER CC");
		pSqlBuilder.AppendLine("		ON U.USER_SEQ = CC.USER_SEQ");

		pWhere = "";
		// 退会日
		if (!string.IsNullOrEmpty(pWithdrawalDateFrom)) {
			DateTime dtFrom = new DateTime();
			if (DateTime.TryParse(pWithdrawalDateFrom,out dtFrom)) {
				SysPrograms.SqlAppendWhere("P.WITHDRAWAL_DATE >= :WITHDRAWAL_DATE_FROM",ref pWhere);
				pParamList.Add(new OracleParameter("WITHDRAWAL_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			}
		}
		if (!string.IsNullOrEmpty(pWithdrawalDateTo)) {
			DateTime dtTo = new DateTime();
			if (DateTime.TryParse(pWithdrawalDateTo,out dtTo)) {
				SysPrograms.SqlAppendWhere("P.WITHDRAWAL_DATE < :WITHDRAWAL_DATE_TO",ref pWhere);
				pParamList.Add(new OracleParameter("WITHDRAWAL_DATE_TO",OracleDbType.Date,dtTo.AddDays(1),ParameterDirection.Input));
			}
		}
		// ログインID
		if (!string.IsNullOrEmpty(pLoginId)) {
			SysPrograms.SqlAppendWhere("U.LOGIN_ID = :LOGIN_ID",ref pWhere);
			pParamList.Add(new OracleParameter("LOGIN_ID",pLoginId));
		}
		// 画像種類
		if (!string.IsNullOrEmpty(pPicType)) {
			SysPrograms.SqlAppendWhere(string.Format("PI.PIC_TYPE IN ({0})",pPicType),ref pWhere);
		}
		// 画像SEQ
		if (!string.IsNullOrEmpty(pPicSeq)) {
			SysPrograms.SqlAppendWhere("PI.PIC_SEQ = :PIC_SEQ",ref pWhere);
			pParamList.Add(new OracleParameter("PIC_SEQ",pPicSeq));
		}
		pSqlBuilder.AppendLine(pWhere);
	}
}
