/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: パートナーサイト
--	Progaram ID		: PartnerSite
--
--  Creation Date	: 2010.03.15
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class PartnerSite:DbSession {

	public PartnerSite() {
	}

	public DataSet GetList(string pStopFlag) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT PARTNER_SITE_SEQ,PARTNER_SITE_NM FROM T_PARTNER_SITE ";
			if (!pStopFlag.Equals("")) {
				sSql += "WHERE STOP_FLAG = :STOP_FLAG ";
			}
			sSql += " ORDER BY PARTNER_SITE_SEQ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				if (!pStopFlag.Equals("")) {
					cmd.Parameters.Add("STOP_FLAG",pStopFlag);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public int GetPageCount() {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_PARTNER_SITE ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY PARTNER_SITE_SEQ";
			string sSql = "SELECT " +
							"PARTNER_SITE_SEQ		," +
							"PARTNER_SITE_NM		," +
							"PARTNER_SITE_INITIAL   ," +
							"REGIST_USER_ACCEPT_URL ," +
							"STOP_FLAG				," +
							"UPDATE_DATE			," +
							"REVISION_NO			" +
						  "FROM(" +
							" SELECT T_PARTNER_SITE.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM T_PARTNER_SITE  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_PARTNER_SITE");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(ref string pWhere) {
		ArrayList list = new ArrayList();
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}
