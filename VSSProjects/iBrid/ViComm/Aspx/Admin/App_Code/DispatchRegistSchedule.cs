/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 新規登録振分け予定
--	Progaram ID		: DispatchRegistSchedule
--
--  Creation Date	: 2010.04.13
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class DispatchRegistSchedule:DbSession {

	public DispatchRegistSchedule() {
	}

	public int GetPageCount(string pRegistDay) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_DISPATCH_REGIST_SCHEDULE01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pRegistDay,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pRegistDay,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY PARTNER_SITE_SEQ";
			string sSql = "SELECT " +
							"PARTNER_SITE_SEQ		," +
							"PARTNER_SITE_NM		," +
							"PARTNER_STOP_FLAG		," +
							"SCHEDULE_SEQ			," +
							"REGIST_USER_START_DATE	," +
							"REGIST_USER_END_DATE	," +
							"SCHEDULE_STOP_FLAG		," +
							"ACCOUNT_LOCK_DAYS		," +
							"UPDATE_DATE			" +
						  "FROM(" +
							" SELECT VW_DISPATCH_REGIST_SCHEDULE01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_DISPATCH_REGIST_SCHEDULE01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pRegistDay,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd))
				using (ds = new DataSet()) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public int GetDataCnt(string pRegistDay) {
		DataSet ds;
		DataRow dr;
		int iCnt = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(SCHEDULE_SEQ) AS ROW_COUNT FROM VW_DISPATCH_REGIST_SCHEDULE02 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pRegistDay,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iCnt = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iCnt;
	}

	private OracleParameter[] CreateWhere(string pRegistDay,ref string pWhere) {
		ArrayList list = new ArrayList();

		SysPrograms.SqlAppendWhere("REGIST_USER_START_DAY <= :REGIST_USER_START_DAY",ref pWhere);
		list.Add(new OracleParameter("REGIST_USER_START_DAY",pRegistDay));
		SysPrograms.SqlAppendWhere("REGIST_USER_END_DAY >= :REGIST_USER_END_DAY",ref pWhere);
		list.Add(new OracleParameter("REGIST_USER_END_DAY",pRegistDay));

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public bool IsExistStart(string pPartnerSiteSeq,string pScheduleSeq,string pRegistDate) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(SCHEDULE_SEQ) AS ROW_COUNT FROM VW_DISPATCH_REGIST_SCHEDULE02 " +
							"WHERE " +
								"PARTNER_SITE_SEQ		=  :PARTNER_SITE_SEQ										AND " +
								"REGIST_USER_START_DATE <= TO_DATE(:REGIST_USER_START_DATE,'yyyy/mm/dd hh24:mi:ss')	AND " +
								"REGIST_USER_END_DATE	>  TO_DATE(:REGIST_USER_END_DATE  ,'yyyy/mm/dd hh24:mi:ss') ";

			if (!pScheduleSeq.Equals("")) {
				sSql += "AND SCHEDULE_SEQ			<> :SCHEDULE_SEQ ";
			}

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("PARTNER_SITE_SEQ",pPartnerSiteSeq);
				cmd.Parameters.Add("REGIST_USER_START_DATE",pRegistDate);
				cmd.Parameters.Add("REGIST_USER_END_DATE",pRegistDate);
				if (!pScheduleSeq.Equals("")) {
					cmd.Parameters.Add("SCHEDULE_SEQ",pScheduleSeq);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					if (int.Parse(dr["ROW_COUNT"].ToString()) > 0) {
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public bool IsExistEnd(string pPartnerSiteSeq,string pScheduleSeq,string pRegistDate) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(SCHEDULE_SEQ) AS ROW_COUNT FROM VW_DISPATCH_REGIST_SCHEDULE02 " +
							"WHERE " +
								"PARTNER_SITE_SEQ		=  :PARTNER_SITE_SEQ										AND " +
								"REGIST_USER_START_DATE <  TO_DATE(:REGIST_USER_START_DATE,'yyyy/mm/dd hh24:mi:ss')	AND " +
								"REGIST_USER_END_DATE	>= TO_DATE(:REGIST_USER_END_DATE  ,'yyyy/mm/dd hh24:mi:ss') ";

			if (!pScheduleSeq.Equals("")) {
				sSql += "AND SCHEDULE_SEQ			<> :SCHEDULE_SEQ ";
			}

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("PARTNER_SITE_SEQ",pPartnerSiteSeq);
				cmd.Parameters.Add("REGIST_USER_START_DATE",pRegistDate);
				cmd.Parameters.Add("REGIST_USER_END_DATE",pRegistDate);
				if (!pScheduleSeq.Equals("")) {
					cmd.Parameters.Add("SCHEDULE_SEQ",pScheduleSeq);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					if (int.Parse(dr["ROW_COUNT"].ToString()) > 0) {
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public bool IsExist(string pPartnerSiteSeq,string pScheduleSeq,string pRegistStartDate,string pRegistEndDate) {
		DataSet ds;
		bool bExist = false;
		try {
			conn = DbConnect();

			string sSql = "SELECT " +
								"TO_CHAR(REGIST_USER_START_DATE,'YYYYMMDDHH24MISS') AS REGIST_USER_START_DATE," +
								"TO_CHAR(REGIST_USER_END_DATE,  'YYYYMMDDHH24MISS') AS REGIST_USER_END_DATE " +
							"FROM VW_DISPATCH_REGIST_SCHEDULE02 " +
							"WHERE " +
								"PARTNER_SITE_SEQ		=  :PARTNER_SITE_SEQ	AND " +
								"REGIST_USER_START_DATE > TO_DATE(:REGIST_USER_START_DATE,'yyyy/mm/dd hh24:mi:ss')	";

			if (!pScheduleSeq.Equals("")) {
				sSql += "AND SCHEDULE_SEQ			<> :SCHEDULE_SEQ ";
			}

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("PARTNER_SITE_SEQ",pPartnerSiteSeq);
				cmd.Parameters.Add("REGIST_USER_START_DATE",pRegistStartDate);
				if (!pScheduleSeq.Equals("")) {
					cmd.Parameters.Add("SCHEDULE_SEQ",pScheduleSeq);
				}

				da.Fill(ds);
				foreach (DataRow dr in ds.Tables[0].Rows) {
					if (System.Int64.Parse(dr["REGIST_USER_END_DATE"].ToString()) <
						System.Int64.Parse(pRegistEndDate.Replace("/","").Replace(":","").Replace(" ",""))) {
						bExist = true;
						break;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public int GetPageCount(string pPartnerSiteSeq,string pRegistDay) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_DISPATCH_REGIST_SCHEDULE01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pPartnerSiteSeq,pRegistDay,ref sWhere);
			sSql += sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pPartnerSiteSeq,string pRegistDay,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY PARTNER_SITE_SEQ, REGIST_USER_START_DATE";
			string sSql = "SELECT " +
							"PARTNER_SITE_SEQ		," +
							"PARTNER_SITE_NM		," +
							"PARTNER_STOP_FLAG		," +
							"SCHEDULE_SEQ			," +
							"REGIST_USER_START_DATE	," +
							"REGIST_USER_END_DATE	," +
							"SCHEDULE_STOP_FLAG		," +
							"ACCOUNT_LOCK_DAYS		," +
							"UPDATE_DATE			" +
						  "FROM(" +
							" SELECT VW_DISPATCH_REGIST_SCHEDULE01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_DISPATCH_REGIST_SCHEDULE01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pPartnerSiteSeq,pRegistDay,ref sWhere);
			sSql += sWhere;

			sSql += ") " +
					"WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql += sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd))
				using (ds = new DataSet()) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pPartnerSiteSeq,string pRegistDay,ref string pWhere) {
		ArrayList list = new ArrayList();

		if (!pPartnerSiteSeq.Equals("")) {
			SysPrograms.SqlAppendWhere("PARTNER_SITE_SEQ = :PARTNER_SITE_SEQ ",ref pWhere);
			list.Add(new OracleParameter("PARTNER_SITE_SEQ",pPartnerSiteSeq));
		}
		SysPrograms.SqlAppendWhere("(REGIST_USER_END_DAY >= :REGIST_USER_END_DAY)",ref pWhere);
		list.Add(new OracleParameter("REGIST_USER_END_DAY",pRegistDay));

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

}
