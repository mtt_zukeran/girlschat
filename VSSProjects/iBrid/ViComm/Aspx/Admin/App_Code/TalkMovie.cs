﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 会話動画
--	Progaram ID		: TalkMovie
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater			Update Explain
  2010/06/29	伊藤和明		検索項目に日付を追加
  2010/07/16	Koyanagi		検索項目に非公開ﾌﾗｸﾞを追加

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class TalkMovie:DbSession {


	public TalkMovie() {
	}



	public int GetApproveManagePageCount(string pSiteCd,string pNotApproveFlag) {
		return GetApproveManagePageCount(pSiteCd,pNotApproveFlag,null,null,null);
	}
	public int GetApproveManagePageCount(string pSiteCd,string pNotApproveFlag,string pNotPublishFlag,string pUploadDateFrom,string pUploadDateTo) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_TALK_MOVIE01 ";
			string sWhere = "";
			OracleParameter[] objParms = CreateApproveManageWhere(pSiteCd,pNotApproveFlag,pNotPublishFlag,pUploadDateFrom,pUploadDateTo,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetApproveManagePageCollection(string pSiteCd,string pNotApproveFlag,int startRowIndex,int maximumRows) {
		return GetApproveManagePageCollection(pSiteCd,pNotApproveFlag,null,null,null,startRowIndex,maximumRows);
	}
	public DataSet GetApproveManagePageCollection(string pSiteCd,string pNotApproveFlag,string pNotPublishFlag,string pUploadDateFrom,string pUploadDateTo,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY SITE_CD,MOVIE_SEQ DESC ";

			string sSql = "SELECT " +
								"SITE_CD		," +
								"SITE_NM		," +
								"USER_SEQ		," +
								"USER_CHAR_NO	," +
								"LOGIN_ID		," +
								"CAST_NM		," +
								"IVP_FILE_NM	," +
								"MOVIE_TITLE	," +
								"RECORDING_SEC	," +
								"PLAY_FILE_NM	," +
								"HANDLE_NM		," +
								"RECORDING_SEC	," +
								"UPLOAD_DATE	," +
								"MOVIE_TITLE	," +
								"MOVIE_SEQ		," +
								"OBJ_NOT_PUBLISH_FLAG	," +
								"NOT_APPROVE_MARK_ADMIN	" +
							"FROM(" +
							" SELECT VW_TALK_MOVIE01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_TALK_MOVIE01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateApproveManageWhere(pSiteCd,pNotApproveFlag,pNotPublishFlag,pUploadDateFrom,pUploadDateTo,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_TALK_MOVIE01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateApproveManageWhere(string pSiteCd,string pNotApproveFlag,string pNotPublishFlag,string pUploadDateFrom,string pUploadDateTo,ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();

		if (pSiteCd.Equals("") == false) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if ((!string.IsNullOrEmpty(pNotApproveFlag)) || (string.IsNullOrEmpty(pNotPublishFlag))) {
			if (pNotApproveFlag.Equals(ViCommConst.FLAG_ON.ToString()) & pNotPublishFlag.Equals(ViCommConst.FLAG_ON.ToString())) {
				SysPrograms.SqlAppendWhere("(OBJ_NOT_APPROVE_FLAG = :OBJ_NOT_APPROVE_FLAG OR OBJ_NOT_PUBLISH_FLAG = :OBJ_NOT_PUBLISH_FLAG)",ref pWhere);
				list.Add(new OracleParameter("OBJ_NOT_APPROVE_FLAG",pNotApproveFlag));
				list.Add(new OracleParameter("OBJ_NOT_PUBLISH_FLAG",pNotPublishFlag));
			} else {
				if (pNotApproveFlag.Equals("") == false) {
					SysPrograms.SqlAppendWhere("OBJ_NOT_APPROVE_FLAG = :OBJ_NOT_APPROVE_FLAG",ref pWhere);
					list.Add(new OracleParameter("OBJ_NOT_APPROVE_FLAG",pNotApproveFlag));
				}

				// 非公開フラグ
				if (!string.IsNullOrEmpty(pNotPublishFlag)) {
					SysPrograms.SqlAppendWhere("OBJ_NOT_PUBLISH_FLAG = :OBJ_NOT_PUBLISH_FLAG",ref pWhere);
					list.Add(new OracleParameter("OBJ_NOT_PUBLISH_FLAG",pNotPublishFlag));
				}
			}
		}

		// アップロード日時(from)
		if (!string.IsNullOrEmpty(pUploadDateFrom)) {
			SysPrograms.SqlAppendWhere("UPLOAD_DATE_RAW >= :UPLOAD_DATE_FROM", ref pWhere);
			DateTime dtFrom = DateTime.ParseExact(pUploadDateFrom, "yyyy/MM/dd", null, System.Globalization.DateTimeStyles.None).Date;
			list.Add(new OracleParameter("UPLOAD_DATE_FROM", OracleDbType.Date, dtFrom, ParameterDirection.Input));
		}

		// アップロード日時(to)
		if (!string.IsNullOrEmpty(pUploadDateTo)) {
			SysPrograms.SqlAppendWhere("UPLOAD_DATE_RAW < :UPLOAD_DATE_TO", ref pWhere);
			DateTime dtFrom = DateTime.ParseExact(pUploadDateTo, "yyyy/MM/dd", null, System.Globalization.DateTimeStyles.None).Date.AddDays(1);
			list.Add(new OracleParameter("UPLOAD_DATE_TO", OracleDbType.Date, dtFrom, ParameterDirection.Input));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public int GetPageCount(string pSiteCd,string pUserSeq,string pUserCharNo) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_TALK_MOVIE01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}


	public DataSet GetPageCollection(string pSiteCd,string pUserSeq,string pUserCharNo,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY SITE_CD,USER_SEQ,USER_CHAR_NO,MOVIE_SEQ DESC ";
			string sSql = "SELECT " +
								"SITE_CD				," +
								"USER_SEQ				," +
								"USER_CHAR_NO			," +
								"MOVIE_SEQ				," +
								"IVP_FILE_NM			," +
								"MOVIE_TITLE			," +
								"CHARGE_POINT			," +
								"RECORDING_SEC			," +
								"PLAY_FILE_NM			," +
								"OBJ_NOT_APPROVE_FLAG	," +
								"NOT_APPROVE_MARK		," +
								"UPLOAD_DATE " +
							"FROM(" +
							" SELECT VW_TALK_MOVIE01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_TALK_MOVIE01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_TALK_MOVIE01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,string pUserSeq,string pUserCharNo,ref string pWhere) {
		ArrayList list = new ArrayList();

		if (!pSiteCd.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!pUserSeq.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhere);
			list.Add(new OracleParameter("USER_SEQ",pUserSeq));
			if (!pUserCharNo.Equals("")) {
				iBridCommLib.SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhere);
				list.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));
			}
		}
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

}
