﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 仮登録
--	Progaram ID		: TempRegist
--
--  Creation Date	: 2009.12.14
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;
using iBridCommLib;
using ViComm;

public class TempRegist : DbSession {
    public TempRegist() {
    }

    public int GetPageCount(string pSiteCd,string pTel,string pEmailAddr,string pCastNm,string pApplyDayFrom,string pApplyDayTo,string pApplyTimeFrom,string pApplyTimeTo) {
        DataSet ds;
        DataRow dr;
        int iPageCount = 0;
        StringBuilder sSql = new StringBuilder();
        try {
            conn = DbConnect();

            sSql.Append("SELECT COUNT(*) AS ROW_COUNT FROM VW_TEMP_REGIST02 ");

            string sWhere = "";
            OracleParameter[] objParms = CreateWhere(pSiteCd,pTel,pEmailAddr,pCastNm,pApplyDayFrom,pApplyDayTo,pApplyTimeFrom,pApplyTimeTo,ref sWhere);
            sSql.Append(sWhere);

            using (cmd = CreateSelectCommand(sSql.ToString(),conn))
            using (da = new OracleDataAdapter(cmd))
            using (ds = new DataSet()) {
                for (int i = 0; i < objParms.Length; i++) {
                    cmd.Parameters.Add((OracleParameter)objParms[i]);
                }
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count != 0) {
                    dr = ds.Tables[0].Rows[0];
                    iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
                }
            }
        } finally {
            conn.Close();
        }
        return iPageCount;
    }


    public DataSet GetPageCollection(string pSiteCd,string pTel,string pEmailAddr,string pCastNm,string pApplyDayFrom,string pApplyDayTo,string pApplyTimeFrom,string pApplyTimeTo,int startRowIndex,int maximumRows) {
        DataSet ds;
        try {
            conn = DbConnect();
            ds = new DataSet();
            StringBuilder sSql = new StringBuilder();
            StringBuilder sOrder = new StringBuilder();

            sOrder.Append(" ORDER BY REGIST_APPLY_DATE DESC ");
            sSql.Append("SELECT ");
            sSql.Append("	TEMP_REGIST_ID		,");
            sSql.Append("	EMAIL_ADDR			,");
            sSql.Append("	SITE_CD				,");
            sSql.Append("	AD_CD				,");
            sSql.Append("	FRIEND_LOGIN_ID		,");
            sSql.Append("	REGIST_APPLY_DATE	,");
            sSql.Append("	TEL					,");
            sSql.Append("	BIRTHDAY			,");
            sSql.Append("	CAST_NM				,");
            sSql.Append("	CAST_KANA_NM		,");
            sSql.Append("	REGIST_AFFILIATE_CD	,");
            sSql.Append("	ID_PIC_SEQ1			,");
            sSql.Append("	ID_PIC_SEQ2			,");
			sSql.Append("	AFFILIATER_NM		,");
            sSql.Append("	NEED_FLAG			,");
			sSql.Append("	SIGNUP_FLAG			,");
            sSql.Append("	TRACKING_URL  		,");
            sSql.Append("	TRACKING_ADDITION_INFO ");
            sSql.Append("FROM(");
            sSql.Append("	SELECT VW_TEMP_REGIST02.*, ROW_NUMBER() OVER (" + sOrder.ToString() + ") AS RNUM FROM VW_TEMP_REGIST02 ");

            string sWhere = "";
            OracleParameter[] objParms = CreateWhere(pSiteCd,pTel,pEmailAddr,pCastNm,pApplyDayFrom,pApplyDayTo,pApplyTimeFrom,pApplyTimeTo,ref sWhere);
            sSql.Append(sWhere);

            sSql.Append(" )WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ");
            sSql.Append(sOrder.ToString());

            using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
                for (int i = 0; i < objParms.Length; i++) {
                    cmd.Parameters.Add((OracleParameter)objParms[i]);
                }

                cmd.Parameters.Add("FIRST_ROW",startRowIndex);
                cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

                using (da = new OracleDataAdapter(cmd)) {
                    da.Fill(ds,"VW_TEMP_REGIST02");
                }
            }
        } finally {
            conn.Close();
        }
        return ds;
    }

    /// <summary>
    /// CSV出力用に「VW_TEMP_REGIST02」からデータを取得
    /// </summary>
    /// <param name="pSiteCd"></param>
    /// <param name="pTel"></param>
    /// <param name="pEmailAddr"></param>
    /// <param name="pCastNm"></param>
    /// <param name="pApplyDayFrom"></param>
    /// <param name="pApplyDayTo"></param>
    /// <param name="pApplyTimeFrom"></param>
    /// <param name="pApplyTimeTo"></param>
    /// <returns></returns>
    public DataSet GetCSVList(string pSiteCd, string pTel, string pEmailAddr, string pCastNm, string pApplyDayFrom, string pApplyDayTo, string pApplyTimeFrom, string pApplyTimeTo)
    {
        StringBuilder sbSql = new StringBuilder();
        StringBuilder sbOrder = new StringBuilder();

        sbOrder.Append(" ORDER BY REGIST_APPLY_DATE DESC ");
        sbSql.Append("SELECT ");
        sbSql.Append("	TEMP_REGIST_ID		,");
        sbSql.Append("	EMAIL_ADDR			,");
        sbSql.Append("	SITE_CD				,");
        sbSql.Append("	AD_CD				,");
        sbSql.Append("	FRIEND_LOGIN_ID		,");
        sbSql.Append("	REGIST_APPLY_DATE	,");
        sbSql.Append("	TEL					,");
        sbSql.Append("	BIRTHDAY			,");
        sbSql.Append("	CAST_NM				,");
        sbSql.Append("	CAST_KANA_NM		,");
        sbSql.Append("	REGIST_AFFILIATE_CD	,");
        sbSql.Append("	ID_PIC_SEQ1			,");
        sbSql.Append("	ID_PIC_SEQ2			,");
        sbSql.Append("	AFFILIATER_NM		,");
        sbSql.Append("	NEED_FLAG			,");
        sbSql.Append("	SIGNUP_FLAG			,");
        sbSql.Append("	TRACKING_URL  		,");
        sbSql.Append("	TRACKING_ADDITION_INFO ");
        sbSql.Append("FROM");
        sbSql.Append("	VW_TEMP_REGIST02 ");

        string sWhere = "";
        OracleParameter[] objParms = CreateWhere(pSiteCd, pTel, pEmailAddr, pCastNm, pApplyDayFrom, pApplyDayTo, pApplyTimeFrom, pApplyTimeTo, ref sWhere);
        sbSql.Append(sWhere);
        sbSql.Append(sbOrder.ToString());

        return ExecuteSelectQueryBase(sbSql.ToString(), objParms);
    }

    public DataSet GetOne(string pTempRegistId) {
        DataSet ds;
        try {
            conn = DbConnect();
            ds = new DataSet();

            StringBuilder sSql = new StringBuilder();
            sSql.Append("SELECT ");
            sSql.Append("	*  ");
            sSql.Append("FROM ");
            sSql.Append("	VW_TEMP_REGIST02 ");
            sSql.Append("WHERE ");
            sSql.Append("	TEMP_REGIST_ID = :TEMP_REGIST_ID ");

            using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
                cmd.Parameters.Add("TEMP_REGIST_ID",pTempRegistId);
                using (da = new OracleDataAdapter(cmd)) {
                    da.Fill(ds,"VW_TEMP_REGIST02");
                }
            }
        } finally {
            conn.Close();
        }
        return ds;
    }

    private OracleParameter[] CreateWhere(string pSiteCd,string pTel,string pEmailAddr,string pCastNm,string pApplyDayFrom,string pApplyDayTo,string pApplyTimeFrom,string pApplyTimeTo,ref string pWhere) {
        ArrayList list = new ArrayList();

        iBridCommLib.SysPrograms.SqlAppendWhere("REGIST_STATUS = :REGIST_STATUS",ref pWhere);
        list.Add(new OracleParameter("REGIST_STATUS","1"));

        iBridCommLib.SysPrograms.SqlAppendWhere("SEX_CD = :SEX_CD",ref pWhere);
        list.Add(new OracleParameter("SEX_CD",ViCommConst.OPERATOR));

        if (!pSiteCd.Equals("")) {
            iBridCommLib.SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
            list.Add(new OracleParameter("SITE_CD",pSiteCd));
        }
        if (!pTel.Equals("")) {
            iBridCommLib.SysPrograms.SqlAppendWhere("TEL LIKE :TEL||'%'",ref pWhere);
            list.Add(new OracleParameter("TEL",pTel));
        }
        if (!pEmailAddr.Equals("")) {
            iBridCommLib.SysPrograms.SqlAppendWhere("EMAIL_ADDR LIKE '%'||:EMAIL_ADDR||'%'",ref pWhere);
            list.Add(new OracleParameter("EMAIL_ADDR",pEmailAddr.ToLower()));
        }
        if (!pCastNm.Equals("")) {
            iBridCommLib.SysPrograms.SqlAppendWhere("CAST_NM LIKE '%'||:CAST_NM||'%'",ref pWhere);
            list.Add(new OracleParameter("CAST_NM",pCastNm));
        }
        if (!pApplyDayFrom.Equals("")) {
            DateTime dtFrom = DateTime.Parse(pApplyDayFrom + " " + pApplyTimeFrom + ":00:00");
            DateTime dtTo = DateTime.Parse(pApplyDayTo + " " + pApplyTimeTo + ":59:59").AddSeconds(1);
            SysPrograms.SqlAppendWhere("(REGIST_APPLY_DATE >= :REGIST_APPLY_DATE_FROM AND REGIST_APPLY_DATE < :REGIST_APPLY_DATE_TO)",ref pWhere);
            list.Add(new OracleParameter("REGIST_APPLY_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
            list.Add(new OracleParameter("REGIST_APPLY_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
        }

        return (OracleParameter[])list.ToArray(typeof(OracleParameter));
    }

    public DataSet GetOneIdPicSeq(string pTempRegistId) {
        StringBuilder oSql = new StringBuilder();
        oSql.AppendLine("SELECT	");
        oSql.AppendLine("	ID_PIC_SEQ1		,");
        oSql.AppendLine("	ID_PIC_SEQ2		,");
        oSql.AppendLine("	ID_PIC_SEQ3		,");
        oSql.AppendLine("	ID_PIC_SEQ4		,");
        oSql.AppendLine("	ID_PIC_SEQ5		,");
        oSql.AppendLine("	ID_PIC_SEQ6		,");
        oSql.AppendLine("	ID_PIC_SEQ7		,");
        oSql.AppendLine("	ID_PIC_SEQ8		,");
        oSql.AppendLine("	ID_PIC_SEQ9		,");
        oSql.AppendLine("	ID_PIC_SEQ10	");
        oSql.AppendLine("FROM	");
        oSql.AppendLine("	T_TEMP_REGIST	");
        oSql.AppendLine("WHERE ");
        oSql.AppendLine("	TEMP_REGIST_ID = :TEMP_REGIST_ID ");

        DataSet oDataSet = new DataSet();
        try {
            conn = DbConnect();
            using (cmd = CreateSelectCommand(oSql.ToString(),conn)) {
                cmd.BindByName = true;
                cmd.Parameters.Add("TEMP_REGIST_ID",pTempRegistId);

                using (da = new OracleDataAdapter(cmd)) {
                    da.Fill(oDataSet);
                }
                return oDataSet;
            }
        } finally {
            conn.Close();
        }
    }

}