/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: エラーメッセージ管理
--	Progaram ID		: Error
--
--  Creation Date	: 2010.03.15
--  Creater			: iBrid(M.Horie)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class Error:DbSession {

	public Error() {
	}

	public int GetPageCount(string pSiteCd) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_ERROR ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY ERROR_CD";
			string sSql = "SELECT " +
							"ERROR_CD		," +
							"ERROR_DTL		," +
							"UPDATE_DATE	," +
							"REVISION_NO	" +
						  "FROM(" +
							" SELECT T_ERROR.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM T_ERROR ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_ERROR");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,ref string pWhere) {
		ArrayList list = new ArrayList();
		pWhere = pWhere + " WHERE SITE_CD = :SITE_CD ";
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public string GetErrorDtl(string pSiteCd,string pErrorCd) {
		DataSet ds;
		DataRow dr;
		string sErrorDtl = "";
		try {
			conn = DbConnect("Error.GetErrorDtl");

			using (cmd = CreateSelectCommand("SELECT * FROM T_ERROR WHERE SITE_CD = :SITE_CD AND ERROR_CD =:ERROR_CD",conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("ERROR_CD",pErrorCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_ERROR");
					if (ds.Tables["T_ERROR"].Rows.Count != 0) {
						dr = ds.Tables["T_ERROR"].Rows[0];
						sErrorDtl = dr["ERROR_DTL"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
		return sErrorDtl;
	}
}
