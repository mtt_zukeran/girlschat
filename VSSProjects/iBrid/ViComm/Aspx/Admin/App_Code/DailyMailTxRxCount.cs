﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 日別メールやりとり数集計

--	Progaram ID		: DailyMailTxRxCount
--
--  Creation Date	: 2016.11.14
--  Creater			: M&TT Zukeran
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using System;
using System.Data;
using System.Text;
using System.Collections.Generic;
using ViComm;

public class DailyMailTxRxCount:DbSession {
	/// <summary>コンストラクタ</summary>
	public DailyMailTxRxCount() {
	}

	/// <summary>
	/// 一覧のデータ取得
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pYYYY"></param>
	/// <param name="pMM"></param>
	/// <param name="pSexCd"></param>
	/// <returns></returns>
	public DataSet GetList(string pSiteCd,string pYYYY,string pMM,string pSexCd) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();
		string sReportDate = string.Format("{0}-{1}-01",pYYYY,pMM);
		DateTime dt = DateTime.Parse(sReportDate).AddMonths(1);

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	T_SUM.*");
		// 男性の返信率
		oSqlBuilder.AppendLine("	,CASE WHEN T_SUM.MAN_TX_RETURN_TOTAL_CNT = 0 OR T_SUM.MAN_RX_TOTAL_CNT = 0");
		oSqlBuilder.AppendLine("		THEN 0 ELSE ROUND(T_SUM.MAN_TX_RETURN_TOTAL_CNT / T_SUM.MAN_RX_TOTAL_CNT, 3) * 100 END AS MAN_RETURN_RATE");
		// 女性の返信率
		oSqlBuilder.AppendLine("	,CASE WHEN T_SUM.WOMAN_TX_RETURN_TOTAL_CNT = 0 OR T_SUM.WOMAN_RX_TOTAL_CNT = 0");
		oSqlBuilder.AppendLine("		THEN 0 ELSE ROUND(T_SUM.WOMAN_TX_RETURN_TOTAL_CNT / T_SUM.WOMAN_RX_TOTAL_CNT, 3) * 100 END AS WOMAN_RETURN_RATE");
		oSqlBuilder.AppendLine("FROM (");
		oSqlBuilder.AppendLine("	SELECT");
		oSqlBuilder.AppendLine("		REPORT_DATE");
		// 男性側の集計数
		oSqlBuilder.AppendLine("		,MAN_TX_UNIQ_USER_CNT");
		oSqlBuilder.AppendLine("		,MAN_TX_NORMAL_CNT");
		oSqlBuilder.AppendLine("		,MAN_TX_PIC_CNT");
		oSqlBuilder.AppendLine("		,MAN_TX_MOVIE_CNT");
		oSqlBuilder.AppendLine("		,MAN_TX_RETURN_UNIQ_USER_CNT");
		oSqlBuilder.AppendLine("		,MAN_TX_RETURN_NORMAL_CNT");
		oSqlBuilder.AppendLine("		,MAN_TX_RETURN_PIC_CNT");
		oSqlBuilder.AppendLine("		,MAN_TX_RETURN_MOVIE_CNT");
		oSqlBuilder.AppendLine("		,(MAN_TX_RETURN_NORMAL_CNT + MAN_TX_RETURN_PIC_CNT + MAN_TX_RETURN_MOVIE_CNT) AS MAN_TX_RETURN_TOTAL_CNT");
		oSqlBuilder.AppendLine("		,MAN_TX_TOTAL_UNIQ_USER_CNT");
		oSqlBuilder.AppendLine("		,(MAN_TX_NORMAL_CNT + MAN_TX_RETURN_NORMAL_CNT) AS MAN_TX_TOTAL_NORMAL_CNT");
		oSqlBuilder.AppendLine("		,(MAN_TX_PIC_CNT + MAN_TX_RETURN_PIC_CNT) AS MAN_TX_TOTAL_PIC_CNT");
		oSqlBuilder.AppendLine("		,(MAN_TX_MOVIE_CNT + MAN_TX_RETURN_MOVIE_CNT) AS MAN_TX_TOTAL_MOVIE_CNT");
		oSqlBuilder.AppendLine("		,MAN_RX_UNIQ_USER_CNT");
		oSqlBuilder.AppendLine("		,MAN_RX_BATCH_CNT");
		oSqlBuilder.AppendLine("		,MAN_RX_CNT");
		oSqlBuilder.AppendLine("		,MAN_RX_RETURN_CNT");
		oSqlBuilder.AppendLine("		,(MAN_RX_BATCH_CNT + MAN_RX_CNT + MAN_RX_RETURN_CNT) AS MAN_RX_TOTAL_CNT");
		oSqlBuilder.AppendLine("		,MAN_TERM_TX_UU_CNT");
		oSqlBuilder.AppendLine("		,MAN_TERM_TX_RETURN_UU_CNT");
		oSqlBuilder.AppendLine("		,MAN_TERM_TX_TOTAL_UU_CNT");
		oSqlBuilder.AppendLine("		,MAN_TERM_RX_UU_CNT");
		// 女性側の集計数
		oSqlBuilder.AppendLine("		,WOMAN_TX_UNIQ_USER_CNT");
		oSqlBuilder.AppendLine("		,WOMAN_TX_BATCH_CNT");
		oSqlBuilder.AppendLine("		,WOMAN_TX_NORMAL_CNT");
		oSqlBuilder.AppendLine("		,WOMAN_TX_PIC_CNT");
		oSqlBuilder.AppendLine("		,WOMAN_TX_MOVIE_CNT");
		oSqlBuilder.AppendLine("		,WOMAN_TX_RETURN_UNIQ_USER_CNT");
		oSqlBuilder.AppendLine("		,WOMAN_TX_RETURN_NORMAL_CNT");
		oSqlBuilder.AppendLine("		,WOMAN_TX_RETURN_PIC_CNT");
		oSqlBuilder.AppendLine("		,WOMAN_TX_RETURN_MOVIE_CNT");
		oSqlBuilder.AppendLine("		,(WOMAN_TX_RETURN_NORMAL_CNT + WOMAN_TX_RETURN_PIC_CNT + WOMAN_TX_RETURN_MOVIE_CNT) AS WOMAN_TX_RETURN_TOTAL_CNT");
		oSqlBuilder.AppendLine("		,WOMAN_TX_TOTAL_UNIQ_USER_CNT");
		oSqlBuilder.AppendLine("		,(WOMAN_TX_BATCH_CNT + WOMAN_TX_NORMAL_CNT + WOMAN_TX_RETURN_NORMAL_CNT) AS WOMAN_TX_TOTAL_NORMAL_CNT");
		oSqlBuilder.AppendLine("		,(WOMAN_TX_PIC_CNT + WOMAN_TX_RETURN_PIC_CNT) AS WOMAN_TX_TOTAL_PIC_CNT");
		oSqlBuilder.AppendLine("		,(WOMAN_TX_MOVIE_CNT + WOMAN_TX_RETURN_MOVIE_CNT) AS WOMAN_TX_TOTAL_MOVIE_CNT");
		oSqlBuilder.AppendLine("		,WOMAN_RX_UNIQ_USER_CNT");
		oSqlBuilder.AppendLine("		,WOMAN_RX_CNT");
		oSqlBuilder.AppendLine("		,WOMAN_RX_RETURN_CNT");
		oSqlBuilder.AppendLine("		,(WOMAN_RX_CNT + WOMAN_RX_RETURN_CNT) AS WOMAN_RX_TOTAL_CNT");
		oSqlBuilder.AppendLine("		,WOMAN_TERM_TX_UU_CNT");
		oSqlBuilder.AppendLine("		,WOMAN_TERM_TX_RETURN_UU_CNT");
		oSqlBuilder.AppendLine("		,WOMAN_TERM_TX_TOTAL_UU_CNT");
		oSqlBuilder.AppendLine("		,WOMAN_TERM_RX_UU_CNT");
		oSqlBuilder.AppendLine("	FROM");
		oSqlBuilder.AppendLine("		T_DAILY_MAIL_TX_RX_CNT");
		oSqlBuilder.AppendLine("	WHERE");
		oSqlBuilder.AppendLine("		SITE_CD = :SITE_CD");
		oSqlBuilder.AppendLine("		AND	REPORT_DATE >= TO_DATE(:REPORT_DATE_FROM,'YYYY-MM-DD')");
		oSqlBuilder.AppendLine("		AND	REPORT_DATE < TO_DATE(:REPORT_DATE_TO,'YYYY-MM-DD')");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":REPORT_DATE_FROM",sReportDate));
		oParamList.Add(new OracleParameter(":REPORT_DATE_TO",dt.ToString("yyyy-MM-dd")));

		oSqlBuilder.AppendLine("	ORDER BY");
		oSqlBuilder.AppendLine("		REPORT_DATE");
		oSqlBuilder.AppendLine(") T_SUM");

		return ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
	}
}
