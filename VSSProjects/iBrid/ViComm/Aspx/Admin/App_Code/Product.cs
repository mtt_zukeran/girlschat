﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 商品

--	Progaram ID		: Product
--
--  Creation Date	: 2010.12.06
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class Product : DbSession {

	#region □■□ inner class(SearchCondition) □■□ ================================================================
	public class SearchCondition {
		private string siteCd;
		public string SiteCd { get { return this.siteCd; } set { this.siteCd = value; } }
		private string productAgentCd;
		public string ProductAgentCd { get { return this.productAgentCd; } set { this.productAgentCd = value; } }
		private string code;
		public string Code { get { return this.code; } set { this.code = value; } }
		private string productName;
		public string ProductName { get { return this.productName; } set { this.productName = value; } }
		private string productSeries;
		public string ProductSeries { get { return this.productSeries; } set { this.productSeries = value; } }
		private string cast;
		public string Cast { get { return this.cast; } set { this.cast = value; } }
		private string productId;
		public string ProductId { get { return this.productId; } set { this.productId = value; } }
		private string partNumber;
		public string PartNumber { get { return this.partNumber; } set { this.partNumber = value; } }
		private string linkId;
		public string LinkId { get { return this.linkId; } set { this.linkId = value; } }
		private string productMaker;
		public string ProductMaker { get { return this.productMaker; } set { this.productMaker = value; } }
		private string publishStartDateFrom;
		public string PublishStartDateFrom { get { return this.publishStartDateFrom; } set { this.publishStartDateFrom = value; } }
		private string publishStartDateTo;
		public string PublishStartDateTo { get { return this.publishStartDateTo; } set { this.publishStartDateTo = value; } }
		private string publishEndDateFrom;
		public string PublishEndDateFrom { get { return this.publishEndDateFrom; } set { this.publishEndDateFrom = value; } }
		private string publishEndDateTo;
		public string PublishEndDateTo { get { return this.publishEndDateTo; } set { this.publishEndDateTo = value; } }
		private string specialCharePointFrom;
		public string SpecialChargePointFrom { get { return this.specialCharePointFrom; } set { this.specialCharePointFrom = value; } }
		private string specialChargePointTo;
		public string SpecialChargePointTo { get { return this.specialChargePointTo; } set { this.specialChargePointTo = value; } }
		private string grossPriceFrom;
		public string GrossPriceFrom { get { return this.grossPriceFrom; } set { this.grossPriceFrom = value; } }
		private string grossPriceTo;
		public string GrossPriceTo { get { return this.grossPriceTo; } set { this.grossPriceTo = value; } }
		private string chargePointFrom;
		public string ChargePointFrom { get { return this.chargePointFrom; } set { this.chargePointFrom = value; } }
		private string chargePointTo;
		public string ChargePointTo { get { return this.chargePointTo; } set { this.chargePointTo = value; } }
		private string releaseDateFrom;
		public string ReleaseDateFrom { get { return this.releaseDateFrom; } set { this.releaseDateFrom = value; } }
		private string releaseDateTo;
		public string ReleaseDateTo { get { return this.releaseDateTo; } set { this.releaseDateTo = value; } }
		private string publishFlag;
		public string PublishFlag { get { return this.publishFlag; } set { this.publishFlag = value; } }
		private bool delFlag;
		public bool DelFlag { get { return this.delFlag; } set { this.delFlag = value; } }
		private string handleName;
		public string HandleName { get { return this.handleName; } set { this.handleName = value; } }
		private string productKeyword;
		public string ProductKeyword { get { return this.productKeyword; } set { this.productKeyword = value; } }
		private List<string> genreList;
		public List<string> GenreList {
			get {
				if (this.genreList == null) {
					this.genreList = new List<string>();
				}
				return this.genreList;
			}
			set { this.genreList = value; }
		}
		private string pickupId;
		public string PickupId { get { return this.pickupId; } set { this.pickupId = value; } }
		private string royaltyRate;
		public string RoyaltyRate { get { return this.royaltyRate; } set { this.royaltyRate = value; } }
		private string loginId;
		public string LoginId { get { return this.loginId; } set { this.loginId = value; } }
		private string notNewFlag;
		public string NotNewFlag { get { return this.notNewFlag; } set { this.notNewFlag = value; } }
		private string buyCountFrom;
		public string BuyCountFrom { get { return this.buyCountFrom; } set { this.buyCountFrom = value; } }
		private string buyCountTo;
		public string BuyCountTo { get { return this.buyCountTo; } set { this.buyCountTo = value; } }
		private string favoriteMeCountFrom;
		public string FavoriteMeCountFrom { get { return this.favoriteMeCountFrom; } set { this.favoriteMeCountFrom = value; } }
		private string favoriteMeCountTo;
		public string FavoriteMeCountTo { get { return this.favoriteMeCountTo; } set { this.favoriteMeCountTo = value; } }
		private bool noGenreFlag;
		public bool NoGenreFlag {	get { return this.noGenreFlag; } set { this.noGenreFlag = value; } }
		private string auctionStatus;
		public string AuctionStatus { get { return this.auctionStatus; } set { this.auctionStatus = value; } }
		private string auctionStartDateFrom;
		public string AuctionStartDateFrom { get { return this.auctionStartDateFrom; } set { this.auctionStartDateFrom = value; } }
		private string auctionStartDateTo;
		public string AuctionStartDateTo { get { return this.auctionStartDateTo; } set { this.auctionStartDateTo = value; } }
		private string blindEndDateFrom;
		public string BlindEndDateFrom { get { return this.blindEndDateFrom; } set { this.blindEndDateFrom = value; } }
		private string auctionEndDateFrom;
		private string blindEndDateTo;
		public string BlindEndDateTo { get { return this.blindEndDateTo; } set { this.blindEndDateTo = value; } }
		public string AuctionEndDateFrom { get { return this.auctionEndDateFrom; } set { this.auctionEndDateFrom = value; } }
		private string auctionEndDateTo;
		public string AuctionEndDateTo { get { return this.auctionEndDateTo; } set { this.auctionEndDateTo = value; } }
		private string auctionStartHourFrom;
		public string AuctionStartHourFrom { get { return this.auctionStartHourFrom; } set { this.auctionStartHourFrom = value; } }
		private string auctionStartHourTo;
		public string AuctionStartHourTo { get { return this.auctionStartHourTo; } set { this.auctionStartHourTo = value; } }
		private string blindEndHourFrom;
		public string BlindEndHourFrom { get { return this.blindEndHourFrom; } set { this.blindEndHourFrom = value; } }
		private string auctionEndHourFrom;
		private string blindEndHourTo;
		public string BlindEndHourTo { get { return this.blindEndHourTo; } set { this.blindEndHourTo = value; } }
		public string AuctionEndHourFrom { get { return this.auctionEndHourFrom; } set { this.auctionEndHourFrom = value; } }
		private string auctionEndHourTo;
		public string AuctionEndHourTo { get { return this.auctionEndHourTo; } set { this.auctionEndHourTo = value; } }
		private string reserveAmtFrom;
		public string ReserveAmtFrom { get { return this.reserveAmtFrom; } set { this.reserveAmtFrom = value; } }
		private string reserveAmtTo;
		public string ReserveAmtTo { get { return this.reserveAmtTo; } set { this.reserveAmtTo = value; } }
		private string minimumBidAmtFrom;
		public string MinimumBidAmtFrom { get { return this.minimumBidAmtFrom; } set { this.minimumBidAmtFrom = value; } }
		private string minimumBidAmtTo;
		public string MinimumBidAmtTo { get { return this.minimumBidAmtTo; } set { this.minimumBidAmtTo = value; } }
		private string entryPointFrom;
		public string EntryPointFrom { get { return this.entryPointFrom; } set { this.entryPointFrom = value; } }
		private string entryPointTo;
		public string EntryPointTo { get { return this.entryPointTo; } set { this.entryPointTo = value; } }
		private string blindEntryPointFrom;
		public string BlindEntryPointFrom { get { return this.blindEntryPointFrom; } set { this.blindEntryPointFrom = value; } }
		private string blindEntryPointTo;
		public string BlindEntryPointTo { get { return this.blindEntryPointTo; } set { this.blindEntryPointTo = value; } }
		private string bidPointFrom;
		public string BidPointFrom { get { return this.bidPointFrom; } set { this.bidPointFrom = value; } }
		private string bidPointTo;
		public string BidPointTo { get { return this.bidPointTo; } set { this.bidPointTo = value; } }
		private string blindBidPointFrom;
		public string BlindBidPointFrom { get { return this.blindBidPointFrom; } set { this.blindBidPointFrom = value; } }
		private string blindBidPointTo;
		public string BlindBidPointTo { get { return this.blindBidPointTo; } set { this.blindBidPointTo = value; } }
		private string autoExtensionFlag;
		public string AutoExtensionFlag { get { return this.autoExtensionFlag; } set { this.autoExtensionFlag = value; } }

	}
	#endregion ========================================================================================================

	public Product() { }

	public int GetPageCount(object pSearchCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT				").AppendLine();
		oSqlBuilder.Append("	COUNT(*)		").AppendLine();
		oSqlBuilder.Append(" FROM				").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01	").AppendLine();
		if (ViCommConst.ProductType.IsAuction((pSearchCondition as SearchCondition).Code)) {
			oSqlBuilder.Append("	,T_CODE_DTL		").AppendLine();
			oSqlBuilder.Append("	,T_PRODUCT_AUCTION	").AppendLine();
		}

		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = CreateWhere((Product.SearchCondition)pSearchCondition, ref sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}
	public DataSet GetPageCollection(object pSearchCondition, int startRowIndex, int maximumRows) {

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT				").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.SITE_CD					,	").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.PRODUCT_AGENT_CD         ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.PRODUCT_SEQ              ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.PRODUCT_ID               ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.PRODUCT_TYPE             ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.PRODUCT_NM               ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.PRODUCT_DISCRIPTION      ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.PART_NUMBER              ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.PRODUCT_SERIES_SUMMARY   ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.CAST_SUMMARY             ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.PRODUCT_MAKER_SUMMARY    ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.PUBLISH_END_DATE         ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.PUBLISH_START_DATE       ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.RELEASE_DATE             ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.GROSS_PRICE              ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.CHARGE_POINT             ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.SPECIAL_CHARGE_POINT     ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.PUBLISH_FLAG             ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.DEL_FLAG                 ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.REMARKS                  ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.LINK_ID                  ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.LINK_DATE                ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.UNEDITABLE_FLAG          ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.REVISION_NO              ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.PICKUP_MASK              ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.UPDATE_DATE              ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.OBJ_SEQ                  ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.PRODUCT_IMG_PATH_S       ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.LOGIN_ID                 ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.HANDLE_NM                ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.BUY_COUNT				  ,	  ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01.FAVORITE_ME_COUNT		  ,   ").AppendLine();
		if (ViCommConst.ProductType.IsAuction((pSearchCondition as SearchCondition).Code)) {
			oSqlBuilder.Append("	T_CODE_DTL.CODE_NM	AUCTION_STATUS_NM	,	").AppendLine();
			oSqlBuilder.Append("	T_PRODUCT_AUCTION.AUCTION_START_DATE	,	").AppendLine();
			oSqlBuilder.Append("	T_PRODUCT_AUCTION.BLIND_END_DATE		,	").AppendLine();
			oSqlBuilder.Append("	T_PRODUCT_AUCTION.AUCTION_END_DATE		,	").AppendLine();
			oSqlBuilder.Append("	T_PRODUCT_AUCTION.RESERVE_AMT			,	").AppendLine();
			oSqlBuilder.Append("	T_PRODUCT_AUCTION.MAX_BID_AMT			,	").AppendLine();
			oSqlBuilder.Append("	T_PRODUCT_AUCTION.BID_COUNT				,	").AppendLine();
			oSqlBuilder.Append("	T_PRODUCT_AUCTION.LAST_BID_DATE			,	").AppendLine();
			oSqlBuilder.Append("	T_PRODUCT_AUCTION.AUCTION_STATUS		,	").AppendLine();
			oSqlBuilder.Append("	T_PRODUCT_AUCTION.AUTO_EXTENSION_FLAG		").AppendLine();
		} else {
			oSqlBuilder.Append("	''	AUCTION_STATUS_NM					,	").AppendLine();
			oSqlBuilder.Append("	''	AUCTION_START_DATE					,	").AppendLine();
			oSqlBuilder.Append("	''	BLIND_END_DATE						,	").AppendLine();
			oSqlBuilder.Append("	''	AUCTION_END_DATE					,	").AppendLine();
			oSqlBuilder.Append("	''	RESERVE_AMT							,	").AppendLine();
			oSqlBuilder.Append("	''	MAX_BID_AMT							,	").AppendLine();
			oSqlBuilder.Append("	''	BID_COUNT							,	").AppendLine();
			oSqlBuilder.Append("	''	LAST_BID_DATE						,	").AppendLine();
			oSqlBuilder.Append("	''	AUCTION_STATUS						,	").AppendLine();
			oSqlBuilder.Append("	''	AUTO_EXTENSION_FLAG						").AppendLine();
		}
		oSqlBuilder.Append(" FROM				").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT01	").AppendLine();
		if (ViCommConst.ProductType.IsAuction((pSearchCondition as SearchCondition).Code)) {
			oSqlBuilder.Append("	,T_CODE_DTL		").AppendLine();
			oSqlBuilder.Append("	,T_PRODUCT_AUCTION	").AppendLine();
		}

		string sWhereClause = string.Empty;
		string sSortExpression = "ORDER BY VW_PRODUCT01.PRODUCT_ID";
		string sPagingSql = string.Empty;

		OracleParameter[] oWhereParams = CreateWhere((Product.SearchCondition)pSearchCondition, ref sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(), sSortExpression, startRowIndex, maximumRows, out sPagingSql);

		DataSet oProductDataSet = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oProductDataSet);
				}
			}
		} finally {
			conn.Close();
		}
		return oProductDataSet;
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition, ref string pWhere) {
		List<OracleParameter> oOracleParameterList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("VW_PRODUCT01.SITE_CD = :SITE_CD", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("SITE_CD", pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.ProductAgentCd)) {
			SysPrograms.SqlAppendWhere("VW_PRODUCT01.PRODUCT_AGENT_CD = :PRODUCT_AGENT_CD", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("PRODUCT_AGENT_CD", pSearchCondition.ProductAgentCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.Code)) {
			SysPrograms.SqlAppendWhere("PRODUCT_TYPE = :PRODUCT_TYPE", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("PRODUCT_TYPE", pSearchCondition.Code));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.ProductName)) {
			SysPrograms.SqlAppendWhere("PRODUCT_NM LIKE '%' || :PRODUCT_NM || '%'", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("PRODUCT_NM", pSearchCondition.ProductName));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.ProductSeries)) {
			SysPrograms.SqlAppendWhere("PRODUCT_SERIES_SUMMARY LIKE '%' || :PRODUCT_SERIES_SUMMARY || '%'", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("PRODUCT_SERIES_SUMMARY", pSearchCondition.ProductSeries));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.Cast)) {
			SysPrograms.SqlAppendWhere("CAST_SUMMARY LIKE '%' || :CAST_SUMMARY || '%'", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("CAST_SUMMARY", pSearchCondition.Cast));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.ProductId)) {
			SysPrograms.SqlAppendWhere("PRODUCT_ID LIKE '%' || :PRODUCT_ID || '%'", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("PRODUCT_ID", pSearchCondition.ProductId));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.PartNumber)) {
			SysPrograms.SqlAppendWhere("PART_NUMBER LIKE '%' || :PART_NUMBER || '%'", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("PART_NUMBER", pSearchCondition.PartNumber));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.LinkId)) {
			SysPrograms.SqlAppendWhere("LINK_ID LIKE '%' || :LINK_ID || '%'", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("LINK_ID", pSearchCondition.LinkId));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.PublishStartDateFrom)) {
			DateTime dtFrom = DateTime.Parse(pSearchCondition.PublishStartDateFrom + " " + "00:00:00");
			DateTime dtTo = DateTime.Parse(pSearchCondition.PublishStartDateTo + " " + "23:59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere("(PUBLISH_START_DATE >= :PUBLISH_START_DATE_FROM AND PUBLISH_START_DATE < :PUBLISH_START_DATE_TO)", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("PUBLISH_START_DATE_FROM", OracleDbType.Date, dtFrom, ParameterDirection.Input));
			oOracleParameterList.Add(new OracleParameter("PUBLISH_START_DATE_TO", OracleDbType.Date, dtTo, ParameterDirection.Input));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.PublishEndDateFrom)) {
			DateTime dtFrom = DateTime.Parse(pSearchCondition.PublishEndDateFrom + " " + "00:00:00");
			DateTime dtTo = DateTime.Parse(pSearchCondition.PublishEndDateTo + " " + "23:59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere("(PUBLISH_END_DATE >= :PUBLISH_END_DATE_FROM AND PUBLISH_END_DATE < :PUBLISH_END_DATE_TO)", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("PUBLISH_END_DATE_FROM", OracleDbType.Date, dtFrom, ParameterDirection.Input));
			oOracleParameterList.Add(new OracleParameter("PUBLISH_END_DATE_TO", OracleDbType.Date, dtTo, ParameterDirection.Input));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.PublishFlag)) {
			SysPrograms.SqlAppendWhere("PUBLISH_FLAG = :PUBLISH_FLAG", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("PUBLISH_FLAG", pSearchCondition.PublishFlag));
		}

		SysPrograms.SqlAppendWhere("DEL_FLAG = :DEL_FLAG", ref pWhere);
		oOracleParameterList.Add(new OracleParameter("DEL_FLAG", pSearchCondition.DelFlag ? "1" : "0"));

		if (!string.IsNullOrEmpty(pSearchCondition.ProductKeyword)) {
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_PRODUCT_KEYWORD WHERE PRODUCT_KEYWORD_VALUE LIKE '%' || :PRODUCT_KEYWORD_VALUE || '%'  AND VW_PRODUCT01.SITE_CD = SITE_CD AND VW_PRODUCT01.PRODUCT_AGENT_CD = PRODUCT_AGENT_CD AND VW_PRODUCT01.PRODUCT_SEQ = PRODUCT_SEQ )", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("PRODUCT_KEYWORD_VALUE", pSearchCondition.ProductKeyword));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.HandleName)) {
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_CAST_CHARACTER, T_PRODUCT_CAST_CHARACTER_REL WHERE HANDLE_NM LIKE '%' || :HANDLE_NM || '%' AND VW_PRODUCT01.SITE_CD = T_PRODUCT_CAST_CHARACTER_REL.SITE_CD AND VW_PRODUCT01.PRODUCT_AGENT_CD = T_PRODUCT_CAST_CHARACTER_REL.PRODUCT_AGENT_CD AND VW_PRODUCT01.PRODUCT_SEQ = T_PRODUCT_CAST_CHARACTER_REL.PRODUCT_SEQ AND T_CAST_CHARACTER.SITE_CD = T_PRODUCT_CAST_CHARACTER_REL.SITE_CD AND T_CAST_CHARACTER.USER_SEQ = T_PRODUCT_CAST_CHARACTER_REL.USER_SEQ AND T_CAST_CHARACTER.USER_CHAR_NO = T_PRODUCT_CAST_CHARACTER_REL.USER_CHAR_NO )", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("HANDLE_NM", pSearchCondition.HandleName));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.PickupId)) {
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_PRODUCT_PICKUP WHERE PICKUP_ID = :PICKUP_ID AND VW_PRODUCT01.SITE_CD = SITE_CD AND VW_PRODUCT01.PRODUCT_AGENT_CD = PRODUCT_AGENT_CD AND VW_PRODUCT01.PRODUCT_SEQ = PRODUCT_SEQ AND VW_PRODUCT01.PRODUCT_TYPE = PRODUCT_TYPE )", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("PICKUP_ID", pSearchCondition.PickupId));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.LoginId)) {
			SysPrograms.SqlAppendWhere("LOGIN_ID = :LOGIN_ID", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("LOGIN_ID", pSearchCondition.LoginId));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.NotNewFlag)) {
			SysPrograms.SqlAppendWhere("NOT_NEW_FLAG = :NOT_NEW_FLAG", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("NOT_NEW_FLAG", pSearchCondition.NotNewFlag));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.BuyCountFrom)) {
			SysPrograms.SqlAppendWhere("(BUY_COUNT >= :BUY_COUNT_FROM AND BUY_COUNT < :BUY_COUNT_TO)", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("BUY_COUNT_FROM", int.Parse(pSearchCondition.BuyCountFrom)));
			oOracleParameterList.Add(new OracleParameter("BUY_COUNT_TO", int.Parse(pSearchCondition.BuyCountTo)));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.FavoriteMeCountFrom)) {
			SysPrograms.SqlAppendWhere("(FAVORITE_ME_COUNT >= :FAVORITE_ME_COUNT_FROM AND FAVORITE_ME_COUNT < :FAVORITE_ME_COUNT_TO)", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("FAVORITE_ME_COUNT_FROM", int.Parse(pSearchCondition.FavoriteMeCountFrom)));
			oOracleParameterList.Add(new OracleParameter("FAVORITE_ME_COUNT_TO", int.Parse(pSearchCondition.FavoriteMeCountTo)));
		}

		if (ViCommConst.ProductType.IsAuction(pSearchCondition.Code)) {

			SysPrograms.SqlAppendWhere("VW_PRODUCT01.SITE_CD = T_PRODUCT_AUCTION.SITE_CD AND VW_PRODUCT01.PRODUCT_AGENT_CD = T_PRODUCT_AUCTION.PRODUCT_AGENT_CD AND VW_PRODUCT01.PRODUCT_SEQ = T_PRODUCT_AUCTION.PRODUCT_SEQ", ref pWhere);
			SysPrograms.SqlAppendWhere("T_PRODUCT_AUCTION.AUCTION_STATUS = T_CODE_DTL.CODE", ref pWhere);
			SysPrograms.SqlAppendWhere("T_CODE_DTL.CODE_TYPE = '02'", ref pWhere);

			if (!string.IsNullOrEmpty(pSearchCondition.AuctionStatus)) {
				SysPrograms.SqlAppendWhere("AUCTION_STATUS = :AUCTION_STATUS", ref pWhere);
				oOracleParameterList.Add(new OracleParameter("AUCTION_STATUS", pSearchCondition.AuctionStatus));
			}

			if (!string.IsNullOrEmpty(pSearchCondition.AuctionStartDateFrom)) {
				DateTime dtFrom = DateTime.Parse(pSearchCondition.AuctionStartDateFrom + " " + string.Format("{0}:00:00", string.IsNullOrEmpty(pSearchCondition.AuctionStartHourFrom) ? "00" : pSearchCondition.AuctionStartHourFrom));
				DateTime dtTo = DateTime.Parse(pSearchCondition.AuctionStartDateTo + " " + string.Format("{0}:59:59", string.IsNullOrEmpty(pSearchCondition.AuctionStartHourTo) ? "23" : pSearchCondition.AuctionStartHourTo)).AddSeconds(1);
				SysPrograms.SqlAppendWhere("(AUCTION_START_DATE >= :AUCTION_START_DATE_FROM AND AUCTION_START_DATE < :AUCTION_START_DATE_TO)", ref pWhere);
				oOracleParameterList.Add(new OracleParameter("AUCTION_START_DATE_FROM", OracleDbType.Date, dtFrom, ParameterDirection.Input));
				oOracleParameterList.Add(new OracleParameter("AUCTION_START_DATE_TO", OracleDbType.Date, dtTo, ParameterDirection.Input));
			}

			if (!string.IsNullOrEmpty(pSearchCondition.BlindEndDateFrom)) {
				DateTime dtFrom = DateTime.Parse(pSearchCondition.BlindEndDateFrom + " " + string.Format("{0}:00:00", string.IsNullOrEmpty(pSearchCondition.BlindEndHourFrom) ? "00" : pSearchCondition.BlindEndHourFrom));
				DateTime dtTo = DateTime.Parse(pSearchCondition.BlindEndDateTo + " " + string.Format("{0}:59:59", string.IsNullOrEmpty(pSearchCondition.BlindEndHourTo) ? "23" : pSearchCondition.BlindEndHourTo)).AddSeconds(1);
				SysPrograms.SqlAppendWhere("(BLIND_END_DATE >= :BLIND_END_DATE_FROM AND BLIND_END_DATE < :BLIND_END_DATE_TO)", ref pWhere);
				oOracleParameterList.Add(new OracleParameter("BLIND_END_DATE_FROM", OracleDbType.Date, dtFrom, ParameterDirection.Input));
				oOracleParameterList.Add(new OracleParameter("BLIND_END_DATE_TO", OracleDbType.Date, dtTo, ParameterDirection.Input));
			}

			if (!string.IsNullOrEmpty(pSearchCondition.AuctionEndDateFrom)) {
				DateTime dtFrom = DateTime.Parse(pSearchCondition.AuctionEndDateFrom + " " + string.Format("{0}:00:00", string.IsNullOrEmpty(pSearchCondition.AuctionEndHourFrom) ? "00" : pSearchCondition.AuctionEndHourFrom));
				DateTime dtTo = DateTime.Parse(pSearchCondition.AuctionEndDateTo + " " + string.Format("{0}:59:59", string.IsNullOrEmpty(pSearchCondition.AuctionEndHourTo) ? "23" : pSearchCondition.AuctionEndHourTo)).AddSeconds(1);
				SysPrograms.SqlAppendWhere("(AUCTION_END_DATE >= :AUCTION_END_DATE_FROM AND AUCTION_END_DATE < :AUCTION_END_DATE_TO)", ref pWhere);
				oOracleParameterList.Add(new OracleParameter("AUCTION_END_DATE_FROM", OracleDbType.Date, dtFrom, ParameterDirection.Input));
				oOracleParameterList.Add(new OracleParameter("AUCTION_END_DATE_TO", OracleDbType.Date, dtTo, ParameterDirection.Input));
			}

			if (!string.IsNullOrEmpty(pSearchCondition.EntryPointFrom)) {
				SysPrograms.SqlAppendWhere("(ENTRY_POINT >= :ENTRY_POINT_FROM AND ENTRY_POINT < :ENTRY_POINT_TO)", ref pWhere);
				oOracleParameterList.Add(new OracleParameter("ENTRY_POINT_FROM", int.Parse(pSearchCondition.EntryPointFrom)));
				oOracleParameterList.Add(new OracleParameter("ENTRY_POINT_TO", int.Parse(pSearchCondition.EntryPointTo)));
			}

			if (!string.IsNullOrEmpty(pSearchCondition.BlindEntryPointFrom)) {
				SysPrograms.SqlAppendWhere("(BLIND_ENTRY_POINT >= :BLIND_ENTRY_POINT_FROM AND BLIND_ENTRY_POINT < :BLIND_ENTRY_POINT_TO)", ref pWhere);
				oOracleParameterList.Add(new OracleParameter("BLIND_ENTRY_POINT_FROM", int.Parse(pSearchCondition.BlindEntryPointFrom)));
				oOracleParameterList.Add(new OracleParameter("BLIND_ENTRY_POINT_TO", int.Parse(pSearchCondition.BlindEntryPointTo)));
			}

			if (!string.IsNullOrEmpty(pSearchCondition.BidPointFrom)) {
				SysPrograms.SqlAppendWhere("(BID_POINT >= :BID_POINT_FROM AND BID_POINT < :BID_POINT_TO)", ref pWhere);
				oOracleParameterList.Add(new OracleParameter("BID_POINT_FROM", int.Parse(pSearchCondition.BidPointFrom)));
				oOracleParameterList.Add(new OracleParameter("BID_POINT_TO", int.Parse(pSearchCondition.BidPointTo)));
			}

			if (!string.IsNullOrEmpty(pSearchCondition.BlindBidPointFrom)) {
				SysPrograms.SqlAppendWhere("(BLIND_BID_POINT >= :BLIND_BID_POINT_FROM AND BLIND_BID_POINT < :BLIND_BID_POINT_TO)", ref pWhere);
				oOracleParameterList.Add(new OracleParameter("BLIND_BID_POINT_FROM", int.Parse(pSearchCondition.BlindBidPointFrom)));
				oOracleParameterList.Add(new OracleParameter("BLIND_BID_POINT_TO", int.Parse(pSearchCondition.BlindBidPointTo)));
			}

			if (!string.IsNullOrEmpty(pSearchCondition.ReserveAmtFrom)) {
				SysPrograms.SqlAppendWhere("(RESERVE_AMT >= :RESERVE_AMT_FROM AND RESERVE_AMT < :RESERVE_AMT_TO)", ref pWhere);
				oOracleParameterList.Add(new OracleParameter("RESERVE_AMT_FROM", int.Parse(pSearchCondition.ReserveAmtFrom)));
				oOracleParameterList.Add(new OracleParameter("RESERVE_AMT_TO", int.Parse(pSearchCondition.ReserveAmtTo)));
			}

			if (!string.IsNullOrEmpty(pSearchCondition.MinimumBidAmtFrom)) {
				SysPrograms.SqlAppendWhere("(MINIMUM_BID_AMT >= :MINIMUM_BID_AMT_FROM AND MINIMUM_BID_AMT < :MINIMUM_BID_AMT_TO)", ref pWhere);
				oOracleParameterList.Add(new OracleParameter("MINIMUM_BID_AMT_FROM", int.Parse(pSearchCondition.MinimumBidAmtFrom)));
				oOracleParameterList.Add(new OracleParameter("MINIMUM_BID_AMT_TO", int.Parse(pSearchCondition.MinimumBidAmtTo)));
			}

			if (!string.IsNullOrEmpty(pSearchCondition.AutoExtensionFlag)) {
				SysPrograms.SqlAppendWhere("AUTO_EXTENSION_FLAG = :AUTO_EXTENSION_FLAG", ref pWhere);
				oOracleParameterList.Add(new OracleParameter("AUTO_EXTENSION_FLAG", pSearchCondition.AutoExtensionFlag));
			}

		} else {
			if (!string.IsNullOrEmpty(pSearchCondition.ProductMaker)) {
				SysPrograms.SqlAppendWhere("PRODUCT_MAKER_SUMMARY LIKE '%' || :PRODUCT_MAKER_SUMMARY || '%'", ref pWhere);
				oOracleParameterList.Add(new OracleParameter("PRODUCT_MAKER_SUMMARY", pSearchCondition.ProductMaker));
			}

			if (!string.IsNullOrEmpty(pSearchCondition.ReleaseDateFrom)) {
				DateTime dtFrom = DateTime.Parse(pSearchCondition.ReleaseDateFrom + " " + "00:00:00");
				DateTime dtTo = DateTime.Parse(pSearchCondition.ReleaseDateTo + " " + "23:59:59").AddSeconds(1);
				SysPrograms.SqlAppendWhere("(RELEASE_DATE >= :RELEASE_DATE_FROM AND RELEASE_DATE < :RELEASE_DATE_TO)", ref pWhere);
				oOracleParameterList.Add(new OracleParameter("RELEASE_DATE_FROM", OracleDbType.Date, dtFrom, ParameterDirection.Input));
				oOracleParameterList.Add(new OracleParameter("RELEASE_DATE_TO", OracleDbType.Date, dtTo, ParameterDirection.Input));
			}

			if (!string.IsNullOrEmpty(pSearchCondition.RoyaltyRate)) {
				SysPrograms.SqlAppendWhere("ROYALTY_RATE = :ROYALTY_RATE", ref pWhere);
				oOracleParameterList.Add(new OracleParameter("ROYALTY_RATE", pSearchCondition.RoyaltyRate));
			}

			if (!string.IsNullOrEmpty(pSearchCondition.ChargePointFrom)) {
				SysPrograms.SqlAppendWhere("(CHARGE_POINT >= :CHARGE_POINT_FROM AND CHARGE_POINT < :CHARGE_POINT_TO)", ref pWhere);
				oOracleParameterList.Add(new OracleParameter("CHARGE_POINT_FROM", int.Parse(pSearchCondition.ChargePointFrom)));
				oOracleParameterList.Add(new OracleParameter("CHARGE_POINT_TO", int.Parse(pSearchCondition.ChargePointTo)));
			}

			if (!string.IsNullOrEmpty(pSearchCondition.SpecialChargePointFrom)) {
				SysPrograms.SqlAppendWhere("(SPECIAL_CHARGE_POINT >= :SPECIAL_CHARGE_POINT_FROM AND SPECIAL_CHARGE_POINT < :SPECIAL_CHARGE_POINT_TO)", ref pWhere);
				oOracleParameterList.Add(new OracleParameter("SPECIAL_CHARGE_POINT_FROM", int.Parse(pSearchCondition.SpecialChargePointFrom)));
				oOracleParameterList.Add(new OracleParameter("SPECIAL_CHARGE_POINT_TO", int.Parse(pSearchCondition.SpecialChargePointTo)));
			}

			if (!string.IsNullOrEmpty(pSearchCondition.GrossPriceFrom)) {
				SysPrograms.SqlAppendWhere("(GROSS_PRICE >= :GROSS_PRICE_FROM AND GROSS_PRICE < :GROSS_PRICE_TO)", ref pWhere);
				oOracleParameterList.Add(new OracleParameter("GROSS_PRICE_FROM", int.Parse(pSearchCondition.GrossPriceFrom)));
				oOracleParameterList.Add(new OracleParameter("GROSS_PRICE_TO", int.Parse(pSearchCondition.GrossPriceTo)));
			}
		}

		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < pSearchCondition.GenreList.Count; i++) {
			string[] codes = pSearchCondition.GenreList[i].Split(';');
			if (codes.Length != 2) {
				continue;
			}

			if (!string.IsNullOrEmpty(codes[0]) && !string.IsNullOrEmpty(codes[1])) {
				if (sb.Length > 0 && i > 0) {
					sb.AppendLine(" OR ");
				}
				sb.AppendFormat("(PRODUCT_GENRE_CATEGORY_CD = :PRODUCT_GENRE_CATEGORY_CD{0} AND PRODUCT_GENRE_CD = :PRODUCT_GENRE_CD{0})", i);
				oOracleParameterList.Add(new OracleParameter(string.Format("PRODUCT_GENRE_CATEGORY_CD{0}", i), codes[0]));
				oOracleParameterList.Add(new OracleParameter(string.Format("PRODUCT_GENRE_CD{0}", i), codes[1]));
			}
		}
		if (sb.Length > 0) {
			sb.Insert(0, "EXISTS (SELECT 1 FROM T_PRODUCT_GENRE_REL WHERE VW_PRODUCT01.SITE_CD = SITE_CD AND VW_PRODUCT01.PRODUCT_AGENT_CD = PRODUCT_AGENT_CD AND VW_PRODUCT01.PRODUCT_SEQ = PRODUCT_SEQ AND (");
			sb.AppendLine(") )");
			SysPrograms.SqlAppendWhere(sb.ToString(), ref pWhere);
		}

		if(pSearchCondition.NoGenreFlag) {
			SysPrograms.SqlAppendWhere(" NOT EXISTS ( SELECT 1 FROM T_PRODUCT_GENRE_REL WHERE VW_PRODUCT01.SITE_CD = SITE_CD AND VW_PRODUCT01.PRODUCT_AGENT_CD = PRODUCT_AGENT_CD AND VW_PRODUCT01.PRODUCT_SEQ = PRODUCT_SEQ ) ",ref pWhere);		
		}

		return oOracleParameterList.ToArray();
	}

	public DataSet GetOne(string pProductId) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT				").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.SITE_CD					,	").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.PRODUCT_AGENT_CD         ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.PRODUCT_SEQ              ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.PRODUCT_ID               ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.PRODUCT_TYPE             ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.PRODUCT_NM               ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.PRODUCT_DISCRIPTION      ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.PART_NUMBER              ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.PRODUCT_SERIES_SUMMARY   ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.CAST_SUMMARY             ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.PRODUCT_MAKER_SUMMARY    ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.PUBLISH_END_DATE         ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.PUBLISH_START_DATE       ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.RELEASE_DATE             ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.GROSS_PRICE              ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.CHARGE_POINT             ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.SPECIAL_CHARGE_POINT     ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.PUBLISH_FLAG             ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.DEL_FLAG                 ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.REMARKS                  ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.LINK_ID                  ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.LINK_DATE                ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.UNEDITABLE_FLAG          ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.REVISION_NO              ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.UPDATE_DATE			  ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.PICKUP_MASK			  ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.ROYALTY_RATE			  ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.NOT_NEW_FLAG			  ,   ").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.PRODUCT_AGENT_NM             ").AppendLine();
		oSqlBuilder.Append(" FROM				").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02	").AppendLine();
		oSqlBuilder.Append(" WHERE				").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT02.PRODUCT_ID			= :PRODUCT_ID ").AppendLine();

		DataSet oProductDataSet = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":PRODUCT_ID", pProductId);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oProductDataSet);
				}
			}
		} finally {
			conn.Close();
		}
		return oProductDataSet;
	}
}
