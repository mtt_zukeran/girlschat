/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin 
--	Title			: キャスト課金設定一時保存設定
--	Progaram ID		: CastChargeTempCondition
--
--  Creation Date	: 2012.05.24
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using System.Text;

public class CastChargeTempCondition:DbSession {

	public CastChargeTempCondition() {
	}

	public DataSet GetList(string pSiteCd) {
		StringBuilder oSql = new StringBuilder();
		oSql.AppendLine("SELECT									");
		oSql.AppendLine("	SITE_CD							,	");
		oSql.AppendLine("	SITE_NM							,	");
		oSql.AppendLine("	CAST_CHARGE_TEMP_CD				,	");
		oSql.AppendLine("	CAST_CHARGE_TEMP_CD_NM			,	");
		oSql.AppendLine("	APPLICATION_DATE				,	");
		oSql.AppendLine("	NA_FLAG							,	");
		oSql.AppendLine("	UPDATE_DATE							");
		oSql.AppendLine("FROM									");
		oSql.AppendLine("	VW_CAST_CHARGE_TEMP_COND01			");
		oSql.AppendLine("WHERE									");
		oSql.AppendLine("	SITE_CD				= :SITE_CD		");
		oSql.AppendLine("ORDER BY SITE_CD,CAST_CHARGE_TEMP_CD");

		try {
			conn = this.DbConnect();

			using (cmd = this.CreateSelectCommand(oSql.ToString(),conn)) {
				cmd.Parameters.Add(new OracleParameter("SITE_CD",pSiteCd));
				using (da = new OracleDataAdapter(cmd))
				using (DataSet ds = new DataSet()) {
					da.Fill(ds);
					return ds;
				}
			}
		} finally {
			conn.Close();
		}
	}

	public int GetNaCount(string pSiteCd) {
		string sSql = "SELECT COUNT(*) FROM T_CAST_CHARGE_TEMP_CONDITION WHERE NA_FLAG = 0 AND SITE_CD = :SITE_CD";

		try {
			return this.ExecuteSelectCountQueryBase(sSql,new OracleParameter[] { new OracleParameter("SITE_CD",pSiteCd) });
		} finally {
			conn.Close();
		}
	}
}
