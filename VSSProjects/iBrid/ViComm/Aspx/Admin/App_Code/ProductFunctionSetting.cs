﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 商品機能

--	Progaram ID		: ProductFunctinSetting
--
--  Creation Date	: 2011.06.17
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;
using iBridCommLib;
using ViComm;

/// <summary>
/// ProductFunctionSetting の概要の説明です
/// </summary>
public class ProductFunctionSetting:DbSession {
	public ProductFunctionSetting() {
		//
		// TODO: コンストラクタ ロジックをここに追加します
		//
	}

	public int GetPageCount() {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_PRODUCT_FUNCTION_SETTING	,");
		oSqlBuilder.AppendLine("	T_SITE						 ");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	T_PRODUCT_FUNCTION_SETTING.SITE_CD	= T_SITE.SITE_CD");

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(int startRowIndex,int maximumRows) {
		DataSet oDataSet;
		try {
			conn = DbConnect();
			oDataSet = new DataSet();

			StringBuilder oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine("SELECT	");
			oSqlBuilder.AppendLine("	T_PRODUCT_FUNCTION_SETTING.SITE_CD		,");
			oSqlBuilder.AppendLine("	T_PRODUCT_FUNCTION_SETTING.UPDATE_DATE	,");
			oSqlBuilder.AppendLine("	T_SITE.SITE_NM");
			oSqlBuilder.AppendLine("FROM	");
			oSqlBuilder.AppendLine("	T_PRODUCT_FUNCTION_SETTING	,");
			oSqlBuilder.AppendLine("	T_SITE						 ");
			oSqlBuilder.AppendLine("WHERE");
			oSqlBuilder.AppendLine("	T_PRODUCT_FUNCTION_SETTING.SITE_CD	= T_SITE.SITE_CD");

			string sSortExpression = "ORDER BY T_PRODUCT_FUNCTION_SETTING.SITE_CD";
			string sPagingSql;
			OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;

				using (da = new OracleDataAdapter(cmd)) {
					cmd.Parameters.AddRange(oPagingParams);
					da.Fill(oDataSet);
				}
			}
		} finally {
			conn.Close();
		}
		return oDataSet;
	}
}
