/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 会員変更履歴
--	Progaram ID		: UserHistory
--
--  Creation Date	: 2010.04.15
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;
using ViComm;

public class UserHistory:DbSession {

	public UserHistory() {
	}

	public int GetPageCount(
		string pSiteCd,
		int pModifyReasonMask,
		string pLoginID,
		string pModifyDayFrom,
		string pModifyTimeFrom,
		string pModifyDayTo,
		string pModifyTimeTo
	) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_USER_HISTORY ";

			string sWhere = "";
			OracleParameter[] oParms = CreateWhere(
											pSiteCd,
											pModifyReasonMask,
											pLoginID,
											pModifyDayFrom,
											pModifyTimeFrom,
											pModifyDayTo,
											pModifyTimeTo,
											ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < oParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)oParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(
		string pSiteCd,
		int pModifyReasonMask,
		string pLoginID,
		string pModifyDayFrom,
		string pModifyTimeFrom,
		string pModifyDayTo,
		string pModifyTimeTo,
		int startRowIndex,
		int maximumRows
	) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY SITE_CD,USER_HIS_SEQ DESC";
			string sSql = "SELECT " + QueryFields() +
							"FROM(" +
							" SELECT " +
								"VW_USER_HISTORY01.*, " +
								"ROW_NUMBER() OVER ( " + sOrder + " ) AS RNUM " +
							" FROM " +
								"VW_USER_HISTORY01";

			string sWhere = "";
			OracleParameter[] oParms = CreateWhere(
											pSiteCd,
											pModifyReasonMask,
											pLoginID,
											pModifyDayFrom,
											pModifyTimeFrom,
											pModifyDayTo,
											pModifyTimeTo,
											ref sWhere);

			sSql = sSql + sWhere;
			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < oParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)oParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USER_HISTORY01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(
		string pSiteCd,
		int pModifyReasonMask,
		string pLoginID,
		string pModifyDayFrom,
		string pModifyTimeFrom,
		string pModifyDayTo,
		string pModifyTimeTo,
		ref string pWhere
	) {
		pWhere = "";

		string[] sValues = new string[28];
		int iCnt = 0;

		SetDefaultTimeValue(ref pModifyTimeFrom,ref pModifyTimeTo);

		ArrayList list = new ArrayList();
		pWhere = pWhere + " WHERE SITE_CD = :SITE_CD  ";
		list.Add(new OracleParameter("SITE_CD",pSiteCd));

		if (pModifyReasonMask != 0) {
			if ((pModifyReasonMask & ViCommConst.MASK_MODIFY_REASON_REGIST) != 0) {
				sValues[iCnt++] = ViCommConst.MODIFY_REASON_REGIST_INFO;
			}
			if ((pModifyReasonMask & ViCommConst.MASK_MODIFY_REASON_MODIFY) != 0) {
				sValues[iCnt++] = ViCommConst.MODIFY_REASON_MODIFY_INFO;
			}
			if ((pModifyReasonMask & ViCommConst.MASK_MODIFY_REASON_DELETE) != 0) {
				sValues[iCnt++] = ViCommConst.MODIFY_REASON_DELETE_INFO;
			}
			if ((pModifyReasonMask & ViCommConst.MASK_MODIFY_REASON_RECEIPT) != 0) {
				sValues[iCnt++] = ViCommConst.MODIFY_REASON_RECEIPT;
				sValues[iCnt++] = ViCommConst.MODIFY_REASON_AUTO_RECEIPT;
				sValues[iCnt++] = ViCommConst.MODIFY_REASON_TRANS_POINT_MOBILE_CASH;
			}
			if ((pModifyReasonMask & ViCommConst.MASK_MODIFY_REASON_TRANS_POINT) != 0) {
				sValues[iCnt++] = ViCommConst.MODIFY_REASON_TRANS_POINT_PREPAID;
				sValues[iCnt++] = ViCommConst.MODIFY_REASON_TRANS_POINT_CREDIT_PACK;
				sValues[iCnt++] = ViCommConst.MODIFY_REASON_TRANS_POINT_C_CHECK;
				sValues[iCnt++] = ViCommConst.MODIFY_REASON_TRANS_POINT_BITCASH;
				sValues[iCnt++] = ViCommConst.MODIFY_REASON_TRANS_POINT_S_MONEY;
				sValues[iCnt++] = ViCommConst.MODIFY_REASON_TRANS_POINT_CVSDL;
				sValues[iCnt++] = ViCommConst.MODIFY_REASON_TRANS_POINT_A_CHECK;
				sValues[iCnt++] = ViCommConst.MODIFY_REASON_TRANS_POINT_SSAD;
				sValues[iCnt++] = ViCommConst.MODIFY_REASON_TRANS_POINT_OFFICIAL_CHECK;
				sValues[iCnt++] = ViCommConst.MODIFY_REASON_TRANS_POINT_MCHA;
				sValues[iCnt++] = ViCommConst.MODIFY_REASON_TRANS_POINT_POINT_GATE;
				sValues[iCnt++] = ViCommConst.MODIFY_REASON_TRANS_POINT_G_MONEY;
				sValues[iCnt++] = ViCommConst.MODIFY_REASON_TRANS_POINT_PAYMENT_AFTER;
				sValues[iCnt++] = ViCommConst.MODIFY_REASON_TRANS_POINT_POICHA;
				sValues[iCnt++] = ViCommConst.MODIFY_REASON_TRANS_POINT_SAKAZUKI;
				sValues[iCnt++] = ViCommConst.MODIFY_REASON_TRANS_POINT_CREDIT;
				sValues[iCnt++] = ViCommConst.MODIFY_REASON_TRANS_POINT_SAKAZUKI_LIST;
				sValues[iCnt++] = ViCommConst.MODIFY_REASON_TRANS_POINT_CREDIT_AUTH;
				sValues[iCnt++] = ViCommConst.MODIFY_REASON_TRANS_POINT_PBK;
				sValues[iCnt++] = ViCommConst.MODIFY_REASON_TRANS_POINT_GIGA_POINT;
				sValues[iCnt++] = ViCommConst.MODIFY_REASON_TRANS_POINT_FRIEND_POINT;				
			}
			if ((pModifyReasonMask & ViCommConst.MASK_MODIFY_REASON_CANCEL_RECEIPT) != 0) {
				sValues[iCnt++] = ViCommConst.MODIFY_REASON_CANCEL_RECEIPT;
			}

			pWhere = pWhere + " AND (MODIFY_REASON_CD IN (";
			for (int i = 0;i < iCnt;i++) {
				pWhere = pWhere + ":USER_RANK" + i.ToString() + ",";
				list.Add(new OracleParameter("USER_RANK",sValues[i]));
			}
			pWhere = pWhere.Substring(0,pWhere.Length - 1) + ")) ";
		}

		if (!pLoginID.Equals("")) {
			SysPrograms.SqlAppendWhere("LOGIN_ID LIKE :LOGIN_ID||'%'",ref pWhere);
			list.Add(new OracleParameter("LOGIN_ID",pLoginID ));
		}

		if (!pModifyDayFrom.Equals("")) {
			DateTime dtFrom = DateTime.Parse(pModifyDayFrom + " " + pModifyTimeFrom + ":00:00");
			DateTime dtTo = DateTime.Parse(pModifyDayTo + " " + pModifyTimeTo + ":59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere("(CREATE_DATE >= :CREATE_DATE_FROM AND CREATE_DATE < :CREATE_DATE_TO)",ref pWhere);
			list.Add(new OracleParameter("CREATE_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("CREATE_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	private void SetDefaultTimeValue(ref string pTimeFrom,ref string pTimeTo) {
		if (string.IsNullOrEmpty(pTimeFrom)) {
			pTimeFrom = "00";
		}
		if (string.IsNullOrEmpty(pTimeTo)) {
			pTimeTo = "23";
		}
	}

	public DataSet GetOne(string pUserHisSeq) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT " +
								QueryFields() +
							"FROM " +
								"VW_USER_HISTORY01 " +
							"WHERE " +
								"USER_HIS_SEQ = :USER_HIS_SEQ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("USER_HIS_SEQ",pUserHisSeq);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USER_HISTORY01");
				}
			}
		} finally {
			conn.Close();
		}

		return ds;
	}

	private string QueryFields() {
		return "SITE_CD							," +
				"USER_SEQ						," +
				"USER_HIS_SEQ					," +
				"MODIFY_BEFORE_AFTER_CD			," +
				"CREATE_DATE					," +
				"MODIFY_REASON_CD				," +
				"SEX_CD							," +
				"USE_TERMINAL_TYPE				," +
				"URI							," +
				"TERMINAL_PASSWORD				," +
				"TEL							," +
				"EMAIL_ADDR						," +
				"LOGIN_ID						," +
				"LOGIN_PASSWORD					," +
				"USER_STATUS					," +
				"MOBILE_CARRIER_CD				," +
				"MOBILE_TERMINAL_NM				," +
				"TERMINAL_UNIQUE_ID				," +
				"TEL_ATTESTED_FLAG				," +
				"NON_EXIST_MAIL_ADDR_FLAG		," +
				"REMARKS1						," +
				"REMARKS2						," +
				"REMARKS3						," +
				"BAL_POINT						," +
				"SERVICE_POINT					," +
				"SERVICE_POINT_EFFECTIVE_DATE	," +
				"LIMIT_POINT					," +
				"LAST_LOGIN_DATE				," +
				"FIRST_POINT_USED_DATE			," +
				"LAST_POINT_USED_DATE			," +
				"FIRST_RECEIPT_DAY				," +
				"LAST_RECEIPT_DATE				," +
				"RECEIPT_LIMIT_DAY				," +
				"BILL_AMT						," +
				"TOTAL_RECEIPT_AMT				," +
				"TOTAL_RECEIPT_COUNT			," +
				"EXIST_CREDIT_DEAL_FLAG			," +
				"BLACK_DAY						," +
				"BLACK_COUNT					," +
				"BLACK_TIME						," +
				"LAST_BLACK_DAY					," +
				"LAST_BLACK_TIME				," +
				"CAST_MAIL_RX_TYPE				," +
				"INFO_MAIL_RX_TYPE				," +
				"LOGIN_MAIL_RX_FLAG1			," +
				"LOGIN_MAIL_RX_FLAG2			," +
				"AD_CD							," +
				"PROFILE_OK_FLAG				," +
				"URGE_LEVEL						," +
				"MAX_URGE_COUNT					," +
				"URGE_COUNT_TODAY				," +
				"URGE_RESTART_DAY				," +
				"URGE_END_DAY					," +
				"URGE_EXEC_LAST_DATE			," +
				"URGE_EXEC_LAST_STATUS			," +
				"URGE_EXEC_COUNT				," +
				"URGE_CONNECT_COUNT				," +
				"MODIFY_USER_NM					," +
				"USER_STATUS_NM					," +
				"MODIFY_REASON_NM				," +
				"MODIFY_BEFORE_AFTER_NM			," +
				"URGE_EXEC_LAST_STATUS_NM		";
	}


	public int GetModifyMailHistoryCount(string pUserSeq) {

		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql =	" SELECT COUNT(*) AS ROW_COUNT FROM T_USER_HISTORY					 " +
							" WHERE																 " +
							"	 USER_SEQ = :USER_SEQ											 " +
							"	 AND (MODIFY_REASON_CD IN ( :MODIFY_MAIL_ADDR ))	 ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":USER_SEQ",pUserSeq);
				cmd.Parameters.Add(":MODIFY_MAIL_ADDR",ViCommConst.MODIFY_REASON_MODIFY_MAIL_ADDR);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_USER_HISTORY");
					if (ds.Tables[0].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
					}
				}
			}

		} finally {
			conn.Close();
		}

		return iPageCount;
	}

	public DataSet GetModifyMailHistory(string pUserSeq,int startRowIndex,int maximumRows) {
		
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

            string sOrder = " ORDER BY CREATE_DATE DESC,MODIFY_BEFORE_AFTER_CD DESC ";
			string sSql =	" SELECT "+
							"	 CREATE_DATE															," +
							"	 MODIFY_USER_CD															," +
							"	 MODIFY_BEFORE_AFTER_NM													," +
							"	 EMAIL_ADDR																 " +
							" FROM(																		 " +
							"	 SELECT																	 " +
							"		 VW_USER_HISTORY01.*,												 " +
							"		 ROW_NUMBER() OVER ( " + sOrder + " ) AS RNUM						 " +
							"	 FROM																	 " +
							"		 VW_USER_HISTORY01													 " +
							"	 WHERE																	 " +
							"		 USER_SEQ = :USER_SEQ												 " +
							"		 AND (MODIFY_REASON_CD IN ( :MODIFY_MAIL_ADDR ))            		 ";
			sSql = sSql + " )WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW		 ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":USER_SEQ",pUserSeq);
				cmd.Parameters.Add(":MODIFY_MAIL_ADDR",ViCommConst.MODIFY_REASON_MODIFY_MAIL_ADDR);

				cmd.Parameters.Add(":FIRST_ROW",startRowIndex);
				cmd.Parameters.Add(":LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
			
		} finally {
			conn.Close();
		}
		
		return ds;
	}

	public int GetModifyTelHistoryCount(string pUserSeq)
	{

		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try
		{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = " SELECT COUNT(*) AS ROW_COUNT FROM T_USER_HISTORY					 " +
							" WHERE																 " +
							"	 USER_SEQ = :USER_SEQ											 " +
							"	 AND (MODIFY_REASON_CD IN ( :MODIFY_TEL ))	 ";

			using (cmd = CreateSelectCommand(sSql, conn))
			{
				cmd.BindByName = true;
				cmd.Parameters.Add(":USER_SEQ", pUserSeq);
				cmd.Parameters.Add(":MODIFY_TEL", ViCommConst.MODIFY_REASON_MODIFY_TEL);

				using (da = new OracleDataAdapter(cmd))
				{
					da.Fill(ds, "T_USER_HISTORY");
					if (ds.Tables[0].Rows.Count != 0)
					{
						dr = ds.Tables[0].Rows[0];
						iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
					}
				}
			}

		}
		finally
		{
			conn.Close();
		}

		return iPageCount;
	}

	public DataSet GetModifyTelHistory(string pUserSeq, int startRowIndex, int maximumRows) {

		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = " ORDER BY CREATE_DATE DESC,MODIFY_BEFORE_AFTER_CD DESC ";
			string sSql = " SELECT " +
							"	 CREATE_DATE															," +
							"	 MODIFY_USER_CD															," +
							"	 MODIFY_BEFORE_AFTER_NM													," +
							"	 TEL																	 " +
							" FROM(																		 " +
							"	 SELECT																	 " +
							"		 VW_USER_HISTORY01.*,													 " +
							"		 ROW_NUMBER() OVER ( " + sOrder + " ) AS RNUM						 " +
							"	 FROM																	 " +
							"		 VW_USER_HISTORY01														 " +
							"	 WHERE																	 " +
							"		 USER_SEQ = :USER_SEQ												 " +
							"		 AND (MODIFY_REASON_CD IN ( :MODIFY_TEL ))				      		 ";
			sSql = sSql + " )WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW		 ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql, conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":USER_SEQ", pUserSeq);
				cmd.Parameters.Add(":MODIFY_TEL", ViCommConst.MODIFY_REASON_MODIFY_TEL);

				cmd.Parameters.Add(":FIRST_ROW", startRowIndex);
				cmd.Parameters.Add(":LAST_ROW", startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}

		} finally {
			conn.Close();
		}

		return ds;
	}

	public DataSet GetManModifyLoginPassword(string pSiteCd,string pUserSeq,string pUserCharNo) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	CREATE_DATE,");
		oSqlBuilder.AppendLine("	MODIFY_USER_CD,");
		oSqlBuilder.AppendLine("	MODIFY_BEFORE_AFTER_CD,");
		oSqlBuilder.AppendLine("	LOGIN_PASSWORD");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_USER_HISTORY");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND");
		oSqlBuilder.AppendLine("	USER_SEQ = :USER_SEQ AND");
		oSqlBuilder.AppendLine("	USER_CHAR_NO = :USER_CHAR_NO AND");
		oSqlBuilder.AppendLine("	MODIFY_REASON_CD = :MODIFY_REASON_CD");
		oSqlBuilder.AppendLine("ORDER BY CREATE_DATE DESC,MODIFY_BEFORE_AFTER_CD DESC");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":MODIFY_REASON_CD",ViCommConst.MODIFY_REASON_MODIFY_INFO));

		DataSet dsTemp = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());
		DataSet ds = dsTemp.Clone();
		DataRow drAfter = ds.Tables[0].NewRow();
		DataRow drBefore = ds.Tables[0].NewRow();

		foreach (DataRow drTemp in dsTemp.Tables[0].Rows) {
			if (iBridUtil.GetStringValue(drTemp["MODIFY_BEFORE_AFTER_CD"]).Equals("2")) {
				drAfter.ItemArray = drTemp.ItemArray;
			} else if (iBridUtil.GetStringValue(drTemp["MODIFY_BEFORE_AFTER_CD"]).Equals("1")) {
				drBefore.ItemArray = drTemp.ItemArray;

				if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(drAfter["CREATE_DATE"]))) {
					if (!iBridUtil.GetStringValue(drAfter["LOGIN_PASSWORD"]).Equals(iBridUtil.GetStringValue(drBefore["LOGIN_PASSWORD"]))) {
						if (DateTime.Parse(iBridUtil.GetStringValue(drAfter["CREATE_DATE"])) >= DateTime.Parse(iBridUtil.GetStringValue(drBefore["CREATE_DATE"]))) {
							ds.Tables[0].Rows.Add().ItemArray = drAfter.ItemArray;
							ds.Tables[0].Rows.Add().ItemArray = drBefore.ItemArray;
						}
					}
				}
			}
		}

		return ds;
	}

	public DataSet GetCastModifyLoginPassword(string pSiteCd,string pUserSeq,string pUserCharNo) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	CREATE_DATE,");
		oSqlBuilder.AppendLine("	MODIFY_USER_CD,");
		oSqlBuilder.AppendLine("	MODIFY_BEFORE_AFTER_CD,");
		oSqlBuilder.AppendLine("	LOGIN_PASSWORD");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_USER_HISTORY");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND");
		oSqlBuilder.AppendLine("	USER_SEQ = :USER_SEQ AND");
		oSqlBuilder.AppendLine("	USER_CHAR_NO = :USER_CHAR_NO AND");
		oSqlBuilder.AppendLine("	MODIFY_REASON_CD = :MODIFY_REASON_CD");
		oSqlBuilder.AppendLine("ORDER BY CREATE_DATE DESC,MODIFY_BEFORE_AFTER_CD DESC");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter(":MODIFY_REASON_CD",ViCommConst.MODIFY_REASON_MODIFY_LOGIN_PASSWORD));

		DataSet ds = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		return ds;
	}
}
