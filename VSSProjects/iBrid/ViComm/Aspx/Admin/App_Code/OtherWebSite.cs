﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 他システムＷＥＢサイト
--	Progaram ID		: OtherWebSite
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class OtherWebSite:DbSession {

	public string siteNm;

	public OtherWebSite() {
	}

	public int GetPageCount() {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_OTHER_WEB_SITE ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}


	public DataSet GetPageCollection(int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY OTHER_WEB_SYS_ID";
			string sSql = "SELECT " +
							"OTHER_WEB_SYS_ID		," +
							"SITE_NM				," +
							"TOP_PAGE_URL			," +
							"USER_INFO_URL			," +
							"LOGIN_URL				," +
							"MY_PAGE_URL			," +
							"UPDATE_DATE " +
							"FROM(" +
							" SELECT T_OTHER_WEB_SITE.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM T_OTHER_WEB_SITE  ";

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_OTHER_WEB_SITE");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetList() {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT OTHER_WEB_SYS_ID,SITE_NM FROM T_OTHER_WEB_SITE ORDER BY OTHER_WEB_SYS_ID ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_OTHER_WEB_SITE");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetUnusedList() {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT OTHER_WEB_SYS_ID,OTHER_WEB_SYS_NM FROM VW_OTHER_WEB_SYS01 " +
							"WHERE SITE_CD IS NULL ORDER BY OTHER_WEB_SYS_ID ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_OTHER_WEB_SYS01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public int GetUnusedPageCount() {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_OTHER_WEB_SYS01 " +
			"WHERE SITE_CD IS NULL ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public bool GetOne(string pOtherWebSysId) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;
		try{
			conn = DbConnect();

			string sSql = " SELECT " +
							" SITE_NM	" +
							" FROM " +
							" T_OTHER_WEB_SITE " +
							" WHERE OTHER_WEB_SYS_ID = :OTHER_WEB_SYS_ID";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				using (da = new OracleDataAdapter(cmd)) {

					cmd.Parameters.Add("OTHER_WEB_SYS_ID",pOtherWebSysId);

					da.Fill(ds,"T_OTHER_WEB_SITE");

					if (ds.Tables["T_OTHER_WEB_SITE"].Rows.Count != 0) {
						dr = ds.Tables["T_OTHER_WEB_SITE"].Rows[0];
						siteNm = iBridUtil.GetStringValue(dr["SITE_NM"]);
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}
}
