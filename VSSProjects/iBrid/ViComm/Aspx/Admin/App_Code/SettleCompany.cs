﻿/*************************************************************************
--	System			: ViComm Sales
--	Sub System Name	: Admin
--	Title			: 決済会社

--	Progaram ID		: SettleCompany
--
--  Creation Date	: 2009.11.05
--  Creater			: iBrid(S.Ohtahara)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class SettleCompany:DbSession {

	public string settleCompanyNm;

	public SettleCompany() {
	}

	public int GetPageCount() {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_SETTLE_COMPANY ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT " +
							"SETTLE_COMPANY_CD	," +
							"SETTLE_COMPANY_NM 	," +
							"UPDATE_DATE		" +
							"FROM " +
							"T_SETTLE_COMPANY " +
							"ORDER BY SETTLE_COMPANY_CD";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_SETTLE_COMPANY");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public bool GetOne(string pSettleCompanyCd) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;
		try{
			conn = DbConnect();

			string sSql = " SELECT " +
							" SETTLE_COMPANY_NM	" +
							" FROM " +
							" T_SETTLE_COMPANY "+
							" WHERE SETTLE_COMPANY_CD = :SETTLE_COMPANY_CD";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				using (da = new OracleDataAdapter(cmd)) {

					cmd.Parameters.Add("SETTLE_COMPANY_CD",pSettleCompanyCd);
				
					da.Fill(ds,"T_SETTLE_COMPANY");

					if (ds.Tables["T_SETTLE_COMPANY"].Rows.Count != 0) {
						dr = ds.Tables["T_SETTLE_COMPANY"].Rows[0];
						settleCompanyNm = iBridUtil.GetStringValue(dr["SETTLE_COMPANY_NM"]);
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public DataSet GetList() {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT SETTLE_COMPANY_CD,SETTLE_COMPANY_NM FROM T_SETTLE_COMPANY ";

			sSql = sSql + " ORDER BY SETTLE_COMPANY_CD ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_SETTLE_COMPANY");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
}
