﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Pack
--	Title			: パック設定
--	Progaram ID		: Pack
--
--  Creation Date	: 2009.12.11
--  Creater			: i-Brid(T.Tokunaga)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater   Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Text;

public class Pack:DbSession {

	public string settleType;
	public int salesAmt;
	public string commoditiesCd;

	public DataSet GetPageCollection(string pSiteCd,int startRowIndex,int maximumRows) {

		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY SITE_CD,SETTLE_TYPE,SALES_AMT ";
			string sSql = "SELECT " +
							"SITE_CD		," +
							"SITE_NM		," +
							"SETTLE_TYPE	," +
							"SALES_AMT	    ," +
							"COMMODITIES_CD	," +
							"FIRST_TIME_FLAG," +
							"UPDATE_DATE	," +
							"CODE_NM		" +
							"FROM(" +
							" SELECT VW_PACK01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_PACK01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_PACK01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		pWhere = pWhere + " WHERE SITE_CD = :SITE_CD ";
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public int GetPageCount(string pSiteCd) {

		int iPageCount = 0;
		try{
			conn = DbConnect();
			DataSet ds;
			StringBuilder sSql = new StringBuilder();

			sSql.Append("SELECT ");
			sSql.Append("   COUNT(SITE_CD) AS ROW_COUNT ");
			sSql.Append("FROM ");
			sSql.Append("   VW_PACK01 ");
			sSql.Append("WHERE ");
			sSql.Append("   SITE_CD = :SITE_CD ");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					using (ds = new DataSet()) {
						cmd.Parameters.Add("SITE_CD",pSiteCd);
						da.Fill(ds);
						iPageCount = Convert.ToInt32(ds.Tables[0].Rows[0]["ROW_COUNT"]);
					}
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public bool GetOne(string pSiteCd,string pSettleType,int pSalesAmt) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try{
			conn = DbConnect("Pack.GetOne");

			using (cmd = CreateSelectCommand("SELECT * FROM T_PACK WHERE SITE_CD = :SITE_CD AND SETTLE_TYPE =:SETTLE_TYPE AND SALES_AMT =:SALES_AMT ",conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("SETTLE_TYPE",pSettleType);
				cmd.Parameters.Add("SALES_AMT",pSalesAmt);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_PACK");
					if (ds.Tables["T_PACK"].Rows.Count != 0) {
						dr = ds.Tables["T_PACK"].Rows[0];
						settleType = dr["SETTLE_TYPE"].ToString();
						salesAmt = int.Parse(dr["SALES_AMT"].ToString());
						commoditiesCd = GetStringValue(dr["COMMODITIES_CD"]);
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}

		return bExist;
	}

	private static string GetStringValue(object value) {
		if (value == null) {
			return "";
		} else {
			return value.ToString();
		}
	}
}
