/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 登録不可番号

--	Progaram ID		: UnableRegistNo
--
--  Creation Date	: 2010.04.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class UnableRegistNo:DbSession {

	public UnableRegistNo() {
	}

	public int GetPageCount(string pNoType,string pTermIdOrTel) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_UNABLE_REGIST_NO ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pNoType,pTermIdOrTel,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pNoType,string pTermIdOrTel,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY NO_TYPE,TERMINAL_UNIQUE_ID_OR_TEL ";
			string sSql = "SELECT "
						+ "T_UNABLE_REGIST_NO.NO_TYPE		,"
						+ "T_CODE_DTL.CODE_NM						AS NO_TYPE_NM	,"
						+ "T_UNABLE_REGIST_NO.TERMINAL_UNIQUE_ID_OR_TEL				,"
						+ "SUBSTRB(T_UNABLE_REGIST_NO.REMARKS,1,96)	AS REMARKS		,"
						+ "T_UNABLE_REGIST_NO.UPDATE_DATE	"
						+ " FROM ( "
						+ " SELECT T_UNABLE_REGIST_NO.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM T_UNABLE_REGIST_NO  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pNoType,pTermIdOrTel,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ") T_UNABLE_REGIST_NO "
						+ " LEFT OUTER JOIN T_CODE_DTL "
						+ "		ON	T_UNABLE_REGIST_NO.NO_TYPE	= T_CODE_DTL.CODE "
						+ "		AND	T_CODE_DTL.CODE_TYPE		= '99' "
						+ " WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_UNABLE_REGIST_NO");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetCsvData(string pNoType,string pTermIdOrTel) {
		DataSet ds = new DataSet();
		try {
			conn = DbConnect();

			string sSql = "SELECT "
						+ "		NO_TYPE						,"
						+ "		TERMINAL_UNIQUE_ID_OR_TEL	,"
						+ "		REMARKS						"
						+ " FROM "
						+ "		T_UNABLE_REGIST_NO ";
			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pNoType,pTermIdOrTel,ref sWhere);
			sSql += sWhere;
			sSql += " ORDER BY NO_TYPE,TERMINAL_UNIQUE_ID_OR_TEL ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_UNABLE_REGIST_NO");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(string pNoType,string pTermIdOrTel,ref string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();

		if (!pNoType.Equals("")) {
			SysPrograms.SqlAppendWhere("NO_TYPE = :NO_TYPE ",ref pWhere);
			list.Add(new OracleParameter("AD_CD",pNoType));
		}

		if (!pTermIdOrTel.Equals("")) {
			SysPrograms.SqlAppendWhere("TERMINAL_UNIQUE_ID_OR_TEL LIKE :TERMINAL_UNIQUE_ID_OR_TEL||'%' ",ref pWhere);
			list.Add(new OracleParameter("TERMINAL_UNIQUE_ID_OR_TEL",pTermIdOrTel));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}
