﻿/*------------------------------------------------------------------------

  Date        Updater		Update Explain


-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Text;
using System.Data.Common;
using System.Collections;
using iBridCommLib;
using ViComm;

public class MailTotal : DbSession
{
    public MailTotal()
    {
    }

    public int GetPageCount(string pSiteCd, string pReportDateFrom, string pReportDateTo, int pOffsetCount)
    {
        DataSet ds;
        DataRow dr;
        int iPages = 0;
        try
        {
            conn = DbConnect();

            StringBuilder sSql = new StringBuilder("SELECT COUNT(*) AS ROW_COUNT FROM (").AppendLine();
            OracleParameter[] oParamsWhere;
            OracleParameter[] oParamsHaving;
            sSql.Append(CreateMainQuery(pSiteCd, pReportDateFrom, pReportDateTo, pOffsetCount, out oParamsWhere, out oParamsHaving));
            sSql.AppendLine(")");

            using (cmd = CreateSelectCommand(sSql.ToString(), conn))
            using (da = new OracleDataAdapter(cmd))
            using (ds = new DataSet())
            {
                cmd.BindByName = true;
                for (int i = 0; i < oParamsWhere.Length; i++)
                {
                    cmd.Parameters.Add((OracleParameter)oParamsWhere[i]);
                }
                for (int i = 0; i < oParamsHaving.Length; i++)
                {
                    cmd.Parameters.Add((OracleParameter)oParamsHaving[i]);
                }

                da.Fill(ds);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    dr = ds.Tables[0].Rows[0];
                    iPages = int.Parse(dr["ROW_COUNT"].ToString());
                }
            }
        }
        finally
        {
            conn.Close();
        }
        return iPages;
    }

    public DataSet GetPageCollection(string pSiteCd, string pReportDateFrom, string pReportDateTo, int pOffsetCount, int startRowIndex, int maximumRows)
    {
        DataSet ds;
        try
        {
            conn = DbConnect();
            ds = new DataSet();

            StringBuilder sSql = new StringBuilder();
            OracleParameter[] oParamsWhere;
            OracleParameter[] oParamsHaving;
            // Main-Query
            sSql.Append(CreateMainQuery(pSiteCd, pReportDateFrom, pReportDateTo, pOffsetCount, out oParamsWhere, out oParamsHaving));
            // ORDER BY
            sSql.AppendLine("ORDER BY COUNT(MAIL_SEQ) DESC");
            
            using (cmd = CreateSelectCommand(sSql.ToString(), conn))
            {
                cmd.BindByName = true;
                for (int i = 0; i < oParamsWhere.Length; i++)
                {
                    cmd.Parameters.Add((OracleParameter)oParamsWhere[i]);
                }
                for (int i = 0; i < oParamsHaving.Length; i++)
                {
                    cmd.Parameters.Add((OracleParameter)oParamsHaving[i]);
                }
                using (da = new OracleDataAdapter(cmd))
                {
                    da.Fill(ds);
                }
            }
        }
        finally
        {
            conn.Close();
        }
        return ds;
    }

    private string CreateMainQuery(string pSiteCd, string pReportDateFrom, string pReportDateTo, int pOffsetCount, out OracleParameter[] oParamsWhere, out OracleParameter[] oParamsHaving)
    {
        StringBuilder sbMainQuery = new StringBuilder();
        // SELECT
        sbMainQuery.Append("SELECT TX_LOGIN_ID, CAST_LOGIN_ID, CAST_HANDLE_NM, COUNT(MAIL_SEQ) MAILCOUNT").AppendLine();
        // FROM
        sbMainQuery.Append("FROM T_MAIL_LOG").AppendLine();
        // WHERE
        StringBuilder sbWhere = new StringBuilder();
        sbWhere.AppendLine("WHERE TX_LOGIN_ID = CAST_LOGIN_ID");
        sbWhere.Append("AND MAIL_TYPE = '").Append(ViCommConst.MAIL_TP_APPROACH_TO_USER).Append("'").AppendLine();
        sbWhere.Append("AND BATCH_MAIL_FLAG = ").Append(ViCommConst.FLAG_OFF).AppendLine();
        string sWhere = sbWhere.ToString();
        oParamsWhere = CreateWhere(pSiteCd, pReportDateFrom, pReportDateTo, ref sWhere);
        sbMainQuery.AppendLine(sWhere);
        // HAVING
        StringBuilder sbHaving = new StringBuilder();
        sbHaving.Append("HAVING ");
        string sHaving = sbHaving.ToString();
        oParamsHaving = CreateHaving(pOffsetCount, ref sHaving);
        sbMainQuery.AppendLine(sHaving);
        // GROUP BY
        sbMainQuery.AppendLine("GROUP BY TX_LOGIN_ID, CAST_LOGIN_ID, CAST_HANDLE_NM");

        return sbMainQuery.ToString();
    }

	private OracleParameter[] CreateWhere(string pSiteCd, string pReportDateFrom, string pReportDateTo, ref string pWhere)
    {
        ArrayList list = new ArrayList();
		
		if (pSiteCd.Equals("") == false) {
			SysPrograms.SqlAppendWhere("TX_SITE_CD = :pTX_SITE_CD", ref pWhere);
            list.Add(new OracleParameter("pTX_SITE_CD", OracleDbType.Varchar2, pSiteCd, ParameterDirection.Input));
		}

        SysPrograms.SqlAppendWhere("CREATE_DATE BETWEEN :pCREATE_DATE_FROM AND :pCREATE_DATE_TO", ref pWhere);

        DateTime dtFrom = DateTime.MinValue;
        DateTime dtTo = DateTime.MaxValue;
        if (!string.IsNullOrEmpty(pReportDateFrom))
        {
            dtFrom = DateTime.Parse(pReportDateFrom + " 00:00:00");
        }
        if (!string.IsNullOrEmpty(pReportDateTo))
        {
            dtTo = DateTime.Parse(pReportDateTo + " 00:00:00");
            dtTo = dtTo.AddDays(1);
        }
        list.Add(new OracleParameter("pCREATE_DATE_FROM", OracleDbType.Date, dtFrom, ParameterDirection.Input));
        list.Add(new OracleParameter("pCREATE_DATE_TO", OracleDbType.Date, dtTo, ParameterDirection.Input));

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	private OracleParameter[] CreateHaving(int pOffsetCount, ref string pHaving)
    {
		ArrayList list = new ArrayList();

        StringBuilder sbHaving = new StringBuilder(pHaving);
        if (string.IsNullOrEmpty(pHaving))
        {
            sbHaving.Append(" HAVING ");
        }

        sbHaving.Append("COUNT(MAIL_SEQ) >= :pOFFSET_COUNT");
        list.Add(new OracleParameter("pOFFSET_COUNT", OracleDbType.Int32, pOffsetCount, ParameterDirection.Input));

        pHaving = sbHaving.ToString();
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}


}
