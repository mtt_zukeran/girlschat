﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 売上集計

--	Progaram ID		: DailySales
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class DailySales:DbSession {

	public DailySales() {
	}

	public DataSet DailySalesInquiry(string pSiteCd,string pYYYY,string pMM,int pSettleCampanyMask) {

		DataColumn col;
		DataSet ds = new DataSet();
		DataTable dt = new DataTable();

		col = new DataColumn("SITE_CD",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("SALES_DAY",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("PRINT_DAY",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("PRINT_LINK",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("SALES_DAY_OF_WEEK",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("CASH_SALES_COUNT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("CASH_SALES_AMT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("PREPAID_SALES_COUNT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("PREPAID_SALES_AMT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("CREDIT_PACK_SALES_COUNT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("CREDIT_PACK_SALES_AMT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("CCHECK_SALES_COUNT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("CCHECK_SALES_AMT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("BITCASH_SALES_COUNT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("BITCASH_SALES_AMT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("SMONEY_SALES_COUNT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("SMONEY_SALES_AMT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("GMONEY_SALES_COUNT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("GMONEY_SALES_AMT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("CONVINI_SALES_COUNT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("CONVINI_SALES_AMT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("CREDIT_SALES_COUNT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("CREDIT_SALES_AMT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("POINT_AFFILIATE_SALES_COUNT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("POINT_AFFILIATE_SALES_AMT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("ADULTID_SALES_COUNT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("ADULTID_SALES_AMT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("CONVINI_DIRECT_SALES_COUNT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("CONVINI_DIRECT_SALES_AMT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("EDY_SALES_COUNT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("EDY_SALES_AMT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("PAY_AFTER_PACK_SALES_COUNT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("PAY_AFTER_PACK_SALES_AMT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("MOBILE_CASH_SALES_COUNT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("MOBILE_CASH_SALES_AMT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("ZERO_SETTLE_SALES_COUNT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("ZERO_SETTLE_SALES_AMT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("GIGA_POINT_SALES_COUNT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("GIGA_POINT_SALES_AMT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("AUTO_RECEIPT_SALES_COUNT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("AUTO_RECEIPT_SALES_AMT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("RAKUTEN_SALES_COUNT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("RAKUTEN_SALES_AMT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);		
		col = new DataColumn("TOTAL_SALES_COUNT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("TOTAL_SALES_AMT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SALES_INQUIRY_DAY");
			db.ProcedureInParm("PSITE_CD",DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PYYYY",DbType.VARCHAR2,pYYYY);
			db.ProcedureInParm("PMM",DbType.VARCHAR2,pMM);
			db.ProcedureInParm("PSETTLE_CAMPANY_MASK",DbType.NUMBER,pSettleCampanyMask);
			db.ProcedureOutArrayParm("PSALES_DAY",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_DAY_OF_WEEK",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PCASH_SALES_COUNT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PCASH_SALES_AMT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PPREPAID_SALES_COUNT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PPREPAID_SALES_AMT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PCREDIT_PACK_SALES_COUNT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PCREDIT_PACK_SALES_AMT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PCCHECK_SALES_COUNT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PCCHECK_SALES_AMT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PBITCASH_SALES_COUNT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PBITCASH_SALES_AMT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSMONEY_SALES_COUNT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSMONEY_SALES_AMT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PCONVINI_SALES_COUNT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PCONVINI_SALES_AMT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PCREDIT_SALES_COUNT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PCREDIT_SALES_AMT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PPOINT_AFFILIATE_SALES_COUNT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PPOINT_AFFILIATE_SALES_AMT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PADULTID_SALES_COUNT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PADULTID_SALES_AMT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PCONVINI_DIRECT_SALES_COUNT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PCONVINI_DIRECT_SALES_AMT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PEDY_SALES_COUNT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PEDY_SALES_AMT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PGMONEY_SALES_COUNT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PGMONEY_SALES_AMT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PPAY_AFTER_PACK_SALES_COUNT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PPAY_AFTER_PACK_SALES_AMT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PMOBILE_CASH_SALES_COUNT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PMOBILE_CASH_SALES_AMT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PZERO_SETTLE_SALES_COUNT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PZERO_SETTLE_SALES_AMT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PGIGA_POINT_SALES_COUNT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PGIGA_POINT_SALES_AMT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PAUTO_RECEIPT_SALES_COUNT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PAUTO_RECEIPT_SALES_AMT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PRAKUTEN_SALES_COUNT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PRAKUTEN_SALES_AMT",DbSession.DbType.VARCHAR2,100);			
			db.ProcedureOutArrayParm("PTOTAL_SALES_COUNT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PTOTAL_SALES_AMT",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();

			for (int i = 0;i < db.GetIntValue("PRECORD_COUNT");i++) {
				string sDay = db.GetArryStringValue("PSALES_DAY",i);
				DataRow newRow = dt.NewRow();
				newRow["SITE_CD"] = pSiteCd;
				newRow["SALES_DAY"] = sDay;
				newRow["PRINT_DAY"] = sDay.Substring(8,2) + "日";
				newRow["PRINT_LINK"] = sDay.Substring(8,2) + "日のアクセス状況";
				newRow["SALES_DAY_OF_WEEK"] = db.GetArryStringValue("PSALES_DAY_OF_WEEK",i);
				newRow["CASH_SALES_COUNT"] = db.GetArryStringValue("PCASH_SALES_COUNT",i);
				newRow["CASH_SALES_AMT"] = db.GetArryStringValue("PCASH_SALES_AMT",i);
				newRow["PREPAID_SALES_COUNT"] = db.GetArryStringValue("PPREPAID_SALES_COUNT",i);
				newRow["PREPAID_SALES_AMT"] = db.GetArryStringValue("PPREPAID_SALES_AMT",i);
				newRow["CREDIT_PACK_SALES_COUNT"] = db.GetArryStringValue("PCREDIT_PACK_SALES_COUNT",i);
				newRow["CREDIT_PACK_SALES_AMT"] = db.GetArryStringValue("PCREDIT_PACK_SALES_AMT",i);
				newRow["CCHECK_SALES_COUNT"] = db.GetArryStringValue("PCCHECK_SALES_COUNT",i);
				newRow["CCHECK_SALES_AMT"] = db.GetArryStringValue("PCCHECK_SALES_AMT",i);
				newRow["BITCASH_SALES_COUNT"] = db.GetArryStringValue("PBITCASH_SALES_COUNT",i);
				newRow["BITCASH_SALES_AMT"] = db.GetArryStringValue("PBITCASH_SALES_AMT",i);
				newRow["SMONEY_SALES_COUNT"] = db.GetArryStringValue("PSMONEY_SALES_COUNT",i);
				newRow["SMONEY_SALES_AMT"] = db.GetArryStringValue("PSMONEY_SALES_AMT",i);
				newRow["CONVINI_SALES_COUNT"] = db.GetArryStringValue("PCONVINI_SALES_COUNT",i);
				newRow["CONVINI_SALES_AMT"] = db.GetArryStringValue("PCONVINI_SALES_AMT",i);
				newRow["CREDIT_SALES_COUNT"] = db.GetArryStringValue("PCREDIT_SALES_COUNT",i);
				newRow["CREDIT_SALES_AMT"] = db.GetArryStringValue("PCREDIT_SALES_AMT",i);
				newRow["POINT_AFFILIATE_SALES_COUNT"] = db.GetArryStringValue("PPOINT_AFFILIATE_SALES_COUNT",i);
				newRow["POINT_AFFILIATE_SALES_AMT"] = db.GetArryStringValue("PPOINT_AFFILIATE_SALES_AMT",i);
				newRow["ADULTID_SALES_COUNT"] = db.GetArryStringValue("PADULTID_SALES_COUNT",i);
				newRow["ADULTID_SALES_AMT"] = db.GetArryStringValue("PADULTID_SALES_AMT",i);
				newRow["CONVINI_DIRECT_SALES_COUNT"] = db.GetArryStringValue("PCONVINI_DIRECT_SALES_COUNT",i);
				newRow["CONVINI_DIRECT_SALES_AMT"] = db.GetArryStringValue("PCONVINI_DIRECT_SALES_AMT",i);
				newRow["EDY_SALES_COUNT"] = db.GetArryStringValue("PEDY_SALES_COUNT",i);
				newRow["EDY_SALES_AMT"] = db.GetArryStringValue("PEDY_SALES_AMT",i);
				newRow["GMONEY_SALES_COUNT"] = db.GetArryStringValue("PGMONEY_SALES_COUNT",i);
				newRow["GMONEY_SALES_AMT"] = db.GetArryStringValue("PGMONEY_SALES_AMT",i);
				newRow["PAY_AFTER_PACK_SALES_COUNT"] = db.GetArryStringValue("PPAY_AFTER_PACK_SALES_COUNT",i);
				newRow["PAY_AFTER_PACK_SALES_AMT"] = db.GetArryStringValue("PPAY_AFTER_PACK_SALES_AMT",i);
				newRow["MOBILE_CASH_SALES_COUNT"] = db.GetArryStringValue("PMOBILE_CASH_SALES_COUNT",i);
				newRow["MOBILE_CASH_SALES_AMT"] = db.GetArryStringValue("PMOBILE_CASH_SALES_AMT",i);
				newRow["ZERO_SETTLE_SALES_COUNT"] = db.GetArryStringValue("PZERO_SETTLE_SALES_COUNT",i);
				newRow["ZERO_SETTLE_SALES_AMT"] = db.GetArryStringValue("PZERO_SETTLE_SALES_AMT",i);
				newRow["GIGA_POINT_SALES_COUNT"] = db.GetArryStringValue("PGIGA_POINT_SALES_COUNT",i);
				newRow["GIGA_POINT_SALES_AMT"] = db.GetArryStringValue("PGIGA_POINT_SALES_AMT",i);
				newRow["AUTO_RECEIPT_SALES_COUNT"] = db.GetArryStringValue("PAUTO_RECEIPT_SALES_COUNT",i);
				newRow["AUTO_RECEIPT_SALES_AMT"] = db.GetArryStringValue("PAUTO_RECEIPT_SALES_AMT",i);
				newRow["RAKUTEN_SALES_COUNT"] = db.GetArryStringValue("PRAKUTEN_SALES_COUNT",i);
				newRow["RAKUTEN_SALES_AMT"] = db.GetArryStringValue("PRAKUTEN_SALES_AMT",i);
				newRow["TOTAL_SALES_COUNT"] = db.GetArryStringValue("PTOTAL_SALES_COUNT",i);
				newRow["TOTAL_SALES_AMT"] = db.GetArryStringValue("PTOTAL_SALES_AMT",i);
				dt.Rows.Add(newRow);
			}
		}

		ds.Tables.Add(dt);

		return ds;
	}

	public DataSet DailyAdSalesInquiry(
		string pSiteCd,
		string pFromYYYY,
		string pFromMM,
		string pFromDD,
		string pToYYYY,
		string pToMM,
		string pToDD,
		int pSettleCampanyMask,
		string pAdCd

	) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sFromDay = pFromYYYY + '/' + pFromMM + '/' + pFromDD;
			string sToDay = pToYYYY + '/' + pToMM + '/' + pToDD;
			string sAdCd = pAdCd + '%';

			string sTotalCount = "";
			string sTotalAmt = "";
			bool totalFlg = false;

			string sSql = "SELECT " +
					"T_AD.AD_NM							," +
					"T_SUM.SITE_CD						," +
					"T_SUM.AD_CD						," +
					"T_SUM.CASH_SALES_COUNT				," +
					"T_SUM.CASH_SALES_AMT				," +
					"T_SUM.PREPAID_SALES_COUNT			," +
					"T_SUM.PREPAID_SALES_AMT			," +
					"T_SUM.CREDIT_PACK_SALES_COUNT		," +
					"T_SUM.CREDIT_PACK_SALES_AMT		," +
					"T_SUM.CCHECK_SALES_COUNT			," +
					"T_SUM.CCHECK_SALES_AMT				," +
					"T_SUM.BITCASH_SALES_COUNT			," +
					"T_SUM.BITCASH_SALES_AMT			," +
					"T_SUM.SMONEY_SALES_COUNT			," +
					"T_SUM.SMONEY_SALES_AMT				," +
					"T_SUM.CONVINI_SALES_COUNT			," +
					"T_SUM.CONVINI_SALES_AMT			," +
					"T_SUM.CREDIT_SALES_COUNT			," +
					"T_SUM.CREDIT_SALES_AMT				," +
					"T_SUM.POINT_AFFILIATE_SALES_COUNT	," +
					"T_SUM.POINT_AFFILIATE_SALES_AMT	," +
					"T_SUM.ADULTID_SALES_COUNT			," +
					"T_SUM.ADULTID_SALES_AMT			," +
					"T_SUM.CONVINI_DIRECT_SALES_COUNT	," +
					"T_SUM.CONVINI_DIRECT_SALES_AMT		," +
					"T_SUM.EDY_SALES_COUNT				," +
					"T_SUM.EDY_SALES_AMT				," +
					"T_SUM.GMONEY_SALES_COUNT			," +
					"T_SUM.GMONEY_SALES_AMT				," +
					"T_SUM.MOBILE_CASH_SALES_COUNT		," +
					"T_SUM.MOBILE_CASH_SALES_AMT		," +
					"T_SUM.PAY_AFTER_PACK_SALES_COUNT	," +
					"T_SUM.PAY_AFTER_PACK_SALES_AMT		," +
					"T_SUM.ZERO_SETTLE_SALES_COUNT		," +
					"T_SUM.ZERO_SETTLE_SALES_AMT		," +
					"T_SUM.GIGA_POINT_SALES_COUNT		," +
					"T_SUM.GIGA_POINT_SALES_AMT			," +
					"T_SUM.AUTO_RECEIPT_SALES_COUNT		," +
					"T_SUM.AUTO_RECEIPT_SALES_AMT		," +
					"T_SUM.RAKUTEN_SALES_COUNT			," +
					"T_SUM.RAKUTEN_SALES_AMT			," +					
					"T_SUM.TOTAL_SALES_COUNT			," +
					"T_SUM.TOTAL_SALES_AMT				" +
				"FROM " +
					"T_AD,(" +
				"SELECT ";

			sSql += "AD_CD															," +
					"SITE_CD														," +
					"SUM(CASH_SALES_COUNT			) AS CASH_SALES_COUNT			," +
					"SUM(CASH_SALES_AMT				) AS CASH_SALES_AMT				," +
					"SUM(PREPAID_SALES_COUNT		) AS PREPAID_SALES_COUNT		," +
					"SUM(PREPAID_SALES_AMT			) AS PREPAID_SALES_AMT			," +
					"SUM(CREDIT_PACK_SALES_COUNT	) AS CREDIT_PACK_SALES_COUNT	," +
					"SUM(CREDIT_PACK_SALES_AMT		) AS CREDIT_PACK_SALES_AMT		," +
					"SUM(CCHECK_SALES_COUNT			) AS CCHECK_SALES_COUNT			," +
					"SUM(CCHECK_SALES_AMT			) AS CCHECK_SALES_AMT			," +
					"SUM(BITCASH_SALES_COUNT		) AS BITCASH_SALES_COUNT		," +
					"SUM(BITCASH_SALES_AMT			) AS BITCASH_SALES_AMT			," +
					"SUM(SMONEY_SALES_COUNT			) AS SMONEY_SALES_COUNT			," +
					"SUM(SMONEY_SALES_AMT			) AS SMONEY_SALES_AMT			," +
					"SUM(CONVINI_SALES_COUNT		) AS CONVINI_SALES_COUNT		," +
					"SUM(CONVINI_SALES_AMT			) AS CONVINI_SALES_AMT			," +
					"SUM(CREDIT_SALES_COUNT			) AS CREDIT_SALES_COUNT			," +
					"SUM(CREDIT_SALES_AMT			) AS CREDIT_SALES_AMT			," +
					"SUM(POINT_AFFILIATE_SALES_COUNT) AS POINT_AFFILIATE_SALES_COUNT," +
					"SUM(POINT_AFFILIATE_SALES_AMT	) AS POINT_AFFILIATE_SALES_AMT	," +
					"SUM(ADULTID_SALES_COUNT		) AS ADULTID_SALES_COUNT		," +
					"SUM(ADULTID_SALES_AMT			) AS ADULTID_SALES_AMT			," +
					"SUM(CONVINI_DIRECT_SALES_COUNT	) AS CONVINI_DIRECT_SALES_COUNT	," +
					"SUM(CONVINI_DIRECT_SALES_AMT	) AS CONVINI_DIRECT_SALES_AMT	," +
					"SUM(EDY_SALES_COUNT			) AS EDY_SALES_COUNT			," +
					"SUM(EDY_SALES_AMT				) AS EDY_SALES_AMT				," +
					"SUM(GMONEY_SALES_COUNT			) AS GMONEY_SALES_COUNT			," +
					"SUM(GMONEY_SALES_AMT			) AS GMONEY_SALES_AMT			," +
					"SUM(MOBILE_CASH_SALES_COUNT	) AS MOBILE_CASH_SALES_COUNT	," +
					"SUM(MOBILE_CASH_SALES_AMT		) AS MOBILE_CASH_SALES_AMT		," +
					"SUM(PAY_AFTER_PACK_SALES_COUNT	) AS PAY_AFTER_PACK_SALES_COUNT	," +
					"SUM(PAY_AFTER_PACK_SALES_AMT	) AS PAY_AFTER_PACK_SALES_AMT	," +
					"SUM(ZERO_SETTLE_SALES_COUNT	) AS ZERO_SETTLE_SALES_COUNT	," +
					"SUM(ZERO_SETTLE_SALES_AMT		) AS ZERO_SETTLE_SALES_AMT		," +
					"SUM(GIGA_POINT_SALES_COUNT		) AS GIGA_POINT_SALES_COUNT		," +
					"SUM(GIGA_POINT_SALES_AMT		) AS GIGA_POINT_SALES_AMT		," +
					"SUM(AUTO_RECEIPT_SALES_COUNT	) AS AUTO_RECEIPT_SALES_COUNT	," +
					"SUM(AUTO_RECEIPT_SALES_AMT		) AS AUTO_RECEIPT_SALES_AMT		," +
					"SUM(RAKUTEN_SALES_COUNT		) AS RAKUTEN_SALES_COUNT		," +
					"SUM(RAKUTEN_SALES_AMT			) AS RAKUTEN_SALES_AMT			,";

			if ((pSettleCampanyMask & ViCommConst.MSK_SETTLE_CASH) > 0) {
				sTotalCount += "CASH_SALES_COUNT +";
				sTotalAmt += "CASH_SALES_AMT +";
				totalFlg = true;
			}
			if ((pSettleCampanyMask & ViCommConst.MSK_SETTLE_PREPAID) > 0) {
				sTotalCount += "PREPAID_SALES_COUNT +";
				sTotalAmt += "PREPAID_SALES_AMT +";
				totalFlg = true;
			}
			if ((pSettleCampanyMask & ViCommConst.MSK_SETTLE_CREDIT_PACK) > 0) {
				sTotalCount += "CREDIT_PACK_SALES_COUNT +";
				sTotalAmt += "CREDIT_PACK_SALES_AMT +";
				totalFlg = true;
			}
			if ((pSettleCampanyMask & ViCommConst.MSK_SETTLE_C_CHECK) > 0) {
				sTotalCount += "CCHECK_SALES_COUNT +";
				sTotalAmt += "CCHECK_SALES_AMT +";
				totalFlg = true;
			}
			if ((pSettleCampanyMask & ViCommConst.MSK_SETTLE_BITCASH) > 0) {
				sTotalCount += "BITCASH_SALES_COUNT +";
				sTotalAmt += "BITCASH_SALES_AMT +";
				totalFlg = true;
			}
			if ((pSettleCampanyMask & ViCommConst.MSK_SETTLE_S_MONEY) > 0) {
				sTotalCount += "SMONEY_SALES_COUNT +";
				sTotalAmt += "SMONEY_SALES_AMT +";
				totalFlg = true;
			}
			if ((pSettleCampanyMask & ViCommConst.MSK_SETTLE_CVSDL) > 0) {
				sTotalCount += "CONVINI_SALES_COUNT +";
				sTotalAmt += "CONVINI_SALES_AMT +";
				totalFlg = true;
			}
			if ((pSettleCampanyMask & ViCommConst.MSK_SETTLE_CREDIT) > 0) {
				sTotalCount += "CREDIT_SALES_COUNT +";
				sTotalAmt += "CREDIT_SALES_AMT +";
				totalFlg = true;
			}
			if ((pSettleCampanyMask & ViCommConst.MSK_SETTLE_POINT_AFFILIATE) > 0) {
				sTotalCount += "POINT_AFFILIATE_SALES_COUNT +";
				sTotalAmt += "POINT_AFFILIATE_SALES_AMT +";
				totalFlg = true;
			}
			if ((pSettleCampanyMask & ViCommConst.MSK_SETTLE_ADULTID) > 0) {
				sTotalCount += "ADULTID_SALES_COUNT +";
				sTotalAmt += "ADULTID_SALES_AMT +";
				totalFlg = true;
			}
			if ((pSettleCampanyMask & ViCommConst.MSK_SETTLE_CVS_DIRECT) > 0) {
				sTotalCount += "CONVINI_DIRECT_SALES_COUNT +";
				sTotalAmt += "CONVINI_DIRECT_SALES_AMT +";
				totalFlg = true;
			}
			if ((pSettleCampanyMask & ViCommConst.MSK_SETTLE_EDY) > 0) {
				sTotalCount += "EDY_SALES_COUNT +";
				sTotalAmt += "EDY_SALES_AMT +";
				totalFlg = true;
			}
			if ((pSettleCampanyMask & ViCommConst.MSK_SETTLE_G_MONEY) > 0) {
				sTotalCount += "GMONEY_SALES_COUNT +";
				sTotalAmt += "GMONEY_SALES_AMT +";
				totalFlg = true;
			}
			if ((pSettleCampanyMask & ViCommConst.MSK_SETTLE_MOBILE_CASH) > 0) {
				sTotalCount += "MOBILE_CASH_SALES_COUNT +";
				sTotalAmt += "MOBILE_CASH_SALES_AMT +";
				totalFlg = true;
			}
			if ((pSettleCampanyMask & ViCommConst.MSK_SETTLE_CREDIT_AUTH) > 0) {
				sTotalCount += "ZERO_SETTLE_SALES_COUNT +";
				sTotalAmt += "ZERO_SETTLE_SALES_AMT +";
				totalFlg = true;
			}
			if ((pSettleCampanyMask & ViCommConst.MSK_SETTLE_GIGA_POINT) > 0) {
				sTotalCount += "GIGA_POINT_SALES_COUNT +";
				sTotalAmt += "GIGA_POINT_SALES_AMT +";
				totalFlg = true;
			}

			if ((pSettleCampanyMask & ViCommConst.MSK_SETTLE_AUTO_RECEIPT) > 0) {
				sTotalCount += "AUTO_RECEIPT_SALES_COUNT +";
				sTotalAmt += "AUTO_RECEIPT_SALES_AMT +";
				totalFlg = true;
			}
			if ((pSettleCampanyMask & ViCommConst.MSK_SETTLE_RAKUTEN) > 0) {
				sTotalCount += "RAKUTEN_SALES_COUNT +";
				sTotalAmt += "RAKUTEN_SALES_AMT +";
				totalFlg = true;
			}
			/*後払パックは合計に加算しない			if ((pSettleCampanyMask & ViCommConst.MSK_SETTLE_PAY_AFLTER_PACK) > 0) {
				sTotalCount += "PAY_AFTER_PACK_SALES_COUNT +";
				sTotalAmt += "PAY_AFTER_PACK_SALES_AMT +";
				totalFlg = true;
			}
			*/

			// 合計対象が存在する場合


			if (totalFlg) {
				// 最後の"+"を除去する
				sTotalCount = sTotalCount.Remove(sTotalCount.LastIndexOf("+"),1);
				sTotalAmt = sTotalAmt.Remove(sTotalAmt.LastIndexOf("+"),1);
			} else {
				sTotalCount = "0";
				sTotalAmt = "0";
			}

			sSql += "SUM(" + sTotalCount + ") AS TOTAL_SALES_COUNT      ,";
			sSql += "SUM(" + sTotalAmt + ") AS TOTAL_SALES_AMT        " +
					"FROM " +
						"T_DAILY_AD_SALES " +
					"WHERE ";

			if (!iBridUtil.GetStringValue(pSiteCd).Equals("")) {
				sSql += "SITE_CD= :SITE_CD	AND ";
			}

			sSql += "AD_CD like :AD_CD  AND ";

			sSql += "REPORT_DAY  >= :REPORT_DAY_FROM  AND REPORT_DAY  <= :REPORT_DAY_TO  ";

			sSql += " GROUP BY SITE_CD, AD_CD " +
							" ORDER BY SITE_CD, AD_CD) T_SUM,T_SITE " +
						"WHERE " +
							"       T_AD.AD_CD      = T_SUM.AD_CD    " +
							"AND    T_SUM.SITE_CD   = T_SITE.SITE_CD " +
						"ORDER BY T_SUM.SITE_CD,T_SUM.AD_CD     ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				if (!iBridUtil.GetStringValue(pSiteCd).Equals("")) {
					cmd.Parameters.Add("SITE_CD",pSiteCd);
				}

				cmd.Parameters.Add("AD_CD",sAdCd);
				cmd.Parameters.Add("REPORT_DATE_FROM",sFromDay);
				cmd.Parameters.Add("REPORT_DAY_TO",sToDay);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}

		} finally {
			conn.Close();
		}

		return ds;
	}
}
