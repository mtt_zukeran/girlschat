﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: レベルアップ必要経験値
--	Progaram ID		: LevelUpExp
--
--  Creation Date	: 2011.07.25
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;


public class LevelUpExp : DbSession {

	public LevelUpExp() {
	}

	public int GetPageCount(string pSiteCd,string pSexCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT COUNT(*) FROM T_LEVEL_UP_EXP ");

		string sWhereClause;
		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pSexCd,out sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		}
		finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pSexCd,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	SITE_CD					,");
		oSqlBuilder.AppendLine("	GAME_CHARACTER_LEVEL	,");
		oSqlBuilder.AppendLine("	SEX_CD					,");
		oSqlBuilder.AppendLine("	NEED_EXPERIENCE_POINT	,");
		oSqlBuilder.AppendLine("	SUM(NEED_EXPERIENCE_POINT) OVER(ORDER BY SITE_CD, GAME_CHARACTER_LEVEL, SEX_CD) AS TOTAL_NEED_EXPERIENCE_POINT	,");
		oSqlBuilder.AppendLine("	CHARACTER_HP			,");
		oSqlBuilder.AppendLine("	FELLOW_COUNT_LIMIT		,");
		oSqlBuilder.AppendLine("	ADD_FORCE_COUNT			,");
		oSqlBuilder.AppendLine("	REVISION_NO				,");
		oSqlBuilder.AppendLine("	UPDATE_DATE				");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_LEVEL_UP_EXP	");

		string sWhereClause;
		string sPagingSql;
		string sSortExpression = "ORDER BY SITE_CD, GAME_CHARACTER_LEVEL, SEX_CD";

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pSexCd,out sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		}
		finally {
			conn.Close();
		}
		return oDs;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pSexCd,out string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}
		if (!string.IsNullOrEmpty(pSexCd)) {
			SysPrograms.SqlAppendWhere("SEX_CD = :SEX_CD",ref pWhere);
			list.Add(new OracleParameter("SEX_CD",pSexCd));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}