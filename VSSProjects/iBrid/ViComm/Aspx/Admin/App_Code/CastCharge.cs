/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: キャスト課金
--	Progaram ID		: CastCharge
--
--  Creation Date	: 2009.07.23
--  Creater			: iBrid(Y.Inoue)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  2010/06/11  M.Horie    通常会員、未入金会員・課金ポイント5追加対応

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;

public class CastCharge:DbSession {

	public CastCharge() {
	}

	public int GetPageCount(string pSiteCd) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_CAST_CHARGE01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY  SITE_CD,ACT_CATEGORY_SEQ,DISPLAY_ORDER ";
			StringBuilder sSql = new StringBuilder();
			sSql.AppendLine("SELECT	");
			sSql.AppendLine("	SITE_CD						,");
			sSql.AppendLine("	SITE_NM						,");
			sSql.AppendLine("	CHARGE_TYPE					,");
			sSql.AppendLine("	ACT_CATEGORY_SEQ			,");
			sSql.AppendLine("	CHARGE_POINT_NORMAL			,");
			sSql.AppendLine("	CHARGE_POINT_NO_RECEIPT		,");
			sSql.AppendLine("	CHARGE_POINT_NORMAL2		,");
			sSql.AppendLine("	CHARGE_POINT_NO_RECEIPT2	,");
			sSql.AppendLine("	CHARGE_POINT_NORMAL3		,");
			sSql.AppendLine("	CHARGE_POINT_NO_RECEIPT3	,");
			sSql.AppendLine("	CHARGE_POINT_NORMAL4		,");
			sSql.AppendLine("	CHARGE_POINT_NO_RECEIPT4	,");
			sSql.AppendLine("	CHARGE_POINT_NORMAL5		,");
			sSql.AppendLine("	CHARGE_POINT_NO_RECEIPT5	,");
			sSql.AppendLine("	CHARGE_UNIT_SEC				,");
			sSql.AppendLine("	FREE_DIAL_FEE				,");
			sSql.AppendLine("	UPDATE_DATE					,");
			sSql.AppendLine("	REVISION_NO					,");
			sSql.AppendLine("	CHARGE_TYPE_NM				,");
			sSql.AppendLine("	CHARGE_UNIT_TYPE			,");
			sSql.AppendLine("	DISPLAY_ORDER				,");
			sSql.AppendLine("	ACT_CATEGORY_NM				,");
			sSql.AppendLine("	CAST_USE_TALK_APP_ADD_POINT	,");
			sSql.AppendLine("	MAN_USE_TALK_APP_ADD_POINT	");
			sSql.AppendLine("FROM(	");
			sSql.AppendLine("	SELECT VW_CAST_CHARGE01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_CAST_CHARGE01	");

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql.AppendLine(sWhere);

			sSql.AppendLine(")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ");
			sSql.AppendLine(sOrder);

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_CHARGE01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	public DataSet GetCsvData(string pSiteCd) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY SITE_CD,CHARGE_TYPE,ACT_CATEGORY_SEQ ";
			StringBuilder sSql = new StringBuilder();
			sSql.AppendLine("SELECT	");
			sSql.AppendLine("	SITE_CD				 		,");
			sSql.AppendLine("	CHARGE_TYPE			 		,");
			sSql.AppendLine("	ACT_CATEGORY_SEQ			,");
			sSql.AppendLine("	CATEGORY_INDEX				,");
			sSql.AppendLine("	CHARGE_POINT_NORMAL			,");
			sSql.AppendLine("	CHARGE_POINT_NO_RECEIPT		,");
			sSql.AppendLine("	CHARGE_POINT_NORMAL2		,");
			sSql.AppendLine("	CHARGE_POINT_NO_RECEIPT2	,");
			sSql.AppendLine("	CHARGE_POINT_NORMAL3		,");
			sSql.AppendLine("	CHARGE_POINT_NO_RECEIPT3	,");
			sSql.AppendLine("	CHARGE_POINT_NORMAL4		,");
			sSql.AppendLine("	CHARGE_POINT_NO_RECEIPT4	,");
			sSql.AppendLine("	CHARGE_POINT_NORMAL5		,");
			sSql.AppendLine("	CHARGE_POINT_NO_RECEIPT5	,");
			sSql.AppendLine("	CHARGE_UNIT_SEC				,");
			sSql.AppendLine("	FREE_DIAL_FEE				,");
			sSql.AppendLine("	CAST_USE_TALK_APP_ADD_POINT	,");
			sSql.AppendLine("	MAN_USE_TALK_APP_ADD_POINT	");
			sSql.AppendLine("FROM ");
			sSql.AppendLine("	VW_CAST_CHARGE01  ");

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql.AppendLine(sWhere);
			sSql.AppendLine(sOrder);

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_CHARGE01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		pWhere = pWhere + " WHERE SITE_CD = :SITE_CD  ";
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}


}
