﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 決済結果
--	Progaram ID		: SettleLog
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using System.Text;
using ViComm;
using System.Collections.Generic;

public class SettleLog:DbSession {

	public SettleLog() {
	}

	public int GetPageCount(string pSiteCd,string pReportDayFrom,string pReportDayTo,string pSettleType,string pLoginId,string pTel,string pSettleStatus,string pAdCd,string pSettleCompanyCd) {
		return GetPageCount(pSiteCd,pReportDayFrom,pReportDayTo,pSettleType,pLoginId,pTel,pSettleStatus,pAdCd,pSettleCompanyCd,string.Empty,string.Empty,string.Empty,ViCommConst.FLAG_OFF_STR,string.Empty);
	}

	public int GetPageCount(string pSiteCd,string pReportDayFrom,string pReportDayTo,string pSettleType,string pLoginId,string pTel,string pSettleStatus,string pAdCd,string pSettleCompanyCd,string pAnswerDayFrom,string pAnswerDayTo) {
		return GetPageCount(pSiteCd,pReportDayFrom,pReportDayTo,pSettleType,pLoginId,pTel,pSettleStatus,pAdCd,pSettleCompanyCd,pAnswerDayFrom,pAnswerDayTo,string.Empty,ViCommConst.FLAG_OFF_STR,string.Empty);
	}

	public int GetPageCount(string pSiteCd,string pReportDayFrom,string pReportDayTo,string pSettleType,string pLoginId,string pTel,string pSettleStatus,string pAdCd,string pSettleCompanyCd,string pAnswerDayFrom,string pAnswerDayTo,string pFanClubFeeFlag,string pWithoutCredit300Flag) {
		return GetPageCount(pSiteCd,pReportDayFrom,pReportDayTo,pSettleType,pLoginId,pTel,pSettleStatus,pAdCd,pSettleCompanyCd,pAnswerDayFrom,pAnswerDayTo,pFanClubFeeFlag,ViCommConst.FLAG_OFF_STR,pWithoutCredit300Flag);
	}

	public int GetPageCount(string pSiteCd,string pReportDayFrom,string pReportDayTo,string pSettleType,string pLoginId,string pTel,string pSettleStatus,string pAdCd,string pSettleCompanyCd,string pAnswerDayFrom,string pAnswerDayTo,string pFanClubFeeFlag,string pSettleCancelFlag,string pWithoutCredit300Flag) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_SETTLE_LOG01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pReportDayFrom,pReportDayTo,pSettleType,pLoginId,pTel,pSettleStatus,pAdCd,pSettleCompanyCd,pAnswerDayFrom,pAnswerDayTo,pFanClubFeeFlag,pSettleCancelFlag,pWithoutCredit300Flag,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pReportDayFrom,string pReportDayTo,string pSettleType,string pLoginId,string pTel,string pSettleStatus,string pAdCd,string pSettleCompanyCd,int startRowIndex,int maximumRows) {
		return GetPageCollection(
			pSiteCd,
			pReportDayFrom,
			pReportDayTo,
			pSettleType,
			pLoginId,
			pTel,
			pSettleStatus,
			pAdCd,
			pSettleCompanyCd,
			string.Empty,
			string.Empty,
			string.Empty,
			ViCommConst.FLAG_OFF_STR,
			string.Empty,
			startRowIndex,
			maximumRows
		);
	}

	public DataSet GetPageCollection(string pSiteCd,string pReportDayFrom,string pReportDayTo,string pSettleType,string pLoginId,string pTel,string pSettleStatus,string pAdCd,string pSettleCompanyCd,string pAnswerDayFrom,string pAnswerDayTo,int startRowIndex,int maximumRows) {
		return GetPageCollection(
			pSiteCd,
			pReportDayFrom,
			pReportDayTo,
			pSettleType,
			pLoginId,
			pTel,
			pSettleStatus,
			pAdCd,
			pSettleCompanyCd,
			pAnswerDayFrom,
			pAnswerDayTo,
			string.Empty,
			ViCommConst.FLAG_OFF_STR,
			string.Empty,
			startRowIndex,
			maximumRows
		);
	}

	public DataSet GetPageCollection(string pSiteCd,string pReportDayFrom,string pReportDayTo,string pSettleType,string pLoginId,string pTel,string pSettleStatus,string pAdCd,string pSettleCompanyCd,string pAnswerDayFrom,string pAnswerDayTo,string pFanClubFeeFlag,string pWithoutCredit300Flag,int startRowIndex,int maximumRows) {
		return GetPageCollection(
			pSiteCd,
			pReportDayFrom,
			pReportDayTo,
			pSettleType,
			pLoginId,
			pTel,
			pSettleStatus,
			pAdCd,
			pSettleCompanyCd,
			pAnswerDayFrom,
			pAnswerDayTo,
			pFanClubFeeFlag,
			ViCommConst.FLAG_OFF_STR,
			pWithoutCredit300Flag,
			startRowIndex,
			maximumRows
		);
	}

	public DataSet GetPageCollection(
		string pSiteCd,
		string pReportDayFrom,
		string pReportDayTo,
		string pSettleType,
		string pLoginId,
		string pTel,
		string pSettleStatus,
		string pAdCd,
		string pSettleCompanyCd,
		string pAnswerDayFrom,
		string pAnswerDayTo,
		string pFanClubFeeFlag,
		string pSettleCancelFlag,
		string pWithoutCredit300Flag,
		int startRowIndex,
		int maximumRows
	) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY CREATE_DATE DESC,SETTLE_SEQ DESC ";
			string sSql = "SELECT " +
							"SETTLE_SEQ				," +
							"SETTLE_COMPANY_CD		," +
							"SITE_CD				," +
							"USER_SEQ				," +
							"SID					," +
							"SETTLE_STATUS			," +
							"SETTLE_POINT			," +
							"SETTLE_AMT				," +
							"SETTLE_ID				," +
							"CREATE_DATE			," +
							"TRANS_DATE				," +
							"ANSWER_DATE			," +
							"SETTLE_TYPE			," +
							"SETTLE_COMPLITE_FLAG	," +
							"LOGIN_ID				," +
							"TEL					," +
							"EMAIL_ADDR				," +
							"SETTLE_STATUS_NM		," +
							"SETTLE_TYPE_NM			," +
							"AD_CD					," +
							"SETTLE_COMPANY_NM		," +
							"SETTLE_CANCEL_FLAG		" +
						"FROM(" +
							" SELECT VW_SETTLE_LOG01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_SETTLE_LOG01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pReportDayFrom,pReportDayTo,pSettleType,pLoginId,pTel,pSettleStatus,pAdCd,pSettleCompanyCd, pAnswerDayFrom,pAnswerDayTo,pFanClubFeeFlag,pSettleCancelFlag,pWithoutCredit300Flag,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_SETTLE_LOG01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public void GetTotal(string pSiteCd,string pReportDayFrom,string pReportDayTo,string pSettleType,string pLoginId,string pTel,string pSettleStatus,string pAdCd,string pSettleCompanyCd,out int pCount,out int pAmount) {
		GetTotal(pSiteCd,pReportDayFrom,pReportDayTo,pSettleType,pLoginId,pTel,pSettleStatus,pAdCd,pSettleCompanyCd,string.Empty,string.Empty,string.Empty,ViCommConst.FLAG_OFF_STR,string.Empty,out pCount,out pAmount);
	}

	public void GetTotal(string pSiteCd,string pReportDayFrom,string pReportDayTo,string pSettleType,string pLoginId,string pTel,string pSettleStatus,string pAdCd,string pSettleCompanyCd,string pAnswerDayFrom,string pAnswerDayTo,out int pCount,out int pAmount) {
		GetTotal(pSiteCd,pReportDayFrom,pReportDayTo,pSettleType,pLoginId,pTel,pSettleStatus,pAdCd,pSettleCompanyCd,pAnswerDayFrom,pAnswerDayTo,string.Empty,ViCommConst.FLAG_OFF_STR,string.Empty,out pCount,out pAmount);
	}

	public void GetTotal(string pSiteCd,string pReportDayFrom,string pReportDayTo,string pSettleType,string pLoginId,string pTel,string pSettleStatus,string pAdCd,string pSettleCompanyCd,string pAnswerDayFrom,string pAnswerDayTo,string pFanClubFeeFlag,string pWithoutCredit300Flag,out int pCount,out int pAmount) {
		GetTotal(pSiteCd,pReportDayFrom,pReportDayTo,pSettleType,pLoginId,pTel,pSettleStatus,pAdCd,pSettleCompanyCd,pAnswerDayFrom,pAnswerDayTo,pFanClubFeeFlag,ViCommConst.FLAG_OFF_STR,pWithoutCredit300Flag,out pCount,out pAmount);
	}

	public void GetTotal(string pSiteCd,string pReportDayFrom,string pReportDayTo,string pSettleType,string pLoginId,string pTel,string pSettleStatus,string pAdCd,string pSettleCompanyCd,string pAnswerDayFrom,string pAnswerDayTo,string pFanClubFeeFlag,string pSettleCancelFlag,string pWithoutCredit300Flag,out int pCount,out int pAmount) {
		DataSet ds;
		DataRow dr;
		pCount = 0;
		pAmount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT " +
								"NVL(COUNT(*),0) AS CNT , " +
								"NVL(SUM(SETTLE_AMT),0) AS AMT	 " +
							"FROM " +
								"VW_SETTLE_LOG01";
			string sWhere = "";

			OracleParameter[] objParms = CreateWhere(
				pSiteCd,
				pReportDayFrom,
				pReportDayTo,
				pSettleType,
				pLoginId,
				pTel,
				ViComm.ViCommConst.SETTLE_STAT_OK,
				pAdCd,
				pSettleCompanyCd,
				pAnswerDayFrom,
				pAnswerDayTo,
				pFanClubFeeFlag,
				pSettleCancelFlag,
				pWithoutCredit300Flag,
				ref sWhere
			);

			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					pCount = int.Parse(dr["CNT"].ToString());
					pAmount = int.Parse(dr["AMT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pReportDayFrom,string pReportDayTo,string pSettleType,string pLoginId,string pTel,string pSettleStatus,string pAdCd,string pSettleCompanyCd,string pAnswerDayFrom,string pAnswerDayTo,string pFanClugFeeFlag,string pSettleCancelFlag,string pWithoutCredit300Flag,ref string pWhere) {
		ArrayList list = new ArrayList();

		if (!pSiteCd.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (pSettleType.Equals("") == false) {
			SysPrograms.SqlAppendWhere("SETTLE_TYPE = :SETTLE_TYPE",ref pWhere);
			list.Add(new OracleParameter("SETTLE_TYPE",pSettleType));
		}

		if ((!pReportDayFrom.Equals("")) && (!pReportDayTo.Equals(""))) {
			DateTime dtFrom = System.DateTime.ParseExact(pReportDayFrom + " 00:00:00","yyyy/MM/dd HH:mm:ss",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);
			DateTime dtTo = System.DateTime.ParseExact(pReportDayTo + " 00:00:00","yyyy/MM/dd HH:mm:ss",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);
			iBridCommLib.SysPrograms.SqlAppendWhere("CREATE_DATE >= :CREATE_DATE_FROM AND CREATE_DATE < :CREATE_DATE_TO",ref pWhere);
			list.Add(new OracleParameter("CREATE_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("CREATE_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (pSettleStatus.Equals("") == false) {
			SysPrograms.SqlAppendWhere("SETTLE_STATUS = :SETTLE_STATUS",ref pWhere);
			list.Add(new OracleParameter("SETTLE_STATUS",pSettleStatus));
		}

		if (!pLoginId.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("LOGIN_ID LIKE :LOGIN_ID||'%'",ref pWhere);
			list.Add(new OracleParameter("LOGIN_ID",pLoginId));
		}

		if (!pTel.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("TEL LIKE :TEL||'%'",ref pWhere);
			list.Add(new OracleParameter("TEL",pTel));
		}

		if (!pAdCd.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("AD_CD = :AD_CD",ref pWhere);
			list.Add(new OracleParameter("AD_CD",pAdCd));
		}

		if (!pSettleCompanyCd.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("SETTLE_COMPANY_CD = :SETTLE_COMPANY_CD",ref pWhere);
			list.Add(new OracleParameter("SETTLE_COMPANY_CD",pSettleCompanyCd));
		}

		if ((!pAnswerDayFrom.Equals("")) && (!pAnswerDayTo.Equals(""))) {
			DateTime dtFrom = System.DateTime.ParseExact(pAnswerDayFrom + " 00:00:00","yyyy/MM/dd HH:mm:ss",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);
			DateTime dtTo = System.DateTime.ParseExact(pAnswerDayTo + " 00:00:00","yyyy/MM/dd HH:mm:ss",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);
			iBridCommLib.SysPrograms.SqlAppendWhere("ANSWER_DATE >= :ANSWER_DATE_FROM AND ANSWER_DATE < :ANSWER_DATE_TO",ref pWhere);
			list.Add(new OracleParameter("ANSWER_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("ANSWER_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (!pFanClugFeeFlag.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("FANCLUB_FEE_FLAG = :FANCLUB_FEE_FLAG",ref pWhere);
			list.Add(new OracleParameter("FANCLUB_FEE_FLAG",pFanClugFeeFlag));
		}

		if (!pSettleCancelFlag.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("SETTLE_CANCEL_FLAG = :SETTLE_CANCEL_FLAG",ref pWhere);
			list.Add(new OracleParameter("SETTLE_CANCEL_FLAG",pSettleCancelFlag));
		}
		
		if (!string.IsNullOrEmpty(pWithoutCredit300Flag) && pWithoutCredit300Flag.Equals(ViCommConst.FLAG_ON_STR)) {
			iBridCommLib.SysPrograms.SqlAppendWhere("NOT(SETTLE_TYPE = :SETTLE_TYPE_CREDIT AND SETTLE_AMT <= :SETTLE_AMT_MIN)",ref pWhere);
			list.Add(new OracleParameter("SETTLE_TYPE_CREDIT",ViCommConst.SETTLE_CREDIT_PACK));
			list.Add(new OracleParameter("SETTLE_AMT_MIN","324"));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public void LogSettleRequest(string pSiteCd,string pUserSeq,string pSettleCompanyCd,string pSettleType,string pSettleStatus,int pSettleAmt,int pExPoint,string pSettleID,out string pSid) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("LOG_SETTLE_REQUEST");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PSETTLE_COMPNAY_CD",DbSession.DbType.VARCHAR2,pSettleCompanyCd);
			db.ProcedureInParm("PSETTLE_TYPE",DbSession.DbType.VARCHAR2,pSettleType);
			db.ProcedureInParm("PSETTLE_STATUS",DbSession.DbType.VARCHAR2,pSettleStatus);
			db.ProcedureInParm("PSETTLE_AMT",DbSession.DbType.NUMBER,pSettleAmt);
			db.ProcedureInParm("PSETTLE_POINT",DbSession.DbType.NUMBER,pExPoint);
			db.ProcedureInParm("PSETTLE_ID",DbSession.DbType.VARCHAR2,pSettleID);
			db.ProcedureInParm("PASP_AD_CD",DbSession.DbType.VARCHAR2,"");
			db.ProcedureOutParm("PSETTLE_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			pSid = db.GetStringValue("PSID");
		}
	}

	public bool LogSettleResult(string pSid,string pUserSeq,int pSettleAmt,int pSettlePoint,string pResponse) {
		string sRet;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("LOG_SETTLE_RESULT");
			db.ProcedureInParm("PSID",DbSession.DbType.VARCHAR2,pSid);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PSETTLE_AMT",DbSession.DbType.NUMBER,pSettleAmt);
			db.ProcedureInParm("PSETTLE_POINT",DbSession.DbType.NUMBER,pSettlePoint);
			db.ProcedureInParm("PRESPONSE",DbSession.DbType.VARCHAR2,pResponse);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			sRet = db.GetStringValue("PRESULT");
		}
		return sRet.Equals("0");
	}

	public int GetSettlePageCount(string pSiteCd, string pUserSeq, string pReportDayFrom, string pReportDayTo) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	COUNT(*)");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	VW_SETTLE_LOG03	");

		string sWhereClause;
		OracleParameter[] oWhereParams = this.CreateSettleWhare(pSiteCd, pUserSeq, pReportDayFrom, pReportDayTo, out sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);

				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetSettlePageCollection(string pSiteCd, string pUserSeq, string pReportDayFrom, string pReportDayTo, int startRowIndex, int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	SITE_CD					,");
		oSqlBuilder.AppendLine("	SITE_NM					,");
		oSqlBuilder.AppendLine("	USER_SEQ				,");
		oSqlBuilder.AppendLine("	SETTLE_SEQ				,");
		oSqlBuilder.AppendLine("	RECEIPT_SEQ				,");
		oSqlBuilder.AppendLine("	CREATE_DATE				,");
		oSqlBuilder.AppendLine("	SETTLE_AMT				,");
		oSqlBuilder.AppendLine("	SETTLE_CANCEL_FLAG		,");
		oSqlBuilder.AppendLine("	RECEIPT_AMT				,");
		oSqlBuilder.AppendLine("	BEFORE_RECEIPT_BILL_AMT	,");
		oSqlBuilder.AppendLine("	AFTER_RECEIPT_BILL_AMT	,");
		oSqlBuilder.AppendLine("	SETTLE_POINT			,");
		oSqlBuilder.AppendLine("	EX_POINT				,");
		oSqlBuilder.AppendLine("	SERVICE_POINT			,");
		oSqlBuilder.AppendLine("	BEFORE_RECEIPT_POINT	,");
		oSqlBuilder.AppendLine("	AFTER_RECEIPT_POINT		,");
		oSqlBuilder.AppendLine("	SETTLE_TYPE				,");
		oSqlBuilder.AppendLine("	RECEIPT_PERSON			,");
		oSqlBuilder.AppendLine("	SETTLE_TYPE_NM			,");
		oSqlBuilder.AppendLine("	SETTLE_STATUS_NM		,");
		oSqlBuilder.AppendLine("	BAL_POINT				,");
		oSqlBuilder.AppendLine("	TOTAL_RECEIPT_AMT		,");
		oSqlBuilder.AppendLine("	TOTAL_RECEIPT_COUNT		,");
		oSqlBuilder.AppendLine("	MONTHLY_RECEIPT_AMT		,");
		oSqlBuilder.AppendLine("	MONTHLY_RECEIPT_COUNT	,");
		oSqlBuilder.AppendLine("	CHARACTER_ONLINE_STATUS	 ");	
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	VW_SETTLE_LOG03	");

		string sWhereClause;
		string sSortExpression = "ORDER BY SITE_CD,CREATE_DATE DESC";
		string sPagingSql = string.Empty;

		OracleParameter[] oWhereParams = this.CreateSettleWhare(pSiteCd, pUserSeq, pReportDayFrom, pReportDayTo, out sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(), sSortExpression, startRowIndex, maximumRows, out sPagingSql);

		DataSet oDataSet = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oPagingParams);
				cmd.Parameters.AddRange(oWhereParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
				}
			}
		} finally {
			conn.Close();
		}
		return oDataSet;
	}

	private OracleParameter[] CreateSettleWhare(string pSiteCd, string pUserSeq, string pReportDayFrom, string pReportDayTo, out string sWhereClause) {
		List<OracleParameter> oList = new List<OracleParameter>();
		sWhereClause = string.Empty;
		
		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere(" SITE_CD = :SITE_CD ",ref sWhereClause);
			oList.Add(new OracleParameter("SITE_CD", pSiteCd));
		}

		if (!string.IsNullOrEmpty(pUserSeq)) {
			SysPrograms.SqlAppendWhere(" USER_SEQ = :USER_SEQ ",ref sWhereClause);
			oList.Add(new OracleParameter("USER_SEQ", pUserSeq));
		}

		if (!string.IsNullOrEmpty(pReportDayFrom) && !string.IsNullOrEmpty(pReportDayTo)) {
			SysPrograms.SqlAppendWhere(" (CREATE_DATE >= :CREATE_DATE_FROM AND CREATE_DATE < :CREATE_DATE_TO) ",ref sWhereClause);
			DateTime dtFrom = DateTime.Parse(pReportDayFrom);
			DateTime dtTo = DateTime.Parse(pReportDayTo).AddDays(1);
			oList.Add(new OracleParameter("CREATE_DATE_FROM", OracleDbType.Date, dtFrom, ParameterDirection.Input));
			oList.Add(new OracleParameter("CREATE_DATE_TO", OracleDbType.Date, dtTo, ParameterDirection.Input));
		}
		
		SysPrograms.SqlAppendWhere(" SETTLE_CANCEL_FLAG	= :SETTLE_CANCEL_FLAG ",ref sWhereClause);
		oList.Add(new OracleParameter("SETTLE_CANCEL_FLAG",ViCommConst.FLAG_OFF));

		return oList.ToArray();
	}

	public void CancelSettleLog(string pSettleSeq) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SETTLE_LOG_CANCEL");
			db.ProcedureInParm("pSETTLE_SEQ",DbSession.DbType.VARCHAR2,pSettleSeq);
			db.ProcedureInParm("pCANCEL_FLAG",DbSession.DbType.NUMBER,ViComm.ViCommConst.FLAG_ON);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public void RevertCancelSettleLog(string pSettleSeq) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SETTLE_LOG_CANCEL");
			db.ProcedureInParm("pSETTLE_SEQ",DbSession.DbType.VARCHAR2,pSettleSeq);
			db.ProcedureInParm("pCANCEL_FLAG",DbSession.DbType.NUMBER,ViComm.ViCommConst.FLAG_OFF);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}
