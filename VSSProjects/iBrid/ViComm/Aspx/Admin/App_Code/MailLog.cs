﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メール記録
--	Progaram ID		: MailLog
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater		Update Explain
  2010/06/15  KazuakiItoh	一括送信メールのデフォルト非表示対応

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Text;
using System.Data.Common;
using System.Collections;
using iBridCommLib;
using ViComm;

public class MailLog:DbSession {


	public MailLog() {
	}

	public int GetPageCount(string pSiteCd,string pMailType,string pReportDayFrom,string pReportTimeFrom,string pReportDayTo,string pReportTimeTo,string pTxLoginId,string pTxUserCharNo,string pRxLoginId,string pRxUserCharNo,string pManagerSeq) {
		return this.GetPageCount(pSiteCd,pMailType,pReportDayFrom,pReportTimeFrom,pReportDayTo,pReportTimeTo,pTxLoginId,pTxUserCharNo,pRxLoginId,pRxUserCharNo,pManagerSeq,string.Empty,string.Empty,string.Empty,string.Empty,string.Empty);
	}
	public int GetPageCount(string pSiteCd,string pMailType,string pReportDayFrom,string pReportTimeFrom,string pReportDayTo,string pReportTimeTo,string pTxLoginId,string pTxUserCharNo,string pRxLoginId,string pRxUserCharNo,string pManagerSeq,string pWithBatchMail){
		return this.GetPageCount(pSiteCd, pMailType, pReportDayFrom, pReportTimeFrom, pReportDayTo, pReportTimeTo, pTxLoginId, pTxUserCharNo, pRxLoginId, pRxUserCharNo, pManagerSeq, pWithBatchMail, string.Empty,string.Empty,string.Empty,string.Empty);
	}
	public int GetPageCount(string pSiteCd,string pMailType,string pReportDayFrom,string pReportTimeFrom,string pReportDayTo,string pReportTimeTo,string pTxLoginId,string pTxUserCharNo,string pRxLoginId,string pRxUserCharNo,string pManagerSeq,string pWithBatchMail,string pAttachedType,string pKeyword,string pSortExpression,string pSortDirection) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(REQUEST_TX_MAIL_SEQ) AS ROW_COUNT FROM VW_MAIL_LOG00 P ";
			string sWhere = "";

			OracleParameter[] oParms = CreateWhere(pSiteCd, pMailType, pReportDayFrom, pReportTimeFrom, pReportDayTo, pReportTimeTo, pTxLoginId, pTxUserCharNo, pRxLoginId, pRxUserCharNo, pManagerSeq, pWithBatchMail, pAttachedType, pKeyword, ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < oParms.Length;i++) {
					cmd.BindByName = true;
					cmd.Parameters.Add((OracleParameter)oParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPages = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPages;
	}
	public DataSet GetPageCollection(string pSiteCd,string pMailType,string pReportDayFrom,string pReportTimeFrom,string pReportDayTo,string pReportTimeTo,string pTxLoginId,string pTxUserCharNo,string pRxLoginId,string pRxUserCharNo,string pManagerSeq,int startRowIndex,int maximumRows) {
		return GetPageCollection(pSiteCd,pMailType,pReportDayFrom,pReportTimeFrom,pReportDayTo,pReportTimeTo,pTxLoginId,pTxUserCharNo,pRxLoginId,pRxUserCharNo,pManagerSeq,startRowIndex,maximumRows,string.Empty,string.Empty,string.Empty,string.Empty,string.Empty);
	}
	public DataSet GetPageCollection(string pSiteCd, string pMailType, string pReportDayFrom, string pReportTimeFrom, string pReportDayTo, string pReportTimeTo, string pTxLoginId, string pTxUserCharNo, string pRxLoginId, string pRxUserCharNo, string pManagerSeq, int startRowIndex, int maximumRows, string pWithBatchMail) {
		return GetPageCollection(pSiteCd, pMailType, pReportDayFrom, pReportTimeFrom, pReportDayTo, pReportTimeTo, pTxLoginId, pTxUserCharNo, pRxLoginId, pRxUserCharNo, pManagerSeq, startRowIndex, maximumRows, pWithBatchMail, string.Empty,string.Empty,string.Empty,string.Empty);
	}
	public DataSet GetPageCollection(string pSiteCd,string pMailType,string pReportDayFrom,string pReportTimeFrom,string pReportDayTo,string pReportTimeTo,string pTxLoginId,string pTxUserCharNo,string pRxLoginId,string pRxUserCharNo,string pManagerSeq,int startRowIndex,int maximumRows,string pWithBatchMail,string pAttachedType,string pKeyword,string pSortExpression,string pSortDirection) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = GetInnerOrder(pSortExpression,pSortDirection);

			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT * FROM(").AppendLine();
			sSql.Append("	SELECT").AppendLine();
			sSql.Append("		ROWNUM AS RNUM			,").AppendLine();
			sSql.Append("		INNER.*					 ").AppendLine();
			sSql.Append("	FROM (						 ").AppendLine();
			sSql.Append("		SELECT					 ").AppendLine();
			sSql.Append("			P.MAIL_SEQ				,").AppendLine();
			sSql.Append("			P.MAIL_TYPE				,").AppendLine();
			sSql.Append("			P.CREATE_DATE			,").AppendLine();
			sSql.Append("			P.MAIL_TITLE			,").AppendLine();
			sSql.Append("			P.MAIL_DOC1				,").AppendLine();
			sSql.Append("			P.MAIL_DOC2				,").AppendLine();
			sSql.Append("			P.MAIL_DOC3				,").AppendLine();
			sSql.Append("			P.MAIL_DOC4				,").AppendLine();
			sSql.Append("			P.MAIL_DOC5				,").AppendLine();
			sSql.Append("			P.READ_FLAG				,").AppendLine();
			sSql.Append("			P.TX_DEL_FLAG			,").AppendLine();
			sSql.Append("			P.RX_DEL_FLAG			,").AppendLine();
			sSql.Append("			P.TX_SITE_CD			,").AppendLine();
			sSql.Append("			P.TX_USER_SEQ			,").AppendLine();
			sSql.Append("			P.TX_USER_CHAR_NO		,").AppendLine();
			sSql.Append("			P.TX_MAIL_BOX_TYPE		,").AppendLine();
			sSql.Append("			P.RX_SITE_CD			,").AppendLine();
			sSql.Append("			P.RX_USER_SEQ			,").AppendLine();
			sSql.Append("			P.RX_MAIL_BOX_TYPE		,").AppendLine();
			sSql.Append("			P.BATCH_MAIL_LOG_SEQ	,").AppendLine();
			sSql.Append("			P.ORIGINAL_TITLE		,").AppendLine();
			sSql.Append("			P.ORIGINAL_DOC1			,").AppendLine();
			sSql.Append("			P.ORIGINAL_DOC2			,").AppendLine();
			sSql.Append("			P.ORIGINAL_DOC3			,").AppendLine();
			sSql.Append("			P.ORIGINAL_DOC4			,").AppendLine();
			sSql.Append("			P.ORIGINAL_DOC5			,").AppendLine();
			sSql.Append("			P.TX_LOGIN_ID			,").AppendLine();
			sSql.Append("			P.TX_SEX_CD				,").AppendLine();
			sSql.Append("			P.TX_MANAGER_SEQ		,").AppendLine();
			sSql.Append("			P.RX_LOGIN_ID			,").AppendLine();
			sSql.Append("			P.RX_SEX_CD				,").AppendLine();
			sSql.Append("			P.ATTACHED_OBJ_TYPE		,").AppendLine();
			sSql.Append("			P.PIC_SEQ				,").AppendLine();
			sSql.Append("			P.MOVIE_SEQ				,").AppendLine();
			sSql.Append("			P.RX_MANAGER_SEQ		,").AppendLine();
			sSql.Append("			P.SERVICE_POINT			,").AppendLine();
			sSql.Append("			P.SERVICE_POINT_EFFECTIVE_DATE				,").AppendLine();
			sSql.Append("			P.SERVICE_POINT_TRANSFER_FLAG				,").AppendLine();
			sSql.Append("			SUBSTR(P.MAIL_DOC1,1,20) AS MAIL_PART_DOC	,").AppendLine();
			sSql.Append("			C1.MAIL_TYPE_NM		").AppendLine();
			sSql.Append("		FROM  ").AppendLine();
			sSql.Append("			VW_MAIL_LOG00	 P	, ").AppendLine();
			sSql.Append("			T_MAIL_TYPE		 C1	  ").AppendLine();


			// join
			StringBuilder oWhereBuilder = new StringBuilder();
			oWhereBuilder.AppendLine(" WHERE ");
			oWhereBuilder.AppendLine(" P.MAIL_TYPE		= C1.MAIL_TYPE(+) ");

			string sWhere = oWhereBuilder.ToString();
			OracleParameter[] oParms = CreateWhere(pSiteCd,pMailType,pReportDayFrom,pReportTimeFrom,pReportDayTo,pReportTimeTo,pTxLoginId,pTxUserCharNo,pRxLoginId,pRxUserCharNo,pManagerSeq,pWithBatchMail,pAttachedType,pKeyword,ref sWhere);

			sSql.Append(sWhere).AppendLine();
			sSql.Append(sOrder).AppendLine();
			sSql.Append(")INNER WHERE ROWNUM <= :MAX_ROW ").AppendLine();
			sSql.Append(")WHERE RNUM >= :FIRST_ROW AND RNUM <= :LAST_ROW ");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				cmd.BindByName = true;
				for (int i = 0;i < oParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)oParms[i]);
				}
				cmd.Parameters.Add("MAX_ROW",startRowIndex + maximumRows);
				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_MAIL_LOG00");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pMailType,string pReportDayFrom,string pReportTimeFrom,string pReportDayTo,string pReportTimeTo,string pTxLoginId,string pTxUserCharNo,string pRxLoginId,string pRxUserCharNo,string pManagerSeq,string pWithBatchMail,string pAttachedType,string pKeyword,ref string pWhere) {
		SetDefaultTimeValue(ref pReportTimeFrom,ref pReportTimeTo);
		ArrayList list = new ArrayList();
		
		if (pSiteCd.Equals("") == false) {
			SysPrograms.SqlAppendWhere("P.TX_SITE_CD = :TX_SITE_CD",ref pWhere);
			list.Add(new OracleParameter("TX_SITE_CD",pSiteCd));
		}

		if (!string.IsNullOrEmpty(pWithBatchMail)) {
			if (ViCommConst.FLAG_ON_STR.Equals(pWithBatchMail)) {
				SysPrograms.SqlAppendWhere("P.BATCH_MAIL_FLAG = :BATCH_MAIL_FLAG", ref pWhere);
				list.Add(new OracleParameter("BATCH_MAIL_FLAG", ViCommConst.FLAG_ON));
			} else {
				SysPrograms.SqlAppendWhere("P.BATCH_MAIL_FLAG = :BATCH_MAIL_FLAG", ref pWhere);
				list.Add(new OracleParameter("BATCH_MAIL_FLAG", ViCommConst.FLAG_OFF));
			}
		}

		SysPrograms.SqlAppendWhere("P.WAIT_TX_FLAG = :WAIT_TX_FLAG", ref pWhere);
		list.Add(new OracleParameter("WAIT_TX_FLAG",ViCommConst.FLAG_OFF_STR));


		if ((!pReportDayFrom.Equals("")) && (!pReportDayTo.Equals(""))) {
			DateTime dtFrom = DateTime.Parse(pReportDayFrom + " " + pReportTimeFrom + ":00:00");
			DateTime dtTo = DateTime.Parse(pReportDayTo + " " + pReportTimeTo + ":59:59").AddSeconds(1);
			iBridCommLib.SysPrograms.SqlAppendWhere("P.CREATE_DATE >= :CREATE_DATE_FROM AND P.CREATE_DATE < :CREATE_DATE_TO", ref pWhere);
			list.Add(new OracleParameter("CREATE_DATE_FROM", OracleDbType.Date, dtFrom, ParameterDirection.Input));
			list.Add(new OracleParameter("CREATE_DATE_TO", OracleDbType.Date, dtTo, ParameterDirection.Input));
		}

		if (pMailType.Equals("") == false) {
			SysPrograms.SqlAppendWhere("P.MAIL_TYPE = :MAIL_TYPE", ref pWhere);
			list.Add(new OracleParameter("MAIL_TYPE",pMailType));
		}


		if (pTxLoginId.Equals("") == false) {
			SysPrograms.SqlAppendWhere("P.TX_LOGIN_ID LIKE :TX_LOGIN_ID||'%'", ref pWhere);
			list.Add(new OracleParameter("TX_LOGIN_ID",pTxLoginId ));

			if (pTxUserCharNo.Equals("") == false) {
				SysPrograms.SqlAppendWhere("P.TX_USER_CHAR_NO = :TX_USER_CHAR_NO", ref pWhere);
				list.Add(new OracleParameter("TX_USER_CHAR_NO",pTxUserCharNo));
			}
		}

		if (pRxLoginId.Equals("") == false) {
			SysPrograms.SqlAppendWhere("P.RX_LOGIN_ID LIKE :RX_LOGIN_ID||'%'", ref pWhere);
			list.Add(new OracleParameter("RX_LOGIN_ID",pRxLoginId ));

			if (pRxUserCharNo.Equals("") == false) {
				SysPrograms.SqlAppendWhere("P.RX_USER_CHAR_NO = :RX_USER_CHAR_NO", ref pWhere);
				list.Add(new OracleParameter("RX_USER_CHAR_NO",pRxUserCharNo));
			}
		}

		if (pManagerSeq.Equals("") == false) {
			SysPrograms.SqlAppendWhere("(P.RX_MANAGER_SEQ = :RX_MANAGER_SEQ OR P.TX_MANAGER_SEQ = :TX_MANAGER_SEQ)", ref pWhere);
			list.Add(new OracleParameter("RX_MANAGER_SEQ", pManagerSeq));
			list.Add(new OracleParameter("TX_MANAGER_SEQ", pManagerSeq));
		}

		if (!pAttachedType.Equals(string.Empty)) {
			StringBuilder oClauseBuilder = new StringBuilder();

			if (!pWhere.Equals(string.Empty)) {
				oClauseBuilder.Append(" AND ");
			}

			oClauseBuilder.Append("P.ATTACHED_OBJ_TYPE IN (");
			int iIndex = 1;
			foreach(string sValue in pAttachedType.Split(',')){
				string sParamName = string.Format(":ATTACHED_OBJ_TYPE{0}", iIndex);
				if(iIndex!= 1){
					oClauseBuilder.Append(",");
				}
				oClauseBuilder.Append(sParamName);
				list.Add(new OracleParameter(sParamName, sValue));
				iIndex+=1;
			}
			oClauseBuilder.Append(") ");
			
			pWhere += oClauseBuilder.ToString();
		}

		if (!string.IsNullOrEmpty(pKeyword)) {
			StringBuilder oWhereKeywords = new StringBuilder("(");
			string[] sKeywordArray = pKeyword.Split(' ','　');
			for (int i = 0; i < sKeywordArray.Length; i++) {
				oWhereKeywords.AppendFormat("(P.MAIL_TITLE LIKE :KEYWORD{0}) OR (P.MAIL_DOC1 LIKE :KEYWORD{0}) OR (P.MAIL_DOC2 LIKE :KEYWORD{0}) OR (P.MAIL_DOC3 LIKE :KEYWORD{0}) OR (P.MAIL_DOC4 LIKE :KEYWORD{0}) OR (P.MAIL_DOC5 LIKE :KEYWORD{0}) OR (P.ORIGINAL_DOC1 LIKE :KEYWORD{0}) OR (P.ORIGINAL_DOC2 LIKE :KEYWORD{0}) OR (P.ORIGINAL_DOC3 LIKE :KEYWORD{0}) OR (P.ORIGINAL_DOC4 LIKE :KEYWORD{0}) OR (P.ORIGINAL_DOC5 LIKE :KEYWORD{0})",i);
				list.Add(new OracleParameter(string.Format("KEYWORD{0}",i),string.Format("%{0}%",sKeywordArray[i])));

				if (i < sKeywordArray.Length - 1) {
					oWhereKeywords.AppendLine(" AND ");
				}
			}
			oWhereKeywords.Append(")");
			SysPrograms.SqlAppendWhere(oWhereKeywords.ToString(),ref pWhere);
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public void UpdateMailDel(string pMailSeq,string pTxRxType) {
		string[] mailSeqList = new string[] { pMailSeq };
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_MAIL_DEL");
			db.ProcedureInArrayParm("PMAIL_SEQ",DbSession.DbType.VARCHAR2,mailSeqList.Length,mailSeqList);
			db.ProcedureInParm("PMAIL_SEQ_COUNT",DbSession.DbType.NUMBER,mailSeqList.Length);
			db.ProcedureInParm("PTX_RX_TYPE",DbSession.DbType.VARCHAR2,pTxRxType);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	private void SetDefaultTimeValue(ref string pTimeFrom,ref string pTimeTo) {
		if (string.IsNullOrEmpty(pTimeFrom)) {
			pTimeFrom = "00";
		}
		if (string.IsNullOrEmpty(pTimeTo)) {
			pTimeTo = "23";
		}
	}

	private string GetInnerOrder(string pSortExpression,string pSortDirection) {
		if (string.IsNullOrEmpty(pSortExpression) || string.IsNullOrEmpty(pSortDirection)) {
			return "ORDER BY CREATE_DATE DESC";
		}

		return string.Format("ORDER BY {0} {1}",pSortExpression,pSortDirection);
	}

}
