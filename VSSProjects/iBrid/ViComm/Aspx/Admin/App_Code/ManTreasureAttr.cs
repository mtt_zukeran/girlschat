﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 男性お宝属性
--	Progaram ID		: ManTreasureAttr
--
--  Creation Date	: 2011.07.21
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  2009/07/09  iBrid(Y.Inoue)    データ表示用にアイテムNOを追加

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using ViComm;
using iBridCommLib;

public class ManTreasureAttr:DbSession {
	public ManTreasureAttr() {
	}

	public int GetPageCount(string pSiteCd) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_MAN_TREASURE_ATTR ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();
			string sOrder = "ORDER BY SITE_CD,PRIORITY";

			string sSql = "SELECT " +
							"SITE_CD				," +
							"CAST_GAME_PIC_ATTR_SEQ	," +
							"CAST_GAME_PIC_ATTR_NM	," +
							"PRIORITY				," +
							"CREATE_DATE			," +
							"UPDATE_DATE			" +
							"FROM(" +
							" SELECT T_MAN_TREASURE_ATTR.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM T_MAN_TREASURE_ATTR  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql += sWhere;

			sSql += ") " +
					"WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql += sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MAN_TREASURE_ATTR");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		pWhere = pWhere + " WHERE  SITE_CD = :SITE_CD";
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public DataSet GetList(string pSiteCd) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = " ORDER BY SITE_CD,PRIORITY ";
			string sSql = "SELECT CAST_GAME_PIC_ATTR_SEQ,CAST_GAME_PIC_ATTR_NM FROM T_MAN_TREASURE_ATTR ";
			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);

			sSql += sWhere;
			sSql += sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MAN_TREASURE_ATTR");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
}
