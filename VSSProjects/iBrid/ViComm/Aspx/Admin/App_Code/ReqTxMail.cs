﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メール配信
--	Progaram ID		: ReqTxMail
--
--  Creation Date	: 2011.08.05
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater		Update Explain
  2010/06/15  KazuakiItoh	一括送信メールのデフォルト非表示対応

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using iBridCommLib;
using ViComm;

public class ReqTxMail:DbSession {


	public ReqTxMail() {
	}

	public int GetPageCount(string pSiteCd) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_REQ_TX_MAIL ";
			string sWhere = "";

			OracleParameter[] oParms = CreateWhere(pSiteCd,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < oParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)oParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPages = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPages;
	}
	public DataSet GetPageCollection(string pSiteCd,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY REQUEST_TX_MAIL_SEQ DESC";

			StringBuilder sSql = new StringBuilder();

			sSql.Append("SELECT * FROM(").AppendLine();
			sSql.Append("	SELECT").AppendLine();
			sSql.Append("		ROWNUM AS RNUM					,").AppendLine();
			sSql.Append("		INNER.*							,").AppendLine();
			sSql.Append("		M.CREATE_DATE					,").AppendLine();
			sSql.Append("		M.RX_SITE_CD					,").AppendLine();
			sSql.Append("		M.RX_LOGIN_ID					").AppendLine();
			sSql.Append("	FROM T_MAIL_LOG M,(").AppendLine();
			sSql.Append("		SELECT ").AppendLine();
			sSql.Append("			REQUEST_TX_MAIL_SEQ		,").AppendLine();
			sSql.Append("			ORIGINAL_TITLE			,").AppendLine();
			sSql.Append("			ORIGINAL_DOC1			,").AppendLine();
			sSql.Append("			ORIGINAL_DOC2			,").AppendLine();
			sSql.Append("			ORIGINAL_DOC3			,").AppendLine();
			sSql.Append("			ORIGINAL_DOC4			,").AppendLine();
			sSql.Append("			ORIGINAL_DOC5			,").AppendLine();
			sSql.Append("			TX_SEX_CD				,").AppendLine();
			sSql.Append("			TX_SITE_CD				,").AppendLine();
			sSql.Append("			TX_USER_SEQ				,").AppendLine();
			sSql.Append("			TX_USER_CHAR_NO			,").AppendLine();
			sSql.Append("			TX_LOGIN_ID				,").AppendLine();
			sSql.Append("			RX_SEX_CD				,").AppendLine();
			sSql.Append("			PIC_SEQ					,").AppendLine();
			sSql.Append("			MOVIE_SEQ				,").AppendLine();
			sSql.Append("			ATTACHED_OBJ_TYPE		,").AppendLine();
			sSql.Append("			BATCH_MAIL_FLAG			").AppendLine();
			sSql.Append("		FROM T_REQ_TX_MAIL ").AppendLine();

			string sWhere = "";
			OracleParameter[] oParms = CreateWhere(pSiteCd,ref sWhere);

			sSql.Append(sWhere).AppendLine();
			sSql.Append(sOrder).AppendLine();
			sSql.Append(")INNER WHERE ROWNUM <= :MAX_ROW AND").AppendLine();
			sSql.Append("INNER.REQUEST_TX_MAIL_SEQ		= M.REQUEST_TX_MAIL_SEQ		AND	").AppendLine();
			sSql.Append("M.REQUEST_TX_MAIL_SUBSEQ		= :REQUEST_TX_MAIL_SUBSEQ		").AppendLine();
			sSql.Append(")WHERE ").AppendLine();

			sSql.Append("RNUM >= :FIRST_ROW AND RNUM <= :LAST_ROW ");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				for (int i = 0;i < oParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)oParms[i]);
				}
				cmd.Parameters.Add("MAX_ROW",startRowIndex + maximumRows);
				cmd.Parameters.Add("REQUEST_TX_MAIL_SUBSEQ",1);
				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_REQ_TX_MAIL_DTL00");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,ref string pWhere) {
		ArrayList list = new ArrayList();

		if (pSiteCd.Equals("") == false) {
			SysPrograms.SqlAppendWhere("TX_SITE_CD = :TX_SITE_CD",ref pWhere);
			list.Add(new OracleParameter("TX_SITE_CD",pSiteCd));
		}

		SysPrograms.SqlAppendWhere("NOT_APPROVE_TX_FLAG = :NOT_APPROVE_TX_FLAG",ref pWhere);
		list.Add(new OracleParameter("NOT_APPROVE_TX_FLAG",ViCommConst.FLAG_ON));

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}
