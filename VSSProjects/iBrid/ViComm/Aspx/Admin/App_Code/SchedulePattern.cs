﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メール配信スケジュール
--	Progaram ID		: SchedulePattern
--
--  Creation Date	: 2010.05.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------
--  Date        Updater    Update Explain
--  yyyy/mm/dd  XXXXXXXXX
-------------------------------------------------------------------------*/
using System.Collections;
using Oracle.DataAccess.Client;
using System.Data;

/// <summary>
/// メール配信スケジュール
/// </summary>
public class SchedulePattern:DbSession {
	public DataSet GetOne(string pPatternSeq) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT PATTERN_SEQ,PATTERN_NAME,PATTERN_FLG,TX_TIME,"
						+ "TX_WEEK1,TX_WEEK2,TX_WEEK3,TX_WEEK4,TX_WEEK5,TX_WEEK6,TX_WEEK7,"
						+ "TX_MAIL_NEXT_DATE,TX_MAIL_LAST_DATE"
						+ " FROM T_SCHEDULE_PATTERN"
						+ " WHERE PATTERN_SEQ = :PATTERN_SEQ"
						+ " ORDER BY PATTERN_SEQ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("PATTERN_SEQ",pPatternSeq);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetListCnd(string pPatternSeq) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT PATTERN_SEARCH_NAME,PATTERN_SEARCH_VALUE"
						+ " FROM T_SCHEDULE_PATTERN_CND"
						+ " WHERE PATTERN_SEQ = :PATTERN_SEQ"
						+ " ORDER BY PATTERN_SEQ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("PATTERN_SEQ",pPatternSeq);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public int GetPageCount(string pSexCd) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(PATTERN_SEQ) AS ROW_COUNT FROM T_SCHEDULE_PATTERN ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSexCd,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}
				
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSexCd,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = " ORDER BY PATTERN_SEQ";
			string sSql = "SELECT " +
							"PATTERN_SEQ      ," +
							"PATTERN_NAME     ," +
							"PATTERN_FLG      ," +
							"TX_TIME          ," +
							"TX_WEEK1         ," +
							"TX_WEEK2  	      ," +
							"TX_WEEK3         ," +
							"TX_WEEK4         ," +
							"TX_WEEK5         ," +
							"TX_WEEK6         ," +
							"TX_WEEK7         ," +
							"TX_MAIL_NEXT_DATE," +
							"TX_MAIL_LAST_DATE," +
							"UPDATE_DATE      ," +
							"REVISION_NO " +
							" FROM(" +
							" SELECT T_SCHEDULE_PATTERN.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM T_SCHEDULE_PATTERN ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSexCd,ref sWhere);
			sSql = sSql + sWhere;
			

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}
				
				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_SCHEDULE_PATTERN");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSexCd,ref string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();
		pWhere = pWhere + " WHERE SEX_CD = :SEX_CD ";
		list.Add(new OracleParameter("SEX_CD",pSexCd));

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public DataSet GetPatternList(string pDate) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT "
						+ "PTN.PATTERN_SEQ         ,"
						+ "CND.PATTERN_SEARCH_NAME ,"
						+ "CND.PATTERN_SEARCH_VALUE "
						+ " FROM"
						+ " T_SCHEDULE_PATTERN PTN    ,"
						+ " T_SCHEDULE_PATTERN_CND CND"
						+ " WHERE"
						+ " PTN.PATTERN_SEQ = CND.PATTERN_SEQ"
						+ " AND PTN.PATTERN_FLG = 1"
						+ " AND PTN.TX_MAIL_NEXT_DATE <= TO_DATE(:TX_MAIL_NEXT_DATE,'YYYY/MM/DD HH24:MI:SS')"
						+ " ORDER BY PTN.PATTERN_SEQ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("TX_MAIL_NEXT_DATE",pDate);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
}
