﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 督促
--	Progaram ID		: Urge
--
--  Creation Date	: 2009.12.21
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;
using iBridCommLib;
using ViComm;

public class Urge:DbSession {
	public Urge() {
	}
	
	public DataSet GetOne(string pSiteCd, string pUserSeq) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT ");
			sSql.Append("	USER_SEQ			,");
			sSql.Append("	URGE_LEVEL			,");
			sSql.Append("	MAX_URGE_COUNT		,");
			sSql.Append("	URGE_COUNT_TODAY	,");
			sSql.Append("	URGE_RESTART_DAY	,");
			sSql.Append("	URGE_END_DAY		,");
			sSql.Append("	URGE_EXEC_LAST_DATE	,");
			sSql.Append("	URGE_EXEC_LAST_STATUS_NM,");
			sSql.Append("	URGE_EXEC_COUNT		");
			sSql.Append("FROM ");
			sSql.Append("	VW_URGE01 ");
			sSql.Append("WHERE ");
			sSql.Append("	USER_SEQ	= :USER_SEQ ");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_URGE");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
}