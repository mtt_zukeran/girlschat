﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 男性用お宝ｺﾝﾌﾟﾘｰﾄ時獲得ｱｲﾃﾑ--	Progaram ID		: CompManTreasureGetItem
--
--  Creation Date	: 2011.07.25
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;


public class CompManTreasureGetItem : DbSession {

	public CompManTreasureGetItem() {
	}

	public int GetPageCount(string pSiteCd,string pDisplayDay) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_COMP_MAN_TREASURE_GET_ITEM	T	,");
		oSqlBuilder.AppendLine("	T_GAME_ITEM						GI	");

		string sWhereClause;
		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pDisplayDay,out sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pDisplayDay,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	T.SITE_CD						,");
		oSqlBuilder.AppendLine("	T.DISPLAY_DAY					,");
		oSqlBuilder.AppendLine("	T.GAME_ITEM_SEQ					,");
		oSqlBuilder.AppendLine("	GI.GAME_ITEM_NM					,");
		oSqlBuilder.AppendLine("	T.ITEM_COUNT					,");
		oSqlBuilder.AppendLine("	T.REVISION_NO					,");
		oSqlBuilder.AppendLine("	T.UPDATE_DATE					");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_COMP_MAN_TREASURE_GET_ITEM	T	,");
		oSqlBuilder.AppendLine("	T_GAME_ITEM						GI	");

		string sWhereClause;
		string sPagingSql;
		string sSortExpression = "ORDER BY T.SITE_CD, T.DISPLAY_DAY DESC";

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pDisplayDay,out sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pDisplayDay,out string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();

		SysPrograms.SqlAppendWhere("T.SITE_CD = GI.SITE_CD AND T.GAME_ITEM_SEQ = GI.GAME_ITEM_SEQ",ref pWhere);

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("T.SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}
		if (!string.IsNullOrEmpty(pDisplayDay)) {
			SysPrograms.SqlAppendWhere("T.DISPLAY_DAY = :DISPLAY_DAY",ref pWhere);
			list.Add(new OracleParameter("DISPLAY_DAY",pDisplayDay));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public bool IsDupulicateItem(string pSiteCd,string pDisplayDay,string pGameItemSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_COMP_MAN_TREASURE_GET_ITEM	");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD			AND");
		oSqlBuilder.AppendLine("	DISPLAY_DAY		= :DISPLAY_DAY		AND");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ	= :GAME_ITEM_SEQ	");

		try {
			int iCount = 0;
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(new OracleParameter("SITE_CD",pSiteCd));
				cmd.Parameters.Add(new OracleParameter("DISPLAY_DAY",pDisplayDay));
				cmd.Parameters.Add(new OracleParameter("GAME_ITEM_SEQ",pGameItemSeq));
				iCount = Convert.ToInt32(cmd.ExecuteScalar());
				return iCount >= 1;
			}
		} finally {
			conn.Close();
		}
	}
}
