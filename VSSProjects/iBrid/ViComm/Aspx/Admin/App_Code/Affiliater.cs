/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: アフリエター
--	Progaram ID		: Affiliater
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class Affiliater:DbSession {

	public Affiliater() {
	}

	public DataSet GetList() {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT AFFILIATER_CD,AFFILIATER_NM FROM T_AFFILIATER ";
			sSql = sSql + " ORDER BY TO_NUMBER(AFFILIATER_CD, '99999999999')";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetAuthResultTransType() {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT CODE,CODE_NM FROM T_CODE_DTL ";
			sSql = sSql + "WHERE CODE_TYPE='90' " +
						" ORDER BY CODE";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	public int GetPageCount() {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {

			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_AFFILIATER ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();
			string sOrder = " ORDER BY TO_NUMBER(AFFILIATER_CD, '99999999999') ";
			string sSql = "SELECT " +
							"AFFILIATER_CD	," +
							"AFFILIATER_NM	," +
							"AS_INFO_PREFIX ," +
							"AS_INFO_SUB_PREFIX ," +
							"AS_INFO_SUB_VALUE ," +
							"UPDATE_DATE    ," +
							"REVISION_NO     " +
						  "FROM( " +
							"SELECT T_AFFILIATER.*,ROW_NUMBER() OVER (" + sOrder + " ) AS RNUM FROM " +
							"T_AFFILIATER " +
							") WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW " +
							sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_AFFILIATER");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

}
