/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ユーザービュー
--	Progaram ID		: UserView
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class UserView:DbSession {

	public UserView() {
	}

	public int GetPageCount(string pSiteCd,string pProgramRoot,string pUserAgentType,string pProgramId,string pUserRank,string pAdGroupCd,string pActCategorySeq,string pSortExpression,string pSortDirection) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_USER_VIEW01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pProgramRoot,pProgramId,pUserAgentType,pUserRank,pAdGroupCd,pActCategorySeq,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pProgramRoot,string pProgramId,string pUserAgentType,string pUserRank,string pAdGroupCd,string pActCategorySeq,int startRowIndex,int maximumRows,string pSortExpression,string pSortDirection) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = GetOrder(pSortExpression,pSortDirection);
			string sSql = "SELECT " +
							"SITE_CD			," +
							"PROGRAM_ROOT		," +
							"PROGRAM_ID			," +
							"USER_AGENT_TYPE	," +
							"AD_GROUP_CD		," +
							"USER_RANK			," +
							"ACT_CATEGORY_SEQ	," +
							"HTML_DOC_SEQ		," +
							"UPDATE_DATE		," +
							"REVISION_NO		," +
							"HTML_DOC_TITLE		," +
							"SUBSTR(HTML_DOC1,1,20) DOC," +
							"HTML_DOC1			," +
							"HTML_DOC2			," +
							"HTML_DOC3			," +
							"HTML_DOC4			," +
							"PROGRAM_NM			," +
							"USER_AGENT_TYPE_NM	," +
							"USER_RANK_NM		," +
							"AD_GROUP_NM		," +
							"ACT_CATEGORY_NM	," +
							"SITE_NM			," +
							"TABLE_INDEX_NM	" +
							"FROM(" +
							" SELECT VW_USER_VIEW01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_USER_VIEW01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pProgramRoot,pProgramId,pUserAgentType,pUserRank,pAdGroupCd,pActCategorySeq,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USER_VIEW01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private string GetOrder(string pSortExpression,string pSortDirection) {
		string sValue = string.Empty;
		switch (pSortExpression) {
			case "PROGRAM_ID":
				sValue = string.Format("ORDER BY SITE_CD,PROGRAM_ID {0},USER_AGENT_TYPE,ACT_CATEGORY_SEQ,AD_GROUP_CD,USER_RANK",pSortDirection);
				break;
			case "UPDATE_DATE":
				sValue = string.Format("ORDER BY SITE_CD,UPDATE_DATE {0},PROGRAM_ID,USER_AGENT_TYPE,ACT_CATEGORY_SEQ,AD_GROUP_CD,USER_RANK",pSortDirection);
				break;
			default:
				sValue = "ORDER BY SITE_CD,UPDATE_DATE DESC,PROGRAM_ID,USER_AGENT_TYPE,ACT_CATEGORY_SEQ,AD_GROUP_CD,USER_RANK";
				break;
		}

		return sValue;
	}


	public DataSet GetCsvData(string pSiteCd,string pProgramRoot,string pProgramId,string pUserAgentType,string pUserRank,string pAdGroupCd,string pActCategorySeq) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY SITE_CD,PROGRAM_ID,USER_AGENT_TYPE,ACT_CATEGORY_SEQ,AD_GROUP_CD,USER_RANK ";
			string sSql = "SELECT " +
							"SITE_CD				," +
							"PROGRAM_ROOT			," +
							"PROGRAM_ID				," +
							"USER_AGENT_TYPE		," +
							"AD_GROUP_CD			," +
							"USER_RANK				," +
							"ACT_CATEGORY_SEQ		," +
							"CATEGORY_INDEX			," +
							"VIEW_KEY_MASK			," +
							"PAGE_TITLE				," +
							"PAGE_KEYWORD			," +
							"PAGE_DESCRIPTION		," +
							"TABLE_INDEX			," +
							"CSS_FILE_NM1			," +
							"CSS_FILE_NM2			," +
							"CSS_FILE_NM3			," +
							"CSS_FILE_NM4			," +
							"CSS_FILE_NM5			," +
							"JAVASCRIPT_FILE_NM1	," +
							"JAVASCRIPT_FILE_NM2	," +
							"JAVASCRIPT_FILE_NM3	," +
							"JAVASCRIPT_FILE_NM4	," +
							"JAVASCRIPT_FILE_NM5	," +
							"HTML_DOC_SEQ			," +
							"HTML_DOC_SUB_SEQ		," +
							"HTML_DOC_TITLE			," +
							"HTML_DOC1				," +
							"HTML_DOC2				," +
							"HTML_DOC3				," +
							"HTML_DOC4				," +
							"HTML_DOC5				," +
							"HTML_DOC6				," +
							"HTML_DOC7				," +
							"HTML_DOC8				," +
							"HTML_DOC9				," +
							"HTML_DOC10				" +
							"FROM " +
							 "VW_USER_VIEW03  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pProgramRoot,pProgramId,pUserAgentType,pUserRank,pAdGroupCd,pActCategorySeq,ref sWhere);
			sSql = sSql + sWhere;
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USER_VIEW03");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pProgramRoot,string pProgramId,string pUserAgentType,string pUserRank,string pAdGroupCd,string pActCategorySeq,ref string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD AND PROGRAM_ROOT = :PROGRAM_ROOT ",ref pWhere);
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		list.Add(new OracleParameter("PROGRAM_ROOT",pProgramRoot));

		if (!pProgramId.Equals("")) {
			SysPrograms.SqlAppendWhere("PROGRAM_ID LIKE '%'||:PROGRAM_ID||'%' ",ref pWhere);
			list.Add(new OracleParameter("PROGRAM_ID",pProgramId));
		}

		if (!pUserAgentType.Equals("")) {
			SysPrograms.SqlAppendWhere("USER_AGENT_TYPE = :USER_AGENT_TYPE ",ref pWhere);
			list.Add(new OracleParameter("USER_AGENT_TYPE",pUserAgentType));
		}

		if (!pUserRank.Equals("")) {
			SysPrograms.SqlAppendWhere("USER_RANK = :USER_RANK ",ref pWhere);
			list.Add(new OracleParameter("USER_RANK",pUserRank));
		}

		if (!pAdGroupCd.Equals("")) {
			SysPrograms.SqlAppendWhere("AD_GROUP_CD = :AD_GROUP_CD ",ref pWhere);
			list.Add(new OracleParameter("AD_GROUP_CD",pAdGroupCd));
		}

		if (!pActCategorySeq.Equals("")) {
			SysPrograms.SqlAppendWhere("ACT_CATEGORY_SEQ = :ACT_CATEGORY_SEQ ",ref pWhere);
			list.Add(new OracleParameter("ACT_CATEGORY_SEQ",pActCategorySeq));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
	
	public DataSet GetOneByProgramId(string pSiteCd,string pProgramRoot,string pProgramId,string pUserAgentType) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	HTML_DOC_SEQ								,	");
		oSqlBuilder.AppendLine("	USER_AGENT_TYPE									");
		oSqlBuilder.AppendLine("FROM												");
		oSqlBuilder.AppendLine("	VW_USER_VIEW03									");
		oSqlBuilder.AppendLine("WHERE												");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	PROGRAM_ROOT		= :PROGRAM_ROOT			AND	");
		oSqlBuilder.AppendLine("	PROGRAM_ID			= :PROGRAM_ID			AND	");
		oSqlBuilder.AppendLine("	AD_GROUP_CD			= :AD_GROUP_CD			AND	");
		oSqlBuilder.AppendLine("	USER_RANK			= :USER_RANK			AND	");
		oSqlBuilder.AppendLine("	ACT_CATEGORY_SEQ	= :ACT_CATEGORY_SEQ			");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":PROGRAM_ROOT",pProgramRoot));
		oParamList.Add(new OracleParameter(":PROGRAM_ID",pProgramId));
		oParamList.Add(new OracleParameter(":AD_GROUP_CD",ViCommConst.DEFAUL_AD_GROUP_CD));
		oParamList.Add(new OracleParameter(":USER_RANK",ViCommConst.DEFAUL_USER_RANK));
		oParamList.Add(new OracleParameter(":ACT_CATEGORY_SEQ",ViCommConst.DEFAULT_ACT_CATEGORY_SEQ));

		if (!string.IsNullOrEmpty(pUserAgentType)) {
			oParamList.Add(new OracleParameter(":USER_AGENT_TYPE",pUserAgentType));
			oSqlBuilder.AppendLine(" AND	USER_AGENT_TYPE		= :USER_AGENT_TYPE			");
		}

		oSqlBuilder.AppendLine("ORDER BY USER_AGENT_TYPE ASC");

		DataSet ds = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return ds;
	}
}
