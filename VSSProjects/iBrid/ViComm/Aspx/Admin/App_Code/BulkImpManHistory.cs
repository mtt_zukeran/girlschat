﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 一括会員取込履歴

--	Progaram ID		: BulkImpManHistory
--
--  Creation Date	: 2010.10.18
--  Creater			: K.Itoh
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Web;
using System.Data;
using System.Data.Common;
using System.Text;
using iBridCommLib;


/// <summary>
/// T_BULK_IMP_MAN_HISTORYテーブルへのデータアクセスを提供するクラス。
/// </summary>
public class BulkImpManHistory:DbSession {
	public BulkImpManHistory() {}

	/// <summary>
	/// 一括会員取込履歴SEQを採番し、取得する
	/// </summary>
	/// <returns>一括会員取込履歴SEQ</returns>
	public decimal GetSeq() {
		string sSql = "SELECT BULK_IMP_MAN_HISTORY_SEQ.NEXTVAL FROM DUAL";
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sSql,conn)) {
				return (decimal)cmd.ExecuteScalar();
			}
		} finally {
			conn.Close();
		}
	}
	
	public DataSet GetList(string pSiteCd,string pSexCd){
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append("SELECT ").AppendLine();
		oSqlBuilder.Append("	SITE_CD						,").AppendLine();
		oSqlBuilder.Append("	BULK_IMP_MAN_HISTORY_SEQ	,").AppendLine();
		oSqlBuilder.Append("	START_DATE					,").AppendLine();
		oSqlBuilder.Append("	END_DATE					,").AppendLine();
		oSqlBuilder.Append("	IMP_COUNT					,").AppendLine();
		oSqlBuilder.Append("	ERR_COUNT					,").AppendLine();
		oSqlBuilder.Append("	BULK_PROC_STATUS			,").AppendLine();
		oSqlBuilder.Append("	BULK_PROC_STATUS_NM			 ").AppendLine();
		oSqlBuilder.Append("FROM ").AppendLine();
		oSqlBuilder.Append("	VW_BULK_IMP_MAN_HISTORY01 ").AppendLine();
		oSqlBuilder.Append("WHERE ").AppendLine();
		oSqlBuilder.Append("	SITE_CD=:SITE_CD AND SEX_CD = :SEX_CD").AppendLine();
		oSqlBuilder.Append("ORDER BY ").AppendLine();
		oSqlBuilder.Append("	BULK_IMP_MAN_HISTORY_SEQ DESC ").AppendLine();	
		
		DataSet oDs = new DataSet();
		try{
			conn = DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(new OracleParameter(":SITE_CD", pSiteCd));
				cmd.Parameters.Add(new OracleParameter(":SEX_CD",pSexCd));
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs, "VW_BULK_IMP_MAN_HISTORY01");
				}			
			}
		}finally{
			conn.Close();
		}
		return oDs;
	}	
}
