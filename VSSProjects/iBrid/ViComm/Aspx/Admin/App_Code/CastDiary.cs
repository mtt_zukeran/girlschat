﻿/*************************************************************************
--	System			: ViComm
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Progaram ID		: CastDiary
--
--  Creation Date	: 2009.06.01
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using iBridCommLib;
using ViComm;

public class CastDiary:DbSession {
	public string year;
	public string month;
	public string day;
	public string diaryTitle;
	public string htmlDoc1;
	public string htmlDoc2;
	public string htmlDoc3;
	public string htmlDoc4;
	public string picSeq;
	public string reportDay;
	public string objPhotoImgPath;

	public CastDiary() {
	}

	public bool GetDiaryInfo(string pSiteCd,string pUserSeq,string pUserCharNo,string pReportDay) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;
		try{
			conn = DbConnect();

			string sSql = "SELECT " +
								"REPORT_DAY			," +
								"DIARY_TITLE		," +
								"HTML_DOC1			," +
								"HTML_DOC2			," +
								"HTML_DOC3			," +
								"HTML_DOC4			," +
								"PIC_SEQ			," +
								"OBJ_PHOTO_IMG_PATH	" +
							"FROM " +
								"VW_CAST_DIARY02 " +
							"WHERE " +
								"SITE_CD		= :SITE_CD		AND " +
								"USER_SEQ		= :USER_SEQ		AND " +
								"USER_CHAR_NO	= :USER_CHAR_NO	AND	" +
								"REPORT_DAY		= :REPORT_DAY ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				cmd.Parameters.Add("REPORT_DAY",pReportDay);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_DIARY02");

					if (ds.Tables["VW_CAST_DIARY02"].Rows.Count != 0) {
						dr = ds.Tables["VW_CAST_DIARY02"].Rows[0];
						year = dr["REPORT_DAY"].ToString().Substring(0,4);
						month = dr["REPORT_DAY"].ToString().Substring(5,2);
						day = dr["REPORT_DAY"].ToString().Substring(8,2);
						diaryTitle = dr["DIARY_TITLE"].ToString();
						htmlDoc1 = dr["HTML_DOC1"].ToString();
						htmlDoc2 = dr["HTML_DOC2"].ToString();
						htmlDoc3 = dr["HTML_DOC3"].ToString();
						htmlDoc4 = dr["HTML_DOC4"].ToString();
						picSeq = dr["PIC_SEQ"].ToString();
						objPhotoImgPath = dr["OBJ_PHOTO_IMG_PATH"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public int GetPageCount(string pSiteCd,string pCreateDayFrom,string pCreateTimeFrom,string pCreateDayTo,string pCreateTimeTo,string pLoginId,string pUserCharNo,string pKeyword,string pScreenId) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_CAST_DIARY03 ";
			string sWhere = string.Empty;

			OracleParameter[] oParms = CreateWhere(pSiteCd,pCreateDayFrom,pCreateTimeFrom,pCreateDayTo,pCreateTimeTo,pLoginId,pUserCharNo,pKeyword,pScreenId,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0; i < oParms.Length; i++) {
					cmd.Parameters.Add(oParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPages = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPages;
	}

	public DataSet GetPageCollection(string pSiteCd,string pCreateDayFrom,string pCreateTimeFrom,string pCreateDayTo,string pCreateTimeTo,string pLoginId,string pUserCharNo,string pKeyword,string pScreenId,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY CREATE_DATE DESC";

			string sSql = "SELECT " +
								"LOGIN_ID					," +
								"HANDLE_NM					," +
								"SITE_CD					," +
								"USER_SEQ					," +
								"REPORT_DAY					," +
								"CAST_DIARY_SUB_SEQ			," +
								"USER_CHAR_NO				," +
								"DIARY_TITLE				," +
								"HTML_DOC1					," +
								"HTML_DOC2					," +
								"HTML_DOC3					," +
								"HTML_DOC4					," +
								"CREATE_DATE				," +
								"UPDATE_DATE				," +
								"PIC_SEQ					," +
								"DEL_FLAG					," +
								"ADMIN_DEL_FLAG				," +
								"LIKE_COUNT					," +
								"OBJ_PHOTO_IMG_PATH			," +
								"OBJ_SMALL_PHOTO_IMG_PATH	," +
								"RNUM 						," +
								"ADMIN_CHECK_FLAG			" +
							string.Format("FROM(SELECT {0}.*, ROW_NUMBER() OVER ({1}) AS RNUM FROM {0} ","VW_CAST_DIARY03",sOrder);

			string sWhere = string.Empty;

			OracleParameter[] oParms = CreateWhere(pSiteCd,pCreateDayFrom,pCreateTimeFrom,pCreateDayTo,pCreateTimeTo,pLoginId,pUserCharNo,pKeyword,pScreenId,ref sWhere);

			sSql = sSql + sWhere;
			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;
			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.BindByName = true;
				for (int i = 0; i < oParms.Length; i++) {
					cmd.Parameters.Add(oParms[i]);
				}
				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_DIARY03");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,string pCreateDayFrom,string pCreateTimeFrom,string pCreateDayTo,string pCreateTimeTo,string pLoginId,string pUserCharNo,string pKeyword,string pScreenId,ref string pWhere) {
		SetDefaultTimeValue(ref pCreateTimeFrom,ref pCreateTimeTo);
		ArrayList list = new ArrayList();

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if ((!string.IsNullOrEmpty(pCreateDayFrom)) && (!string.IsNullOrEmpty(pCreateDayTo))) {
			DateTime dtFrom = DateTime.Parse(pCreateDayFrom + " " + pCreateTimeFrom + ":00:00");
			DateTime dtTo = DateTime.Parse(pCreateDayTo + " " + pCreateTimeTo + ":59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere("CREATE_DATE >= :CREATE_DATE_FROM AND CREATE_DATE < :CREATE_DATE_TO",ref pWhere);
			list.Add(new OracleParameter("CREATE_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("CREATE_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (!string.IsNullOrEmpty(pLoginId)) {
			SysPrograms.SqlAppendWhere("LOGIN_ID LIKE :LOGIN_ID||'%'",ref pWhere);
			list.Add(new OracleParameter("LOGIN_ID",pLoginId));

			if (!string.IsNullOrEmpty(pUserCharNo)) {
				SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhere);
				list.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));
			}
		}

		SysPrograms.SqlAppendWhere("WAIT_TX_FLAG = :WAIT_TX_FLAG",ref pWhere);
		list.Add(new OracleParameter("WAIT_TX_FLAG",ViCommConst.FLAG_OFF));

		if (!string.IsNullOrEmpty(pKeyword)) {
			StringBuilder oWhereKeywords = new StringBuilder("(");
			string[] sKeywordArray = pKeyword.Split(' ','　');
			for (int i = 0; i < sKeywordArray.Length; i++) {
				oWhereKeywords.AppendFormat("(DIARY_TITLE LIKE :KEYWORD{0}) OR (HTML_DOC1 LIKE :KEYWORD{0}) OR (HTML_DOC2 LIKE :KEYWORD{0}) OR (HTML_DOC3 LIKE :KEYWORD{0}) OR (HTML_DOC4 LIKE :KEYWORD{0})",i);
				list.Add(new OracleParameter(string.Format("KEYWORD{0}",i),string.Format("%{0}%",sKeywordArray[i])));

				if (i < sKeywordArray.Length - 1) {
					oWhereKeywords.AppendLine(" AND ");
				}
			}
			oWhereKeywords.Append(")");
			SysPrograms.SqlAppendWhere(oWhereKeywords.ToString(),ref pWhere);
		}

		if (string.IsNullOrEmpty(pScreenId)) {
			SysPrograms.SqlAppendWhere("SCREEN_ID IS NULL",ref pWhere);
		} else {
			SysPrograms.SqlAppendWhere("SCREEN_ID = :SCREEN_ID",ref pWhere);
			list.Add(new OracleParameter("SCREEN_ID",pScreenId));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	private void SetDefaultTimeValue(ref string pTimeFrom,ref string pTimeTo) {
		if (string.IsNullOrEmpty(pTimeFrom)) {
			pTimeFrom = "00";
		}
		if (string.IsNullOrEmpty(pTimeTo)) {
			pTimeTo = "23";
		}
	}

	public int GetUnconfirmCount(string pSiteCd,string pReportDayFrom) {
		int iValue;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_CAST_DIARY");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	ADMIN_CHECK_FLAG	= :ADMIN_CHECK_FLAG	AND	");
		oSqlBuilder.AppendLine("	CREATE_DATE			>= :CREATE_DATE		AND	");
		oSqlBuilder.AppendLine("	WAIT_TX_FLAG		= :WAIT_TX_FLAG		AND	");
		oSqlBuilder.AppendLine("	SCREEN_ID			IS NULL");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":ADMIN_CHECK_FLAG",ViCommConst.FLAG_OFF));
		DateTime dtFrom = DateTime.Parse(pReportDayFrom);
		oParamList.Add(new OracleParameter("CREATE_DATE",OracleDbType.Date,dtFrom,ParameterDirection.Input));
		oParamList.Add(new OracleParameter(":WAIT_TX_FLAG",ViCommConst.FLAG_OFF));

		iValue = ExecuteSelectCountQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return iValue;
	}
}
