/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 録音

--	Progaram ID		: Recording
--
--  Creation Date	: 2010.04.20
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class Recording:DbSession {

	public Recording() {
	}

	public int GetPageCount(string pSiteCd,string pRecType) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_RECORDING02 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pRecType,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pRecType,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY SITE_CD,RECORDING_DATE DESC ";
			string sSql = "SELECT " +
								"SITE_CD				," +
								"USER_SEQ				," +
								"USER_CHAR_NO			," +
								"PARTNER_USER_SEQ		," +
								"PARTNER_USER_CHAR_NO	," +
								"REC_SEQ				," +
								"RECORDING_DATE			," +
								"REC_TYPE				," +
								"FOWARD_TEL_NO			," +
								"PLAY_COUNT				," +
								"CALL_COUNT				," +
								"PARTNER_SITE_CD		," +
								"FOWARD_TERM_DATE		," +
								"WAITING_COMMENT		," +
								"SHORT_COMMENT			," +
								"WAITING_TYPE			," +
								"LOGIN_ID				," +
								"PARTNER_LOGIN_ID		," +
								"WAITING_TYPE_NM		," +
								"BAL_POINT				 " +
							"FROM(" +
							" SELECT VW_RECORDING02.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_RECORDING02  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pRecType,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_RECORDING02");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,string pRecType,ref string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();
		pWhere = pWhere + " WHERE SITE_CD = :SITE_CD AND REC_TYPE = :REC_TYPE ";
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		list.Add(new OracleParameter("REC_TYPE",pRecType));
		if (pRecType.Equals(ViCommConst.REC_TYPE_MAN_WAITING)) {
			pWhere = pWhere + " AND FOWARD_TERM_DATE >= :FOWARD_TERM_DATE ";
			list.Add(new OracleParameter("FOWARD_TERM_DATE",OracleDbType.Date,DateTime.Now,ParameterDirection.Input));
		}
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}