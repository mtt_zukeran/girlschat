﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: ｷｬｽﾄ用お宝ﾄﾞﾛｯﾌﾟ率
--	Progaram ID		: CastTreasureDropRate
--
--  Creation Date	: 2011.07.25
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;


public class CastTreasureDropRate : DbSession {

	public CastTreasureDropRate() {
	}

	public bool IsDupulicate(string pSiteCd,string pStageGroupType,string pCastTreasureSeq,string pGameCharacterType) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_CAST_TREASURE_DROP_RATE	");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	SITE_CD				= :pSITE_CD				AND");
		oSqlBuilder.AppendLine("	GAME_CHARACTER_TYPE	= :pGAME_CHARACTER_TYPE	AND");
		oSqlBuilder.AppendLine("	CAST_TREASURE_SEQ	= :pCAST_TREASURE_SEQ	AND");
		oSqlBuilder.AppendLine("	STAGE_GROUP_TYPE	= :pSTAGE_GROUP_TYPE	");

		try {
			int iCount = 0;
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(new OracleParameter("SITE_CD",pSiteCd));
				cmd.Parameters.Add(new OracleParameter("CAST_TREASURE_SEQ",pCastTreasureSeq));
				cmd.Parameters.Add(new OracleParameter("GAME_CHARACTER_TYPE",pGameCharacterType));
				cmd.Parameters.Add(new OracleParameter("STAGE_GROUP_TYPE", pStageGroupType));
				iCount = Convert.ToInt32(cmd.ExecuteScalar());
				return iCount > 0;
			}
		}
		finally {
			conn.Close();
		}
	}

	public int GetDropRateSummary(string pSiteCd, string pStageGroupType, string pGameCharacterType) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	NVL(SUM(DROP_RATE),0)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_CAST_TREASURE_DROP_RATE	");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD				AND");
		oSqlBuilder.AppendLine("	GAME_CHARACTER_TYPE	= :GAME_CHARACTER_TYPE	AND");
		oSqlBuilder.AppendLine("	STAGE_GROUP_TYPE	= :STAGE_GROUP_TYPE		");

		try {
			int iSummary = 0;
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(new OracleParameter("SITE_CD",pSiteCd));
				cmd.Parameters.Add(new OracleParameter("GAME_CHARACTER_TYPE",pGameCharacterType));
				cmd.Parameters.Add(new OracleParameter("STAGE_GROUP_TYPE", pStageGroupType));
				iSummary = Convert.ToInt32(cmd.ExecuteScalar());
				return iSummary;
			}
		} finally {
			conn.Close();
		}
	}

	public DataSet GetListByGameCharacterType(string pSiteCd, string pStageGroupType, string pGameCharacterType) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	A.SITE_CD						,");
		oSqlBuilder.AppendLine("	A.STAGE_GROUP_TYPE				,");
		oSqlBuilder.AppendLine("	A.GAME_CHARACTER_TYPE			,");
		oSqlBuilder.AppendLine("	A.GAME_CHARACTER_TYPE_NM		,");
		oSqlBuilder.AppendLine("	A.CAST_TREASURE_SEQ				,");
		oSqlBuilder.AppendLine("	A.CAST_TREASURE_NM				,");
		oSqlBuilder.AppendLine("	NVL(DR.DROP_RATE,0)	DROP_RATE	,");
		oSqlBuilder.AppendLine("	DR.REVISION_NO					,");
		oSqlBuilder.AppendLine("	DR.UPDATE_DATE					");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_CAST_TREASURE_DROP_RATE	DR,");
		oSqlBuilder.AppendLine("	(");
		oSqlBuilder.AppendLine("		SELECT	");
		oSqlBuilder.AppendLine("			SITE_CD											,");
		oSqlBuilder.AppendLine("			STAGE_GROUP_TYPE								,");
		oSqlBuilder.AppendLine("			:GAME_CHARACTER_TYPE	GAME_CHARACTER_TYPE		,");
		oSqlBuilder.AppendLine("			CODE_NM					GAME_CHARACTER_TYPE_NM	,");
		oSqlBuilder.AppendLine("			CAST_TREASURE_SEQ								,");
		oSqlBuilder.AppendLine("			CAST_TREASURE_NM								");
		oSqlBuilder.AppendLine("		FROM	");
		oSqlBuilder.AppendLine("			T_CAST_TREASURE	,");
		oSqlBuilder.AppendLine("			T_CODE_DTL	");
		oSqlBuilder.AppendLine("		WHERE		");
		oSqlBuilder.AppendLine("			T_CAST_TREASURE.STAGE_GROUP_TYPE	= :STAGE_GROUP_TYPE			AND	");
		oSqlBuilder.AppendLine("			'A4'								= T_CODE_DTL.CODE_TYPE	(+)	AND	");
		oSqlBuilder.AppendLine("			:GAME_CHARACTER_TYPE				= T_CODE_DTL.CODE		(+)	");
		oSqlBuilder.AppendLine("	)	A	");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	A.SITE_CD					= DR.SITE_CD				(+)	AND");
		oSqlBuilder.AppendLine("	A.CAST_TREASURE_SEQ			= DR.CAST_TREASURE_SEQ		(+)	AND");
		oSqlBuilder.AppendLine("	A.GAME_CHARACTER_TYPE		= DR.GAME_CHARACTER_TYPE	(+)	AND");
		oSqlBuilder.AppendLine("	A.STAGE_GROUP_TYPE			= DR.STAGE_GROUP_TYPE		(+)	AND");
		oSqlBuilder.AppendLine("	A.SITE_CD					= :SITE_CD						AND");
		oSqlBuilder.AppendLine("	A.GAME_CHARACTER_TYPE		= :GAME_CHARACTER_TYPE			");
		oSqlBuilder.AppendLine("ORDER BY A.SITE_CD, A.CAST_TREASURE_SEQ, A.STAGE_GROUP_TYPE	");

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":STAGE_GROUP_TYPE", pStageGroupType);
				cmd.Parameters.Add(":GAME_CHARACTER_TYPE",pGameCharacterType);

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}
}