﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者動画属性種別値
--	Progaram ID		: CastMovieAttrTypeValue
--
--  Creation Date	: 2010.05.17
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using ViComm;
using iBridCommLib;

public class CastMovieAttrTypeValue:DbSession {

	public CastMovieAttrTypeValue() {
	}

	public int GetPageCount(string pSiteCd) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_CAST_MOVIE_ATTR_TYPE_VALUE1 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();
			string sOrder = "ORDER BY SITE_CD,CAST_MOVIE_ATTR_TYPE_SEQ,CAST_MOVIE_ATTR_TYPE_PRIORITY,PRIORITY";

			string sSql = "SELECT " +
							"SITE_CD						," +
							"CAST_MOVIE_ATTR_TYPE_SEQ		," +
							"CAST_MOVIE_ATTR_SEQ			," +
							"CAST_MOVIE_ATTR_NM				," +
							"PRIORITY						," +
							"POSTER_DEL_NA_FLAG				," +
							"USE_INDIVIDUAL_PAY_FLAG		," +
							"POST_PAY_AMT					," +
							"VIEW_PAY_AMT					," +
							"USE_INDIVIDUAL_CHARGE_FLAG		," +
							"VIEWER_CHARGE_POINT			," +
							"UPLOAD_LIMIT_COUNT				," +
							"CAST_MOVIE_ATTR_TYPE_PRIORITY	," +
							"CAST_MOVIE_ATTR_TYPE_NM " +
							"FROM(" +
							" SELECT VW_CAST_MOVIE_ATTR_TYPE_VALUE1.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_CAST_MOVIE_ATTR_TYPE_VALUE1  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_MOVIE_ATTR_TYPE_VALUE1");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		pWhere += " WHERE SITE_CD = :SITE_CD AND CAST_MOVIE_ATTR_TYPE_SEQ <> :CAST_MOVIE_ATTR_TYPE_SEQ ";
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		list.Add(new OracleParameter("CAST_MOVIE_ATTR_TYPE_SEQ",ViCommConst.DEFAULT_CAST_MOVIE_ATTR_TYPE_SEQ));
		
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public DataSet GetListIncDefault(string pSiteCd,string pCastMovieAttrTypeSeq) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT CAST_MOVIE_ATTR_SEQ,CAST_MOVIE_ATTR_NM FROM T_CAST_MOVIE_ATTR_TYPE_VALUE WHERE SITE_CD = :SITE_CD AND CAST_MOVIE_ATTR_TYPE_SEQ = :CAST_MOVIE_ATTR_TYPE_SEQ ";
			sSql = sSql + " ORDER BY SITE_CD,PRIORITY";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("CAST_MOVIE_ATTR_TYPE_SEQ",pCastMovieAttrTypeSeq);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
}
