/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 未認証キャスト勧誘スケジュール
--	Progaram ID		: UnauthCastInviteSchedule
--
--  Creation Date	: 2010.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class UnauthCastInviteSchedule:DbSession {

	public string colorBack;
	public string colorChar;
	public string colorLink;

	public UnauthCastInviteSchedule() {
	}

	public int GetPageCount(string pSiteCd,string pInviteLevel) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_UNAUTH_CAST_INVITE_SCHE01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pInviteLevel,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pInviteLevel,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY SITE_CD,INVITE_LEVEL,INVITE_COUNT_PER_DAY ";
			string sSql = "SELECT " +
							"SITE_CD				," +
							"SITE_NM				," +
							"INVITE_LEVEL			," +
							"INVITE_COUNT_PER_DAY	," +
							"INVITE_START_TIME		," +
							"INVITE_END_TIME		," +
							"INVITE_LAST_EXEC_DATE	," +
							"UPDATE_DATE			," +
							"TEMPLATE_NM			" +
							"FROM(" +
							" SELECT VW_UNAUTH_CAST_INVITE_SCHE01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_UNAUTH_CAST_INVITE_SCHE01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pInviteLevel,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_UNAUTH_CAST_INVITE_SCHE01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pInviteLevel,ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		pWhere = pWhere + " WHERE SITE_CD = :SITE_CD ";
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		if (!string.IsNullOrEmpty(pInviteLevel)) {
			pWhere += "AND INVITE_LEVEL != :INVITE_LEVEL ";
			list.Add(new OracleParameter("INVITE_LEVEL",pInviteLevel));
		}
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}