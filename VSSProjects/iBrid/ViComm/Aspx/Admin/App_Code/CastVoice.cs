﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者ボイス
--	Progaram ID		: CastVoice
--
--  Creation Date	: 2010.07.27
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater		Update Explain

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Collections.Generic;
using iBridCommLib;
using ViComm;

public class CastVoice:DbSession {
	public CastVoice() {
	}

	public int GetPageCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pVoiceType) {
		decimal iPageCount = decimal.Zero;

		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_CAST_VOICE01 ";
			string sWhere = string.Empty;

			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pVoiceType,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				iPageCount = (cmd.ExecuteScalar() as decimal?) ?? 0;
			}
		} finally {
			conn.Close();
		}

		return Convert.ToInt32(iPageCount);
		;
	}


	public DataSet GetPageCollection(string pSiteCd,string pUserSeq,string pUserCharNo,string pVoiceType,int startRowIndex,int maximumRows) {

		string sSql = string.Empty;
		string sWhere = string.Empty;
		string sSortExpression = "ORDER BY SITE_CD,USER_SEQ,USER_CHAR_NO,VOICE_SEQ DESC ";

		StringBuilder objSqlBuilder = new StringBuilder();
		objSqlBuilder.Append(" SELECT                     ").AppendLine();
		objSqlBuilder.Append("     SITE_CD,               ").AppendLine();
		objSqlBuilder.Append("     USER_SEQ,              ").AppendLine();
		objSqlBuilder.Append("     USER_CHAR_NO,          ").AppendLine();
		objSqlBuilder.Append("     LOGIN_ID,              ").AppendLine();
		objSqlBuilder.Append("     VOICE_SEQ,             ").AppendLine();
		objSqlBuilder.Append("     VOICE_FILE_NM,         ").AppendLine();
		objSqlBuilder.Append("     VOICE_TITLE,           ").AppendLine();
		objSqlBuilder.Append("     VOICE_DOC,             ").AppendLine();
		objSqlBuilder.Append("     CHARGE_POINT,          ").AppendLine();
		objSqlBuilder.Append("     OBJ_NOT_APPROVE_FLAG,  ").AppendLine();
		objSqlBuilder.Append("     NOT_APPROVE_MARK,      ").AppendLine();
		objSqlBuilder.Append("     UPLOAD_DATE,           ").AppendLine();
		objSqlBuilder.Append("     VOICE_TYPE             ").AppendLine();
		objSqlBuilder.Append(" FROM                       ").AppendLine();
		objSqlBuilder.Append("     VW_CAST_VOICE01        ").AppendLine();

		OracleParameter[] objParams = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pVoiceType,ref sWhere);
		objSqlBuilder.Append(sWhere);

		OracleParameter[] objPagingParams
			= ViCommPrograms.CreatePagingSql(objSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sSql);

		DataSet ds = new DataSet();

		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd)) {

				this.cmd.Parameters.AddRange(objParams);
				this.cmd.Parameters.AddRange(objPagingParams);

				da.Fill(ds,"VW_CAST_VOICE01");
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,string pUserSeq,string pUserCharNo,string pVoiceType,ref string pWhere) {
		// 無駄なnull検索を防ぐためにAssersion
		if (string.IsNullOrEmpty(pSiteCd))
			throw new ArgumentException("pSiteCd");
		if (string.IsNullOrEmpty(pUserSeq))
			throw new ArgumentException("pUserSeq");
		if (string.IsNullOrEmpty(pUserCharNo))
			throw new ArgumentException("pUserCharNo");
		if (string.IsNullOrEmpty(pVoiceType))
			throw new ArgumentException("pVoiceType");


		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
		SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhere);
		SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhere);
		SysPrograms.SqlAppendWhere("VOICE_TYPE = :VOICE_TYPE",ref pWhere);

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter("SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter("USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));
		oParamList.Add(new OracleParameter("VOICE_TYPE",pVoiceType));

		return (OracleParameter[])oParamList.ToArray();
	}


	#region □■□ SEQ 取得関連 □■□ ============================================================

	public decimal GetVoiceNo() {
		try {
			conn = DbConnect();

			string sSql = "SELECT SEQ_OBJ.NEXTVAL FROM DUAL";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				return (decimal)cmd.ExecuteScalar();
			}
		} finally {
			conn.Close();
		}
	}

	public static decimal GetVoiceSeq() {
		using (CastVoice oVoice = new CastVoice()) {
			return oVoice.GetVoiceNo();
		}
	}

	#endregion ====================================================================================


}
