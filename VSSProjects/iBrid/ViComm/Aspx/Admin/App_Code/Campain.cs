﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: キャンペーン設定--	Progaram ID		: Campain
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;


public class Campain:DbSession {

	public Campain() {
	}

	public DataSet GetList(string pSiteCd,string pCampainType) {
		DataSet ds;
		try{
			conn = DbConnect("Campain.GetList");
			ds = new DataSet();

			string sSql = "SELECT CAMPAIN_CD,CAMPAIN_DESC FROM T_CAMPAIN " +
							"WHERE " +
							" SITE_CD	= :SITE_CD AND (CAMPAIN_TYPE IN ('0',:CAMPAIN_TYPE)) " +
							"ORDER BY " +
								"SITE_CD,CAMPAIN_CD ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("CAMPAIN_TYPE",pCampainType);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_CAMPAIN");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetCheckList(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	SITE_CD					,");
		oSqlBuilder.AppendLine("	CAMPAIN_CD				,");
		oSqlBuilder.AppendLine("	CAMPAIN_TYPE			,");
		oSqlBuilder.AppendLine("	CAMPAIN_DESC			,");
		oSqlBuilder.AppendLine("	APPLICATION_START_DATE	,");
		oSqlBuilder.AppendLine("	APPLICATION_END_DATE	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_CAMPAIN");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND CAMPAIN_CD != :CAMPAIN_CD");
		oSqlBuilder.AppendLine("ORDER BY	");
		oSqlBuilder.AppendLine("	SITE_CD, APPLICATION_START_DATE, APPLICATION_END_DATE");

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":CAMPAIN_CD","00");

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}

	public int GetPageCount(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT COUNT(*) FROM T_CAMPAIN WHERE SITE_CD = :SITE_CD AND CAMPAIN_CD != :CAMPAIN_CD");

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":CAMPAIN_CD","00");
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	SITE_CD					,");
		oSqlBuilder.AppendLine("	CAMPAIN_CD				,");
		oSqlBuilder.AppendLine("	CAMPAIN_TYPE			,");
		oSqlBuilder.AppendLine("	CAMPAIN_DESC			,");
		oSqlBuilder.AppendLine("	APPLICATION_START_DATE	,");
		oSqlBuilder.AppendLine("	APPLICATION_END_DATE	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_CAMPAIN");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD AND CAMPAIN_CD != :CAMPAIN_CD");

		string sPagingSql = string.Empty;
		string sSortExpression = "ORDER BY SITE_CD, APPLICATION_START_DATE, APPLICATION_END_DATE";

		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":CAMPAIN_CD","00");
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}

}