﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 商品

--	Progaram ID		: ProductBuyHistory
--
--  Creation Date	: 2010.12.06
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class ProductBuyHistory : DbSession {

	public ProductBuyHistory() { }

	public int GetPageCount(string pSiteCd, string pProductAgentCd, string pProductType, string pFromYYYY, string pFromMM, string pFromDD, string pToYYYY, string pToMM, string pToDD, string pProductId) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append("SELECT	COUNT(*)	").AppendLine();
		oSqlBuilder.Append("FROM	").AppendLine();
		oSqlBuilder.Append("	T_PRODUCT_BUY_HISTORY	H	,	").AppendLine();
		oSqlBuilder.Append("	T_USER					U	,	").AppendLine();
		oSqlBuilder.Append("	T_USER_MAN_CHARACTER	C	,	").AppendLine();
		oSqlBuilder.Append("	T_PRODUCT				P		").AppendLine();

		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd, pProductAgentCd, pProductType, pFromYYYY, pFromMM, pFromDD, pToYYYY, pToMM, pToDD, pProductId, ref sWhereClause);

		oSqlBuilder.Append(sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				cmd.BindByName = true;

				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd, string pProductAgentCd, string pProductType, string pFromYYYY, string pFromMM, string pFromDD, string pToYYYY, string pToMM, string pToDD, string pProductId, int startRowIndex, int maximumRows) {

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append("SELECT	").AppendLine();
		oSqlBuilder.Append("	H.BUY_DATE		,	").AppendLine();
		oSqlBuilder.Append("	U.LOGIN_ID		,	").AppendLine();
		oSqlBuilder.Append("	C.HANDLE_NM			").AppendLine();
		oSqlBuilder.Append("FROM	").AppendLine();
		oSqlBuilder.Append("	T_PRODUCT_BUY_HISTORY	H	,	").AppendLine();
		oSqlBuilder.Append("	T_USER					U	,	").AppendLine();
		oSqlBuilder.Append("	T_USER_MAN_CHARACTER	C	,	").AppendLine();
		oSqlBuilder.Append("	T_PRODUCT				P		").AppendLine();

		string sSortExpression = "ORDER BY H.BUY_DATE DESC, U.LOGIN_ID";
		string sWhereClause = string.Empty;
		string sPagesSql = string.Empty;

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd, pProductAgentCd, pProductType, pFromYYYY, pFromMM, pFromDD, pToYYYY, pToMM, pToDD, pProductId, ref sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.AppendLine(sWhereClause).ToString(), sSortExpression, startRowIndex, maximumRows, out sPagesSql);

		DataSet oProductDataSet = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagesSql,conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);
				cmd.BindByName = true;

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oProductDataSet);
				}
			}
		} finally {
			conn.Close();
		}
		return oProductDataSet;
	}

	private OracleParameter[] CreateWhere(string pSiteCd, string pProductAgentCd, string pProductType, string pFromYYYY, string pFromMM, string pFromDD, string pToYYYY, string pToMM, string pToDD, string pProductId, ref string pWhere) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append("WHERE	").AppendLine();
		oSqlBuilder.Append("	P.SITE_CD			=	H.SITE_CD			AND").AppendLine();
		oSqlBuilder.Append("	P.PRODUCT_AGENT_CD	=	H.PRODUCT_AGENT_CD	AND").AppendLine();
		oSqlBuilder.Append("	P.PRODUCT_SEQ		=	H.PRODUCT_SEQ		AND").AppendLine();
		oSqlBuilder.Append("	H.SITE_CD			=	C.SITE_CD			AND").AppendLine();
		oSqlBuilder.Append("	H.USER_SEQ			=	C.USER_SEQ			AND").AppendLine();
		oSqlBuilder.Append("	H.USER_CHAR_NO		=	C.USER_CHAR_NO		AND").AppendLine();
		oSqlBuilder.Append("	H.USER_SEQ			=	U.USER_SEQ			AND").AppendLine();
		oSqlBuilder.Append("	P.SITE_CD			=	:SITE_CD			AND").AppendLine();
		oSqlBuilder.Append("	P.PRODUCT_ID		=	:PRODUCT_ID			AND").AppendLine();
		oSqlBuilder.Append("	P.PRODUCT_TYPE		=	:PRODUCT_TYPE		AND").AppendLine();
		oSqlBuilder.Append("	(H.BUY_DATE >= :BUY_DATE_FROM AND H.BUY_DATE < :BUY_DATE_TO)");

		DateTime dtFrom = DateTime.Parse(string.Format("{0}/{1}/{2} 00:00:00", pFromYYYY, pFromMM, pFromDD));
		DateTime dtTo = DateTime.Parse(string.Format("{0}/{1}/{2} 23:59:59", pToYYYY, pToMM, pToDD)).AddSeconds(1);

		List<OracleParameter> oParameters = new List<OracleParameter>();
		oParameters.Add(new OracleParameter(":SITE_CD", pSiteCd));
		oParameters.Add(new OracleParameter(":PRODUCT_TYPE", pProductType));
		oParameters.Add(new OracleParameter(":PRODUCT_ID", pProductId.TrimEnd()));
		oParameters.Add(new OracleParameter(":BUY_DATE_FROM", dtFrom));
		oParameters.Add(new OracleParameter(":BUY_DATE_TO", dtTo));

		if (!string.IsNullOrEmpty(pProductAgentCd)) {
			oSqlBuilder.AppendLine("	AND");
			oSqlBuilder.AppendLine("	P.PRODUCT_AGENT_CD	=	:PRODUCT_AGENT_CD");
			oParameters.Add(new OracleParameter(":PRODUCT_AGENT_CD", pProductAgentCd));
		}

		pWhere = oSqlBuilder.ToString();
		return oParameters.ToArray();
	}

	public int GetPageCountByUser(string pSiteCd, string pUserSeq, string pUserCharNo) {

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_PRODUCT				P,");
		oSqlBuilder.AppendLine("	T_PRODUCT_BUY_HISTORY	H");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	P.SITE_CD			=	H.SITE_CD				AND");
		oSqlBuilder.AppendLine("	P.PRODUCT_AGENT_CD	=	H.PRODUCT_AGENT_CD		AND");
		oSqlBuilder.AppendLine("	P.PRODUCT_SEQ		=	H.PRODUCT_SEQ			AND");
		oSqlBuilder.AppendLine("	H.SITE_CD			=	:SITE_CD				AND");
		oSqlBuilder.AppendLine("	H.USER_SEQ			=	:USER_SEQ				AND");
		oSqlBuilder.AppendLine("	H.USER_CHAR_NO		=	:USER_CHAR_NO");

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(new OracleParameter(":SITE_CD", pSiteCd));
				cmd.Parameters.Add(new OracleParameter(":USER_SEQ", pUserSeq));
				cmd.Parameters.Add(new OracleParameter(":USER_CHAR_NO", pUserCharNo));
				cmd.BindByName = true;

				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollectionByUser(string pSiteCd, string pUserSeq, string pUserCharNo, int startRowIndex, int maximumRows) {

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	H.BUY_DATE		,");
		oSqlBuilder.AppendLine("	P.PRODUCT_ID	,");
		oSqlBuilder.AppendLine("	P.PRODUCT_NM	,");
		oSqlBuilder.AppendLine("	CASE WHEN H.SPECIAL_CHARGE_POINT = 0 THEN H.CHARGE_POINT ELSE H.SPECIAL_CHARGE_POINT END POINT");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_PRODUCT				P,");
		oSqlBuilder.AppendLine("	T_PRODUCT_BUY_HISTORY	H");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	P.SITE_CD			=	H.SITE_CD				AND");
		oSqlBuilder.AppendLine("	P.PRODUCT_AGENT_CD	=	H.PRODUCT_AGENT_CD		AND");
		oSqlBuilder.AppendLine("	P.PRODUCT_SEQ		=	H.PRODUCT_SEQ			AND");
		oSqlBuilder.AppendLine("	H.SITE_CD			=	:SITE_CD				AND");
		oSqlBuilder.AppendLine("	H.USER_SEQ			=	:USER_SEQ				AND");
		oSqlBuilder.AppendLine("	H.USER_CHAR_NO		=	:USER_CHAR_NO");

		string sSortExpression = "ORDER BY H.BUY_DATE DESC";
		string sPagesSql = string.Empty;

		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(), sSortExpression, startRowIndex, maximumRows, out sPagesSql);

		DataSet oProductDataSet = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagesSql,conn)) {
				cmd.Parameters.AddRange(oPagingParams);
				cmd.Parameters.Add(new OracleParameter(":SITE_CD", pSiteCd));
				cmd.Parameters.Add(new OracleParameter(":USER_SEQ", pUserSeq));
				cmd.Parameters.Add(new OracleParameter(":USER_CHAR_NO", pUserCharNo));
				cmd.BindByName = true;

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oProductDataSet);
				}
			}
		} finally {
			conn.Close();
		}
		return oProductDataSet;
	}
}
