﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: ブログ投稿


--	Progaram ID		: BlogArticle
--  Creation Date	: 2011.03.24
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class BlogArticle : DbSession {

	#region SearchCondition
	public class SearchCondition {
		private string siteCd;
		public string SiteCd { get { return this.siteCd; } set { this.siteCd = value; } }
		private string loginId;
		public string LoginId { get { return this.loginId; } set { this.loginId = value; } }
		private string registDateFrom;
		public string RegistDateFrom { get { return this.registDateFrom; } set { this.registDateFrom = value; } }
		private string registDateTo;
		public string RegistDateTo { get { return this.registDateTo; } set { this.registDateTo = value; } }
		private string keyword;
		public string Keyword { get { return this.keyword; } set { this.keyword = value; } }
		private string blogArticleStatus;
		public string BlogArticleStatus { get { return this.blogArticleStatus; } set { this.blogArticleStatus = value; } }
		private bool attachedPicFlag;
		public bool AttachedPicFlag { get { return this.attachedPicFlag; } set { this.attachedPicFlag = value; } }
		private bool attachedMovieFlag;
		public bool AttachedMovieFlag { get { return this.attachedMovieFlag; } set { this.attachedMovieFlag = value; } }
		private string pickupFlag;
		public string PickupFlag { get { return this.pickupFlag; } set { this.pickupFlag = value; } }
		private string checkedFlag;
		public string CheckedFlag { get { return this.checkedFlag; } set { this.checkedFlag = value; } }
		private string sortDirection;
		public string SortDirection { get { return this.sortDirection; } set { this.sortDirection = value; } }
		private string sortExpression;
		public string SortExpression { get { return this.sortExpression; } set { this.sortExpression = value; } }
	}
	#endregion SearchCondition

	public BlogArticle() {
	}

	public int GetPageCount(object pSearchCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_BLOG_ARTICLE			BA	,");
		oSqlBuilder.AppendLine("	T_BLOG_ARTICLE_PICKUP	BAP	,");
		oSqlBuilder.AppendLine("	T_BLOG_OBJS				BO	,");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER		CC	,");
		oSqlBuilder.AppendLine("	T_USER					U	");

		string sWhereClause;
		OracleParameter[] oWhereParams = this.CreateWhere((SearchCondition)pSearchCondition, out sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(), conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(object pSearchCondition, int startRowIndex, int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	U.LOGIN_ID				,");
		oSqlBuilder.AppendLine("	CC.HANDLE_NM			,");
		oSqlBuilder.AppendLine("	BA.SITE_CD				,");
		oSqlBuilder.AppendLine("	BA.USER_SEQ				,");
		oSqlBuilder.AppendLine("	BA.USER_CHAR_NO			,");
		oSqlBuilder.AppendLine("	BA.BLOG_SEQ				,");
		oSqlBuilder.AppendLine("	BA.BLOG_ARTICLE_SEQ		,");
		oSqlBuilder.AppendLine("	BA.BLOG_ARTICLE_STATUS	,");
		oSqlBuilder.AppendLine("	BA.BLOG_ARTICLE_TITLE	,");
		oSqlBuilder.AppendLine("	BA.BLOG_ARTICLE_BODY1	,");
		oSqlBuilder.AppendLine("	BA.BLOG_ARTICLE_BODY2	,");
		oSqlBuilder.AppendLine("	BA.BLOG_ARTICLE_BODY3	,");
		oSqlBuilder.AppendLine("	BA.BLOG_ARTICLE_BODY4	,");
		oSqlBuilder.AppendLine("	BA.BLOG_ARTICLE_BODY5	,");
		oSqlBuilder.AppendLine("	BA.OBJ_SEQ				,");
		oSqlBuilder.AppendLine("	BA.READING_COUNT		,");
		oSqlBuilder.AppendLine("	BA.COMMENT_COUNT		,");
		oSqlBuilder.AppendLine("	BA.POINT_ACQUIRED_FLAG	,");
		oSqlBuilder.AppendLine("	BA.ATTACHED_PIC_FLAG	,");
		oSqlBuilder.AppendLine("	BA.ATTACHED_MOVIE_FLAG	,");
		oSqlBuilder.AppendLine("	BA.REGIST_DATE			,");
		oSqlBuilder.AppendLine("	BA.PUBLISH_START_DATE	,");
		oSqlBuilder.AppendLine("	BA.REVISION_NO			,");
		oSqlBuilder.AppendLine("	BA.DEL_FLAG				,");
		oSqlBuilder.AppendLine("	BAP.PRIORITY			,");
		oSqlBuilder.AppendLine("	BO.BLOG_FILE_TYPE		,");
		oSqlBuilder.AppendLine("	CASE WHEN BO.BLOG_FILE_TYPE = '1' THEN GET_PHOTO_IMG_PATH(BA.SITE_CD, U.LOGIN_ID, BA.OBJ_SEQ) END PHOTO_IMG_PATH,");
		oSqlBuilder.AppendLine("	CASE WHEN BO.BLOG_FILE_TYPE = '1' THEN GET_SMALL_PHOTO_IMG_PATH(BA.SITE_CD, U.LOGIN_ID, BA.OBJ_SEQ) END SMALL_PHOTO_IMG_PATH");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_BLOG_ARTICLE			BA	,");
		oSqlBuilder.AppendLine("	T_BLOG_ARTICLE_PICKUP	BAP	,");
		oSqlBuilder.AppendLine("	T_BLOG_OBJS				BO	,");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER		CC	,");
		oSqlBuilder.AppendLine("	T_USER					U	");

		string sWhereClause;
		string sSortExpression = this.GetOrder((SearchCondition)pSearchCondition);
		string sPagingSql;

		OracleParameter[] oWhereParams = this.CreateWhere((SearchCondition)pSearchCondition, out sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(), sSortExpression, startRowIndex, maximumRows, out sPagingSql);

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql, conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}

	private string GetOrder(SearchCondition pSearchCondition) {
		if (string.IsNullOrEmpty(pSearchCondition.SortExpression)) {
			return "ORDER BY BA.REGIST_DATE DESC, U.LOGIN_ID";
		} else {
			return string.Format("ORDER BY {0} {1}, REGIST_DATE DESC", pSearchCondition.SortExpression, pSearchCondition.SortDirection);
		}
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition, out string pWhere) {
		StringBuilder oWhereBuilder = new StringBuilder();
		oWhereBuilder.AppendLine("WHERE	");
		oWhereBuilder.AppendLine("	BA.SITE_CD				= CC.SITE_CD				AND");
		oWhereBuilder.AppendLine("	BA.USER_SEQ				= CC.USER_SEQ				AND");
		oWhereBuilder.AppendLine("	BA.USER_CHAR_NO			= CC.USER_CHAR_NO			AND");
		oWhereBuilder.AppendLine("	BA.USER_SEQ				= U.USER_SEQ				AND");
		oWhereBuilder.AppendLine("	BA.SITE_CD				= BAP.SITE_CD			(+)	AND");
		oWhereBuilder.AppendLine("	BA.USER_SEQ				= BAP.USER_SEQ			(+)	AND");
		oWhereBuilder.AppendLine("	BA.USER_CHAR_NO			= BAP.USER_CHAR_NO		(+)	AND");
		oWhereBuilder.AppendLine("	BA.BLOG_SEQ				= BAP.BLOG_SEQ			(+)	AND");
		oWhereBuilder.AppendLine("	BA.BLOG_ARTICLE_SEQ		= BAP.BLOG_ARTICLE_SEQ	(+)	AND");
		oWhereBuilder.AppendLine("	BA.SITE_CD				= BO.SITE_CD			(+)	AND");
		oWhereBuilder.AppendLine("	BA.USER_SEQ				= BO.USER_SEQ			(+)	AND");
		oWhereBuilder.AppendLine("	BA.USER_CHAR_NO			= BO.USER_CHAR_NO		(+)	AND");
		oWhereBuilder.AppendLine("	BA.OBJ_SEQ				= BO.OBJ_SEQ			(+)	AND");
		oWhereBuilder.AppendLine("	BA.BLOG_ARTICLE_STATUS	<> '1'						");
		pWhere = oWhereBuilder.ToString();

		List<OracleParameter> oOracleParameterList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("BA.SITE_CD = :SITE_CD", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("SITE_CD", pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.LoginId)) {
			SysPrograms.SqlAppendWhere("U.LOGIN_ID = :LOGIN_ID", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("LOGIN_ID", pSearchCondition.LoginId));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.BlogArticleStatus)) {
			SysPrograms.SqlAppendWhere("BA.BLOG_ARTICLE_STATUS = :BLOG_ARTICLE_STATUS", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("BLOG_ARTICLE_STATUS", pSearchCondition.BlogArticleStatus));
		}

		if (pSearchCondition.AttachedPicFlag && !pSearchCondition.AttachedMovieFlag) {
			SysPrograms.SqlAppendWhere("(BA.ATTACHED_PIC_FLAG = :ATTACHED_PIC_FLAG_ON)", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("ATTACHED_PIC_FLAG_ON", ViCommConst.FLAG_ON));
		} else if (!pSearchCondition.AttachedPicFlag && pSearchCondition.AttachedMovieFlag) {
			SysPrograms.SqlAppendWhere("(BA.ATTACHED_MOVIE_FLAG = :ATTACHED_MOVIE_FLAG_ON)", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("ATTACHED_MOVIE_FLAG_ON", ViCommConst.FLAG_ON));
		} else if (!pSearchCondition.AttachedPicFlag && !pSearchCondition.AttachedMovieFlag) {
			SysPrograms.SqlAppendWhere("(BA.ATTACHED_PIC_FLAG = :ATTACHED_PIC_FLAG_ON AND BA.ATTACHED_MOVIE_FLAG = :ATTACHED_MOVIE_FLAG_ON)", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("ATTACHED_PIC_FLAG_ON", ViCommConst.FLAG_ON));
			oOracleParameterList.Add(new OracleParameter("ATTACHED_MOVIE_FLAG_ON", ViCommConst.FLAG_ON));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.PickupFlag)) {
			if (ViCommConst.FLAG_ON_STR.Equals(pSearchCondition.PickupFlag)) {
				SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_BLOG_ARTICLE_PICKUP P WHERE P.SITE_CD = BA.SITE_CD AND P.USER_SEQ = BA.USER_SEQ AND P.USER_CHAR_NO = BA.USER_CHAR_NO AND P.BLOG_SEQ = BA.BLOG_SEQ AND P.BLOG_ARTICLE_SEQ = BA.BLOG_ARTICLE_SEQ )", ref pWhere);
			} else if (ViCommConst.FLAG_OFF_STR.EndsWith(pSearchCondition.PickupFlag)) {
				SysPrograms.SqlAppendWhere("NOT EXISTS (SELECT 1 FROM T_BLOG_ARTICLE_PICKUP P WHERE P.SITE_CD = BA.SITE_CD AND P.USER_SEQ = BA.USER_SEQ AND P.USER_CHAR_NO = BA.USER_CHAR_NO AND P.BLOG_SEQ = BA.BLOG_SEQ AND P.BLOG_ARTICLE_SEQ = BA.BLOG_ARTICLE_SEQ )", ref pWhere);
			}
		}

		if (!string.IsNullOrEmpty(pSearchCondition.RegistDateFrom) && !string.IsNullOrEmpty(pSearchCondition.RegistDateTo)) {
			DateTime dtFrom = DateTime.Parse(pSearchCondition.RegistDateFrom);
			DateTime dtTo = DateTime.Parse(pSearchCondition.RegistDateTo).AddSeconds(1);
			SysPrograms.SqlAppendWhere("(BA.REGIST_DATE >= :REGIST_DATE_FROM AND BA.REGIST_DATE <= :REGIST_DATE_TO)", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("REGIST_DATE_FROM", OracleDbType.Date, dtFrom, ParameterDirection.Input));
			oOracleParameterList.Add(new OracleParameter("REGIST_DATE_TO", OracleDbType.Date, dtTo, ParameterDirection.Input));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.Keyword)) {
			StringBuilder oWhereKeywords = new StringBuilder("(");
			string[] sKeywordArray = pSearchCondition.Keyword.Split(' ', '　');
			for (int i = 0; i < sKeywordArray.Length; i++) {
				oWhereKeywords.AppendFormat("(BA.BLOG_ARTICLE_BODY1 LIKE :BLOG_ARTICLE_BODY{0}) OR (BA.BLOG_ARTICLE_BODY2 LIKE :BLOG_ARTICLE_BODY{0}) OR (BA.BLOG_ARTICLE_BODY3 LIKE :BLOG_ARTICLE_BODY{0}) OR (BA.BLOG_ARTICLE_BODY4 LIKE :BLOG_ARTICLE_BODY{0}) OR (BA.BLOG_ARTICLE_BODY5 LIKE :BLOG_ARTICLE_BODY{0})", i);
				oOracleParameterList.Add(new OracleParameter(string.Format("BLOG_ARTICLE_BODY{0}", i), string.Format("%{0}%", sKeywordArray[i])));

				if (i < sKeywordArray.Length - 2) {
					oWhereKeywords.AppendLine(" AND ");
				}
			}
			oWhereKeywords.Append(")");
			SysPrograms.SqlAppendWhere(oWhereKeywords.ToString(), ref pWhere);
		}

		// チェック状態
		if (!string.IsNullOrEmpty(pSearchCondition.CheckedFlag)) {
			SysPrograms.SqlAppendWhere("(BA.CHECKED_FLAG = :CHECKED_FLAG)",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("CHECKED_FLAG",pSearchCondition.CheckedFlag));
		}

		return oOracleParameterList.ToArray();
	}

	public DataSet GetOne(string pSiteCd, string pUserSeq, string pUserCharNo, string pBlogSeq, string pBlogArticleSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	U.LOGIN_ID				,");
		oSqlBuilder.AppendLine("	CC.HANDLE_NM			,");
		oSqlBuilder.AppendLine("	BA.SITE_CD				,");
		oSqlBuilder.AppendLine("	BA.USER_SEQ				,");
		oSqlBuilder.AppendLine("	BA.USER_CHAR_NO			,");
		oSqlBuilder.AppendLine("	BA.BLOG_SEQ				,");
		oSqlBuilder.AppendLine("	BA.BLOG_ARTICLE_SEQ		,");
		oSqlBuilder.AppendLine("	BA.BLOG_ARTICLE_STATUS	,");
		oSqlBuilder.AppendLine("	BA.BLOG_ARTICLE_TITLE	,");
		oSqlBuilder.AppendLine("	BA.BLOG_ARTICLE_BODY1	,");
		oSqlBuilder.AppendLine("	BA.BLOG_ARTICLE_BODY2	,");
		oSqlBuilder.AppendLine("	BA.BLOG_ARTICLE_BODY3	,");
		oSqlBuilder.AppendLine("	BA.BLOG_ARTICLE_BODY4	,");
		oSqlBuilder.AppendLine("	BA.BLOG_ARTICLE_BODY5	,");
		oSqlBuilder.AppendLine("	BA.OBJ_SEQ				,");
		oSqlBuilder.AppendLine("	BA.READING_COUNT		,");
		oSqlBuilder.AppendLine("	BA.POINT_ACQUIRED_FLAG	,");
		oSqlBuilder.AppendLine("	BA.ATTACHED_PIC_FLAG	,");
		oSqlBuilder.AppendLine("	BA.ATTACHED_MOVIE_FLAG	,");
		oSqlBuilder.AppendLine("	BA.REGIST_DATE			,");
		oSqlBuilder.AppendLine("	BA.PUBLISH_START_DATE	,");
		oSqlBuilder.AppendLine("	BA.REVISION_NO			,");
		oSqlBuilder.AppendLine("	BA.DEL_FLAG				,");
		oSqlBuilder.AppendLine("	BAP.PRIORITY			");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_BLOG_ARTICLE			BA	,");
		oSqlBuilder.AppendLine("	T_BLOG_ARTICLE_PICKUP	BAP	,");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER		CC	,");
		oSqlBuilder.AppendLine("	T_USER					U	");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	BA.SITE_CD			= CC.SITE_CD				AND");
		oSqlBuilder.AppendLine("	BA.USER_SEQ			= CC.USER_SEQ				AND");
		oSqlBuilder.AppendLine("	BA.USER_CHAR_NO		= CC.USER_CHAR_NO			AND");
		oSqlBuilder.AppendLine("	BA.USER_SEQ			= U.USER_SEQ				AND");
		oSqlBuilder.AppendLine("	BA.SITE_CD			= BAP.SITE_CD			(+)	AND");
		oSqlBuilder.AppendLine("	BA.USER_SEQ			= BAP.USER_SEQ			(+)	AND");
		oSqlBuilder.AppendLine("	BA.USER_CHAR_NO		= BAP.USER_CHAR_NO		(+)	AND");
		oSqlBuilder.AppendLine("	BA.BLOG_SEQ			= BAP.BLOG_SEQ			(+)	AND");
		oSqlBuilder.AppendLine("	BA.BLOG_ARTICLE_SEQ	= BAP.BLOG_ARTICLE_SEQ	(+)	AND");
		oSqlBuilder.AppendLine("	BA.SITE_CD			= :SITE_CD					AND");
		oSqlBuilder.AppendLine("	BA.USER_SEQ			= :USER_SEQ					AND");
		oSqlBuilder.AppendLine("	BA.USER_CHAR_NO		= :USER_CHAR_NO				AND");
		oSqlBuilder.AppendLine("	BA.BLOG_SEQ			= :BLOG_SEQ					AND");
		oSqlBuilder.AppendLine("	BA.BLOG_ARTICLE_SEQ	= :BLOG_ARTICLE_SEQ			");

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(), conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(new OracleParameter("SITE_CD", pSiteCd));
				cmd.Parameters.Add(new OracleParameter("USER_SEQ", pUserSeq));
				cmd.Parameters.Add(new OracleParameter("USER_CHAR_NO", pUserCharNo));
				cmd.Parameters.Add(new OracleParameter("BLOG_SEQ", pBlogSeq));
				cmd.Parameters.Add(new OracleParameter("BLOG_ARTICLE_SEQ", pBlogArticleSeq));

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}

	/// <summary>
	/// 未確認件数の取得
	/// (対象：非表示かつ未チェック)
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <returns></returns>
	public int GetUncofirmCount(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_BLOG_ARTICLE");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD");
		oSqlBuilder.AppendLine("	AND BLOG_ARTICLE_STATUS = :BLOG_ARTICLE_STATUS");
		oSqlBuilder.AppendLine("	AND CHECKED_FLAG = :CHECKED_FLAG");

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(new OracleParameter("SITE_CD",pSiteCd));
				cmd.Parameters.Add(new OracleParameter("BLOG_ARTICLE_STATUS",ViCommConst.BlogArticleStatus.PRIVATE));
				cmd.Parameters.Add(new OracleParameter("CHECKED_FLAG",ViCommConst.FLAG_OFF));
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	/// <summary>
	/// チェック済みフラグを変更
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pUserSeq"></param>
	/// <param name="pUserCharNo"></param>
	/// <param name="pBlogSeq"></param>
	/// <param name="pBlogArticleSeq"></param>
	/// <param name="pCheckedFlag"></param>
	/// <returns></returns>
	public string CheckBlogArticleMainte(string pSiteCd,string pUserSeq,string pUserCharNo,string pBlogSeq,string pBlogArticleSeq,string pCheckedFlag) {
		string pStatus = string.Empty;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("BLOG_ARTICLE_CHECK");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("pBLOG_SEQ",DbSession.DbType.VARCHAR2,pBlogSeq);
			db.ProcedureInParm("pBLOG_ARTICLE_SEQ",DbSession.DbType.VARCHAR2,pBlogArticleSeq);
			db.ProcedureInParm("pCHECKED_FLAG",DbSession.DbType.VARCHAR2,pCheckedFlag);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			pStatus = db.GetStringValue("pSTATUS");
		}
		return pStatus;
	}
}
