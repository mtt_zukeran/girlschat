﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 商品ジャンル

--	Progaram ID		: ProductGenre
--
--  Creation Date	: 2010.12.07
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class ProductGenre :DbSession{
	public ProductGenre() {}
	
	public DataSet GetList(string pSiteCd, string pGenreCategoryCd){
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine(" SELECT						");
		oSqlBuilder.AppendLine("	SITE_CD				,	");
		oSqlBuilder.AppendLine("	PRODUCT_GENRE_CD    ,	");
		oSqlBuilder.AppendLine("	PRODUCT_GENRE_NM    ,	");
		oSqlBuilder.AppendLine("	PRIORITY            	");		
		oSqlBuilder.AppendLine(" FROM				");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE ");
		oSqlBuilder.AppendLine(" WHERE				");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE.SITE_CD						= :SITE_CD						AND	");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE.PRODUCT_GENRE_CATEGORY_CD	= :PRODUCT_GENRE_CATEGORY_CD		");
		oSqlBuilder.AppendLine(" ORDER BY						");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE.PRIORITY	");
		
		DataSet oDs = new DataSet();
		try{
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD", pSiteCd);
				cmd.Parameters.Add(":PRODUCT_GENRE_CATEGORY_CD", pGenreCategoryCd);
				
				using(da = new OracleDataAdapter(cmd)){
					da.Fill(oDs);
				}
			}
		}finally{
			conn.Close();
		}
		return oDs;		
	}

    public int GetPageCount(string pSiteCd, string pProductType) {
        StringBuilder oSqlBuilder = new StringBuilder();
        oSqlBuilder.AppendLine(" SELECT						");
        oSqlBuilder.AppendLine("	COUNT(*)		").AppendLine();
		oSqlBuilder.AppendLine(" FROM				 ");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE, ");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY       ");
		oSqlBuilder.AppendLine(" WHERE				");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE.SITE_CD								= :SITE_CD									AND	");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY.PRODUCT_TYPE				= :PRODUCT_TYPE								AND	");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY.SITE_CD					= T_PRODUCT_GENRE.SITE_CD					AND	");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY.PRODUCT_GENRE_CATEGORY_CD	= T_PRODUCT_GENRE.PRODUCT_GENRE_CATEGORY_CD	");

        int iPageCount = 0;
        try {
            conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
                cmd.Parameters.Add(":SITE_CD", pSiteCd);
                cmd.Parameters.Add(":PRODUCT_TYPE", pProductType);
                iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
            }
        } finally {
            conn.Close();
        }
        return iPageCount;
    }
    public DataSet GetPageCollection(string pSiteCd, string pProductType, int startRowIndex, int maximumRows) {
        StringBuilder oSqlBuilder = new StringBuilder();

        oSqlBuilder.AppendLine(" SELECT						        ");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE.SITE_CD							        ,	");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE.PRODUCT_GENRE_CATEGORY_CD			    ,	");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE.PRODUCT_GENRE_CD			            ,	");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE.PRODUCT_GENRE_NM			            ,	");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE.PUBLISH_FLAG							,	");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE.PRIORITY			            	    ,   ");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY.PRODUCT_GENRE_CATEGORY_NM	    ,   ");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY.PRODUCT_TYPE				        "); 
		oSqlBuilder.AppendLine(" FROM				 ");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE, ");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY       ");
        oSqlBuilder.AppendLine(" WHERE				");
        oSqlBuilder.AppendLine("	T_PRODUCT_GENRE.SITE_CD								= :SITE_CD									AND	");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY.PRODUCT_TYPE				= :PRODUCT_TYPE								AND	");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY.SITE_CD					= T_PRODUCT_GENRE.SITE_CD					AND	");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY.PRODUCT_GENRE_CATEGORY_CD	= T_PRODUCT_GENRE.PRODUCT_GENRE_CATEGORY_CD	");

        string sPagingSql = string.Empty;
        string sSortExpression = "ORDER BY T_PRODUCT_GENRE.PRODUCT_GENRE_CATEGORY_CD, T_PRODUCT_GENRE.PRODUCT_GENRE_CD, T_PRODUCT_GENRE.PRIORITY";
        OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(), sSortExpression, startRowIndex, maximumRows, out sPagingSql);
        
        DataSet oDs = new DataSet();
        try {
            conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
                cmd.BindByName = true;
                cmd.Parameters.Add(":SITE_CD", pSiteCd);
                cmd.Parameters.Add(":PRODUCT_TYPE", pProductType);
                cmd.Parameters.AddRange(oPagingParams);

                using (da = new OracleDataAdapter(cmd)) {
                    da.Fill(oDs);
                }
            }
        } finally {
            conn.Close();
        }
        return oDs;
    }
}
