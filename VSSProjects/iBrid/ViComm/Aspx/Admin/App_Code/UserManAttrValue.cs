﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 男性会員属性値
--	Progaram ID		: UserManAttrValue
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  2009/07/24  iBrid(Y.Inoue)

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;

public class UserManAttrValue:DbSession {

	public UserManAttrValue() {
	}


	public DataSet GetList(string pSiteCd,string pUserSeq,string pUserCharNo) {

		DataSet ds = new DataSet();
		string sSql;
		try{
			conn = DbConnect("UserManAttrTypeValue.GetList");

			sSql = "SELECT " +
						"MAN_ATTR_TYPE_SEQ		," +
						"MAN_ATTR_TYPE_NM		," +
						"MAN_ATTR_SEQ			," +
						"MAN_ATTR_NM			," +
						"DISPLAY_VALUE			," +
						"INPUT_TYPE				," +
						"MAN_ATTR_TYPE_PRIORITY	," +
						"ROW_COUNT				," +
						"ITEM_NO				" +
					"FROM " +
						"VW_USER_MAN_ATTR_VALUE01 " +
					"WHERE " +
						"SITE_CD		= :SITE_CD		AND	" +
						"USER_SEQ		= :USER_SEQ		AND	" +
						"USER_CHAR_NO	= :USER_CHAR_NO AND " +
						"NA_FLAG		= 0			" +
					"ORDER BY " +
						"SITE_CD				," +
						"USER_SEQ				," +
						"USER_CHAR_NO			," +
						"MAN_ATTR_TYPE_PRIORITY ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
	
	public DataSet GetAllList(string pSiteCd) {

		DataSet ds = new DataSet();
		string sSql;
		try{
			conn = DbConnect("UserManAttrTypeValue.GetAllList");

			sSql = "SELECT " +
						"SITE_CD				," +
						"USER_SEQ				," +
						"MAN_ATTR_TYPE_SEQ		," +
						"MAN_ATTR_TYPE_NM		," +
						"MAN_ATTR_SEQ			," +
						"MAN_ATTR_NM			," +
						"DISPLAY_VALUE			," +
						"INPUT_TYPE				," +
						"MAN_ATTR_TYPE_PRIORITY	" +
					"FROM " +
						"VW_USER_MAN_ATTR_VALUE01 " +
					"WHERE " +
						"SITE_CD = :SITE_CD	AND	" +
						"NA_FLAG = :NA_FLAG	";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("NA_FLAG",ViComm.ViCommConst.FLAG_OFF);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

}