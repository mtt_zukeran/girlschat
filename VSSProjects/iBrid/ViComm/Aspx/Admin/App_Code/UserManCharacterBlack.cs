﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: UserManCharacterBlack
--	Title			: 会員ブラックリスト
--	Progaram ID		: UserManCharacterBlack
--
--  Creation Date	: 2011.05.18
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater   Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Text;
using iBridCommLib;
using ViComm;

public class UserManCharacterBlack:DbSession {
	public UserManCharacterBlack() {}
	public DataSet GetPageCollection(string pSiteCd,string pBlackType,string pLoginId,int startRowIndex,int maximumRows) {

		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();
			StringBuilder oSql = new StringBuilder();

			string sOrder = "ORDER BY SITE_CD,REGIST_DATE,USER_SEQ ";
			oSql.AppendLine("SELECT ");
			oSql.AppendLine("	SITE_CD		,	");
			oSql.AppendLine("	USER_SEQ	,	");
			oSql.AppendLine("	USER_CHAR_NO,	");
			oSql.AppendLine("	HANDLE_NM	,	");
			oSql.AppendLine("	LOGIN_ID	,	");
			oSql.AppendLine("	BLACK_TYPE	,	");
			oSql.AppendLine("	REGIST_DATE	,	");
			oSql.AppendLine("	REMARKS		,	");
			oSql.AppendLine("	DEL_FLAG	,	");
			oSql.AppendLine("	UPDATE_DATE		");
			oSql.AppendLine("FROM(");
			oSql.AppendLine(" SELECT VW_USER_MAN_CHARACTER_BLACK01.*, ROW_NUMBER() OVER ( " + sOrder + ") AS RNUM FROM VW_USER_MAN_CHARACTER_BLACK01 ");

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pBlackType,pLoginId,ref sWhere);
			oSql.AppendLine(sWhere);

			oSql.AppendLine(")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ");
			oSql.AppendLine(sOrder);

			using (cmd = CreateSelectCommand(oSql.ToString(),conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USER_MAN_CHARACTER_BLACK01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pBlackType,string pLoginId,ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		SysPrograms.SqlAppendWhere(" SITE_CD = :SITE_CD ",ref pWhere);
		list.Add(new OracleParameter("SITE_CD",pSiteCd));

		if (!pBlackType.Equals(string.Empty)) {
			SysPrograms.SqlAppendWhere(" BLACK_TYPE = :BLACK_TYPE ",ref pWhere);
			list.Add(new OracleParameter("BLACK_TYPE",pBlackType));
		}

		if (!pLoginId.Equals(string.Empty)) {
			SysPrograms.SqlAppendWhere(" LOGIN_ID = :LOGIN_ID ",ref pWhere);
			list.Add(new OracleParameter("LOGIN_ID",pLoginId));
		}

		SysPrograms.SqlAppendWhere(" DEL_FLAG = :DEL_FLAG ",ref pWhere);
		list.Add(new OracleParameter("DEL_FLAG",ViCommConst.FLAG_OFF));

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public int GetPageCount(string pSiteCd,string pBlackType,string pLoginId) {

		int iPageCount = 0;
		try {
			conn = DbConnect();
			DataSet ds;
			StringBuilder sSql = new StringBuilder();

			sSql.AppendLine("SELECT ");
			sSql.AppendLine("   COUNT(*) AS ROW_COUNT ");
			sSql.AppendLine("FROM ");
			sSql.AppendLine("   VW_USER_MAN_CHARACTER_BLACK01 ");
			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pBlackType,pLoginId,ref sWhere);
			sSql.AppendLine(sWhere);

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					using (ds = new DataSet()) {
						for (int i = 0;i < objParms.Length;i++) {
							cmd.Parameters.Add((OracleParameter)objParms[i]);
						}
						da.Fill(ds);
						iPageCount = Convert.ToInt32(ds.Tables[0].Rows[0]["ROW_COUNT"]);
					}
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}
}
