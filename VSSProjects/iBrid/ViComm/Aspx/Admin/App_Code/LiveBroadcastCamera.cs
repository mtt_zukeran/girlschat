﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ライブ配信カメラ
--	Progaram ID		: LiveBroadcastCamera
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class LiveBroadcastCamera:DbSession {

	public LiveBroadcastCamera() {
	}

	public DataSet GetLiveList(string pCameraId) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT " +
								"SITE_CD				," +
								"IVP_LOC_CD				," +
								"IVP_SITE_CD			," +
								"LIVE_SEQ				," +
								"SHARED_MEDIA_NM		," +
								"USE_TERMINAL_TYPE		," +
								"TERMINAL_ID			," +
								"MAIN_CAMERA_FLAG		," +
								"CAMERA_OPERATION_DTMF	," +
								"CAMERA_TITLE			," +
								"SUBSTRB(SITE_NM,1,20)			SITE_NM," +
								"SUBSTRB(LIVE_NM,1,30)			LIVE_NM," +
								"SUBSTRB(LIVE_CAMERA_NM,1,30)	LIVE_CAMERA_NM " +
							"FROM VW_LIVE_BROADCAST_CAMERA01 " +
								" WHERE " +
									"CAMERA_ID	= :CAMERA_ID		" +
								" ORDER BY SITE_CD,LIVE_SEQ DESC,CAMERA_ID  ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("CAMERA_ID",pCameraId);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_LIVE_BROADCAST_CAMERA01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public void ExecuteCameraCall(string pCameraId,int pRecFlag) {
		DataSet ds = GetLiveList(pCameraId);
		string sUrl = "",sRequestUrl,sLiveKey;
		int iCall = 1;

		using (Sys oSys = new Sys()) {
			oSys.GetValue("IVP_STARTUP_CAMERA_URL",out sRequestUrl);
			oSys.GetValue("LIVE_KEY",out sLiveKey);
		}

		foreach (DataRow dr in ds.Tables[0].Rows) {

			sUrl = string.Format("{0}?loc={1}&site={2}&camera={3}&desttype={4}&destno={5}&main={6}&dtmf={7}&title={8}&rec={9}&call={10}&key={11}",
						sRequestUrl,
						dr["IVP_LOC_CD"].ToString(),
						dr["IVP_SITE_CD"].ToString(),
						dr["SHARED_MEDIA_NM"].ToString(),
						dr["USE_TERMINAL_TYPE"].ToString(),
						dr["TERMINAL_ID"].ToString(),
						dr["MAIN_CAMERA_FLAG"].ToString(),
						dr["CAMERA_OPERATION_DTMF"].ToString(),
						dr["CAMERA_TITLE"].ToString(),
						pRecFlag,
						iCall,
						sLiveKey);
			iCall = 0;
			ViCommInterface.TransIVPControl(sUrl);
		}
	}

}
