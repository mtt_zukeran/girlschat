﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 広告コード
--	Progaram ID		: SiteUser
--
--  Creation Date	: 2011.03.23
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;


public class SiteUser:DbSession {
	public SiteUser() {
	}
	
	public DataSet GetAdCdByUserSeq(string pUserSeq){
		
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();
			StringBuilder oSql = new StringBuilder();
			oSql.Append("SELECT											 ").AppendLine();
			oSql.Append("	T.AD_CD						AS AD_CD		,").AppendLine();
			oSql.Append("	T.AD_NM						AS AD_NM		 ").AppendLine();
			oSql.Append("FROM (											 ").AppendLine();
			oSql.Append("	SELECT										 ").AppendLine();
			oSql.Append("		T_SITE_USER.AD_CD						,").AppendLine();
			oSql.Append("		T_AD.AD_NM								,").AppendLine();
			oSql.Append("		T_SITE_USER.SITE_CD						,").AppendLine();
			oSql.Append("		T_SITE_USER.USER_SEQ					,").AppendLine();
			oSql.Append("		T_SITE_USER.USER_CHAR_NO				 ").AppendLine();
			oSql.Append("	FROM										 ").AppendLine();
			oSql.Append("		T_SITE_USER								,").AppendLine();
			oSql.Append("		T_AD									 ").AppendLine();
			oSql.Append("	WHERE										 ").AppendLine();
			oSql.Append("		T_SITE_USER.USER_SEQ	= :USER_SEQ	AND	 ").AppendLine();
			oSql.Append("		T_SITE_USER.AD_CD		IS NOT NULL	AND	 ").AppendLine();
			oSql.Append("		T_SITE_USER.AD_CD		= T_AD.AD_CD	 ").AppendLine();
			oSql.Append(") T											 ").AppendLine();
			oSql.Append("ORDER BY										 ").AppendLine();
			oSql.Append("	T.SITE_CD									,").AppendLine();
			oSql.Append("	T.USER_SEQ									,").AppendLine();
			oSql.Append("	T.USER_CHAR_NO								 ").AppendLine();

			using (cmd = CreateSelectCommand(oSql.ToString(),conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					cmd.BindByName = true;
					cmd.Parameters.Add(":USER_SEQ",pUserSeq);
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
}
