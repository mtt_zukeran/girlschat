﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: サイト構成HTML文章
--	Progaram ID		: SiteHtmlDoc
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class SiteHtmlDoc:DbSession {

	public string htmlDoc1;
	public string htmlDoc2;
	public string htmlDoc3;
	public string htmlDoc4;

	public SiteHtmlDoc() {
	}

	public int GetPageCount(string pSiteCd,string pUserAgentType,string pHtmlDocType,string pSortExpression,string pSortDirection) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;

		string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_SITE_HTML_DOC02 ";

		string sWhere = "";
		OracleParameter[] objParms = CreateWhere(pSiteCd,pUserAgentType,pHtmlDocType,ref sWhere);
		sSql = sSql + sWhere;
		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pUserAgentType,string pHtmlDocType,int startRowIndex,int maximumRows,string pSortExpression,string pSortDirection) {
		DataSet ds;
		ds = new DataSet();
		string sViewNm = "VW_SITE_HTML_DOC02";
		string sOrder = GetOrder(pSortExpression,pSortDirection);
		string sSql = "SELECT " +
							"SITE_CD			," +
							"USER_AGENT_TYPE	," +
							"HTML_DOC_SEQ		," +
							"HTML_DOC_TYPE		," +
							"HTML_DOC_TYPE_NM	," +
							"START_PUB_DAY		," +
							"CSS_FILE_NM1		," +
							"CSS_FILE_NM2		," +
							"CSS_FILE_NM3		," +
							"CSS_FILE_NM4		," +
							"CSS_FILE_NM5		," +
							"SUBSTR(HTML_DOC1,1,37) DOC," +
							"HTML_DOC1			," +
							"HTML_DOC2			," +
							"HTML_DOC3			," +
							"HTML_DOC4			," +
							"HTML_DOC5			," +
							"HTML_DOC6			," +
							"HTML_DOC7			," +
							"HTML_DOC8			," +
							"HTML_DOC9			," +
							"HTML_DOC10			," +
							"HTML_DOC11			," +
							"HTML_DOC12			," +
							"HTML_DOC13			," +
							"HTML_DOC14			," +
							"HTML_DOC15			," +
							"HTML_DOC16			," +
							"HTML_DOC17			," +
							"HTML_DOC18			," +
							"HTML_DOC19			," +
							"HTML_DOC20			," +
							"END_PUB_DAY		," +
							"UPDATE_DATE		," +
							"NA_FLAG			," +
							"USER_AGENT_TYPE_NM	," +
							"TABLE_INDEX_NM		," +
							"SEX_NM	" +
						"FROM(" +
						string.Format(" SELECT {0}.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM {0}  ",sViewNm);

		string sWhere = "";
		OracleParameter[] objParms = CreateWhere(pSiteCd,pUserAgentType,pHtmlDocType,ref sWhere);
		sSql = sSql + sWhere;

		sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
		sSql = sSql + sOrder;
		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,sViewNm);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private string GetOrder(string pSortExpression,string pSortDirection) {
		string sValue = string.Empty;
		switch (pSortExpression) {
			case "HTML_DOC_TYPE" :
				sValue = string.Format("ORDER BY SITE_CD,TO_NUMBER(HTML_DOC_TYPE) {0},USER_AGENT_TYPE",pSortDirection);
				break;
			case "UPDATE_DATE" :
				sValue = string.Format("ORDER BY SITE_CD,UPDATE_DATE {0},TO_NUMBER(HTML_DOC_TYPE),USER_AGENT_TYPE",pSortDirection);
				break;
			default :
				sValue = "ORDER BY SITE_CD,UPDATE_DATE DESC,TO_NUMBER(HTML_DOC_TYPE),USER_AGENT_TYPE";
				break;
		}

		return sValue;
	}

	public DataSet GetCsvData(string pSiteCd,string pUserAgentType,string pHtmlDocType) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = " ORDER BY SITE_CD,HTML_DOC_GROUP,TO_NUMBER(HTML_DOC_TYPE),USER_AGENT_TYPE,HTML_DOC_SEQ,HTML_DOC_SUB_SEQ ";
			string sSql = "SELECT " +
							"HTML_DOC_SEQ			," +
							"SITE_CD				," +
							"USER_AGENT_TYPE		," +
							"HTML_DOC_GROUP			," +
							"HTML_DOC_TYPE			," +
							"PAGE_TITLE				," +
							"PAGE_KEYWORD			," +
							"PAGE_DESCRIPTION		," +
							"TABLE_INDEX			," +
							"WEB_FACE_SEQ			," +
							"SEX_CD					," +
							"CSS_FILE_NM1			," +
							"CSS_FILE_NM2			," +
							"CSS_FILE_NM3			," +
							"CSS_FILE_NM4			," +
							"CSS_FILE_NM5			," +
							"JAVASCRIPT_FILE_NM1	," +
							"JAVASCRIPT_FILE_NM2	," +
							"JAVASCRIPT_FILE_NM3	," +
							"JAVASCRIPT_FILE_NM4	," +
							"JAVASCRIPT_FILE_NM5	," +
							"NA_FLAG				," +
							"HTML_DOC_SUB_SEQ		," +
							"HTML_DOC_TITLE			," +
							"HTML_DOC1				," +
							"HTML_DOC2				," +
							"HTML_DOC3				," +
							"HTML_DOC4				," +
							"HTML_DOC5				," +
							"HTML_DOC6				," +
							"HTML_DOC7				," +
							"HTML_DOC8				," +
							"HTML_DOC9				," +	
							"HTML_DOC10				," +
							"HTML_DOC11				," +
							"HTML_DOC12				," +
							"HTML_DOC13				," +
							"HTML_DOC14				," +
							"HTML_DOC15				," +
							"HTML_DOC16				," +
							"HTML_DOC17				," +
							"HTML_DOC18				," +
							"HTML_DOC19				," +
							"HTML_DOC20				" +
						"FROM " +
							 "VW_SITE_HTML_DOC03  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserAgentType,pHtmlDocType,ref sWhere);
			sSql = sSql + sWhere;
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_SITE_HTML_DOC03");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pUserAgentType,string pHtmlDocType,ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		pWhere = pWhere + " WHERE SITE_CD = :SITE_CD AND HTML_DOC_GROUP = :HTML_DOC_GROUP";
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		list.Add(new OracleParameter("HTML_DOC_GROUP",ViComm.ViCommConst.DOC_GRP_NORMAL));

		if (pUserAgentType.Equals("") == false) {
			pWhere = pWhere + " AND USER_AGENT_TYPE = :USER_AGENT_TYPE ";
			list.Add(new OracleParameter("USER_AGENT_TYPE",pUserAgentType));
		}

		if (pHtmlDocType.Equals("") == false) {
			pWhere = pWhere + " AND HTML_DOC_TYPE LIKE '%'||:HTML_DOC_TYPE||'%' ";
			list.Add(new OracleParameter("HTML_DOC_TYPE",pHtmlDocType));
		}
	
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public DataSet GetList(string pSiteCd,string pHtmlDocGroup) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT * FROM T_SITE_HTML_DOC WHERE SITE_CD =:SITE_CD AND HTML_DOC_GROUP =:HTML_DOC_GROUP";
			sSql = sSql + " ORDER BY HTML_DOC_SEQ ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("HTML_DOC_GROUP",pHtmlDocGroup);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}

		return ds;
	}

	public DataSet GetListByDocSeq(string pHtmlDocSeq) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT * FROM T_SITE_HTML_DOC WHERE HTML_DOC_SEQ = :HTML_DOC_SEQ";
			sSql = sSql + " ORDER BY HTML_DOC_SEQ, HTML_DOC_SUB_SEQ ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("HTML_DOC_SEQ",pHtmlDocSeq);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public bool GetOne(string pSiteCd,string pUserAgentType,string pHtmlDocType,string pStartPubDay) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;
		try {
			conn = DbConnect();

			string sSql = "SELECT * FROM VW_SITE_HTML_DOC03 WHERE SITE_CD = :SITE_CD AND HTML_DOC_TYPE = :HTML_DOC_TYPE AND START_PUB_DAY = :START_PUB_DAY";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("HTML_DOC_TYPE",pHtmlDocType);
				cmd.Parameters.Add("START_PUB_DAY",pStartPubDay);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_SITE_HTML_DOC03");

					if (ds.Tables["VW_SITE_HTML_DOC03"].Rows.Count != 0) {
						dr = ds.Tables["VW_SITE_HTML_DOC03"].Rows[0];
						htmlDoc1 = dr["HTML_DOC1"].ToString();
						htmlDoc2 = dr["HTML_DOC2"].ToString();
						htmlDoc3 = dr["HTML_DOC3"].ToString();
						htmlDoc4 = dr["HTML_DOC4"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}

		return bExist;
	}

	public string GetDocSeq(string pSiteCd,string pUserAgentType,string pHtmlDocType) {
		DataSet ds;
		DataRow dr;

		string sSeq = "";
		try {
			conn = DbConnect();
			string sSql = "SELECT HTML_DOC_SEQ FROM T_SITE_HTML_MANAGE WHERE SITE_CD = :SITE_CD AND HTML_DOC_GROUP =:HTML_DOC_GROUP AND HTML_DOC_TYPE = :HTML_DOC_TYPE AND USER_AGENT_TYPE = :USER_AGENT_TYPE";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("HTML_DOC_GROUP",ViComm.ViCommConst.DOC_GRP_NORMAL);
				cmd.Parameters.Add("HTML_DOC_TYPE",pHtmlDocType);
				cmd.Parameters.Add("USER_AGENT_TYPE",pUserAgentType);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_SITE_HTML_MANAGE");
					if (ds.Tables["T_SITE_HTML_MANAGE"].Rows.Count != 0) {
						dr = ds.Tables["T_SITE_HTML_MANAGE"].Rows[0];
						sSeq = dr["HTML_DOC_SEQ"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
		return sSeq;
	}

	public DataSet GetDataByDocType(string pSiteCd,string pHtmlDocType,string pUserAgentType) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	HTML_DOC_SEQ								,	");
		oSqlBuilder.AppendLine("	USER_AGENT_TYPE									");
		oSqlBuilder.AppendLine("FROM												");
		oSqlBuilder.AppendLine("	VW_SITE_HTML_DOC03								");
		oSqlBuilder.AppendLine("WHERE												");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	HTML_DOC_TYPE		= :HTML_DOC_TYPE			");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":HTML_DOC_TYPE",pHtmlDocType));
		
		if (!string.IsNullOrEmpty(pUserAgentType)) {
			oParamList.Add(new OracleParameter(":USER_AGENT_TYPE",pUserAgentType));
			oSqlBuilder.AppendLine(" AND	USER_AGENT_TYPE		= :USER_AGENT_TYPE			");
		}
		
		oSqlBuilder.AppendLine("ORDER BY USER_AGENT_TYPE ASC");
		
		DataSet ds = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		return ds;
	}

}
