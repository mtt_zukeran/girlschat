﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 担当
--	Progaram ID		: Manager
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class Manager:DbSession {

	public string productionSeq;
	public string managerSeq;
	public string managerNm;

	public Manager() {
	}

	public int GetPageCount() {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_MANAGER01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY PRODUCTION_SEQ,MANAGER_SEQ";
			string sSql = "SELECT " +
							"PRODUCTION_SEQ	," +
							"MANAGER_SEQ	," +
							"MANAGER_NM		," +
							"TEL			," +
							"EMAIL_ADDR		," +
							"REGIST_DATE	," +
							"UPDATE_DATE	," +
							"REVISION_NO	," +
							"PRODUCTION_NM	," +
							"MANAGE_COUNT " +
							"FROM(" +
							" SELECT VW_MANAGER01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_MANAGER01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_MANAGER01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(ref string pWhere) {
		ArrayList list = new ArrayList();
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public DataSet GetList() {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT MANAGER_SEQ,MANAGER_NM FROM T_MANAGER ORDER BY PRODUCTION_SEQ,MANAGER_SEQ ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MANAGER");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetProductionList(string pProductionSeq) {

		if (string.IsNullOrEmpty(pProductionSeq)) {
			return GetList();
		}

		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT MANAGER_SEQ,MANAGER_NM FROM T_MANAGER WHERE PRODUCTION_SEQ = :PRODUCTION_SEQ ORDER BY PRODUCTION_SEQ,MANAGER_SEQ ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("PRODUCTION_SEQ",pProductionSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MANAGER");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	public bool GetOne(string pLoginId,string pPassword) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try{
			conn = DbConnect();
			string sSql = "SELECT " +
								"PRODUCTION_SEQ ," +
								"MANAGER_SEQ	," +
								"MANAGER_NM		 " +
							"FROM " +
								"T_MANAGER " +
							"WHERE " +
								"LOGIN_ID = :LOGIN_ID AND LOGIN_PASSWORD = :LOGIN_PASSWORD ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("LOGIN_ID",pLoginId);
				cmd.Parameters.Add("LOGIN_PASSWORD",pPassword);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MANAGER");
					if (ds.Tables["T_MANAGER"].Rows.Count != 0) {
						dr = ds.Tables["T_MANAGER"].Rows[0];
						productionSeq = dr["PRODUCTION_SEQ"].ToString();
						managerSeq = dr["MANAGER_SEQ"].ToString();
						managerNm = dr["MANAGER_NM"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public bool IsExistLoginId(string pLoginId) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try{
			conn = DbConnect();
			string sSql = "SELECT " +
								"MANAGER_NM		 " +
							"FROM " +
								"T_MANAGER " +
							"WHERE " +
								"LOGIN_ID = :LOGIN_ID ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("LOGIN_ID",pLoginId);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MANAGER");
					if (ds.Tables["T_MANAGER"].Rows.Count != 0) {
						dr = ds.Tables["T_MANAGER"].Rows[0];
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}
}
