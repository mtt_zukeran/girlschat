﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: バイトプラン
--	Progaram ID		: PartTimeJobPlan
--
--  Creation Date	: 2011.07.25
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;


public class PartTimeJobPlan : DbSession {

	public PartTimeJobPlan() {
	}

	public int GetPageCount(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_PART_TIME_JOB_PLAN	P,");
		oSqlBuilder.AppendLine("	T_STAGE					S");

		string sWhereClause;
		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,out sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		}
		finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	P.SITE_CD				,");
		oSqlBuilder.AppendLine("	P.PART_TIME_JOB_SEQ		,");
		oSqlBuilder.AppendLine("	P.PART_TIME_JOB_NM		,");
		oSqlBuilder.AppendLine("	P.INCOME				,");
		oSqlBuilder.AppendLine("	P.WAITING_MIN			,");
		oSqlBuilder.AppendLine("	P.STAGE_SEQ				,");
		oSqlBuilder.AppendLine("	S.STAGE_NM				,");
		oSqlBuilder.AppendLine("	S.STAGE_LEVEL			,");
		oSqlBuilder.AppendLine("	P.REVISION_NO			,");
		oSqlBuilder.AppendLine("	P.UPDATE_DATE			");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_PART_TIME_JOB_PLAN	P,");
		oSqlBuilder.AppendLine("	T_STAGE					S");

		string sWhereClause;
		string sPagingSql;
		string sSortExpression = "ORDER BY P.SITE_CD, S.STAGE_LEVEL, P.WAITING_MIN, P.PART_TIME_JOB_SEQ";

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,out sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		}
		finally {
			conn.Close();
		}
		return oDs;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,out string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();

		SysPrograms.SqlAppendWhere("P.SITE_CD = S.SITE_CD AND P.STAGE_SEQ = S.STAGE_SEQ",ref pWhere);

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("S.SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}