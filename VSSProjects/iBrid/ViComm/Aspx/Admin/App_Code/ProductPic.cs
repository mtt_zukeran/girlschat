﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 商品画像
--	Progaram ID		: ProductPic
--
--  Creation Date	: 2010.12.08
--  Creater			: iBrid
--
**************************************************************************/


using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;
using ViComm;

/// <summary>
/// ProductPic の概要の説明です
/// </summary>
public class ProductPic:DbSession {

	public ProductPic() {
		//
		// TODO: コンストラクタ ロジックをここに追加します
		//
	}

	public DataSet GetList(string pSiteCd,string pProductAgentCd,string pProductSeq, string pProductPicType) {
		DataSet ds;
		DataColumn col;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql =	"SELECT " +
								"VW_PRODUCT_PIC00.OBJ_SEQ											," +
								"VW_PRODUCT_PIC00.DISPLAY_POSITION									," +
								"GET_PRODUCT_IMG_PATH(SITE_CD,PRODUCT_ID,OBJ_SEQ) AS PHOTO_IMG_PATH	," +
								"VW_PRODUCT_PIC00.REVISION_NO	" +
							"FROM " +
								"VW_PRODUCT_PIC00 " +
							"WHERE " +
								"SITE_CD			= :SITE_CD			AND " +
								"PRODUCT_AGENT_CD	= :PRODUCT_AGENT_CD	AND " +
								"PRODUCT_SEQ		= :PRODUCT_SEQ		AND " +
								"PRODUCT_PIC_TYPE	= :PRODUCT_PIC_TYPE ";
			sSql += "ORDER BY SITE_CD,PRODUCT_AGENT_CD,PRODUCT_PIC_TYPE,DISPLAY_POSITION,OBJ_SEQ DESC ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("PRODUCT_AGENT_CD", pProductAgentCd);
				cmd.Parameters.Add("PRODUCT_SEQ", pProductSeq);
				cmd.Parameters.Add("PRODUCT_PIC_TYPE", pProductPicType);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_PRODUCT_PIC00");
				}
			}
			
		} finally {
			conn.Close();
		}

		if (ds.Tables[0].Rows.Count > 0) {
			col = new DataColumn("UP_BTN_FLAG",System.Type.GetType("System.Boolean"));
			ds.Tables[0].Columns.Add(col);

			col = new DataColumn("DOWN_BTN_FLAG",System.Type.GetType("System.Boolean"));
			ds.Tables[0].Columns.Add(col);

			foreach (DataRow dr in ds.Tables[0].Rows) {
				dr["UP_BTN_FLAG"] = true;
				dr["DOWN_BTN_FLAG"] = true;
			}
			DataRow drFisrt = ds.Tables[0].Rows[0];
			drFisrt["UP_BTN_FLAG"] = false;

			DataRow drLast = ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1];
			drLast["DOWN_BTN_FLAG"] = false;
		}
		return ds;
	}
	
	public decimal GetPicNo() {

		decimal dPicNo = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT SEQ_OBJ.NEXTVAL FROM DUAL";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				dPicNo = (decimal)cmd.ExecuteScalar();
			}
		} finally {
			conn.Close();
		}

		return dPicNo;
	}

	public DataSet GetListAllParam(string pSiteCd,string pProductAgentCd,string pProductSeq,string pProductPicType) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT " +
								"SITE_CD			," +
								"PRODUCT_AGENT_CD	," +
								"PRODUCT_SEQ		," +
								"OBJ_SEQ			," +
								"PRODUCT_PIC_TYPE	," +
								"DISPLAY_POSITION	," +
								"UPLOAD_DATE		," +
								"UNEDITABLE_FLAG	," +
								"REVISION_NO		," +
								"UPDATE_DATE " +
							"FROM " +
								"VW_PRODUCT_PIC01 " +
							"WHERE " +
								"SITE_CD			= :SITE_CD		AND " +
								"PRODUCT_AGENT_CD	= :PRODUCT_AGENT_CD	AND " +
								"PRODUCT_SEQ		= :PRODUCT_SEQ	AND " +
								"PRODUCT_PIC_TYPE	= :PRODUCT_PIC_TYPE ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("PRODUCT_AGENT_CD",pProductAgentCd);
				cmd.Parameters.Add("PRODUCT_SEQ",pProductSeq);
				cmd.Parameters.Add("PRODUCT_PIC_TYPE",pProductPicType);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_PRODUCT_PIC01");
				}
			}

		} finally {
			conn.Close();

		}

		return ds;
	}
	public DataSet GetOne(string pSiteCd,string pProductAgentCd,string pProductSeq,string pObjSeq,string pProductPicType)
	{
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT " +
								"SITE_CD			," +
								"PRODUCT_AGENT_CD	," +
								"PRODUCT_SEQ		," +
								"OBJ_SEQ			," +
								"PRODUCT_PIC_TYPE	," +
								"DISPLAY_POSITION	," +
								"UPLOAD_DATE		," +
								"UNEDITABLE_FLAG	," +
								"REVISION_NO		," +
								"UPDATE_DATE " +
							"FROM " +
								"VW_PRODUCT_PIC01 " +
							"WHERE " +
								"SITE_CD			= :SITE_CD		AND " +
								"PRODUCT_AGENT_CD	= :PRODUCT_AGENT_CD	AND " +
								"PRODUCT_SEQ		= :PRODUCT_SEQ	AND " +
								"POBJ_SEQ		= :POBJ_SEQ	AND " +
								"PRODUCT_PIC_TYPE	= :PRODUCT_PIC_TYPE ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("PRODUCT_AGENT_CD",pProductAgentCd);
				cmd.Parameters.Add("PRODUCT_SEQ",pProductSeq);
				cmd.Parameters.Add("POBJ_SEQ",pObjSeq);
				cmd.Parameters.Add("PRODUCT_PIC_TYPE",pProductPicType);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_PRODUCT_PIC01");
				}
			}

		} finally {
			conn.Close();

		}

		return ds;
		
	}

	public void DeleteProductPic(string pSiteCd,string pProductAgentCd,int pProductSeq,int pObjSeq,int pRevisionNo)
	{
		using (DbSession db = new DbSession())
		{
			db.PrepareProcedure("PRODUCT_PIC_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PPRODUCT_AGENT_CD",DbType.VARCHAR2,pProductAgentCd);
			db.ProcedureInParm("PPRODUCT_SEQ",DbType.NUMBER,pProductSeq);
			db.ProcedureInParm("POBJ_SEQ",DbType.NUMBER,pObjSeq);

			db.ProcedureInParm("PPRODUCT_PIC_TYPE",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PDISPLAY_POSITION",DbSession.DbType.NUMBER,0);
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,pRevisionNo);
			db.ProcedureInParm("PDELETE_FLAG",DbSession.DbType.NUMBER,1);
			db.ProcedureInParm("PWITHOUT_COMMIT",DbSession.DbType.NUMBER,0);
			
			db.ProcedureOutParm("PSTATUS",DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

    public int GetCount(string pSiteCd, string pProductAgentCd, string pProductSeq, string pProductPicType) {
        StringBuilder oSqlBuilder = new StringBuilder();
        oSqlBuilder.AppendLine(" SELECT						");
        oSqlBuilder.AppendLine("	COUNT(1)                ");
        oSqlBuilder.AppendLine(" FROM						");
        oSqlBuilder.AppendLine("	T_PRODUCT_PIC			");
        oSqlBuilder.AppendLine(" WHERE                      ");
        oSqlBuilder.AppendLine("	T_PRODUCT_PIC.SITE_CD			= :SITE_CD			AND ");
        oSqlBuilder.AppendLine("	T_PRODUCT_PIC.PRODUCT_AGENT_CD	= :PRODUCT_AGENT_CD AND ");
        oSqlBuilder.AppendLine("	T_PRODUCT_PIC.PRODUCT_SEQ		= :PRODUCT_SEQ		AND ");
        oSqlBuilder.AppendLine("	T_PRODUCT_PIC.PRODUCT_PIC_TYPE	= :PRODUCT_PIC_TYPE   ");

        try {
            conn = DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
                cmd.BindByName = true;
                cmd.Parameters.Add(":SITE_CD", pSiteCd);
                cmd.Parameters.Add(":PRODUCT_AGENT_CD", pProductAgentCd);
                cmd.Parameters.Add(":PRODUCT_SEQ", pProductSeq);
                cmd.Parameters.Add(":PRODUCT_PIC_TYPE", pProductPicType);

                int iCount;
                if (int.TryParse(cmd.ExecuteScalar().ToString(), out iCount)) {
                    return iCount;
                }
                return -1;
            }
        } finally {
            conn.Close();
        }
    }
}
