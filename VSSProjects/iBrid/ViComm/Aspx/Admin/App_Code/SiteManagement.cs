﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: サイト運営
--	Progaram ID		: SiteManagement
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class SiteManagement:DbSession {

	public int rankingCreateCount;
	public SiteManagement() {
	}

	public int GetPageCount() {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_SITE01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY SITE_CD";
			string sSql = "SELECT " +
							"SITE_CD				," +
							"SITE_NM				," +
							"REGIST_SERVICE_POINT	," +
							"INVITE_MAIL_AVA_HOUR	," +
							"RE_INVITE_MAIL_NA_HOUR	," +
							"TX_MAIL_REF_MARKING	," +
							"NEW_FACE_DAYS			," +
							"FIND_MAN_ELAPASED_DAYS	," +
							"CM_MAIL_RX_LIMIT		," +
							"SUPPORT_EMAIL_ADDR		," +
							"INFO_EMAIL_ADDR		," +
							"SUPPORT_TEL			," +
							"WEB_FACE_SEQ			," +
							"REVISION_NO			," +
							"HTML_FACE_NAME			," +
							"MANAGEMENT_UPDATE_DATE " +
							"FROM(" +
							" SELECT VW_SITE01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_SITE01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_SITE01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(ref string pWhere) {
		ArrayList list = new ArrayList();
		string sSiteCd = iBridUtil.GetStringValue(HttpContext.Current.Session["SiteCd"]);
		SysPrograms.SqlAppendWhere("NA_FLAG = :NA_FLAG",ref pWhere);
		list.Add(new OracleParameter("NA_FLAG","0"));
		if (!sSiteCd.Equals("")) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",sSiteCd));
		}
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public DataSet GetList() {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSiteCd = iBridUtil.GetStringValue(HttpContext.Current.Session["SiteCd"]);
			string sSql = "SELECT SITE_CD,SITE_NM,SITE_MARK,CAST_MINIMUM_PAYMENT FROM VW_SITE01 WHERE NA_FLAG = 0 ";

			if (!sSiteCd.Equals("")) {
				sSql += " AND SITE_CD =: SITE_CD ";
			}
			sSql += " ORDER BY PRIORITY,SITE_CD ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				if (!sSiteCd.Equals("")) {
					cmd.Parameters.Add("SITE_CD",sSiteCd);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_SITE01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public bool GetOne(string pSiteCd) {
		DataSet ds;
		bool bExist = false;
		try{
			conn = DbConnect();

			using (cmd = CreateSelectCommand("SELECT * FROM T_SITE_MANAGEMENT WHERE SITE_CD = :SITE_CD",conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				bExist = SetData(ds);
			}
		} finally {
			conn.Close();
		}

		return bExist;
	}

	private bool SetData(DataSet ds) {
		DataRow dr;
		bool bRet = false;
		using (da = new OracleDataAdapter(cmd)) {
			da.Fill(ds,"T_SITE_MANAGEMENT");
			if (ds.Tables["T_SITE_MANAGEMENT"].Rows.Count != 0) {
				dr = ds.Tables["T_SITE_MANAGEMENT"].Rows[0];
				rankingCreateCount = int.Parse(dr["RANKING_CREATE_COUNT"].ToString());
				bRet = true;
			}
		}
		return bRet;
	}
}
