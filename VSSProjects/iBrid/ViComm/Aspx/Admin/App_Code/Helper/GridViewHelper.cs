﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public class GridViewHelper {

	public static void RegiserCreateGridHeader(int pHeaderRowsCount, GridView pGridView, Panel pPanelGrid) {
		if (pGridView.Rows.Count > 0) {
			Page oPage = pGridView.Page;
			if (!oPage.ClientScript.IsStartupScriptRegistered(oPage.GetType(), "CreateGridHeader")) {
				oPage.ClientScript.RegisterStartupScript(oPage.GetType(), "CreateGridHeader", string.Format("CreateGridHeader({0}, '{1}', '{2}');", pHeaderRowsCount, pGridView.ClientID, pPanelGrid.ClientID), true);
			}
		}
	}
}
