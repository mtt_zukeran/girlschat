﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ViComm;

/// <summary>
/// PickupHelper の概要の説明です
/// </summary>
public class PickupHelper {

	public static string GeneratePickupObjsViewerUrl(Page pPage,string pSiteCd,string pObjSeq,string pPickupType) {
		return GeneratePickupObjsViewerUrl(pPage,pSiteCd,pObjSeq,pPickupType,ViCommConst.FLAG_ON);
	}
	public static string GeneratePickupObjsViewerUrl(Page pPage,string pSiteCd,string pObjSeq,string pPickupType,int pWithPickup) {
		return string.Format("javascript:openPickupObjViewer('{0}','{1}','{2}','{3}');",pSiteCd,pObjSeq,pPickupType,pWithPickup);
	}
}

public delegate void ObjClickEventHandler(object sender,ObjClickEventArgs e);

public class ObjClickEventArgs : EventArgs {
	private string objSeq;
	private string objType;
	private string addableFlag;

	public string ObjSeq {
		get { return objSeq; }
	}

	public string ObjType {
		get { return objType; }
	}

	public string AddableFlag {
		get { return addableFlag; }
	}

	private ObjClickEventArgs() {
		this.objSeq = string.Empty;
		this.objType = string.Empty;
		this.addableFlag = string.Empty;
	}

	public ObjClickEventArgs(string pObjSeq,string pObjType,string pAddableFlag) {
		this.objSeq = pObjSeq;
		this.objType = pObjType;
		this.addableFlag = pAddableFlag;
	}
}

public interface IPickupObj {
	event ObjClickEventHandler ObjClick;
}
