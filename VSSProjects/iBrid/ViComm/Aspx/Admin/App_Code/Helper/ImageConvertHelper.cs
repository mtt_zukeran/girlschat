﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 画像変換機能を提供するヘルパークラス

--	Progaram ID		: ImageConvertHelper
--
--  Creation Date	: 2010.08.18
--  Creater			: K.Itoh
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------
  Date			Updater			Update Explain

-------------------------------------------------------------------------*/
using System;
using System.IO;
using System.Data;
using System.Configuration;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

/// <summary>
/// ImageHelper の概要の説明です
/// </summary>
public class ImageHelper {

	/// <summary>
	/// 画像サイズ取得用クラス
	/// </summary>
	[DataContract]
	public class ImageSizeClass {
		[DataMember]
		public string width;
		[DataMember]
		public string height;
	}

	public static void SetCopyRight(string pSiteCd, string pInDirPath, string pInFileName) {
		string sMailParseDir = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["MailParseDir"]);

		string sStampFileNm = string.Empty;
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("GET_MAILPARSE_USE");
			db.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, pSiteCd);
			db.ProcedureOutParm("pSTAMP_FILE_NM", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pSTAMP_SAMPLE_FILE_NM", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pPIC_SIZE_LARGE", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pPIC_SIZE_SMALL", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pSCALE_TYPE", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pRECORD_COUNT", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			sStampFileNm = db.GetStringValue("pSTAMP_FILE_NM");
		}

		string sStampFilePath = Path.Combine(sMailParseDir, sStampFileNm);
		using (localhost.Service objWebSrv = new localhost.Service()) {
			objWebSrv.SetCopyRight(sMailParseDir, pInDirPath, pInFileName, sStampFilePath);
		}
	}

	public static void ConvertMobileImage(string pSiteCd, string pInputDir, string pOutputDir, string pFileNm) {
		string sMailParseDir = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["MailParseDir"]);
		using (localhost.Service objWebSrv = new localhost.Service()) {
			objWebSrv.ConvertMobileImage(sMailParseDir,pSiteCd, pInputDir, pOutputDir, pFileNm);
		}
	}

	/// <summary>
	/// 画像サイズ取得
	/// </summary>
	/// <param name="pHost"></param>
	/// <param name="pImagePath">画像のフルパス</param>
	/// <returns></returns>
	public static ImageSizeClass GetImageSize(string pHost,string pImagePath) {
		// URI形式では画像サイズが取得できなかったのでPHPで取得する
		ImageSizeClass info = new ImageSizeClass();
		string url = string.Format("http://{0}/getimagesize.php?src={1}",pHost,pImagePath);
		WebRequest oReq = WebRequest.Create(url);
		oReq.Headers.Add("Accept-Language:ja,en-us;q=0.7,en;q=0.3");
		WebResponse oRes = oReq.GetResponse();
		using (oRes) {
			using (Stream oStream = oRes.GetResponseStream()) {
				// Json形式で取得した画像サイズをクラスに設定
				DataContractJsonSerializer oSerializer = new DataContractJsonSerializer(typeof(ImageSizeClass));
				info = (ImageSizeClass)oSerializer.ReadObject(oStream);
			}
		}
		return info;
	}

	/// <summary>
	/// 画像ダウンロード (変換前の画像をダウンロード)
	/// </summary>
	/// <param name="pFilter"></param>
	/// <param name="pSiteCd"></param>
	/// <param name="pLoginId"></param>
	/// <param name="pPicSeq"></param>
	public static void DownloadCastPicOriginalImage(Stream pFilter,string pSiteCd,string pLoginId,string pPicSeq) {
		string sFileNm = ViCommConst.PIC_HEADER + iBridUtil.addZero(pPicSeq,ViCommConst.OBJECT_NM_LENGTH) + ViCommConst.PIC_FOODER_ORIGINAL;
		string sFullPath = HttpContext.Current.Server.MapPath(string.Format(
			"..{0}\\{1}\\Operator\\{2}\\{3}"
			,ViCommConst.PIC_DIRECTRY
			,pSiteCd
			,pLoginId
			,sFileNm
		));

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,string.Empty,ViCommConst.FILE_UPLOAD_PASSWORD)) {
			FileInfo oInfo = new FileInfo(sFullPath);
			HttpResponse resp = HttpContext.Current.Response;
			resp.Clear();
			resp.ClearContent();
			resp.ClearHeaders();
			resp.Filter = pFilter;
			resp.AddHeader("Content-Disposition","attachment;filename=" + sFileNm);
			resp.AppendHeader("Content-Length",oInfo.Length.ToString());
			resp.Charset = string.Empty;
			resp.Cache.SetCacheability(HttpCacheability.NoCache);
			resp.ContentType = "image/jpeg";
			resp.TransmitFile(sFullPath);
			resp.Flush();
			resp.Close();
			resp.End();
		}
	}
}
