﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.IO;
using System.Text;
using System.Xml.Serialization;

using iBridCommLib;
using ViComm;

public class SeekConditionHelper {
	[Serializable]
	public class InputValueEntity {
		private string _id;
		private string _value;
		public string Id {
			get { return this._id; }
			set { this._id = value; }
		}
		public string Value {
			get { return this._value; }
			set { this._value = value; }
		}
		public InputValueEntity() { }
		public InputValueEntity(string pId, string pValue) {
			this._id = pId;
			this._value = pValue;
		}
	}

	#region □■□ 公開メソッド　 □■□ ==============================================================================
	
	public static void SetupSeekConditionDropDownList(string pAdminId,string pSeekConditionType, DropDownList pTagetControl){
		pTagetControl.Items.Clear();
		pTagetControl.Items.Add(new ListItem(" [ 未選択 ] ", "*1"));
		
		using(SeekCondition oSeekCondition = new SeekCondition()){
			using (DataSet oDataSet = oSeekCondition.GetListOwn(pAdminId, pSeekConditionType)) {
				if(oDataSet.Tables[0].Rows.Count>0){
					pTagetControl.Items.Add(new ListItem("-- 自分のﾌｨﾙﾀｰ ----------------", "*2"));
				}
				foreach (DataRow oDataRow in oDataSet.Tables[0].Rows) {
					string sSeq = iBridUtil.GetStringValue(oDataRow["SEEK_CONDITION_SEQ"]);
					string sNm = iBridUtil.GetStringValue(oDataRow["SEEK_CONDITION_NM"]);

					pTagetControl.Items.Add(new ListItem("　" + sNm, sSeq));
				}
			}
			using (DataSet oDataSet = oSeekCondition.GetListOther(pAdminId, pSeekConditionType)) {
				if (oDataSet.Tables[0].Rows.Count > 0) {
					pTagetControl.Items.Add(new ListItem("-- 他の管理者のﾌｨﾙﾀｰ ----------", "*3"));
				}
				foreach (DataRow oDataRow in oDataSet.Tables[0].Rows) {
					string sSeq = iBridUtil.GetStringValue(oDataRow["SEEK_CONDITION_SEQ"]);
					string sNm = iBridUtil.GetStringValue(oDataRow["SEEK_CONDITION_NM"]);
					string sOwner = iBridUtil.GetStringValue(oDataRow["ADMIN_ID"]);

					pTagetControl.Items.Add(new ListItem(string.Format("　[{0}]{1}", sOwner, sNm), sSeq));
				}
			}			
		}
	}

	public static DataSet Load(string pSeekConditionSeq, Control pTargetControl) {
		using (SeekCondition oSeekCondition = new SeekCondition()) {

			DataSet oSeekConditionDataSet = oSeekCondition.GetOne(pSeekConditionSeq);
			if (oSeekConditionDataSet.Tables[0].Rows.Count == 0) {
				return oSeekConditionDataSet;
			}

			DataRow oSeekConditionDataRow = oSeekConditionDataSet.Tables[0].Rows[0];

			string sSeekCondition = iBridUtil.GetStringValue(oSeekConditionDataRow["SEEK_CONDITION1"]) +
									iBridUtil.GetStringValue(oSeekConditionDataRow["SEEK_CONDITION2"]) +
									iBridUtil.GetStringValue(oSeekConditionDataRow["SEEK_CONDITION3"]) +
									iBridUtil.GetStringValue(oSeekConditionDataRow["SEEK_CONDITION4"]) +
									iBridUtil.GetStringValue(oSeekConditionDataRow["SEEK_CONDITION5"]) +
									iBridUtil.GetStringValue(oSeekConditionDataRow["SEEK_CONDITION6"]) +
									iBridUtil.GetStringValue(oSeekConditionDataRow["SEEK_CONDITION7"]) +
									iBridUtil.GetStringValue(oSeekConditionDataRow["SEEK_CONDITION8"]) +
									iBridUtil.GetStringValue(oSeekConditionDataRow["SEEK_CONDITION9"]) +
									iBridUtil.GetStringValue(oSeekConditionDataRow["SEEK_CONDITION10"]);

			XmlSerializer oSerialzier = new XmlSerializer(typeof(ArrayList), new Type[] { typeof(InputValueEntity) });
			using (StringReader oStringReader = new StringReader(sSeekCondition)) {
				ArrayList oInputValueEntityList = (ArrayList)oSerialzier.Deserialize(oStringReader);

				ConvertEntityList2ControlInputValue(oInputValueEntityList, pTargetControl);
			}
			return oSeekConditionDataSet;
		}
	}

	public static void Delete(string pAdminId, string pSeekConditionSeq, string pRevisionNo) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("SEEK_CONDITION_MAINTE");
			oDbSession.ProcedureInParm("pADMIN_ID", DbSession.DbType.VARCHAR2, pAdminId);
			oDbSession.ProcedureBothParm("pSEEK_CONDITION_SEQ", DbSession.DbType.NUMBER, pSeekConditionSeq);
			oDbSession.ProcedureInParm("pSEEK_CONDITION_TYPE", DbSession.DbType.VARCHAR2, null);
			oDbSession.ProcedureInParm("pSEEK_CONDITION_NM", DbSession.DbType.VARCHAR2, null);
			oDbSession.ProcedureInArrayParm("pSEEK_CONDITIONS", DbSession.DbType.VARCHAR2, new string[] { });
			oDbSession.ProcedureInParm("pPUBLISH_FLAG", DbSession.DbType.NUMBER, null);
			oDbSession.ProcedureInParm("pREVISION_NO", DbSession.DbType.NUMBER, pRevisionNo);
			oDbSession.ProcedureInParm("pDEL_FLAG", DbSession.DbType.NUMBER, ViCommConst.FLAG_ON);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
		}
	}
	public static string Save(string pAdminId, string pSeekConditionSeq, string pSeekConditionType, string pSeekConditionNm, int pPublishFlag, string pRevisionNo, Control pTargetCtrl) {
		IList oInputValueEntityList = ConvertControlInputValue2EntityList(pTargetCtrl);

		XmlSerializer oSerialzier = new XmlSerializer(oInputValueEntityList.GetType(), new Type[] { typeof(InputValueEntity) });
		StringBuilder oStringBuilder = new StringBuilder();
		using (StringWriter oStringWriter = new StringWriter(oStringBuilder)) {
			oSerialzier.Serialize(oStringWriter, oInputValueEntityList);
		}

		string[] oSeekConditions = SysPrograms.SplitBytes(Encoding.GetEncoding("UTF-8"), oStringBuilder.ToString(), 3000);
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("SEEK_CONDITION_MAINTE");
			oDbSession.ProcedureInParm("pADMIN_ID", DbSession.DbType.VARCHAR2, pAdminId);
			oDbSession.ProcedureBothParm("pSEEK_CONDITION_SEQ", DbSession.DbType.NUMBER, pSeekConditionSeq);
			oDbSession.ProcedureInParm("pSEEK_CONDITION_TYPE", DbSession.DbType.VARCHAR2, pSeekConditionType);
			oDbSession.ProcedureInParm("pSEEK_CONDITION_NM", DbSession.DbType.VARCHAR2, pSeekConditionNm);
			oDbSession.ProcedureInArrayParm("pSEEK_CONDITIONS", DbSession.DbType.VARCHAR2, oSeekConditions);
			oDbSession.ProcedureInParm("pPUBLISH_FLAG", DbSession.DbType.NUMBER, pPublishFlag);
			oDbSession.ProcedureInParm("pREVISION_NO", DbSession.DbType.NUMBER, pRevisionNo);
			oDbSession.ProcedureInParm("pDEL_FLAG", DbSession.DbType.NUMBER, ViCommConst.FLAG_OFF);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();

			return oDbSession.GetStringValue("pSEEK_CONDITION_SEQ");
		}

	}
	#endregion ========================================================================================================

	private static Control FindControl(Control oParentControl, string pId) {
		Control oResult = null;
		foreach (Control oCtrl in oParentControl.Controls) {
			if (oCtrl.ID == null) continue;

			if (oCtrl.ID.Equals(pId)) {
				oResult = oCtrl;
				break;
			}
			oResult = FindControl(oCtrl, pId);
			if (oResult != null) {
				break;
			}
		}
		return oResult;
	}
	
	private static void ConvertEntityList2ControlInputValue(IList pInputValueEntityList, Control pTargetControl){
		foreach (InputValueEntity oInputValue in pInputValueEntityList) {
			Control oCtrl = FindControl(pTargetControl, oInputValue.Id);
			if (oCtrl != null) {
				if (oCtrl is TextBox) {
					((TextBox)oCtrl).Text = oInputValue.Value;
				} else if (oCtrl is CheckBox) {
					((CheckBox)oCtrl).Checked = bool.Parse(oInputValue.Value);
				} else if (oCtrl is RadioButton) {
					((RadioButton)oCtrl).Checked = bool.Parse(oInputValue.Value);
				} else if (oCtrl is DropDownList) {
					((DropDownList)oCtrl).SelectedValue = oInputValue.Value;
				} else if (oCtrl is RadioButtonList) {
					((RadioButtonList)oCtrl).SelectedValue = oInputValue.Value;
				} else if (oCtrl is CheckBoxList) {
					string[] oValues = oInputValue.Value.Split(',');
					foreach (ListItem oItem in ((CheckBoxList)oCtrl).Items) {
						foreach (string oValue in oValues) {
							if (oItem.Value.Equals(oValue)) {
								oItem.Selected = true;
								break;
							}
						}
					}
				}
			}
		}
	}
	
	private static IList ConvertControlInputValue2EntityList(Control pTargetControl) {
		ArrayList oInputValueEntityList = new ArrayList();

		foreach (Control oCtrl in pTargetControl.Controls) {
			if (oCtrl is TextBox) {
				TextBox oTextBox = (TextBox)oCtrl;
				oInputValueEntityList.Add(new InputValueEntity(oTextBox.ID, oTextBox.Text));
			} else if (oCtrl is CheckBox) {
				CheckBox oCheckBox = (CheckBox)oCtrl;
				oInputValueEntityList.Add(new InputValueEntity(oCheckBox.ID, oCheckBox.Checked.ToString()));
			} else if (oCtrl is RadioButton) {
				RadioButton oRadioButton = (RadioButton)oCtrl;
				oInputValueEntityList.Add(new InputValueEntity(oRadioButton.ID, oRadioButton.Checked.ToString()));
			} else if (oCtrl is DropDownList) {
				DropDownList oDropDownList = (DropDownList)oCtrl;
				oInputValueEntityList.Add(new InputValueEntity(oDropDownList.ID, oDropDownList.SelectedValue));
			} else if (oCtrl is RadioButtonList) {
				RadioButtonList oRadioButtonList = (RadioButtonList)oCtrl;
				oInputValueEntityList.Add(new InputValueEntity(oRadioButtonList.ID,oRadioButtonList.SelectedValue));
			} else if (oCtrl is CheckBoxList) {
				CheckBoxList oCheckBoxList = (CheckBoxList)oCtrl;
				StringBuilder a = new StringBuilder();
				foreach (ListItem oItem in oCheckBoxList.Items) {
					if (oItem.Selected) {
						if (a.Length != 0) {
							a.Append(",");
						}
						a.Append(oItem.Value);
					}
				}
				oInputValueEntityList.Add(new InputValueEntity(oCheckBoxList.ID, a.ToString()));
			}

			oInputValueEntityList.AddRange(ConvertControlInputValue2EntityList(oCtrl));
		}
		return oInputValueEntityList;
	}

}

