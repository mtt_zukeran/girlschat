﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: OpenCV機能を提供するヘルパークラス

--	Progaram ID		: OpenCvHelper
--
--  Creation Date	: 2015.11.30
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------
  Date			Updater			Update Explain

-------------------------------------------------------------------------*/
using System;
using System.IO;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;
using OpenCvSharp;


/// <summary>
/// ImageHelper の概要の説明です

/// </summary>
public class OpenCvHelper {
	public static DataSet SeekResembledPicByDataSet(string pSiteCd,string pLoginId,string pOriginalObjSeq,DataSet pDS) {
		if (pDS.Tables[0].Rows.Count == 0) {
			return pDS;
		}
		
		DataSet oResultDS = pDS.Clone();
		
		double dDistLimit;
		
		if (!double.TryParse(iBridUtil.GetStringValue(ConfigurationManager.AppSettings["PicDistLimit"]),out dDistLimit)) {
			dDistLimit = 0.2;
		}

		string sWebPhisicalDir = string.Empty;
		using (Site oSite = new Site()) {
			oSite.GetValue(pSiteCd,"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
		}

		string sCastPicDir = ViCommPrograms.GetCastPicDir(sWebPhisicalDir,pSiteCd,pLoginId);

		int i = 0;
		int sch = 0;
		float[] range_0 = { 0,256 };
		float[][] ranges = { range_0 };
		double tmp = 0;
		double dist = 0;

		IplImage src_img1,src_img2;
		IplImage[] dst_img1 = new IplImage[4];
		IplImage[] dst_img2 = new IplImage[4];

		CvHistogram[] hist1 = new CvHistogram[4];
		CvHistogram hist2;
		
		string sOriginalPicPath = string.Format("{0}\\{1}_s.jpg",sCastPicDir,iBridUtil.addZero(pOriginalObjSeq,ViCommConst.OBJECT_NM_LENGTH));
		if (!File.Exists(sOriginalPicPath)) {
			return oResultDS;
		}
		
		string sCheckPicPath = string.Empty;

		src_img1 = IplImage.FromFile(sOriginalPicPath,LoadMode.AnyDepth | LoadMode.AnyColor);

		sch = src_img1.NChannels;
		for (i = 0;i < sch;i++) {
			dst_img1[i] = Cv.CreateImage(Cv.Size(src_img1.Width,src_img1.Height),src_img1.Depth,1);
		}

		int[] nHisSize = new int[1];
		nHisSize[0] = 256;
		hist1[0] = Cv.CreateHist(nHisSize,HistogramFormat.Array,ranges,true);

		if (sch == 1) {
			Cv.Copy(src_img1,dst_img1[0]);
		} else {
			Cv.Split(src_img1,dst_img1[0],dst_img1[1],dst_img1[2],dst_img1[3]);
		}

		for (i = 0;i < sch;i++) {
			Cv.CalcHist(dst_img1[i],hist1[i],false);
			Cv.NormalizeHist(hist1[i],10000);
			if (i < 3) {
				Cv.CopyHist(hist1[i],ref hist1[i + 1]);
			}
		}

		Cv.ReleaseImage(src_img1);

		try {
			foreach (DataRow dr in pDS.Tables[0].Rows) {
				sCheckPicPath = string.Format("{0}\\{1}_s.jpg",sCastPicDir,iBridUtil.addZero(dr["PIC_SEQ"].ToString(),ViCommConst.OBJECT_NM_LENGTH));
				if (File.Exists(sCheckPicPath)) {
					dist = 0.0;
					src_img2 = IplImage.FromFile(sCheckPicPath,LoadMode.AnyDepth | LoadMode.AnyColor);

					for (i = 0;i < sch;i++) {
						dst_img2[i] = Cv.CreateImage(Cv.Size(src_img2.Width,src_img2.Height),src_img2.Depth,1);
					}

					nHisSize[0] = 256;

					hist2 = Cv.CreateHist(nHisSize,HistogramFormat.Array,ranges,true);

					if (sch == 1) {
						Cv.Copy(src_img2,dst_img2[0]);
					} else {
						Cv.Split(src_img2,dst_img2[0],dst_img2[1],dst_img2[2],dst_img2[3]);
					}

					for (i = 0;i < sch;i++) {
						Cv.CalcHist(dst_img2[i],hist2,false);
						Cv.NormalizeHist(hist2,10000);
						tmp = Cv.CompareHist(hist1[i],hist2,HistogramComparison.Bhattacharyya);

						dist += tmp * tmp;
					}

					dist = Math.Sqrt(dist);

					Cv.ReleaseHist(hist2);
					Cv.ReleaseImage(src_img2);

					if (dist <= dDistLimit) {
						oResultDS.Tables[0].ImportRow(dr);
					}
					
				}
			}

		} catch (OpenCVException ex) {
			Console.WriteLine("Error : " + ex.Message);
		}
		
		return oResultDS;
	}
}
