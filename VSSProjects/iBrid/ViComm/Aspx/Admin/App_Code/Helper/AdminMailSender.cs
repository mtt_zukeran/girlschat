﻿using System;
using System.Data;
using System.Configuration;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using iBridCommLib;
using ViComm;


#region □■□ AdminMailSender □■□ =================================================================================

public abstract class AdminMailSender{
	private string siteCd;
	private string mailTemplateNo;
	private string mailSendType;
	private string txLoginId;
	private string txUserCharNo;
	private string registAdminId;
	private string mailServer;

	protected string SiteCd { get { return this.siteCd; } }
	protected string MailTemplateNo { get { return this.mailTemplateNo; } }
	protected string MailSendType { get { return this.mailSendType; } }
	protected string TxLoginId { get { return this.txLoginId; } }
	protected string TxUserCharNo { get { return this.txUserCharNo; } }
	protected string RegistAdminId { get { return this.registAdminId; } }
	protected string MailServer { get { return this.mailServer; } }

	public int ProcessingAvailableCount { get { return 3; } }
	public int ProcessingAvailablePeriod { get { return 5; } }
	
	public AdminMailSender(string pSiteCd, string pMailTemplateNo, string pMailSendType, string pTxLoginId, string pTxUserCharNo, string pRegistAdminId,string pMailServer) {
		this.siteCd = pSiteCd;
		this.mailTemplateNo = pMailTemplateNo;
		this.mailSendType = pMailSendType;
		this.txLoginId = pTxLoginId;
		this.txUserCharNo = pTxUserCharNo;
		this.registAdminId = pRegistAdminId;
		this.mailServer = pMailServer;
	}
	
	public bool Send(){
		if(IsAvailableAsyncTx()){
			if(HasProcessing()) return false;
			Thread oThread = new Thread(new ThreadStart(delegate(){SendImpl();}));
			oThread.Start();
		}else{
			this.OnSend();
		}
		return true;
	}

	private void SendImpl(){
		decimal? dAdminMailTxHistorySeq = null;
		try{
			dAdminMailTxHistorySeq = LogBegin(); 
			
			this.OnSend();
			
			LogSuccessful(dAdminMailTxHistorySeq.Value);
		}catch(Exception ex){
			EventLogWriter.Error(ex);			
			if(dAdminMailTxHistorySeq != null){
				LogError(dAdminMailTxHistorySeq.Value);
			}
		}
	}

	private decimal LogBegin() {
		decimal dAdminMailTxHistorySeq;
		
		using(DbSession oDbSession = new DbSession()){
			oDbSession.PrepareProcedure("ADMIN_MAIL_TX_HISTORY_BEGIN");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.siteCd);
			oDbSession.ProcedureInParm("pRX_SEX_CD", DbSession.DbType.VARCHAR2, this.RxSexCd);
			oDbSession.ProcedureInParm("pMAIL_TEMPLATE_NO", DbSession.DbType.VARCHAR2, this.mailTemplateNo);
			oDbSession.ProcedureInParm("pMAIL_SEND_TYPE", DbSession.DbType.VARCHAR2, this.mailSendType);
			oDbSession.ProcedureInParm("pTX_LOGIN_ID", DbSession.DbType.VARCHAR2, this.txLoginId);
			oDbSession.ProcedureInParm("pTX_USER_CHAR_NO", DbSession.DbType.VARCHAR2, this.txUserCharNo);
			oDbSession.ProcedureInParm("pREGIST_ADMIN_ID", DbSession.DbType.VARCHAR2, this.RegistAdminId);
			oDbSession.ProcedureOutParm("pADMIN_MAIL_TX_HISTORY_SEQ", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();

			dAdminMailTxHistorySeq = oDbSession.GetDecimalValue("pADMIN_MAIL_TX_HISTORY_SEQ");
		}
		return dAdminMailTxHistorySeq;
	}
	private void LogSuccessful(decimal pAdminMailTxHistorySeq) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("ADMIN_MAIL_TX_HISTORY_END");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.siteCd);
			oDbSession.ProcedureInParm("pADMIN_MAIL_TX_HISTORY_SEQ", DbSession.DbType.NUMBER, pAdminMailTxHistorySeq);
			oDbSession.ProcedureInParm("pBULK_PROC_STATUS", DbSession.DbType.VARCHAR2, ViCommConst.BulkProcStatus.SUCCESS);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
		}
	}
	private void LogError(decimal pAdminMailTxHistorySeq) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("ADMIN_MAIL_TX_HISTORY_END");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.siteCd);
			oDbSession.ProcedureInParm("pADMIN_MAIL_TX_HISTORY_SEQ", DbSession.DbType.NUMBER, pAdminMailTxHistorySeq);
			oDbSession.ProcedureInParm("pBULK_PROC_STATUS", DbSession.DbType.VARCHAR2, ViCommConst.BulkProcStatus.ERROR);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2 );
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
		}
	}

	private bool IsAvailableAsyncTx() {
		using (ManageCompany oManageCompany = new ManageCompany()) {
			return oManageCompany.IsAvailableService(ViCommConst.RELEASE_ASYNC_TX_ADMIN_MAIL);
		}
	}
	/// <summary>
	///  実行中のタスクが存在するかどうかを示す値を取得する
	/// </summary>
	/// <returns>実行中のタスクが存在する場合はtrue。それ以外はfalse。</returns>
	private bool HasProcessing(){
		bool bHasProcessing = false;
		
		using(AdminMailTxHistory oAdminMailTxHistory = new AdminMailTxHistory()){
			bHasProcessing = oAdminMailTxHistory.GetProcessingCount(this.siteCd, this.ProcessingAvailablePeriod, this.ProcessingAvailableCount) >= this.ProcessingAvailableCount;
		}
		
		return bHasProcessing;
	}
	
	protected abstract string RxSexCd { get;}	
	protected abstract void OnSend();
	
}

#endregion ============================================================================================================


#region □■□ AdminToManMailSender □■□ ============================================================================

public class AdminToManMailSender : AdminMailSender{
	private string[] userSeqArray;
	private int mailAvaHour;
	private int pointTransferAvaHour;
	private int servicePoint;
	private string originalTitle;
	private string[] originalDoc;
	private int originalDocCount;
	private int textSendFlag;
	private int pointMailOutcomeSeq;

	public AdminToManMailSender(
			string pSiteCd, 
			string pMailTemplateNo, 
			string pMailSendType, 
			string pTxLoginId, 
			string pTxUserCharNo,
			string pRegistAdminId,
			string[] pUserSeqArray,
			int pMailAvaHour,
			int pPointTransferAvaHour,
			int pServicePoint,
			string pOriginalTitle,
			string[] pOriginalDoc,
			int pOriginalDocCount,
			int pPointMailOutcomeSeq,
			int pTextSendFlag,
			string pMailServer)
		: base(pSiteCd, pMailTemplateNo, pMailSendType, pTxLoginId, pTxUserCharNo, pRegistAdminId,pMailServer) {
			
		this.userSeqArray = pUserSeqArray;
		this.mailAvaHour = pMailAvaHour;
		this.pointTransferAvaHour = pPointTransferAvaHour;
		this.servicePoint = pServicePoint;
		this.originalTitle = pOriginalTitle;
		this.originalDoc = pOriginalDoc;
		this.originalDocCount = pOriginalDocCount;
		this.textSendFlag = pTextSendFlag;
		this.pointMailOutcomeSeq = pPointMailOutcomeSeq;
	}

	protected override string RxSexCd { get { return ViCommConst.MAN; } }

	protected override void OnSend() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("TX_ADMIN_TO_MAN_MAIL");
			
			db.ProcedureInParm("PSITE_CD", DbSession.DbType.VARCHAR2, this.SiteCd);
			db.ProcedureInParm("PMAIL_TEMPLATE_NO", DbSession.DbType.VARCHAR2, this.MailTemplateNo);
			db.ProcedureInParm("PCAST_LOGIN_ID", DbSession.DbType.VARCHAR2, this.TxLoginId);
			db.ProcedureInParm("PCAST_CHAR_NO", DbSession.DbType.VARCHAR2, this.TxUserCharNo);
			db.ProcedureInArrayParm("PMAN_USER_SEQ", DbSession.DbType.VARCHAR2, this.userSeqArray.Length, this.userSeqArray);
			db.ProcedureInParm("PMAN_USER_COUNT", DbSession.DbType.NUMBER, this.userSeqArray.Length);
			db.ProcedureInParm("PMAIL_AVA_HOUR", DbSession.DbType.NUMBER, this.mailAvaHour);
			db.ProcedureInParm("PPOINT_TRANSFER_AVA_HOUR", DbSession.DbType.NUMBER, this.pointTransferAvaHour);
			db.ProcedureInParm("PSERVICE_POINT", DbSession.DbType.NUMBER, this.servicePoint);
			db.ProcedureInParm("PORIGINAL_TITLE", DbSession.DbType.VARCHAR2, this.originalTitle);
			db.ProcedureInArrayParm("PORIGINAL_DOC", DbSession.DbType.VARCHAR2, this.originalDocCount, this.originalDoc);
			db.ProcedureInParm("PORIGINAL_DOC_COUNT", DbSession.DbType.NUMBER, this.originalDocCount);
			db.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);
			db.ProcedureInParm("pTEST_SEND_FLAG", DbSession.DbType.VARCHAR2, this.textSendFlag);
			db.ProcedureInParm("pMAIL_SEND_TYPE", DbSession.DbType.VARCHAR2, this.MailSendType);
			db.ProcedureInParm("pPOINT_MAIL_OUTCOME_SEQ",DbSession.DbType.NUMBER,this.pointMailOutcomeSeq);
			db.ProcedureInParm("pMAIL_SERVER",DbSession.DbType.VARCHAR2,this.MailServer);
			
			db.ExecuteProcedure();
			
		}		
	}
}

#endregion ============================================================================================================


#region □■□ AdminToCastMailSender □■□ ============================================================================

public class AdminToCastMailSender : AdminMailSender {
	private string[] userSeqArray;
	private string originalTitle;
	private string[] originalDoc;
	private int originalDocCount;
	private int textSendFlag;

	public AdminToCastMailSender(
			string pSiteCd,
			string pMailTemplateNo,
			string pMailSendType,
			string pRegistAdminId,
			string[] pUserSeqArray,
			string pOriginalTitle,
			string[] pOriginalDoc,
			int pOriginalDocCount,
			int pTextSendFlag,
			string pMailServer)
		: base(pSiteCd, pMailTemplateNo, pMailSendType, string.Empty, string.Empty, pRegistAdminId,pMailServer) {

		this.userSeqArray = pUserSeqArray;
		this.originalTitle = pOriginalTitle;
		this.originalDoc = pOriginalDoc;
		this.originalDocCount = pOriginalDocCount;
		this.textSendFlag = pTextSendFlag;

	}

	protected override string RxSexCd { get { return ViCommConst.OPERATOR; } }

	protected override void OnSend() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("TX_ADMIN_TO_CAST_MAIL");
			db.ProcedureInParm("PSITE_CD", DbSession.DbType.VARCHAR2, this.SiteCd);
			db.ProcedureInParm("PMAIL_TEMPLATE_NO", DbSession.DbType.VARCHAR2, this.MailTemplateNo);
			db.ProcedureInArrayParm("PCAST_USER_SEQ", DbSession.DbType.VARCHAR2, userSeqArray.Length, userSeqArray);
			db.ProcedureInParm("PCAST_USER_COUNT", DbSession.DbType.NUMBER, userSeqArray.Length);
			db.ProcedureInParm("PORIGINAL_TITLE", DbSession.DbType.VARCHAR2, originalTitle);
			db.ProcedureInArrayParm("PORIGINAL_DOC", DbSession.DbType.VARCHAR2, originalDocCount, originalDoc);
			db.ProcedureInParm("PORIGINAL_DOC_COUNT", DbSession.DbType.NUMBER, originalDocCount);
			db.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);
			db.ProcedureInParm("pTEST_SEND_FLAG", DbSession.DbType.VARCHAR2, textSendFlag);
			db.ProcedureInParm("pMAIL_SEND_TYPE", DbSession.DbType.VARCHAR2, this.MailSendType);
			db.ProcedureInParm("pMAIL_SERVER",DbSession.DbType.VARCHAR2,this.MailServer);
			db.ExecuteProcedure();
		}

	}
}

#endregion ============================================================================================================

#region □■□ ApproachAdminToCastMailSender □■□ ============================================================================

public class ApproachAdminToCastMailSender : AdminMailSender {
	private string[] userSeqArray;
	private string[] userCharNoArray;
	private string originalTitle;
	private string[] originalDoc;
	private int textSendFlag;

	public ApproachAdminToCastMailSender(
			string pSiteCd,
			string pMailTemplateNo,
			string pMailSendType,
			string pTxLoginId,
			string pRegistAdminId,
			string[] pUserSeqArray,
			string[] pUserCharNoArray,
			string pOriginalTitle,
			string[] pOriginalDoc,
			int pTextSendFlag)
		: base(pSiteCd, pMailTemplateNo, pMailSendType, pTxLoginId, ViCommConst.MAIN_CHAR_NO, pRegistAdminId,ViCommConst.MailServer.BEAM) {

		this.userSeqArray = pUserSeqArray;
		this.userCharNoArray = pUserCharNoArray;
		this.originalTitle = pOriginalTitle;
		this.originalDoc = pOriginalDoc;
		this.textSendFlag = pTextSendFlag;

	}

	protected override string RxSexCd { get { return ViCommConst.OPERATOR; } }

	protected override void OnSend() {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("TX_APPROACH_ADMIN_TO_CAST_MAIL");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.SiteCd);
			oDbSession.ProcedureInParm("pMAIL_TEMPLATE_NO", DbSession.DbType.VARCHAR2, this.MailTemplateNo);
			oDbSession.ProcedureInParm("pMAN_LOGIN_ID", DbSession.DbType.VARCHAR2, this.TxLoginId);
			oDbSession.ProcedureInParm("pMAN_CHAR_NO", DbSession.DbType.VARCHAR2, ViCommConst.MAIN_CHAR_NO);
			oDbSession.ProcedureInArrayParm("pCAST_USER_SEQ", DbSession.DbType.ARRAY_VARCHAR2, this.userSeqArray);
			oDbSession.ProcedureInArrayParm("pCAST_USER_CHAR_NO", DbSession.DbType.ARRAY_VARCHAR2, this.userCharNoArray);
			oDbSession.ProcedureInParm("pORIGINAL_TITLE", DbSession.DbType.VARCHAR2, this.originalTitle);
			oDbSession.ProcedureInArrayParm("pORIGINAL_DOC", DbSession.DbType.ARRAY_VARCHAR2, this.originalDoc);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureInParm("pTEST_SEND_FLAG", DbSession.DbType.NUMBER, this.textSendFlag);
			oDbSession.ProcedureInParm("pMAIL_SEND_TYPE", DbSession.DbType.VARCHAR2, this.MailSendType);
			oDbSession.ExecuteProcedure();
		}
	}
}

#endregion ============================================================================================================
