﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 動画変換登録

--	Progaram ID		: MovieConvert
--
--  Creation Date	: 2010.07.01
--  Creater			: Kazuaki.Itoh
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------
  Date			Updater			Update Explain

-------------------------------------------------------------------------*/
using System;
using System.IO;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using iBridCommLib;
using ViComm;

/// <summary>
/// MovieConvertHelper の概要の説明です
/// </summary>
public class MovieConvertHelper {
	public MovieConvertHelper() { }
	
	public static string GetWebPhisicalDir(string pSiteCd){
		string sWebPhisicalDir = string.Empty;
		using (Site oSite = new Site()) {
			oSite.GetValue(pSiteCd, "WEB_PHISICAL_DIR", ref sWebPhisicalDir);
		}
		return sWebPhisicalDir;
	}
	
	public static string GetFileNm(decimal pMovieSeq){
		return ViCommConst.MOVIE_HEADER + iBridUtil.addZero(pMovieSeq.ToString(), ViCommConst.OBJECT_NM_LENGTH);
	}
	
	public static string GetMovieFilePath(string pSiteCd,string pLoginId,string pFileNm){
		return GetWebPhisicalDir(pSiteCd) +
					ViCommConst.MOVIE_DIRECTRY
					 + "\\" + pSiteCd
					 + "\\Operator\\" + pLoginId
					 + "\\" +pFileNm + ViCommConst.MOVIE_FOODER;
	}
	public static string GetUploadFilePath(decimal pMovieSeq) {
		string sFileNm = GetFileNm(pMovieSeq);
		return GetUploadFilePath(sFileNm);
	}		
	public static string GetUploadFilePath(string pFileNm) {
		return ConfigurationManager.AppSettings["MailParseDir"] + "\\" + pFileNm + ViCommConst.MOVIE_FOODER;
	}

	#region □■□ Convert □■□ =====================================================================================

	public static localhost.ConvertMobileMovieResult[] ConvertProductMovie(string pSiteCd, string pProductId, decimal pMovieSeq, string pAspectType) {

		string sMailParserDir = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["MailParseDir"]);
		string sFileNm = MovieConvertHelper.GetFileNm(pMovieSeq);

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME, string.Empty, ViCommConst.FILE_UPLOAD_PASSWORD)) {
			string sProductPhisicalDir = ViCommPrograms.GetProductPhysicalDir(MovieConvertHelper.GetWebPhisicalDir(pSiteCd), pSiteCd, pProductId);
			using (localhost.Service objWebSrv = new localhost.Service()) {
				string sInDir = sMailParserDir;
				string sInFileNm = sFileNm + ViCommConst.MOVIE_FOODER;
				string sOutDir = sProductPhisicalDir;
				string sOutTumbnailDir = sProductPhisicalDir;
				string sOutFileNm = sFileNm;

				objWebSrv.Timeout = 36000000;// 設定値にするほどでもないので、直指定(10Hourに変更 K.Tabei)


				return objWebSrv.ConvertProdSplitMobileMovieWithAspectRate(sMailParserDir, pSiteCd, sInDir, sInFileNm, sOutDir, sOutFileNm, pAspectType, sOutTumbnailDir);
			}
		}
	}

	public static void Convert(string pSiteCd,string pUserSeq,string pUserCharNo,decimal pMovieSeq,string pMovieType,string pLoginId,decimal pCastMovieAttrTypeSeq,decimal pCastMovieAttrSeq,decimal? pSamleMovieSeq,string pAspectType) {
		Convert(pSiteCd, pUserSeq, pUserCharNo, pMovieSeq, pMovieType, pLoginId, pCastMovieAttrTypeSeq, pCastMovieAttrSeq, pSamleMovieSeq, pAspectType, null);
	}

	public static void Convert(string pSiteCd, string pUserSeq, string pUserCharNo, decimal pMovieSeq, string pMovieType, string pLoginId, decimal pCastMovieAttrTypeSeq, decimal pCastMovieAttrSeq, decimal? pSamleMovieSeq, string pAspectType, decimal? pThumbnailPicSeq) {

		string sMailParserDir = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["MailParseDir"]);
		string sFileNm = MovieConvertHelper.GetFileNm(pMovieSeq);
		string sFullPath = MovieConvertHelper.GetMovieFilePath(pSiteCd, pLoginId, sFileNm);
		string sUploadFile = MovieConvertHelper.GetUploadFilePath(sFileNm);

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME, string.Empty, ViCommConst.FILE_UPLOAD_PASSWORD)) {
			if (!System.IO.File.Exists(sFullPath)) {

				using (localhost.Service objWebSrv = new localhost.Service()) {
					string sInDir = sMailParserDir;
					string sInFileNm = sFileNm + ViCommConst.MOVIE_FOODER;
					string sOutDir = MovieConvertHelper.GetWebPhisicalDir(pSiteCd) + ViCommConst.MOVIE_DIRECTRY + "\\" + pSiteCd + "\\Operator\\" + pLoginId;
					string sOutTumbnailDir = MovieConvertHelper.GetWebPhisicalDir(pSiteCd) + ViCommConst.PIC_DIRECTRY + "\\" + pSiteCd + "\\Operator\\" + pLoginId;
					string sOutFileNm = sFileNm;

					objWebSrv.Timeout = 36000000;// 設定値にするほどでもないので、直指定 (10Hourに変更 K.Tabei)

					if(pSamleMovieSeq!=null){
						string sSampleFileNm = MovieConvertHelper.GetFileNm(pSamleMovieSeq.Value);
						string sSampleInFileNm = sSampleFileNm+ViCommConst.MOVIE_FOODER;
						string sSampleOutFileNm = sSampleFileNm;
						objWebSrv.ConvertMobileMovieWithAspectRate(sMailParserDir, pSiteCd, sInDir, sSampleInFileNm, sOutDir, sSampleOutFileNm, pAspectType, sOutTumbnailDir);
					}

					localhost.ConvertMobileMovieResult[] resultArry
						= objWebSrv.ConvertSplitMobileMovieWithAspectRate(sMailParserDir, pSiteCd, sInDir, sInFileNm, sOutDir, sOutFileNm, pAspectType, sOutTumbnailDir);

					List<string> movileFileTypeList = new List<string>();
					List<string> fileNumList = new List<string>();

					foreach (localhost.ConvertMobileMovieResult result in resultArry) {
						movileFileTypeList.Add(result.MobileFileType);
						fileNumList.Add(result.FileNum.ToString());
					}

					using (DbSession db = new DbSession()) {
						db.PrepareProcedure("CAST_MOVIE_UPLOAD");
						db.ProcedureInParm("PSITE_CD", DbSession.DbType.VARCHAR2, pSiteCd);
						db.ProcedureInParm("PUSER_SEQ", DbSession.DbType.NUMBER, pUserSeq);
						db.ProcedureInParm("PUSER_CHAR_NO", DbSession.DbType.VARCHAR2, pUserCharNo);
						db.ProcedureInParm("PMOVIE_SEQ", DbSession.DbType.NUMBER, pMovieSeq);
						db.ProcedureInParm("PMOVIE_TYPE", DbSession.DbType.NUMBER, int.Parse(pMovieType));
						db.ProcedureInParm("PCAST_MOVIE_ATTR_TYPE_SEQ", DbSession.DbType.VARCHAR2, -1);
						db.ProcedureInParm("PCAST_MOVIE_ATTR_SEQ", DbSession.DbType.VARCHAR2, -1);
						db.ProcedureInArrayParm("PMOVIE_FILE_TYPE_ARRAY", DbSession.DbType.VARCHAR2, movileFileTypeList.Count, movileFileTypeList.ToArray());
						db.ProcedureInArrayParm("PFILE_COUNT_ARRAY", DbSession.DbType.VARCHAR2, fileNumList.Count, fileNumList.ToArray());
						db.ProcedureInParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,1);
						db.ProcedureInParm("PSAMPLE_MOVIE_SEQ", DbSession.DbType.NUMBER, pSamleMovieSeq);
						db.ProcedureInParm("PTHUMBNAIL_PIC_SEQ",DbSession.DbType.NUMBER,pThumbnailPicSeq); 
						db.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);
						db.ExecuteProcedure();
					}
				}
			}
		}
	}
	
	#endregion ========================================================================================================

	public FileInfo[] GetConvertTargetFileList() {
		string sDirPath = this.GetMovieConvertTargetDir();

		using (Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME, ViCommConst.FILE_UPLOAD_PASSWORD)) {
			DirectoryInfo oDirInfo = new DirectoryInfo(sDirPath);

			List<FileInfo> oTargetFileInfoList = new List<FileInfo>();
			foreach (FileInfo oFileInfo in oDirInfo.GetFiles()) {
				if (!oFileInfo.Name.ToLower().StartsWith(ViCommConst.PREFIX_SAMPLE_MOVIE)) {
					oTargetFileInfoList.Add(oFileInfo);
				}
			}
			return oTargetFileInfoList.ToArray();
		}
	}

	/// <summary>
	/// 変換対象ファイルを格納するディレクトリの設定値を取得する
	/// </summary>
	/// <returns>変換対象ファイルを格納するディレクトリの設定値</returns>
	private string GetMovieConvertTargetDir() {
		string sDirPath = AdminConfigs.MovieConvertTargetDir;
		using (Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME, ViCommConst.FILE_UPLOAD_PASSWORD)) {
			if (!Directory.Exists(sDirPath)) {
				string sErrorMessage = string.Format("「MovieConvertTargetDir」が設定されていないか、設定値に誤りがあります。(設定値:{0})", sDirPath);
				throw new InvalidOperationException(sErrorMessage);
			}
			return sDirPath;
		}
	}
}
