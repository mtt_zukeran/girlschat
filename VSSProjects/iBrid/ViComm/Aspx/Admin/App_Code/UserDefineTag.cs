﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ユーザー定義タグ
--	Progaram ID		: UserDefineTag
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class UserDefineTag:DbSession {

	public string htmlDoc;
	public string htmlDoc2;
	public string htmlDoc3;
	public string htmlDoc4;
	public string htmlDoc5;
	public string htmlDoc6;

	public UserDefineTag() {
	}

	public int GetPageCount(string pSiteCd,string pUserAgentType,string pCategorySeq,string pVariableId) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;

		string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_USER_DEFINE_TAG ";

		string sWhere = "";
		OracleParameter[] objParms = CreateWhere(pSiteCd,pUserAgentType,pCategorySeq,pVariableId,ref sWhere);
		sSql = sSql + sWhere;
		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pUserAgentType,string pCategorySeq,string pVariableId,int startRowIndex,int maximumRows) {
		DataSet ds;
		ds = new DataSet();
		string sViewNm = "VW_USER_DEFINE_TAG01";
		string sOrder = "ORDER BY SITE_CD,VARIABLE_ID,USER_AGENT_TYPE";
		string sSql = "SELECT " +
							"SITE_CD			," +
							"VARIABLE_ID		," +
							"USER_AGENT_TYPE	," +
							"CATEGORY_NM		," +
							"VARIABLE_NM		," +
							"SUBSTR(HTML_DOC1,1,30) DOC," +
							"HTML_DOC1			," +
							"HTML_DOC2			," +
							"HTML_DOC3			," +
							"HTML_DOC4			," +
							"HTML_DOC5			," +
							"HTML_DOC6			," +
							"HTML_DOC7			," +
							"HTML_DOC8			," +
							"HTML_DOC9			," +
							"HTML_DOC10			," +
							"USER_AGENT_TYPE_NM	," +
							"UPDATE_DATE		" +
						"FROM(" +
						string.Format(" SELECT {0}.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM {0}  ",sViewNm);

		string sWhere = "";
		OracleParameter[] objParms = CreateWhere(pSiteCd,pUserAgentType,pCategorySeq,pVariableId,ref sWhere);
		sSql = sSql + sWhere;

		sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
		sSql = sSql + sOrder;
		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,sViewNm);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetCsvData(string pSiteCd,string pUserAgentType,string pCategorySeq,string pVariableId) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = " ORDER BY SITE_CD,VARIABLE_ID ";
			string sSql = "SELECT " +
								"SITE_CD			," +
								"VARIABLE_ID		," +
								"USER_AGENT_TYPE	," +
								"VARIABLE_NM		," +
								"USER_AGENT_TYPE_NM	," +
								"HTML_DOC1			," +
								"HTML_DOC2			," +
								"HTML_DOC3			," +
								"HTML_DOC4			," +
								"HTML_DOC5			," +
								"HTML_DOC6			," +
								"HTML_DOC7			," +
								"HTML_DOC8			," +
								"HTML_DOC9			," +
								"HTML_DOC10		" +
						"FROM " +
							 "VW_USER_DEFINE_TAG01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserAgentType,pCategorySeq,pVariableId,ref sWhere);
			sSql = sSql + sWhere;
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USER_DEFINE_TAG01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pUserAgentType,string pCategorySeq,string pVariableId,ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		pWhere = pWhere + " WHERE SITE_CD = :SITE_CD ";
		list.Add(new OracleParameter("SITE_CD",pSiteCd));

		if (pUserAgentType.Equals("") == false) {
			pWhere = pWhere + " AND USER_AGENT_TYPE = :USER_AGENT_TYPE ";
			list.Add(new OracleParameter("USER_AGENT_TYPE",pUserAgentType));
		}

		if (!string.IsNullOrEmpty(pCategorySeq)) {
			if (pCategorySeq.Equals("null")) {
				pWhere = pWhere + " AND CATEGORY_SEQ IS NULL ";
			} else {
				pWhere = pWhere + " AND CATEGORY_SEQ = :CATEGORY_SEQ ";
				list.Add(new OracleParameter("CATEGORY_SEQ",pCategorySeq));
			}
		}

		if (!string.IsNullOrEmpty(pVariableId)) {
			pWhere = pWhere + " AND VARIABLE_ID LIKE '%'||:VARIABLE_ID||'%' ";
			list.Add(new OracleParameter("VARIABLE_ID",pVariableId));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}


	public DataSet GetListByVariableId(string pSiteCd,string pVariableId,string pUserAgentType) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT * FROM T_USER_DEFINE_TAG WHERE SITE_CD = :SITE_CD AND VARIABLE_ID = :VARIABLE_ID AND USER_AGENT_TYPE = :USER_AGENT_TYPE";
			sSql = sSql + " ORDER BY SITE_CD, VARIABLE_ID ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("VARIABLE_ID",pVariableId);
				cmd.Parameters.Add("USER_AGENT_TYPE",pUserAgentType);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
}
