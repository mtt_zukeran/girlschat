﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: プロダクション業務連絡
--	Progaram ID		: BusinessReport
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  2009/07/23  iBrid(Y.Inoue)

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class BusinessReport:DbSession {

	public BusinessReport() {
	}

	public int GetPageCount(string pProductionSeq,string pSiteCd) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_BUSINESS_REPORT ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pProductionSeq,pSiteCd,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pProductionSeq,string pSiteCd,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY PRODUCTION_SEQ,START_PUB_DAY DESC,DOC_SEQ DESC ";
			string sSql = "SELECT " +
								"DOC_SEQ			," +
								"PRODUCTION_SEQ		," +
								"DOC_TITLE			," +
								"START_PUB_DAY		," +
								"HTML_DOC			," +
								"END_PUB_DAY		," +
								"UPDATE_DATE " +
							"FROM(" +
							" SELECT T_BUSINESS_REPORT.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM T_BUSINESS_REPORT  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pProductionSeq,pSiteCd,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_BUSINESS_REPORT");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(string pProductionSeq, string pSiteCd,ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();

		pWhere = pWhere + " WHERE ";
		pWhere = pWhere + "		PRODUCTION_SEQ	= :PRODUCTION_SEQ	AND ";
		pWhere = pWhere + "		SITE_CD			= :SITE_CD ";
		list.Add(new OracleParameter("PRODUCTION_SEQ",pProductionSeq));
		list.Add(new OracleParameter("SITE_CD",pSiteCd));

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

}
