﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: この娘を探せ・スケジュール


--	Progaram ID		: SiteManagementEx
--  Creation Date	: 2011.03.29
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;

public class SiteManagementEx : DbSession {
	public SiteManagementEx() {
	}

	public int GetPageCount() {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_SITE_MANAGEMENT_EX	SMEX,");
		oSqlBuilder.AppendLine("	T_SITE					S");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SMEX.SITE_CD	= S.SITE_CD");

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(), conn)) {
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(int startRowIndex, int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	SMEX.SITE_CD		,");
		oSqlBuilder.AppendLine("	SMEX.UPDATE_DATE	,");
		oSqlBuilder.AppendLine("	S.SITE_NM");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_SITE_MANAGEMENT_EX	SMEX,");
		oSqlBuilder.AppendLine("	T_SITE					S");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SMEX.SITE_CD	= S.SITE_CD");

		string sSortExpression = "ORDER BY SMEX.SITE_CD";
		string sPagingSql;
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(), sSortExpression, startRowIndex, maximumRows, out sPagingSql);

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql, conn)) {
				cmd.BindByName = true;

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					cmd.Parameters.AddRange(oPagingParams);
					da.Fill(oDataSet);
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}

	public string GetTpManWaiingCastWithTalk(string pSiteCd) {
		string sQuery = "SELECT TP_MAN_WAITING_CAST_WITH_TALK FROM T_SITE_MANAGEMENT_EX WHERE SITE_CD = :SITE_CD";

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sQuery,conn)) {
				cmd.Parameters.Add(new OracleParameter("SITE_CD", pSiteCd));
				return iBridUtil.GetStringValue(cmd.ExecuteScalar());
			}
		} finally {
			conn.Clone();
		}
	}
}
