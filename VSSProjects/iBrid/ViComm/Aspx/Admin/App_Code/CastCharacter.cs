﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者キャラクター
--	Progaram ID		: CastCharacter
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class CastCharacter:DbSession {
	public string userSeq;
	public string handleNm;

	public CastCharacter() {
	}

	public int GetPageCount(
		string pOnline,
		string pLoginId,
		string pProductionSeq,
		string pCastNm,
		string pSiteCd,
		string pTalkType,
		string pTerminalType,
		string pHandleNm,
		string pActCategorySeq,
		string pEmailAddr,
		string pPicType,
		int pNonExistMailAddrFlagMask,
		string pMainCharOnlyFlag,
		string pManagerSeq,
		string pRegistDayFrom,
		string pRegistDayTo,
		string pLastLoginDayFrom,
		string pLastLoginDayTo,

		string pFinalLoginDayFrom,
		string pFinalLoginDayTo,
		
		string pTotalPaymentAmtFrom,
		string pTotalPaymentAmtTo,
		string pNaFlag,
		string pUserRank,
		string pAdCd,
		string pTel,
		string pCastAttrValue1,
		string pCastAttrValue2,
		string pCastAttrValue3,
		string pCastAttrValue4
	) {
		return this.GetPageCount(
			pOnline,
			pLoginId,
			pProductionSeq,
			pCastNm,
			pSiteCd,
			pTalkType,
			pTerminalType,
			pHandleNm,
			pActCategorySeq,
			pEmailAddr,
			pPicType,
			pNonExistMailAddrFlagMask,
			pMainCharOnlyFlag,
			pManagerSeq,
			pRegistDayFrom,
			pRegistDayTo,
			pLastLoginDayFrom,
			pLastLoginDayTo,

			pFinalLoginDayFrom,
			pFinalLoginDayTo,
			
			pTotalPaymentAmtFrom,
			pTotalPaymentAmtTo,
			pNaFlag,
			pUserRank,
			pAdCd,
			pTel,
			pCastAttrValue1,
			pCastAttrValue2,
			pCastAttrValue3,
			pCastAttrValue4,
			"",
			"",
			"",
			"",
			0,
			"",
			"",
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			false,
			string.Empty,
			string.Empty,
			0,
			false,
			false,
			false,
			false,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			false,
			string.Empty,
			string.Empty,
			string.Empty
		);
	}

	public int GetPageCount(
		string pOnline,
		string pLoginId,
		string pProductionSeq,
		string pCastNm,
		string pSiteCd,
		string pTalkType,
		string pTerminalType,
		string pHandleNm,
		string pActCategorySeq,
		string pEmailAddr,
		string pPicType,
		int pNonExistMailAddrFlagMask,
		string pMainCharOnlyFlag,
		string pManagerSeq,
		string pRegistDayFrom,
		string pRegistDayTo,
		string pLastLoginDayFrom,
		string pLastLoginDayTo,

		string pFinalLoginDayFrom,
		string pFinalLoginDayTo,
		
		string pTotalPaymentAmtFrom,
		string pTotalPaymentAmtTo,
		string pNaFlag,
		string pUserRank,
		string pAdCd,
		string pTel,
		string pCastAttrValue1,
		string pCastAttrValue2,
		string pCastAttrValue3,
		string pCastAttrValue4,
		string pAgeFrom,
		string pAgeTo,
		string pFavoritMeCountFrom,
		string pFavoritMeCountTo,
		int pUserStatusMask,
		string pPickupId,
		string pGuid,
		string pRemarks1,
		string pEnabledBlogFlag,
		string pEnabledRichinoFlag,
		string pSiteUseStatus,
		string pInfoMailRxType,
		string pUserMailRxType,
		string pReportAffiliateDateFrom,
		string pReportAffiliateDateTo,
		string pUserDefineMask,
		string pExceptRefusedByManUserSeq,
		string pLastTxMailDateFrom,
		string pLastTxMailDateTo,
		string pGameHandleNm,
		string pGameCharacterType,
		string pGameRegistDateFrom,
		string pGameRegistDateTo,
		string pGameCharacterLevelFrom,
		string pGameCharacterLevelTo,
		string pGamePointFrom,
		string pGamePointTo,
		string pCastGamePointFrom,
		string pCastGamePointTo,
		bool pHandleNmExactFlag,
		string pBeforeSystemId,
		string pKeyword,
		int pCarrier,
		bool pLoginIdNegativeFlag,
		bool pLastActionDayNegativeFlag,
		bool pUserDefineMaskNegatieFlag,
		bool pIntroducerFriendAllFlag,
		string pCrosmileLastUsedVersionFrom,
		string pCrosmileLastUsedVersionTo,
		string pUseCrosmileFlag,
		string pSortExpression,
		string pSortDirection,
		string pGuardianTel,
		string pUserSeq,
		string pUseVoiceappFlag,
		string pGcappLastLoginVersionFrom,
		string pGcappLastLoginVersionTo,
		string pUseJealousyFlag,
		bool pExcludeEmailAddr,
		string pTxMuller3NgMailAddrFlag,
		string pNotPaymentAmtFrom,
		string pNotPaymentAmtTo
	) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			StringBuilder sSql = new StringBuilder();

			sSql.Append("SELECT COUNT(*) AS ROW_COUNT FROM (SELECT P.*, GET_NOT_PAYMENT_AMT(P.USER_SEQ) AS NOT_PAYMENT_AMT FROM VW_CAST_CHARACTER00 P) P ");

			string[] sCastAttrValue = { pCastAttrValue1,pCastAttrValue2,pCastAttrValue3,pCastAttrValue4 };
			for (int i = 0;i < ViCommConst.MAX_ADMIN_CAST_INQUIRY_ITEMS;i++) {
				if (!string.IsNullOrEmpty(sCastAttrValue[i])) {
					sSql.Append(string.Format(",VW_CAST_ATTR_VALUE01 AT{0} ",i + 1));
				}
			}
			if (!string.IsNullOrEmpty(pPickupId)) {
				SetPickupTable(pSiteCd,pPickupId,ref sSql);
			}

			if (!string.IsNullOrEmpty(pAdCd)) {
				sSql.Append(",T_SITE_USER SU ");
			}

			sSql.Append(",T_LOGIN_USER LU ");

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(
												pOnline,
												pLoginId,
												pProductionSeq,
												pCastNm,
												pSiteCd,
												pTalkType,
												pTerminalType,
												pHandleNm,
												pActCategorySeq,
												pEmailAddr,
												pPicType,
												pNonExistMailAddrFlagMask,
												pMainCharOnlyFlag,
												pManagerSeq,
												pRegistDayFrom,
												pRegistDayTo,
												pLastLoginDayFrom,
												pLastLoginDayTo,
												
												pFinalLoginDayFrom,
												pFinalLoginDayTo,
												
												pTotalPaymentAmtFrom,
												pTotalPaymentAmtTo,
												pNaFlag,
												pUserRank,
												pAdCd,
												pTel,
												pCastAttrValue1,
												pCastAttrValue2,
												pCastAttrValue3,
												pCastAttrValue4,
												pAgeFrom,
												pAgeTo,
												pFavoritMeCountFrom,
												pFavoritMeCountTo,
												pUserStatusMask,
												pPickupId,
												pGuid,
												pRemarks1,
												pEnabledBlogFlag,
												pEnabledRichinoFlag,
												pSiteUseStatus,
												pInfoMailRxType,
												pUserMailRxType,
												pReportAffiliateDateFrom,
												pReportAffiliateDateTo,
												pUserDefineMask,
												pExceptRefusedByManUserSeq,
												pLastTxMailDateFrom,
												pLastTxMailDateTo,
												pGameHandleNm,
												pGameCharacterType,
												pGameRegistDateFrom,
												pGameRegistDateTo,
												pGameCharacterLevelFrom,
												pGameCharacterLevelTo,
												pGamePointFrom,
												pGamePointTo,
												pCastGamePointFrom,
												pCastGamePointTo,
												pHandleNmExactFlag,
												pBeforeSystemId,
												pKeyword,
												pCarrier,
												pLoginIdNegativeFlag,
												pLastActionDayNegativeFlag,
												pUserDefineMaskNegatieFlag,
												pIntroducerFriendAllFlag,
												pCrosmileLastUsedVersionFrom,
												pCrosmileLastUsedVersionTo,
												pUseCrosmileFlag,
												pGuardianTel,
												pUserSeq,
												pUseVoiceappFlag,
												pGcappLastLoginVersionFrom,
												pGcappLastLoginVersionTo,
												pUseJealousyFlag,
												pExcludeEmailAddr,
												pTxMuller3NgMailAddrFlag,
												pNotPaymentAmtFrom,
												pNotPaymentAmtTo,
												ref sWhere);
			sSql.Append(sWhere);

			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				cmd.BindByName = true;
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(
		string pOnline,
		string pLoginId,
		string pProductionSeq,
		string pCastNm,
		string pSiteCd,
		string pTalkType,
		string pTerminalType,
		string pHandleNm,
		string pActCategorySeq,
		string pEmailAddr,
		string pPicType,
		int pNonExistMailAddrFlagMask,
		string pMainCharOnlyFlag,
		string pManagerSeq,
		string pRegistDayFrom,
		string pRegistDayTo,
		string pLastLoginDayFrom,
		string pLastLoginDayTo,

		string pFinalLoginDayFrom,
		string pFinalLoginDayTo,
		
		string pTotalPaymentAmtFrom,
		string pTotalPaymentAmtTo,
		string pNaFlag,
		string pUserRank,
		string pAdCd,
		string pTel,
		string pCastAttrValue1,
		string pCastAttrValue2,
		string pCastAttrValue3,
		string pCastAttrValue4,
		bool pExcludeEmailAddr,
		int startRowIndex,
		int maximumRows
	) {
		return GetPageCollection(
			pOnline,
			pLoginId,
			pProductionSeq,
			pCastNm,
			pSiteCd,
			pTalkType,
			pTerminalType,
			pHandleNm,
			pActCategorySeq,
			pEmailAddr,
			pPicType,
			pNonExistMailAddrFlagMask,
			pMainCharOnlyFlag,
			pManagerSeq,
			pRegistDayFrom,
			pRegistDayTo,
			pLastLoginDayFrom,
			pLastLoginDayTo,
			
			pFinalLoginDayFrom,
			pFinalLoginDayTo,
			
			pTotalPaymentAmtFrom,
			pTotalPaymentAmtTo,
			pNaFlag,
			pUserRank,
			pAdCd,
			pTel,
			pCastAttrValue1,
			pCastAttrValue2,
			pCastAttrValue3,
			pCastAttrValue4,
			"",
			"",
			"",
			"",
			0,
			"",
			"",
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			false,
			string.Empty,
			string.Empty,
			0,
			false,
			false,
			false,
			false,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			string.Empty,
			false,
			string.Empty,
			string.Empty,
			string.Empty,
			startRowIndex,
			maximumRows
		);
	}

	public DataSet GetPageCollection(
		string pOnline,
		string pLoginId,
		string pProductionSeq,
		string pCastNm,
		string pSiteCd,
		string pTalkType,
		string pTerminalType,
		string pHandleNm,
		string pActCategorySeq,
		string pEmailAddr,
		string pPicType,
		int pNonExistMailAddrFlagMask,
		string pMainCharOnlyFlag,
		string pManagerSeq,
		string pRegistDayFrom,
		string pRegistDayTo,
		string pLastLoginDayFrom,
		string pLastLoginDayTo,
		
		string pFinalLoginDayFrom,
		string pFinalLoginDayTo,
		
		string pTotalPaymentAmtFrom,
		string pTotalPaymentAmtTo,
		string pNaFlag,
		string pUserRank,
		string pAdCd,
		string pTel,
		string pCastAttrValue1,
		string pCastAttrValue2,
		string pCastAttrValue3,
		string pCastAttrValue4,
		string pAgeFrom,
		string pAgeTo,
		string pFavoritMeCountFrom,
		string pFavoritMeCountTo,
		int pUserStatusMask,
		string pPickupId,
		string pGuid,
		string pRemarks1,
		string pEnabledBlogFlag,
		string pEnabledRichinoFlag,
		string pSiteUseStatus,
		string pInfoMailRxType,
		string pUserMailRxType,
		string pReportAffiliateDateFrom,
		string pReportAffiliateDateTo,
		string pUserDefineMask,
		string pExceptRefusedByManUserSeq,
		string pLastTxMailDateFrom,
		string pLastTxMailDateTo,
		string pGameHandleNm,
		string pGameCharacterType,
		string pGameRegistDateFrom,
		string pGameRegistDateTo,
		string pGameCharacterLevelFrom,
		string pGameCharacterLevelTo,
		string pGamePointFrom,
		string pGamePointTo,
		string pCastGamePointFrom,
		string pCastGamePointTo,
		bool pHandleNmExactFlag,
		string pBeforeSystemId,
		string pKeyword,
		int pCarrier,
		bool pLoginIdNegativeFlag,
		bool pLastActionDayNegativeFlag,
		bool pUserDefineMaskNegatieFlag,
		bool pIntroducerFriendAllFlag,
		string pCrosmileLastUsedVersionFrom,
		string pCrosmileLastUsedVersionTo,
		string pUseCrosmileFlag,
		string pSortExpression,
		string pSortDirection,
		string pGuardianTel,
		string pUserSeq,
		string pUseVoiceappFlag,
		string pGcappLastLoginVersionFrom,
		string pGcappLastLoginVersionTo,
		string pUseJealousyFlag,
		bool pExcludeEmailAddr,
		string pTxMuller3NgMailAddrFlag,
		string pNotPaymentAmtFrom,
		string pNotPaymentAmtTo,
		int startRowIndex,
		int maximumRows
	) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			StringBuilder sSql = new StringBuilder();

			string sWhere = "";

			OracleParameter[] oParms = CreateWhere(
												pOnline,
												pLoginId,
												pProductionSeq,
												pCastNm,
												pSiteCd,
												pTalkType,
												pTerminalType,
												pHandleNm,
												pActCategorySeq,
												pEmailAddr,
												pPicType,
												pNonExistMailAddrFlagMask,
												pMainCharOnlyFlag,
												pManagerSeq,
												pRegistDayFrom,
												pRegistDayTo,
												pLastLoginDayFrom,
												pLastLoginDayTo,

												pFinalLoginDayFrom,
												pFinalLoginDayTo,
												
												pTotalPaymentAmtFrom,
												pTotalPaymentAmtTo,
												pNaFlag,
												pUserRank,
												pAdCd,
												pTel,
												pCastAttrValue1,
												pCastAttrValue2,
												pCastAttrValue3,
												pCastAttrValue4,
												pAgeFrom,
												pAgeTo,
												pFavoritMeCountFrom,
												pFavoritMeCountTo,
												pUserStatusMask,
												pPickupId,
												pGuid,
												pRemarks1,
												pEnabledBlogFlag,
												pEnabledRichinoFlag,
												pSiteUseStatus,
												pInfoMailRxType,
												pUserMailRxType,
												pReportAffiliateDateFrom,
												pReportAffiliateDateTo,
												pUserDefineMask,
												pExceptRefusedByManUserSeq,
												pLastTxMailDateFrom,
												pLastTxMailDateTo,
												pGameHandleNm,
												pGameCharacterType,
												pGameRegistDateFrom,
												pGameRegistDateTo,
												pGameCharacterLevelFrom,
												pGameCharacterLevelTo,
												pGamePointFrom,
												pGamePointTo,
												pCastGamePointFrom,
												pCastGamePointTo,
												pHandleNmExactFlag,
												pBeforeSystemId,
												pKeyword,
												pCarrier,
												pLoginIdNegativeFlag,
												pLastActionDayNegativeFlag,
												pUserDefineMaskNegatieFlag,
												pIntroducerFriendAllFlag,
												pCrosmileLastUsedVersionFrom,
												pCrosmileLastUsedVersionTo,
												pUseCrosmileFlag,
												pGuardianTel,
												pUserSeq,
												pUseVoiceappFlag,
												pGcappLastLoginVersionFrom,
												pGcappLastLoginVersionTo,
												pUseJealousyFlag,
												pExcludeEmailAddr,
												pTxMuller3NgMailAddrFlag,
												pNotPaymentAmtFrom,
												pNotPaymentAmtTo,
												ref sWhere);

			sSql.Append("SELECT									").AppendLine();
			sSql.Append(" 	T1.*                			    ,").AppendLine();
			sSql.Append("	T_SITE.SITE_NM						,").AppendLine();
			sSql.Append("	T_SITE.USE_OTHER_SYS_INFO_FLAG		,").AppendLine();
			sSql.Append("	T_SITE_USER.AD_CD					,").AppendLine();
			sSql.Append("	T_AD.AD_NM							,").AppendLine();
			sSql.Append("	T_ACT_CATEGORY.ACT_CATEGORY_NM		,").AppendLine();
			sSql.Append("	T_MANAGER.MANAGER_NM				,").AppendLine();
			sSql.Append("	T_PRODUCTION.PRODUCTION_NM			,").AppendLine();
			sSql.Append("	T1.USER_UPDATE_DATE					AS UPDATE_DATE				,").AppendLine();
			sSql.Append("	T_CODE_DTL1.CODE_NM					AS USER_STATUS_NM			,").AppendLine();
			sSql.Append("	T_CODE_DTL2.CODE_NM					AS ONLINE_STATUS_NM			,").AppendLine();
			sSql.Append("	T_CODE_DTL3.CODE_NM					AS CONNECT_TYPE_NM			,").AppendLine();
			sSql.Append("	T_CODE_DTL4.CODE_NM					AS USE_TERMINAL_TYPE_NM		,").AppendLine();
			sSql.Append("	GET_NOT_PAYMENT_AMT(T1.USER_SEQ) 	AS NOT_PAYMENT_AMT			,").AppendLine();
			sSql.Append("	GET_SMALL_PHOTO_IMG_PATH(T1.SITE_CD,T1.LOGIN_ID,T1.PROFILE_PIC_SEQ) 																AS SMALL_PHOTO_IMG_PATH	,").AppendLine();
			sSql.Append("	GET_SYS_ONLINE_IMG_PATH(T1.CHARACTER_ONLINE_STATUS,T1.IVP_REQUEST_SEQ,T1.DUMMY_TALK_FLAG,T1.MONITOR_ENABLE_FLAG,T1.TALK_LOCK_FLAG,T1.TALK_LOCK_DATE)	AS SYS_ONLINE_IMG_PATH	,").AppendLine();
			sSql.Append("	NVL(T1.PROFILE_PIC_SEQ,0)																											AS PROFILE_PIC_SEQ_NVL	,").AppendLine();
			sSql.Append("	CASE																																						").AppendLine();
			sSql.Append("		WHEN (T1.CHARACTER_ONLINE_STATUS NOT IN(0,1)) THEN																										").AppendLine();
			sSql.Append("			GET_TALK_TERM(T1.CHARACTER_ONLINE_STATUS,T1.IVP_REQUEST_SEQ,T1.MONITOR_ENABLE_FLAG,T1.TALK_LOCK_FLAG,T1.TALK_LOCK_DATE)										").AppendLine();
			sSql.Append("		ELSE																																					").AppendLine();
			sSql.Append("			NULL																																				").AppendLine();
			sSql.Append("	END AS TALK_TERM																																			,").AppendLine();
			sSql.Append("	(SELECT															").AppendLine();
			sSql.Append("			COUNT(*) AS FAVORITE_COUNT								").AppendLine();
			sSql.Append("		FROM														").AppendLine();
			sSql.Append("			T_FAVORIT FAV											").AppendLine();
			sSql.Append("		WHERE														").AppendLine();
			sSql.Append("			T1.SITE_CD 		= FAV.SITE_CD		AND					").AppendLine();
			sSql.Append("			T1.USER_SEQ 	= FAV.USER_SEQ		AND					").AppendLine();
			sSql.Append("			T1.USER_CHAR_NO = FAV.USER_CHAR_NO) AS FAVORITE_COUNT	,").AppendLine();
			sSql.Append("	(SELECT															").AppendLine();
			sSql.Append("			MAX(PAYMENT_DATE) AS LAST_PAYMENT_DATE					").AppendLine();
			sSql.Append("		FROM														").AppendLine();
			sSql.Append("			T_PAYMENT_HISTORY PH									").AppendLine();
			sSql.Append("		WHERE														").AppendLine();
			sSql.Append("			T1.USER_SEQ 	= PH.USER_SEQ)	AS LAST_PAYMENT_DATE	").AppendLine();
			sSql.Append("	FROM							").AppendLine();
			sSql.Append("	(								").AppendLine();
			sSql.Append(" 	SELECT							").AppendLine();
			sSql.Append(" 		INNER.*			,			").AppendLine();
			sSql.Append(" 		ROWNUM AS RNUM				").AppendLine();
			sSql.Append(" 	FROM							").AppendLine();
			sSql.Append(" 	(								").AppendLine();
			sSql.Append(" 	SELECT							").AppendLine();
			sSql.Append("		SITE_CD						,").AppendLine();
			sSql.Append("		USER_SEQ					,").AppendLine();
			sSql.Append("		USER_CHAR_NO				,").AppendLine();
			sSql.Append("		CAST_NM						,").AppendLine();
			sSql.Append("		CAST_KANA_NM				,").AppendLine();
			sSql.Append("		LOGIN_ID					,").AppendLine();
			sSql.Append("		LOGIN_PASSWORD				,").AppendLine();
			sSql.Append("		HANDLE_NM					,").AppendLine();
			sSql.Append("		HANDLE_KANA_NM				,").AppendLine();
			sSql.Append("		TALK_LOCK_FLAG				,").AppendLine();
			sSql.Append("		TALK_LOCK_DATE				,").AppendLine();
			sSql.Append("		IVP_REQUEST_SEQ				,").AppendLine();
			sSql.Append("		MONITOR_ENABLE_FLAG			,").AppendLine();
			sSql.Append("		ACT_CATEGORY_SEQ			,").AppendLine();
			sSql.Append("		PRODUCTION_SEQ				,").AppendLine();
			sSql.Append("		MANAGER_SEQ					,").AppendLine();
			sSql.Append("		USER_STATUS					,").AppendLine();
			sSql.Append("		LOGIN_SEQ					,").AppendLine();
			sSql.Append("		TEL							,").AppendLine();
			sSql.Append("		IMODE_ID					,").AppendLine();
			sSql.Append("		TERMINAL_UNIQUE_ID			,").AppendLine();
			sSql.Append("		EMAIL_ADDR					,").AppendLine();
			sSql.Append("		NON_EXIST_MAIL_ADDR_FLAG	,").AppendLine();
			sSql.Append("		URI							,").AppendLine();
			sSql.Append("		CONNECT_TYPE				,").AppendLine();
			sSql.Append("		USE_TERMINAL_TYPE			,").AppendLine();
			sSql.Append("		USER_RANK					,").AppendLine();
			sSql.Append("		CHARACTER_ONLINE_STATUS		,").AppendLine();
			sSql.Append("		FAVORIT_ME_COUNT			,").AppendLine();
			sSql.Append("		CAST_REVISION_NO			,").AppendLine();
			sSql.Append("		COMMENT_LIST				,").AppendLine();
			sSql.Append("		COMMENT_DETAIL				,").AppendLine();
			sSql.Append("		COMMENT_ADMIN				,").AppendLine();
			sSql.Append("		PROFILE_PIC_SEQ				,").AppendLine();
			sSql.Append("		START_PERFORM_DAY			,").AppendLine();
			sSql.Append("		OK_PLAY_MASK				,").AppendLine();
			sSql.Append("		PICKUP_MASK					,").AppendLine();
			sSql.Append("		REGIST_DATE					,").AppendLine();
			sSql.Append("		LAST_LOGIN_DATE					,").AppendLine();
			sSql.Append("		USER_UPDATE_DATE			,").AppendLine();
			sSql.Append("		LAST_ACTION_DATE			,").AppendLine();
			sSql.Append("		TOTAL_PAYMENT_AMT			,").AppendLine();
			sSql.Append("		DUMMY_TALK_FLAG				,").AppendLine();
			sSql.Append("		BANK_NM						,").AppendLine();
			sSql.Append("		BANK_OFFICE_NM				,").AppendLine();
			sSql.Append("		BANK_OFFICE_KANA_NM			,").AppendLine();
			sSql.Append("		BANK_ACCOUNT_TYPE			,").AppendLine();
			sSql.Append("		BANK_ACCOUNT_NO				,").AppendLine();
			sSql.Append("		BANK_ACCOUNT_HOLDER_NM		,").AppendLine();
			sSql.Append("		BEFORE_SYSTEM_ID			").AppendLine();
			sSql.Append("		FROM(						").AppendLine();
			sSql.Append("			SELECT P.* ,NVL(LU.DUMMY_TALK_FLAG, 0) DUMMY_TALK_FLAG").AppendLine();
			sSql.Append("			FROM (SELECT P.*, GET_NOT_PAYMENT_AMT(P.USER_SEQ) AS NOT_PAYMENT_AMT FROM VW_CAST_CHARACTER00 P) P ").AppendLine();

			string[] sCastAttrValue = { pCastAttrValue1,pCastAttrValue2,pCastAttrValue3,pCastAttrValue4 };
			for (int i = 0;i < ViCommConst.MAX_ADMIN_CAST_INQUIRY_ITEMS;i++) {
				if (!string.IsNullOrEmpty(sCastAttrValue[i])) {
					sSql.Append(string.Format(",VW_CAST_ATTR_VALUE01 AT{0} ",i + 1));
				}
			}
			if (!string.IsNullOrEmpty(pPickupId)) {
				SetPickupTable(pSiteCd,pPickupId,ref sSql);
			}
			if (!string.IsNullOrEmpty(pAdCd)) {
				sSql.Append(",T_SITE_USER SU ");
			}

			sSql.Append(",T_LOGIN_USER LU ");

			sSql.Append(sWhere);
			sSql.Append(") ").AppendLine();
			sSql.AppendLine(this.GetInnerOrder(pSortExpression,pSortDirection)).AppendLine();
			sSql.Append("	)INNER WHERE ROWNUM <= :MAX_ROW ").AppendLine();
			sSql.Append(") T1								,").AppendLine();
			sSql.Append("T_SITE_USER            			,").AppendLine();
			sSql.Append("T_AD            					,").AppendLine();
			sSql.Append("T_ACT_CATEGORY         			,").AppendLine();
			sSql.Append("T_MANAGER              			,").AppendLine();
			sSql.Append("T_PRODUCTION           			,").AppendLine();
			sSql.Append("T_SITE                 			,").AppendLine();
			sSql.Append("T_CODE_DTL  		T_CODE_DTL1		,").AppendLine();
			sSql.Append("T_CODE_DTL  		T_CODE_DTL2		,").AppendLine();
			sSql.Append("T_CODE_DTL  		T_CODE_DTL3		,").AppendLine();
			sSql.Append("T_CODE_DTL  		T_CODE_DTL4		").AppendLine();
			sSql.Append("WHERE								").AppendLine();
			sSql.Append("	RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW AND ").AppendLine();
			sSql.Append("	T1.PRODUCTION_SEQ					= T_MANAGER.PRODUCTION_SEQ				AND").AppendLine();
			sSql.Append("	T1.MANAGER_SEQ						= T_MANAGER.MANAGER_SEQ					AND").AppendLine();
			sSql.Append("	T1.SITE_CD							= T_SITE.SITE_CD						AND").AppendLine();
			sSql.Append("	T_MANAGER.PRODUCTION_SEQ			= T_PRODUCTION.PRODUCTION_SEQ			AND").AppendLine();
			sSql.Append("	T1.SITE_CD							= T_SITE_USER.SITE_CD				(+)	AND").AppendLine();
			sSql.Append("	T1.USER_SEQ							= T_SITE_USER.USER_SEQ				(+)	AND").AppendLine();
			sSql.Append("	T1.USER_CHAR_NO						= T_SITE_USER.USER_CHAR_NO			(+)	AND").AppendLine();
			sSql.Append("	T_SITE_USER.AD_CD					= T_AD.AD_CD						(+)	AND").AppendLine();
			sSql.Append("	T1.SITE_CD							= T_ACT_CATEGORY.SITE_CD			(+)	AND").AppendLine();
			sSql.Append("	T1.ACT_CATEGORY_SEQ					= T_ACT_CATEGORY.ACT_CATEGORY_SEQ	(+)	AND").AppendLine();
			sSql.Append("	'51'								= T_CODE_DTL1.CODE_TYPE				(+)	AND").AppendLine();
			sSql.Append("	T1.USER_STATUS						= T_CODE_DTL1.CODE					(+)	AND").AppendLine();
			sSql.Append("	'52'								= T_CODE_DTL2.CODE_TYPE				(+)	AND").AppendLine();
			sSql.Append("	T1.CHARACTER_ONLINE_STATUS			= T_CODE_DTL2.CODE					(+)	AND").AppendLine();
			sSql.Append("	'61'								= T_CODE_DTL3.CODE_TYPE				(+)	AND").AppendLine();
			sSql.Append("	T1.CONNECT_TYPE						= T_CODE_DTL3.CODE					(+)	AND").AppendLine();
			sSql.Append("	'55'								= T_CODE_DTL4.CODE_TYPE				(+)	AND").AppendLine();
			sSql.Append("	T1.USE_TERMINAL_TYPE				= T_CODE_DTL4.CODE					(+)	");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				cmd.BindByName = true;
				for (int i = 0;i < oParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)oParms[i]);
				}
				cmd.Parameters.Add("MAX_ROW",startRowIndex + maximumRows);
				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_CHARACTER00");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private string GetInnerOrder(string pSortExpression,string pSortDirection) {
		if (string.IsNullOrEmpty(pSortExpression) || string.IsNullOrEmpty(pSortDirection)) {
			return "ORDER BY REGIST_DATE DESC";
		}

		List<string> oT1List = new List<string>(new string[] { "ACT_CATEGORY_NM","AD_CD","CONNECT_TYPE_NM","FAVORITE_COUNT","LAST_PAYMENT_DATE","MANAGER_NM","NOT_PAYMENT_AMT","ONLINE_STATUS_NM","PRODUCTION_NM","PROFILE_PIC_SEQ","SITE_NM","SMALL_PHOTO_IMG_PATH","SYS_ONLINE_IMG_PATH","TALK_TERM","UPDATE_DATE","USER_STATUS_NM","USE_OTHER_SYS_INFO_FLAG","USE_TERMINAL_TYPE_NM" });

		return string.Format("ORDER BY {0} REGIST_DATE DESC",
			oT1List.Contains(pSortExpression) ? string.Empty : string.Format("{0} {1}, ",pSortExpression,pSortDirection));
	}

	private OracleParameter[] CreateWhere(
		string pOnline,
		string pLoginId,
		string pProductionSeq,
		string pCastNm,
		string pSiteCd,
		string pConnectType,
		string pTerminalType,
		string pHandleNm,
		string pActCategorySeq,
		string pEmailAddr,
		string pPicType,
		int pNonExistMailAddrFlagMask,
		string pMainCharOnlyFlag,
		string pManagerSeq,
		string pRegistDayFrom,
		string pRegistDayTo,
		string pLastLoginDayFrom,
		string pLastLoginDayTo,
		
		string pFinalLoginDayFrom,
		string pFinalLoginDayTo,
		
		string pTotalPaymentAmtFrom,
		string pTotalPaymentAmtTo,
		string pNaFlag,
		string pUserRank,
		string pAdCd,
		string pTel,
		string pCastAttrValue1,
		string pCastAttrValue2,
		string pCastAttrValue3,
		string pCastAttrValue4,
		string pAgeFrom,
		string pAgeTo,
		string pFavoritMeCountFrom,
		string pFavoritMeCountTo,
		int pUserStatusMask,
		string pPickupId,
		string pGuid,
		string pRemarks1,
		string pEnabledBlogFlag,
		string pEnabledRichinoFlag,
		string pSiteUseStatus,
		string pInfoMailRxType,
		string pUserMailRxType,
		string pReportAffiliateDateFrom,
		string pReportAffiliateDateTo,
		string pUserDefineMask,
		string pExceptRefusedByManUserSeq,
		string pLastTxMailDateFrom,
		string pLastTxMailDateTo,
		string pGameHandleNm,
		string pGameCharacterType,
		string pGameRegistDateFrom,
		string pGameRegistDateTo,
		string pGameCharacterLevelFrom,
		string pGameCharacterLevelTo,
		string pGamePointFrom,
		string pGamePointTo,
		string pCastGamePointFrom,
		string pCastGamePointTo,
		bool pHandleNmExactFlag,
		string pBeforeSystemId,
		string pKeyword,
		int pCarrier,
		bool pLoginIdNegativeFlag,
		bool pLastActionDayNegativeFlag,
		bool pUserDefineMaskNegatieFlag,
		bool pIntroducerFriendAllFlag,
		string pCrosmileLastUsedVersionFrom,
		string pCrosmileLastUsedVersionTo,
		string pUseCrosmileFlag,
		string pGuardianTel,
		string pUserSeq,
		string pUseVoiceappFlag,
		string pGcappLastLoginVersionFrom,
		string pGcappLastLoginVersionTo,
		string pUseJealousyFlag,
		bool pExcludeEmailAddr,
		string pTxMuller3NgMailAddrFlag,
		string pNotPaymentAmtFrom,
		string pNotPaymentAmtTo,
		ref string pWhere
	) {
		pWhere = "";

		ArrayList list = new ArrayList();

		string[] sValues = new string[6];
		int iCnt = 0;
		string sWhere = string.Empty;

		iCnt = 0;
		sWhere = string.Empty;
		if (pUserStatusMask != 0) {
			if ((pUserStatusMask & ViCommConst.MASK_WOMAN_AUTH_WAIT) != 0) {
				sValues[iCnt++] = ViCommConst.USER_WOMAN_AUTH_WAIT;
			}
			if ((pUserStatusMask & ViCommConst.MASK_WOMAN_NORMAL) != 0) {
				sValues[iCnt++] = ViCommConst.USER_WOMAN_NORMAL;
			}
			if ((pUserStatusMask & ViCommConst.MASK_WOMAN_STOP) != 0) {
				sValues[iCnt++] = ViCommConst.USER_WOMAN_STOP;
			}
			if ((pUserStatusMask & ViCommConst.MASK_WOMAN_HOLD) != 0) {
				sValues[iCnt++] = ViCommConst.USER_WOMAN_HOLD;
			}
			if ((pUserStatusMask & ViCommConst.MASK_WOMAN_RESIGNED) != 0) {
				sValues[iCnt++] = ViCommConst.USER_WOMAN_RESIGNED;
			}
			if ((pUserStatusMask & ViCommConst.MASK_WOMAN_BAN) != 0) {
				sValues[iCnt++] = ViCommConst.USER_WOMAN_BAN;
			}
			sWhere = "(P.USER_STATUS IN (";
			for (int i = 0;i < iCnt;i++) {
				sWhere = sWhere + ":USER_STATUS" + i.ToString() + ",";
				list.Add(new OracleParameter("USER_STATUS" + i.ToString(),int.Parse(sValues[i])));
			}
			sWhere = sWhere.Substring(0,sWhere.Length - 1) + ")) ";
			SysPrograms.SqlAppendWhere(sWhere,ref pWhere);
		}

		iCnt = 0;
		if (pCarrier != 0) {
			if ((pCarrier & ViCommConst.MASK_DOCOMO) != 0) {
				sValues[iCnt++] = ViCommConst.DOCOMO;
			}
			if ((pCarrier & ViCommConst.MASK_KDDI) != 0) {
				sValues[iCnt++] = ViCommConst.KDDI;
			}
			if ((pCarrier & ViCommConst.MASK_SOFTBANK) != 0) {
				sValues[iCnt++] = ViCommConst.SOFTBANK;
			}
			if ((pCarrier & ViCommConst.MASK_ANDROID) != 0) {
				sValues[iCnt++] = ViCommConst.ANDROID;
			}
			if ((pCarrier & ViCommConst.MASK_IPHONE) != 0) {
				sValues[iCnt++] = ViCommConst.IPHONE;
			}
			sWhere = "(P.MOBILE_CARRIER_CD IN (";
			for (int i = 0; i < iCnt; i++) {
				sWhere = sWhere + ":MOBILE_CARRIER_CD" + i.ToString() + ",";
				list.Add(new OracleParameter("MOBILE_CARRIER_CD" + i.ToString(),sValues[i]));
			}
			sWhere = sWhere.Substring(0,sWhere.Length - 1) + ")) ";
			SysPrograms.SqlAppendWhere(sWhere,ref pWhere);
		}

		if (pOnline.Equals("") == false) {
			string[] sOnlineStatusArray = pOnline.Split(new char[] { ',' });
			if (sOnlineStatusArray.Length > 1) {
				StringBuilder sOnlineStatusExp = new StringBuilder();
				StringBuilder sOnlineInExp = new StringBuilder();
				StringBuilder sDummyExp = new StringBuilder();

				sOnlineStatusExp.Append("( ");

				if (!(pOnline.Contains(ViCommConst.USER_TALKING.ToString())
					&& pOnline.Contains(ViCommConst.USER_DUMMY_TALKING.ToString())
					&& sOnlineStatusArray.Length == 2)) {
					sOnlineInExp.Append(" P.CHARACTER_ONLINE_STATUS IN (");
					string sCanma = string.Empty;
					for (int i = 0;i < sOnlineStatusArray.Length;i++) {
						if (sOnlineStatusArray[i].Equals(ViCommConst.USER_DUMMY_TALKING.ToString()) ||
							sOnlineStatusArray[i].Equals(ViCommConst.USER_TALKING.ToString())) {
							continue;
						}
						sOnlineInExp.Append(sCanma);

						string sParamName = string.Format(":CHARACTER_ONLINE_STATUS{0}",i);
						sOnlineInExp.AppendFormat(sParamName);
						list.Add(new OracleParameter(sParamName,sOnlineStatusArray[i]));
						sCanma = ",";

					}
					sOnlineInExp.Append(")");
				}

				if (pOnline.Contains(ViCommConst.USER_DUMMY_TALKING.ToString()) ||
					pOnline.Contains(ViCommConst.USER_TALKING.ToString())) {
					sDummyExp.Append(" ( ");
					bool bDummyTalk = false;
					if (pOnline.Contains(ViCommConst.USER_DUMMY_TALKING.ToString())) {
						sDummyExp.Append("(P.CHARACTER_ONLINE_STATUS = :DUMMY_TALK AND LU.DUMMY_TALK_FLAG = :DUMMY_TALK_FLAG )");
						list.Add(new OracleParameter(":DUMMY_TALK",ViCommConst.USER_TALKING));
						list.Add(new OracleParameter(":DUMMY_TALK_FLAG",ViCommConst.FLAG_ON_STR));
						bDummyTalk = true;
					}
					if (pOnline.Contains(ViCommConst.USER_TALKING.ToString())) {

						if (bDummyTalk) {
							sDummyExp.Append(" OR ");
						}

						sDummyExp.Append("(P.CHARACTER_ONLINE_STATUS = :TALKING AND LU.DUMMY_TALK_FLAG = :TALKING_FLAG )");
						list.Add(new OracleParameter(":TALKING",ViCommConst.USER_TALKING));
						list.Add(new OracleParameter(":TALKING_FLAG",ViCommConst.FLAG_OFF_STR));
					}
					sDummyExp.Append(" ) ");
				}

				if (sDummyExp.ToString().Equals(string.Empty) || sOnlineInExp.ToString().Equals(string.Empty)) {
					sOnlineStatusExp.AppendFormat(" {0} {1} ",sOnlineInExp.ToString(),sDummyExp.ToString());
				} else {
					sOnlineStatusExp.AppendFormat("{0} OR {1}",sOnlineInExp.ToString(),sDummyExp.ToString());
				}
				sOnlineStatusExp.Append(")");

				SysPrograms.SqlAppendWhere(sOnlineStatusExp.ToString(),ref pWhere);

			} else {
				switch (pOnline) {
					case "#":
						SysPrograms.SqlAppendWhere("P.CHARACTER_ONLINE_STATUS = :CHARACTER_ONLINE_STATUS",ref pWhere);
						list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS",ViCommConst.USER_LOGINED));
						break;

					case "*":
						SysPrograms.SqlAppendWhere("(P.CHARACTER_ONLINE_STATUS IN (:CHARACTER_ONLINE_STATUS1,:CHARACTER_ONLINE_STATUS2))",ref pWhere);
						list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS1",ViCommConst.USER_WAITING));
						list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS2",ViCommConst.USER_TALKING));
						break;

					default:
						SysPrograms.SqlAppendWhere("P.CHARACTER_ONLINE_STATUS = :CHARACTER_ONLINE_STATUS",ref pWhere);
						string sOnline = pOnline;
						if (sOnline.Equals(ViCommConst.USER_DUMMY_TALKING.ToString())) {
							sOnline = ViCommConst.USER_TALKING.ToString();
						}
						list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS",sOnline));

						if (pOnline.Equals(ViCommConst.USER_DUMMY_TALKING.ToString())) {
							SysPrograms.SqlAppendWhere("LU.DUMMY_TALK_FLAG = :DUMMY_TALK_FLAG",ref pWhere);
							list.Add(new OracleParameter(":DUMMY_TALK_FLAG",ViCommConst.FLAG_ON_STR));
						} else if (pOnline.Equals(ViCommConst.USER_TALKING.ToString())) {
							SysPrograms.SqlAppendWhere("LU.DUMMY_TALK_FLAG = :DUMMY_TALK_FLAG",ref pWhere);
							list.Add(new OracleParameter(":DUMMY_TALK_FLAG",ViCommConst.FLAG_OFF_STR));
						}
						break;
				}
			}
		}

		if (pSiteCd.Equals("") == false) {
			SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (pActCategorySeq.Equals("") == false) {
			SysPrograms.SqlAppendWhere("P.ACT_CATEGORY_SEQ = :ACT_CATEGORY_SEQ",ref pWhere);
			list.Add(new OracleParameter("ACT_CATEGORY_SEQ",pActCategorySeq));
		}


		if (!pLoginId.Equals("")) {
			string sLoginQuery = string.Empty;

			if (Regex.IsMatch(pLoginId,",|\r?\n")) {
				string sConjunction = "OR ";
				string[] sLoginIdArray = Regex.Split(pLoginId,",|\r?\n");
				StringBuilder oLoginIdWhere = new StringBuilder();
				for (int iIndex = 0;iIndex < sLoginIdArray.Length;iIndex++) {
					if (string.IsNullOrEmpty(sLoginIdArray[iIndex].Trim())) {
						continue;
					}
					oLoginIdWhere.AppendFormat("P.LOGIN_ID = :LOGIN_ID{0} OR ",iIndex);
					list.Add(new OracleParameter(string.Concat("LOGIN_ID",iIndex),sLoginIdArray[iIndex].Trim()));
				}

				if (oLoginIdWhere.Length > 0) {
					sLoginQuery = string.Format("{0} ({1})",
						this.GetNegative(pLoginIdNegativeFlag),
						oLoginIdWhere.Remove(oLoginIdWhere.Length - sConjunction.Length,sConjunction.Length));
					SysPrograms.SqlAppendWhere(sLoginQuery,ref pWhere);
				}
			} else {
				sLoginQuery = string.Format("P.LOGIN_ID {0} LIKE :LOGIN_ID||'%'",this.GetNegative(pLoginIdNegativeFlag));
				SysPrograms.SqlAppendWhere(sLoginQuery,ref pWhere);
				list.Add(new OracleParameter("LOGIN_ID",pLoginId));
			}
		}

		if (!string.IsNullOrEmpty(pUserSeq)) {
			SysPrograms.SqlAppendWhere("P.USER_SEQ = :USER_SEQ",ref pWhere);
			list.Add(new OracleParameter("USER_SEQ",pUserSeq));
		}

		if (pProductionSeq.Equals("") == false) {
			SysPrograms.SqlAppendWhere("P.PRODUCTION_SEQ = :PRODUCTION_SEQ",ref pWhere);
			list.Add(new OracleParameter("PRODUCTION_SEQ",pProductionSeq));
		}

		if (pCastNm.Equals("") == false) {
			SysPrograms.SqlAppendWhere("REPLACE(REPLACE(P.CAST_NM, '　', NULL), ' ', NULL) LIKE '%' || :CAST_NM || '%'",ref pWhere);
			list.Add(new OracleParameter("CAST_NM",pCastNm));
		}

		if (pHandleNm.Equals("") == false) {
			if (!pHandleNmExactFlag) {
				SysPrograms.SqlAppendWhere("P.HANDLE_NM LIKE '%' || :HANDLE_NM || '%'", ref pWhere);
			} else {
				SysPrograms.SqlAppendWhere("P.HANDLE_NM = :HANDLE_NM", ref pWhere);
			}
			list.Add(new OracleParameter("HANDLE_NM",pHandleNm));
		}

		if (pConnectType.Equals("") == false) {
			SysPrograms.SqlAppendWhere("P.CONNECT_TYPE = :CONNECT_TYPE",ref pWhere);
			list.Add(new OracleParameter("CONNECT_TYPE",pConnectType));
		}

		if (pTerminalType.Equals("") == false) {
			SysPrograms.SqlAppendWhere("P.USE_TERMINAL_TYPE = :USE_TERMINAL_TYPE",ref pWhere);
			list.Add(new OracleParameter("USE_TERMINAL_TYPE",pTerminalType));
		}

		// メールアドレス
		if (Regex.IsMatch(pEmailAddr,@",|\s")) {
			string sConjunction = "OR ";
			string[] sEmailAddrArray = Regex.Split(pEmailAddr,@",|\s");
			StringBuilder oEmailAddrWhere = new StringBuilder();
			for (int iIndex = 0;iIndex < sEmailAddrArray.Length;iIndex++) {
				if (string.IsNullOrEmpty(sEmailAddrArray[iIndex].Trim())) {
					continue;
				}
				oEmailAddrWhere.AppendFormat("P.EMAIL_ADDR LIKE '%' || :EMAIL_ADDR{0} || '%' OR ",iIndex);
				list.Add(new OracleParameter(string.Concat("EMAIL_ADDR",iIndex),sEmailAddrArray[iIndex].Trim().ToLower()));
			}

			if (oEmailAddrWhere.Length > 0) {
				SysPrograms.SqlAppendWhere(
					string.Format(
						"{0} ({1})"
						,this.GetNegative(pExcludeEmailAddr)
						,oEmailAddrWhere.Remove(oEmailAddrWhere.Length - sConjunction.Length,sConjunction.Length)
					)
					,ref pWhere
				);
			}
		} else {
			SysPrograms.SqlAppendWhere(
				string.Format(
					"P.EMAIL_ADDR {0} LIKE '%' || :EMAIL_ADDR || '%'"
					,this.GetNegative(pExcludeEmailAddr)
				)
				,ref pWhere
			);
			list.Add(new OracleParameter("EMAIL_ADDR",pEmailAddr.ToLower()));
		}

		if (pPicType.Equals("") == false) {
			SysPrograms.SqlAppendWhere("P.PIC_TYPE = :PIC_TYPE",ref pWhere);
			list.Add(new OracleParameter("PIC_TYPE",pPicType));
		}

		iCnt = 0;
		if (pNonExistMailAddrFlagMask != 0) {
			if ((pNonExistMailAddrFlagMask & ViCommConst.MSK_MAIL_ADDR_OK) != 0) {
				sValues[iCnt++] = ViCommConst.MAIL_ADDR_OK.ToString();
			}
			if ((pNonExistMailAddrFlagMask & ViCommConst.MSK_MAIL_ADDR_FILTERING_ERROR) != 0) {
				sValues[iCnt++] = ViCommConst.MAIL_ADDR_FILTERING_ERROR.ToString();
			}
			if ((pNonExistMailAddrFlagMask & ViCommConst.MSK_MAIL_ADDR_NG) != 0) {
				sValues[iCnt++] = ViCommConst.MAIL_ADDR_NG.ToString();
			}
			sWhere = "(P.NON_EXIST_MAIL_ADDR_FLAG IN (";
			for (int i = 0;i < iCnt;i++) {
				sWhere = sWhere + ":NON_EXIST_MAIL_ADDR_FLAG" + i.ToString() + ",";
				list.Add(new OracleParameter("NON_EXIST_MAIL_ADDR_FLAG" + i.ToString(),sValues[i]));
			}
			sWhere = sWhere.Substring(0,sWhere.Length - 1) + ")) ";
			SysPrograms.SqlAppendWhere(sWhere,ref pWhere);
		}
		
		if (!string.IsNullOrEmpty(pTxMuller3NgMailAddrFlag)) {
			SysPrograms.SqlAppendWhere("EXISTS(SELECT * FROM T_USER WHERE USER_SEQ = P.USER_SEQ AND TX_MULLER3_NG_MAIL_ADDR_FLAG = :TX_MULLER3_NG_MAIL_ADDR_FLAG)",ref pWhere);
			list.Add(new OracleParameter("TX_MULLER3_NG_MAIL_ADDR_FLAG",pTxMuller3NgMailAddrFlag));
		}

		if (pMainCharOnlyFlag.Equals("1")) {
			SysPrograms.SqlAppendWhere("P.USER_CHAR_NO = :USER_CHAR_NO",ref pWhere);
			list.Add(new OracleParameter("USER_CHAR_NO",ViCommConst.MAIN_CHAR_NO));
		} else if (pMainCharOnlyFlag.Equals("0")) {
			SysPrograms.SqlAppendWhere("P.USER_CHAR_NO > :USER_CHAR_NO",ref pWhere);
			list.Add(new OracleParameter("USER_CHAR_NO",ViCommConst.MAIN_CHAR_NO));
		}
		if (!string.IsNullOrEmpty(pManagerSeq)) {
			SysPrograms.SqlAppendWhere("P.MANAGER_SEQ = :MANAGER_SEQ",ref pWhere);
			list.Add(new OracleParameter("MANAGER_SEQ",pManagerSeq));
		}

		if (!pGuid.Equals("")) {
			SysPrograms.SqlAppendWhere("P.IMODE_ID = :IMODE_ID",ref pWhere);
			list.Add(new OracleParameter("IMODE_ID",pGuid));
		}

		if (!string.IsNullOrEmpty(pBeforeSystemId)) {
			SysPrograms.SqlAppendWhere("P.BEFORE_SYSTEM_ID LIKE :BEFORE_SYSTEM_ID || '%'", ref pWhere);
			list.Add(new OracleParameter("BEFORE_SYSTEM_ID", pBeforeSystemId.TrimEnd()));
		}



		if (pNaFlag.Equals(ViCommConst.WITHOUT)) {
			//条件に含めない 
		} else
			if (!pNaFlag.Equals(string.Empty)) {
				// 画面の条件と合致せず、紛らわしいのでｺﾒﾝﾄｱｳﾄ 
				StringBuilder oClauseBuilder = new StringBuilder();

				if (!pWhere.Equals(string.Empty)) {
					oClauseBuilder.Append(" AND ");
				}
				oClauseBuilder.Append(" P.NA_FLAG IN (");
				int iNum = 1;
				foreach (string sNaFlag in pNaFlag.Split(',')) {
					string sParamName = string.Format(":NA_FLAG{0}",iNum);

					if (iNum != 1)
						oClauseBuilder.Append(",");
					oClauseBuilder.Append(sParamName);
					list.Add(new OracleParameter(sParamName,sNaFlag));

					iNum += 1;
				}
				oClauseBuilder.Append(" )").AppendLine();

				pWhere += oClauseBuilder.ToString();
			} else {
			}

		if (!pRegistDayFrom.Equals("")) {
			DateTime dtFrom = DateTime.Parse(pRegistDayFrom + " " + "00:00:00");
			DateTime dtTo = DateTime.Parse(pRegistDayTo + " " + "23:59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere("(P.REGIST_DATE >= :REGIST_DATE_FROM AND P.REGIST_DATE < :REGIST_DATE_TO)",ref pWhere);
			list.Add(new OracleParameter("REGIST_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("REGIST_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}
		//最終ログイン
		if (!pFinalLoginDayFrom.Equals("")) {
			DateTime dtFrom = DateTime.Parse(pFinalLoginDayFrom + " " + "00:00:00");
			DateTime dtTo = DateTime.Parse(pFinalLoginDayTo + " " + "23:59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere("(P.LAST_LOGIN_DATE >= :LAST_LOGIN_DATE_FROM AND P.LAST_LOGIN_DATE < :LAST_LOGIN_DATE_TO)",ref pWhere);
			list.Add(new OracleParameter("LAST_LOGIN_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("LAST_LOGIN_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}
		
		if (!pLastLoginDayFrom.Equals("")) {
			string sLastLoginDayQuery = string.Format("({0}(P.LAST_ACTION_DATE >= :LAST_ACTION_DATE_FROM AND P.LAST_ACTION_DATE < :LAST_ACTION_DATE_TO){1})",
				this.GetNegative(pLastActionDayNegativeFlag),
				pLastActionDayNegativeFlag ? " OR P.LAST_ACTION_DATE IS NULL" : string.Empty);
			SysPrograms.SqlAppendWhere(sLastLoginDayQuery,ref pWhere);
			DateTime dtFrom = DateTime.Parse(pLastLoginDayFrom + " " + "00:00:00");
			DateTime dtTo = DateTime.Parse(pLastLoginDayTo + " " + "23:59:59").AddSeconds(1);
			list.Add(new OracleParameter("LAST_ACTION_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("LAST_ACTION_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (!pTotalPaymentAmtFrom.Equals("")) {
			SysPrograms.SqlAppendWhere("(P.TOTAL_PAYMENT_AMT >= :TOTAL_PAYMENT_AMT_FROM AND P.TOTAL_PAYMENT_AMT <= :TOTAL_PAYMENT_AMT_TO)",ref pWhere);
			list.Add(new OracleParameter("TOTAL_PAYMENT_AMT_FROM",int.Parse(pTotalPaymentAmtFrom)));
			list.Add(new OracleParameter("TOTAL_PAYMENT_AMT_TO",int.Parse(pTotalPaymentAmtTo)));
		}
		if (!pUserRank.Equals("")) {
			SysPrograms.SqlAppendWhere("P.USER_RANK = :USER_RANK",ref pWhere);
			list.Add(new OracleParameter("USER_RANK",pUserRank));
		}
		if (!pTel.Equals("")) {
			SysPrograms.SqlAppendWhere("P.TEL LIKE :TEL",ref pWhere);
			list.Add(new OracleParameter("TEL",pTel + "%"));
		}

		if (!pAgeFrom.Equals("")) {
			SysPrograms.SqlAppendWhere("P.AGE >= :AGE_FROM",ref pWhere);
			list.Add(new OracleParameter("AGE_FROM",int.Parse(pAgeFrom)));
		}

		if (!pAgeTo.Equals("")) {
			SysPrograms.SqlAppendWhere("P.AGE <= :AGE_TO",ref pWhere);
			list.Add(new OracleParameter("AGE_TO",int.Parse(pAgeTo)));
		}

		if (!pFavoritMeCountFrom.Equals("")) {
			SysPrograms.SqlAppendWhere("P.FAVORIT_ME_COUNT >= :FAVORIT_ME_COUNT_FROM",ref pWhere);
			list.Add(new OracleParameter("FAVORIT_ME_COUNT_FROM",int.Parse(pFavoritMeCountFrom)));
		}

		if (!pFavoritMeCountTo.Equals("")) {
			SysPrograms.SqlAppendWhere("P.FAVORIT_ME_COUNT <= :FAVORIT_ME_COUNT_TO",ref pWhere);
			list.Add(new OracleParameter("FAVORIT_ME_COUNT_TO",int.Parse(pFavoritMeCountTo)));
		}

		if (!pCrosmileLastUsedVersionFrom.Equals("")) {
			SysPrograms.SqlAppendWhere("(P.CROSMILE_LAST_USED_VERSION >= :LAST_USED_VERSION_FROM AND P.CROSMILE_LAST_USED_VERSION <= :LAST_USED_VERSION_TO)",ref pWhere);
			list.Add(new OracleParameter("LAST_USED_VERSION_FROM",pCrosmileLastUsedVersionFrom));
			list.Add(new OracleParameter("LAST_USED_VERSION_TO",pCrosmileLastUsedVersionTo));
		}

		if (!string.IsNullOrEmpty(pUseCrosmileFlag)) {
			SysPrograms.SqlAppendWhere("P.USE_CROSMILE_FLAG = :USE_CROSMILE_FLAG",ref pWhere);
			list.Add(new OracleParameter("USE_CROSMILE_FLAG",pUseCrosmileFlag));
		}

		if (!pGuardianTel.Equals("")) {
			SysPrograms.SqlAppendWhere("P.GUARDIAN_TEL LIKE :GUARDIAN_TEL",ref pWhere);
			list.Add(new OracleParameter("GUARDIAN_TEL",pGuardianTel + "%"));
		}

		if (!string.IsNullOrEmpty(pUseVoiceappFlag)) {
			SysPrograms.SqlAppendWhere("P.USE_VOICEAPP_FLAG = :USE_VOICEAPP_FLAG",ref pWhere);
			list.Add(new OracleParameter("USE_VOICEAPP_FLAG",pUseVoiceappFlag));
		}

		if (!pGcappLastLoginVersionFrom.Equals("")) {
			SysPrograms.SqlAppendWhere("(P.GCAPP_LAST_LOGIN_VERSION >= :GCAPP_LAST_LOGIN_VERSION_FROM AND P.GCAPP_LAST_LOGIN_VERSION <= :GCAPP_LAST_LOGIN_VERSION_TO)",ref pWhere);
			list.Add(new OracleParameter("GCAPP_LAST_LOGIN_VERSION_FROM",pGcappLastLoginVersionFrom));
			list.Add(new OracleParameter("GCAPP_LAST_LOGIN_VERSION_TO",pGcappLastLoginVersionTo));
		}

		if (!pRemarks1.Equals(string.Empty)) {
			SysPrograms.SqlAppendWhere("(P.REMARKS1 LIKE '%'||:REMARKS1||'%' OR P.REMARKS2 LIKE '%'||:REMARKS2||'%' OR P.REMARKS3 LIKE '%'||:REMARKS3||'%' OR P.REMARKS4 LIKE '%'||:REMARKS4||'%' OR P.REMARKS5 LIKE '%'||:REMARKS5||'%' OR P.REMARKS6 LIKE '%'||:REMARKS6||'%' OR P.REMARKS7 LIKE '%'||:REMARKS7||'%' OR P.REMARKS8 LIKE '%'||:REMARKS8||'%' OR P.REMARKS9 LIKE '%'||:REMARKS9||'%' OR P.REMARKS10 LIKE '%'||:REMARKS10||'%' )",ref pWhere);
			list.Add(new OracleParameter("REMARKS1",pRemarks1));
			list.Add(new OracleParameter("REMARKS2",pRemarks1));
			list.Add(new OracleParameter("REMARKS3",pRemarks1));
			list.Add(new OracleParameter("REMARKS4",pRemarks1));
			list.Add(new OracleParameter("REMARKS5",pRemarks1));
			list.Add(new OracleParameter("REMARKS6",pRemarks1));
			list.Add(new OracleParameter("REMARKS7",pRemarks1));
			list.Add(new OracleParameter("REMARKS8",pRemarks1));
			list.Add(new OracleParameter("REMARKS9",pRemarks1));
			list.Add(new OracleParameter("REMARKS10",pRemarks1));
		}

		if (!string.IsNullOrEmpty(pUserMailRxType)) {
			SysPrograms.SqlAppendWhere("P.USER_MAIL_RX_TYPE = :USER_MAIL_RX_TYPE",ref pWhere);
			list.Add(new OracleParameter("USER_MAIL_RX_TYPE",pUserMailRxType));
		}

		if (!string.IsNullOrEmpty(pInfoMailRxType)) {
			SysPrograms.SqlAppendWhere("P.INFO_MAIL_RX_TYPE = :INFO_MAIL_RX_TYPE",ref pWhere);
			list.Add(new OracleParameter("INFO_MAIL_RX_TYPE",pInfoMailRxType));
		}

		if (!string.IsNullOrEmpty(pUseJealousyFlag)) {
			StringBuilder oUseJealousyWhere = new StringBuilder();
			if (pUseJealousyFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
				oUseJealousyWhere.Append(" NOT ");
			}
			
			oUseJealousyWhere.AppendLine("EXISTS(																			");
			oUseJealousyWhere.AppendLine("	SELECT																			");
			oUseJealousyWhere.AppendLine("		1																			");
			oUseJealousyWhere.AppendLine("	FROM																			");
			oUseJealousyWhere.AppendLine("		T_FAVORIT																	");
			oUseJealousyWhere.AppendLine("	WHERE																			");
			oUseJealousyWhere.AppendLine("		SITE_CD								= P.SITE_CD							AND	");
			oUseJealousyWhere.AppendLine("		USER_SEQ							= P.USER_SEQ						AND	");
			oUseJealousyWhere.AppendLine("		USER_CHAR_NO						= P.USER_CHAR_NO					AND	");
			oUseJealousyWhere.AppendLine("		(																			");
			oUseJealousyWhere.AppendLine("			LOGIN_VIEW_STATUS				<> :LOGIN_VIEW_STATUS				OR	");
			oUseJealousyWhere.AppendLine("			WAITING_VIEW_STATUS				<> :WAITING_VIEW_STATUS				OR	");
			oUseJealousyWhere.AppendLine("			BBS_NON_PUBLISH_FLAG			<> :BBS_NON_PUBLISH_FLAG			OR	");
			oUseJealousyWhere.AppendLine("			BLOG_NON_PUBLISH_FLAG			<> :BLOG_NON_PUBLISH_FLAG			OR	");
			oUseJealousyWhere.AppendLine("			DIARY_NON_PUBLISH_FLAG			<> :DIARY_NON_PUBLISH_FLAG			OR	");
			oUseJealousyWhere.AppendLine("			PROFILE_PIC_NON_PUBLISH_FLAG	<> :PROFILE_PIC_NON_PUBLISH_FLAG	OR	");
			oUseJealousyWhere.AppendLine("			USE_COMMENT_DETAIL_FLAG			<> :USE_COMMENT_DETAIL_FLAG				");
			oUseJealousyWhere.AppendLine("		)																			");
			oUseJealousyWhere.AppendLine(")																					");
			
			SysPrograms.SqlAppendWhere(oUseJealousyWhere.ToString(),ref pWhere);
			
			list.Add(new OracleParameter("LOGIN_VIEW_STATUS",ViCommConst.FLAG_OFF_STR));
			list.Add(new OracleParameter("WAITING_VIEW_STATUS",ViCommConst.FLAG_OFF_STR));
			list.Add(new OracleParameter("BBS_NON_PUBLISH_FLAG",ViCommConst.FLAG_OFF_STR));
			list.Add(new OracleParameter("BLOG_NON_PUBLISH_FLAG",ViCommConst.FLAG_OFF_STR));
			list.Add(new OracleParameter("DIARY_NON_PUBLISH_FLAG",ViCommConst.FLAG_OFF_STR));
			list.Add(new OracleParameter("DIARY_NON_PUBLISH_FLAG",ViCommConst.FLAG_OFF_STR));
			list.Add(new OracleParameter("PROFILE_PIC_NON_PUBLISH_FLAG",ViCommConst.FLAG_OFF_STR));
			list.Add(new OracleParameter("USE_COMMENT_DETAIL_FLAG",ViCommConst.FLAG_OFF_STR));
		}


		DataSet ds = new DataSet();
		using (CastAttrType oCastAttrType = new CastAttrType()) {
			ds = oCastAttrType.GetInqAdminCastAttrType(pSiteCd);
		}
		string[] sCastAttrValue = { pCastAttrValue1,pCastAttrValue2,pCastAttrValue3,pCastAttrValue4 };
		for (int i = 0;i < ViCommConst.MAX_ADMIN_CAST_INQUIRY_ITEMS;i++) {
			if (!string.IsNullOrEmpty(sCastAttrValue[i])) {
				SysPrograms.SqlAppendWhere(string.Format("AT{0}.SITE_CD				= P.SITE_CD				 ",i + 1),ref pWhere);
				SysPrograms.SqlAppendWhere(string.Format("AT{0}.USER_SEQ			= P.USER_SEQ			 ",i + 1),ref pWhere);
				SysPrograms.SqlAppendWhere(string.Format("AT{0}.USER_CHAR_NO		= P.USER_CHAR_NO		 ",i + 1),ref pWhere);
				SysPrograms.SqlAppendWhere(string.Format("AT{0}.CAST_ATTR_TYPE_SEQ	= :CAST_ATTR_TYPE_SEQ{0} ",i + 1),ref pWhere);
				list.Add(new OracleParameter(string.Format("CAST_ATTR_TYPE_SEQ{0}",i + 1),ds.Tables[0].Rows[0][string.Format("CAST_ATTR_TYPE_SEQ{0}",i + 1)]));
				if (ds.Tables[0].Rows[0][string.Format("GROUPING_CATEGORY_CD{0}",i + 1)].ToString().Equals("")) {
					SysPrograms.SqlAppendWhere(string.Format("AT{0}.CAST_ATTR_SEQ	= :CAST_ATTR_SEQ{0}		 ",i + 1),ref pWhere);
					list.Add(new OracleParameter(string.Format("CAST_ATTR_SEQ{0}",i + 1),sCastAttrValue[i]));
				} else {
					SysPrograms.SqlAppendWhere(string.Format("AT{0}.GROUPING_CD		= :GROUPING_CD{0}		 ",i + 1),ref pWhere);
					list.Add(new OracleParameter(string.Format("GROUPING_CD{0}",i + 1),sCastAttrValue[i]));
				}
			}
		}

		if (!string.IsNullOrEmpty(pPickupId) && !string.IsNullOrEmpty(this.GetPickupType(pSiteCd,pPickupId))) {
			SysPrograms.SqlAppendWhere("PK.SITE_CD		= P.SITE_CD",ref pWhere);
			SysPrograms.SqlAppendWhere("PK.USER_SEQ		= P.USER_SEQ",ref pWhere);
			SysPrograms.SqlAppendWhere("PK.USER_CHAR_NO	= P.USER_CHAR_NO",ref pWhere);
			SysPrograms.SqlAppendWhere("PK.PICKUP_ID	= :PICKUP_ID",ref pWhere);
			SysPrograms.SqlAppendWhere("PK.PICKUP_FLAG	= :PICKUP_FLAG",ref pWhere);
			list.Add(new OracleParameter("PICKUP_ID",pPickupId));
			list.Add(new OracleParameter("PICKUP_FLAG",ViCommConst.FLAG_ON));
		}

		if (!string.IsNullOrEmpty(pAdCd)) {
			if (Regex.IsMatch(pAdCd, ",|\r?\n")) {
				string[] sAdDdArray = Regex.Split(pAdCd, ",|\r?\n");
				string sConjunction = string.Empty;
				StringBuilder oAdDdWhere = new StringBuilder();
				for (int iIndex = 0; iIndex < sAdDdArray.Length; iIndex++) {
					if (string.IsNullOrEmpty(sAdDdArray[iIndex].Trim())) {
						continue;
					}
					oAdDdWhere.AppendFormat("{0} :AD_CD{1} ", sConjunction, iIndex);
					list.Add(new OracleParameter(string.Concat("AD_CD", iIndex), sAdDdArray[iIndex].Trim()));
					sConjunction = ",";
				}

				if (oAdDdWhere.Length > 0) {
					SysPrograms.SqlAppendWhere("SU.SITE_CD      = P.SITE_CD", ref pWhere);
					SysPrograms.SqlAppendWhere("SU.USER_SEQ     = P.USER_SEQ", ref pWhere);
					SysPrograms.SqlAppendWhere("SU.USER_CHAR_NO = P.USER_CHAR_NO", ref pWhere);
					SysPrograms.SqlAppendWhere(string.Format("SU.AD_CD IN({0})", oAdDdWhere.ToString()), ref pWhere);
				}
			} else {
				SysPrograms.SqlAppendWhere("SU.SITE_CD      = P.SITE_CD", ref pWhere);
				SysPrograms.SqlAppendWhere("SU.USER_SEQ     = P.USER_SEQ", ref pWhere);
				SysPrograms.SqlAppendWhere("SU.USER_CHAR_NO = P.USER_CHAR_NO", ref pWhere);
				SysPrograms.SqlAppendWhere("SU.AD_CD        = :AD_CD", ref pWhere);
				list.Add(new OracleParameter("AD_CD", pAdCd));
			}
		}
		SysPrograms.SqlAppendWhere("LU.LOGIN_SEQ(+) = P.LOGIN_SEQ",ref pWhere);

		if (!string.IsNullOrEmpty(pEnabledBlogFlag)) {
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_CAST_CHARACTER_EX CCE WHERE CCE.SITE_CD = P.SITE_CD AND CCE.USER_SEQ = P.USER_SEQ AND CCE.USER_CHAR_NO = P.USER_CHAR_NO AND CCE.ENABLED_BLOG_FLAG = :ENABLED_BLOG_FLAG)",ref pWhere);
			list.Add(new OracleParameter("ENABLED_BLOG_FLAG",pEnabledBlogFlag));
		}

		if (!string.IsNullOrEmpty(pEnabledRichinoFlag)) {
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_CAST_CHARACTER_EX CCE WHERE CCE.SITE_CD = P.SITE_CD AND CCE.USER_SEQ = P.USER_SEQ AND CCE.USER_CHAR_NO = P.USER_CHAR_NO AND CCE.ENABLED_RICHINO_FLAG = :ENABLED_RICHINO_FLAG)",ref pWhere);
			list.Add(new OracleParameter("ENABLED_RICHINO_FLAG",pEnabledRichinoFlag));
		}

		if (!string.IsNullOrEmpty(pSiteUseStatus)) {
			SysPrograms.SqlAppendWhere(string.Format("EXISTS (SELECT 1 FROM T_CAST_CHARACTER_EX CCE WHERE CCE.SITE_CD = P.SITE_CD AND CCE.USER_SEQ = P.USER_SEQ AND CCE.USER_CHAR_NO = P.USER_CHAR_NO AND CCE.SITE_USE_STATUS IN ({0}))",pSiteUseStatus),ref pWhere);
		}
		
		if (!string.IsNullOrEmpty(pReportAffiliateDateFrom)) {
			DateTime dtFrom = DateTime.Parse(pReportAffiliateDateFrom + " " + "00:00:00");
			DateTime dtTo = DateTime.Parse(pReportAffiliateDateTo + " " + "23:59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_SITE_USER SU2 WHERE SU2.SITE_CD = P.SITE_CD AND SU2.USER_SEQ = P.USER_SEQ AND SU2.USER_CHAR_NO = P.USER_CHAR_NO AND SU2.REPORT_AFFILIATE_DATE >= :REPORT_AFFILIATE_DATE_FROM AND SU2.REPORT_AFFILIATE_DATE < :REPORT_AFFILIATE_DATE_TO)",ref pWhere);
			list.Add(new OracleParameter("REPORT_AFFILIATE_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("REPORT_AFFILIATE_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (!string.IsNullOrEmpty(pUserDefineMask)) {
			SysPrograms.SqlAppendWhere(this.GetNegative(pUserDefineMaskNegatieFlag) + "BITAND(P.USER_DEFINE_MASK, :USER_DEFINE_MASK) > 0",ref pWhere);
			list.Add(new OracleParameter("USER_DEFINE_MASK",pUserDefineMask));
		}

		if (!string.IsNullOrEmpty(pExceptRefusedByManUserSeq)) {
			SysPrograms.SqlAppendWhere("NOT EXISTS (SELECT 1 FROM T_REFUSE WHERE T_REFUSE.SITE_CD = P.SITE_CD AND T_REFUSE.USER_SEQ = :USER_SEQ AND T_REFUSE.USER_CHAR_NO = :USER_CHAR_NO AND T_REFUSE.PARTNER_USER_SEQ = P.USER_SEQ AND T_REFUSE.PARTNER_USER_CHAR_NO = P.USER_CHAR_NO )",ref pWhere);
			list.Add(new OracleParameter("USER_SEQ",pExceptRefusedByManUserSeq));
			list.Add(new OracleParameter("USER_CHAR_NO",ViCommConst.MAIN_CHAR_NO));
			SysPrograms.SqlAppendWhere("NOT EXISTS (SELECT 1 FROM T_REFUSE WHERE T_REFUSE.SITE_CD = P.SITE_CD AND T_REFUSE.USER_SEQ = P.USER_SEQ AND T_REFUSE.USER_CHAR_NO = P.USER_CHAR_NO AND T_REFUSE.PARTNER_USER_SEQ = :PARTNER_USER_SEQ AND T_REFUSE.PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO )",ref pWhere);
			list.Add(new OracleParameter("PARTNER_USER_SEQ",pExceptRefusedByManUserSeq));
			list.Add(new OracleParameter("PARTNER_USER_CHAR_NO",ViCommConst.MAIN_CHAR_NO));
		}

		if (!pLastTxMailDateFrom.Equals(string.Empty)) {
			DateTime dtFrom = DateTime.Parse(pLastTxMailDateFrom + " " + "00:00:00");
			DateTime dtTo = DateTime.Parse(pLastTxMailDateTo + " " + "23:59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere("(P.LAST_TX_MAIL_DATE >= :LAST_TX_MAIL_DATE_FROM AND P.LAST_TX_MAIL_DATE < :LAST_TX_MAIL_DATE_TO)", ref pWhere);
			list.Add(new OracleParameter("LAST_TX_MAIL_DATE_FROM", OracleDbType.Date, dtFrom, ParameterDirection.Input));
			list.Add(new OracleParameter("LAST_TX_MAIL_DATE_TO", OracleDbType.Date, dtTo, ParameterDirection.Input));
		}

		if (!string.IsNullOrEmpty(pGameHandleNm)) {
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_GAME_CHARACTER GC WHERE GC.SITE_CD = P.SITE_CD AND GC.USER_SEQ = P.USER_SEQ AND GC.USER_CHAR_NO = P.USER_CHAR_NO AND GC.GAME_HANDLE_NM LIKE '%' || :GAME_HANDLE_NM || '%')",ref pWhere);
			list.Add(new OracleParameter("GAME_HANDLE_NM",pGameHandleNm));
		}

		if (!string.IsNullOrEmpty(pGameCharacterType)) {
			SysPrograms.SqlAppendWhere(string.Format("EXISTS (SELECT 1 FROM T_GAME_CHARACTER GC WHERE GC.SITE_CD = P.SITE_CD AND GC.USER_SEQ = P.USER_SEQ AND GC.USER_CHAR_NO = P.USER_CHAR_NO AND GC.GAME_CHARACTER_TYPE IN ({0}))",pGameCharacterType),ref pWhere);
		}

		if (!string.IsNullOrEmpty(pGameRegistDateFrom) && !string.IsNullOrEmpty(pGameRegistDateTo)) {
			DateTime dtFrom = DateTime.Parse(pGameRegistDateFrom + " 00:00:00");
			DateTime dtTo = DateTime.Parse(pGameRegistDateTo + " 23:59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_GAME_CHARACTER GC WHERE GC.SITE_CD = P.SITE_CD AND GC.USER_SEQ = P.USER_SEQ AND GC.USER_CHAR_NO = P.USER_CHAR_NO AND GC.REGIST_DATE >= :GAME_REGIST_DATE_FROM AND GC.REGIST_DATE <= :GAME_REGIST_DATE_TO)",ref pWhere);
			list.Add(new OracleParameter("GAME_REGIST_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("GAME_REGIST_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		} else if (!string.IsNullOrEmpty(pGameRegistDateFrom)) {
			DateTime dtFrom = DateTime.Parse(pGameRegistDateFrom + " 00:00:00");
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_GAME_CHARACTER GC WHERE GC.SITE_CD = P.SITE_CD AND GC.USER_SEQ = P.USER_SEQ AND GC.USER_CHAR_NO = P.USER_CHAR_NO AND GC.REGIST_DATE >= :GAME_REGIST_DATE_FROM)",ref pWhere);
			list.Add(new OracleParameter("GAME_REGIST_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
		} else if (!string.IsNullOrEmpty(pGameRegistDateTo)) {
			DateTime dtTo = DateTime.Parse(pGameRegistDateTo + " 23:59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_GAME_CHARACTER GC WHERE GC.SITE_CD = P.SITE_CD AND GC.USER_SEQ = P.USER_SEQ AND GC.USER_CHAR_NO = P.USER_CHAR_NO AND GC.REGIST_DATE <= :GAME_REGIST_DATE_TO)",ref pWhere);
			list.Add(new OracleParameter("GAME_REGIST_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (!string.IsNullOrEmpty(pGameCharacterLevelFrom) && !string.IsNullOrEmpty(pGameCharacterLevelTo)) {
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_GAME_CHARACTER GC WHERE GC.SITE_CD = P.SITE_CD AND GC.USER_SEQ = P.USER_SEQ AND GC.USER_CHAR_NO = P.USER_CHAR_NO AND GC.GAME_CHARACTER_LEVEL >= :GAME_CHARACTER_LEVEL_FROM AND GC.GAME_CHARACTER_LEVEL <= :GAME_CHARACTER_LEVEL_TO)",ref pWhere);
			list.Add(new OracleParameter("GAME_CHARACTER_LEVEL_FROM",int.Parse(pGameCharacterLevelFrom)));
			list.Add(new OracleParameter("GAME_CHARACTER_LEVEL_TO",int.Parse(pGameCharacterLevelTo)));
		} else if (!string.IsNullOrEmpty(pGameCharacterLevelFrom)) {
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_GAME_CHARACTER GC WHERE GC.SITE_CD = P.SITE_CD AND GC.USER_SEQ = P.USER_SEQ AND GC.USER_CHAR_NO = P.USER_CHAR_NO AND GC.GAME_CHARACTER_LEVEL >= :GAME_CHARACTER_LEVEL_FROM)",ref pWhere);
			list.Add(new OracleParameter("GAME_CHARACTER_LEVEL_FROM",int.Parse(pGameCharacterLevelFrom)));
		} else if (!string.IsNullOrEmpty(pGameCharacterLevelTo)) {
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_GAME_CHARACTER GC WHERE GC.SITE_CD = P.SITE_CD AND GC.USER_SEQ = P.USER_SEQ AND GC.USER_CHAR_NO = P.USER_CHAR_NO AND GC.GAME_CHARACTER_LEVEL <= :GAME_CHARACTER_LEVEL_TO)",ref pWhere);
			list.Add(new OracleParameter("GAME_CHARACTER_LEVEL_TO",int.Parse(pGameCharacterLevelTo)));
		}

		if (!string.IsNullOrEmpty(pGamePointFrom) && !string.IsNullOrEmpty(pGamePointTo)) {
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_GAME_CHARACTER GC WHERE GC.SITE_CD = P.SITE_CD AND GC.USER_SEQ = P.USER_SEQ AND GC.USER_CHAR_NO = P.USER_CHAR_NO AND GC.GAME_POINT >= :GAME_POINT_FROM AND GC.GAME_POINT <= :GAME_POINT_TO)",ref pWhere);
			list.Add(new OracleParameter("GAME_POINT_FROM",pGamePointFrom));
			list.Add(new OracleParameter("GAME_POINT_TO",pGamePointTo));
		} else if (!string.IsNullOrEmpty(pGamePointFrom)) {
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_GAME_CHARACTER GC WHERE GC.SITE_CD = P.SITE_CD AND GC.USER_SEQ = P.USER_SEQ AND GC.USER_CHAR_NO = P.USER_CHAR_NO AND GC.GAME_POINT >= :GAME_POINT_FROM)",ref pWhere);
			list.Add(new OracleParameter("GAME_POINT_FROM",pGamePointFrom));
		} else if (!string.IsNullOrEmpty(pGamePointTo)) {
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_GAME_CHARACTER GC WHERE GC.SITE_CD = P.SITE_CD AND GC.USER_SEQ = P.USER_SEQ AND GC.USER_CHAR_NO = P.USER_CHAR_NO AND GC.GAME_POINT <= :GAME_POINT_TO)",ref pWhere);
			list.Add(new OracleParameter("GAME_POINT_TO",pGamePointTo));
		}

		if (!string.IsNullOrEmpty(pCastGamePointFrom) && !string.IsNullOrEmpty(pCastGamePointTo)) {
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_CAST_GAME_POINT CGP WHERE CGP.SITE_CD = P.SITE_CD AND CGP.USER_SEQ = P.USER_SEQ AND CGP.USER_CHAR_NO = P.USER_CHAR_NO AND CGP.CAST_GAME_POINT >= :CAST_GAME_POINT_FROM AND CGP.CAST_GAME_POINT <= :CAST_GAME_POINT_TO)",ref pWhere);
			list.Add(new OracleParameter("CAST_GAME_POINT_FROM",pCastGamePointFrom));
			list.Add(new OracleParameter("CAST_GAME_POINT_TO",pCastGamePointTo));
		} else if (!string.IsNullOrEmpty(pCastGamePointFrom)) {
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_CAST_GAME_POINT CGP WHERE CGP.SITE_CD = P.SITE_CD AND CGP.USER_SEQ = P.USER_SEQ AND CGP.USER_CHAR_NO = P.USER_CHAR_NO AND CGP.CAST_GAME_POINT >= :CAST_GAME_POINT_FROM)",ref pWhere);
			list.Add(new OracleParameter("CAST_GAME_POINT_FROM",pCastGamePointFrom));
		} else if (!string.IsNullOrEmpty(pCastGamePointTo)) {
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_CAST_GAME_POINT CGP WHERE CGP.SITE_CD = P.SITE_CD AND CGP.USER_SEQ = P.USER_SEQ AND CGP.USER_CHAR_NO = P.USER_CHAR_NO AND CGP.CAST_GAME_POINT <= :CAST_GAME_POINT_TO)",ref pWhere);
			list.Add(new OracleParameter("CAST_GAME_POINT_TO",pCastGamePointTo));
		}

		if (!string.IsNullOrEmpty(pKeyword)) {
			StringBuilder oWhereKeywords = new StringBuilder("(");
			string[] sKeywordArray = pKeyword.Split(' ','　');
			for (int i = 0; i < sKeywordArray.Length; i++) {
				oWhereKeywords.AppendFormat("((P.COMMENT_LIST LIKE :KEYWORD{0}) OR (P.COMMENT_DETAIL LIKE :KEYWORD{0}) OR (P.WAITING_COMMENT LIKE :KEYWORD{0}) OR (EXISTS (SELECT 1 FROM T_CAST_ATTR_VALUE WHERE SITE_CD = P.SITE_CD AND USER_SEQ = P.USER_SEQ AND USER_CHAR_NO = P.USER_CHAR_NO AND CAST_ATTR_INPUT_VALUE LIKE :KEYWORD{0})))",i);
				list.Add(new OracleParameter(string.Format("KEYWORD{0}",i),string.Format("%{0}%",sKeywordArray[i])));

				if (i < sKeywordArray.Length - 1) {
					oWhereKeywords.AppendLine(" OR ");
				}
			}
			oWhereKeywords.Append(")");
			SysPrograms.SqlAppendWhere(oWhereKeywords.ToString(),ref pWhere);
		}

		if (pIntroducerFriendAllFlag) {
			SysPrograms.SqlAppendWhere("P.INTRODUCER_FRIEND_CD IS NOT NULL", ref pWhere);
		}

		// 未精算報酬額の最小値、最大値の両方が入力されている場合
		if (!string.IsNullOrEmpty(pNotPaymentAmtFrom) && !string.IsNullOrEmpty(pNotPaymentAmtTo)) {
			// 未精算報酬額が指定範囲内である出演者を検索
			SysPrograms.SqlAppendWhere("NOT_PAYMENT_AMT BETWEEN :NOT_PAYMENT_AMT_FROM AND :NOT_PAYMENT_AMT_TO",ref pWhere);
			list.Add(new OracleParameter("NOT_PAYMENT_AMT_FROM",pNotPaymentAmtFrom));
			list.Add(new OracleParameter("NOT_PAYMENT_AMT_TO",pNotPaymentAmtTo));
		}
		// 未精算報酬額の最小値のみ入力されている場合
		else if (!string.IsNullOrEmpty(pNotPaymentAmtFrom)) {
			// 未精算報酬額が指定最小値以上である出演者を検索
			SysPrograms.SqlAppendWhere("NOT_PAYMENT_AMT >= :NOT_PAYMENT_AMT_FROM",ref pWhere);
			list.Add(new OracleParameter("NOT_PAYMENT_AMT_FROM",pNotPaymentAmtFrom));
		}
		// 未精算報酬額の最大値のみ入力されている場合
		else if (!string.IsNullOrEmpty(pNotPaymentAmtTo)) {
			// 未精算報酬額が指定最大値以下である出演者を検索
			SysPrograms.SqlAppendWhere("NOT_PAYMENT_AMT <= :NOT_PAYMENT_AMT_TO",ref pWhere);
			list.Add(new OracleParameter("NOT_PAYMENT_AMT_TO",pNotPaymentAmtTo));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	private string GetNegative(bool pFlag) {
		return pFlag ? "NOT " : string.Empty;
	}

	public DataSet GetCharacterList(string pSiteCd,string pUserSeq,string pMainCharOnlyFlag) {
		DataSet ds;
		string sSiteCd = iBridUtil.GetStringValue(HttpContext.Current.Session["SiteCd"]);
		if (sSiteCd.Equals("")) {
			sSiteCd = pSiteCd;
		}
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT " +
								"VW_CAST01.SITE_CD						," +
								"VW_CAST01.SITE_NM            			," +
								"VW_CAST01.USER_SEQ						," +
								"VW_CAST01.USER_CHAR_NO					," +
								"VW_CAST01.CAST_NM						," +
								"VW_CAST01.LOGIN_ID						," +
								"VW_CAST01.HANDLE_NM					," +
								"VW_CAST01.SMALL_PHOTO_IMG_PATH			," +
								"VW_CAST01.PIC_SEQ						," +
								"VW_CAST01.ACT_CATEGORY_SEQ				," +
								"VW_CAST01.ACT_CATEGORY_NM				," +
								"VW_CAST01.USE_OTHER_SYS_INFO_FLAG		," +
								"SUBSTRB(VW_CAST01.COMMENT_LIST,1,80) COMMENT_LIST," +
								"VW_CAST01.FRIEND_INTRO_CD				," +
								"VW_CAST01.NA_MARK						," +
								"VW_CAST01.SYS_ONLINE_IMG_PATH			," +
								"VW_CAST01.CONNECT_TYPE_NM				," +
								"VW_CAST01.USER_DEFINE_MASK				," +
								"CODE_MAN_RX.CODE_NM	MAN_MAIL_RX_TYPE_NM," +
								"CODE_INFO_RX.CODE_NM	INFO_MAIL_RX_TYPE_NM," +
								"T_CAST_CHARACTER_EX.SITE_USE_STATUS	" +
							"FROM " +
								"VW_CAST01								," +
								"T_CAST_CHARACTER_EX					," +
								"T_CODE_DTL				CODE_MAN_RX		," +
								"T_CODE_DTL				CODE_INFO_RX	" +
							"WHERE	" +
								"VW_CAST01.SITE_CD				= T_CAST_CHARACTER_EX.SITE_CD		(+)	AND	" +
								"VW_CAST01.USER_SEQ				= T_CAST_CHARACTER_EX.USER_SEQ		(+)	AND	" +
								"VW_CAST01.USER_CHAR_NO			= T_CAST_CHARACTER_EX.USER_CHAR_NO	(+)	AND " +
								"'66'							= CODE_MAN_RX.CODE_TYPE				(+)	AND " +
								"VW_CAST01.MAN_MAIL_RX_TYPE		= CODE_MAN_RX.CODE					(+)	AND " +
								"'15'							= CODE_INFO_RX.CODE_TYPE			(+)	AND " +
								"VW_CAST01.INFO_MAIL_RX_TYPE	= CODE_INFO_RX.CODE					(+)		";
			if (!sSiteCd.Equals("")) {
				sSql += " AND VW_CAST01.SITE_CD = :SITE_CD " +
							"AND VW_CAST01.USER_SEQ = :USER_SEQ AND VW_CAST01.SITE_NA_FLAG = 0 ";
			} else {
				sSql += " AND VW_CAST01.USER_SEQ = :USER_SEQ AND VW_CAST01.SITE_NA_FLAG = 0 ";
			}
			if (pMainCharOnlyFlag.Equals("1")) {
				sSql += " AND VW_CAST01.USER_CHAR_NO = :USER_CHAR_NO ";
			} else if (pMainCharOnlyFlag.Equals("0")) {
				sSql += " AND VW_CAST01.USER_CHAR_NO > :USER_CHAR_NO ";
			}

			sSql += " ORDER BY VW_CAST01.SITE_CD, VW_CAST01.USER_SEQ, VW_CAST01.USER_CHAR_NO ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				if (!sSiteCd.Equals("")) {
					cmd.Parameters.Add("SITE_CD",sSiteCd);
				}
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				if (pMainCharOnlyFlag.Equals("1") || pMainCharOnlyFlag.Equals("0")) {
					cmd.Parameters.Add("USER_CHAR_NO",ViCommConst.MAIN_CHAR_NO);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	public bool IsExistLoginId(string pSiteCd,string pLoginID,string pUserCharNo) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT USER_SEQ,HANDLE_NM FROM VW_CAST_CHARACTER00 WHERE SITE_CD = :SITE_CD AND LOGIN_ID= :LOGIN_ID ";

			if (pUserCharNo.Equals("") == false) {
				sSql += " AND USER_CHAR_NO = :USER_CHAR_NO ";
			}
			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("LOGIN_ID",pLoginID);
				if (pUserCharNo.Equals("") == false) {
					cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				}

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_CHARACTER00");

					if (ds.Tables["VW_CAST_CHARACTER00"].Rows.Count != 0) {
						dr = ds.Tables["VW_CAST_CHARACTER00"].Rows[0];
						userSeq = dr["USER_SEQ"].ToString();
						handleNm = dr["HANDLE_NM"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	//PickUpは現在メインキャラのみ表示、編集可能
	public DataSet GetPickupList(string pSiteCd,string pActCategorySeq) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT " +
								"SITE_CD			," +
								"LOGIN_ID			," +
								"USER_SEQ			," +
								"CAST_NM			," +
								"HANDLE_NM			," +
								"ACT_CATEGORY_NM	," +
								"ACT_CATEGORY_SEQ	," +
								"PICKUP_FLAG		," +
								"PICKUP_MARK		," +
								"SUBSTRB(COMMENT_PICKUP,1,80) COMMENT_PICKUP," +
								"NA_MARK " +
							"FROM " +
								"VW_CAST01 " +
							"WHERE " +
								"SITE_CD			= :SITE_CD			AND " +
								"ACT_CATEGORY_SEQ	= :ACT_CATEGORY_SEQ	AND " +
								"USER_STATUS		= :USER_STATUS		AND " +
								"NA_FLAG			= 0					AND " +
								"USER_CHAR_NO		= :USER_CHAR_NO	" +
							"ORDER BY SITE_CD,LOGIN_ID ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("ACT_CATEGORY_SEQ",pActCategorySeq);
				cmd.Parameters.Add("USER_STATUS",ViCommConst.USER_WOMAN_NORMAL);
				cmd.Parameters.Add("USER_CHAR_NO",ViCommConst.MAIN_CHAR_NO);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	//PickUpは現在メインキャラのみ表示、編集可能
	public string[] GetPickUpCast(string pSiteCd,string pActCategorySeq) {
		DataSet ds;
		string[] sPickUpCast = null;
		try {
			conn = DbConnect();

			string sSql = "SELECT USER_SEQ FROM T_CAST_CHARACTER " +
							"WHERE " +
								"SITE_CD			= :SITE_CD			AND " +
								"ACT_CATEGORY_SEQ	= :ACT_CATEGORY_SEQ	AND " +
								"PICKUP_FLAG		= 1 				AND " +
								"USER_CHAR_NO		= :USER_CHAR_NO ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("ACT_CATEGORY_SEQ",pActCategorySeq);
				cmd.Parameters.Add("USER_CHAR_NO",ViCommConst.MAIN_CHAR_NO);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_CAST_CHARACTER");

					if (ds.Tables["T_CAST_CHARACTER"].Rows.Count != 0) {
						sPickUpCast = new string[ds.Tables["T_CAST_CHARACTER"].Rows.Count];
						int i = 0;
						foreach (DataRow dr in ds.Tables[0].Rows) {
							sPickUpCast.SetValue(dr["USER_SEQ"].ToString(),i++);
						}
					}
				}
			}
		} finally {
			conn.Close();
		}
		return sPickUpCast;
	}

	//PickUpは現在メインキャラのみ表示、編集可能
	public DataSet GetPickUpCast(string pSiteCd,string pActCategorySeq,string[] pUserSeq) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT USER_SEQ,ACT_CATEGORY_NM,HANDLE_NM,COMMENT_PICKUP,LOGIN_ID FROM VW_CAST01 " +
							" WHERE SITE_CD = :SITE_CD AND ACT_CATEGORY_SEQ	= :ACT_CATEGORY_SEQ	AND USER_SEQ IN ( ";

			sSql = sSql + "'0'";
			for (int i = 0;i < pUserSeq.Length;i++) {
				sSql = sSql + "," + pUserSeq[i];
			}

			sSql = sSql + " ) AND USER_CHAR_NO = : USER_CHAR_NO ORDER BY LOGIN_ID";
			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("ACT_CATEGORY_SEQ",pActCategorySeq);
				cmd.Parameters.Add("USER_CHAR_NO",ViCommConst.MAIN_CHAR_NO);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetMultiPickUpCast(string pSiteCd,string pPickupID,string[] pUserSeq) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT SITE_CD,USER_SEQ,USER_CHAR_NO,ACT_CATEGORY_NM,HANDLE_NM,LOGIN_ID,COMMENT_LIST,SMALL_PHOTO_IMG_PATH FROM VW_CAST04 " +
							" WHERE SITE_CD = :SITE_CD AND USER_STATUS = :USER_STATUS AND NA_FLAG = :NA_FLAG  ";

			if (pUserSeq.Length > 0) {
				sSql += "AND USER_SEQ IN ( ";
				for (int i = 0;i < pUserSeq.Length;i++) {
					sSql = sSql + "'" + pUserSeq[i] + "',";
				}
				sSql = sSql.Substring(0,sSql.Length - 1);
				sSql = sSql + " ) AND USER_CHAR_NO = :USER_CHAR_NO ";
			}

			sSql += " ORDER BY SITE_CD,LOGIN_ID ";
			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_STATUS",ViCommConst.USER_WOMAN_NORMAL);
				cmd.Parameters.Add("NA_FLAG",ViCommConst.FLAG_OFF);
				cmd.Parameters.Add("USER_CHAR_NO",ViCommConst.MAIN_CHAR_NO);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		AppendPickupInfo(pPickupID,ds);
		return ds;
	}

	private void AppendPickupInfo(string pPickupID,DataSet pDS) {

		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		string sSql = "SELECT PICKUP_ID,COMMENT_PICKUP,PICKUP_FLAG FROM T_PICKUP_CHARACTER " +
						"WHERE " +
							"SITE_CD		= :SITE_CD		AND " +
							"USER_SEQ		= :USER_SEQ		AND " +
							"USER_CHAR_NO	= :USER_CHAR_NO " +
						"ORDER BY SITE_CD,PICKUP_ID ";

		DataColumn col;
		col = new DataColumn("COMMENT_PICKUP",System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		col = new DataColumn("OTHER_PICKUP",System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		foreach (DataRow dr in pDS.Tables[0].Rows) {

			using (DataSet dsSub = new DataSet())
			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",dr["SITE_CD"].ToString());
				cmd.Parameters.Add("USER_SEQ",dr["USER_SEQ"].ToString());
				cmd.Parameters.Add("USER_CHAR_NO",dr["USER_CHAR_NO"].ToString());
				dr["COMMENT_PICKUP"] = dr["COMMENT_LIST"].ToString();
				dr["OTHER_PICKUP"] = "";

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(dsSub,"T_PICKUP_CHARACTER");

					foreach (DataRow drSub in dsSub.Tables[0].Rows) {
						if (pPickupID.Equals(drSub["PICKUP_ID"].ToString())) {
							dr["COMMENT_PICKUP"] = drSub["COMMENT_PICKUP"].ToString();
						} else {
							if (drSub["PICKUP_FLAG"].ToString().Equals("1")) {
								string sOther = dr["OTHER_PICKUP"].ToString();
								sOther = sOther + "<BR>" + drSub["PICKUP_ID"].ToString();
								dr["OTHER_PICKUP"] = sOther;
							}
						}
					}
				}
			}
		}
	}

	public DataSet GetMultiPickupList(string pSiteCd,string pPickupId) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT " +
								"SITE_CD	," +
								"USER_SEQ	" +
							"FROM " +
								"T_PICKUP_CHARACTER " +
							"WHERE " +
								"SITE_CD			= :SITE_CD		AND " +
								"PICKUP_ID			= :PICKUP_ID	AND " +
								"PICKUP_FLAG		= :PICKUP_FLAG ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("PICKUP_ID",pPickupId);
				cmd.Parameters.Add("PICKUP_FLAG",ViCommConst.FLAG_ON);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}

		return ds;
	}

	public DataSet GetOneByUserSeq(string pSietCd,string pUserSeq,string pUserCharNo) {

		System.Text.StringBuilder oSqlBuilder = new System.Text.StringBuilder();
		oSqlBuilder.Append(" SELECT                                            ").AppendLine();
		oSqlBuilder.Append(" 	SITE_CD					,                      ").AppendLine();
		oSqlBuilder.Append(" 	USER_SEQ				,                      ").AppendLine();
		oSqlBuilder.Append(" 	USER_CHAR_NO			,                      ").AppendLine();
		oSqlBuilder.Append(" 	LOGIN_ID				,                      ").AppendLine();
		oSqlBuilder.Append(" 	CAST_NM					,                      ").AppendLine();
		oSqlBuilder.Append(" 	HANDLE_NM				,                      ").AppendLine();
		oSqlBuilder.Append(" 	GET_PHOTO_IMG_PATH(SITE_CD,LOGIN_ID,PROFILE_PIC_SEQ) AS PHOTO_IMG_PATH").AppendLine();
		oSqlBuilder.Append(" FROM                                              ").AppendLine();
		oSqlBuilder.Append(" 	VW_CAST_CHARACTER00                            ").AppendLine();
		oSqlBuilder.Append(" WHERE                                             ").AppendLine();
		oSqlBuilder.Append(" 	SITE_CD			=	:SITE_CD 		AND        ").AppendLine();
		oSqlBuilder.Append(" 	USER_SEQ		=	:USER_SEQ 		AND        ").AppendLine();
		oSqlBuilder.Append(" 	USER_CHAR_NO	=	:USER_CHAR_NO              ").AppendLine();

		DataSet ds = new DataSet();
		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(":SITE_CD",pSietCd);
				cmd.Parameters.Add(":USER_SEQ",pUserSeq);
				cmd.Parameters.Add(":USER_CHAR_NO",pUserCharNo);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}

		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetOneByProductSeq(string pSietCd,string pProductSeq,string pProductAgentCd) {

		System.Text.StringBuilder oSqlBuilder = new System.Text.StringBuilder();
		oSqlBuilder.Append(" SELECT										").AppendLine();
		oSqlBuilder.Append(" 	VW_CAST_CHARACTER00.SITE_CD			,	").AppendLine();
		oSqlBuilder.Append(" 	VW_CAST_CHARACTER00.USER_SEQ		,	").AppendLine();
		oSqlBuilder.Append(" 	VW_CAST_CHARACTER00.USER_CHAR_NO	,	").AppendLine();
		oSqlBuilder.Append(" 	VW_CAST_CHARACTER00.LOGIN_ID		,	").AppendLine();
		oSqlBuilder.Append(" 	VW_CAST_CHARACTER00.HANDLE_NM			").AppendLine();
		oSqlBuilder.Append(" FROM									").AppendLine();
		oSqlBuilder.Append(" 	VW_CAST_CHARACTER00				,	").AppendLine();
		oSqlBuilder.Append(" 	T_PRODUCT_CAST_CHARACTER_REL		").AppendLine();
		oSqlBuilder.Append(" WHERE									").AppendLine();
		oSqlBuilder.Append(" 	T_PRODUCT_CAST_CHARACTER_REL.SITE_CD			=	:SITE_CD									AND	").AppendLine();
		oSqlBuilder.Append(" 	T_PRODUCT_CAST_CHARACTER_REL.PRODUCT_SEQ		=	:PRODUCT_SEQ								AND	").AppendLine();
		oSqlBuilder.Append(" 	T_PRODUCT_CAST_CHARACTER_REL.PRODUCT_AGENT_CD	=	:PRODUCT_AGENT_CD							AND	").AppendLine();
		oSqlBuilder.Append(" 	VW_CAST_CHARACTER00.SITE_CD						=	T_PRODUCT_CAST_CHARACTER_REL.SITE_CD		AND	").AppendLine();
		oSqlBuilder.Append(" 	VW_CAST_CHARACTER00.USER_SEQ					=	T_PRODUCT_CAST_CHARACTER_REL.USER_SEQ		AND	").AppendLine();
		oSqlBuilder.Append(" 	VW_CAST_CHARACTER00.USER_CHAR_NO				=	T_PRODUCT_CAST_CHARACTER_REL.USER_CHAR_NO		").AppendLine();

		DataSet ds = new DataSet();
		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(":SITE_CD",pSietCd);
				cmd.Parameters.Add(":PRODUCT_SEQ",pProductSeq);
				cmd.Parameters.Add(":PRODUCT_AGENT_CD",pProductAgentCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}

		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetOne(string sSietCd,string pLoginID,string pUserCharNo) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT " +
							"USER_SEQ				," +
							"USER_CHAR_NO			," +
							"PRODUCTION_SEQ			," +
							"MANAGER_SEQ			," +
							"CAST_NM				," +
							"CAST_KANA_NM			," +
							"HANDLE_NM				," +
							"LOGIN_ID				," +
							"LOGIN_PASSWORD			," +
							"URI					," +
							"TEL					," +
							"ACT_CATEGORY_SEQ		," +
							"ACT_CATEGORY_NM		," +
							"CONNECT_TYPE			," +
							"CONNECT_TYPE_NM		," +
							"SMALL_PHOTO_IMG_PATH	," +
							"PHOTO_IMG_PATH			," +
							"COMMENT_LIST			," +
							"MONITOR_ENABLE_FLAG	," +
							"PROFILE_MOVIE_CNT		," +
							"TALK_MOVIE_CNT			," +
							"UPDATE_DATE			," +
							"REGIST_DATE			 " +
							"FROM VW_CAST04 WHERE SITE_CD =:SITE_CD AND LOGIN_ID =:LOGIN_ID AND USER_CHAR_NO =:USER_CHAR_NO";
			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",sSietCd);
				cmd.Parameters.Add("LOGIN_ID",pLoginID);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public bool IsExistUserCharNo(string pSiteCd,string pUserSeq,string pUserCharNo) {
		DataSet ds;
		bool bExist = false;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT USER_SEQ FROM VW_CAST_CHARACTER00 WHERE SITE_CD = :SITE_CD AND USER_SEQ = :USER_SEQ AND USER_CHAR_NO = :USER_CHAR_NO ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_CHARACTER00");

					if (ds.Tables["VW_CAST_CHARACTER00"].Rows.Count != 0) {
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public bool IsExistUserCharNoByLoginId(string pSiteCd,string pLoginId,string pUserCharNo) {
		DataSet ds;
		bool bExist = false;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT USER_SEQ FROM VW_CAST_CHARACTER00 WHERE SITE_CD = :SITE_CD AND LOGIN_ID = :LOGIN_ID AND USER_CHAR_NO = :USER_CHAR_NO ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("LOGIN_ID",pLoginId);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_CHARACTER00");

					if (ds.Tables["VW_CAST_CHARACTER00"].Rows.Count != 0) {
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public DataSet GetOneByFriendIntroCd(string sFriendIntroCd) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT " +
							"LOGIN_ID	" +
							"FROM VW_CAST_CHARACTER00 WHERE FRIEND_INTRO_CD =:FRIEND_INTRO_CD";
			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("FRIEND_INTRO_CD",sFriendIntroCd);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	public bool CheckHandleNmDupli(string pSiteCd,string pUserSeq,string pUserCharNo,string pHandleNm) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("CHECK_CAST_HANDLE_NM_DUPLI");
			oDbSession.ProcedureInParm("pSITE_CD",DbType.VARCHAR2,pSiteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ",DbType.VARCHAR2,pUserSeq);
			oDbSession.ProcedureInParm("pUSER_CHAR_NO",DbType.VARCHAR2,pUserCharNo);
			oDbSession.ProcedureInParm("pHANDLE_NM",DbType.VARCHAR2,pHandleNm);
			oDbSession.ProcedureOutParm("pDUPLI_FLAG",DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			return oDbSession.GetIntValue("pDUPLI_FLAG") == 0;
		}
	}

	public bool IsUsePickup(string pSiteCd,string pUserSeq,string pUserCharNo) {
		bool bExist = false;
		try {
			conn = DbConnect();
			System.Text.StringBuilder oSqlBuilder = new System.Text.StringBuilder();
			oSqlBuilder.Append("SELECT																					").AppendLine();
			oSqlBuilder.Append("	COUNT(*)																			").AppendLine();
			oSqlBuilder.Append("FROM																					").AppendLine();
			oSqlBuilder.Append("	T_USER				,																").AppendLine();
			oSqlBuilder.Append("	T_CAST_CHARACTER																	").AppendLine();
			oSqlBuilder.Append("WHERE																					").AppendLine();
			oSqlBuilder.Append("	T_USER.USER_SEQ								= T_CAST_CHARACTER.USER_SEQ		AND		").AppendLine();
			oSqlBuilder.Append("	T_CAST_CHARACTER.SITE_CD					= :SITE_CD						AND		").AppendLine();
			oSqlBuilder.Append("	T_CAST_CHARACTER.USER_SEQ					= :USER_SEQ						AND		").AppendLine();
			oSqlBuilder.Append("	T_CAST_CHARACTER.USER_CHAR_NO				= :USER_CHAR_NO					AND		").AppendLine();
			oSqlBuilder.Append("	T_CAST_CHARACTER.NA_FLAG					= :NA_FLAG						AND		").AppendLine();
			oSqlBuilder.Append("	T_USER.USER_STATUS							= :USER_STATUS							").AppendLine();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":USER_SEQ",pUserSeq);
				cmd.Parameters.Add(":USER_CHAR_NO",pUserCharNo);
				cmd.Parameters.Add(":NA_FLAG",ViCommConst.FLAG_OFF);
				cmd.Parameters.Add(":USER_STATUS",ViCommConst.USER_WOMAN_NORMAL);

				bExist = ((decimal)cmd.ExecuteScalar() > 0);
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}


	public bool IsTerminalUniqueIdDupli(string pUserSeq,string pTerminalUniqueId) {
		DataSet ds;
		bool bExist = false;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT USER_SEQ,USER_STATUS FROM T_USER WHERE TERMINAL_UNIQUE_ID = :TERMINAL_UNIQUE_ID";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":TERMINAL_UNIQUE_ID",pTerminalUniqueId);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					foreach (DataRow dr in ds.Tables[0].Rows) {
						if (!dr["USER_SEQ"].ToString().Equals(pUserSeq) &&
							!dr["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_HOLD)) {
							bExist = true;
							break;
						}
					}
				}
			}
		} finally {
			conn.Clone();
		}

		return !bExist;
	}

	public bool IsIModeIdDupli(string pUserSeq,string pIModeId) {
		DataSet ds;
		bool bExist = false;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT USER_SEQ,USER_STATUS FROM T_USER WHERE IMODE_ID = :IMODE_ID";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":IMODE_ID",pIModeId);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					foreach (DataRow dr in ds.Tables[0].Rows) {
						if (!dr["USER_SEQ"].ToString().Equals(pUserSeq) &&
							!dr["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_HOLD)) {
							bExist = true;
							break;
						}
					}
				}
			}
		} finally {
			conn.Clone();
		}

		return !bExist;
	}
	public bool IsEmailDupli(string pUserSeq,string pEmailAddr) {
		DataSet ds;
		bool bExist = false;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT USER_SEQ,USER_STATUS FROM T_USER WHERE EMAIL_ADDR = :EMAIL_ADDR";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":EMAIL_ADDR",pEmailAddr.ToLower());
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					foreach (DataRow dr in ds.Tables[0].Rows) {
						if (!dr["USER_SEQ"].ToString().Equals(pUserSeq) &&
							!dr["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_HOLD)) {
							bExist = true;
							break;
						}
					}
				}
			}
		} finally {
			conn.Clone();
		}

		return !bExist;
	}

	public bool IsTelDupli(string pUserSeq,string pTel) {
		DataSet ds;
		bool bExist = false;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT USER_SEQ,USER_STATUS FROM T_USER WHERE TEL = :TEL";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":TEL",pTel);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					foreach (DataRow dr in ds.Tables[0].Rows) {
						if (!dr["USER_SEQ"].ToString().Equals(pUserSeq) &&
							!dr["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_HOLD)) {
							bExist = true;
							break;
						}
					}
				}
			}
		} finally {
			conn.Clone();
		}

		return !bExist;
	}

	public string GetLastLoginDate(
		string pUserSeq
	) {
		DataSet ds;
		DataRow dr;
		string sLastLoginDate = string.Empty;

		try {
			conn = DbConnect();

			string sSql = "SELECT " +
								" LAST_LOGIN_DATE " +
							" FROM " +
								" T_CAST_CHARACTER " +
							" WHERE " +
								" USER_SEQ	= :USER_SEQ " +
							" ORDER BY " +
								" LAST_LOGIN_DATE DESC NULLS LAST ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("USER_SEQ",pUserSeq);

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					sLastLoginDate = iBridUtil.GetStringValue(dr["LAST_LOGIN_DATE"]);
				}
			}
		} finally {
			conn.Close();
		}

		return sLastLoginDate;
	}

	void SetPickupTable(string pSiteCd,string pPickupId, ref StringBuilder pSql) {
		switch (this.GetPickupType(pSiteCd,pPickupId)) {
			case ViCommConst.PicupTypes.CAST_CHARACTER:
				pSql.Append(",T_PICKUP_CHARACTER PK ");
				break;
			case ViCommConst.PicupTypes.PROFILE_PIC:
				pSql.Append(",VW_BBS_OBJS03 PK ");
				break;
			case ViCommConst.PicupTypes.PROFILE_MOVIE:
				pSql.Append(",VW_BBS_OBJS02 PK ");
				break;
			case ViCommConst.PicupTypes.BBS_PIC:
			case ViCommConst.PicupTypes.BBS_MOVIE:
				pSql.Append(",VW_BBS_OBJS01 PK ");
				break;
		}
	}

	private string GetPickupType(string pSiteCd,string pPickupId) {
		using (Pickup oPickup = new Pickup()) {
			return oPickup.GetPickupType(pSiteCd,pPickupId);
		}
	}
	
	public bool GetValue(string pSiteCd,string pUserSeq,string pUserCharNo,string pItem,ref string pValue) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect();
			string sSql = "SELECT " +
								"*	" +
							"FROM " +
								"T_CAST_CHARACTER " +
							"WHERE " +
								"SITE_CD		= :SITE_CD		AND	" +
								"USER_SEQ		= :USER_SEQ		AND	" + 
								"USER_CHAR_NO	= :USER_CHAR_NO		";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_CAST_CHARACTER");
					if (ds.Tables["T_CAST_CHARACTER"].Rows.Count != 0) {
						dr = ds.Tables["T_CAST_CHARACTER"].Rows[0];
						pValue = dr[pItem].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

}
