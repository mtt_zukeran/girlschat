﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 商品オークション

--	Progaram ID		: ProductAuction
--
--  Creation Date	: 2011.06.20
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class ProductAuction:DbSession {
	public ProductAuction() {

	}
	
	public DataSet GetOne(string pSiteCd,string pProductAgentCd, string pProductSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT				").AppendLine();
		oSqlBuilder.Append("	 T_PRODUCT_AUCTION.SITE_CD				, ").AppendLine();
		oSqlBuilder.Append("	 T_PRODUCT_AUCTION.PRODUCT_AGENT_CD		, ").AppendLine();
		oSqlBuilder.Append("	 T_PRODUCT_AUCTION.PRODUCT_SEQ			, ").AppendLine();
		oSqlBuilder.Append("	 T_PRODUCT_AUCTION.AUCTION_STATUS		, ").AppendLine();
		oSqlBuilder.Append("	 T_PRODUCT_AUCTION.AUCTION_START_DATE	, ").AppendLine();
		oSqlBuilder.Append("	 T_PRODUCT_AUCTION.BLIND_END_DATE		, ").AppendLine();
		oSqlBuilder.Append("	 T_PRODUCT_AUCTION.AUCTION_END_DATE		, ").AppendLine();
		oSqlBuilder.Append("	 T_PRODUCT_AUCTION.RESERVE_AMT			, ").AppendLine();
		oSqlBuilder.Append("	 T_PRODUCT_AUCTION.MINIMUM_BID_AMT		, ").AppendLine();
		oSqlBuilder.Append("	 T_PRODUCT_AUCTION.ENTRY_POINT			, ").AppendLine();
		oSqlBuilder.Append("	 T_PRODUCT_AUCTION.BLIND_ENTRY_POINT	, ").AppendLine();
		oSqlBuilder.Append("	 T_PRODUCT_AUCTION.BID_POINT			, ").AppendLine();
		oSqlBuilder.Append("	 T_PRODUCT_AUCTION.BLIND_BID_POINT		, ").AppendLine();
		oSqlBuilder.Append("	 T_PRODUCT_AUCTION.MAX_BID_AMT			, ").AppendLine();
		oSqlBuilder.Append("	 T_PRODUCT_AUCTION.BID_COUNT			, ").AppendLine();
		oSqlBuilder.Append("	 T_PRODUCT_AUCTION.AUTO_EXTENSION_FLAG	  ").AppendLine();
		oSqlBuilder.Append(" FROM										  ").AppendLine();
		oSqlBuilder.Append("	T_PRODUCT_AUCTION						  ").AppendLine();
		oSqlBuilder.Append(" WHERE										  ").AppendLine();
		oSqlBuilder.Append("	T_PRODUCT_AUCTION.SITE_CD			= :SITE_CD			AND ").AppendLine();
		oSqlBuilder.Append("	T_PRODUCT_AUCTION.PRODUCT_AGENT_CD	= :PRODUCT_AGENT_CD	AND ").AppendLine();
		oSqlBuilder.Append("	T_PRODUCT_AUCTION.PRODUCT_SEQ		= :PRODUCT_SEQ			").AppendLine();

		DataSet oProductDataSet = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":PRODUCT_AGENT_CD",pProductAgentCd);
				cmd.Parameters.Add(":PRODUCT_SEQ",pProductSeq);				

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oProductDataSet);
				}
			}
		} finally {
			conn.Close();
		}
		return oProductDataSet;

	}
}
