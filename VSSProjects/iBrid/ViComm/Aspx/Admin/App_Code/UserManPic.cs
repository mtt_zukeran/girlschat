﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 男性写真

--	Progaram ID		: UserManPic
--
--  Creation Date	: 2011.08.04
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater			Update Explain

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;
using ViComm;

public class UserManPic : DbSession {
	public UserManPic() {
	}

	public int GetApproveManagePageCount(string pSiteCd,string pNotApproveFlag,string pNotPublishFlag,string pPicType,string pUploadDateFrom,string pUploadDateTo,string pLoginId) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_USER_MAN_PIC01 ";

			string sWhere = string.Empty;
			OracleParameter[] objParms = CreateApproveManageWhere(pSiteCd,pNotApproveFlag,pNotPublishFlag,pPicType,pUploadDateFrom,pUploadDateTo,pLoginId,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0; i < objParms.Length; i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetApproveManagePageCollection(string pSiteCd,string pNotApproveFlag,string pNotPublishFlag,string pPicType,string pUploadDateFrom,string pUploadDateTo,string pLoginId,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY SITE_CD,PIC_SEQ DESC ";

			string sSql = "SELECT " +
								"SITE_CD					," +
								"SMALL_PHOTO_IMG_PATH		," +
								"PHOTO_IMG_PATH				," +
								"USER_SEQ					," +
								"USER_CHAR_NO				," +
								"LOGIN_ID					," +
								"HANDLE_NM					," +
								"UPLOAD_DATE				," +
								"PIC_SEQ					," +
								"PIC_TYPE					," +
								"OBJ_NOT_PUBLISH_FLAG		," +
								"DECODE(OBJ_NOT_APPROVE_FLAG, 0, DECODE(OBJ_NOT_PUBLISH_FLAG, 0, '公開', '非公開'), '認証待ち')	NOT_APPROVE_MARK_ADMIN	" +
							"FROM(" +
							" SELECT VW_USER_MAN_PIC01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_USER_MAN_PIC01";
			string sWhere = string.Empty;
			OracleParameter[] objParms = CreateApproveManageWhere(pSiteCd,pNotApproveFlag,pNotPublishFlag,pPicType,pUploadDateFrom,pUploadDateTo,pLoginId,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ") INNER WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0; i < objParms.Length; i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USER_MAN_PIC01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateApproveManageWhere(string pSiteCd,string pNotApproveFlag,string pNotPublishFlag,string pPicType,string pUploadDateFrom,string pUploadDateTo,string pLoginId,ref string pWhere) {
		pWhere = string.Empty;

		ArrayList list = new ArrayList();

		if (pSiteCd.Equals(string.Empty) == false) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (pPicType.Equals(ViCommConst.ATTACHED_PROFILE.ToString())) {
			iBridCommLib.SysPrograms.SqlAppendWhere("PIC_TYPE IN (:PIC_TYPE1,:PIC_TYPE2)",ref pWhere);
			list.Add(new OracleParameter("PIC_TYPE1",pPicType));
			list.Add(new OracleParameter("PIC_TYPE2",ViCommConst.ATTACHED_HIDE.ToString()));
		} else {
			iBridCommLib.SysPrograms.SqlAppendWhere("PIC_TYPE = :PIC_TYPE",ref pWhere);
			list.Add(new OracleParameter("PIC_TYPE",pPicType));
		}

		if ((!string.IsNullOrEmpty(pNotApproveFlag)) || (string.IsNullOrEmpty(pNotPublishFlag))) {
			if (pNotApproveFlag.Equals(ViCommConst.FLAG_ON.ToString()) & pNotPublishFlag.Equals(ViCommConst.FLAG_ON.ToString())) {
				SysPrograms.SqlAppendWhere("(OBJ_NOT_APPROVE_FLAG = :OBJ_NOT_APPROVE_FLAG OR OBJ_NOT_PUBLISH_FLAG = :OBJ_NOT_PUBLISH_FLAG)",ref pWhere);
				list.Add(new OracleParameter("OBJ_NOT_APPROVE_FLAG",pNotApproveFlag));
				list.Add(new OracleParameter("OBJ_NOT_PUBLISH_FLAG",pNotPublishFlag));
			} else {
				if (pNotApproveFlag.Equals(string.Empty) == false) {
					SysPrograms.SqlAppendWhere("OBJ_NOT_APPROVE_FLAG = :OBJ_NOT_APPROVE_FLAG",ref pWhere);
					list.Add(new OracleParameter("OBJ_NOT_APPROVE_FLAG",pNotApproveFlag));
				}

				// 非公開フラグ
				if (!string.IsNullOrEmpty(pNotPublishFlag)) {
					SysPrograms.SqlAppendWhere("OBJ_NOT_PUBLISH_FLAG = :OBJ_NOT_PUBLISH_FLAG",ref pWhere);
					list.Add(new OracleParameter("OBJ_NOT_PUBLISH_FLAG",pNotPublishFlag));
				}
			}
		}

		// アップロード日時(from)
		if (!string.IsNullOrEmpty(pUploadDateFrom)) {
			SysPrograms.SqlAppendWhere("UPLOAD_DATE_RAW >= :UPLOAD_DATE_FROM",ref pWhere);
			DateTime dtFrom = DateTime.ParseExact(pUploadDateFrom,"yyyy/MM/dd",null,System.Globalization.DateTimeStyles.None).Date;
			list.Add(new OracleParameter("UPLOAD_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
		}

		// アップロード日時(to)
		if (!string.IsNullOrEmpty(pUploadDateTo)) {
			SysPrograms.SqlAppendWhere("UPLOAD_DATE_RAW < :UPLOAD_DATE_TO",ref pWhere);
			DateTime dtFrom = DateTime.ParseExact(pUploadDateTo,"yyyy/MM/dd",null,System.Globalization.DateTimeStyles.None).Date.AddDays(1);
			list.Add(new OracleParameter("UPLOAD_DATE_TO",OracleDbType.Date,dtFrom,ParameterDirection.Input));
		}

		// ﾛｸﾞｲﾝID
		if (!string.IsNullOrEmpty(pLoginId)) {
			SysPrograms.SqlAppendWhere("LOGIN_ID = :LOGIN_ID",ref pWhere);
			list.Add(new OracleParameter("LOGIN_ID",pLoginId));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}
