﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: MailDeBingo
--	Title			: ランキング集計設定
--	Progaram ID		: MailDeBingo
--
--  Creation Date	: 2011.04.05
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater   Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Text;
using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class MailDeBingo:DbSession {
	public int jackpot;
	public string bingoStartDate;
	public string bingoEndDate;

	public MailDeBingo() {
		jackpot = 0;
		bingoStartDate = string.Empty;
		bingoEndDate = string.Empty;
	}

	public DataSet GetPageCollection(string pSiteCd,string pSexCd, int startRowIndex,int maximumRows) {

		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();
			StringBuilder oSql = new StringBuilder();

			string sOrder = "ORDER BY SITE_CD,BINGO_START_DATE DESC";
			oSql.AppendLine("SELECT ");
			oSql.AppendLine("	SITE_CD				,	");
			oSql.AppendLine("	SEX_CD         		,	");
			oSql.AppendLine("	BINGO_TERM_SEQ		,	");
			oSql.AppendLine("	BINGO_START_DATE	,	");
			oSql.AppendLine("	BINGO_END_DATE	    ,	");
			oSql.AppendLine("	JACKPOT				,	");
			oSql.AppendLine("	REMARKS				,	");
			oSql.AppendLine("	UPDATE_DATE			,	");
			oSql.AppendLine("	BINGO_BALL_FLAG			");
			oSql.AppendLine("FROM(");
			oSql.AppendLine(" SELECT T_MAIL_DE_BINGO.*, ROW_NUMBER() OVER ( " + sOrder + ") AS RNUM FROM T_MAIL_DE_BINGO ");

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pSexCd,ref sWhere);
			oSql.AppendLine(sWhere);

			oSql.AppendLine(")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ");
			oSql.AppendLine(sOrder);

			using (cmd = CreateSelectCommand(oSql.ToString(),conn)) {
				cmd.BindByName = true;
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MAIL_DE_BINGO");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pSexCd, ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		SysPrograms.SqlAppendWhere(" SITE_CD = :SITE_CD ",ref pWhere);
		list.Add(new OracleParameter("SITE_CD",pSiteCd));

		SysPrograms.SqlAppendWhere(" SEX_CD = :SEX_CD ",ref pWhere);
		list.Add(new OracleParameter("SEX_CD",pSexCd));

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public int GetPageCount(string pSiteCd,string pSexCd) {

		int iPageCount = 0;
		try {
			conn = DbConnect();
			DataSet ds;
			StringBuilder sSql = new StringBuilder();

			sSql.AppendLine("SELECT ");
			sSql.AppendLine("   COUNT(*) AS ROW_COUNT ");
			sSql.AppendLine("FROM ");
			sSql.AppendLine("   T_MAIL_DE_BINGO ");
			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pSexCd,ref sWhere);
			sSql.AppendLine(sWhere);

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					using (ds = new DataSet()) {
						for (int i = 0;i < objParms.Length;i++) {
							cmd.Parameters.Add((OracleParameter)objParms[i]);
						}
						da.Fill(ds);
						iPageCount = Convert.ToInt32(ds.Tables[0].Rows[0]["ROW_COUNT"]);
					}
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public bool IsDuplicateDate(string pSiteCd,string pSexCd, DateTime pBingoStartDate, DateTime pBingoEndDate, string pBingoTermSeq) {
		bool bDuplicate = false;
		try {
			conn = DbConnect();
			DataSet ds;
			StringBuilder sSql = new StringBuilder();
			sSql.AppendLine("SELECT ");
			sSql.AppendLine("   BINGO_TERM_SEQ ");
			sSql.AppendLine("FROM ");
			sSql.AppendLine("   T_MAIL_DE_BINGO ");
			sSql.AppendLine("WHERE ");
			sSql.AppendLine("	SITE_CD				= :SITE_CD				AND ");
			sSql.AppendLine("	SEX_CD      		= :SEX_CD   			AND ");
			sSql.AppendLine("	((BINGO_START_DATE>= :BINGO_START_DATE1	AND	");
			sSql.AppendLine("	 BINGO_START_DATE <= :BINGO_END_DATE1)	OR	");
			sSql.AppendLine("	(BINGO_END_DATE	>= :BINGO_START_DATE2	AND	");
			sSql.AppendLine("	 BINGO_END_DATE	<= :BINGO_END_DATE2)	OR	");
			sSql.AppendLine("	(BINGO_START_DATE <= :BINGO_START_DATE3	AND	");
			sSql.AppendLine("	 BINGO_END_DATE	>= :BINGO_END_DATE3))		");
			if (!pBingoTermSeq.Equals(string.Empty)) {
				sSql.AppendLine(" AND	BINGO_TERM_SEQ		!=:BINGO_TERM_SEQ		");
			}
			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					using (ds = new DataSet()) {
						cmd.Parameters.Add("SITE_CD",pSiteCd);
						cmd.Parameters.Add("SEX_CD",pSexCd);
						cmd.Parameters.Add("BINGO_START_DATE1",OracleDbType.Date,pBingoStartDate,ParameterDirection.Input);
						cmd.Parameters.Add("BINGO_END_DATE1",OracleDbType.Date,pBingoEndDate,ParameterDirection.Input);
						cmd.Parameters.Add("BINGO_START_DATE2",OracleDbType.Date,pBingoStartDate,ParameterDirection.Input);
						cmd.Parameters.Add("BINGO_END_DATE2",OracleDbType.Date,pBingoEndDate,ParameterDirection.Input);
						cmd.Parameters.Add("BINGO_START_DATE3",OracleDbType.Date,pBingoStartDate,ParameterDirection.Input);
						cmd.Parameters.Add("BINGO_END_DATE3",OracleDbType.Date,pBingoEndDate,ParameterDirection.Input);
						if (!string.IsNullOrEmpty(pBingoTermSeq)) {
							cmd.Parameters.Add("BINGO_TERM_SEQ",pBingoTermSeq);
						}
						da.Fill(ds);
						if (ds.Tables[0].Rows.Count != 0) {
							bDuplicate = true;
						}
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bDuplicate;
	}

	public DataSet GetList(string pSiteCd,string pSexType, string pBingoTermSeq) {

		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();
			StringBuilder oSql = new StringBuilder();

			string sOrder = "ORDER BY SITE_CD,BINGO_START_DATE DESC";
			oSql.AppendLine("SELECT ");
			oSql.AppendLine("	SEX_CD      		,");
			oSql.AppendLine("	BINGO_TERM_SEQ		,");
			oSql.AppendLine("	JACKPOT				,");
			oSql.AppendLine("	REMARKS				,");
			oSql.AppendLine("	BINGO_START_DATE	,");
			oSql.AppendLine("	BINGO_END_DATE	");
			oSql.AppendLine("FROM ");
			oSql.AppendLine("	T_MAIL_DE_BINGO	");

			string sWhere = "";
			List<OracleParameter> oParameterList = new List<OracleParameter>(CreateWhere(pSiteCd,pSexType,ref sWhere));
			oSql.AppendLine(sWhere);

			if (!string.IsNullOrEmpty(pBingoTermSeq)) {
				oSql.AppendLine(" AND BINGO_TERM_SEQ = :BINGO_TERM_SEQ");
				oParameterList.Add(new OracleParameter("BINGO_TERM_SEQ",pBingoTermSeq));
			}
			oSql.AppendLine(sOrder);

			using (cmd = CreateSelectCommand(oSql.ToString(),conn)) {
				cmd.Parameters.AddRange(oParameterList.ToArray());
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MAIL_DE_BINGO");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public bool GetOne(string pBingoTermSeq) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect("MailDeBingo.GetOne");

			using (cmd = CreateSelectCommand("SELECT * FROM T_MAIL_DE_BINGO WHERE BINGO_TERM_SEQ = :BINGO_TERM_SEQ",conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("BINGO_ENTRY_SEQ",pBingoTermSeq);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MAIL_DE_BINGO");
					if (ds.Tables["T_MAIL_DE_BINGO"].Rows.Count != 0) {
						dr = ds.Tables["T_MAIL_DE_BINGO"].Rows[0];
						jackpot = int.Parse(dr["JACKPOT"].ToString());
						bingoStartDate = dr["BINGO_START_DATE"].ToString();
						bingoEndDate = dr["BINGO_END_DATE"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

}
