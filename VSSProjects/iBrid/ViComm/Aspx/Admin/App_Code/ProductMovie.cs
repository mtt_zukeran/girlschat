﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 商品動画

--	Progaram ID		: ProductMovie
--
--  Creation Date	: 2010.12.09
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class ProductMovie :DbSession {
	public ProductMovie() {}
	
	public decimal GetMovieNo() {
		decimal dMovieNo = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT SEQ_OBJ.NEXTVAL FROM DUAL";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				dMovieNo = (decimal)cmd.ExecuteScalar();
			}
		} finally {
			conn.Close();
		}
		return dMovieNo;
	}

	//public DataSet GetListByProductMovieType(string pSiteCd,string pProductAgentCd,string pProductSeq,string pProductMovieType){
	//    StringBuilder oSqlBuilder = new StringBuilder();
	//    oSqlBuilder.AppendLine(" SELECT						");
	//    oSqlBuilder.AppendLine("	T_PRODUCT_MOVIE.OBJ_SEQ ");
	//    oSqlBuilder.AppendLine(" FROM						");
	//    oSqlBuilder.AppendLine("	T_PRODUCT_MOVIE			");
	//    oSqlBuilder.AppendLine(" WHERE ");
	//    oSqlBuilder.AppendLine("	T_PRODUCT_MOVIE.SITE_CD				= :SITE_CD			AND ");
	//    oSqlBuilder.AppendLine("	T_PRODUCT_MOVIE.PRODUCT_AGENT_CD	= :PRODUCT_AGENT_CD AND ");
	//    oSqlBuilder.AppendLine("	T_PRODUCT_MOVIE.PRODUCT_SEQ			= :PRODUCT_SEQ		AND ");
	//    oSqlBuilder.AppendLine("	T_PRODUCT_MOVIE.LINK_ID				= :LINK_ID			AND ");
	//    oSqlBuilder.AppendLine("	T_PRODUCT_MOVIE.PRODUCT_MOVIE_TYPE	= :PRODUCT_MOVIE_TYPE   ");

	//    try {
	//        conn = DbConnect();

	//        using (cmd = CreateSelectCommand(oSqlBuilder.ToString(), conn)) {
	//            cmd.BindByName = true;
	//            cmd.Parameters.Add(":SITE_CD", pSiteCd);
	//            cmd.Parameters.Add(":PRODUCT_AGENT_CD", pProductAgentCd);
	//            cmd.Parameters.Add(":PRODUCT_SEQ", pProductSeq);
	//            cmd.Parameters.Add(":LINK_ID", pLinkId);
	//            cmd.Parameters.Add(":PRODUCT_MOVIE_TYPE", pProductMovieType);

	//            return iBridUtil.GetStringValue(cmd.ExecuteScalar());
	//        }
	//    } finally {
	//        conn.Close();
	//    }
	//}
	
	public string GetObjSeqByLinkId(string pSiteCd,string pProductAgentCd,string pProductSeq,string pLinkId,string pProductMovieType){
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT						");
		oSqlBuilder.AppendLine("	T_PRODUCT_MOVIE.OBJ_SEQ ");
		oSqlBuilder.AppendLine(" FROM						");
		oSqlBuilder.AppendLine("	T_PRODUCT_MOVIE			");
		oSqlBuilder.AppendLine(" WHERE ");
		oSqlBuilder.AppendLine("	T_PRODUCT_MOVIE.SITE_CD				= :SITE_CD			AND ");
		oSqlBuilder.AppendLine("	T_PRODUCT_MOVIE.PRODUCT_AGENT_CD	= :PRODUCT_AGENT_CD AND ");
		oSqlBuilder.AppendLine("	T_PRODUCT_MOVIE.PRODUCT_SEQ			= :PRODUCT_SEQ		AND ");
		oSqlBuilder.AppendLine("	T_PRODUCT_MOVIE.LINK_ID				= :LINK_ID			AND ");
		oSqlBuilder.AppendLine("	T_PRODUCT_MOVIE.PRODUCT_MOVIE_TYPE	= :PRODUCT_MOVIE_TYPE   ");
		
		try{
			conn = DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD", pSiteCd);
				cmd.Parameters.Add(":PRODUCT_AGENT_CD", pProductAgentCd);
				cmd.Parameters.Add(":PRODUCT_SEQ", pProductSeq);
				cmd.Parameters.Add(":LINK_ID", pLinkId);
				cmd.Parameters.Add(":PRODUCT_MOVIE_TYPE", pProductMovieType);
				
				return iBridUtil.GetStringValue(cmd.ExecuteScalar());
			}
		}finally{
			conn.Close();
		}	
	}

	public DataSet GetListByMovieType(string pSiteCd,string pProductAgentCd,string pProductSeq,string pProductMovieType)
	{
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT						");
		oSqlBuilder.AppendLine("	VW_PRODUCT_MOVIE00.SITE_CD				,");
		oSqlBuilder.AppendLine("	VW_PRODUCT_MOVIE00.PRODUCT_AGENT_CD    ,");
		oSqlBuilder.AppendLine("	VW_PRODUCT_MOVIE00.PRODUCT_SEQ         ,");
		oSqlBuilder.AppendLine("	VW_PRODUCT_MOVIE00.OBJ_SEQ             ,");
		oSqlBuilder.AppendLine("	VW_PRODUCT_MOVIE00.PRODUCT_MOVIE_TYPE  ,");
		oSqlBuilder.AppendLine("	VW_PRODUCT_MOVIE00.OUTER_MOVIE_FLAG    ,");
		oSqlBuilder.AppendLine("	VW_PRODUCT_MOVIE00.PLAY_TIME           ,");
		oSqlBuilder.AppendLine("	VW_PRODUCT_MOVIE00.LINK_ID             ,");
		oSqlBuilder.AppendLine("	VW_PRODUCT_MOVIE00.LINK_DATE           ,");
		oSqlBuilder.AppendLine("	VW_PRODUCT_MOVIE00.UPLOAD_DATE         ,");
		oSqlBuilder.AppendLine("	VW_PRODUCT_MOVIE00.UNEDITABLE_FLAG     ,");
		oSqlBuilder.AppendLine("	VW_PRODUCT_MOVIE00.PRODUCT_ID		    ");
		oSqlBuilder.AppendLine(" FROM						");
		oSqlBuilder.AppendLine("	VW_PRODUCT_MOVIE00			");
		oSqlBuilder.AppendLine(" WHERE ");
		oSqlBuilder.AppendLine("	VW_PRODUCT_MOVIE00.SITE_CD				= :SITE_CD			AND ");
		oSqlBuilder.AppendLine("	VW_PRODUCT_MOVIE00.PRODUCT_AGENT_CD		= :PRODUCT_AGENT_CD AND ");
		oSqlBuilder.AppendLine("	VW_PRODUCT_MOVIE00.PRODUCT_SEQ			= :PRODUCT_SEQ		AND ");
		oSqlBuilder.AppendLine("	VW_PRODUCT_MOVIE00.PRODUCT_MOVIE_TYPE	= :PRODUCT_MOVIE_TYPE   ");

        DataSet oProductmovieDataSet = new DataSet();
        try
        {
            conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn))
            {
                cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":PRODUCT_AGENT_CD",pProductAgentCd);
				cmd.Parameters.Add(":PRODUCT_SEQ",pProductSeq);
				cmd.Parameters.Add(":PRODUCT_MOVIE_TYPE",pProductMovieType);

                using (da = new OracleDataAdapter(cmd))
                {
					da.Fill(oProductmovieDataSet);
                }
            }
        }
        finally
        {
            conn.Close();
        }
		return oProductmovieDataSet;
	}

    public int GetCount(string pSiteCd, string pProductAgentCd, string pProductSeq, string pProductMovieType) {
        StringBuilder oSqlBuilder = new StringBuilder();
        oSqlBuilder.AppendLine(" SELECT						");
        oSqlBuilder.AppendLine("	COUNT(1)                ");
        oSqlBuilder.AppendLine(" FROM						");
        oSqlBuilder.AppendLine("	T_PRODUCT_MOVIE			");
        oSqlBuilder.AppendLine(" WHERE                      ");
        oSqlBuilder.AppendLine("	T_PRODUCT_MOVIE.SITE_CD				= :SITE_CD			AND ");
        oSqlBuilder.AppendLine("	T_PRODUCT_MOVIE.PRODUCT_AGENT_CD	= :PRODUCT_AGENT_CD AND ");
        oSqlBuilder.AppendLine("	T_PRODUCT_MOVIE.PRODUCT_SEQ			= :PRODUCT_SEQ		AND ");
        oSqlBuilder.AppendLine("	T_PRODUCT_MOVIE.PRODUCT_MOVIE_TYPE	= :PRODUCT_MOVIE_TYPE   ");

        try {
            conn = DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
                cmd.BindByName = true;
                cmd.Parameters.Add(":SITE_CD", pSiteCd);
                cmd.Parameters.Add(":PRODUCT_AGENT_CD", pProductAgentCd);
                cmd.Parameters.Add(":PRODUCT_SEQ", pProductSeq);
                cmd.Parameters.Add(":PRODUCT_MOVIE_TYPE", pProductMovieType);

                int iCount;
                if (int.TryParse(cmd.ExecuteScalar().ToString(), out iCount)) {
                    return iCount;
                }
                return -1;
            }
        } finally {
            conn.Close();
        }
    }
}
