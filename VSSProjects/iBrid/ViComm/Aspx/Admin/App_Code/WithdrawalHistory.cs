﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 退会履歴

--	Progaram ID		: WithdrawalHistory
--
--  Creation Date	: 2016.11.02
--  Creater			: M&TT Zukeran
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;
using ViComm;

public class WithdrawalHistory:DbSession {
	string pSiteCd;
	string pUserSeq;
	string pWithdrawalHistorySeq;

	public WithdrawalHistory() {
		pSiteCd = string.Empty;
		pUserSeq = string.Empty;
		pWithdrawalHistorySeq = string.Empty;
	}

	private void SetParam(string pSiteCd,string pUserSeq,string pWithdrawalHistorySeq) {
		this.pSiteCd = pSiteCd;
		this.pUserSeq = pUserSeq;
		this.pWithdrawalHistorySeq = pWithdrawalHistorySeq;
	}

	public int GetPageCount(string pSiteCd,string pUserSeq) {
		int iPageCount = 0;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		string sWhere = string.Empty;

		oSqlBuilder.AppendLine("SELECT COUNT(*) AS ROW_COUNT FROM T_WITHDRAWAL_HISTORY");

		SetParam(pSiteCd,pUserSeq,null);
		CreateWhere(ref oParamList,ref sWhere);
		oSqlBuilder.AppendLine(sWhere);

		DataSet ds = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (ds.Tables[0].Rows.Count > 0) {
			DataRow dr = ds.Tables[0].Rows[0];
			iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
		}

		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pUserSeq,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		string sExecSql = string.Empty;
		string sWhere = string.Empty;
		string sSortExpression = string.Empty;

		oSqlBuilder.AppendLine("SELECT * FROM T_WITHDRAWAL_HISTORY");

		SetParam(pSiteCd,pUserSeq,null);
		CreateWhere(ref oParamList,ref sWhere);
		oSqlBuilder.AppendLine(sWhere);
		oSqlBuilder.AppendLine("ORDER BY CREATE_DATE DESC");

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),startRowIndex,maximumRows,out sExecSql));

		return ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
	}

	/// <summary>
	/// レコードを1件取得する
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pUserSeq"></param>
	/// <param name="pWithdrawalHistorySeq"></param>
	/// <returns></returns>
	public DataSet GetOne(string pSiteCd,string pUserSeq,string pWithdrawalHistorySeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		string sWhere = string.Empty;

		oSqlBuilder.AppendLine("SELECT * FROM T_WITHDRAWAL_HISTORY");

		SetParam(pSiteCd,pUserSeq,pWithdrawalHistorySeq);
		CreateWhere(ref oParamList,ref sWhere);
		oSqlBuilder.AppendLine(sWhere);

		return ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
	}

	/// <summary>
	/// 検索条件を生成＆設定する
	/// </summary>
	/// <param name="sSiteCd"></param>
	/// <param name="sUserSeq"></param>
	/// <param name="oParamList"></param>
	/// <param name="sWhere"></param>
	private void CreateWhere(ref List<OracleParameter> oParamList,ref string sWhere) {

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref sWhere);
			oParamList.Add(new OracleParameter("SITE_CD",pSiteCd));
		}
		if (!string.IsNullOrEmpty(pUserSeq)) {
			SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref sWhere);
			oParamList.Add(new OracleParameter("USER_SEQ",pUserSeq));
		}
		if (!string.IsNullOrEmpty(pWithdrawalHistorySeq)) {
			SysPrograms.SqlAppendWhere("WITHDRAWAL_HISTORY_SEQ = :WITHDRAWAL_HISTORY_SEQ",ref sWhere);
			oParamList.Add(new OracleParameter("WITHDRAWAL_HISTORY_SEQ",pWithdrawalHistorySeq));
		}
	}

	/// <summary>
	/// 退会履歴の追加・更新
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pUserSeq"></param>
	/// <param name="pRemarksId"></param>
	/// <param name="pWithdrawalSeq"></param>
	/// <param name="pWithdrawalHistorySeq"></param>
	/// <param name="pAddPointCnt"></param>
	/// <param name="pRemarks"></param>
	/// <param name="pCreateDate"></param>
	/// <param name="pStatus"></param>
	public void Mainte(
		string pSiteCd,
		string pUserSeq,
		string pRemarksId,
		string pWithdrawalSeq,
		ref string pWithdrawalHistorySeq,
		string pAddPointCnt,
		string pRemarks,
		string pCreateDate,
		string pDelFlag,
		out string pStatus
	) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("WITHDRAWAL_HISTORY_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			oDbSession.ProcedureInParm("pREMARKS_ID",DbSession.DbType.VARCHAR2,pRemarksId);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureInParm("pWITHDRAWAL_SEQ",DbSession.DbType.VARCHAR2,pWithdrawalSeq);
			oDbSession.ProcedureBothParm("pWITHDRAWAL_HISTORY_SEQ",DbSession.DbType.VARCHAR2,pWithdrawalHistorySeq);
			oDbSession.ProcedureInParm("pADD_POINT_CNT",DbSession.DbType.VARCHAR2,pAddPointCnt);
			oDbSession.ProcedureInParm("pREMARKS",DbSession.DbType.VARCHAR2,pRemarks);
			oDbSession.ProcedureInParm("pCREATE_DATE",DbSession.DbType.VARCHAR2,pCreateDate);
			oDbSession.ProcedureInParm("pDEL_FLAG",DbSession.DbType.VARCHAR2,pDelFlag);
			oDbSession.ExecuteProcedure();

			pWithdrawalHistorySeq = oDbSession.GetStringValue("pWITHDRAWAL_HISTORY_SEQ");
			pStatus = oDbSession.GetStringValue("pSTATUS");
		}
	}
}

