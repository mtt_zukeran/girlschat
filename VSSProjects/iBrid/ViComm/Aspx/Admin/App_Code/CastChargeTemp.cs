/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: キャスト課金設定一時保存
--	Progaram ID		: CastChargeTemp
--
--  Creation Date	: 2012.05.30
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class CastChargeTemp:DbSession {

	public CastChargeTemp() {
	}

	public int GetPageCount(string pSiteCd,string pCastChargeTempCd) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_CAST_CHARGE_TEMP01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pCastChargeTempCd,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0; i < objParms.Length; i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pCastChargeTempCd,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY  SITE_CD,ACT_CATEGORY_SEQ,DISPLAY_ORDER ";
			string sSql = "SELECT " +
							"SITE_CD				 		," +
							"SITE_NM				 		," +
							"CHARGE_TYPE			 		," +
							"CAST_CHARGE_TEMP_CD	 		," +
							"CAST_CHARGE_TEMP_CD_NM	 		," +
							"ACT_CATEGORY_SEQ		 		," +
							"CHARGE_POINT_NORMAL	 		," +
							"CHARGE_POINT_NO_RECEIPT 		," +
							"CHARGE_POINT_NORMAL2	 		," +
							"CHARGE_POINT_NO_RECEIPT2		," +
							"CHARGE_POINT_NORMAL3	 		," +
							"CHARGE_POINT_NO_RECEIPT3		," +
							"CHARGE_POINT_NORMAL4	 		," +
							"CHARGE_POINT_NO_RECEIPT4		," +
							"CHARGE_POINT_NORMAL5	 		," +
							"CHARGE_POINT_NO_RECEIPT5		," +
							"CAST_USE_TALK_APP_ADD_POINT	," +
							"MAN_USE_TALK_APP_ADD_POINT		," +
							"CHARGE_UNIT_SEC		 		," +
							"FREE_DIAL_FEE           		," +
							"UPDATE_DATE			 		," +
							"REVISION_NO			 		," +
							"CHARGE_TYPE_NM			 		," +
							"CHARGE_UNIT_TYPE		 		," +
							"DISPLAY_ORDER			 		," +
							"ACT_CATEGORY_NM		 		" +
							"FROM(" +
							" SELECT VW_CAST_CHARGE_TEMP01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_CAST_CHARGE_TEMP01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pCastChargeTempCd,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0; i < objParms.Length; i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_CHARGE_TEMP01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	public DataSet GetCsvData(string pSiteCd) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY SITE_CD,CHARGE_TYPE,ACT_CATEGORY_SEQ ";
			string sSql = "SELECT " +
							"SITE_CD				 ," +
							"CHARGE_TYPE			 ," +
							"ACT_CATEGORY_SEQ		 ," +
							"CATEGORY_INDEX			 ," +
							"CHARGE_POINT_NORMAL	 ," +
							"CHARGE_POINT_NO_RECEIPT ," +
							"CHARGE_POINT_NORMAL2	 ," +
							"CHARGE_POINT_NO_RECEIPT2," +
							"CHARGE_POINT_NORMAL3	 ," +
							"CHARGE_POINT_NO_RECEIPT3," +
							"CHARGE_POINT_NORMAL4	 ," +
							"CHARGE_POINT_NO_RECEIPT4," +
							"CHARGE_POINT_NORMAL5	 ," +
							"CHARGE_POINT_NO_RECEIPT5," +
							"CHARGE_UNIT_SEC		 ," +
							"FREE_DIAL_FEE			  " +
							"FROM " +
							 "VW_CAST_CHARGE01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql = sSql + sWhere;
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0; i < objParms.Length; i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_CHARGE01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		pWhere = pWhere + " WHERE SITE_CD = :SITE_CD  ";
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pCastChargeTempCd,ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!string.IsNullOrEmpty(pCastChargeTempCd)) {
			SysPrograms.SqlAppendWhere("CAST_CHARGE_TEMP_CD = :CAST_CHARGE_TEMP_CD",ref pWhere);
			list.Add(new OracleParameter("CAST_CHARGE_TEMP_CD",pCastChargeTempCd));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public DataSet GetList(string pSiteCd,string pActCategorySeq,string pCastChargeTempCd) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY  SITE_CD,ACT_CATEGORY_SEQ,DISPLAY_ORDER ";
			string sSql = "SELECT " +
							"SITE_CD				 ," +
							"SITE_NM				 ," +
							"CHARGE_TYPE			 ," +
							"ACT_CATEGORY_SEQ		 ," +
							"CHARGE_POINT_NORMAL	 ," +
							"CHARGE_POINT_NO_RECEIPT ," +
							"CHARGE_POINT_NORMAL2	 ," +
							"CHARGE_POINT_NO_RECEIPT2," +
							"CHARGE_POINT_NORMAL3	 ," +
							"CHARGE_POINT_NO_RECEIPT3," +
							"CHARGE_POINT_NORMAL4	 ," +
							"CHARGE_POINT_NO_RECEIPT4," +
							"CHARGE_POINT_NORMAL5	 ," +
							"CHARGE_POINT_NO_RECEIPT5," +
							"CHARGE_UNIT_SEC		 ," +
							"FREE_DIAL_FEE           ," +
							"UPDATE_DATE			 ," +
							"REVISION_NO			 ," +
							"CHARGE_TYPE_NM			 ," +
							"CHARGE_UNIT_TYPE		 ," +
							"DISPLAY_ORDER			 ," +
							"ACT_CATEGORY_NM		 " +
							"FROM VW_CAST_CHARGE_TEMP01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pActCategorySeq,pCastChargeTempCd,ref sWhere);
			sSql = sSql + sWhere;
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0; i < objParms.Length; i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_CHARGE_TEMP01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pActCategorySeq,string pCastChargeTempCd,ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!string.IsNullOrEmpty(pActCategorySeq)) {
			SysPrograms.SqlAppendWhere("ACT_CATEGORY_SEQ = :ACT_CATEGORY_SEQ",ref pWhere);
			list.Add(new OracleParameter("ACT_CATEGORY_SEQ",pActCategorySeq));
		}

		if (!string.IsNullOrEmpty(pActCategorySeq)) {
			SysPrograms.SqlAppendWhere("CAST_CHARGE_TEMP_CD = :CAST_CHARGE_TEMP_CD",ref pWhere);
			list.Add(new OracleParameter("CAST_CHARGE_TEMP_CD",pCastChargeTempCd));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public DataSet GetList2(string pSiteCd,string pActCategorySeq) {
		StringBuilder oSql = new StringBuilder();
		oSql.AppendLine("SELECT									");
		oSql.AppendLine("	SITE_CD							,	");
		oSql.AppendLine("	ACT_CATEGORY_SEQ				,	");
		oSql.AppendLine("	ACT_CATEGORY_NM					,	");
		oSql.AppendLine("	CAST_CHARGE_TEMP_CD				,	");
		oSql.AppendLine("	CAST_CHARGE_TEMP_NM					");
		oSql.AppendLine("FROM									");
		oSql.AppendLine("	VW_CAST_CHARGE_TEMP_BIND01			");
		oSql.AppendLine("WEHRE									");
		oSql.AppendLine("	SITE_CD				= :SITE_CD			AND	");
		oSql.AppendLine("	ACT_CATEGORY_SEQ	= :ACT_CATEGORY_SEQ	");
		oSql.AppendLine("GROUP BY								");
		oSql.AppendLine("	SITE_CD							,	");
		oSql.AppendLine("	ACT_CATEGORY_SEQ				,	");
		oSql.AppendLine("	ACT_CATEGORY_NM					,	");
		oSql.AppendLine("	CAST_CHARGE_TEMP_CD				,	");
		oSql.AppendLine("	CAST_CHARGE_TEMP_NM					");
		oSql.AppendLine("ORDER BY SITE_CD,ACT_CATEGORY_SEQ,CAST_CHARGE_TEMP_CD");

		try {
			conn = this.DbConnect();

			using (cmd = this.CreateSelectCommand(oSql.ToString(),conn))
			using (da = new OracleDataAdapter(cmd))
			using (DataSet ds = new DataSet()) {
				da.Fill(ds);
				return ds;
			}
		} finally {
			conn.Close();
		}
	}

}
