﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: N月登録者売上レポート
--	Progaram ID		: RegistMonthPointReport
--  Creation Date	: 2011.08.10
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;

public class RegistMonthPointReport : DbSession {
	public RegistMonthPointReport() {
	}

	public DataSet GetListReport(string pSiteCd,string pRegistMonth,string pSexCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	MKR.SITE_CD						,");
		oSqlBuilder.AppendLine("	RMPR.REGIST_MONTH				,");
		oSqlBuilder.AppendLine("	RMPR.REPORT_MONTH				,");
		oSqlBuilder.AppendLine("	MKR.REGIST_COUNT				,");
		oSqlBuilder.AppendLine("	RMPR.USED_POINT_COUNT_UNIQUE	,");
		oSqlBuilder.AppendLine("	RMPR.USED_POINT					,");
		oSqlBuilder.AppendLine("	RMPR.USED_POINT_SUM				,");
		oSqlBuilder.AppendLine("	CASE WHEN MKR.REGIST_COUNT = 0 THEN 0 ELSE TRUNC(RMPR.USED_POINT / MKR.REGIST_COUNT, 2) END POINT_PER_REGIST_COUNT,");
		oSqlBuilder.AppendLine("	CASE WHEN MKR.REGIST_COUNT = 0 THEN 0 ELSE TRUNC(RMPR.USED_POINT_SUM / MKR.REGIST_COUNT, 2) END POINT_SUM_PER_REGIST_COUNT,");
		oSqlBuilder.AppendLine("	CASE WHEN RMPR.USED_POINT_COUNT_UNIQUE = 0 THEN 0 ELSE TRUNC(RMPR.USED_POINT / RMPR.USED_POINT_COUNT_UNIQUE, 2) END ARPPU");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_MONTHLY_KPI_REPORT MKR	,");
		oSqlBuilder.AppendLine("	(	");
		oSqlBuilder.AppendLine("	SELECT	");
		oSqlBuilder.AppendLine("		RMPR1.SITE_CD					,");
		oSqlBuilder.AppendLine("		RMPR1.SEX_CD					,");
		oSqlBuilder.AppendLine("		RMPR1.REGIST_MONTH				,");
		oSqlBuilder.AppendLine("		RMPR1.REPORT_MONTH				,");
		oSqlBuilder.AppendLine("		RMPR1.USED_POINT				,");
		oSqlBuilder.AppendLine("		RMPR1.USED_POINT_COUNT_UNIQUE	,");
		oSqlBuilder.AppendLine("		SUM (CASE WHEN RMPR2.REPORT_MONTH < RMPR1.REPORT_MONTH THEN RMPR2.USED_POINT ELSE 0 END) + RMPR1.USED_POINT USED_POINT_SUM");
		oSqlBuilder.AppendLine("	FROM	");
		oSqlBuilder.AppendLine("		T_REGIST_MONTH_POINT_REPORT RMPR1	,");
		oSqlBuilder.AppendLine("		T_REGIST_MONTH_POINT_REPORT RMPR2");
		oSqlBuilder.AppendLine("	WHERE	");
		oSqlBuilder.AppendLine("		RMPR1.SITE_CD		= RMPR2.SITE_CD			AND");
		oSqlBuilder.AppendLine("		RMPR1.REGIST_MONTH	= RMPR2.REGIST_MONTH	AND");
		oSqlBuilder.AppendLine("		RMPR1.SEX_CD		= RMPR2.SEX_CD			AND");
		oSqlBuilder.AppendLine("		RMPR1.SITE_CD		= :SITE_CD				AND");
		oSqlBuilder.AppendLine("		RMPR1.REGIST_MONTH	= :REGIST_MONTH			AND");
		oSqlBuilder.AppendLine("		RMPR1.SEX_CD		= :SEX_CD				");
		oSqlBuilder.AppendLine("	GROUP BY RMPR1.SITE_CD, RMPR1.REPORT_MONTH, RMPR1.REGIST_MONTH, RMPR1.USED_POINT, RMPR1.SEX_CD, RMPR1.USED_POINT_COUNT_UNIQUE");
		oSqlBuilder.AppendLine("	) RMPR	");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	MKR.SITE_CD			= RMPR.SITE_CD		AND");
		oSqlBuilder.AppendLine("	MKR.SEX_CD			= RMPR.SEX_CD		AND");
		oSqlBuilder.AppendLine("	MKR.REPORT_MONTH	= RMPR.REGIST_MONTH	");
		oSqlBuilder.AppendLine("ORDER BY MKR.SITE_CD, RMPR.REPORT_MONTH	");

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":REGIST_MONTH",pRegistMonth);
				cmd.Parameters.Add(":SEX_CD",pSexCd);

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}
}
