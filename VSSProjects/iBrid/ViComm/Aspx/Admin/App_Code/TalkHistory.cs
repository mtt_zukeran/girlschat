﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 会話履歴
--	Progaram ID		: TalkHistory
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;
using ViComm;

public class TalkHistory:DbSession {

	public TalkHistory() {
	}

	public void GetTalkCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pCastSeq,string pCastCharNo,out int pTalkCount,out string pLastTalkDay) {

		string sTalkHistory = "SELECT NVL(COUNT(*),0) AS CNT, TO_CHAR(MAX(TALK_START_DATE),'YY/MM/DD HH24:MI') AS LAST_DATE  FROM T_TALK_HISTORY " +
			   "WHERE " +
					"SITE_CD				= :SITE_CD			AND " +
					"USER_SEQ				= :USER_SEQ			AND " +
					"USER_CHAR_NO			= :USER_CHAR_NO		AND " +
					"PARTNER_USER_SEQ		= :PARTNER_USER_SEQ	AND	" +
					"PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO";

		pTalkCount = 0;
		pLastTalkDay = "";
		try {
			conn = DbConnect();

			using (DataSet ds = new DataSet())
			using (cmd = CreateSelectCommand(sTalkHistory,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				cmd.Parameters.Add("PARTNER_USER_SEQ",pCastSeq);
				cmd.Parameters.Add("PARTNER_USER_CHAR_NO",pCastCharNo);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_TALK_HISTORY");
					if (ds.Tables[0].Rows.Count != 0) {
						DataRow dr = ds.Tables[0].Rows[0];
						pTalkCount = int.Parse(dr["CNT"].ToString());
						pLastTalkDay = dr["LAST_DATE"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
	}
}
