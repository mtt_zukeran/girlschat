﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 日別KPIレポート
--	Progaram ID		: DailyKpiReport
--  Creation Date	: 2011.08.10
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;

public class DailyKpiReport : DbSession {
	public DailyKpiReport() {
	}

	public DataSet GetListServiseReport(string pSiteCd,string pReportMonth,string pSexCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	SITE_CD					,");
		oSqlBuilder.AppendLine("	SEX_CD					,");
		oSqlBuilder.AppendLine("	REPORT_DAY				,");
		oSqlBuilder.AppendLine("	PAGE_VIEW_COUNT			,");
		oSqlBuilder.AppendLine("	PAGE_VIEW_UNIQUE_COUNT	,");
		oSqlBuilder.AppendLine("	REGIST_COUNT			,");
		oSqlBuilder.AppendLine("	WITHDRAWAL_COUNT		,");
		oSqlBuilder.AppendLine("	USED_POINT				,");
		oSqlBuilder.AppendLine("	USED_POINT_COUNT		,");
		oSqlBuilder.AppendLine("	USED_POINT_COUNT_UNIQUE	,");
		oSqlBuilder.AppendLine("	CASE WHEN PAGE_VIEW_UNIQUE_COUNT = 0 THEN 0 ELSE TRUNC(USED_POINT_COUNT_UNIQUE / PAGE_VIEW_UNIQUE_COUNT * 100,2) END CONSUMPTION_RATE	,");
		oSqlBuilder.AppendLine("	CASE WHEN USED_POINT_COUNT_UNIQUE = 0 THEN 0 ELSE TRUNC(USED_POINT / USED_POINT_COUNT_UNIQUE,2) END ARPPU	,");
		oSqlBuilder.AppendLine("	CASE WHEN PAGE_VIEW_UNIQUE_COUNT = 0 THEN 0 ELSE TRUNC(USED_POINT / PAGE_VIEW_UNIQUE_COUNT,2) END ARPU	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_DAILY_KPI_REPORT		");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD		AND");
		oSqlBuilder.AppendLine("	SEX_CD			= :SEX_CD		AND");
		oSqlBuilder.AppendLine("	TO_CHAR(TO_DATE(REPORT_DAY,'YYYY/MM/DD'),'YYYY/MM') = :REPORT_MONTH	");
		oSqlBuilder.AppendLine("ORDER BY SITE_CD, REPORT_DAY	");

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":REPORT_MONTH",pReportMonth);
				cmd.Parameters.Add(":SEX_CD",pSexCd);

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}
}
