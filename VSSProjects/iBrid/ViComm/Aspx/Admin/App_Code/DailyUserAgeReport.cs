﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 日別ﾕｰｻﾞ年齢別ﾚﾎﾟｰﾄ
--	Progaram ID		: DailyUserAgeReport
--  Creation Date	: 2011.08.10
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;

public class DailyUserAgeReport : DbSession {
	public DailyUserAgeReport() {
	}

	public DataSet GetListPageViewReport(string pSiteCd,string pSexCd,string pReportDayFrom,string pReportDayTo) {
		string sWhereClause;
		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pSexCd,pReportDayFrom,pReportDayTo,out sWhereClause);

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	SITE_CD		,");
		oSqlBuilder.AppendLine("	SEX_CD		,");
		oSqlBuilder.AppendLine("	REPORT_DAY	,");
		oSqlBuilder.AppendLine("	SUM(PAGE_VIEW_COUNT) COUNT_ALL	,");
		oSqlBuilder.AppendLine("	SUM(CASE WHEN 18 <= AGE AND AGE < 20 THEN PAGE_VIEW_COUNT ELSE 0 END) COUNT_18		,");
		oSqlBuilder.AppendLine("	SUM(CASE WHEN 20 <= AGE AND AGE < 25 THEN PAGE_VIEW_COUNT ELSE 0 END) COUNT_20		,");
		oSqlBuilder.AppendLine("	SUM(CASE WHEN 25 <= AGE AND AGE < 30 THEN PAGE_VIEW_COUNT ELSE 0 END) COUNT_25		,");
		oSqlBuilder.AppendLine("	SUM(CASE WHEN 30 <= AGE				 THEN PAGE_VIEW_COUNT ELSE 0 END) COUNT_30		,");
		oSqlBuilder.AppendLine("	SUM(CASE WHEN AGE = 0				 THEN PAGE_VIEW_COUNT ELSE 0 END) COUNT_OTHER	,");
		oSqlBuilder.AppendLine("	SUM(PAGE_VIEW_UNIQUE_COUNT) COUNT_UNIQUE_ALL									,");
		oSqlBuilder.AppendLine("	SUM(CASE WHEN 18 <= AGE AND AGE < 20 THEN PAGE_VIEW_UNIQUE_COUNT ELSE 0 END) COUNT_UNIQUE_18		,");
		oSqlBuilder.AppendLine("	SUM(CASE WHEN 20 <= AGE AND AGE < 25 THEN PAGE_VIEW_UNIQUE_COUNT ELSE 0 END) COUNT_UNIQUE_20		,");
		oSqlBuilder.AppendLine("	SUM(CASE WHEN 25 <= AGE AND AGE < 30 THEN PAGE_VIEW_UNIQUE_COUNT ELSE 0 END) COUNT_UNIQUE_25		,");
		oSqlBuilder.AppendLine("	SUM(CASE WHEN 30 <= AGE				 THEN PAGE_VIEW_UNIQUE_COUNT ELSE 0 END) COUNT_UNIQUE_30		,");
		oSqlBuilder.AppendLine("	SUM(CASE WHEN AGE = 0				 THEN PAGE_VIEW_UNIQUE_COUNT ELSE 0 END) COUNT_UNIQUE_OTHER	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_DAILY_USER_AGE_REPORT	");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("GROUP BY SITE_CD, REPORT_DAY, SEX_CD	");
		oSqlBuilder.AppendLine("ORDER BY SITE_CD, REPORT_DAY	");

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
					return this.AddSummaryRow(oDataSet);
				}
			}
		} finally {
			conn.Close();
		}
	}

	public DataSet GetListUsedPointReport(string pSiteCd,string pSexCd,string pReportDayFrom,string pReportDayTo) {
		string sWhereClause;
		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pSexCd,pReportDayFrom,pReportDayTo,out sWhereClause);

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	SITE_CD		,");
		oSqlBuilder.AppendLine("	SEX_CD		,");
		oSqlBuilder.AppendLine("	REPORT_DAY	,");
		oSqlBuilder.AppendLine("	SUM(USED_POINT) COUNT_ALL	,");
		oSqlBuilder.AppendLine("	SUM(CASE WHEN 18 <= AGE AND AGE < 20 THEN USED_POINT ELSE 0 END) COUNT_18		,");
		oSqlBuilder.AppendLine("	SUM(CASE WHEN 20 <= AGE AND AGE < 25 THEN USED_POINT ELSE 0 END) COUNT_20		,");
		oSqlBuilder.AppendLine("	SUM(CASE WHEN 25 <= AGE AND AGE < 30 THEN USED_POINT ELSE 0 END) COUNT_25		,");
		oSqlBuilder.AppendLine("	SUM(CASE WHEN 30 <= AGE				 THEN USED_POINT ELSE 0 END) COUNT_30		,");
		oSqlBuilder.AppendLine("	SUM(CASE WHEN AGE = 0				 THEN USED_POINT ELSE 0 END) COUNT_OTHER	,");
		oSqlBuilder.AppendLine("	SUM(USED_POINT_COUNT_UNIQUE) COUNT_UNIQUE_ALL									,");
		oSqlBuilder.AppendLine("	SUM(CASE WHEN 18 <= AGE AND AGE < 20 THEN USED_POINT_COUNT_UNIQUE ELSE 0 END) COUNT_UNIQUE_18		,");
		oSqlBuilder.AppendLine("	SUM(CASE WHEN 20 <= AGE AND AGE < 25 THEN USED_POINT_COUNT_UNIQUE ELSE 0 END) COUNT_UNIQUE_20		,");
		oSqlBuilder.AppendLine("	SUM(CASE WHEN 25 <= AGE AND AGE < 30 THEN USED_POINT_COUNT_UNIQUE ELSE 0 END) COUNT_UNIQUE_25		,");
		oSqlBuilder.AppendLine("	SUM(CASE WHEN 30 <= AGE				 THEN USED_POINT_COUNT_UNIQUE ELSE 0 END) COUNT_UNIQUE_30		,");
		oSqlBuilder.AppendLine("	SUM(CASE WHEN AGE = 0				 THEN USED_POINT_COUNT_UNIQUE ELSE 0 END) COUNT_UNIQUE_OTHER	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_DAILY_USER_AGE_REPORT	");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("GROUP BY SITE_CD, REPORT_DAY, SEX_CD	");
		oSqlBuilder.AppendLine("ORDER BY SITE_CD, REPORT_DAY	");

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
					return this.AddSummaryRow(oDataSet);
				}
			}
		} finally {
			conn.Close();
		}
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pSexCd,string pReportDayFrom,string pReportDayTo,out string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD ",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!string.IsNullOrEmpty(pSexCd)) {
			SysPrograms.SqlAppendWhere("SEX_CD = :SEX_CD ",ref pWhere);
			list.Add(new OracleParameter("SEX_CD",pSexCd));
		}

		if (!string.IsNullOrEmpty(pReportDayFrom) && !string.IsNullOrEmpty(pReportDayTo)) {
			SysPrograms.SqlAppendWhere("REPORT_DAY >= :REPORT_DAY_FROM AND REPORT_DAY <= :REPORT_DAY_TO ",ref pWhere);
			list.Add(new OracleParameter("REPORT_DAY_FROM",pReportDayFrom));
			list.Add(new OracleParameter("REPORT_DAY_TO",pReportDayTo));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	private DataSet AddSummaryRow(DataSet pDataSet) {
		if (pDataSet == null) {
			return pDataSet;
		}

		DataTable oDataTable = pDataSet.Tables[0];

		if (oDataTable.Rows.Count == 0) {
			return pDataSet;
		}

		DataRow oDataRowSummary = oDataTable.NewRow();
		DataRow oDataRowRate = oDataTable.NewRow();

		int iCountAll = 0;
		int iCount18 = 0;
		int iCount20 = 0;
		int iCount25 = 0;
		int iCount30 = 0;
		int iCountOther = 0;
		int iCountUniqueAll = 0;
		int iCountUnique18 = 0;
		int iCountUnique20 = 0;
		int iCountUnique25 = 0;
		int iCountUnique30 = 0;
		int iCountUniqueOther = 0;

		foreach (DataRow row in oDataTable.Rows) {
			oDataRowSummary["SITE_CD"] = row["SITE_CD"].ToString();
			oDataRowSummary["SEX_CD"] = row["SEX_CD"].ToString();
			iCountAll += int.Parse(row["COUNT_ALL"].ToString());
			iCount18 += int.Parse(row["COUNT_18"].ToString());
			iCount20 += int.Parse(row["COUNT_20"].ToString());
			iCount25 += int.Parse(row["COUNT_25"].ToString());
			iCount30 += int.Parse(row["COUNT_30"].ToString());
			iCountOther += int.Parse(row["COUNT_OTHER"].ToString());
			iCountUniqueAll += int.Parse(row["COUNT_UNIQUE_ALL"].ToString());
			iCountUnique18 += int.Parse(row["COUNT_UNIQUE_18"].ToString());
			iCountUnique20 += int.Parse(row["COUNT_UNIQUE_20"].ToString());
			iCountUnique25 += int.Parse(row["COUNT_UNIQUE_25"].ToString());
			iCountUnique30 += int.Parse(row["COUNT_UNIQUE_30"].ToString());
			iCountUniqueOther += int.Parse(row["COUNT_UNIQUE_OTHER"].ToString());
			oDataRowRate["SITE_CD"] = row["SITE_CD"].ToString();
			oDataRowRate["SEX_CD"] = row["SEX_CD"].ToString();
		}
		
		oDataRowSummary["REPORT_DAY"] = "総計";
		oDataRowSummary["COUNT_ALL"] = iCountAll;
		oDataRowSummary["COUNT_18"] = iCount18;
		oDataRowSummary["COUNT_20"] = iCount20;
		oDataRowSummary["COUNT_25"] = iCount25;
		oDataRowSummary["COUNT_30"] = iCount30;
		oDataRowSummary["COUNT_OTHER"] = iCountOther;
		oDataRowSummary["COUNT_UNIQUE_ALL"] = iCountUniqueAll;
		oDataRowSummary["COUNT_UNIQUE_18"] = iCountUnique18;
		oDataRowSummary["COUNT_UNIQUE_20"] = iCountUnique20;
		oDataRowSummary["COUNT_UNIQUE_25"] = iCountUnique25;
		oDataRowSummary["COUNT_UNIQUE_30"] = iCountUnique30;
		oDataRowSummary["COUNT_UNIQUE_OTHER"] = iCountUniqueOther;
		oDataTable.Rows.InsertAt(oDataRowSummary,oDataTable.Rows.Count);

		oDataRowRate["REPORT_DAY"] = "シェア率";
		oDataRowRate["COUNT_ALL"] = 100;
		oDataRowRate["COUNT_18"] = iCountAll == 0 ? 0 : iCount18 * 100 / iCountAll;
		oDataRowRate["COUNT_20"] = iCountAll == 0 ? 0 : iCount20 * 100 / iCountAll;
		oDataRowRate["COUNT_25"] = iCountAll == 0 ? 0 : iCount25 * 100 / iCountAll;
		oDataRowRate["COUNT_30"] = iCountAll == 0 ? 0 : iCount30 * 100 / iCountAll;
		oDataRowRate["COUNT_OTHER"] = iCountAll == 0 ? 0 : iCountOther * 100 / iCountAll;
		oDataRowRate["COUNT_UNIQUE_ALL"] = 100;
		oDataRowRate["COUNT_UNIQUE_18"] = iCountUniqueAll == 0 ? 0 : iCountUnique18 * 100 / iCountUniqueAll;
		oDataRowRate["COUNT_UNIQUE_20"] = iCountUniqueAll == 0 ? 0 : iCountUnique20 * 100 / iCountUniqueAll;
		oDataRowRate["COUNT_UNIQUE_25"] = iCountUniqueAll == 0 ? 0 : iCountUnique25 * 100 / iCountUniqueAll;
		oDataRowRate["COUNT_UNIQUE_30"] = iCountUniqueAll == 0 ? 0 : iCountUnique30 * 100 / iCountUniqueAll;
		oDataRowRate["COUNT_UNIQUE_OTHER"] = iCountUniqueAll == 0 ? 0 : iCountUniqueOther * 100 / iCountUniqueAll;

		oDataTable.Rows.InsertAt(oDataRowRate,oDataTable.Rows.Count);

		return pDataSet;
	}
}
