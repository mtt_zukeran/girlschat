﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 商品ｵｰｸｼｮﾝ入札履歴

--	Progaram ID		: ProductAuctionBidLog
--
--  Creation Date	: 2011.06.20
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class ProductAuctionEntryLog:DbSession {
	#region □■□ inner class(SearchCondition) □■□ ================================================================
	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}

		private string productAgentCd;
		public string ProductAgentCd {
			get {
				return this.productAgentCd;
			}
			set {
				this.productAgentCd = value;
			}
		}

		private string productSeq;
		public string ProductSeq {
			get {
				return this.productSeq;
			}
			set {
				this.productSeq = value;
			}
		}

		private string userSeq;
		public string UserSeq {
			get {
				return this.userSeq;
			}
			set {
				this.userSeq = value;
			}
		}

		private string userCharNo;
		public string UserCharNo {
			get {
				return this.userCharNo;
			}
			set {
				this.userCharNo = value;
			}
		}

	}
	#endregion ========================================================================================================

	public ProductAuctionEntryLog() {}

	public int GetPageCount(object pSearchCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT								").AppendLine();
		oSqlBuilder.Append("	COUNT(*)						").AppendLine();
		oSqlBuilder.Append(" FROM								").AppendLine();
		oSqlBuilder.Append("	VW_PRODUCT_AUCTION_ENTRY_LOG01	").AppendLine();

		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = CreateWhere((ProductAuctionEntryLog.SearchCondition)pSearchCondition,ref sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(object pSearchCondition,int startRowIndex,int maximumRows) {

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	VW_PRODUCT_AUCTION_ENTRY_LOG01.SITE_CD					,");
		oSqlBuilder.AppendLine("	VW_PRODUCT_AUCTION_ENTRY_LOG01.PRODUCT_AGENT_CD			,");
		oSqlBuilder.AppendLine("	VW_PRODUCT_AUCTION_ENTRY_LOG01.PRODUCT_SEQ				,");
		oSqlBuilder.AppendLine("	VW_PRODUCT_AUCTION_ENTRY_LOG01.USER_SEQ					,");
		oSqlBuilder.AppendLine("	VW_PRODUCT_AUCTION_ENTRY_LOG01.USER_CHAR_NO				,");
		oSqlBuilder.AppendLine("	VW_PRODUCT_AUCTION_ENTRY_LOG01.ENTRY_POINT				,");
		oSqlBuilder.AppendLine("	VW_PRODUCT_AUCTION_ENTRY_LOG01.TOTAL_BID_POINT			,");
		oSqlBuilder.AppendLine("	VW_PRODUCT_AUCTION_ENTRY_LOG01.REMAINING_RETURN_POINT	,");
		oSqlBuilder.AppendLine("	VW_PRODUCT_AUCTION_ENTRY_LOG01.REGIST_DATE				,");
		oSqlBuilder.AppendLine("	VW_PRODUCT_AUCTION_ENTRY_LOG01.LOGIN_ID					,");
		oSqlBuilder.AppendLine("	VW_PRODUCT_AUCTION_ENTRY_LOG01.HANDLE_NM				 ");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	VW_PRODUCT_AUCTION_ENTRY_LOG01	 ");

		string sWhereClause = string.Empty;
		string sSortExpression = " ORDER BY VW_PRODUCT_AUCTION_ENTRY_LOG01.REGIST_DATE DESC, VW_PRODUCT_AUCTION_ENTRY_LOG01.USER_SEQ ASC ";
		string sPagingSql = string.Empty;

		OracleParameter[] oWhereParams = CreateWhere((ProductAuctionEntryLog.SearchCondition)pSearchCondition,ref sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oProductDataSet = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oProductDataSet);
				}
			}
		} finally {
			conn.Close();
		}
		return oProductDataSet;
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhere) {
		List<OracleParameter> oOracleParameterList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(":SITE_CD = SITE_CD",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("SITE_CD",pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.ProductAgentCd)) {
			SysPrograms.SqlAppendWhere(":PRODUCT_AGENT_CD = PRODUCT_AGENT_CD",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("PRODUCT_AGENT_CD",pSearchCondition.ProductAgentCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.ProductSeq)) {
			SysPrograms.SqlAppendWhere(":PRODUCT_SEQ = PRODUCT_SEQ",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("PRODUCT_SEQ",pSearchCondition.ProductSeq));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.UserSeq)) {
			SysPrograms.SqlAppendWhere(":USER_SEQ = USER_SEQ",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("USER_SEQ",pSearchCondition.UserSeq));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.UserCharNo)) {
			SysPrograms.SqlAppendWhere(":USER_CHAR_NO = USER_CHAR_NO",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("USER_CHAR_NO",pSearchCondition.UserCharNo));
		}

		return oOracleParameterList.ToArray();
	}
}
