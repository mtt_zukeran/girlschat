﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: リアルタイムレポート


--	Progaram ID		: RealTimeReport
--  Creation Date	: 2011.08.10
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;

public class RealTimeReport : DbSession {
	public RealTimeReport() {
	}

	public DataSet GetList(string pSiteCd,string pReportDay,string pSexCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	REPORT_DAY				,");
		oSqlBuilder.AppendLine("	REPORT_HOUR				,");
		oSqlBuilder.AppendLine("	LOGIN_COUNT				,");
		oSqlBuilder.AppendLine("	'' LOGIN_COUNT_HOUR_SUM	,");
		oSqlBuilder.AppendLine("	USED_GAME_POINT			,");
		oSqlBuilder.AppendLine("	USED_POINT				,");
		oSqlBuilder.AppendLine("	BATTLE_COUNT			,");
		oSqlBuilder.AppendLine("	MISSION_COUNT			,");
		oSqlBuilder.AppendLine("	TREASURE_GET_COUNT		");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_REAL_TIME_REPORT	");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	SITE_CD		= :SITE_CD		AND");
		oSqlBuilder.AppendLine("	SEX_CD		= :SEX_CD		AND");
		oSqlBuilder.AppendLine("	REPORT_DAY	= :REPORT_DAY	");
		oSqlBuilder.AppendLine("ORDER BY SITE_CD, REPORT_DAY, REPORT_HOUR");

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":REPORT_DAY",pReportDay);
				cmd.Parameters.Add(":SEX_CD",pSexCd);

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);

					int iSumLoginCount = 0;
					DataTable oDataTable = oDataSet.Tables[0];
					oDataTable.PrimaryKey = new DataColumn[] { oDataTable.Columns["REPORT_DAY"],oDataTable.Columns["REPORT_HOUR"] };

					for (int i = 0; i < 24; i++) {
						DataRow oDataRow = oDataTable.Rows.Find(new string[] { pReportDay,i.ToString("00") });
						if (oDataRow == null) {
							oDataRow = oDataTable.NewRow();
							oDataRow["REPORT_DAY"] = pReportDay;
							oDataRow["REPORT_HOUR"] = i.ToString("00");
							oDataRow["LOGIN_COUNT"] = 0;
							oDataRow["LOGIN_COUNT_HOUR_SUM"] = iSumLoginCount;
							oDataRow["USED_GAME_POINT"] = 0;
							oDataRow["USED_POINT"] = 0;
							oDataRow["BATTLE_COUNT"] = 0;
							oDataRow["MISSION_COUNT"] = 0;
							oDataRow["TREASURE_GET_COUNT"] = 0;
							oDataTable.Rows.InsertAt(oDataRow,i);
						} else {
							iSumLoginCount += int.Parse(iBridUtil.GetStringValue(oDataRow["LOGIN_COUNT"])) ;
							oDataRow["LOGIN_COUNT_HOUR_SUM"] = iSumLoginCount;
						}
					}
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}
}
