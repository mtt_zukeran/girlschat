﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 日別アフリエート売上集計 
--	Progaram ID		: DailyAffiliateSales
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;
using ViComm;

public class DailyAffiliateSales:DbSession {

	public DailyAffiliateSales() {
	}


	public DataSet DailyAffiliateSalesInquiry(string pSiteCd,string pYYYY,string pMM) {

		DataColumn col;
		DataSet ds = new DataSet();
		DataTable dt = new DataTable();

		col = new DataColumn("SITE_CD",System.Type.GetType("System.String"));
		dt.Columns.Add(col);

		col = new DataColumn("SALES_DAY",System.Type.GetType("System.String"));
		dt.Columns.Add(col);

		col = new DataColumn("PRINT_DAY",System.Type.GetType("System.String"));
		dt.Columns.Add(col);

		for (int i = 0;i < ViCommConst.MAX_AF_SETTLE_COMPANY;i++) {
			col = new DataColumn(string.Format("SALES_COUNT{0}",i + 1),System.Type.GetType("System.String"));
			dt.Columns.Add(col);

			col = new DataColumn(string.Format("SALES_AMT{0}",i + 1),System.Type.GetType("System.String"));
			dt.Columns.Add(col);
		}
		col = new DataColumn("SALES_DAY_OF_WEEK",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("TOTAL_SALES_COUNT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("TOTAL_SALES_AMT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("AFFILIATE_SALES_INQUIRY_DAY");
			db.ProcedureInParm("PSITE_CD",DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PYYYY",DbType.VARCHAR2,pYYYY);
			db.ProcedureInParm("PMM",DbType.VARCHAR2,pMM);
			db.ProcedureOutArrayParm("PSALES_DAY",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_DAY_OF_WEEK",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSETTLE_COMPANY_CD",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSETTLE_COMPANY_NM",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_COUNT01",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_COUNT02",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_COUNT03",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_COUNT04",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_COUNT05",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_COUNT06",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_COUNT07",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_COUNT08",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_COUNT09",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_COUNT10",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_COUNT11",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_COUNT12",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_COUNT13",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_COUNT14",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_COUNT15",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_COUNT16",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_COUNT17",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_COUNT18",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_COUNT19",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_COUNT20",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_AMT01",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_AMT02",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_AMT03",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_AMT04",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_AMT05",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_AMT06",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_AMT07",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_AMT08",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_AMT09",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_AMT10",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_AMT11",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_AMT12",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_AMT13",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_AMT14",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_AMT15",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_AMT16",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_AMT17",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_AMT18",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_AMT19",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PSALES_AMT20",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutParm("PCOMPANY_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();

			for (int i = 0;i < db.GetIntValue("PRECORD_COUNT");i++) {
				string sDay = db.GetArryStringValue("PSALES_DAY",i);
				DataRow newRow = dt.NewRow();
				newRow["SITE_CD"] = pSiteCd;
				newRow["SALES_DAY"] = sDay;
				newRow["PRINT_DAY"] = sDay.Substring(8,2) + "日";
				newRow["SALES_DAY_OF_WEEK"] = db.GetArryStringValue("PSALES_DAY_OF_WEEK",i);
				int iTotalCnt = 0,iTotalAmt = 0;
				for (int j = 0;j < ViCommConst.MAX_AF_SETTLE_COMPANY;j++) {
					newRow[string.Format("SALES_COUNT{0}",j + 1)] = db.GetArryStringValue(string.Format("PSALES_COUNT{0:D2}",j + 1),i);
					newRow[string.Format("SALES_AMT{0}",j + 1)] = db.GetArryStringValue(string.Format("PSALES_AMT{0:D2}",j + 1),i);
					iTotalCnt += int.Parse(db.GetArryStringValue(string.Format("PSALES_COUNT{0:D2}",j + 1),i));
					iTotalAmt += int.Parse(db.GetArryStringValue(string.Format("PSALES_AMT{0:D2}",j + 1),i));
				}
				newRow["TOTAL_SALES_COUNT"] = iTotalCnt.ToString();
				newRow["TOTAL_SALES_AMT"] = iTotalAmt.ToString();
				dt.Rows.Add(newRow);
			}
		}
		ds.Tables.Add(dt);
		return ds;
	}

}
