﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public class AdminConfigs {
	public AdminConfigs() {	}
	
	/// <summary>
	/// 動画変換登録画面が利用可能かどうかを示す設定値を取得する
	/// </summary>
	public static bool EnabledMovieConvert {
		get{
			bool bResult = false;
			string sConfgValue = ConfigurationManager.AppSettings["EnabledMovieConvert"];
			if(!bool.TryParse(sConfgValue,out bResult)){
				bResult = false;
			}
			return bResult;
		}
	}
	
	/// <summary>
	/// 動画変換登録対象の動画ファイルを格納するディレクトリの設定値を取得する。
	/// </summary>
	public static string MovieConvertTargetDir {
		get {
			return ConfigurationManager.AppSettings["MovieConvertTargetDir"];			
		}
	}
}
