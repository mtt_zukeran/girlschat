﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: ステージグループ--	Progaram ID		: Stage
--
--  Creation Date	: 2011.07.27
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class StageGroup : DbSession {
	public StageGroup() { }

	public DataSet GetList(string pSiteCd,string pSexCd) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT					");
		oSqlBuilder.AppendLine("	SITE_CD				,");
		oSqlBuilder.AppendLine("	STAGE_GROUP_TYPE	,");
		oSqlBuilder.AppendLine("	STAGE_GROUP_NM		,");
        oSqlBuilder.AppendLine("	SEX_CD				,");
        oSqlBuilder.AppendLine("	UPDATE_DATE			");
		oSqlBuilder.AppendLine("FROM					");
		oSqlBuilder.AppendLine("	T_STAGE_GROUP		");
		oSqlBuilder.AppendLine("WHERE					");
		oSqlBuilder.AppendLine("	SITE_CD		= :SITE_CD	AND");
		oSqlBuilder.AppendLine("	SEX_CD		= :SEX_CD	");
		oSqlBuilder.AppendLine("ORDER BY SITE_CD, TO_NUMBER(STAGE_GROUP_TYPE)	");

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":SEX_CD",pSexCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		}
		finally {
			conn.Close();
		}
		return oDs;
	}
}
