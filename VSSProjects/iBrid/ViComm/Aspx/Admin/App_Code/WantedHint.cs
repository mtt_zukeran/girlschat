﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: この娘を探せ・ヒント

--	Progaram ID		: WantedHint
--  Creation Date	: 2011.03.23
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;

public class WantedHint : DbSession {
	public WantedHint() {
	}

	public DataSet GetList(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	SITE_CD				,");
		oSqlBuilder.AppendLine("	DAY_CD				,");
		oSqlBuilder.AppendLine("	PRIORITY			,");
		oSqlBuilder.AppendLine("	CAST_ATTR_TYPE_SEQ	,");
		oSqlBuilder.AppendLine("	REVISION_NO	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_WANTED_HINT	");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	SITE_CD		= :SITE_CD	");
		oSqlBuilder.AppendLine("ORDER BY	");
		oSqlBuilder.AppendLine("	SITE_CD, DAY_CD, PRIORITY ");
		DataSet oDataSet = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(), conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD", pSiteCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
				}
			}
		} finally {
			conn.Close();
		}
		return oDataSet;
	}
}
