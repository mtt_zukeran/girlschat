﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Withdrawal
--	Title			: 退会申請 
--	Progaram ID		: Pack
--
--  Creation Date	: 2009.12.11
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater   Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Text;
using ViComm;
using iBridCommLib;

public class Withdrawal:DbSession {

	public string withdrawalStatus;
	public string withdrawalReasonCd;
	public string withdrawalReasonDoc;
	public string remarks;
	public string updateDate;
	public string withdrawalStatusNm;
	public string withdrawalReasonNm;
	public string sIntroduceFriendFlag;
	public string sUnConfirmed;
	public string sManualFlag;

	public Withdrawal() {
		withdrawalStatus = string.Empty;
		withdrawalReasonCd = string.Empty;
		withdrawalReasonDoc = string.Empty;
		remarks = string.Empty;
		updateDate = string.Empty;
		withdrawalStatusNm = string.Empty;
		withdrawalReasonNm = string.Empty;
		sIntroduceFriendFlag = string.Empty;
		sUnConfirmed = string.Empty;
		sManualFlag = string.Empty;
	}

	public bool GetOne(string pWithdrawalSeq,string pSexCd) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect("Withdrawal.GetOne");
			string sViewNm;
			if (pSexCd.Equals(ViComm.ViCommConst.MAN)) {
				sViewNm = "VW_ACCEPT_WITHDRAWAL01";
			} else {
				sViewNm = "VW_ACCEPT_WITHDRAWAL02";
			}

			using (cmd = CreateSelectCommand(string.Format("SELECT * FROM {0} WHERE WITHDRAWAL_SEQ = :WITHDRAWAL_SEQ",sViewNm),conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("WITHDRAWAL_SEQ",pWithdrawalSeq);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_ACCEPT_WITHDRAWAL");
					if (ds.Tables["T_ACCEPT_WITHDRAWAL"].Rows.Count != 0) {
						dr = ds.Tables["T_ACCEPT_WITHDRAWAL"].Rows[0];
						withdrawalStatus = dr["WITHDRAWAL_STATUS"].ToString();
						withdrawalReasonCd = dr["WITHDRAWAL_REASON_CD"].ToString();
						withdrawalReasonDoc = dr["WITHDRAWAL_REASON_DOC"].ToString();
						remarks = dr["REMARKS"].ToString();
						updateDate = dr["UPDATE_DATE"].ToString();
						withdrawalStatusNm = dr["WITHDRAWAL_STATUS_NM"].ToString();
						withdrawalReasonNm = dr["WITHDRAWAL_REASON_NM"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public int GetPageCount(string pSiteCd,string pLoginId,string pWithdrawalStatus,string pWithdrawalReasonCd,string pWithdrawalReasonDoc,string pRemarks,string pDayFrom,string pDayTo,string pSexCd,string pIntroduceFriendFlag,string pUnConfirmed,string pManualFlag) {
		sIntroduceFriendFlag = pIntroduceFriendFlag;
		sUnConfirmed = pUnConfirmed;
		sManualFlag = pManualFlag;
		return GetPageCount(pSiteCd,pLoginId,pWithdrawalStatus,pWithdrawalReasonCd,pWithdrawalReasonDoc,pRemarks,pDayFrom,pDayTo,pSexCd);
	}

	public int GetPageCount(string pSiteCd,string pLoginId,string pWithdrawalStatus,string pWithdrawalReasonCd,string pWithdrawalReasonDoc,string pRemarks,string pDayFrom,string pDayTo,string pSexCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		if (pSexCd.Equals(ViCommConst.MAN)) {
			oSqlBuilder.AppendLine("VW_ACCEPT_WITHDRAWAL01	");
		} else {
			oSqlBuilder.AppendLine("VW_ACCEPT_WITHDRAWAL02	");
		}

		string sWhere;
		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pLoginId,pWithdrawalStatus,pWithdrawalReasonCd,pWithdrawalReasonDoc,pRemarks,pDayFrom,pDayTo,out sWhere);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.AppendLine(sWhere).ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pLoginId,string pWithdrawalStatus,string pWithdrawalReasonCd,string pWithdrawalReasonDoc,string pRemarks,string pDayFrom,string pDayTo,string pSexCd,string pIntroduceFriendFlag,string pUnConfirmed,string pManualFlag,int startRowIndex,int maximumRows) {
		sIntroduceFriendFlag = pIntroduceFriendFlag;
		sUnConfirmed = pUnConfirmed;
		sManualFlag = pManualFlag;
		return GetPageCollection(pSiteCd,pLoginId,pWithdrawalStatus,pWithdrawalReasonCd,pWithdrawalReasonDoc,pRemarks,pDayFrom,pDayTo,pSexCd,startRowIndex,maximumRows);
	}

	public DataSet GetPageCollection(string pSiteCd,string pLoginId,string pWithdrawalStatus,string pWithdrawalReasonCd,string pWithdrawalReasonDoc,string pRemarks,string pDayFrom,string pDayTo,string pSexCd,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	*	");
		oSqlBuilder.AppendLine("FROM	");
		if (pSexCd.Equals(ViCommConst.MAN)) {
			oSqlBuilder.AppendLine("VW_ACCEPT_WITHDRAWAL01	");
		} else {
			oSqlBuilder.AppendLine("VW_ACCEPT_WITHDRAWAL02	");
		}

		string sWhere;
		string sSortExpression = "ORDER BY SITE_CD, WITHDRAWAL_SEQ DESC ";
		string sPagingSql;

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pLoginId,pWithdrawalStatus,pWithdrawalReasonCd,pWithdrawalReasonDoc,pRemarks,pDayFrom,pDayTo,out sWhere);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhere).ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
					if (pSexCd.Equals(ViCommConst.MAN)) {
						oDataSet.Tables[0].Columns.Add("TOTAL_PAYMENT_AMT");
					} else {
						oDataSet.Tables[0].Columns.Add("TOTAL_RECEIPT_AMT");
						oDataSet.Tables[0].Columns.Add("TOTAL_RECEIPT_COUNT");
						oDataSet.Tables[0].Columns.Add("LAST_POINT_USED_DATE");
					}
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pLoginId,string pWithdrawalStatus,string pWithdrawalReasonCd,string pWithdrawalReasonDoc,string pRemarks,string pDayFrom,string pDayTo,out string pWhere) {
		pWhere = string.Empty;
		ArrayList list = new ArrayList();

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!string.IsNullOrEmpty(pDayFrom) && !string.IsNullOrEmpty(pDayTo)) {
			DateTime dtFrom = DateTime.Parse(pDayFrom + " " + "00:00:00");
			DateTime dtTo = DateTime.Parse(pDayTo + " " + "23:59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere("(UPDATE_DATE >= :UPDATE_DATE_FROM AND UPDATE_DATE < :UPDATE_DATE_TO)",ref pWhere);
			list.Add(new OracleParameter("UPDATE_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("UPDATE_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (!string.IsNullOrEmpty(pLoginId)) {
			SysPrograms.SqlAppendWhere("LOGIN_ID = :LOGIN_ID",ref pWhere);
			list.Add(new OracleParameter("LOGIN_ID",pLoginId));
		}

		if (!string.IsNullOrEmpty(pWithdrawalStatus)) {
			SysPrograms.SqlAppendWhere("WITHDRAWAL_STATUS = :WITHDRAWAL_STATUS",ref pWhere);
			list.Add(new OracleParameter("WITHDRAWAL_STATUS",pWithdrawalStatus));
		}

		if (!string.IsNullOrEmpty(pWithdrawalReasonCd)) {
			SysPrograms.SqlAppendWhere("WITHDRAWAL_REASON_CD = :WITHDRAWAL_REASON_CD",ref pWhere);
			list.Add(new OracleParameter("WITHDRAWAL_REASON_CD",pWithdrawalReasonCd));
		}

		if (!string.IsNullOrEmpty(pWithdrawalReasonDoc)) {
			SysPrograms.SqlAppendWhere("WITHDRAWAL_REASON_DOC LIKE '%' || :WITHDRAWAL_REASON_DOC || '%'",ref pWhere);
			list.Add(new OracleParameter("WITHDRAWAL_REASON_DOC",pWithdrawalReasonDoc));
		}

		if (!string.IsNullOrEmpty(pRemarks)) {
			SysPrograms.SqlAppendWhere("REMARKS LIKE '%' || :REMARKS || '%'",ref pWhere);
			list.Add(new OracleParameter("REMARKS",pRemarks));
		}

		// 友達紹介による登録
		if (ViCommConst.FLAG_ON_STR.Equals(sIntroduceFriendFlag)) {
			SysPrograms.SqlAppendWhere("AD_CD Like 'os%'",ref pWhere);
		}

		// 未確認
		if (ViCommConst.FLAG_ON_STR.Equals(sUnConfirmed)) {
			SysPrograms.SqlAppendWhere("CONFIRMED_FLAG = :CONFIRMED_FLAG",ref pWhere);
			list.Add(new OracleParameter("CONFIRMED_FLAG",ViCommConst.FLAG_OFF_STR));
		}

		// 手動対応
		if (ViCommConst.FLAG_ON_STR.Equals(sManualFlag)) {
			SysPrograms.SqlAppendWhere("MANUAL_WITHDRAWAL_FLAG = :MANUAL_WITHDRAWAL_FLAG",ref pWhere);
			list.Add(new OracleParameter("MANUAL_WITHDRAWAL_FLAG",sManualFlag));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	/// <summary>
	/// 退会時ポイント付与（会員用）
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pUserSeq"></param>
	/// <param name="pWithdrawalSeq"></param>
	/// <param name="pAddPointId"></param>
	/// <param name="pStatus"></param>
	public void AddPointWithdrawalMan(
		string pSiteCd,
		string pUserSeq,
		ref string pWithdrawalSeq,
		string pAddPointId,
		out string pStatus
	) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("ADD_POINT_WITHDRAWAL_MAN");
			oDbSession.ProcedureBothParm("pWITHDRAWAL_SEQ",DbSession.DbType.VARCHAR2,pWithdrawalSeq);
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			oDbSession.ProcedureInParm("pADD_POINT_ID",DbSession.DbType.VARCHAR2,pAddPointId);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			pWithdrawalSeq = oDbSession.GetStringValue("pWITHDRAWAL_SEQ");
			pStatus = oDbSession.GetStringValue("pSTATUS");
		}
	}

	/// <summary>
	/// 引きとめメール送信（出演者用）
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pUserSeq"></param>
	/// <param name="pWithdrawalSeq"></param>
	/// <param name="pStatus"></param>
	public void CastSendDetainMail(
		string pSiteCd,
		string pUserSeq,
		ref string pWithdrawalSeq,
		out string pStatus
	) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("TX_CAST_DETAIN_MAIL");
			oDbSession.ProcedureBothParm("pWITHDRAWAL_SEQ",DbSession.DbType.VARCHAR2,pWithdrawalSeq);
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			pWithdrawalSeq = oDbSession.GetStringValue("pWITHDRAWAL_SEQ");
			pStatus = oDbSession.GetStringValue("pSTATUS");
		}
	}

	/// <summary>
	/// 退会申請チェック状態変更
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pUserSeq"></param>
	/// <param name="pWithdrawalSeq"></param>
	/// <param name="pStatus"></param>
	public void AcceptWithdrawalCheck(
		string pSiteCd,
		string pUserSeq,
		string pWithdrawalSeq,
		string pCheckedFlag,
		out string pStatus
	) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("ACCEPT_WITHDRAWAL_CHECK");
			oDbSession.ProcedureInParm("pWITHDRAWAL_SEQ",DbSession.DbType.VARCHAR2,pWithdrawalSeq);
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			oDbSession.ProcedureInParm("pCHECKED_FLAG",DbSession.DbType.VARCHAR2,pCheckedFlag);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
			pStatus = oDbSession.GetStringValue("pSTATUS");
		}
	}
}
