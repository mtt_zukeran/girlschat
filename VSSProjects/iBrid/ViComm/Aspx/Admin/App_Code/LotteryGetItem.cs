﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 抽選獲得アイテム
--	Progaram ID		: LotteryGetItem
--
--  Creation Date	: 2011.07.25
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;


public class LotteryGetItem : DbSession {

	public LotteryGetItem() {
	}

	public int GetPageCount(string pSiteCd,string pLotterySeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_LOTTERY_GET_ITEM	L	,");
		oSqlBuilder.AppendLine("	T_GAME_ITEM			GI	");

		string sWhereClause;
		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pLotterySeq,out sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public int GetPageCount_2(string pSiteCd,string pLotterySeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_LOTTERY_GET_ITEM_NOT_COMP 	L	,");
		oSqlBuilder.AppendLine("	T_GAME_ITEM						GI	 ");

		string sWhereClause;
		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pLotterySeq,out sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pLotterySeq,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	L.SITE_CD						,");
		oSqlBuilder.AppendLine("	L.LOTTERY_SEQ					,");
		oSqlBuilder.AppendLine("	L.GAME_ITEM_SEQ					,");
		oSqlBuilder.AppendLine("	GI.GAME_ITEM_NM					,");
        oSqlBuilder.AppendLine("	L.RARE_ITEM_FLAG				,");
        oSqlBuilder.AppendLine("	L.PICKUP_FLAG	    			,");
		oSqlBuilder.AppendLine("	L.RATE							,");
		oSqlBuilder.AppendLine("	L.REVISION_NO					,");
		oSqlBuilder.AppendLine("	L.UPDATE_DATE					");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_LOTTERY_GET_ITEM	L	,");
		oSqlBuilder.AppendLine("	T_GAME_ITEM			GI	");

		string sWhereClause;
		string sPagingSql;
		string sSortExpression = "ORDER BY L.SITE_CD, L.LOTTERY_SEQ, L.GAME_ITEM_SEQ";

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pLotterySeq,out sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}

	public DataSet GetPageCollection_2(string pSiteCd,string pLotterySeq,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	L.SITE_CD						,");
		oSqlBuilder.AppendLine("	L.LOTTERY_SEQ					,");
		oSqlBuilder.AppendLine("	L.GAME_ITEM_SEQ					,");
		oSqlBuilder.AppendLine("	GI.GAME_ITEM_NM					,");
		oSqlBuilder.AppendLine("	L.PM_ITEM_FLAG				    ,");
		oSqlBuilder.AppendLine("	L.PICKUP_FLAG	    			,");
		oSqlBuilder.AppendLine("	L.RATE							,");
		oSqlBuilder.AppendLine("	L.PM_RATE   				    ,");
		oSqlBuilder.AppendLine("	L.REVISION_NO					,");
		oSqlBuilder.AppendLine("	L.UPDATE_DATE					 ");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_LOTTERY_GET_ITEM_NOT_COMP	L	,");
		oSqlBuilder.AppendLine("	T_GAME_ITEM			GI	         ");

		string sWhereClause;
		string sPagingSql;
		string sSortExpression = "ORDER BY L.SITE_CD, L.LOTTERY_SEQ, L.GAME_ITEM_SEQ";

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pLotterySeq,out sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pLotterySeq,out string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();

		SysPrograms.SqlAppendWhere("L.SITE_CD = GI.SITE_CD AND L.GAME_ITEM_SEQ = GI.GAME_ITEM_SEQ",ref pWhere);

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("L.SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}
		if (!string.IsNullOrEmpty(pLotterySeq)) {
			SysPrograms.SqlAppendWhere("L.LOTTERY_SEQ = :LOTTERY_SEQ",ref pWhere);
			list.Add(new OracleParameter("LOTTERY_SEQ",pLotterySeq));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public bool IsDupulicateItem(string pSiteCd,string pLotterySeq,string pGameItemSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_LOTTERY_GET_ITEM	");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD			AND");
		oSqlBuilder.AppendLine("	LOTTERY_SEQ		= :LOTTERY_SEQ		AND");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ	= :GAME_ITEM_SEQ	");

		try {
			int iCount = 0;
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(new OracleParameter("SITE_CD",pSiteCd));
				cmd.Parameters.Add(new OracleParameter("LOTTERY_SEQ",pLotterySeq));
				cmd.Parameters.Add(new OracleParameter("GAME_ITEM_SEQ",pGameItemSeq));
				iCount = Convert.ToInt32(cmd.ExecuteScalar());
				return iCount >= 1;
			}
		} finally {
			conn.Close();
		}
	}

	public bool IsDupulicateItemNotComp(string pSiteCd,string pLotterySeq,string pGameItemSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_LOTTERY_GET_ITEM_NOT_COMP	");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD			AND");
		oSqlBuilder.AppendLine("	LOTTERY_SEQ		= :LOTTERY_SEQ		AND");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ	= :GAME_ITEM_SEQ	");

		try {
			int iCount = 0;
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(new OracleParameter("SITE_CD",pSiteCd));
				cmd.Parameters.Add(new OracleParameter("LOTTERY_SEQ",pLotterySeq));
				cmd.Parameters.Add(new OracleParameter("GAME_ITEM_SEQ",pGameItemSeq));
				iCount = Convert.ToInt32(cmd.ExecuteScalar());
				return iCount >= 1;
			}
		} finally {
			conn.Close();
		}
	}

	public int GetRateSummary(string pSiteCd,string pLotterySeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	NVL(SUM(RATE),0)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_LOTTERY_GET_ITEM	");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD			AND");
		oSqlBuilder.AppendLine("	LOTTERY_SEQ		= :LOTTERY_SEQ		");

		try {
			int iSummary = 0;
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(new OracleParameter("SITE_CD",pSiteCd));
				cmd.Parameters.Add(new OracleParameter("LOTTERY_SEQ",pLotterySeq));
				iSummary = Convert.ToInt32(cmd.ExecuteScalar());
				return iSummary;
			}
		} finally {
			conn.Close();
		}
	}

	public int GetRateNotCompSummary(string pSiteCd,string pLotterySeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	NVL(SUM(RATE),0)			");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_LOTTERY_GET_ITEM_NOT_COMP	");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD		AND");
		oSqlBuilder.AppendLine("	LOTTERY_SEQ		= :LOTTERY_SEQ	   ");

		try {
			int iSummary = 0;
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(new OracleParameter("SITE_CD",pSiteCd));
				cmd.Parameters.Add(new OracleParameter("LOTTERY_SEQ",pLotterySeq));
				iSummary = Convert.ToInt32(cmd.ExecuteScalar());
				return iSummary;
			}
		} finally {
			conn.Close();
		}
	}

	public int GetPmRateNotCompSummary(string pSiteCd,string pLotterySeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	NVL(SUM(PM_RATE),0)			");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_LOTTERY_GET_ITEM_NOT_COMP	");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD		AND");
		oSqlBuilder.AppendLine("	LOTTERY_SEQ		= :LOTTERY_SEQ	AND");
		oSqlBuilder.AppendLine("	PM_ITEM_FLAG	= :PM_ITEM_FLAG	   ");

		try {
			int iSummary = 0;
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(new OracleParameter("SITE_CD",pSiteCd));
				cmd.Parameters.Add(new OracleParameter("LOTTERY_SEQ",pLotterySeq));
				cmd.Parameters.Add(new OracleParameter("PM_ITEM_FLAG",ViCommConst.FLAG_ON));
				iSummary = Convert.ToInt32(cmd.ExecuteScalar());
				return iSummary;
			}
		} finally {
			conn.Close();
		}
	}
}
