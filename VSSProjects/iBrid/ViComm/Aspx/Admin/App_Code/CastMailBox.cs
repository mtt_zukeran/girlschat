﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Cast PC
--	Title			: メールBOX(キャスト側からみて)
--	Progaram ID		: CastMailBox
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using iBridCommLib;
using ViComm;

public class CastMailBox:DbSession {


	public CastMailBox() {
	}


	public int GetPageCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pMailBoxType,string pTxRxType) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		try{
			conn = DbConnect();
			string sViewNm;
			if (pTxRxType.Equals(ViCommConst.RX)) {
				sViewNm = "VW_MAIL_BOX02";
			} else {
				sViewNm = "VW_MAIL_BOX03";
			}

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM " + sViewNm;
			string sWhere = "";

			OracleParameter[] oParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pMailBoxType,pTxRxType,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < oParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)oParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPages = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPages;
	}

	public DataSet GetPageCollection(string pSiteCd,string pUserSeq,string pUserCharNo,string pMailBoxType,string pTxRxType,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY SITE_CD,USER_SEQ,USER_CHAR_NO,MAIL_BOX_TYPE,MAIL_SEQ DESC";

			string sViewNm;
			if (pTxRxType.Equals(ViCommConst.RX)) {
				sViewNm = "VW_MAIL_BOX02";
			} else {
				sViewNm = "VW_MAIL_BOX03";
			}


			string sSql = string.Format("SELECT " +
								"SITE_CD			," +
								"USER_SEQ			," +
								"USER_CHAR_NO		," +
								"MAIL_BOX_TYPE		," +
								"MAIL_SEQ			," +
								"MAIL_TYPE			," +
								"CREATE_DATE		," +
								"MAIL_TITLE			," +
								"MAIL_DOC1			," +
								"MAIL_DOC2			," +
								"MAIL_DOC3			," +
								"MAIL_DOC4			," +
								"MAIL_DOC5			," +
								"READ_FLAG			," +
								"TX_DEL_FLAG		," +
								"RX_DEL_FLAG		," +
								"TX_SITE_CD			," +
								"TX_USER_SEQ		," +
								"TX_USER_CHAR_NO	," +
								"TX_MAIL_BOX_TYPE	," +
								"RX_SITE_CD			," +
								"RX_USER_SEQ		," +
								"RX_USER_CHAR_NO	," +
								"RX_MAIL_BOX_TYPE	," +
								"SERVICE_POINT		," +
								"SERVICE_POINT_EFFECTIVE_DATE	," +
								"SERVICE_POINT_TRANSFER_FLAG	," +
								"BATCH_MAIL_LOG_SEQ	," +
								"HANDLE_NM			," +
								"PARTNER_LOGIN_ID	," +
								"RNUM " +
							"FROM(SELECT {0}.*, ROW_NUMBER() OVER ({1}) AS RNUM FROM {0}  ",sViewNm,sOrder);
			string sWhere = "";

			OracleParameter[] oParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pMailBoxType,pTxRxType,ref sWhere);

			sSql = sSql + sWhere;
			sSql = sSql + ")WHERE RNUM >= :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;
			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < oParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)oParms[i]);
				}
				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"MAIL_BOX");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pUserSeq,string pUserCharNo,string pMailBoxType,string pTxRxType,ref string pWhere) {
		ArrayList list = new ArrayList();
		if (pTxRxType.Equals(ViCommConst.RX)) {
			pWhere = " WHERE SITE_CD = :SITE_CD AND  USER_SEQ = :USER_SEQ AND USER_CHAR_NO = :USER_CHAR_NO AND MAIL_BOX_TYPE = :MAIL_BOX_TYPE AND RX_DEL_FLAG = 0 ";
		} else {
			pWhere = " WHERE SITE_CD = :SITE_CD AND  USER_SEQ = :USER_SEQ AND USER_CHAR_NO = :USER_CHAR_NO AND MAIL_BOX_TYPE = :MAIL_BOX_TYPE AND TX_DEL_FLAG = 0 ";
		}
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		list.Add(new OracleParameter("USER_SEQ",pUserSeq));
		list.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));
		list.Add(new OracleParameter("MAIL_BOX_TYPE",pMailBoxType));
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public void UpdateMailDel(string pMailSeq,string pTxRxType) {
		string[] mailSeqList = new string[] { pMailSeq };
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_MAIL_DEL");
			db.ProcedureInArrayParm("PMAIL_SEQ",DbSession.DbType.VARCHAR2,mailSeqList.Length,mailSeqList);
			db.ProcedureInParm("PMAIL_SEQ_COUNT",DbSession.DbType.NUMBER,mailSeqList.Length);
			db.ProcedureInParm("PTX_RX_TYPE",DbSession.DbType.VARCHAR2,pTxRxType);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}
