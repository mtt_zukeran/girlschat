﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: ゲームアイテム
--	Progaram ID		: GameItem
--
--  Creation Date	: 2011.07.19
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;


public class GameItem : DbSession {

	public GameItem() {
	}

	public DataSet GetListByCategoryType(string pSiteCd,string pSexCd,string pGameItemCategoryType,string pGameItemGetCd,string pPresentFlag) {
		return GetListByCategoryTypeBase(pSiteCd,pSexCd,pGameItemCategoryType,pGameItemGetCd,pPresentFlag);
    }

	public DataSet GetListByCategoryTypeBase(string pSiteCd,string pSexCd,string pGameItemCategoryType,string pGameItemGetCd,string pPresentFlag) {
    
        StringBuilder oSqlBuilder = new StringBuilder();
        oSqlBuilder.AppendLine("SELECT	");
        oSqlBuilder.AppendLine("	SITE_CD					,");
        oSqlBuilder.AppendLine("	GAME_ITEM_SEQ			,");
        oSqlBuilder.AppendLine("	GAME_ITEM_NM			");
        oSqlBuilder.AppendLine("FROM	");
        oSqlBuilder.AppendLine("	T_GAME_ITEM				");

        string sWhereClause;
		string sSortExpression = "ORDER BY SITE_CD,GAME_ITEM_NM,GAME_ITEM_SEQ";
		OracleParameter[] oWhereParams = this.CreateWhereGetList(pSiteCd,pSexCd,pGameItemCategoryType,pGameItemGetCd,pPresentFlag,out sWhereClause);

        oSqlBuilder.AppendLine(sWhereClause);
        oSqlBuilder.AppendLine(sSortExpression);

        DataSet oDs = new DataSet();
        try
        {
            conn = DbConnect();
            using (cmd = CreateSelectCommand(oSqlBuilder.ToString(), conn))
            {
                cmd.BindByName = true;
                cmd.Parameters.AddRange(oWhereParams);

                using (da = new OracleDataAdapter(cmd))
                {
                    da.Fill(oDs);
                }
            }
        }
        finally
        {
            conn.Close();
        }
        return oDs;

    }
    
    public DataSet GetListNotRegisterdPresentItemByCategoryType(string pSiteCd, string pSexCd, string pGameItemCategoryType, string pPresentFlag, string pExceptPresentGameItemSeq,string pSelectGameItemFlag) {

		string pSexCd2 = ViCommConst.MAN.Equals(pSexCd) ? ViCommConst.OPERATOR : ViCommConst.MAN;
		
		StringBuilder oSqlBuilder = new StringBuilder();
        oSqlBuilder.AppendLine("SELECT																	");
        oSqlBuilder.AppendLine("	SITE_CD															,	");
        oSqlBuilder.AppendLine("	GAME_ITEM_SEQ													,	");
        oSqlBuilder.AppendLine("	GAME_ITEM_NM														");
        oSqlBuilder.AppendLine("FROM																	");
        oSqlBuilder.AppendLine("	T_GAME_ITEM ITEM													");
        oSqlBuilder.AppendLine("WHERE																	");
        oSqlBuilder.AppendLine("	ITEM.SITE_CD					= :SITE_CD						AND	");
        oSqlBuilder.AppendLine("	ITEM.SEX_CD						= :SEX_CD						AND	");
		if (!string.IsNullOrEmpty(pGameItemCategoryType)) {
			oSqlBuilder.AppendLine("	ITEM.GAME_ITEM_CATEGORY_TYPE	= :GAME_ITEM_CATEGORY_TYPE		AND	");
        }
        oSqlBuilder.AppendLine("	ITEM.PRESENT_FLAG               = :PRESENT_FLAG					AND	");
        oSqlBuilder.AppendLine("	NOT EXISTS															");
        oSqlBuilder.AppendLine("	(																	");
        oSqlBuilder.AppendLine("		SELECT															");
        oSqlBuilder.AppendLine("			*															");
        oSqlBuilder.AppendLine("		FROM															");
        oSqlBuilder.AppendLine("			T_GAME_ITEM REG												");
        oSqlBuilder.AppendLine("		WHERE															");
        oSqlBuilder.AppendLine("			REG.SITE_CD					= :SITE_CD					AND	");
        oSqlBuilder.AppendLine("			REG.SEX_CD					= :SEX_CD2					AND	");
        oSqlBuilder.AppendLine("			REG.SITE_CD					= ITEM.SITE_CD				AND	");
        oSqlBuilder.AppendLine("			REG.PRESENT_GAME_ITEM_SEQ	= ITEM.GAME_ITEM_SEQ			");
        if(!string.IsNullOrEmpty(pExceptPresentGameItemSeq)) {
			oSqlBuilder.AppendLine("	AND		REG.PRESENT_GAME_ITEM_SEQ	!= :EXCEPT_PRESENT_GAME_ITEM_SEQ	");
        }
		oSqlBuilder.AppendLine("	)																	");
		oSqlBuilder.AppendLine("ORDER BY SITE_CD, GAME_ITEM_SEQ											");
		
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":SEX_CD",pSexCd));
		if(!string.IsNullOrEmpty(pGameItemCategoryType)) {
			oParamList.Add(new OracleParameter(":GAME_ITEM_CATEGORY_TYPE",pGameItemCategoryType));
		}
		oParamList.Add(new OracleParameter(":PRESENT_FLAG",pPresentFlag));
		oParamList.Add(new OracleParameter(":SEX_CD2",pSexCd2));
		if (!string.IsNullOrEmpty(pExceptPresentGameItemSeq)) {
			oParamList.Add(new OracleParameter(":EXCEPT_PRESENT_GAME_ITEM_SEQ",pExceptPresentGameItemSeq));
		}
		
		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oParamList.ToArray());

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		
		if(pSelectGameItemFlag.Equals(ViCommConst.FLAG_ON_STR) && oDs.Tables[0].Rows.Count < 1) {
			oDs = new DataSet();
			DataTable oDt = new DataTable();
			oDs.Tables.Add(oDt);
			oDt.Columns.Add("SITE_CD",Type.GetType("System.String"));
			oDt.Columns.Add("GAME_ITEM_SEQ",Type.GetType("System.String"));
			oDt.Columns.Add("GAME_ITEM_NM",Type.GetType("System.String"));
			DataRow oDr = oDs.Tables[0].NewRow();
			oDr["SITE_CD"] = string.Empty;
			oDr["GAME_ITEM_SEQ"] = string.Empty;
			oDr["GAME_ITEM_NM"] = string.Empty;
			oDs.Tables[0].Rows.Add(oDr);
		}
		
		return oDs;
    }

	private OracleParameter[] CreateWhereGetList(string pSiteCd,string pSexCd,string pGameItemCategoryType,string pGameItemGetCd,string pPresentFlag,out string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}
		if (!string.IsNullOrEmpty(pSexCd)) {
			SysPrograms.SqlAppendWhere("SEX_CD = :SEX_CD",ref pWhere);
			list.Add(new OracleParameter("SEX_CD",pSexCd));
		}
		if (!string.IsNullOrEmpty(pGameItemCategoryType)) {
			SysPrograms.SqlAppendWhere("GAME_ITEM_CATEGORY_TYPE = :GAME_ITEM_CATEGORY_TYPE",ref pWhere);
			list.Add(new OracleParameter("GAME_ITEM_CATEGORY_TYPE",pGameItemCategoryType));
		}
		if (!string.IsNullOrEmpty(pGameItemGetCd)) {
			if (pGameItemGetCd == "3") {
				SysPrograms.SqlAppendWhere("GAME_ITEM_GET_CD IS NULL",ref pWhere);
			} else {
				SysPrograms.SqlAppendWhere("GAME_ITEM_GET_CD = :GAME_ITEM_GET_CD",ref pWhere);
				list.Add(new OracleParameter("GAME_ITEM_GET_CD",pGameItemGetCd));
			}
		}
        if (!string.IsNullOrEmpty(pPresentFlag)) {
            SysPrograms.SqlAppendWhere("PRESENT_FLAG = :PRESENT_FLAG", ref pWhere);
            list.Add(new OracleParameter("PRESENT_FLAG", pPresentFlag));
        }

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

    public bool CheckGameItemPresented(string pSiteCd,string pSexCd,string pPresentGameItemSeq,string pGameItemSeq) {
        StringBuilder oSqlBuilder = new StringBuilder();
        oSqlBuilder.AppendLine("SELECT COUNT(*) FROM T_GAME_ITEM ");
        oSqlBuilder.AppendLine("WHERE ");
        oSqlBuilder.AppendLine("SITE_CD = :SITE_CD AND ");
        oSqlBuilder.AppendLine("SEX_CD  = :SEX_CD  AND ");
        oSqlBuilder.AppendLine("PRESENT_FLAG  = :PRESENT_FLAG  AND ");
        oSqlBuilder.AppendLine("PRESENT_GAME_ITEM_SEQ  = :PRESENT_GAME_ITEM_SEQ   ");
        if(!string.IsNullOrEmpty(pGameItemSeq)) {
			oSqlBuilder.AppendLine(" AND GAME_ITEM_SEQ != :GAME_ITEM_SEQ");
        }

        List<OracleParameter> oParamList = new List<OracleParameter>();
        oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
        oParamList.Add(new OracleParameter(":SEX_CD", pSexCd));
        oParamList.Add(new OracleParameter(":PRESENT_FLAG", ViCommConst.FLAG_ON_STR));
        oParamList.Add(new OracleParameter(":PRESENT_GAME_ITEM_SEQ", pPresentGameItemSeq));
		if (!string.IsNullOrEmpty(pGameItemSeq)) {
			oParamList.Add(new OracleParameter(":GAME_ITEM_SEQ",pGameItemSeq));
		}

        int iCount = this.ExecuteSelectCountQueryBase(oSqlBuilder.ToString(), oParamList.ToArray());

        return (iCount > 0);
    }


	public int GetPageCount(string pSiteCd,string pSexCd,string pGameItemName,string pGameItemSeq,string pGameItemGetCd,string pGameItemCategoryType,string pPublishFlag,string pPresentFlag) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT COUNT(*) FROM VW_GAME_ITEM01");

		string sWhereClause;
		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pSexCd,pGameItemName,pGameItemSeq,pGameItemGetCd,pGameItemCategoryType,pPublishFlag,pPresentFlag,out sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		}
		finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pSexCd,string pGameItemName,string pGameItemSeq,string pGameItemGetCd,string pGameItemCategoryType,string pPublishFlag,string pPresentFlag,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT						");
		oSqlBuilder.AppendLine("	SITE_CD					,");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ			,");
		oSqlBuilder.AppendLine("	SEX_CD					,");
		oSqlBuilder.AppendLine("	GAME_ITEM_NM			,");
		oSqlBuilder.AppendLine("	ATTACK_POWER			,");
		oSqlBuilder.AppendLine("	DEFENCE_POWER			,");
		oSqlBuilder.AppendLine("	ENDURANCE				,");
		oSqlBuilder.AppendLine("	DESCRIPTION				,");
		oSqlBuilder.AppendLine("	GAME_ITEM_GET_CD		,");
		oSqlBuilder.AppendLine("	GAME_ITEM_GET_NM		,");
		oSqlBuilder.AppendLine("	PRICE					,");
		oSqlBuilder.AppendLine("	PUBLISH_FLAG			,");
		oSqlBuilder.AppendLine("	PUBLISH_DATE			,");
		oSqlBuilder.AppendLine("	CREATE_DATE				,");
		oSqlBuilder.AppendLine("	STAGE_SEQ				,");
		oSqlBuilder.AppendLine("	STAGE_NM				,");
		oSqlBuilder.AppendLine("	GAME_ITEM_CATEGORY_TYPE ,");
		oSqlBuilder.AppendLine("	GAME_ITEM_CATEGORY_NM	,");
		oSqlBuilder.AppendLine("	PRESENT_FLAG			,");
		oSqlBuilder.AppendLine("	PRESENT_GAME_ITEM_SEQ	,");
		oSqlBuilder.AppendLine("	REMARKS					,");
		oSqlBuilder.AppendLine("	REVISION_NO				,");
		oSqlBuilder.AppendLine("	UPDATE_DATE				");
		oSqlBuilder.AppendLine("FROM						");
		oSqlBuilder.AppendLine("	VW_GAME_ITEM01			");

		string sWhereClause;
		string sPagingSql;
		string sSortExpression = "ORDER BY SITE_CD, GAME_ITEM_SEQ";

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pSexCd,pGameItemName,pGameItemSeq,pGameItemGetCd,pGameItemCategoryType,pPublishFlag,pPresentFlag,out sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		}
		finally {
			conn.Close();
		}
		return oDs;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pSexCd,string pGameItemName,string pGameItemSeq,string pGameItemGetCd,string pGameItemCategoryType,string pPublishFlag,string pPresentFlag,out string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}
		if (!string.IsNullOrEmpty(pSexCd)) {
			SysPrograms.SqlAppendWhere("SEX_CD = :SEX_CD",ref pWhere);
			list.Add(new OracleParameter("SEX_CD",pSexCd));
		}
		if (!string.IsNullOrEmpty(pGameItemName)) {
			SysPrograms.SqlAppendWhere("GAME_ITEM_NM LIKE :GAME_ITEM_NM",ref pWhere);
			list.Add(new OracleParameter("GAME_ITEM_NM","%" + pGameItemName + "%"));
		}
		if (!string.IsNullOrEmpty(pGameItemSeq)) {
			SysPrograms.SqlAppendWhere("GAME_ITEM_SEQ = :GAME_ITEM_SEQ",ref pWhere);
			list.Add(new OracleParameter("GAME_ITEM_SEQ",pGameItemSeq));
		}
		if (!string.IsNullOrEmpty(pGameItemGetCd)) {

			if (pGameItemGetCd == "3") {
				SysPrograms.SqlAppendWhere("GAME_ITEM_GET_CD IS NULL",ref pWhere);
			} else {
				SysPrograms.SqlAppendWhere("GAME_ITEM_GET_CD = :GAME_ITEM_GET_CD",ref pWhere);
				list.Add(new OracleParameter("GAME_ITEM_GET_CD",pGameItemGetCd));
			}
		}
		if (!string.IsNullOrEmpty(pGameItemCategoryType)) {
			SysPrograms.SqlAppendWhere("GAME_ITEM_CATEGORY_TYPE = :GAME_ITEM_CATEGORY_TYPE",ref pWhere);
			list.Add(new OracleParameter("GAME_ITEM_CATEGORY_TYPE",pGameItemCategoryType));
		}
		if (!string.IsNullOrEmpty(pPublishFlag)) {
			SysPrograms.SqlAppendWhere("PUBLISH_FLAG = :PUBLISH_FLAG",ref pWhere);
			list.Add(new OracleParameter("PUBLISH_FLAG",pPublishFlag));
		}
		if (!string.IsNullOrEmpty(pPresentFlag)) {
			SysPrograms.SqlAppendWhere("PRESENT_FLAG = :PRESENT_FLAG",ref pWhere);
			list.Add(new OracleParameter("PRESENT_FLAG",pPresentFlag));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}


	public bool IsExists(string pSiteCd, string pGameItemSeq,string pStageSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.Append("SELECT																 ").AppendLine();
		oSqlBuilder.Append("	COUNT(*)	AS	CNT												 ").AppendLine();
		oSqlBuilder.Append("FROM																 ").AppendLine();
		oSqlBuilder.Append("	VW_GAME_ITEM01													,").AppendLine();
		oSqlBuilder.Append("	(																 ").AppendLine();
		oSqlBuilder.Append("		SELECT														 ").AppendLine();
		oSqlBuilder.Append("			STAGE_LEVEL												 ").AppendLine();
		oSqlBuilder.Append("		FROM														 ").AppendLine();
		oSqlBuilder.Append("			T_STAGE													 ").AppendLine();
		oSqlBuilder.Append("		WHERE														 ").AppendLine();
		oSqlBuilder.Append("			SITE_CD		= :SITE_CD								AND  ").AppendLine();
		oSqlBuilder.Append("			STAGE_SEQ	= :STAGE_SEQ								 ").AppendLine();
		oSqlBuilder.Append("	)	T1															 ").AppendLine();
		oSqlBuilder.Append("WHERE																 ").AppendLine();
		oSqlBuilder.Append("	SITE_CD						= :SITE_CD						AND	 ").AppendLine();
		oSqlBuilder.Append("	GAME_ITEM_SEQ				= :GAME_ITEM_SEQ				AND	 ").AppendLine();
		oSqlBuilder.Append("	PRESENT_FLAG				= :PRESENT_FLAG					AND	 ").AppendLine();
		oSqlBuilder.Append("	GAME_ITEM_CATEGORY_TYPE	IN(	:GAME_ITEM_CATEGORY_TYPE1			,").AppendLine();
		oSqlBuilder.Append("								:GAME_ITEM_CATEGORY_TYPE2			,").AppendLine();
		oSqlBuilder.Append("								:GAME_ITEM_CATEGORY_TYPE3	)	AND  ").AppendLine();
		oSqlBuilder.Append("	(																 ").AppendLine();
		oSqlBuilder.Append("		GAME_ITEM_GET_CD			IS NULL						OR	 ").AppendLine();
		oSqlBuilder.Append("		GAME_ITEM_GET_CD			= :GAME_ITEM_GET_CD		AND	 ").AppendLine();
		oSqlBuilder.Append("		VW_GAME_ITEM01.STAGE_LEVEL <= T1.STAGE_LEVEL				 ").AppendLine();
		oSqlBuilder.Append("	)																 ").AppendLine();

		DataSet oDataSet = new DataSet();
		try {
			conn = DbConnect();

			using(cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)){
				cmd.BindByName = true;
				cmd.Parameters.Add(new OracleParameter(":SITE_CD", pSiteCd));
				cmd.Parameters.Add(new OracleParameter(":STAGE_SEQ", pStageSeq));
				cmd.Parameters.Add(new OracleParameter(":GAME_ITEM_SEQ", pGameItemSeq));
				cmd.Parameters.Add(new OracleParameter(":PRESENT_FLAG", ViCommConst.FLAG_OFF_STR));
				cmd.Parameters.Add(new OracleParameter(":GAME_ITEM_GET_CD", ViComm.Extension.Pwild.PwViCommConst.GameItemGetCd.NOT_CHARGE));
				cmd.Parameters.Add(new OracleParameter(":GAME_ITEM_CATEGORY_TYPE1", ViComm.Extension.Pwild.PwViCommConst.GameItemCategory.ITEM_CATEGORY_ACCESSORY));
				cmd.Parameters.Add(new OracleParameter(":GAME_ITEM_CATEGORY_TYPE2", ViComm.Extension.Pwild.PwViCommConst.GameItemCategory.ITEM_CATEGORY_CLOTHES));
				cmd.Parameters.Add(new OracleParameter(":GAME_ITEM_CATEGORY_TYPE3", ViComm.Extension.Pwild.PwViCommConst.GameItemCategory.ITEM_CATEGORY_VEHICLE));
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
				}
			}

		} finally {
			conn.Close();
		}

		return (oDataSet.Tables[0].Rows.Count > 0 && 
				int.Parse(oDataSet.Tables[0].Rows[0]["CNT"].ToString()) > 0);
	}
	
	public DataSet GetOneByGameItemSeq(string pSiteCd,string pGameItemSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	PRESENT_GAME_ITEM_SEQ					");
		oSqlBuilder.AppendLine("FROM										");
		oSqlBuilder.AppendLine("	T_GAME_ITEM								");
		oSqlBuilder.AppendLine("WHERE										");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ	= :GAME_ITEM_SEQ		");

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":GAME_ITEM_SEQ",pGameItemSeq));

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oParamList.ToArray());

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		
		return oDs;
	}
}
