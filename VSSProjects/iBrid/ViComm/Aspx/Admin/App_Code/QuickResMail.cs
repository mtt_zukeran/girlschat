﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: QuickResMail
--	Title			: ｸｲｯｸﾚｽﾎﾟﾝｽ稼動明細
--	Progaram ID		: QuickResMail
--
--  Creation Date	: 2009.12.11
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater   Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Text;
using ViComm;
using iBridCommLib;

public class QuickResMail:DbSession {

	public QuickResMail() {
	}

	public int GetPageCount(string pSiteCd,string pReportDay,string pReportHour) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	COUNT(*) ");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("T_QUICK_RES_MAIL ");

		string sWhere;
		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pReportDay,pReportHour,out sWhere);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.AppendLine(sWhere).ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}


	public DataSet GetPageCollection(string pSiteCd,string pReportDay,string pReportHour,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	*	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("T_QUICK_RES_MAIL ");

		string sWhere;
		string sSortExpression = "ORDER BY TX_SITE_CD,REPORT_DAY,REPORT_HOUR,MAIL_SEQ ";
		string sPagingSql;

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pReportDay,pReportHour,out sWhere);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhere).ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);
		DataSet oDataSet = new DataSet();

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pReportDay,string pReportHour,out string pWhere) {
		pWhere = string.Empty;
		ArrayList list = new ArrayList();

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("TX_SITE_CD = :TX_SITE_CD",ref pWhere);
			list.Add(new OracleParameter("TX_SITE_CD",pSiteCd));
		}

		if (!string.IsNullOrEmpty(pReportDay)) {
			SysPrograms.SqlAppendWhere("REPORT_DAY = :REPORT_DAY",ref pWhere);
			list.Add(new OracleParameter("REPORT_DAY",pReportDay));
		}

		if (!string.IsNullOrEmpty(pReportHour)) {
			SysPrograms.SqlAppendWhere("REPORT_HOUR = :REPORT_HOUR",ref pWhere);
			list.Add(new OracleParameter("REPORT_HOUR",pReportHour));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}
