﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Cast PC
--	Title			: お気に入り(キャスト側からみて)
--	Progaram ID		: CastFavorit
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;


public class CastFavorit:DbSession {
	public string favoritComment;
	public string registDay;

	public CastFavorit() {
		favoritComment = "";
		registDay = "";
	}

	public int GetPageCount(string pSiteCd,string pUserSeq,string pUserCharNo,int pLikeMeFlag) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql;
			if (pLikeMeFlag != 0) {
				sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_FAVORIT01 ";
			} else {
				sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_FAVORIT02 ";
			}

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pLikeMeFlag,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}


	public DataSet GetPageCollection(string pSiteCd,string pUserSeq,string pUserCharNo,int pLikeMeFlag,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();
			string sViewNm;
			if (pLikeMeFlag != 0) {
				sViewNm = " VW_FAVORIT01 ";
			} else {
				sViewNm = " VW_FAVORIT02 ";
			}

			string sOrder = "ORDER BY FAVORIT_REGIST_DATE DESC,SITE_CD,PARTNER_USER_SEQ ";
			string sSql = "SELECT " +
								"SITE_CD				," +
								"USER_SEQ				," +
								"USER_CHAR_NO			," +
								"PARTNER_USER_SEQ		," +
								"PARTNER_USER_CHAR_NO	," +
								"CHARACTER_ONLINE_STATUS," +
								"FAVORIT_REGIST_DATE	," +
								"HANDLE_NM				," +
								"LOGIN_ID				," +
								"LAST_LOGIN_DATE		" +
							"FROM(" +
							" SELECT " + sViewNm + ".*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM " + sViewNm;

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pLikeMeFlag,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_FAVORIT");
				}
			}
		} finally {
			conn.Close();
		}

		ds.Tables[0].Columns.Add(new DataColumn("LAST_TALK_DATE",System.Type.GetType("System.String")));
		ds.Tables[0].Columns.Add(new DataColumn("TALK_COUNT",System.Type.GetType("System.String")));

		int iTalkCount;
		string sLastTalkDay;

		using (TalkHistory oTalkHis = new TalkHistory()) {
			foreach (DataRow dr in ds.Tables[0].Rows) {
				if (pLikeMeFlag != 0) {
					oTalkHis.GetTalkCount(pSiteCd,dr["USER_SEQ"].ToString(),dr["USER_CHAR_NO"].ToString(),pUserSeq,pUserCharNo,out iTalkCount,out sLastTalkDay);
				} else {
					oTalkHis.GetTalkCount(pSiteCd,dr["PARTNER_USER_SEQ"].ToString(),dr["PARTNER_USER_CHAR_NO"].ToString(),pUserSeq,pUserCharNo,out iTalkCount,out sLastTalkDay);
				}
				dr["LAST_TALK_DATE"] =sLastTalkDay;
				dr["TALK_COUNT"] = iTalkCount;
			}
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,string pUserSeq,string pUserCharNo,int pLikeMeFlag,ref string pWhere) {
		ArrayList list = new ArrayList();

		if (!pSiteCd.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!pUserSeq.Equals("")) {
			if (pLikeMeFlag != 0) {
				iBridCommLib.SysPrograms.SqlAppendWhere("PARTNER_USER_SEQ = :PARTNER_USER_SEQ",ref pWhere);
				list.Add(new OracleParameter("PARTNER_USER_SEQ",pUserSeq));
				iBridCommLib.SysPrograms.SqlAppendWhere("PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO",ref pWhere);
				list.Add(new OracleParameter("PARTNER_USER_CHAR_NO",pUserCharNo));
			} else {
				iBridCommLib.SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhere);
				list.Add(new OracleParameter("USER_SEQ",pUserSeq));
				iBridCommLib.SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhere);
				list.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));
			}
		}
		
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public bool GetOne(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try{
			conn = DbConnect();

			string sSql = "SELECT " +
								"TO_CHAR(REGIST_DATE,'yyyy/mm/dd') AS REGIST_DAY," +
								"FAVORIT_COMMENT	 " +
							 "FROM T_FAVORIT " +
								"WHERE " +
									"SITE_CD				=:SITE_CD				AND " +
									"USER_SEQ				=:USER_SEQ				AND " +
									"USER_CHAR_NO			=:USER_CHAR_NO			AND " +
									"PARTNER_USER_SEQ		=:PARTNER_USER_SEQ		AND " +
									"PARTNER_USER_CHAR_NO	=:PARTNER_USER_CHAR_NO ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				cmd.Parameters.Add("PARTNER_USER_SEQ",pPartnerUserSeq);
				cmd.Parameters.Add("PARTNER_USER_CHAR_NO",pPartnerUserCharNo);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_FAVORIT");
					if (ds.Tables["T_FAVORIT"].Rows.Count != 0) {
						dr = ds.Tables["T_FAVORIT"].Rows[0];
						registDay = dr["REGIST_DAY"].ToString();
						favoritComment = dr["FAVORIT_COMMENT"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}
}
