﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: ビンゴカード


--	Progaram ID		: BingoCard
--  Creation Date	: 2011.07.06
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;

public class BingoCard : DbSession
{
	public BingoCard() { }

	public DataSet GetList(string pSiteCd, string pBingoTermSeq)
	{
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	C.SITE_CD				,");
		oSqlBuilder.AppendLine("	C.BINGO_TERM_SEQ		,");
		oSqlBuilder.AppendLine("	C.BINGO_CARD_NO			,");
		oSqlBuilder.AppendLine("	C.HIGH_PROBABILITY_FLAG	,");
		oSqlBuilder.AppendLine("	MAX(CASE WHEN CA.X_POS = 1 AND CA.Y_POS = 1 THEN CA.BINGO_BALL_NO ELSE 0 END) BALL_NO_1		,");
		oSqlBuilder.AppendLine("	MAX(CASE WHEN CA.X_POS = 2 AND CA.Y_POS = 1 THEN CA.BINGO_BALL_NO ELSE 0 END) BALL_NO_2		,");
		oSqlBuilder.AppendLine("	MAX(CASE WHEN CA.X_POS = 3 AND CA.Y_POS = 1 THEN CA.BINGO_BALL_NO ELSE 0 END) BALL_NO_3		,");
		oSqlBuilder.AppendLine("	MAX(CASE WHEN CA.X_POS = 4 AND CA.Y_POS = 1 THEN CA.BINGO_BALL_NO ELSE 0 END) BALL_NO_4		,");
		oSqlBuilder.AppendLine("	MAX(CASE WHEN CA.X_POS = 5 AND CA.Y_POS = 1 THEN CA.BINGO_BALL_NO ELSE 0 END) BALL_NO_5		,");
		oSqlBuilder.AppendLine("	MAX(CASE WHEN CA.X_POS = 1 AND CA.Y_POS = 2 THEN CA.BINGO_BALL_NO ELSE 0 END) BALL_NO_6		,");
		oSqlBuilder.AppendLine("	MAX(CASE WHEN CA.X_POS = 2 AND CA.Y_POS = 2 THEN CA.BINGO_BALL_NO ELSE 0 END) BALL_NO_7		,");
		oSqlBuilder.AppendLine("	MAX(CASE WHEN CA.X_POS = 3 AND CA.Y_POS = 2 THEN CA.BINGO_BALL_NO ELSE 0 END) BALL_NO_8		,");
		oSqlBuilder.AppendLine("	MAX(CASE WHEN CA.X_POS = 4 AND CA.Y_POS = 2 THEN CA.BINGO_BALL_NO ELSE 0 END) BALL_NO_9		,");
		oSqlBuilder.AppendLine("	MAX(CASE WHEN CA.X_POS = 5 AND CA.Y_POS = 2 THEN CA.BINGO_BALL_NO ELSE 0 END) BALL_NO_10	,");
		oSqlBuilder.AppendLine("	MAX(CASE WHEN CA.X_POS = 1 AND CA.Y_POS = 3 THEN CA.BINGO_BALL_NO ELSE 0 END) BALL_NO_11	,");
		oSqlBuilder.AppendLine("	MAX(CASE WHEN CA.X_POS = 2 AND CA.Y_POS = 3 THEN CA.BINGO_BALL_NO ELSE 0 END) BALL_NO_12	,");
		oSqlBuilder.AppendLine("	MAX(CASE WHEN CA.X_POS = 3 AND CA.Y_POS = 3 THEN CA.BINGO_BALL_NO ELSE 0 END) BALL_NO_13	,");
		oSqlBuilder.AppendLine("	MAX(CASE WHEN CA.X_POS = 4 AND CA.Y_POS = 3 THEN CA.BINGO_BALL_NO ELSE 0 END) BALL_NO_14	,");
		oSqlBuilder.AppendLine("	MAX(CASE WHEN CA.X_POS = 5 AND CA.Y_POS = 3 THEN CA.BINGO_BALL_NO ELSE 0 END) BALL_NO_15	,");
		oSqlBuilder.AppendLine("	MAX(CASE WHEN CA.X_POS = 1 AND CA.Y_POS = 4 THEN CA.BINGO_BALL_NO ELSE 0 END) BALL_NO_16	,");
		oSqlBuilder.AppendLine("	MAX(CASE WHEN CA.X_POS = 2 AND CA.Y_POS = 4 THEN CA.BINGO_BALL_NO ELSE 0 END) BALL_NO_17	,");
		oSqlBuilder.AppendLine("	MAX(CASE WHEN CA.X_POS = 3 AND CA.Y_POS = 4 THEN CA.BINGO_BALL_NO ELSE 0 END) BALL_NO_18	,");
		oSqlBuilder.AppendLine("	MAX(CASE WHEN CA.X_POS = 4 AND CA.Y_POS = 4 THEN CA.BINGO_BALL_NO ELSE 0 END) BALL_NO_19	,");
		oSqlBuilder.AppendLine("	MAX(CASE WHEN CA.X_POS = 5 AND CA.Y_POS = 4 THEN CA.BINGO_BALL_NO ELSE 0 END) BALL_NO_20	,");
		oSqlBuilder.AppendLine("	MAX(CASE WHEN CA.X_POS = 1 AND CA.Y_POS = 5 THEN CA.BINGO_BALL_NO ELSE 0 END) BALL_NO_21	,");
		oSqlBuilder.AppendLine("	MAX(CASE WHEN CA.X_POS = 2 AND CA.Y_POS = 5 THEN CA.BINGO_BALL_NO ELSE 0 END) BALL_NO_22	,");
		oSqlBuilder.AppendLine("	MAX(CASE WHEN CA.X_POS = 3 AND CA.Y_POS = 5 THEN CA.BINGO_BALL_NO ELSE 0 END) BALL_NO_23	,");
		oSqlBuilder.AppendLine("	MAX(CASE WHEN CA.X_POS = 4 AND CA.Y_POS = 5 THEN CA.BINGO_BALL_NO ELSE 0 END) BALL_NO_24	,");
		oSqlBuilder.AppendLine("	MAX(CASE WHEN CA.X_POS = 5 AND CA.Y_POS = 5 THEN CA.BINGO_BALL_NO ELSE 0 END) BALL_NO_25	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_BINGO_CARD		C	,");
		oSqlBuilder.AppendLine("	T_BINGO_CARD_ARRAY	CA	");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	C.SITE_CD			= CA.SITE_CD			AND");
		oSqlBuilder.AppendLine("	C.BINGO_TERM_SEQ	= CA.BINGO_TERM_SEQ		AND");
		oSqlBuilder.AppendLine("	C.BINGO_CARD_NO		= CA.BINGO_CARD_NO		AND");
		oSqlBuilder.AppendLine("	C.SITE_CD			= :SITE_CD				AND");
		oSqlBuilder.AppendLine("	C.BINGO_TERM_SEQ	= :BINGO_TERM_SEQ		");
		oSqlBuilder.AppendLine("GROUP BY	");
		oSqlBuilder.AppendLine("	C.SITE_CD				,");
		oSqlBuilder.AppendLine("	C.BINGO_TERM_SEQ		,");
		oSqlBuilder.AppendLine("	C.BINGO_CARD_NO			,");
		oSqlBuilder.AppendLine("	C.HIGH_PROBABILITY_FLAG	");
		oSqlBuilder.AppendLine("ORDER BY	");
		oSqlBuilder.AppendLine("	C.SITE_CD				,");
		oSqlBuilder.AppendLine("	C.BINGO_TERM_SEQ		,");
		oSqlBuilder.AppendLine("	C.BINGO_CARD_NO			");

		try
		{
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(), conn))
			{
				cmd.BindByName = true;
				cmd.Parameters.Add("SITE_CD", pSiteCd);
				cmd.Parameters.Add("BINGO_TERM_SEQ", pBingoTermSeq);

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd))
				{
					da.Fill(oDataSet);
					return oDataSet;
				}
			}
		}
		finally
		{
			conn.Close();
		}
	}

	public int GetCount(string pSiteCd, string pBingoTermSeq)
	{
		string sCommand = "SELECT COUNT(*) FROM T_BINGO_CARD WHERE SITE_CD = :SITE_CD AND BINGO_TERM_SEQ = :BINGO_TERM_SEQ";
		try
		{
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sCommand, conn))
			{
				cmd.Parameters.Add("SITE_CD", pSiteCd);
				cmd.Parameters.Add("BINGO_TERM_SEQ", pBingoTermSeq);
				int iCount = Convert.ToInt32(cmd.ExecuteScalar());
				return iCount;
			}
		}
		finally
		{
			conn.Close();
		}
	}
}
