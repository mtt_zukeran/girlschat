﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: テスト送信先
--	Progaram ID		: TestMailAddr
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Text;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class TestMailAddr:DbSession {


	public TestMailAddr() {
	}

	public int GetPageCount(string pSiteCd) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(TEST_EMAIL_ADDR) AS ROW_COUNT FROM VW_TEST_MAIL_ADDR01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY SITE_CD,TEST_EMAIL_ADDR";
			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT ").AppendLine();
			sSql.Append("	SITE_CD			,").AppendLine();
			sSql.Append("	TEST_EMAIL_ADDR	,").AppendLine();
			sSql.Append("	USER_SEQ		,").AppendLine();
			sSql.Append("	USER_CHAR_NO	,").AppendLine();
			sSql.Append("	LOGIN_ID		,").AppendLine();
			sSql.Append("	SEX_CD			,").AppendLine();
			sSql.Append("	SEX_NM			,").AppendLine();
			sSql.Append("	HANDLE_NM		 ").AppendLine();
			sSql.Append("FROM(SELECT VW_TEST_MAIL_ADDR01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_TEST_MAIL_ADDR01").AppendLine();

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql.Append(sWhere).AppendLine();
			sSql.Append(")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW").AppendLine();
			sSql.Append(sOrder).AppendLine();

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_TEST_MAIL_ADDR01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		pWhere = pWhere + " WHERE SITE_CD = :SITE_CD";
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}
