﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 会話動画アクセス集計
--	Progaram ID		: AccessTalkMovie
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using iBridCommLib;

public class AccessTalkMovie:DbSession {

	public AccessTalkMovie() {
	}

	public DataSet GetList(string pSiteCd,string pFromYYYY,string pFromMM,string pFromDD,
										string pToYYYY,string pToMM,string pToDD) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();
			string sSql;
			sSql = "SELECT " +
					   "T_USER.LOGIN_ID		," +
					   "T_CAST.CAST_NM		," +
					   "T_SUM.SITE_CD		," +
					   "T_SUM.USER_SEQ		," +
					   "T_SUM.ACCESS_COUNT	," +
					   "T_SUM.TOTAL_USED_POINT," +
					   "T_SUM.TOTAL_USED_SEC " +
					"FROM " +
					   "T_USER,T_CAST,(" +
					   "SELECT " +
						   "SITE_CD			," +
						   "USER_SEQ	," +
						   "SUM(ACCESS_COUNT)		AS ACCESS_COUNT		," +
						   "SUM(TOTAL_USED_POINT)	AS TOTAL_USED_POINT ," +
						   "SUM(TOTAL_USED_SEC)		AS TOTAL_USED_SEC " +
					   "FROM " +
						   "VW_ACCESS_TALK_MOVIE01 " +
					   "WHERE " +
						   "SITE_CD= :SITE_CD	AND " +
						   "REPORT_DAY >= :REPORT_DAY_FROM AND REPORT_DAY <= :REPORT_DAY_TO " +
					   "GROUP BY SITE_CD, USER_SEQ ) T_SUM " +
				   "WHERE " +
					   "T_CAST.USER_SEQ = T_SUM.USER_SEQ AND " +
					   "T_CAST.USER_SEQ = T_USER.USER_SEQ " +
					"ORDER BY " +
						"SITE_CD, ACCESS_COUNT DESC, USER_SEQ  ";


			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("REPORT_DAY_FROM",pFromYYYY + "/" + pFromMM + "/" + pFromDD);
				cmd.Parameters.Add("REPORT_DAY_TO",pToYYYY + "/" + pToMM + "/" + pToDD);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetListByCast(string pSiteCd,string pFromYYYY,string pFromMM,string pFromDD,
										string pToYYYY,string pToMM,string pToDD,string pUserSeq) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();
			string sSql;

			if (pUserSeq == null) {
				pUserSeq = "0";
			}

			sSql = "SELECT " +
					   "VW_TALK_MOVIE02.LOGIN_ID		," +
					   "VW_TALK_MOVIE02.CAST_NM			," +
					   "VW_TALK_MOVIE02.MOVIE_TITLE		," +
					   "T_SUM.SITE_CD					," +
					   "T_SUM.USER_SEQ					," +
					   "T_SUM.MOVIE_SEQ					," +
					   "T_SUM.ACCESS_COUNT				," +
					   "T_SUM.TOTAL_USED_POINT			," +
					   "T_SUM.TOTAL_USED_SEC			" +
				   "FROM " +
					   "VW_TALK_MOVIE02,(" +
					   "SELECT " +
						   "SITE_CD			," +
						   "USER_SEQ		," +
						   "MOVIE_SEQ		," +
						   "SUM(ACCESS_COUNT)		AS ACCESS_COUNT		," +
						   "SUM(TOTAL_USED_POINT)	AS TOTAL_USED_POINT ," +
						   "SUM(TOTAL_USED_SEC)		AS TOTAL_USED_SEC " +
					   "FROM " +
						   "VW_ACCESS_TALK_MOVIE01 " +
					   "WHERE " +
						   "SITE_CD		=:SITE_CD	AND " +
						   "USER_SEQ	=:USER_SEQ	AND " +
						   "REPORT_DAY >= :REPORT_DAY_FROM AND REPORT_DAY <= :REPORT_DAY_TO " +
					   "GROUP BY SITE_CD, USER_SEQ,MOVIE_SEQ) T_SUM " +
				   "WHERE " +
						"VW_TALK_MOVIE02.SITE_CD	= T_SUM.SITE_CD		AND " +
						"VW_TALK_MOVIE02.USER_SEQ	= T_SUM.USER_SEQ	AND " +
						"VW_TALK_MOVIE02.MOVIE_SEQ	= T_SUM.MOVIE_SEQ " +
					"ORDER BY SITE_CD, USER_SEQ,ACCESS_COUNT DESC,MOVIE_SEQ ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("REPORT_DAY_FROM",pFromYYYY + "/" + pFromMM + "/" + pFromDD);
				cmd.Parameters.Add("REPORT_DAY_TO",pToYYYY + "/" + pToMM + "/" + pToDD);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}

		return ds;
	}
}
