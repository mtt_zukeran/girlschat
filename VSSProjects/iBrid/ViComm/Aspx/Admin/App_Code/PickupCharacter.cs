﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ピックアップキャラクター

--	Progaram ID		: PickupCharacter
--
--  Creation Date	: 2010.11.15
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System.Text.RegularExpressions;
using iBridCommLib;
using ViComm;

public class PickupCharacter :DbSession{
	public PickupCharacter() { }
	
	public bool Excists(string pSiteCd, string pPickupId, string pUserSeq,string pUserCharNo) {

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT					").AppendLine();
		oSqlBuilder.Append("	COUNT(*)			").AppendLine();
		oSqlBuilder.Append(" FROM					").AppendLine();
		oSqlBuilder.Append("	T_PICKUP_CHARACTER	").AppendLine();
		oSqlBuilder.Append(" WHERE				").AppendLine();
		oSqlBuilder.Append("	SITE_CD			=	:SITE_CD		AND	").AppendLine();
		oSqlBuilder.Append("	PICKUP_ID		=	:PICKUP_ID		AND	").AppendLine();
		oSqlBuilder.Append("	USER_SEQ		=	:USER_SEQ		AND	").AppendLine();
		oSqlBuilder.Append("	USER_CHAR_NO	=	:USER_CHAR_NO		").AppendLine();

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD", pSiteCd);
				cmd.Parameters.Add(":PICKUP_ID", pPickupId);
				cmd.Parameters.Add(":USER_SEQ", pUserSeq);
				cmd.Parameters.Add(":USER_CHAR_NO", pUserCharNo);
				
				return ((decimal)cmd.ExecuteScalar()>0);
			}
		} finally {
			conn.Close();
		}
	}

	public DataSet GetList(string pSiteCd, string pPickupId) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT ").AppendLine();
		oSqlBuilder.Append("	SITE_CD							, 	").AppendLine();
		oSqlBuilder.Append("	PICKUP_ID						, 	").AppendLine();
		oSqlBuilder.Append("	USER_SEQ						, 	").AppendLine();
		oSqlBuilder.Append("	USER_CHAR_NO					, 	").AppendLine();
		oSqlBuilder.Append("	COMMENT_PICKUP					, 	").AppendLine();
		oSqlBuilder.Append("	PICKUP_START_PUB_DAY			, 	").AppendLine();
		oSqlBuilder.Append("	PICKUP_END_PUB_DAY				, 	").AppendLine();
		oSqlBuilder.Append("	PRIORITY						, 	").AppendLine();
		oSqlBuilder.Append("	PICKUP_FLAG						, 	").AppendLine();
		oSqlBuilder.Append("	UPDATE_DATE						, 	").AppendLine();
		oSqlBuilder.Append("	REVISION_NO						, 	").AppendLine();
		oSqlBuilder.Append("	PICKUP_TYPE						,	").AppendLine();
		oSqlBuilder.Append("	LOGIN_ID						,	").AppendLine();
		oSqlBuilder.Append("	CAST_NM							,	").AppendLine();
		oSqlBuilder.Append("	HANDLE_NM						,	").AppendLine();
		oSqlBuilder.Append("	ACT_CATEGORY_NM					,	").AppendLine();
		oSqlBuilder.Append("	OBJ_NOT_APPROVE_FLAG			,	").AppendLine();
		oSqlBuilder.Append("	OBJ_NOT_PUBLISH_FLAG			,	").AppendLine();
		oSqlBuilder.Append("	SMALL_PHOTO_IMG_PATH				").AppendLine();
		oSqlBuilder.Append(" FROM							").AppendLine();
		oSqlBuilder.Append("	VW_PICKUP_CHARACTER01		").AppendLine();
		oSqlBuilder.Append(" WHERE									").AppendLine();
		oSqlBuilder.Append("	SITE_CD					= :SITE_CD						AND	").AppendLine();
		oSqlBuilder.Append("	PICKUP_ID				= :PICKUP_ID					AND ").AppendLine();
		oSqlBuilder.Append("	USER_STATUS				= :USER_STATUS					AND ").AppendLine();
		oSqlBuilder.Append("	NA_FLAG					= :NA_FLAG						    ").AppendLine();
		oSqlBuilder.Append("ORDER BY PRIORITY ASC,SITE_CD,PICKUP_ID,USER_SEQ,USER_CHAR_NO").AppendLine();
		
		DataSet oDs = new DataSet();
		try{
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn))
			using(da = new OracleDataAdapter(cmd)){
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD"				, pSiteCd);
				cmd.Parameters.Add(":PICKUP_ID"				,pPickupId);
				cmd.Parameters.Add(":USER_STATUS"			,ViCommConst.USER_WOMAN_NORMAL);
				cmd.Parameters.Add(":NA_FLAG"				,ViCommConst.FLAG_OFF);
				da.Fill(oDs);					
			}			
		}finally{
			conn.Close();
		}
		this.AppendExtendInfo(oDs.Tables[0]);
		return oDs;
	}

	public DataSet GetPageCollection(string pSiteCd, string pPickupId, string pLoginId, string pHandleNm, string pPickupFlag, string pSortType, int startRowIndex, int maximumRows) {
		return GetPageCollection(pSiteCd, pPickupId, pLoginId, pHandleNm, pPickupFlag, false, false, pSortType, startRowIndex, maximumRows);
	}
	public DataSet GetPageCollection(string pSiteCd,string pPickupId,string pLoginId,string pHandleNm,string pPickupFlag,bool pCheckUserStatus,bool pCheckNaFlag,string pSortType,int startRowIndex,int maximumRows) {
		int iUserStatusMask = pCheckUserStatus ? ViCommConst.MASK_WOMAN_NORMAL : 0;
		string sNaFlag = pCheckNaFlag ? ViCommConst.FLAG_OFF_STR : string.Empty;
		return GetPageCollection(pSiteCd,pPickupId,pLoginId,pHandleNm,pPickupFlag,iUserStatusMask,sNaFlag,pSortType,startRowIndex,maximumRows);
	}
	public DataSet GetPageCollection(string pSiteCd, string pPickupId, string pLoginId, string pHandleNm, string pPickupFlag, int pUserStatusMask, string pNaFlag, string pSortType, int startRowIndex, int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT ");
		oSqlBuilder.AppendLine("	SITE_CD							, 	");
		oSqlBuilder.AppendLine("	PICKUP_ID						, 	");
		oSqlBuilder.AppendLine("	USER_SEQ						, 	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO					, 	");
		oSqlBuilder.AppendLine("	COMMENT_PICKUP					, 	");
		oSqlBuilder.AppendLine("	PICKUP_START_PUB_DAY			, 	");
		oSqlBuilder.AppendLine("	PICKUP_END_PUB_DAY				, 	");
		oSqlBuilder.AppendLine("	PRIORITY						, 	");
		oSqlBuilder.AppendLine("	PICKUP_FLAG						, 	");
		oSqlBuilder.AppendLine("	UPDATE_DATE						, 	");
		oSqlBuilder.AppendLine("	REVISION_NO						, 	");
		oSqlBuilder.AppendLine("	PICKUP_TYPE						,	");
		oSqlBuilder.AppendLine("	LOGIN_ID						,	");
		oSqlBuilder.AppendLine("	CAST_NM							,	");
		oSqlBuilder.AppendLine("	HANDLE_NM						,	");
		oSqlBuilder.AppendLine("	ACT_CATEGORY_NM					,	");
		oSqlBuilder.AppendLine("	OBJ_NOT_APPROVE_FLAG			,	");
		oSqlBuilder.AppendLine("	OBJ_NOT_PUBLISH_FLAG			,	");
		oSqlBuilder.AppendLine("	SMALL_PHOTO_IMG_PATH			,	");
		oSqlBuilder.AppendLine("	USER_STATUS						,	");
		oSqlBuilder.AppendLine("	NA_FLAG								");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine("	VW_PICKUP_CHARACTER01		");
		
		string sSortExpression = "ORDER BY PRIORITY ASC,SITE_CD,PICKUP_ID,USER_SEQ,USER_CHAR_NO";
		string sWhere;
		OracleParameter[] oParameters = this.CreateWhere(pSiteCd, pPickupId, pLoginId, pHandleNm, pPickupFlag, pUserStatusMask, pNaFlag, out sWhere);
		string sPagingSql;
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhere).ToString(), sSortExpression, startRowIndex, maximumRows, out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql, conn))
			using (da = new OracleDataAdapter(cmd)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oParameters);
				cmd.Parameters.AddRange(oPagingParams);
				da.Fill(oDs);
			}
		} finally {
			conn.Close();
		}
		this.AppendExtendInfo(oDs.Tables[0]);
		return oDs;
	}
	public int GetPageCount(string pSiteCd, string pPickupId, string pLoginId, string pHandleNm, string pPickupFlag) {
		return GetPageCount(pSiteCd, pPickupId, pLoginId, pHandleNm, pPickupFlag, false, false);
	}
	public int GetPageCount(string pSiteCd,string pPickupId,string pLoginId,string pHandleNm,string pPickupFlag,bool pCheckUserStatus,bool pCheckNaFlag) {
		int iUserStatusMask = pCheckUserStatus ? ViCommConst.MASK_WOMAN_NORMAL : 0;
		string sNaFlag = pCheckNaFlag ? ViCommConst.FLAG_OFF_STR : string.Empty;
		return GetPageCount(pSiteCd,pPickupId,pLoginId,pHandleNm,pPickupFlag,iUserStatusMask,sNaFlag);
	}
	public int GetPageCount(string pSiteCd,string pPickupId,string pLoginId,string pHandleNm,string pPickupFlag, int pUserStatusMask, string pNaFlag) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine("	VW_PICKUP_CHARACTER01		");

		string sWhere;
		OracleParameter[] oParameters = this.CreateWhere(pSiteCd, pPickupId, pLoginId, pHandleNm, pPickupFlag, pUserStatusMask, pNaFlag, out sWhere);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhere).ToString(), conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oParameters);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	private OracleParameter[] CreateWhere(string pSiteCd, string pPickupId, string pLoginId, string pHandleNm, string pPickupFlag, int pUserStatusMask, string pNaFlag, out string pWhere) {
		pWhere = string.Empty;
		List<OracleParameter> parameterList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD	= :SITE_CD", ref pWhere);
			parameterList.Add(new OracleParameter("SITE_CD", pSiteCd));
		}
		if (!string.IsNullOrEmpty(pPickupId)) {
			SysPrograms.SqlAppendWhere("PICKUP_ID = :PICKUP_ID", ref pWhere);
			parameterList.Add(new OracleParameter("PICKUP_ID", pPickupId));
		}
		if (!string.IsNullOrEmpty(pLoginId)) {
			string sLoginQuery = string.Empty;

			if (Regex.IsMatch(pLoginId,",|\r?\n")) {
				string sConjunction = "OR ";
				string[] sLoginIdArray = Regex.Split(pLoginId,",|\r?\n");
				StringBuilder oLoginIdWhere = new StringBuilder();
				for (int iIndex = 0; iIndex < sLoginIdArray.Length; iIndex++) {
					if (string.IsNullOrEmpty(sLoginIdArray[iIndex].Trim())) {
						continue;
					}
					oLoginIdWhere.AppendFormat("LOGIN_ID = :LOGIN_ID{0} OR ",iIndex);
					parameterList.Add(new OracleParameter(string.Concat("LOGIN_ID",iIndex),sLoginIdArray[iIndex].Trim()));
				}

				if (oLoginIdWhere.Length > 0) {
					sLoginQuery = string.Format("({0})",oLoginIdWhere.Remove(oLoginIdWhere.Length - sConjunction.Length,sConjunction.Length));
					SysPrograms.SqlAppendWhere(sLoginQuery,ref pWhere);
				}
			} else {
				SysPrograms.SqlAppendWhere("LOGIN_ID = :LOGIN_ID",ref pWhere);
				parameterList.Add(new OracleParameter("LOGIN_ID",pLoginId));
			}
		}
		if (!string.IsNullOrEmpty(pHandleNm)) {
			SysPrograms.SqlAppendWhere("HANDLE_NM LIKE :HANDLE_NM || '%'", ref pWhere);
			parameterList.Add(new OracleParameter("HANDLE_NM", pHandleNm));
		}
		if (!string.IsNullOrEmpty(pPickupFlag)) {
			SysPrograms.SqlAppendWhere("PICKUP_FLAG = :PICKUP_FLAG", ref pWhere);
			parameterList.Add(new OracleParameter("PICKUP_FLAG", pPickupFlag));
		}

		string[] sValues = new string[6];
		int iCnt = 0;
		string sWhere = string.Empty;

		if (pUserStatusMask != 0) {
			if ((pUserStatusMask & ViCommConst.MASK_WOMAN_AUTH_WAIT) != 0) {
				sValues[iCnt++] = ViCommConst.USER_WOMAN_AUTH_WAIT;
			}
			if ((pUserStatusMask & ViCommConst.MASK_WOMAN_NORMAL) != 0) {
				sValues[iCnt++] = ViCommConst.USER_WOMAN_NORMAL;
			}
			if ((pUserStatusMask & ViCommConst.MASK_WOMAN_STOP) != 0) {
				sValues[iCnt++] = ViCommConst.USER_WOMAN_STOP;
			}
			if ((pUserStatusMask & ViCommConst.MASK_WOMAN_HOLD) != 0) {
				sValues[iCnt++] = ViCommConst.USER_WOMAN_HOLD;
			}
			if ((pUserStatusMask & ViCommConst.MASK_WOMAN_RESIGNED) != 0) {
				sValues[iCnt++] = ViCommConst.USER_WOMAN_RESIGNED;
			}
			if ((pUserStatusMask & ViCommConst.MASK_WOMAN_BAN) != 0) {
				sValues[iCnt++] = ViCommConst.USER_WOMAN_BAN;
			}
			sWhere = "(USER_STATUS IN (";
			for (int i = 0; i < iCnt; i++) {
				sWhere = sWhere + ":USER_STATUS" + i.ToString() + ",";
				parameterList.Add(new OracleParameter("USER_STATUS" + i.ToString(),int.Parse(sValues[i])));
			}
			sWhere = sWhere.Substring(0,sWhere.Length - 1) + ")) ";
			SysPrograms.SqlAppendWhere(sWhere,ref pWhere);
		}

		if (!string.IsNullOrEmpty(pNaFlag)) {
			SysPrograms.SqlAppendWhere("NA_FLAG	= :NA_FLAG",ref pWhere);
			parameterList.Add(new OracleParameter("NA_FLAG",pNaFlag));
		}

		return parameterList.ToArray();
	}

	public List<string> GetPickupIdList(string pSiteCd, string pUserSeq, string pUserCharNo) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT ").AppendLine();
		oSqlBuilder.Append("	PICKUP_ID				 	").AppendLine();
		oSqlBuilder.Append(" FROM							").AppendLine();
		oSqlBuilder.Append("	T_PICKUP_CHARACTER			").AppendLine();
		oSqlBuilder.Append(" WHERE									").AppendLine();
		oSqlBuilder.Append("	SITE_CD			=	:SITE_CD		AND	").AppendLine();
		oSqlBuilder.Append("	USER_SEQ		=	:USER_SEQ		AND	").AppendLine();
		oSqlBuilder.Append("	USER_CHAR_NO	=	:USER_CHAR_NO		").AppendLine();
		oSqlBuilder.Append(" ORDER BY									").AppendLine();
		oSqlBuilder.Append("	SITE_CD,PICKUP_ID,USER_SEQ,USER_CHAR_NO	").AppendLine();

		List<string> oOtherPickupList = new List<string>();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD", pSiteCd);
				cmd.Parameters.Add(":USER_SEQ", pUserSeq);
				cmd.Parameters.Add(":USER_CHAR_NO", pUserCharNo);
				
				using(OracleDataReader oDataReader = cmd.ExecuteReader()){
					while(oDataReader.Read()){
						string sOtherPickupId = iBridUtil.GetStringValue(oDataReader["PICKUP_ID"]);

						oOtherPickupList.Add(sOtherPickupId);
					}
				}
			}
		} finally {
			conn.Close();
		}
		
		return oOtherPickupList;
	}
	
	private void AppendExtendInfo(DataTable pPickupCharDataTable){
		
		pPickupCharDataTable.Columns.Add(new DataColumn("OTHER_PICKUP",typeof(string)));
	
		if(pPickupCharDataTable.Rows.Count==0)return;
		
		foreach(DataRow oPickupCharDataRow in pPickupCharDataTable.Rows){
			string sSiteCd = iBridUtil.GetStringValue(oPickupCharDataRow["SITE_CD"]);
			string sPickupId = iBridUtil.GetStringValue(oPickupCharDataRow["PICKUP_ID"]);
			string sUserSeq = iBridUtil.GetStringValue(oPickupCharDataRow["USER_SEQ"]);
			string sUserCharNo = iBridUtil.GetStringValue(oPickupCharDataRow["USER_CHAR_NO"]);

			string sOtherPickup = string.Empty;
			foreach (string sOtherPickupId in this.GetPickupIdList(sSiteCd, sUserSeq, sUserCharNo)) {
				if(sOtherPickupId.Equals(sPickupId)) continue;
				if (sOtherPickup.Length != 0) {
					sOtherPickup += ",";
				}
				sOtherPickup += sOtherPickupId;				
			}
			oPickupCharDataRow["OTHER_PICKUP"] = sOtherPickup;
		}
	}
}
