﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 管理者連絡
--	Progaram ID		: AdminReport
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  2009/07/23  iBrid(Y.Inoue)

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using System.Collections;
using System.Data;

public class AdminReport:DbSession {
	public int GetPageCount(string pSiteCd,string pSexCd) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_ADMIN_REPORT ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pSexCd,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pSexCd,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY START_PUB_DAY DESC,DOC_SEQ DESC";
			string sSql = "SELECT " +
								"DOC_SEQ			," +
								"DOC_TITLE			," +
								"START_PUB_DAY		," +
								"REPORT_HOUR_MIN	," +
								"HTML_DOC1			," +
								"HTML_DOC2			," +
								"HTML_DOC3			," +
								"HTML_DOC4			," +
								"HTML_DOC5			," +
								"END_PUB_DAY		," +
								"UPDATE_DATE " +
							"FROM(" +
							" SELECT T_ADMIN_REPORT.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM T_ADMIN_REPORT  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pSexCd,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_ADMIN_REPORT");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,string pSexCd,ref string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();
		
		pWhere = pWhere + " WHERE SITE_CD = :SITE_CD  ";
		list.Add(new OracleParameter("SITE_CD",pSiteCd));

		pWhere = pWhere + " AND SEX_CD = :SEX_CD  ";
		list.Add(new OracleParameter("SEX_CD",pSexCd));

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

}
