﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 商品

--	Progaram ID		: Product
--
--  Creation Date	: 2010.12.06
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class ProductHistory : DbSession {

	private string sConvertRoyaltyRate = "0.01";		//ロイヤリティをパーセンテージへ変換するときに利用

	public ProductHistory() { }

	public int GetPageCount(string pSiteCd, string pProductAgentCd, string pProductType, string pFromYYYY, string pFromMM, string pFromDD, string pToYYYY, string pToMM, string pToDD,string pProductMakerSumary) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append("SELECT	").AppendLine();
		oSqlBuilder.Append("	P.SITE_CD							,").AppendLine();
		oSqlBuilder.Append("	P.PRODUCT_AGENT_CD					,").AppendLine();
		oSqlBuilder.Append("	P.PRODUCT_SEQ						,").AppendLine();
		oSqlBuilder.Append("	P.PRODUCT_ID						,").AppendLine();
		oSqlBuilder.Append("	P.PRODUCT_NM						").AppendLine();
		oSqlBuilder.Append("FROM	").AppendLine();
		oSqlBuilder.Append("	T_PRODUCT P				,").AppendLine();
		oSqlBuilder.Append("	T_PRODUCT_BUY_HISTORY H	").AppendLine();

		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pProductAgentCd,pProductType,pFromYYYY,pFromMM,pFromDD,pToYYYY,pToMM,pToDD,pProductMakerSumary,ref sWhereClause);

		oSqlBuilder.Insert(0, "SELECT COUNT(1) FROM (");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine(")");

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				cmd.BindByName = true;

				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pProductAgentCd,string pProductType,string pFromYYYY,string pFromMM,string pFromDD,string pToYYYY,string pToMM,string pToDD,string pProductMakerSumary,int startRowIndex,int maximumRows) {

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append("SELECT	").AppendLine();
		oSqlBuilder.Append("	P.SITE_CD							,").AppendLine();
		oSqlBuilder.Append("	P.PRODUCT_AGENT_CD					,").AppendLine();
		oSqlBuilder.Append("	P.PRODUCT_SEQ						,").AppendLine();
		oSqlBuilder.Append("	P.PRODUCT_ID						,").AppendLine();
		oSqlBuilder.Append("	P.PRODUCT_NM						,").AppendLine();
		oSqlBuilder.Append("	COUNT(P.PRODUCT_ID)		BUY_COUNT	,").AppendLine();
		oSqlBuilder.Append("	SUM(CASE WHEN H.SPECIAL_CHARGE_POINT = 0 THEN H.CHARGE_POINT ELSE H.SPECIAL_CHARGE_POINT END) TOTAL_POINT	,").AppendLine();
		oSqlBuilder.AppendFormat("	SUM(CASE WHEN H.SPECIAL_CHARGE_POINT = 0 THEN TRUNC(H.CHARGE_POINT * (H.ROYALTY_RATE * {0}),2) ELSE TRUNC(H.SPECIAL_CHARGE_POINT * (H.ROYALTY_RATE * {0}),2) END) ROYALTY_POINT	,",sConvertRoyaltyRate).AppendLine();
		oSqlBuilder.Append("	P.PRODUCT_MAKER_SUMMARY				 ").AppendLine();
		oSqlBuilder.Append("FROM	").AppendLine();
		oSqlBuilder.Append("	T_PRODUCT P				,").AppendLine();
		oSqlBuilder.Append("	T_PRODUCT_BUY_HISTORY H	").AppendLine();

		string sSortExpression = "ORDER BY P.PRODUCT_ID";
		string sWhereClause = string.Empty;
		string sPagesSql = string.Empty;

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd, pProductAgentCd, pProductType, pFromYYYY, pFromMM, pFromDD, pToYYYY, pToMM, pToDD, pProductMakerSumary, ref sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.AppendLine(sWhereClause).ToString(), sSortExpression, startRowIndex, maximumRows, out sPagesSql);

		DataSet oProductDataSet = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagesSql,conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);
				cmd.BindByName = true;

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oProductDataSet);
				}
			}
		} finally {
			conn.Close();
		}
		return oProductDataSet;
	}

	public DataSet GetList(string pSiteCd,string pProductAgentCd,string pProductType,string pFromYYYY,string pFromMM,string pFromDD,string pToYYYY,string pToMM,string pToDD,string pProductMakerSumary) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append("SELECT	").AppendLine();
		oSqlBuilder.Append("	P.SITE_CD							,").AppendLine();
		oSqlBuilder.Append("	P.PRODUCT_AGENT_CD					,").AppendLine();
		oSqlBuilder.Append("	P.PRODUCT_SEQ						,").AppendLine();
		oSqlBuilder.Append("	P.PRODUCT_ID						,").AppendLine();
		oSqlBuilder.Append("	P.PRODUCT_NM						,").AppendLine();
		oSqlBuilder.Append("	COUNT(P.PRODUCT_ID)		BUY_COUNT	,").AppendLine();
		oSqlBuilder.Append("	SUM(CASE WHEN H.SPECIAL_CHARGE_POINT = 0 THEN H.CHARGE_POINT ELSE H.SPECIAL_CHARGE_POINT END) TOTAL_POINT	,").AppendLine();
		oSqlBuilder.AppendFormat("	SUM(CASE WHEN H.SPECIAL_CHARGE_POINT = 0 THEN TRUNC(H.CHARGE_POINT * (H.ROYALTY_RATE * {0}),2) ELSE TRUNC(H.SPECIAL_CHARGE_POINT * (H.ROYALTY_RATE * {0}),2) END) ROYALTY_POINT	,",sConvertRoyaltyRate).AppendLine();
		oSqlBuilder.Append("	P.PRODUCT_MAKER_SUMMARY				 ").AppendLine();
		oSqlBuilder.Append("FROM	").AppendLine();
		oSqlBuilder.Append("	T_PRODUCT P				,").AppendLine();
		oSqlBuilder.Append("	T_PRODUCT_BUY_HISTORY H	").AppendLine();

		string sSortExpression = "ORDER BY P.PRODUCT_ID";
		string sWhereClause = string.Empty;

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pProductAgentCd,pProductType,pFromYYYY,pFromMM,pFromDD,pToYYYY,pToMM,pToDD,pProductMakerSumary,ref sWhereClause);

		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine(sSortExpression);

		DataSet oProductDataSet = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				cmd.BindByName = true;

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oProductDataSet);
				}
			}
		} finally {
			conn.Close();
		}
		return oProductDataSet;
	}

	private OracleParameter[] CreateWhere(string pSiteCd, string pProductAgentCd, string pProductType, string pFromYYYY, string pFromMM, string pFromDD, string pToYYYY, string pToMM, string pToDD, string pProductMakerSumary, ref string pWhere) {
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.Append("WHERE	").AppendLine();
		oSqlBuilder.Append("	P.SITE_CD 			=	H.SITE_CD			AND").AppendLine();
		oSqlBuilder.Append("	P.PRODUCT_AGENT_CD	=	H.PRODUCT_AGENT_CD	AND").AppendLine();
		oSqlBuilder.Append("	P.PRODUCT_SEQ		=	H.PRODUCT_SEQ		AND").AppendLine();
		oSqlBuilder.Append("	P.SITE_CD			=	:SITE_CD			AND").AppendLine();
		oSqlBuilder.Append("	P.PRODUCT_TYPE		=	:PRODUCT_TYPE		AND").AppendLine();
		oSqlBuilder.Append("	(BUY_DATE >= :BUY_DATE_FROM AND BUY_DATE < :BUY_DATE_TO)");

		string sGroupExpression = "GROUP BY P.SITE_CD, P.PRODUCT_AGENT_CD, P.PRODUCT_SEQ, P.PRODUCT_ID, P.PRODUCT_NM, P.PRODUCT_MAKER_SUMMARY";

		DateTime dtFrom = DateTime.Parse(string.Format("{0}/{1}/{2}", pFromYYYY, pFromMM, pFromDD) + " " + "00:00:00");
		DateTime dtTo = DateTime.Parse(string.Format("{0}/{1}/{2}", pToYYYY, pToMM, pToDD) + " " + "23:59:59").AddSeconds(1);

		List<OracleParameter> oParameters = new List<OracleParameter>();
		oParameters.Add(new OracleParameter(":SITE_CD", pSiteCd));
		oParameters.Add(new OracleParameter(":PRODUCT_TYPE", pProductType));
		oParameters.Add(new OracleParameter(":BUY_DATE_FROM", dtFrom));
		oParameters.Add(new OracleParameter(":BUY_DATE_TO", dtTo));

		if (!string.IsNullOrEmpty(pProductAgentCd)) {
			oSqlBuilder.AppendLine("	AND");
			oSqlBuilder.AppendLine("	P.PRODUCT_AGENT_CD	=	:PRODUCT_AGENT_CD");
			oParameters.Add(new OracleParameter(":PRODUCT_AGENT_CD", pProductAgentCd));
		}


		if (!string.IsNullOrEmpty(pProductMakerSumary)) {
			string sProductMakerSumary = pProductMakerSumary.TrimEnd();
			if (!string.IsNullOrEmpty(pProductMakerSumary)) {
				oSqlBuilder.AppendLine("	AND");
				oSqlBuilder.AppendLine("	P.PRODUCT_MAKER_SUMMARY	LIKE	:PRODUCT_MAKER_SUMMARY||'%'");
				oParameters.Add(new OracleParameter(":PRODUCT_MAKER_SUMMARY",sProductMakerSumary));
			}			
		}

		oSqlBuilder.AppendLine(sGroupExpression);

		pWhere = oSqlBuilder.ToString();
		return oParameters.ToArray();
	}
}
