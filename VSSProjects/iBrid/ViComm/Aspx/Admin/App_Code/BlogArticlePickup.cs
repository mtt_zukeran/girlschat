﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: ブログ投稿ピックアップ


--	Progaram ID		: BlogArticlePickup
--  Creation Date	: 2011.03.24
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class BlogArticlePickup : DbSession {

	public BlogArticlePickup() {
	}

	public DataSet GetPriorityList(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	TO_CHAR(PRIORITY) || '番'	PRIORITY_TEXT,");
		oSqlBuilder.AppendLine("	PRIORITY					");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_BLOG_ARTICLE_PICKUP");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD");
		oSqlBuilder.AppendLine("ORDER BY	");
		oSqlBuilder.AppendLine("	SITE_CD, PRIORITY");

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(), conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(new OracleParameter("SITE_CD", pSiteCd));

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}
}
