﻿/*************************************************************************
--	System			: ViComm System
--	Sub System Name	: Admin
--	Title			: 変更履歴
--	Progaram ID		: Message
--
--  Creation Date	: 2010.07.05
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class Message:DbSession {

	public Message() {
	}

	public int GetPageCount(string pSiteCd,string pSexCd,string pCreateDateFrom,string pCreateDateTo) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT ";
			if (pSexCd.Equals(ViCommConst.MAN)) {
				sSql += " FROM VW_MESSAGE01 ";
			} else {
				sSql += " FROM VW_MESSAGE02 ";
			}

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pSexCd,pCreateDateFrom,pCreateDateTo,ref sWhere);
			sSql += sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pSexCd,string pCreateDateFrom,string pCreateDateTo,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY CREATE_DATE DESC";
			string sFrom;
			if (pSexCd.Equals(ViCommConst.MAN)) {
				sFrom = "VW_MESSAGE01";
			} else {
				sFrom = "VW_MESSAGE02";
			}

			string sSql = "SELECT " +
								"SITE_CD				," +
								"USER_SEQ				," +
								"USER_CHAR_NO			," +
								"PARTNER_USER_SEQ		," +
								"PARTNER_USER_CHAR_NO	," +
								"MESSAGE_SEQ			," +
								"SUBSTR(MESSAGE_DOC,1,30) AS MESSAGE_DOC," +
								"CREATE_DATE			," +
								"HANDLE_NM				," +
								"PARTNER_HANDLE_NM		" +
							"FROM(" +
							string.Format(" SELECT {0}.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM {0} ",sFrom);

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pSexCd,pCreateDateFrom,pCreateDateTo,ref sWhere);
			sSql += sWhere +
					")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW " +
					sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_MESSAGE01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pSexCd,string pCreateDateFrom,string pCreateDateTo,ref string pWhere) {
		ArrayList list = new ArrayList();
		pWhere = "";

		if (!pSiteCd.Equals("")) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD ",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!pSexCd.Equals("")) {
			SysPrograms.SqlAppendWhere("SEX_CD = :SEX_CD ",ref pWhere);
			list.Add(new OracleParameter("SEX_CD",pSexCd));
		}

		if (!pCreateDateFrom.Equals("") && !pCreateDateTo.Equals("")) {
			DateTime dtFrom = DateTime.Parse(pCreateDateFrom + " " + "00:00:00");
			DateTime dtTo = DateTime.Parse(pCreateDateTo + " " + "23:59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere("CREATE_DATE >= :CREATE_DATE_FROM AND CREATE_DATE < :CREATE_DATE_TO",ref pWhere);
			list.Add(new OracleParameter("CREATE_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("CREATE_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}
