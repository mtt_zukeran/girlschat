﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: お友達紹介

--	Progaram ID		: Friends
--
--  Creation Date	: 2011.02.02
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class Friends : DbSession {
	public Friends() {
	}

	/// <summary>
	/// 男性会員紹介履歴の件数取得
	/// </summary>
	/// <param name="pFriendIntroCd"></param>
	/// <returns></returns>
	public int GetManLogPageCount(string pFriendIntroCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_CAST_FRIEND_INTRO_LOG");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	FRIEND_INTRO_CD = :FRIEND_INTRO_CD");

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add("FRIEND_INTRO_CD",pFriendIntroCd);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	/// <summary>
	/// 男性会員紹介履歴のデータ取得
	/// </summary>
	/// <param name="pFriendIntroCd"></param>
	/// <param name="startRowIndex"></param>
	/// <param name="maximumRows"></param>
	/// <returns></returns>
	public DataSet GetManLogPageCollection(string pFriendIntroCd,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	UMC.SITE_CD,");
		oSqlBuilder.AppendLine("	U.LOGIN_ID,");
		oSqlBuilder.AppendLine("	UMC.HANDLE_NM,");
		oSqlBuilder.AppendLine("	UMC.REGIST_DATE,");
		oSqlBuilder.AppendLine("	U.LAST_LOGIN_DATE,");
		oSqlBuilder.AppendLine("	UMC.PROFILE_OK_FLAG,");
		oSqlBuilder.AppendLine("	UM.LIMIT_POINT");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_CAST_FRIEND_INTRO_LOG CFL,");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER UMC,");
		oSqlBuilder.AppendLine("	T_USER_MAN UM,");
		oSqlBuilder.AppendLine("	T_USER U");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	CFL.FRIEND_INTRO_CD		= :FRIEND_INTRO_CD	AND");
		oSqlBuilder.AppendLine("	CFL.PARTNER_USER_SEQ	= UMC.USER_SEQ		AND");
		oSqlBuilder.AppendLine("	UMC.USER_SEQ			= UM.USER_SEQ		AND");
		oSqlBuilder.AppendLine("	UMC.USER_SEQ			= U.USER_SEQ");

		string sPagingSql = string.Empty;
		string sSortExpression = "ORDER BY UMC.REGIST_DATE DESC";

		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDataSet = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add("FRIEND_INTRO_CD",pFriendIntroCd);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
				}
			}
		} finally {
			conn.Close();
		}
		return oDataSet;
	}

	public int GetManPageCount(string pSiteCd, string pFriendIntroCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	VW_USER_MAN_CHARACTER00");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	INTRODUCER_FRIEND_CD	= :FRIEND_INTRO_CD	AND");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD			");

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add("FRIEND_INTRO_CD",pFriendIntroCd);
				cmd.Parameters.Add("SITE_CD", pSiteCd);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetManPageCollection(string pSiteCd, string pFriendIntroCd, int startRowIndex, int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	SITE_CD				,");
		oSqlBuilder.AppendLine("	LOGIN_ID			,");
		oSqlBuilder.AppendLine("	HANDLE_NM			,");
		oSqlBuilder.AppendLine("	REGIST_DATE			,");
		oSqlBuilder.AppendLine("	LAST_LOGIN_DATE		,");
		oSqlBuilder.AppendLine("	PROFILE_OK_FLAG		,");
		oSqlBuilder.AppendLine("	LIMIT_POINT");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	VW_USER_MAN_CHARACTER00");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	INTRODUCER_FRIEND_CD	= :FRIEND_INTRO_CD	AND");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD			");

		string sPagingSql = string.Empty;
		string sSortExpression = "ORDER BY REGIST_DATE DESC";

		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(), sSortExpression, startRowIndex, maximumRows, out sPagingSql);

		DataSet oDataSet = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql, conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add("FRIEND_INTRO_CD",pFriendIntroCd);
				cmd.Parameters.Add("SITE_CD", pSiteCd);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
				}
			}
		} finally {
			conn.Close();
		}
		return oDataSet;
	}

	public int GetCastPageCount(string pSiteCd, string pFriendIntroCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	VW_CAST_CHARACTER00");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	INTRODUCER_FRIEND_CD	= :FRIEND_INTRO_CD	AND");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD			");

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add("INTRODUCER_FRIEND_CD",pFriendIntroCd);
				cmd.Parameters.Add("SITE_CD", pSiteCd);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetCastPageCollection(string pSiteCd, string pFriendIntroCd, int startRowIndex, int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	SITE_CD					,");
		oSqlBuilder.AppendLine("	LOGIN_ID				,");
		oSqlBuilder.AppendLine("	USER_CHAR_NO			,");
		oSqlBuilder.AppendLine("	HANDLE_NM				,");
		oSqlBuilder.AppendLine("	REGIST_DATE				,");
		oSqlBuilder.AppendLine("	LAST_LOGIN_DATE			,");
		oSqlBuilder.AppendLine("	NA_FLAG					,");
		oSqlBuilder.AppendLine("	TOTAL_PAYMENT_AMT");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	VW_CAST_CHARACTER00");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	INTRODUCER_FRIEND_CD	= :FRIEND_INTRO_CD	AND");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD			");

		string sPagingSql = string.Empty;
		string sSortExpression = "ORDER BY REGIST_DATE DESC";

		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(), sSortExpression, startRowIndex, maximumRows, out sPagingSql);

		DataSet oDataSet = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql, conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add("FRIEND_INTRO_CD",pFriendIntroCd);
				cmd.Parameters.Add("SITE_CD", pSiteCd);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
				}
			}
		} finally {
			conn.Close();
		}
		return oDataSet;
	}

}
