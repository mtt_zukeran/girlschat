﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ページアクセス集計 
--	Progaram ID		: AccessPage
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;
using ViComm;

public class AccessPage:DbSession {

	public AccessPage() {
	}

	public DataSet AccessInquiryDay(string pSiteCd,string pYYYY,string pMM,string pSexCd) {

		DataColumn col;
		DataSet ds = new DataSet();
		DataTable dt = new DataTable();

		col = new DataColumn("SITE_CD",System.Type.GetType("System.String"));
		dt.Columns.Add(col);

		col = new DataColumn("ACCESS_DAY",System.Type.GetType("System.String"));
		dt.Columns.Add(col);

		col = new DataColumn("PRINT_DAY",System.Type.GetType("System.String"));
		dt.Columns.Add(col);

		col = new DataColumn("PRINT_LINK",System.Type.GetType("System.String"));
		dt.Columns.Add(col);

		col = new DataColumn("ACCESS_DAY_OF_WEEK",System.Type.GetType("System.String"));
		dt.Columns.Add(col);

		col = new DataColumn("TOP_PAGE",System.Type.GetType("System.String"));
		dt.Columns.Add(col);

		col = new DataColumn("LINK_DAY",System.Type.GetType("System.String"));
		dt.Columns.Add(col);

		dt.Columns.Add(new DataColumn("USER_TOP_PAGE",System.Type.GetType("System.String")));
		dt.Columns.Add(new DataColumn("USER_TOP_PAGE_UNIQUE",System.Type.GetType("System.String")));
		dt.Columns.Add(new DataColumn("LOGIN_SUCCESS",System.Type.GetType("System.String")));
		dt.Columns.Add(new DataColumn("LOGIN_SUCCESS_UNIQUE",System.Type.GetType("System.String")));
		dt.Columns.Add(new DataColumn("CAST_PAGE",System.Type.GetType("System.String")));
		dt.Columns.Add(new DataColumn("CAST_PAGE_UNIQUE",System.Type.GetType("System.String")));
		dt.Columns.Add(new DataColumn("REGIST_USER",System.Type.GetType("System.String")));
		dt.Columns.Add(new DataColumn("REGIST_REPORT_USER",System.Type.GetType("System.String")));
		dt.Columns.Add(new DataColumn("RE_REGIST_COUNT",System.Type.GetType("System.String")));
		dt.Columns.Add(new DataColumn("WITHDRAWAL_COUNT",System.Type.GetType("System.String")));
		dt.Columns.Add(new DataColumn("FIRST_RECEIPT_COUNT",System.Type.GetType("System.String")));
		dt.Columns.Add(new DataColumn("RECEIPT_COUNT_UNIQUE",System.Type.GetType("System.String")));
		dt.Columns.Add(new DataColumn("RECEIPT_POINT_AF_COUNT_UNIQUE",System.Type.GetType("System.String")));

		if (pMM != null) {
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("ACCESS_INQUIRY_DAY");
				db.ProcedureInParm("PSITE_CD",DbType.VARCHAR2,pSiteCd);
				db.ProcedureInParm("PYYYY",DbType.VARCHAR2,pYYYY);
				db.ProcedureInParm("PMM",DbType.VARCHAR2,pMM);
				db.ProcedureInParm("PSEX_CD",DbType.VARCHAR2,pSexCd);

				db.ProcedureOutArrayParm("PACCESS_DAY",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("PACCESS_DAY_OF_WEEK",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("PTOP_PAGE",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("pUSER_TOP_PAGE",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("pUSER_TOP_PAGE_UNIQUE",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("pCAST_PAGE",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("pCAST_PAGE_UNIQUE",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("pLOGIN_SUCCESS",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("pLOGIN_SUCCESS_UNIQUE",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("pREGIST_USER",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("pREGIST_REPORT_USER",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("pRE_REGIST_COUNT",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("pWITHDRAWAL_COUNT",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("pFIRST_RECEIPT_COUNT",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("pRECEIPT_COUNT_UNIQUE",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("pRECEIPT_POINT_AF_COUNT_UNIQUE",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

				db.ExecuteProcedure();

				for (int i = 0;i < db.GetIntValue("PRECORD_COUNT");i++) {
					string sDay = db.GetArryStringValue("PACCESS_DAY",i);
					DataRow newRow = dt.NewRow();
					newRow["SITE_CD"] = pSiteCd;
					newRow["ACCESS_DAY"] = sDay;
					newRow["PRINT_DAY"] = sDay.Substring(8,2) + "日";
					newRow["PRINT_LINK"] = sDay.Substring(8,2) + "日のｱｸｾｽ状況";
					newRow["ACCESS_DAY_OF_WEEK"] = db.GetArryStringValue("PACCESS_DAY_OF_WEEK",i);
					newRow["TOP_PAGE"] = db.GetArryStringValue("PTOP_PAGE",i);
					newRow["USER_TOP_PAGE"] = db.GetArryStringValue("pUSER_TOP_PAGE",i);
					newRow["USER_TOP_PAGE_UNIQUE"] = db.GetArryStringValue("pUSER_TOP_PAGE_UNIQUE",i);
					newRow["CAST_PAGE"] = db.GetArryStringValue("PCAST_PAGE",i);
					newRow["CAST_PAGE_UNIQUE"] = db.GetArryStringValue("pCAST_PAGE_UNIQUE",i);
					newRow["LOGIN_SUCCESS"] = db.GetArryStringValue("pLOGIN_SUCCESS",i);
					newRow["LOGIN_SUCCESS_UNIQUE"] = db.GetArryStringValue("pLOGIN_SUCCESS_UNIQUE",i);
					newRow["REGIST_USER"] = db.GetArryStringValue("pREGIST_USER",i);
					newRow["REGIST_REPORT_USER"] = db.GetArryStringValue("pREGIST_REPORT_USER",i);
					newRow["RE_REGIST_COUNT"] = db.GetArryStringValue("pRE_REGIST_COUNT",i);
					newRow["WITHDRAWAL_COUNT"] = db.GetArryStringValue("pWITHDRAWAL_COUNT",i);
					newRow["FIRST_RECEIPT_COUNT"] = db.GetArryStringValue("pFIRST_RECEIPT_COUNT",i);
					newRow["RECEIPT_COUNT_UNIQUE"] = db.GetArryStringValue("pRECEIPT_COUNT_UNIQUE",i);
					newRow["RECEIPT_POINT_AF_COUNT_UNIQUE"] = db.GetArryStringValue("pRECEIPT_POINT_AF_COUNT_UNIQUE",i);
					newRow["LINK_DAY"] = sDay.Substring(8,2);
					dt.Rows.Add(newRow);
				}
			}
		} else {
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("ACCESS_INQUIRY_MONTH");
				db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
				db.ProcedureInParm("PYYYY",DbSession.DbType.VARCHAR2,pYYYY);
				db.ProcedureInParm("PSEX_CD",DbType.VARCHAR2,pSexCd);

				db.ProcedureOutArrayParm("PACCESS_MONTH",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("PTOP_PAGE",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("pUSER_TOP_PAGE",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("pUSER_TOP_PAGE_UNIQUE",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("pCAST_PAGE",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("pCAST_PAGE_UNIQUE",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("pLOGIN_SUCCESS",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("pLOGIN_SUCCESS_UNIQUE",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("pREGIST_USER",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("pREGIST_REPORT_USER",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("pRE_REGIST_COUNT",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("pWITHDRAWAL_COUNT",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("pFIRST_RECEIPT_COUNT",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("pRECEIPT_COUNT_UNIQUE",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("pRECEIPT_POINT_AF_COUNT_UNIQUE",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

				db.ExecuteProcedure();

				for (int i = 0;i < db.GetIntValue("PRECORD_COUNT");i++) {
					DataRow newRow = dt.NewRow();
					newRow["SITE_CD"] = pSiteCd;
					newRow["ACCESS_DAY"] = "";
					newRow["PRINT_DAY"] = "";
					newRow["PRINT_LINK"] = "";
					newRow["ACCESS_DAY_OF_WEEK"] = db.GetArryStringValue("PACCESS_MONTH",i);
					newRow["TOP_PAGE"] = db.GetArryStringValue("PTOP_PAGE",i);
					newRow["USER_TOP_PAGE"] = db.GetArryStringValue("pUSER_TOP_PAGE",i);
					newRow["USER_TOP_PAGE_UNIQUE"] = db.GetArryStringValue("pUSER_TOP_PAGE_UNIQUE",i);
					newRow["CAST_PAGE"] = db.GetArryStringValue("PCAST_PAGE",i);
					newRow["CAST_PAGE_UNIQUE"] = db.GetArryStringValue("pCAST_PAGE_UNIQUE",i);
					newRow["LOGIN_SUCCESS"] = db.GetArryStringValue("pLOGIN_SUCCESS",i);
					newRow["LOGIN_SUCCESS_UNIQUE"] = db.GetArryStringValue("pLOGIN_SUCCESS_UNIQUE",i);
					newRow["REGIST_USER"] = db.GetArryStringValue("pREGIST_USER",i);
					newRow["REGIST_REPORT_USER"] = db.GetArryStringValue("pREGIST_REPORT_USER",i);
					newRow["RE_REGIST_COUNT"] = db.GetArryStringValue("pRE_REGIST_COUNT",i);
					newRow["WITHDRAWAL_COUNT"] = db.GetArryStringValue("pWITHDRAWAL_COUNT",i);
					newRow["FIRST_RECEIPT_COUNT"] = db.GetArryStringValue("pFIRST_RECEIPT_COUNT",i);
					newRow["RECEIPT_COUNT_UNIQUE"] = db.GetArryStringValue("pRECEIPT_COUNT_UNIQUE",i);
					newRow["RECEIPT_POINT_AF_COUNT_UNIQUE"] = db.GetArryStringValue("pRECEIPT_POINT_AF_COUNT_UNIQUE",i);
					newRow["LINK_DAY"] = "";
					dt.Rows.Add(newRow);
				}
			}
		}

		ds.Tables.Add(dt);

		return ds;
	}

	public DataSet GetAdPerformanceList(
		string pSiteCd,
		string pFromYYYY,
		string pFromMM,
		string pFromDD,
		string pToYYYY,
		string pToMM,
		string pToDD,
		string pAdGroupCd,
		string pAdCd,
		string pSexCd,
		string pAdminType,
		string pSummaryType,
		string pUsedFlag
	) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sFromDay = pFromYYYY + '/' + pFromMM + '/' + pFromDD;
			string sToDay = pToYYYY + '/' + pToMM + '/' + pToDD;

			StringBuilder sSql = new StringBuilder();

			if (pSummaryType.Equals("1")) {
				// 広告コード別
				sSql.Append("SELECT ").AppendLine();
				sSql.Append("	T_AD.AD_NM								,").AppendLine();
				sSql.Append("	T_AD.USED_FLAG							,").AppendLine();
				sSql.Append("	T_SUM.SITE_CD							,").AppendLine();
				sSql.Append("	T_SUM.AD_CD								,").AppendLine();
				sSql.Append("	T_SUM.LOGIN_COUNT						,").AppendLine();
				sSql.Append("	T_SUM.ACCESS_COUNT						,").AppendLine();
				sSql.Append("	T_SUM.REGIST_COUNT						,").AppendLine();
				sSql.Append("	T_SUM.WITHDRAWAL_COUNT      			,").AppendLine();
				sSql.Append("	T_SUM.RE_REGIST_COUNT					,").AppendLine();
				sSql.Append("	T_SUM.LOGIN_SUCCESS_COUNT				,").AppendLine();
				sSql.Append("	T_SUM.LOGIN_SUCCESS_COUNT_UNIQUE		,").AppendLine();
				sSql.Append("	T_SUM.TOTAL_USED_POINT					,").AppendLine();
				sSql.Append("	T_SUM.LOGIN_COUNT_AD					,").AppendLine();
				sSql.Append("	T_SUM.ACCESS_COUNT_AD					,").AppendLine();
				sSql.Append("	T_SUM.REGIST_COUNT_AD					,").AppendLine();
				sSql.Append("	T_SUM.TOTAL_USED_POINT_AD				,").AppendLine();
				sSql.Append("	T_SUM.LOGIN_SUCCESS_COUNT_AD			,").AppendLine();
				sSql.Append("	T_SUM.LOGIN_SUCCESS_COUNT_UNIQUE_AD		,").AppendLine();
				sSql.Append("	T_SUM.REGIST_MAIL_SEND_FAILED_COUNT		,").AppendLine();
				sSql.Append("	NVL(T_SUM2.ORDINARY_SALES_AMT	,0) AS ORDINARY_SALES_AMT	,").AppendLine();
				sSql.Append("	NVL(T_SUM2.POINT_BACK_SALES_AMT	,0) AS POINT_BACK_SALES_AMT	,").AppendLine();
				sSql.Append("	NVL(T_SUM2.TOTAL_SALES_AMT		,0) AS TOTAL_SALES_AMT		,").AppendLine();
				sSql.Append("	NVL(T_SUM2.AFTER_PACK_SALES_AMT	,0) AS AFTER_PACK_SALES_AMT	").AppendLine();
			} else {
				// 広告グループ別
				sSql.Append("SELECT ").AppendLine();
				sSql.Append("	T_AD_GROUP.AD_GROUP_NM		,").AppendLine();
				sSql.Append("	T_SUM.SITE_CD				,").AppendLine();
				sSql.Append("	T_SITE_AD_GROUP.AD_GROUP_CD	,").AppendLine();
				sSql.Append("	SUM(T_SUM.LOGIN_COUNT					)	AS LOGIN_COUNT						,").AppendLine();
				sSql.Append("	SUM(T_SUM.ACCESS_COUNT					)	AS ACCESS_COUNT						,").AppendLine();
				sSql.Append("	SUM(T_SUM.REGIST_COUNT					)	AS REGIST_COUNT						,").AppendLine();
				sSql.Append("	SUM(T_SUM.WITHDRAWAL_COUNT				)	AS WITHDRAWAL_COUNT					,").AppendLine();
				sSql.Append("	SUM(T_SUM.RE_REGIST_COUNT				)	AS RE_REGIST_COUNT					,").AppendLine();
				sSql.Append("	SUM(T_SUM.LOGIN_SUCCESS_COUNT			)	AS LOGIN_SUCCESS_COUNT				,").AppendLine();
				sSql.Append("	SUM(T_SUM.LOGIN_SUCCESS_COUNT_UNIQUE	)	AS LOGIN_SUCCESS_COUNT_UNIQUE		,").AppendLine();
				sSql.Append("	SUM(T_SUM.TOTAL_USED_POINT				)	AS TOTAL_USED_POINT					,").AppendLine();
				sSql.Append("	SUM(T_SUM.LOGIN_COUNT_AD				)	AS LOGIN_COUNT_AD					,").AppendLine();
				sSql.Append("	SUM(T_SUM.ACCESS_COUNT_AD				)	AS ACCESS_COUNT_AD					,").AppendLine();
				sSql.Append("	SUM(T_SUM.REGIST_COUNT_AD				)	AS REGIST_COUNT_AD					,").AppendLine();
				sSql.Append("	SUM(T_SUM.TOTAL_USED_POINT_AD			)	AS TOTAL_USED_POINT_AD				,").AppendLine();
				sSql.Append("	SUM(T_SUM.LOGIN_SUCCESS_COUNT_AD		)	AS LOGIN_SUCCESS_COUNT_AD			,").AppendLine();
				sSql.Append("	SUM(T_SUM.LOGIN_SUCCESS_COUNT_UNIQUE_AD	)	AS LOGIN_SUCCESS_COUNT_UNIQUE_AD	,").AppendLine();
				sSql.Append("	SUM(T_SUM.REGIST_MAIL_SEND_FAILED_COUNT	)	AS REGIST_MAIL_SEND_FAILED_COUNT	,").AppendLine();
				sSql.Append("	SUM(NVL(T_SUM2.ORDINARY_SALES_AMT	,0)	)	AS ORDINARY_SALES_AMT				,").AppendLine();
				sSql.Append("	SUM(NVL(T_SUM2.POINT_BACK_SALES_AMT	,0)	)	AS POINT_BACK_SALES_AMT				,").AppendLine();
				sSql.Append("	SUM(NVL(T_SUM2.TOTAL_SALES_AMT		,0)	)	AS TOTAL_SALES_AMT					,").AppendLine();
				sSql.Append("	SUM(NVL(T_SUM2.AFTER_PACK_SALES_AMT	,0)	)	AS AFTER_PACK_SALES_AMT				").AppendLine();
			}
			sSql.Append("FROM ").AppendLine();
			sSql.Append("	T_AD,T_SITE , ").AppendLine();
			if (!pSummaryType.Equals("1")) {
				sSql.Append("	T_AD_GROUP		,").AppendLine();
			}
			sSql.Append("	T_SITE_AD_GROUP	,(").AppendLine();
			sSql.Append("	SELECT ").AppendLine();
			sSql.Append("		AD_CD																										,").AppendLine();
			sSql.Append("		SITE_CD																										,").AppendLine();
			sSql.Append("		SUM(LOGIN_COUNT					) AS LOGIN_COUNT															,").AppendLine();
			sSql.Append("		SUM(ACCESS_COUNT				) AS ACCESS_COUNT															,").AppendLine();
			sSql.Append("		SUM(REGIST_COUNT				) AS REGIST_COUNT															,").AppendLine();
			sSql.Append("		SUM(WITHDRAWAL_COUNT			) AS WITHDRAWAL_COUNT														,").AppendLine();
			sSql.Append("		SUM(RE_REGIST_COUNT				) AS RE_REGIST_COUNT														,").AppendLine();
			sSql.Append("		SUM(TOTAL_USED_POINT			) AS TOTAL_USED_POINT														,").AppendLine();
			sSql.Append("		SUM(LOGIN_SUCCESS_COUNT			) AS LOGIN_SUCCESS_COUNT													,").AppendLine();
			sSql.Append("		SUM(LOGIN_SUCCESS_COUNT_UNIQUE	) AS LOGIN_SUCCESS_COUNT_UNIQUE												,").AppendLine();
			sSql.Append("		SUM(RES_LOGIN_COUNT					- MAQIA_AF_FAIL_LOGIN_COUNT			) AS LOGIN_COUNT_AD					,").AppendLine();
			sSql.Append("		SUM(RES_ACCESS_COUNT				- MAQIA_AF_FAIL_ACCESS_COUNT		) AS ACCESS_COUNT_AD				,").AppendLine();
			sSql.Append("		SUM(RES_REGIST_COUNT				- MAQIA_AF_FAIL_REGIST_COUNT		) AS REGIST_COUNT_AD				,").AppendLine();
			sSql.Append("		SUM(RES_TOTAL_USED_POINT			- MAQIA_AF_FAIL_TOTAL_USED_POINT	) AS TOTAL_USED_POINT_AD			,").AppendLine();
			sSql.Append("		SUM(RES_LOGIN_SUCCESS_COUNT			- FAIL_LOGIN_SUCCESS				) AS LOGIN_SUCCESS_COUNT_AD			,").AppendLine();
			sSql.Append("		SUM(RES_LOGIN_SUCCESS_COUNT_UNIQUE	- FAIL_LOGIN_SUCCESS_UNIQUE			) AS LOGIN_SUCCESS_COUNT_UNIQUE_AD	,").AppendLine();
			sSql.Append("		SUM(REGIST_MAIL_SEND_FAILED_COUNT) AS REGIST_MAIL_SEND_FAILED_COUNT	     ").AppendLine();
			sSql.Append("	FROM ").AppendLine();
			sSql.Append("		T_ACCESS_AD ").AppendLine();
			sSql.Append("	WHERE ").AppendLine();

			if (!iBridUtil.GetStringValue(pSiteCd).Equals("")) {
				sSql.Append("		SITE_CD= :SITE_CD	AND ").AppendLine();
			}

			if (!iBridUtil.GetStringValue(pAdCd).Equals("")) {
				if (IsAdManager(pAdminType)) {
					sSql.Append("		AD_CD = :AD_CD AND ").AppendLine();
				} else {
					sSql.Append("		AD_CD LIKE :AD_CD||'%' AND ").AppendLine();
				}
			}

			if (!iBridUtil.GetStringValue(pSexCd).Equals("")) {
				sSql.Append("		SEX_CD = :SEX_CD AND ").AppendLine();
			}
			sSql.Append("		REPORT_DAY  >= :REPORT_DAY_FROM  AND REPORT_DAY  <= :REPORT_DAY_TO ").AppendLine();
			sSql.Append("	GROUP BY SITE_CD, AD_CD) T_SUM,(").AppendLine();

			sSql.Append("	SELECT ").AppendLine();
			sSql.Append("		AD_CD			,").AppendLine();
			sSql.Append("		SITE_CD			,").AppendLine();
			sSql.Append("		SUM(ORDINARY_SALES_AMT		) AS ORDINARY_SALES_AMT		,").AppendLine();
			sSql.Append("		SUM(POINT_BACK_SALES_AMT	) AS POINT_BACK_SALES_AMT	,").AppendLine();
			sSql.Append("		SUM(TOTAL_SALES_AMT			) AS TOTAL_SALES_AMT		,").AppendLine();
			sSql.Append("		SUM(AFTER_PACK_SALES_AMT	) AS AFTER_PACK_SALES_AMT	").AppendLine();
			sSql.Append("	FROM ").AppendLine();
			sSql.Append("		VW_DAILY_AD_SALES01 ").AppendLine();
			sSql.Append("	WHERE ").AppendLine();
			if (!iBridUtil.GetStringValue(pSiteCd).Equals("")) {
				sSql.Append("		SITE_CD= :SITE_CD2	AND ").AppendLine();
			}
			if (!iBridUtil.GetStringValue(pAdCd).Equals("")) {
				if (IsAdManager(pAdminType)) {
					sSql.Append("		AD_CD = :AD_CD2 AND ").AppendLine();
				} else {
					sSql.Append("		AD_CD LIKE :AD_CD2 ||'%' AND ").AppendLine();
				}
			}
			
			sSql.Append("		REPORT_DAY  >= :REPORT_DAY_FROM2  AND REPORT_DAY  <= :REPORT_DAY_TO2 ").AppendLine();
			sSql.Append("	GROUP BY SITE_CD, AD_CD) T_SUM2").AppendLine();

			sSql.Append("WHERE ").AppendLine();
			sSql.Append("	T_SUM.AD_CD				= T_AD.AD_CD						AND").AppendLine();			
			sSql.Append("	T_SUM.SITE_CD   		= T_SITE.SITE_CD					AND").AppendLine();
			sSql.Append("	T_SUM.SITE_CD			= T_SUM2.SITE_CD				(+)	AND").AppendLine();
			sSql.Append("	T_SUM.AD_CD				= T_SUM2.AD_CD					(+)	AND").AppendLine();
			sSql.Append("	T_SUM.SITE_CD			= T_SITE_AD_GROUP.SITE_CD		(+)	AND").AppendLine();
			sSql.Append("	T_SUM.AD_CD				= T_SITE_AD_GROUP.AD_CD			(+)	");

			if (pSummaryType.Equals("1")) {
				if (!iBridUtil.GetStringValue(pUsedFlag).Equals("")) {
					sSql.Append("AND	T_AD.USED_FLAG		= :USED_FLAG					").AppendLine();
				}
			} 
			
			if (pSummaryType.Equals("1")) {
				sSql.AppendLine();
			} else {
				sSql.AppendLine("AND");
				sSql.AppendLine("	T_AD_GROUP.AD_GROUP_CD	= T_SITE_AD_GROUP.AD_GROUP_CD		").AppendLine();
			}

			if (!iBridUtil.GetStringValue(pAdGroupCd).Equals("")) {
				sSql.Append("AND T_SITE_AD_GROUP.AD_GROUP_CD= :AD_GROUP_CD  ").AppendLine();
			}

			if (pSummaryType.Equals("1")) {
				sSql.Append("ORDER BY T_SUM.SITE_CD,T_SUM.AD_CD ").AppendLine();
			} else {
				sSql.Append("GROUP BY T_SUM.SITE_CD,T_SITE_AD_GROUP.AD_GROUP_CD,T_AD_GROUP.AD_GROUP_NM ").AppendLine();
				sSql.Append("ORDER BY T_SUM.SITE_CD,T_SITE_AD_GROUP.AD_GROUP_CD ").AppendLine();
			}

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				if (!iBridUtil.GetStringValue(pSiteCd).Equals("")) {
					cmd.Parameters.Add("SITE_CD",pSiteCd);
				}
				if (!iBridUtil.GetStringValue(pAdCd).Equals("")) {
					cmd.Parameters.Add("AD_CD",pAdCd);
				}
				if (!iBridUtil.GetStringValue(pSexCd).Equals("")) {
					cmd.Parameters.Add("SEX_CD",pSexCd);
				}
				cmd.Parameters.Add("REPORT_DAY_FROM",sFromDay);
				cmd.Parameters.Add("REPORT_DAY_TO",sToDay);

				if (!iBridUtil.GetStringValue(pSiteCd).Equals("")) {
					cmd.Parameters.Add("SITE_CD2",pSiteCd);
				}
				if (!iBridUtil.GetStringValue(pAdCd).Equals("")) {
					cmd.Parameters.Add("AD_CD2",pAdCd);
				}
				cmd.Parameters.Add("REPORT_DAY_FROM2",sFromDay);
				cmd.Parameters.Add("REPORT_DAY_TO2",sToDay);

				if (pSummaryType.Equals("1")) {
					if (!iBridUtil.GetStringValue(pUsedFlag).Equals("")) {
						cmd.Parameters.Add("USED_FLAG",pUsedFlag);
					}
				}

				if (!iBridUtil.GetStringValue(pAdGroupCd).Equals("")) {
					cmd.Parameters.Add("AD_GROUP_CD",pAdGroupCd);
				}

				using (da = new OracleDataAdapter(cmd)) {
					this.Fill(da,ds,"T_ACCESS_AD");
				}

				if (pSummaryType.Equals("1")) {
					ds.Tables[0].Columns.Add("AD_GROUP_CD");
					ds.Tables[0].Columns.Add("AD_GROUP_NM");
				} else {
					ds.Tables[0].Columns.Add("AD_CD");
					ds.Tables[0].Columns.Add("AD_NM");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetAdGroupDetailPerformanceList(
		string pSiteCd,
		string pFromYYYYRegist,
		string pFromMMRegist,
		string pFromDDRegist,
		string pToYYYYRegist,
		string pToMMRegist,
		string pToDDRegist,
		string pFromYYYYPurchase,
		string pFromMMPurchase,
		string pFromDDPurchase,
		string pToYYYYPurchase,
		string pToMMPurchase,
		string pToDDPurchase,
		string pAdGroupCd,
		string pAdCd,
		string pSexCd,
		bool pIsGroupAmt,
		bool pIsAdManager
	) {
		DataSet ds;

		try {
			conn = DbConnect();
			ds = new DataSet();

			string sFromDayRegist = pFromYYYYRegist + '/' + pFromMMRegist + '/' + pFromDDRegist;
			string sToDayRegist = pToYYYYRegist + '/' + pToMMRegist + '/' + pToDDRegist;
			string sFromDayPurchase = pFromYYYYPurchase + '/' + pFromMMPurchase + '/' + pFromDDPurchase;
			string sToDayPurchase = pToYYYYPurchase + '/' + pToMMPurchase + '/' + pToDDPurchase;

			StringBuilder sSql = new StringBuilder();


			sSql.Append("SELECT	").AppendLine();
			if (!pIsGroupAmt) {
				sSql.Append("	R.AD_CD	,").AppendLine();
				sSql.Append("	R.AD_NM	,").AppendLine();
			}
			sSql.Append("	R.AD_GROUP_CD	,").AppendLine();
			sSql.Append("	R.AD_GROUP_NM	,").AppendLine();
			sSql.Append("	NVL(R.REGIST_COUNT,0)					AS REGIST_COUNT						,").AppendLine();
			sSql.Append("	NVL(R.ACCESS_COUNT,0)					AS ACCESS_COUNT						,").AppendLine();
			sSql.Append("	NVL(D.LOGIN_COUNT,0)					AS LOGIN_COUNT						,").AppendLine();
			sSql.Append("	NVL(D.LOGIN_SUCCESS_COUNT,0)			AS LOGIN_SUCCESS_COUNT				,").AppendLine();
			sSql.Append("	NVL(D.LOGIN_SUCCESS_COUNT_UNIQUE,0)		AS LOGIN_SUCCESS_COUNT_UNIQUE		,").AppendLine();
			sSql.Append("	NVL(D.TOTAL_USED_POINT,0)				AS TOTAL_USED_POINT					,").AppendLine();
			sSql.Append("	NVL(D.ORDINARY_SALES_AMT,0)				AS ORDINARY_SALES_AMT				,").AppendLine();
			sSql.Append("	NVL(D.POINT_BACK_SALES_AMT,0)			AS POINT_BACK_SALES_AMT				,").AppendLine();
			sSql.Append("	NVL(D.TOTAL_SALES_AMT,0)				AS TOTAL_SALES_AMT					,").AppendLine();
			sSql.Append("	NVL(R.REGIST_COUNT_AD,0)				AS REGIST_COUNT_AD					,").AppendLine();
			sSql.Append("	NVL(R.ACCESS_COUNT_AD,0)				AS ACCESS_COUNT_AD					,").AppendLine();
			sSql.Append("	NVL(D.LOGIN_COUNT_AD,0)					AS LOGIN_COUNT_AD					,").AppendLine();
			sSql.Append("	NVL(D.LOGIN_SUCCESS_COUNT_AD,0)			AS LOGIN_SUCCESS_COUNT_AD			,").AppendLine();
			sSql.Append("	NVL(D.LOGIN_SUCCESS_COUNT_UNIQUE_AD,0)	AS LOGIN_SUCCESS_COUNT_UNIQUE_AD	,").AppendLine();
			sSql.Append("	NVL(D.TOTAL_USED_POINT_AD,0)			AS TOTAL_USED_POINT_AD				").AppendLine();
			sSql.Append("FROM (SELECT").AppendLine();
			sSql.Append("		SITE_CD,").AppendLine();
			if (!pIsGroupAmt) {
				sSql.Append("		AD_CD	,").AppendLine();
				sSql.Append("		AD_NM	,").AppendLine();
			}
			sSql.Append(string.Format("		NVL(AD_GROUP_CD,'{0}') AS AD_GROUP_CD,",ViCommConst.DEFAUL_AD_GROUP_CD)).AppendLine();
			sSql.Append("		NVL(AD_GROUP_NM,  NVL(AD_GROUP_CD,'未割当')) AS AD_GROUP_NM,").AppendLine();
			sSql.Append("		SUM(ACCESS_COUNT) AS ACCESS_COUNT		,").AppendLine();
			sSql.Append("		SUM(REGIST_COUNT) AS REGIST_COUNT		,").AppendLine();
			sSql.Append("		SUM(ACCESS_COUNT - MAQIA_AF_FAIL_ACCESS_COUNT	) AS ACCESS_COUNT_AD	,").AppendLine();
			sSql.Append("		SUM(REGIST_COUNT - MAQIA_AF_FAIL_REGIST_COUNT	) AS REGIST_COUNT_AD	").AppendLine();
			sSql.Append("	FROM VW_REGIST_AD01 ").AppendLine();
			sSql.Append("	WHERE ").AppendLine();
			sSql.Append("		SITE_CD = :SITE_CD AND ").AppendLine();

			if (!iBridUtil.GetStringValue(pAdGroupCd).Equals("")) {
				sSql.Append("		AD_GROUP_CD = :AD_GROUP_CD AND ").AppendLine();
			}
			if (!iBridUtil.GetStringValue(pAdCd).Equals("")) {
				if (pIsAdManager) {
					sSql.Append("		AD_CD = :AD_CD AND ").AppendLine();
				} else {
					sSql.Append("		AD_CD LIKE :AD_CD||'%' AND ").AppendLine();
				}
			}
			if (!iBridUtil.GetStringValue(pSexCd).Equals("")) {
				sSql.Append("		SEX_CD = :SEX_CD AND ").AppendLine();
			}

			sSql.Append("		REGIST_DAY >= :REGIST_DAY_FROM AND REGIST_DAY <= :REGIST_DAY_TO ").AppendLine();
			sSql.Append("	GROUP BY ").AppendLine();
			if (!pIsGroupAmt) {
				sSql.Append("		SITE_CD, AD_CD, AD_NM, AD_GROUP_CD, AD_GROUP_NM) R ,  ").AppendLine();
			} else {
				sSql.Append("		SITE_CD, AD_GROUP_CD, AD_GROUP_NM) R ,  ").AppendLine();
			}

			// 購入日・登録日指定


			sSql.Append("	(SELECT ").AppendLine();
			sSql.Append("		SITE_CD,").AppendLine();
			if (!pIsGroupAmt) {
				sSql.Append("		AD_CD	,").AppendLine();
				sSql.Append("		AD_NM	,").AppendLine();
			}
			sSql.Append(string.Format("		NVL(AD_GROUP_CD,'{0}') AS AD_GROUP_CD,",ViCommConst.DEFAUL_AD_GROUP_CD)).AppendLine();
			sSql.Append("		NVL(AD_GROUP_NM,  NVL(AD_GROUP_CD,'未割当')) AS AD_GROUP_NM,").AppendLine();
			sSql.Append("		SUM(LOGIN_COUNT)					AS LOGIN_COUNT					,").AppendLine();
			sSql.Append("		SUM(LOGIN_SUCCESS_COUNT)			AS LOGIN_SUCCESS_COUNT			,").AppendLine();
			sSql.Append("		SUM(LOGIN_SUCCESS_COUNT_UNIQUE)		AS LOGIN_SUCCESS_COUNT_UNIQUE	,").AppendLine();
			sSql.Append("		SUM(TOTAL_USED_POINT)				AS TOTAL_USED_POINT				,").AppendLine();
			sSql.Append("		SUM(ORDINARY_SALES_AMT)				AS ORDINARY_SALES_AMT			,").AppendLine();
			sSql.Append("		SUM(POINT_BACK_SALES_AMT)			AS POINT_BACK_SALES_AMT			,").AppendLine();
			sSql.Append("		SUM(TOTAL_SALES_AMT)				AS TOTAL_SALES_AMT				,").AppendLine();
			sSql.Append("		SUM(LOGIN_COUNT					- MAQIA_AF_FAIL_LOGIN_COUNT)		AS LOGIN_COUNT_AD					,").AppendLine();
			sSql.Append("		SUM(LOGIN_SUCCESS_COUNT			- FAIL_LOGIN_SUCCESS)				AS LOGIN_SUCCESS_COUNT_AD			,").AppendLine();
			sSql.Append("		SUM(LOGIN_SUCCESS_COUNT_UNIQUE	- FAIL_LOGIN_SUCCESS_UNIQUE)		AS LOGIN_SUCCESS_COUNT_UNIQUE_AD	,").AppendLine();
			sSql.Append("		SUM(TOTAL_USED_POINT			- MAQIA_AF_FAIL_TOTAL_USED_POINT)	AS TOTAL_USED_POINT_AD				").AppendLine();
			sSql.Append("	FROM VW_DAILY_AD_ACCESS_SALES01 ").AppendLine();
			sSql.Append("	WHERE ").AppendLine();
			sSql.Append("		SITE_CD = :SITE_CD AND ").AppendLine();
			if (!iBridUtil.GetStringValue(pAdGroupCd).Equals("")) {
				sSql.Append("		AD_GROUP_CD = :AD_GROUP_CD AND ").AppendLine();
			}
			if (!iBridUtil.GetStringValue(pAdCd).Equals("")) {
				if (pIsAdManager) {
					sSql.Append("		AD_CD = :AD_CD AND ").AppendLine();
				} else {
					sSql.Append("		AD_CD LIKE :AD_CD||'%' AND ").AppendLine();
				}
			}
			if (!iBridUtil.GetStringValue(pSexCd).Equals("")) {
				sSql.Append("		SEX_CD = :SEX_CD AND ").AppendLine();
			}
			sSql.Append("		REPORT_DAY >= :REPORT_FROM_DAY AND REPORT_DAY <= :REPORT_TO_DAY AND REGIST_DAY >= :REGIST_FROM_DAY AND REGIST_DAY <= :REGIST_TO_DAY").AppendLine();
			sSql.Append("	GROUP BY ").AppendLine();
			if (!pIsGroupAmt) {
				sSql.Append("		SITE_CD, AD_CD, AD_NM, AD_GROUP_CD, AD_GROUP_NM) D  ").AppendLine();
			} else {
				sSql.Append("		SITE_CD, AD_GROUP_CD, AD_GROUP_NM) D  ").AppendLine();
			}

			sSql.Append("WHERE ").AppendLine();
			if (!pIsGroupAmt) {
				sSql.Append("	R.SITE_CD		= D.SITE_CD		(+) AND").AppendLine();
				sSql.Append("	R.AD_CD			= D.AD_CD		(+) AND").AppendLine();
				sSql.Append("	R.AD_GROUP_CD	= D.AD_GROUP_CD (+) ").AppendLine();
				sSql.Append("ORDER BY ").AppendLine();
				sSql.Append("	R.SITE_CD, R.AD_GROUP_NM ASC NULLS LAST , R.AD_NM ASC NULLS LAST ");
			} else {
				sSql.Append("	R.SITE_CD		= D.SITE_CD			(+) AND").AppendLine();
				sSql.Append("	R.AD_GROUP_CD	= D.AD_GROUP_CD		(+) ").AppendLine();
				sSql.Append("ORDER BY ").AppendLine();
				sSql.Append("	R.SITE_CD, R.AD_GROUP_NM ASC NULLS LAST ").AppendLine();
			}
			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				if (!iBridUtil.GetStringValue(pAdGroupCd).Equals("")) {
					cmd.Parameters.Add("AD_GROUP_CD",pAdGroupCd);
				}
				if (!iBridUtil.GetStringValue(pAdCd).Equals("")) {
					cmd.Parameters.Add("AD_CD",pAdCd);
				}
				if (!iBridUtil.GetStringValue(pSexCd).Equals("")) {
					cmd.Parameters.Add("SEX_CD",pSexCd);
				}
				cmd.Parameters.Add("REGIST_DAY_FROM",sFromDayRegist);
				cmd.Parameters.Add("REGIST_DAY_TO",sToDayRegist);

				// 購入日・登録日指定


				cmd.Parameters.Add("SITE_CD",pSiteCd);
				if (!iBridUtil.GetStringValue(pAdGroupCd).Equals("")) {
					cmd.Parameters.Add("AD_GROUP_CD",pAdGroupCd);
				}
				if (!iBridUtil.GetStringValue(pAdCd).Equals("")) {
					cmd.Parameters.Add("AD_CD",pAdCd);
				}
				if (!iBridUtil.GetStringValue(pSexCd).Equals("")) {
					cmd.Parameters.Add("SEX_CD",pSexCd);
				}
				cmd.Parameters.Add("REPORT_DAY_FROM",sFromDayPurchase);
				cmd.Parameters.Add("REPORT_DAY_TO",sToDayPurchase);
				cmd.Parameters.Add("REGIST_DAY_FROM",sFromDayRegist);
				cmd.Parameters.Add("REGIST_DAY_TO",sToDayRegist);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}

			}
		} finally {
			conn.Close();
		}
		return ds;

	}


	public DataSet AccessAdInquiryDay(string pSiteCd,string pYYYY,string pMM,string pAdCd,string pSexCd) {
		try {
			string sFromDay = pYYYY + "/" + pMM + "/" + "01";
			DateTime dtTo = DateTime.Parse(sFromDay).AddMonths(1);
			dtTo = dtTo.AddDays(-1);
			string sToDay = dtTo.ToString("yyyy/MM/dd");
			int iEndDay = int.Parse(dtTo.ToString("dd"));

			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT ").AppendLine();
			sSql.Append("	P1.REPORT_DAY						,").AppendLine();
			sSql.Append("	P1.SITE_CD							,").AppendLine();
			sSql.Append("	P1.AD_CD							,").AppendLine();
			sSql.Append("	P1.LOGIN_COUNT						,").AppendLine();
			sSql.Append("	P1.ACCESS_COUNT						,").AppendLine();
			sSql.Append("	P1.REGIST_COUNT						,").AppendLine();
			sSql.Append("	P1.WITHDRAWAL_COUNT					,").AppendLine();
			sSql.Append("	P1.RE_REGIST_COUNT					,").AppendLine();
			sSql.Append("	P1.LOGIN_SUCCESS_COUNT				,").AppendLine();
			sSql.Append("	P1.LOGIN_SUCCESS_COUNT_UNIQUE		,").AppendLine();
			sSql.Append("	P1.TOTAL_USED_POINT					,").AppendLine();
			sSql.Append("	P1.LOGIN_COUNT_AD					,").AppendLine();
			sSql.Append("	P1.ACCESS_COUNT_AD					,").AppendLine();
			sSql.Append("	P1.REGIST_COUNT_AD					,").AppendLine();
			sSql.Append("	P1.TOTAL_USED_POINT_AD				,").AppendLine();
			sSql.Append("	P1.LOGIN_SUCCESS_COUNT_AD			,").AppendLine();
			sSql.Append("	P1.LOGIN_SUCCESS_COUNT_UNIQUE_AD	,").AppendLine();
			sSql.Append("	P1.REGIST_MAIL_SEND_FAILED_COUNT	,").AppendLine();
			sSql.Append("	NVL(P2.ORDINARY_SALES_AMT	,0) AS ORDINARY_SALES_AMT	,").AppendLine();
			sSql.Append("	NVL(P2.POINT_BACK_SALES_AMT	,0) AS POINT_BACK_SALES_AMT	,").AppendLine();
			sSql.Append("	NVL(P2.TOTAL_SALES_AMT		,0) AS TOTAL_SALES_AMT		,").AppendLine();
			sSql.Append("	NVL(P2.AFTER_PACK_SALES_AMT	,0) AS AFTER_PACK_SALES_AMT	 ").AppendLine();
			sSql.Append("FROM									 ").AppendLine();
			sSql.Append("	T_SITE,(							 ").AppendLine();
			sSql.Append("	SELECT								 ").AppendLine();
			sSql.Append("		AD_CD							,").AppendLine();
			sSql.Append("		REPORT_DAY						,").AppendLine();
			sSql.Append("		SITE_CD							,").AppendLine();
			sSql.Append("		LOGIN_COUNT						,").AppendLine();
			sSql.Append("		ACCESS_COUNT					,").AppendLine();
			sSql.Append("		REGIST_COUNT					,").AppendLine();
			sSql.Append("		WITHDRAWAL_COUNT				,").AppendLine();
			sSql.Append("		RE_REGIST_COUNT					,").AppendLine();
			sSql.Append("		TOTAL_USED_POINT				,").AppendLine();
			sSql.Append("		LOGIN_SUCCESS_COUNT				,").AppendLine();
			sSql.Append("		LOGIN_SUCCESS_COUNT_UNIQUE		,").AppendLine();
			sSql.Append("		RES_LOGIN_COUNT					- MAQIA_AF_FAIL_LOGIN_COUNT			 AS LOGIN_COUNT_AD					,").AppendLine();
			sSql.Append("		RES_ACCESS_COUNT				- MAQIA_AF_FAIL_ACCESS_COUNT		 AS ACCESS_COUNT_AD					,").AppendLine();
			sSql.Append("		RES_REGIST_COUNT				- MAQIA_AF_FAIL_REGIST_COUNT		 AS REGIST_COUNT_AD					,").AppendLine();
			sSql.Append("		RES_TOTAL_USED_POINT			- MAQIA_AF_FAIL_TOTAL_USED_POINT	 AS TOTAL_USED_POINT_AD				,").AppendLine();
			sSql.Append("		RES_LOGIN_SUCCESS_COUNT			- FAIL_LOGIN_SUCCESS				 AS LOGIN_SUCCESS_COUNT_AD			,").AppendLine();
			sSql.Append("		RES_LOGIN_SUCCESS_COUNT_UNIQUE	- FAIL_LOGIN_SUCCESS_UNIQUE			 AS LOGIN_SUCCESS_COUNT_UNIQUE_AD	,").AppendLine();
			sSql.Append("		REGIST_MAIL_SEND_FAILED_COUNT	").AppendLine();
			sSql.Append("	FROM								").AppendLine();
			sSql.Append("		T_ACCESS_AD						").AppendLine();
			sSql.Append("	WHERE								").AppendLine();
			sSql.Append("		SITE_CD		= :SITE_CD			AND").AppendLine();
			sSql.Append("		REPORT_DAY  >= :REPORT_DAY_FROM	AND").AppendLine();
			sSql.Append("		REPORT_DAY  <= :REPORT_DAY_TO	AND").AppendLine();
			sSql.Append("		AD_CD		= :AD_CD			AND").AppendLine();
			sSql.Append("		SEX_CD		= :SEX_CD			").AppendLine();
			sSql.Append("	ORDER BY SITE_CD,SEX_CD,REPORT_DAY) P1,(").AppendLine();
			sSql.Append("	SELECT ").AppendLine();
			sSql.Append("		AD_CD							,").AppendLine();
			sSql.Append("		REPORT_DAY						,").AppendLine();
			sSql.Append("		SITE_CD							,").AppendLine();
			sSql.Append("		ORDINARY_SALES_AMT				,").AppendLine();
			sSql.Append("		POINT_BACK_SALES_AMT			,").AppendLine();
			sSql.Append("		TOTAL_SALES_AMT					,").AppendLine();
			sSql.Append("		AFTER_PACK_SALES_AMT			 ").AppendLine();
			sSql.Append("	FROM								 ").AppendLine();
			sSql.Append("		VW_DAILY_AD_SALES01				 ").AppendLine();
			sSql.Append("	WHERE								 ").AppendLine();
			sSql.Append("		SITE_CD		= :SITE_CD2				AND").AppendLine();
			sSql.Append("		REPORT_DAY  >= :REPORT_DAY_FROM2	AND").AppendLine();
			sSql.Append("		REPORT_DAY  <= :REPORT_DAY_TO2		AND").AppendLine();
			sSql.Append("		AD_CD		= :AD_CD				   ").AppendLine();
			sSql.Append("	ORDER BY SITE_CD, REPORT_DAY) P2").AppendLine();
			sSql.Append("WHERE ").AppendLine();
			sSql.Append("	P1.SITE_CD   	= T_SITE.SITE_CD			AND").AppendLine();
			sSql.Append("	P1.SITE_CD		= P2.SITE_CD			(+)	AND").AppendLine();
			sSql.Append("	P1.REPORT_DAY	= P2.REPORT_DAY			(+)	AND").AppendLine();
			sSql.Append("	P1.AD_CD		= P2.AD_CD				(+)	").AppendLine();
			sSql.Append("ORDER BY P1.SITE_CD,P1.REPORT_DAY,P1.AD_CD ").AppendLine();

			DataSet oDataSet = new DataSet();

			conn = DbConnect();
			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(new OracleParameter("SITE_CD",pSiteCd));
				cmd.Parameters.Add(new OracleParameter("SEX_CD",pSexCd));
				cmd.Parameters.Add(new OracleParameter("REPORT_DAY_FROM",sFromDay));
				cmd.Parameters.Add(new OracleParameter("REPORT_DAY_TO",sToDay));
				cmd.Parameters.Add(new OracleParameter("SITE_CD2",pSiteCd));
				cmd.Parameters.Add(new OracleParameter("REPORT_DAY_FROM2",sFromDay));
				cmd.Parameters.Add(new OracleParameter("REPORT_DAY_TO2",sToDay));
				cmd.Parameters.Add(new OracleParameter("AD_CD",pAdCd));
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
				}
			}
			DataSet ds = new DataSet();
			DataTable dt = new DataTable();
			DataRow[] oCurRows;

			dt.Columns.Add(new DataColumn("SITE_CD",System.Type.GetType("System.String")));
			dt.Columns.Add(new DataColumn("ACCESS_DAY",System.Type.GetType("System.String")));
			dt.Columns.Add(new DataColumn("ACCESS_DAY_OF_WEEK",System.Type.GetType("System.String")));
			dt.Columns.Add(new DataColumn("LOGIN_COUNT",System.Type.GetType("System.String")));
			dt.Columns.Add(new DataColumn("ACCESS_COUNT",System.Type.GetType("System.String")));
			dt.Columns.Add(new DataColumn("REGIST_COUNT",System.Type.GetType("System.String")));
			dt.Columns.Add(new DataColumn("WITHDRAWAL_COUNT",System.Type.GetType("System.String")));
			dt.Columns.Add(new DataColumn("RE_REGIST_COUNT",System.Type.GetType("System.String")));
			dt.Columns.Add(new DataColumn("TOTAL_USED_POINT",System.Type.GetType("System.String")));
			dt.Columns.Add(new DataColumn("LOGIN_SUCCESS_COUNT",System.Type.GetType("System.String")));
			dt.Columns.Add(new DataColumn("LOGIN_SUCCESS_COUNT_UNIQUE",System.Type.GetType("System.String")));
			dt.Columns.Add(new DataColumn("LOGIN_COUNT_AD",System.Type.GetType("System.String")));
			dt.Columns.Add(new DataColumn("ACCESS_COUNT_AD",System.Type.GetType("System.String")));
			dt.Columns.Add(new DataColumn("REGIST_COUNT_AD",System.Type.GetType("System.String")));
			dt.Columns.Add(new DataColumn("TOTAL_USED_POINT_AD",System.Type.GetType("System.String")));
			dt.Columns.Add(new DataColumn("LOGIN_SUCCESS_COUNT_AD",System.Type.GetType("System.String")));
			dt.Columns.Add(new DataColumn("LOGIN_SUCCESS_COUNT_UNIQUE_AD",System.Type.GetType("System.String")));
			dt.Columns.Add(new DataColumn("REGIST_MAIL_SEND_FAILED_COUNT",System.Type.GetType("System.String")));
			dt.Columns.Add(new DataColumn("ORDINARY_SALES_AMT",System.Type.GetType("System.String")));
			dt.Columns.Add(new DataColumn("POINT_BACK_SALES_AMT",System.Type.GetType("System.String")));
			dt.Columns.Add(new DataColumn("TOTAL_SALES_AMT",System.Type.GetType("System.String")));
			dt.Columns.Add(new DataColumn("AFTER_PACK_SALES_AMT",System.Type.GetType("System.String")));

			DateTime dtStart = DateTime.Parse(sFromDay);

			for (int i = 1;i <= iEndDay;i++) {
				DataRow newRow = dt.NewRow();
				newRow["SITE_CD"] = pSiteCd;
				newRow["ACCESS_DAY"] = dtStart.AddDays(i - 1).ToString("yyyy/MM/dd");
				newRow["ACCESS_DAY_OF_WEEK"] = dtStart.AddDays(i - 1).DayOfWeek.ToString();

				oCurRows = oDataSet.Tables[0].Select(string.Format("REPORT_DAY = '{0}'",dtStart.AddDays(i - 1).ToString("yyyy/MM/dd")));
				if (oCurRows.Length >= 1) {
					newRow["LOGIN_COUNT"] = oCurRows[0]["LOGIN_COUNT"].ToString();
					newRow["ACCESS_COUNT"] = oCurRows[0]["ACCESS_COUNT"].ToString();
					newRow["REGIST_COUNT"] = oCurRows[0]["REGIST_COUNT"].ToString();
					newRow["WITHDRAWAL_COUNT"] = oCurRows[0]["WITHDRAWAL_COUNT"].ToString();
					newRow["RE_REGIST_COUNT"] = oCurRows[0]["RE_REGIST_COUNT"].ToString();
					newRow["TOTAL_USED_POINT"] = oCurRows[0]["TOTAL_USED_POINT"].ToString();
					newRow["LOGIN_SUCCESS_COUNT"] = oCurRows[0]["LOGIN_SUCCESS_COUNT"].ToString();
					newRow["LOGIN_SUCCESS_COUNT_UNIQUE"] = oCurRows[0]["LOGIN_SUCCESS_COUNT_UNIQUE"].ToString();
					newRow["LOGIN_COUNT_AD"] = oCurRows[0]["LOGIN_COUNT_AD"].ToString();
					newRow["ACCESS_COUNT_AD"] = oCurRows[0]["ACCESS_COUNT_AD"].ToString();
					newRow["REGIST_COUNT_AD"] = oCurRows[0]["REGIST_COUNT_AD"].ToString();
					newRow["TOTAL_USED_POINT_AD"] = oCurRows[0]["TOTAL_USED_POINT_AD"].ToString();
					newRow["LOGIN_SUCCESS_COUNT_AD"] = oCurRows[0]["LOGIN_SUCCESS_COUNT_AD"].ToString();
					newRow["LOGIN_SUCCESS_COUNT_UNIQUE_AD"] = oCurRows[0]["LOGIN_SUCCESS_COUNT_UNIQUE_AD"].ToString();
					newRow["REGIST_MAIL_SEND_FAILED_COUNT"] = oCurRows[0]["REGIST_MAIL_SEND_FAILED_COUNT"].ToString();
					newRow["ORDINARY_SALES_AMT"] = oCurRows[0]["ORDINARY_SALES_AMT"].ToString();
					newRow["POINT_BACK_SALES_AMT"] = oCurRows[0]["POINT_BACK_SALES_AMT"].ToString();
					newRow["TOTAL_SALES_AMT"] = oCurRows[0]["TOTAL_SALES_AMT"].ToString();
					newRow["AFTER_PACK_SALES_AMT"] = oCurRows[0]["AFTER_PACK_SALES_AMT"].ToString();
				} else {
					newRow["LOGIN_COUNT"] = "0";
					newRow["ACCESS_COUNT"] = "0";
					newRow["REGIST_COUNT"] = "0";
					newRow["WITHDRAWAL_COUNT"] = "0";
					newRow["RE_REGIST_COUNT"] = "0";
					newRow["TOTAL_USED_POINT"] = "0";
					newRow["LOGIN_SUCCESS_COUNT"] = "0";
					newRow["LOGIN_SUCCESS_COUNT_UNIQUE"] = "0";
					newRow["LOGIN_COUNT_AD"] = "0";
					newRow["ACCESS_COUNT_AD"] = "0";
					newRow["REGIST_COUNT_AD"] = "0";
					newRow["TOTAL_USED_POINT_AD"] = "0";
					newRow["LOGIN_SUCCESS_COUNT_AD"] = "0";
					newRow["LOGIN_SUCCESS_COUNT_UNIQUE_AD"] = "0";
					newRow["REGIST_MAIL_SEND_FAILED_COUNT"] = "0";
					newRow["ORDINARY_SALES_AMT"] = "0";
					newRow["POINT_BACK_SALES_AMT"] = "0";
					newRow["TOTAL_SALES_AMT"] = "0";
					newRow["AFTER_PACK_SALES_AMT"] = "0";
				}
				dt.Rows.Add(newRow);
			}
			ds.Tables.Add(dt);
			return ds;
		} finally {
			conn.Close();
		}
	}


	/* -- Previous
		public DataSet AccessAdInquiryDay(string pSiteCd,string pYYYY,string pMM,string pAdCd,string pSexCd) {

			DataColumn col;
			DataSet ds = new DataSet();
			DataTable dt = new DataTable();

			col = new DataColumn("SITE_CD",System.Type.GetType("System.String"));
			dt.Columns.Add(col);

			col = new DataColumn("ACCESS_DAY",System.Type.GetType("System.String"));
			dt.Columns.Add(col);

			col = new DataColumn("PRINT_DAY",System.Type.GetType("System.String"));
			dt.Columns.Add(col);

			col = new DataColumn("ACCESS_DAY_OF_WEEK",System.Type.GetType("System.String"));
			dt.Columns.Add(col);

			col = new DataColumn("LOGIN_COUNT",System.Type.GetType("System.String"));
			dt.Columns.Add(col);

			col = new DataColumn("ACCESS_COUNT",System.Type.GetType("System.String"));
			dt.Columns.Add(col);

			col = new DataColumn("REGIST_COUNT",System.Type.GetType("System.String"));
			dt.Columns.Add(col);

			col = new DataColumn("TOTAL_USED_POINT",System.Type.GetType("System.String"));
			dt.Columns.Add(col);

			dt.Columns.Add(new DataColumn("LOGIN_SUCCESS_COUNT",System.Type.GetType("System.String")));
			dt.Columns.Add(new DataColumn("LOGIN_SUCCESS_COUNT_UNIQUE",System.Type.GetType("System.String")));

			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("ACCESS_AD_INQUIRY_DAY");
				db.ProcedureInParm("PSITE_CD",DbType.VARCHAR2,pSiteCd);
				db.ProcedureInParm("PYYYY",DbType.VARCHAR2,pYYYY);
				db.ProcedureInParm("PMM",DbType.VARCHAR2,pMM);
				db.ProcedureInParm("PAD_CD",DbType.VARCHAR2,pAdCd);
				db.ProcedureInParm("PSEX_CD",DbType.VARCHAR2,pSexCd);

				db.ProcedureOutArrayParm("PACCESS_DAY",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("PACCESS_DAY_OF_WEEK",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("PACCESS_COUNT",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("PREGIST_COUNT",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("PLOGIN_COUNT",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("PLOGIN_SUCCESS_COUNT",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("PLOGIN_SUCCESS_COUNT_UNIQUE",DbSession.DbType.VARCHAR2,100);
				db.ProcedureOutArrayParm("PTOTAL_USED_POINT",DbSession.DbType.VARCHAR2,100);

				db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

				db.ExecuteProcedure();

				for (int i = 0;i < db.GetIntValue("PRECORD_COUNT");i++) {
					string sDay = db.GetArryStringValue("PACCESS_DAY",i);
					DataRow newRow = dt.NewRow();
					newRow["SITE_CD"] = pSiteCd;
					newRow["ACCESS_DAY"] = sDay;
					newRow["PRINT_DAY"] = sDay.Substring(8,2) + "日";
					newRow["ACCESS_DAY_OF_WEEK"] = db.GetArryStringValue("PACCESS_DAY_OF_WEEK",i);
					newRow["ACCESS_COUNT"] = db.GetArryStringValue("PACCESS_COUNT",i);
					newRow["REGIST_COUNT"] = db.GetArryStringValue("PREGIST_COUNT",i);
					newRow["LOGIN_COUNT"] = db.GetArryStringValue("PLOGIN_COUNT",i);
					newRow["TOTAL_USED_POINT"] = db.GetArryStringValue("PTOTAL_USED_POINT",i);
					newRow["LOGIN_SUCCESS_COUNT"] = db.GetArryStringValue("PLOGIN_SUCCESS_COUNT",i);
					newRow["LOGIN_SUCCESS_COUNT_UNIQUE"] = db.GetArryStringValue("PLOGIN_SUCCESS_COUNT_UNIQUE",i);
					dt.Rows.Add(newRow);
				}
			}
			ds.Tables.Add(dt);
			return ds;
		}
	*/
	protected bool IsAdManager(string pAdminType) {
		return pAdminType.ToString().Equals(ViCommConst.RIGHT_AD_MANAGE);
	}
}
