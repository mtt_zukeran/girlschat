﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: OmikujiLog
--	Title			: おみくじ履歴

--	Progaram ID		: OmikujiLog
--
--  Creation Date	: 2011.04.05
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater   Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Text;
using iBridCommLib;
using ViComm;
using System.Collections.Generic;

/// <summary>
/// OmikujiLog の概要の説明です
/// </summary>
public class DailyOmikuji : DbSession
{
    public DailyOmikuji() {
    }

	public DataSet GetPageCollection(string pSiteCd, string pSexCd, string pYYYY, string pMM)
	{
		DataSet ds = new DataSet();
		DataTable dt = new DataTable();
		DataColumn col;

		col = new DataColumn("SITE_CD", System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("SEX_CD", System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("REPORT_DAY", System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("REPORT_DAY_OF_WEEK", System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("PRINT_DAY", System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("OMIKUJI_TYPE", System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("NO_RCPT_OMIKUJI_TOTAL_COUNT", System.Type.GetType("System.String"));
		dt.Columns.Add(col); 
		col = new DataColumn("NO_RCPT_OMIKUJI_TOTAL_POINT", System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("RCPT_OMIKUJI_TOTAL_COUNT", System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("RCPT_OMIKUJI_TOTAL_POINT", System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("ALL_OMIKUJI_TOTAL_COUNT", System.Type.GetType("System.String"));
		dt.Columns.Add(col);
		col = new DataColumn("ALL_OMIKUJI_TOTAL_POINT", System.Type.GetType("System.String"));
		dt.Columns.Add(col);

		using (DbSession db = new DbSession())
		{
			db.PrepareProcedure("OMIKUJI_INQUIRY_DAY");
			db.ProcedureInParm("PSITE_CD", DbType.VARCHAR2, pSiteCd);
			db.ProcedureInParm("PYYYY", DbType.VARCHAR2, pYYYY);
			db.ProcedureInParm("PMM", DbType.VARCHAR2, pMM);
			db.ProcedureInParm("PSEX_CD", DbType.VARCHAR2, pSexCd);
			db.ProcedureOutArrayParm("PREPORT_DAY", DbSession.DbType.VARCHAR2, 200);
			db.ProcedureOutArrayParm("PREPORT_DAY_OF_WEEK", DbSession.DbType.VARCHAR2, 200);
			db.ProcedureOutArrayParm("POMIKUJI_TYPE", DbSession.DbType.VARCHAR2, 200);
			db.ProcedureOutArrayParm("PNO_RCPT_OMIKUJI_TOTAL_COUNT", DbSession.DbType.VARCHAR2, 200);
			db.ProcedureOutArrayParm("PNO_RCPT_OMIKUJI_TOTAL_POINT", DbSession.DbType.VARCHAR2, 200);
			db.ProcedureOutArrayParm("PRCPT_OMIKUJI_TOTAL_COUNT", DbSession.DbType.VARCHAR2, 200);
			db.ProcedureOutArrayParm("PRCPT_OMIKUJI_TOTAL_POINT", DbSession.DbType.VARCHAR2, 200);
			db.ProcedureOutArrayParm("PALL_OMIKUJI_TOTAL_COUNT", DbSession.DbType.VARCHAR2, 200);
			db.ProcedureOutArrayParm("PALL_OMIKUJI_TOTAL_POINT", DbSession.DbType.VARCHAR2, 200);
			db.ProcedureOutParm("PRECORD_COUNT", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);
			
			db.ExecuteProcedure();

			for (int i = 0; i < db.GetIntValue("PRECORD_COUNT"); i++) {
				string sDay = db.GetArryStringValue("PREPORT_DAY", i);
				DataRow newRow = dt.NewRow();
				newRow["SITE_CD"] = pSiteCd;
				newRow["REPORT_DAY"] = sDay;
				newRow["PRINT_DAY"] = string.Format("{0}日", sDay.Substring(8, 2));
				newRow["REPORT_DAY_OF_WEEK"] = db.GetArryStringValue("PREPORT_DAY_OF_WEEK", i);
				newRow["SEX_CD"] = pSexCd;

				newRow["OMIKUJI_TYPE"] = db.GetArryStringValue("POMIKUJI_TYPE", i);

				newRow["NO_RCPT_OMIKUJI_TOTAL_COUNT"] = db.GetArryStringValue("PNO_RCPT_OMIKUJI_TOTAL_COUNT", i);
				newRow["NO_RCPT_OMIKUJI_TOTAL_POINT"] = db.GetArryStringValue("PNO_RCPT_OMIKUJI_TOTAL_POINT", i);
				newRow["RCPT_OMIKUJI_TOTAL_COUNT"] = db.GetArryStringValue("PRCPT_OMIKUJI_TOTAL_COUNT", i);
				newRow["RCPT_OMIKUJI_TOTAL_POINT"] = db.GetArryStringValue("PRCPT_OMIKUJI_TOTAL_POINT", i);
				newRow["ALL_OMIKUJI_TOTAL_COUNT"] = db.GetArryStringValue("PALL_OMIKUJI_TOTAL_COUNT", i);
				newRow["ALL_OMIKUJI_TOTAL_POINT"] = db.GetArryStringValue("PALL_OMIKUJI_TOTAL_POINT", i);

				dt.Rows.Add(newRow);
			}
		}

		ds.Tables.Add(dt);

        return ds;
    }

}
