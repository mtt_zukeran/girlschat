﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ライブカメラ
--	Progaram ID		: LiveCamera
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class LiveCamera:DbSession {

	public LiveCamera() {
	}

	public int GetPageCount() {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_LIVE_CAMERA01 ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}


	public DataSet GetPageCollection(int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY CAMERA_ID";
			string sSql = "SELECT " +
							"CAMERA_ID			," +
							"USE_TERMINAL_TYPE	," +
							"TERMINAL_ID		," +
							"CAMERA_NM			," +
							"SHARED_MEDIA_NM	," +
							"CONNECTED_CAMERA_FLAG," +
							"CAMERA_START_DATE	," +
							"CAMERA_END_DATE	," +
							"UPDATE_DATE		," +
							"USE_TERMINAL_TYPE_NM," +
							"CONNECTED_CAMERA_MARK " +
							"FROM(" +
							" SELECT VW_LIVE_CAMERA01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_LIVE_CAMERA01  ";

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_LIVE_CAMERA01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	public DataSet GetList() {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT CAMERA_ID,CAMERA_NM FROM T_LIVE_CAMERA " +
								" ORDER BY CAMERA_ID  ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_LIVE_CAMERA");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

}
