﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 時間別稼動集計


--	Progaram ID		: TimeOperation
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater		Update Explain
  2010/10/08  K.Itoh		パフォーマンス見直し


-------------------------------------------------------------------------*/
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

public class TimeOperation:DbSession {

	public TimeOperation() {
	}

	public DataSet GetList(
		string pSiteCd,
		string pFromYYYY,
		string pFromMM,
		string pFromDD,
		string pFromHH,
		string pToYYYY,
		string pToMM,
		string pToDD,
		string pToHH,
		string pFromRegistDay,
		string pToRegistDay,
		string pLoginId,
		string pProductionSeq,
		string pManagerSeq,
		string pAdGroupCd,
		string pAdCd,
		bool pOutputHandleNmFlag
	) {
		string sBaseQuery = null;
		OracleParameter[] oBaseQueryParameters = this.GenerateGetPageCollectionQuery(
															out sBaseQuery,
															pSiteCd,
															pFromYYYY,
															pFromMM,
															pFromDD,
															pFromHH,
															pToYYYY,
															pToMM,
															pToDD,
															pToHH,
															pFromRegistDay,
															pToRegistDay,
															pLoginId,
															pProductionSeq,
															pManagerSeq,
															pAdGroupCd,
															pAdCd,
															pOutputHandleNmFlag);

		string sSortExpression = null;
		if (!string.IsNullOrEmpty(pSiteCd)) {
			if (pOutputHandleNmFlag) {
				sSortExpression = " ORDER BY T_SUM.SITE_CD,LOGIN_ID,T_SUM.USER_CHAR_NO ";
			} else {
				sSortExpression = " ORDER BY T_SUM.SITE_CD,LOGIN_ID ";
			}
		} else {
			sSortExpression = " ORDER BY LOGIN_ID ";
		}

		DataSet ds = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sBaseQuery + sSortExpression,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oBaseQueryParameters);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}

		return ds;
	}

	private OracleParameter[] GenerateCastPerformanceQuery(
		out string pQuery,
		string pSiteCd,
		string pFromYYYY,
		string pFromMM,
		string pFromDD,
		string pFromHH,
		string pToYYYY,
		string pToMM,
		string pToDD,
		string pToHH,
		string pFromRegistDay,
		string pToRegistDay,
		string pLoginId,
		string pProductionSeq,
		string pManagerSeq,
		string pAdGroupCd,
		string pAdCd,
		bool pOutputHandleNmFlag,
		bool pIsSummary) {
		// サイトコードが指定されていない場合はハンドル名の取得は行わない 
		if (string.IsNullOrEmpty(pSiteCd)) {
			pOutputHandleNmFlag = false;
		}

		string sFromDay = pFromYYYY + pFromMM + pFromDD + pFromHH;
		string sToDay = pToYYYY + pToMM + pToDD + pToHH;

		// ==========================
		//  サブクエリ生成
		// ==========================
		List<OracleParameter> oInnerParameterList = new List<OracleParameter>();
		StringBuilder oInnerSqlBuilder = new StringBuilder();

		/*** INNER SELECT ****/
		oInnerSqlBuilder.Append("SELECT	").AppendLine();
		if (!pIsSummary) {
			if (!pIsSummary) {
				if (!string.IsNullOrEmpty(pSiteCd)) {
					oInnerSqlBuilder.Append("	SITE_CD			, ").AppendLine();
					if (pOutputHandleNmFlag) {
						oInnerSqlBuilder.Append("	USER_CHAR_NO	, ").AppendLine();
					}
				}
			}
			oInnerSqlBuilder.Append("	USER_SEQ			,").AppendLine();
		}
		oInnerSqlBuilder.Append("	SUM(PRV_TV_TALK_POINT								) AS PRV_TV_TALK_POINT				,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PRV_TV_TALK_MIN	- INV_PRV_TV_TALK_MIN			) AS PRV_TV_TALK_MIN				,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PRV_TV_TALK_PAY_POINT							) AS PRV_TV_TALK_PAY_POINT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PUB_TV_TALK_POINT								) AS PUB_TV_TALK_POINT				,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PUB_TV_TALK_MIN	- INV_PUB_TV_TALK_MIN			) AS PUB_TV_TALK_MIN				,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PUB_TV_TALK_PAY_POINT							) AS PUB_TV_TALK_PAY_POINT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PRV_VOICE_TALK_POINT							) AS PRV_VOICE_TALK_POINT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PRV_VOICE_TALK_MIN	- INV_PRV_VOICE_TALK_MIN	) AS PRV_VOICE_TALK_MIN				,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PRV_VOICE_TALK_PAY_POINT						) AS PRV_VOICE_TALK_PAY_POINT		,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PUB_VOICE_TALK_POINT							) AS PUB_VOICE_TALK_POINT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PUB_VOICE_TALK_MIN	- INV_PUB_VOICE_TALK_MIN	) AS PUB_VOICE_TALK_MIN				,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PUB_VOICE_TALK_PAY_POINT						) AS PUB_VOICE_TALK_PAY_POINT		,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(VIEW_TALK_POINT									) AS VIEW_TALK_POINT				,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(VIEW_TALK_MIN		- INV_VIEW_TALK_MIN			) AS VIEW_TALK_MIN					,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(VIEW_TALK_PAY_POINT								) AS VIEW_TALK_PAY_POINT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(VIEW_BROADCAST_POINT							) AS VIEW_BROADCAST_POINT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(VIEW_BROADCAST_MIN	- INV_VIEW_BROADCAST_MIN	) AS VIEW_BROADCAST_MIN				,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(VIEW_BROADCAST_PAY_POINT						) AS VIEW_BROADCAST_PAY_POINT		,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(LIVE_POINT										) AS LIVE_POINT						,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(LIVE_MIN			- INV_LIVE_MIN				) AS LIVE_MIN						,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(LIVE_PAY_POINT									) AS LIVE_PAY_POINT					,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(MOVIE_POINT										) AS MOVIE_POINT					,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(MOVIE_MIN			- INV_MOVIE_MIN				) AS MOVIE_MIN						,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(MOVIE_PAY_POINT									) AS MOVIE_PAY_POINT				,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PLAY_PROFILE_POINT								) AS PLAY_PROFILE_POINT				,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PLAY_PROFILE_MIN	- INV_PLAY_PROFILE_MIN		) AS PLAY_PROFILE_MIN				,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PLAY_PROFILE_PAY_POINT							) AS PLAY_PROFILE_PAY_POINT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(REC_PROFILE_POINT								) AS REC_PROFILE_POINT				,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(REC_PROFILE_MIN	- INV_REC_PROFILE_MIN			) AS REC_PROFILE_MIN				,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(REC_PROFILE_PAY_POINT							) AS REC_PROFILE_PAY_POINT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(WIRETAP_POINT									) AS WIRETAP_POINT					,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(WIRETAP_MIN		- INV_WIRETAP_MIN				) AS WIRETAP_MIN					,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(WIRETAP_PAY_POINT								) AS WIRETAP_PAY_POINT				,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(GPF_TALK_VOICE_POINT							) AS GPF_TALK_VOICE_POINT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(GPF_TALK_VOICE_MIN	- INV_GPF_TALK_VOICE_MIN	) AS GPF_TALK_VOICE_MIN				,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(GPF_TALK_VOICE_PAY_POINT						) AS GPF_TALK_VOICE_PAY_POINT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PLAY_PV_MSG_COUNT								) AS PLAY_PV_MSG_COUNT				,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PLAY_PV_MSG_POINT								) AS PLAY_PV_MSG_POINT				,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PLAY_PV_MSG_MIN	- INV_PLAY_PV_MSG_MIN			) AS PLAY_PV_MSG_MIN				,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PLAY_PV_MSG_PAY_POINT							) AS PLAY_PV_MSG_PAY_POINT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(CAST_PRV_TV_TALK_POINT							) AS CAST_PRV_TV_TALK_POINT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(CAST_PRV_TV_TALK_MIN							) AS CAST_PRV_TV_TALK_MIN			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(CAST_PRV_TV_TALK_PAY_POINT						) AS CAST_PRV_TV_TALK_PAY_POINT		,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(CAST_PUB_TV_TALK_POINT							) AS CAST_PUB_TV_TALK_POINT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(CAST_PUB_TV_TALK_MIN							) AS CAST_PUB_TV_TALK_MIN			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(CAST_PUB_TV_TALK_PAY_POINT						) AS CAST_PUB_TV_TALK_PAY_POINT		,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(CAST_PRV_VOICE_TALK_POINT						) AS CAST_PRV_VOICE_TALK_POINT		,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(CAST_PRV_VOICE_TALK_MIN							) AS CAST_PRV_VOICE_TALK_MIN		,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(CAST_PRV_VOICE_TALK_PAY_POINT					) AS CAST_PRV_VOICE_TALK_PAY_POINT	,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(CAST_PUB_VOICE_TALK_POINT						) AS CAST_PUB_VOICE_TALK_POINT		,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(CAST_PUB_VOICE_TALK_MIN							) AS CAST_PUB_VOICE_TALK_MIN		,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(CAST_PUB_VOICE_TALK_PAY_POINT					) AS CAST_PUB_VOICE_TALK_PAY_POINT	,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PRV_TV_TALK_POINT	+ PUB_TV_TALK_POINT		+ PRV_VOICE_TALK_POINT	+ PUB_VOICE_TALK_POINT	+ VIEW_TALK_POINT	+ VIEW_BROADCAST_POINT	+ LIVE_POINT	+ MOVIE_POINT	+ PLAY_PROFILE_POINT	+ REC_PROFILE_POINT		+ WIRETAP_POINT		+ GPF_TALK_VOICE_POINT		+ PLAY_PV_MSG_POINT		+ CAST_PRV_TV_TALK_POINT	+ CAST_PUB_TV_TALK_POINT	+ CAST_PRV_VOICE_TALK_POINT		+ CAST_PUB_VOICE_TALK_POINT	) AS TALK_POINT, ").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PRV_TV_TALK_MIN		+ PUB_TV_TALK_MIN		+ PRV_VOICE_TALK_MIN	+ PUB_VOICE_TALK_MIN	+ VIEW_TALK_MIN		+ VIEW_BROADCAST_MIN	+ LIVE_MIN		+ MOVIE_MIN		+ PLAY_PROFILE_MIN		+ REC_PROFILE_MIN		+ WIRETAP_MIN		+ GPF_TALK_VOICE_MIN		+ PLAY_PV_MSG_MIN		+ CAST_PRV_TV_TALK_MIN		+ CAST_PUB_TV_TALK_MIN		+ CAST_PRV_VOICE_TALK_MIN		+ CAST_PUB_VOICE_TALK_MIN	- INV_PRV_TV_TALK_MIN - INV_PUB_TV_TALK_MIN - INV_PRV_VOICE_TALK_MIN - INV_PUB_VOICE_TALK_MIN - INV_VIEW_TALK_MIN - INV_VIEW_BROADCAST_MIN - INV_LIVE_MIN - INV_MOVIE_MIN - INV_PLAY_PROFILE_MIN - INV_REC_PROFILE_MIN - INV_WIRETAP_MIN - INV_GPF_TALK_VOICE_MIN - INV_PLAY_PV_MSG_MIN) AS TALK_MIN,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PRV_TV_TALK_PAY_POINT	+ PUB_TV_TALK_PAY_POINT	+ PRV_VOICE_TALK_PAY_POINT+ PUB_VOICE_TALK_PAY_POINT+ VIEW_TALK_PAY_POINT	+ VIEW_BROADCAST_PAY_POINT+ LIVE_PAY_POINT	+ MOVIE_PAY_POINT	+ PLAY_PROFILE_PAY_POINT	+ REC_PROFILE_PAY_POINT	+ WIRETAP_PAY_POINT	+ GPF_TALK_VOICE_PAY_POINT	+ PLAY_PV_MSG_PAY_POINT	+ CAST_PRV_TV_TALK_PAY_POINT	+ CAST_PUB_TV_TALK_PAY_POINT	+ CAST_PRV_VOICE_TALK_PAY_POINT	+ CAST_PUB_VOICE_TALK_PAY_POINT) AS TALK_PAY_POINT, ").AppendLine();
		oInnerSqlBuilder.Append("	SUM(USER_MAIL_POINT									) AS USER_MAIL_POINT				,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(USER_MAIL_COUNT									) AS USER_MAIL_COUNT				,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(USER_MAIL_PAY_POINT								) AS USER_MAIL_PAY_POINT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(OPEN_PIC_MAIL_POINT								) AS OPEN_PIC_MAIL_POINT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(OPEN_PIC_MAIL_COUNT								) AS OPEN_PIC_MAIL_COUNT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(OPEN_PIC_MAIL_PAY_POINT							) AS OPEN_PIC_MAIL_PAY_POINT		,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(OPEN_MOVIE_MAIL_POINT							) AS OPEN_MOVIE_MAIL_POINT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(OPEN_MOVIE_MAIL_COUNT							) AS OPEN_MOVIE_MAIL_COUNT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(OPEN_MOVIE_MAIL_PAY_POINT						) AS OPEN_MOVIE_MAIL_PAY_POINT		,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(OPEN_PIC_BBS_POINT								) AS OPEN_PIC_BBS_POINT				,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(OPEN_PIC_BBS_COUNT								) AS OPEN_PIC_BBS_COUNT				,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(OPEN_PIC_BBS_PAY_POINT							) AS OPEN_PIC_BBS_PAY_POINT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(OPEN_MOVIE_BBS_POINT							) AS OPEN_MOVIE_BBS_POINT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(OPEN_MOVIE_BBS_COUNT							) AS OPEN_MOVIE_BBS_COUNT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(OPEN_MOVIE_BBS_PAY_POINT						) AS OPEN_MOVIE_BBS_PAY_POINT		,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(USER_MAIL_PIC_POINT								) AS USER_MAIL_PIC_POINT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(USER_MAIL_PIC_COUNT								) AS USER_MAIL_PIC_COUNT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(USER_MAIL_PIC_PAY_POINT							) AS USER_MAIL_PIC_PAY_POINT		,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(USER_MAIL_MOVIE_POINT							) AS USER_MAIL_MOVIE_POINT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(USER_MAIL_MOVIE_COUNT							) AS USER_MAIL_MOVIE_COUNT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(USER_MAIL_MOVIE_PAY_POINT						) AS USER_MAIL_MOVIE_PAY_POINT		,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PRV_MESSAGE_RX_POINT							) AS PRV_MESSAGE_RX_POINT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PRV_MESSAGE_RX_COUNT							) AS PRV_MESSAGE_RX_COUNT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PRV_MESSAGE_RX_PAY_POINT						) AS PRV_MESSAGE_RX_PAY_POINT		,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PRV_PROFILE_RX_POINT							) AS PRV_PROFILE_RX_POINT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PRV_PROFILE_RX_COUNT							) AS PRV_PROFILE_RX_COUNT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PRV_PROFILE_RX_PAY_POINT						) AS PRV_PROFILE_RX_PAY_POINT		,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(DOWNLOAD_MOVIE_POINT							) AS DOWNLOAD_MOVIE_POINT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(DOWNLOAD_MOVIE_COUNT							) AS DOWNLOAD_MOVIE_COUNT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(DOWNLOAD_MOVIE_PAY_POINT						) AS DOWNLOAD_MOVIE_PAY_POINT		,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(DOWNLOAD_VOICE_POINT							) AS DOWNLOAD_VOICE_POINT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(DOWNLOAD_VOICE_COUNT							) AS DOWNLOAD_VOICE_COUNT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(DOWNLOAD_VOICE_PAY_POINT						) AS DOWNLOAD_VOICE_PAY_POINT		,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(SOCIAL_GAME_POINT								) AS SOCIAL_GAME_POINT				,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(SOCIAL_GAME_COUNT								) AS SOCIAL_GAME_COUNT				,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(SOCIAL_GAME_PAY_POINT							) AS SOCIAL_GAME_PAY_POINT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(YAKYUKEN_POINT									) AS YAKYUKEN_POINT					,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(YAKYUKEN_COUNT									) AS YAKYUKEN_COUNT					,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(YAKYUKEN_PAY_POINT								) AS YAKYUKEN_PAY_POINT				,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PRESENT_MAIL_POINT								) AS PRESENT_MAIL_POINT				,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PRESENT_MAIL_COUNT								) AS PRESENT_MAIL_COUNT				,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(PRESENT_MAIL_PAY_POINT							) AS PRESENT_MAIL_PAY_POINT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(TIME_CALL_POINT									) AS TIME_CALL_POINT				,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(TIME_CALL_COUNT									) AS TIME_CALL_COUNT				,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(TIME_CALL_PAY_POINT								) AS TIME_CALL_PAY_POINT			,").AppendLine();
		oInnerSqlBuilder.Append("	SUM(USER_MAIL_POINT		+ OPEN_PIC_MAIL_POINT	+ OPEN_MOVIE_MAIL_POINT		+ OPEN_PIC_BBS_POINT	+ OPEN_MOVIE_BBS_POINT	+	USER_MAIL_PIC_POINT		+ USER_MAIL_MOVIE_POINT		+ PRV_MESSAGE_RX_POINT		+ PRV_PROFILE_RX_POINT	+ DOWNLOAD_MOVIE_POINT	+ DOWNLOAD_VOICE_POINT	+ SOCIAL_GAME_POINT + YAKYUKEN_POINT + PRESENT_MAIL_POINT + TIME_CALL_POINT	) AS WEB_TOTAL_POINT	,	").AppendLine();
		oInnerSqlBuilder.Append("	SUM(USER_MAIL_COUNT		+ OPEN_PIC_MAIL_COUNT	+ OPEN_MOVIE_MAIL_COUNT		+ OPEN_PIC_BBS_COUNT	+ OPEN_MOVIE_BBS_COUNT	+	USER_MAIL_PIC_COUNT		+ USER_MAIL_MOVIE_COUNT		+ PRV_MESSAGE_RX_COUNT		+ PRV_PROFILE_RX_COUNT	+ DOWNLOAD_MOVIE_COUNT	+ DOWNLOAD_VOICE_COUNT	+ SOCIAL_GAME_COUNT + YAKYUKEN_COUNT + PRESENT_MAIL_COUNT + TIME_CALL_COUNT	) AS WEB_TOTAL_COUNT	,	").AppendLine();
		oInnerSqlBuilder.Append("	SUM(USER_MAIL_PAY_POINT	+ OPEN_PIC_MAIL_PAY_POINT + OPEN_MOVIE_MAIL_PAY_POINT	+ OPEN_PIC_BBS_PAY_POINT	+ OPEN_MOVIE_BBS_PAY_POINT+	USER_MAIL_PIC_PAY_POINT	+ USER_MAIL_MOVIE_PAY_POINT	+ PRV_MESSAGE_RX_PAY_POINT	+ PRV_PROFILE_RX_PAY_POINT+ DOWNLOAD_MOVIE_PAY_POINT+ DOWNLOAD_VOICE_PAY_POINT+ SOCIAL_GAME_PAY_POINT + YAKYUKEN_PAY_POINT + PRESENT_MAIL_PAY_POINT + TIME_CALL_PAY_POINT	) AS WEB_TOTAL_PAY_POINT		").AppendLine();
		oInnerSqlBuilder.Append("FROM	").AppendLine();
		oInnerSqlBuilder.Append("	VW_TIME_OPERATION02	").AppendLine();
		/*** INNER WHERE ****/
		string sInnerWhere = string.Empty;
		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("	SITE_CD		=		:SITE_CD	",ref sInnerWhere);
			oInnerParameterList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		}
		SysPrograms.SqlAppendWhere("	REPORT_DAY_TIME		>=	:REPORT_DAY_TIME_FROM	",ref sInnerWhere);
		oInnerParameterList.Add(new OracleParameter(":REPORT_DAY_TIME_FROM",sFromDay));
		SysPrograms.SqlAppendWhere("	REPORT_DAY_TIME		<=	:REPORT_DAY_TIME_TO		",ref sInnerWhere);
		oInnerParameterList.Add(new OracleParameter(":REPORT_DAY_TIME_TO",sToDay));

		if (!pFromRegistDay.Equals(string.Empty)) {
			DateTime dtFrom = DateTime.Parse(pFromRegistDay + " 00:00:00");
			DateTime dtTo = DateTime.Parse(pToRegistDay + " 23:59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere(" REGIST_DATE > =:REGIST_DATE_FROM ",ref sInnerWhere);
			SysPrograms.SqlAppendWhere(" REGIST_DATE <	:REGIST_DATE_TO ",ref sInnerWhere);
			oInnerParameterList.Add(new OracleParameter("REGIST_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			oInnerParameterList.Add(new OracleParameter("REGIST_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}
		
		if (!string.IsNullOrEmpty(pProductionSeq)) {
			SysPrograms.SqlAppendWhere("	PRODUCTION_SEQ		=		:PRODUCTION_SEQ	",ref sInnerWhere);
			oInnerParameterList.Add(new OracleParameter(":PRODUCTION_SEQ",pProductionSeq));
		}
		if (!string.IsNullOrEmpty(pManagerSeq)) {
			SysPrograms.SqlAppendWhere("	MANAGER_SEQ			=		:MANAGER_SEQ	",ref sInnerWhere);
			oInnerParameterList.Add(new OracleParameter(":MANAGER_SEQ",pManagerSeq));
		}
		if (!string.IsNullOrEmpty(pLoginId)) {
			SysPrograms.SqlAppendWhere("	 LOGIN_ID			LIKE	:LOGIN_ID||'%' 	",ref sInnerWhere);
			oInnerParameterList.Add(new OracleParameter(":LOGIN_ID",pLoginId));
		}
		if (!string.IsNullOrEmpty(pAdGroupCd)) {
			SysPrograms.SqlAppendWhere("	AD_GROUP_CD			=		:AD_GROUP_CD	",ref sInnerWhere);
			oInnerParameterList.Add(new OracleParameter(":AD_GROUP_CD",pAdGroupCd));
		}
		if (!string.IsNullOrEmpty(pAdCd)) {
			SysPrograms.SqlAppendWhere("	AD_CD				=		:AD_CD			",ref sInnerWhere);
			oInnerParameterList.Add(new OracleParameter(":AD_CD",pAdCd));
		}
		oInnerSqlBuilder.Append(sInnerWhere).AppendLine();

		/*** INNER GROUP BY ****/
		if (!pIsSummary) {
			oInnerSqlBuilder.Append("GROUP BY	").AppendLine();
			if (!string.IsNullOrEmpty(pSiteCd) && pOutputHandleNmFlag) {
				oInnerSqlBuilder.Append("	SITE_CD, USER_SEQ, USER_CHAR_NO	").AppendLine();
			} else if (!string.IsNullOrEmpty(pSiteCd) && !pOutputHandleNmFlag) {
				oInnerSqlBuilder.Append("	SITE_CD, USER_SEQ				").AppendLine();
			} else {
				oInnerSqlBuilder.Append("	USER_SEQ						").AppendLine();
			}
		}

		pQuery = oInnerSqlBuilder.ToString();
		return oInnerParameterList.ToArray();
	}
	private OracleParameter[] GenerateGetPageCollectionQuery(
		out string pQuery,
		string pSiteCd,
		string pFromYYYY,
		string pFromMM,
		string pFromDD,
		string pFromHH,
		string pToYYYY,
		string pToMM,
		string pToDD,
		string pToHH,
		string pFromRegistDay,
		string pToRegistDay,
		string pLoginId,
		string pProductionSeq,
		string pManagerSeq,
		string pAdGroupCd,
		string pAdCd,
		bool pOutputHandleNmFlag) {



		// ==========================
		//  クエリ生成
		// ==========================
		List<OracleParameter> oParameterList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		string sCastPerformanceQuery = null;
		OracleParameter[] oCastPerformanceQueryParams = this.GenerateCastPerformanceQuery(
															out sCastPerformanceQuery,
															pSiteCd,
															pFromYYYY,
															pFromMM,
															pFromDD,
															pFromHH,
															pToYYYY,
															pToMM,
															pToDD,
															pToHH,
															pFromRegistDay,
															pToRegistDay,
															pLoginId,
															pProductionSeq,
															pManagerSeq,
															pAdGroupCd,
															pAdCd,
															pOutputHandleNmFlag,
															false);


		oParameterList.AddRange(oCastPerformanceQueryParams);

		/*** SELECT ***/
		oSqlBuilder.Append("SELECT	").AppendLine();
		oSqlBuilder.Append("	T_USER.LOGIN_ID		, ").AppendLine();
		oSqlBuilder.Append("	T_CAST.CAST_NM		, ").AppendLine();
		oSqlBuilder.Append("	T_SUM.*				, ").AppendLine();
		oSqlBuilder.Append("	(T_SUM.WEB_TOTAL_PAY_POINT + T_SUM.TALK_PAY_POINT) TOTAL_PAY_POINT 	, ").AppendLine();
		if (!string.IsNullOrEmpty(pSiteCd) && pOutputHandleNmFlag) {
			oSqlBuilder.Append("	T_CAST_CHARACTER.HANDLE_NM	 ").AppendLine();
		} else {
			oSqlBuilder.Append("	NULL AS HANDLE_NM			 ").AppendLine();
		}

		/*** FROM ***/
		oSqlBuilder.Append("FROM	").AppendLine();
		if (!string.IsNullOrEmpty(pSiteCd) && pOutputHandleNmFlag) {
			oSqlBuilder.Append("	T_CAST_CHARACTER	,	").AppendLine();
		}
		oSqlBuilder.Append("	T_USER	,	").AppendLine();
		oSqlBuilder.Append("	T_CAST	,	").AppendLine();
		oSqlBuilder.Append("	(" + sCastPerformanceQuery + ") T_SUM").AppendLine();

		/*** WHERE ***/
		if (!string.IsNullOrEmpty(pSiteCd)) {
			if (pOutputHandleNmFlag) {
				oSqlBuilder.Append("WHERE	").AppendLine();
				oSqlBuilder.Append("	T_CAST.USER_SEQ		= T_SUM.USER_SEQ				AND	").AppendLine();
				oSqlBuilder.Append("	T_CAST.USER_SEQ		= T_USER.USER_SEQ				AND	").AppendLine();
				oSqlBuilder.Append("	T_SUM.SITE_CD		= T_CAST_CHARACTER.SITE_CD		AND	").AppendLine();
				oSqlBuilder.Append("	T_SUM.USER_SEQ		= T_CAST_CHARACTER.USER_SEQ		AND	").AppendLine();
				oSqlBuilder.Append("	T_SUM.USER_CHAR_NO	= T_CAST_CHARACTER.USER_CHAR_NO		").AppendLine();
			} else {
				oSqlBuilder.Append("WHERE	").AppendLine();
				oSqlBuilder.Append("	T_CAST.USER_SEQ		= T_SUM.USER_SEQ				AND	").AppendLine();
				oSqlBuilder.Append("	T_CAST.USER_SEQ		= T_USER.USER_SEQ					").AppendLine();
			}
		} else {
			oSqlBuilder.Append("WHERE	").AppendLine();
			oSqlBuilder.Append("	T_CAST.USER_SEQ		= T_SUM.USER_SEQ				AND	").AppendLine();
			oSqlBuilder.Append("	T_CAST.USER_SEQ		= T_USER.USER_SEQ					").AppendLine();
		}

		pQuery = oSqlBuilder.ToString();
		return oParameterList.ToArray();
	}

	public DataSet GetManagerSummaryList(
		string pSiteCd,
		string pFromYYYY,
		string pFromMM,
		string pFromDD,
		string pFromHH,
		string pToYYYY,
		string pToMM,
		string pToDD,
		string pToHH,
		string pProductionSeq,
		string pManagerSeq
	) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sFromDay = pFromYYYY + pFromMM + pFromDD + pFromHH;
			string sToDay = pToYYYY + pToMM + pToDD + pToHH;
			string sSql = "SELECT " +
							"T_MANAGER.MANAGER_NM,";

			if (!iBridUtil.GetStringValue(pSiteCd).Equals("")) {
				sSql += "T_SUM.SITE_CD,";
			}


			sSql += "T_SUM.PRV_TV_TALK_COUNT			," +
					"T_SUM.PRV_TV_TALK_MIN				," +
					"T_SUM.PRV_TV_TALK_POINT			," +
					"T_SUM.PRV_TV_TALK_PAY_POINT		," +
					"T_SUM.PUB_TV_TALK_COUNT			," +
					"T_SUM.PUB_TV_TALK_MIN				," +
					"T_SUM.PUB_TV_TALK_POINT			," +
					"T_SUM.PUB_TV_TALK_PAY_POINT		," +
					"T_SUM.PRV_VOICE_TALK_COUNT			," +
					"T_SUM.PRV_VOICE_TALK_MIN			," +
					"T_SUM.PRV_VOICE_TALK_POINT			," +
					"T_SUM.PRV_VOICE_TALK_PAY_POINT		," +
					"T_SUM.PUB_VOICE_TALK_COUNT			," +
					"T_SUM.PUB_VOICE_TALK_MIN			," +
					"T_SUM.PUB_VOICE_TALK_POINT			," +
					"T_SUM.PUB_VOICE_TALK_PAY_POINT		," +
					"T_SUM.VIEW_TALK_COUNT				," +
					"T_SUM.VIEW_TALK_MIN				," +
					"T_SUM.VIEW_TALK_POINT				," +
					"T_SUM.VIEW_TALK_PAY_POINT			," +
					"T_SUM.VIEW_BROADCAST_COUNT			," +
					"T_SUM.VIEW_BROADCAST_MIN			," +
					"T_SUM.VIEW_BROADCAST_POINT			," +
					"T_SUM.VIEW_BROADCAST_PAY_POINT		," +
					"T_SUM.LIVE_COUNT					," +
					"T_SUM.LIVE_MIN						," +
					"T_SUM.LIVE_POINT					," +
					"T_SUM.LIVE_PAY_POINT				," +
					"T_SUM.MOVIE_COUNT					," +
					"T_SUM.MOVIE_MIN					," +
					"T_SUM.MOVIE_POINT					," +
					"T_SUM.MOVIE_PAY_POINT				," +
					"T_SUM.PLAY_PROFILE_COUNT			," +
					"T_SUM.PLAY_PROFILE_MIN				," +
					"T_SUM.PLAY_PROFILE_POINT			," +
					"T_SUM.PLAY_PROFILE_PAY_POINT		," +
					"T_SUM.REC_PROFILE_COUNT			," +
					"T_SUM.REC_PROFILE_MIN				," +
					"T_SUM.REC_PROFILE_POINT			," +
					"T_SUM.REC_PROFILE_PAY_POINT		," +
					"T_SUM.TALK_COUNT					," +
					"T_SUM.TALK_MIN						," +
					"T_SUM.TALK_POINT					," +
					"T_SUM.TALK_PAY_POINT				," +
					"T_SUM.WIRETAP_COUNT				," +
					"T_SUM.WIRETAP_MIN					," +
					"T_SUM.WIRETAP_POINT				," +
					"T_SUM.WIRETAP_PAY_POINT			," +
					"T_SUM.GPF_TALK_VOICE_COUNT			," +
					"T_SUM.GPF_TALK_VOICE_MIN			," +
					"T_SUM.GPF_TALK_VOICE_POINT			," +
					"T_SUM.GPF_TALK_VOICE_PAY_POINT		," +
					"T_SUM.PLAY_PV_MSG_COUNT			," +
					"T_SUM.PLAY_PV_MSG_MIN				," +
					"T_SUM.PLAY_PV_MSG_POINT			," +
					"T_SUM.PLAY_PV_MSG_PAY_POINT		," +
					"T_SUM.CAST_PRV_TV_TALK_COUNT		," +
					"T_SUM.CAST_PRV_TV_TALK_MIN			," +
					"T_SUM.CAST_PRV_TV_TALK_POINT		," +
					"T_SUM.CAST_PRV_TV_TALK_PAY_POINT	," +
					"T_SUM.CAST_PUB_TV_TALK_COUNT		," +
					"T_SUM.CAST_PUB_TV_TALK_MIN			," +
					"T_SUM.CAST_PUB_TV_TALK_POINT		," +
					"T_SUM.CAST_PUB_TV_TALK_PAY_POINT	," +
					"T_SUM.CAST_PRV_VOICE_TALK_COUNT	," +
					"T_SUM.CAST_PRV_VOICE_TALK_MIN		," +
					"T_SUM.CAST_PRV_VOICE_TALK_POINT	," +
					"T_SUM.CAST_PRV_VOICE_TALK_PAY_POINT," +
					"T_SUM.CAST_PUB_VOICE_TALK_COUNT	," +
					"T_SUM.CAST_PUB_VOICE_TALK_MIN		," +
					"T_SUM.CAST_PUB_VOICE_TALK_POINT	," +
					"T_SUM.CAST_PUB_VOICE_TALK_PAY_POINT," +
					"T_SUM.USER_MAIL_COUNT				," +
					"T_SUM.USER_MAIL_POINT				," +
					"T_SUM.USER_MAIL_PAY_POINT			," +
					"T_SUM.OPEN_PIC_MAIL_COUNT			," +
					"T_SUM.OPEN_PIC_MAIL_POINT			," +
					"T_SUM.OPEN_PIC_MAIL_PAY_POINT		," +
					"T_SUM.OPEN_MOVIE_MAIL_COUNT		," +
					"T_SUM.OPEN_MOVIE_MAIL_POINT		," +
					"T_SUM.OPEN_MOVIE_MAIL_PAY_POINT	," +
					"T_SUM.OPEN_PIC_BBS_COUNT			," +
					"T_SUM.OPEN_PIC_BBS_POINT			," +
					"T_SUM.OPEN_PIC_BBS_PAY_POINT		," +
					"T_SUM.OPEN_MOVIE_BBS_COUNT			," +
					"T_SUM.OPEN_MOVIE_BBS_POINT			," +
					"T_SUM.OPEN_MOVIE_BBS_PAY_POINT		," +
					"T_SUM.USER_MAIL_PIC_POINT			," +
					"T_SUM.USER_MAIL_PIC_COUNT			," +
					"T_SUM.USER_MAIL_PIC_PAY_POINT		," +
					"T_SUM.USER_MAIL_MOVIE_POINT		," +
					"T_SUM.USER_MAIL_MOVIE_COUNT		," +
					"T_SUM.USER_MAIL_MOVIE_PAY_POINT	," +
					"T_SUM.PRV_MESSAGE_RX_POINT			," +
					"T_SUM.PRV_MESSAGE_RX_COUNT			," +
					"T_SUM.PRV_MESSAGE_RX_PAY_POINT		," +
					"T_SUM.PRV_PROFILE_RX_POINT			," +
					"T_SUM.PRV_PROFILE_RX_COUNT			," +
					"T_SUM.PRV_PROFILE_RX_PAY_POINT		," +
					"T_SUM.DOWNLOAD_MOVIE_POINT			," +
					"T_SUM.DOWNLOAD_MOVIE_COUNT			," +
					"T_SUM.DOWNLOAD_MOVIE_PAY_POINT		," +
					"T_SUM.DOWNLOAD_VOICE_POINT			," +
					"T_SUM.DOWNLOAD_VOICE_COUNT			," +
					"T_SUM.DOWNLOAD_VOICE_PAY_POINT		," +
					"T_SUM.SOCIAL_GAME_POINT			," +
					"T_SUM.SOCIAL_GAME_COUNT			," +
					"T_SUM.SOCIAL_GAME_PAY_POINT		," +
					"T_SUM.YAKYUKEN_POINT				," +
					"T_SUM.YAKYUKEN_COUNT				," +
					"T_SUM.YAKYUKEN_PAY_POINT			," +
					"T_SUM.PRESENT_MAIL_POINT			," +
					"T_SUM.PRESENT_MAIL_COUNT			," +
					"T_SUM.PRESENT_MAIL_PAY_POINT		," +
					"T_SUM.TIME_CALL_POINT				," +
					"T_SUM.TIME_CALL_COUNT				," +
					"T_SUM.TIME_CALL_PAY_POINT			," +
					"T_SUM.WEB_TOTAL_COUNT				," +
					"T_SUM.WEB_TOTAL_POINT				," +
					"T_SUM.WEB_TOTAL_PAY_POINT			" +
				"FROM " +
					"T_MANAGER,(" +
				"SELECT ";

			if (!iBridUtil.GetStringValue(pSiteCd).Equals("")) {
				sSql += "SITE_CD	,";
			}

			sSql += "MANAGER_SEQ," +
					"SUM(PRV_TV_TALK_COUNT				) AS PRV_TV_TALK_COUNT				," +
					"SUM(PRV_TV_TALK_MIN2				) AS PRV_TV_TALK_MIN				," +
					"SUM(PRV_TV_TALK_POINT				) AS PRV_TV_TALK_POINT				," +
					"SUM(PRV_TV_TALK_PAY_POINT			) AS PRV_TV_TALK_PAY_POINT			," +
					"SUM(PUB_TV_TALK_COUNT				) AS PUB_TV_TALK_COUNT				," +
					"SUM(PUB_TV_TALK_MIN2				) AS PUB_TV_TALK_MIN				," +
					"SUM(PUB_TV_TALK_POINT				) AS PUB_TV_TALK_POINT				," +
					"SUM(PUB_TV_TALK_PAY_POINT			) AS PUB_TV_TALK_PAY_POINT			," +
					"SUM(PRV_VOICE_TALK_COUNT			) AS PRV_VOICE_TALK_COUNT			," +
					"SUM(PRV_VOICE_TALK_MIN2			) AS PRV_VOICE_TALK_MIN				," +
					"SUM(PRV_VOICE_TALK_POINT			) AS PRV_VOICE_TALK_POINT			," +
					"SUM(PRV_VOICE_TALK_PAY_POINT		) AS PRV_VOICE_TALK_PAY_POINT		," +
					"SUM(PUB_VOICE_TALK_COUNT			) AS PUB_VOICE_TALK_COUNT			," +
					"SUM(PUB_VOICE_TALK_MIN2			) AS PUB_VOICE_TALK_MIN				," +
					"SUM(PUB_VOICE_TALK_POINT			) AS PUB_VOICE_TALK_POINT			," +
					"SUM(PUB_VOICE_TALK_PAY_POINT		) AS PUB_VOICE_TALK_PAY_POINT		," +
					"SUM(VIEW_TALK_COUNT				) AS VIEW_TALK_COUNT				," +
					"SUM(VIEW_TALK_MIN2					) AS VIEW_TALK_MIN					," +
					"SUM(VIEW_TALK_POINT				) AS VIEW_TALK_POINT				," +
					"SUM(VIEW_TALK_PAY_POINT			) AS VIEW_TALK_PAY_POINT			," +
					"SUM(VIEW_BROADCAST_COUNT			) AS VIEW_BROADCAST_COUNT			," +
					"SUM(VIEW_BROADCAST_MIN2			) AS VIEW_BROADCAST_MIN				," +
					"SUM(VIEW_BROADCAST_POINT			) AS VIEW_BROADCAST_POINT			," +
					"SUM(VIEW_BROADCAST_PAY_POINT		) AS VIEW_BROADCAST_PAY_POINT		," +
					"SUM(LIVE_COUNT						) AS LIVE_COUNT						," +
					"SUM(LIVE_MIN2						) AS LIVE_MIN						," +
					"SUM(LIVE_POINT						) AS LIVE_POINT						," +
					"SUM(LIVE_PAY_POINT					) AS LIVE_PAY_POINT					," +
					"SUM(MOVIE_COUNT					) AS MOVIE_COUNT					," +
					"SUM(MOVIE_MIN2						) AS MOVIE_MIN						," +
					"SUM(MOVIE_POINT					) AS MOVIE_POINT					," +
					"SUM(MOVIE_PAY_POINT				) AS MOVIE_PAY_POINT				," +
					"SUM(PLAY_PROFILE_COUNT				) AS PLAY_PROFILE_COUNT				," +
					"SUM(PLAY_PROFILE_MIN2				) AS PLAY_PROFILE_MIN				," +
					"SUM(PLAY_PROFILE_POINT				) AS PLAY_PROFILE_POINT				," +
					"SUM(PLAY_PROFILE_PAY_POINT			) AS PLAY_PROFILE_PAY_POINT			," +
					"SUM(REC_PROFILE_COUNT				) AS REC_PROFILE_COUNT				," +
					"SUM(REC_PROFILE_MIN2				) AS REC_PROFILE_MIN				," +
					"SUM(REC_PROFILE_POINT				) AS REC_PROFILE_POINT				," +
					"SUM(REC_PROFILE_PAY_POINT			) AS REC_PROFILE_PAY_POINT			," +
					"SUM(WIRETAP_COUNT					) AS WIRETAP_COUNT					," +
					"SUM(WIRETAP_MIN2					) AS WIRETAP_MIN					," +
					"SUM(WIRETAP_POINT					) AS WIRETAP_POINT					," +
					"SUM(WIRETAP_PAY_POINT				) AS WIRETAP_PAY_POINT				," +
					"SUM(GPF_TALK_VOICE_COUNT			) AS GPF_TALK_VOICE_COUNT			," +
					"SUM(GPF_TALK_VOICE_MIN2			) AS GPF_TALK_VOICE_MIN				," +
					"SUM(GPF_TALK_VOICE_POINT			) AS GPF_TALK_VOICE_POINT			," +
					"SUM(GPF_TALK_VOICE_PAY_POINT		) AS GPF_TALK_VOICE_PAY_POINT		," +
					"SUM(PLAY_PV_MSG_COUNT				) AS PLAY_PV_MSG_COUNT				," +
					"SUM(PLAY_PV_MSG_MIN2				) AS PLAY_PV_MSG_MIN				," +
					"SUM(PLAY_PV_MSG_POINT				) AS PLAY_PV_MSG_POINT				," +
					"SUM(PLAY_PV_MSG_PAY_POINT			) AS PLAY_PV_MSG_PAY_POINT			," +
					"SUM(CAST_PRV_TV_TALK_COUNT			) AS CAST_PRV_TV_TALK_COUNT			," +
					"SUM(CAST_PRV_TV_TALK_MIN2			) AS CAST_PRV_TV_TALK_MIN			," +
					"SUM(CAST_PRV_TV_TALK_POINT			) AS CAST_PRV_TV_TALK_POINT			," +
					"SUM(CAST_PRV_TV_TALK_PAY_POINT		) AS CAST_PRV_TV_TALK_PAY_POINT		," +
					"SUM(CAST_PUB_TV_TALK_COUNT			) AS CAST_PUB_TV_TALK_COUNT			," +
					"SUM(CAST_PUB_TV_TALK_MIN2			) AS CAST_PUB_TV_TALK_MIN			," +
					"SUM(CAST_PUB_TV_TALK_POINT			) AS CAST_PUB_TV_TALK_POINT			," +
					"SUM(CAST_PUB_TV_TALK_PAY_POINT		) AS CAST_PUB_TV_TALK_PAY_POINT		," +
					"SUM(CAST_PRV_VOICE_TALK_COUNT		) AS CAST_PRV_VOICE_TALK_COUNT		," +
					"SUM(CAST_PRV_VOICE_TALK_MIN2		) AS CAST_PRV_VOICE_TALK_MIN		," +
					"SUM(CAST_PRV_VOICE_TALK_POINT		) AS CAST_PRV_VOICE_TALK_POINT		," +
					"SUM(CAST_PRV_VOICE_TALK_PAY_POINT	) AS CAST_PRV_VOICE_TALK_PAY_POINT	," +
					"SUM(CAST_PUB_VOICE_TALK_COUNT		) AS CAST_PUB_VOICE_TALK_COUNT		," +
					"SUM(CAST_PUB_VOICE_TALK_MIN2		) AS CAST_PUB_VOICE_TALK_MIN		," +
					"SUM(CAST_PUB_VOICE_TALK_POINT		) AS CAST_PUB_VOICE_TALK_POINT		," +
					"SUM(CAST_PUB_VOICE_TALK_PAY_POINT	) AS CAST_PUB_VOICE_TALK_PAY_POINT	," +
					"SUM(PRV_TV_TALK_COUNT	+ PUB_TV_TALK_COUNT		+ PRV_VOICE_TALK_COUNT	+ PUB_VOICE_TALK_COUNT	+ VIEW_TALK_COUNT	+ VIEW_BROADCAST_COUNT	+ LIVE_COUNT	+ PLAY_PROFILE_COUNT	+ REC_PROFILE_COUNT		+ WIRETAP_COUNT		+ GPF_TALK_VOICE_COUNT	 + PLAY_PV_MSG_COUNT	+ CAST_PRV_TV_TALK_COUNT	+ CAST_PUB_TV_TALK_COUNT	+ CAST_PRV_VOICE_TALK_COUNT		+ CAST_PUB_VOICE_TALK_COUNT		) AS TALK_COUNT	," +
					"SUM(PRV_TV_TALK_MIN2	+ PUB_TV_TALK_MIN2		+ PRV_VOICE_TALK_MIN2	+ PUB_VOICE_TALK_MIN2	+ VIEW_TALK_MIN2	+ VIEW_BROADCAST_MIN2	+ LIVE_MIN2		+ PLAY_PROFILE_MIN2		+ REC_PROFILE_MIN2		+ WIRETAP_MIN2		+ GPF_TALK_VOICE_MIN2	 + PLAY_PV_MSG_MIN2		+ CAST_PRV_TV_TALK_MIN2		+ CAST_PUB_TV_TALK_MIN2		+ CAST_PRV_VOICE_TALK_MIN2		+ CAST_PUB_VOICE_TALK_MIN2		) AS TALK_MIN	," +
					"SUM(PRV_TV_TALK_POINT	+ PUB_TV_TALK_POINT		+ PRV_VOICE_TALK_POINT	+ PUB_VOICE_TALK_POINT	+ VIEW_TALK_POINT	+ VIEW_BROADCAST_POINT	+ LIVE_POINT	+ PLAY_PROFILE_POINT	+ REC_PROFILE_POINT		+ WIRETAP_POINT		+ GPF_TALK_VOICE_POINT	 + PLAY_PV_MSG_POINT	+ CAST_PRV_TV_TALK_POINT	+ CAST_PUB_TV_TALK_POINT	+ CAST_PRV_VOICE_TALK_POINT		+ CAST_PUB_VOICE_TALK_POINT		) AS TALK_POINT	," +
					"SUM(PRV_TV_TALK_PAY_POINT+ PUB_TV_TALK_PAY_POINT	+ PRV_VOICE_TALK_PAY_POINT+ PUB_VOICE_TALK_PAY_POINT+ VIEW_TALK_PAY_POINT	+ VIEW_BROADCAST_PAY_POINT+ LIVE_PAY_POINT	+ PLAY_PROFILE_PAY_POINT	+ REC_PROFILE_PAY_POINT	+ WIRETAP_PAY_POINT	+ GPF_TALK_VOICE_PAY_POINT + PLAY_PV_MSG_PAY_POINT	+ CAST_PRV_TV_TALK_PAY_POINT	+ CAST_PUB_TV_TALK_PAY_POINT	+ CAST_PRV_VOICE_TALK_PAY_POINT	+ CAST_PUB_VOICE_TALK_PAY_POINT	) AS TALK_PAY_POINT," +
					"SUM(USER_MAIL_COUNT			) AS USER_MAIL_COUNT			," +
					"SUM(USER_MAIL_POINT			) AS USER_MAIL_POINT			," +
					"SUM(USER_MAIL_PAY_POINT		) AS USER_MAIL_PAY_POINT		," +
					"SUM(OPEN_PIC_MAIL_COUNT		) AS OPEN_PIC_MAIL_COUNT		," +
					"SUM(OPEN_PIC_MAIL_POINT		) AS OPEN_PIC_MAIL_POINT		," +
					"SUM(OPEN_PIC_MAIL_PAY_POINT	) AS OPEN_PIC_MAIL_PAY_POINT	," +
					"SUM(OPEN_MOVIE_MAIL_COUNT		) AS OPEN_MOVIE_MAIL_COUNT		," +
					"SUM(OPEN_MOVIE_MAIL_POINT		) AS OPEN_MOVIE_MAIL_POINT		," +
					"SUM(OPEN_MOVIE_MAIL_PAY_POINT	) AS OPEN_MOVIE_MAIL_PAY_POINT	," +
					"SUM(OPEN_PIC_BBS_COUNT			) AS OPEN_PIC_BBS_COUNT			," +
					"SUM(OPEN_PIC_BBS_POINT			) AS OPEN_PIC_BBS_POINT			," +
					"SUM(OPEN_PIC_BBS_PAY_POINT		) AS OPEN_PIC_BBS_PAY_POINT		," +
					"SUM(OPEN_MOVIE_BBS_COUNT		) AS OPEN_MOVIE_BBS_COUNT		," +
					"SUM(OPEN_MOVIE_BBS_POINT		) AS OPEN_MOVIE_BBS_POINT		," +
					"SUM(OPEN_MOVIE_BBS_PAY_POINT	) AS OPEN_MOVIE_BBS_PAY_POINT	," +
					"SUM(USER_MAIL_PIC_POINT		) AS USER_MAIL_PIC_POINT		," +
					"SUM(USER_MAIL_PIC_COUNT		) AS USER_MAIL_PIC_COUNT		," +
					"SUM(USER_MAIL_PIC_PAY_POINT	) AS USER_MAIL_PIC_PAY_POINT	," +
					"SUM(USER_MAIL_MOVIE_POINT		) AS USER_MAIL_MOVIE_POINT		," +
					"SUM(USER_MAIL_MOVIE_COUNT		) AS USER_MAIL_MOVIE_COUNT		," +
					"SUM(USER_MAIL_MOVIE_PAY_POINT	) AS USER_MAIL_MOVIE_PAY_POINT	," +
					"SUM(PRV_MESSAGE_RX_POINT		) AS PRV_MESSAGE_RX_POINT		," +
					"SUM(PRV_MESSAGE_RX_COUNT		) AS PRV_MESSAGE_RX_COUNT		," +
					"SUM(PRV_MESSAGE_RX_PAY_POINT	) AS PRV_MESSAGE_RX_PAY_POINT	," +
					"SUM(PRV_PROFILE_RX_POINT		) AS PRV_PROFILE_RX_POINT		," +
					"SUM(PRV_PROFILE_RX_COUNT		) AS PRV_PROFILE_RX_COUNT		," +
					"SUM(PRV_PROFILE_RX_PAY_POINT	) AS PRV_PROFILE_RX_PAY_POINT	," +
					"SUM(DOWNLOAD_MOVIE_POINT		) AS DOWNLOAD_MOVIE_POINT		," +
					"SUM(DOWNLOAD_MOVIE_COUNT		) AS DOWNLOAD_MOVIE_COUNT		," +
					"SUM(DOWNLOAD_MOVIE_PAY_POINT	) AS DOWNLOAD_MOVIE_PAY_POINT	," +
					"SUM(DOWNLOAD_VOICE_POINT		) AS DOWNLOAD_VOICE_POINT		," +
					"SUM(DOWNLOAD_VOICE_COUNT		) AS DOWNLOAD_VOICE_COUNT		," +
					"SUM(DOWNLOAD_VOICE_PAY_POINT	) AS DOWNLOAD_VOICE_PAY_POINT	," +
					"SUM(SOCIAL_GAME_POINT			) AS SOCIAL_GAME_POINT		," +
					"SUM(SOCIAL_GAME_COUNT			) AS SOCIAL_GAME_COUNT		," +
					"SUM(SOCIAL_GAME_PAY_POINT		) AS SOCIAL_GAME_PAY_POINT	," +
					"SUM(YAKYUKEN_POINT				) AS YAKYUKEN_POINT				," +
					"SUM(YAKYUKEN_COUNT				) AS YAKYUKEN_COUNT				," +
					"SUM(YAKYUKEN_PAY_POINT			) AS YAKYUKEN_PAY_POINT			," +
					"SUM(PRESENT_MAIL_POINT			) AS PRESENT_MAIL_POINT			," +
					"SUM(PRESENT_MAIL_COUNT			) AS PRESENT_MAIL_COUNT			," +
					"SUM(PRESENT_MAIL_PAY_POINT		) AS PRESENT_MAIL_PAY_POINT		," +
					"SUM(TIME_CALL_POINT			) AS TIME_CALL_POINT			," +
					"SUM(TIME_CALL_COUNT			) AS TIME_CALL_COUNT			," +
					"SUM(TIME_CALL_PAY_POINT		) AS TIME_CALL_PAY_POINT		," +
					"SUM(USER_MAIL_POINT	+ OPEN_PIC_MAIL_POINT	+ OPEN_MOVIE_MAIL_POINT		+ OPEN_PIC_BBS_POINT	+ OPEN_MOVIE_BBS_POINT	 + USER_MAIL_PIC_POINT   + USER_MAIL_MOVIE_POINT	+ PRV_MESSAGE_RX_POINT		+ PRV_PROFILE_RX_POINT	+ DOWNLOAD_MOVIE_POINT	+ DOWNLOAD_VOICE_POINT	+ SOCIAL_GAME_POINT + YAKYUKEN_POINT + PRESENT_MAIL_POINT + TIME_CALL_POINT	) AS WEB_TOTAL_POINT," +
					"SUM(USER_MAIL_COUNT	+ OPEN_PIC_MAIL_COUNT	+ OPEN_MOVIE_MAIL_COUNT		+ OPEN_PIC_BBS_COUNT	+ OPEN_MOVIE_BBS_COUNT	 + USER_MAIL_PIC_COUNT   + USER_MAIL_MOVIE_COUNT	+ PRV_MESSAGE_RX_COUNT		+ PRV_PROFILE_RX_COUNT	+ DOWNLOAD_MOVIE_COUNT	+ DOWNLOAD_VOICE_COUNT	+ SOCIAL_GAME_COUNT + YAKYUKEN_COUNT + PRESENT_MAIL_COUNT + TIME_CALL_COUNT	) AS WEB_TOTAL_COUNT," +
					"SUM(USER_MAIL_PAY_POINT	+ OPEN_PIC_MAIL_PAY_POINT + OPEN_MOVIE_MAIL_PAY_POINT	+ OPEN_PIC_BBS_PAY_POINT	+ OPEN_MOVIE_BBS_PAY_POINT + USER_MAIL_PIC_PAY_POINT + USER_MAIL_MOVIE_PAY_POINT	+ PRV_MESSAGE_RX_PAY_POINT	+ PRV_PROFILE_RX_PAY_POINT+ DOWNLOAD_MOVIE_PAY_POINT+ DOWNLOAD_VOICE_PAY_POINT+ SOCIAL_GAME_PAY_POINT + YAKYUKEN_PAY_POINT + PRESENT_MAIL_PAY_POINT + TIME_CALL_PAY_POINT	) AS WEB_TOTAL_PAY_POINT " +
					" FROM " +
						"VW_TIME_OPERATION02 " +
					"WHERE ";

			if (!iBridUtil.GetStringValue(pSiteCd).Equals("")) {
				sSql += "SITE_CD= :SITE_CD	AND ";
			}

			sSql += "REPORT_DAY_TIME  >= :REPORT_DAY_TIME_FROM  AND REPORT_DAY_TIME < :REPORT_DAY_TIME_TO  ";

			if (!iBridUtil.GetStringValue(pProductionSeq).Equals("")) {
				sSql += " AND PRODUCTION_SEQ = :PRODUCTION_SEQ ";
			}

			if (!string.IsNullOrEmpty(pManagerSeq)) {
				sSql += " AND MANAGER_SEQ = :MANAGER_SEQ ";
			}

			if (!iBridUtil.GetStringValue(pSiteCd).Equals("")) {
				sSql += " GROUP BY SITE_CD,MANAGER_SEQ " +
								" ORDER BY SITE_CD, MANAGER_SEQ) T_SUM " +
							"WHERE " +
								"T_MANAGER.MANAGER_SEQ = T_SUM.MANAGER_SEQ " +
							"ORDER BY T_SUM.SITE_CD,T_SUM.MANAGER_SEQ ";
			} else {
				sSql += " GROUP BY MANAGER_SEQ " +
								" ORDER BY MANAGER_SEQ) T_SUM " +
							"WHERE " +
								"T_MANAGER.MANAGER_SEQ = T_SUM.MANAGER_SEQ " +
							"ORDER BY T_SUM.MANAGER_SEQ ";
			}

			using (cmd = CreateSelectCommand(sSql,conn)) {
				if (!iBridUtil.GetStringValue(pSiteCd).Equals("")) {
					cmd.Parameters.Add("SITE_CD",pSiteCd);
				}

				cmd.Parameters.Add("REPORT_DAY_FROM",sFromDay);
				cmd.Parameters.Add("REPORT_DAY_TIME_TO",sToDay);

				if (!iBridUtil.GetStringValue(pProductionSeq).Equals("")) {
					cmd.Parameters.Add("PRODUCTION_SEQ",pProductionSeq);
				}
				if (!string.IsNullOrEmpty(pManagerSeq)) {
					cmd.Parameters.Add("MANAGER_SEQ",pManagerSeq);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetNotPaymentList(
		string pFromYYYY,
		string pFromMM,
		string pFromDD,
		string pFromHH,
		string pToYYYY,
		string pToMM,
		string pToDD,
		string pToHH,
		string pLoginId,
		string pProductionSeq,
		string pTotalPayAmt,
		string pNoBankCastDispFlag,
		string pInvalidBankCastDispFlag,
		string pNotNormalCastDispFlag,
		string pWaitPaymentFlag,
		string pReqPaymentDate
	) {
		return GetNotPaymentList(
									pFromYYYY,
									pFromMM,
									pFromDD,
									pFromHH,
									pToYYYY,
									pToMM,
									pToDD,
									pToHH,
									pLoginId,
									pProductionSeq,
									pTotalPayAmt,
									pNoBankCastDispFlag,
									pInvalidBankCastDispFlag,
									pNotNormalCastDispFlag,
									pWaitPaymentFlag,
									pReqPaymentDate,
									0);

	}

	public DataSet GetNotPaymentList(
		string pFromYYYY,
		string pFromMM,
		string pFromDD,
		string pFromHH,
		string pToYYYY,
		string pToMM,
		string pToDD,
		string pToHH,
		string pLoginId,
		string pProductionSeq,
		string pTotalPayAmt,
		string pNoBankCastDispFlag,
		string pInvalidBankCastDispFlag,
		string pNotNormalCastDispFlag,
		string pWaitPaymentFlag,
		string pReqPaymentDate,
		int pUserStatusMask
	) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sFromDay = pFromYYYY + pFromMM + pFromDD + pFromHH;
			string sToDay = pToYYYY + pToMM + pToDD + pToHH;
			StringBuilder sSql = new StringBuilder();
			sSql.AppendLine("SELECT	");
			sSql.AppendLine("	LOGIN_ID					,");
			sSql.AppendLine("	CAST_NM						,");
			sSql.AppendLine("	USER_SEQ					,");
			sSql.AppendLine("	STAFF_ID					,");
			sSql.AppendLine("	BANK_CD						,");
			sSql.AppendLine("	BANK_NM						,");
			sSql.AppendLine("	BANK_OFFICE_CD				,");
			sSql.AppendLine("	BANK_OFFICE_NM				,");
			sSql.AppendLine("	BANK_OFFICE_KANA_NM			,");
			sSql.AppendLine("	BANK_ACCOUNT_TYPE			,");
			sSql.AppendLine("	BANK_ACCOUNT_TYPE2			,");
			sSql.AppendLine("	BANK_ACCOUNT_HOLDER_NM		,");
			sSql.AppendLine("	BANK_ACCOUNT_NO				,");
			sSql.AppendLine("	BANK_ACCOUNT_INVALID_FLAG	,");
			sSql.AppendLine("	WAIT_PAYMENT_FLAG			,");
			sSql.AppendLine("	REQ_PAYMENT_DATE			,");
			sSql.AppendLine("	NVL(PRV_TV_TALK_PAY_AMT			,0) AS PRV_TV_TALK_PAY_AMT			,");
			sSql.AppendLine("	NVL(PUB_TV_TALK_PAY_AMT			,0) AS PUB_TV_TALK_PAY_AMT			,");
			sSql.AppendLine("	NVL(PRV_VOICE_TALK_PAY_AMT		,0) AS PRV_VOICE_TALK_PAY_AMT		,");
			sSql.AppendLine("	NVL(PUB_VOICE_TALK_PAY_AMT		,0) AS PUB_VOICE_TALK_PAY_AMT		,");
			sSql.AppendLine("	NVL(VIEW_TALK_PAY_AMT			,0) AS VIEW_TALK_PAY_AMT			,");
			sSql.AppendLine("	NVL(VIEW_BROADCAST_PAY_AMT		,0) AS VIEW_BROADCAST_PAY_AMT		,");
			sSql.AppendLine("	NVL(LIVE_PAY_AMT				,0) AS LIVE_PAY_AMT					,");
			sSql.AppendLine("	NVL(MOVIE_PAY_AMT				,0) AS MOVIE_PAY_AMT				,");
			sSql.AppendLine("	NVL(PLAY_PROFILE_PAY_AMT		,0) AS PLAY_PROFILE_PAY_AMT			,");
			sSql.AppendLine("	NVL(REC_PROFILE_PAY_AMT			,0) AS REC_PROFILE_PAY_AMT			,");
			sSql.AppendLine("	NVL(TALK_PAY_AMT				,0) AS TALK_PAY_AMT					,");
			sSql.AppendLine("	NVL(WIRETAP_PAY_AMT				,0) AS WIRETAP_PAY_AMT				,");
			sSql.AppendLine("	NVL(GPF_TALK_VOICE_PAY_AMT		,0) AS GPF_TALK_VOICE_PAY_AMT		,");
			sSql.AppendLine("	NVL(PLAY_PV_MSG_PAY_AMT			,0) AS PLAY_PV_MSG_PAY_AMT			,");
			sSql.AppendLine("	NVL(CAST_PRV_TV_TALK_PAY_AMT	,0) AS CAST_PRV_TV_TALK_PAY_AMT		,");
			sSql.AppendLine("	NVL(CAST_PUB_TV_TALK_PAY_AMT	,0) AS CAST_PUB_TV_TALK_PAY_AMT		,");
			sSql.AppendLine("	NVL(CAST_PRV_VOICE_TALK_PAY_AMT	,0) AS CAST_PRV_VOICE_TALK_PAY_AMT	,");
			sSql.AppendLine("	NVL(CAST_PUB_VOICE_TALK_PAY_AMT	,0) AS CAST_PUB_VOICE_TALK_PAY_AMT	,");
			sSql.AppendLine("	NVL(USER_MAIL_PAY_AMT			,0) AS USER_MAIL_PAY_AMT			,");
			sSql.AppendLine("	NVL(OPEN_PIC_MAIL_PAY_AMT		,0) AS OPEN_PIC_MAIL_PAY_AMT		,");
			sSql.AppendLine("	NVL(OPEN_MOVIE_MAIL_PAY_AMT		,0) AS OPEN_MOVIE_MAIL_PAY_AMT		,");
			sSql.AppendLine("	NVL(OPEN_PIC_BBS_PAY_AMT		,0) AS OPEN_PIC_BBS_PAY_AMT			,");
			sSql.AppendLine("	NVL(OPEN_MOVIE_BBS_PAY_AMT		,0) AS OPEN_MOVIE_BBS_PAY_AMT		,");
			sSql.AppendLine("	NVL(WEB_TOTAL_PAY_AMT			,0) AS WEB_TOTAL_PAY_AMT			,");
			sSql.AppendLine("	NVL(USER_MAIL_PIC_PAY_AMT		,0) AS USER_MAIL_PIC_PAY_AMT		,");
			sSql.AppendLine("	NVL(USER_MAIL_MOVIE_PAY_AMT		,0) AS USER_MAIL_MOVIE_PAY_AMT		,");
			sSql.AppendLine("	NVL(PRV_MESSAGE_RX_PAY_AMT		,0) AS PRV_MESSAGE_RX_PAY_AMT		,");
			sSql.AppendLine("	NVL(PRV_PROFILE_RX_PAY_AMT		,0) AS PRV_PROFILE_RX_PAY_AMT		,");
			sSql.AppendLine("	NVL(DOWNLOAD_MOVIE_PAY_AMT		,0) AS DOWNLOAD_MOVIE_PAY_AMT		,");
			sSql.AppendLine("	NVL(DOWNLOAD_VOICE_PAY_AMT		,0) AS DOWNLOAD_VOICE_PAY_AMT		,");
			sSql.AppendLine("	NVL(SOCIAL_GAME_PAY_AMT			,0) AS SOCIAL_GAME_PAY_AMT			,");
			sSql.AppendLine("	NVL(YAKYUKEN_PAY_AMT			,0) AS YAKYUKEN_PAY_AMT				,");
			sSql.AppendLine("	NVL(PRESENT_MAIL_PAY_AMT		,0) AS PRESENT_MAIL_PAY_AMT			,");
			sSql.AppendLine("	NVL(TIME_CALL_PAY_AMT			,0) AS TIME_CALL_PAY_AMT			,");
			sSql.AppendLine("	NVL(FRIEND_INTRO_PAY_AMT		,0) AS FRIEND_INTRO_PAY_AMT			,");
			sSql.AppendLine("	NVL(BONUS_AMT					,0) AS BONUS_AMT					,");
			sSql.AppendLine("	(NVL(TALK_PAY_AMT,0) + NVL(WEB_TOTAL_PAY_AMT,0) + NVL(BONUS_AMT,0) + NVL(FRIEND_INTRO_PAY_AMT,0) ) AS TOTAL_PAY_AMT	");
			sSql.AppendLine("FROM ");
			sSql.AppendLine("	(SELECT ");
			sSql.AppendLine("		T_USER.LOGIN_ID						,");
			sSql.AppendLine("		T_CAST.CAST_NM						,");
			sSql.AppendLine("		T_CAST.STAFF_ID						,");
			sSql.AppendLine("		T_CAST.BANK_ACCOUNT_INVALID_FLAG	,");
			sSql.AppendLine("		T_CAST.BANK_CD						,");
			sSql.AppendLine("		T_CAST.BANK_NM						,");
			sSql.AppendLine("		T_CAST.BANK_OFFICE_CD				,");
			sSql.AppendLine("		T_CAST.BANK_OFFICE_NM				,");
			sSql.AppendLine("		T_CAST.BANK_OFFICE_KANA_NM			,");
			sSql.AppendLine("		T_CAST.BANK_ACCOUNT_TYPE			,");
			sSql.AppendLine("		T_CAST.BANK_ACCOUNT_TYPE2			,");
			sSql.AppendLine("		T_CAST.BANK_ACCOUNT_HOLDER_NM		,");
			sSql.AppendLine("		T_CAST.BANK_ACCOUNT_NO				,");
			sSql.AppendLine("		T_CAST.WAIT_PAYMENT_FLAG			,");
			sSql.AppendLine("		T_CAST.REQ_PAYMENT_DATE				,");
			sSql.AppendLine("		T_SUM.USER_SEQ						,");
			sSql.AppendLine("		T_SUM.PRV_TV_TALK_PAY_AMT			,");
			sSql.AppendLine("		T_SUM.PUB_TV_TALK_PAY_AMT			,");
			sSql.AppendLine("		T_SUM.PRV_VOICE_TALK_PAY_AMT		,");
			sSql.AppendLine("		T_SUM.PUB_VOICE_TALK_PAY_AMT		,");
			sSql.AppendLine("		T_SUM.VIEW_TALK_PAY_AMT				,");
			sSql.AppendLine("		T_SUM.VIEW_BROADCAST_PAY_AMT		,");
			sSql.AppendLine("		T_SUM.LIVE_PAY_AMT					,");
			sSql.AppendLine("		T_SUM.MOVIE_PAY_AMT					,");
			sSql.AppendLine("		T_SUM.PLAY_PROFILE_PAY_AMT			,");
			sSql.AppendLine("		T_SUM.REC_PROFILE_PAY_AMT			,");
			sSql.AppendLine("		T_SUM.TALK_PAY_AMT					,");
			sSql.AppendLine("		T_SUM.WIRETAP_PAY_AMT				,");
			sSql.AppendLine("		T_SUM.GPF_TALK_VOICE_PAY_AMT		,");
			sSql.AppendLine("		T_SUM.PLAY_PV_MSG_PAY_AMT			,");
			sSql.AppendLine("		T_SUM.CAST_PRV_TV_TALK_PAY_AMT		,");
			sSql.AppendLine("		T_SUM.CAST_PUB_TV_TALK_PAY_AMT		,");
			sSql.AppendLine("		T_SUM.CAST_PRV_VOICE_TALK_PAY_AMT	,");
			sSql.AppendLine("		T_SUM.CAST_PUB_VOICE_TALK_PAY_AMT	,");
			sSql.AppendLine("		T_SUM.USER_MAIL_PAY_AMT				,");
			sSql.AppendLine("		T_SUM.OPEN_PIC_MAIL_PAY_AMT			,");
			sSql.AppendLine("		T_SUM.OPEN_MOVIE_MAIL_PAY_AMT		,");
			sSql.AppendLine("		T_SUM.OPEN_PIC_BBS_PAY_AMT			,");
			sSql.AppendLine("		T_SUM.OPEN_MOVIE_BBS_PAY_AMT		,");
			sSql.AppendLine("		T_SUM.WEB_TOTAL_PAY_AMT				,");
			sSql.AppendLine("		T_SUM.USER_MAIL_PIC_PAY_AMT			,");
			sSql.AppendLine("		T_SUM.USER_MAIL_MOVIE_PAY_AMT		,");
			sSql.AppendLine("		T_SUM.PRV_MESSAGE_RX_PAY_AMT		,");
			sSql.AppendLine("		T_SUM.PRV_PROFILE_RX_PAY_AMT		,");
			sSql.AppendLine("		T_SUM.DOWNLOAD_MOVIE_PAY_AMT		,");
			sSql.AppendLine("		T_SUM.DOWNLOAD_VOICE_PAY_AMT		,");
			sSql.AppendLine("		T_SUM.SOCIAL_GAME_PAY_AMT			,");
			sSql.AppendLine("		T_SUM.YAKYUKEN_PAY_AMT				,");
			sSql.AppendLine("		T_SUM.PRESENT_MAIL_PAY_AMT			,");
			sSql.AppendLine("		T_SUM.TIME_CALL_PAY_AMT				,");
			sSql.AppendLine("		T_SUM.FRIEND_INTRO_PAY_POINT * T_CAST_RANK.POINT_PRICE	AS FRIEND_INTRO_PAY_AMT,");
			sSql.AppendLine("		NVL(T_BONUS_SUM.BONUS_AMT,0) AS BONUS_AMT	");
			sSql.AppendLine("	FROM	");
			sSql.AppendLine("		T_USER	,");
			sSql.AppendLine("		T_CAST	,");
			sSql.AppendLine("		T_CAST_RANK	,");
			sSql.AppendLine("		(SELECT	");
			sSql.AppendLine("			USER_SEQ	,");
			sSql.AppendLine("			SUM(PRV_TV_TALK_PAY_AMT			) AS PRV_TV_TALK_PAY_AMT			,");
			sSql.AppendLine("			SUM(PUB_TV_TALK_PAY_AMT			) AS PUB_TV_TALK_PAY_AMT			,");
			sSql.AppendLine("			SUM(PRV_VOICE_TALK_PAY_AMT		) AS PRV_VOICE_TALK_PAY_AMT			,");
			sSql.AppendLine("			SUM(PUB_VOICE_TALK_PAY_AMT		) AS PUB_VOICE_TALK_PAY_AMT			,");
			sSql.AppendLine("			SUM(VIEW_TALK_PAY_AMT			) AS VIEW_TALK_PAY_AMT				,");
			sSql.AppendLine("			SUM(VIEW_BROADCAST_PAY_AMT		) AS VIEW_BROADCAST_PAY_AMT			,");
			sSql.AppendLine("			SUM(LIVE_PAY_AMT				) AS LIVE_PAY_AMT					,");
			sSql.AppendLine("			SUM(MOVIE_PAY_AMT				) AS MOVIE_PAY_AMT					,");
			sSql.AppendLine("			SUM(PLAY_PROFILE_PAY_AMT		) AS PLAY_PROFILE_PAY_AMT			,");
			sSql.AppendLine("			SUM(REC_PROFILE_PAY_AMT			) AS REC_PROFILE_PAY_AMT			,");
			sSql.AppendLine("			SUM(WIRETAP_PAY_AMT				) AS WIRETAP_PAY_AMT				,");
			sSql.AppendLine("			SUM(GPF_TALK_VOICE_PAY_AMT		) AS GPF_TALK_VOICE_PAY_AMT			,");
			sSql.AppendLine("			SUM(PLAY_PV_MSG_PAY_AMT			) AS PLAY_PV_MSG_PAY_AMT			,");
			sSql.AppendLine("			SUM(CAST_PRV_TV_TALK_PAY_AMT	) AS CAST_PRV_TV_TALK_PAY_AMT		,");
			sSql.AppendLine("			SUM(CAST_PUB_TV_TALK_PAY_AMT	) AS CAST_PUB_TV_TALK_PAY_AMT		,");
			sSql.AppendLine("			SUM(CAST_PRV_VOICE_TALK_PAY_AMT	) AS CAST_PRV_VOICE_TALK_PAY_AMT	,");
			sSql.AppendLine("			SUM(CAST_PUB_VOICE_TALK_PAY_AMT	) AS CAST_PUB_VOICE_TALK_PAY_AMT	,");
			sSql.AppendLine("			SUM(PRV_TV_TALK_POINT	+ PUB_TV_TALK_POINT		+ PRV_VOICE_TALK_POINT	+ PUB_VOICE_TALK_POINT	+ VIEW_TALK_POINT	+ VIEW_BROADCAST_POINT	+ LIVE_POINT	+ MOVIE_POINT	+ PLAY_PROFILE_POINT	+ REC_PROFILE_POINT		+ WIRETAP_POINT		+ GPF_TALK_VOICE_POINT		+ PLAY_PV_MSG_POINT		+ CAST_PRV_TV_TALK_POINT	+ CAST_PUB_TV_TALK_POINT	+ CAST_PRV_VOICE_TALK_POINT		+ CAST_PUB_VOICE_TALK_POINT	) AS TALK_POINT, ");
			sSql.AppendLine("			SUM(PRV_TV_TALK_MIN	+ PUB_TV_TALK_MIN		+ PRV_VOICE_TALK_MIN	+ PUB_VOICE_TALK_MIN	+ VIEW_TALK_MIN		+ VIEW_BROADCAST_MIN	+ LIVE_MIN		+ MOVIE_MIN		+ PLAY_PROFILE_MIN		+ REC_PROFILE_MIN		+ WIRETAP_MIN		+ GPF_TALK_VOICE_MIN		+ PLAY_PV_MSG_MIN		+ CAST_PRV_TV_TALK_MIN		+ CAST_PUB_TV_TALK_MIN		+ CAST_PRV_VOICE_TALK_MIN		+ CAST_PUB_VOICE_TALK_MIN	+ INV_PRV_TV_TALK_MIN - INV_PUB_TV_TALK_MIN - INV_PRV_VOICE_TALK_MIN - INV_PUB_VOICE_TALK_MIN - INV_VIEW_TALK_MIN - INV_VIEW_BROADCAST_MIN - INV_LIVE_MIN - INV_MOVIE_MIN - INV_PLAY_PROFILE_MIN - INV_REC_PROFILE_MIN - INV_WIRETAP_MIN - INV_GPF_TALK_VOICE_MIN - INV_PLAY_PV_MSG_MIN) AS TALK_MIN	,");
			sSql.AppendLine("			SUM(PRV_TV_TALK_PAY_AMT+ PUB_TV_TALK_PAY_AMT	+ PRV_VOICE_TALK_PAY_AMT+ PUB_VOICE_TALK_PAY_AMT+ VIEW_TALK_PAY_AMT	+ VIEW_BROADCAST_PAY_AMT+ LIVE_PAY_AMT	+ MOVIE_PAY_AMT	+ PLAY_PROFILE_PAY_AMT	+ REC_PROFILE_PAY_AMT	+ WIRETAP_PAY_AMT	+ GPF_TALK_VOICE_PAY_AMT	+ PLAY_PV_MSG_PAY_AMT	+ CAST_PRV_TV_TALK_PAY_AMT	+ CAST_PUB_TV_TALK_PAY_AMT	+ CAST_PRV_VOICE_TALK_PAY_AMT	+ CAST_PUB_VOICE_TALK_PAY_AMT) AS TALK_PAY_AMT	,");
			sSql.AppendLine("			SUM(USER_MAIL_PAY_AMT			) AS USER_MAIL_PAY_AMT				,");
			sSql.AppendLine("			SUM(OPEN_PIC_MAIL_PAY_AMT		) AS OPEN_PIC_MAIL_PAY_AMT			,");
			sSql.AppendLine("			SUM(OPEN_MOVIE_MAIL_PAY_AMT		) AS OPEN_MOVIE_MAIL_PAY_AMT		,");
			sSql.AppendLine("			SUM(OPEN_PIC_BBS_PAY_AMT		) AS OPEN_PIC_BBS_PAY_AMT			,");
			sSql.AppendLine("			SUM(OPEN_MOVIE_BBS_PAY_AMT		) AS OPEN_MOVIE_BBS_PAY_AMT			,");
			sSql.AppendLine("			SUM(USER_MAIL_PIC_PAY_AMT		) AS USER_MAIL_PIC_PAY_AMT			,");
			sSql.AppendLine("			SUM(USER_MAIL_MOVIE_PAY_AMT		) AS USER_MAIL_MOVIE_PAY_AMT		,");
			sSql.AppendLine("			SUM(PRV_MESSAGE_RX_PAY_AMT		) AS PRV_MESSAGE_RX_PAY_AMT			,");
			sSql.AppendLine("			SUM(PRV_PROFILE_RX_PAY_AMT		) AS PRV_PROFILE_RX_PAY_AMT			,");
			sSql.AppendLine("			SUM(DOWNLOAD_MOVIE_PAY_AMT		) AS DOWNLOAD_MOVIE_PAY_AMT			,");
			sSql.AppendLine("			SUM(DOWNLOAD_VOICE_PAY_AMT		) AS DOWNLOAD_VOICE_PAY_AMT			,");
			sSql.AppendLine("			SUM(SOCIAL_GAME_PAY_AMT			) AS SOCIAL_GAME_PAY_AMT			,");
			sSql.AppendLine("			SUM(YAKYUKEN_PAY_AMT			) AS YAKYUKEN_PAY_AMT				,");
			sSql.AppendLine("			SUM(PRESENT_MAIL_PAY_AMT		) AS PRESENT_MAIL_PAY_AMT			,");
			sSql.AppendLine("			SUM(TIME_CALL_PAY_AMT			) AS TIME_CALL_PAY_AMT				,");
			sSql.AppendLine("			SUM(FRIEND_INTRO_PAY_POINT		) AS FRIEND_INTRO_PAY_POINT			,");
			sSql.AppendLine("			SUM(USER_MAIL_POINT		+ OPEN_PIC_MAIL_POINT	+ OPEN_MOVIE_MAIL_POINT		+ OPEN_PIC_BBS_POINT	+ OPEN_MOVIE_BBS_POINT	+	USER_MAIL_PIC_POINT		+ USER_MAIL_MOVIE_POINT		+ PRV_MESSAGE_RX_POINT		+ PRV_PROFILE_RX_POINT	+ DOWNLOAD_MOVIE_POINT	+ DOWNLOAD_VOICE_POINT	+ SOCIAL_GAME_POINT		+ YAKYUKEN_POINT	+ PRESENT_MAIL_POINT	+ TIME_CALL_POINT	) AS WEB_TOTAL_POINT,");
			sSql.AppendLine("			SUM(USER_MAIL_COUNT		+ OPEN_PIC_MAIL_COUNT	+ OPEN_MOVIE_MAIL_COUNT		+ OPEN_PIC_BBS_COUNT	+ OPEN_MOVIE_BBS_COUNT	+	USER_MAIL_PIC_COUNT		+ USER_MAIL_MOVIE_COUNT		+ PRV_MESSAGE_RX_COUNT		+ PRV_PROFILE_RX_COUNT	+ DOWNLOAD_MOVIE_COUNT	+ DOWNLOAD_VOICE_COUNT	+ SOCIAL_GAME_COUNT		+ YAKYUKEN_COUNT	+ PRESENT_MAIL_COUNT	+ TIME_CALL_COUNT	) AS WEB_TOTAL_COUNT,");
			sSql.AppendLine("			SUM(USER_MAIL_PAY_AMT	+ OPEN_PIC_MAIL_PAY_AMT + OPEN_MOVIE_MAIL_PAY_AMT	+ OPEN_PIC_BBS_PAY_AMT	+ OPEN_MOVIE_BBS_PAY_AMT+	USER_MAIL_PIC_PAY_AMT	+ USER_MAIL_MOVIE_PAY_AMT	+ PRV_MESSAGE_RX_PAY_AMT	+ PRV_PROFILE_RX_PAY_AMT+ DOWNLOAD_MOVIE_PAY_AMT+ DOWNLOAD_VOICE_PAY_AMT+ SOCIAL_GAME_PAY_AMT	+ YAKYUKEN_PAY_AMT	+ PRESENT_MAIL_PAY_AMT	+ TIME_CALL_PAY_AMT	) AS WEB_TOTAL_PAY_AMT ");
			sSql.AppendLine("		FROM	");
			sSql.AppendLine("			VW_TIME_OPERATION02	");
			sSql.AppendLine("		WHERE	");
			sSql.AppendLine("			PAYMENT_FLAG	=  :PAYMENT_FLAG			AND	");
			sSql.AppendLine("			REPORT_DAY_TIME >= :REPORT_DAY_TIME_FROM	AND	");
			sSql.AppendLine("			REPORT_DAY_TIME <= :REPORT_DAY_TIME_TO			");
			sSql.AppendLine("		GROUP BY	");
			sSql.AppendLine("			USER_SEQ	");
			sSql.AppendLine("		ORDER BY	");
			sSql.AppendLine("			USER_SEQ) T_SUM, ");//TABLE1 TIME_OPERATION
			sSql.AppendLine("	(SELECT	");
			sSql.AppendLine("		USER_SEQ	,");
			sSql.AppendLine("		SUM(BONUS_AMT) AS BONUS_AMT ");
			sSql.AppendLine("	FROM ");
			sSql.AppendLine("		T_BONUS_LOG ");
			sSql.AppendLine("	WHERE ");
			sSql.AppendLine("		PAYMENT_FLAG	=  :PAYMENT_FLAG			AND	");
			sSql.AppendLine("		REPORT_DAY_TIME >= :REPORT_DAY_TIME_FROM	AND	");
			sSql.AppendLine("		REPORT_DAY_TIME <= :REPORT_DAY_TIME_TO			");
			sSql.AppendLine("	GROUP BY	");
			sSql.AppendLine("		USER_SEQ ");
			sSql.AppendLine("	ORDER BY	");
			sSql.AppendLine("		USER_SEQ) T_BONUS_SUM ");//TABLE2 BONUS_LOG
			sSql.AppendLine("	WHERE ");
			sSql.AppendLine("		T_CAST.USER_SEQ = T_USER.USER_SEQ			AND ");
			sSql.AppendLine("		T_USER.USER_SEQ = T_SUM.USER_SEQ		(+)	AND ");
			sSql.AppendLine("		T_USER.USER_SEQ = T_BONUS_SUM.USER_SEQ	(+)		");
			sSql.AppendLine("AND T_CAST_RANK.USER_RANK = T_CAST.USER_RANK	");

			if (!iBridUtil.GetStringValue(pLoginId).Equals("")) {
				sSql.AppendLine("AND T_USER.LOGIN_ID LIKE :LOGIN_ID ||'%'	");
			}

			if (pNotNormalCastDispFlag.Equals(ViCommConst.FLAG_OFF.ToString())) {
				sSql.AppendLine("AND T_USER.USER_STATUS = :USER_STATUS		");
			}
			List<OracleParameter> oUserStatusParamList = new List<OracleParameter>();
			if (pUserStatusMask != 0) {
				string[] sValues = new string[6];
				int iCnt = 0;
				string sWhere = string.Empty;

				if ((pUserStatusMask & ViCommConst.MASK_WOMAN_AUTH_WAIT) != 0) {
					sValues[iCnt++] = ViCommConst.USER_WOMAN_AUTH_WAIT;
				}
				if ((pUserStatusMask & ViCommConst.MASK_WOMAN_NORMAL) != 0) {
					sValues[iCnt++] = ViCommConst.USER_WOMAN_NORMAL;
				}
				if ((pUserStatusMask & ViCommConst.MASK_WOMAN_STOP) != 0) {
					sValues[iCnt++] = ViCommConst.USER_WOMAN_STOP;
				}
				if ((pUserStatusMask & ViCommConst.MASK_WOMAN_HOLD) != 0) {
					sValues[iCnt++] = ViCommConst.USER_WOMAN_HOLD;
				}
				if ((pUserStatusMask & ViCommConst.MASK_WOMAN_RESIGNED) != 0) {
					sValues[iCnt++] = ViCommConst.USER_WOMAN_RESIGNED;
				}
				if ((pUserStatusMask & ViCommConst.MASK_WOMAN_BAN) != 0) {
					sValues[iCnt++] = ViCommConst.USER_WOMAN_BAN;
				}
				sWhere = "AND (T_USER.USER_STATUS IN (";
				for (int i = 0; i < iCnt; i++) {
					sWhere = sWhere + ":USER_STATUS" + i.ToString() + ",";
					oUserStatusParamList.Add(new OracleParameter("USER_STATUS" + i.ToString(),int.Parse(sValues[i])));
				}
				sWhere = sWhere.Substring(0,sWhere.Length - 1) + ")) ";
				sSql.AppendLine(sWhere);
			}

			if (pInvalidBankCastDispFlag.Equals(ViCommConst.FLAG_OFF.ToString())) {
				sSql.AppendLine("AND T_CAST.BANK_ACCOUNT_INVALID_FLAG = :BANK_ACCOUNT_INVALID_FLAG	");
			}

			if (!iBridUtil.GetStringValue(pProductionSeq).Equals("")) {
				sSql.AppendLine("AND T_CAST.PRODUCTION_SEQ = :PRODUCTION_SEQ	");
			}

			if (!pWaitPaymentFlag.Equals("")) {
				sSql.AppendLine("AND T_CAST.WAIT_PAYMENT_FLAG = :WAIT_PAYMENT_FLAG	");
				if (!pReqPaymentDate.Equals("")) {
					sSql.AppendLine("AND T_CAST.REQ_PAYMENT_DATE < :REQ_PAYMENT_DATE	");
				}
			}

			if (pNoBankCastDispFlag.Equals(ViCommConst.FLAG_ON.ToString())) {
				sSql.AppendLine(" ) ");
			} else {
				sSql.AppendLine("AND T_CAST.BANK_NM					IS NOT NULL ");
				sSql.AppendLine("AND T_CAST.BANK_OFFICE_NM			IS NOT NULL ");
				sSql.AppendLine("AND T_CAST.BANK_OFFICE_KANA_NM		IS NOT NULL ");
				sSql.AppendLine("AND (T_CAST.BANK_ACCOUNT_TYPE		IS NOT NULL OR T_CAST.BANK_ACCOUNT_TYPE2 IS NOT NULL) ");
				sSql.AppendLine("AND T_CAST.BANK_ACCOUNT_HOLDER_NM	IS NOT NULL ");
				sSql.AppendLine("AND T_CAST.BANK_ACCOUNT_NO			IS NOT NULL ");
				sSql.AppendLine(") ");
			}
			sSql.AppendLine("	WHERE	");
			sSql.AppendLine("		(NVL(WEB_TOTAL_PAY_AMT,0) + NVL(TALK_PAY_AMT,0) + NVL(BONUS_AMT,0) + NVL(FRIEND_INTRO_PAY_AMT,0) ) >= :TOTAL_PAY_AMT	");
			sSql.AppendLine("	ORDER BY	");
			sSql.AppendLine("		REQ_PAYMENT_DATE	,");
			sSql.AppendLine("		LOGIN_ID			");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				//TIME_OPERATION
				cmd.Parameters.Add("PAYMENT_FLAG",ViCommConst.FLAG_OFF.ToString());
				cmd.Parameters.Add("REPORT_DAY_FROM",sFromDay);
				cmd.Parameters.Add("REPORT_DAY_TIME_TO",sToDay);
				//BONUS_LOG
				cmd.Parameters.Add("PAYMENT_FLAG",ViCommConst.FLAG_OFF.ToString());
				cmd.Parameters.Add("REPORT_DAY_FROM",sFromDay);
				cmd.Parameters.Add("REPORT_DAY_TIME_TO",sToDay);

				if (!iBridUtil.GetStringValue(pLoginId).Equals("")) {
					cmd.Parameters.Add("LOGIN_ID",pLoginId);
				}
				if (pNotNormalCastDispFlag.Equals(ViCommConst.FLAG_OFF.ToString())) {
					cmd.Parameters.Add("USER_STATUS",ViCommConst.USER_WOMAN_NORMAL);
				}
				if (pUserStatusMask != 0) {
					cmd.Parameters.AddRange(oUserStatusParamList.ToArray());
				}
				if (pInvalidBankCastDispFlag.Equals(ViCommConst.FLAG_OFF.ToString())) {
					cmd.Parameters.Add("BANK_ACCOUNT_INVALID_FLAG",ViCommConst.FLAG_OFF);
				}
				if (!iBridUtil.GetStringValue(pProductionSeq).Equals("")) {
					cmd.Parameters.Add("PRODUCTION_SEQ",pProductionSeq);
				}
				if (!pWaitPaymentFlag.Equals("")) {
					cmd.Parameters.Add("WAIT_PAYMENT_FLAG",pWaitPaymentFlag);
					if (!pReqPaymentDate.Equals("")) {
						DateTime dt = new DateTime();
						dt = DateTime.ParseExact(pReqPaymentDate,"yyyy/MM/dd",null);
						dt = dt.AddDays(1);
						cmd.Parameters.Add("REQ_PAYMENT_DATE",OracleDbType.Date,dt,ParameterDirection.Input);
					}
				}

				cmd.Parameters.Add("TOTAL_PAY_AMT",pTotalPayAmt);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
}
