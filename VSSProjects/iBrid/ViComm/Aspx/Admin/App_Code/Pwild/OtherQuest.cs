﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 裏クエスト設定--	Progaram ID		: OtherQuest
--
--  Creation Date	: 2012.07.06
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class OtherQuest:DbSession {
	public OtherQuest() {
	}

	public int GetPageCount(string pSiteCd,string pQuestSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT COUNT(*) FROM VW_PW_OTHER_QUEST01 WHERE SITE_CD = :SITE_CD AND QUEST_SEQ = :QUEST_SEQ");

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":QUEST_SEQ",pQuestSeq);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pQuestSeq,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	SITE_CD						,	");
		oSqlBuilder.AppendLine("	QUEST_SEQ					,	");
		oSqlBuilder.AppendLine("	OTHER_QUEST_SEQ				,	");
		oSqlBuilder.AppendLine("	OTHER_QUEST_NM				,	");
		oSqlBuilder.AppendLine("	REMARKS						,	");
		oSqlBuilder.AppendLine("	NVL(TIME_LIMIT_D,0) AS TIME_LIMIT_D		,	");
		oSqlBuilder.AppendLine("	NVL(TIME_LIMIT_H,0) AS TIME_LIMIT_H		,	");
		oSqlBuilder.AppendLine("	NVL(TIME_LIMIT_M,0) AS TIME_LIMIT_M		,	");
		oSqlBuilder.AppendLine("	CLEAR_LIMIT					,	");
		oSqlBuilder.AppendLine("	DAILY_CLEAR_LIMIT			,	");
		oSqlBuilder.AppendLine("	SEX_CD							");
		oSqlBuilder.AppendLine("FROM								");
		oSqlBuilder.AppendLine("	VW_PW_OTHER_QUEST01				");
		oSqlBuilder.AppendLine("WHERE								");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	QUEST_SEQ			= :QUEST_SEQ				");

		string sPagingSql = string.Empty;
		string sSortExpression = "ORDER BY SITE_CD, OTHER_QUEST_SEQ";

		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":QUEST_SEQ",pQuestSeq);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}
}
