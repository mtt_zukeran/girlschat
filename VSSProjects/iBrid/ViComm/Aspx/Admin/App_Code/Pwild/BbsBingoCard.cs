﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: お宝deビンゴカード
--	Progaram ID		: BbsBingoCard
--  Creation Date	: 2013.10.29
--  Creater			: K.Miyazato
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;
using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class BbsBingoCard:DbSession {
	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}

		private string termSeq;
		public string TermSeq {
			get {
				return this.termSeq;
			}
			set {
				this.termSeq = value;
			}
		}
	}

	public BbsBingoCard() {
	}

	public int GetCount(SearchCondition pSearchCondition) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_BBS_BINGO_CARD");
		oSqlBuilder.AppendLine(sWhereClause);

		return ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
	}

	public DataSet GetList(SearchCondition pSearchCondition) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SITE_CD,");
		oSqlBuilder.AppendLine("	TERM_SEQ,");
		oSqlBuilder.AppendLine("	CARD_NO,");
		oSqlBuilder.AppendLine("	HIGH_PRB_FLAG,");
		oSqlBuilder.AppendLine("	BINGO_NO_1,");
		oSqlBuilder.AppendLine("	BINGO_NO_2,");
		oSqlBuilder.AppendLine("	BINGO_NO_3,");
		oSqlBuilder.AppendLine("	BINGO_NO_4,");
		oSqlBuilder.AppendLine("	BINGO_NO_5,");
		oSqlBuilder.AppendLine("	BINGO_NO_6,");
		oSqlBuilder.AppendLine("	BINGO_NO_7,");
		oSqlBuilder.AppendLine("	BINGO_NO_8,");
		oSqlBuilder.AppendLine("	BINGO_NO_9,");
		oSqlBuilder.AppendLine("	BINGO_NO_10,");
		oSqlBuilder.AppendLine("	BINGO_NO_11,");
		oSqlBuilder.AppendLine("	BINGO_NO_12,");
		oSqlBuilder.AppendLine("	BINGO_NO_13,");
		oSqlBuilder.AppendLine("	BINGO_NO_14,");
		oSqlBuilder.AppendLine("	BINGO_NO_15,");
		oSqlBuilder.AppendLine("	BINGO_NO_16,");
		oSqlBuilder.AppendLine("	BINGO_NO_17,");
		oSqlBuilder.AppendLine("	BINGO_NO_18,");
		oSqlBuilder.AppendLine("	BINGO_NO_19,");
		oSqlBuilder.AppendLine("	BINGO_NO_20,");
		oSqlBuilder.AppendLine("	BINGO_NO_21,");
		oSqlBuilder.AppendLine("	BINGO_NO_22,");
		oSqlBuilder.AppendLine("	BINGO_NO_23,");
		oSqlBuilder.AppendLine("	BINGO_NO_24,");
		oSqlBuilder.AppendLine("	BINGO_NO_25");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_BBS_BINGO_CARD01");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("ORDER BY");
		oSqlBuilder.AppendLine("	SITE_CD,");
		oSqlBuilder.AppendLine("	TERM_SEQ,");
		oSqlBuilder.AppendLine("	CARD_NO");

		return ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.TermSeq)) {
			SysPrograms.SqlAppendWhere("TERM_SEQ = :TERM_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":TERM_SEQ",pSearchCondition.TermSeq));
		}

		return oParamList.ToArray();
	}
}
