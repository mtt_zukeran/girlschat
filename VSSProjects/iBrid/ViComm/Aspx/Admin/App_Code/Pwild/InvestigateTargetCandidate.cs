﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: この娘を探せ・候補者


--	Progaram ID		: InvestigateTargetCandidate
--  Creation Date	: 2015.02.26
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class InvestigateTargetCandidate:DbSession {

	#region SearchCondition
	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}
		
		private string loginId;
		public string LoginId {
			get {
				return this.loginId;
			}
			set {
				this.loginId = value;
			}
		}

		private string lastWantedTargetDate;
		public string LastWantedTargetDate {
			get {
				return this.lastWantedTargetDate;
			}
			set {
				this.lastWantedTargetDate = value;
			}
		}

		private string sortDirection;
		public string SortDirection {
			get {
				return this.sortDirection;
			}
			set {
				this.sortDirection = value;
			}
		}

		private string sortExpression;
		public string SortExpression {
			get {
				return this.sortExpression;
			}
			set {
				this.sortExpression = value;
			}
		}
	}
	#endregion SearchCondition
	
	public InvestigateTargetCandidate() {
	}

	public int GetPageCount(SearchCondition pSearchCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pSearchCondition,ref sWhereClause);
		
		oSqlBuilder.AppendLine("SELECT											");
		oSqlBuilder.AppendLine("	COUNT(*)									");
		oSqlBuilder.AppendLine("FROM											");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER CC						,	");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER_EX CCEX				,	");
		oSqlBuilder.AppendLine("	T_USER U								,	");
		oSqlBuilder.AppendLine("	VW_CAST_YESTERDAY_TX_MAIL01	CYTM			");
		oSqlBuilder.AppendLine(sWhereClause);

		int iPageCount = 0;
		iPageCount = ExecuteSelectCountQueryBase(oSqlBuilder.ToString(),oWhereParams);
		return iPageCount;
	}

	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT																						");
		oSqlBuilder.AppendLine("	CC.SITE_CD																			,	");
		oSqlBuilder.AppendLine("	CC.USER_SEQ																			,	");
		oSqlBuilder.AppendLine("	CC.USER_CHAR_NO																		,	");
		oSqlBuilder.AppendLine("	CC.HANDLE_NM																		,	");
		oSqlBuilder.AppendLine("	CC.AGE																				,	");
		oSqlBuilder.AppendLine("	CC.COMMENT_LIST																		,	");
		oSqlBuilder.AppendLine("	U.LOGIN_ID																			,	");
		oSqlBuilder.AppendLine("	TO_CHAR(CCEX.LAST_WANTED_TARGET_DATE,'YYYY/MM/DD') AS LAST_WANTED_TARGET_DATE		,	");
		oSqlBuilder.AppendLine("	CCEX.WANTED_TARGET_CANCEL_FLAG														,	");
		oSqlBuilder.AppendLine("	CCEX.WANTED_PICKUP_COUNT															,	");
		oSqlBuilder.AppendLine("	CYTM.MAIL_COUNT																		,	");
		oSqlBuilder.AppendLine("	GET_SMALL_PHOTO_IMG_PATH(CC.SITE_CD, U.LOGIN_ID, CC.PIC_SEQ) SMALL_PHOTO_IMG_PATH	,	");
		oSqlBuilder.AppendLine("	(																						");
		oSqlBuilder.AppendLine("		SELECT																				");
		oSqlBuilder.AppendLine("			COUNT(*) PIC_COUNT																");
		oSqlBuilder.AppendLine("		FROM																				");
		oSqlBuilder.AppendLine("			T_CAST_PIC																		");
		oSqlBuilder.AppendLine("		WHERE																				");
		oSqlBuilder.AppendLine("			T_CAST_PIC.SITE_CD			= CC.SITE_CD									AND	");
		oSqlBuilder.AppendLine("			T_CAST_PIC.USER_SEQ			= CC.USER_SEQ									AND	");
		oSqlBuilder.AppendLine("			T_CAST_PIC.USER_CHAR_NO		= CC.USER_CHAR_NO								AND	");
		oSqlBuilder.AppendLine("			T_CAST_PIC.PIC_TYPE			= '1'											AND	");
		oSqlBuilder.AppendLine("			T_CAST_PIC.OBJ_NA_FLAG		= 0													");
		oSqlBuilder.AppendLine("	) AS PIC_COUNT																			");
		oSqlBuilder.AppendLine("FROM																						");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER CC																	,	");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER_EX CCEX															,	");
		oSqlBuilder.AppendLine("	T_USER U																			,	");
		oSqlBuilder.AppendLine("	VW_CAST_YESTERDAY_TX_MAIL01	CYTM														");
		oSqlBuilder.AppendLine(sWhereClause);

		sSortExpression = GetOrder(pSearchCondition);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
		
		using (CastAttrTypeValue oCastAttrTypeValue = new CastAttrTypeValue()) {
			oCastAttrTypeValue.AppendCastAttr(oDataSet);
		}
		
		return oDataSet;
	}

	private string GetOrder(SearchCondition pSearchCondition) {
		if (string.IsNullOrEmpty(pSearchCondition.SortExpression)) {
			return "ORDER BY CYTM.MAIL_COUNT DESC, U.USER_SEQ ASC";
		} else {
			return string.Format("ORDER BY {0} {1}, U.USER_SEQ ASC",pSearchCondition.SortExpression,pSearchCondition.SortDirection);
		}
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhere) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("CC.SITE_CD = CCEX.SITE_CD",ref pWhere);
		SysPrograms.SqlAppendWhere("CC.USER_SEQ = CCEX.USER_SEQ",ref pWhere);
		SysPrograms.SqlAppendWhere("CC.USER_CHAR_NO = CCEX.USER_CHAR_NO",ref pWhere);
		SysPrograms.SqlAppendWhere("CC.SITE_CD = CYTM.SITE_CD",ref pWhere);
		SysPrograms.SqlAppendWhere("CC.USER_SEQ = CYTM.USER_SEQ",ref pWhere);
		SysPrograms.SqlAppendWhere("CC.USER_CHAR_NO = CYTM.USER_CHAR_NO",ref pWhere);
		SysPrograms.SqlAppendWhere("CC.USER_SEQ = U.USER_SEQ",ref pWhere);

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(":SITE_CD = CC.SITE_CD",ref pWhere);
			oParamList.Add(new OracleParameter("SITE_CD",pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.LoginId)) {
			SysPrograms.SqlAppendWhere(":LOGIN_ID = U.LOGIN_ID",ref pWhere);
			oParamList.Add(new OracleParameter("LOGIN_ID",pSearchCondition.LoginId));
		}

		SysPrograms.SqlAppendWhere("CC.PIC_SEQ IS NOT NULL",ref pWhere);

		if (!string.IsNullOrEmpty(pSearchCondition.LastWantedTargetDate)) {
			SysPrograms.SqlAppendWhere("((CCEX.LAST_WANTED_TARGET_DATE <= :LAST_WANTED_TARGET_DATE AND CCEX.WANTED_TARGET_CANCEL_FLAG = :WANTED_TARGET_CANCEL_FLAG_OFF) OR CCEX.WANTED_TARGET_CANCEL_FLAG = :WANTED_TARGET_CANCEL_FLAG_ON OR CCEX.LAST_WANTED_TARGET_DATE IS NULL)",ref pWhere);
			oParamList.Add(new OracleParameter(":LAST_WANTED_TARGET_DATE",pSearchCondition.LastWantedTargetDate));
			oParamList.Add(new OracleParameter(":WANTED_TARGET_CANCEL_FLAG_OFF",ViCommConst.FLAG_OFF_STR));
			oParamList.Add(new OracleParameter(":WANTED_TARGET_CANCEL_FLAG_ON",ViCommConst.FLAG_ON_STR));
		}

		SysPrograms.SqlAppendWhere(":WANTED_TARGET_REFUSE_FLAG = CCEX.WANTED_TARGET_REFUSE_FLAG",ref pWhere);
		oParamList.Add(new OracleParameter("WANTED_TARGET_REFUSE_FLAG",ViCommConst.FLAG_OFF_STR));

		return oParamList.ToArray();
	}
}
