﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 女性お宝ｺﾝﾌﾟ人数レポート
--	Progaram ID		: CastTreasureCompleteTotalReport
--
--  Creation Date	: 2012.12.01
--  Creater			: M&TT A.Taba
--
**************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class CastTreasureCompleteTotalReport:DbSession {

	public CastTreasureCompleteTotalReport() {
	}

	public DataSet GetList(string pSiteCd,string pReportDayFrom,string pReportDayTo) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	STAGE_GROUP_TYPE	,				");
		oSqlBuilder.AppendLine("	STAGE_GROUP_NM					    ");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	T_STAGE_GROUP						");
		oSqlBuilder.AppendLine("WHERE									");
		oSqlBuilder.AppendLine("	SITE_CD		= :SITE_CD		AND		");
		oSqlBuilder.AppendLine("	SEX_CD		= 3						");
		oSqlBuilder.AppendLine("ORDER BY TO_NUMBER(STAGE_GROUP_TYPE)	");
		
		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}

		AppendCompCount(oDs,pSiteCd,pReportDayFrom,pReportDayTo);
		
		return oDs;
	}

	private void AppendCompCount(DataSet pDS,string pSiteCd,string pReportDayFrom,string pReportDayTo) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}
		
		DataColumn col;

		col = new DataColumn(string.Format("COMP_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			StringBuilder oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine("SELECT											 ");
			oSqlBuilder.AppendLine("	COUNT(*) AS COMP_COUNT						 ");
			oSqlBuilder.AppendLine(" FROM							   				 ");
			oSqlBuilder.AppendLine("	T_CAST_TREASURE_COMPLETE_LOG  				 ");
			oSqlBuilder.AppendLine(" WHERE											 ");
			oSqlBuilder.AppendLine("	SITE_CD			 = :SITE_CD			    AND	 ");
			oSqlBuilder.AppendLine("	STAGE_GROUP_TYPE = :STAGE_GROUP_TYPE	AND	 ");
			oSqlBuilder.AppendLine("	COMPLETE_DATE	 >=:COMPLETE_DATE_FROM	AND	 ");
			oSqlBuilder.AppendLine("	COMPLETE_DATE    <=:COMPLETE_DATE_TO		 ");

			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
					cmd.BindByName = true;

					cmd.Parameters.Add(":SITE_CD",pSiteCd);
					cmd.Parameters.Add(":STAGE_GROUP_TYPE",dr["STAGE_GROUP_TYPE"].ToString());
					cmd.Parameters.Add(":COMPLETE_DATE_FROM",pReportDayFrom);
					cmd.Parameters.Add(":COMPLETE_DATE_TO",pReportDayTo);
				
					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub);
					}

					dr["COMP_COUNT"] = dsSub.Tables[0].Rows[0]["COMP_COUNT"];
				}
			}
		}
	}
}
