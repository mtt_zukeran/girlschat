﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 野球拳画像
--	Progaram ID		: YakyukenPic
--  Creation Date	: 2013.05.02
--  Creater			: K.Miyazato
--
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;
using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class YakyukenPic:DbSession {

	public string siteCd;
	public string castUserSeq;
	public string castCharNo;
	public string castNm;
	public string handleNm;
	public string loginId;
	public string yakyukenPicSeq;
	public string yakyukenType;
	public string commentText;
	public string objPhotoImgPath;
	public string objSmallPhotoImgPath;
	public string picSeq;

	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}
		private string loginId;
		public string LoginId {
			get {
				return this.loginId;
			}
			set {
				this.loginId = value;
			}
		}
		private string sortDirection;
		public string SortDirection {
			get {
				return this.sortDirection;
			}
			set {
				this.sortDirection = value;
			}
		}
		private string sortExpression;
		public string SortExpression {
			get {
				return this.sortExpression;
			}
			set {
				this.sortExpression = value;
			}
		}
	}

	public YakyukenPic() {
	}

	public int GetPageCount(SearchCondition pSearchCondition) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_YAKYUKEN_PIC01");
		oSqlBuilder.AppendLine(sWhereClause);

		return ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
	}

	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));
		sSortExpression = this.GetOrder(pSearchCondition);

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SITE_CD,");
		oSqlBuilder.AppendLine("	CAST_USER_SEQ,");
		oSqlBuilder.AppendLine("	CAST_CHAR_NO,");
		oSqlBuilder.AppendLine("	YAKYUKEN_PIC_SEQ,");
		oSqlBuilder.AppendLine("	YAKYUKEN_TYPE,");
		oSqlBuilder.AppendLine("	COMMENT_TEXT,");
		oSqlBuilder.AppendLine("	PIC_SEQ,");
		oSqlBuilder.AppendLine("	AUTH_FLAG,");
		oSqlBuilder.AppendLine("	SHOW_FACE_FLAG,");
		oSqlBuilder.AppendLine("	CREATE_DATE,");
		oSqlBuilder.AppendLine("	REVISION_NO,");
		oSqlBuilder.AppendLine("	VIEW_COUNT,");
		oSqlBuilder.AppendLine("	PHOTO_IMG_PATH,");
		oSqlBuilder.AppendLine("	SMALL_PHOTO_IMG_PATH,");
		oSqlBuilder.AppendLine("	LOGIN_ID,");
		oSqlBuilder.AppendLine("	HANDLE_NM");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_YAKYUKEN_PIC01");
		oSqlBuilder.AppendLine(sWhereClause);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		return ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.LoginId)) {
			SysPrograms.SqlAppendWhere("LOGIN_ID = :LOGIN_ID",ref pWhereClause);
			oParamList.Add(new OracleParameter(":LOGIN_ID",pSearchCondition.LoginId));
		}

		return oParamList.ToArray();
	}

	private string GetOrder(SearchCondition pSearchCondition) {
		if (string.IsNullOrEmpty(pSearchCondition.SortExpression)) {
			return "ORDER BY CREATE_DATE DESC";
		} else {
			return string.Format("ORDER BY {0} {1},CREATE_DATE DESC",pSearchCondition.SortExpression,pSearchCondition.SortDirection);
		}
	}

	public DataSet GetList(string pSiteCd,string pUserSeq,string pUserCharNo) {
		DataSet ds = new DataSet();
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	PIC_SEQ,");
		oSqlBuilder.AppendLine("	COMMENT_TEXT,");
		oSqlBuilder.AppendLine("	CREATE_DATE,");
		oSqlBuilder.AppendLine("	AUTH_FLAG,");
		oSqlBuilder.AppendLine("	YAKYUKEN_TYPE,");
		oSqlBuilder.AppendLine("	PHOTO_IMG_PATH,");
		oSqlBuilder.AppendLine("	SMALL_PHOTO_IMG_PATH");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_YAKYUKEN_PIC01");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD				AND ");
		oSqlBuilder.AppendLine("	CAST_USER_SEQ	= :CAST_USER_SEQ		AND ");
		oSqlBuilder.AppendLine("	CAST_CHAR_NO	= :CAST_CHAR_NO				");
		oSqlBuilder.AppendLine("ORDER BY SITE_CD,CAST_USER_SEQ,CAST_CHAR_NO,CREATE_DATE DESC,PIC_SEQ DESC ");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pUserSeq));
		oParamList.Add(new OracleParameter(":CAST_CHAR_NO",pUserCharNo));

		ds = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		return ds;
	}
	public bool GetOne(string pPicSeq) {
		DataSet ds;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	SITE_CD						,	");
		oSqlBuilder.AppendLine("	CAST_USER_SEQ				,	");
		oSqlBuilder.AppendLine("	CAST_CHAR_NO				,	");
		oSqlBuilder.AppendLine("	CAST_NM						,	");
		oSqlBuilder.AppendLine("	HANDLE_NM					,	");
		oSqlBuilder.AppendLine("	LOGIN_ID					,	");
		oSqlBuilder.AppendLine("	YAKYUKEN_PIC_SEQ			,	");
		oSqlBuilder.AppendLine("	YAKYUKEN_TYPE				,	");
		oSqlBuilder.AppendLine("	COMMENT_TEXT				,	");
		oSqlBuilder.AppendLine("	PHOTO_IMG_PATH				,	");
		oSqlBuilder.AppendLine("	SMALL_PHOTO_IMG_PATH		,	");
		oSqlBuilder.AppendLine("	PIC_SEQ							");
		oSqlBuilder.AppendLine("FROM								");
		oSqlBuilder.AppendLine("	VW_YAKYUKEN_PIC01				");
		oSqlBuilder.AppendLine("WHERE								");
		oSqlBuilder.AppendLine("	PIC_SEQ	= :PIC_SEQ				");
		
		oParamList.Add(new OracleParameter(":PIC_SEQ",pPicSeq));

		ds = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if (ds.Tables[0].Rows.Count > 0) {
			siteCd = ds.Tables[0].Rows[0]["SITE_CD"].ToString();
			castUserSeq = ds.Tables[0].Rows[0]["CAST_USER_SEQ"].ToString();
			castCharNo = ds.Tables[0].Rows[0]["CAST_CHAR_NO"].ToString();
			castNm = ds.Tables[0].Rows[0]["CAST_NM"].ToString();
			handleNm = ds.Tables[0].Rows[0]["HANDLE_NM"].ToString();
			loginId = ds.Tables[0].Rows[0]["LOGIN_ID"].ToString();
			yakyukenPicSeq = ds.Tables[0].Rows[0]["YAKYUKEN_PIC_SEQ"].ToString();
			yakyukenType = ds.Tables[0].Rows[0]["YAKYUKEN_TYPE"].ToString();
			commentText = ds.Tables[0].Rows[0]["COMMENT_TEXT"].ToString();
			objPhotoImgPath = ds.Tables[0].Rows[0]["PHOTO_IMG_PATH"].ToString();
			objSmallPhotoImgPath = ds.Tables[0].Rows[0]["SMALL_PHOTO_IMG_PATH"].ToString();
			picSeq = ds.Tables[0].Rows[0]["PIC_SEQ"].ToString();
					return true;
		} else {
			return false;
		}
	}

	public int GetUnconfirmCount(string pSiteCd) {
		int iValue;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_YAKYUKEN_PIC");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD		= :SITE_CD	AND	");
		oSqlBuilder.AppendLine("	AUTH_FLAG	= :AUTH_FLAG");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":AUTH_FLAG",ViCommConst.FLAG_OFF));

		iValue = ExecuteSelectCountQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return iValue;
	}
}
