﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 会員広告集計(月別)
--	Progaram ID		: AdDataUserMonthly
--  Creation Date	: 2014.06.13
--  Creater			: Y.Ikemiya
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;
using iBridCommLib;
using ViComm;
using System.Collections.Generic;
using ViComm.Extension.Pwild;

public class AdDataUserMonthly:DbSession {
	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}
		
		private string adCd;
		public string AdCd {
			get {
				return this.adCd;
			}
			set {
				this.adCd = value;
			}
		}

		private string reportMonth;
		public string ReportMonth {
			get {
				return this.reportMonth;
			}
			set {
				this.reportMonth = value;
			}
		}

		private string reportMonthFrom;
		public string ReportMonthFrom {
			get {
				return this.reportMonthFrom;
			}
			set {
				this.reportMonthFrom = value;
			}
		}

		private string reportMonthTo;
		public string ReportMonthTo {
			get {
				return this.reportMonthTo;
			}
			set {
				this.reportMonthTo = value;
			}
		}
		
		private string usedFlag;
		public string UsedFlag {
			get {
				return this.usedFlag;
			}
			set {
				this.usedFlag = value;
			}
		}
		
		private string adGroupCd;
		public string AdGroupCd {
			get {
				return this.adGroupCd;
			}
			set {
				this.adGroupCd = value;
			}
		}
		
		private string noAdGroupFlag;
		public string NoAdGroupFlag {
			get {
				return this.noAdGroupFlag;
			}
			set {
				this.noAdGroupFlag = value;
			}
		}

		private string sortDirection;
		public string SortDirection {
			get {
				return this.sortDirection;
			}
			set {
				this.sortDirection = value;
			}
		}

		private string sortExpression;
		public string SortExpression {
			get {
				return this.sortExpression;
			}
			set {
				this.sortExpression = value;
			}
		}
	}

	public AdDataUserMonthly() {
	}

	public int GetPageCount(SearchCondition pSearchCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();

		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere((SearchCondition)pSearchCondition,ref sWhereClause);

		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	COUNT(*)							");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	T_AD_DATA_USER_MONTHLY P		,	");
		oSqlBuilder.AppendLine("	T_AD A							,	");
		oSqlBuilder.AppendLine("	T_CODE_DTL C					,	");
		oSqlBuilder.AppendLine("	T_SITE_AD_GROUP SA					");
		oSqlBuilder.AppendLine(sWhereClause);

		int iPageCount = 0;

		iPageCount = ExecuteSelectCountQueryBase(oSqlBuilder.ToString(),oWhereParams);
		return iPageCount;
	}

	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));
		sSortExpression = this.GetOrder(pSearchCondition);

		oSqlBuilder.AppendLine("SELECT																										");
		oSqlBuilder.AppendLine("	P.SITE_CD																							,	");
		oSqlBuilder.AppendLine("	P.AD_CD																								,	");
		oSqlBuilder.AppendLine("	P.AD_COST																							,	");
		oSqlBuilder.AppendLine("	P.ACCESS_COUNT																						,	");
		oSqlBuilder.AppendLine("	P.REGIST_COUNT																						,	");
		oSqlBuilder.AppendLine("	P.REGIST_RATE																						,	");
		oSqlBuilder.AppendLine("	P.REGIST_COUNT_AD																					,	");
		oSqlBuilder.AppendLine("	P.LOGIN_COUNT																						,	");
		oSqlBuilder.AppendLine("	P.CHARGE_POINT_USER_COUNT																			,	");
		oSqlBuilder.AppendLine("	P.CHARGE_POINT_USER_RATE																			,	");
		oSqlBuilder.AppendLine("	P.START_RECEIPT_USER_COUNT																			,	");
		oSqlBuilder.AppendLine("	P.START_RECEIPT_USER_RATE																			,	");
		oSqlBuilder.AppendLine("	P.AD_COST_BY_REGIST																					,	");
		oSqlBuilder.AppendLine("	P.AD_COST_BY_START_RECEIPT																			,	");
		oSqlBuilder.AppendLine("	P.ENT_MONTH_RECEIPT_COUNT																			,	");
		oSqlBuilder.AppendLine("	P.ENT_MONTH_RECEIPT_AMT																				,	");
		oSqlBuilder.AppendLine("	P.ENT_MONTH_RETURN_RATE																				,	");
		oSqlBuilder.AppendLine("	P.REPEAT_RECEIPT_COUNT																				,	");
		oSqlBuilder.AppendLine("	P.REPEAT_RECEIPT_AMT																				,	");
		oSqlBuilder.AppendLine("	P.TOTAL_RECEIPT_COUNT																				,	");
		oSqlBuilder.AppendLine("	P.TOTAL_RECEIPT_AMT																					,	");
		oSqlBuilder.AppendLine("	P.REPEAT_RATE																						,	");
		oSqlBuilder.AppendLine("	P.AGGR_AD_COST_RATE																					,	");
		oSqlBuilder.AppendLine("	P.AGGR_ACCESS_COUNT																					,	");
		oSqlBuilder.AppendLine("	P.AGGR_REGIST_COUNT																					,	");
		oSqlBuilder.AppendLine("	P.AGGR_REGIST_RATE																					,	");
		oSqlBuilder.AppendLine("	P.AGGR_CHARGE_POINT_USER																			,	");
		oSqlBuilder.AppendLine("	P.AGGR_AD_COST_BY_REGIST																			,	");
		oSqlBuilder.AppendLine("	P.AGGR_REGIST_COUNT_AD																				,	");
		oSqlBuilder.AppendLine("	P.AGGR_CHARGE_POINT_USER_RATE																		,	");
		oSqlBuilder.AppendLine("	P.AGGR_RECEIPT_RATE																					,	");
		oSqlBuilder.AppendLine("	P.AGGR_AD_COST_BY_RECEIPT_USER																		,	");
		oSqlBuilder.AppendLine("	P.AGGR_RECEIPT_BY_USER																				,	");
		oSqlBuilder.AppendLine("	P.AGGR_REPEAT_RECEIPT_USER																			,	");
		oSqlBuilder.AppendLine("	P.AGGR_REPEAT_RATE																					,	");
		oSqlBuilder.AppendLine("	P.AGGR_RETURN_RATE																					,	");
		oSqlBuilder.AppendLine("	P.AGGR_RECEIPT_AMT																					,	");
		oSqlBuilder.AppendLine("	P.AGGR_RECEIPT_USER_COUNT																			,	");
		oSqlBuilder.AppendLine("	P.AGGR_AD_COST																						,	");
		oSqlBuilder.AppendLine("	P.AD_NM																								,	");
		oSqlBuilder.AppendLine("	P.PUBLISH_START_DATE																				,	");
		oSqlBuilder.AppendLine("	P.PUBLISH_END_DATE																					,	");
		oSqlBuilder.AppendLine("	P.CODE_NM AS AD_MENU_NM																				,	");
		oSqlBuilder.AppendLine("	AG.AD_GROUP_NM																							");
		oSqlBuilder.AppendLine("FROM																										");
		oSqlBuilder.AppendLine("	(																										");
		oSqlBuilder.AppendLine("		SELECT																								");
		oSqlBuilder.AppendLine("			P.SITE_CD																					,	");
		oSqlBuilder.AppendLine("			P.AD_CD																						,	");
		oSqlBuilder.AppendLine("			P.AD_COST																					,	");
		oSqlBuilder.AppendLine("			P.ACCESS_COUNT																				,	");
		oSqlBuilder.AppendLine("			P.REGIST_COUNT																				,	");
		oSqlBuilder.AppendLine("			CASE																							");
		oSqlBuilder.AppendLine("				WHEN P.ACCESS_COUNT > 0																		");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(P.REGIST_COUNT / P.ACCESS_COUNT * 100,2))								");
		oSqlBuilder.AppendLine("				ELSE '-'																					");
		oSqlBuilder.AppendLine("			END AS REGIST_RATE																			,	");
		oSqlBuilder.AppendLine("			P.REGIST_COUNT_AD																			,	");
		oSqlBuilder.AppendLine("			P.LOGIN_COUNT																				,	");
		oSqlBuilder.AppendLine("			P.CHARGE_POINT_USER_COUNT																	,	");
		oSqlBuilder.AppendLine("			CASE																							");
		oSqlBuilder.AppendLine("				WHEN P.REGIST_COUNT > 0																		");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(P.CHARGE_POINT_USER_COUNT / P.REGIST_COUNT * 100,2))						");
		oSqlBuilder.AppendLine("				ELSE '-'																					");
		oSqlBuilder.AppendLine("			END AS CHARGE_POINT_USER_RATE																,	");
		oSqlBuilder.AppendLine("			P.START_RECEIPT_USER_COUNT																	,	");
		oSqlBuilder.AppendLine("			CASE																							");
		oSqlBuilder.AppendLine("				WHEN P.REGIST_COUNT > 0																		");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(P.START_RECEIPT_USER_COUNT / P.REGIST_COUNT * 100,2))					");
		oSqlBuilder.AppendLine("				ELSE '-'																					");
		oSqlBuilder.AppendLine("			END AS START_RECEIPT_USER_RATE																,	");
		oSqlBuilder.AppendLine("			CASE																							");
		oSqlBuilder.AppendLine("				WHEN P.REGIST_COUNT > 0																		");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(P.AD_COST / P.REGIST_COUNT,2))											");
		oSqlBuilder.AppendLine("				ELSE '-'																					");
		oSqlBuilder.AppendLine("			END AS AD_COST_BY_REGIST																	,	");
		oSqlBuilder.AppendLine("			CASE																							");
		oSqlBuilder.AppendLine("				WHEN P.START_RECEIPT_USER_COUNT > 0															");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(P.AD_COST / P.START_RECEIPT_USER_COUNT,2))								");
		oSqlBuilder.AppendLine("				ELSE '-'																					");
		oSqlBuilder.AppendLine("			END AS AD_COST_BY_START_RECEIPT																,	");
		oSqlBuilder.AppendLine("			P.ENT_MONTH_RECEIPT_COUNT																	,	");
		oSqlBuilder.AppendLine("			P.ENT_MONTH_RECEIPT_AMT																		,	");
		oSqlBuilder.AppendLine("			CASE																							");
		oSqlBuilder.AppendLine("				WHEN P.AD_COST > 0																			");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(P.ENT_MONTH_RECEIPT_AMT / P.AD_COST * 100,2))							");
		oSqlBuilder.AppendLine("				ELSE '-'																					");
		oSqlBuilder.AppendLine("			END AS ENT_MONTH_RETURN_RATE																,	");
		oSqlBuilder.AppendLine("			P.REPEAT_RECEIPT_COUNT																		,	");
		oSqlBuilder.AppendLine("			P.REPEAT_RECEIPT_AMT																		,	");
		oSqlBuilder.AppendLine("			P.ENT_MONTH_RECEIPT_COUNT + P.REPEAT_RECEIPT_COUNT AS TOTAL_RECEIPT_COUNT					,	");
		oSqlBuilder.AppendLine("			P.ENT_MONTH_RECEIPT_AMT + P.REPEAT_RECEIPT_AMT AS TOTAL_RECEIPT_AMT							,	");
		oSqlBuilder.AppendLine("			CASE																							");
		oSqlBuilder.AppendLine("				WHEN P.AGGR_RECEIPT_USER_COUNT > 0															");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(P.REPEAT_RECEIPT_COUNT / P.AGGR_RECEIPT_USER_COUNT * 100,2))				");
		oSqlBuilder.AppendLine("				ELSE '-'																					");
		oSqlBuilder.AppendLine("			END AS REPEAT_RATE																			,	");
		oSqlBuilder.AppendLine("			CASE																							");
		oSqlBuilder.AppendLine("				WHEN P.AGGR_RECEIPT_AMT > 0																	");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(P.AGGR_AD_COST / P.AGGR_RECEIPT_AMT * 100,2))							");
		oSqlBuilder.AppendLine("				ELSE '-'																					");
		oSqlBuilder.AppendLine("			END AS AGGR_AD_COST_RATE																	,	");
		oSqlBuilder.AppendLine("			P.AGGR_ACCESS_COUNT																			,	");
		oSqlBuilder.AppendLine("			P.AGGR_REGIST_COUNT																			,	");
		oSqlBuilder.AppendLine("			CASE																							");
		oSqlBuilder.AppendLine("				WHEN P.AGGR_ACCESS_COUNT > 0																");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(P.AGGR_REGIST_COUNT / P.AGGR_ACCESS_COUNT * 100,2))						");
		oSqlBuilder.AppendLine("				ELSE '-'																					");
		oSqlBuilder.AppendLine("			END AS AGGR_REGIST_RATE																		,	");
		oSqlBuilder.AppendLine("			P.AGGR_CHARGE_POINT_USER																	,	");
		oSqlBuilder.AppendLine("			CASE																							");
		oSqlBuilder.AppendLine("				WHEN P.AGGR_REGIST_COUNT > 0																");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(P.AGGR_AD_COST / P.AGGR_REGIST_COUNT,2))									");
		oSqlBuilder.AppendLine("				ELSE '-'																					");
		oSqlBuilder.AppendLine("			END AS AGGR_AD_COST_BY_REGIST																,	");
		oSqlBuilder.AppendLine("			P.AGGR_REGIST_COUNT_AD																		,	");
		oSqlBuilder.AppendLine("			CASE																							");
		oSqlBuilder.AppendLine("				WHEN P.AGGR_REGIST_COUNT > 0																");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(P.AGGR_CHARGE_POINT_USER / P.AGGR_REGIST_COUNT * 100,2))					");
		oSqlBuilder.AppendLine("				ELSE '-'																					");
		oSqlBuilder.AppendLine("			END AS AGGR_CHARGE_POINT_USER_RATE															,	");
		oSqlBuilder.AppendLine("			CASE																							");
		oSqlBuilder.AppendLine("				WHEN P.AGGR_REGIST_COUNT > 0																");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(P.AGGR_RECEIPT_USER_COUNT / P.AGGR_REGIST_COUNT * 100,2))				");
		oSqlBuilder.AppendLine("				ELSE '-'																					");
		oSqlBuilder.AppendLine("			END AS AGGR_RECEIPT_RATE																	,	");
		oSqlBuilder.AppendLine("			CASE																							");
		oSqlBuilder.AppendLine("				WHEN P.AGGR_RECEIPT_USER_COUNT > 0															");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(P.AGGR_AD_COST / P.AGGR_RECEIPT_USER_COUNT,2))							");
		oSqlBuilder.AppendLine("				ELSE '-'																					");
		oSqlBuilder.AppendLine("			END AS AGGR_AD_COST_BY_RECEIPT_USER															,	");
		oSqlBuilder.AppendLine("			CASE																							");
		oSqlBuilder.AppendLine("				WHEN P.AGGR_RECEIPT_USER_COUNT > 0															");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(P.AGGR_RECEIPT_AMT / P.AGGR_RECEIPT_USER_COUNT,2))						");
		oSqlBuilder.AppendLine("				ELSE '-'																					");
		oSqlBuilder.AppendLine("			END AS AGGR_RECEIPT_BY_USER																	,	");
		oSqlBuilder.AppendLine("			P.AGGR_REPEAT_RECEIPT_USER																	,	");
		oSqlBuilder.AppendLine("			CASE																							");
		oSqlBuilder.AppendLine("				WHEN P.AGGR_RECEIPT_USER_COUNT > 0															");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(P.REPEAT_RECEIPT_COUNT / P.AGGR_RECEIPT_USER_COUNT * 100,2))				");
		oSqlBuilder.AppendLine("				ELSE '-'																					");
		oSqlBuilder.AppendLine("			END AS AGGR_REPEAT_RATE																		,	");
		oSqlBuilder.AppendLine("			CASE																							");
		oSqlBuilder.AppendLine("				WHEN P.AGGR_AD_COST > 0																		");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(P.AGGR_RECEIPT_AMT / P.AGGR_AD_COST * 100,2))							");
		oSqlBuilder.AppendLine("			END AS AGGR_RETURN_RATE																		,	");
		oSqlBuilder.AppendLine("			P.AGGR_RECEIPT_AMT																			,	");
		oSqlBuilder.AppendLine("			P.AGGR_AD_COST																				,	");
		oSqlBuilder.AppendLine("			P.AGGR_RECEIPT_USER_COUNT																	,	");
		oSqlBuilder.AppendLine("			A.AD_NM																						,	");
		oSqlBuilder.AppendLine("			A.PUBLISH_START_DATE																		,	");
		oSqlBuilder.AppendLine("			A.PUBLISH_END_DATE																			,	");
		oSqlBuilder.AppendLine("			C.CODE_NM																					,	");
		oSqlBuilder.AppendLine("			SA.AD_GROUP_CD																					");
		oSqlBuilder.AppendLine("		FROM																								");
		oSqlBuilder.AppendLine("			T_AD_DATA_USER_MONTHLY P																	,	");
		oSqlBuilder.AppendLine("			T_AD A																						,	");
		oSqlBuilder.AppendLine("			T_CODE_DTL C																				,	");
		oSqlBuilder.AppendLine("			T_SITE_AD_GROUP SA																				");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("	) P																									,	");
		oSqlBuilder.AppendLine("	T_AD_GROUP AG																							");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	P.AD_GROUP_CD	= AG.AD_GROUP_CD																(+)		");

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("P.AD_CD = A.AD_CD",ref pWhereClause);
		SysPrograms.SqlAppendWhere("P.SITE_CD = SA.SITE_CD (+)",ref pWhereClause);
		SysPrograms.SqlAppendWhere("P.AD_CD = SA.AD_CD (+)",ref pWhereClause);
		SysPrograms.SqlAppendWhere("A.AD_MENU = C.CODE",ref pWhereClause);
		SysPrograms.SqlAppendWhere("C.CODE_TYPE = :CODE_TYPE",ref pWhereClause);
		oParamList.Add(new OracleParameter(":CODE_TYPE",ViCommConst.CODE_TYPE_AD_MENU));

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pSearchCondition.SiteCd));
		}
		
		if (!string.IsNullOrEmpty(pSearchCondition.AdCd)) {
			SysPrograms.SqlAppendWhere("P.AD_CD = :AD_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":AD_CD",pSearchCondition.AdCd));
		}
		
		if (!string.IsNullOrEmpty(pSearchCondition.ReportMonth)) {
			SysPrograms.SqlAppendWhere("P.REPORT_MONTH = :REPORT_MONTH",ref pWhereClause);
			oParamList.Add(new OracleParameter(":REPORT_MONTH",pSearchCondition.ReportMonth));
		} else {
			if (!string.IsNullOrEmpty(pSearchCondition.ReportMonthFrom)) {
				SysPrograms.SqlAppendWhere("P.REPORT_MONTH >= :REPORT_MONTH_FROM",ref pWhereClause);
				oParamList.Add(new OracleParameter(":REPORT_MONTH_FROM",pSearchCondition.ReportMonthFrom));
			}

			if (!string.IsNullOrEmpty(pSearchCondition.ReportMonthTo)) {
				SysPrograms.SqlAppendWhere("P.REPORT_MONTH = :REPORT_MONTH_TO",ref pWhereClause);
				oParamList.Add(new OracleParameter(":REPORT_MONTH_TO",pSearchCondition.ReportMonthTo));
			}
		}
		
		if (!string.IsNullOrEmpty(pSearchCondition.UsedFlag)) {
			SysPrograms.SqlAppendWhere("A.USED_FLAG = :USED_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USED_FLAG",pSearchCondition.UsedFlag));
		}
		
		if (!string.IsNullOrEmpty(pSearchCondition.AdGroupCd)) {
			SysPrograms.SqlAppendWhere("EXISTS(SELECT * FROM T_SITE_AD_GROUP WHERE SITE_CD = P.SITE_CD AND AD_CD = P.AD_CD AND AD_GROUP_CD = :AD_GROUP_CD)",ref pWhereClause);
			oParamList.Add(new OracleParameter(":AD_GROUP_CD",pSearchCondition.AdGroupCd));
		}
		
		if (!string.IsNullOrEmpty(pSearchCondition.NoAdGroupFlag)) {
			SysPrograms.SqlAppendWhere("NOT EXISTS(SELECT * FROM T_SITE_AD_GROUP WHERE SITE_CD = P.SITE_CD AND AD_CD = P.AD_CD)",ref pWhereClause);
		}

		return oParamList.ToArray();
	}

	private string GetOrder(SearchCondition pSearchCondition) {
		if (string.IsNullOrEmpty(pSearchCondition.SortExpression)) {
			return "ORDER BY AD_COST DESC";
		} else {
			return string.Format("ORDER BY {0} {1}",pSearchCondition.SortExpression,pSearchCondition.SortDirection);
		}
	}

	public int GetPageCountByAdGroup(SearchCondition pSearchCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();

		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhereByAdGroup((SearchCondition)pSearchCondition,ref sWhereClause);

		oSqlBuilder.AppendLine("SELECT																");
		oSqlBuilder.AppendLine("	COUNT(*)														");
		oSqlBuilder.AppendLine("FROM																");
		oSqlBuilder.AppendLine("	(																");
		oSqlBuilder.AppendLine("		SELECT														");
		oSqlBuilder.AppendLine("			P.SITE_CD											,	");
		oSqlBuilder.AppendLine("			SA.AD_GROUP_CD			,								");
		oSqlBuilder.AppendLine("			AG.AD_GROUP_NM											");
		oSqlBuilder.AppendLine("		FROM														");
		oSqlBuilder.AppendLine("			T_AD_DATA_USER_MONTHLY P							,	");
		oSqlBuilder.AppendLine("			T_SITE_AD_GROUP SA									,	");
		oSqlBuilder.AppendLine("			T_AD_GROUP AG											");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("		GROUP BY P.SITE_CD,SA.AD_GROUP_CD,AG.AD_GROUP_NM			");
		oSqlBuilder.AppendLine("	) P																");

		int iPageCount = 0;

		iPageCount = ExecuteSelectCountQueryBase(oSqlBuilder.ToString(),oWhereParams);
		return iPageCount;
	}

	public DataSet GetPageCollectionByAdGroup(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhereByAdGroup(pSearchCondition,ref sWhereClause));
		sSortExpression = this.GetOrder(pSearchCondition);

		oSqlBuilder.AppendLine("SELECT																												");
		oSqlBuilder.AppendLine("	P.SITE_CD																									,	");
		oSqlBuilder.AppendLine("	P.AD_COST																									,	");
		oSqlBuilder.AppendLine("	P.ACCESS_COUNT																								,	");
		oSqlBuilder.AppendLine("	P.REGIST_COUNT																								,	");
		oSqlBuilder.AppendLine("	P.REGIST_RATE																								,	");
		oSqlBuilder.AppendLine("	P.REGIST_COUNT_AD																							,	");
		oSqlBuilder.AppendLine("	P.LOGIN_COUNT																								,	");
		oSqlBuilder.AppendLine("	P.CHARGE_POINT_USER_COUNT																					,	");
		oSqlBuilder.AppendLine("	P.CHARGE_POINT_USER_RATE																					,	");
		oSqlBuilder.AppendLine("	P.START_RECEIPT_USER_COUNT																					,	");
		oSqlBuilder.AppendLine("	P.START_RECEIPT_USER_RATE																					,	");
		oSqlBuilder.AppendLine("	P.AD_COST_BY_REGIST																							,	");
		oSqlBuilder.AppendLine("	P.AD_COST_BY_START_RECEIPT																					,	");
		oSqlBuilder.AppendLine("	P.ENT_MONTH_RECEIPT_COUNT																					,	");
		oSqlBuilder.AppendLine("	P.ENT_MONTH_RECEIPT_AMT																						,	");
		oSqlBuilder.AppendLine("	P.ENT_MONTH_RETURN_RATE																						,	");
		oSqlBuilder.AppendLine("	P.REPEAT_RECEIPT_COUNT																						,	");
		oSqlBuilder.AppendLine("	P.REPEAT_RECEIPT_AMT																						,	");
		oSqlBuilder.AppendLine("	P.TOTAL_RECEIPT_COUNT																						,	");
		oSqlBuilder.AppendLine("	P.TOTAL_RECEIPT_AMT																							,	");
		oSqlBuilder.AppendLine("	P.REPEAT_RATE																								,	");
		oSqlBuilder.AppendLine("	P.AGGR_AD_COST_RATE																							,	");
		oSqlBuilder.AppendLine("	P.AGGR_AD_COST																								,	");
		oSqlBuilder.AppendLine("	P.AGGR_RECEIPT_USER_COUNT																					,	");
		oSqlBuilder.AppendLine("	P.AGGR_RECEIPT_AMT																							,	");
		oSqlBuilder.AppendLine("	P.AD_GROUP_CD																								,	");
		oSqlBuilder.AppendLine("	P.AD_GROUP_NM																									");
		oSqlBuilder.AppendLine("FROM																												");
		oSqlBuilder.AppendLine("	(																												");
		oSqlBuilder.AppendLine("		SELECT																										");
		oSqlBuilder.AppendLine("			P.SITE_CD																							,	");
		oSqlBuilder.AppendLine("			SUM(P.AD_COST) AS AD_COST																			,	");
		oSqlBuilder.AppendLine("			SUM(P.ACCESS_COUNT) AS ACCESS_COUNT																	,	");
		oSqlBuilder.AppendLine("			SUM(P.REGIST_COUNT) AS REGIST_COUNT																	,	");
		oSqlBuilder.AppendLine("			CASE																									");
		oSqlBuilder.AppendLine("				WHEN SUM(ACCESS_COUNT) > 0																			");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(SUM(REGIST_COUNT) / SUM(ACCESS_COUNT) * 100,2))									");
		oSqlBuilder.AppendLine("				ELSE '-'																							");
		oSqlBuilder.AppendLine("			END AS REGIST_RATE																					,	");
		oSqlBuilder.AppendLine("			SUM(P.REGIST_COUNT_AD) AS REGIST_COUNT_AD															,	");
		oSqlBuilder.AppendLine("			SUM(P.LOGIN_COUNT) AS LOGIN_COUNT																	,	");
		oSqlBuilder.AppendLine("			SUM(P.CHARGE_POINT_USER_COUNT) AS CHARGE_POINT_USER_COUNT											,	");
		oSqlBuilder.AppendLine("			CASE																									");
		oSqlBuilder.AppendLine("				WHEN SUM(P.REGIST_COUNT) > 0																		");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(SUM(P.CHARGE_POINT_USER_COUNT) / SUM(P.REGIST_COUNT) * 100,2))					");
		oSqlBuilder.AppendLine("				ELSE '-'																							");
		oSqlBuilder.AppendLine("			END AS CHARGE_POINT_USER_RATE																		,	");
		oSqlBuilder.AppendLine("			SUM(P.START_RECEIPT_USER_COUNT) AS START_RECEIPT_USER_COUNT											,	");
		oSqlBuilder.AppendLine("			CASE																									");
		oSqlBuilder.AppendLine("				WHEN SUM(P.REGIST_COUNT) > 0																		");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(SUM(P.START_RECEIPT_USER_COUNT) / SUM(P.REGIST_COUNT) * 100,2))					");
		oSqlBuilder.AppendLine("				ELSE '-'																							");
		oSqlBuilder.AppendLine("			END AS START_RECEIPT_USER_RATE																		,	");
		oSqlBuilder.AppendLine("			CASE																									");
		oSqlBuilder.AppendLine("				WHEN SUM(P.REGIST_COUNT) > 0																		");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(SUM(P.AD_COST) / SUM(P.REGIST_COUNT),2))											");
		oSqlBuilder.AppendLine("				ELSE '-'																							");
		oSqlBuilder.AppendLine("			END AS AD_COST_BY_REGIST																			,	");
		oSqlBuilder.AppendLine("			CASE																									");
		oSqlBuilder.AppendLine("				WHEN SUM(P.START_RECEIPT_USER_COUNT) > 0															");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(SUM(P.AD_COST) / SUM(P.START_RECEIPT_USER_COUNT),2))								");
		oSqlBuilder.AppendLine("				ELSE '-'																							");
		oSqlBuilder.AppendLine("			END AS AD_COST_BY_START_RECEIPT																		,	");
		oSqlBuilder.AppendLine("			SUM(P.ENT_MONTH_RECEIPT_COUNT) AS ENT_MONTH_RECEIPT_COUNT											,	");
		oSqlBuilder.AppendLine("			SUM(P.ENT_MONTH_RECEIPT_AMT) AS ENT_MONTH_RECEIPT_AMT												,	");
		oSqlBuilder.AppendLine("			CASE																									");
		oSqlBuilder.AppendLine("				WHEN SUM(P.AD_COST) > 0																				");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(SUM(P.ENT_MONTH_RECEIPT_AMT) / SUM(P.AD_COST) * 100,2))							");
		oSqlBuilder.AppendLine("				ELSE '-'																							");
		oSqlBuilder.AppendLine("			END AS ENT_MONTH_RETURN_RATE																		,	");
		oSqlBuilder.AppendLine("			SUM(P.REPEAT_RECEIPT_COUNT) AS REPEAT_RECEIPT_COUNT													,	");
		oSqlBuilder.AppendLine("			SUM(P.REPEAT_RECEIPT_AMT) AS REPEAT_RECEIPT_AMT														,	");
		oSqlBuilder.AppendLine("			SUM(P.ENT_MONTH_RECEIPT_COUNT) + SUM(P.REPEAT_RECEIPT_COUNT) AS TOTAL_RECEIPT_COUNT					,	");
		oSqlBuilder.AppendLine("			SUM(P.ENT_MONTH_RECEIPT_AMT) + SUM(P.REPEAT_RECEIPT_AMT) AS TOTAL_RECEIPT_AMT						,	");
		oSqlBuilder.AppendLine("			CASE																									");
		oSqlBuilder.AppendLine("				WHEN SUM(P.AGGR_RECEIPT_USER_COUNT) > 0																");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(SUM(P.REPEAT_RECEIPT_COUNT) / SUM(P.AGGR_RECEIPT_USER_COUNT) * 100,2))			");
		oSqlBuilder.AppendLine("				ELSE '-'																							");
		oSqlBuilder.AppendLine("			END AS REPEAT_RATE																					,	");
		oSqlBuilder.AppendLine("			CASE																									");
		oSqlBuilder.AppendLine("				WHEN SUM(P.AGGR_RECEIPT_AMT) > 0																	");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(SUM(P.AGGR_AD_COST) / SUM(P.AGGR_RECEIPT_AMT) * 100,2))							");
		oSqlBuilder.AppendLine("				ELSE '-'																							");
		oSqlBuilder.AppendLine("			END AS AGGR_AD_COST_RATE																			,	");
		oSqlBuilder.AppendLine("			SUM(P.AGGR_AD_COST) AS AGGR_AD_COST																	,	");
		oSqlBuilder.AppendLine("			SUM(P.AGGR_RECEIPT_USER_COUNT) AS AGGR_RECEIPT_USER_COUNT											,	");
		oSqlBuilder.AppendLine("			SUM(P.AGGR_RECEIPT_AMT) AS AGGR_RECEIPT_AMT															,	");
		oSqlBuilder.AppendLine("			SA.AD_GROUP_CD			,																				");
		oSqlBuilder.AppendLine("			AG.AD_GROUP_NM																							");
		oSqlBuilder.AppendLine("		FROM																										");
		oSqlBuilder.AppendLine("			T_AD_DATA_USER_MONTHLY P																			,	");
		oSqlBuilder.AppendLine("			T_SITE_AD_GROUP SA																					,	");
		oSqlBuilder.AppendLine("			T_AD_GROUP AG																							");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("		GROUP BY P.SITE_CD,SA.AD_GROUP_CD,AG.AD_GROUP_NM															");
		oSqlBuilder.AppendLine("	) P																												");


		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhereByAdGroup(SearchCondition pSearchCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("P.SITE_CD = SA.SITE_CD",ref pWhereClause);
		SysPrograms.SqlAppendWhere("P.AD_CD = SA.AD_CD",ref pWhereClause);
		SysPrograms.SqlAppendWhere("SA.AD_GROUP_CD = AG.AD_GROUP_CD",ref pWhereClause);

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.ReportMonth)) {
			SysPrograms.SqlAppendWhere("P.REPORT_MONTH = :REPORT_MONTH",ref pWhereClause);
			oParamList.Add(new OracleParameter(":REPORT_MONTH",pSearchCondition.ReportMonth));
		}

		return oParamList.ToArray();
	}

	public DataSet GetTotalByNoAdGroup(string pSiteCd,string pReportMonth) {
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT																												");
		oSqlBuilder.AppendLine("	NVL(SUM(P.AD_COST),0) AS AD_COST																			,	");
		oSqlBuilder.AppendLine("	NVL(SUM(P.ACCESS_COUNT),0) AS ACCESS_COUNT																	,	");
		oSqlBuilder.AppendLine("	NVL(SUM(P.REGIST_COUNT),0) AS REGIST_COUNT																	,	");
		oSqlBuilder.AppendLine("	CASE																											");
		oSqlBuilder.AppendLine("		WHEN NVL(SUM(P.ACCESS_COUNT),0) > 0																			");
		oSqlBuilder.AppendLine("		THEN TO_CHAR(TRUNC(NVL(SUM(P.REGIST_COUNT),0) / NVL(SUM(P.ACCESS_COUNT),0) * 100,2))						");
		oSqlBuilder.AppendLine("		ELSE '-'																									");
		oSqlBuilder.AppendLine("	END AS REGIST_RATE																							,	");
		oSqlBuilder.AppendLine("	NVL(SUM(P.REGIST_COUNT_AD),0) AS REGIST_COUNT_AD															,	");
		oSqlBuilder.AppendLine("	NVL(SUM(P.LOGIN_COUNT),0) AS LOGIN_COUNT																	,	");
		oSqlBuilder.AppendLine("	NVL(SUM(P.CHARGE_POINT_USER_COUNT),0) AS CHARGE_POINT_USER_COUNT											,	");
		oSqlBuilder.AppendLine("	CASE																											");
		oSqlBuilder.AppendLine("		WHEN NVL(SUM(P.REGIST_COUNT),0) > 0																			");
		oSqlBuilder.AppendLine("		THEN TO_CHAR(TRUNC(NVL(SUM(P.CHARGE_POINT_USER_COUNT),0) / NVL(SUM(P.REGIST_COUNT),0) * 100,2))				");
		oSqlBuilder.AppendLine("		ELSE '-'																									");
		oSqlBuilder.AppendLine("	END AS CHARGE_POINT_USER_RATE																				,	");
		oSqlBuilder.AppendLine("	NVL(SUM(P.START_RECEIPT_USER_COUNT),0) AS START_RECEIPT_USER_COUNT											,	");
		oSqlBuilder.AppendLine("	CASE																											");
		oSqlBuilder.AppendLine("		WHEN NVL(SUM(P.REGIST_COUNT),0) > 0																			");
		oSqlBuilder.AppendLine("		THEN TO_CHAR(TRUNC(NVL(SUM(P.START_RECEIPT_USER_COUNT),0) / NVL(SUM(P.REGIST_COUNT),0) * 100, 2))			");
		oSqlBuilder.AppendLine("		ELSE '-'																									");
		oSqlBuilder.AppendLine("	END AS START_RECEIPT_USER_RATE																				,	");
		oSqlBuilder.AppendLine("	CASE																											");
		oSqlBuilder.AppendLine("		WHEN NVL(SUM(P.REGIST_COUNT),0) > 0																			");
		oSqlBuilder.AppendLine("		THEN TO_CHAR(TRUNC(NVL(SUM(P.AD_COST),0) / NVL(SUM(P.REGIST_COUNT),0),2))									");
		oSqlBuilder.AppendLine("		ELSE '-'																									");
		oSqlBuilder.AppendLine("	END AS AD_COST_BY_REGIST																					,	");
		oSqlBuilder.AppendLine("	CASE																											");
		oSqlBuilder.AppendLine("		WHEN NVL(SUM(P.START_RECEIPT_USER_COUNT),0) > 0																");
		oSqlBuilder.AppendLine("		THEN TO_CHAR(TRUNC(NVL(SUM(P.AD_COST),0) / NVL(SUM(P.START_RECEIPT_USER_COUNT),0),2))						");
		oSqlBuilder.AppendLine("		ELSE '-'																									");
		oSqlBuilder.AppendLine("	END AS AD_COST_BY_START_RECEIPT																				,	");
		oSqlBuilder.AppendLine("	NVL(SUM(P.ENT_MONTH_RECEIPT_COUNT),0) AS ENT_MONTH_RECEIPT_COUNT											,	");
		oSqlBuilder.AppendLine("	NVL(SUM(P.ENT_MONTH_RECEIPT_AMT),0) AS ENT_MONTH_RECEIPT_AMT												,	");
		oSqlBuilder.AppendLine("	CASE																											");
		oSqlBuilder.AppendLine("		WHEN NVL(SUM(P.AD_COST),0) > 0																				");
		oSqlBuilder.AppendLine("		THEN TO_CHAR(TRUNC(NVL(SUM(P.ENT_MONTH_RECEIPT_AMT),0) / NVL(SUM(P.AD_COST),0) * 100,2))					");
		oSqlBuilder.AppendLine("		ELSE '-'																									");
		oSqlBuilder.AppendLine("	END AS ENT_MONTH_RETURN_RATE																				,	");
		oSqlBuilder.AppendLine("	NVL(SUM(P.REPEAT_RECEIPT_COUNT),0) AS REPEAT_RECEIPT_COUNT													,	");
		oSqlBuilder.AppendLine("	NVL(SUM(P.REPEAT_RECEIPT_AMT),0) AS REPEAT_RECEIPT_AMT														,	");
		oSqlBuilder.AppendLine("	NVL(SUM(P.ENT_MONTH_RECEIPT_COUNT),0) + NVL(SUM(P.REPEAT_RECEIPT_COUNT),0) AS TOTAL_RECEIPT_COUNT			,	");
		oSqlBuilder.AppendLine("	NVL(SUM(P.ENT_MONTH_RECEIPT_AMT),0) + NVL(SUM(P.REPEAT_RECEIPT_AMT),0) AS TOTAL_RECEIPT_AMT					,	");
		oSqlBuilder.AppendLine("	CASE																											");
		oSqlBuilder.AppendLine("		WHEN NVL(SUM(P.AGGR_RECEIPT_USER_COUNT),0) > 0																");
		oSqlBuilder.AppendLine("		THEN TO_CHAR(TRUNC(NVL(SUM(P.REPEAT_RECEIPT_COUNT),0) / NVL(SUM(P.AGGR_RECEIPT_USER_COUNT),0) * 100,2))		");
		oSqlBuilder.AppendLine("		ELSE '-'																									");
		oSqlBuilder.AppendLine("	END AS REPEAT_RATE																							,	");
		oSqlBuilder.AppendLine("	CASE																											");
		oSqlBuilder.AppendLine("		WHEN NVL(SUM(P.AGGR_RECEIPT_AMT),0) > 0																		");
		oSqlBuilder.AppendLine("		THEN TO_CHAR(TRUNC(NVL(SUM(P.AGGR_AD_COST),0) / NVL(SUM(P.AGGR_RECEIPT_AMT),0) * 100,2))					");
		oSqlBuilder.AppendLine("		ELSE '-'																									");
		oSqlBuilder.AppendLine("	END AS AGGR_AD_COST_RATE																					,	");
		oSqlBuilder.AppendLine("	NVL(SUM(P.AGGR_AD_COST),0) AS AGGR_AD_COST																	,	");
		oSqlBuilder.AppendLine("	NVL(SUM(P.AGGR_RECEIPT_USER_COUNT),0) AS AGGR_RECEIPT_USER_COUNT											,	");
		oSqlBuilder.AppendLine("	NVL(SUM(P.AGGR_RECEIPT_AMT),0) AS AGGR_RECEIPT_AMT																");
		oSqlBuilder.AppendLine("FROM																												");
		oSqlBuilder.AppendLine("	T_AD_DATA_USER_MONTHLY P																						");
		oSqlBuilder.AppendLine("WHERE																												");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD																					AND	");
		oSqlBuilder.AppendLine("	REPORT_MONTH	= :REPORT_MONTH																				AND	");
		oSqlBuilder.AppendLine("	NOT EXISTS																										");
		oSqlBuilder.AppendLine("	(																												");
		oSqlBuilder.AppendLine("		SELECT																										");
		oSqlBuilder.AppendLine("			*																										");
		oSqlBuilder.AppendLine("		FROM																										");
		oSqlBuilder.AppendLine("			T_SITE_AD_GROUP																							");
		oSqlBuilder.AppendLine("		WHERE																										");
		oSqlBuilder.AppendLine("			SITE_CD		= P.SITE_CD																				AND	");
		oSqlBuilder.AppendLine("			AD_CD		= P.AD_CD																					");
		oSqlBuilder.AppendLine("	)																												");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":REPORT_MONTH",pReportMonth));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
}
