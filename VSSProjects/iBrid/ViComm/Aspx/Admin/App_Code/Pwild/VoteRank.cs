﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 投票順位
--	Progaram ID		: VoteRank
--  Creation Date	: 2013.10.15
--  Creater			: K.Miyazato
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;
using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class VoteRank:DbSession {
	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}

		private string voteTermSeq;
		public string VoteTermSeq {
			get {
				return this.voteTermSeq;
			}
			set {
				this.voteTermSeq = value;
			}
		}

		private string sortDirection;
		public string SortDirection {
			get {
				return this.sortDirection;
			}
			set {
				this.sortDirection = value;
			}
		}

		private string sortExpression;
		public string SortExpression {
			get {
				return this.sortExpression;
			}
			set {
				this.sortExpression = value;
			}
		}
	}

	public VoteRank() {
	}

	public int GetPageCount(SearchCondition pSearchCondition) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_VOTE_RANK00");
		oSqlBuilder.AppendLine(sWhereClause);

		return ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
	}

	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));
		sSortExpression = this.GetOrder(pSearchCondition);

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SITE_CD,");
		oSqlBuilder.AppendLine("	LOGIN_ID,");
		oSqlBuilder.AppendLine("	HANDLE_NM,");
		oSqlBuilder.AppendLine("	NA_FLAG,");
		oSqlBuilder.AppendLine("	VOTE_TERM_SEQ,");
		oSqlBuilder.AppendLine("	VOTE_RANK_SEQ,");
		oSqlBuilder.AppendLine("	REAL_VOTE_COUNT,");
		oSqlBuilder.AppendLine("	ADD_VOTE_COUNT,");
		oSqlBuilder.AppendLine("	VOTE_COUNT,");
		oSqlBuilder.AppendLine("	VOTE_RANK,");
		oSqlBuilder.AppendLine("	REVISION_NO");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_VOTE_RANK00");
		oSqlBuilder.AppendLine(sWhereClause);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		return ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.VoteTermSeq)) {
			SysPrograms.SqlAppendWhere("VOTE_TERM_SEQ = :VOTE_TERM_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":VOTE_TERM_SEQ",pSearchCondition.VoteTermSeq));
		}

		return oParamList.ToArray();
	}

	private string GetOrder(SearchCondition pSearchCondition) {
		if (string.IsNullOrEmpty(pSearchCondition.SortExpression)) {
			return "ORDER BY SITE_CD,VOTE_TERM_SEQ,VOTE_COUNT DESC,LAST_VOTE_DATE ASC";
		} else {
			return string.Format("ORDER BY {0} {1},SITE_CD,VOTE_TERM_SEQ",pSearchCondition.SortExpression,pSearchCondition.SortDirection);
		}
	}
}
