﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 会員つぶやき
--	Progaram ID		: ManTweet
--  Creation Date	: 2013.02.19
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class ManTweet:DbSession {

	#region SearchCondition
	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}
		private string tweetDateFrom;
		public string TweetDateFrom {
			get {
				return this.tweetDateFrom;
			}
			set {
				this.tweetDateFrom = value;
			}
		}
		private string tweetDateTo;
		public string TweetDateTo {
			get {
				return this.tweetDateTo;
			}
			set {
				this.tweetDateTo = value;
			}
		}
		private string loginId;
		public string LoginId {
			get {
				return this.loginId;
			}
			set {
				this.loginId = value;
			}
		}
		private string keyword;
		public string Keyword {
			get {
				return this.keyword;
			}
			set {
				this.keyword = value;
			}
		}
		private string unAuthPicFlag;
		public string UnAuthPicFlag {
			get {
				return this.unAuthPicFlag;
			}
			set {
				this.unAuthPicFlag = value;
			}
		}
		private string manTweetSeq;
		public string ManTweetSeq {
			get {
				return this.manTweetSeq;
			}
			set {
				this.manTweetSeq = value;
			}
		}
	}
	#endregion SearchCondition

	public ManTweet() {
	}

	public int GetPageCount(object pSearchCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_MAN_TWEET");
		oSqlBuilder.AppendLine("	INNER JOIN T_USER");
		oSqlBuilder.AppendLine("		ON (T_MAN_TWEET.USER_SEQ = T_USER.USER_SEQ)");

		string sWhereClause;
		OracleParameter[] oWhereParams = this.CreateWhere((SearchCondition)pSearchCondition,out sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(object pSearchCondition,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.SITE_CD,");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.MAN_TWEET_SEQ,");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.PIC_SEQ AS MAN_TWEET_PIC_SEQ,");
		oSqlBuilder.AppendLine("	T_USER.LOGIN_ID,");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.HANDLE_NM,");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.USER_SEQ,");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.USER_CHAR_NO,");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.TWEET_TEXT,");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.TWEET_DATE,");
		oSqlBuilder.AppendLine("	GET_MAN_SMALL_PHOTO_IMG_PATH(T_MAN_TWEET.SITE_CD,T_USER.LOGIN_ID,T_MAN_TWEET.PIC_SEQ,0) AS MAN_TWEET_SMALL_IMG_PATH,");
		oSqlBuilder.AppendLine("	GET_MAN_PHOTO_IMG_PATH(T_MAN_TWEET.SITE_CD,T_USER.LOGIN_ID,T_MAN_TWEET.PIC_SEQ,0) AS MAN_TWEET_IMG_PATH,");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.COMMENT_COUNT,");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.ADMIN_DEL_FLAG,");
		oSqlBuilder.AppendLine("	CASE WHEN T_MAN_TWEET.ADMIN_DEL_FLAG = 1 THEN '管理者削除' ELSE NULL END AS ADMIN_DEL_STATUS,");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.UNAUTH_PIC_FLAG AS MAN_TWEET_UNAUTH_PIC_FLAG,");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.DEL_FLAG,");
		oSqlBuilder.AppendLine("	CASE WHEN T_MAN_TWEET.DEL_FLAG = 1 THEN 'ユーザー削除' ELSE NULL END AS DEL_STATUS,");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.ADMIN_CHECK_FLAG");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_MAN_TWEET");
		oSqlBuilder.AppendLine("	INNER JOIN T_USER");
		oSqlBuilder.AppendLine("		ON (T_MAN_TWEET.USER_SEQ = T_USER.USER_SEQ)");
		oSqlBuilder.AppendLine("	INNER JOIN T_USER_MAN_CHARACTER");
		oSqlBuilder.AppendLine("		ON (T_MAN_TWEET.SITE_CD = T_USER_MAN_CHARACTER.SITE_CD AND");
		oSqlBuilder.AppendLine("			T_MAN_TWEET.USER_SEQ = T_USER_MAN_CHARACTER.USER_SEQ AND");
		oSqlBuilder.AppendLine("			T_MAN_TWEET.USER_CHAR_NO = T_USER_MAN_CHARACTER.USER_CHAR_NO)");

		string sWhereClause;
		string sSortExpression = this.GetOrder((SearchCondition)pSearchCondition);
		string sPagingSql;

		OracleParameter[] oWhereParams = this.CreateWhere((SearchCondition)pSearchCondition,out sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}

	private string GetOrder(SearchCondition pSearchCondition) {
		return "ORDER BY T_MAN_TWEET.TWEET_DATE DESC";
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,out string pWhere) {
		List<OracleParameter> oOracleParameterList = new List<OracleParameter>();
		pWhere = string.Empty;

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("T_MAN_TWEET.SITE_CD = :SITE_CD",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("SITE_CD",pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.TweetDateFrom) && !string.IsNullOrEmpty(pSearchCondition.TweetDateTo)) {
			DateTime dtFrom = DateTime.Parse(pSearchCondition.TweetDateFrom);
			DateTime dtTo = DateTime.Parse(pSearchCondition.TweetDateTo).AddSeconds(1);
			SysPrograms.SqlAppendWhere("(T_MAN_TWEET.TWEET_DATE >= :TWEET_DATE_FROM AND T_MAN_TWEET.TWEET_DATE < :TWEET_DATE_TO)",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("TWEET_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			oOracleParameterList.Add(new OracleParameter("TWEET_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.LoginId)) {
			SysPrograms.SqlAppendWhere("T_USER.LOGIN_ID = :LOGIN_ID",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("LOGIN_ID",pSearchCondition.LoginId));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.Keyword)) {
			StringBuilder oWhereKeywords = new StringBuilder("(");
			string[] sKeywordArray = pSearchCondition.Keyword.Split(' ','　');
			for (int i = 0;i < sKeywordArray.Length;i++) {
				oWhereKeywords.AppendFormat("(T_MAN_TWEET.TWEET_TEXT LIKE :TWEET_TEXT{0})",i);
				oOracleParameterList.Add(new OracleParameter(string.Format("TWEET_TEXT{0}",i),string.Format("%{0}%",sKeywordArray[i])));

				if (i < sKeywordArray.Length - 1) {
					oWhereKeywords.AppendLine(" AND ");
				}
			}
			oWhereKeywords.Append(")");
			SysPrograms.SqlAppendWhere(oWhereKeywords.ToString(),ref pWhere);
		}

		if (!string.IsNullOrEmpty(pSearchCondition.UnAuthPicFlag)) {
			SysPrograms.SqlAppendWhere("T_MAN_TWEET.UNAUTH_PIC_FLAG = :MAN_TWEET_UNAUTH_PIC_FLAG",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("MAN_TWEET_UNAUTH_PIC_FLAG",pSearchCondition.UnAuthPicFlag));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.ManTweetSeq)) {
			SysPrograms.SqlAppendWhere("T_MAN_TWEET.MAN_TWEET_SEQ = :MAN_TWEET_SEQ",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("MAN_TWEET_SEQ",pSearchCondition.ManTweetSeq));
		}

		return oOracleParameterList.ToArray();
	}

	/// <summary>
	/// 検索条件に一致するレコードを取得する
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <returns></returns>
	public DataSet GetList(object pSearchCondition) {
		// 検索用のクエリを設定
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.SITE_CD,");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.MAN_TWEET_SEQ,");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.PIC_SEQ AS MAN_TWEET_PIC_SEQ,");
		oSqlBuilder.AppendLine("	T_USER.LOGIN_ID,");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER.HANDLE_NM,");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.USER_SEQ,");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.USER_CHAR_NO,");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.TWEET_TEXT,");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.TWEET_DATE,");
		oSqlBuilder.AppendLine("	GET_MAN_SMALL_PHOTO_IMG_PATH(T_MAN_TWEET.SITE_CD,T_USER.LOGIN_ID,T_MAN_TWEET.PIC_SEQ,0) AS MAN_TWEET_SMALL_IMG_PATH,");
		oSqlBuilder.AppendLine("	GET_MAN_PHOTO_IMG_PATH(T_MAN_TWEET.SITE_CD,T_USER.LOGIN_ID,T_MAN_TWEET.PIC_SEQ,0) AS MAN_TWEET_IMG_PATH,");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.COMMENT_COUNT,");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.ADMIN_DEL_FLAG,");
		oSqlBuilder.AppendLine("	CASE WHEN T_MAN_TWEET.ADMIN_DEL_FLAG = 1 THEN '管理者削除' ELSE NULL END AS ADMIN_DEL_STATUS,");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.UNAUTH_PIC_FLAG AS MAN_TWEET_UNAUTH_PIC_FLAG,");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.DEL_FLAG,");
		oSqlBuilder.AppendLine("	CASE WHEN T_MAN_TWEET.DEL_FLAG = 1 THEN 'ユーザー削除' ELSE NULL END AS DEL_STATUS,");
		oSqlBuilder.AppendLine("	T_MAN_TWEET.ADMIN_CHECK_FLAG");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_MAN_TWEET");
		oSqlBuilder.AppendLine("	INNER JOIN T_USER");
		oSqlBuilder.AppendLine("		ON (T_MAN_TWEET.USER_SEQ = T_USER.USER_SEQ)");
		oSqlBuilder.AppendLine("	INNER JOIN T_USER_MAN_CHARACTER");
		oSqlBuilder.AppendLine("		ON (T_MAN_TWEET.SITE_CD = T_USER_MAN_CHARACTER.SITE_CD AND");
		oSqlBuilder.AppendLine("			T_MAN_TWEET.USER_SEQ = T_USER_MAN_CHARACTER.USER_SEQ AND");
		oSqlBuilder.AppendLine("			T_MAN_TWEET.USER_CHAR_NO = T_USER_MAN_CHARACTER.USER_CHAR_NO)");

		// 検索条件を設定
		string sWhereClause;
		OracleParameter[] oWhereParams = this.CreateWhere((SearchCondition)pSearchCondition,out sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		// ソート条件を設定
		string sSortExpression = this.GetOrder((SearchCondition)pSearchCondition);
		oSqlBuilder.AppendLine(sSortExpression);

		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);

				// 検索実行
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public int GetUnconfirmCount(string pSiteCd,string pReportDayFrom) {
		int iValue;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_MAN_TWEET");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	ADMIN_CHECK_FLAG	= :ADMIN_CHECK_FLAG	AND	");
		oSqlBuilder.AppendLine("	TWEET_DATE			>= :TWEET_DATE");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":ADMIN_CHECK_FLAG",ViCommConst.FLAG_OFF));
		DateTime dtFrom = DateTime.Parse(pReportDayFrom);
		oParamList.Add(new OracleParameter("TWEET_DATE",OracleDbType.Date,dtFrom,ParameterDirection.Input));

		iValue = ExecuteSelectCountQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return iValue;
	}

	public int GetUnAuthPicCount(string pSiteCd) {
		int iValue;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_MAN_TWEET");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	UNAUTH_PIC_FLAG	= :UNAUTH_PIC_FLAG");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":UNAUTH_PIC_FLAG",ViCommConst.FLAG_ON));

		iValue = ExecuteSelectCountQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return iValue;
	}
}
