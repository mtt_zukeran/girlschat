﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 月別FC会費売上--	Progaram ID		: MonthlyFanClubFee
--
--  Creation Date	: 2013.03.06
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

public class MonthlyFanClubFee:DbSession {

	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}
		private string createDateFrom;
		public string CreateDateFrom {
			get {
				return this.createDateFrom;
			}
			set {
				this.createDateFrom = value;
			}
		}
		private string createDateTo;
		public string CreateDateTo {
			get {
				return this.createDateTo;
			}
			set {
				this.createDateTo = value;
			}
		}
	}

	public MonthlyFanClubFee() {
	}

	public DataSet GetList(MonthlyFanClubFee.SearchCondition pSearchCondition) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		DataSet ds = CreateInitDataSet(pSearchCondition);

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	TO_CHAR(TRUNC(CREATE_DATE,'Month'),'YYYY/MM') AS CREATE_MONTH,");
		oSqlBuilder.AppendLine("	COUNT(*) AS SETTLE_COUNT,");
		oSqlBuilder.AppendLine("	SUM(SETTLE_AMT) AS SETTLE_AMT");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_SETTLE_LOG");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("GROUP BY");
		oSqlBuilder.AppendLine("	TRUNC(CREATE_DATE,'Month')");
		oSqlBuilder.AppendLine("ORDER BY");
		oSqlBuilder.AppendLine("	CREATE_MONTH");

		DataSet dsTmp = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		foreach (DataRow drTmp in dsTmp.Tables[0].Rows) {
			DataRow dr = ds.Tables[0].Rows.Find(drTmp["CREATE_MONTH"]);
			dr["SETTLE_COUNT"] = drTmp["SETTLE_COUNT"];
			dr["SETTLE_AMT"] = drTmp["SETTLE_AMT"];
		}

		return ds;
	}

	private DataSet CreateInitDataSet(MonthlyFanClubFee.SearchCondition pSearchCondition) {
		DataSet ds = new DataSet();
		DataTable dt = new DataTable();
		ds.Tables.Add(dt);
		dt.Columns.Add(new DataColumn(string.Format("CREATE_MONTH"),System.Type.GetType("System.String")));
		dt.Columns.Add(new DataColumn(string.Format("SETTLE_COUNT"),System.Type.GetType("System.String")));
		dt.Columns.Add(new DataColumn(string.Format("SETTLE_AMT"),System.Type.GetType("System.String")));
		dt.PrimaryKey = new DataColumn[] { dt.Columns["CREATE_MONTH"] };

		DateTime dtCreateDateFrom = DateTime.Parse(pSearchCondition.CreateDateFrom);
		DateTime dtCreateDateTo = DateTime.Parse(pSearchCondition.CreateDateTo);

		for (int i = 0; i <= 99; i++) {
			DataRow dr = dt.NewRow();
			dr["CREATE_MONTH"] = dtCreateDateFrom.AddMonths(i).ToString("yyyy/MM");
			dr["SETTLE_COUNT"] = "0";
			dr["SETTLE_AMT"] = "0";
			dt.Rows.Add(dr);

			if (dtCreateDateFrom.AddMonths(i).ToString("yyyyMM") == dtCreateDateTo.ToString("yyyyMM")) {
				break;
			}
		}

		return ds;
	}

	private OracleParameter[] CreateWhere(MonthlyFanClubFee.SearchCondition pSearchCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter("SITE_CD",pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.CreateDateFrom)) {
			DateTime dtCreateDateFrom = DateTime.Parse(pSearchCondition.CreateDateFrom);
			SysPrograms.SqlAppendWhere("CREATE_DATE >= :CREATE_DATE_FROM",ref pWhereClause);
			oParamList.Add(new OracleParameter("CREATE_DATE_FROM",OracleDbType.Date,dtCreateDateFrom,ParameterDirection.Input));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.CreateDateTo)) {
			DateTime dtCreateDateTo = DateTime.Parse(pSearchCondition.CreateDateTo);
			SysPrograms.SqlAppendWhere("CREATE_DATE <= :CREATE_DATE_TO",ref pWhereClause);
			oParamList.Add(new OracleParameter("CREATE_DATE_TO",OracleDbType.Date,dtCreateDateTo,ParameterDirection.Input));
		}

		SysPrograms.SqlAppendWhere("SETTLE_STATUS = :SETTLE_STATUS",ref pWhereClause);
		oParamList.Add(new OracleParameter("SETTLE_STATUS","0"));

		SysPrograms.SqlAppendWhere("SETTLE_COMPLITE_FLAG = :SETTLE_COMPLITE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter("SETTLE_COMPLITE_FLAG",ViCommConst.FLAG_ON_STR));

		SysPrograms.SqlAppendWhere("SETTLE_CANCEL_FLAG = :SETTLE_CANCEL_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter("SETTLE_CANCEL_FLAG",ViCommConst.FLAG_OFF_STR));

		SysPrograms.SqlAppendWhere("FANCLUB_FEE_FLAG = :FANCLUB_FEE_FLAG",ref pWhereClause);
		oParamList.Add(new OracleParameter("FANCLUB_FEE_FLAG",ViCommConst.FLAG_ON_STR));

		return oParamList.ToArray();
	}
}
