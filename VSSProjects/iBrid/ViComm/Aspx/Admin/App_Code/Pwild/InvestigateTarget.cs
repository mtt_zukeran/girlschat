﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: この娘を探せ・ピックアップ


--	Progaram ID		: InvestigateTarget
--  Creation Date	: 2015.02.26
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class InvestigateTarget:DbSession {

	#region SearchCondition
	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}

		private string accessDay;
		public string AccessDay {
			get {
				return this.accessDay;
			}
			set {
				this.accessDay = value;
			}
		}
	}
	#endregion SearchCondition
	
	public InvestigateTarget() {
	}

	public int GetPageCount(SearchCondition pSearchCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pSearchCondition,ref sWhereClause);
		
		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	COUNT(*)										");
		oSqlBuilder.AppendLine("FROM												");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER CC							,	");
		oSqlBuilder.AppendLine("	T_USER U									,	");
		oSqlBuilder.AppendLine("	T_INVESTIGATE_TARGET IT							");
		oSqlBuilder.AppendLine(sWhereClause);

		int iPageCount = 0;
		iPageCount = ExecuteSelectCountQueryBase(oSqlBuilder.ToString(),oWhereParams);
		return iPageCount;
	}

	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));
		
		oSqlBuilder.AppendLine("SELECT																								");
		oSqlBuilder.AppendLine("	CC.SITE_CD																					,	");
		oSqlBuilder.AppendLine("	CC.USER_SEQ																					,	");
		oSqlBuilder.AppendLine("	CC.USER_CHAR_NO																				,	");
		oSqlBuilder.AppendLine("	CC.HANDLE_NM																				,	");
		oSqlBuilder.AppendLine("	CC.AGE																						,	");
		oSqlBuilder.AppendLine("	CC.COMMENT_LIST																				,	");
		oSqlBuilder.AppendLine("	U.LOGIN_ID																					,	");
		oSqlBuilder.AppendLine("	GET_SMALL_PHOTO_IMG_PATH(CC.SITE_CD, U.LOGIN_ID, CC.PIC_SEQ) SMALL_PHOTO_IMG_PATH			,	");
		oSqlBuilder.AppendLine("	IT.INVESTIGATE_TARGET_SEQ																	,	");
		oSqlBuilder.AppendLine("	IT.EXECUTION_DAY																			,	");
		oSqlBuilder.AppendLine("	TO_CHAR(TO_DATE(IT.EXECUTION_DAY, 'YYYY/MM/DD'), 'fmMM/DD') DAY								,	");
		oSqlBuilder.AppendLine("	TO_CHAR(TO_DATE(IT.EXECUTION_DAY, 'YYYY/MM/DD'), 'DY') DAY_OF_WEEK							,	");
		oSqlBuilder.AppendLine("	IT.REGIST_DATE																				,	");
		oSqlBuilder.AppendLine("	(																								");
		oSqlBuilder.AppendLine("		SELECT																						");
		oSqlBuilder.AppendLine("			COUNT(*) PIC_COUNT																		");
		oSqlBuilder.AppendLine("		FROM																						");
		oSqlBuilder.AppendLine("			T_CAST_PIC																				");
		oSqlBuilder.AppendLine("		WHERE																						");
		oSqlBuilder.AppendLine("			T_CAST_PIC.SITE_CD			= CC.SITE_CD											AND	");
		oSqlBuilder.AppendLine("			T_CAST_PIC.USER_SEQ			= CC.USER_SEQ											AND	");
		oSqlBuilder.AppendLine("			T_CAST_PIC.USER_CHAR_NO		= CC.USER_CHAR_NO										AND	");
		oSqlBuilder.AppendLine("			T_CAST_PIC.PIC_TYPE			= '1'													AND	");
		oSqlBuilder.AppendLine("			T_CAST_PIC.OBJ_NA_FLAG		= 0															");
		oSqlBuilder.AppendLine("	) AS PIC_COUNT																					");
		oSqlBuilder.AppendLine("FROM																								");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER CC																			,	");
		oSqlBuilder.AppendLine("	T_USER U																					,	");
		oSqlBuilder.AppendLine("	T_INVESTIGATE_TARGET IT																			");
		oSqlBuilder.AppendLine(sWhereClause);
		
		sSortExpression = "ORDER BY IT.EXECUTION_DAY, IT.REGIST_DATE, CC.SITE_CD, CC.USER_SEQ, CC.USER_CHAR_NO";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		using (CastAttrTypeValue oCastAttrTypeValue = new CastAttrTypeValue()) {
			oCastAttrTypeValue.AppendCastAttr(oDataSet);
		}
		
		return oDataSet;
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhere) {
		List<OracleParameter> oOracleParameterList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("CC.SITE_CD = IT.SITE_CD",ref pWhere);
		SysPrograms.SqlAppendWhere("CC.USER_SEQ = IT.USER_SEQ",ref pWhere);
		SysPrograms.SqlAppendWhere("CC.USER_CHAR_NO = IT.USER_CHAR_NO",ref pWhere);
		SysPrograms.SqlAppendWhere("CC.USER_SEQ = U.USER_SEQ",ref pWhere);

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(":SITE_CD = CC.SITE_CD",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("SITE_CD",pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.AccessDay)) {
			SysPrograms.SqlAppendWhere(":EXECUTION_DAY <= IT.EXECUTION_DAY",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("EXECUTION_DAY",pSearchCondition.AccessDay));
		}

		return oOracleParameterList.ToArray();
	}
	
	public bool IsExistTarget(string pUserSeq) {
		bool bOk = false;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	1														");
		oSqlBuilder.AppendLine("FROM														");
		oSqlBuilder.AppendLine("	T_INVESTIGATE_TARGET									");
		oSqlBuilder.AppendLine("WHERE														");
		oSqlBuilder.AppendLine("	USER_SEQ	= :USER_SEQ								AND	");
		oSqlBuilder.AppendLine("	EXECUTION_DAY	>= TO_CHAR(SYSDATE,'YYYY/MM/DD')		");
		
		oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		if (oDataSet.Tables[0].Rows.Count > 0) {
			bOk = true;
		}
		
		return bOk;
	}
}
