﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 男性お宝ｺﾝﾌﾟ人数レポート--	Progaram ID		: ManTreasureCompleteTotalReport
--
--  Creation Date	: 2012.12.01
--  Creater			: M&TT A.Taba
--
**************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class ManTreasureCompleteTotalReport:DbSession {

	public ManTreasureCompleteTotalReport() {
	}

	public int GetPageCount(string pSiteCd,string pReportDayFrom,string pReportDayTo) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	DISPLAY_DAY									,");
		oSqlBuilder.AppendLine("	COUNT(DISPLAY_DAY) AS  DISPLAY_DAY_COUNT	 ");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_MAN_TREASURE_COMPLETE_LOG					 ");

		string sWhereClause;

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pReportDayFrom,pReportDayTo,out sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("GROUP BY DISPLAY_DAY							 ");

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs.Tables[0].Rows.Count;
	}

	public DataSet GetPageCollection(string pSiteCd,string pReportDayFrom,string pReportDayTo,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	DISPLAY_DAY									,");
		oSqlBuilder.AppendLine("	COUNT(DISPLAY_DAY) AS  DISPLAY_DAY_COUNT	 ");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_MAN_TREASURE_COMPLETE_LOG					 ");

		string sWhereClause;
		string sPagingSql;
		string sSortExpression = "ORDER BY DISPLAY_DAY	";

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pReportDayFrom,pReportDayTo,out sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("GROUP BY DISPLAY_DAY							 ");
		
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pReportDayFrom,string pReportDayTo,out string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!string.IsNullOrEmpty(pReportDayFrom) && !string.IsNullOrEmpty(pReportDayTo)) {
			SysPrograms.SqlAppendWhere("DISPLAY_DAY >= :DISPLAY_DAY_FROM AND DISPLAY_DAY <= :DISPLAY_DAY_TO",ref pWhere);
			list.Add(new OracleParameter("DISPLAY_DAY_FROM",pReportDayFrom));
			list.Add(new OracleParameter("DISPLAY_DAY_TO",pReportDayTo));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}