﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: プロフアクセス集計(日別)
--	Progaram ID		: ProfAccessCountDaily
--  Creation Date	: 2015.09.30
--  Creater			: M&TT Y.Ikemiya
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;
using iBridCommLib;
using ViComm;
using System.Collections.Generic;
using ViComm.Extension.Pwild;

public class ProfAccessCountDaily:DbSession {
	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}

		private string year;
		public string Year {
			get {
				return this.year;
			}
			set {
				this.year = value;
			}
		}

		private string month;
		public string Month {
			get {
				return this.month;
			}
			set {
				this.month = value;
			}
		}

		private string day;
		public string Day {
			get {
				return this.day;
			}
			set {
				this.day = value;
			}
		}

		private string sortDirection;
		public string SortDirection {
			get {
				return this.sortDirection;
			}
			set {
				this.sortDirection = value;
			}
		}

		private string sortExpression;
		public string SortExpression {
			get {
				return this.sortExpression;
			}
			set {
				this.sortExpression = value;
			}
		}
	}

	public ProfAccessCountDaily() {
	}

	public int GetPageCount(SearchCondition pSearchCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();

		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere((SearchCondition)pSearchCondition,ref sWhereClause);

		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	COUNT(*)							");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	T_PROF_ACCESS_COUNT_DAILY P			");
		oSqlBuilder.AppendLine(sWhereClause);

		int iPageCount = 0;

		iPageCount = ExecuteSelectCountQueryBase(oSqlBuilder.ToString(),oWhereParams);
		return iPageCount;
	}

	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	P.REFERER_ASPX_NM				,	");
		oSqlBuilder.AppendLine("	P.ACCESS_COUNT					,	");
		oSqlBuilder.AppendLine("	P.UNIQUE_ACCESS_COUNT			,	");
		oSqlBuilder.AppendLine("	M.DISPLAY_NM						");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	T_PROF_ACCESS_COUNT_DAILY P		,	");
		oSqlBuilder.AppendLine("	T_MARKING_REFERER M					");

		SysPrograms.SqlAppendWhere("P.SITE_CD = M.SITE_CD (+) ",ref sWhereClause);
		SysPrograms.SqlAppendWhere("P.REFERER_ASPX_NM = M.ASPX_NM (+) ",ref sWhereClause);

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));
		oSqlBuilder.AppendLine(sWhereClause);
		
		sSortExpression = this.GetOrder(pSearchCondition);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.Year) &&
			!string.IsNullOrEmpty(pSearchCondition.Month) &&
			!string.IsNullOrEmpty(pSearchCondition.Day)
		) {
			SysPrograms.SqlAppendWhere("P.REPORT_DAY = :REPORT_DAY",ref pWhereClause);
			oParamList.Add(new OracleParameter(":REPORT_DAY",string.Format("{0}/{1}/{2}",pSearchCondition.Year,pSearchCondition.Month,pSearchCondition.Day)));
		}

		return oParamList.ToArray();
	}

	private string GetOrder(SearchCondition pSearchCondition) {
		if (string.IsNullOrEmpty(pSearchCondition.SortExpression)) {
			return "ORDER BY P.ACCESS_COUNT DESC,P.REFERER_ASPX_NM ASC";
		} else {
			return string.Format("ORDER BY {0} {1},P.REFERER_ASPX_NM ASC",pSearchCondition.SortExpression,pSearchCondition.SortDirection);
		}
	}
}
