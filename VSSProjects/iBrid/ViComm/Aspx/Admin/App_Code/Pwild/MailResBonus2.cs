﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: メール返信ボーナス期間設定2
--	Progaram ID		: MailResBonus2
--  Creation Date	: 2016.09.02
--  Creater			: Zukeran
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;
using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class MailResBonus2:DbSession {
	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}
		private string mailResBonusSeq;
		public string MailResBonusSeq {
			get {
				return this.mailResBonusSeq;
			}
			set {
				this.mailResBonusSeq = value;
			}
		}
	}

	public MailResBonus2() {
	}

	/// <summary>
	/// 検索結果の総件数取得
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <returns></returns>
	public int GetPageCount(SearchCondition pSearchCondition) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(CreateWhere(pSearchCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(MAIL_RES_BONUS_SEQ)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_MAIL_RES_BONUS2");
		oSqlBuilder.AppendLine(sWhereClause);

		return ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
	}

	/// <summary>
	/// 検索結果のデータセットを取得
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <param name="startRowIndex"></param>
	/// <param name="maximumRows"></param>
	/// <returns></returns>
	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(CreateWhere(pSearchCondition,ref sWhereClause));
		sSortExpression = CreateOrder(pSearchCondition);

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SITE_CD,");
		oSqlBuilder.AppendLine("	MAIL_RES_BONUS_SEQ,");
		oSqlBuilder.AppendLine("	RES_LIMIT_SEC1,");
		oSqlBuilder.AppendLine("	RES_LIMIT_SEC2,");
		oSqlBuilder.AppendLine("	UNRECEIVED_DAYS,");
		oSqlBuilder.AppendLine("	RES_BONUS_MAX_NUM,");
		oSqlBuilder.AppendLine("	START_DATE,");
		oSqlBuilder.AppendLine("	END_DATE");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_MAIL_RES_BONUS2");
		oSqlBuilder.AppendLine(sWhereClause);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		return ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
	}

	/// <summary>
	/// 検索結果のデータレコードを取得
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <param name="startRowIndex"></param>
	/// <param name="maximumRows"></param>
	/// <returns></returns>
	public DataRow GetData(SearchCondition pSearchCondition) {
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		OracleParameter[] oParamList;

		DataSet ds;
		DataRow dr = null;
		try {
			conn = DbConnect();

			oSqlBuilder.AppendLine("SELECT");
			oSqlBuilder.AppendLine("	SITE_CD,");
			oSqlBuilder.AppendLine("	MAIL_RES_BONUS_SEQ,");
			oSqlBuilder.AppendLine("	RES_LIMIT_SEC1,");
			oSqlBuilder.AppendLine("	RES_LIMIT_SEC2,");
			oSqlBuilder.AppendLine("	UNRECEIVED_DAYS,");
			oSqlBuilder.AppendLine("	RES_BONUS_MAX_NUM,");
			oSqlBuilder.AppendLine("	START_DATE,");
			oSqlBuilder.AppendLine("	END_DATE,");
			oSqlBuilder.AppendLine("	UPDATE_DATE,");
			oSqlBuilder.AppendLine("	REVISION_NO");
			oSqlBuilder.AppendLine("FROM");
			oSqlBuilder.AppendLine("	T_MAIL_RES_BONUS2");

			oParamList = CreateWhere(pSearchCondition,ref sWhereClause);
			oSqlBuilder.AppendLine(sWhereClause);

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < oParamList.Length;i++) {
					cmd.Parameters.Add((OracleParameter)oParamList[i]);
				}
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
				}
			}
		} finally {
			conn.Close();
		}

		return dr;
	}

	/// <summary>
	/// 検索条件を生成
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <param name="pWhereClause"></param>
	/// <returns></returns>
	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.MailResBonusSeq)) {
			SysPrograms.SqlAppendWhere("MAIL_RES_BONUS_SEQ = :MAIL_RES_BONUS_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":MAIL_RES_BONUS_SEQ",pSearchCondition.MailResBonusSeq));
		}

		return oParamList.ToArray();
	}

	/// <summary>
	/// 検索のソート条件を生成
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <returns></returns>
	private string CreateOrder(SearchCondition pSearchCondition) {
		return "ORDER BY SITE_CD,START_DATE DESC";
	}
}
