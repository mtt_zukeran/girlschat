﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 女性検索回数集計データクラス
--	Progaram ID		: FindCastCountDaily
--
--  Creation Date	: 2016.10.14
--  Creater			: M&TT Zukeran
--
**************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class FindCastCountDaily:DbSession {
	public FindCastCountDaily() {
	}

	/// <summary>
	/// 集計データ取得（画面出力用）
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pYear"></param>
	/// <param name="pMonth"></param>
	/// <returns></returns>
	public DataSet GetList(string pSiteCd,string pYear,string pMonth) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	TO_CHAR(REPORT_DATE,'DD') AS DISPLAY_DAY,");
		oSqlBuilder.AppendLine("	TO_CHAR(REPORT_DATE,'Dy','NLS_DATE_LANGUAGE = ENGLISH') AS REPORT_DAY_OF_WEEK,");
		oSqlBuilder.AppendLine("	*");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_FIND_CAST_HISTORY_DAILY");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD");
		oSqlBuilder.AppendLine("	AND TO_CHAR(REPORT_DATE,'YYYY-MM') = :REPORT_MONTH");
		// 報告年月が当月指定の場合、当日までを対象にする（表示制御用）
		if (string.Format("{0:D4}-{1:D2}",pYear,pMonth).Equals(DateTime.Now.ToString("yyyy-MM"))) {
			oSqlBuilder.AppendLine("	AND REPORT_DATE <= TRUNC(SYSDATE)");
		}
		oSqlBuilder.AppendLine("ORDER BY");
		oSqlBuilder.AppendLine("	REPORT_DATE ASC");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":REPORT_MONTH",string.Format("{0:D4}-{1:D2}",pYear,pMonth)));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		return oDataSet;
	}

	/// <summary>
	/// 集計データ取得（CSV出力用）
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pYear"></param>
	/// <param name="pMonth"></param>
	/// <returns></returns>
	public DataSet GetListCsv(string pSiteCd,string pYear,string pMonth) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	*");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_FIND_CAST_HISTORY_DAILY");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD");
		oSqlBuilder.AppendLine("	AND TO_CHAR(REPORT_DATE,'YYYY-MM') = :REPORT_MONTH");
		oSqlBuilder.AppendLine("ORDER BY");
		oSqlBuilder.AppendLine("	REPORT_DATE ASC");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":REPORT_MONTH",string.Format("{0:D4}-{1:D2}",pYear,pMonth)));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		return oDataSet;
	}
}