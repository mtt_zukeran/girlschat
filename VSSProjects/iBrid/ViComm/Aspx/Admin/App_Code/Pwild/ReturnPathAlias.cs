﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: Return-Pathエイリアス設定
--	Progaram ID		: ReturnPathAlias
--  Creation Date	: 2015.07.09
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

public class ReturnPathAlias:DbSession {
	public ReturnPathAlias() {
	}

	public int GetPageCount() {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_RETURN_PATH_ALIAS");

		return ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
	}

	public DataSet GetPageCollection(int startRowIndex,int maximumRows) {
		string sExecSql = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	RETURN_PATH_ALIAS_SEQ,");
		oSqlBuilder.AppendLine("	ALIAS_NAME,");
		oSqlBuilder.AppendLine("	USED_NOW_FLAG");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_RETURN_PATH_ALIAS");

		string sSortExpression = "ORDER BY RETURN_PATH_ALIAS_SEQ";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		return ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
	}
}
