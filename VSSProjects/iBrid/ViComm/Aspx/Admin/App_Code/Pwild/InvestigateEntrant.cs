﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: この娘を探せ・男性エントリー(詳細)


--	Progaram ID		: InvestigateEntrant
--  Creation Date	: 2015.02.26
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;

public class InvestigateEntrant:DbSession {

	#region SearchCondition
	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}

		private string executionDay;
		public string ExecutionDay {
			get {
				return this.executionDay;
			}
			set {
				this.executionDay = value;
			}
		}

		private string caughtFlag;
		public string CaughtFlag {
			get {
				return this.caughtFlag;
			}
			set {
				this.caughtFlag = value;
			}
		}

		private string pointAcquiredFlag;
		public string PointAcquiredFlag {
			get {
				return this.pointAcquiredFlag;
			}
			set {
				this.pointAcquiredFlag = value;
			}
		}
	}
	#endregion SearchCondition
	
	public InvestigateEntrant() {
	}

	public int GetPageCount(SearchCondition pSearchCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pSearchCondition,ref sWhereClause);
		
		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	COUNT(*)							");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	T_INVESTIGATE_ENTRANT IE		,	");
		oSqlBuilder.AppendLine("	T_USER U						,	");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER UMC			");
		oSqlBuilder.AppendLine(sWhereClause);

		int iPageCount = 0;
		iPageCount = ExecuteSelectCountQueryBase(oSqlBuilder.ToString(),oWhereParams);
		return iPageCount;
	}


	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));
		
		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	U.LOGIN_ID					,	");
		oSqlBuilder.AppendLine("	UMC.HANDLE_NM				,	");
		oSqlBuilder.AppendLine("	IE.SITE_CD					,	");
		oSqlBuilder.AppendLine("	IE.USER_SEQ					,	");
		oSqlBuilder.AppendLine("	IE.USER_CHAR_NO				,	");
		oSqlBuilder.AppendLine("	IE.EXECUTION_DAY			,	");
		oSqlBuilder.AppendLine("	IE.ENTRY_TIME				,	");
		oSqlBuilder.AppendLine("	IE.ENTRY_LEVEL				,	");
		oSqlBuilder.AppendLine("	IE.CAUGHT_TIME				,	");
		oSqlBuilder.AppendLine("	IE.POINT_ACQUIRED_TIME			");
		oSqlBuilder.AppendLine("FROM								");
		oSqlBuilder.AppendLine("	T_INVESTIGATE_ENTRANT IE	,	");
		oSqlBuilder.AppendLine("	T_USER U					,	");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER UMC		");
		oSqlBuilder.AppendLine(sWhereClause);

		sSortExpression = "ORDER BY IE.SITE_CD, IE.ENTRY_TIME, U.LOGIN_ID";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		return ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhere) {
		List<OracleParameter> oOracleParameterList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("IE.USER_SEQ = U.USER_SEQ",ref pWhere);
		SysPrograms.SqlAppendWhere("IE.SITE_CD = UMC.SITE_CD",ref pWhere);
		SysPrograms.SqlAppendWhere("IE.USER_SEQ = UMC.USER_SEQ",ref pWhere);
		SysPrograms.SqlAppendWhere("IE.USER_CHAR_NO = UMC.USER_CHAR_NO",ref pWhere);

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere(":SITE_CD = IE.SITE_CD",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("SITE_CD",pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.ExecutionDay)) {
			SysPrograms.SqlAppendWhere(":EXECUTION_DAY = IE.EXECUTION_DAY",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("EXECUTION_DAY",pSearchCondition.ExecutionDay));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.CaughtFlag)) {
			SysPrograms.SqlAppendWhere(":CAUGHT_FLAG = IE.CAUGHT_FLAG",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("CAUGHT_FLAG",pSearchCondition.CaughtFlag));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.PointAcquiredFlag)) {
			SysPrograms.SqlAppendWhere(":POINT_ACQUIRED_FLAG = IE.POINT_ACQUIRED_FLAG",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("POINT_ACQUIRED_FLAG",pSearchCondition.PointAcquiredFlag));
		}

		return oOracleParameterList.ToArray();
	}
}
