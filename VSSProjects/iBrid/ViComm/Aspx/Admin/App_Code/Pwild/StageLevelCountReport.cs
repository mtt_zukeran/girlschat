﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: ステージレベル分布レポート
--	Progaram ID		: StageLevelCountReport
--
--  Creation Date	: 2012.12.04
--  Creater			: M&TT A.Taba
--
**************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class StageLevelCountReport:DbSession {
	public StageLevelCountReport() {
	}

	public DataSet GetList(string pSiteCd,string pSexCd,string pStageSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	GAME_CHARACTER_LEVEL									　  ");
		oSqlBuilder.AppendLine("FROM															");
		oSqlBuilder.AppendLine("	T_LEVEL_UP_EXP												");
		oSqlBuilder.AppendLine("WHERE															");
		oSqlBuilder.AppendLine("	SITE_CD		= :SITE_CD		AND								");
		oSqlBuilder.AppendLine("	SEX_CD		= :SEX_CD										");
		oSqlBuilder.AppendLine("ORDER BY GAME_CHARACTER_LEVEL									");

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":SEX_CD",pSexCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}

		AppendCount(oDs,pSiteCd,pStageSeq);

		return oDs;
	}

	private void AppendCount(DataSet pDS,string pSiteCd,string pStageSeq) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;

		col = new DataColumn(string.Format("GAME_CHARACTER_LEVEL_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			StringBuilder oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine("SELECT											    ");
			oSqlBuilder.AppendLine("	COUNT(*) AS GAME_CHARACTER_LEVEL_COUNT		    ");
			oSqlBuilder.AppendLine(" FROM							   				    ");
			oSqlBuilder.AppendLine("	T_STAGE_PROGRESS		     		SP	,	    ");
			oSqlBuilder.AppendLine("	T_GAME_CHARACTER		     		GC		    ");
			oSqlBuilder.AppendLine(" WHERE											    ");
			oSqlBuilder.AppendLine("	SP.SITE_CD		        = GC.SITE_CD       AND  ");
			oSqlBuilder.AppendLine("	SP.USER_SEQ		        = GC.USER_SEQ      AND  ");
			oSqlBuilder.AppendLine("	SP.USER_CHAR_NO		    = GC.USER_CHAR_NO  AND  ");
			oSqlBuilder.AppendLine("	SP.SITE_CD   		    = :SITE_CD         AND  ");
			oSqlBuilder.AppendLine("	SP.STAGE_SEQ   		    = :STAGE_SEQ       AND  ");
			oSqlBuilder.AppendLine("	SP.STAGE_CLEAR_FLAG     = 1                AND  ");
			oSqlBuilder.AppendLine("	GC.GAME_CHARACTER_LEVEL = :GAME_CHARACTER_LEVEL ");
			
			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
					cmd.BindByName = true;

					cmd.Parameters.Add(":SITE_CD",pSiteCd);
					cmd.Parameters.Add(":STAGE_SEQ",pStageSeq);
					cmd.Parameters.Add(":GAME_CHARACTER_LEVEL",dr["GAME_CHARACTER_LEVEL"].ToString());

					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub);
					}

					dr["GAME_CHARACTER_LEVEL_COUNT"] = dsSub.Tables[0].Rows[0]["GAME_CHARACTER_LEVEL_COUNT"];
				}
			}
		}
	}

	public string GetStageNm(string pSiteCd,string pStageSeq,string pSexCd) {
		string sValue = null;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	STAGE_NM												");
		oSqlBuilder.AppendLine(" FROM							   							");
		oSqlBuilder.AppendLine("	T_STAGE													");
		oSqlBuilder.AppendLine(" WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD				AND		");
		oSqlBuilder.AppendLine("	STAGE_SEQ				= :STAGE_SEQ			AND		");
		oSqlBuilder.AppendLine("	SEX_CD			        = :SEX_CD   					");

		try {
			this.conn = this.DbConnect();

			using (this.cmd = this.CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.Add(":SITE_CD",pSiteCd);
				this.cmd.Parameters.Add(":STAGE_SEQ",pStageSeq);
				this.cmd.Parameters.Add(":SEX_CD",pSexCd);

				OracleDataReader oDataReader = this.cmd.ExecuteReader();

				if (oDataReader.Read()) {
					sValue = iBridUtil.GetStringValue(oDataReader["STAGE_NM"].ToString());
				}
			}
		} finally {
			this.conn.Close();
		}

		return sValue;
	}

	public string GetStageClearCount(string pSiteCd,string pStageSeq) {
		string sValue = null;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT											 ");
		oSqlBuilder.AppendLine("	COUNT(*) AS STAGE_COUNT	            		 ");
		oSqlBuilder.AppendLine(" FROM							   				 ");
		oSqlBuilder.AppendLine("	T_STAGE_PROGRESS		     				 ");
		oSqlBuilder.AppendLine(" WHERE											 ");
		oSqlBuilder.AppendLine("	SITE_CD		         = :SITE_CD		AND		 ");
		oSqlBuilder.AppendLine("	STAGE_SEQ		     = :STAGE_SEQ			 ");

		try {
			this.conn = this.DbConnect();

			using (this.cmd = this.CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.Add(":SITE_CD",pSiteCd);
				this.cmd.Parameters.Add(":STAGE_SEQ",pStageSeq);

				OracleDataReader oDataReader = this.cmd.ExecuteReader();

				if (oDataReader.Read()) {
					sValue = iBridUtil.GetStringValue(oDataReader["STAGE_COUNT"].ToString());
				}
			}
		} finally {
			this.conn.Close();
		}

		return sValue;
	}
}
