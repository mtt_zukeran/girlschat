﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 投票券コード(エントリー制)
--	Progaram ID		: ElectionVoteTicketCd
--  Creation Date	: 2013.12.10
--  Creater			: Y.Ikemiya
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;
using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class ElectionVoteTicketCd:DbSession {
	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}

		private string electionPeriodSeq;
		public string ElectionPeriodSeq {
			get {
				return this.electionPeriodSeq;
			}
			set {
				this.electionPeriodSeq = value;
			}
		}

		private string sortDirection;
		public string SortDirection {
			get {
				return this.sortDirection;
			}
			set {
				this.sortDirection = value;
			}
		}

		private string sortExpression;
		public string SortExpression {
			get {
				return this.sortExpression;
			}
			set {
				this.sortExpression = value;
			}
		}
	}

	public ElectionVoteTicketCd() {
	}

	public int GetPageCount(SearchCondition pSearchCondition) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	COUNT(*)						");
		oSqlBuilder.AppendLine("FROM								");
		oSqlBuilder.AppendLine("	VW_ELECTION_VOTE_TICKET_CD01	");
		oSqlBuilder.AppendLine(sWhereClause);

		return ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
	}

	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));
		sSortExpression = this.GetOrder(pSearchCondition);

		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	SITE_CD							,	");
		oSqlBuilder.AppendLine("	ELECTION_PERIOD_SEQ				,	");
		oSqlBuilder.AppendLine("	CODE_STR						,	");
		oSqlBuilder.AppendLine("	TICKET_COUNT					,	");
		oSqlBuilder.AppendLine("	CREATE_DATE						,	");
		oSqlBuilder.AppendLine("	USED_FLAG						,	");
		oSqlBuilder.AppendLine("	USED_DATE						,	");
		oSqlBuilder.AppendLine("	LOGIN_ID						,	");
		oSqlBuilder.AppendLine("	HANDLE_NM							");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	VW_ELECTION_VOTE_TICKET_CD01		");
		oSqlBuilder.AppendLine(sWhereClause);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		return ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.ElectionPeriodSeq)) {
			SysPrograms.SqlAppendWhere("ELECTION_PERIOD_SEQ = :ELECTION_PERIOD_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":ELECTION_PERIOD_SEQ",pSearchCondition.ElectionPeriodSeq));
		}

		return oParamList.ToArray();
	}

	private string GetOrder(SearchCondition pSearchCondition) {
		if (string.IsNullOrEmpty(pSearchCondition.SortExpression)) {
			return "ORDER BY SITE_CD,ELECTION_PERIOD_SEQ,CREATE_DATE ASC,CODE_STR ASC";
		} else {
			return string.Format("ORDER BY {0} {1},SITE_CD,ELECTION_PERIOD_SEQ",pSearchCondition.SortExpression,pSearchCondition.SortDirection);
		}
	}
}
