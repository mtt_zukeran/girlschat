﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: ページビュー集計対象
--	Progaram ID		: CastPageViewDaily
--  Creation Date	: 2017.02.02
--  Creater			: M&TT Zukeran
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;
using iBridCommLib;
using ViComm;
using System.Collections.Generic;
using ViComm.Extension.Pwild;

public class PageViewTarget:DbSession {
	// 検索項目
	public class SearchCondition {
		public string SiteCd;
		public string ProgramNm;
		public string ProgramId;
		public string HtmlDocType;
		public string SexCd;

		public SearchCondition() {
			this.SiteCd = string.Empty;
			this.ProgramNm = string.Empty;
			this.ProgramId = string.Empty;
			this.HtmlDocType = string.Empty;
			this.SexCd = string.Empty;
		}
	}
	// 更新項目
	public class MainteData {
		public string SiteCd;
		public string ProgramRoot;
		public string ProgramId;
		public string HtmlDocType;
		public string SexCd;
		public string DelFlag;

		public MainteData() {
			this.SiteCd = string.Empty;
			this.ProgramRoot = string.Empty;
			this.ProgramId = string.Empty;
			this.HtmlDocType = string.Empty;
			this.SexCd = string.Empty;
			this.DelFlag = ViCommConst.FLAG_OFF_STR;
		}
	}

	/// <summary>
	/// コンストラクタ
	/// </summary>
	public PageViewTarget() {
	}

	/// <summary>
	/// 検索結果の件数を取得
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <returns></returns>
	public int GetPageCount(SearchCondition pSearchCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		string sFromClause = CreateFrom();
		string sWhereClause = string.Empty;

		OracleParameter[] oWhereParams = CreateWhere((SearchCondition)pSearchCondition,ref sWhereClause);

		oSqlBuilder.AppendLine("SELECT COUNT(*)");
		oSqlBuilder.AppendLine(sFromClause);
		oSqlBuilder.AppendLine(sWhereClause);

		return ExecuteSelectCountQueryBase(oSqlBuilder.ToString(),oWhereParams);
	}

	/// <summary>
	/// 検索結果のデータを取得
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <param name="startRowIndex"></param>
	/// <param name="maximumRows"></param>
	/// <returns></returns>
	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sExecSql = string.Empty;
		string sFromClause = CreateFrom();
		string sWhereClause = string.Empty;
		string sSortExpression = "ORDER BY P.PROGRAM_ROOT ASC,P.HTML_DOC_TYPE ASC,P.PROGRAM_ID ASC";
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(CreateWhere(pSearchCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT P.* ");
		oSqlBuilder.AppendLine(sFromClause);
		oSqlBuilder.AppendLine(sWhereClause);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		return ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
	}

	/// <summary>
	/// 検索用SQLのFrom句を生成する
	/// </summary>
	/// <returns></returns>
	private string CreateFrom() {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	((SELECT PV.SITE_CD");
		oSqlBuilder.AppendLine("		,'-' AS PROGRAM_ROOT");
		oSqlBuilder.AppendLine("		,'DisplayDoc.aspx' AS PROGRAM_ID");
		oSqlBuilder.AppendLine("		,MN.HTML_DOC_TYPE");
		oSqlBuilder.AppendLine("		,DC.HTML_DOC_TITLE AS PROGRAM_NM");
		oSqlBuilder.AppendLine("		,'-' AS SEX_NM");
		oSqlBuilder.AppendLine("	FROM T_PAGE_VIEW_TARGET PV");
		oSqlBuilder.AppendLine("		INNER JOIN T_SITE_HTML_MANAGE MN");
		oSqlBuilder.AppendLine("			ON PV.SITE_CD=MN.SITE_CD AND MN.HTML_DOC_GROUP='11' AND PV.HTML_DOC_TYPE=MN.HTML_DOC_TYPE");
		oSqlBuilder.AppendLine("		INNER JOIN T_SITE_HTML_DOC DC");
		oSqlBuilder.AppendLine("			ON MN.HTML_DOC_SEQ=DC.HTML_DOC_SEQ");
		oSqlBuilder.AppendLine("	WHERE MN.ROWID IN (SELECT MIN(ROWID)");
		oSqlBuilder.AppendLine("			FROM T_SITE_HTML_MANAGE");
		oSqlBuilder.AppendLine("			WHERE HTML_DOC_GROUP='11'");
		oSqlBuilder.AppendLine("			GROUP BY SITE_CD,HTML_DOC_TYPE)");
		oSqlBuilder.AppendLine("	) UNION (SELECT PV.SITE_CD");
		oSqlBuilder.AppendLine("		,PG.PROGRAM_ROOT");
		oSqlBuilder.AppendLine("		,PG.PROGRAM_ID");
		oSqlBuilder.AppendLine("		,'-' AS HTML_DOC_TYPE");
		oSqlBuilder.AppendLine("		,PG.PROGRAM_NM");
		oSqlBuilder.AppendLine(string.Format("		,CASE PG.PROGRAM_ROOT WHEN '{0}' THEN '男性' ELSE '女性' END AS SEX_NM",ViCommConst.PGM_ROOT_MAN));
		oSqlBuilder.AppendLine("	FROM T_PAGE_VIEW_TARGET PV");
		oSqlBuilder.AppendLine("		INNER JOIN T_PROGRAM PG");
		oSqlBuilder.AppendLine("			ON PV.PROGRAM_ROOT=PG.PROGRAM_ROOT AND PV.PROGRAM_ID=PG.PROGRAM_ID");
		oSqlBuilder.AppendLine("	) UNION (SELECT SITE_CD");
		oSqlBuilder.AppendLine("		,PROGRAM_ROOT");
		oSqlBuilder.AppendLine("		,PROGRAM_ID");
		oSqlBuilder.AppendLine("		,HTML_DOC_TYPE");
		oSqlBuilder.AppendLine("		,'-' AS PROGRAM_NM");
		oSqlBuilder.AppendLine("		,'-' AS SEX_NM");
		oSqlBuilder.AppendLine("	FROM T_PAGE_VIEW_TARGET");
		oSqlBuilder.AppendLine("	WHERE PROGRAM_ROOT='-'");
		oSqlBuilder.AppendLine("		AND HTML_DOC_TYPE='-'");
		oSqlBuilder.AppendLine("		AND PROGRAM_ID IN ('DisplayDoc.aspx','GameDisplayDoc.aspx')");
		oSqlBuilder.AppendLine("	)) P");
		return oSqlBuilder.ToString();
	}

	/// <summary>
	/// 検索条件を生成
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <param name="pWhereClause"></param>
	/// <returns></returns>
	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		// サイトCD
		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pSearchCondition.SiteCd));
		}

		// プログラムID
		if (!string.IsNullOrEmpty(pSearchCondition.ProgramId)) {
			SysPrograms.SqlAppendWhere("P.PROGRAM_ID Like :PROGRAM_ID",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PROGRAM_ID",string.Format("{0}%",pSearchCondition.ProgramId)));

			// 性別（プログラムルート）
			if (!string.IsNullOrEmpty(pSearchCondition.SexCd)) {
				string sProgramRoot = string.Empty;
				if (ViCommConst.MAN.Equals(pSearchCondition.SexCd)) {
					sProgramRoot = ViCommConst.PGM_ROOT_MAN;
				} else {
					sProgramRoot = ViCommConst.PGM_ROOT_WOMAN;
				}
				SysPrograms.SqlAppendWhere("P.PROGRAM_ROOT = :PROGRAM_ROOT",ref pWhereClause);
				oParamList.Add(new OracleParameter(":PROGRAM_ROOT",sProgramRoot));
			}
		}

		// 画面名称
		if (!string.IsNullOrEmpty(pSearchCondition.ProgramNm)) {
			SysPrograms.SqlAppendWhere("P.PROGRAM_NM Like :PROGRAM_NM",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PROGRAM_NM",string.Format("%{0}%",pSearchCondition.ProgramNm)));
		}

		// HTML文章コード
		if (!string.IsNullOrEmpty(pSearchCondition.HtmlDocType)) {
			SysPrograms.SqlAppendWhere("P.HTML_DOC_TYPE Like :HTML_DOC_TYPE",ref pWhereClause);
			oParamList.Add(new OracleParameter(":HTML_DOC_TYPE",string.Format("%{0}%",pSearchCondition.HtmlDocType)));
		}

		return oParamList.ToArray();
	}

	/// <summary>
	/// 更新処理
	/// </summary>
	/// <param name="pParam"></param>
	/// <param name="pStatus"></param>
	public void Mainte(MainteData pParam,out string pStatus) {
		// 静的ページが指定されてる場合
		if (!string.IsNullOrEmpty(pParam.HtmlDocType)
			|| pParam.ProgramId.Equals("DisplayDoc.aspx")
			|| pParam.ProgramId.Equals("GameDisplayDoc.aspx")
		) {
			pParam.ProgramRoot = "-";
			if (string.IsNullOrEmpty(pParam.HtmlDocType)) {
				pParam.HtmlDocType = "-";
			}
		}
		// 動的ページが指定されてる場合
		else if (string.IsNullOrEmpty(pParam.ProgramRoot)) {
			if (pParam.SexCd.Equals(ViCommConst.MAN)) {
				pParam.ProgramRoot = ViCommConst.PGM_ROOT_MAN;
			} else if (!string.IsNullOrEmpty(pParam.SexCd)) {
				pParam.ProgramRoot = ViCommConst.PGM_ROOT_WOMAN;
			}
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("PAGE_VIEW_TARGET_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pParam.SiteCd);
			oDbSession.ProcedureInParm("pPROGRAM_ROOT",DbSession.DbType.VARCHAR2,pParam.ProgramRoot);
			oDbSession.ProcedureInParm("pPROGRAM_ID",DbSession.DbType.VARCHAR2,pParam.ProgramId);
			oDbSession.ProcedureInParm("pHTML_DOC_TYPE",DbSession.DbType.VARCHAR2,pParam.HtmlDocType);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureInParm("pDEL_FLAG",DbSession.DbType.NUMBER,pParam.DelFlag);
			oDbSession.ExecuteProcedure();
			pStatus = oDbSession.GetStringValue("pSTATUS");
		}
	}

	/// <summary>
	/// 登録・更新対象のページが存在するかを判定して返す
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <returns></returns>
	public bool IsExistsTargetPage(SearchCondition pSearchCondition) {
		// 静的ページのページ番号の指定なし
		if ((pSearchCondition.ProgramId.Equals("DisplayDoc.aspx")
			|| pSearchCondition.ProgramId.Equals("GameDisplayDoc.aspx"))
			&& string.IsNullOrEmpty(pSearchCondition.HtmlDocType)
		) {
			return true;
		}

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT COUNT(*) FROM VW_PAGE01 WHERE SITE_CD=:SITE_CD AND PROGRAM_ID=:PROGRAM_ID");
		oParamList.Add(new OracleParameter(":SITE_CD",pSearchCondition.SiteCd));
		oParamList.Add(new OracleParameter(":PROGRAM_ID",pSearchCondition.ProgramId));

		if (!string.IsNullOrEmpty(pSearchCondition.HtmlDocType)) {
			oSqlBuilder.AppendLine(" AND HTML_DOC_TYPE=:HTML_DOC_TYPE");
			oParamList.Add(new OracleParameter(":HTML_DOC_TYPE",pSearchCondition.HtmlDocType));
		}
		else if (!string.IsNullOrEmpty(pSearchCondition.SexCd)) {
			oSqlBuilder.AppendLine(" AND PROGRAM_ROOT=:PROGRAM_ROOT");
			if (pSearchCondition.SexCd.Equals(ViCommConst.MAN)) {
				oParamList.Add(new OracleParameter(":PROGRAM_ROOT",ViCommConst.PGM_ROOT_MAN));
			} else {
				oParamList.Add(new OracleParameter(":PROGRAM_ROOT",ViCommConst.PGM_ROOT_WOMAN));
			}
		}

		int iCount = ExecuteSelectCountQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		return (iCount > 0);
	}
}
