﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: プレゼントメールアイテム設定
--	Progaram ID		: PresentMailItem
--  Creation Date	: 2014.12.01
--  Creater			: Y.Ikemiya
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;
using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class PresentMailItem:DbSession {
	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}

		private string presentMailTermSeq;
		public string PresentMailTermSeq {
			get {
				return this.presentMailTermSeq;
			}
			set {
				this.presentMailTermSeq = value;
			}
		}

		private string sortDirection;
		public string SortDirection {
			get {
				return this.sortDirection;
			}
			set {
				this.sortDirection = value;
			}
		}

		private string sortExpression;
		public string SortExpression {
			get {
				return this.sortExpression;
			}
			set {
				this.sortExpression = value;
			}
		}
	}

	public PresentMailItem() {
	}

	public int GetPageCount(SearchCondition pSearchCondition) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT						");
		oSqlBuilder.AppendLine("	COUNT(*)				");
		oSqlBuilder.AppendLine("FROM						");
		oSqlBuilder.AppendLine("	T_PRESENT_MAIL_ITEM		");
		oSqlBuilder.AppendLine(sWhereClause);

		return ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
	}

	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));
		sSortExpression = this.GetOrder(pSearchCondition);

		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	SITE_CD							,	");
		oSqlBuilder.AppendLine("	PRESENT_MAIL_TERM_SEQ			,	");
		oSqlBuilder.AppendLine("	PRESENT_MAIL_ITEM_SEQ			,	");
		oSqlBuilder.AppendLine("	CHARGE_POINT					,	");
		oSqlBuilder.AppendLine("	REWARD_POINT					,	");
		oSqlBuilder.AppendLine("	PRESENT_MAIL_ITEM_NM				");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	T_PRESENT_MAIL_ITEM					");
		oSqlBuilder.AppendLine(sWhereClause);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		return ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.PresentMailTermSeq)) {
			SysPrograms.SqlAppendWhere("PRESENT_MAIL_TERM_SEQ = :PRESENT_MAIL_TERM_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PRESENT_MAIL_TERM_SEQ",pSearchCondition.PresentMailTermSeq));
		}

		return oParamList.ToArray();
	}

	private string GetOrder(SearchCondition pSearchCondition) {
		return "ORDER BY SITE_CD,CHARGE_POINT ASC";
	}
}
