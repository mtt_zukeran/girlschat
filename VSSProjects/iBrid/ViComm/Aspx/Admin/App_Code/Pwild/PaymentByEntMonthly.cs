﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 入会月別報酬(月別)
--	Progaram ID		: PaymentByEntMonthly
--  Creation Date	: 2014.06.26
--  Creater			: Y.Ikemiya
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;
using iBridCommLib;
using ViComm;
using System.Collections.Generic;
using ViComm.Extension.Pwild;

public class PaymentByEntMonthly:DbSession {

	public PaymentByEntMonthly() {
	}

	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}

		private string entMonth;
		public string EntMonth {
			get {
				return this.entMonth;
			}
			set {
				this.entMonth = value;
			}
		}

		private string reportMonth;
		public string ReportMonth {
			get {
				return this.reportMonth;
			}
			set {
				this.reportMonth = value;
			}
		}
	}

	public DataSet GetSalesList(SearchCondition pSearchCondition) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	ENT_MONTH					,	");
		oSqlBuilder.AppendLine("	REPORT_MONTH				,	");
		oSqlBuilder.AppendLine("	PAYMENT_COUNT				,	");
		oSqlBuilder.AppendLine("	PAYMENT_AMT						");
		oSqlBuilder.AppendLine("FROM								");
		oSqlBuilder.AppendLine("	T_PAYMENT_BY_ENT_MONTHLY		");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("ORDER BY ENT_MONTH ASC,REPORT_MONTH ASC	");

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.EntMonth)) {
			SysPrograms.SqlAppendWhere("ENT_MONTH = :ENT_MONTH",ref pWhereClause);
			oParamList.Add(new OracleParameter(":ENT_MONTH",pSearchCondition.EntMonth));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.ReportMonth)) {
			SysPrograms.SqlAppendWhere("REPORT_MONTH = :REPORT_MONTH",ref pWhereClause);
			oParamList.Add(new OracleParameter(":REPORT_MONTH",pSearchCondition.ReportMonth));
		}

		return oParamList.ToArray();
	}
}
