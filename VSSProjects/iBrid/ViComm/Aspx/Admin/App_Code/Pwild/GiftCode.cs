﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: ギフトコード管理
--	Progaram ID		: GiftCode
--  Creation Date	: 2017.03.21
--  Creater			: M&TT Zukeran
--
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ViComm;
using iBridCommLib;

public class GiftCode:DbSession {

	#region PaymentStatus
	/// <summary>割当状態</summary>
	public class PaymentStatus {
		/// <summary>未割当</summary>
		public const string Unassigned = "0";
		/// <summary>仮割当 (割り当て処理中)</summary>
		public const string Temprary = "1";
		/// <summary>割当済み</summary>
		public const string Assigned = "2";
	}
	#endregion PaymentStatus

	#region DispRemainingNum
	/// <summary>残枚数表示</summary>
	public class DispRemainingNum {
		/// <summary>一括</summary>
		public const string All = "1";
		/// <summary>有効期限別</summary>
		public const string Expiration = "2";
		/// <summary>非表示</summary>
		public const string Hide = "3";
	}
	#endregion DispRemainingNum

	#region GiftCodeType
	/// <summary>ギフトコード種類</summary>
	public class GiftCodeType {
		/// <summary>Amazonギフト券</summary>
		public const string Amazon = "1";
	}
	#endregion GiftCodeType

	#region SearchCondition
	public class SearchCondition {
		/// <summary>表示日付(年)</summary>
		public string Year;
		/// <summary>表示日付(月)</summary>
		public string Month;
		/// <summary>残枚数の表示</summary>
		public string DispRemainingNum;
		/// <summary>ギフトコード種類</summary>
		public string GiftCodeType;

		public SearchCondition() {
			this.Year = string.Empty;
			this.Month = string.Empty;
			this.DispRemainingNum = string.Empty;
			this.GiftCodeType = string.Empty;
		}
	}
	#endregion SearchCondition

	public GiftCode() {
	}


	/// <summary>
	/// 条件設定
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <param name="pWhere"></param>
	/// <returns></returns>
	public OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhere) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		// 表示日付(年月のみ)
		if (!string.IsNullOrEmpty(pSearchCondition.Year) && !string.IsNullOrEmpty(pSearchCondition.Month)) {
			string sDate = string.Format("{0:D4}-{1:D2}-01",pSearchCondition.Year,pSearchCondition.Month);
			DateTime oFromDate = DateTime.Today;
			if (DateTime.TryParse(sDate,out oFromDate)) {
				SysPrograms.SqlAppendWhere("PAYMENT_DATE >= TO_DATE(:PAYMENT_DATE_FROM, 'YYYY-MM-DD')",ref pWhere);
				SysPrograms.SqlAppendWhere("PAYMENT_DATE < TO_DATE(:PAYMENT_DATE_TO, 'YYYY-MM-DD')",ref pWhere);
				oParamList.Add(new OracleParameter("PAYMENT_DATE_FROM",oFromDate.ToString("yyyy-MM-dd")));
				oParamList.Add(new OracleParameter("PAYMENT_DATE_TO",oFromDate.AddMonths(1).ToString("yyyy-MM-dd")));
			}
		}

		// 残枚数の表示
		if (!string.IsNullOrEmpty(pSearchCondition.DispRemainingNum)) {
			SysPrograms.SqlAppendWhere("PAYMENT_STATUS = :PAYMENT_STATUS",ref pWhere);
			oParamList.Add(new OracleParameter("PAYMENT_STATUS",PaymentStatus.Unassigned));
		} else {
			SysPrograms.SqlAppendWhere("PAYMENT_STATUS = :PAYMENT_STATUS",ref pWhere);
			oParamList.Add(new OracleParameter("PAYMENT_STATUS",PaymentStatus.Assigned));
		}

		// ギフトコード種類
		if (!string.IsNullOrEmpty(pSearchCondition.GiftCodeType)) {
		    SysPrograms.SqlAppendWhere("GIFT_CODE_TYPE = :GIFT_CODE_TYPE",ref pWhere);
		    oParamList.Add(new OracleParameter("GIFT_CODE_TYPE",pSearchCondition.GiftCodeType));
		}

		return oParamList.ToArray();
	}

	/// <summary>
	/// ギフト券履歴 件数取得
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <param name="startRowIndex"></param>
	/// <param name="maximumRows"></param>
	/// <returns></returns>
	public int GetPageCount(SearchCondition pSearchCondition) {
		string sWhere = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(CreateWhere(pSearchCondition,ref sWhere));

		oSqlBuilder.AppendLine("SELECT COUNT(*) FROM T_GIFT_CODE_MANAGE");
		oSqlBuilder.AppendLine(sWhere);

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return int.Parse(oDataSet.Tables[0].Rows[0][0].ToString());
	}

	/// <summary>
	/// ギフト券履歴 データ取得
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <param name="startRowIndex"></param>
	/// <param name="maximumRows"></param>
	/// <returns></returns>
	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sWhere = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		bool bFlag = false;

		oParamList.AddRange(CreateWhere(pSearchCondition,ref sWhere));

		if (!string.IsNullOrEmpty(pSearchCondition.Year) && !string.IsNullOrEmpty(pSearchCondition.Month)) {
			string sDate = string.Format("{0:D4}-{1:D2}-01",pSearchCondition.Year,pSearchCondition.Month);
			DateTime oFromDate = DateTime.Today;
			if (DateTime.TryParse(sDate,out oFromDate)) {
				bFlag = true;
				oSqlBuilder.AppendLine("SELECT");
				oSqlBuilder.AppendLine("	DT.DISPLAY_DATE");
				oSqlBuilder.AppendLine("	,TO_CHAR(DT.DISPLAY_DATE,'DD') AS DISPLAY_DAY");
				oSqlBuilder.AppendLine("	,TO_CHAR(DT.DISPLAY_DATE,'Dy','NLS_DATE_LANGUAGE = ENGLISH') AS DISPLAY_DAY_OF_WEEK");
				oSqlBuilder.AppendLine("	,NVL(P.PAYMENT_CNT,0) AS PAYMENT_CNT");
				oSqlBuilder.AppendLine("FROM");
				oSqlBuilder.AppendLine("	(");
				oSqlBuilder.AppendLine("		SELECT");
				oSqlBuilder.AppendLine("			STARTDATE + ROWNUM -1 AS DISPLAY_DATE");
				oSqlBuilder.AppendLine("		FROM (");
				oSqlBuilder.AppendLine("			SELECT");
				oSqlBuilder.AppendLine("				TRUNC(TO_DATE(:CATALOG_DATE,'YYYY-MM-DD')) AS STARTDATE");
				oSqlBuilder.AppendLine("				,TO_NUMBER(TO_CHAR(LAST_DAY(TO_DATE(:CATALOG_DATE,'YYYY-MM-DD')), 'DD')) AS DAYS");
				oSqlBuilder.AppendLine("			FROM DUAL");
				oSqlBuilder.AppendLine("		), ALL_CATALOG");
				oSqlBuilder.AppendLine("		WHERE ROWNUM <= DAYS");
				oSqlBuilder.AppendLine("	) DT");

				oParamList.Add(new OracleParameter("CATALOG_DATE",oFromDate.ToString("yyyy-MM-dd")));

				oSqlBuilder.AppendLine("	LEFT JOIN (");
				oSqlBuilder.AppendLine("		SELECT");
				oSqlBuilder.AppendLine("			TRUNC(PAYMENT_DATE) AS DISPLAY_DATE");
				oSqlBuilder.AppendLine("			,COUNT(*) AS PAYMENT_CNT");
				oSqlBuilder.AppendLine("		FROM T_GIFT_CODE_MANAGE");
				oSqlBuilder.AppendLine(sWhere);
				oSqlBuilder.AppendLine("		GROUP BY TRUNC(PAYMENT_DATE)");
				oSqlBuilder.AppendLine("	) P");
				oSqlBuilder.AppendLine("	ON DT.DISPLAY_DATE=P.DISPLAY_DATE");
				oSqlBuilder.AppendLine("ORDER BY DT.DISPLAY_DATE ASC");
			}
		}
		if (!bFlag) {
			oSqlBuilder.AppendLine("SELECT");
			oSqlBuilder.AppendLine("	DISPLAY_DATE");
			oSqlBuilder.AppendLine("	,TO_CHAR(DISPLAY_DATE,'DD') AS DISPLAY_DAY");
			oSqlBuilder.AppendLine("	,TO_CHAR(DISPLAY_DATE,'Dy','NLS_DATE_LANGUAGE = ENGLISH') AS DISPLAY_DAY_OF_WEEK");
			oSqlBuilder.AppendLine("	,P.PAYMENT_CNT");
			oSqlBuilder.AppendLine("FROM (");
			oSqlBuilder.AppendLine("	SELECT");
			oSqlBuilder.AppendLine("		TRUNC(PAYMENT_DATE) AS DISPLAY_DATE");
			oSqlBuilder.AppendLine("		,COUNT(*) AS PAYMENT_CNT");
			oSqlBuilder.AppendLine("	FROM T_GIFT_CODE_MANAGE");
			oSqlBuilder.AppendLine(sWhere);
			oSqlBuilder.AppendLine("	GROUP BY TRUNC(PAYMENT_DATE)");
			oSqlBuilder.AppendLine(") P");
			oSqlBuilder.AppendLine("ORDER BY P.DISPLAY_DATE ASC");
		}

		return ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
	}

	/// <summary>
	/// ギフト券残数 データ取得
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <param name="startRowIndex"></param>
	/// <param name="maximumRows"></param>
	/// <returns></returns>
	public DataSet GetRemainingCnt(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sWhere = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		DispRemainingNum oDispRemainingNum = new DispRemainingNum();

		oParamList.AddRange(CreateWhere(pSearchCondition,ref sWhere));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*) AS CNT");
		if (pSearchCondition.DispRemainingNum.Equals(DispRemainingNum.Expiration)) {
			oSqlBuilder.AppendLine(",EXPIRATION_DATE");
		}
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_GIFT_CODE_MANAGE");
		oSqlBuilder.AppendLine(sWhere);
		if (pSearchCondition.DispRemainingNum.Equals(DispRemainingNum.Expiration)) {
			oSqlBuilder.AppendLine("GROUP BY EXPIRATION_DATE");
		}

		sSortExpression = "ORDER BY EXPIRATION_DATE ASC";
		oSqlBuilder.AppendLine(sSortExpression);

		return ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
	}
	public DataSet GetRemainingCnt(SearchCondition pSearchCondition) {
		return this.GetRemainingCnt(pSearchCondition,0,0);
	}

	/// <summary>
	/// 支払方法の選択肢を設定
	/// </summary>
	/// <param name="pDropDownList"></param>
	/// <returns></returns>
	public void SetPaymentMethod(DropDownList pDropDownList) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		// 初期値設定
		pDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		pDropDownList.Items.Insert(1,new ListItem("現金振込","0"));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	GIFT_CODE_TYPE");
		oSqlBuilder.AppendLine("	,GIFT_CODE_TYPE_NM");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_GIFT_CODE_TYPE_MANAGE");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	PUBLISH_FLAG = 1");
		oSqlBuilder.AppendLine("ORDER BY");
		oSqlBuilder.AppendLine("	GIFT_CODE_TYPE ASC");

		DataSet oDS = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		if (oDS.Tables[0].Rows.Count > 0) {
			for (int i=0;i<oDS.Tables[0].Rows.Count;i++) {
				pDropDownList.Items.Insert(2+i,new ListItem(
					oDS.Tables[0].Rows[i]["GIFT_CODE_TYPE_NM"].ToString()
					,oDS.Tables[0].Rows[i]["GIFT_CODE_TYPE"].ToString()
				));
			}
		}
	}
}
