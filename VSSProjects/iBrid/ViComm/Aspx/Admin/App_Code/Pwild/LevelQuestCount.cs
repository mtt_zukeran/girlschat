﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: レベルクエストクリア数--	Progaram ID		: LevelQuestCount
--
--  Creation Date	: 2012.12.05
--  Creater			: M&TT A.Taba
--
**************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class LevelQuestCount:DbSession {

	public LevelQuestCount() {
	}

	public DataSet GetList(string pSiteCd,string pQuestSeq,string pSexCd) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	SITE_CD					,		");
		oSqlBuilder.AppendLine("	QUEST_SEQ				,		");
		oSqlBuilder.AppendLine("	LEVEL_QUEST_SEQ			,		");
		oSqlBuilder.AppendLine("	SEX_CD					,		");
		oSqlBuilder.AppendLine("	QUEST_LEVEL				,		");
		oSqlBuilder.AppendLine("	QUEST_EX_REMARKS				");
		oSqlBuilder.AppendLine("FROM								");
		oSqlBuilder.AppendLine("	VW_PW_LEVEL_QUEST01				");
		oSqlBuilder.AppendLine("WHERE								");
		oSqlBuilder.AppendLine("	SITE_CD		= :SITE_CD		AND	");
		oSqlBuilder.AppendLine("	QUEST_SEQ	= :QUEST_SEQ	AND	");
		oSqlBuilder.AppendLine("	SEX_CD	    = :SEX_CD		    ");
		oSqlBuilder.AppendLine("ORDER BY QUEST_LEVEL                ");
		
		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":QUEST_SEQ",pQuestSeq);
				cmd.Parameters.Add(":SEX_CD",pSexCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}

		AppendCount(oDs,pSiteCd,pQuestSeq);

		return oDs;
	}

	private void AppendCount(DataSet pDS,string pSiteCd,string pQuestSeq) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;

		col = new DataColumn(string.Format("QUEST_LEVEL_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		col = new DataColumn(string.Format("QUEST_EX_ENTRY_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		col = new DataColumn(string.Format("QUEST_EX_RETIRE_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		col = new DataColumn(string.Format("QUEST_EX_CLEAR_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			StringBuilder oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine("SELECT											 ");
			oSqlBuilder.AppendLine("	COUNT(*) AS QUEST_LEVEL_COUNT				 ");
			oSqlBuilder.AppendLine(" FROM							   				 ");
			oSqlBuilder.AppendLine("	T_LEVEL_QUEST_CLR_HISTORY		     		 ");
			oSqlBuilder.AppendLine(" WHERE											 ");
			oSqlBuilder.AppendLine("	SITE_CD		         = :SITE_CD		AND		 ");
			oSqlBuilder.AppendLine("	QUEST_SEQ		     = :QUEST_SEQ	AND		 ");
			oSqlBuilder.AppendLine("	LEVEL_QUEST_SEQ      = :LEVEL_QUEST_SEQ      ");

			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
					cmd.BindByName = true;

					cmd.Parameters.Add(":SITE_CD",pSiteCd);
					cmd.Parameters.Add(":QUEST_SEQ",pQuestSeq);
					cmd.Parameters.Add(":LEVEL_QUEST_SEQ",dr["LEVEL_QUEST_SEQ"].ToString());

					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub);
					}

					dr["QUEST_LEVEL_COUNT"] = dsSub.Tables[0].Rows[0]["QUEST_LEVEL_COUNT"];
				}
			}

			oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine("SELECT											 ");
			oSqlBuilder.AppendLine("	COUNT(*) AS QUEST_EX_ENTRY_COUNT			 ");
			oSqlBuilder.AppendLine(" FROM							   				 ");
			oSqlBuilder.AppendLine("	T_QUEST_ENTRY_LOG				     		 ");
			oSqlBuilder.AppendLine(" WHERE											 ");
			oSqlBuilder.AppendLine("	SITE_CD		         = :SITE_CD		    AND	 ");
			oSqlBuilder.AppendLine("	QUEST_SEQ		     = :QUEST_SEQ	    AND	 ");
			oSqlBuilder.AppendLine("	LEVEL_QUEST_SEQ      = :LEVEL_QUEST_SEQ AND  ");
			oSqlBuilder.AppendLine("	QUEST_TYPE           = 2                     ");

			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
					cmd.BindByName = true;

					cmd.Parameters.Add(":SITE_CD",pSiteCd);
					cmd.Parameters.Add(":QUEST_SEQ",pQuestSeq);
					cmd.Parameters.Add(":LEVEL_QUEST_SEQ",dr["LEVEL_QUEST_SEQ"].ToString());

					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub);
					}

					dr["QUEST_EX_ENTRY_COUNT"] = dsSub.Tables[0].Rows[0]["QUEST_EX_ENTRY_COUNT"];
				}
			}

			oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine("SELECT											 ");
			oSqlBuilder.AppendLine("	COUNT(*) AS QUEST_EX_RETIRE_COUNT			 ");
			oSqlBuilder.AppendLine(" FROM							   				 ");
			oSqlBuilder.AppendLine("	T_QUEST_ENTRY_LOG				     		 ");
			oSqlBuilder.AppendLine(" WHERE											 ");
			oSqlBuilder.AppendLine("	SITE_CD		         = :SITE_CD		    AND	 ");
			oSqlBuilder.AppendLine("	QUEST_SEQ		     = :QUEST_SEQ	    AND	 ");
			oSqlBuilder.AppendLine("	LEVEL_QUEST_SEQ      = :LEVEL_QUEST_SEQ AND  ");
			oSqlBuilder.AppendLine("	QUEST_TYPE           = 2				AND  ");
			oSqlBuilder.AppendLine("	QUEST_STATUS         = 2				     ");

			
			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
					cmd.BindByName = true;

					cmd.Parameters.Add(":SITE_CD",pSiteCd);
					cmd.Parameters.Add(":QUEST_SEQ",pQuestSeq);
					cmd.Parameters.Add(":LEVEL_QUEST_SEQ",dr["LEVEL_QUEST_SEQ"].ToString());

					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub);
					}

					dr["QUEST_EX_RETIRE_COUNT"] = dsSub.Tables[0].Rows[0]["QUEST_EX_RETIRE_COUNT"];
				}
			}

			oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine("SELECT											 ");
			oSqlBuilder.AppendLine("	COUNT(*) AS QUEST_EX_CLEAR_COUNT			 ");
			oSqlBuilder.AppendLine(" FROM							   				 ");
			oSqlBuilder.AppendLine("	T_QUEST_ENTRY_LOG				     		 ");
			oSqlBuilder.AppendLine(" WHERE											 ");
			oSqlBuilder.AppendLine("	SITE_CD		         = :SITE_CD		    AND	 ");
			oSqlBuilder.AppendLine("	QUEST_SEQ		     = :QUEST_SEQ	    AND	 ");
			oSqlBuilder.AppendLine("	LEVEL_QUEST_SEQ      = :LEVEL_QUEST_SEQ AND  ");
			oSqlBuilder.AppendLine("	QUEST_TYPE           = 2                AND  ");
			oSqlBuilder.AppendLine("	QUEST_STATUS         = 3				     ");


			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
					cmd.BindByName = true;

					cmd.Parameters.Add(":SITE_CD",pSiteCd);
					cmd.Parameters.Add(":QUEST_SEQ",pQuestSeq);
					cmd.Parameters.Add(":LEVEL_QUEST_SEQ",dr["LEVEL_QUEST_SEQ"].ToString());

					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub);
					}

					dr["QUEST_EX_CLEAR_COUNT"] = dsSub.Tables[0].Rows[0]["QUEST_EX_CLEAR_COUNT"];
				}
			}
		}
	}

	public string GetQuestLevel(string pSiteCd,string pQuestSeq,string pLevelQuestSeq) {
		string sValue = null;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	QUEST_LEVEL												");
		oSqlBuilder.AppendLine(" FROM							   							");
		oSqlBuilder.AppendLine("	T_LEVEL_QUEST											");
		oSqlBuilder.AppendLine(" WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD				AND		");
		oSqlBuilder.AppendLine("	QUEST_SEQ				= :QUEST_SEQ			AND		");
		oSqlBuilder.AppendLine("	LEVEL_QUEST_SEQ			= :LEVEL_QUEST_SEQ   			");

		try {
			this.conn = this.DbConnect();

			using (this.cmd = this.CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.Add(":SITE_CD",pSiteCd);
				this.cmd.Parameters.Add(":QUEST_SEQ",pQuestSeq);
				this.cmd.Parameters.Add(":LEVEL_QUEST_SEQ",pLevelQuestSeq);

				OracleDataReader oDataReader = this.cmd.ExecuteReader();

				if (oDataReader.Read()) {
					sValue = iBridUtil.GetStringValue(oDataReader["QUEST_LEVEL"].ToString());
				}
			}
		} finally {
			this.conn.Close();
		}

		return sValue;
	}
}
