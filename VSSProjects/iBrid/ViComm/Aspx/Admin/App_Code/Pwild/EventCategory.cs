﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: イベント大カテゴリ設定--	Progaram ID		: EventCategory
--
--  Creation Date	: 2013.07.04
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class EventCategory:DbSession {
	public EventCategory() {
	}

	public int GetPageCount(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT COUNT(*) FROM T_EVENT_CATEGORY WHERE SITE_CD = :SITE_CD");

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT							");
		oSqlBuilder.AppendLine("	SITE_CD					,	");
		oSqlBuilder.AppendLine("	EVENT_CATEGORY_SEQ		,	");
		oSqlBuilder.AppendLine("	EVENT_CATEGORY_NM			");
		oSqlBuilder.AppendLine("FROM							");
		oSqlBuilder.AppendLine("	T_EVENT_CATEGORY			");
		oSqlBuilder.AppendLine("WHERE							");
		oSqlBuilder.AppendLine("	SITE_CD	= :SITE_CD			");

		string sPagingSql = string.Empty;
		string sSortExpression = "ORDER BY SITE_CD, EVENT_CATEGORY_SEQ";

		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}

	public DataSet GetList(string pSiteCd) {

		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();
			StringBuilder oSql = new StringBuilder();
			List<OracleParameter> oParameterList = new List<OracleParameter>();

			string sOrder = "ORDER BY SITE_CD,EVENT_CATEGORY_SEQ";
			oSql.AppendLine("SELECT						");
			oSql.AppendLine("	EVENT_CATEGORY_SEQ	,	");
			oSql.AppendLine("	EVENT_CATEGORY_NM		");
			oSql.AppendLine("FROM						");
			oSql.AppendLine("	T_EVENT_CATEGORY		");
			oSql.AppendLine("WHERE						");
			oSql.AppendLine("	SITE_CD	= :SITE_CD		");

			oParameterList.Add(new OracleParameter(":SITE_CD",pSiteCd));

			oSql.AppendLine(sOrder);

			using (cmd = CreateSelectCommand(oSql.ToString(),conn)) {
				cmd.Parameters.AddRange(oParameterList.ToArray());
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_EVENT_CATEGORY");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
}