﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: Fromアドレスエイリアス設定
--	Progaram ID		: FromAddressAlias
--  Creation Date	: 2015.07.21
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

public class FromAddressAlias:DbSession {
	public FromAddressAlias() {
	}

	public int GetPageCount() {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_FROM_ADDRESS_ALIAS");

		return ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
	}

	public DataSet GetPageCollection(int startRowIndex,int maximumRows) {
		string sExecSql = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	FROM_ADDRESS_ALIAS_SEQ,");
		oSqlBuilder.AppendLine("	ALIAS_NAME,");
		oSqlBuilder.AppendLine("	USED_NOW_FLAG");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_FROM_ADDRESS_ALIAS");

		string sSortExpression = "ORDER BY FROM_ADDRESS_ALIAS_SEQ";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		return ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
	}
}
