﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 会員つぶやきコメント数--	Progaram ID		: ManTweetCommentCnt
--
--  Creation Date	: 2013.03.27
--  Creater			: Y.Ikemiya
--
**************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

public class ManTweetCommentCnt:DbSession {
	private const int DISP_RANK_TYPE_MANAGE	= 0; // ランキング表示順位(管理画面)
	private const int DISP_RANK_TYPE_NOW	= 1; // ランキング表示順位(現在ランキング)
	private const int DISP_RANK_TYPE_PAST	= 2; // ランキング表示順位(過去ランキング)

	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}
		private string termSeq;
		public string TermSeq {
			get {
				return this.termSeq;
			}
			set {
				this.termSeq = value;
			}
		}
	}

	public ManTweetCommentCnt() {
	}

	public int GetPageCount(SearchCondition pSearchCondition) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	(");
		oSqlBuilder.AppendLine("		SELECT");
		oSqlBuilder.AppendLine("			DENSE_RANK() OVER(");
		oSqlBuilder.AppendLine("				PARTITION BY SITE_CD,TERM_SEQ");
		oSqlBuilder.AppendLine("				ORDER BY COMMENT_COUNT DESC,LAST_COMMENT_DATE ASC");
		oSqlBuilder.AppendLine("			) AS RANK_NO");
		oSqlBuilder.AppendLine("			,DISP_RANK_SEQ");
		oSqlBuilder.AppendLine("		FROM");
		oSqlBuilder.AppendLine("		VW_MAN_TWEET_COMMENT_CNT01");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("	) SUB");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	RANK_NO IN (");
		oSqlBuilder.AppendLine("		SELECT");
		oSqlBuilder.AppendLine("			DISP_RANK_NO");
		oSqlBuilder.AppendLine("		FROM");
		oSqlBuilder.AppendLine("			T_TWEET_COMMENT_DISP_RANK");
		oSqlBuilder.AppendLine("		WHERE");
		oSqlBuilder.AppendLine("			DISP_RANK_SEQ=SUB.DISP_RANK_SEQ");
		oSqlBuilder.AppendLine("			AND DISP_RANK_TYPE = :DISP_RANK_TYPE");
		oSqlBuilder.AppendLine("			AND SEX_CD = :DISP_SEX_CD");
		oSqlBuilder.AppendLine("	)");
		oParamList.Add(new OracleParameter("DISP_RANK_TYPE",DISP_RANK_TYPE_MANAGE));
		oParamList.Add(new OracleParameter("DISP_SEX_CD",ViCommConst.OPERATOR));

		return ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
	}

	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));
		string sSortExpression = this.GetOrder(pSearchCondition);

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SUB.*");
		oSqlBuilder.AppendLine("	,CASE");
		oSqlBuilder.AppendLine("		WHEN TCRH.REWARD_POINT IS NOT NULL	");
		oSqlBuilder.AppendLine("		THEN TCRH.REWARD_POINT				");
		oSqlBuilder.AppendLine("		ELSE TCRR.REWARD_POINT				");
		oSqlBuilder.AppendLine("	END AS REWARD_POINT");
		oSqlBuilder.AppendLine("	,CASE");
		oSqlBuilder.AppendLine("		WHEN SUB.TERM_END_DATE > SYSDATE	");
		oSqlBuilder.AppendLine("		THEN NULL");
		oSqlBuilder.AppendLine("		WHEN TCRH.USER_SEQ IS NOT NULL		");
		oSqlBuilder.AppendLine("		THEN '付与済み'");
		oSqlBuilder.AppendLine("		ELSE '付与'");
		oSqlBuilder.AppendLine("	END AS ADD_REWARD_STATUS");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	(");
		oSqlBuilder.AppendLine("	SELECT");
		oSqlBuilder.AppendLine("		DENSE_RANK() OVER(");
		oSqlBuilder.AppendLine("			PARTITION BY SITE_CD,TERM_SEQ");
		oSqlBuilder.AppendLine("			ORDER BY COMMENT_COUNT DESC,LAST_COMMENT_DATE ASC");
		oSqlBuilder.AppendLine("		) AS RANK_NO");
		oSqlBuilder.AppendLine("		,SITE_CD");
		oSqlBuilder.AppendLine("		,CAST_USER_SEQ");
		oSqlBuilder.AppendLine("		,CAST_CHAR_NO");
		oSqlBuilder.AppendLine("		,TERM_SEQ");
		oSqlBuilder.AppendLine("		,TERM_END_DATE");
		oSqlBuilder.AppendLine("		,COMMENT_COUNT");
		oSqlBuilder.AppendLine("		,LAST_COMMENT_DATE");
		oSqlBuilder.AppendLine("		,LOGIN_ID");
		oSqlBuilder.AppendLine("		,HANDLE_NM");
		oSqlBuilder.AppendLine("		,DISP_RANK_SEQ");
		oSqlBuilder.AppendLine("	FROM");
		oSqlBuilder.AppendLine("		VW_MAN_TWEET_COMMENT_CNT01");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("	) SUB");
		oSqlBuilder.AppendLine("	,T_TWEET_COMMENT_RANK_REWARD TCRR");
		oSqlBuilder.AppendLine("	,T_TWEET_COMMENT_RWRD_HSTRY TCRH ");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SUB.SITE_CD				= TCRR.SITE_CD			(+)");
		oSqlBuilder.AppendLine("	AND	SUB.RANK_NO			= TCRR.RANK_NO			(+)");
		oSqlBuilder.AppendLine("	AND	SUB.SITE_CD			= TCRH.SITE_CD			(+)");
		oSqlBuilder.AppendLine("	AND	SUB.CAST_USER_SEQ	= TCRH.USER_SEQ			(+)");
		oSqlBuilder.AppendLine("	AND	SUB.CAST_CHAR_NO	= TCRH.USER_CHAR_NO		(+)");
		oSqlBuilder.AppendLine("	AND	SUB.TERM_SEQ		= TCRH.TERM_SEQ			(+)");
		oSqlBuilder.AppendLine("	AND	SUB.RANK_NO IN (");
		oSqlBuilder.AppendLine("		SELECT");
		oSqlBuilder.AppendLine("			DISP_RANK_NO");
		oSqlBuilder.AppendLine("		FROM");
		oSqlBuilder.AppendLine("			T_TWEET_COMMENT_DISP_RANK");
		oSqlBuilder.AppendLine("		WHERE");
		oSqlBuilder.AppendLine("			DISP_RANK_SEQ=SUB.DISP_RANK_SEQ");
		oSqlBuilder.AppendLine("			AND DISP_RANK_TYPE = :DISP_RANK_TYPE");
		oSqlBuilder.AppendLine("			AND SEX_CD = :DISP_SEX_CD");
		oSqlBuilder.AppendLine("	)");
		oParamList.Add(new OracleParameter("DISP_RANK_TYPE",DISP_RANK_TYPE_MANAGE));
		oParamList.Add(new OracleParameter("DISP_SEX_CD",ViCommConst.OPERATOR));

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter("SITE_CD",pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.TermSeq)) {
			SysPrograms.SqlAppendWhere("TERM_SEQ = :TERM_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter("TERM_SEQ",pSearchCondition.TermSeq));
		}

		return oParamList.ToArray();
	}

	private string GetOrder(SearchCondition pSearchCondition) {
		return "ORDER BY SUB.RANK_NO ASC";
	}
}