﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 男性バトル練習用お宝設定--	Progaram ID		: ManTreasureBattle
--
--  Creation Date	: 2012.09.10
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class ManTreasureBattle:DbSession {
	public ManTreasureBattle() {
	}

	public int GetPageCount(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT COUNT(*) FROM VW_PW_MAN_TREASURE_BATTLE01 WHERE SITE_CD = :SITE_CD");

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	SITE_CD							,	");
		oSqlBuilder.AppendLine("	MAN_TREASURE_BATTLE_SEQ			,	");
		oSqlBuilder.AppendLine("	USER_SEQ						,	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO					,	");
		oSqlBuilder.AppendLine("	CAST_GAME_PIC_ATTR_SEQ			,	");
		oSqlBuilder.AppendLine("	CAST_GAME_PIC_ATTR_NM			,	");
		oSqlBuilder.AppendLine("	PIC_TITLE						,	");
		oSqlBuilder.AppendLine("	PIC_DOC							,	");
		oSqlBuilder.AppendLine("	UPDATE_DATE						,	");
		oSqlBuilder.AppendLine("	OBJ_TUTORIAL_TREASURE_IMG_PATH		");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	VW_PW_MAN_TREASURE_BATTLE01			");
		oSqlBuilder.AppendLine("WHERE									");
		oSqlBuilder.AppendLine("	SITE_CD	= :SITE_CD					");

		string sPagingSql = string.Empty;
		string sSortExpression = "ORDER BY SITE_CD, MAN_TREASURE_BATTLE_SEQ DESC";

		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}
}
