﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 会員つぶやきコメントランキング--	Progaram ID		: ManTweetCommentTerm
--
--  Creation Date	: 2013.03.26
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

public class ManTweetCommentTerm:DbSession {

	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}
	}

	public ManTweetCommentTerm() {
	}

	public int GetPageCount(SearchCondition pSearchCondition) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_MAN_TWEET_COMMENT_TERM");
		oSqlBuilder.AppendLine(sWhereClause);

		return ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());		
	}

	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));
		string sSortExpression = this.GetOrder(pSearchCondition);

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SITE_CD,");
		oSqlBuilder.AppendLine("	TERM_SEQ,");
		oSqlBuilder.AppendLine("	START_DATE,");
		oSqlBuilder.AppendLine("	END_DATE");
        oSqlBuilder.AppendLine("	,DISP_RANK_SEQ"); //2017.04.10 kyogi ランキング表示順位追加
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_MAN_TWEET_COMMENT_TERM");
		oSqlBuilder.AppendLine(sWhereClause);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	public bool IsDuplicateDate(string pSiteCd,string pTermSeq,DateTime pStartDate,DateTime pEndDate) {
		bool bDuplicate = false;

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("   1");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("   T_MAN_TWEET_COMMENT_TERM");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD	= :SITE_CD");
		oSqlBuilder.AppendLine("	AND (");
		oSqlBuilder.AppendLine("		(START_DATE	>= :START_DATE1 AND START_DATE	<= :END_DATE1) OR ");
		oSqlBuilder.AppendLine("		(END_DATE	>= :START_DATE2 AND END_DATE	<= :END_DATE2) OR ");
		oSqlBuilder.AppendLine("		(START_DATE	<= :START_DATE3 AND END_DATE	>= :END_DATE3)");
		oSqlBuilder.AppendLine("	)");

		if (!string.IsNullOrEmpty(pTermSeq)) {
			oSqlBuilder.AppendLine(" AND TERM_SEQ != :TERM_SEQ");
		}

		oParamList.Add(new OracleParameter("SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter("START_DATE1",OracleDbType.Date,pStartDate,ParameterDirection.Input));
		oParamList.Add(new OracleParameter("START_DATE2",OracleDbType.Date,pStartDate,ParameterDirection.Input));
		oParamList.Add(new OracleParameter("START_DATE3",OracleDbType.Date,pStartDate,ParameterDirection.Input));
		oParamList.Add(new OracleParameter("END_DATE1",OracleDbType.Date,pEndDate,ParameterDirection.Input));
		oParamList.Add(new OracleParameter("END_DATE2",OracleDbType.Date,pEndDate,ParameterDirection.Input));
		oParamList.Add(new OracleParameter("END_DATE3",OracleDbType.Date,pEndDate,ParameterDirection.Input));

		if (!string.IsNullOrEmpty(pTermSeq)) {
			oParamList.Add(new OracleParameter("TERM_SEQ",pTermSeq));
		}

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count > 0) {
			bDuplicate = true;
		}

		return bDuplicate;
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter("SITE_CD",pSearchCondition.SiteCd));
		}

		return oParamList.ToArray();
	}

	private string GetOrder(SearchCondition pSearchCondition) {
		return "ORDER BY START_DATE DESC";
	}

	public DataSet GetList(string pSiteCd,string pTermSeq) {

		StringBuilder oSql = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		string sOrder = "ORDER BY SITE_CD,START_DATE DESC";
		oSql.AppendLine("SELECT ");
		oSql.AppendLine("	START_DATE	,");
		oSql.AppendLine("	END_DATE	");
		oSql.AppendLine("FROM ");
		oSql.AppendLine("	T_MAN_TWEET_COMMENT_TERM	");
		oSql.AppendLine("WHERE");
		oSql.AppendLine("	SITE_CD		= :SITE_CD		AND	");
		oSql.AppendLine("	TERM_SEQ	= :TERM_SEQ			");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":TERM_SEQ",pTermSeq));

		oSql.AppendLine(sOrder);

		DataSet oDataSet = ExecuteSelectQueryBase(oSql.ToString(),oParamList.ToArray());

		return oDataSet;
	}
}
