﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: お知らせメール送信履歴
--	Progaram ID		: TxInfoMailHistory
--  Creation Date	: 2017.01.05
--  Creater			: M&TT Zukeran
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;
using System.Text.RegularExpressions;

using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class TxInfoMailHistory:DbSession {
	public class SearchCondition {
		/// <summary>サイトCD</summary>
		public string SiteCd;
		/// <summary>送信日時(From)</summary>
		public string SendDateFrom;
		/// <summary>送信日時(To)</summary>
		public string SendDateTo;
		/// <summary>送信済みフラグ(0:未送信、1:送信済み)</summary>
		public string SentFlag;
		/// <summary>受信者性別(1:男性、3:女性)</summary>
		public string SexCd;
		/// <summary>送信方法(0:手動送信、1:予約送信)</summary>
		public string ReservationFlag;

		public SearchCondition() {
			SiteCd = string.Empty;
			SendDateFrom = string.Empty;
			SendDateTo = string.Empty;
			SentFlag = string.Empty;
			SexCd = string.Empty;
			ReservationFlag = string.Empty;
		}
	}
	public class MainteData {
		/// <summary>サイトCD</summary>
		public string SiteCd;
		/// <summary>メールテンプレートNo</summary>
		public string MailTemplateNo;
		/// <summary>送信種別(1:HTML、2:テキスト)</summary>
		public string MailSendType;
		/// <summary>送信サーバ(1:BEAM、2:MULLER3)</summary>
		public string MailServer;
		/// <summary>メール種類(ViCommConst.MAIL_TP_XXX)</summary>
		public string MailType;
		/// <summary>予約送信対象の検索条件(JSON)</summary>
		public string Conditions;
		/// <summary>オリジナルテキスト(JSON)</summary>
		public string Doc;
		/// <summary>オリジナルテキスト</summary>
		public int DocCount;
		/// <summary>受信者性別</summary>
		public string RxSexCd;
		/// <summary>送信予定日時</summary>
		public string ReservationSendDate;
		/// <summary>送信予定人数(予約時の人数)</summary>
		public int ReservationSendNum;
		/// <summary>予約フラグ</summary>
		public string ReservationFlag;
		/// <summary>登録者・更新者ID</summary>
		public string AdminId;
		public string[] aConditionNames;
		public string[] aConditionVals;
		public int ConditionCount;

		public MainteData() {
			SiteCd = string.Empty;
			MailTemplateNo = string.Empty;
			MailSendType = string.Empty;
			MailServer = string.Empty;
			MailType = string.Empty;
			Conditions = string.Empty;
			Doc = string.Empty;
			DocCount = 0;
			RxSexCd = string.Empty;
			ReservationSendDate = string.Empty;
			ReservationSendNum = 0;
			ReservationFlag = string.Empty;
			AdminId = string.Empty;
			aConditionNames = new string[0];
			aConditionVals = new string[0];
			ConditionCount = 0;
		}
	}

	/// <summary>
	/// コンストラクタ
	/// </summary>
	public TxInfoMailHistory() {
	}

	/// <summary>
	/// メールカウントテーブル条件設定
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <param name="pWhere"></param>
	/// <returns></returns>
	public OracleParameter[] CreateWhereMailCount(SearchCondition pSearchCondition,ref string pWhere) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		// サイトCD
		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
		oParamList.Add(new OracleParameter("SITE_CD",pSearchCondition.SiteCd));

		// 送信日時From
		DateTime dtCheck;
		if (!string.IsNullOrEmpty(pSearchCondition.SendDateFrom)) {
			if (DateTime.TryParse(pSearchCondition.SendDateFrom,out dtCheck)) {
				SysPrograms.SqlAppendWhere("TX_MAIL_DAY >= :TX_MAIL_DAY_FROM",ref pWhere);
				oParamList.Add(new OracleParameter("TX_MAIL_DAY_FROM",dtCheck.ToString("yyyy/MM/dd")));
			}
		}

		// 送信日時To
		if (!string.IsNullOrEmpty(pSearchCondition.SendDateTo)) {
			if (DateTime.TryParse(pSearchCondition.SendDateTo,out dtCheck)) {
				SysPrograms.SqlAppendWhere("TX_MAIL_DAY <= :TX_MAIL_DAY_TO",ref pWhere);
				oParamList.Add(new OracleParameter("TX_MAIL_DAY_TO",dtCheck.ToString("yyyy/MM/dd")));
			}
		}

		// 性別
		if (!string.IsNullOrEmpty(pSearchCondition.SexCd)) {
			SysPrograms.SqlAppendWhere("SEX_CD = :SEX_CD",ref pWhere);
			oParamList.Add(new OracleParameter("SEX_CD",pSearchCondition.SexCd));
		}

		return oParamList.ToArray();
	}

	/// <summary>
	/// 条件設定
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <param name="pWhere"></param>
	/// <returns></returns>
	public OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhere) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		// 送信日時From
		if (!string.IsNullOrEmpty(pSearchCondition.SendDateFrom)) {
			SysPrograms.SqlAppendWhere("P.RESERVATION_SEND_DATE >= TO_DATE(:RESERVATION_SEND_DATE_FROM, 'YYYY/MM/DD HH24:MI:SS')",ref pWhere);
			oParamList.Add(new OracleParameter("RESERVATION_SEND_DATE_FROM",pSearchCondition.SendDateFrom));
		}
		// 送信日時To
		if (!string.IsNullOrEmpty(pSearchCondition.SendDateTo)) {
			SysPrograms.SqlAppendWhere("P.RESERVATION_SEND_DATE <= TO_DATE(:RESERVATION_SEND_DATE_TO, 'YYYY/MM/DD HH24:MI:SS')",ref pWhere);
			oParamList.Add(new OracleParameter("RESERVATION_SEND_DATE_TO",pSearchCondition.SendDateTo));
		}
		// 送信状態
		if (!string.IsNullOrEmpty(pSearchCondition.SentFlag)) {
			SysPrograms.SqlAppendWhere("P.SENT_FLAG = :SENT_FLAG",ref pWhere);
			oParamList.Add(new OracleParameter("SENT_FLAG",pSearchCondition.SentFlag));
		}
		// 性別
		if (!string.IsNullOrEmpty(pSearchCondition.SexCd)) {
			SysPrograms.SqlAppendWhere("P.RX_SEX_CD = :RX_SEX_CD",ref pWhere);
			oParamList.Add(new OracleParameter("RX_SEX_CD",pSearchCondition.SexCd));
		}
		// 送信方法（0:手動送信、1:予約送信）
		if (!string.IsNullOrEmpty(pSearchCondition.ReservationFlag)) {
			SysPrograms.SqlAppendWhere("P.RESERVATION_FLAG = :RESERVATION_FLAG",ref pWhere);
			oParamList.Add(new OracleParameter("RESERVATION_FLAG",pSearchCondition.ReservationFlag));
		}

		return oParamList.ToArray();
	}

	/// <summary>
	/// お知らせメール送信履歴一覧 件数取得
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <param name="startRowIndex"></param>
	/// <param name="maximumRows"></param>
	/// <returns></returns>
	public int GetPageCount(SearchCondition pSearchCondition) {
		string sWhere = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(CreateWhere(pSearchCondition,ref sWhere));

		oSqlBuilder.AppendLine("SELECT COUNT(*) FROM T_TX_RESERVATION_MAIL_HISTORY P");
		oSqlBuilder.AppendLine(sWhere);

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return int.Parse(oDataSet.Tables[0].Rows[0][0].ToString());
	}

	/// <summary>
	/// お知らせメール送信履歴一覧 データ取得
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <param name="startRowIndex"></param>
	/// <param name="maximumRows"></param>
	/// <returns></returns>
	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sWhere = string.Empty;
		string sWhereMailCnt = string.Empty;
		string sExecSql = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(CreateWhereMailCount(pSearchCondition,ref sWhereMailCnt));
		oParamList.AddRange(CreateWhere(pSearchCondition,ref sWhere));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	P.*");
		oSqlBuilder.AppendLine("	,T.TEMPLATE_NM");
		oSqlBuilder.AppendLine("	,NVL(TXD.TX_MAIL_COUNT,0) AS TX_MAIL_COUNT");
		oSqlBuilder.AppendLine("	,NVL(ACD.ACCESS_COUNT,0) AS ACCESS_COUNT");
		oSqlBuilder.AppendLine("	,NVL(ACD.UNIQUE_ACCESS_COUNT,0) AS UNIQUE_ACCESS_COUNT");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_TX_RESERVATION_MAIL_HISTORY P");
		oSqlBuilder.AppendLine("	INNER JOIN T_MAIL_TEMPLATE T");
		oSqlBuilder.AppendLine("		ON P.SITE_CD=T.SITE_CD AND P.MAIL_TEMPLATE_NO=T.MAIL_TEMPLATE_NO");
		oSqlBuilder.AppendLine("	LEFT JOIN (");
		oSqlBuilder.AppendLine("		SELECT");
		oSqlBuilder.AppendLine("			SITE_CD");
		oSqlBuilder.AppendLine("			,MAIL_TEMPLATE_NO");
		oSqlBuilder.AppendLine("			,SEX_CD");
		oSqlBuilder.AppendLine("			,TX_MAIL_DAY");
		oSqlBuilder.AppendLine("			,TX_MAIL_COUNT");
		oSqlBuilder.AppendLine("		FROM");
		oSqlBuilder.AppendLine("			T_TX_MAIL_COUNT_DAILY");
		oSqlBuilder.AppendLine(sWhereMailCnt);
		oSqlBuilder.AppendLine("	) TXD ON TXD.SITE_CD=P.SITE_CD");
		oSqlBuilder.AppendLine("		AND TXD.MAIL_TEMPLATE_NO=P.MAIL_TEMPLATE_NO");
		oSqlBuilder.AppendLine("		AND TXD.SEX_CD=P.RX_SEX_CD");
		oSqlBuilder.AppendLine("		AND TXD.TX_MAIL_DAY=TO_CHAR(P.RESERVATION_SEND_DATE,'YYYY/MM/DD')");
		oSqlBuilder.AppendLine("	LEFT JOIN (");
		oSqlBuilder.AppendLine("		SELECT");
		oSqlBuilder.AppendLine("			SITE_CD");
		oSqlBuilder.AppendLine("			,MAIL_TEMPLATE_NO");
		oSqlBuilder.AppendLine("			,SEX_CD");
		oSqlBuilder.AppendLine("			,TX_MAIL_DAY");
		oSqlBuilder.AppendLine("			,SUM(ACCESS_COUNT) AS ACCESS_COUNT");
		oSqlBuilder.AppendLine("			,SUM(UNIQUE_ACCESS_COUNT) AS UNIQUE_ACCESS_COUNT");
		oSqlBuilder.AppendLine("		FROM");
		oSqlBuilder.AppendLine("			T_ACCESS_MAIL_COUNT_DAILY");
		oSqlBuilder.AppendLine(sWhereMailCnt);
		oSqlBuilder.AppendLine("		GROUP BY");
		oSqlBuilder.AppendLine("			SITE_CD,MAIL_TEMPLATE_NO,SEX_CD,TX_MAIL_DAY");
		oSqlBuilder.AppendLine("	) ACD ON ACD.SITE_CD=TXD.SITE_CD");
		oSqlBuilder.AppendLine("		AND ACD.MAIL_TEMPLATE_NO=TXD.MAIL_TEMPLATE_NO");
		oSqlBuilder.AppendLine("		AND ACD.SEX_CD=TXD.SEX_CD");
		oSqlBuilder.AppendLine("		AND ACD.TX_MAIL_DAY=TXD.TX_MAIL_DAY");

		oSqlBuilder.AppendLine(sWhere);

		sSortExpression = "ORDER BY P.RESERVATION_SEND_DATE ASC,P.RX_SEX_CD ASC,P.MAIL_TEMPLATE_NO ASC";
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		return ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
	}

	/// <summary>
	/// お知らせメール送信履歴 1件データ取得
	/// </summary>
	/// <param name="pSeq"></param>
	/// <returns></returns>
	public DataSet GetOne(string pSeq) {
		string sWhere = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	*");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_TX_RESERVATION_MAIL_HISTORY");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	RESERVATION_MAIL_HISTORY_SEQ = :RESERVATION_MAIL_HISTORY_SEQ");

		oParamList.Add(new OracleParameter(":RESERVATION_MAIL_HISTORY_SEQ",pSeq));

		return ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
	}

	/// <summary>
	/// お知らせメール送信履歴 1件詳細データ（検索条件）取得
	/// </summary>
	/// <param name="pSeq"></param>
	/// <returns></returns>
	public DataSet GetDetail(string pSeq) {
		string sWhere = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	*");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_TX_RESERVATION_HIS_CONDS");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	RESERVATION_MAIL_HISTORY_SEQ = :RESERVATION_MAIL_HISTORY_SEQ");

		oParamList.Add(new OracleParameter(":RESERVATION_MAIL_HISTORY_SEQ",pSeq));

		return ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
	}

	/// <summary>
	/// お知らせメール送信履歴一覧の編集
	/// </summary>
	/// <param name="pData"></param>
	/// <param name="sRevisionNo"></param>
	/// <param name="sSeq"></param>
	/// <param name="sDelFlag"></param>
	public void Mainte(MainteData pData,string sRevisionNo,string sSeq,string sDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("RESERVATION_MAIL_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pData.SiteCd);
			db.ProcedureInParm("PMAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,pData.MailTemplateNo);
			db.ProcedureInParm("PMAIL_SEND_TYPE",DbSession.DbType.VARCHAR2,pData.MailSendType);
			db.ProcedureInParm("PMAIL_SERVER",DbSession.DbType.VARCHAR2,pData.MailServer);
			db.ProcedureInParm("PMAIL_TYPE",DbSession.DbType.VARCHAR2,pData.MailType);
			db.ProcedureInArrayParm("PCONDITION_NAMES",DbSession.DbType.ARRAY_VARCHAR2,pData.aConditionNames);
			db.ProcedureInArrayParm("pCONDITION_VALS",DbSession.DbType.ARRAY_VARCHAR2,pData.aConditionVals);
			db.ProcedureInParm("pCONDITION_COUNT",DbSession.DbType.NUMBER,pData.ConditionCount);
			//db.ProcedureInParm("PDOC",DbSession.DbType.VARCHAR2,pData.Doc);
			db.ProcedureInParm("PDOC_COUNT",DbSession.DbType.NUMBER,pData.DocCount);
			db.ProcedureInParm("PRX_SEX_CD",DbSession.DbType.VARCHAR2,pData.RxSexCd);
			db.ProcedureInParm("PRESERVATION_SEND_DATE",DbSession.DbType.VARCHAR2,pData.ReservationSendDate);
			db.ProcedureInParm("PRESERVATION_SEND_NUM",DbSession.DbType.NUMBER,pData.ReservationSendNum);
			db.ProcedureInParm("PRESERVATION_FLAG",DbSession.DbType.NUMBER,pData.ReservationFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,sRevisionNo);
			db.ProcedureInParm("PRESERVATION_MAIL_HISTORY_SEQ",DbSession.DbType.NUMBER,sSeq);
			db.ProcedureInParm("PDELETE_FLAG",DbSession.DbType.NUMBER,sDelFlag);
			db.ProcedureInParm("PADMIN_ID",DbSession.DbType.VARCHAR2,pData.AdminId);
			db.ExecuteProcedure();
			string sStatus = db.GetStringValue("PSTATUS");
		}
	}
}
