﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 小クエストクリア数
--	Progaram ID		: LittleQuestCount
--
--  Creation Date	: 2012.12.05
--  Creater			: M&TT A.Taba
--
**************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class LittleQuestCount:DbSession {
	public LittleQuestCount() {
	}
	
	public DataSet GetList(string pSiteCd,string pQuestSeq,string pLevelQuestSeq,string pSexCd) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT								        ");
		oSqlBuilder.AppendLine("	SITE_CD					,		        ");
		oSqlBuilder.AppendLine("	QUEST_SEQ				,		        ");
		oSqlBuilder.AppendLine("	LEVEL_QUEST_SEQ			,		        ");
		oSqlBuilder.AppendLine("	LITTLE_QUEST_SEQ		,		        ");
		oSqlBuilder.AppendLine("	LEVEL_QUEST_SUB_SEQ		,		        ");
		oSqlBuilder.AppendLine("	LITTLE_QUEST_NM  		,		        ");
		oSqlBuilder.AppendLine("	SEX_CD  						        ");
		oSqlBuilder.AppendLine("FROM								        ");
		oSqlBuilder.AppendLine("	VW_PW_LITTLE_QUEST01			        ");
		oSqlBuilder.AppendLine("WHERE								        ");
		oSqlBuilder.AppendLine("	SITE_CD		    = :SITE_CD		   AND	");
		oSqlBuilder.AppendLine("	QUEST_SEQ	    = :QUEST_SEQ	   AND	");
		oSqlBuilder.AppendLine("	LEVEL_QUEST_SEQ	= :LEVEL_QUEST_SEQ AND  ");
		oSqlBuilder.AppendLine("	SEX_CD	        = :SEX_CD		     	");
		oSqlBuilder.AppendLine("ORDER BY LEVEL_QUEST_SUB_SEQ                ");

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":QUEST_SEQ",pQuestSeq);
				cmd.Parameters.Add(":LEVEL_QUEST_SEQ",pLevelQuestSeq);
				cmd.Parameters.Add(":SEX_CD",pSexCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}

		AppendCount(oDs,pSiteCd,pQuestSeq,pLevelQuestSeq);

		return oDs;
	}

	private void AppendCount(DataSet pDS,string pSiteCd,string pQuestSeq,string pLevelQuestSeq) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;

		col = new DataColumn(string.Format("ENTRY_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		col = new DataColumn(string.Format("RETIRE_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		col = new DataColumn(string.Format("CLEAR_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			StringBuilder oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine("SELECT											    ");
			oSqlBuilder.AppendLine("	COUNT(*) AS ENTRY_COUNT						    ");
			oSqlBuilder.AppendLine(" FROM							   				    ");
			oSqlBuilder.AppendLine("	T_QUEST_ENTRY_LOG				     		    ");
			oSqlBuilder.AppendLine(" WHERE											    ");
			oSqlBuilder.AppendLine("	SITE_CD		         = :SITE_CD		        AND ");
			oSqlBuilder.AppendLine("	QUEST_SEQ		     = :QUEST_SEQ	        AND ");
			oSqlBuilder.AppendLine("	LEVEL_QUEST_SEQ	     = :LEVEL_QUEST_SEQ	    AND ");
			oSqlBuilder.AppendLine("	LITTLE_QUEST_SEQ     = :LITTLE_QUEST_SEQ    AND ");
			oSqlBuilder.AppendLine("	QUEST_TYPE           = 1                        ");

			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
					cmd.BindByName = true;

					cmd.Parameters.Add(":SITE_CD",pSiteCd);
					cmd.Parameters.Add(":QUEST_SEQ",pQuestSeq);
					cmd.Parameters.Add(":LEVEL_QUEST_SEQ",pLevelQuestSeq);
					cmd.Parameters.Add(":LITTLE_QUEST_SEQ",dr["LITTLE_QUEST_SEQ"].ToString());

					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub);
					}

					dr["ENTRY_COUNT"] = dsSub.Tables[0].Rows[0]["ENTRY_COUNT"];
				}
			}

			oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine("SELECT											  ");
			oSqlBuilder.AppendLine("	COUNT(*) AS RETIRE_COUNT					  ");
			oSqlBuilder.AppendLine(" FROM							   				  ");
			oSqlBuilder.AppendLine("	T_QUEST_ENTRY_LOG				     		  ");
			oSqlBuilder.AppendLine(" WHERE											  ");
			oSqlBuilder.AppendLine("	SITE_CD		         = :SITE_CD		     AND  ");
			oSqlBuilder.AppendLine("	QUEST_SEQ		     = :QUEST_SEQ	     AND  ");
			oSqlBuilder.AppendLine("	LEVEL_QUEST_SEQ	     = :LEVEL_QUEST_SEQ	 AND  ");
			oSqlBuilder.AppendLine("	LITTLE_QUEST_SEQ     = :LITTLE_QUEST_SEQ AND  ");
			oSqlBuilder.AppendLine("	QUEST_TYPE           = 1				 AND  ");
			oSqlBuilder.AppendLine("	QUEST_STATUS         = 2				      ");


			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
					cmd.BindByName = true;

					cmd.Parameters.Add(":SITE_CD",pSiteCd);
					cmd.Parameters.Add(":QUEST_SEQ",pQuestSeq);
					cmd.Parameters.Add(":LEVEL_QUEST_SEQ",pLevelQuestSeq);
					cmd.Parameters.Add(":LITTLE_QUEST_SEQ",dr["LITTLE_QUEST_SEQ"].ToString());

					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub);
					}

					dr["RETIRE_COUNT"] = dsSub.Tables[0].Rows[0]["RETIRE_COUNT"];
				}
			}

			oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine("SELECT											 ");
			oSqlBuilder.AppendLine("	COUNT(*) AS CLEAR_COUNT						 ");
			oSqlBuilder.AppendLine(" FROM							   				 ");
			oSqlBuilder.AppendLine("	T_QUEST_ENTRY_LOG				     		 ");
			oSqlBuilder.AppendLine(" WHERE											 ");
			oSqlBuilder.AppendLine("	SITE_CD		         = :SITE_CD		     AND ");
			oSqlBuilder.AppendLine("	QUEST_SEQ		     = :QUEST_SEQ	     AND ");
			oSqlBuilder.AppendLine("	LEVEL_QUEST_SEQ	     = :LEVEL_QUEST_SEQ	 AND ");
			oSqlBuilder.AppendLine("	LITTLE_QUEST_SEQ     = :LITTLE_QUEST_SEQ AND ");
			oSqlBuilder.AppendLine("	QUEST_TYPE           = 1                 AND ");
			oSqlBuilder.AppendLine("	QUEST_STATUS         = 3				     ");


			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
					cmd.BindByName = true;

					cmd.Parameters.Add(":SITE_CD",pSiteCd);
					cmd.Parameters.Add(":QUEST_SEQ",pQuestSeq);
					cmd.Parameters.Add(":LEVEL_QUEST_SEQ",pLevelQuestSeq);
					cmd.Parameters.Add(":LITTLE_QUEST_SEQ",dr["LITTLE_QUEST_SEQ"].ToString());

					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub);
					}

					dr["CLEAR_COUNT"] = dsSub.Tables[0].Rows[0]["CLEAR_COUNT"];
				}
			}
		}
	}
}
