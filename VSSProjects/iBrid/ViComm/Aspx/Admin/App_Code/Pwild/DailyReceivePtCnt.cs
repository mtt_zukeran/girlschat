﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 期間別出演者獲得報酬集計
--	Progaram ID		: DailyReceivePtCnt
--  Creation Date	: 2017.03.07
--  Creater			: M&TT Zukeran
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;

public class DailyReceivePtCnt:DbSession {
	public const string GROUP_LIST = "1";
	public const string GROUP_DETAIL = "2";

	#region SearchCondition
	public class SearchCondition {
		/// <summary>サイトCD</summary>
		public string SiteCd;
		/// <summary>表示日付(年)</summary>
		public string Year;
		/// <summary>表示日付(月)</summary>
		public string Month;
		/// <summary>表示日付</summary>
		public string ReportDate;
		/// <summary>やりとりの継続日数</summary>
		public string DispElapsedDays;

		public SearchCondition() {
			this.SiteCd = string.Empty;
			this.Year = string.Empty;
			this.Month = string.Empty;
			this.ReportDate = string.Empty;
			this.DispElapsedDays = string.Empty;
		}
	}
	#endregion SearchCondition

	public DailyReceivePtCnt() {
	}

	/// <summary>
	/// 条件設定
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <param name="pWhere"></param>
	/// <returns></returns>
	public OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhere) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		// サイトCD
		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
		oParamList.Add(new OracleParameter("SITE_CD",pSearchCondition.SiteCd));

		// 表示日付(年月のみ)
		if (!string.IsNullOrEmpty(pSearchCondition.Year) && !string.IsNullOrEmpty(pSearchCondition.Month)) {
			string sDate = string.Format("{0:D4}-{1:D2}-01",pSearchCondition.Year,pSearchCondition.Month);
			DateTime oFromDate = DateTime.Today;
			if (DateTime.TryParse(sDate,out oFromDate)) {
				SysPrograms.SqlAppendWhere("REPORT_DATE >= TO_DATE(:REPORT_DATE_FROM, 'YYYY-MM-DD')",ref pWhere);
				SysPrograms.SqlAppendWhere("REPORT_DATE < TO_DATE(:REPORT_DATE_TO, 'YYYY-MM-DD')",ref pWhere);
				oParamList.Add(new OracleParameter("REPORT_DATE_FROM",oFromDate.ToString("yyyy-MM-dd")));
				oParamList.Add(new OracleParameter("REPORT_DATE_TO",oFromDate.AddMonths(1).ToString("yyyy-MM-dd")));
			}
		}

		// 表示日付(年月日指定)
		if (!string.IsNullOrEmpty(pSearchCondition.ReportDate)) {
			DateTime oFromDate = DateTime.Today;
			if (DateTime.TryParse(pSearchCondition.ReportDate,out oFromDate)) {
				SysPrograms.SqlAppendWhere("REPORT_DATE = TO_DATE(:REPORT_DATE, 'YYYY-MM-DD')",ref pWhere);
				oParamList.Add(new OracleParameter("REPORT_DATE",oFromDate.ToString("yyyy-MM-dd")));
			}
		}

		// やりとりの継続日数
		if (!string.IsNullOrEmpty(pSearchCondition.DispElapsedDays)) {
			SysPrograms.SqlAppendWhere("ELAPSED_DAYS_DISP_TERM = :ELAPSED_DAYS_DISP_TERM",ref pWhere);
			oParamList.Add(new OracleParameter("ELAPSED_DAYS_DISP_TERM",pSearchCondition.DispElapsedDays));
		}

		return oParamList.ToArray();
	}

	/// <summary>
	/// 出演者獲得報酬集計結果 件数取得
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <param name="startRowIndex"></param>
	/// <param name="maximumRows"></param>
	/// <returns></returns>
	public int GetPageCount(SearchCondition pSearchCondition) {
		string sWhere = string.Empty;
		string sGroup = this.GetGrouping(pSearchCondition);
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(CreateWhere(pSearchCondition,ref sWhere));

		oSqlBuilder.AppendLine("SELECT COUNT(*) FROM T_DAILY_RECEIVE_PT_CNT");
		oSqlBuilder.AppendLine(sWhere);
		if (!string.IsNullOrEmpty(sGroup)) {
			oSqlBuilder.AppendLine(sGroup);
		}

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return int.Parse(oDataSet.Tables[0].Rows[0][0].ToString());
	}

	/// <summary>
	/// 出演者獲得報酬集計結果 データ取得
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <param name="startRowIndex"></param>
	/// <param name="maximumRows"></param>
	/// <returns></returns>
	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sWhere = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(CreateWhere(pSearchCondition,ref sWhere));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	ELAPSED_DAYS_DISP_TERM");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'01',1,0)) AS COUNT_1");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'01',RECEIVE_POINT,0)) AS POINT_1");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'02',1,0)) AS COUNT_2");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'02',RECEIVE_POINT,0)) AS POINT_2");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'03',1,0)) AS COUNT_3");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'03',RECEIVE_POINT,0)) AS POINT_3");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'04',1,0)) AS COUNT_4");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'04',RECEIVE_POINT,0)) AS POINT_4");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'05',1,0)) AS COUNT_5");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'05',RECEIVE_POINT,0)) AS POINT_5");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'06',1,0)) AS COUNT_6");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'06',RECEIVE_POINT,0)) AS POINT_6");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'07',1,0)) AS COUNT_7");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'07',RECEIVE_POINT,0)) AS POINT_7");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'08',1,0)) AS COUNT_8");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'08',RECEIVE_POINT,0)) AS POINT_8");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'09',1,0)) AS COUNT_9");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'09',RECEIVE_POINT,0)) AS POINT_9");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'10',1,0)) AS COUNT_10");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'10',RECEIVE_POINT,0)) AS POINT_10");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'11',1,0)) AS COUNT_11");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'11',RECEIVE_POINT,0)) AS POINT_11");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'12',1,0)) AS COUNT_12");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'12',RECEIVE_POINT,0)) AS POINT_12");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'13',1,0)) AS COUNT_13");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'13',RECEIVE_POINT,0)) AS POINT_13");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'14',1,0)) AS COUNT_14");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'14',RECEIVE_POINT,0)) AS POINT_14");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'15',1,0)) AS COUNT_15");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'15',RECEIVE_POINT,0)) AS POINT_15");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'16',1,0)) AS COUNT_16");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'16',RECEIVE_POINT,0)) AS POINT_16");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'17',1,0)) AS COUNT_17");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'17',RECEIVE_POINT,0)) AS POINT_17");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'18',1,0)) AS COUNT_18");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'18',RECEIVE_POINT,0)) AS POINT_18");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'19',1,0)) AS COUNT_19");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'19',RECEIVE_POINT,0)) AS POINT_19");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'20',1,0)) AS COUNT_20");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'20',RECEIVE_POINT,0)) AS POINT_20");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'21',1,0)) AS COUNT_21");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'21',RECEIVE_POINT,0)) AS POINT_21");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'22',1,0)) AS COUNT_22");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'22',RECEIVE_POINT,0)) AS POINT_22");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'23',1,0)) AS COUNT_23");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'23',RECEIVE_POINT,0)) AS POINT_23");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'24',1,0)) AS COUNT_24");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'24',RECEIVE_POINT,0)) AS POINT_24");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'25',1,0)) AS COUNT_25");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'25',RECEIVE_POINT,0)) AS POINT_25");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'26',1,0)) AS COUNT_26");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'26',RECEIVE_POINT,0)) AS POINT_26");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'27',1,0)) AS COUNT_27");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'27',RECEIVE_POINT,0)) AS POINT_27");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'28',1,0)) AS COUNT_28");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'28',RECEIVE_POINT,0)) AS POINT_28");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'29',1,0)) AS COUNT_29");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'29',RECEIVE_POINT,0)) AS POINT_29");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'30',1,0)) AS COUNT_30");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'30',RECEIVE_POINT,0)) AS POINT_30");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'31',1,0)) AS COUNT_31");
		oSqlBuilder.AppendLine("	,SUM(DECODE(REPORT_DAY,'31',RECEIVE_POINT,0)) AS POINT_31");
		oSqlBuilder.AppendLine("	,COUNT(*) AS TOTAL_CNT");
		oSqlBuilder.AppendLine("	,SUM(RECEIVE_POINT) AS TOTAL_PT");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_DAILY_RECEIVE_PT_CNT");
		oSqlBuilder.AppendLine(sWhere);
		oSqlBuilder.AppendLine(this.GetGrouping(pSearchCondition));

		sSortExpression = "ORDER BY ELAPSED_DAYS_DISP_TERM DESC";
		oSqlBuilder.AppendLine(sSortExpression);

		return ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
	}

	/// <summary>
	/// 出演者獲得報酬集計結果詳細 データ取得
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <param name="startRowIndex"></param>
	/// <param name="maximumRows"></param>
	/// <returns></returns>
	public DataSet GetPageCollectionDtl(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sWhere = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(CreateWhere(pSearchCondition,ref sWhere));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SITE_CD");
		oSqlBuilder.AppendLine("	,CAST_USER_SEQ");
		oSqlBuilder.AppendLine("	,CAST_LOGIN_ID");
		oSqlBuilder.AppendLine("	,MAN_USER_SEQ");
		oSqlBuilder.AppendLine("	,MAN_LOGIN_ID");
		oSqlBuilder.AppendLine("	,RECEIVE_POINT");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_DAILY_RECEIVE_PT_CNT");
		oSqlBuilder.AppendLine(sWhere);
		oSqlBuilder.AppendLine(this.GetGrouping(pSearchCondition));

		sSortExpression = "ORDER BY CAST_LOGIN_ID ASC,RECEIVE_POINT DESC,MAN_LOGIN_ID ASC";
		oSqlBuilder.AppendLine(sSortExpression);

		return ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
	}

	private string GetGrouping(SearchCondition pSearchCondition) {
		if (!string.IsNullOrEmpty(pSearchCondition.Year) && !string.IsNullOrEmpty(pSearchCondition.Month)) {
			return "GROUP BY ELAPSED_DAYS_DISP_TERM";
		}
		return string.Empty;
	}
}
