﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: ステージクリア分布レポート
--	Progaram ID		: StageClearCountReport
--
--  Creation Date	: 2012.12.04
--  Creater			: M&TT A.Taba
--
**************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class StageClearCountReport:DbSession {
	public StageClearCountReport() {
	}

	public DataSet GetList(string pSiteCd,string pSexCd) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	STAGE_SEQ						 ,  ");
		oSqlBuilder.AppendLine("	STAGE_NM						 ,  ");
		oSqlBuilder.AppendLine("	SEX_CD  						    ");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	T_STAGE								");
		oSqlBuilder.AppendLine("WHERE									");
		oSqlBuilder.AppendLine("	SITE_CD		= :SITE_CD		AND		");
		oSqlBuilder.AppendLine("	SEX_CD		= :SEX_CD				");
		oSqlBuilder.AppendLine("ORDER BY STAGE_SEQ		     			");

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":SEX_CD",pSexCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}

		AppendCount(oDs,pSiteCd);

		return oDs;
	}

	private void AppendCount(DataSet pDS,string pSiteCd) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;

		col = new DataColumn(string.Format("STAGE_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			StringBuilder oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine("SELECT											 ");
			oSqlBuilder.AppendLine("	COUNT(*) AS STAGE_COUNT	            		 ");
			oSqlBuilder.AppendLine(" FROM							   				 ");
			oSqlBuilder.AppendLine("	T_STAGE_PROGRESS		     				 ");
			oSqlBuilder.AppendLine(" WHERE											 ");
			oSqlBuilder.AppendLine("	SITE_CD		         = :SITE_CD		AND		 ");
			oSqlBuilder.AppendLine("	STAGE_SEQ		     = :STAGE_SEQ	AND		 ");
			oSqlBuilder.AppendLine("	STAGE_CLEAR_FLAG	 = 1            		 ");

			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
					cmd.BindByName = true;

					cmd.Parameters.Add(":SITE_CD",pSiteCd);
					cmd.Parameters.Add(":STAGE_SEQ",dr["STAGE_SEQ"].ToString());

					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub);
					}

					dr["STAGE_COUNT"] = dsSub.Tables[0].Rows[0]["STAGE_COUNT"];
				}
			}
		}
	}
}