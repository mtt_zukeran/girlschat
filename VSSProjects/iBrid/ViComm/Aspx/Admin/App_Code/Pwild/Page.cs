﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: ページ一覧
--	Progaram ID		: PageList
--
--  Creation Date	: 2014.02.11
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;


public class PageList:DbSession {

	public PageList() {
	}

	public DataSet GetList(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	PROGRAM_ROOT			,	");
		oSqlBuilder.AppendLine("	PROGRAM_ID				,	");
		oSqlBuilder.AppendLine("	HTML_DOC_TYPE			,	");
		oSqlBuilder.AppendLine("	PROGRAM_NM					");
		oSqlBuilder.AppendLine("FROM							");
		oSqlBuilder.AppendLine("	VW_PAGE01					");
		oSqlBuilder.AppendLine("WHERE							");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD			");
		oSqlBuilder.AppendLine("ORDER BY	");
		oSqlBuilder.AppendLine("	SITE_CD,PROGRAM_ROOT ASC,HTML_DOC_TYPE ASC,PROGRAM_ID ASC");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		DataSet oDs = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		return oDs;
	}

}
