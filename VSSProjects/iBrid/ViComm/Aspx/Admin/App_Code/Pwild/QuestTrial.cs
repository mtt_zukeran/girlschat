﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: クエストクリア条件設定--	Progaram ID		: OtherQuest
--
--  Creation Date	: 2012.07.07
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class QuestTrial:DbSession {
	public QuestTrial() {
	}

	public int GetPageCount(string pSiteCd,string pQuestSeq,string pLevelQuestSeq,string pLittleQuestSeq,string pOtherQuestSeq,string pQuestType) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT COUNT(*) FROM VW_PW_QUEST_TRIAL01");
		string sWhereClause;

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pQuestSeq,pLevelQuestSeq,pLittleQuestSeq,pOtherQuestSeq,pQuestType,out sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pQuestSeq,string pLevelQuestSeq,string pLittleQuestSeq,string pOtherQuestSeq,string pQuestType,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	SITE_CD						,	");
		oSqlBuilder.AppendLine("	QUEST_SEQ					,	");
		oSqlBuilder.AppendLine("	QUEST_TRIAL_SEQ				,	");
		oSqlBuilder.AppendLine("	LEVEL_QUEST_SEQ				,	");
		oSqlBuilder.AppendLine("	LITTLE_QUEST_SEQ			,	");
		oSqlBuilder.AppendLine("	OTHER_QUEST_SEQ				,	");
		oSqlBuilder.AppendLine("	QUEST_TYPE					,	");
		oSqlBuilder.AppendLine("	TRIAL_CATEGORY_NM			,	");
		oSqlBuilder.AppendLine("	TRIAL_CATEGORY_DETAIL		,	");
		oSqlBuilder.AppendLine("	QUEST_TRIAL_COUNT			,	");
		oSqlBuilder.AppendLine("	COUNT_UNIT					,	");
		oSqlBuilder.AppendLine("	QUEST_COUNT_START_TYPE		,	");
		oSqlBuilder.AppendLine("	SEX_CD							");
		oSqlBuilder.AppendLine("FROM								");
		oSqlBuilder.AppendLine("	VW_PW_QUEST_TRIAL01				");

		string sWhereClause;

		string sPagingSql = string.Empty;
		string sSortExpression = "ORDER BY SITE_CD, QUEST_TRIAL_SEQ";

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pQuestSeq,pLevelQuestSeq,pLittleQuestSeq,pOtherQuestSeq,pQuestType,out sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);
		
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}
	
	private OracleParameter[] CreateWhere(string pSiteCd,string pQuestSeq,string pLevelQuestSeq,string pLittleQuestSeq,string pOtherQuestSeq,string pQuestType,out string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}
		if (!string.IsNullOrEmpty(pQuestSeq)) {
			SysPrograms.SqlAppendWhere("QUEST_SEQ = :QUEST_SEQ",ref pWhere);
			list.Add(new OracleParameter("QUEST_SEQ",pQuestSeq));
		}
		if (!string.IsNullOrEmpty(pLevelQuestSeq)) {
			SysPrograms.SqlAppendWhere("LEVEL_QUEST_SEQ = :LEVEL_QUEST_SEQ",ref pWhere);
			list.Add(new OracleParameter("LEVEL_QUEST_SEQ",pLevelQuestSeq));
		}
		if (!string.IsNullOrEmpty(pLittleQuestSeq)) {
			SysPrograms.SqlAppendWhere("LITTLE_QUEST_SEQ = :LITTLE_QUEST_SEQ",ref pWhere);
			list.Add(new OracleParameter("LITTLE_QUEST_SEQ",pLittleQuestSeq));
		}
		if (!string.IsNullOrEmpty(pOtherQuestSeq)) {
			SysPrograms.SqlAppendWhere("OTHER_QUEST_SEQ = :OTHER_QUEST_SEQ",ref pWhere);
			list.Add(new OracleParameter("OTHER_QUEST_SEQ",pOtherQuestSeq));
		}
		if (!string.IsNullOrEmpty(pQuestType)) {
			SysPrograms.SqlAppendWhere("QUEST_TYPE = :QUEST_TYPE",ref pWhere);
			list.Add(new OracleParameter("QUEST_TYPE",pQuestType));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}