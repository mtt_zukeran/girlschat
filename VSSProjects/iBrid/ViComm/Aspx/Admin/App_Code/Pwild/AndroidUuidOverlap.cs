﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: Android端末UUID重複
--	Progaram ID		: AndroidUuidOverlap
--  Creation Date	: 2015.03.17
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

public class AndroidUuidOverlap:DbSession {
	public class SearchCondition {
		private string siteCd;

		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}
	}

	public AndroidUuidOverlap() {
	}

	public int GetPageCount(SearchCondition pSearchCondition) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_ANDROID_UUID_OVERLAP");
		oSqlBuilder.AppendLine(sWhereClause);

		return ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
	}

	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	T_ANDROID_UUID_OVERLAP.SITE_CD,");
		oSqlBuilder.AppendLine("	T_ANDROID_UUID_OVERLAP.USER_SEQ,");
		oSqlBuilder.AppendLine("	T_ANDROID_UUID_OVERLAP.DEVICE_UUID,");
		oSqlBuilder.AppendLine("	T_ANDROID_UUID_OVERLAP.OVERLAP_DATE,");
		oSqlBuilder.AppendLine("	U1.LOGIN_ID,");
		oSqlBuilder.AppendLine("	U2.LOGIN_ID AS OVERLAP_LOGIN_ID");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_ANDROID_UUID_OVERLAP");
		oSqlBuilder.AppendLine("	LEFT OUTER JOIN T_USER U1 ON (T_ANDROID_UUID_OVERLAP.USER_SEQ = U1.USER_SEQ)");
		oSqlBuilder.AppendLine("	LEFT OUTER JOIN T_USER U2 ON (T_ANDROID_UUID_OVERLAP.DEVICE_UUID = U2.DEVICE_UUID)");
		oSqlBuilder.AppendLine(sWhereClause);

		string sSortExpression = "ORDER BY T_ANDROID_UUID_OVERLAP.OVERLAP_DATE DESC";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		return ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pSearchCondition.SiteCd));
		}

		return oParamList.ToArray();
	}
}
