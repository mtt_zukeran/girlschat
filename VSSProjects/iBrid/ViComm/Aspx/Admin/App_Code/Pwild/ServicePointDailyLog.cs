﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: サービスポイント集計
--	Progaram ID		: ServicePointDailyLog
--  Creation Date	: 2015.05.11
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class ServicePointDailyLog:DbSession {

	#region SearchCondition
	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}
		private string reportDayFrom;
		public string ReportDayFrom {
			get {
				return this.reportDayFrom;
			}
			set {
				this.reportDayFrom = value;
			}
		}
		private string reportDaYTo;
		public string ReportDayTo {
			get {
				return this.reportDaYTo;
			}
			set {
				this.reportDaYTo = value;
			}
		}
	}
	#endregion SearchCondition

	public ServicePointDailyLog() {
	}

	public int GetPageCount(SearchCondition pSearchCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere(pSearchCondition,ref sWhereClause);

		oSqlBuilder.AppendLine("SELECT											");
		oSqlBuilder.AppendLine("	COUNT(*)									");
		oSqlBuilder.AppendLine("FROM											");
		oSqlBuilder.AppendLine("	T_SERVICE_POINT_DAILY_LOG					");
		oSqlBuilder.AppendLine(sWhereClause);

		int iPageCount = 0;
		iPageCount = ExecuteSelectCountQueryBase(oSqlBuilder.ToString(),oWhereParams);
		return iPageCount;
	}

	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT											");
		oSqlBuilder.AppendLine("	REPORT_DAY								,	");
		oSqlBuilder.AppendLine("	ADD_SERVICE_POINT						,	");
		oSqlBuilder.AppendLine("	LOST_SERVICE_POINT							");
		oSqlBuilder.AppendLine("FROM											");
		oSqlBuilder.AppendLine("	T_SERVICE_POINT_DAILY_LOG					");
		oSqlBuilder.AppendLine(sWhereClause);

		sSortExpression = "ORDER BY REPORT_DAY ASC";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhere) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			oParamList.Add(new OracleParameter(":SITE_CD",pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.ReportDayFrom)) {
			SysPrograms.SqlAppendWhere("REPORT_DAY >= :REPORT_DAY_FROM",ref pWhere);
			oParamList.Add(new OracleParameter(":REPORT_DAY_FROM",pSearchCondition.ReportDayFrom));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.ReportDayTo)) {
			SysPrograms.SqlAppendWhere("REPORT_DAY <= :REPORT_DAY_END",ref pWhere);
			oParamList.Add(new OracleParameter(":REPORT_DAY_END",pSearchCondition.ReportDayTo));
		}

		return oParamList.ToArray();
	}
}
