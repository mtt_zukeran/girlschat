﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 入会月別報酬(日別)
--	Progaram ID		: PaymentByEntDaily
--  Creation Date	: 2014.06.26
--  Creater			: Y.Ikemiya
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;
using iBridCommLib;
using ViComm;
using System.Collections.Generic;
using ViComm.Extension.Pwild;

public class PaymentByEntDaily:DbSession {

	public PaymentByEntDaily() {
	}

	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}

		private string entMonth;
		public string EntMonth {
			get {
				return this.entMonth;
			}
			set {
				this.entMonth = value;
			}
		}

		private string reportMonth;
		public string ReportMonth {
			get {
				return this.reportMonth;
			}
			set {
				this.reportMonth = value;
			}
		}
	}

	public DataSet GetPaymentList(SearchCondition pSearchCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT																									");
		oSqlBuilder.AppendLine("	C.DAYS																							,	");
		oSqlBuilder.AppendLine("	P.PAYMENT_COUNT																					,	");
		oSqlBuilder.AppendLine("	P.PAYMENT_AMT																						");
		oSqlBuilder.AppendLine("FROM																									");
		oSqlBuilder.AppendLine("	(																									");
		oSqlBuilder.AppendLine("		SELECT																							");
		oSqlBuilder.AppendLine("			TO_CHAR(STARTDATE + ROWNUM -1, 'YYYY/MM/DD') AS DAYS										");
		oSqlBuilder.AppendLine("		FROM																							");
		oSqlBuilder.AppendLine("			(																							");
		oSqlBuilder.AppendLine("				SELECT																					");
		oSqlBuilder.AppendLine("					TRUNC(TO_DATE(:REPORT_DAY_START)) AS STARTDATE									,	");
		oSqlBuilder.AppendLine("					TO_NUMBER(TO_CHAR(LAST_DAY(TO_DATE(:REPORT_DAY_START)), 'DD')) AS DAYS				");
		oSqlBuilder.AppendLine("				FROM																					");
		oSqlBuilder.AppendLine("					DUAL																				");
		oSqlBuilder.AppendLine("			)																						,	");
		oSqlBuilder.AppendLine("			ALL_CATALOG																					");
		oSqlBuilder.AppendLine("		WHERE																							");
		oSqlBuilder.AppendLine("			ROWNUM <= DAYS																				");
		oSqlBuilder.AppendLine("	) C																								,	");
		oSqlBuilder.AppendLine("	(																									");
		oSqlBuilder.AppendLine("		SELECT																							");
		oSqlBuilder.AppendLine("			REPORT_DAY																				,	");
		oSqlBuilder.AppendLine("			PAYMENT_COUNT																			,	");
		oSqlBuilder.AppendLine("			PAYMENT_AMT																					");
		oSqlBuilder.AppendLine("		FROM																							");
		oSqlBuilder.AppendLine("			T_PAYMENT_BY_ENT_DAILY																		");
		oSqlBuilder.AppendLine("		WHERE																							");
		oSqlBuilder.AppendLine("			SITE_CD			= :SITE_CD																AND	");
		oSqlBuilder.AppendLine("			ENT_MONTH		= :ENT_MONTH															AND	");
		oSqlBuilder.AppendLine("			REPORT_DAY		>= :REPORT_DAY_START													AND	");
		oSqlBuilder.AppendLine("			REPORT_DAY		<= :REPORT_DAY_END															");
		oSqlBuilder.AppendLine("	) P																									");
		oSqlBuilder.AppendLine("WHERE																									");
		oSqlBuilder.AppendLine("	C.DAYS	= P.REPORT_DAY																			(+)	");

		oParamList.Add(new OracleParameter(":SITE_CD",pSearchCondition.SiteCd));
		oParamList.Add(new OracleParameter(":ENT_MONTH",pSearchCondition.EntMonth));
		DateTime dtReportDayStart = DateTime.Parse(string.Format("{0}/01",pSearchCondition.ReportMonth));
		DateTime dtReportDayEnd = dtReportDayStart.AddMonths(1).AddDays(-1);
		oParamList.Add(new OracleParameter(":REPORT_DAY_START",string.Format("{0}/01",pSearchCondition.ReportMonth)));
		oParamList.Add(new OracleParameter(":REPORT_DAY_END",dtReportDayEnd.ToString("yyyy/MM/dd")));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
}
