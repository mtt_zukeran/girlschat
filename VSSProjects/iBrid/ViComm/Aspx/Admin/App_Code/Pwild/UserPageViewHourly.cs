﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 会員ページビュー時間別
--	Progaram ID		: UserPageViewHourly
--  Creation Date	: 2014.02.11
--  Creater			: Y.Ikemiya
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;
using iBridCommLib;
using ViComm;
using System.Collections.Generic;
using ViComm.Extension.Pwild;

public class UserPageViewHourly:DbSession {
	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}

		private string programRoot;
		public string ProgramRoot {
			get {
				return this.programRoot;
			}
			set {
				this.programRoot = value;
			}
		}

		private string programId;
		public string ProgramId {
			get {
				return this.programId;
			}
			set {
				this.programId = value;
			}
		}

		private string htmlDocType;
		public string HtmlDocType {
			get {
				return this.htmlDocType;
			}
			set {
				this.htmlDocType = value;
			}
		}

		private string accessType;
		public string AccessType {
			get {
				return this.accessType;
			}
			set {
				this.accessType = value;
			}
		}

		private string reportDayFrom;
		public string ReportDayFrom {
			get {
				return this.reportDayFrom;
			}
			set {
				this.reportDayFrom = value;
			}
		}

		private string reportDayTo;
		public string ReportDayTo {
			get {
				return this.reportDayTo;
			}
			set {
				this.reportDayTo = value;
			}
		}

		private string loginFlag;
		public string LoginFlag {
			get {
				return this.loginFlag;
			}
			set {
				this.loginFlag = value;
			}
		}

		private string pageUserAgentType;
		public string PageUserAgentType {
			get {
				return this.pageUserAgentType;
			}
			set {
				this.pageUserAgentType = value;
			}
		}

		private string carrierType;
		public string CarrierType {
			get {
				return this.carrierType;
			}
			set {
				this.carrierType = value;
			}
		}

		private string zeroAccessNotDisp;
		public string ZeroAccessNotDisp {
			get {
				return this.zeroAccessNotDisp;
			}
			set {
				this.zeroAccessNotDisp = value;
			}
		}

		private string userRank;
		public string UserRank {
			get {
				return this.userRank;
			}
			set {
				this.userRank = value;
			}
		}

		private string newUserFlag;
		public string NewUserFlag {
			get {
				return this.newUserFlag;
			}
			set {
				this.newUserFlag = value;
			}
		}

		private string listType;
		public string ListType {
			get {
				return this.listType;
			}
			set {
				this.listType = value;
			}
		}

		private string sortDirection;
		public string SortDirection {
			get {
				return this.sortDirection;
			}
			set {
				this.sortDirection = value;
			}
		}

		private string sortExpression;
		public string SortExpression {
			get {
				return this.sortExpression;
			}
			set {
				this.sortExpression = value;
			}
		}
	}

	public UserPageViewHourly() {
	}

	public int GetPageCount(SearchCondition pSearchCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();

		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere((SearchCondition)pSearchCondition,ref sWhereClause);

		oSqlBuilder.AppendLine("SELECT																		");
		oSqlBuilder.AppendLine("	COUNT(*)																");
		oSqlBuilder.AppendLine("FROM																		");
		oSqlBuilder.AppendLine("	VW_PAGE01 PG														,	");
		oSqlBuilder.AppendLine("	(																		");
		oSqlBuilder.AppendLine("		SELECT																");
		oSqlBuilder.AppendLine("			SITE_CD														,	");
		oSqlBuilder.AppendLine("			PROGRAM_ROOT												,	");
		oSqlBuilder.AppendLine("			PROGRAM_ID													,	");
		oSqlBuilder.AppendLine("			HTML_DOC_TYPE												,	");
		oSqlBuilder.AppendLine("			SUM(TOTAL_COUNT) AS TOTAL_COUNT									");
		oSqlBuilder.AppendLine("		FROM																");
		oSqlBuilder.AppendLine("			T_USER_PAGE_VIEW_HOURLY											");
		oSqlBuilder.AppendLine("		" + sWhereClause + "												");
		oSqlBuilder.AppendLine("		GROUP BY SITE_CD,PROGRAM_ROOT,PROGRAM_ID,HTML_DOC_TYPE				");
		oSqlBuilder.AppendLine("	) PV																	");
		oSqlBuilder.AppendLine("WHERE																		");
		if (pSearchCondition.ZeroAccessNotDisp.Equals(ViCommConst.FLAG_ON_STR)) {
			oSqlBuilder.AppendLine("	PG.SITE_CD				= PV.SITE_CD								AND	");
			oSqlBuilder.AppendLine("	PG.PROGRAM_ROOT			= PV.PROGRAM_ROOT							AND	");
			oSqlBuilder.AppendLine("	PG.PROGRAM_ID			= PV.PROGRAM_ID								AND	");
			oSqlBuilder.AppendLine("	PG.HTML_DOC_TYPE		= PV.HTML_DOC_TYPE								");
		} else {
			oSqlBuilder.AppendLine("	PG.SITE_CD				= PV.SITE_CD							(+)	AND	");
			oSqlBuilder.AppendLine("	PG.PROGRAM_ROOT			= PV.PROGRAM_ROOT						(+)	AND	");
			oSqlBuilder.AppendLine("	PG.PROGRAM_ID			= PV.PROGRAM_ID							(+)	AND	");
			oSqlBuilder.AppendLine("	PG.HTML_DOC_TYPE		= PV.HTML_DOC_TYPE						(+)		");
		}

		int iPageCount = 0;

		iPageCount = ExecuteSelectCountQueryBase(oSqlBuilder.ToString(),oWhereParams);
		return iPageCount;
	}

	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));
		sSortExpression = this.GetOrder(pSearchCondition);

		oSqlBuilder.AppendLine("SELECT																		");
		oSqlBuilder.AppendLine("	PG.SITE_CD															,	");
		oSqlBuilder.AppendLine("	PG.PROGRAM_ROOT														,	");
		oSqlBuilder.AppendLine("	PG.PROGRAM_ID														,	");
		oSqlBuilder.AppendLine("	PG.HTML_DOC_TYPE													,	");
		oSqlBuilder.AppendLine("	PG.PROGRAM_NM														,	");
		oSqlBuilder.AppendLine("	NVL(PV.COUNT00,0) AS COUNT00										,	");
		oSqlBuilder.AppendLine("	NVL(PV.COUNT01,0) AS COUNT01										,	");
		oSqlBuilder.AppendLine("	NVL(PV.COUNT02,0) AS COUNT02										,	");
		oSqlBuilder.AppendLine("	NVL(PV.COUNT03,0) AS COUNT03										,	");
		oSqlBuilder.AppendLine("	NVL(PV.COUNT04,0) AS COUNT04										,	");
		oSqlBuilder.AppendLine("	NVL(PV.COUNT05,0) AS COUNT05										,	");
		oSqlBuilder.AppendLine("	NVL(PV.COUNT06,0) AS COUNT06										,	");
		oSqlBuilder.AppendLine("	NVL(PV.COUNT07,0) AS COUNT07										,	");
		oSqlBuilder.AppendLine("	NVL(PV.COUNT08,0) AS COUNT08										,	");
		oSqlBuilder.AppendLine("	NVL(PV.COUNT09,0) AS COUNT09										,	");
		oSqlBuilder.AppendLine("	NVL(PV.COUNT10,0) AS COUNT10										,	");
		oSqlBuilder.AppendLine("	NVL(PV.COUNT11,0) AS COUNT11										,	");
		oSqlBuilder.AppendLine("	NVL(PV.COUNT12,0) AS COUNT12										,	");
		oSqlBuilder.AppendLine("	NVL(PV.COUNT13,0) AS COUNT13										,	");
		oSqlBuilder.AppendLine("	NVL(PV.COUNT14,0) AS COUNT14										,	");
		oSqlBuilder.AppendLine("	NVL(PV.COUNT15,0) AS COUNT15										,	");
		oSqlBuilder.AppendLine("	NVL(PV.COUNT16,0) AS COUNT16										,	");
		oSqlBuilder.AppendLine("	NVL(PV.COUNT17,0) AS COUNT17										,	");
		oSqlBuilder.AppendLine("	NVL(PV.COUNT18,0) AS COUNT18										,	");
		oSqlBuilder.AppendLine("	NVL(PV.COUNT19,0) AS COUNT19										,	");
		oSqlBuilder.AppendLine("	NVL(PV.COUNT20,0) AS COUNT20										,	");
		oSqlBuilder.AppendLine("	NVL(PV.COUNT21,0) AS COUNT21										,	");
		oSqlBuilder.AppendLine("	NVL(PV.COUNT22,0) AS COUNT22										,	");
		oSqlBuilder.AppendLine("	NVL(PV.COUNT23,0) AS COUNT23										,	");
		oSqlBuilder.AppendLine("	NVL(PV.TOTAL_COUNT,0) AS TOTAL_COUNT									");
		oSqlBuilder.AppendLine("FROM																		");
		oSqlBuilder.AppendLine("	VW_PAGE01 PG														,	");
		oSqlBuilder.AppendLine("	(																		");
		oSqlBuilder.AppendLine("		SELECT																");
		oSqlBuilder.AppendLine("			SITE_CD														,	");
		oSqlBuilder.AppendLine("			PROGRAM_ROOT												,	");
		oSqlBuilder.AppendLine("			PROGRAM_ID													,	");
		oSqlBuilder.AppendLine("			HTML_DOC_TYPE												,	");
		oSqlBuilder.AppendLine("			SUM(NVL(COUNT00,0)) AS COUNT00								,	");
		oSqlBuilder.AppendLine("			SUM(NVL(COUNT01,0)) AS COUNT01								,	");
		oSqlBuilder.AppendLine("			SUM(NVL(COUNT02,0)) AS COUNT02								,	");
		oSqlBuilder.AppendLine("			SUM(NVL(COUNT03,0)) AS COUNT03								,	");
		oSqlBuilder.AppendLine("			SUM(NVL(COUNT04,0)) AS COUNT04								,	");
		oSqlBuilder.AppendLine("			SUM(NVL(COUNT05,0)) AS COUNT05								,	");
		oSqlBuilder.AppendLine("			SUM(NVL(COUNT06,0)) AS COUNT06								,	");
		oSqlBuilder.AppendLine("			SUM(NVL(COUNT07,0)) AS COUNT07								,	");
		oSqlBuilder.AppendLine("			SUM(NVL(COUNT08,0)) AS COUNT08								,	");
		oSqlBuilder.AppendLine("			SUM(NVL(COUNT09,0)) AS COUNT09								,	");
		oSqlBuilder.AppendLine("			SUM(NVL(COUNT10,0)) AS COUNT10								,	");
		oSqlBuilder.AppendLine("			SUM(NVL(COUNT11,0)) AS COUNT11								,	");
		oSqlBuilder.AppendLine("			SUM(NVL(COUNT12,0)) AS COUNT12								,	");
		oSqlBuilder.AppendLine("			SUM(NVL(COUNT13,0)) AS COUNT13								,	");
		oSqlBuilder.AppendLine("			SUM(NVL(COUNT14,0)) AS COUNT14								,	");
		oSqlBuilder.AppendLine("			SUM(NVL(COUNT15,0)) AS COUNT15								,	");
		oSqlBuilder.AppendLine("			SUM(NVL(COUNT16,0)) AS COUNT16								,	");
		oSqlBuilder.AppendLine("			SUM(NVL(COUNT17,0)) AS COUNT17								,	");
		oSqlBuilder.AppendLine("			SUM(NVL(COUNT18,0)) AS COUNT18								,	");
		oSqlBuilder.AppendLine("			SUM(NVL(COUNT19,0)) AS COUNT19								,	");
		oSqlBuilder.AppendLine("			SUM(NVL(COUNT20,0)) AS COUNT20								,	");
		oSqlBuilder.AppendLine("			SUM(NVL(COUNT21,0)) AS COUNT21								,	");
		oSqlBuilder.AppendLine("			SUM(NVL(COUNT22,0)) AS COUNT22								,	");
		oSqlBuilder.AppendLine("			SUM(NVL(COUNT23,0)) AS COUNT23								,	");
		oSqlBuilder.AppendLine("			SUM(TOTAL_COUNT) AS TOTAL_COUNT									");
		oSqlBuilder.AppendLine("		FROM																");
		oSqlBuilder.AppendLine("			T_USER_PAGE_VIEW_HOURLY											");
		oSqlBuilder.AppendLine("		" + sWhereClause + "												");
		oSqlBuilder.AppendLine("		GROUP BY SITE_CD,PROGRAM_ROOT,PROGRAM_ID,HTML_DOC_TYPE				");
		oSqlBuilder.AppendLine("	) PV																	");
		oSqlBuilder.AppendLine("WHERE																		");
		if (pSearchCondition.ZeroAccessNotDisp.Equals(ViCommConst.FLAG_ON_STR)) {
			oSqlBuilder.AppendLine("	PG.SITE_CD				= PV.SITE_CD								AND	");
			oSqlBuilder.AppendLine("	PG.PROGRAM_ROOT			= PV.PROGRAM_ROOT							AND	");
			oSqlBuilder.AppendLine("	PG.PROGRAM_ID			= PV.PROGRAM_ID								AND	");
			oSqlBuilder.AppendLine("	PG.HTML_DOC_TYPE		= PV.HTML_DOC_TYPE								");
		} else {
			oSqlBuilder.AppendLine("	PG.SITE_CD				= PV.SITE_CD							(+)	AND	");
			oSqlBuilder.AppendLine("	PG.PROGRAM_ROOT			= PV.PROGRAM_ROOT						(+)	AND	");
			oSqlBuilder.AppendLine("	PG.PROGRAM_ID			= PV.PROGRAM_ID							(+)	AND	");
			oSqlBuilder.AppendLine("	PG.HTML_DOC_TYPE		= PV.HTML_DOC_TYPE						(+)		");
		}

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		return ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
	}

	public int GetPageCountTerm(SearchCondition pSearchCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();

		string sWhereClause = string.Empty;
		OracleParameter[] oWhereParams = this.CreateWhere((SearchCondition)pSearchCondition,ref sWhereClause);

		oSqlBuilder.AppendLine("SELECT													");
		oSqlBuilder.AppendLine("	COUNT(*)											");
		oSqlBuilder.AppendLine("FROM													");
		oSqlBuilder.AppendLine("	(													");
		oSqlBuilder.AppendLine("		SELECT											");
		oSqlBuilder.AppendLine("			1											");
		oSqlBuilder.AppendLine("		FROM											");
		oSqlBuilder.AppendLine("			T_USER_PAGE_VIEW_HOURLY						");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("		GROUP BY SITE_CD,REPORT_DATE					");
		oSqlBuilder.AppendLine("	)");

		int iPageCount = 0;

		iPageCount = ExecuteSelectCountQueryBase(oSqlBuilder.ToString(),oWhereParams);
		return iPageCount;
	}

	public DataSet GetPageCollectionTerm(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));
		sSortExpression = "ORDER BY SITE_CD,REPORT_DATE";

		oSqlBuilder.AppendLine("SELECT																");
		oSqlBuilder.AppendLine("	SITE_CD														,	");
		oSqlBuilder.AppendLine("	REPORT_DATE													,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT00,0)) AS COUNT00								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT01,0)) AS COUNT01								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT02,0)) AS COUNT02								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT03,0)) AS COUNT03								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT04,0)) AS COUNT04								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT05,0)) AS COUNT05								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT06,0)) AS COUNT06								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT07,0)) AS COUNT07								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT08,0)) AS COUNT08								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT09,0)) AS COUNT09								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT10,0)) AS COUNT10								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT11,0)) AS COUNT11								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT12,0)) AS COUNT12								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT13,0)) AS COUNT13								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT14,0)) AS COUNT14								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT15,0)) AS COUNT15								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT16,0)) AS COUNT16								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT17,0)) AS COUNT17								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT18,0)) AS COUNT18								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT19,0)) AS COUNT19								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT20,0)) AS COUNT20								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT21,0)) AS COUNT21								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT22,0)) AS COUNT22								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT23,0)) AS COUNT23								,	");
		oSqlBuilder.AppendLine("	SUM(TOTAL_COUNT) AS TOTAL_COUNT									");
		oSqlBuilder.AppendLine("FROM																");
		oSqlBuilder.AppendLine("	T_USER_PAGE_VIEW_HOURLY											");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("GROUP BY SITE_CD,REPORT_DATE										");

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		return ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.ProgramRoot)) {
			SysPrograms.SqlAppendWhere("PROGRAM_ROOT = :PROGRAM_ROOT",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PROGRAM_ROOT",pSearchCondition.ProgramRoot));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.ProgramId)) {
			SysPrograms.SqlAppendWhere("PROGRAM_ID = :PROGRAM_ID",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PROGRAM_ID",pSearchCondition.ProgramId));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.HtmlDocType)) {
			SysPrograms.SqlAppendWhere("HTML_DOC_TYPE = :HTML_DOC_TYPE",ref pWhereClause);
			oParamList.Add(new OracleParameter(":HTML_DOC_TYPE",pSearchCondition.HtmlDocType));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.AccessType)) {
			switch (pSearchCondition.AccessType) {
				case PwViCommConst.AccessType.ALL_ACCESS:
					SysPrograms.SqlAppendWhere("UNIQUE_FLAG = :UNIQUE_FLAG",ref pWhereClause);
					SysPrograms.SqlAppendWhere("PRIORITY_ACCESS_COUNT = :PRIORITY_ACCESS_COUNT",ref pWhereClause);
					oParamList.Add(new OracleParameter(":UNIQUE_FLAG",ViCommConst.FLAG_OFF_STR));
					oParamList.Add(new OracleParameter(":PRIORITY_ACCESS_COUNT",ViCommConst.FLAG_OFF_STR));
					break;
				case PwViCommConst.AccessType.UNIQUE_ACCESS:
					SysPrograms.SqlAppendWhere("UNIQUE_FLAG = :UNIQUE_FLAG",ref pWhereClause);
					oParamList.Add(new OracleParameter(":UNIQUE_FLAG",ViCommConst.FLAG_ON_STR));
					break;
				case PwViCommConst.AccessType.PRIORITY_1ST:
					SysPrograms.SqlAppendWhere("PRIORITY_ACCESS_COUNT = :PRIORITY_ACCESS_COUNT",ref pWhereClause);
					oParamList.Add(new OracleParameter(":PRIORITY_ACCESS_COUNT","1"));
					break;
				case PwViCommConst.AccessType.PRIORITY_2ND:
					SysPrograms.SqlAppendWhere("PRIORITY_ACCESS_COUNT = :PRIORITY_ACCESS_COUNT",ref pWhereClause);
					oParamList.Add(new OracleParameter(":PRIORITY_ACCESS_COUNT","2"));
					break;
				case PwViCommConst.AccessType.PRIORITY_3RD:
					SysPrograms.SqlAppendWhere("PRIORITY_ACCESS_COUNT = :PRIORITY_ACCESS_COUNT",ref pWhereClause);
					oParamList.Add(new OracleParameter(":PRIORITY_ACCESS_COUNT","3"));
					break;
				case PwViCommConst.AccessType.PRIORITY_4TH:
					SysPrograms.SqlAppendWhere("PRIORITY_ACCESS_COUNT = :PRIORITY_ACCESS_COUNT",ref pWhereClause);
					oParamList.Add(new OracleParameter(":PRIORITY_ACCESS_COUNT","4"));
					break;
				case PwViCommConst.AccessType.PRIORITY_5TH:
					SysPrograms.SqlAppendWhere("PRIORITY_ACCESS_COUNT = :PRIORITY_ACCESS_COUNT",ref pWhereClause);
					oParamList.Add(new OracleParameter(":PRIORITY_ACCESS_COUNT","5"));
					break;
				case PwViCommConst.AccessType.PRIORITY_TOTAL:
					SysPrograms.SqlAppendWhere("PRIORITY_ACCESS_COUNT != :PRIORITY_ACCESS_COUNT",ref pWhereClause);
					oParamList.Add(new OracleParameter(":PRIORITY_ACCESS_COUNT","0"));
					break;
			}
		}

		if (!string.IsNullOrEmpty(pSearchCondition.ReportDayFrom)) {
			SysPrograms.SqlAppendWhere("REPORT_DATE >= TO_DATE(:REPORT_DATE_FROM,'YYYY-MM-DD')",ref pWhereClause);
			oParamList.Add(new OracleParameter(":REPORT_DATE_FROM",pSearchCondition.ReportDayFrom));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.ReportDayTo)) {
			SysPrograms.SqlAppendWhere("REPORT_DATE <= TO_DATE(:REPORT_DATE_TO,'YYYY-MM-DD')",ref pWhereClause);
			oParamList.Add(new OracleParameter(":REPORT_DATE_TO",pSearchCondition.ReportDayTo));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.LoginFlag)) {
			SysPrograms.SqlAppendWhere("LOGIN_FLAG = :LOGIN_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":LOGIN_FLAG",pSearchCondition.LoginFlag));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.PageUserAgentType)) {
			if (pSearchCondition.PageUserAgentType.Equals(ViCommConst.DEVICE_SMART_PHONE)) {
				SysPrograms.SqlAppendWhere("PAGE_USER_AGENT_TYPE IN (:PAGE_USER_AGENT_TYPE1,:PAGE_USER_AGENT_TYPE2,:PAGE_USER_AGENT_TYPE3)",ref pWhereClause);
				oParamList.Add(new OracleParameter(":PAGE_USER_AGENT_TYPE1",ViCommConst.DEVICE_SMART_PHONE));
				oParamList.Add(new OracleParameter(":PAGE_USER_AGENT_TYPE2",ViCommConst.DEVICE_ANDROID));
				oParamList.Add(new OracleParameter(":PAGE_USER_AGENT_TYPE3",ViCommConst.DEVICE_IPHONE));
			} else {
				SysPrograms.SqlAppendWhere("PAGE_USER_AGENT_TYPE = :PAGE_USER_AGENT_TYPE",ref pWhereClause);
				oParamList.Add(new OracleParameter(":PAGE_USER_AGENT_TYPE",pSearchCondition.PageUserAgentType));
			}
		}

		if (!string.IsNullOrEmpty(pSearchCondition.CarrierType)) {
			SysPrograms.SqlAppendWhere("CARRIER_TYPE = :CARRIER_TYPE",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CARRIER_TYPE",pSearchCondition.CarrierType));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.UserRank)) {
			SysPrograms.SqlAppendWhere("USER_RANK = :USER_RANK",ref pWhereClause);
			oParamList.Add(new OracleParameter(":USER_RANK",pSearchCondition.UserRank));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.NewUserFlag)) {
			SysPrograms.SqlAppendWhere("NEW_USER_FLAG = :NEW_USER_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":NEW_USER_FLAG",pSearchCondition.NewUserFlag));
		}

		return oParamList.ToArray();
	}

	private string GetOrder(SearchCondition pSearchCondition) {
		return "ORDER BY NVL(PV.TOTAL_COUNT,0) DESC,PG.PROGRAM_ROOT ASC,PG.HTML_DOC_TYPE ASC,PG.PROGRAM_ID ASC";
	}

	public DataSet GetTotalCountCollection(SearchCondition pSearchCondition) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT																");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT00,0)) AS COUNT00								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT01,0)) AS COUNT01								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT02,0)) AS COUNT02								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT03,0)) AS COUNT03								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT04,0)) AS COUNT04								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT05,0)) AS COUNT05								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT06,0)) AS COUNT06								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT07,0)) AS COUNT07								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT08,0)) AS COUNT08								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT09,0)) AS COUNT09								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT10,0)) AS COUNT10								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT11,0)) AS COUNT11								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT12,0)) AS COUNT12								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT13,0)) AS COUNT13								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT14,0)) AS COUNT14								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT15,0)) AS COUNT15								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT16,0)) AS COUNT16								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT17,0)) AS COUNT17								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT18,0)) AS COUNT18								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT19,0)) AS COUNT19								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT20,0)) AS COUNT20								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT21,0)) AS COUNT21								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT22,0)) AS COUNT22								,	");
		oSqlBuilder.AppendLine("	SUM(NVL(COUNT23,0)) AS COUNT23								,	");
		oSqlBuilder.AppendLine("	SUM(TOTAL_COUNT) AS TOTAL_COUNT									");
		oSqlBuilder.AppendLine("FROM																");
		oSqlBuilder.AppendLine("	T_USER_PAGE_VIEW_HOURLY											");
		oSqlBuilder.AppendLine(sWhereClause);

		return ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
	}
}
