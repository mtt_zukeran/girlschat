﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ガルチャにお願い
--	Progaram ID		: Request
--  Creation Date	: 2012.10.29
--  Creater			: PW K.Miyazato
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;
using ViComm;

public class Request:DbSession {
	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}
		private string requestSeq;
		public string RequestSeq {
			get {
				return this.requestSeq;
			}
			set {
				this.requestSeq = value;
			}
		}
		private string sexCd;
		public string SexCd {
			get {
				return this.sexCd;
			}
			set {
				this.sexCd = value;
			}
		}
		private string requestCategorySeq;
		public string RequestCategorySeq {
			get {
				return this.requestCategorySeq;
			}
			set {
				this.requestCategorySeq = value;
			}
		}
		private string requestProgressSeq;
		public string RequestProgressSeq {
			get {
				return this.requestProgressSeq;
			}
			set {
				this.requestProgressSeq = value;
			}
		}
		private List<string> publicStatus = new List<string>();
		public List<string> PublicStatus {
			get {
				return this.publicStatus;
			}
			set {
				this.publicStatus = value;
			}
		}
		private string loginId;
		public string LoginId {
			get {
				return this.loginId;
			}
			set {
				this.loginId = value;
			}
		}
		private string keyword;
		public string Keyword {
			get {
				return this.keyword;
			}
			set {
				this.keyword = value;
			}
		}
		private string sortDirection;
		public string SortDirection {
			get {
				return this.sortDirection;
			}
			set {
				this.sortDirection = value;
			}
		}
		private string sortExpression;
		public string SortExpression {
			get {
				return this.sortExpression;
			}
			set {
				this.sortExpression = value;
			}
		}
	}

	public int GetPageCount(SearchCondition pSearchCondition) {
		int iRecCount;
		string sTblNm;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition, ref sWhereClause));

		if (pSearchCondition.SexCd.Equals(ViCommConst.MAN)) {
			sTblNm = "VW_PW_MAN_REQUEST01";
		} else {
			sTblNm = "VW_PW_CAST_REQUEST01";
		}

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	" + sTblNm);
		oSqlBuilder.AppendLine(sWhereClause);

		iRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
		
		return iRecCount;
	}

	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sExecSql;
		string sTblNm;
		string sWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition, ref sWhereClause));
		sSortExpression = this.GetOrder(pSearchCondition);

		if (pSearchCondition.SexCd.Equals(ViCommConst.MAN)) {
			sTblNm = "VW_PW_MAN_REQUEST01";
		} else {
			sTblNm = "VW_PW_CAST_REQUEST01";
		}

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SITE_CD,");
		oSqlBuilder.AppendLine("	REQUEST_SEQ,");
		oSqlBuilder.AppendLine("	SEX_CD,");
		oSqlBuilder.AppendLine("	TITLE,");
		oSqlBuilder.AppendLine("	TEXT,");
		oSqlBuilder.AppendLine("	REQUEST_CATEGORY_SEQ,");
		oSqlBuilder.AppendLine("	ADMIN_COMMENT,");
		oSqlBuilder.AppendLine("	REQUEST_PROGRESS_SEQ,");
		oSqlBuilder.AppendLine("	GOOD_COUNT,");
		oSqlBuilder.AppendLine("	BAD_COUNT,");
		oSqlBuilder.AppendLine("	CREATE_DATE,");
		oSqlBuilder.AppendLine("	PUBLIC_STATUS,");
		oSqlBuilder.AppendLine("	PUBLIC_STATUS_NM,");
		oSqlBuilder.AppendLine("	PUBLIC_DATE,");
		oSqlBuilder.AppendLine("	REQUEST_CATEGORY_NM,");
		oSqlBuilder.AppendLine("	REQUEST_PROGRESS_NM,");
		oSqlBuilder.AppendLine("	HANDLE_NM,");
		oSqlBuilder.AppendLine("	LOGIN_ID");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	" + sTblNm);
		oSqlBuilder.AppendLine(sWhereClause);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
		
		return oDataSet;
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.RequestSeq)) {
			SysPrograms.SqlAppendWhere("REQUEST_SEQ = :REQUEST_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":REQUEST_SEQ",pSearchCondition.RequestSeq));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.SexCd)) {
			SysPrograms.SqlAppendWhere("SEX_CD = :SEX_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",pSearchCondition.SexCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.RequestCategorySeq)) {
			SysPrograms.SqlAppendWhere("REQUEST_CATEGORY_SEQ = :REQUEST_CATEGORY_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":REQUEST_CATEGORY_SEQ",pSearchCondition.RequestCategorySeq));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.RequestProgressSeq)) {
			SysPrograms.SqlAppendWhere("REQUEST_PROGRESS_SEQ = :REQUEST_PROGRESS_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":REQUEST_PROGRESS_SEQ",pSearchCondition.RequestProgressSeq));
		}

		if (pSearchCondition.PublicStatus.Count > 0) {
			SysPrograms.SqlAppendWhere(string.Format("PUBLIC_STATUS IN ({0})",string.Join(",",pSearchCondition.PublicStatus.ToArray())),ref pWhereClause);
		}

		if (!string.IsNullOrEmpty(pSearchCondition.LoginId)) {
			SysPrograms.SqlAppendWhere("LOGIN_ID = :LOGIN_ID",ref pWhereClause);
			oParamList.Add(new OracleParameter(":LOGIN_ID",pSearchCondition.LoginId));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.Keyword)) {
			SysPrograms.SqlAppendWhere("(TITLE LIKE '%'||:KEYWORD||'%' OR TEXT LIKE '%'||:KEYWORD||'%' OR ADMIN_COMMENT LIKE '%'||:KEYWORD||'%')",ref pWhereClause);
			oParamList.Add(new OracleParameter(":KEYWORD",pSearchCondition.Keyword));
		}

		return oParamList.ToArray();
	}

	private string GetOrder(SearchCondition pSearchCondition) {
		if (string.IsNullOrEmpty(pSearchCondition.SortExpression)) {
			return "ORDER BY CREATE_DATE DESC";
		} else {
			return string.Format("ORDER BY {0} {1},CREATE_DATE DESC",pSearchCondition.SortExpression,pSearchCondition.SortDirection);
		}
	}

	public DataSet GetRequestCategoryList(string pSiteCd,string pSexCd) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		SysPrograms.SqlAppendWhere("SEX_CD = :SEX_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SEX_CD",pSexCd));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	REQUEST_CATEGORY_SEQ,");
		oSqlBuilder.AppendLine("	REQUEST_CATEGORY_NM");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_REQUEST_CATEGORY");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("ORDER BY");
		oSqlBuilder.AppendLine("	REQUEST_CATEGORY_SEQ");

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetRequestProgressList(string pSiteCd) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	REQUEST_PROGRESS_SEQ,");
		oSqlBuilder.AppendLine("	REQUEST_PROGRESS_NM,");
		oSqlBuilder.AppendLine("	REQUEST_PROGRESS_COLOR");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_REQUEST_PROGRESS");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("ORDER BY");
		oSqlBuilder.AppendLine("	REQUEST_PROGRESS_SEQ");

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		return oDataSet;
	}
}
