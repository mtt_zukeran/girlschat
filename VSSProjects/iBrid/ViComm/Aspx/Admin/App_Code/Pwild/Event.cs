﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: イベント情報--	Progaram ID		: Event
--
--  Creation Date	: 2013.07.04
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class Event:DbSession {
	public Event() {
	}

	#region SearchCondition
	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}
		private string eventCategorySeq;
		public string EventCategorySeq {
			get {
				return this.eventCategorySeq;
			}
			set {
				this.eventCategorySeq = value;
			}
		}
		private string prefectureCd;
		public string PrefectureCd {
			get {
				return this.prefectureCd;
			}
			set {
				this.prefectureCd = value;
			}
		}
		private string eventAreaCd;
		public string EventAreaCd {
			get {
				return this.eventAreaCd;
			}
			set {
				this.eventAreaCd = value;
			}
		}
		private string eventNm;
		public string EventNm {
			get {
				return this.eventNm;
			}
			set {
				this.eventNm = value;
			}
		}
		private string eventDateFrom;
		public string EventDateFrom {
			get {
				return this.eventDateFrom;
			}
			set {
				this.eventDateFrom = value;
			}
		}
		private string eventDateTo;
		public string EventDateTo {
			get {
				return this.eventDateTo;
			}
			set {
				this.eventDateTo = value;
			}
		}
	}
	#endregion SearchCondition

	public int GetPageCount(object pSearchCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT COUNT(*) FROM VW_EVENT01 ");
		
		string sWhereClause;
		OracleParameter[] oWhereParams = this.CreateWhere((SearchCondition)pSearchCondition,out sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(object pSearchCondition,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT							");
		oSqlBuilder.AppendLine("	SITE_CD					,	");
		oSqlBuilder.AppendLine("	EVENT_SEQ				,	");
		oSqlBuilder.AppendLine("	EVENT_NM				,	");
		oSqlBuilder.AppendLine("	EVENT_CATEGORY_SEQ		,	");
		oSqlBuilder.AppendLine("	EVENT_CATEGORY_NM		,	");
		oSqlBuilder.AppendLine("	PREFECTURE_CD			,	");
		oSqlBuilder.AppendLine("	PREFECTURE_NM			,	");
		oSqlBuilder.AppendLine("	EVENT_AREA_CD			,	");
		oSqlBuilder.AppendLine("	EVENT_AREA_NM			,	");
		oSqlBuilder.AppendLine("	EVENT_DETAIL			,	");
		oSqlBuilder.AppendLine("	EVENT_START_DATE		,	");
		oSqlBuilder.AppendLine("	EVENT_END_DATE				");
		oSqlBuilder.AppendLine("FROM							");
		oSqlBuilder.AppendLine("	VW_EVENT01					");

		string sWhereClause;
		OracleParameter[] oWhereParams = this.CreateWhere((SearchCondition)pSearchCondition,out sWhereClause);

		string sPagingSql = string.Empty;
		string sSortExpression = "ORDER BY SITE_CD, EVENT_START_DATE DESC, EVENT_SEQ";

		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,out string pWhere) {
		pWhere = string.Empty;
		List<OracleParameter> oOracleParameterList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("SITE_CD",pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.EventCategorySeq)) {
			SysPrograms.SqlAppendWhere("EVENT_CATEGORY_SEQ = :EVENT_CATEGORY_SEQ",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("EVENT_CATEGORY_SEQ",pSearchCondition.EventCategorySeq));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.PrefectureCd)) {
			SysPrograms.SqlAppendWhere("PREFECTURE_CD = :PREFECTURE_CD",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("PREFECTURE_CD",pSearchCondition.PrefectureCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.EventAreaCd)) {
			SysPrograms.SqlAppendWhere("EVENT_AREA_CD = :EVENT_AREA_CD",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("EVENT_AREA_CD",pSearchCondition.EventAreaCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.EventNm)) {
			SysPrograms.SqlAppendWhere("EVENT_NM LIKE '%' || :EVENT_NM || '%'",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("EVENT_NM",pSearchCondition.EventNm));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.EventDateFrom)) {
			DateTime dtFrom = DateTime.Parse(pSearchCondition.EventDateFrom);
			SysPrograms.SqlAppendWhere("EVENT_END_DATE >= :EVENT_DATE_FROM",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("EVENT_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.EventDateTo)) {
			DateTime dtTo = DateTime.Parse(pSearchCondition.EventDateTo);
			SysPrograms.SqlAppendWhere("EVENT_START_DATE <= :EVENT_DATE_TO",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("EVENT_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		return oOracleParameterList.ToArray();
	}
}