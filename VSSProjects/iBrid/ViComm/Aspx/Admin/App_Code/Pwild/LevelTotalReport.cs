﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: レベル分布レポート--	Progaram ID		: LevelTotalReport
--
--  Creation Date	: 2012.12.01
--  Creater			: M&TT A.Taba
--
**************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;


public class LevelTotalReport:DbSession {

	public LevelTotalReport() {
	}
	
	public DataSet GetList(string pSiteCd,string pSexCd) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	GAME_CHARACTER_LEVEL									　  ");
		oSqlBuilder.AppendLine("FROM															");
		oSqlBuilder.AppendLine("	T_LEVEL_UP_EXP												");
		oSqlBuilder.AppendLine("WHERE															");
		oSqlBuilder.AppendLine("	SITE_CD		= :SITE_CD		AND								");
		oSqlBuilder.AppendLine("	SEX_CD		= :SEX_CD										");
		oSqlBuilder.AppendLine("ORDER BY GAME_CHARACTER_LEVEL									");
	
		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":SEX_CD",pSexCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}

		AppendCount(oDs,pSiteCd,pSexCd);
		
		return oDs;
	}

	private void AppendCount(DataSet pDS,string pSiteCd,string pSexCd) {
		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}

		DataColumn col;

		col = new DataColumn(string.Format("GAME_CHARACTER_LEVEL_COUNT"),System.Type.GetType("System.String"));
		pDS.Tables[0].Columns.Add(col);

		foreach (DataRow dr in pDS.Tables[0].Rows) {
			StringBuilder oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine("SELECT											 ");
			oSqlBuilder.AppendLine("	COUNT(*) AS GAME_CHARACTER_LEVEL_COUNT		 ");
			oSqlBuilder.AppendLine(" FROM							   				 ");
			oSqlBuilder.AppendLine("	T_GAME_CHARACTER		     				 ");
			oSqlBuilder.AppendLine(" WHERE											 ");
			oSqlBuilder.AppendLine("	SITE_CD		         = :SITE_CD		AND		 ");
			oSqlBuilder.AppendLine("	SEX_CD		         = :SEX_CD		AND		 ");
			oSqlBuilder.AppendLine("	GAME_CHARACTER_LEVEL = :GAME_CHARACTER_LEVEL ");

			using (DataSet dsSub = new DataSet()) {
				using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
					cmd.BindByName = true;

					cmd.Parameters.Add(":SITE_CD",pSiteCd);
					cmd.Parameters.Add(":SEX_CD",pSexCd);
					cmd.Parameters.Add(":GAME_CHARACTER_LEVEL",dr["GAME_CHARACTER_LEVEL"].ToString());

					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(dsSub);
					}

					dr["GAME_CHARACTER_LEVEL_COUNT"] = dsSub.Tables[0].Rows[0]["GAME_CHARACTER_LEVEL_COUNT"];
				}
			}
		}
	}
}
