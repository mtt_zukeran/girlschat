﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: イベント開催地域--	Progaram ID		: EventArea
--
--  Creation Date	: 2013.07.04
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class EventArea:DbSession {
	public EventArea() {
	}

	#region SearchCondition
	public class SearchCondition {
		private string prefectureCd;
		public string PrefectureCd {
			get {
				return this.prefectureCd;
			}
			set {
				this.prefectureCd = value;
			}
		}
		private string eventAreaNm;
		public string EventAreaNm {
			get {
				return this.eventAreaNm;
			}
			set {
				this.eventAreaNm = value;
			}
		}
	}
	#endregion SearchCondition

	public int GetPageCount(SearchCondition pSearchCondition) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));
		
		oSqlBuilder.AppendLine("SELECT					");
		oSqlBuilder.AppendLine("	COUNT(*)			");
		oSqlBuilder.AppendLine("FROM					");
		oSqlBuilder.AppendLine("	T_EVENT_AREA		");
		oSqlBuilder.AppendLine(sWhereClause);

		return ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
	}

	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	P.EVENT_AREA_CD					,	");
		oSqlBuilder.AppendLine("	P.EVENT_AREA_NM					,	");
		oSqlBuilder.AppendLine("	CD.CODE_NM AS PREFECTURE_NM			");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	(									");
		oSqlBuilder.AppendLine("		SELECT							");
		oSqlBuilder.AppendLine("			PREFECTURE_CD			,	");
		oSqlBuilder.AppendLine("			EVENT_AREA_CD			,	");
		oSqlBuilder.AppendLine("			EVENT_AREA_NM				");
		oSqlBuilder.AppendLine("		FROM							");
		oSqlBuilder.AppendLine("			T_EVENT_AREA				");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("	) P								,	");
		oSqlBuilder.AppendLine("	T_CODE_DTL CD						");
		oSqlBuilder.AppendLine("WHERE									");
		oSqlBuilder.AppendLine("	P.PREFECTURE_CD	= CD.CODE		AND	");
		oSqlBuilder.AppendLine("	CD.CODE_TYPE	= :CODE_TYPE		");
		
		oParamList.Add(new OracleParameter(":CODE_TYPE","67"));
		
		sSortExpression = "ORDER BY PREFECTURE_CD ASC,EVENT_AREA_NM ASC";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		return ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.PrefectureCd)) {
			SysPrograms.SqlAppendWhere("PREFECTURE_CD = :PREFECTURE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PREFECTURE_CD",pSearchCondition.PrefectureCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.EventAreaNm)) {
			SysPrograms.SqlAppendWhere("EVENT_AREA_NM LIKE '%' || :EVENT_AREA_NM || '%'",ref pWhereClause);
			oParamList.Add(new OracleParameter(":EVENT_AREA_NM",pSearchCondition.EventAreaNm));
		}

		return oParamList.ToArray();
	}
	
	public DataSet GetList(string pPrefectureCd) {

		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();
			StringBuilder oSql = new StringBuilder();
			List<OracleParameter> oParameterList = new List<OracleParameter>();

			string sOrder = "ORDER BY EVENT_AREA_CD";
			oSql.AppendLine("SELECT						");
			oSql.AppendLine("	EVENT_AREA_CD		,	");
			oSql.AppendLine("	EVENT_AREA_NM			");
			oSql.AppendLine("FROM						");
			oSql.AppendLine("	T_EVENT_AREA			");

			if (!string.IsNullOrEmpty(pPrefectureCd)) {
				oSql.AppendLine(" WHERE	PREFECTURE_CD	= :PREFECTURE_CD		");
				oParameterList.Add(new OracleParameter(":PREFECTURE_CD",pPrefectureCd));
			}

			oSql.AppendLine(sOrder);

			using (cmd = CreateSelectCommand(oSql.ToString(),conn)) {
				cmd.Parameters.AddRange(oParameterList.ToArray());
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_EVENT_AREA");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
}