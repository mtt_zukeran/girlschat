﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: クエスト設定--	Progaram ID		: Quest
--
--  Creation Date	: 2012.07.05
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class Quest:DbSession {
	public Quest() {
	}

	public int GetPageCount(string pSiteCd,string pSexCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT COUNT(*) FROM T_QUEST WHERE SITE_CD = :SITE_CD AND SEX_CD = :SEX_CD");

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":SEX_CD",pSexCd);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pSexCd,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT						");
		oSqlBuilder.AppendLine("	SITE_CD				,	");
		oSqlBuilder.AppendLine("	QUEST_SEQ			,	");
		oSqlBuilder.AppendLine("	SEX_CD				,	");
		oSqlBuilder.AppendLine("	QUEST_NM			,	");
		oSqlBuilder.AppendLine("	PUBLISH_START_DATE	,	");
		oSqlBuilder.AppendLine("	PUBLISH_END_DATE	,	");
		oSqlBuilder.AppendLine("	GET_REWARD_END_DATE	,	");
		oSqlBuilder.AppendLine("	PUBLISH_FLAG			");
		oSqlBuilder.AppendLine("FROM						");
		oSqlBuilder.AppendLine("	T_QUEST					");
		oSqlBuilder.AppendLine("WHERE						");
		oSqlBuilder.AppendLine("	SITE_CD	= :SITE_CD	AND	");
		oSqlBuilder.AppendLine("	SEX_CD	= :SEX_CD		");

		string sPagingSql = string.Empty;
		string sSortExpression = "ORDER BY SITE_CD, SEX_CD, PUBLISH_START_DATE DESC";

		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":SEX_CD",pSexCd);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}

	public string GetQuestNm(string pSiteCd,string pQuestSeq,string pSexCd) {
		string sValue = null;

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT														");
		oSqlBuilder.AppendLine("	QUEST_NM												");
		oSqlBuilder.AppendLine(" FROM							   							");
		oSqlBuilder.AppendLine("	T_QUEST													");
		oSqlBuilder.AppendLine(" WHERE														");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD				AND		");
		oSqlBuilder.AppendLine("	QUEST_SEQ				= :QUEST_SEQ			AND		");
		oSqlBuilder.AppendLine("	SEX_CD			        = :SEX_CD   					");

		try {
			this.conn = this.DbConnect();

			using (this.cmd = this.CreateSelectCommand(oSqlBuilder.ToString(),this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.Add(":SITE_CD",pSiteCd);
				this.cmd.Parameters.Add(":QUEST_SEQ",pQuestSeq);
				this.cmd.Parameters.Add(":SEX_CD",pSexCd);

				OracleDataReader oDataReader = this.cmd.ExecuteReader();

				if (oDataReader.Read()) {
					sValue = iBridUtil.GetStringValue(oDataReader["QUEST_NM"].ToString());
				}
			}
		} finally {
			this.conn.Close();
		}

		return sValue;
	}
}
