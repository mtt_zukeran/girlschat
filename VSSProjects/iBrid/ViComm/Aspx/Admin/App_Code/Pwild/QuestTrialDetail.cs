﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 美女コレ クエストクリア条件カテゴリ
--	Progaram ID		: QuestTrialCategory
--
--  Creation Date	: 2012.07.07
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;


public class QuestTrialDetail:DbSession {

	public QuestTrialDetail() {
	}

	public DataSet GetListByTrialCategoryNo(string pSiteCd,string pTrialCategoryNo) {

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	SITE_CD							,	");
		oSqlBuilder.AppendLine("	TRIAL_CATEGORY_DETAIL_NO		,	");
		oSqlBuilder.AppendLine("	TRIAL_CATEGORY_DETAIL				");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	T_QUEST_TRIAL_DETAIL				");
		oSqlBuilder.AppendLine("WHERE									");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD		");
		
		if(!string.IsNullOrEmpty(pTrialCategoryNo)) {
			oSqlBuilder.AppendLine(" AND	TRIAL_CATEGORY_NO	= :TRIAL_CATEGORY_NO			");
		}

		string sSortExpression = "ORDER BY SITE_CD, TRIAL_CATEGORY_DETAIL_NO";

		oSqlBuilder.AppendLine(sSortExpression);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				if (!string.IsNullOrEmpty(pTrialCategoryNo)) {
					cmd.Parameters.Add(":TRIAL_CATEGORY_NO",pTrialCategoryNo);
				}

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}
}
