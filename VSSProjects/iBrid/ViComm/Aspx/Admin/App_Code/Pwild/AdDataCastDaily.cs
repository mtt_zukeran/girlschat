﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 出演者広告集計(日別)
--	Progaram ID		: AdDataCastDaily
--  Creation Date	: 2014.06.16
--  Creater			: Y.Ikemiya
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;
using iBridCommLib;
using ViComm;
using System.Collections.Generic;
using ViComm.Extension.Pwild;

public class AdDataCastDaily:DbSession {

	public AdDataCastDaily() {
	}

	public DataSet GetListDaily(string pSiteCd,string pAdCd,string pYear,string pMonth) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT																									");
		oSqlBuilder.AppendLine("	C.DAYS																							,	");
		oSqlBuilder.AppendLine("	AD.AD_COST																						,	");
		oSqlBuilder.AppendLine("	AD.ACCESS_COUNT																					,	");
		oSqlBuilder.AppendLine("	AD.REGIST_COUNT																					,	");
		oSqlBuilder.AppendLine("	AD.REGIST_RATE																					,	");
		oSqlBuilder.AppendLine("	AD.REGIST_COUNT_AD																				,	");
		oSqlBuilder.AppendLine("	AD.LOGIN_COUNT																					,	");
		oSqlBuilder.AppendLine("	AD.GET_POINT_CAST_COUNT																			,	");
		oSqlBuilder.AppendLine("	AD.GET_POINT_CAST_RATE																			,	");
		oSqlBuilder.AppendLine("	AD.START_PAYMENT_CAST_COUNT																		,	");
		oSqlBuilder.AppendLine("	AD.START_PAYMENT_CAST_RATE																		,	");
		oSqlBuilder.AppendLine("	AD.AD_COST_BY_START_PAYMENT																		,	");
		oSqlBuilder.AppendLine("	AD.AD_COST_BY_REGIST																			,	");
		oSqlBuilder.AppendLine("	AD.ENT_MONTH_PAYMENT_COUNT																		,	");
		oSqlBuilder.AppendLine("	AD.ENT_MONTH_PAYMENT_AMT																		,	");
		oSqlBuilder.AppendLine("	AD.ENT_MONTH_RETURN_RATE																		,	");
		oSqlBuilder.AppendLine("	AD.REPEAT_PAYMENT_COUNT																			,	");
		oSqlBuilder.AppendLine("	AD.REPEAT_PAYMENT_AMT																			,	");
		oSqlBuilder.AppendLine("	AD.TOTAL_PAYMENT_COUNT																			,	");
		oSqlBuilder.AppendLine("	AD.TOTAL_PAYMENT_AMT																			,	");
		oSqlBuilder.AppendLine("	AD.REPEAT_RATE																					,	");
		oSqlBuilder.AppendLine("	AD.AGGR_AD_COST_RATE																			,	");
		oSqlBuilder.AppendLine("	AD.TOTAL_AMT_BY_CAST																				");
		oSqlBuilder.AppendLine("FROM																									");
		oSqlBuilder.AppendLine("	(																									");
		oSqlBuilder.AppendLine("		SELECT																							");
		oSqlBuilder.AppendLine("			TO_CHAR(STARTDATE + ROWNUM -1, 'YYYY/MM/DD') AS DAYS										");
		oSqlBuilder.AppendLine("		FROM																							");
		oSqlBuilder.AppendLine("			(																							");
		oSqlBuilder.AppendLine("				SELECT																					");
		oSqlBuilder.AppendLine("					TRUNC(TO_DATE(:REPORT_DAY_START)) AS STARTDATE									,	");
		oSqlBuilder.AppendLine("					TO_NUMBER(TO_CHAR(LAST_DAY(TO_DATE(:REPORT_DAY_START)), 'DD')) AS DAYS				");
		oSqlBuilder.AppendLine("				FROM																					");
		oSqlBuilder.AppendLine("					DUAL																				");
		oSqlBuilder.AppendLine("			)																						,	");
		oSqlBuilder.AppendLine("			ALL_CATALOG																					");
		oSqlBuilder.AppendLine("		WHERE																							");
		oSqlBuilder.AppendLine("			ROWNUM <= DAYS																				");
		oSqlBuilder.AppendLine("	) C																								,	");
		oSqlBuilder.AppendLine("	(																									");
		oSqlBuilder.AppendLine("		SELECT																							");
		oSqlBuilder.AppendLine("			AD_COST																					,	");
		oSqlBuilder.AppendLine("			ACCESS_COUNT																			,	");
		oSqlBuilder.AppendLine("			REGIST_COUNT																			,	");
		oSqlBuilder.AppendLine("			CASE																						");
		oSqlBuilder.AppendLine("				WHEN REGIST_COUNT > 0																	");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(REGIST_COUNT / ACCESS_COUNT * 100,2))								");
		oSqlBuilder.AppendLine("				ELSE '-'																				");
		oSqlBuilder.AppendLine("			END AS REGIST_RATE																		,	");
		oSqlBuilder.AppendLine("			REGIST_COUNT_AD																			,	");
		oSqlBuilder.AppendLine("			LOGIN_COUNT																				,	");
		oSqlBuilder.AppendLine("			GET_POINT_CAST_COUNT																	,	");
		oSqlBuilder.AppendLine("			CASE																						");
		oSqlBuilder.AppendLine("				WHEN REGIST_COUNT > 0																	");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(GET_POINT_CAST_COUNT / REGIST_COUNT * 100,2))						");
		oSqlBuilder.AppendLine("				ELSE '-'																				");
		oSqlBuilder.AppendLine("			END AS GET_POINT_CAST_RATE																,	");
		oSqlBuilder.AppendLine("			START_PAYMENT_CAST_COUNT																,	");
		oSqlBuilder.AppendLine("			CASE																						");
		oSqlBuilder.AppendLine("				WHEN REGIST_COUNT > 0																	");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(START_PAYMENT_CAST_COUNT / REGIST_COUNT * 100,2))					");
		oSqlBuilder.AppendLine("				ELSE '-'																				");
		oSqlBuilder.AppendLine("			END AS START_PAYMENT_CAST_RATE															,	");
		oSqlBuilder.AppendLine("			CASE																						");
		oSqlBuilder.AppendLine("				WHEN START_PAYMENT_CAST_COUNT > 0														");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(AD_COST / START_PAYMENT_CAST_COUNT,2))								");
		oSqlBuilder.AppendLine("				ELSE '-'																				");
		oSqlBuilder.AppendLine("			END AS AD_COST_BY_START_PAYMENT															,	");
		oSqlBuilder.AppendLine("			CASE																						");
		oSqlBuilder.AppendLine("				WHEN REGIST_COUNT > 0																	");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(AD_COST / REGIST_COUNT,2))											");
		oSqlBuilder.AppendLine("				ELSE '-'																				");
		oSqlBuilder.AppendLine("			END AS AD_COST_BY_REGIST																,	");
		oSqlBuilder.AppendLine("			ENT_MONTH_PAYMENT_COUNT																	,	");
		oSqlBuilder.AppendLine("			ENT_MONTH_PAYMENT_AMT																	,	");
		oSqlBuilder.AppendLine("			CASE																						");
		oSqlBuilder.AppendLine("				WHEN AD_COST > 0																		");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(ENT_MONTH_PAYMENT_AMT / AD_COST * 100,2))							");
		oSqlBuilder.AppendLine("				ELSE '-'																				");
		oSqlBuilder.AppendLine("			END AS ENT_MONTH_RETURN_RATE															,	");
		oSqlBuilder.AppendLine("			REPEAT_PAYMENT_COUNT																	,	");
		oSqlBuilder.AppendLine("			REPEAT_PAYMENT_AMT																		,	");
		oSqlBuilder.AppendLine("			ENT_MONTH_PAYMENT_COUNT + REPEAT_PAYMENT_COUNT AS TOTAL_PAYMENT_COUNT					,	");
		oSqlBuilder.AppendLine("			ENT_MONTH_PAYMENT_AMT + REPEAT_PAYMENT_AMT AS TOTAL_PAYMENT_AMT							,	");
		oSqlBuilder.AppendLine("			CASE																						");
		oSqlBuilder.AppendLine("				WHEN AGGR_PAYMENT_CAST_COUNT > 0														");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(REPEAT_PAYMENT_COUNT / AGGR_PAYMENT_CAST_COUNT * 100,2))				");
		oSqlBuilder.AppendLine("				ELSE '-'																				");
		oSqlBuilder.AppendLine("			END AS REPEAT_RATE																		,	");
		oSqlBuilder.AppendLine("			CASE																						");
		oSqlBuilder.AppendLine("				WHEN AGGR_PAYMENT_AMT > 0																");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC(AGGR_AD_COST / AGGR_PAYMENT_AMT * 100,2))							");
		oSqlBuilder.AppendLine("				ELSE '-'																				");
		oSqlBuilder.AppendLine("			END AS AGGR_AD_COST_RATE																,	");
		oSqlBuilder.AppendLine("			CASE																						");
		oSqlBuilder.AppendLine("				WHEN ENT_MONTH_PAYMENT_COUNT + REPEAT_PAYMENT_COUNT > 0									");
		oSqlBuilder.AppendLine("				THEN TO_CHAR(TRUNC((ENT_MONTH_PAYMENT_AMT + REPEAT_PAYMENT_AMT) / (ENT_MONTH_PAYMENT_COUNT + REPEAT_PAYMENT_COUNT),2))");
		oSqlBuilder.AppendLine("				ELSE '-'																				");
		oSqlBuilder.AppendLine("			END AS TOTAL_AMT_BY_CAST																,	");
		oSqlBuilder.AppendLine("			REPORT_DAY																					");
		oSqlBuilder.AppendLine("		FROM																							");
		oSqlBuilder.AppendLine("			T_AD_DATA_CAST_DAILY																		");
		oSqlBuilder.AppendLine("		WHERE																							");
		oSqlBuilder.AppendLine("			SITE_CD		= :SITE_CD																	AND	");
		oSqlBuilder.AppendLine("			AD_CD		= :AD_CD																	AND	");
		oSqlBuilder.AppendLine("			REPORT_DAY	>= :REPORT_DAY_START														AND	");
		oSqlBuilder.AppendLine("			REPORT_DAY	<= :REPORT_DAY_END																");
		oSqlBuilder.AppendLine("	) AD																								");
		oSqlBuilder.AppendLine("WHERE																									");
		oSqlBuilder.AppendLine("	C.DAYS	= AD.REPORT_DAY																		(+)		");
		oSqlBuilder.AppendLine("ORDER BY C.DAYS ASC																						");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":AD_CD",pAdCd));

		DateTime dtReportDayStart = DateTime.Parse(string.Format("{0}/{1}/01",pYear,pMonth));
		DateTime dtReportDayEnd = dtReportDayStart.AddMonths(1).AddDays(-1);
		oParamList.Add(new OracleParameter(":REPORT_DAY_START",string.Format("{0}/{1}/01",pYear,pMonth)));
		oParamList.Add(new OracleParameter(":REPORT_DAY_END",dtReportDayEnd.ToString("yyyy/MM/dd")));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
}
