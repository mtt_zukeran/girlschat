﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ガルチャにお願い評価
--	Progaram ID		: RequestValue
--  Creation Date	: 2012.10.30
--  Creater			: PW K.Miyazato
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;
using ViComm;

public class RequestValue:DbSession {
	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}
		private string requestValueSeq;
		public string RequestValueSeq {
			get {
				return this.requestValueSeq;
			}
			set {
				this.requestValueSeq = value;
			}
		}
		private string sexCd;
		public string SexCd {
			get {
				return this.sexCd;
			}
			set {
				this.sexCd = value;
			}
		}
		private string requestSeq;
		public string RequestSeq {
			get {
				return this.requestSeq;
			}
			set {
				this.requestSeq = value;
			}
		}
		private List<string> valurNo = new List<string>();
		public List<string> ValueNo {
			get {
				return this.valurNo;
			}
			set {
				this.valurNo = value;
			}
		}
		private List<string> commentPublicStatus = new List<string>();
		public List<string> CommentPublicStatus {
			get {
				return this.commentPublicStatus;
			}
			set {
				this.commentPublicStatus = value;
			}
		}
		private string loginId;
		public string LoginId {
			get {
				return this.loginId;
			}
			set {
				this.loginId = value;
			}
		}
		private string keyword;
		public string Keyword {
			get {
				return this.keyword;
			}
			set {
				this.keyword = value;
			}
		}
		private string commentFlag;
		public string CommentFlag {
			get {
				return this.commentFlag;
			}
			set {
				this.commentFlag = value;
			}
		}
		private string sortDirection;
		public string SortDirection {
			get {
				return this.sortDirection;
			}
			set {
				this.sortDirection = value;
			}
		}
		private string sortExpression;
		public string SortExpression {
			get {
				return this.sortExpression;
			}
			set {
				this.sortExpression = value;
			}
		}
	}

	public int GetPageCount(SearchCondition pSearchCondition) {
		int iRecCount;
		string sTblNm;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));

		if (pSearchCondition.SexCd.Equals(ViCommConst.MAN)) {
			sTblNm = "VW_PW_MAN_REQUEST_VALUE01";
		} else {
			sTblNm = "VW_PW_CAST_REQUEST_VALUE01";
		}

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	" + sTblNm);
		oSqlBuilder.AppendLine(sWhereClause);

		iRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());

		return iRecCount;
	}

	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sExecSql;
		string sTblNm;
		string sWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));
		sSortExpression = this.GetOrder(pSearchCondition);

		if (pSearchCondition.SexCd.Equals(ViCommConst.MAN)) {
			sTblNm = "VW_PW_MAN_REQUEST_VALUE01";
		} else {
			sTblNm = "VW_PW_CAST_REQUEST_VALUE01";
		}

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SITE_CD,");
		oSqlBuilder.AppendLine("	REQUEST_VALUE_SEQ,");
		oSqlBuilder.AppendLine("	SEX_CD,");
		oSqlBuilder.AppendLine("	REQUEST_SEQ,");
		oSqlBuilder.AppendLine("	VALUE_NO,");
		oSqlBuilder.AppendLine("	VALUE_NM,");
		oSqlBuilder.AppendLine("	COMMENT_TEXT,");
		oSqlBuilder.AppendLine("	CREATE_DATE,");
		oSqlBuilder.AppendLine("	COMMENT_PUBLIC_STATUS,");
		oSqlBuilder.AppendLine("	COMMENT_PUBLIC_STATUS_NM,");
		oSqlBuilder.AppendLine("	COMMENT_PUBLIC_DATE,");
		oSqlBuilder.AppendLine("	REQUEST_TITLE,");
		oSqlBuilder.AppendLine("	REQUEST_TEXT,");
		oSqlBuilder.AppendLine("	HANDLE_NM,");
		oSqlBuilder.AppendLine("	LOGIN_ID");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	" + sTblNm);
		oSqlBuilder.AppendLine(sWhereClause);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.RequestValueSeq)) {
			SysPrograms.SqlAppendWhere("REQUEST_VALUE_SEQ = :REQUEST_VALUE_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":REQUEST_VALUE_SEQ",pSearchCondition.RequestValueSeq));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.SexCd)) {
			SysPrograms.SqlAppendWhere("SEX_CD = :SEX_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",pSearchCondition.SexCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.RequestSeq)) {
			SysPrograms.SqlAppendWhere("REQUEST_SEQ = :REQUEST_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":REQUEST_SEQ",pSearchCondition.RequestSeq));
		}

		if (pSearchCondition.ValueNo.Count > 0) {
			SysPrograms.SqlAppendWhere(string.Format("VALUE_NO IN ({0})",string.Join(",",pSearchCondition.ValueNo.ToArray())),ref pWhereClause);
		}

		if (pSearchCondition.CommentPublicStatus.Count > 0) {
			SysPrograms.SqlAppendWhere(string.Format("COMMENT_PUBLIC_STATUS IN ({0})",string.Join(",",pSearchCondition.CommentPublicStatus.ToArray())),ref pWhereClause);
		}

		if (!string.IsNullOrEmpty(pSearchCondition.LoginId)) {
			SysPrograms.SqlAppendWhere("LOGIN_ID = :LOGIN_ID",ref pWhereClause);
			oParamList.Add(new OracleParameter(":LOGIN_ID",pSearchCondition.LoginId));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.Keyword)) {
			SysPrograms.SqlAppendWhere("COMMENT_TEXT LIKE '%'||:KEYWORD||'%'",ref pWhereClause);
			oParamList.Add(new OracleParameter(":KEYWORD",pSearchCondition.Keyword));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.CommentFlag)) {
			if (pSearchCondition.CommentFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				SysPrograms.SqlAppendWhere("COMMENT_TEXT IS NOT NULL",ref pWhereClause);
			}
		}

		return oParamList.ToArray();
	}

	private string GetOrder(SearchCondition pSearchCondition) {
		if (string.IsNullOrEmpty(pSearchCondition.SortExpression)) {
			return "ORDER BY CREATE_DATE DESC";
		} else {
			return string.Format("ORDER BY {0} {1},CREATE_DATE DESC",pSearchCondition.SortExpression,pSearchCondition.SortDirection);
		}
	}
}
