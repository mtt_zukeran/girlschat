﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 課金者ログイン集計データクラス--	Progaram ID		: UserLoginCountDaily
--
--  Creation Date	: 2016.06.27
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class UserLoginCountDaily:DbSession {
	public UserLoginCountDaily() {
	}

	public DataSet GetList(string pSiteCd,string pYear,string pMonth,string pSinceLastLoginDays) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oSqlBuilder.AppendLine("SELECT																									");
		oSqlBuilder.AppendLine("	SUBSTR(C.DAYS,9) AS DISPLAY_DAY																	,	");
		oSqlBuilder.AppendLine("	TO_CHAR(TO_DATE(C.DAYS),'Dy','NLS_DATE_LANGUAGE = ENGLISH') AS REPORT_DAY_OF_WEEK				,	");
		oSqlBuilder.AppendLine("	NVL(LC.START_MAIL_ADDR_STATUS_OK,0) AS START_MAIL_ADDR_STATUS_OK								,	");
		oSqlBuilder.AppendLine("	NVL(LC.START_MAIL_ADDR_STATUS_FT,0) AS START_MAIL_ADDR_STATUS_FT								,	");
		oSqlBuilder.AppendLine("	NVL(LC.START_MAIL_ADDR_STATUS_NG,0) AS START_MAIL_ADDR_STATUS_NG								,	");
		oSqlBuilder.AppendLine("	NVL(LC.TOTAL_START_COUNT,0) AS TOTAL_START_COUNT												,	");
		oSqlBuilder.AppendLine("	NVL(LC.MAIL_ADDR_STATUS_OK_OK,0) AS MAIL_ADDR_STATUS_OK_OK										,	");
		oSqlBuilder.AppendLine("	NVL(LC.MAIL_ADDR_STATUS_OK_FT,0) AS MAIL_ADDR_STATUS_OK_FT										,	");
		oSqlBuilder.AppendLine("	NVL(LC.MAIL_ADDR_STATUS_OK_NG,0) AS MAIL_ADDR_STATUS_OK_NG										,	");
		oSqlBuilder.AppendLine("	NVL(LC.TOTAL_START_OK_LOGIN,0) AS TOTAL_START_OK_LOGIN											,	");
		oSqlBuilder.AppendLine("	NVL(LC.MAIL_ADDR_STATUS_FT_OK,0) AS MAIL_ADDR_STATUS_FT_OK										,	");
		oSqlBuilder.AppendLine("	NVL(LC.MAIL_ADDR_STATUS_FT_FT,0) AS MAIL_ADDR_STATUS_FT_FT										,	");
		oSqlBuilder.AppendLine("	NVL(LC.MAIL_ADDR_STATUS_FT_NG,0) AS MAIL_ADDR_STATUS_FT_NG										,	");
		oSqlBuilder.AppendLine("	NVL(LC.TOTAL_START_FT_LOGIN,0) AS TOTAL_START_FT_LOGIN											,	");
		oSqlBuilder.AppendLine("	NVL(LC.MAIL_ADDR_STATUS_NG_OK,0) AS MAIL_ADDR_STATUS_NG_OK										,	");
		oSqlBuilder.AppendLine("	NVL(LC.MAIL_ADDR_STATUS_NG_FT,0) AS MAIL_ADDR_STATUS_NG_FT										,	");
		oSqlBuilder.AppendLine("	NVL(LC.MAIL_ADDR_STATUS_NG_NG,0) AS MAIL_ADDR_STATUS_NG_NG										,	");
		oSqlBuilder.AppendLine("	NVL(LC.TOTAL_START_NG_LOGIN,0) AS TOTAL_START_NG_LOGIN											,	");
		oSqlBuilder.AppendLine("	NVL(LC.TOTAL_END_OK_LOGIN,0) AS TOTAL_END_OK_LOGIN												,	");
		oSqlBuilder.AppendLine("	NVL(LC.TOTAL_END_FT_LOGIN,0) AS TOTAL_END_FT_LOGIN												,	");
		oSqlBuilder.AppendLine("	NVL(LC.TOTAL_END_NG_LOGIN,0) AS TOTAL_END_NG_LOGIN												,	");
		oSqlBuilder.AppendLine("	NVL(LC.TOTAL_LOGIN_COUNT,0) AS TOTAL_LOGIN_COUNT													");
		oSqlBuilder.AppendLine("FROM																									");
		oSqlBuilder.AppendLine("	(																									");
		oSqlBuilder.AppendLine("		SELECT																							");
		oSqlBuilder.AppendLine("			TO_CHAR(STARTDATE + ROWNUM -1, 'YYYY/MM/DD') AS DAYS									,	");
		oSqlBuilder.AppendLine("			STARTDATE + ROWNUM -1 AS REPORT_DAY															");
		oSqlBuilder.AppendLine("		FROM																							");
		oSqlBuilder.AppendLine("			(																							");
		oSqlBuilder.AppendLine("				SELECT																					");
		oSqlBuilder.AppendLine("					TRUNC(TO_DATE(:REPORT_MONTH || '01')) AS STARTDATE								,	");
		oSqlBuilder.AppendLine("					TO_NUMBER(TO_CHAR(LAST_DAY(TO_DATE(:REPORT_MONTH || '01')), 'DD')) AS DAYS			");
		oSqlBuilder.AppendLine("				FROM																					");
		oSqlBuilder.AppendLine("					DUAL																				");
		oSqlBuilder.AppendLine("			)																						,	");
		oSqlBuilder.AppendLine("			ALL_CATALOG																					");
		oSqlBuilder.AppendLine("		WHERE																							");
		oSqlBuilder.AppendLine("			ROWNUM <= DAYS																				");
		oSqlBuilder.AppendLine("	) C																								,	");
		oSqlBuilder.AppendLine("	(																									");
		oSqlBuilder.AppendLine("		SELECT																							");
		oSqlBuilder.AppendLine("			REPORT_DAY																				,	");
		oSqlBuilder.AppendLine("			START_MAIL_ADDR_STATUS_OK																,	");
		oSqlBuilder.AppendLine("			START_MAIL_ADDR_STATUS_FT																,	");
		oSqlBuilder.AppendLine("			START_MAIL_ADDR_STATUS_NG																,	");
		oSqlBuilder.AppendLine("			START_MAIL_ADDR_STATUS_OK + START_MAIL_ADDR_STATUS_FT + START_MAIL_ADDR_STATUS_NG AS TOTAL_START_COUNT	,	");
		oSqlBuilder.AppendLine("			MAIL_ADDR_STATUS_OK_OK																	,	");
		oSqlBuilder.AppendLine("			MAIL_ADDR_STATUS_OK_FT																	,	");
		oSqlBuilder.AppendLine("			MAIL_ADDR_STATUS_OK_NG																	,	");
		oSqlBuilder.AppendLine("			MAIL_ADDR_STATUS_OK_OK + MAIL_ADDR_STATUS_OK_FT + MAIL_ADDR_STATUS_OK_NG AS TOTAL_START_OK_LOGIN		,	");
		oSqlBuilder.AppendLine("			MAIL_ADDR_STATUS_FT_OK																	,	");
		oSqlBuilder.AppendLine("			MAIL_ADDR_STATUS_FT_FT																	,	");
		oSqlBuilder.AppendLine("			MAIL_ADDR_STATUS_FT_NG																	,	");
		oSqlBuilder.AppendLine("			MAIL_ADDR_STATUS_FT_OK + MAIL_ADDR_STATUS_FT_FT + MAIL_ADDR_STATUS_FT_NG AS TOTAL_START_FT_LOGIN		,	");
		oSqlBuilder.AppendLine("			MAIL_ADDR_STATUS_NG_OK																	,	");
		oSqlBuilder.AppendLine("			MAIL_ADDR_STATUS_NG_FT																	,	");
		oSqlBuilder.AppendLine("			MAIL_ADDR_STATUS_NG_NG																	,	");
		oSqlBuilder.AppendLine("			MAIL_ADDR_STATUS_NG_OK + MAIL_ADDR_STATUS_NG_FT + MAIL_ADDR_STATUS_NG_NG AS TOTAL_START_NG_LOGIN		,	");
		oSqlBuilder.AppendLine("			MAIL_ADDR_STATUS_OK_OK + MAIL_ADDR_STATUS_FT_OK + MAIL_ADDR_STATUS_NG_OK AS TOTAL_END_OK_LOGIN			,	");
		oSqlBuilder.AppendLine("			MAIL_ADDR_STATUS_OK_FT + MAIL_ADDR_STATUS_FT_FT + MAIL_ADDR_STATUS_NG_FT AS TOTAL_END_FT_LOGIN			,	");
		oSqlBuilder.AppendLine("			MAIL_ADDR_STATUS_OK_NG + MAIL_ADDR_STATUS_FT_NG + MAIL_ADDR_STATUS_NG_NG AS TOTAL_END_NG_LOGIN			,	");
		oSqlBuilder.AppendLine("			MAIL_ADDR_STATUS_OK_OK + MAIL_ADDR_STATUS_OK_FT + MAIL_ADDR_STATUS_OK_NG + MAIL_ADDR_STATUS_FT_OK + MAIL_ADDR_STATUS_FT_FT + MAIL_ADDR_STATUS_FT_NG + MAIL_ADDR_STATUS_NG_OK + MAIL_ADDR_STATUS_NG_FT + MAIL_ADDR_STATUS_NG_NG AS TOTAL_LOGIN_COUNT	");
		oSqlBuilder.AppendLine("		FROM																							");
		oSqlBuilder.AppendLine("			T_USER_LOGIN_COUNT_DAILY																	");
		oSqlBuilder.AppendLine("		WHERE																							");
		oSqlBuilder.AppendLine("			SITE_CD					= :SITE_CD														AND	");
		oSqlBuilder.AppendLine("			REPORT_MONTH			= :REPORT_MONTH													AND	");
		oSqlBuilder.AppendLine("			SINCE_LAST_LOGIN_DAYS	= :SINCE_LAST_LOGIN_DAYS											");
		oSqlBuilder.AppendLine("		ORDER BY SITE_CD,REPORT_DAY																		");
		oSqlBuilder.AppendLine("	) LC																								");
		oSqlBuilder.AppendLine("WHERE																									");
		if (string.Format("{0}/{1}",pYear,pMonth).Equals(DateTime.Now.ToString("yyyy/MM"))) {
			oSqlBuilder.AppendLine("	C.REPORT_DAY	<= TRUNC(SYSDATE)																AND	");
		}
		oSqlBuilder.AppendLine("	C.DAYS			= LC.REPORT_DAY																(+)		");
		oSqlBuilder.AppendLine("ORDER BY C.DAYS ASC																						");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":REPORT_MONTH",string.Format("{0}/{1}",pYear,pMonth)));
		oParamList.Add(new OracleParameter(":SINCE_LAST_LOGIN_DAYS",pSinceLastLoginDays));
		
		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		return oDataSet;
	}

	public DataSet GetListCsv(string pSiteCd,string pYear,string pMonth) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		oSqlBuilder.AppendLine("SELECT																							");
		oSqlBuilder.AppendLine("	REPORT_DAY																				,	");
		oSqlBuilder.AppendLine("	SINCE_LAST_LOGIN_DAYS																	,	");
		oSqlBuilder.AppendLine("	START_MAIL_ADDR_STATUS_OK																,	");
		oSqlBuilder.AppendLine("	START_MAIL_ADDR_STATUS_FT																,	");
		oSqlBuilder.AppendLine("	START_MAIL_ADDR_STATUS_NG																,	");
		oSqlBuilder.AppendLine("	START_MAIL_ADDR_STATUS_OK + START_MAIL_ADDR_STATUS_FT + START_MAIL_ADDR_STATUS_NG AS TOTAL_START_COUNT	,	");
		oSqlBuilder.AppendLine("	MAIL_ADDR_STATUS_OK_OK																	,	");
		oSqlBuilder.AppendLine("	MAIL_ADDR_STATUS_OK_FT																	,	");
		oSqlBuilder.AppendLine("	MAIL_ADDR_STATUS_OK_NG																	,	");
		oSqlBuilder.AppendLine("	MAIL_ADDR_STATUS_OK_OK + MAIL_ADDR_STATUS_OK_FT + MAIL_ADDR_STATUS_OK_NG AS TOTAL_START_OK_LOGIN		,	");
		oSqlBuilder.AppendLine("	MAIL_ADDR_STATUS_FT_OK																	,	");
		oSqlBuilder.AppendLine("	MAIL_ADDR_STATUS_FT_FT																	,	");
		oSqlBuilder.AppendLine("	MAIL_ADDR_STATUS_FT_NG																	,	");
		oSqlBuilder.AppendLine("	MAIL_ADDR_STATUS_FT_OK + MAIL_ADDR_STATUS_FT_FT + MAIL_ADDR_STATUS_FT_NG AS TOTAL_START_FT_LOGIN		,	");
		oSqlBuilder.AppendLine("	MAIL_ADDR_STATUS_NG_OK																	,	");
		oSqlBuilder.AppendLine("	MAIL_ADDR_STATUS_NG_FT																	,	");
		oSqlBuilder.AppendLine("	MAIL_ADDR_STATUS_NG_NG																	,	");
		oSqlBuilder.AppendLine("	MAIL_ADDR_STATUS_NG_OK + MAIL_ADDR_STATUS_NG_FT + MAIL_ADDR_STATUS_NG_NG AS TOTAL_START_NG_LOGIN		,	");
		oSqlBuilder.AppendLine("	MAIL_ADDR_STATUS_OK_OK + MAIL_ADDR_STATUS_FT_OK + MAIL_ADDR_STATUS_NG_OK AS TOTAL_END_OK_LOGIN			,	");
		oSqlBuilder.AppendLine("	MAIL_ADDR_STATUS_OK_FT + MAIL_ADDR_STATUS_FT_FT + MAIL_ADDR_STATUS_NG_FT AS TOTAL_END_FT_LOGIN			,	");
		oSqlBuilder.AppendLine("	MAIL_ADDR_STATUS_OK_NG + MAIL_ADDR_STATUS_FT_NG + MAIL_ADDR_STATUS_NG_NG AS TOTAL_END_NG_LOGIN			,	");
		oSqlBuilder.AppendLine("	MAIL_ADDR_STATUS_OK_OK + MAIL_ADDR_STATUS_OK_FT + MAIL_ADDR_STATUS_OK_NG + MAIL_ADDR_STATUS_FT_OK + MAIL_ADDR_STATUS_FT_FT + MAIL_ADDR_STATUS_FT_NG + MAIL_ADDR_STATUS_NG_OK + MAIL_ADDR_STATUS_NG_FT + MAIL_ADDR_STATUS_NG_NG AS TOTAL_LOGIN_COUNT	");
		oSqlBuilder.AppendLine("FROM																							");
		oSqlBuilder.AppendLine("	T_USER_LOGIN_COUNT_DAILY																	");
		oSqlBuilder.AppendLine("WHERE																							");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD														AND	");
		oSqlBuilder.AppendLine("	REPORT_MONTH			= :REPORT_MONTH														");
		oSqlBuilder.AppendLine("ORDER BY SITE_CD,SINCE_LAST_LOGIN_DAYS,REPORT_DAY												");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":REPORT_MONTH",string.Format("{0}/{1}",pYear,pMonth)));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		return oDataSet;
	}
}