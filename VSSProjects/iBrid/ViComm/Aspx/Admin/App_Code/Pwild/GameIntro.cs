﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: ゲーム友達紹介設定--	Progaram ID		: GameIntro
--
--  Creation Date	: 2012.02.06
--  Creater			: PW K.Miyazato
--
**************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class GameIntro:DbSession {
	public GameIntro() {
	}

	public int GetPageCount(string pSiteCd,string pSexCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT COUNT(*) FROM T_GAME_INTRO WHERE SITE_CD = :SITE_CD AND SEX_CD = :SEX_CD");

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":SEX_CD",pSexCd);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pSexCd,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT						");
		oSqlBuilder.AppendLine("	SITE_CD				,	");
		oSqlBuilder.AppendLine("	GAME_INTRO_SEQ		,	");
		oSqlBuilder.AppendLine("	SEX_CD				,	");
		oSqlBuilder.AppendLine("	INTRO_MIN			,	");
		oSqlBuilder.AppendLine("	INTRO_MAX			,	");
		oSqlBuilder.AppendLine("	INTRO_COMMENT			");
		oSqlBuilder.AppendLine("FROM						");
		oSqlBuilder.AppendLine("	T_GAME_INTRO			");
		oSqlBuilder.AppendLine("WHERE						");
		oSqlBuilder.AppendLine("	SITE_CD	= :SITE_CD	AND	");
		oSqlBuilder.AppendLine("	SEX_CD	= :SEX_CD		");

		string sPagingSql = string.Empty;
		string sSortExpression = "ORDER BY SITE_CD, SEX_CD, INTRO_MIN";

		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":SEX_CD",pSexCd);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}
	
	public bool IsDupulicateIntroCount(string pSiteCd,string pGameIntroSeq,string pSexCd,string pIntroMin,string pIntroMax) {
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT				");
		oSqlBuilder.AppendLine("	COUNT(*)		");
		oSqlBuilder.AppendLine("FROM				");
		oSqlBuilder.AppendLine("	T_GAME_INTRO	");
		oSqlBuilder.AppendLine("WHERE				");
		oSqlBuilder.AppendLine("	SITE_CD			=	:SITE_CD		AND	");
		
		if (!string.IsNullOrEmpty(pGameIntroSeq)) {
			oSqlBuilder.AppendLine("	GAME_INTRO_SEQ	<>	:GAME_INTRO_SEQ	AND	");
		}
		
		oSqlBuilder.AppendLine("	SEX_CD			=	:SEX_CD			AND	");
		oSqlBuilder.AppendLine("	(										");
		oSqlBuilder.AppendLine("		(INTRO_MIN BETWEEN :INTRO_MIN AND :INTRO_MAX) OR");
		oSqlBuilder.AppendLine("		(INTRO_MAX BETWEEN :INTRO_MAX AND :INTRO_MAX)	");
		oSqlBuilder.AppendLine("	)										");

		try {
			int iCount = 0;
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(new OracleParameter("SITE_CD",pSiteCd));
				
				if (!string.IsNullOrEmpty(pGameIntroSeq)) {
					cmd.Parameters.Add(new OracleParameter("GAME_INTRO_SEQ",pGameIntroSeq));
				}
				
				cmd.Parameters.Add(new OracleParameter("SEX_CD",pSexCd));
				cmd.Parameters.Add(new OracleParameter("INTRO_MIN",pIntroMin));
				cmd.Parameters.Add(new OracleParameter("INTRO_MAX",pIntroMax));
				iCount = Convert.ToInt32(cmd.ExecuteScalar());
				return iCount > 0;
			}
		} finally {
			conn.Close();
		}
	}
}
