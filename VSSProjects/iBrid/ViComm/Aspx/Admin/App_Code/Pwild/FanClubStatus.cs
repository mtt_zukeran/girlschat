﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: ファンクラブ状態
--	Progaram ID		: FanClubStatus
--  Creation Date	: 2013.02.20
--  Creater			: K.Miyazato
--
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;
using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class FanClubStatus:DbSession {

	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}
		private string castUserSeq;
		public string CastUserSeq {
			get {
				return this.castUserSeq;
			}
			set {
				this.castUserSeq = value;
			}
		}
		private string castCharNo;
		public string CastCharNo {
			get {
				return this.castCharNo;
			}
			set {
				this.castCharNo = value;
			}
		}
		private string enableFlag;
		public string EnableFlag {
			get {
				return this.enableFlag;
			}
			set {
				this.enableFlag = value;
			}
		}
		private string sortDirection;
		public string SortDirection {
			get {
				return this.sortDirection;
			}
			set {
				this.sortDirection = value;
			}
		}
		private string sortExpression;
		public string SortExpression {
			get {
				return this.sortExpression;
			}
			set {
				this.sortExpression = value;
			}
		}
	}

	public FanClubStatus() {
	}

	public int GetPageCount(SearchCondition pSearchCondition) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_MAN_FANCLUB_STATUS00");
		oSqlBuilder.AppendLine(sWhereClause);

		return ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
	}

	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));
		sSortExpression = this.GetOrder(pSearchCondition);

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SITE_CD,");
		oSqlBuilder.AppendLine("	CAST_USER_SEQ,");
		oSqlBuilder.AppendLine("	CAST_CHAR_NO,");
		oSqlBuilder.AppendLine("	MAN_USER_SEQ,");
		oSqlBuilder.AppendLine("	LOGIN_ID,");
		oSqlBuilder.AppendLine("	HANDLE_NM,");
		oSqlBuilder.AppendLine("	ADMISSION_DATE,");
		oSqlBuilder.AppendLine("	EXPIRE_DATE,");
		oSqlBuilder.AppendLine("	MEMBER_RANK,");
		oSqlBuilder.AppendLine("	MEMBER_RANK_EXPIRE_DATE,");
		oSqlBuilder.AppendLine("	COIN_COUNT,");
		oSqlBuilder.AppendLine("	ENABLE_FLAG");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_MAN_FANCLUB_STATUS00");
		oSqlBuilder.AppendLine(sWhereClause);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		return ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.CastUserSeq)) {
			SysPrograms.SqlAppendWhere("CAST_USER_SEQ = :CAST_USER_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_USER_SEQ",pSearchCondition.CastUserSeq));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.CastCharNo)) {
			SysPrograms.SqlAppendWhere("CAST_CHAR_NO = :CAST_CHAR_NO",ref pWhereClause);
			oParamList.Add(new OracleParameter(":CAST_CHAR_NO",pSearchCondition.CastCharNo));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.EnableFlag)) {
			SysPrograms.SqlAppendWhere("ENABLE_FLAG = :ENABLE_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":ENABLE_FLAG",pSearchCondition.EnableFlag));
		}

		return oParamList.ToArray();
	}

	private string GetOrder(SearchCondition pSearchCondition) {
		if (string.IsNullOrEmpty(pSearchCondition.SortExpression)) {
			return "ORDER BY ADMISSION_DATE DESC";
		} else {
			return string.Format("ORDER BY {0} {1},ADMISSION_DATE DESC",pSearchCondition.SortExpression,pSearchCondition.SortDirection);
		}
	}
}
