﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: メールdeガチャ確立設定
--	Progaram ID		: MailLotteryRate
--  Creation Date	: 2014.12.29
--  Creater			: Y.Ikemiya
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;
using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class MailLotteryRate:DbSession {
	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}

		private string mailLotterySeq;
		public string MailLotterySeq {
			get {
				return this.mailLotterySeq;
			}
			set {
				this.mailLotterySeq = value;
			}
		}

		private string secondLotteryFlag;
		public string SecondLotteryFlag {
			get {
				return this.secondLotteryFlag;
			}
			set {
				this.secondLotteryFlag = value;
			}
		}

		private string sortDirection;
		public string SortDirection {
			get {
				return this.sortDirection;
			}
			set {
				this.sortDirection = value;
			}
		}

		private string sortExpression;
		public string SortExpression {
			get {
				return this.sortExpression;
			}
			set {
				this.sortExpression = value;
			}
		}
	}

	public MailLotteryRate() {
	}

	public int GetPageCount(SearchCondition pSearchCondition) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT						");
		oSqlBuilder.AppendLine("	COUNT(*)				");
		oSqlBuilder.AppendLine("FROM						");
		oSqlBuilder.AppendLine("	T_MAIL_LOTTERY_RATE		");
		oSqlBuilder.AppendLine(sWhereClause);

		return ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
	}

	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));
		sSortExpression = this.GetOrder(pSearchCondition);

		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	SITE_CD							,	");
		oSqlBuilder.AppendLine("	MAIL_LOTTERY_SEQ				,	");
		oSqlBuilder.AppendLine("	MAIL_LOTTERY_RATE_SEQ			,	");
		oSqlBuilder.AppendLine("	MAIL_LOTTERY_RATE_NM			,	");
		oSqlBuilder.AppendLine("	REWARD_POINT					,	");
		oSqlBuilder.AppendLine("	RATE								");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	T_MAIL_LOTTERY_RATE					");
		oSqlBuilder.AppendLine(sWhereClause);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		return ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.MailLotterySeq)) {
			SysPrograms.SqlAppendWhere("MAIL_LOTTERY_SEQ = :MAIL_LOTTERY_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":MAIL_LOTTERY_SEQ",pSearchCondition.MailLotterySeq));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.SecondLotteryFlag)) {
			SysPrograms.SqlAppendWhere("SECOND_LOTTERY_FLAG = :SECOND_LOTTERY_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SECOND_LOTTERY_FLAG",pSearchCondition.SecondLotteryFlag));
		}

		return oParamList.ToArray();
	}

	private string GetOrder(SearchCondition pSearchCondition) {
		return "ORDER BY SITE_CD,REWARD_POINT DESC";
	}

	public DataSet GetTotalRate(string pSiteCd,string pMailLotterySeq,string pSecondLotteryFlag) {
		string sExecSql = string.Empty;		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	SUM(RATE) AS TOTAL_RATE							");
		oSqlBuilder.AppendLine("FROM												");
		oSqlBuilder.AppendLine("	T_MAIL_LOTTERY_RATE								");
		oSqlBuilder.AppendLine("WHERE												");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	MAIL_LOTTERY_SEQ	= :MAIL_LOTTERY_SEQ		AND	");
		oSqlBuilder.AppendLine("	SECOND_LOTTERY_FLAG	= :SECOND_LOTTERY_FLAG		");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":MAIL_LOTTERY_SEQ",pMailLotterySeq));
		oParamList.Add(new OracleParameter(":SECOND_LOTTERY_FLAG",pSecondLotteryFlag));

		return ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
	}
}
