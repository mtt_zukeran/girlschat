﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: お宝deビンゴエントリー
--	Progaram ID		: BbsBingoEntry
--  Creation Date	: 2013.10.29
--  Creater			: K.Miyazato
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;
using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class BbsBingoEntry:DbSession {
	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}

		private string termSeq;
		public string TermSeq {
			get {
				return this.termSeq;
			}
			set {
				this.termSeq = value;
			}
		}

		private string prizeFlag;
		public string PrizeFlag {
			get {
				return this.prizeFlag;
			}
			set {
				this.prizeFlag = value;
			}
		}

		private string loginId;
		public string LoginId {
			get {
				return this.loginId;
			}
			set {
				this.loginId = value;
			}
		}

		private string sortDirection;
		public string SortDirection {
			get {
				return this.sortDirection;
			}
			set {
				this.sortDirection = value;
			}
		}

		private string sortExpression;
		public string SortExpression {
			get {
				return this.sortExpression;
			}
			set {
				this.sortExpression = value;
			}
		}
	}

	public BbsBingoEntry() {
	}

	public int GetPageCount(SearchCondition pSearchCondition) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_BBS_BINGO_ENTRY01");
		oSqlBuilder.AppendLine(sWhereClause);

		return ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
	}

	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));
		sSortExpression = this.GetOrder(pSearchCondition);

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SITE_CD,");
		oSqlBuilder.AppendLine("	TERM_SEQ,");
		oSqlBuilder.AppendLine("	ENTRY_SEQ,");
		oSqlBuilder.AppendLine("	USER_SEQ,");
		oSqlBuilder.AppendLine("	CARD_NO,");
		oSqlBuilder.AppendLine("	ENTRY_DATE,");
		oSqlBuilder.AppendLine("	TRY_COUNT,");
		oSqlBuilder.AppendLine("	COMPLETE_DATE,");
		oSqlBuilder.AppendLine("	BINGO_BALL_FLAG,");
		oSqlBuilder.AppendLine("	PRIZE_FLAG,");
		oSqlBuilder.AppendLine("	PRIZE_DATE,");
		oSqlBuilder.AppendLine("	PRIZE_AMT,");
		oSqlBuilder.AppendLine("	PRIZE_POINT,");
		oSqlBuilder.AppendLine("	PRIZE_JACKPOT,");
		oSqlBuilder.AppendLine("	LOGIN_ID,");
		oSqlBuilder.AppendLine("	HANDLE_NM");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_BBS_BINGO_ENTRY01");
		oSqlBuilder.AppendLine(sWhereClause);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		return ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.TermSeq)) {
			SysPrograms.SqlAppendWhere("TERM_SEQ = :TERM_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":TERM_SEQ",pSearchCondition.TermSeq));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.PrizeFlag)) {
			SysPrograms.SqlAppendWhere("PRIZE_FLAG = :PRIZE_FLAG",ref pWhereClause);
			oParamList.Add(new OracleParameter(":PRIZE_FLAG",pSearchCondition.PrizeFlag));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.LoginId)) {
			SysPrograms.SqlAppendWhere("LOGIN_ID = :LOGIN_ID",ref pWhereClause);
			oParamList.Add(new OracleParameter(":LOGIN_ID",pSearchCondition.LoginId));
		}

		return oParamList.ToArray();
	}

	private string GetOrder(SearchCondition pSearchCondition) {
		if (string.IsNullOrEmpty(pSearchCondition.SortExpression)) {
			return "ORDER BY SITE_CD,TERM_SEQ,PRIZE_DATE DESC NULLS LAST,ENTRY_DATE DESC";
		} else {
			return string.Format("ORDER BY {0} {1},SITE_CD,TERM_SEQ,ENTRY_DATE DESC",pSearchCondition.SortExpression,pSearchCondition.SortDirection);
		}
	}
}
