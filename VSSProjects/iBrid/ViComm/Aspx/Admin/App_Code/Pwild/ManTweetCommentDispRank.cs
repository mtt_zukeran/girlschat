﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: つぶやきコメントランキング表示順位--	Progaram ID		: ManTweetCommentDispRank
--
--  Creation Date	: 2017.04.04
--  Creater			: K.Yogi
--
**************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

/// <summary>
/// 「T_TWEET_COMMENT_DISP_RANK」テーブルに対して処理を行います
/// </summary>
public class ManTweetCommentDispRank : DbSession
{
    #region 変数定義
    private const int DISP_RANK_TYPE_MANAGE = 0; // ランキング表示順位(管理画面)
    private const int DISP_RANK_TYPE_NOW = 1; // ランキング表示順位(現在ランキング)
    private const int DISP_RANK_TYPE_PAST = 2; // ランキング表示順位(過去ランキング)

    #endregion

    public ManTweetCommentDispRank()
    {
        //
        // TODO: コンストラクタ ロジックをここに追加します
        //
    }

    /// <summary>
    /// レコード件数を取得
    /// </summary>
    //public int GetPageCount(SearchCondition pSearchCondition)
    public int GetPageCount()
    {
        StringBuilder sbSqlBuilder = new StringBuilder();
        sbSqlBuilder.Append(@"
                            SELECT
                                COUNT(*)
                            FROM
                            (
                                SELECT 
                                    DISP_RANK_SEQ,SEX_CD
                                FROM
                                    T_TWEET_COMMENT_DISP_RANK
                                GROUP BY
                                    DISP_RANK_SEQ,SEX_CD
                            )
                            ");

        int nCount = ExecuteSelectCountQueryBase(sbSqlBuilder, new OracleParameter[0]);
        
        return nCount;
    }

    /// <summary>
    /// 「SexCD」を指定して登録済みの「DispRankSeq」を全取得
    /// </summary>
    /// <returns></returns>
    public string[] GetDispRankSeqAll(string sSexCD)
    {
        StringBuilder sbSqlBuilder = new StringBuilder();
        sbSqlBuilder.Append(@"
                            SELECT 
                                DISP_RANK_SEQ
                            FROM
                                T_TWEET_COMMENT_DISP_RANK
                            WHERE
                                SEX_CD = :pSEX_CD
                            GROUP BY
                                DISP_RANK_SEQ
                            ORDER BY
                                DISP_RANK_SEQ
                            ");

        List<OracleParameter> oParamList = new List<OracleParameter>();
        oParamList.Add(new OracleParameter("pSEX_CD", sSexCD));
        DataSet dsTable = ExecuteSelectQueryBase(sbSqlBuilder, oParamList.ToArray());

        List<string> lstDispRankSeq = new List<string>();
        for (int i = 0; i < dsTable.Tables[0].Rows.Count; i++)
        {
            lstDispRankSeq.Add(dsTable.Tables[0].Rows[i]["DISP_RANK_SEQ"].ToString());
        }

        return lstDispRankSeq.ToArray();
    }

    /// <summary>
    /// 指定した「DispRankSeq」の存在チェック
    /// </summary>
    /// <returns></returns>
    public bool IsExistDispRankSeq(string sDispRankSeq, string sSexCD)
    {
        bool bIsExist = false;

        StringBuilder sbSqlBuilder = new StringBuilder();
        sbSqlBuilder.Append(@"
                            SELECT 
                                1
                            FROM
                                T_TWEET_COMMENT_DISP_RANK
                            WHERE
                                DISP_RANK_SEQ = :pDISP_RANK_SEQ
                                AND SEX_CD = :pSEX_CD
                            GROUP BY
                                DISP_RANK_SEQ
                            ");

        List<OracleParameter> oParamList = new List<OracleParameter>();
        oParamList.Add(new OracleParameter("pDISP_RANK_SEQ", sDispRankSeq));
        oParamList.Add(new OracleParameter("pSEX_CD", sSexCD));

        DataSet dsTable = ExecuteSelectQueryBase(sbSqlBuilder, oParamList.ToArray());

        if (dsTable.Tables[0].Rows.Count > 0)
            bIsExist = true;

        return bIsExist;
    }

    /// <summary>
    /// 画面出力用の全データを取得
    /// </summary>
    /// <returns></returns>
    //public DataSet GetPageCollection(SearchCondition pSearchCondition, int startRowIndex, int maximumRows)
    public DataSet GetPageCollection(int startRowIndex, int maximumRows)
    {
        string sSQL = CreatPageSQL(string.Empty);
        DataSet dsTable = ExecuteSelectQueryBase(sSQL, new OracleParameter[0]);
        DataTable dtPage = ChangePageData(dsTable.Tables[0]);

        DataSet ds = new DataSet();
        ds.Tables.Add(dtPage);

        return ds;
    }

    /// <summary>
    /// 「DispRankSeq」「SexCD」を指定して画面出力用のレコードを取得
    /// </summary>
    /// <returns></returns>
    public DataTable GetRecord(string sDispRankSeq, string sSexCD)
    {
        StringBuilder sbWhere = new StringBuilder();
        sbWhere.Append(@"
                         WHERE
                            DISP_RANK_SEQ = :pDISP_RANK_SEQ
                            AND SEX_CD = :pSEX_CD
                        ");
        string sSQL = CreatPageSQL(sbWhere.ToString());

        List<OracleParameter> oParamList = new List<OracleParameter>();
        oParamList.Add(new OracleParameter("pDISP_RANK_SEQ", sDispRankSeq));
        oParamList.Add(new OracleParameter("pSEX_CD", sSexCD));

        DataSet dsTable = ExecuteSelectQueryBase(sSQL, oParamList.ToArray());
        DataTable dtPage = ChangePageData(dsTable.Tables[0]);

        return dtPage;
    }

    /// <summary>
    /// 画面出力用のSQL文を作成
    /// </summary>
    /// <returns></returns>
    private string CreatPageSQL(string sWhere)
    {
        StringBuilder sbSql = new StringBuilder();
        sbSql.Append(@"
                            SELECT 
                                DISP_RANK_SEQ
                                ,DISP_RANK_TYPE
                                ,SEX_CD
                                ,DISP_RANK_NO
                            FROM
                                T_TWEET_COMMENT_DISP_RANK
                            " + sWhere + @"
                            ORDER BY
                                DISP_RANK_SEQ,SEX_CD,DISP_RANK_TYPE,DISP_RANK_NO
                            ");
        return sbSql.ToString();
    }

    /// <summary>
    /// 画面出力用にDISP_RANK_SEQ別の現在、過去、管理画面の表示順位を保持するように変換
    /// </summary>
    /// <returns></returns>
    private DataTable ChangePageData(DataTable dtTable)
    {
        //DISP_RANK_SEQ別に現在、過去、管理画面用の「DISP_RANK_NO」を取得
        DataView dv = new DataView(dtTable);
        DataTable dtDistinctSeq = dv.ToTable("DistinctTable", true, new string[] { "DISP_RANK_SEQ", "SEX_CD" });

        dtDistinctSeq.Columns.Add("DISP_RANK_NO_NOW", Type.GetType("System.String"));
        dtDistinctSeq.Columns.Add("DISP_RANK_NO_PAST", Type.GetType("System.String"));
        dtDistinctSeq.Columns.Add("DISP_RANK_NO_MANAGE", Type.GetType("System.String"));

        for (int i = 0; i < dtDistinctSeq.Rows.Count; i++)
        {
            //現在
            DataRow[] drNows = dtTable.Select(string.Format("DISP_RANK_SEQ = {0} AND SEX_CD = {1} AND DISP_RANK_TYPE = {2} "
                            , dtDistinctSeq.Rows[i]["DISP_RANK_SEQ"].ToString()
                            , dtDistinctSeq.Rows[i]["SEX_CD"].ToString()
                            , DISP_RANK_TYPE_NOW));
            dtDistinctSeq.Rows[i]["DISP_RANK_NO_NOW"] = CreatDispRankNoStr(drNows);

            //過去
            DataRow[] drPasts = dtTable.Select(string.Format("DISP_RANK_SEQ = {0} AND SEX_CD = {1} AND DISP_RANK_TYPE = {2} "
                            , dtDistinctSeq.Rows[i]["DISP_RANK_SEQ"].ToString()
                            , dtDistinctSeq.Rows[i]["SEX_CD"].ToString()
                            , DISP_RANK_TYPE_PAST));
            dtDistinctSeq.Rows[i]["DISP_RANK_NO_PAST"] = CreatDispRankNoStr(drPasts);

            //管理画面
            DataRow[] drManages = dtTable.Select(string.Format("DISP_RANK_SEQ = {0} AND SEX_CD = {1} AND DISP_RANK_TYPE = {2} "
                            , dtDistinctSeq.Rows[i]["DISP_RANK_SEQ"].ToString()
                            , dtDistinctSeq.Rows[i]["SEX_CD"].ToString()
                            , DISP_RANK_TYPE_MANAGE));
            dtDistinctSeq.Rows[i]["DISP_RANK_NO_MANAGE"] = CreatDispRankNoStr(drManages);
        }

        return dtDistinctSeq;
    }

    /// <summary>
    /// 表示順位を画面出力用にカンマ区切りで作成
    /// </summary>
    /// <param name="drRows"></param>
    /// <returns></returns>
    private string CreatDispRankNoStr(DataRow[] drRows)
    {
        StringBuilder sbDispRankNoStr = new StringBuilder();

        int nPreRankNo = -1;
        int nSeqCount = 0;

        for (int j = 0; j < drRows.Length; j++)
        {
            DataRow row = drRows[j];

            int nRankNo;
            if (!int.TryParse(row["DISP_RANK_NO"].ToString(), out nRankNo))
                break;

            if (nPreRankNo + 1 == nRankNo)
            {
                nSeqCount++;
                nPreRankNo = nRankNo;

                //最後のレコードで無ければスキップ
                if (j < drRows.Length - 1)
                    continue;
            }
            if (sbDispRankNoStr.Length > 0)
            {
                //連番の場合
                if (nSeqCount > 0)
                {
                    //2つ以上続いた場合はハイフンで表示
                    if (nSeqCount >= 2)
                        sbDispRankNoStr.AppendFormat("-{0}", nPreRankNo);
                    else
                        sbDispRankNoStr.AppendFormat(",{0}", nPreRankNo);

                    //最後のレコードの場合はここで終わり
                    if (j == drRows.Length - 1 && nPreRankNo == nRankNo)
                        continue;
                }
                sbDispRankNoStr.Append(",");
            }

            sbDispRankNoStr.AppendFormat("{0}", nRankNo);
            nSeqCount = 0;
            nPreRankNo = nRankNo;
        }

        return sbDispRankNoStr.ToString();
    }
}
