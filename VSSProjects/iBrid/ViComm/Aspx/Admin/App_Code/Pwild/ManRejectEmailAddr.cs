﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 会員登録拒否ﾒｰﾙｱﾄﾞﾚｽ
--	Progaram ID		: ManRejectEmailAddr
--  Creation Date	: 2015.05.21
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

public class ManRejectEmailAddr:DbSession {
	public class SearchCondition {
		private string lastRejectDateFrom;
		private string lastRejectDateTo;
		private string rejectEmailAddr;

		public string LastRejectDateFrom {
			get {
				return this.lastRejectDateFrom;
			}
			set {
				this.lastRejectDateFrom = value;
			}
		}

		public string LastRejectDateTo {
			get {
				return this.lastRejectDateTo;
			}
			set {
				this.lastRejectDateTo = value;
			}
		}

		public string RejectEmailAddr {
			get {
				return this.rejectEmailAddr;
			}
			set {
				this.rejectEmailAddr = value;
			}
		}
	}

	public ManRejectEmailAddr() {
	}

	public int GetPageCount(SearchCondition pSearchCondition) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_MAN_REJECT_EMAIL_ADDR");
		oSqlBuilder.AppendLine(sWhereClause);

		return ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
	}

	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	T_MAN_REJECT_EMAIL_ADDR.EMAIL_ADDR AS REJECT_EMAIL_ADDR,");
		oSqlBuilder.AppendLine("	T_MAN_REJECT_EMAIL_ADDR.REJECT_COUNT,");
		oSqlBuilder.AppendLine("	T_MAN_REJECT_EMAIL_ADDR.LAST_REJECT_DATE,");
		oSqlBuilder.AppendLine("	T_TEMP_REGIST.EMAIL_ADDR AS REGIST_EMAIL_ADDR,");
		oSqlBuilder.AppendLine("	T_TEMP_REGIST.REGIST_COMPLITE_DATE,");
		oSqlBuilder.AppendLine("	T_TEMP_REGIST.REGIST_STATUS,");
		oSqlBuilder.AppendLine("	T_USER.LOGIN_ID");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_MAN_REJECT_EMAIL_ADDR");
		oSqlBuilder.AppendLine("	LEFT OUTER JOIN T_TEMP_REGIST");
		oSqlBuilder.AppendLine("		ON (T_MAN_REJECT_EMAIL_ADDR.LAST_TEMP_REGIST_ID = T_TEMP_REGIST.TEMP_REGIST_ID)");
		oSqlBuilder.AppendLine("	LEFT OUTER JOIN T_USER");
		oSqlBuilder.AppendLine("		ON (T_TEMP_REGIST.USER_SEQ = T_USER.USER_SEQ)");
		oSqlBuilder.AppendLine(sWhereClause);

		string sSortExpression = "ORDER BY T_MAN_REJECT_EMAIL_ADDR.LAST_REJECT_DATE DESC";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		return ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.LastRejectDateFrom)) {
			DateTime dtFrom;

			if (DateTime.TryParse(pSearchCondition.LastRejectDateFrom + " 00:00:00",out dtFrom)) {
				SysPrograms.SqlAppendWhere("T_MAN_REJECT_EMAIL_ADDR.LAST_REJECT_DATE >= :LAST_REJECT_DATE_FROM",ref pWhereClause);
				oParamList.Add(new OracleParameter(":LAST_REJECT_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			}
		}

		if (!string.IsNullOrEmpty(pSearchCondition.LastRejectDateTo)) {
			DateTime dtTo;

			if (DateTime.TryParse(pSearchCondition.LastRejectDateTo + " 23:59:59",out dtTo)) {
				SysPrograms.SqlAppendWhere("T_MAN_REJECT_EMAIL_ADDR.LAST_REJECT_DATE <= :LAST_REJECT_DATE_TO",ref pWhereClause);
				oParamList.Add(new OracleParameter(":LAST_REJECT_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
			}
		}

		if (!string.IsNullOrEmpty(pSearchCondition.RejectEmailAddr)) {
			SysPrograms.SqlAppendWhere("T_MAN_REJECT_EMAIL_ADDR.EMAIL_ADDR LIKE '%' || :REJECT_EMAIL_ADDR || '%'",ref pWhereClause);
			oParamList.Add(new OracleParameter(":REJECT_EMAIL_ADDR",pSearchCondition.RejectEmailAddr));
		}

		return oParamList.ToArray();
	}
}
