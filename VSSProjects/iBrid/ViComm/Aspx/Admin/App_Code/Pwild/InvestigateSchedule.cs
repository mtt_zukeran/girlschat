﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: この娘を探せ・スケジュール


--	Progaram ID		: InvestigateSchedule
--  Creation Date	: 2015.02.26
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;

public class InvestigateSchedule:DbSession {
	public InvestigateSchedule() {
	}

	public DataSet GetList(string pSiteCd,string pTargetMonth) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT																		");
		oSqlBuilder.AppendLine("	SITE_CD																,	");
		oSqlBuilder.AppendLine("	EXECUTION_DAY														,	");
		oSqlBuilder.AppendLine("	TO_CHAR(TO_DATE(EXECUTION_DAY, 'YYYY/MM/DD'), 'DD') DAY				,	");
		oSqlBuilder.AppendLine("	TO_CHAR(TO_DATE(EXECUTION_DAY, 'YYYY/MM/DD'), 'DY') DAY_OF_WEEK		,	");
		oSqlBuilder.AppendLine("	FIRST_HINT_ANNOUNCE_TIME											,	");
		oSqlBuilder.AppendLine("	SECOND_HINT_ANNOUNCE_TIME											,	");
		oSqlBuilder.AppendLine("	THIRD_HINT_ANNOUNCE_TIME											,	");
		oSqlBuilder.AppendLine("	BOUNTY_POINT														,	");
		oSqlBuilder.AppendLine("	POINT_PER_ENTRANT													,	");
		oSqlBuilder.AppendLine("	REVISION_NO																");
		oSqlBuilder.AppendLine("FROM																		");
		oSqlBuilder.AppendLine("	T_INVESTIGATE_SCHEDULE													");
		oSqlBuilder.AppendLine("WHERE																		");
		oSqlBuilder.AppendLine("	SITE_CD							= :SITE_CD							AND	");
		oSqlBuilder.AppendLine("	TO_DATE(:TARGET_MONTH || '01')	<= TO_DATE(EXECUTION_DAY)			AND	");
		oSqlBuilder.AppendLine("	TO_DATE(EXECUTION_DAY) <= LAST_DAY(TO_DATE(:TARGET_MONTH || '01'))		");
		oSqlBuilder.AppendLine("ORDER BY																	");
		oSqlBuilder.AppendLine("	EXECUTION_DAY															");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":TARGET_MONTH",pTargetMonth));

		return ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
	}
}
