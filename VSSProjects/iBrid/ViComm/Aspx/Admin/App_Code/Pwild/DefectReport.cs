﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 不具合報告
--	Progaram ID		: DefectReport
--  Creation Date	: 2015.06.24
--  Creater			: M&TT Y.Ikemiya
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;
using ViComm;

public class DefectReport:DbSession {
	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}
		private string defectReportSeq;
		public string DefectReportSeq {
			get {
				return this.defectReportSeq;
			}
			set {
				this.defectReportSeq = value;
			}
		}
		private string sexCd;
		public string SexCd {
			get {
				return this.sexCd;
			}
			set {
				this.sexCd = value;
			}
		}
		private string defectReportCategorySeq;
		public string DefectReportCategorySeq {
			get {
				return this.defectReportCategorySeq;
			}
			set {
				this.defectReportCategorySeq = value;
			}
		}
		private string defectReportProgressSeq;
		public string DefectReportProgressSeq {
			get {
				return this.defectReportProgressSeq;
			}
			set {
				this.defectReportProgressSeq = value;
			}
		}
		private List<string> publicStatus = new List<string>();
		public List<string> PublicStatus {
			get {
				return this.publicStatus;
			}
			set {
				this.publicStatus = value;
			}
		}
		private string loginId;
		public string LoginId {
			get {
				return this.loginId;
			}
			set {
				this.loginId = value;
			}
		}
		private string keyword;
		public string Keyword {
			get {
				return this.keyword;
			}
			set {
				this.keyword = value;
			}
		}
		private string sortDirection;
		public string SortDirection {
			get {
				return this.sortDirection;
			}
			set {
				this.sortDirection = value;
			}
		}
		private string sortExpression;
		public string SortExpression {
			get {
				return this.sortExpression;
			}
			set {
				this.sortExpression = value;
			}
		}
	}

	public int GetPageCount(SearchCondition pSearchCondition) {
		int iRecCount;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT						");
		oSqlBuilder.AppendLine("	COUNT(*)				");
		oSqlBuilder.AppendLine("FROM						");
		oSqlBuilder.AppendLine("	T_DEFECT_REPORT P	,	");
		oSqlBuilder.AppendLine("	T_USER U				");
		oSqlBuilder.AppendLine(sWhereClause);

		iRecCount = ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());

		return iRecCount;
	}

	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sExecSql;
		string sJoinTableNm;
		string sWhereClause = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));
		sSortExpression = this.GetOrder(pSearchCondition);

		if (pSearchCondition.SexCd.Equals(ViCommConst.MAN)) {
			sJoinTableNm = "T_USER_MAN_CHARACTER";
		} else {
			sJoinTableNm = "T_CAST_CHARACTER";
		}

		oSqlBuilder.AppendLine("SELECT																		");
		oSqlBuilder.AppendLine("	P.SITE_CD															,	");
		oSqlBuilder.AppendLine("	P.DEFECT_REPORT_SEQ													,	");
		oSqlBuilder.AppendLine("	P.SEX_CD															,	");
		oSqlBuilder.AppendLine("	P.TITLE																,	");
		oSqlBuilder.AppendLine("	P.TEXT																,	");
		oSqlBuilder.AppendLine("	P.ADMIN_COMMENT														,	");
		oSqlBuilder.AppendLine("	P.OVERLAP_COUNT														,	");
		oSqlBuilder.AppendLine("	P.CREATE_DATE														,	");
		oSqlBuilder.AppendLine("	P.PUBLIC_STATUS														,	");
		oSqlBuilder.AppendLine("	P.PUBLIC_DATE														,	");
		oSqlBuilder.AppendLine("	P.LOGIN_ID															,	");
		oSqlBuilder.AppendLine("	DRC.DEFECT_REPORT_CATEGORY_NM										,	");
		oSqlBuilder.AppendLine("	DRP.DEFECT_REPORT_PROGRESS_NM										,	");
		oSqlBuilder.AppendLine("	DRP.DEFECT_REPORT_PROGRESS_COLOR									,	");
		oSqlBuilder.AppendLine("	C.HANDLE_NM																");
		oSqlBuilder.AppendLine("FROM																		");
		oSqlBuilder.AppendLine("	(																		");
		oSqlBuilder.AppendLine("		SELECT																");
		oSqlBuilder.AppendLine("			P.SITE_CD													,	");
		oSqlBuilder.AppendLine("			P.USER_SEQ													,	");
		oSqlBuilder.AppendLine("			P.USER_CHAR_NO												,	");
		oSqlBuilder.AppendLine("			P.DEFECT_REPORT_SEQ											,	");
		oSqlBuilder.AppendLine("			P.SEX_CD													,	");
		oSqlBuilder.AppendLine("			P.TITLE														,	");
		oSqlBuilder.AppendLine("			P.TEXT														,	");
		oSqlBuilder.AppendLine("			P.DEFECT_REPORT_CATEGORY_SEQ								,	");
		oSqlBuilder.AppendLine("			P.ADMIN_COMMENT												,	");
		oSqlBuilder.AppendLine("			P.DEFECT_REPORT_PROGRESS_SEQ								,	");
		oSqlBuilder.AppendLine("			P.OVERLAP_COUNT												,	");
		oSqlBuilder.AppendLine("			P.CREATE_DATE												,	");
		oSqlBuilder.AppendLine("			P.PUBLIC_STATUS												,	");
		oSqlBuilder.AppendLine("			P.PUBLIC_DATE												,	");
		oSqlBuilder.AppendLine("			U.LOGIN_ID														");
		oSqlBuilder.AppendLine("		FROM																");
		oSqlBuilder.AppendLine("			T_DEFECT_REPORT P											,	");
		oSqlBuilder.AppendLine("			T_USER U														");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("	) P																	,	");
		oSqlBuilder.AppendLine("	T_DEFECT_REPORT_PROGRESS DRP										,	");
		oSqlBuilder.AppendLine("	T_DEFECT_REPORT_CATEGORY DRC										,	");
		oSqlBuilder.AppendFormat("	{0} C																	",sJoinTableNm).AppendLine();
		oSqlBuilder.AppendLine("WHERE																		");
		oSqlBuilder.AppendLine("	P.SITE_CD						= DRP.SITE_CD						AND	");
		oSqlBuilder.AppendLine("	P.DEFECT_REPORT_PROGRESS_SEQ	= DRP.DEFECT_REPORT_PROGRESS_SEQ	AND	");
		oSqlBuilder.AppendLine("	P.SITE_CD						= DRC.SITE_CD						AND	");
		oSqlBuilder.AppendLine("	P.DEFECT_REPORT_CATEGORY_SEQ	= DRC.DEFECT_REPORT_CATEGORY_SEQ	AND	");
		oSqlBuilder.AppendLine("	P.SEX_CD						= DRC.SEX_CD						AND	");
		oSqlBuilder.AppendLine("	P.SITE_CD						= C.SITE_CD							AND	");
		oSqlBuilder.AppendLine("	P.USER_SEQ						= C.USER_SEQ						AND	");
		oSqlBuilder.AppendLine("	P.USER_CHAR_NO					= C.USER_CHAR_NO						");

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("P.USER_SEQ = U.USER_SEQ",ref pWhereClause);
		
		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.DefectReportSeq)) {
			SysPrograms.SqlAppendWhere("P.DEFECT_REPORT_SEQ = :DEFECT_REPORT_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":DEFECT_REPORT_SEQ",pSearchCondition.DefectReportSeq));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.SexCd)) {
			SysPrograms.SqlAppendWhere("P.SEX_CD = :SEX_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SEX_CD",pSearchCondition.SexCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.DefectReportCategorySeq)) {
			SysPrograms.SqlAppendWhere("P.DEFECT_REPORT_CATEGORY_SEQ = :DEFECT_REPORT_CATEGORY_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":DEFECT_REPORT_CATEGORY_SEQ",pSearchCondition.DefectReportCategorySeq));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.DefectReportProgressSeq)) {
			SysPrograms.SqlAppendWhere("P.DEFECT_REPORT_PROGRESS_SEQ = :DEFECT_REPORT_PROGRESS_SEQ",ref pWhereClause);
			oParamList.Add(new OracleParameter(":DEFECT_REPORT_PROGRESS_SEQ",pSearchCondition.DefectReportProgressSeq));
		}

		if (pSearchCondition.PublicStatus.Count > 0) {
			SysPrograms.SqlAppendWhere(string.Format("PUBLIC_STATUS IN ({0})",string.Join(",",pSearchCondition.PublicStatus.ToArray())),ref pWhereClause);
		}

		if (!string.IsNullOrEmpty(pSearchCondition.LoginId)) {
			SysPrograms.SqlAppendWhere("U.LOGIN_ID = :LOGIN_ID",ref pWhereClause);
			oParamList.Add(new OracleParameter(":LOGIN_ID",pSearchCondition.LoginId));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.Keyword)) {
			SysPrograms.SqlAppendWhere("(P.TITLE LIKE '%'||:KEYWORD||'%' OR P.TEXT LIKE '%'||:KEYWORD||'%' OR P.ADMIN_COMMENT LIKE '%'||:KEYWORD||'%')",ref pWhereClause);
			oParamList.Add(new OracleParameter(":KEYWORD",pSearchCondition.Keyword));
		}

		return oParamList.ToArray();
	}

	private string GetOrder(SearchCondition pSearchCondition) {
		if (string.IsNullOrEmpty(pSearchCondition.SortExpression)) {
			return "ORDER BY CREATE_DATE DESC";
		} else {
			return string.Format("ORDER BY {0} {1},CREATE_DATE DESC",pSearchCondition.SortExpression,pSearchCondition.SortDirection);
		}
	}

	public DataSet GetDefectReportCategoryList(string pSiteCd,string pSexCd) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		SysPrograms.SqlAppendWhere("SEX_CD = :SEX_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SEX_CD",pSexCd));

		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	DEFECT_REPORT_CATEGORY_SEQ	,	");
		oSqlBuilder.AppendLine("	DEFECT_REPORT_CATEGORY_NM		");
		oSqlBuilder.AppendLine("FROM								");
		oSqlBuilder.AppendLine("	T_DEFECT_REPORT_CATEGORY		");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("ORDER BY DEFECT_REPORT_CATEGORY_SEQ		");

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetDefectReportProgressList(string pSiteCd) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	DEFECT_REPORT_PROGRESS_SEQ		,	");
		oSqlBuilder.AppendLine("	DEFECT_REPORT_PROGRESS_NM		,	");
		oSqlBuilder.AppendLine("	DEFECT_REPORT_PROGRESS_COLOR		");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	T_DEFECT_REPORT_PROGRESS			");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("ORDER BY DEFECT_REPORT_PROGRESS_SEQ			");

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		return oDataSet;
	}
	
	public DataSet GetOne(string pSiteCd,string pDefectReportSeq) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref sWhereClause);
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));

		SysPrograms.SqlAppendWhere("DEFECT_REPORT_SEQ = :DEFECT_REPORT_SEQ",ref sWhereClause);
		oParamList.Add(new OracleParameter(":DEFECT_REPORT_SEQ",pDefectReportSeq));

		oSqlBuilder.AppendLine("SELECT						");
		oSqlBuilder.AppendLine("	TITLE				,	");
		oSqlBuilder.AppendLine("	TEXT					");
		oSqlBuilder.AppendLine("FROM						");
		oSqlBuilder.AppendLine("	T_DEFECT_REPORT			");
		oSqlBuilder.AppendLine(sWhereClause);

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		return oDataSet;
	}
}
