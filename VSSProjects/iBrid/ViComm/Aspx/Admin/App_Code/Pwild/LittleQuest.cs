﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 小クエスト設定--	Progaram ID		: LittleQuest
--
--  Creation Date	: 2012.07.06
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class LittleQuest:DbSession {
	public LittleQuest() {
	}

	public int GetPageCount(string pSiteCd,string pQuestSeq,string pLevelQuestSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT COUNT(*) FROM VW_PW_LITTLE_QUEST01 WHERE SITE_CD = :SITE_CD AND QUEST_SEQ = :QUEST_SEQ AND LEVEL_QUEST_SEQ = :LEVEL_QUEST_SEQ");

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":QUEST_SEQ",pQuestSeq);
				cmd.Parameters.Add(":LEVEL_QUEST_SEQ",pLevelQuestSeq);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pQuestSeq,string pLevelQuestSeq,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT								");
		oSqlBuilder.AppendLine("	SITE_CD						,	");
		oSqlBuilder.AppendLine("	QUEST_SEQ					,	");
		oSqlBuilder.AppendLine("	LEVEL_QUEST_SEQ				,	");
		oSqlBuilder.AppendLine("	LITTLE_QUEST_SEQ			,	");
		oSqlBuilder.AppendLine("	LEVEL_QUEST_SUB_SEQ			,	");
		oSqlBuilder.AppendLine("	LITTLE_QUEST_NM				,	");
		oSqlBuilder.AppendLine("	REMARKS						,	");
		oSqlBuilder.AppendLine("	NVL(TIME_LIMIT_D,0) AS TIME_LIMIT_D		,	");
		oSqlBuilder.AppendLine("	NVL(TIME_LIMIT_H,0) AS TIME_LIMIT_H		,	");
		oSqlBuilder.AppendLine("	NVL(TIME_LIMIT_M,0) AS TIME_LIMIT_M		,	");
		oSqlBuilder.AppendLine("	SEX_CD							");
		oSqlBuilder.AppendLine("FROM								");
		oSqlBuilder.AppendLine("	VW_PW_LITTLE_QUEST01			");
		oSqlBuilder.AppendLine("WHERE								");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	QUEST_SEQ			= :QUEST_SEQ			AND	");
		oSqlBuilder.AppendLine("	LEVEL_QUEST_SEQ		= :LEVEL_QUEST_SEQ			");

		string sPagingSql = string.Empty;
		string sSortExpression = "ORDER BY SITE_CD, LEVEL_QUEST_SUB_SEQ";

		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":QUEST_SEQ",pQuestSeq);
				cmd.Parameters.Add(":LEVEL_QUEST_SEQ",pLevelQuestSeq);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}

	public bool IsDupulicateLevelQuestSubSeq(string pSiteCd,string pQuestSeq,string pLevelQuestSeq,string pLittleQuestSeq,string pLevelQuestSubSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT				");
		oSqlBuilder.AppendLine("	COUNT(*)		");
		oSqlBuilder.AppendLine("FROM				");
		oSqlBuilder.AppendLine("	T_LITTLE_QUEST	");
		oSqlBuilder.AppendLine("WHERE				");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	QUEST_SEQ			= :QUEST_SEQ		AND	");
		oSqlBuilder.AppendLine("	LEVEL_QUEST_SEQ		= :LEVEL_QUEST_SEQ	AND	");

		if (!string.IsNullOrEmpty(pLittleQuestSeq)) {
			oSqlBuilder.AppendLine("	LITTLE_QUEST_SEQ	<>	:LITTLE_QUEST_SEQ	AND	");
		}

		oSqlBuilder.AppendLine("	LEVEL_QUEST_SUB_SEQ		=	:LEVEL_QUEST_SUB_SEQ");

		try {
			int iCount = 0;
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(new OracleParameter("SITE_CD",pSiteCd));
				cmd.Parameters.Add(new OracleParameter("QUEST_SEQ",pQuestSeq));
				cmd.Parameters.Add(new OracleParameter("LEVEL_SEQ",pLevelQuestSeq));

				if (!string.IsNullOrEmpty(pLittleQuestSeq)) {
					cmd.Parameters.Add(new OracleParameter("LITTLE_QUEST_SEQ",pLittleQuestSeq));
				}

				cmd.Parameters.Add(new OracleParameter("LEVEL_QUEST_SUB_SEQ",pLevelQuestSubSeq));
				iCount = Convert.ToInt32(cmd.ExecuteScalar());
				return iCount > 0;
			}
		} finally {
			conn.Close();
		}
	}
}
