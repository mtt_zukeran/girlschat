﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: メールおねだり
--	Progaram ID		: MailRequestLog
--  Creation Date	: 2015.01.12
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class MailRequestLog:DbSession {

	#region SearchCondition
	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}
		private string createDateFrom;
		public string CreateDateFrom {
			get {
				return this.createDateFrom;
			}
			set {
				this.createDateFrom = value;
			}
		}
		private string createDateTo;
		public string CreateDateTo {
			get {
				return this.createDateTo;
			}
			set {
				this.createDateTo = value;
			}
		}
		private string manLoginId;
		public string ManLoginId {
			get {
				return this.manLoginId;
			}
			set {
				this.manLoginId = value;
			}
		}
		private string castLoginId;
		public string CastLoginId {
			get {
				return this.castLoginId;
			}
			set {
				this.castLoginId = value;
			}
		}
		private string notDisplayFlag;
		public string NotDisplayFlag {
			get {
				return this.notDisplayFlag;
			}
			set {
				this.notDisplayFlag = value;
			}
		}
		private string rxMailFlag;
		public string RxMailFlag {
			get {
				return this.rxMailFlag;
			}
			set {
				this.rxMailFlag = value;
			}
		}
	}
	#endregion SearchCondition

	public MailRequestLog() {
	}

	public int GetPageCount(SearchCondition pSearchCondition) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));
		
		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	COUNT(*)							");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	VW_MAIL_REQUEST_LOG01				");
		oSqlBuilder.AppendLine(sWhereClause);

		return ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
	}

	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));
		string sSortExpression = this.GetOrder(pSearchCondition);
		
		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	SITE_CD							,	");
		oSqlBuilder.AppendLine("	MAN_LOGIN_ID					,	");
		oSqlBuilder.AppendLine("	MAN_HANDLE_NM					,	");
		oSqlBuilder.AppendLine("	CAST_LOGIN_ID					,	");
		oSqlBuilder.AppendLine("	CAST_HANDLE_NM					,	");
		oSqlBuilder.AppendLine("	NOT_DISPLAY_FLAG				,	");
		oSqlBuilder.AppendLine("	RX_MAIL_FLAG					,	");
		oSqlBuilder.AppendLine("	CREATE_DATE							");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	VW_MAIL_REQUEST_LOG01				");
		oSqlBuilder.AppendLine(sWhereClause);

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		DataSet oDataSet = ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());

		return oDataSet;
	}

	private string GetOrder(SearchCondition pSearchCondition) {
		return "ORDER BY CREATE_DATE DESC, MAN_LOGIN_ID";
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhere) {
		List<OracleParameter> oOracleParameterList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			oOracleParameterList.Add(new OracleParameter(":SITE_CD",pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.CreateDateFrom)) {
			DateTime dtFrom = DateTime.Parse(pSearchCondition.CreateDateFrom);
			SysPrograms.SqlAppendWhere("CREATE_DATE >= :CREATE_DATE_FROM",ref pWhere);
			oOracleParameterList.Add(new OracleParameter(":CREATE_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.CreateDateTo)) {
			DateTime dtTo = DateTime.Parse(pSearchCondition.CreateDateTo);
			SysPrograms.SqlAppendWhere("CREATE_DATE <= :CREATE_DATE_TO",ref pWhere);
			oOracleParameterList.Add(new OracleParameter(":CREATE_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.ManLoginId)) {
			SysPrograms.SqlAppendWhere("MAN_LOGIN_ID = :MAN_LOGIN_ID",ref pWhere);
			oOracleParameterList.Add(new OracleParameter(":MAN_LOGIN_ID",pSearchCondition.ManLoginId));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.CastLoginId)) {
			SysPrograms.SqlAppendWhere("CAST_LOGIN_ID = :CAST_LOGIN_ID",ref pWhere);
			oOracleParameterList.Add(new OracleParameter(":CAST_LOGIN_ID",pSearchCondition.CastLoginId));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.NotDisplayFlag)) {
			SysPrograms.SqlAppendWhere("NOT_DISPLAY_FLAG = :NOT_DISPLAY_FLAG",ref pWhere);
			oOracleParameterList.Add(new OracleParameter(":NOT_DISPLAY_FLAG",pSearchCondition.NotDisplayFlag));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.RxMailFlag)) {
			SysPrograms.SqlAppendWhere("RX_MAIL_FLAG = :RX_MAIL_FLAG",ref pWhere);
			oOracleParameterList.Add(new OracleParameter(":RX_MAIL_FLAG",pSearchCondition.RxMailFlag));
		}

		return oOracleParameterList.ToArray();
	}

	public DataSet GetDailyRegistCount(string pSiteCd,string pYear,string pMonth) {
		
		
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT																																					");
		oSqlBuilder.AppendLine("	C.DAYS																																			,	");
		oSqlBuilder.AppendLine("	(SELECT COUNT(*) FROM T_MAIL_REQUEST_LOG WHERE SITE_CD = :SITE_CD AND REPORT_DAY = C.DAYS) AS TOTAL_COUNT										,	");
		oSqlBuilder.AppendLine("	(SELECT COUNT(*) FROM T_MAIL_REQUEST_LOG WHERE SITE_CD = :SITE_CD AND REPORT_DAY = C.DAYS AND (NOT_DISPLAY_FLAG = 0 OR RX_MAIL_DATE IS NOT NULL)) AS VALID_COUNT	,	");
		oSqlBuilder.AppendLine("	(SELECT COUNT(*) FROM T_MAIL_REQUEST_LOG WHERE SITE_CD = :SITE_CD AND REPORT_DAY = C.DAYS AND NOT_DISPLAY_FLAG = 0) AS NO_MAIL_COUNT			,	");
		oSqlBuilder.AppendLine("	(SELECT COUNT(*) FROM T_MAIL_REQUEST_LOG WHERE SITE_CD = :SITE_CD AND REPORT_DAY = C.DAYS AND RX_MAIL_DATE IS NOT NULL) AS RX_MAIL_COUNT		,	");
		oSqlBuilder.AppendLine("	(SELECT COUNT(*) FROM T_MAIL_REQUEST_LOG WHERE SITE_CD = :SITE_CD AND TO_CHAR(RX_MAIL_DATE,'YYYY/MM/DD') = C.DAYS) AS TODAY_RX_MAIL_COUNT		,	");
		oSqlBuilder.AppendLine("	(SELECT COUNT(DISTINCT USER_SEQ) FROM T_MAIL_REQUEST_LOG WHERE SITE_CD = :SITE_CD AND REPORT_DAY = C.DAYS) AS UNIQUE_USER_COUNT					,	");
		oSqlBuilder.AppendLine("	(SELECT COUNT(*) FROM T_MAIL_REQUEST_LOG WHERE SITE_CD = :SITE_CD AND REPORT_DAY = C.DAYS AND MAN_TX_MAIL_SEQ IS NOT NULL) AS MAN_TX_MAIL_COUNT		");
		oSqlBuilder.AppendLine("FROM																																					");
		oSqlBuilder.AppendLine("	(																																					");
		oSqlBuilder.AppendLine("		SELECT																																			");
		oSqlBuilder.AppendLine("			TO_CHAR(STARTDATE + ROWNUM -1, 'YYYY/MM/DD') AS DAYS																						");
		oSqlBuilder.AppendLine("		FROM																																			");
		oSqlBuilder.AppendLine("			(																																			");
		oSqlBuilder.AppendLine("				SELECT																																	");
		oSqlBuilder.AppendLine("					TRUNC(TO_DATE(:REPORT_DAY_START)) AS STARTDATE																					,	");
		oSqlBuilder.AppendLine("					TO_NUMBER(TO_CHAR(LAST_DAY(TO_DATE(:REPORT_DAY_START)), 'DD')) AS DAYS																");
		oSqlBuilder.AppendLine("				FROM																																	");
		oSqlBuilder.AppendLine("					DUAL																																");
		oSqlBuilder.AppendLine("			)																																		,	");
		oSqlBuilder.AppendLine("			ALL_CATALOG																																	");
		oSqlBuilder.AppendLine("		WHERE																																			");
		oSqlBuilder.AppendLine("			ROWNUM <= DAYS																																");
		oSqlBuilder.AppendLine("	) C																																					");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		DateTime dtReportDayStart = DateTime.Parse(string.Format("{0}/{1}/01",pYear,pMonth));
		oParamList.Add(new OracleParameter(":REPORT_DAY_START",string.Format("{0}/{1}/01",pYear,pMonth)));
		
		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetCsvData(SearchCondition pSearchCondition) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));
		string sSortExpression = this.GetOrder(pSearchCondition);

		oSqlBuilder.AppendLine("SELECT																");
		oSqlBuilder.AppendLine("	SITE_CD														,	");
		oSqlBuilder.AppendLine("	MAN_LOGIN_ID												,	");
		oSqlBuilder.AppendLine("	MAN_HANDLE_NM												,	");
		oSqlBuilder.AppendLine("	CAST_LOGIN_ID												,	");
		oSqlBuilder.AppendLine("	CAST_HANDLE_NM												,	");
		oSqlBuilder.AppendLine("	CASE															");
		oSqlBuilder.AppendLine("		WHEN NOT_DISPLAY_FLAG = 1									");
		oSqlBuilder.AppendLine("		THEN '非表示'												");
		oSqlBuilder.AppendLine("		ELSE NULL													");
		oSqlBuilder.AppendLine("	END AS NOT_DISPLAY_MESSAGE									,	");
		oSqlBuilder.AppendLine("	CASE															");
		oSqlBuilder.AppendLine("		WHEN RX_MAIL_FLAG = 1										");
		oSqlBuilder.AppendLine("		THEN '済'													");
		oSqlBuilder.AppendLine("		ELSE NULL													");
		oSqlBuilder.AppendLine("	END AS RX_MAIL_MESSAGE										,	");
		oSqlBuilder.AppendLine("	CREATE_DATE														");
		oSqlBuilder.AppendLine("FROM																");
		oSqlBuilder.AppendLine("	VW_MAIL_REQUEST_LOG01											");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine(GetOrder(pSearchCondition));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
}
