﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: Twilio
--	Progaram ID		: Twilio
--  Creation Date	: 2014.12.25
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Data;
using System.Net;
using System.IO;
using System.Text;
using System.Xml;
using System.Collections.Generic;
using Oracle.DataAccess.Client;
using iBridCommLib;
using ViComm;

public class Twilio:DbSession {
	public Twilio() {
	}

	public DataSet GetSessionList(string pAccountSid,string pAuthToken) {
		DataSet ds = new DataSet();
		DataTable dt = new DataTable();
		dt.Columns.Add(new DataColumn("CALL_SID",System.Type.GetType("System.String")));
		dt.Columns.Add(new DataColumn("PARENT_CALL_SID",System.Type.GetType("System.String")));
		dt.Columns.Add(new DataColumn("START_TIME",System.Type.GetType("System.String")));
		dt.Columns.Add(new DataColumn("END_TIME",System.Type.GetType("System.String")));
		dt.Columns.Add(new DataColumn("DURATION",System.Type.GetType("System.String")));
		dt.Columns.Add(new DataColumn("FROM_TEL_NO",System.Type.GetType("System.String")));
		dt.Columns.Add(new DataColumn("TO_TEL_NO",System.Type.GetType("System.String")));
		dt.Columns.Add(new DataColumn("STATUS",System.Type.GetType("System.String")));
		dt.Columns.Add(new DataColumn("REQUESTER_SEX_CD",System.Type.GetType("System.String")));
		dt.Columns.Add(new DataColumn("USE_FREE_DIAL_FLAG",System.Type.GetType("System.String")));
		dt.Columns.Add(new DataColumn("USE_WHITE_PLAN_FLAG",System.Type.GetType("System.String")));
		dt.Columns.Add(new DataColumn("AVAILABLE_SEC",System.Type.GetType("System.String")));
		dt.Columns.Add(new DataColumn("USE_MAN_VOICEAPP_FLAG",System.Type.GetType("System.String")));
		dt.Columns.Add(new DataColumn("USE_CAST_VOICEAPP_FLAG",System.Type.GetType("System.String")));
		ds.Tables.Add(dt);

		if (string.IsNullOrEmpty(pAccountSid) || string.IsNullOrEmpty(pAuthToken)) {
			ViCommInterface.WriteIFLog("Twilio.GetSessionList",string.Format("Parameter Error pAccountSid:{0},pAuthToken:{1}",pAccountSid,pAuthToken));
			return ds;
		}

		string sXmlText = GetTwilioCallList(pAccountSid,pAuthToken,50);

		if (sXmlText == "-1") {
			return ds;
		}

		XmlDocument doc = new XmlDocument();
		doc.LoadXml(sXmlText);
		XmlNodeList calls = doc.SelectNodes("TwilioResponse/Calls/Call");
		List<string> sParentCallSids = new List<string>();

		foreach (XmlNode call in calls) {
			string sDirection = call.SelectSingleNode("Direction").InnerText;
			string sStatus = call.SelectSingleNode("Status").InnerText;

			if (sDirection.Equals("outbound-dial")) {// && (sStatus.Equals("queued") || sStatus.Equals("ringing") || sStatus.Equals("in-progress"))) {
				DataRow dr = dt.NewRow();
				dr["CALL_SID"] = call.SelectSingleNode("Sid").InnerText;
				dr["PARENT_CALL_SID"] = call.SelectSingleNode("ParentCallSid").InnerText;

				if (!string.IsNullOrEmpty(call.SelectSingleNode("StartTime").InnerText)) {
					dr["START_TIME"] = DateTime.Parse(call.SelectSingleNode("StartTime").InnerText).ToString("MM/dd HH:mm:ss");
				}

				if (!string.IsNullOrEmpty(call.SelectSingleNode("EndTime").InnerText)) {
					dr["END_TIME"] = DateTime.Parse(call.SelectSingleNode("EndTime").InnerText).ToString("MM/dd HH:mm:ss");
				}

				dr["DURATION"] = call.SelectSingleNode("Duration").InnerText;
				dr["TO_TEL_NO"] = call.SelectSingleNode("To").InnerText.Replace("+81","0");
				dr["STATUS"] = call.SelectSingleNode("Status").InnerText;
				ds.Tables[0].Rows.Add(dr);
				sParentCallSids.Add(string.Format("'{0}'",call.SelectSingleNode("ParentCallSid").InnerText));
			}
		}

		foreach (XmlNode call in calls) {
			string sCallSid = call.SelectSingleNode("Sid").InnerText;
			string sDirection = call.SelectSingleNode("Direction").InnerText;

			foreach (DataRow dr in ds.Tables[0].Rows) {
				if (sCallSid.Equals(iBridUtil.GetStringValue(dr["PARENT_CALL_SID"]))) {
					dr["FROM_TEL_NO"] = call.SelectSingleNode("From").InnerText.Replace("+81","0");
				}
			}
		}

		if (sParentCallSids.Count > 0) {
			DataSet dsIvpRequest = GetIvpRequestByCallSids(sParentCallSids);

			foreach (DataRow drIvpRequest in dsIvpRequest.Tables[0].Rows) {
				string sTwilioCallSid = iBridUtil.GetStringValue(drIvpRequest["TWILIO_CALL_SID"]);

				foreach (DataRow dr in ds.Tables[0].Rows) {
					if (sTwilioCallSid.Equals(iBridUtil.GetStringValue(dr["PARENT_CALL_SID"]))) {
						dr["REQUESTER_SEX_CD"] = iBridUtil.GetStringValue(drIvpRequest["REQUESTER_SEX_CD"]);
						dr["USE_FREE_DIAL_FLAG"] = iBridUtil.GetStringValue(drIvpRequest["USE_FREE_DIAL_FLAG"]);
						dr["USE_WHITE_PLAN_FLAG"] = iBridUtil.GetStringValue(drIvpRequest["USE_WHITE_PLAN_FLAG"]);
						dr["AVAILABLE_SEC"] = iBridUtil.GetStringValue(drIvpRequest["AVAILABLE_SEC"]);
						dr["USE_MAN_VOICEAPP_FLAG"] = iBridUtil.GetStringValue(drIvpRequest["USE_MAN_VOICEAPP_FLAG"]);
						dr["USE_CAST_VOICEAPP_FLAG"] = iBridUtil.GetStringValue(drIvpRequest["USE_CAST_VOICEAPP_FLAG"]);
					}
				}
			}
		}

		return ds;
	}

	private DataSet GetIvpRequestByCallSids(List<string> sCallSids) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	IVP_REQUEST_SEQ,");
		oSqlBuilder.AppendLine("	REQUEST_SRC_ID,");
		oSqlBuilder.AppendLine("	REQUESTER_SEX_CD,");
		oSqlBuilder.AppendLine("	USE_FREE_DIAL_FLAG,");
		oSqlBuilder.AppendLine("	USE_WHITE_PLAN_FLAG,");
		oSqlBuilder.AppendLine("	TWILIO_CALL_SID,");
		oSqlBuilder.AppendLine("	AVAILABLE_SEC,");
		oSqlBuilder.AppendLine("	USE_MAN_VOICEAPP_FLAG,");
		oSqlBuilder.AppendLine("	USE_CAST_VOICEAPP_FLAG");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_IVP_REQUEST");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendFormat("	TWILIO_CALL_SID IN ({0})",string.Join(",",sCallSids.ToArray())).AppendLine();
		oSqlBuilder.AppendLine("ORDER BY IVP_REQUEST_SEQ");

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	private string GetTwilioCallList(string pAccountSid,string pAuthToken,int pPageSize) {
		string sUrl = string.Format("https://api.twilio.com/2010-04-01/Accounts/{0}/Calls?PageSize={1}",pAccountSid,pPageSize);

		try {
			//ViCommInterface.WriteIFLog("GetTwilioCallList",sUrl);
			WebRequest req = WebRequest.Create(sUrl);
			req.Credentials = new NetworkCredential(pAccountSid,pAuthToken);
			req.Timeout = 30000;
			WebResponse res = req.GetResponse();
			Encoding encoding = Encoding.GetEncoding("UTF-8");
			Stream st = res.GetResponseStream();

			using (StreamReader sr = new StreamReader(st,encoding)) {
				string sResponse = sr.ReadToEnd();
				sr.Close();
				st.Close();
				return sResponse;
			}
		} catch (Exception e) {
			ViCommInterface.WriteIFError(e,"GetTwilioCallList",sUrl);
			return "-1";
		}
	}

	public string DisconnectTwilioCall(string pAccountSid,string pAuthToken,string pCallSid) {
		string sUrl = string.Format("https://api.twilio.com/2010-04-01/Accounts/{0}/Calls/{1}",pAccountSid,pCallSid);
		string sPostData = "Status=completed";

		try {
			Encoding enc = Encoding.GetEncoding("UTF-8");
			byte[] postDataBytes = enc.GetBytes(sPostData);
			WebClient wc = new WebClient();
			wc.Headers.Add("Content-Type","application/x-www-form-urlencoded");
			wc.Credentials = new NetworkCredential(pAccountSid,pAuthToken);
			byte[] resData = wc.UploadData(sUrl,postDataBytes);
			wc.Dispose();
			return "0";
			//resText = enc.GetString(resData);
			//return resText;
		} catch (Exception e) {
			ViCommInterface.WriteIFError(e,"DisconnectTwilioCall",sUrl);
			return "-1";
		}
	}
}
