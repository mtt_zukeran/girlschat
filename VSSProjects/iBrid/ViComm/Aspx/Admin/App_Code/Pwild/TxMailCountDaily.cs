﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 日別メール送信数
--	Progaram ID		: TxMailCountDaily
--  Creation Date	: 2015.06.03
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class TxMailCountDaily:DbSession {

	public TxMailCountDaily() {
	}

	public DataSet GetOne(string pSiteCd,string pMailTeomplateNo,string pSexCd,string pTxMailDay) {

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT												");
		oSqlBuilder.AppendLine("	TX_MAIL_COUNT									");
		oSqlBuilder.AppendLine("FROM												");
		oSqlBuilder.AppendLine("	T_TX_MAIL_COUNT_DAILY							");
		oSqlBuilder.AppendLine("WHERE												");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD				AND	");
		oSqlBuilder.AppendLine("	MAIL_TEMPLATE_NO	= :MAIL_TEMPLATE_NO		AND	");
		oSqlBuilder.AppendLine("	SEX_CD				= :SEX_CD				AND	");
		oSqlBuilder.AppendLine("	TX_MAIL_DAY			= :TX_MAIL_DAY				");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":MAIL_TEMPLATE_NO",pMailTeomplateNo));
		oParamList.Add(new OracleParameter(":SEX_CD",pSexCd));
		oParamList.Add(new OracleParameter(":TX_MAIL_DAY",pTxMailDay));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
}
