﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 日別メールアクセス数
--	Progaram ID		: AccessMailCountDaily
--  Creation Date	: 2015.06.03
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;
using System.Text.RegularExpressions;

using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class AccessMailCountDaily:DbSession {

	#region SearchCondition
	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}
		private string fromYYYY;
		public string FromYYYY {
			get {
				return this.fromYYYY;
			}
			set {
				this.fromYYYY = value;
			}
		}
		private string fromMM;
		public string FromMM {
			get {
				return this.fromMM;
			}
			set {
				this.fromMM = value;
			}
		}
		private string fromDD;
		public string FromDD {
			get {
				return this.fromDD;
			}
			set {
				this.fromDD = value;
			}
		}
		private string toYYYY;
		public string ToYYYY {
			get {
				return this.toYYYY;
			}
			set {
				this.toYYYY = value;
			}
		}
		private string toMM;
		public string ToMM {
			get {
				return this.toMM;
			}
			set {
				this.toMM = value;
			}
		}
		private string toDD;
		public string ToDD {
			get {
				return this.toDD;
			}
			set {
				this.toDD = value;
			}
		}
		private string mailTemplateNo;
		public string MailTemplateNo {
			get {
				return this.mailTemplateNo;
			}
			set {
				this.mailTemplateNo = value;
			}
		}
		private string sexCd;
		public string SexCd {
			get {
				return this.sexCd;
			}
			set {
				this.sexCd = value;
			}
		}
	}
	#endregion SearchCondition

	public AccessMailCountDaily() {
	}

	public DataSet GetAccessCountByTxMailDay(SearchCondition pSearchCondition) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhereByTxMailDay(pSearchCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT																");
		oSqlBuilder.AppendLine("	T.SITE_CD													,	");
		oSqlBuilder.AppendLine("	T.MAIL_TEMPLATE_NO											,	");
		oSqlBuilder.AppendLine("	T.SEX_CD													,	");
		oSqlBuilder.AppendLine("	T.TX_MAIL_DAY												,	");
		oSqlBuilder.AppendLine("	T.TX_MAIL_COUNT												,	");
		oSqlBuilder.AppendLine("	NVL(A.ACCESS_COUNT,0) AS ACCESS_COUNT						,	");
		oSqlBuilder.AppendLine("	NVL(A.UNIQUE_ACCESS_COUNT,0) AS UNIQUE_ACCESS_COUNT				");
		oSqlBuilder.AppendLine("FROM																");
		oSqlBuilder.AppendLine("	(");
		oSqlBuilder.AppendLine("		SELECT");
		oSqlBuilder.AppendLine("			SITE_CD	,	");
		oSqlBuilder.AppendLine("			MAIL_TEMPLATE_NO	,	");
		oSqlBuilder.AppendLine("			SEX_CD				,	");
		oSqlBuilder.AppendLine("			TX_MAIL_DAY			,	");
		oSqlBuilder.AppendLine("			TX_MAIL_COUNT			");
		oSqlBuilder.AppendLine("		FROM");
		oSqlBuilder.AppendLine("			T_TX_MAIL_COUNT_DAILY									");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("	) T															,	");
		oSqlBuilder.AppendLine("	(																");
		oSqlBuilder.AppendLine("		SELECT														");
		oSqlBuilder.AppendLine("			SITE_CD												,	");
		oSqlBuilder.AppendLine("			MAIL_TEMPLATE_NO									,	");
		oSqlBuilder.AppendLine("			SEX_CD												,	");
		oSqlBuilder.AppendLine("			TX_MAIL_DAY											,	");
		oSqlBuilder.AppendLine("			SUM(ACCESS_COUNT) AS ACCESS_COUNT					,	");
		oSqlBuilder.AppendLine("			SUM(UNIQUE_ACCESS_COUNT) AS UNIQUE_ACCESS_COUNT			");
		oSqlBuilder.AppendLine("		FROM														");
		oSqlBuilder.AppendLine("			T_ACCESS_MAIL_COUNT_DAILY								");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("		GROUP BY SITE_CD,MAIL_TEMPLATE_NO,SEX_CD,TX_MAIL_DAY		");
		oSqlBuilder.AppendLine("	) A																");
		oSqlBuilder.AppendLine("WHERE																");
		oSqlBuilder.AppendLine("	T.SITE_CD			= A.SITE_CD							(+)	AND	");
		oSqlBuilder.AppendLine("	T.MAIL_TEMPLATE_NO	= A.MAIL_TEMPLATE_NO				(+)	AND	");
		oSqlBuilder.AppendLine("	T.TX_MAIL_DAY		= A.TX_MAIL_DAY						(+)		");
		oSqlBuilder.AppendLine("	ORDER BY T.TX_MAIL_DAY ASC										");

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	private OracleParameter[] CreateWhereByTxMailDay(SearchCondition pSearchCondition,ref string pWhere) {
		List<OracleParameter> oOracleParameterList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			oOracleParameterList.Add(new OracleParameter(":SITE_CD",pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.MailTemplateNo)) {
			string sMailTemplateNoQuery = string.Empty;
			
			if (Regex.IsMatch(pSearchCondition.MailTemplateNo,",|\r?\n")) {
				string[] sMailTemplateNoArray = Regex.Split(pSearchCondition.MailTemplateNo,",|\r?\n");
				StringBuilder oMailTemplateNoWhere = new StringBuilder("MAIL_TEMPLATE_NO IN(");
				List<string> oMailTemplateNoWhereList = new List<string>();
				
				for (int iIndex = 0;iIndex < sMailTemplateNoArray.Length;iIndex++) {
					if (string.IsNullOrEmpty(sMailTemplateNoArray[iIndex].Trim())) {
						continue;
					}
					oMailTemplateNoWhereList.Add(string.Format(":MAIL_TEMPLATE_NO{0}",iIndex));
					oOracleParameterList.Add(new OracleParameter(string.Concat(":MAIL_TEMPLATE_NO",iIndex),sMailTemplateNoArray[iIndex].Trim()));
				}
				
				oMailTemplateNoWhere.Append(string.Join(",",oMailTemplateNoWhereList.ToArray()));
				oMailTemplateNoWhere.Append(")");
				SysPrograms.SqlAppendWhere(iBridUtil.GetStringValue(oMailTemplateNoWhere),ref pWhere);
			} else {
				SysPrograms.SqlAppendWhere("MAIL_TEMPLATE_NO = :MAIL_TEMPLATE_NO",ref pWhere);
				oOracleParameterList.Add(new OracleParameter(":MAIL_TEMPLATE_NO",pSearchCondition.MailTemplateNo));
			}
		}

		if (!string.IsNullOrEmpty(pSearchCondition.SexCd)) {
			SysPrograms.SqlAppendWhere("SEX_CD = :SEX_CD",ref pWhere);
			oOracleParameterList.Add(new OracleParameter(":SEX_CD",pSearchCondition.SexCd));
		}

		string sFromDay = pSearchCondition.FromYYYY + '/' + pSearchCondition.FromMM + '/' + pSearchCondition.FromDD;
		string sToDay = pSearchCondition.ToYYYY + '/' + pSearchCondition.ToMM + '/' + pSearchCondition.ToDD;

		DateTime dtCheck;
		if (DateTime.TryParse(sFromDay,out dtCheck)) {
			SysPrograms.SqlAppendWhere("TX_MAIL_DAY >= :TX_MAIL_DAY_FROM",ref pWhere);
			oOracleParameterList.Add(new OracleParameter(":TX_MAIL_DAY_FROM",sFromDay));
		}
		
		if (DateTime.TryParse(sToDay,out dtCheck)) {
			SysPrograms.SqlAppendWhere("TX_MAIL_DAY <= :TX_MAIL_DAY_TO",ref pWhere);
			oOracleParameterList.Add(new OracleParameter(":TX_MAIL_DAY_TO",sToDay));
		}

		return oOracleParameterList.ToArray();
	}

	public DataSet GetAccessMailCountByAccessDay(string pSiteCd,string pMailTemplateNo,string pSexCd,string pTxMailDay) {

		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT																							");
		oSqlBuilder.AppendLine("	C.DAYS																					,	");
		oSqlBuilder.AppendLine("	P.ACCESS_COUNT																			,	");
		oSqlBuilder.AppendLine("	P.UNIQUE_ACCESS_COUNT																	,	");
		oSqlBuilder.AppendLine("	P.PASSAGE_DAY_COUNT																			");
		oSqlBuilder.AppendLine("FROM																							");
		oSqlBuilder.AppendLine("	(																							");
		oSqlBuilder.AppendLine("		SELECT																					");
		oSqlBuilder.AppendLine("			TO_CHAR(TRUNC(TO_DATE(:TX_MAIL_DAY)) + ROWNUM -1, 'YYYY/MM/DD') AS DAYS				");
		oSqlBuilder.AppendLine("		FROM																					");
		oSqlBuilder.AppendLine("			ALL_CATALOG																			");
		oSqlBuilder.AppendLine("		WHERE																					");
		oSqlBuilder.AppendLine("			ROWNUM <= 10																		");
		oSqlBuilder.AppendLine("	) C																						,	");
		oSqlBuilder.AppendLine("	(																							");
		oSqlBuilder.AppendLine("		SELECT																					");
		oSqlBuilder.AppendLine("			ACCESS_COUNT																	,	");
		oSqlBuilder.AppendLine("			UNIQUE_ACCESS_COUNT																,	");
		oSqlBuilder.AppendLine("			ACCESS_DAY																		,	");
		oSqlBuilder.AppendLine("			TO_DATE(ACCESS_DAY,'YYYY/MM/DD') - TO_DATE(TX_MAIL_DAY,'YYYY/MM/DD') AS PASSAGE_DAY_COUNT	");
		oSqlBuilder.AppendLine("		FROM																					");
		oSqlBuilder.AppendLine("			T_ACCESS_MAIL_COUNT_DAILY															");
		oSqlBuilder.AppendLine("		WHERE																					");
		oSqlBuilder.AppendLine("			SITE_CD				= :SITE_CD													AND	");
		oSqlBuilder.AppendLine("			MAIL_TEMPLATE_NO	= :MAIL_TEMPLATE_NO											AND	");
		oSqlBuilder.AppendLine("			SEX_CD				= :SEX_CD													AND	");
		oSqlBuilder.AppendLine("			TX_MAIL_DAY			= :TX_MAIL_DAY													");
		oSqlBuilder.AppendLine("	) P																							");
		oSqlBuilder.AppendLine("WHERE																							");
		oSqlBuilder.AppendLine("	C.DAYS	= P.ACCESS_DAY																		");
		oSqlBuilder.AppendLine("ORDER BY C.DAYS ASC																				");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":MAIL_TEMPLATE_NO",pMailTemplateNo));
		oParamList.Add(new OracleParameter(":SEX_CD",pSexCd));
		oParamList.Add(new OracleParameter(":TX_MAIL_DAY",pTxMailDay));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}

	public DataSet GetAccessCountByMailTemplate(SearchCondition pSearchCondition) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhereByTxMailDay(pSearchCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT																");
		oSqlBuilder.AppendLine("	TMC.SITE_CD													,	");
		oSqlBuilder.AppendLine("	TMC.MAIL_TEMPLATE_NO										,	");
		oSqlBuilder.AppendLine("	TMC.TX_MAIL_COUNT											,	");
		oSqlBuilder.AppendLine("	NVL(AMC.ACCESS_COUNT,0) AS ACCESS_COUNT						,	");
		oSqlBuilder.AppendLine("	NVL(AMC.UNIQUE_ACCESS_COUNT,0) AS UNIQUE_ACCESS_COUNT		,	");
		oSqlBuilder.AppendLine("	MT.TEMPLATE_NM													");
		oSqlBuilder.AppendLine("FROM																");
		oSqlBuilder.AppendLine("	(																");
		oSqlBuilder.AppendLine("		SELECT														");
		oSqlBuilder.AppendLine("			SITE_CD												,	");
		oSqlBuilder.AppendLine("			MAIL_TEMPLATE_NO									,	");
		oSqlBuilder.AppendLine("			SEX_CD												,	");
		oSqlBuilder.AppendLine("			SUM(TX_MAIL_COUNT) AS TX_MAIL_COUNT						");
		oSqlBuilder.AppendLine("		FROM														");
		oSqlBuilder.AppendLine("			T_TX_MAIL_COUNT_DAILY									");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("		GROUP BY SITE_CD,MAIL_TEMPLATE_NO,SEX_CD					");
		oSqlBuilder.AppendLine("	) TMC														,	");
		oSqlBuilder.AppendLine("	(																");
		oSqlBuilder.AppendLine("		SELECT														");
		oSqlBuilder.AppendLine("			SITE_CD												,	");
		oSqlBuilder.AppendLine("			MAIL_TEMPLATE_NO									,	");
		oSqlBuilder.AppendLine("			SEX_CD												,	");
		oSqlBuilder.AppendLine("			SUM(ACCESS_COUNT) AS ACCESS_COUNT					,	");
		oSqlBuilder.AppendLine("			SUM(UNIQUE_ACCESS_COUNT) AS UNIQUE_ACCESS_COUNT			");
		oSqlBuilder.AppendLine("		FROM														");
		oSqlBuilder.AppendLine("			T_ACCESS_MAIL_COUNT_DAILY								");
		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine("		GROUP BY SITE_CD,MAIL_TEMPLATE_NO,SEX_CD					");
		oSqlBuilder.AppendLine("	) AMC														,	");
		oSqlBuilder.AppendLine("	T_MAIL_TEMPLATE MT												");
		oSqlBuilder.AppendLine("WHERE																");
		oSqlBuilder.AppendLine("	TMC.SITE_CD				= MT.SITE_CD						AND	");
		oSqlBuilder.AppendLine("	TMC.MAIL_TEMPLATE_NO	= MT.MAIL_TEMPLATE_NO				AND	");
		oSqlBuilder.AppendLine("	TMC.SITE_CD				= AMC.SITE_CD					(+)	AND	");
		oSqlBuilder.AppendLine("	TMC.MAIL_TEMPLATE_NO	= AMC.MAIL_TEMPLATE_NO			(+)	AND	");
		oSqlBuilder.AppendLine("	TMC.SEX_CD				= AMC.SEX_CD					(+)		");
		oSqlBuilder.AppendLine("ORDER BY MAIL_TEMPLATE_NO ASC										");

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return oDataSet;
	}
}
