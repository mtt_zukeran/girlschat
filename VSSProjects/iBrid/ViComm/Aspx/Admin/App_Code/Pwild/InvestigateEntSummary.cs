﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: この娘を探せ・男性エントリー(合計)


--	Progaram ID		: InvestigateEntSummary
--  Creation Date	: 2015.02.26
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;

public class InvestigateEntSummary:DbSession {
	public InvestigateEntSummary() {
	}

	public DataSet GetList(string pSiteCd,string pTargetMonth) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();
		
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	IES.SITE_CD																		,	");
		oSqlBuilder.AppendLine("	IES.EXECUTION_DAY																,	");
		oSqlBuilder.AppendLine("	TO_CHAR(TO_DATE(IES.EXECUTION_DAY, 'YYYY/MM/DD'), 'DD') DAY						,	");
		oSqlBuilder.AppendLine("	TO_CHAR(TO_DATE(IES.EXECUTION_DAY, 'YYYY/MM/DD'), 'DY') DAY_OF_WEEK				,	");
		oSqlBuilder.AppendLine("	IES.ENTRY_COUNT																	,	");
		oSqlBuilder.AppendLine("	IES.CAUGHT_COUNT																,	");
		oSqlBuilder.AppendLine("	IES.POINT_ACQUIRED_COUNT														,	");
		oSqlBuilder.AppendLine("	IES.POINT_DIVISOR																,	");
		oSqlBuilder.AppendLine("	IVS.BOUNTY_POINT																,	");
		oSqlBuilder.AppendLine("	IVS.POINT_PER_ENTRANT																");
		oSqlBuilder.AppendLine("FROM																					");
		oSqlBuilder.AppendLine("	T_INVESTIGATE_ENT_SUMMARY IES													,	");
		oSqlBuilder.AppendLine("	T_INVESTIGATE_SCHEDULE IVS															");
		oSqlBuilder.AppendLine("WHERE																					");
		oSqlBuilder.AppendLine("	IES.SITE_CD						= IVS.SITE_CD									AND	");
		oSqlBuilder.AppendLine("	IES.EXECUTION_DAY				= IVS.EXECUTION_DAY								AND	");
		oSqlBuilder.AppendLine("	IES.SITE_CD						= :SITE_CD										AND	");
		oSqlBuilder.AppendLine("	TO_DATE(:TARGET_MONTH || '01')	<= TO_DATE(IES.EXECUTION_DAY)					AND ");
		oSqlBuilder.AppendLine("	TO_DATE(IES.EXECUTION_DAY)		<= LAST_DAY(TO_DATE(:TARGET_MONTH || '01'))			");
		oSqlBuilder.AppendLine("ORDER BY																				");
		oSqlBuilder.AppendLine("	EXECUTION_DAY																		");
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":TARGET_MONTH",pTargetMonth));

		return ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
	}
	
	public DataSet GetWantedCompSheetCntList (string pSiteCd,string pReportMonth) {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.Append("SELECT													");
		oSqlBuilder.Append("	U.LOGIN_ID										,	");
		oSqlBuilder.Append("	UMC.HANDLE_NM									,	");
		oSqlBuilder.Append("	WCSC.COMPLETE_COUNT									");
		oSqlBuilder.Append("FROM													");
		oSqlBuilder.Append("	T_WANTED_COMP_SHEET_CNT	WCSC					,	");
		oSqlBuilder.Append("	T_USER_MAN_CHARACTER UMC						,	");
		oSqlBuilder.Append("	T_USER U											");
		oSqlBuilder.Append("WHERE													");
		oSqlBuilder.Append("	WCSC.SITE_CD			= UMC.SITE_CD			AND	");
		oSqlBuilder.Append("	WCSC.USER_SEQ			= UMC.USER_SEQ			AND	");
		oSqlBuilder.Append("	WCSC.USER_CHAR_NO		= UMC.USER_CHAR_NO		AND	");
		oSqlBuilder.Append("	WCSC.USER_SEQ			= U.USER_SEQ			AND	");
		oSqlBuilder.Append("	WCSC.SITE_CD			= :SITE_CD				AND	");
		oSqlBuilder.Append("	WCSC.REPORT_MONTH		= :REPORT_MONTH				");

		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":REPORT_MONTH",pReportMonth));

		return ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
	}
}
