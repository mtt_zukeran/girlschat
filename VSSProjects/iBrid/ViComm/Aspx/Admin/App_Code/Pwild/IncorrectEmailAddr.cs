﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 不正メールアドレス
--	Progaram ID		: IncorrectEmailAddr
--  Creation Date	: 2015.03.13
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

public class IncorrectEmailAddr:DbSession {
	public class SearchCondition {
		private string siteCd;

		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}
	}

	public IncorrectEmailAddr() {
	}

	public int GetPageCount(SearchCondition pSearchCondition) {
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_INCORRECT_EMAIL_ADDR");
		oSqlBuilder.AppendLine(sWhereClause);

		return ExecuteSelectCountQueryBase(oSqlBuilder,oParamList.ToArray());
	}

	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sExecSql = string.Empty;
		string sWhereClause = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(this.CreateWhere(pSearchCondition,ref sWhereClause));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	T_INCORRECT_EMAIL_ADDR.SITE_CD,");
		oSqlBuilder.AppendLine("	T_INCORRECT_EMAIL_ADDR.EMAIL_ADDR,");
		oSqlBuilder.AppendLine("	T_INCORRECT_EMAIL_ADDR.REGIST_DATE,");
		oSqlBuilder.AppendLine("	T_USER.SEX_CD,");
		oSqlBuilder.AppendLine("	T_USER.LOGIN_ID");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_INCORRECT_EMAIL_ADDR");
		oSqlBuilder.AppendLine("	LEFT OUTER JOIN T_USER ON (T_INCORRECT_EMAIL_ADDR.USER_SEQ = T_USER.USER_SEQ)");
		oSqlBuilder.AppendLine(sWhereClause);

		string sSortExpression = "ORDER BY T_INCORRECT_EMAIL_ADDR.REGIST_DATE DESC";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		return ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhereClause) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pSearchCondition.SiteCd));
		}

		return oParamList.ToArray();
	}
}
