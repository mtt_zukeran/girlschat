﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 会員つぶやきコメント
--	Progaram ID		: ManTweetComment
--  Creation Date	: 2013.02.19
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class ManTweetComment:DbSession {

	#region SearchCondition
	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}
		private string tweetDateFrom;
		public string TweetDateFrom {
			get {
				return this.tweetDateFrom;
			}
			set {
				this.tweetDateFrom = value;
			}
		}
		private string tweetDateTo;
		public string TweetDateTo {
			get {
				return this.tweetDateTo;
			}
			set {
				this.tweetDateTo = value;
			}
		}
		private string loginId;
		public string LoginId {
			get {
				return this.loginId;
			}
			set {
				this.loginId = value;
			}
		}
		private string keyword;
		public string Keyword {
			get {
				return this.keyword;
			}
			set {
				this.keyword = value;
			}
		}
		private string manTweetSeq;
		public string ManTweetSeq {
			get {
				return this.manTweetSeq;
			}
			set {
				this.manTweetSeq = value;
			}
		}
	}
	#endregion SearchCondition

	public ManTweetComment() {
	}

	public int GetPageCount(object pSearchCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT									");
		oSqlBuilder.AppendLine("	COUNT(*)							");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	VW_MAN_TWEET_COMMENT00				");

		string sWhereClause;
		OracleParameter[] oWhereParams = this.CreateWhere((SearchCondition)pSearchCondition,out sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(object pSearchCondition,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	LOGIN_ID						,	");
		oSqlBuilder.AppendLine("	HANDLE_NM						,	");
		oSqlBuilder.AppendLine("	SITE_CD							,	");
		oSqlBuilder.AppendLine("	USER_SEQ						,	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO					,	");
		oSqlBuilder.AppendLine("	MAN_TWEET_COMMENT_SEQ			,	");
		oSqlBuilder.AppendLine("	MAN_TWEET_SEQ					,	");
		oSqlBuilder.AppendLine("	COMMENT_TEXT					,	");
		oSqlBuilder.AppendLine("	COMMENT_DATE					,	");
		oSqlBuilder.AppendLine("	ADMIN_DEL_FLAG					,	");
		oSqlBuilder.AppendLine("	CASE								");
		oSqlBuilder.AppendLine("		WHEN ADMIN_DEL_FLAG = 1			");
		oSqlBuilder.AppendLine("		THEN '管理者削除'				");
		oSqlBuilder.AppendLine("		ELSE NULL						");
		oSqlBuilder.AppendLine("	END AS ADMIN_DEL_STATUS			,	");
		oSqlBuilder.AppendLine("	DEL_FLAG						,	");
		oSqlBuilder.AppendLine("	CASE								");
		oSqlBuilder.AppendLine("		WHEN DEL_FLAG = 1				");
		oSqlBuilder.AppendLine("		THEN 'ユーザー削除'				");
		oSqlBuilder.AppendLine("		ELSE NULL						");
		oSqlBuilder.AppendLine("	END AS DEL_STATUS				,	");
		oSqlBuilder.AppendLine("	TWEET_DATE							");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	VW_MAN_TWEET_COMMENT00				");

		string sWhereClause;
		string sSortExpression = this.GetOrder((SearchCondition)pSearchCondition);
		string sPagingSql;

		OracleParameter[] oWhereParams = this.CreateWhere((SearchCondition)pSearchCondition,out sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}

	private string GetOrder(SearchCondition pSearchCondition) {
		return "ORDER BY COMMENT_DATE DESC, LOGIN_ID";
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,out string pWhere) {
		pWhere = string.Empty;
		List<OracleParameter> oOracleParameterList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("SITE_CD",pSearchCondition.SiteCd));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.ManTweetSeq)) {
			SysPrograms.SqlAppendWhere("MAN_TWEET_SEQ = :MAN_TWEET_SEQ",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("MAN_TWEET_SEQ",pSearchCondition.ManTweetSeq));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.TweetDateFrom) && !string.IsNullOrEmpty(pSearchCondition.TweetDateTo)) {
			DateTime dtFrom = DateTime.Parse(pSearchCondition.TweetDateFrom);
			DateTime dtTo = DateTime.Parse(pSearchCondition.TweetDateTo).AddSeconds(1);
			SysPrograms.SqlAppendWhere("(COMMENT_DATE >= :COMMENT_DATE_FROM AND COMMENT_DATE < :COMMENT_DATE_TO)",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("COMMENT_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			oOracleParameterList.Add(new OracleParameter("COMMENT_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.LoginId)) {
			SysPrograms.SqlAppendWhere("LOGIN_ID = :LOGIN_ID",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("LOGIN_ID",pSearchCondition.LoginId));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.Keyword)) {
			StringBuilder oWhereKeywords = new StringBuilder("(");
			string[] sKeywordArray = pSearchCondition.Keyword.Split(' ','　');
			for (int i = 0;i < sKeywordArray.Length;i++) {
				oWhereKeywords.AppendFormat("(COMMENT_TEXT LIKE :COMMENT_TEXT{0})",i);
				oOracleParameterList.Add(new OracleParameter(string.Format("COMMENT_TEXT{0}",i),string.Format("%{0}%",sKeywordArray[i])));

				if (i < sKeywordArray.Length - 1) {
					oWhereKeywords.AppendLine(" AND ");
				}
			}
			oWhereKeywords.Append(")");
			SysPrograms.SqlAppendWhere(oWhereKeywords.ToString(),ref pWhere);
		}

		return oOracleParameterList.ToArray();
	}

	/// <summary>
	/// 検索条件に一致するレコードを取得する
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <returns></returns>
	public DataSet GetList(object pSearchCondition) {
		// 検索用のクエリを設定
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	LOGIN_ID						,	");
		oSqlBuilder.AppendLine("	HANDLE_NM						,	");
		oSqlBuilder.AppendLine("	SITE_CD							,	");
		oSqlBuilder.AppendLine("	USER_SEQ						,	");
		oSqlBuilder.AppendLine("	USER_CHAR_NO					,	");
		oSqlBuilder.AppendLine("	MAN_TWEET_COMMENT_SEQ			,	");
		oSqlBuilder.AppendLine("	MAN_TWEET_SEQ					,	");
		oSqlBuilder.AppendLine("	COMMENT_TEXT					,	");
		oSqlBuilder.AppendLine("	COMMENT_DATE					,	");
		oSqlBuilder.AppendLine("	ADMIN_DEL_FLAG					,	");
		oSqlBuilder.AppendLine("	CASE								");
		oSqlBuilder.AppendLine("		WHEN ADMIN_DEL_FLAG = 1			");
		oSqlBuilder.AppendLine("		THEN '管理者削除'				");
		oSqlBuilder.AppendLine("		ELSE NULL						");
		oSqlBuilder.AppendLine("	END AS ADMIN_DEL_STATUS			,	");
		oSqlBuilder.AppendLine("	DEL_FLAG						,	");
		oSqlBuilder.AppendLine("	CASE								");
		oSqlBuilder.AppendLine("		WHEN DEL_FLAG = 1				");
		oSqlBuilder.AppendLine("		THEN 'ユーザー削除'				");
		oSqlBuilder.AppendLine("		ELSE NULL						");
		oSqlBuilder.AppendLine("	END AS DEL_STATUS				,	");
		oSqlBuilder.AppendLine("	TWEET_DATE							");
		oSqlBuilder.AppendLine("FROM									");
		oSqlBuilder.AppendLine("	VW_MAN_TWEET_COMMENT00				");

		// 検索条件を設定
		string sWhereClause;
		OracleParameter[] oWhereParams = this.CreateWhere((SearchCondition)pSearchCondition,out sWhereClause);
		oSqlBuilder.AppendLine(sWhereClause);

		// ソート条件を設定
		string sSortExpression = this.GetOrder((SearchCondition)pSearchCondition);
		oSqlBuilder.AppendLine(sSortExpression);

		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);

				// 検索実行
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
}