﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 電話番号重複
--	Progaram ID		: TelOverlap
--  Creation Date	: 2014.09.16
--  Creater			: K.Miyazato
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;
using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class TelOverlap:DbSession {
	public TelOverlap() {
	}

	public DataSet GetList() {
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	T.USER_SEQ,");
		oSqlBuilder.AppendLine("	T.TEL,");
		oSqlBuilder.AppendLine("	T.REGIST_DATE,");
		oSqlBuilder.AppendLine("	U.LOGIN_ID");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_TEL_OVERLAP T,");
		oSqlBuilder.AppendLine("	T_USER U");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	T.USER_SEQ = U.USER_SEQ");
		oSqlBuilder.AppendLine("ORDER BY");
		oSqlBuilder.AppendLine("	T.REGIST_DATE DESC");

		return ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());
	}
}
