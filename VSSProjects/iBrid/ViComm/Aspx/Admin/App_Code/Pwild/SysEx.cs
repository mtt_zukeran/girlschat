﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: システム設定拡張
--	Progaram ID		: SysEx
--  Creation Date	: 2012.09.06
--  Creater			: PW K.Miyazato
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System.Collections.Generic;
using iBridCommLib;

public class SysEx:DbSession {
	public SysEx() {
	}

	public bool GetValue(string pItem,out string pValue) {
		bool bExist = false;
		pValue = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSqlBuilder.AppendLine("SELECT			");
		oSqlBuilder.AppendLine("	CSV_PASSWORD");
		oSqlBuilder.AppendLine("FROM			");
		oSqlBuilder.AppendLine("	T_SYS_EX	");

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder,oParamList.ToArray());

		if (oDataSet.Tables[0].Rows.Count != 0) {
			DataRow dr = oDataSet.Tables[0].Rows[0];
			pValue = dr[pItem].ToString();
			bExist = true;
		}

		return bExist;
	}
}
