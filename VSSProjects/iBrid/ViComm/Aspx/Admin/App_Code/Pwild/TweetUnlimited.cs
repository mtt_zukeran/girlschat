﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: つぶやき無制限イベント設定
--	Progaram ID		: TweetUnlimited
--  Creation Date	: 2017.03.23
--  Creater			: M&TT Zukeran
--
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Web;
using System.Web.UI;
using ViComm;
using iBridCommLib;

public class TweetUnlimited:DbSession {

	#region SearchCondition
	public class SearchCondition {
		/// <summary>サイトCD</summary>
		public string SiteCd;
		/// <summary>性別</summary>
		public string SexCd;
		/// <summary>判定用開始日時(形式：'YYYY-MM-DD HH24:MI:SS')</summary>
		public string CheckStartDate;
		/// <summary>判定用終了日時(形式：'YYYY-MM-DD HH24:MI:SS')</summary>
		public string CheckEndDate;
		/// <summary>開始日時、終了日時の判定対象外レコードID</summary>
		public string RowId;

		public SearchCondition() {
			this.SiteCd = string.Empty;
			this.SexCd = string.Empty;
			this.CheckStartDate = string.Empty;
			this.CheckEndDate = string.Empty;
			this.RowId = string.Empty;
		}
	}
	#endregion SearchCondition

	#region MainteData
	public class MainteData {
		/// <summary>サイトCD</summary>
		public string SiteCd;
		/// <summary>性別</summary>
		public string SexCd;
		/// <summary>開始日時(形式：'YYYY-MM-DD HH24:MI:SS')</summary>
		public string StartDate;
		/// <summary>終了日時(形式：'YYYY-MM-DD HH24:MI:SS')</summary>
		public string EndDate;
		/// <summary>初回入金後、経過日数</summary>
		public string ElapsedDays;
		/// <summary>レコードID</summary>
		public string RowId;
		/// <summary>リビジョンNo</summary>
		public string RevisionNo;
		/// <summary>削除フラグ</summary>
		public string DelFlag;

		public MainteData() {
			this.SiteCd = string.Empty;
			this.SexCd = string.Empty;
			this.StartDate = string.Empty;
			this.EndDate = string.Empty;
			this.ElapsedDays = string.Empty;
			this.RowId = string.Empty;
			this.RevisionNo = string.Empty;
			this.DelFlag = string.Empty;
		}
	}
	#endregion MainteData

	public TweetUnlimited() {
	}

	/// <summary>
	/// 条件設定
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <param name="pWhere"></param>
	/// <returns></returns>
	public OracleParameter[] CreateWhere(SearchCondition pSearchCondition,ref string pWhere) {
		List<OracleParameter> oParamList = new List<OracleParameter>();

		// サイトCD
		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("P.SITE_CD = :SITE_CD",ref pWhere);
			oParamList.Add(new OracleParameter("SITE_CD",pSearchCondition.SiteCd));
		}

		// 性別
		if (!string.IsNullOrEmpty(pSearchCondition.SexCd)) {
			SysPrograms.SqlAppendWhere("P.SEX_CD = :SEX_CD",ref pWhere);
			oParamList.Add(new OracleParameter("SEX_CD",pSearchCondition.SexCd));
		}

		// 判定用開始日時、終了日時
		if (!string.IsNullOrEmpty(pSearchCondition.CheckStartDate) && !string.IsNullOrEmpty(pSearchCondition.CheckEndDate)) {
			bool bStartDate = false;
			bool bEndDate = false;
			DateTime oDateTime = DateTime.Today;
			bStartDate = DateTime.TryParse(pSearchCondition.CheckStartDate,out oDateTime);
			bEndDate = DateTime.TryParse(pSearchCondition.CheckEndDate,out oDateTime);

			if (bStartDate && bEndDate) {
				SysPrograms.SqlAppendWhere("P.START_DATE <= TO_DATE(:END_DATE, 'YYYY-MM-DD HH24:MI:SS')",ref pWhere);
				SysPrograms.SqlAppendWhere("P.END_DATE >= TO_DATE(:START_DATE, 'YYYY-MM-DD HH24:MI:SS')",ref pWhere);
				oParamList.Add(new OracleParameter("START_DATE",pSearchCondition.CheckStartDate));
				oParamList.Add(new OracleParameter("END_DATE",pSearchCondition.CheckEndDate));

				// 判定対象外
				if (!string.IsNullOrEmpty(pSearchCondition.RowId)) {
					SysPrograms.SqlAppendWhere("P.ROWID <> :TWEET_UNLIMITED_ROWID",ref pWhere);
					oParamList.Add(new OracleParameter("TWEET_UNLIMITED_ROWID",pSearchCondition.RowId));
				}
			}
		}

		return oParamList.ToArray();
	}

	/// <summary>
	/// イベント情報 件数取得
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <param name="startRowIndex"></param>
	/// <param name="maximumRows"></param>
	/// <returns></returns>
	public int GetPageCount(SearchCondition pSearchCondition) {
		string sWhere = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(CreateWhere(pSearchCondition,ref sWhere));

		oSqlBuilder.AppendLine("SELECT COUNT(*) FROM T_TWEET_UNLIMITED P");
		oSqlBuilder.AppendLine(sWhere);

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());

		return int.Parse(oDataSet.Tables[0].Rows[0][0].ToString());
	}

	/// <summary>
	/// イベント情報 データ取得
	/// </summary>
	/// <param name="pSearchCondition"></param>
	/// <param name="startRowIndex"></param>
	/// <param name="maximumRows"></param>
	/// <returns></returns>
	public DataSet GetPageCollection(SearchCondition pSearchCondition,int startRowIndex,int maximumRows) {
		string sExecSql = string.Empty;
		string sWhere = string.Empty;
		string sSortExpression = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(CreateWhere(pSearchCondition,ref sWhere));

		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	P.ROWID AS TWEET_UNLIMITED_ROWID");
		oSqlBuilder.AppendLine("	,P.ELAPSED_DAYS");
		oSqlBuilder.AppendLine("	,P.START_DATE");
		oSqlBuilder.AppendLine("	,P.END_DATE");
		oSqlBuilder.AppendLine("	,P.REVISION_NO");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_TWEET_UNLIMITED P");
		oSqlBuilder.AppendLine(sWhere);
		sSortExpression = "ORDER BY P.START_DATE DESC";

		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sExecSql));

		return ExecuteSelectQueryBase(sExecSql,oParamList.ToArray());
	}

	/// <summary>
	/// レコード編集
	/// </summary>
	/// <param name="oMainteData"></param>
	/// <returns></returns>
	public string Mainte(MainteData oMainteData) {
		string sStatus = string.Empty;

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("TWEET_UNLIMITED_MAINTE");
			oDbSession.ProcedureInParm("pTWEET_UNLIMITED_ROWID",DbSession.DbType.VARCHAR2,oMainteData.RowId);
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,oMainteData.SiteCd);
			oDbSession.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,oMainteData.SexCd);
			oDbSession.ProcedureInParm("pELAPSED_DAYS",DbSession.DbType.NUMBER,oMainteData.ElapsedDays);
			oDbSession.ProcedureInParm("pSTART_DATE",DbSession.DbType.VARCHAR2,oMainteData.StartDate);
			oDbSession.ProcedureInParm("pEND_DATE",DbSession.DbType.VARCHAR2,oMainteData.EndDate);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,oMainteData.RevisionNo);
			oDbSession.ProcedureInParm("pADMIN_ID",DbSession.DbType.VARCHAR2,SessionObjs.AdminId);
			oDbSession.ProcedureInParm("pDELETE_FLAG",DbSession.DbType.NUMBER,oMainteData.DelFlag);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
			sStatus = oDbSession.GetStringValue("pSTATUS");
		}

		return sStatus;
	}

	/// <summary>
	/// イベント期間の重複チェック
	/// </summary>
	/// <param name="oSearchCondition"></param>
	/// <returns></returns>
	public bool IsDuplicateTerm(SearchCondition pSearchCondition) {
		string sWhere = string.Empty;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oParamList.AddRange(CreateWhere(pSearchCondition,ref sWhere));

		oSqlBuilder.AppendLine("SELECT ROWID FROM T_TWEET_UNLIMITED P");
		oSqlBuilder.AppendLine(sWhere);

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		return (oDataSet.Tables[0].Rows.Count > 0);
	}
}
