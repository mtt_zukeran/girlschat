/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ユーザー課金
--	Progaram ID		: UserCharge
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class UserCharge:DbSession {

	public UserCharge() {
	}

	public int GetPageCount(string pSiteCd,string pCastRank,string pCampainCd) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_USER_CHARGE01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pCastRank,pCampainCd,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pCastRank,string pCampainCd,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY  SITE_CD,ACT_CATEGORY_SEQ,USER_RANK,CAST_RANK,DISPLAY_ORDER ";
			string sSql = "SELECT " +
							"SITE_CD					," +
							"SITE_NM					," +
							"USER_RANK					," +
							"USER_RANK_NM				," +
							"CHARGE_TYPE				," +
							"ACT_CATEGORY_SEQ			," +
							"CAST_RANK					," +
							"CAMPAIN_CD					," +
							"CHARGE_POINT_NORMAL		," +
							"CHARGE_POINT_NEW			," +
							"CHARGE_POINT_NO_RECEIPT	," +
							"CHARGE_POINT_NO_USE		," +
							"CHARGE_POINT_FREE_DIAL		," +
							"CHARGE_UNIT_SEC			," +
							"UPDATE_DATE				," +
							"REVISION_NO				," +
							"CHARGE_POINT_WHITE_PLAN	," +
							"CHARGE_CAST_TALK_APP_POINT	," +
							"CHARGE_MAN_TALK_APP_POINT	," +
							"CHARGE_TYPE_NM				," +
							"CHARGE_UNIT_TYPE			," +
							"DISPLAY_ORDER				," +
							"ACT_CATEGORY_NM			" +
							"FROM(" +
							" SELECT VW_USER_CHARGE01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_USER_CHARGE01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pCastRank,pCampainCd,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USER_CHARGE01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	public DataSet GetCsvData(string pSiteCd,string pCastRank,string pCampainType) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY SITE_CD,USER_RANK,CHARGE_TYPE,ACT_CATEGORY_SEQ,CAST_RANK ";
			string sSql = "SELECT " +
							"SITE_CD					," +
							"USER_RANK					," +
							"CHARGE_TYPE				," +
							"CATEGORY_INDEX				," +
							"CAST_RANK					," +
							"CAMPAIN_CD					," +
							"CHARGE_POINT_NORMAL		," +
							"CHARGE_POINT_NEW			," +
							"CHARGE_POINT_NO_RECEIPT	," +
							"CHARGE_POINT_NO_USE		," +
							"CHARGE_POINT_FREE_DIAL		," +
							"CHARGE_POINT_WHITE_PLAN	," +
							"CHARGE_CAST_TALK_APP_POINT	," +
							"CHARGE_MAN_TALK_APP_POINT	," +
							"CHARGE_UNIT_SEC			" +
							"FROM " +
							 "VW_USER_CHARGE01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pCastRank,pCampainType,ref sWhere);
			sSql = sSql + sWhere;
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USER_CHARGE01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,string pCastRank,string pCampainCd,ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		pWhere = pWhere + " WHERE SITE_CD = :SITE_CD  ";
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		if (!pCastRank.Equals(string.Empty)) {
			pWhere = pWhere + " AND CAST_RANK = :CAST_RANK ";
			list.Add(new OracleParameter("CAST_RANK",pCastRank));
		}
		if (!pCampainCd.Equals(string.Empty)) {
			pWhere = pWhere + " AND CAMPAIN_CD = :CAMPAIN_CD ";
			list.Add(new OracleParameter("CAMPAIN_CD",pCampainCd));
		}
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}


}
