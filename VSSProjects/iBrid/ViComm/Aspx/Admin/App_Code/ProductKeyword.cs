﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 商品ｷｰﾜｰﾄﾞ

--	Progaram ID		: ProductKeyword
--
--  Creation Date	: 2010.12.08
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class ProductKeyword :DbSession{
	public ProductKeyword() {}
	
	public DataSet GetList(string pSiteCd,string pProductAgentCd,string pProductSeq){
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT ");
		oSqlBuilder.Append(" 	SITE_CD					,	");
		oSqlBuilder.Append(" 	PRODUCT_AGENT_CD       	,	");
		oSqlBuilder.Append(" 	PRODUCT_SEQ            	,	");
		oSqlBuilder.Append(" 	PRODUCT_KEYWORD_SEQ    	,	");
		oSqlBuilder.Append(" 	PRODUCT_KEYWORD_VALUE  		");
		oSqlBuilder.Append(" FROM ");
		oSqlBuilder.Append("	T_PRODUCT_KEYWORD ");
		oSqlBuilder.Append(" WHERE ");
		oSqlBuilder.Append("	T_PRODUCT_KEYWORD.SITE_CD			= :SITE_CD			AND ");
		oSqlBuilder.Append("	T_PRODUCT_KEYWORD.PRODUCT_AGENT_CD	= :PRODUCT_AGENT_CD	AND ");
		oSqlBuilder.Append("	T_PRODUCT_KEYWORD.PRODUCT_SEQ		= :PRODUCT_SEQ			");
		
		DataSet oDs = new DataSet();
		try{
			conn = DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD", pSiteCd);
				cmd.Parameters.Add(":PRODUCT_AGENT_CD", pProductAgentCd);
				cmd.Parameters.Add(":PRODUCT_SEQ", pProductSeq);
				
				using(da = new OracleDataAdapter(cmd)){
					da.Fill(oDs);
				}
			}
		}finally{
			conn.Close();
		}
		return oDs;
	}
}
