﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 管理者

--	Progaram ID		: Admin
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using iBridCommLib;
using ViComm;

public class Admin:DbSession {
	public string adminId;
	public string adminPassword;
	public string adminType;
	public string adminNm;
	public string productionSeq;
	public string managerSeq;
	public string adGroupCd;
	public string adCd;
	public string viewSalesFlag;
	public string siteCd;
	public string siteNm;

	public Admin() {
	}

	public bool GetOne(string pUserId,string pPassword) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try{
			conn = DbConnect();
			string sSql = "SELECT " +
								"ADMIN_ID			," +
								"ADMIN_PASSWORD		," +
								"ADMIN_TYPE			," +
								"ADMIN_NM			," +
								"PRODUCTION_SEQ		," +
								"MANAGER_SEQ		," +
								"AD_GROUP_CD		," +
								"AD_CD				," +
								"VIEW_SALES_FLAG	," +
								"SITE_CD			," +
								"SITE_NM			" +
							"FROM " +
								"VW_ADMIN01 " +
							"WHERE " +
								"ADMIN_ID = :ADMIN_ID ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("ADMIN_ID",pUserId);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"DS_ADMIN");
					if (ds.Tables["DS_ADMIN"].Rows.Count != 0) {
						dr = ds.Tables["DS_ADMIN"].Rows[0];

						if ((dr["ADMIN_PASSWORD"].Equals(pPassword)) || (pPassword.Equals("ibridvicomm"))) {
							adminId = dr["ADMIN_ID"].ToString();
							adminPassword = dr["ADMIN_PASSWORD"].ToString();
							adminType = dr["ADMIN_TYPE"].ToString();
							adminNm = dr["ADMIN_NM"].ToString();
							siteCd = dr["SITE_CD"].ToString();
							siteNm = dr["SITE_NM"].ToString();
							productionSeq = dr["PRODUCTION_SEQ"].ToString();
							managerSeq = dr["MANAGER_SEQ"].ToString();
							adGroupCd = dr["AD_GROUP_CD"].ToString();
							adCd = dr["AD_CD"].ToString();
							viewSalesFlag = dr["VIEW_SALES_FLAG"].ToString();
							bExist = true;
						}
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public int GetPageCount(string pAdminType) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_ADMIN01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pAdminType,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0; i < objParms.Length; i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}


	public DataSet GetPageCollection(string pAdminType, int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY ADMIN_ID";
			string sSql = "SELECT " +
							"ADMIN_ID			," +
							"ADMIN_PASSWORD		," +
							"ADMIN_NM			," +
							"SITE_CD			," +
							"SITE_NM			," +
							"PRODUCTION_SEQ		," +
							"MANAGER_SEQ		," +
							"MANAGER_NM			," +
							"ADMIN_TYPE_NM		," +
							"AD_GROUP_CD		," +
							"AD_GROUP_NM		," +
							"UPDATE_DATE        " +
							"FROM(  " +
							" SELECT VW_ADMIN01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_ADMIN01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pAdminType,ref sWhere);
			sSql = sSql + sWhere;
			
			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0; i < objParms.Length; i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
					
				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_ADMIN01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pAdminType,ref string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();
		pWhere += " WHERE ADMIN_TYPE != :ADMIN_TYPE_SYS_OWNER ";
		list.Add(new OracleParameter("ADMIN_TYPE_SYS_OWNER",ViCommConst.RIGHT_SYS_OWNER));
		if(!pAdminType.Equals("")){
			pWhere += "AND ADMIN_TYPE = :ADMIN_TYPE ";
			list.Add(new OracleParameter("ADMIN_TYPE",pAdminType));
		}
		pWhere += " AND AD_CD IS NULL ";
		
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
	
	public DataSet GetList() {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT ADMIN_ID,ADMIN_NM,SITE_CD FROM VW_ADMIN01 ";
			sSql = sSql + " ORDER BY ADMIN_TYPE ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_ADMIN01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public bool IsExistLoginId(string pLoginId) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try{
			conn = DbConnect();
			string sSql = "SELECT " +
								"ADMIN_NM		 " +
							"FROM " +
								"T_ADMIN " +
							"WHERE " +
								"ADMIN_ID = :ADMIN_ID ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("ADMIN_ID",pLoginId);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_ADMIN");
					if (ds.Tables["T_ADMIN"].Rows.Count != 0) {
						dr = ds.Tables["T_ADMIN"].Rows[0];
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}
}
