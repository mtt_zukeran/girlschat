﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: CastDiaryLike
--	Title			: いいね履歴

--	Progaram ID		: CastDiaryLike
--
--  Creation Date	: 2011.04.05
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater   Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Text;
using iBridCommLib;
using ViComm;
using System.Collections.Generic;

/// <summary>
/// CastDiaryLike の概要の説明です

/// </summary>
public class CastDiaryLike : DbSession {
	public CastDiaryLike() {
	}

	public int GetPageCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pReportDay,string pSubSeq,string pLoginId) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_CAST_DIARY_LIKE TCDL, T_USER TU ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pReportDay,pSubSeq,pLoginId,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0; i < objParms.Length; i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pUserSeq,string pUserCharNo,string pReportDay,string pSubSeq,string pLoginId,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = " ORDER BY UPDATE_DATE DESC ";
			string sSql = "SELECT " +
							"TCDL.SITE_CD			        	," +
							"TU.LOGIN_ID                		," +
							"TCDL.UPDATE_DATE			         " +
						  "FROM                                  " +
							" T_CAST_DIARY_LIKE     TCDL        ," +
							" T_USER				TU           ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pReportDay,pSubSeq,pLoginId,ref sWhere);
			sSql += sWhere;
			string sPagingSql = string.Empty;
			OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(sSql,sOrder,startRowIndex,maximumRows,out sPagingSql);


			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				for (int i = 0; i < objParms.Length; i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,string pUserSeq,string pUserCharNo,string pReportDay,string pSubSeq,string pLoginId,ref string pWhere) {
		pWhere = string.Empty;

		ArrayList list = new ArrayList();
		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere(" TCDL.SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!string.IsNullOrEmpty(pUserSeq)) {
			SysPrograms.SqlAppendWhere("TCDL.USER_SEQ = :USER_SEQ",ref pWhere);
			list.Add(new OracleParameter("USER_SEQ",pUserSeq));
			if (!string.IsNullOrEmpty(pUserCharNo)) {
				SysPrograms.SqlAppendWhere("TCDL.USER_CHAR_NO = :USER_CHAR_NO",ref pWhere);
				list.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));
			}
		}
		
		if (!string.IsNullOrEmpty(pReportDay)) {
			SysPrograms.SqlAppendWhere("TCDL.REPORT_DAY = :REPORT_DAY",ref pWhere);
			list.Add(new OracleParameter("REPORT_DAY",pReportDay));
		}
		
		if (!string.IsNullOrEmpty(pSubSeq)) {
			SysPrograms.SqlAppendWhere("TCDL.CAST_DIARY_SUB_SEQ = :CAST_DIARY_SUB_SEQ",ref pWhere);
			list.Add(new OracleParameter("CAST_DIARY_SUB_SEQ",pSubSeq));
		}

		if (!string.IsNullOrEmpty(pLoginId)) {
			SysPrograms.SqlAppendWhere(" TU.LOGIN_ID = :LOGIN_ID",ref pWhere);
			list.Add(new OracleParameter("LOGIN_ID",pLoginId));
		}

		SysPrograms.SqlAppendWhere(" TU.USER_SEQ = TCDL.PARTNER_USER_SEQ ",ref pWhere);

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}
