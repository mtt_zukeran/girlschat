/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ｹﾞｰﾑｷｬﾗｸﾀ

--	Progaram ID		: GameCharacter
--
--  Creation Date	: 2011.07.22
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;
using System.Text;

public class GameCharacter:DbSession {

	public GameCharacter() {
	}

	public bool GetValue(string pSiteCd,string pUserSeq,string pUserCharNo,string pItem,ref string pValue) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect();
			string sSql = "SELECT " +
								"*	" +
							"FROM " +
								"T_GAME_CHARACTER " +
							"WHERE " +
								"SITE_CD		= :SITE_CD		AND	" +
								"USER_SEQ		= :USER_SEQ		AND	" +
								"USER_CHAR_NO	= :USER_CHAR_NO		";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_GAME_CHARACTER");
					if (ds.Tables["T_GAME_CHARACTER"].Rows.Count != 0) {
						dr = ds.Tables["T_GAME_CHARACTER"].Rows[0];
						pValue = dr[pItem].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

    public DataSet GetOne(string pSiteCd, string pUserSeq, string pUserCharNo,string pSexCd) {

        StringBuilder oSqlBuilder = new StringBuilder();

        oSqlBuilder.Append("SELECT                                                                  ").AppendLine();
        oSqlBuilder.Append("    GC.GAME_HANDLE_NM                                               ,   ").AppendLine();
        oSqlBuilder.Append("    GC.GAME_POINT                                                   ,   ").AppendLine();
        oSqlBuilder.Append("    GC.EXP                                                          ,   ").AppendLine();
        oSqlBuilder.Append("    GC.TOTAL_EXP                                                    ,   ").AppendLine();
        oSqlBuilder.Append("    GC.GAME_CHARACTER_LEVEL                                         ,   ").AppendLine();
		oSqlBuilder.Append("    GC.UNASSIGNED_FORCE_COUNT                                       ,   ").AppendLine();
		oSqlBuilder.Append("    GC.MISSION_MAX_FORCE_COUNT                                      ,   ").AppendLine();
		oSqlBuilder.Append("    GC.ATTACK_MAX_FORCE_COUNT                                       ,   ").AppendLine();
		oSqlBuilder.Append("    GC.DEFENCE_MAX_FORCE_COUNT                                      ,   ").AppendLine();
		oSqlBuilder.Append("    GC.COOPERATION_POINT                                            ,   ").AppendLine();
        oSqlBuilder.Append("    GC.STAGE_NM                                                     ,   ").AppendLine();
		oSqlBuilder.Append("    GC.REST_EXP                                                     ,   ").AppendLine();
		oSqlBuilder.Append("    GC.GAME_REGIST_DATE                                             ,   ").AppendLine();
		oSqlBuilder.Append("    GC.CAST_GAME_POINT                                              ,   ").AppendLine();
		oSqlBuilder.Append("    GC.REVISION_NO													,   ").AppendLine();
		oSqlBuilder.Append("    GC.CAST_POINT_REVISION_NO										,   ").AppendLine();
        oSqlBuilder.Append("    CD.CODE_NM                              AS  GAME_CHAR_TYPE_NM       ").AppendLine();
        oSqlBuilder.Append("FROM                                                                    ").AppendLine();
        oSqlBuilder.Append("    VW_GAME_CHARACTER01                     GC                      ,   ").AppendLine();
        oSqlBuilder.Append("    T_CODE_DTL                              CD                          ").AppendLine();
        oSqlBuilder.Append("WHERE                                                                   ").AppendLine();
        oSqlBuilder.Append("    GC.SITE_CD      = :SITE_CD                                      AND ").AppendLine();
        oSqlBuilder.Append("    GC.USER_SEQ     = :USER_SEQ                                     AND ").AppendLine();
        oSqlBuilder.Append("    GC.USER_CHAR_NO = :USER_CHAR_NO                                 AND ").AppendLine();
        oSqlBuilder.Append("    CD.CODE_TYPE    = :CODE_TYPE                                    AND ").AppendLine();
        oSqlBuilder.Append("    CD.CODE         = GC.GAME_CHARACTER_TYPE                            ").AppendLine();

        System.Collections.Generic.List<OracleParameter> oParamList = new System.Collections.Generic.List<OracleParameter>();

        oParamList.Add(new OracleParameter(":SITE_CD"       ,pSiteCd));
        oParamList.Add(new OracleParameter(":USER_SEQ"      ,pUserSeq));
        oParamList.Add(new OracleParameter(":USER_CHAR_NO", pUserCharNo));
        if (pSexCd.Equals(ViCommConst.MAN)) {
            oParamList.Add(new OracleParameter(":CODE_TYPE", "A3"));
        } else {
            oParamList.Add(new OracleParameter(":CODE_TYPE", "A4"));
        }

        return this.ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
    }
}
