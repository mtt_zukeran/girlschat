/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin 
--	Title			: 会員課金追加設定適応条件

--	Progaram ID		: UserChargeAddCondition
--
--  Creation Date	: 2012.05.24
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class UserChargeAddCondition:DbSession {

	public UserChargeAddCondition() {
	}

	public DataSet GetList(string pSiteCd) {
		StringBuilder oSql = new StringBuilder();
		oSql.AppendLine("SELECT									");
		oSql.AppendLine("	SITE_CD							,	");
		oSql.AppendLine("	USER_CHARGE_ADD_CD				,	");
		oSql.AppendLine("	USER_CHARGE_ADD_CD_NM			,	");
		oSql.AppendLine("	LAST_RECEIPT_DATE_START			,	");
		oSql.AppendLine("	LAST_RECEIPT_DATE_END			,	");
		oSql.AppendLine("	LAST_RECEIPT_USE_FLAG			,	");
		oSql.AppendLine("	RECEIPT_SUM_START_DATE			,	");
		oSql.AppendLine("	RECEIPT_SUM_SPECIFIED_MIN		,	");
		oSql.AppendLine("	RECEIPT_SUM_SPECIFIED_MAX		,	");
		oSql.AppendLine("	AF_CONTAIN_SPECIFIED_FLAG		,	");
		oSql.AppendLine("	RECEIPT_SUM_SPECIFIED_USE_FLAG	,	");
		oSql.AppendLine("	RECEIPT_SUM_MIN					,	");
		oSql.AppendLine("	RECEIPT_SUM_MAX					,	");
		oSql.AppendLine("	AF_CONTAIN_FLAG					,	");
		oSql.AppendLine("	RECEIPT_SUM_USE_FLAG			,	");
		oSql.AppendLine("	NA_FLAG							,	");
		oSql.AppendLine("	APPLICATION_DATE				,	");
		oSql.AppendLine("	UPDATE_DATE						,	");
		oSql.AppendLine("	SITE_NM								");
		oSql.AppendLine("FROM									");
		oSql.AppendLine("	VW_USER_CHARGE_ADD_CONDITION01		");
		oSql.AppendLine("WHERE									");
		oSql.AppendLine("	SITE_CD				= :SITE_CD		");
		oSql.AppendLine("ORDER BY SITE_CD,USER_CHARGE_ADD_CD");

		try {
			conn = this.DbConnect();

			using (cmd = this.CreateSelectCommand(oSql.ToString(),conn)) {
				cmd.Parameters.Add(new OracleParameter("SITE_CD",pSiteCd));
				using (da = new OracleDataAdapter(cmd))
				using (DataSet ds = new DataSet()) {
					da.Fill(ds);
					return ds;
				}
			}
		} finally {
			conn.Close();
		}
	}

	public int GetNaCount(string pSiteCd) {
		string sSql = "SELECT COUNT(*) FROM T_USER_CHARGE_ADD_CONDITION WHERE NA_FLAG = 0 AND SITE_CD = :SITE_CD";

		try {
			return this.ExecuteSelectCountQueryBase(sSql,new OracleParameter[] { new OracleParameter("SITE_CD",pSiteCd) });
		} finally {
			conn.Close();
		}
	}

	public int GetUseFlagCount(string pSiteCd, string pUserChargeAddCd) {
		string sSql = "SELECT SUM(LAST_RECEIPT_USE_FLAG+RECEIPT_SUM_SPECIFIED_USE_FLAG+RECEIPT_SUM_USE_FLAG) FROM T_USER_CHARGE_ADD_CONDITION WHERE SITE_CD = :SITE_CD AND USER_CHARGE_ADD_CD = :USER_CHARGE_ADD_CD";

		try {
			return this.ExecuteSelectCountQueryBase(sSql,new OracleParameter[] {
				new OracleParameter("SITE_CD",pSiteCd),
				new OracleParameter("USER_CHARGE_ADD_CD",pUserChargeAddCd)
			});
		} finally {
			conn.Close();
		}
	}
}
