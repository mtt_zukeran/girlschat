﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 掲示板書込
--	Progaram ID		: BbsLog
--
--  Creation Date	: 2010.05.19
--  Creater			: Nakano
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using iBridCommLib;
using ViComm;

/// <summary>
/// 掲示板の書込一覧を取得します。
/// </summary>
public class BbsLog:DbSession {

	public int GetPageCount(string pSiteCd,string pSexCd,string pReportDayFrom,string pReportTimeFrom,string pReportDayTo,string pReportTimeTo,string pTxLoginId,string pTxUserCharNo, string pKeyword) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		try{
			conn = DbConnect();

			string sViewName = pSexCd == ViCommConst.MAN ? "VW_USER_MAN_BBS01" : "VW_CAST_BBS01";

			string sSql = string.Format("SELECT COUNT(*) AS ROW_COUNT FROM {0} ", sViewName);
			string sWhere = "";

			OracleParameter[] oParms = CreateWhere(pSiteCd,pReportDayFrom,pReportTimeFrom,pReportDayTo,pReportTimeTo,pTxLoginId,pTxUserCharNo,pKeyword,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < oParms.Length;i++) {
					cmd.BindByName = true;
					cmd.Parameters.Add(oParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPages = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPages;
	}

	public DataSet GetPageCollection(string pSiteCd,string pSexCd,string pReportDayFrom,string pReportTimeFrom,string pReportDayTo,string pReportTimeTo,string pTxLoginId,string pTxUserCharNo,string pKeyword,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY BBS_SEQ DESC";

			string sSql = "SELECT " +
								"LOGIN_ID			," +
								"HANDLE_NM			," +
								"SITE_CD			," +
								"USER_SEQ			," +
								"USER_CHAR_NO		," +
								"BBS_SEQ			," +
								"BBS_TITLE			," +
								"BBS_DOC			," +
								"CREATE_DATE		," +
								"READING_COUNT		," +
								"DEL_FLAG			," +
								"ADMIN_DEL_FLAG		," +
								"ADMIN_CHECK_FLAG	," +
								string.Format("'{0}' AS SEX_CD,", pSexCd) +
								"RNUM ";

			string sViewName = pSexCd == ViCommConst.MAN ? "VW_USER_MAN_BBS01" : "VW_CAST_BBS01";

			sSql += string.Format("FROM(SELECT {0}.*, ROW_NUMBER() OVER ({1}) AS RNUM FROM {0}  ",sViewName,sOrder);
			string sWhere = "";

			OracleParameter[] oParms = CreateWhere(pSiteCd,pReportDayFrom,pReportTimeFrom,pReportDayTo,pReportTimeTo,pTxLoginId,pTxUserCharNo,pKeyword,ref sWhere);

			sSql = sSql + sWhere;
			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;
			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.BindByName = true;
				for (int i = 0;i < oParms.Length;i++) {
					cmd.Parameters.Add(oParms[i]);
				}
				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,sViewName);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,string pReportDayFrom,string pReportTimeFrom,string pReportDayTo,string pReportTimeTo,string pTxLoginId,string pTxUserCharNo,string pKeyword,ref string pWhere) {
		SetDefaultTimeValue(ref pReportTimeFrom,ref pReportTimeTo);
		ArrayList list = new ArrayList();

		if (pSiteCd.Equals("") == false) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if ((!pReportDayFrom.Equals("")) && (!pReportDayTo.Equals(""))) {
			DateTime dtFrom = DateTime.Parse(pReportDayFrom + " " + pReportTimeFrom + ":00:00");
			DateTime dtTo = DateTime.Parse(pReportDayTo + " " + pReportTimeTo + ":59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere("CREATE_DATE >= :CREATE_DATE_FROM AND CREATE_DATE < :CREATE_DATE_TO",ref pWhere);
			list.Add(new OracleParameter("CREATE_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("CREATE_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (pTxLoginId.Equals("") == false) {
			SysPrograms.SqlAppendWhere("LOGIN_ID LIKE :LOGIN_ID||'%'",ref pWhere);
			list.Add(new OracleParameter("LOGIN_ID",pTxLoginId));

			if (pTxUserCharNo.Equals("") == false) {
				SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhere);
				list.Add(new OracleParameter("USER_CHAR_NO",pTxUserCharNo));
			}
		}

		if (!string.IsNullOrEmpty(pKeyword)) {
			StringBuilder oWhereKeywords = new StringBuilder("(");
			string[] sKeywordArray = pKeyword.Split(' ','　');
			for (int i = 0; i < sKeywordArray.Length; i++) {
				oWhereKeywords.AppendFormat("(BBS_TITLE LIKE :KEYWORD{0}) OR (BBS_DOC LIKE :KEYWORD{0})",i);
				list.Add(new OracleParameter(string.Format("KEYWORD{0}",i),string.Format("%{0}%",sKeywordArray[i])));

				if (i < sKeywordArray.Length - 1) {
					oWhereKeywords.AppendLine(" AND ");
				}
			}
			oWhereKeywords.Append(")");
			SysPrograms.SqlAppendWhere(oWhereKeywords.ToString(),ref pWhere);
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	private void SetDefaultTimeValue(ref string pTimeFrom,ref string pTimeTo) {
		if (string.IsNullOrEmpty(pTimeFrom)) {
			pTimeFrom = "00";
		}
		if (string.IsNullOrEmpty(pTimeTo)) {
			pTimeTo = "23";
		}
	}

	public void DeleteBbs(string pBbsSeq,string pSexCd,int pDelFlag) {
		string[] bbsSeqList = new string[] { pBbsSeq };
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("DELETE_BBS");
			db.ProcedureInArrayParm("PBBS_SEQ",DbType.VARCHAR2,bbsSeqList.Length,bbsSeqList);
			db.ProcedureInParm("PBBS_SEQ_COUNT",DbType.NUMBER,bbsSeqList.Length);
			db.ProcedureInParm("PSEX_CD",DbType.VARCHAR2,pSexCd);
			db.ProcedureInParm("PDEL_FLAG",DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	public int GetUnconfirmCount(string pSiteCd,string pSexCd,string pReportDayFrom) {
		int iValue;
		StringBuilder oSqlBuilder = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		string sTableName = pSexCd == ViCommConst.MAN ? "T_USER_MAN_BBS" : "T_CAST_BBS";
		
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendFormat("	{0}",sTableName).AppendLine();
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD			AND	");
		oSqlBuilder.AppendLine("	ADMIN_CHECK_FLAG	= :ADMIN_CHECK_FLAG	AND	");
		oSqlBuilder.AppendLine("	CREATE_DATE			>= :CREATE_DATE");
		
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":ADMIN_CHECK_FLAG",ViCommConst.FLAG_OFF));
		DateTime dtFrom = DateTime.Parse(pReportDayFrom);
		oParamList.Add(new OracleParameter("CREATE_DATE",OracleDbType.Date,dtFrom,ParameterDirection.Input));

		iValue = ExecuteSelectCountQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		return iValue;
	}
}
