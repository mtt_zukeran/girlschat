﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: ステージ設定--	Progaram ID		: Stage
--
--  Creation Date	: 2011.07.19
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;


public class Stage : DbSession {

	public Stage() {
	}

	public int GetPageCount(string pSiteCd,string pSexCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT COUNT(*) FROM T_STAGE WHERE SITE_CD = :SITE_CD AND SEX_CD = :SEX_CD");

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":SEX_CD",pSexCd);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		}
		finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pSexCd,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT					");
		oSqlBuilder.AppendLine("	SITE_CD				,");
		oSqlBuilder.AppendLine("	STAGE_SEQ			,");
		oSqlBuilder.AppendLine("	STAGE_NM			,");
		oSqlBuilder.AppendLine("	STAGE_LEVEL			,");
		oSqlBuilder.AppendLine("	SEX_CD				,");
		oSqlBuilder.AppendLine("	BOSS_NM				,");
		oSqlBuilder.AppendLine("	BOSS_DESCRIPTION	,");
		oSqlBuilder.AppendLine("	EXP					,");
		oSqlBuilder.AppendLine("	INCOME_MIN			,");
		oSqlBuilder.AppendLine("	INCOME_MAX			,");
		oSqlBuilder.AppendLine("	HONOR				,");
		oSqlBuilder.AppendLine("	BOSS_HP_FRIEND_1	,");
		oSqlBuilder.AppendLine("	BOSS_HP_FRIEND_2	,");
		oSqlBuilder.AppendLine("	BOSS_HP_FRIEND_3	,");
		oSqlBuilder.AppendLine("	BOSS_HP				");
		oSqlBuilder.AppendLine("FROM					");
		oSqlBuilder.AppendLine("	T_STAGE				");
		oSqlBuilder.AppendLine("WHERE					");
		oSqlBuilder.AppendLine("	SITE_CD	= :SITE_CD	AND");
		oSqlBuilder.AppendLine("	SEX_CD	= :SEX_CD	");

		string sPagingSql = string.Empty;
		string sSortExpression = "ORDER BY SITE_CD, STAGE_LEVEL";

		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":SEX_CD",pSexCd);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		}
		finally {
			conn.Close();
		}
		return oDs;
	}

	public DataSet GetOne(string pSiteCd,string pStageSeq,string pSexCd) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT					");
		oSqlBuilder.AppendLine("	SITE_CD				,");
		oSqlBuilder.AppendLine("	STAGE_SEQ			,");
		oSqlBuilder.AppendLine("	STAGE_NM			,");
		oSqlBuilder.AppendLine("	STAGE_LEVEL			,");
		oSqlBuilder.AppendLine("	SEX_CD				,");
		oSqlBuilder.AppendLine("	BOSS_NM				,");
		oSqlBuilder.AppendLine("	BOSS_DESCRIPTION	,");
		oSqlBuilder.AppendLine("	EXP					,");
		oSqlBuilder.AppendLine("	INCOME_MIN			,");
		oSqlBuilder.AppendLine("	INCOME_MAX			,");
		oSqlBuilder.AppendLine("	HONOR				,");
		oSqlBuilder.AppendLine("	BOSS_HP_FRIEND_1	,");
		oSqlBuilder.AppendLine("	BOSS_HP_FRIEND_2	,");
		oSqlBuilder.AppendLine("	BOSS_HP_FRIEND_3	");
		oSqlBuilder.AppendLine("FROM					");
		oSqlBuilder.AppendLine("	T_STAGE				");
		oSqlBuilder.AppendLine("WHERE					");
		oSqlBuilder.AppendLine("	SITE_CD		= :SITE_CD		AND");
		oSqlBuilder.AppendLine("	STAGE_SEQ	= :STAGE_SEQ	AND");
		oSqlBuilder.AppendLine("	SEX_CD		= :SEX_CD	");

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":SEX_CD",pSexCd);
				cmd.Parameters.Add(":STAGE_SEQ",pStageSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		}
		finally {
			conn.Close();
		}
		return oDs;
	}

	public DataSet GetList(string pSiteCd,string pSexCd) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT					");
		oSqlBuilder.AppendLine("	SITE_CD				,");
		oSqlBuilder.AppendLine("	STAGE_SEQ			,");
		oSqlBuilder.AppendLine("	STAGE_NM			,");
		oSqlBuilder.AppendLine("	STAGE_LEVEL			,");
		oSqlBuilder.AppendLine("	STAGE_NM || ' (' || STAGE_LEVEL || ')' STAGE_DISPLAY	");
		oSqlBuilder.AppendLine("FROM					");
		oSqlBuilder.AppendLine("	T_STAGE				");
		oSqlBuilder.AppendLine("WHERE					");
		oSqlBuilder.AppendLine("	SITE_CD		= :SITE_CD		AND");
		oSqlBuilder.AppendLine("	SEX_CD		= :SEX_CD	");
		oSqlBuilder.AppendLine("ORDER BY SITE_CD, STAGE_LEVEL	");

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":SEX_CD",pSexCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		}
		finally {
			conn.Close();
		}
		return oDs;
	}

	public DataSet GetLevelList(string pSiteCd,string pSexCd) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT					");
		oSqlBuilder.AppendLine("	SITE_CD				,");
		oSqlBuilder.AppendLine("	STAGE_SEQ			,");
		oSqlBuilder.AppendLine("	STAGE_NM			,");
		oSqlBuilder.AppendLine("	STAGE_LEVEL			,");
		oSqlBuilder.AppendLine("	STAGE_LEVEL || ' : ' || STAGE_NM STAGE_DISPLAY	");
		oSqlBuilder.AppendLine("FROM					");
		oSqlBuilder.AppendLine("	T_STAGE				");
		oSqlBuilder.AppendLine("WHERE					");
		oSqlBuilder.AppendLine("	SITE_CD		= :SITE_CD		AND");
		oSqlBuilder.AppendLine("	SEX_CD		= :SEX_CD	");
		oSqlBuilder.AppendLine("ORDER BY SITE_CD, STAGE_LEVEL	");

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":SEX_CD",pSexCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}

}