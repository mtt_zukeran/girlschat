﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 美人時計写真
--	Progaram ID		: BeautyClock
--
--  Creation Date	: 2011.02.22
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------
  Date			Updater			Update Explain
-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;
using ViComm;

/// <summary>
/// BeautyClockMainte の概要の説明です
/// </summary>
public class BeautyClock:DbSession {
	public BeautyClock() {
		//
		// TODO: コンストラクタ ロジックをここに追加します
		//
	}


	public decimal GetPicNo() {

		decimal dPicNo = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT SEQ_OBJ.NEXTVAL FROM DUAL";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				dPicNo = (decimal)cmd.ExecuteScalar();
			}
		} finally {
			conn.Close();
		}

		return dPicNo;
	}

	public DataSet GetList(string pSiteCd){
		string sOrderBy = "ORDER BY MOD(VW_BEAUTY_CLOCK01.ASSIGNED_TIME,60)	ASC ,VW_BEAUTY_CLOCK01.ASSIGNED_TIME	ASC";
		return this.GetListBase(pSiteCd,string.Empty,ViCommConst.FLAG_ON,sOrderBy);
	}

	public DataSet GetListByAssignedTime(string pSiteCd,string pAssignedTime) {
		string sOrderBy = "ORDER BY VW_BEAUTY_CLOCK01.PUBLISH_FLAG DESC , VW_BEAUTY_CLOCK01.UPDATE_DATE DESC";
		return this.GetListBase(pSiteCd,pAssignedTime,null,sOrderBy);
	}

	private DataSet GetListBase(string pSiteCd,string pAssignedTime,int? pPublishFlag, string sOrderBy) {

		DataSet ds;
	
		try {
			conn = DbConnect();
			ds = new DataSet();

			StringBuilder sSql = new StringBuilder();

			string sWhere = string.Empty;
			OracleParameter[] objParms = CreateWhere(pSiteCd,pAssignedTime,pPublishFlag,ref sWhere);
			
			sSql.Append("SELECT																			  ").AppendLine();
			sSql.Append("  ASSIGNED_TIME																, ").AppendLine();
			sSql.Append("  PUBLISH_FLAG																	, ").AppendLine();
			sSql.Append("  UPDATE_DATE																	, ").AppendLine();
			sSql.Append("  HANDLE_NM																	, ").AppendLine();
			sSql.Append("  LOGIN_ID																		, ").AppendLine();
			sSql.Append("  USER_SEQ																		, ").AppendLine();
			sSql.Append("  PIC_SEQ																		, ").AppendLine();
			sSql.Append("  USER_STATUS																	, ").AppendLine();
			sSql.Append("  NA_FLAG																		, ").AppendLine();
			sSql.Append("  GET_BEAUTY_CLOCK_IMG_PATH(SITE_CD,ASSIGNED_TIME,PIC_SEQ) AS BEAUTY_IMG_PATH	  ").AppendLine();
			sSql.Append("FROM																			  ").AppendLine();
			sSql.Append("  VW_BEAUTY_CLOCK01															  ").AppendLine();
			
			sSql.AppendFormat("{0}",sWhere).AppendLine();						
			sSql.AppendFormat("{0}",sOrderBy).AppendLine();
			
			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				cmd.BindByName = true;
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Clone();
		}

		return ds;
	}
	private OracleParameter[] CreateWhere(string pSiteCd,string pAssignedTime,int? pPublishFlag,ref string pWhere) {

		pWhere = string.Empty;
		ArrayList list = new ArrayList();


		if (!pSiteCd.Equals(string.Empty)) {
			SysPrograms.SqlAppendWhere("VW_BEAUTY_CLOCK01.SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter(":SITE_CD",pSiteCd));
		}
		if (!pAssignedTime.Equals(string.Empty)) {
			SysPrograms.SqlAppendWhere("VW_BEAUTY_CLOCK01.ASSIGNED_TIME = :ASSIGNED_TIME",ref pWhere);
			list.Add(new OracleParameter(":ASSIGNED_TIME",pAssignedTime));
		}
		if (pPublishFlag != null) {
			SysPrograms.SqlAppendWhere("VW_BEAUTY_CLOCK01.PUBLISH_FLAG = :PUBLISH_FLAG",ref pWhere);
			list.Add(new OracleParameter(":PUBLISH_FLAG",pPublishFlag));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}
