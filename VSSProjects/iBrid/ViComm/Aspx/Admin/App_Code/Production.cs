﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: プロダクション
--	Progaram ID		: Production
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class Production:DbSession {

	public Production() {
	}

	public int GetPageCount() {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_PRODUCTION ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY PRODUCTION_SEQ";
			string sSql = "SELECT " +
							"PRODUCTION_SEQ	," +
							"PRODUCTION_NM	," +
							"TEL			," +
							"EMAIL_ADDR		," +
							"REVISION_NO	," +
							"UPDATE_DATE " +
							"FROM(" +
							" SELECT T_PRODUCTION.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM T_PRODUCTION  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_PRODUCTION");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(ref string pWhere) {
		ArrayList list = new ArrayList();
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}


	public DataSet GetList() {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT PRODUCTION_SEQ,PRODUCTION_NM FROM T_PRODUCTION ";
			sSql = sSql + " ORDER BY PRODUCTION_SEQ ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_PRODUCTION");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public bool GetValue(string pProductionSeq,string pItem,ref string pValue) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		pValue = "";
		try{
			conn = DbConnect();
			string sSql = "SELECT " +
								"PRODUCTION_NM		 " +
							"FROM " +
								"T_PRODUCTION " +
							"WHERE " +
								"PRODUCTION_SEQ = :PRODUCTION_SEQ ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("PRODUCTION_SEQ",pProductionSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_PRODUCTION");
					if (ds.Tables["T_PRODUCTION"].Rows.Count != 0) {
						dr = ds.Tables["T_PRODUCTION"].Rows[0];
						pValue = dr[pItem].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}
}
