﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: RankingEx
--	Title			: ランキング集計拡張

--	Progaram ID		: RankingEx
--
--  Creation Date	: 2011.04.05
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater   Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Text;
using iBridCommLib;
using ViComm;

public class RankingEx:DbSession {

	public RankingEx() {
	}

	public DataSet GetPageCollection(string pSiteCd, string pSummaryType, string pRankingCtlSeq, int startRowIndex, int maximumRows) {
		
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sViewName = this.GetViewName(pSummaryType);
			if (string.IsNullOrEmpty(sViewName)) {
				return ds;
			}
			StringBuilder oSql = new StringBuilder();

			string sOrder = "ORDER BY SITE_CD,SUMMARY_COUNT DESC,LAST_LOGIN_DATE DESC,USER_SEQ ";
			oSql.AppendLine("SELECT ");
			oSql.AppendLine("	SITE_CD				,	");
			oSql.AppendLine("	SUMMARY_TYPE		,	");
			oSql.AppendLine("	USER_SEQ			,	");
			oSql.AppendLine("	USER_CHAR_NO		,	");
			oSql.AppendLine("	HANDLE_NM			,	");
			oSql.AppendLine("	LOGIN_ID			,	");
			oSql.AppendLine("	RANKING_CTL_SEQ		,	");
			oSql.AppendLine("	SUMMARY_COUNT		,	");
			oSql.AppendLine("	RNUM					");
			oSql.AppendLine("FROM(");
			oSql.AppendFormat(" SELECT {0}.*, ROW_NUMBER() OVER ({1}) AS RNUM FROM {0} ", sViewName, sOrder).AppendLine();

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pSummaryType,pRankingCtlSeq,ref sWhere);
			oSql.AppendLine(sWhere);

			oSql.AppendLine(")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ");
			oSql.AppendLine(sOrder);

			using (cmd = CreateSelectCommand(oSql.ToString(),conn)) {
				cmd.BindByName = true;
				for (int i = 0; i < objParms.Length; i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds, sViewName);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd, string pSummaryType, string pRankingCtlSeq, ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		SysPrograms.SqlAppendWhere(" SITE_CD = :SITE_CD ",ref pWhere);
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		SysPrograms.SqlAppendWhere(" SUMMARY_TYPE = :SUMMARY_TYPE ", ref pWhere);
		list.Add(new OracleParameter("SUMMARY_TYPE", pSummaryType));

		if (!pRankingCtlSeq.Equals(string.Empty)) {
			SysPrograms.SqlAppendWhere(" RANKING_CTL_SEQ = :RANKING_CTL_SEQ ",ref pWhere);
			list.Add(new OracleParameter("RANKING_CTL_SEQ",pRankingCtlSeq));
		}
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public int GetPageCount(string pSiteCd, string pSummaryType, string pRankingCtlSeq) {

		int iPageCount = 0;
		try {
			conn = DbConnect();
			DataSet ds;
			string sViewName = this.GetViewName(pSummaryType);
			if (string.IsNullOrEmpty(sViewName)) {
				return 0;
			}

			StringBuilder sSql = new StringBuilder();

			sSql.AppendLine("SELECT ");
			sSql.AppendLine("   COUNT(*) AS ROW_COUNT ");
			sSql.AppendLine("FROM ");
			sSql.AppendFormat("   {0} ", this.GetViewName(pSummaryType)).AppendLine();
			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pSummaryType,pRankingCtlSeq,ref sWhere);
			sSql.AppendLine(sWhere);

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					using (ds = new DataSet()) {
						for (int i = 0;i < objParms.Length;i++) {
							cmd.Parameters.Add((OracleParameter)objParms[i]);
						}
						da.Fill(ds);
						iPageCount = Convert.ToInt32(ds.Tables[0].Rows[0]["ROW_COUNT"]);
					}
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	private string GetViewName(string pSummaryType) {
		string sViewName = string.Empty;
		switch (pSummaryType) {
			case ViCommConst.ExRanking.EX_RANKING_MARKING:
			case ViCommConst.ExRanking.EX_RANKING_FAVORIT_WOMAN:
				sViewName = "VW_RANKING_EX01";
				break;
			case ViCommConst.ExRanking.EX_RANKING_FAVORIT_MAN:
				sViewName = "VW_RANKING_EX02";
				break;
		}

		return sViewName;
	}
}
