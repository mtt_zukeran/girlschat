﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 街設定--	Progaram ID		: Town
--
--  Creation Date	: 2011.07.19
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;


public class Town : DbSession {

	public Town() {
	}

	public int GetPageCount(string pSiteCd,string pStageSeq,string pSexCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT COUNT(*) FROM T_TOWN");

		string sWhereClause;
		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pStageSeq,pSexCd,out sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pStageSeq,string pSexCd,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT					");
		oSqlBuilder.AppendLine("	SITE_CD				,");
		oSqlBuilder.AppendLine("	STAGE_SEQ			,");
		oSqlBuilder.AppendLine("	TOWN_SEQ			,");
		oSqlBuilder.AppendLine("	TOWN_NM				,");
		oSqlBuilder.AppendLine("	TOWN_NM_EXTRA		,");
		oSqlBuilder.AppendLine("	TOWN_LEVEL			,");
		oSqlBuilder.AppendLine("	EXP					,");
		oSqlBuilder.AppendLine("	LOSS_FORCE_COUNT	,");
		oSqlBuilder.AppendLine("	INCOME_MIN			,");
		oSqlBuilder.AppendLine("	INCOME_MAX			,");
		oSqlBuilder.AppendLine("	TARGET_COUNT		");
		oSqlBuilder.AppendLine("FROM					");
		oSqlBuilder.AppendLine("	T_TOWN				");

		string sWhereClause;
		string sPagingSql;
		string sSortExpression = "ORDER BY SITE_CD, TOWN_LEVEL";

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pStageSeq,pSexCd,out sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,string pStageSeq,string pSexCd,out string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}
		if (!string.IsNullOrEmpty(pStageSeq)) {
			SysPrograms.SqlAppendWhere("STAGE_SEQ = :STAGE_SEQ",ref pWhere);
			list.Add(new OracleParameter("STAGE_SEQ",pStageSeq));
		}
		if (!string.IsNullOrEmpty(pSexCd)) {
			SysPrograms.SqlAppendWhere("SEX_CD = :SEX_CD",ref pWhere);
			list.Add(new OracleParameter("SEX_CD",pSexCd));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public DataSet GetLevelList(string pSiteCd,string pStageSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT					");
		oSqlBuilder.AppendLine("	SITE_CD				,");
		oSqlBuilder.AppendLine("	STAGE_SEQ			,");
		oSqlBuilder.AppendLine("	TOWN_SEQ			,");
		oSqlBuilder.AppendLine("	TOWN_NM				,");
		oSqlBuilder.AppendLine("	TOWN_LEVEL			,");
		oSqlBuilder.AppendLine("	TOWN_LEVEL || ' : ' || TOWN_NM TOWN_DISPLAY	");
		oSqlBuilder.AppendLine("FROM					");
		oSqlBuilder.AppendLine("	T_TOWN				");
		oSqlBuilder.AppendLine("WHERE					");
		oSqlBuilder.AppendLine("	SITE_CD		= :SITE_CD		AND");
		oSqlBuilder.AppendLine("	STAGE_SEQ	= :STAGE_SEQ	");
		oSqlBuilder.AppendLine("ORDER BY SITE_CD, TOWN_LEVEL	");

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":STAGE_SEQ",pStageSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}
}