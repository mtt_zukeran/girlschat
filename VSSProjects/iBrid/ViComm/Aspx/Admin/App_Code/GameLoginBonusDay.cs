﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 日別ゲームログインボーナス
--	Progaram ID		: GameLoginBonusDay
--
--  Creation Date	: 2011.07.19
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;


public class GameLoginBonusDay : DbSession {

	public GameLoginBonusDay() {
	}

	public int GetPageCount(string pSiteCd,string pSexCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	COUNT(*) ");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_GAME_LOGIN_BONUS_DAY	BD,");
		oSqlBuilder.AppendLine("	T_GAME_ITEM				GI");

		string sWhereClause;
		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pSexCd,out sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pSexCd,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	BD.SITE_CD						,");
		oSqlBuilder.AppendLine("	BD.GAME_LOGIN_BONUS_SEQ			,");
		oSqlBuilder.AppendLine("	BD.GAME_LOGIN_BONUS_START_DATE	,");
		oSqlBuilder.AppendLine("	BD.GAME_LOGIN_BONUS_END_DATE	,");
		oSqlBuilder.AppendLine("	BD.SEX_CD						,");
		oSqlBuilder.AppendLine("	BD.GAME_ITEM_SEQ				,");
		oSqlBuilder.AppendLine("	GI.GAME_ITEM_NM					,");
		oSqlBuilder.AppendLine("	BD.ITEM_COUNT					,");
		oSqlBuilder.AppendLine("	BD.GAME_POINT					,");
		oSqlBuilder.AppendLine("	BD.REVISION_NO					,");
		oSqlBuilder.AppendLine("	BD.UPDATE_DATE					");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_GAME_LOGIN_BONUS_DAY	BD,");
		oSqlBuilder.AppendLine("	T_GAME_ITEM				GI");

		string sWhereClause;
		string sPagingSql;
		string sSortExpression = "ORDER BY BD.SITE_CD, BD.GAME_LOGIN_BONUS_START_DATE DESC ";

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pSexCd,out sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pSexCd,out string pWhere) {
		pWhere = "";
		SysPrograms.SqlAppendWhere("BD.SITE_CD = GI.SITE_CD (+) AND BD.SEX_CD = GI.SEX_CD (+) AND BD.GAME_ITEM_SEQ = GI.GAME_ITEM_SEQ (+) ",ref pWhere);

		ArrayList list = new ArrayList();
		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("BD.SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}
		if (!string.IsNullOrEmpty(pSexCd)) {
			SysPrograms.SqlAppendWhere("BD.SEX_CD = :SEX_CD",ref pWhere);
			list.Add(new OracleParameter("SEX_CD",pSexCd));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

/*
	public bool IsDupulicateItem(string pSiteCd,string pGameLoginBonusDay,string pGameItemSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_GAME_LOGIN_BONUS_DAY	");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	SITE_CD					= :SITE_CD				AND");
		oSqlBuilder.AppendLine("	GAME_LOGIN_BONUS_DAY	= :GAME_LOGIN_BONUS_DAY	AND");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ			= :GAME_ITEM_SEQ	");

		try {
			int iCount = 0;
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(new OracleParameter("SITE_CD",pSiteCd));
				cmd.Parameters.Add(new OracleParameter("GAME_LOGIN_BONUS_DAY",pGameLoginBonusDay));
				cmd.Parameters.Add(new OracleParameter("GAME_ITEM_SEQ",pGameItemSeq));
				iCount = Convert.ToInt32(cmd.ExecuteScalar());
				return iCount > 1;
			}
		} finally {
			conn.Close();
		}
	}
 */
}
