﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 共有ライブトーク
--	Progaram ID		: ShareLiveTalk
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class ShareLiveTalk:DbSession {

	public ShareLiveTalk() {
	}



	public int GetPageCount(string pReportDayFrom,string pReportDayTo) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_SHARE_LIVE_TALK01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pReportDayFrom,pReportDayTo,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}


	public DataSet GetPageCollection(string pReportDayFrom,string pReportDayTo,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY START_DATE DESC ";
			ds = new DataSet();

			string sSql = "SELECT " +
								"LOGIN_ID				," +
								"USER_SEQ				," +
								"SHARE_LIVE_TALK_KEY	," +
								"LIVE_NM				," +
								"START_DATE				," +
								"END_DATE				," +
								"UPDATE_DATE			," +
								"REVISION_NO			," +
								"CAST_NM				" +
							"FROM(" +
							" SELECT VW_SHARE_LIVE_TALK01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_SHARE_LIVE_TALK01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pReportDayFrom,pReportDayTo,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_SHARE_LIVE_TALK01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(string pReportDayFrom,string pReportDayTo,ref string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();
		pWhere = pWhere + " WHERE START_DATE >= :START_DATE_FROM AND START_DATE < :START_DATE_TO ";

		DateTime dtFrom = System.DateTime.ParseExact(pReportDayFrom + " 00:00:00","yyyy/MM/dd HH:mm:ss",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);
		DateTime dtTo = System.DateTime.ParseExact(pReportDayTo + " 00:00:00","yyyy/MM/dd HH:mm:ss",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);
		dtTo.AddDays(1);

		list.Add(new OracleParameter("START_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
		list.Add(new OracleParameter("START_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}
