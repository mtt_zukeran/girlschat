﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: ドコモケータイ送金


--	Progaram ID		: DocomoTransfer
--  Creation Date	: 2011.09.27
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;

public class DocomoTransfer : DbSession {
	public DocomoTransfer() {
	}

	public DataSet GetList(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	SITE_CD						,");
		oSqlBuilder.AppendLine("	DOCOMO_MONEY_TRANSFER_TEL	,");
		oSqlBuilder.AppendLine("	DOCOMO_MONEY_TRANSFER_NM	,");
		oSqlBuilder.AppendLine("	REMARKS						,");
		oSqlBuilder.AppendLine("	USED_FLAG					,");
		oSqlBuilder.AppendLine("	APPLICATION_DATE			,");
		oSqlBuilder.AppendLine("	UPDATE_DATE					,");
		oSqlBuilder.AppendLine("	REVISION_NO	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_DOCOMO_TRANSFER	");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	SITE_CD		= :SITE_CD	");
		oSqlBuilder.AppendLine("ORDER BY	");
		oSqlBuilder.AppendLine("	SITE_CD, DOCOMO_MONEY_TRANSFER_TEL ");
		DataSet oDataSet = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
				}
			}
		} finally {
			conn.Close();
		}
		return oDataSet;
	}

	public bool IsDuplicate(string pSiteCd,string pDocomoMoneyTransferTel) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT												").AppendLine();
		oSqlBuilder.Append(" 	COUNT(*)										").AppendLine();
		oSqlBuilder.Append(" FROM												").AppendLine();
		oSqlBuilder.Append(" 	T_DOCOMO_TRANSFER								").AppendLine();
		oSqlBuilder.Append(" WHERE												").AppendLine();
		oSqlBuilder.Append(" 	SITE_CD						= :SITE_CD					AND	").AppendLine();
		oSqlBuilder.Append(" 	DOCOMO_MONEY_TRANSFER_TEL	= :DOCOMO_MONEY_TRANSFER_TEL	").AppendLine();

		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":DOCOMO_MONEY_TRANSFER_TEL",pDocomoMoneyTransferTel);

				int iCount = int.Parse(cmd.ExecuteScalar().ToString());
				return iCount > 0;
			}
		} finally {
			conn.Close();
		}
	}
}
