﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 入金
--	Progaram ID		: Receipt
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class Receipt:DbSession {

	public Receipt() {
	}

	public int GetPageCount(string pSiteCd,string pUserSeq,string pReportDayFrom,string pReportDayTo,string pSettleType,string pLoginId,string pTel,string pAdCd,string pReceiptCancelFlag,bool pWithoutCredit0Yen,bool pWithoutCredit300Yen) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_RECEIPT01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pReportDayFrom,pReportDayTo,pSettleType,pLoginId,pTel,pAdCd,pReceiptCancelFlag,pWithoutCredit0Yen,pWithoutCredit300Yen,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pUserSeq,string pReportDayFrom,string pReportDayTo,string pSettleType,string pLoginId,string pTel,string pAdCd,string pReceiptCancelFlag,bool pWithoutCredit0Yen,bool pWithoutCredit300Yen,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY SITE_CD,CREATE_DATE DESC,RECEIPT_SEQ DESC";
			string sSql = "SELECT " +
							"SITE_CD				," +
							"SITE_NM				," +
							"USER_SEQ				," +
							"SETTLE_TYPE			," +
							"SETTLE_TYPE_NM			," +
							"CREATE_DATE			," +
							"RECEIPT_SEQ			," +
							"RECEIPT_AMT			," +
							"RECEIPT_PERSON			," +
							"RECEIPT_CANCEL_FLAG	," +
							"BEFORE_RECEIPT_BILL_AMT," +
							"AFTER_RECEIPT_BILL_AMT	," +
							"EX_POINT				," +
							"SERVICE_POINT			," +
							"BEFORE_RECEIPT_POINT	," +
							"AFTER_RECEIPT_POINT	," +
							"LOGIN_ID				," +
							"TEL					," +
							"EMAIL_ADDR				," +
							"AD_CD					," +
							"BAL_POINT				," +
							"TOTAL_RECEIPT_AMT		," +
							"TOTAL_RECEIPT_COUNT	," +
							"MONTHLY_RECEIPT_AMT	," +
							"MONTHLY_RECEIPT_COUNT	," +
							"CHARACTER_ONLINE_STATUS," +
							"REGIST_DATE			," +
							"FIRST_RECEIPT_DAY		 " +
						"FROM(" +
							" SELECT VW_RECEIPT01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_RECEIPT01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pReportDayFrom,pReportDayTo,pSettleType,pLoginId,pTel,pAdCd,pReceiptCancelFlag,pWithoutCredit0Yen,pWithoutCredit300Yen,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_RECEIPT01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public void GetTotal(string pSiteCd,string pUserSeq,string pReportDayFrom,string pReportDayTo,string pSettleType,string pLoginId,string pTel,string pAdCd,bool pWithoutCredit0Yen,bool pWithoutCredit300Yen,out int pCount,out int pAmt) {
		DataSet ds;
		DataRow dr;
		pCount = 0;
		pAmt = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT NVL(SUM(RECEIPT_AMT),0) AS AMT,NVL(COUNT(*),0) AS CNT FROM VW_RECEIPT01";
			string sWhere = "";

			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pReportDayFrom,pReportDayTo,pSettleType,pLoginId,pTel,pAdCd,ViComm.ViCommConst.FLAG_OFF_STR,pWithoutCredit0Yen,pWithoutCredit300Yen,ref sWhere);

			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					pAmt = int.Parse(dr["AMT"].ToString());
					pCount = int.Parse(dr["CNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pUserSeq,string pReportDayFrom,string pReportDayTo,string pSettleType,string pLoginId,string pTel,string pAdCd,string pReceiptCancelFlag,bool pWithoutCredit0Yen,bool pWithoutCredit300Yen,ref string pWhere) {
		ArrayList list = new ArrayList();
		if(!pSiteCd.Equals("")){
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!pUserSeq.Equals("")) {
			SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhere);
			list.Add(new OracleParameter("USER_SEQ",pUserSeq));
		}

		if (!string.IsNullOrEmpty(pSettleType)) {
			SysPrograms.SqlAppendWhere("SETTLE_TYPE = :SETTLE_TYPE",ref pWhere);
			list.Add(new OracleParameter("SETTLE_TYPE",pSettleType));
		}

		if ((!pReportDayFrom.Equals("")) && (!pReportDayTo.Equals(""))) {
			DateTime dtFrom = DateTime.Parse(pReportDayFrom);
			DateTime dtTo = DateTime.Parse(pReportDayTo).AddDays(1);

			SysPrograms.SqlAppendWhere("(CREATE_DATE >= :CREATE_DATE_FROM AND CREATE_DATE < :CREATE_DATE_TO)",ref pWhere);
			list.Add(new OracleParameter("CREATE_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("CREATE_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (!pLoginId.Equals("")) {
			SysPrograms.SqlAppendWhere("LOGIN_ID LIKE :LOGIN_ID||'%'",ref pWhere);
			list.Add(new OracleParameter("LOGIN_ID",pLoginId));
		}

		if (!pTel.Equals("")) {
			SysPrograms.SqlAppendWhere("TEL LIKE :TEL||'%'",ref pWhere);
			list.Add(new OracleParameter("TEL",pTel));
		}

		if (!pAdCd.Equals("")) {
			SysPrograms.SqlAppendWhere("AD_CD =: AD_CD",ref pWhere);
			list.Add(new OracleParameter("AD_CD",pAdCd));
		}

		if (!string.IsNullOrEmpty(pReceiptCancelFlag)) {
			SysPrograms.SqlAppendWhere("RECEIPT_CANCEL_FLAG = :RECEIPT_CANCEL_FLAG",ref pWhere);
			list.Add(new OracleParameter("RECEIPT_CANCEL_FLAG",pReceiptCancelFlag));
		}

		if (pWithoutCredit0Yen) {
			SysPrograms.SqlAppendWhere(string.Format("SETTLE_TYPE != '{0}'",ViCommConst.SETTLE_CREDIT_AUTH),ref pWhere);
		}

		if (pWithoutCredit300Yen) {
			SysPrograms.SqlAppendWhere(string.Format("NOT(SETTLE_TYPE = '{0}' AND RECEIPT_AMT <= 324)",ViCommConst.SETTLE_CREDIT_PACK),ref pWhere);
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public void CancelReceipt(string pReceiptSeq) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("RECEIPT_CANCEL");
			db.ProcedureInParm("PRECEIPT_SEQ",DbSession.DbType.VARCHAR2,pReceiptSeq);
			db.ProcedureInParm("PCANCEL_FLAG",DbSession.DbType.NUMBER,ViComm.ViCommConst.FLAG_ON);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
	public void RevertCancelReceipt(string pReceiptSeq) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("RECEIPT_CANCEL");
			db.ProcedureInParm("PRECEIPT_SEQ",DbSession.DbType.VARCHAR2,pReceiptSeq);
			db.ProcedureInParm("PCANCEL_FLAG",DbSession.DbType.NUMBER,ViComm.ViCommConst.FLAG_OFF);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}
