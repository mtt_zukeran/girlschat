﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: テストメール送信元 
--	Progaram ID		: Admin
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using iBridCommLib;
using ViComm;

public class TestMailFrom:DbSession {
	public string fromManLoginId;
	public string fromManCharNo;
	public string fromCastLoginId;
	public string fromCastCharNo;

	public TestMailFrom() {
	}

	public bool GetOne(string pSiteCd) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect();
			string sSql = "SELECT " +
								"FROM_MAN_LOGIN_ID	," +
								"FROM_MAN_CHAR_NO	," +
								"FROM_CAST_LOGIN_ID	," +
								"FROM_CAST_CHAR_NO	" +
							"FROM " +
								"VW_TEST_MAIL_FROM01 " +
							"WHERE " +
								"SITE_CD = :SITE_CD ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_TEST_MAIL_FROM01");
					if (ds.Tables["VW_TEST_MAIL_FROM01"].Rows.Count != 0) {
						dr = ds.Tables["VW_TEST_MAIL_FROM01"].Rows[0];
						fromManLoginId = dr["FROM_MAN_LOGIN_ID"].ToString();
						fromManCharNo = dr["FROM_MAN_CHAR_NO"].ToString();
						fromCastLoginId = dr["FROM_CAST_LOGIN_ID"].ToString();
						fromCastCharNo = dr["FROM_CAST_CHAR_NO"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}
}
