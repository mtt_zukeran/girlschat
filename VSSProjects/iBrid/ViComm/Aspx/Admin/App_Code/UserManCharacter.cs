/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 男性会員キャラクター
--	Progaram ID		: UserManCharacter
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;
using ViComm;
using System.Text.RegularExpressions;

public class UserManCharacter:DbSession {

	public string userSeq;
	public string handleNm;

	public UserManCharacter() {
	}

	public int GetPageCount(
		string pSiteCd,
		int pUserRankMask,
		int pUserStatusRank,
		int pUserOnlineStatus,
		int pCarrier,
		string pTel,
		string pLoginID,
		string pBalPointFrom,
		string pBalPointTo,
		string pEMailAddr,
		string pHandleNm,
		string pAdCd,
		string pRemarks,
		string pTotalReceiptAmtFrom,
		string pTotalReceiptAmtTo,
		string pRegistDayFrom,
		string pRegistDayTo,
		string pFirstLoginDayFrom,
		string pFirstLoginDayTo,
		string pLastLoginDayFrom,
		string pLastLoginDayTo,
		string pLastPointUsedDayFrom,
		string pLastPointUsedDayTo,
		string pFirstReceiptDayFrom,
		string pFirstReceiptDayTo,
		string pLastReceiptDayFrom,
		string pLastReceiptDayTo,
		string pTotalReceiptCountFrom,
		string pTotalReceiptCountTo,
		int pNonExistMailAddrFlagMask,
		string pFirstLoginTimeFrom,
		string pFirstLoginTimeTo,
		string pLastLoginTimeFrom,
		string pLastLoginTimeTo,
		string pLastPointUsedTimeFrom,
		string pLastPointUsedTimeTo,
		string pRegistTimeFrom,
		string pRegistTimeTo,
		string pLastBlackDayFrom,
		string pLastBlackDayTo,
		string pBillAmtFrom,
		string pBillAmtTo,
		string pMchaTotalReceiptAmtFrom,
		string pMchaTotalReceiptAmtTo,
		string pMchaTotalReceiptCountFrom,
		string pMchaTotalReceiptCountTo,
		string pUrgeLevel,
		bool pCreditMeasured,
		bool pMchaUsedFlag,
		bool pCreditUsedFlag,
		string pManAttrValue1,
		string pManAttrValue2,
		string pManAttrValue3,
		string pManAttrValue4,
		int? pWithUnRegistComplete,
		string pGuid,
		bool pPointAffiFlag,
		string pCastMailRxType,
		string pInfoMailRxType,
		string pIntroducerFriendCd,
		string pRemarks4,
		string pRegistCarrier,
		string pMobileCarrier,
		bool pIsNullAdCd,
		string pNaFlag,
		string pReportAffiliateDateFrom,
		string pReportAffiliateDateTo,
		string pServicePointGetCountFrom,
		string pServicePointGetCountTo,
		string pChkExistAddrExeCountFrom,
		string pChkExistAddrExeCountTo,
		string pSiteUseStatus,
		string pUserDefineMask,
		string pExceptRefusedByCastUserSeq,
		string pGameHandleNm,
		string pGameCharacterType,
		string pGameRegistDateFrom,
		string pGameRegistDateTo,
		string pGameCharacterLevelFrom,
		string pGameCharacterLevelTo,
		string pGamePointFrom,
		string pGamePointTo,
		bool pHandleNmExactFlag,
		string pBeforeSystemId,
		string pKeyword,
		bool pLoginIdNegativeFlag,
		bool pLastLoginDayNegativeFlag,
		bool pLastPointUsedDayNegativeFlag,
		bool pRegistDayNegativeFlag,
		bool pUserDefineMaskNegatieFlag,
		bool pIntroducerFriendAllFlag,
		string pCrosmileLastUsedVersionFrom,
		string pCrosmileLastUsedVersionTo,
		bool pTalkDirectSmartNotMonitorFlag,
		string pUseCrosmileFlag,
		string pSortExpression,
		string pSortDirection,
		bool pIsNullTel,
		string pUserSeq,
		string pUseVoiceappFlag,
		string pPointUseFlag,
		string pAuthFlag,
		string pGcappLastLoginVersionFrom,
		string pGcappLastLoginVersionTo,
		string pUserTestType,
		int pRegistCarrierCd,
		bool pNotExistFirstPointUsedDate,
		bool pExcludeEmailAddr,
		string pTxMuller3NgMailAddrFlag,
		string pRichinoRankFrom,
		string pRichinoRankTo,
		bool pExcludeAdCd,
		string pMchaReceiptDateFrom,
		string pMchaReceiptDateTo
	) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_USER_MAN_CHARACTER03 ";

			string[] sManAttrValue = { pManAttrValue1,pManAttrValue2,pManAttrValue3,pManAttrValue4 };
			for (int i = 0;i < ViCommConst.MAX_ADMIN_MAN_INQUIRY_ITEMS;i++) {
				if (!sManAttrValue[i].Equals("")) {
					sSql = sSql + string.Format(",VW_USER_MAN_ATTR_VALUE01 AT{0} ",i + 1);
				}
			}
			// リッチーノランクが指定されてる場合、テーブル連結追加
			if (!string.IsNullOrEmpty(pRichinoRankFrom) || !string.IsNullOrEmpty(pRichinoRankTo)) {
				sSql = sSql + ",T_USER_MAN_CHARACTER_EX ";
			}

			string sWhere = " WHERE ";
			OracleParameter[] oParms = CreateWhere(
											pSiteCd,
											pUserRankMask,
											pUserStatusRank,
											pUserOnlineStatus,
											pCarrier,
											pTel,
											pLoginID,
											pBalPointFrom,
											pBalPointTo,
											pEMailAddr,
											pHandleNm,
											pAdCd,
											pRemarks,
											pTotalReceiptAmtFrom,
											pTotalReceiptAmtTo,
											pRegistDayFrom,
											pRegistDayTo,
											pFirstLoginDayFrom,
											pFirstLoginDayTo,
											pLastLoginDayFrom,
											pLastLoginDayTo,
											pLastPointUsedDayFrom,
											pLastPointUsedDayTo,
											pFirstReceiptDayFrom,
											pFirstReceiptDayTo,
											pLastReceiptDayFrom,
											pLastReceiptDayTo,
											pTotalReceiptCountFrom,
											pTotalReceiptCountTo,
											pNonExistMailAddrFlagMask,
											pFirstLoginTimeFrom,
											pFirstLoginTimeTo,
											pLastLoginTimeFrom,
											pLastLoginTimeTo,
											pLastPointUsedTimeFrom,
											pLastPointUsedTimeTo,
											pRegistTimeFrom,
											pRegistTimeTo,
											pLastBlackDayFrom,
											pLastBlackDayTo,
											pBillAmtFrom,
											pBillAmtTo,
											pMchaTotalReceiptAmtFrom,
											pMchaTotalReceiptAmtTo,
											pMchaTotalReceiptCountFrom,
											pMchaTotalReceiptCountTo,
											pUrgeLevel,
											pCreditMeasured,
											pMchaUsedFlag,
											pCreditUsedFlag,
											pManAttrValue1,
											pManAttrValue2,
											pManAttrValue3,
											pManAttrValue4,
											pWithUnRegistComplete,
											pGuid,
											pPointAffiFlag,
											pCastMailRxType,
											pInfoMailRxType,
											pIntroducerFriendCd,
											pRemarks4,
											pRegistCarrier,
											pMobileCarrier,
											pIsNullAdCd,
											pNaFlag,
											pReportAffiliateDateFrom,
											pReportAffiliateDateTo,
											pServicePointGetCountFrom,
											pServicePointGetCountTo,
											pChkExistAddrExeCountFrom,
											pChkExistAddrExeCountTo,
											pSiteUseStatus,
											pUserDefineMask,
											pExceptRefusedByCastUserSeq,
											pGameHandleNm,
											pGameCharacterType,
											pGameRegistDateFrom,
											pGameRegistDateTo,
											pGameCharacterLevelFrom,
											pGameCharacterLevelTo,
											pGamePointFrom,
											pGamePointTo,
											pHandleNmExactFlag,
											pBeforeSystemId,
											pKeyword,
											pLoginIdNegativeFlag,
											pLastLoginDayNegativeFlag,
											pLastPointUsedDayNegativeFlag,
											pRegistDayNegativeFlag,
											pUserDefineMaskNegatieFlag,
											pIntroducerFriendAllFlag,
											pCrosmileLastUsedVersionFrom,
											pCrosmileLastUsedVersionTo,
											pTalkDirectSmartNotMonitorFlag,
											pUseCrosmileFlag,
											pIsNullTel,
											pUserSeq,
											pUseVoiceappFlag,
											pPointUseFlag,
											pAuthFlag,
											pGcappLastLoginVersionFrom,
											pGcappLastLoginVersionTo,
											pUserTestType,
											pRegistCarrierCd,
											pNotExistFirstPointUsedDate,
											pExcludeEmailAddr,
											pTxMuller3NgMailAddrFlag,
											pRichinoRankFrom,
											pRichinoRankTo,
											pExcludeAdCd,
											pMchaReceiptDateFrom,
											pMchaReceiptDateTo,
											ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < oParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)oParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(
		string pSiteCd,
		int pUserRankMask,
		int pUserStatusRank,
		int pUserOnlineStatus,
		int pCarrier,
		string pTel,
		string pLoginID,
		string pBalPointFrom,
		string pBalPointTo,
		string pEMailAddr,
		string pHandleNm,
		string pAdCd,
		string pRemarks,
		string pTotalReceiptAmtFrom,
		string pTotalReceiptAmtTo,
		string pRegistDayFrom,
		string pRegistDayTo,
		string pFirstLoginDayFrom,
		string pFirstLoginDayTo,
		string pLastLoginDayFrom,
		string pLastLoginDayTo,
		string pLastPointUsedDayFrom,
		string pLastPointUsedDayTo,
		string pFirstReceiptDayFrom,
		string pFirstReceiptDayTo,
		string pLastReceiptDayFrom,
		string pLastReceiptDayTo,
		string pTotalReceiptCountFrom,
		string pTotalReceiptCountTo,
		int pNonExistMailAddrFlagMask,
		string pFirstLoginTimeFrom,
		string pFirstLoginTimeTo,
		string pLastLoginTimeFrom,
		string pLastLoginTimeTo,
		string pLastPointUsedTimeFrom,
		string pLastPointUsedTimeTo,
		string pRegistTimeFrom,
		string pRegistTimeTo,
		string pLastBlackDayFrom,
		string pLastBlackDayTo,
		string pBillAmtFrom,
		string pBillAmtTo,
		string pMchaTotalReceiptAmtFrom,
		string pMchaTotalReceiptAmtTo,
		string pMchaTotalReceiptCountFrom,
		string pMchaTotalReceiptCountTo,
		string pUrgeLevel,
		bool pCreditMeasured,
		bool pMchaUsedFlag,
		bool pCreditUsedFlag,
		string pManAttrValue1,
		string pManAttrValue2,
		string pManAttrValue3,
		string pManAttrValue4,
		int? pWithUnRegistComplete,
		string pGuid,
		bool pPointAffiFlag,
		string pCastMailRxType,
		string pInfoMailRxType,
		string pIntroducerFriendCd,
		string pRemarks4,
		string pRegistCarrier,
		string pMobileCarrier,
		bool pIsNullAdCd,
		string pNaFlag,
		string pReportAffiliateDateFrom,
		string pReportAffiliateDateTo,
		string pServicePointGetCountFrom,
		string pServicePointGetCountTo,
		string pChkExistAddrExeCountFrom,
		string pChkExistAddrExeCountTo,
		string pSiteUseStatus,
		string pUserDefineMask,
		string pExceptRefusedByCastUserSeq,
		string pGameHandleNm,
		string pGameCharacterType,
		string pGameRegistDateFrom,
		string pGameRegistDateTo,
		string pGameCharacterLevelFrom,
		string pGameCharacterLevelTo,
		string pGamePointFrom,
		string pGamePointTo,
		bool pHandleNmExactFlag,
		string pBeforeSystemId,
		string pKeyword,
		bool pLoginIdNegativeFlag,
		bool pLastLoginDayNegativeFlag,
		bool pLastPointUsedDayNegativeFlag,
		bool pRegistDayNegativeFlag,
		bool pUserDefineMaskNegatieFlag,
		bool pIntroducerFriendAllFlag,
		string pCrosmileLastUsedVersionFrom,
		string pCrosmileLastUsedVersionTo,
		bool pTalkDirectSmartNotMonitorFlag,
		string pUseCrosmileFlag,
		string pSortExpression,
		string pSortDirection,
		int startRowIndex,
		int maximumRows,
		bool pIsNullTel,
		string pUserSeq,
		string pUseVoiceappFlag,
		string pPointUseFlag,
		string pAuthFlag,
		string pGcappLastLoginVersionFrom,
		string pGcappLastLoginVersionTo,
		string pUserTestType,
		int pRegistCarrierCd,
		bool pNotExistFirstPointUsedDate,
		bool pExcludeEmailAddr,
		string pTxMuller3NgMailAddrFlag,
		string pRichinoRankFrom,
		string pRichinoRankTo,
		bool pExcludeAdCd,
		string pMchaReceiptDateFrom,
		string pMchaReceiptDateTo
	) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sTblName = "VW_USER_MAN_CHARACTER03.";
			string sOrder = string.Format("ORDER BY {0} VW_USER_MAN_CHARACTER03.REGIST_DATE DESC",
				string.IsNullOrEmpty(pSortExpression) || string.IsNullOrEmpty(pSortDirection) ? string.Empty : string.Format("{0} {1},",sTblName + pSortExpression,sTblName + pSortDirection));
			string sSql;
			if (maximumRows != SysConst.DB_MAX_ROWS) {
				sSql = "SELECT \n" + QueryFieldsIntro() +
							"FROM \n" +
							"( \n" +
							    " SELECT \n" +
							  	   "VW_USER_MAN_CHARACTER03.*			, \n" +
							  	   "nvl(substr(VW_USER_MAN_CHARACTER03.INTRODUCER_FRIEND_CD,10,LENGTH(VW_USER_MAN_CHARACTER03.INTRODUCER_FRIEND_CD)),'0') as CSEQ, \n" +
						      	   "T_USER.USER_SEQ as CAST_USER_SEQ, \n" +
      							   "T_USER.LOGIN_ID as CAST_LOGIN_ID, \n" +
								   "ROW_NUMBER() OVER ( " + sOrder + " ) AS RNUM \n" +
							   " FROM \n" +
								   "VW_USER_MAN_CHARACTER03 \n" +
								   "left outer join T_USER on \n" +
      							   "( \n" +
       							   " to_char(T_USER.USER_SEQ) = nvl(substr(VW_USER_MAN_CHARACTER03.INTRODUCER_FRIEND_CD,10,LENGTH(VW_USER_MAN_CHARACTER03.INTRODUCER_FRIEND_CD)),0) \n" +
			          			   " and T_USER.SEX_CD = '3' \n" +
      							   ") \n" ;
			} 
			else {
				sSql = "SELECT \n" + QueryFieldsIntro() +
							"FROM( \n" +
								" SELECT \n" +
								   "VW_USER_MAN_CHARACTER03.*			, \n" +
								   "nvl(substr(VW_USER_MAN_CHARACTER03.INTRODUCER_FRIEND_CD,10,LENGTH(VW_USER_MAN_CHARACTER03.INTRODUCER_FRIEND_CD)),'0') as CSEQ, \n" +
								   "T_USER.USER_SEQ as CAST_USER_SEQ, \n" +
								   "T_USER.LOGIN_ID as CAST_LOGIN_ID \n" +
							   " FROM \n" +
								   "VW_USER_MAN_CHARACTER03 \n" +
								   "left outer join T_USER on \n" +
								   "( \n" +
								   " to_char(T_USER.USER_SEQ) = nvl(substr(VW_USER_MAN_CHARACTER03.INTRODUCER_FRIEND_CD,10,LENGTH(VW_USER_MAN_CHARACTER03.INTRODUCER_FRIEND_CD)),0) \n" +
								   " and T_USER.SEX_CD = '3' \n" +
								   ") \n";
			}
			string[] sManAttrValue = { pManAttrValue1,pManAttrValue2,pManAttrValue3,pManAttrValue4 };
			for (int i = 0;i < ViCommConst.MAX_ADMIN_MAN_INQUIRY_ITEMS;i++) {
				if (!sManAttrValue[i].Equals("")) {
					sSql = sSql + string.Format(",VW_USER_MAN_ATTR_VALUE01 AT{0} ",i + 1);
				}
			}
			// リッチーノランクが指定されてる場合、テーブル連結追加
			if (!string.IsNullOrEmpty(pRichinoRankFrom) || !string.IsNullOrEmpty(pRichinoRankTo)) {
				sSql = sSql + ",T_USER_MAN_CHARACTER_EX ";
			}

			string sWhere = "";
			OracleParameter[] oParms = CreateWhere(
											pSiteCd,
											pUserRankMask,
											pUserStatusRank,
											pUserOnlineStatus,
											pCarrier,
											pTel,
											pLoginID,
											pBalPointFrom,
											pBalPointTo,
											pEMailAddr,
											pHandleNm,
											pAdCd,
											pRemarks,
											pTotalReceiptAmtFrom,
											pTotalReceiptAmtTo,
											pRegistDayFrom,
											pRegistDayTo,
											pFirstLoginDayFrom,
											pFirstLoginDayTo,
											pLastLoginDayFrom,
											pLastLoginDayTo,
											pLastPointUsedDayFrom,
											pLastPointUsedDayTo,
											pFirstReceiptDayFrom,
											pFirstReceiptDayTo,
											pLastReceiptDayFrom,
											pLastReceiptDayTo,
											pTotalReceiptCountFrom,
											pTotalReceiptCountTo,
											pNonExistMailAddrFlagMask,
											pFirstLoginTimeFrom,
											pFirstLoginTimeTo,
											pLastLoginTimeFrom,
											pLastLoginTimeTo,
											pLastPointUsedTimeFrom,
											pLastPointUsedTimeTo,
											pRegistTimeFrom,
											pRegistTimeTo,
											pLastBlackDayFrom,
											pLastBlackDayTo,
											pBillAmtFrom,
											pBillAmtTo,
											pMchaTotalReceiptAmtFrom,
											pMchaTotalReceiptAmtTo,
											pMchaTotalReceiptCountFrom,
											pMchaTotalReceiptCountTo,
											pUrgeLevel,
											pCreditMeasured,
											pMchaUsedFlag,
											pCreditUsedFlag,
											pManAttrValue1,
											pManAttrValue2,
											pManAttrValue3,
											pManAttrValue4,
											pWithUnRegistComplete,
											pGuid,
											pPointAffiFlag,
											pCastMailRxType,
											pInfoMailRxType,
											pIntroducerFriendCd,
											pRemarks4,
											pRegistCarrier,
											pMobileCarrier,
											pIsNullAdCd,
											pNaFlag,
											pReportAffiliateDateFrom,
											pReportAffiliateDateTo,
											pServicePointGetCountFrom,
											pServicePointGetCountTo,
											pChkExistAddrExeCountFrom,
											pChkExistAddrExeCountTo,
											pSiteUseStatus,
											pUserDefineMask,
											pExceptRefusedByCastUserSeq,
											pGameHandleNm,
											pGameCharacterType,
											pGameRegistDateFrom,
											pGameRegistDateTo,
											pGameCharacterLevelFrom,
											pGameCharacterLevelTo,
											pGamePointFrom,
											pGamePointTo,
											pHandleNmExactFlag,
											pBeforeSystemId,
											pKeyword,
											pLoginIdNegativeFlag,
											pLastLoginDayNegativeFlag,
											pLastPointUsedDayNegativeFlag,
											pRegistDayNegativeFlag,
											pUserDefineMaskNegatieFlag,
											pIntroducerFriendAllFlag,
											pCrosmileLastUsedVersionFrom,
											pCrosmileLastUsedVersionTo,
											pTalkDirectSmartNotMonitorFlag,
											pUseCrosmileFlag,
											pIsNullTel,
											pUserSeq,
											pUseVoiceappFlag,
											pPointUseFlag,
											pAuthFlag,
											pGcappLastLoginVersionFrom,
											pGcappLastLoginVersionTo,
											pUserTestType,
											pRegistCarrierCd,
											pNotExistFirstPointUsedDate,
											pExcludeEmailAddr,
											pTxMuller3NgMailAddrFlag,
											pRichinoRankFrom,
											pRichinoRankTo,
											pExcludeAdCd,
											pMchaReceiptDateFrom,
											pMchaReceiptDateTo,
											ref sWhere);

			sSql = sSql + sWhere;

			if (maximumRows != SysConst.DB_MAX_ROWS) {
				//order byのテーブル名置換
				string sRepOrder = sOrder.Replace("VW_USER_MAN_CHARACTER03","VWUSER03");
				sSql = sSql + "\n) VWUSER03 \n" +
					  "WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW \n" + 
					  sRepOrder;
			} else {
				sSql += "\n) VWUSER03 \n";
			}

			if (maximumRows == SysConst.DB_MAX_ROWS) {
				StringBuilder sCsvSql = new StringBuilder();
				StringBuilder sCsvWhere = new StringBuilder(" WHERE").AppendLine();
				StringBuilder sCsvFrom = new StringBuilder("FROM (" + sSql + ") MST").AppendLine();

				sCsvSql.Append("SELECT * FROM(SELECT MST.* ");

				using (UserManAttrType oUserManAttrType = new UserManAttrType()) {
					DataSet dsAttrType = oUserManAttrType.GetPageCollection(pSiteCd,0,ViCommConst.MAX_ATTR_COUNT);
					int i = 0;
					foreach (DataRow dr in dsAttrType.Tables[0].Rows) {
						i++;
						sCsvSql.Append(string.Format(",MAN_VAL{0}.INPUT_TYPE			AS INPUT_TYPE{0}",i)).AppendLine();
						sCsvSql.Append(string.Format(",MAN_VAL{0}.MAN_ATTR_INPUT_VALUE	AS MAN_ATTR_INPUT_VALUE{0}",i)).AppendLine();
						sCsvSql.Append(string.Format(",MAN_VAL{0}.MAN_ATTR_NM			AS MAN_ATTR_NM{0}",i)).AppendLine();
						sCsvFrom.Append(",VW_USER_MAN_ATTR_VALUE01 MAN_VAL" + i.ToString());
						sCsvWhere.Append(string.Format("MST.SITE_CD		= MAN_VAL{0}.SITE_CD			(+)AND",i)).AppendLine();
						sCsvWhere.Append(string.Format("MST.USER_SEQ	= MAN_VAL{0}.USER_SEQ			(+)AND",i)).AppendLine();
						sCsvWhere.Append(string.Format("MST.USER_CHAR_NO= MAN_VAL{0}.USER_CHAR_NO		(+)AND",i)).AppendLine();
						sCsvWhere.Append(string.Format("{1}				= MAN_VAL{0}.MAN_ATTR_TYPE_SEQ	(+) ",i,dr["MAN_ATTR_TYPE_SEQ"].ToString()));
						if (i != dsAttrType.Tables[0].Rows.Count) {
							sCsvWhere.Append("AND").AppendLine();
						}
					}
				}
				sSql = sCsvSql.ToString() + sCsvFrom.ToString() + sCsvWhere.ToString() + ") ORDER BY SITE_CD,USER_SEQ ";
			}

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < oParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)oParms[i]);
				}

				if (maximumRows != SysConst.DB_MAX_ROWS) {
					cmd.Parameters.Add("FIRST_ROW",startRowIndex);
					cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);
				}

				using (da = new OracleDataAdapter(cmd)) {
					//da.Fill(ds,"VW_USER_MAN_CHARACTER03");
					this.Fill(da,ds,"VW_USER_MAN_CHARACTER03");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(
		string pSiteCd,
		int pUserRankMask,
		int pUserStatusRank,
		int pUserOnlineStatus,
		int pCarrier,
		string pTel,
		string pLoginID,
		string pBalPointFrom,
		string pBalPointTo,
		string pEMailAddr,
		string pHandleNm,
		string pAdCd,
		string pRemarks,
		string pTotalReceiptAmtFrom,
		string pTotalReceiptAmtTo,
		string pRegistDayFrom,
		string pRegistDayTo,
		string pFirstLoginDayFrom,
		string pFirstLoginDayTo,
		string pLastLoginDayFrom,
		string pLastLoginDayTo,
		string pLastPointUsedDayFrom,
		string pLastPointUsedDayTo,
		string pFirstReceiptDayFrom,
		string pFirstReceiptDayTo,
		string pLastReceiptDayFrom,
		string pLastReceiptDayTo,
		string pTotalReceiptCountFrom,
		string pTotalReceiptCountTo,
		int pNonExistMailAddrFlagMask,
		string pFirstLoginTimeFrom,
		string pFirstLoginTimeTo,
		string pLastLoginTimeFrom,
		string pLastLoginTimeTo,
		string pLastPointUsedTimeFrom,
		string pLastPointUsedTimeTo,
		string pRegistTimeFrom,
		string pRegistTimeTo,
		string pLastBlackDayFrom,
		string pLastBlackDayTo,
		string pBillAmtFrom,
		string pBillAMtTo,
		string pMchaTotalReceiptAmtFrom,
		string pMchaTotalReceiptAmtTo,
		string pMchaTotalReceiptCountFrom,
		string pMchaTotalReceiptCountTo,
		string pUrgeLevel,
		bool pCreditMeasured,
		bool pMchaUsedFlag,
		bool pCreditUsedFlag,
		string pManAttrValue1,
		string pManAttrValue2,
		string pManAttrValue3,
		string pManAttrValue4,
		int? pWithUnRegistComplete,
		string pGuid,
		bool pPointAffiFlag,
		string pCastMailRxType,
		string pInfoMailRxType,
		string pIntroducerFriendCd,
		string pRemarks4,
		string pRegistCarrier,
		string pMobileCarrier,
		bool pIsNullAdCd,
		string pNaFlag,
		string pReportAffiliateDateFrom,
		string pReportAffiliateDateTo,
		string pServicePointGetCountFrom,
		string pServicePointGetCountTo,
		string pChkExistAddrExeCountFrom,
		string pChkExistAddrExeCountTo,
		string pSiteUseStatus,
		string pUserDefineMask,
		string pExceptRefusedByCastUserSeq,
		string pGameHandleNm,
		string pGameCharacterType,
		string pGameRegistDateFrom,
		string pGameRegistDateTo,
		string pGameCharacterLevelFrom,
		string pGameCharacterLevelTo,
		string pGamePointFrom,
		string pGamePointTo,
		bool pHandleNmExactFlag,
		string pBeforeSystemId,
		string pKeyword,
		bool pLoginIdNegativeFlag,
		bool pLastLoginDayNegativeFlag,
		bool pLastPointUsedDayNegativeFlag,
		bool pRegistDayNegativeFlag,
		bool pUserDefineMaskNegatieFlag,
		bool pIntroducerFriendAllFlag,
		string pCrosmileLastUsedVersionFrom,
		string pCrosmileLastUsedVersionTo,
		bool pTalkDirectSmartNotMonitorFlag,
		string pUseCrosmileFlag,
		bool pIsNullTel,
		string pUserSeq,
		string pUseVoiceappFlag,
		string pFirstPointUseFlag,
		string pAuthFlag,
		string pGcappLastLoginVersionFrom,
		string pGcappLastLoginVersionTo,
		string pUserTestType,
		int pRegistCarrierCd,
		bool pNotExistFirstPointUsedDate,
		bool pExcludeEmailAddr,
		string pTxMuller3NgMailAddrFlag,
		string pRichinoRankFrom,
		string pRichinoRankTo,
		bool pExcludeAdCd,
		string pMchaReceiptDateFrom,
		string pMchaReceiptDateTo,
		ref string pWhere
	) {
		pWhere = "";

		string[] sValues = new string[10];
		int iCnt = 0;

		SetDefaultTimeValue(ref pFirstLoginTimeFrom,ref pFirstLoginTimeTo);
		SetDefaultTimeValue(ref pLastLoginTimeFrom,ref pLastLoginTimeTo);
		SetDefaultTimeValue(ref pLastPointUsedTimeFrom,ref pLastPointUsedTimeTo);
		SetDefaultTimeValue(ref pRegistTimeFrom,ref pRegistTimeTo);

		ArrayList list = new ArrayList();
		pWhere = pWhere + " WHERE VW_USER_MAN_CHARACTER03.SITE_CD = :SITE_CD  ";
		list.Add(new OracleParameter("SITE_CD",pSiteCd));

		if (pWithUnRegistComplete != null) {
			pWhere = pWhere + " AND VW_USER_MAN_CHARACTER03.NA_FLAG = :NA_FLAG  ";
			list.Add(new OracleParameter("NA_FLAG",pWithUnRegistComplete));
		}

		if (pUserRankMask != 0) {
			if ((pUserRankMask & ViCommConst.MASK_RANK_A) != 0) {
				sValues[iCnt++] = ViCommConst.USER_RANK_A;
			}
			if ((pUserRankMask & ViCommConst.MASK_RANK_B) != 0) {
				sValues[iCnt++] = ViCommConst.USER_RANK_B;
			}
			if ((pUserRankMask & ViCommConst.MASK_RANK_C) != 0) {
				sValues[iCnt++] = ViCommConst.USER_RANK_C;
			}
			if ((pUserRankMask & ViCommConst.MASK_RANK_D) != 0) {
				sValues[iCnt++] = ViCommConst.USER_RANK_D;
			}
			if ((pUserRankMask & ViCommConst.MASK_RANK_E) != 0) {
				sValues[iCnt++] = ViCommConst.USER_RANK_E;
			}
			if ((pUserRankMask & ViCommConst.MASK_RANK_F) != 0) {
				sValues[iCnt++] = ViCommConst.USER_RANK_F;
			}
			if ((pUserRankMask & ViCommConst.MASK_RANK_G) != 0) {
				sValues[iCnt++] = ViCommConst.USER_RANK_G;
			}
			if ((pUserRankMask & ViCommConst.MASK_RANK_H) != 0) {
				sValues[iCnt++] = ViCommConst.USER_RANK_H;
			}

			pWhere = pWhere + " AND (VW_USER_MAN_CHARACTER03.USER_RANK IN (";
			for (int i = 0;i < iCnt;i++) {
				pWhere = pWhere + ":USER_RANK" + i.ToString() + ",";
				list.Add(new OracleParameter("USER_RANK",sValues[i]));
			}
			pWhere = pWhere.Substring(0,pWhere.Length - 1) + ")) ";
		}

		iCnt = 0;
		if (pUserStatusRank != 0) {
			if ((pUserStatusRank & ViCommConst.MASK_MAN_NEW) != 0) {
				sValues[iCnt++] = ViCommConst.USER_MAN_NEW;
			}
			if ((pUserStatusRank & ViCommConst.MASK_MAN_BLACK) != 0) {
				sValues[iCnt++] = ViCommConst.USER_MAN_BLACK;
			}
			if ((pUserStatusRank & ViCommConst.MASK_MAN_NO_RECEIPT) != 0) {
				sValues[iCnt++] = ViCommConst.USER_MAN_NO_RECEIPT;
			}
			if ((pUserStatusRank & ViCommConst.MASK_MAN_NO_USED) != 0) {
				sValues[iCnt++] = ViCommConst.USER_MAN_NO_USED;
			}
			if ((pUserStatusRank & ViCommConst.MASK_MAN_NORMAL) != 0) {
				sValues[iCnt++] = ViCommConst.USER_MAN_NORMAL;
			}
			if ((pUserStatusRank & ViCommConst.MASK_MAN_DELAY) != 0) {
				sValues[iCnt++] = ViCommConst.USER_MAN_DELAY;
			}
			if ((pUserStatusRank & ViCommConst.MASK_MAN_HOLD) != 0) {
				sValues[iCnt++] = ViCommConst.USER_MAN_HOLD;
			}
			if ((pUserStatusRank & ViCommConst.MASK_MAN_RESIGNED) != 0) {
				sValues[iCnt++] = ViCommConst.USER_MAN_RESIGNED;
			}
			if ((pUserStatusRank & ViCommConst.MASK_MAN_STOP) != 0) {
				sValues[iCnt++] = ViCommConst.USER_MAN_STOP;
			}
			if ((pUserStatusRank & ViCommConst.MASK_MAN_TEL_NO_AUTH) != 0) {
				sValues[iCnt++] = ViCommConst.USER_MAN_TEL_NO_AUTH;
			}
			pWhere = pWhere + " AND (VW_USER_MAN_CHARACTER03.USER_STATUS IN (";
			for (int i = 0;i < iCnt;i++) {
				pWhere = pWhere + ":USER_STATUS" + i.ToString() + ",";
				list.Add(new OracleParameter("MUSER_STATUS",sValues[i]));
			}
			pWhere = pWhere.Substring(0,pWhere.Length - 1) + ")) ";
		}
		if (pCreditMeasured) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.CREDIT_MEASURED_RATE_TARGET = :CREDIT_MEASURED_RATE_TARGET",ref pWhere);
			list.Add(new OracleParameter("CREDIT_MEASURED_RATE_TARGET",ViCommConst.FLAG_ON));
		}
		iCnt = 0;
		if (pUserOnlineStatus != 0) {
			if ((pUserOnlineStatus & ViCommConst.MASK_LOGINED) != 0) {
				sValues[iCnt++] = ViCommConst.USER_LOGINED.ToString();
			}
			if ((pUserOnlineStatus & ViCommConst.MASK_OFFLINE) != 0) {
				sValues[iCnt++] = ViCommConst.USER_OFFLINE.ToString();
			}
			if ((pUserOnlineStatus & ViCommConst.MASK_TALKING) != 0) {
				sValues[iCnt++] = ViCommConst.USER_TALKING.ToString();
			}
			if ((pUserOnlineStatus & ViCommConst.MASK_WAITING) != 0) {
				sValues[iCnt++] = ViCommConst.USER_WAITING.ToString();
			}
			pWhere = pWhere + " AND (VW_USER_MAN_CHARACTER03.CHARACTER_ONLINE_STATUS IN (";
			for (int i = 0;i < iCnt;i++) {
				pWhere = pWhere + ":CHARACTER_ONLINE_STATUS" + i.ToString() + ",";
				list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS",sValues[i]));
			}
			pWhere = pWhere.Substring(0,pWhere.Length - 1) + ")) ";
		}

		//利用キャリア
		iCnt = 0;
		if (pCarrier != 0) {
			if ((pCarrier & ViCommConst.MASK_DOCOMO) != 0) {
				sValues[iCnt++] = ViCommConst.DOCOMO;
			}
			if ((pCarrier & ViCommConst.MASK_KDDI) != 0) {
				sValues[iCnt++] = ViCommConst.KDDI;
			}
			if ((pCarrier & ViCommConst.MASK_SOFTBANK) != 0) {
				sValues[iCnt++] = ViCommConst.SOFTBANK;
			}
			if ((pCarrier & ViCommConst.MASK_ANDROID) != 0) {
				sValues[iCnt++] = ViCommConst.ANDROID;
			}
			if ((pCarrier & ViCommConst.MASK_IPHONE) != 0) {
				sValues[iCnt++] = ViCommConst.IPHONE;
			}
			if ((pCarrier & ViCommConst.MASK_OTHER) != 0) {
				sValues[iCnt++] = ViCommConst.CARRIER_OTHERS;
			}
			pWhere = pWhere + " AND (VW_USER_MAN_CHARACTER03.MOBILE_CARRIER_CD IN (";
			for (int i = 0;i < iCnt;i++) {
				pWhere = pWhere + ":MOBILE_CARRIER_CD" + i.ToString() + ",";
				list.Add(new OracleParameter("MOBILE_CARRIER_CD",sValues[i]));
			}
			pWhere = pWhere.Substring(0,pWhere.Length - 1) + ")) ";
		}

		//登録キャリア
		iCnt = 0;
		if (pRegistCarrierCd != 0) {
			if ((pRegistCarrierCd & ViCommConst.MASK_DOCOMO) != 0) {
				sValues[iCnt++] = ViCommConst.DOCOMO;
			}
			if ((pRegistCarrierCd & ViCommConst.MASK_KDDI) != 0) {
				sValues[iCnt++] = ViCommConst.KDDI;
			}
			if ((pRegistCarrierCd & ViCommConst.MASK_SOFTBANK) != 0) {
				sValues[iCnt++] = ViCommConst.SOFTBANK;
			}
			if ((pRegistCarrierCd & ViCommConst.MASK_ANDROID) != 0) {
				sValues[iCnt++] = ViCommConst.ANDROID;
			}
			if ((pRegistCarrierCd & ViCommConst.MASK_IPHONE) != 0) {
				sValues[iCnt++] = ViCommConst.IPHONE;
			}
			if ((pRegistCarrierCd & ViCommConst.MASK_OTHER) != 0) {
				sValues[iCnt++] = ViCommConst.CARRIER_OTHERS;
			}
			pWhere = pWhere + " AND (VW_USER_MAN_CHARACTER03.REGIST_CARRIER_CD IN (";
			for (int i = 0;i < iCnt;i++) {
				pWhere = pWhere + ":REGIST_CARRIER_CD" + i.ToString() + ",";
				list.Add(new OracleParameter("REGIST_CARRIER_CD",sValues[i]));
			}
			pWhere = pWhere.Substring(0,pWhere.Length - 1) + ")) ";
		}

		if (pIsNullTel) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.TEL IS NULL",ref pWhere);
		} else if (!pTel.Equals("")) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.TEL LIKE :TEL||'%'",ref pWhere);
			list.Add(new OracleParameter("TEL",pTel));
		}

		if (!pLoginID.Equals("")) {
			string sLoginQuery = string.Empty;

			if (Regex.IsMatch(pLoginID,",|\r?\n")) {
				string sConjunction = "OR ";
				string[] sLoginIdArray = Regex.Split(pLoginID,",|\r?\n");
				StringBuilder oLoginIdWhere = new StringBuilder();
				for (int iIndex = 0;iIndex < sLoginIdArray.Length;iIndex++) {
					if (string.IsNullOrEmpty(sLoginIdArray[iIndex].Trim())) {
						continue;
					}
					oLoginIdWhere.AppendFormat("VW_USER_MAN_CHARACTER03.LOGIN_ID = :LOGIN_ID{0} OR ",iIndex);
					list.Add(new OracleParameter(string.Concat("LOGIN_ID",iIndex),sLoginIdArray[iIndex].Trim()));
				}

				if (oLoginIdWhere.Length > 0) {
					sLoginQuery = string.Format("{0} ({1})",
						this.GetNegative(pLoginIdNegativeFlag),
						oLoginIdWhere.Remove(oLoginIdWhere.Length - sConjunction.Length,sConjunction.Length));
					SysPrograms.SqlAppendWhere(sLoginQuery,ref pWhere);
				}
			} else {
				sLoginQuery = string.Format("VW_USER_MAN_CHARACTER03.LOGIN_ID {0} LIKE :LOGIN_ID||'%'",this.GetNegative(pLoginIdNegativeFlag));
				SysPrograms.SqlAppendWhere(sLoginQuery,ref pWhere);
				list.Add(new OracleParameter("LOGIN_ID",pLoginID));
			}
		}

		if (!pUserSeq.Equals("")) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.USER_SEQ = :USER_SEQ",ref pWhere);
			list.Add(new OracleParameter("USER_SEQ",pUserSeq));
		}

		if (!pBalPointFrom.Equals("")) {
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.BAL_POINT >= :BAL_POINT_FROM AND VW_USER_MAN_CHARACTER03.BAL_POINT <= :BAL_POINT_TO)",ref pWhere);
			list.Add(new OracleParameter("BAL_POINT_FROM",int.Parse(pBalPointFrom)));
			list.Add(new OracleParameter("BAL_POINT_TO",int.Parse(pBalPointTo)));
		}

		// メールアドレス
		if (Regex.IsMatch(pEMailAddr,@",|\s")) {
			string sConjunction = "OR ";
			string[] sEmailAddrArray = Regex.Split(pEMailAddr,@",|\s");
			StringBuilder oEmailAddrWhere = new StringBuilder();
			for (int iIndex = 0;iIndex < sEmailAddrArray.Length;iIndex++) {
				if (string.IsNullOrEmpty(sEmailAddrArray[iIndex].Trim())) {
					continue;
				}
				oEmailAddrWhere.AppendFormat("VW_USER_MAN_CHARACTER03.EMAIL_ADDR LIKE '%' || :EMAIL_ADDR{0} || '%' OR ",iIndex);
				list.Add(new OracleParameter(string.Concat("EMAIL_ADDR",iIndex),sEmailAddrArray[iIndex].Trim().ToLower()));
			}

			if (oEmailAddrWhere.Length > 0) {
				SysPrograms.SqlAppendWhere(
					string.Format(
						"{0} ({1})"
						,this.GetNegative(pExcludeEmailAddr)
						,oEmailAddrWhere.Remove(oEmailAddrWhere.Length - sConjunction.Length,sConjunction.Length)
					)
					,ref pWhere
				);
			}
		} else {
			SysPrograms.SqlAppendWhere(
				string.Format(
					"VW_USER_MAN_CHARACTER03.EMAIL_ADDR {0} LIKE '%' || :EMAIL_ADDR || '%'"
					,this.GetNegative(pExcludeEmailAddr)
				)
				,ref pWhere
			);
			list.Add(new OracleParameter("EMAIL_ADDR",pEMailAddr.ToLower()));
		}

		if (!pHandleNm.Equals("")) {
			if (!pHandleNmExactFlag) {
				SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.HANDLE_NM LIKE '%'||:HANDLE_NM||'%'",ref pWhere);
			} else {
				SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.HANDLE_NM = :HANDLE_NM ",ref pWhere);
			}
			list.Add(new OracleParameter("HANDLE_NM",pHandleNm));
		}

		// 広告ｺｰﾄﾞ
		if (pIsNullAdCd) {
			if (pExcludeAdCd) {
				// 広告ｺｰﾄﾞ登録あり
				SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.AD_CD IS NOT NULL",ref pWhere);
			} else {
				// 広告ｺｰﾄﾞ未登録
				SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.AD_CD IS NULL",ref pWhere);
			}
		} else if (!pAdCd.Equals("")) {
			if (Regex.IsMatch(pAdCd,",|\r?\n")) {
				string[] sAdDdArray = Regex.Split(pAdCd,",|\r?\n");
				string sConjunction = string.Empty;
				StringBuilder oAdDdWhere = new StringBuilder();
				for (int iIndex = 0;iIndex < sAdDdArray.Length;iIndex++) {
					if (string.IsNullOrEmpty(sAdDdArray[iIndex].Trim())) {
						continue;
					}
					oAdDdWhere.AppendFormat("{0} :AD_CD{1} ",sConjunction,iIndex);
					list.Add(new OracleParameter(string.Concat("AD_CD",iIndex),sAdDdArray[iIndex].Trim()));
					sConjunction = ",";
				}

				if (oAdDdWhere.Length > 0) {
					if (pExcludeAdCd) {
						// 入力された広告ｺｰﾄﾞ以外で登録
						SysPrograms.SqlAppendWhere(string.Format("NVL(VW_USER_MAN_CHARACTER03.AD_CD,'NULL') NOT IN({0})",oAdDdWhere.ToString()),ref pWhere);
					} else {
						// 入力された広告ｺｰﾄﾞで登録
						SysPrograms.SqlAppendWhere(string.Format("VW_USER_MAN_CHARACTER03.AD_CD IN({0})",oAdDdWhere.ToString()),ref pWhere);
					}
				}
			} else {
				if (pExcludeAdCd) {
					// 入力された広告ｺｰﾄﾞ以外で登録
					SysPrograms.SqlAppendWhere("NVL(VW_USER_MAN_CHARACTER03.AD_CD,'NULL') <> :AD_CD",ref pWhere);
				} else {
					// 入力された広告ｺｰﾄﾞで登録
					SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.AD_CD = :AD_CD",ref pWhere);
				}
				list.Add(new OracleParameter("AD_CD",pAdCd));
			}
		}

		if (!pRemarks.Equals("")) {
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.REMARKS1 LIKE '%'||:REMARKS1||'%' OR VW_USER_MAN_CHARACTER03.REMARKS2 LIKE '%'||:REMARKS2||'%' OR VW_USER_MAN_CHARACTER03.REMARKS3 LIKE '%'||:REMARKS3||'%')",ref pWhere);
			list.Add(new OracleParameter("REMARKS1",pRemarks));
			list.Add(new OracleParameter("REMARKS2",pRemarks));
			list.Add(new OracleParameter("REMARKS3",pRemarks));
		}

		if (!string.IsNullOrEmpty(pRemarks4)) {
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.REMARKS4 LIKE '%'||:REMARKS4||'%' OR VW_USER_MAN_CHARACTER03.REMARKS5 LIKE '%'||:REMARKS5||'%' OR VW_USER_MAN_CHARACTER03.REMARKS6 LIKE '%'||:REMARKS6||'%' OR VW_USER_MAN_CHARACTER03.REMARKS7 LIKE '%'||:REMARKS7||'%' OR VW_USER_MAN_CHARACTER03.REMARKS8 LIKE '%'||:REMARKS8||'%' OR VW_USER_MAN_CHARACTER03.REMARKS9 LIKE '%'||:REMARKS9||'%' OR VW_USER_MAN_CHARACTER03.REMARKS10 LIKE '%'||:REMARKS10||'%' )",ref pWhere);
			list.Add(new OracleParameter("REMARKS4",pRemarks4));
			list.Add(new OracleParameter("REMARKS5",pRemarks4));
			list.Add(new OracleParameter("REMARKS6",pRemarks4));
			list.Add(new OracleParameter("REMARKS7",pRemarks4));
			list.Add(new OracleParameter("REMARKS8",pRemarks4));
			list.Add(new OracleParameter("REMARKS9",pRemarks4));
			list.Add(new OracleParameter("REMARKS10",pRemarks4));
			
		}
		
		if (!pTotalReceiptAmtFrom.Equals("")) {
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.TOTAL_RECEIPT_AMT >= :TOTAL_RECEIPT_AMT_FROM AND VW_USER_MAN_CHARACTER03.TOTAL_RECEIPT_AMT <= :TOTAL_RECEIPT_AMT_TO)",ref pWhere);
			list.Add(new OracleParameter("TOTAL_RECEIPT_AMT_FROM",int.Parse(pTotalReceiptAmtFrom)));
			list.Add(new OracleParameter("TOTAL_RECEIPT_AMT_TO",int.Parse(pTotalReceiptAmtTo)));
		}

		if (!pBillAmtFrom.Equals("")) {
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.BILL_AMT >= :BILL_AMT_FROM AND VW_USER_MAN_CHARACTER03.BILL_AMT <= :BILL_AMT_TO)",ref pWhere);
			list.Add(new OracleParameter("BILL_AMT_FROM",int.Parse(pBillAmtFrom)));
			list.Add(new OracleParameter("BILL_AMT_TO",int.Parse(pBillAMtTo)));
		}

		if (!pTotalReceiptCountFrom.Equals("")) {
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.TOTAL_RECEIPT_COUNT >= :TOTAL_RECEIPT_COUNT_FROM AND TOTAL_RECEIPT_COUNT <= :TOTAL_RECEIPT_COUNT_TO)",ref pWhere);
			list.Add(new OracleParameter("TOTAL_RECEIPT_COUNT_FROM",int.Parse(pTotalReceiptCountFrom)));
			list.Add(new OracleParameter("TOTAL_RECEIPT_COUNT_TO",int.Parse(pTotalReceiptCountTo)));
		}

		if (!pMchaTotalReceiptAmtFrom.Equals("")) {
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.POINT_AFFILIATE_TOTAL_AMT >= :POINT_AFFILIATE_TOTAL_AMT_FROM AND VW_USER_MAN_CHARACTER03.POINT_AFFILIATE_TOTAL_AMT <= :POINT_AFFILIATE_TOTAL_AMT_TO)",ref pWhere);
			list.Add(new OracleParameter("POINT_AFFILIATE_TOTAL_AMT_FROM",int.Parse(pMchaTotalReceiptAmtFrom)));
			list.Add(new OracleParameter("POINT_AFFILIATE_TOTAL_AMT_TO",int.Parse(pMchaTotalReceiptAmtTo)));
		}

		if (!pMchaTotalReceiptCountFrom.Equals("")) {
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.POINT_AFFILIATE_TOTAL_COUNT >= :POINT_AFFILIATE_TOTAL_COUNT_FM AND POINT_AFFILIATE_TOTAL_COUNT <= :POINT_AFFILIATE_TOTAL_COUNT_TO)",ref pWhere);
			list.Add(new OracleParameter("POINT_AFFILIATE_TOTAL_COUNT_FM",int.Parse(pMchaTotalReceiptCountFrom)));
			list.Add(new OracleParameter("POINT_AFFILIATE_TOTAL_COUNT_TO",int.Parse(pMchaTotalReceiptCountTo)));
		}

		if (!pRegistDayFrom.Equals("")) {
			string sRegistDayQuery = string.Format("({0}(VW_USER_MAN_CHARACTER03.REGIST_DATE >= :REGIST_DATE_FROM AND VW_USER_MAN_CHARACTER03.REGIST_DATE < :REGIST_DATE_TO){1})",
				this.GetNegative(pRegistDayNegativeFlag),
				pRegistDayNegativeFlag ? " OR VW_USER_MAN_CHARACTER03.REGIST_DATE IS NULL" : string.Empty);
			SysPrograms.SqlAppendWhere(sRegistDayQuery,ref pWhere);
			DateTime dtFrom = DateTime.Parse(pRegistDayFrom + " " + pRegistTimeFrom + ":00:00");
			DateTime dtTo = DateTime.Parse(pRegistDayTo + " " + pRegistTimeTo + ":59:59").AddSeconds(1);
			list.Add(new OracleParameter("REGIST_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("REGIST_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (!pFirstLoginDayFrom.Equals("")) {
			DateTime dtFrom = DateTime.Parse(pFirstLoginDayFrom + " " + pFirstLoginTimeFrom + ":00:00");
			DateTime dtTo = DateTime.Parse(pFirstLoginDayTo + " " + pFirstLoginTimeTo + ":59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.FIRST_LOGIN_DATE >= :FIRST_LOGIN_DATE_FROM AND VW_USER_MAN_CHARACTER03.FIRST_LOGIN_DATE < :FIRST_LOGIN_DATE_TO)",ref pWhere);
			list.Add(new OracleParameter("FIRST_LOGIN_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("FIRST_LOGIN_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (!pLastLoginDayFrom.Equals("")) {
			string sLastLoginDayQuery = string.Format("({0}(VW_USER_MAN_CHARACTER03.LAST_LOGIN_DATE >= :LAST_LOGIN_DATE_FROM AND VW_USER_MAN_CHARACTER03.LAST_LOGIN_DATE < :LAST_LOGIN_DATE_TO){1})",
				this.GetNegative(pLastLoginDayNegativeFlag),
				pLastLoginDayNegativeFlag ? " OR VW_USER_MAN_CHARACTER03.LAST_LOGIN_DATE IS NULL" : string.Empty);
			SysPrograms.SqlAppendWhere(sLastLoginDayQuery,ref pWhere);
			DateTime dtFrom = DateTime.Parse(pLastLoginDayFrom + " " + pLastLoginTimeFrom + ":00:00");
			DateTime dtTo = DateTime.Parse(pLastLoginDayTo + " " + pLastLoginTimeTo + ":59:59").AddSeconds(1);
			list.Add(new OracleParameter("LAST_LOGIN_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("LAST_LOGIN_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (!pLastPointUsedDayFrom.Equals("")) {
			string sLastPointUsedDayQuery = string.Format("({0}(VW_USER_MAN_CHARACTER03.LAST_POINT_USED_DATE >= :LAST_POINT_USED_DATE_FROM AND VW_USER_MAN_CHARACTER03.LAST_POINT_USED_DATE < :LAST_POINT_USED_DATE_TO){1})",
				this.GetNegative(pLastPointUsedDayNegativeFlag),
				pLastPointUsedDayNegativeFlag ? " OR VW_USER_MAN_CHARACTER03.LAST_POINT_USED_DATE IS NULL" : string.Empty);
			SysPrograms.SqlAppendWhere(sLastPointUsedDayQuery,ref pWhere);
			DateTime dtFrom = DateTime.Parse(pLastPointUsedDayFrom + " " + pLastPointUsedTimeFrom + ":00:00");
			DateTime dtTo = DateTime.Parse(pLastPointUsedDayTo + " " + pLastPointUsedTimeTo + ":59:59").AddSeconds(1);
			list.Add(new OracleParameter("LAST_POINT_USED_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("LAST_POINT_USED_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (!pFirstReceiptDayFrom.Equals("")) {
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.FIRST_RECEIPT_DAY >= :FIRST_RECEIPT_DAY_FROM AND VW_USER_MAN_CHARACTER03.FIRST_RECEIPT_DAY <= :FIRST_RECEIPT_DAY_TO)",ref pWhere);
			list.Add(new OracleParameter("FIRST_RECEIPT_DAY_FROM",pFirstReceiptDayFrom));
			list.Add(new OracleParameter("FIRST_RECEIPT_DAY_TO",pFirstReceiptDayTo));
		}

		if (!pLastReceiptDayFrom.Equals("")) {
			DateTime dtFrom = DateTime.Parse(pLastReceiptDayFrom + " " + "00:00:00");
			DateTime dtTo = DateTime.Parse(pLastReceiptDayTo + " " + "23:59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.LAST_RECEIPT_DATE >= :LAST_RECEIPT_DATE_FROM AND VW_USER_MAN_CHARACTER03.LAST_RECEIPT_DATE <= :LAST_RECEIPT_DATE_TO)",ref pWhere);
			list.Add(new OracleParameter("LAST_RECEIPT_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("LAST_RECEIPT_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		//メールアドレス状態
		iCnt = 0;
		if (pNonExistMailAddrFlagMask != 0) {
			if ((pNonExistMailAddrFlagMask & ViCommConst.MSK_MAIL_ADDR_OK) != 0) {
				sValues[iCnt++] = ViCommConst.MAIL_ADDR_OK.ToString();
			}
			if ((pNonExistMailAddrFlagMask & ViCommConst.MSK_MAIL_ADDR_FILTERING_ERROR) != 0) {
				sValues[iCnt++] = ViCommConst.MAIL_ADDR_FILTERING_ERROR.ToString();
			}
			if ((pNonExistMailAddrFlagMask & ViCommConst.MSK_MAIL_ADDR_NG) != 0) {
				sValues[iCnt++] = ViCommConst.MAIL_ADDR_NG.ToString();
			}

			pWhere = pWhere + " AND (VW_USER_MAN_CHARACTER03.NON_EXIST_MAIL_ADDR_FLAG IN (";
			for (int i = 0;i < iCnt;i++) {
				pWhere = pWhere + ":NON_EXIST_MAIL_ADDR_FLAG" + i.ToString() + ",";
				list.Add(new OracleParameter("NON_EXIST_MAIL_ADDR_FLAG",sValues[i]));
			}
			pWhere = pWhere.Substring(0,pWhere.Length - 1) + ")) ";
		}
		
		if (!string.IsNullOrEmpty(pTxMuller3NgMailAddrFlag)) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.TX_MULLER3_NG_MAIL_ADDR_FLAG = :TX_MULLER3_NG_MAIL_ADDR_FLAG",ref pWhere);
			list.Add(new OracleParameter("TX_MULLER3_NG_MAIL_ADDR_FLAG",pTxMuller3NgMailAddrFlag));
		}

		if (!pLastBlackDayFrom.Equals("")) {
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.LAST_BLACK_DAY >= :LAST_BLACK_DAY_FROM AND VW_USER_MAN_CHARACTER03.LAST_BLACK_DAY <= :LAST_BLACK_DAY_TO)",ref pWhere);
			list.Add(new OracleParameter("LAST_BLACK_DAY_FROM",pLastBlackDayFrom));
			list.Add(new OracleParameter("LAST_BLACK_DAY_TO",pLastBlackDayTo));
		}

		if (!string.IsNullOrEmpty(pUrgeLevel)) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.URGE_LEVEL = :URGE_LEVEL",ref pWhere);
			list.Add(new OracleParameter("URGE_LEVEL",pUrgeLevel));
		}

		if (pMchaUsedFlag) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.POINT_AFFILIATE_TOTAL_COUNT > :POINT_AFFILIATE_TOTAL_COUNT",ref pWhere);
			list.Add(new OracleParameter("POINT_AFFILIATE_TOTAL_COUNT","0"));
		}

		if (pCreditUsedFlag) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.EXIST_CREDIT_DEAL_FLAG = :EXIST_CREDIT_DEAL_FLAG",ref pWhere);
			list.Add(new OracleParameter("EXIST_CREDIT_DEAL_FLAG",ViCommConst.FLAG_ON));
		}

		if (!pGuid.Equals("")) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.IMODE_ID = :IMODE_ID",ref pWhere);
			list.Add(new OracleParameter("IMODE_ID",pGuid));
		}
		if (pPointAffiFlag) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.POINT_AFFILIATE_TOTAL_AMT > 0 AND VW_USER_MAN_CHARACTER03.TOTAL_RECEIPT_AMT = 0 ",ref pWhere);
		}

		if (!string.IsNullOrEmpty(pCastMailRxType)) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.CAST_MAIL_RX_TYPE = :CAST_MAIL_RX_TYPE",ref pWhere);
			list.Add(new OracleParameter("CAST_MAIL_RX_TYPE",pCastMailRxType));
		}

		if (!string.IsNullOrEmpty(pInfoMailRxType)) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.INFO_MAIL_RX_TYPE = :INFO_MAIL_RX_TYPE",ref pWhere);
			list.Add(new OracleParameter("INFO_MAIL_RX_TYPE",pInfoMailRxType));
		}

		if (!string.IsNullOrEmpty(pIntroducerFriendCd)) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.INTRODUCER_FRIEND_CD = :INTRODUCER_FRIEND_CD",ref pWhere);
			list.Add(new OracleParameter("INTRODUCER_FRIEND_CD",pIntroducerFriendCd));
		} else if (pIntroducerFriendAllFlag) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.INTRODUCER_FRIEND_CD IS NOT NULL", ref pWhere);
		}

		if (!string.IsNullOrEmpty(pRegistCarrier)) {
			SysPrograms.SqlAppendWhere("UPPER(VW_USER_MAN_CHARACTER03.REGIST_CARRIER_NM || VW_USER_MAN_CHARACTER03.REGIST_TERMINAL_NM) LIKE '%' || UPPER(:REGIST_CARRIER) || '%'",ref pWhere);
			list.Add(new OracleParameter("REGIST_CARRIER",pRegistCarrier));
		}

		if (!string.IsNullOrEmpty(pMobileCarrier)) {
			SysPrograms.SqlAppendWhere("UPPER(VW_USER_MAN_CHARACTER03.MOBILE_CARRIER_NM || VW_USER_MAN_CHARACTER03.MOBILE_TERMINAL_NM) LIKE '%' || UPPER(:MOBILE_CARRIER) || '%'",ref pWhere);
			list.Add(new OracleParameter("MOBILE_CARRIER",pMobileCarrier));
		}
		if (!string.IsNullOrEmpty(pBeforeSystemId)) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.BEFORE_SYSTEM_ID  LIKE :BEFORE_SYSTEM_ID||'%'",ref pWhere);
			list.Add(new OracleParameter("BEFORE_SYSTEM_ID",pBeforeSystemId));
		}

		if (!string.IsNullOrEmpty(pReportAffiliateDateFrom)) {
			DateTime dtFrom = DateTime.Parse(pReportAffiliateDateFrom + " " + "00:00:00");
			DateTime dtTo = DateTime.Parse(pReportAffiliateDateTo + " " + "23:59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_SITE_USER SU2 WHERE SU2.SITE_CD = VW_USER_MAN_CHARACTER03.SITE_CD AND SU2.USER_SEQ = VW_USER_MAN_CHARACTER03.USER_SEQ AND SU2.USER_CHAR_NO = VW_USER_MAN_CHARACTER03.USER_CHAR_NO AND SU2.REPORT_AFFILIATE_DATE >= :REPORT_AFFILIATE_DATE_FROM AND SU2.REPORT_AFFILIATE_DATE < :REPORT_AFFILIATE_DATE_TO)",ref pWhere);
			list.Add(new OracleParameter("REPORT_AFFILIATE_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("REPORT_AFFILIATE_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}


		if (!pServicePointGetCountFrom.Equals("")) {
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.SERVICE_POINT_GET_COUNT >= :SERVICE_POINT_GET_COUNT_FROM AND VW_USER_MAN_CHARACTER03.SERVICE_POINT_GET_COUNT <= :SERVICE_POINT_GET_COUNT_TO)",ref pWhere);
			list.Add(new OracleParameter("SERVICE_POINT_GET_COUNT_FROM",int.Parse(pServicePointGetCountFrom)));
			list.Add(new OracleParameter("SERVICE_POINT_GET_COUNT_TO",int.Parse(pServicePointGetCountTo)));
		}

		if (!pChkExistAddrExeCountFrom.Equals("")) {
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.CHECK_EXIST_ADDR_EXE_COUNT >= :EXIST_ADDR_EXE_COUNT_FROM AND VW_USER_MAN_CHARACTER03.CHECK_EXIST_ADDR_EXE_COUNT <= :EXIST_ADDR_EXE_COUNT_TO)",ref pWhere);
			list.Add(new OracleParameter("EXIST_ADDR_EXE_COUNT_FROM",int.Parse(pChkExistAddrExeCountFrom)));
			list.Add(new OracleParameter("EXIST_ADDR_EXE_COUNT_TO",int.Parse(pChkExistAddrExeCountTo)));
		}

		if (!pCrosmileLastUsedVersionFrom.Equals("")) {
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.CROSMILE_LAST_USED_VERSION >= :LAST_USED_VERSION_FROM AND VW_USER_MAN_CHARACTER03.CROSMILE_LAST_USED_VERSION <= :LAST_USED_VERSION_TO)",ref pWhere);
			list.Add(new OracleParameter("LAST_USED_VERSION_FROM",pCrosmileLastUsedVersionFrom));
			list.Add(new OracleParameter("LAST_USED_VERSION_TO",pCrosmileLastUsedVersionTo));
		}

		if (!string.IsNullOrEmpty(pUseCrosmileFlag)) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.USE_CROSMILE_FLAG = :USE_CROSMILE_FLAG",ref pWhere);
			list.Add(new OracleParameter("USE_CROSMILE_FLAG",pUseCrosmileFlag));
		}

		if (!string.IsNullOrEmpty(pUseVoiceappFlag)) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.USE_VOICEAPP_FLAG = :USE_VOICEAPP_FLAG",ref pWhere);
			list.Add(new OracleParameter("USE_VOICEAPP_FLAG",pUseVoiceappFlag));
		}

		if (!pGcappLastLoginVersionFrom.Equals("")) {
			SysPrograms.SqlAppendWhere("(VW_USER_MAN_CHARACTER03.GCAPP_LAST_LOGIN_VERSION >= :GCAPP_LAST_LOGIN_VERSION_FROM AND VW_USER_MAN_CHARACTER03.GCAPP_LAST_LOGIN_VERSION <= :GCAPP_LAST_LOGIN_VERSION_TO)",ref pWhere);
			list.Add(new OracleParameter("GCAPP_LAST_LOGIN_VERSION_FROM",pGcappLastLoginVersionFrom));
			list.Add(new OracleParameter("GCAPP_LAST_LOGIN_VERSION_TO",pGcappLastLoginVersionTo));
		}

		if (pNaFlag.Equals(ViCommConst.WITHOUT)) {
			//条件に含めない 
		} else {
			if (!pNaFlag.Equals(string.Empty)) {
				StringBuilder oClauseBuilder = new StringBuilder();
				if (!pWhere.Equals(string.Empty)) {
					oClauseBuilder.Append(" AND ");
				}
				oClauseBuilder.Append(" VW_USER_MAN_CHARACTER03.NA_FLAG IN (");
				int iNum = 1;
				foreach (string sNaFlag in pNaFlag.Split(',')) {
					string sParamName = string.Format(":NA_FLAG{0}",iNum);

					if (iNum != 1)
						oClauseBuilder.Append(",");
					oClauseBuilder.Append(sParamName);
					list.Add(new OracleParameter(sParamName,sNaFlag));

					iNum += 1;
				}
				oClauseBuilder.Append(" )").AppendLine();
				pWhere += oClauseBuilder.ToString();
			} else {
			}
		}

		if (pTalkDirectSmartNotMonitorFlag) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.TALK_SMART_DIRECT_NOT_MONITOR = :TALK_SMART_DIRECT_NOT_MONITOR",ref pWhere);
			list.Add(new OracleParameter("TALK_SMART_DIRECT_NOT_MONITOR",ViCommConst.FLAG_ON_STR));
		}

		DataSet ds = new DataSet();
		using (UserManAttrType oUserManAttrType = new UserManAttrType()) {
			ds = oUserManAttrType.GetInqAdminManAttrType(pSiteCd);
		}
		string[] sManAttrValue = { pManAttrValue1,pManAttrValue2,pManAttrValue3,pManAttrValue4 };
		for (int i = 0;i < ViCommConst.MAX_ADMIN_MAN_INQUIRY_ITEMS;i++) {
			if (!sManAttrValue[i].Equals("")) {
				pWhere = pWhere + string.Format(" AND AT{0}.SITE_CD				= VW_USER_MAN_CHARACTER03.SITE_CD		",i + 1);
				pWhere = pWhere + string.Format(" AND AT{0}.USER_SEQ			= VW_USER_MAN_CHARACTER03.USER_SEQ		",i + 1);
				pWhere = pWhere + string.Format(" AND AT{0}.USER_CHAR_NO		= VW_USER_MAN_CHARACTER03.USER_CHAR_NO	",i + 1);
				pWhere = pWhere + string.Format(" AND AT{0}.MAN_ATTR_TYPE_SEQ	= :MAN_ATTR_TYPE_SEQ{0} ",i + 1);
				list.Add(new OracleParameter(string.Format("MAN_ATTR_TYPE_SEQ{0}",i + 1),ds.Tables[0].Rows[0][string.Format("MAN_ATTR_TYPE_SEQ{0}",i + 1)]));
				if (ds.Tables[0].Rows[0][string.Format("GROUPING_CATEGORY_CD{0}",i + 1)].ToString().Equals("")) {
					pWhere = pWhere + string.Format(" AND AT{0}.MAN_ATTR_SEQ = :MAN_ATTR_SEQ{0}		",i + 1);
					list.Add(new OracleParameter(string.Format("MAN_ATTR_SEQ{0}",i + 1),sManAttrValue[i]));
				} else {
					pWhere = pWhere + string.Format(" AND AT{0}.GROUPING_CD		= :GROUPING_CD{0}	",i + 1);
					list.Add(new OracleParameter(string.Format("GROUPING_CD{0}",i + 1),sManAttrValue[i]));
				}
			}
		}
		if (!string.IsNullOrEmpty(pSiteUseStatus)) {
			SysPrograms.SqlAppendWhere(string.Format("EXISTS (SELECT 1 FROM T_USER_MAN_CHARACTER_EX UCE WHERE UCE.SITE_CD = VW_USER_MAN_CHARACTER03.SITE_CD AND UCE.USER_SEQ = VW_USER_MAN_CHARACTER03.USER_SEQ AND UCE.USER_CHAR_NO = VW_USER_MAN_CHARACTER03.USER_CHAR_NO AND UCE.SITE_USE_STATUS IN ({0}))",pSiteUseStatus),ref pWhere);
		}

		if (!string.IsNullOrEmpty(pUserDefineMask)) {
			SysPrograms.SqlAppendWhere(this.GetNegative(pUserDefineMaskNegatieFlag) + "BITAND(VW_USER_MAN_CHARACTER03.USER_DEFINE_MASK, :USER_DEFINE_MASK) > 0",ref pWhere);
			list.Add(new OracleParameter("USER_DEFINE_MASK",pUserDefineMask));
		}

		if (!string.IsNullOrEmpty(pExceptRefusedByCastUserSeq)) {
			SysPrograms.SqlAppendWhere("NOT EXISTS (SELECT 1 FROM T_REFUSE WHERE T_REFUSE.SITE_CD = VW_USER_MAN_CHARACTER03.SITE_CD AND T_REFUSE.USER_SEQ = :USER_SEQ AND T_REFUSE.USER_CHAR_NO = :USER_CHAR_NO AND T_REFUSE.PARTNER_USER_SEQ = VW_USER_MAN_CHARACTER03.USER_SEQ AND T_REFUSE.PARTNER_USER_CHAR_NO = VW_USER_MAN_CHARACTER03.USER_CHAR_NO )",ref pWhere);
			list.Add(new OracleParameter("USER_SEQ",pExceptRefusedByCastUserSeq));
			list.Add(new OracleParameter("USER_CHAR_NO",ViCommConst.MAIN_CHAR_NO));
			SysPrograms.SqlAppendWhere("NOT EXISTS (SELECT 1 FROM T_REFUSE WHERE T_REFUSE.SITE_CD = VW_USER_MAN_CHARACTER03.SITE_CD AND T_REFUSE.USER_SEQ = VW_USER_MAN_CHARACTER03.USER_SEQ AND T_REFUSE.USER_CHAR_NO = VW_USER_MAN_CHARACTER03.USER_CHAR_NO AND T_REFUSE.PARTNER_USER_SEQ = :PARTNER_USER_SEQ AND T_REFUSE.PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO )",ref pWhere);
			list.Add(new OracleParameter("PARTNER_USER_SEQ",pExceptRefusedByCastUserSeq));
			list.Add(new OracleParameter("PARTNER_USER_CHAR_NO",ViCommConst.MAIN_CHAR_NO));
		}

		if (!string.IsNullOrEmpty(pGameHandleNm)) {
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_GAME_CHARACTER GC WHERE GC.SITE_CD = VW_USER_MAN_CHARACTER03.SITE_CD AND GC.USER_SEQ = VW_USER_MAN_CHARACTER03.USER_SEQ AND GC.USER_CHAR_NO = VW_USER_MAN_CHARACTER03.USER_CHAR_NO AND GC.GAME_HANDLE_NM LIKE '%' || :GAME_HANDLE_NM || '%')",ref pWhere);
			list.Add(new OracleParameter("GAME_HANDLE_NM",pGameHandleNm));
		}

		if (!string.IsNullOrEmpty(pGameCharacterType)) {
			SysPrograms.SqlAppendWhere(string.Format("EXISTS (SELECT 1 FROM T_GAME_CHARACTER GC WHERE GC.SITE_CD = VW_USER_MAN_CHARACTER03.SITE_CD AND GC.USER_SEQ = VW_USER_MAN_CHARACTER03.USER_SEQ AND GC.USER_CHAR_NO = VW_USER_MAN_CHARACTER03.USER_CHAR_NO AND GC.GAME_CHARACTER_TYPE IN ({0}))",pGameCharacterType),ref pWhere);
		}

		if (!string.IsNullOrEmpty(pGameRegistDateFrom) && !string.IsNullOrEmpty(pGameRegistDateTo)) {
			DateTime dtFrom = DateTime.Parse(pGameRegistDateFrom + " 00:00:00");
			DateTime dtTo = DateTime.Parse(pGameRegistDateTo + " 23:59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_GAME_CHARACTER GC WHERE GC.SITE_CD = VW_USER_MAN_CHARACTER03.SITE_CD AND GC.USER_SEQ = VW_USER_MAN_CHARACTER03.USER_SEQ AND GC.USER_CHAR_NO = VW_USER_MAN_CHARACTER03.USER_CHAR_NO AND GC.REGIST_DATE >= :GAME_REGIST_DATE_FROM AND GC.REGIST_DATE <= :GAME_REGIST_DATE_TO)",ref pWhere);
			list.Add(new OracleParameter("GAME_REGIST_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("GAME_REGIST_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		} else if (!string.IsNullOrEmpty(pGameRegistDateFrom)) {
			DateTime dtFrom = DateTime.Parse(pGameRegistDateFrom + " 00:00:00");
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_GAME_CHARACTER GC WHERE GC.SITE_CD = VW_USER_MAN_CHARACTER03.SITE_CD AND GC.USER_SEQ = VW_USER_MAN_CHARACTER03.USER_SEQ AND GC.USER_CHAR_NO = VW_USER_MAN_CHARACTER03.USER_CHAR_NO AND GC.REGIST_DATE >= :GAME_REGIST_DATE_FROM)",ref pWhere);
			list.Add(new OracleParameter("GAME_REGIST_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
		} else if (!string.IsNullOrEmpty(pGameRegistDateTo)) {
			DateTime dtTo = DateTime.Parse(pGameRegistDateTo + " 23:59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_GAME_CHARACTER GC WHERE GC.SITE_CD = VW_USER_MAN_CHARACTER03.SITE_CD AND GC.USER_SEQ = VW_USER_MAN_CHARACTER03.USER_SEQ AND GC.USER_CHAR_NO = VW_USER_MAN_CHARACTER03.USER_CHAR_NO AND GC.REGIST_DATE <= :GAME_REGIST_DATE_TO)",ref pWhere);
			list.Add(new OracleParameter("GAME_REGIST_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (!string.IsNullOrEmpty(pGameCharacterLevelFrom) && !string.IsNullOrEmpty(pGameCharacterLevelTo)) {
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_GAME_CHARACTER GC WHERE GC.SITE_CD = VW_USER_MAN_CHARACTER03.SITE_CD AND GC.USER_SEQ = VW_USER_MAN_CHARACTER03.USER_SEQ AND GC.USER_CHAR_NO = VW_USER_MAN_CHARACTER03.USER_CHAR_NO AND GC.GAME_CHARACTER_LEVEL >= :GAME_CHARACTER_LEVEL_FROM AND GC.GAME_CHARACTER_LEVEL <= :GAME_CHARACTER_LEVEL_TO)",ref pWhere);
			list.Add(new OracleParameter("GAME_CHARACTER_LEVEL_FROM",int.Parse(pGameCharacterLevelFrom)));
			list.Add(new OracleParameter("GAME_CHARACTER_LEVEL_TO",int.Parse(pGameCharacterLevelTo)));
		} else if (!string.IsNullOrEmpty(pGameCharacterLevelFrom)) {
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_GAME_CHARACTER GC WHERE GC.SITE_CD = VW_USER_MAN_CHARACTER03.SITE_CD AND GC.USER_SEQ = VW_USER_MAN_CHARACTER03.USER_SEQ AND GC.USER_CHAR_NO = VW_USER_MAN_CHARACTER03.USER_CHAR_NO AND GC.GAME_CHARACTER_LEVEL >= :GAME_CHARACTER_LEVEL_FROM)",ref pWhere);
			list.Add(new OracleParameter("GAME_CHARACTER_LEVEL_FROM",int.Parse(pGameCharacterLevelFrom)));
		} else if (!string.IsNullOrEmpty(pGameCharacterLevelTo)) {
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_GAME_CHARACTER GC WHERE GC.SITE_CD = VW_USER_MAN_CHARACTER03.SITE_CD AND GC.USER_SEQ = VW_USER_MAN_CHARACTER03.USER_SEQ AND GC.USER_CHAR_NO = VW_USER_MAN_CHARACTER03.USER_CHAR_NO AND GC.GAME_CHARACTER_LEVEL <= :GAME_CHARACTER_LEVEL_TO)",ref pWhere);
			list.Add(new OracleParameter("GAME_CHARACTER_LEVEL_TO",int.Parse(pGameCharacterLevelTo)));
		}

		if (!string.IsNullOrEmpty(pGamePointFrom) && !string.IsNullOrEmpty(pGamePointTo)) {
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_GAME_CHARACTER GC WHERE GC.SITE_CD = VW_USER_MAN_CHARACTER03.SITE_CD AND GC.USER_SEQ = VW_USER_MAN_CHARACTER03.USER_SEQ AND GC.USER_CHAR_NO = VW_USER_MAN_CHARACTER03.USER_CHAR_NO AND GC.GAME_POINT >= :GAME_POINT_FROM AND GC.GAME_POINT <= :GAME_POINT_TO)",ref pWhere);
			list.Add(new OracleParameter("GAME_POINT_FROM",pGamePointFrom));
			list.Add(new OracleParameter("GAME_POINT_TO",pGamePointTo));
		} else if (!string.IsNullOrEmpty(pGamePointFrom)) {
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_GAME_CHARACTER GC WHERE GC.SITE_CD = VW_USER_MAN_CHARACTER03.SITE_CD AND GC.USER_SEQ = VW_USER_MAN_CHARACTER03.USER_SEQ AND GC.USER_CHAR_NO = VW_USER_MAN_CHARACTER03.USER_CHAR_NO AND GC.GAME_POINT >= :GAME_POINT_FROM)",ref pWhere);
			list.Add(new OracleParameter("GAME_POINT_FROM",pGamePointFrom));
		} else if (!string.IsNullOrEmpty(pGamePointTo)) {
			SysPrograms.SqlAppendWhere("EXISTS (SELECT 1 FROM T_GAME_CHARACTER GC WHERE GC.SITE_CD = VW_USER_MAN_CHARACTER03.SITE_CD AND GC.USER_SEQ = VW_USER_MAN_CHARACTER03.USER_SEQ AND GC.USER_CHAR_NO = VW_USER_MAN_CHARACTER03.USER_CHAR_NO AND GC.GAME_POINT <= :GAME_POINT_TO)",ref pWhere);
			list.Add(new OracleParameter("GAME_POINT_TO",pGamePointTo));
		}

		if (!string.IsNullOrEmpty(pKeyword)) {
			StringBuilder oWhereKeywords = new StringBuilder("(");
			string[] sKeywordArray = Regex.Split(pKeyword," |　");
			for (int i = 0;i < sKeywordArray.Length;i++) {
				oWhereKeywords.AppendFormat("EXISTS (SELECT 1 FROM T_MAN_ATTR_VALUE WHERE SITE_CD = VW_USER_MAN_CHARACTER03.SITE_CD AND USER_SEQ = VW_USER_MAN_CHARACTER03.USER_SEQ AND USER_CHAR_NO = VW_USER_MAN_CHARACTER03.USER_CHAR_NO AND MAN_ATTR_INPUT_VALUE LIKE :KEYWORD{0})",i);
				list.Add(new OracleParameter(string.Format("KEYWORD{0}",i),string.Format("%{0}%",sKeywordArray[i])));

				if (i < sKeywordArray.Length - 1) {
					oWhereKeywords.AppendLine(" OR ");
				}
			}
			oWhereKeywords.Append(")");
			SysPrograms.SqlAppendWhere(oWhereKeywords.ToString(),ref pWhere);
		}
		
		if (!string.IsNullOrEmpty(pUserTestType)) {
			SysPrograms.SqlAppendWhere("MOD(VW_USER_MAN_CHARACTER03.USER_SEQ,2) = :USER_TEST_TYPE",ref pWhere);
			list.Add(new OracleParameter("USER_TEST_TYPE",int.Parse(pUserTestType)));
		}
		
		if (pNotExistFirstPointUsedDate) {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.FIRST_POINT_USED_DATE IS NULL",ref pWhere);
		}
		
		//ポイント利用
		if (pFirstPointUseFlag == "1") {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.FIRST_POINT_USED_DATE IS NOT NULL",ref pWhere);
		} 
		else if (pFirstPointUseFlag == "0") {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.FIRST_POINT_USED_DATE IS NULL",ref pWhere);
		}

		//承認有無
		if (pAuthFlag == "1") {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.REGIST_SERVICE_POINT_FLAG = 1",ref pWhere);
		} 
		else if (pAuthFlag == "0") {
			SysPrograms.SqlAppendWhere("VW_USER_MAN_CHARACTER03.REGIST_SERVICE_POINT_FLAG = 0",ref pWhere);
		}

		// リッチーノランク
		if (!string.IsNullOrEmpty(pRichinoRankFrom) || !string.IsNullOrEmpty(pRichinoRankTo)) {
			SysPrograms.SqlAppendWhere("T_USER_MAN_CHARACTER_EX.USER_SEQ = VW_USER_MAN_CHARACTER03.USER_SEQ",ref pWhere);
		}
		if (!string.IsNullOrEmpty(pRichinoRankFrom)) {
			SysPrograms.SqlAppendWhere("NVL(T_USER_MAN_CHARACTER_EX.RICHINO_RANK, 0) >= :RICHINO_RANK_FROM",ref pWhere);
			list.Add(new OracleParameter("RICHINO_RANK_FROM",pRichinoRankFrom));
		}
		if (!string.IsNullOrEmpty(pRichinoRankTo)) {
			SysPrograms.SqlAppendWhere("NVL(T_USER_MAN_CHARACTER_EX.RICHINO_RANK, 0) <= :RICHINO_RANK_TO",ref pWhere);
			list.Add(new OracleParameter("RICHINO_RANK_TO",pRichinoRankTo));
		}

		// ﾎﾟｲﾝﾄｱﾌﾘ決済日
		if (!string.IsNullOrEmpty(pMchaReceiptDateFrom) || !string.IsNullOrEmpty(pMchaReceiptDateTo)) {
			StringBuilder oExistSql = new StringBuilder();
			oExistSql.Append("EXISTS(SELECT * FROM T_SETTLE_LOG");
			oExistSql.Append(" WHERE SETTLE_TYPE = :SETTLE_TYPE_PA");
			oExistSql.Append(" AND SETTLE_STATUS = :SETTLE_STATUS_PA");
			if (!string.IsNullOrEmpty(pMchaReceiptDateFrom)) {
				oExistSql.Append(" AND CREATE_DATE >= TO_DATE(:SETTLE_PA_CREATE_DATE_FROM,'YYYY/MM/DD')");
			}
			if (!string.IsNullOrEmpty(pMchaReceiptDateTo)) {
				oExistSql.Append(" AND CREATE_DATE < TO_DATE(:SETTLE_PA_CREATE_DATE_TO,'YYYY/MM/DD')");
			}
			oExistSql.Append(" AND VW_USER_MAN_CHARACTER03.SITE_CD = SITE_CD");
			oExistSql.Append(" AND VW_USER_MAN_CHARACTER03.USER_SEQ = USER_SEQ");
			oExistSql.AppendLine(" GROUP BY SITE_CD,USER_SEQ)");
			SysPrograms.SqlAppendWhere(oExistSql.ToString(),ref pWhere);

			// 決済種別（ポイントアフリ＝A）
			list.Add(new OracleParameter("SETTLE_TYPE_PA",ViCommConst.SETTLE_POINT_AFFILIATE));

			// 決済ステータス（決済OK=0）
			list.Add(new OracleParameter("SETTLE_STATUS_PA",ViCommConst.SETTLE_STAT_OK));

			// 決済日
			if (!string.IsNullOrEmpty(pMchaReceiptDateFrom)) {
				list.Add(new OracleParameter("SETTLE_PA_CREATE_DATE_FROM",pMchaReceiptDateFrom));
			}
			if (!string.IsNullOrEmpty(pMchaReceiptDateTo)) {
				list.Add(new OracleParameter("SETTLE_PA_CREATE_DATE_TO",pMchaReceiptDateTo));
			}
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	private string GetNegative(bool pFlag) {
		return pFlag ? "NOT " : string.Empty;
	}

	private void SetDefaultTimeValue(ref string pTimeFrom,ref string pTimeTo) {
		if (string.IsNullOrEmpty(pTimeFrom)) {
			pTimeFrom = "00";
		}
		if (string.IsNullOrEmpty(pTimeTo)) {
			pTimeTo = "23";
		}
	}

	public DataSet GetOne(string pSiteCd, string pLoginID) {
		return GetOne(pSiteCd,pLoginID,ViCommConst.FLAG_ON_STR);
	}
	public DataSet GetOne(string pSiteCd,string pLoginID,string pTelAttestedFlag) {
		return GetOne(pSiteCd,pLoginID,pTelAttestedFlag,string.Empty);
	}
	public DataSet GetOne(string pSiteCd,string pLoginID,string pTelAttestedFlag,string pUserSeq) {
		if (string.IsNullOrEmpty(pLoginID) && string.IsNullOrEmpty(pUserSeq)) {
			throw new ArgumentException();
		}

		string sWhereClause = string.Empty;
		StringBuilder sSql = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref sWhereClause);
			oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		}

		if (!string.IsNullOrEmpty(pLoginID)) {
			if (pLoginID.IndexOf('@') >= 0) {
				SysPrograms.SqlAppendWhere("EMAIL_ADDR LIKE :ID ||'%'",ref sWhereClause);
				pLoginID = pLoginID.ToLower();
				if (pLoginID.EndsWith("@")) {
					pLoginID = pLoginID.Substring(0,pLoginID.Length - 1);
				}
			} else if ((pLoginID.StartsWith("070")) || (pLoginID.StartsWith("080")) || (pLoginID.StartsWith("090"))) {
				SysPrograms.SqlAppendWhere("TEL = :ID",ref sWhereClause);

				if (!string.IsNullOrEmpty(pTelAttestedFlag)) {
					SysPrograms.SqlAppendWhere("TEL_ATTESTED_FLAG = :TEL_ATTESTED_FLAG",ref sWhereClause);
					oParamList.Add(new OracleParameter(":TEL_ATTESTED_FLAG",pTelAttestedFlag));
				}
			} else {
				SysPrograms.SqlAppendWhere("(LOGIN_ID = :ID OR PREVIOUS_LOGIN_ID = :ID)",ref sWhereClause);
			}

			oParamList.Add(new OracleParameter(":ID",pLoginID));
		}

		if (!string.IsNullOrEmpty(pUserSeq)) {
			SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref sWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		}

		sSql.AppendLine("SELECT");
		sSql.AppendLine("	" + QueryFields());
		sSql.AppendLine("FROM");
		sSql.AppendLine("	VW_USER_MAN_CHARACTER03");
		sSql.AppendLine(sWhereClause);

		DataSet ds = ExecuteSelectQueryBase(sSql.ToString(),oParamList.ToArray());

		if (ds.Tables[0].Rows.Count != 0) {
			using (UserManAttrValue oAttr = new UserManAttrValue()) {
				DataSet dsSub = oAttr.GetList(pSiteCd,ds.Tables[0].Rows[0]["USER_SEQ"].ToString(),ViCommConst.MAIN_CHAR_NO);
				int i = 0;
				foreach (DataRow dr in dsSub.Tables[0].Rows) {
					ds.Tables[0].Columns.Add(new DataColumn("MAN_ATTR_TYPE_NM" + i.ToString(),System.Type.GetType("System.String")));
					ds.Tables[0].Columns.Add(new DataColumn("DISPLAY_VALUE" + i.ToString(),System.Type.GetType("System.String")));
					ds.Tables[0].Rows[0]["MAN_ATTR_TYPE_NM" + i.ToString()] = dr["MAN_ATTR_TYPE_NM"];
					ds.Tables[0].Rows[0]["DISPLAY_VALUE" + i.ToString()] = dr["DISPLAY_VALUE"];
					i = i + 1;
				}
				ds.Tables[0].Columns.Add(new DataColumn("COUNT",System.Type.GetType("System.String")));
				ds.Tables[0].Rows[0]["COUNT"] = i;
			}
		}

		return ds;
	}

	private string QueryFields() {
		StringBuilder sFiled = new StringBuilder();
		sFiled.Append("SITE_CD						,").AppendLine();
		sFiled.Append("USER_SEQ						,").AppendLine();
		sFiled.Append("USER_CHAR_NO					,").AppendLine();

		sFiled.Append("BAL_POINT					,").AppendLine();
		sFiled.Append("SERVICE_POINT				,").AppendLine();
		sFiled.Append("SERVICE_POINT_EFFECTIVE_DATE	,").AppendLine();
		sFiled.Append("LIMIT_POINT					,").AppendLine();
		sFiled.Append("FIRST_LOGIN_DATE				,").AppendLine();
		sFiled.Append("LAST_LOGIN_DATE				,").AppendLine();
		sFiled.Append("FIRST_POINT_USED_DATE		,").AppendLine();
		sFiled.Append("LAST_POINT_USED_DATE			,").AppendLine();
		sFiled.Append("FIRST_RECEIPT_DAY			,").AppendLine();
		sFiled.Append("LAST_RECEIPT_DATE			,").AppendLine();
		sFiled.Append("RECEIPT_LIMIT_DAY			,").AppendLine();
		sFiled.Append("BILL_AMT						,").AppendLine();
		sFiled.Append("TOTAL_RECEIPT_AMT			,").AppendLine();
		sFiled.Append("TOTAL_RECEIPT_COUNT			,").AppendLine();
		sFiled.Append("MONTHLY_RECEIPT_AMT			,").AppendLine();
		sFiled.Append("MONTHLY_RECEIPT_COUNT		,").AppendLine();
		sFiled.Append("POINT_AFFILIATE_TOTAL_AMT	,").AppendLine();
		sFiled.Append("POINT_AFFILIATE_TOTAL_COUNT	,").AppendLine();
		sFiled.Append("EXIST_CREDIT_DEAL_FLAG		,").AppendLine();
		sFiled.Append("CREDIT_MEASURED_RATE_TARGET	,").AppendLine();
		sFiled.Append("MEASURED_RATE_REGIST_DATE	,").AppendLine();
		sFiled.Append("BLACK_DAY					,").AppendLine();
		sFiled.Append("BLACK_COUNT					,").AppendLine();
		sFiled.Append("BLACK_TIME					,").AppendLine();
		sFiled.Append("LAST_BLACK_DAY				,").AppendLine();
		sFiled.Append("LAST_BLACK_TIME				,").AppendLine();
		sFiled.Append("CAST_MAIL_RX_TYPE			,").AppendLine();
		sFiled.Append("INFO_MAIL_RX_TYPE			,").AppendLine();
		sFiled.Append("LOGIN_MAIL_RX_FLAG1			,").AppendLine();
		sFiled.Append("LOGIN_MAIL_RX_FLAG2			,").AppendLine();
		sFiled.Append("AD_CD						,").AppendLine();
		sFiled.Append("SEX_CD						,").AppendLine();
		sFiled.Append("USE_TERMINAL_TYPE			,").AppendLine();
		sFiled.Append("URI							,").AppendLine();
		sFiled.Append("TERMINAL_PASSWORD			,").AppendLine();
		sFiled.Append("TEL							,").AppendLine();
		sFiled.Append("EMAIL_ADDR					,").AppendLine();
		sFiled.Append("LOGIN_ID						,").AppendLine();
		sFiled.Append("LOGIN_PASSWORD				,").AppendLine();
		sFiled.Append("USER_STATUS					,").AppendLine();
		sFiled.Append("NA_FLAG						,").AppendLine();
		sFiled.Append("TALKING_FLAG					,").AppendLine();
		sFiled.Append("TALK_LOCK_FLAG				,").AppendLine();
		sFiled.Append("TALK_LOCK_DATE				,").AppendLine();
		sFiled.Append("CALL_STATUS_SEQ				,").AppendLine();
		sFiled.Append("IVP_REQUEST_SEQ				,").AppendLine();
		sFiled.Append("LOGIN_SEQ					,").AppendLine();
		sFiled.Append("MOBILE_CARRIER_CD			,").AppendLine();
		sFiled.Append("MOBILE_TERMINAL_SHORT_NM		,").AppendLine();
		sFiled.Append("MOBILE_TERMINAL_NM			,").AppendLine();
		sFiled.Append("REGIST_CARRIER_CD			,").AppendLine();
		sFiled.Append("REGIST_TERMINAL_NM			,").AppendLine();
		sFiled.Append("TERMINAL_UNIQUE_ID			,").AppendLine();
		sFiled.Append("IMODE_ID						,").AppendLine();
		sFiled.Append("HAVE_TERM_UNIQUE_ID_FLAG		,").AppendLine();
		sFiled.Append("TEL_ATTESTED_FLAG			,").AppendLine();
		sFiled.Append("USE_FREE_DIAL_FLAG			,").AppendLine();
		sFiled.Append("NON_EXIST_MAIL_ADDR_FLAG		,").AppendLine();
		sFiled.Append("REGIST_DATE					,").AppendLine();
		sFiled.Append("UPDATE_DATE					,").AppendLine();
		sFiled.Append("REVISION_NO					,").AppendLine();
		sFiled.Append("HANDLE_NM					,").AppendLine();
		sFiled.Append("BIRTHDAY						,").AppendLine();
		sFiled.Append("REMARKS1						,").AppendLine();
		sFiled.Append("REMARKS2						,").AppendLine();
		sFiled.Append("REMARKS3						,").AppendLine();
		sFiled.Append("REMARKS4						,").AppendLine();
		sFiled.Append("REMARKS5						,").AppendLine();
		sFiled.Append("REMARKS6						,").AppendLine();
		sFiled.Append("REMARKS7						,").AppendLine();
		sFiled.Append("REMARKS8						,").AppendLine();
		sFiled.Append("REMARKS9						,").AppendLine();
		sFiled.Append("REMARKS10					,").AppendLine();
		sFiled.Append("INTRODUCER_FRIEND_CD			,").AppendLine();
		sFiled.Append("FRIEND_INTRO_CD				,").AppendLine();
		sFiled.Append("USER_RANK					,").AppendLine();
		sFiled.Append("CHARACTER_ONLINE_STATUS		,").AppendLine();
		sFiled.Append("REVISION_NO_MAN				,").AppendLine();
		sFiled.Append("URGE_LEVEL					,").AppendLine();
		sFiled.Append("MAX_URGE_COUNT				,").AppendLine();
		sFiled.Append("URGE_COUNT_TODAY				,").AppendLine();
		sFiled.Append("URGE_RESTART_DAY				,").AppendLine();
		sFiled.Append("URGE_END_DAY					,").AppendLine();
		sFiled.Append("URGE_EXEC_LAST_DATE			,").AppendLine();
		sFiled.Append("URGE_EXEC_LAST_STATUS		,").AppendLine();
		sFiled.Append("URGE_EXEC_COUNT				,").AppendLine();
		sFiled.Append("URGE_CONNECT_COUNT			,").AppendLine();
		sFiled.Append("REVISION_NO_URGE				,").AppendLine();
		sFiled.Append("AD_NM						,").AppendLine();
		sFiled.Append("MOBILE_CARRIER_NM			,").AppendLine();
		sFiled.Append("REGIST_CARRIER_NM			,").AppendLine();
		sFiled.Append("USER_STATUS_NM				,").AppendLine();
		sFiled.Append("PIC_SEQ						,").AppendLine();
		sFiled.Append("PHOTO_IMG_PATH				,").AppendLine();
		sFiled.Append("USER_RANK_NM					,").AppendLine();
		sFiled.Append("CAST_MAIL_RX_TYPE_NM			,").AppendLine();
		sFiled.Append("INFO_MAIL_RX_TYPE_NM			,").AppendLine();
		sFiled.Append("BEFORE_SYSTEM_ID				,").AppendLine();
		sFiled.Append("USER_DEFINE_MASK				,").AppendLine();
		sFiled.Append("SMALL_PHOTO_IMG_PATH			,").AppendLine();
		sFiled.Append("CROSMILE_LAST_USED_VERSION	,").AppendLine();
		sFiled.Append("DEVICE_TOKEN					,").AppendLine();
		sFiled.Append("DEVICE_UUID					,").AppendLine();
		sFiled.Append("GCAPP_LAST_LOGIN_VERSION		,").AppendLine();
		sFiled.Append("GCAPP_LAST_LOGIN_DATE		,").AppendLine();
		sFiled.Append("REGIST_SERVICE_POINT_FLAG	,").AppendLine();
		sFiled.Append("REGIST_SERVICE_POINT_DATE	,").AppendLine();
		sFiled.Append("TX_MULLER3_NG_MAIL_ADDR_FLAG	,").AppendLine();
		sFiled.Append("CASE WHEN MOD(USER_SEQ,2) = 1 THEN 'A' ELSE 'B' END AS USER_TEST_TYPE	").AppendLine();
		return sFiled.ToString();
	}
	
	//紹介元コードログインID変換用
	private string QueryFieldsIntro() {
		StringBuilder sFiled = new StringBuilder();
		sFiled.Append("VWUSER03.SITE_CD						,").AppendLine();
		sFiled.Append("VWUSER03.USER_SEQ						,").AppendLine();
		sFiled.Append("VWUSER03.USER_CHAR_NO					,").AppendLine();
		sFiled.Append("VWUSER03.CAST_USER_SEQ		,").AppendLine();
		sFiled.Append("VWUSER03.CAST_LOGIN_ID		,").AppendLine();
		sFiled.Append("VWUSER03.CSEQ				,").AppendLine();

		sFiled.Append("VWUSER03.BAL_POINT					,").AppendLine();
		sFiled.Append("VWUSER03.SERVICE_POINT				,").AppendLine();
		sFiled.Append("VWUSER03.SERVICE_POINT_EFFECTIVE_DATE	,").AppendLine();
		sFiled.Append("VWUSER03.LIMIT_POINT					,").AppendLine();
		sFiled.Append("VWUSER03.FIRST_LOGIN_DATE				,").AppendLine();
		sFiled.Append("VWUSER03.LAST_LOGIN_DATE				,").AppendLine();
		sFiled.Append("VWUSER03.FIRST_POINT_USED_DATE		,").AppendLine();
		sFiled.Append("VWUSER03.LAST_POINT_USED_DATE			,").AppendLine();
		sFiled.Append("VWUSER03.FIRST_RECEIPT_DAY			,").AppendLine();
		sFiled.Append("VWUSER03.LAST_RECEIPT_DATE			,").AppendLine();
		sFiled.Append("VWUSER03.RECEIPT_LIMIT_DAY			,").AppendLine();
		sFiled.Append("VWUSER03.BILL_AMT						,").AppendLine();
		sFiled.Append("VWUSER03.TOTAL_RECEIPT_AMT			,").AppendLine();
		sFiled.Append("VWUSER03.TOTAL_RECEIPT_COUNT			,").AppendLine();
		sFiled.Append("VWUSER03.MONTHLY_RECEIPT_AMT			,").AppendLine();
		sFiled.Append("VWUSER03.MONTHLY_RECEIPT_COUNT		,").AppendLine();
		sFiled.Append("VWUSER03.POINT_AFFILIATE_TOTAL_AMT	,").AppendLine();
		sFiled.Append("VWUSER03.POINT_AFFILIATE_TOTAL_COUNT	,").AppendLine();
		sFiled.Append("VWUSER03.EXIST_CREDIT_DEAL_FLAG		,").AppendLine();
		sFiled.Append("VWUSER03.CREDIT_MEASURED_RATE_TARGET	,").AppendLine();
		sFiled.Append("VWUSER03.MEASURED_RATE_REGIST_DATE	,").AppendLine();
		sFiled.Append("VWUSER03.BLACK_DAY					,").AppendLine();
		sFiled.Append("VWUSER03.BLACK_COUNT					,").AppendLine();
		sFiled.Append("VWUSER03.BLACK_TIME					,").AppendLine();
		sFiled.Append("VWUSER03.LAST_BLACK_DAY				,").AppendLine();
		sFiled.Append("VWUSER03.LAST_BLACK_TIME				,").AppendLine();
		sFiled.Append("VWUSER03.CAST_MAIL_RX_TYPE			,").AppendLine();
		sFiled.Append("VWUSER03.INFO_MAIL_RX_TYPE			,").AppendLine();
		sFiled.Append("VWUSER03.LOGIN_MAIL_RX_FLAG1			,").AppendLine();
		sFiled.Append("VWUSER03.LOGIN_MAIL_RX_FLAG2			,").AppendLine();
		sFiled.Append("VWUSER03.AD_CD						,").AppendLine();
		sFiled.Append("VWUSER03.SEX_CD						,").AppendLine();
		sFiled.Append("VWUSER03.USE_TERMINAL_TYPE			,").AppendLine();
		sFiled.Append("VWUSER03.URI							,").AppendLine();
		sFiled.Append("VWUSER03.TERMINAL_PASSWORD			,").AppendLine();
		sFiled.Append("VWUSER03.TEL							,").AppendLine();
		sFiled.Append("VWUSER03.EMAIL_ADDR					,").AppendLine();
		sFiled.Append("VWUSER03.LOGIN_ID						,").AppendLine();
		sFiled.Append("VWUSER03.LOGIN_PASSWORD				,").AppendLine();
		sFiled.Append("VWUSER03.USER_STATUS					,").AppendLine();
		sFiled.Append("VWUSER03.NA_FLAG						,").AppendLine();
		sFiled.Append("VWUSER03.TALKING_FLAG					,").AppendLine();
		sFiled.Append("VWUSER03.TALK_LOCK_FLAG				,").AppendLine();
		sFiled.Append("VWUSER03.TALK_LOCK_DATE				,").AppendLine();
		sFiled.Append("VWUSER03.CALL_STATUS_SEQ				,").AppendLine();
		sFiled.Append("VWUSER03.IVP_REQUEST_SEQ				,").AppendLine();
		sFiled.Append("VWUSER03.LOGIN_SEQ					,").AppendLine();
		sFiled.Append("VWUSER03.MOBILE_CARRIER_CD			,").AppendLine();
		sFiled.Append("VWUSER03.MOBILE_TERMINAL_SHORT_NM		,").AppendLine();
		sFiled.Append("VWUSER03.MOBILE_TERMINAL_NM			,").AppendLine();
		sFiled.Append("VWUSER03.REGIST_CARRIER_CD			,").AppendLine();
		sFiled.Append("VWUSER03.REGIST_TERMINAL_NM			,").AppendLine();
		sFiled.Append("VWUSER03.TERMINAL_UNIQUE_ID			,").AppendLine();
		sFiled.Append("VWUSER03.IMODE_ID						,").AppendLine();
		sFiled.Append("VWUSER03.HAVE_TERM_UNIQUE_ID_FLAG		,").AppendLine();
		sFiled.Append("VWUSER03.TEL_ATTESTED_FLAG			,").AppendLine();
		sFiled.Append("VWUSER03.USE_FREE_DIAL_FLAG			,").AppendLine();
		sFiled.Append("VWUSER03.NON_EXIST_MAIL_ADDR_FLAG		,").AppendLine();
		sFiled.Append("VWUSER03.REGIST_DATE					,").AppendLine();
		sFiled.Append("VWUSER03.UPDATE_DATE					,").AppendLine();
		sFiled.Append("VWUSER03.REVISION_NO					,").AppendLine();
		sFiled.Append("VWUSER03.HANDLE_NM					,").AppendLine();
		sFiled.Append("VWUSER03.BIRTHDAY						,").AppendLine();
		sFiled.Append("VWUSER03.REMARKS1						,").AppendLine();
		sFiled.Append("VWUSER03.REMARKS2						,").AppendLine();
		sFiled.Append("VWUSER03.REMARKS3						,").AppendLine();
		sFiled.Append("VWUSER03.REMARKS4						,").AppendLine();
		sFiled.Append("VWUSER03.REMARKS5						,").AppendLine();
		sFiled.Append("VWUSER03.REMARKS6						,").AppendLine();
		sFiled.Append("VWUSER03.REMARKS7						,").AppendLine();
		sFiled.Append("VWUSER03.REMARKS8						,").AppendLine();
		sFiled.Append("VWUSER03.REMARKS9						,").AppendLine();
		sFiled.Append("VWUSER03.REMARKS10					,").AppendLine();
		sFiled.Append("VWUSER03.INTRODUCER_FRIEND_CD			,").AppendLine();
		sFiled.Append("VWUSER03.FRIEND_INTRO_CD				,").AppendLine();
		sFiled.Append("VWUSER03.USER_RANK					,").AppendLine();
		sFiled.Append("VWUSER03.CHARACTER_ONLINE_STATUS		,").AppendLine();
		sFiled.Append("VWUSER03.REVISION_NO_MAN				,").AppendLine();
		sFiled.Append("VWUSER03.URGE_LEVEL					,").AppendLine();
		sFiled.Append("VWUSER03.MAX_URGE_COUNT				,").AppendLine();
		sFiled.Append("VWUSER03.URGE_COUNT_TODAY				,").AppendLine();
		sFiled.Append("VWUSER03.URGE_RESTART_DAY				,").AppendLine();
		sFiled.Append("VWUSER03.URGE_END_DAY					,").AppendLine();
		sFiled.Append("VWUSER03.URGE_EXEC_LAST_DATE			,").AppendLine();
		sFiled.Append("VWUSER03.URGE_EXEC_LAST_STATUS		,").AppendLine();
		sFiled.Append("VWUSER03.URGE_EXEC_COUNT				,").AppendLine();
		sFiled.Append("VWUSER03.URGE_CONNECT_COUNT			,").AppendLine();
		sFiled.Append("VWUSER03.REVISION_NO_URGE				,").AppendLine();
		sFiled.Append("VWUSER03.AD_NM						,").AppendLine();
		sFiled.Append("VWUSER03.MOBILE_CARRIER_NM			,").AppendLine();
		sFiled.Append("VWUSER03.REGIST_CARRIER_NM			,").AppendLine();
		sFiled.Append("VWUSER03.USER_STATUS_NM				,").AppendLine();
		sFiled.Append("VWUSER03.PIC_SEQ						,").AppendLine();
		sFiled.Append("VWUSER03.PHOTO_IMG_PATH				,").AppendLine();
		sFiled.Append("VWUSER03.USER_RANK_NM					,").AppendLine();
		sFiled.Append("VWUSER03.CAST_MAIL_RX_TYPE_NM			,").AppendLine();
		sFiled.Append("VWUSER03.INFO_MAIL_RX_TYPE_NM			,").AppendLine();
		sFiled.Append("VWUSER03.BEFORE_SYSTEM_ID				,").AppendLine();
		sFiled.Append("VWUSER03.USER_DEFINE_MASK				,").AppendLine();
		sFiled.Append("VWUSER03.SMALL_PHOTO_IMG_PATH			,").AppendLine();
		sFiled.Append("VWUSER03.CROSMILE_LAST_USED_VERSION	,").AppendLine();
		sFiled.Append("VWUSER03.DEVICE_TOKEN					,").AppendLine();
		sFiled.Append("VWUSER03.DEVICE_UUID					,").AppendLine();
		sFiled.Append("VWUSER03.GCAPP_LAST_LOGIN_VERSION		,").AppendLine();
		sFiled.Append("VWUSER03.GCAPP_LAST_LOGIN_DATE		,").AppendLine();
		sFiled.Append("VWUSER03.REGIST_SERVICE_POINT_FLAG	,").AppendLine();
		sFiled.Append("VWUSER03.REGIST_SERVICE_POINT_DATE	,").AppendLine();
		sFiled.Append("CASE WHEN MOD(VWUSER03.USER_SEQ,2) = 1 THEN 'A' ELSE 'B' END AS USER_TEST_TYPE	").AppendLine();
		return sFiled.ToString();
	}

	public bool IsUniqueTel(string pSiteCd,string pTel,string pUserSeq) {
		DataSet ds;
		bool bExist = false;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT SITE_CD,USER_SEQ,USER_STATUS FROM VW_USER_MAN01 WHERE TEL= :TEL AND TEL_ATTESTED_FLAG = 1 ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("TEL",pTel);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USER_MAN01");
					foreach (DataRow dr in ds.Tables[0].Rows) {
						if (dr["SITE_CD"].ToString().Equals(pSiteCd) &&
							!dr["USER_SEQ"].ToString().Equals(pUserSeq) &&
							!dr["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_HOLD)) {
							bExist = true;
							break;
						}
					}
				}
			}
		} finally {
			conn.Close();
		}
		return !bExist;
	}


	public bool IsTerminalUniqueIdDupli(string pSiteCd,string pUserSeq,string pTerminalUniqueId) {
		DataSet ds;
		bool bExist = false;
		try {
			conn = DbConnect();
			ds = new DataSet();

			StringBuilder sSql = new StringBuilder();

			sSql.Append("SELECT																			 ").AppendLine();
			sSql.Append("	T_USER.USER_SEQ						USER_SEQ								,").AppendLine();
			sSql.Append("	T_USER.USER_STATUS					USER_STATUS								 ").AppendLine();
			sSql.Append("FROM																			 ").AppendLine();
			sSql.Append("	T_USER																		,").AppendLine();
			sSql.Append("	T_USER_MAN																	,").AppendLine();
			sSql.Append("	T_USER_MAN_CHARACTER														 ").AppendLine();
			sSql.Append("WHERE																			 ").AppendLine();
			sSql.Append("	T_USER.TERMINAL_UNIQUE_ID			= :TERMINAL_UNIQUE_ID				AND	 ").AppendLine();
			sSql.Append("	(																			 ").AppendLine();
			sSql.Append("		T_USER_MAN_CHARACTER.SITE_CD	IS NULL								 OR	 ").AppendLine();
			sSql.Append("		T_USER_MAN_CHARACTER.SITE_CD	= :SITE_CD								 ").AppendLine();
			sSql.Append("	)																		AND	 ").AppendLine();
			sSql.Append("	T_USER.USER_SEQ						= T_USER_MAN.USER_SEQ			(+)	AND	 ").AppendLine();
			sSql.Append("	T_USER_MAN.USER_SEQ					= T_USER_MAN_CHARACTER.USER_SEQ	(+)		 ").AppendLine();

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":TERMINAL_UNIQUE_ID",pTerminalUniqueId);
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					foreach (DataRow dr in ds.Tables[0].Rows) {
						if (!dr["USER_SEQ"].ToString().Equals(pUserSeq) &&
							!dr["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_HOLD)) {
							bExist = true;
							break;
						}
					}
				}
			}
		} finally {
			conn.Clone();
		}

		return !bExist;
	}

	public bool IsIModeIdDupli(string pSiteCd,string pUserSeq,string pIModeId) {
		DataSet ds;
		bool bExist = false;
		try {
			conn = DbConnect();
			ds = new DataSet();

			StringBuilder sSql = new StringBuilder();

			sSql.Append("SELECT																			 ").AppendLine();
			sSql.Append("	T_USER.USER_SEQ						USER_SEQ								,").AppendLine();
			sSql.Append("	T_USER.USER_STATUS					USER_STATUS								 ").AppendLine();
			sSql.Append("FROM																			 ").AppendLine();
			sSql.Append("	T_USER																		,").AppendLine();
			sSql.Append("	T_USER_MAN																	,").AppendLine();
			sSql.Append("	T_USER_MAN_CHARACTER														 ").AppendLine();
			sSql.Append("WHERE																			 ").AppendLine();
			sSql.Append("	T_USER.IMODE_ID						= :IMODE_ID							AND	 ").AppendLine();
			sSql.Append("	(																			 ").AppendLine();
			sSql.Append("		T_USER_MAN_CHARACTER.SITE_CD	IS NULL								 OR	 ").AppendLine();
			sSql.Append("		T_USER_MAN_CHARACTER.SITE_CD	= :SITE_CD								 ").AppendLine();
			sSql.Append("	)																		AND	 ").AppendLine();
			sSql.Append("	T_USER.USER_SEQ						= T_USER_MAN.USER_SEQ			(+)	AND	 ").AppendLine();
			sSql.Append("	T_USER_MAN.USER_SEQ					= T_USER_MAN_CHARACTER.USER_SEQ	(+)		 ").AppendLine();

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":IMODE_ID",pIModeId);
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					foreach (DataRow dr in ds.Tables[0].Rows) {
						if (!dr["USER_SEQ"].ToString().Equals(pUserSeq) &&
							!dr["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_HOLD)) {
							bExist = true;
							break;
						}
					}
				}
			}
		} finally {
			conn.Clone();
		}

		return !bExist;
	}
	public bool IsEmailDupli(string pSiteCd,string pUserSeq,string pEmailAddr) {
		DataSet ds;
		bool bExist = false;
		try {
			conn = DbConnect();
			ds = new DataSet();

			StringBuilder sSql = new StringBuilder();

			sSql.Append("SELECT																			 ").AppendLine();
			sSql.Append("	T_USER.USER_SEQ						USER_SEQ								,").AppendLine();
			sSql.Append("	T_USER.USER_STATUS					USER_STATUS								 ").AppendLine();
			sSql.Append("FROM																			 ").AppendLine();
			sSql.Append("	T_USER																		,").AppendLine();
			sSql.Append("	T_USER_MAN																	,").AppendLine();
			sSql.Append("	T_USER_MAN_CHARACTER														 ").AppendLine();
			sSql.Append("WHERE																			 ").AppendLine();
			sSql.Append("	T_USER.EMAIL_ADDR					= :EMAIL_ADDR						AND	 ").AppendLine();
			sSql.Append("	(																			 ").AppendLine();
			sSql.Append("		T_USER_MAN_CHARACTER.SITE_CD	IS NULL								 OR	 ").AppendLine();
			sSql.Append("		T_USER_MAN_CHARACTER.SITE_CD	= :SITE_CD								 ").AppendLine();
			sSql.Append("	)																		AND	 ").AppendLine();
			sSql.Append("	T_USER.USER_SEQ						= T_USER_MAN.USER_SEQ			(+)	AND	 ").AppendLine();
			sSql.Append("	T_USER_MAN.USER_SEQ					= T_USER_MAN_CHARACTER.USER_SEQ	(+)		 ").AppendLine();

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":EMAIL_ADDR",pEmailAddr.ToLower());
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					foreach (DataRow dr in ds.Tables[0].Rows) {
						if (!dr["USER_SEQ"].ToString().Equals(pUserSeq) &&
							!dr["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_HOLD)) {
							bExist = true;
							break;
						}
					}
				}
			}
		} finally {
			conn.Clone();
		}

		return !bExist;
	}

	public bool IsTelDupli(string pSiteCd,string pUserSeq,string pTel) {
		DataSet ds;
		bool bExist = false;
		try {
			conn = DbConnect();
			ds = new DataSet();

			StringBuilder sSql = new StringBuilder();

			sSql.Append("SELECT																			 ").AppendLine();
			sSql.Append("	T_USER.USER_SEQ						USER_SEQ								,").AppendLine();
			sSql.Append("	T_USER.USER_STATUS					USER_STATUS								 ").AppendLine();
			sSql.Append("FROM																			 ").AppendLine();
			sSql.Append("	T_USER																		,").AppendLine();
			sSql.Append("	T_USER_MAN																	,").AppendLine();
			sSql.Append("	T_USER_MAN_CHARACTER														 ").AppendLine();
			sSql.Append("WHERE																			 ").AppendLine();
			sSql.Append("	T_USER.TEL							= :TEL								AND	 ").AppendLine();
			sSql.Append("	(																			 ").AppendLine();
			sSql.Append("		T_USER_MAN_CHARACTER.SITE_CD	IS NULL								 OR	 ").AppendLine();
			sSql.Append("		T_USER_MAN_CHARACTER.SITE_CD	= :SITE_CD								 ").AppendLine();
			sSql.Append("	)																		AND	 ").AppendLine();
			sSql.Append("	T_USER.USER_SEQ						= T_USER_MAN.USER_SEQ			(+)	AND	 ").AppendLine();
			sSql.Append("	T_USER_MAN.USER_SEQ					= T_USER_MAN_CHARACTER.USER_SEQ	(+)		 ").AppendLine();

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":TEL",pTel);
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					foreach (DataRow dr in ds.Tables[0].Rows) {
						if (!dr["USER_SEQ"].ToString().Equals(pUserSeq) &&
							!dr["USER_STATUS"].ToString().Equals(ViCommConst.USER_MAN_HOLD)) {
							bExist = true;
							break;
						}
					}
				}
			}
		} finally {
			conn.Clone();
		}

		return !bExist;
	}

	public bool CheckHandleNmDupli(string pSiteCd,string pUserSeq,string pHandleNm) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("CHECK_MAN_HANDLE_NM_DUPLI");
			oDbSession.ProcedureInParm("pSITE_CD",DbType.VARCHAR2,pSiteCd);
			oDbSession.ProcedureInParm("pUSER_SEQ",DbType.VARCHAR2,pUserSeq);
			oDbSession.ProcedureInParm("pHANDLE_NM",DbType.VARCHAR2,pHandleNm);
			oDbSession.ProcedureOutParm("pDUPLI_FLAG",DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			return oDbSession.GetIntValue("pDUPLI_FLAG") == 0;
		}
	}

	public int GetProfilePicPageCount(string pSiteCd,int pUnAuthFlag,string pLoginId,string pUploadDateFrom,string pUploadDateTo) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_USER_MAN_CHARACTER02 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateProfilePicWhere(pSiteCd,pUnAuthFlag,pLoginId,pUploadDateFrom,pUploadDateTo,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				cmd.BindByName = true;
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetProfilePicPageCollection(string pSiteCd,int pUnAuthFlag,string pLoginId,string pUploadDateFrom,string pUploadDateTo,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder;
			if (pUnAuthFlag == 0) {
				sOrder = "ORDER BY UPLOAD_DATE DESC,SITE_CD,PIC_SEQ DESC ";
			} else {
				sOrder = "ORDER BY UNAUTH_UPLOAD_DATE DESC NULLS LAST,SITE_CD,UNAUTH_PIC_SEQ DESC ";
			}

			string sSql = "SELECT " +
								"SITE_CD				," +
								"USER_SEQ				," +
								"USER_CHAR_NO			," +
								"LOGIN_ID				," +
								"HANDLE_NM				,";
			if (pUnAuthFlag == 0) {
				sSql += "UPLOAD_DATE			," +
						"PIC_SEQ				," +
						"SMALL_PHOTO_IMG_PATH	," +
						"PHOTO_IMG_PATH			";

			} else {
				sSql += "UNAUTH_UPLOAD_DATE				UPLOAD_DATE				," +
						"UNAUTH_PIC_SEQ					PIC_SEQ					," +
						"UNAUTH_SMALL_PHOTO_IMG_PATH	SMALL_PHOTO_IMG_PATH	," +
						"UNAUTH_PHOTO_IMG_PATH			PHOTO_IMG_PATH ";
			}

			sSql += "FROM(SELECT VW_USER_MAN_CHARACTER02.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_USER_MAN_CHARACTER02  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateProfilePicWhere(pSiteCd,pUnAuthFlag,pLoginId,pUploadDateFrom,pUploadDateTo,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.BindByName = true;
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USER_MAN_CHARACTER02");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateProfilePicWhere(string pSiteCd,int pUnAuthFlag,string pLoginId,string pUploadDateFrom,string pUploadDateTo,ref string pWhere) {
		ArrayList list = new ArrayList();

		if (pUnAuthFlag == 0) {
			SysPrograms.SqlAppendWhere("PIC_SEQ IS NOT NULL",ref pWhere);
		} else {
			SysPrograms.SqlAppendWhere("UNAUTH_PIC_FLAG = :UNAUTH_PIC_FLAG",ref pWhere);
			list.Add(new OracleParameter("UNAUTH_PIC_FLAG",pUnAuthFlag));
		}

		if (pSiteCd.Equals("") == false) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!string.IsNullOrEmpty(pLoginId)) {
			SysPrograms.SqlAppendWhere("LOGIN_ID = :LOGIN_ID",ref pWhere);
			list.Add(new OracleParameter("LOGIN_ID",pLoginId));
		}

		string sUploadDateColumn = string.Empty;
		if (pUnAuthFlag == 0) {
			sUploadDateColumn = "UPLOAD_DATE";
		} else {
			sUploadDateColumn = "UNAUTH_UPLOAD_DATE";
		}
		// アップロード日時(from)
		if (!string.IsNullOrEmpty(pUploadDateFrom)) {
			SysPrograms.SqlAppendWhere(string.Format("{0} >= :UPLOAD_DATE_FROM",sUploadDateColumn),ref pWhere);
			DateTime dtFrom = DateTime.ParseExact(pUploadDateFrom,"yyyy/MM/dd",null,System.Globalization.DateTimeStyles.None).Date;
			list.Add(new OracleParameter("UPLOAD_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
		}

		// アップロード日時(to)
		if (!string.IsNullOrEmpty(pUploadDateTo)) {
			SysPrograms.SqlAppendWhere(string.Format("{0} < :UPLOAD_DATE_TO",sUploadDateColumn),ref pWhere);
			DateTime dtFrom = DateTime.ParseExact(pUploadDateTo,"yyyy/MM/dd",null,System.Globalization.DateTimeStyles.None).Date.AddDays(1);
			list.Add(new OracleParameter("UPLOAD_DATE_TO",OracleDbType.Date,dtFrom,ParameterDirection.Input));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public DataSet GetOneByFriendIntroCd(string sFriendIntroCd) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT " +
							"SITE_CD	," +
							"LOGIN_ID	" +
							"FROM VW_USER_MAN_CHARACTER01 WHERE FRIEND_INTRO_CD =:FRIEND_INTRO_CD";
			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("FRIEND_INTRO_CD",sFriendIntroCd);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public bool IsExistLoginId(string pSiteCd,string pLoginID,string pUserCharNo) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT USER_SEQ,HANDLE_NM FROM VW_USER_MAN_CHARACTER01 WHERE SITE_CD = :SITE_CD AND LOGIN_ID= :LOGIN_ID ";

			if (pUserCharNo.Equals("") == false) {
				sSql += " AND USER_CHAR_NO = :USER_CHAR_NO ";
			}
			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("LOGIN_ID",pLoginID);
				if (pUserCharNo.Equals("") == false) {
					cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				}

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USER_MAN_CHARACTER01");

					if (ds.Tables["VW_USER_MAN_CHARACTER01"].Rows.Count != 0) {
						dr = ds.Tables["VW_USER_MAN_CHARACTER01"].Rows[0];
						userSeq = dr["USER_SEQ"].ToString();
						handleNm = dr["HANDLE_NM"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public DataSet GetActivatedSiteList(string pUserSeq,string pUserCharNo) {

		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();
			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT																				").AppendLine();
			sSql.Append("	T_SITE.SITE_NM	SITE_NM															").AppendLine();
			sSql.Append("FROM																				").AppendLine();
			sSql.Append("	T_USER_MAN_CHARACTER,															").AppendLine();
			sSql.Append("	T_SITE																			").AppendLine();
			sSql.Append("WHERE																				").AppendLine();
			sSql.Append("		T_SITE.SITE_CD								= T_USER_MAN_CHARACTER.SITE_CD	").AppendLine();
			sSql.Append("	AND T_USER_MAN_CHARACTER.USER_SEQ				= :USER_SEQ						").AppendLine();
			sSql.Append("	AND T_USER_MAN_CHARACTER.USER_CHAR_NO			= :USER_CHAR_NO					").AppendLine();
			sSql.Append("	AND T_USER_MAN_CHARACTER.NA_FLAG				= :NA_FLAG						").AppendLine();

			string sOutSql = string.Empty;
			sOutSql = sSql.ToString();

			using (cmd = CreateSelectCommand(sOutSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				cmd.Parameters.Add("NA_FLAG",ViCommConst.FLAG_OFF);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}

		} finally {
			conn.Close();
		}
		return ds;
	}

	/// <summary>
	/// 検索条件に一致するレコードを取得する
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pUnAuthFlag"></param>
	/// <param name="pLoginId"></param>
	/// <returns></returns>
	public DataSet GetProfilePicList(string pSiteCd,int pUnAuthFlag,string pLoginId,string pUploadDateFrom,string pUploadDateTo) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			// ソート条件を設定
			string sOrder;
			if (pUnAuthFlag == 0) {
				sOrder = "ORDER BY UPLOAD_DATE DESC,SITE_CD,PIC_SEQ DESC ";
			} else {
				sOrder = "ORDER BY UNAUTH_UPLOAD_DATE DESC NULLS LAST,SITE_CD,UNAUTH_PIC_SEQ DESC ";
			}

			StringBuilder oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine("SELECT");
			oSqlBuilder.AppendLine("	SITE_CD,");
			oSqlBuilder.AppendLine("	USER_SEQ,");
			oSqlBuilder.AppendLine("	USER_CHAR_NO,");
			oSqlBuilder.AppendLine("	LOGIN_ID,");
			oSqlBuilder.AppendLine("	HANDLE_NM,");

			if (pUnAuthFlag == 0) {
				oSqlBuilder.AppendLine("	UPLOAD_DATE,");
				oSqlBuilder.AppendLine("	PIC_SEQ,");
				oSqlBuilder.AppendLine("	SMALL_PHOTO_IMG_PATH,");
				oSqlBuilder.AppendLine("	PHOTO_IMG_PATH");
			} else {
				oSqlBuilder.AppendLine("	UNAUTH_UPLOAD_DATE				UPLOAD_DATE,");
				oSqlBuilder.AppendLine("	UNAUTH_PIC_SEQ					PIC_SEQ,");
				oSqlBuilder.AppendLine("	UNAUTH_SMALL_PHOTO_IMG_PATH		SMALL_PHOTO_IMG_PATH,");
				oSqlBuilder.AppendLine("	UNAUTH_PHOTO_IMG_PATH			PHOTO_IMG_PATH");
			}

			oSqlBuilder.AppendLine("FROM(SELECT VW_USER_MAN_CHARACTER02.*, ROW_NUMBER() OVER (");
			oSqlBuilder.AppendLine(sOrder);
			oSqlBuilder.AppendLine(") AS RNUM FROM VW_USER_MAN_CHARACTER02");

			// 検索条件を設定
			string sWhere = "";
			OracleParameter[] objParms = CreateProfilePicWhere(pSiteCd,pUnAuthFlag,pLoginId,pUploadDateFrom,pUploadDateTo,ref sWhere);
			oSqlBuilder.AppendLine(sWhere);
			oSqlBuilder.AppendLine(")");

			// ソート条件を設定
			oSqlBuilder.AppendLine(sOrder);

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				// 検索実行
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USER_MAN_CHARACTER02");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
}
