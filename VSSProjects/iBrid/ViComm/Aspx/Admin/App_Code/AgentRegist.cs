﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 仮登録
--	Progaram ID		: AgentRegist
--
--  Creation Date	: 2012.01.19
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;
using iBridCommLib;
using ViComm;

public class AgentRegist : DbSession {
	public AgentRegist() {
	}

	public int GetPageCount(string pSiteCd,string pProductionSeq) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		StringBuilder sSql = new StringBuilder();
		try {
			conn = DbConnect();

			sSql.Append("SELECT COUNT(*) AS ROW_COUNT FROM VW_AGENT_REGIST01 ");

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pProductionSeq,ref sWhere);
			sSql.Append(sWhere);

			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				cmd.Parameters.AddRange(objParms);
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}


	public DataSet GetPageCollection(string pSiteCd,string pProductionSeq,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();
			StringBuilder sSql = new StringBuilder();
			string sOrder = " ORDER BY REGIST_DATE DESC ";

			sSql.Append("SELECT ");
			sSql.AppendLine("	TEMP_REGIST_ID		,");
			sSql.AppendLine("	SITE_CD				,");
			sSql.AppendLine("	CAST_NM				,");
			sSql.AppendLine("	CAST_KANA_NM		,");
			sSql.AppendLine("	BIRTHDAY			,");
			sSql.AppendLine("	LOGIN_PASSWORD		,");
			sSql.AppendLine("	EMAIL_ADDR			,");
			sSql.AppendLine("	TEL					,");
			sSql.AppendLine("	REMARKS				,");
			sSql.AppendLine("	REGIST_DATE			,");
			sSql.AppendLine("	UPDATE_DATE			,");
			sSql.AppendLine("	REVISION_NO			,");
			sSql.AppendLine("	PRODUCTION_NM		,");
			sSql.AppendLine("	MANAGER_NM			");
			sSql.Append("FROM(");
			sSql.Append("	SELECT VW_AGENT_REGIST01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_AGENT_REGIST01 ");

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pProductionSeq,ref sWhere);
			sSql.Append(sWhere);

			sSql.Append(" )WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ");
			sSql.Append(sOrder.ToString());

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				cmd.Parameters.AddRange(objParms);
				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_AGENT_REGIST01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetOne(string pTempRegistId) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT ");
			sSql.Append("	*  ");
			sSql.Append("FROM ");
			sSql.Append("	VW_AGENT_REGIST01 ");
			sSql.Append("WHERE ");
			sSql.Append("	TEMP_REGIST_ID = :TEMP_REGIST_ID ");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				cmd.Parameters.Add("TEMP_REGIST_ID",pTempRegistId);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_AGENT_REGIST01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pProductionSeq,ref string pWhere) {
		ArrayList list = new ArrayList();

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!string.IsNullOrEmpty(pProductionSeq)) {
			SysPrograms.SqlAppendWhere("PRODUCTION_SEQ = :PRODUCTION_SEQ",ref pWhere);
			list.Add(new OracleParameter("PRODUCTION_SEQ",pProductionSeq));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public DataSet GetOneIdPicSeq(string pTempRegistId) {
		StringBuilder oSql = new StringBuilder();
		oSql.AppendLine("SELECT	");
		oSql.AppendLine("	ID_PIC_SEQ1		,");
		oSql.AppendLine("	ID_PIC_SEQ2		,");
		oSql.AppendLine("	ID_PIC_SEQ3		,");
		oSql.AppendLine("	ID_PIC_SEQ4		,");
		oSql.AppendLine("	ID_PIC_SEQ5		,");
		oSql.AppendLine("	ID_PIC_SEQ6		,");
		oSql.AppendLine("	ID_PIC_SEQ7		,");
		oSql.AppendLine("	ID_PIC_SEQ8		,");
		oSql.AppendLine("	ID_PIC_SEQ9		,");
		oSql.AppendLine("	ID_PIC_SEQ10	");
		oSql.AppendLine("FROM	");
		oSql.AppendLine("	T_AGENT_REGIST	");
		oSql.AppendLine("WHERE ");
		oSql.AppendLine("	TEMP_REGIST_ID = :TEMP_REGIST_ID ");

		DataSet oDataSet = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSql.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add("TEMP_REGIST_ID",pTempRegistId);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
				}
				return oDataSet;
			}
		} finally {
			conn.Close();
		}
	}

}