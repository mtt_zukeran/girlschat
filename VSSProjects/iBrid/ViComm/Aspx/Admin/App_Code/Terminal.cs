﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 端末
--	Progaram ID		: Terminal
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class Terminal:DbSession {

	public Terminal() {
	}

	public int GetPageCount() {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_TERMINAL01 ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY TERMINAL_IP_ADDR";
			string sSql = "SELECT " +
							"TERMINAL_IP_ADDR	," +
							"TERMINAL_TYPE		," +
							"URI				," +
							"REMARKS			," +
							"TERMINAL_TYPE_NM " +
							"FROM(" +
							" SELECT VW_TERMINAL01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_TERMINAL01  ";

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_TERMINAL01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public bool IsUniqueUri(string pUri,string pTerminalIpAddr) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT COUNT(*) CNT FROM T_TERMINAL WHERE URI= :URI AND TERMINAL_IP_ADDR != :TERMINAL_IP_ADDR";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("URI",pUri);
				cmd.Parameters.Add("TERMINAL_IP_ADDR",pTerminalIpAddr);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_TERMINAL");

					if (ds.Tables["T_TERMINAL"].Rows.Count != 0) {
						dr = ds.Tables["T_TERMINAL"].Rows[0];
						int iCnt = int.Parse(dr["CNT"].ToString());
						if (iCnt > 0) {
							bExist = true;
						}
					}
				}
			}
		} finally {
			conn.Close();
		}
		return !bExist;
	}


}
