/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 課金種別
--	Progaram ID		: ChargeType
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class ChargeType:DbSession {

	public ChargeType() {
	}

	public DataSet GetList() {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT  CHARGE_TYPE,CHARGE_TYPE_NM FROM T_CHARGE_TYPE " + 
							"WHERE CHARGE_UNIT_TYPE='1' " +
							"ORDER BY DISPLAY_ORDER";

			using (cmd =  CreateSelectCommand(sSql,conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
}
