﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: この娘を探せ・男性エントリー(合計)


--	Progaram ID		: WantedEntrantSummary
--  Creation Date	: 2011.03.23
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;

public class WantedEntrantSummary : DbSession {
	public WantedEntrantSummary() {
	}

	public DataSet GetList(string pSiteCd, string pTargetMonth) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	WES.SITE_CD					,");
		oSqlBuilder.AppendLine("	WES.EXECUTION_DAY			,");
		oSqlBuilder.AppendLine("	TO_CHAR(TO_DATE(WES.EXECUTION_DAY, 'YYYY/MM/DD'), 'DD') DAY,");
		oSqlBuilder.AppendLine("	TO_CHAR(TO_DATE(WES.EXECUTION_DAY, 'YYYY/MM/DD'), 'DY') DAY_OF_WEEK,");
		oSqlBuilder.AppendLine("	WES.ENTRY_COUNT				,");
		oSqlBuilder.AppendLine("	WES.CAUGHT_COUNT			,");
		oSqlBuilder.AppendLine("	WES.POINT_ACQUIRED_COUNT	,");
		oSqlBuilder.AppendLine("	WS.START_TIME				,");
		oSqlBuilder.AppendLine("	WS.END_TIME					,");
		oSqlBuilder.AppendLine("	WS.ANNOUNCE_TIME			,");
		oSqlBuilder.AppendLine("	WS.BOUNTY_POINT				,");
		oSqlBuilder.AppendLine("	WS.POINT_PER_ENTRANT		");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_WANTED_ENTRANT_SUMMARY	WES,");
		oSqlBuilder.AppendLine("	T_WANTED_SCHEDULE			WS");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	WES.SITE_CD			= WS.SITE_CD		AND");
		oSqlBuilder.AppendLine("	WES.EXECUTION_DAY	= WS.EXECUTION_DAY	AND");
		oSqlBuilder.AppendLine("	WES.SITE_CD			= :SITE_CD			AND");
		oSqlBuilder.AppendLine("	TO_DATE(:TARGET_MONTH || '01') <= TO_DATE(WES.EXECUTION_DAY) AND ");
		oSqlBuilder.AppendLine("	TO_DATE(WES.EXECUTION_DAY) <= LAST_DAY(TO_DATE(:TARGET_MONTH || '01'))");
		oSqlBuilder.AppendLine("ORDER BY	");
		oSqlBuilder.AppendLine("	EXECUTION_DAY");

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(), conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD", pSiteCd);
				cmd.Parameters.Add(":TARGET_MONTH", pTargetMonth);

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}
}
