/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 督促レベル
--	Progaram ID		: UrgeLevel
--
--  Creation Date	: 2009.12.14
--  Creater			: iBrid(T.Tokunaga)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class UrgeLevel:DbSession {

	public string colorBack;
	public string colorChar;
	public string colorLink;

	public UrgeLevel() {
	}

	public int GetPageCount(string pSiteCd) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_URGE_LEVEL01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY SITE_CD,URGE_LEVEL ";
			string sSql = "SELECT " +
							"SITE_CD			," +
							"URGE_LEVEL			," +
							"DELAY_DAYS			," +
							"UPDATE_DATE		" +
							"FROM(" +
							" SELECT VW_URGE_LEVEL01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_URGE_LEVEL01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_URGE_LEVEL01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		pWhere = pWhere + " WHERE SITE_CD = :SITE_CD";
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}


	public DataSet GetList(string pSiteCd) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT * FROM VW_URGE_LEVEL01 WHERE SITE_CD = :SITE_CD ";
			sSql = sSql + " ORDER BY SITE_CD,URGE_LEVEL";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public bool GetOne(string pSiteCd,string pUrgeLevel) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;
		try{
			conn = DbConnect();

			string sSql = "SELECT " +
							"SITE_CD			," +
							"URGE_LEVEL			," +
							"DELAY_DAYS			" +
						"FROM " +
							"VW_URGE_LEVEL01 " +
						"WHERE " +
							"SITE_CD = :SITE_CD AND URGE_LEVEL = :URGE_LEVEL";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("URGE_LEVEL",pUrgeLevel);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_URGE_LEVEL01");

					if (ds.Tables["VW_URGE_LEVEL01"].Rows.Count != 0) {
						dr = ds.Tables["VW_URGE_LEVEL01"].Rows[0];
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}
}
