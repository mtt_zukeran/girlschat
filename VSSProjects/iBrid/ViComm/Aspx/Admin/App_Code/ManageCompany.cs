﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: サイト運営会社

--	Progaram ID		: ManageCompany
--
--  Creation Date	: 2009.09.03
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;

public class ManageCompany:DbSession {


	public ManageCompany() {
	}

	public string manageCompanyNm;
	public string tel;
	public string managePersonNm;
	public string emailAddress;
	public int settleCompanyMask;
	public string castSelfMinimumPayment;
	public string castAutoMinimumPayment;
	public int callTimeoutSec;
	public string adManageHost;
	public int companyMask;
	public int notPayExpireMonth;
	public int execMonthlyAutoPaymentFlag;
	public bool GetOne() {
		DataSet ds;
		DataRow dr;

		bool bExist = false;
		try {
			conn = DbConnect();

			string sSql = " SELECT " +
							" MANAGE_COMPANY_NM					," +
							" TEL								," +
							" MANAGE_PERSON_NM					," +
							" EMAIL_ADDR						," +
							" SETTLE_COMPANY_MASK				," +
							" AD_MANAGE_HOST					," +
							" CAST_SELF_MINIMUM_PAYMENT			," +
							" CAST_AUTO_MINIMUM_PAYMENT			," +
							" COMPANY_MASK						," +
							" CALL_TIMEOUT_SEC					," +
							" NOT_PAY_EXPIRE_MONTH				," +
							" EXEC_MONTHLY_AUTO_PAYMENT_FLAG	" +
						" FROM " +
							" T_MANAGE_COMPANY ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MANAGE_COMPANY");

					if (ds.Tables["T_MANAGE_COMPANY"].Rows.Count != 0) {
						dr = ds.Tables["T_MANAGE_COMPANY"].Rows[0];
						manageCompanyNm = iBridUtil.GetStringValue(dr["MANAGE_COMPANY_NM"]);
						tel = iBridUtil.GetStringValue(dr["TEL"]);
						managePersonNm = iBridUtil.GetStringValue(dr["MANAGE_PERSON_NM"]);
						emailAddress = iBridUtil.GetStringValue(dr["EMAIL_ADDR"]);
						settleCompanyMask = int.Parse(iBridUtil.GetStringValue(dr["SETTLE_COMPANY_MASK"]));
						castSelfMinimumPayment = iBridUtil.GetStringValue(dr["CAST_SELF_MINIMUM_PAYMENT"]);
						castAutoMinimumPayment = iBridUtil.GetStringValue(dr["CAST_AUTO_MINIMUM_PAYMENT"]);
						adManageHost = iBridUtil.GetStringValue(dr["AD_MANAGE_HOST"]);
						companyMask = int.Parse(iBridUtil.GetStringValue(dr["COMPANY_MASK"]));
						notPayExpireMonth = int.Parse(iBridUtil.GetStringValue(dr["NOT_PAY_EXPIRE_MONTH"]));
						callTimeoutSec = int.Parse(dr["CALL_TIMEOUT_SEC"].ToString());
						execMonthlyAutoPaymentFlag = int.Parse(dr["EXEC_MONTHLY_AUTO_PAYMENT_FLAG"].ToString());
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public DataSet GetList() {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sManageCompanyCd = iBridUtil.GetStringValue(HttpContext.Current.Session["ManageCompanyCd"]);

			string sSql = "SELECT " +
							"MANAGE_COMPANY_CD  ," +
							"MANAGE_COMPANY_NM  ," +
							"TEL                ," +
							"MANAGE_PERSON_NM   ," +
							"EMAIL_ADDR         ," +
							"REVISION_NO        " +
						  "FROM " +
							"T_MANAGE_COMPANY ";

			if (!sManageCompanyCd.Equals("")) {
				sSql += " WHERE MANAGE_COMPANY_CD =: MANAGE_COMPANY_CD ";
			}

			using (cmd = CreateSelectCommand(sSql,conn)) {
				if (!sManageCompanyCd.Equals("")) {
					cmd.Parameters.Add("MANAGE_COMPANY_CD",sManageCompanyCd);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MANAGE_COMPANY");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public int GetPageCount() {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_MANAGE_COMPANY ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT " +
							"MANAGE_COMPANY_CD  ," +
							"MANAGE_COMPANY_NM  ," +
							"TEL                ," +
							"MANAGE_PERSON_NM   ," +
							"EMAIL_ADDR         ," +
							"RELEASE_PROGRAM_MASK   ," +
							"REVISION_NO        " +
						  "FROM " +
							"T_MANAGE_COMPANY ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MANAGE_COMPANY");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public bool IsAvailableService(ulong pService) {
		return IsAvailableService(pService,string.Empty);
	}
	public bool IsAvailableService(ulong pService,int pNo) {
		return IsAvailableService(pService,pNo.ToString());
	}
	private bool IsAvailableService(ulong pService,string pNo) {
		DataSet ds;
		DataRow dr;
		bool bOk = false;
		string sColumnsNm = string.Format("RELEASE_PROGRAM_MASK{0}",pNo);
		try {
			conn = DbConnect();
			ulong uMask = 0;
			StringBuilder oSql = new StringBuilder();
			oSql.AppendLine("SELECT	");
			oSql.AppendLine(sColumnsNm);
			oSql.AppendLine("FROM	");
			oSql.AppendLine("	T_MANAGE_COMPANY	");

			using (cmd = CreateSelectCommand(oSql.ToString(),conn))
			using (ds = new DataSet()) {
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MANAGE_COMPANY");

					if (ds.Tables["T_MANAGE_COMPANY"].Rows.Count != 0) {
						dr = ds.Tables["T_MANAGE_COMPANY"].Rows[0];
						uMask = ulong.Parse(iBridUtil.GetStringValue(dr[sColumnsNm]));
						bOk = ((pService & uMask) > 0L);
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bOk;
	}

	#region □■□ 機能別利用可否判定メソッド □■□ ==================================================================

	/// <summary>
	/// ｱｸｾｽ集計のﾕﾆｰｸ数集計が利用可能かどうかを示す値を取得する

	/// </summary>
	/// <returns></returns>
	public static bool IsAvailableUniqueAccessSummary() {
		using (ManageCompany oManageCompany = new ManageCompany()) {
			return oManageCompany.IsAvailableService(ViComm.ViCommConst.RELEASE_UNIQUE_ACCESS_SUMMARY);
		}
	}
	/// <summary>
	/// 画像変換が利用可能かどうかを示す値を取得する


	/// </summary>
	/// <returns></returns>
	public static bool IsAvailableConvertImage() {
		using (ManageCompany oManageCompany = new ManageCompany()) {
			return oManageCompany.IsAvailableService(ViComm.ViCommConst.RELEASE_CONVERT_IMAGE);
		}
	}
	#endregion ========================================================================================================

	public DataSet GetReleaseProgramMask(string pManageCompanyCd) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY RELEASE_PROGRAM_SUB_ID,RELEASE_PROGRAM_ID ";
			string sSql = "SELECT " +
							"MANAGE_COMPANY_CD          ," +
							"RELEASE_PROGRAM_ID         ," +
							"RELEASE_PROGRAM_SUB_ID     ," +
							"RELEASE_PROGRAM_BIT        ," +
							"RELEASE_PROGRAM_NM         ," +
							"RELEASE_PROGRAM_SUMMARY	," +
							"REQUEST_COMPANY			" +
						  "FROM " +
							"VW_MANAGE_COMPANY01 ";

			if (!pManageCompanyCd.Equals("")) {
				sSql += " WHERE MANAGE_COMPANY_CD =: MANAGE_COMPANY_CD ";
				sSql += sOrder;
			}

			using (cmd = CreateSelectCommand(sSql,conn)) {
				if (!pManageCompanyCd.Equals("")) {
					cmd.Parameters.Add("MANAGE_COMPANY_CD",pManageCompanyCd);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_MANAGE_COMPANY01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
}
