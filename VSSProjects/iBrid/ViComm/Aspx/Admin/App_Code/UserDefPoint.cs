﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ユーザー定義ポイント
--	Progaram ID		: UserDefPoint
--
--  Creation Date	: 2010.03.30
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Web;
using System.Data;
using System.Data.Common;
using iBridCommLib;

public class UserDefPoint:DbSession {
	public UserDefPoint() {
	}

	public int GetPageCount(string pSiteCd,string pAddPointId) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(ADD_POINT_ID) AS ROW_COUNT FROM T_USER_DEF_POINT ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pAddPointId,ref sWhere);
			sSql = sSql + sWhere;
			
			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
			
		} finally{
			conn.Close();
		}

		return iPageCount;

	}

	public DataSet GetPageCollection(string pSiteCd,string pAddPointId,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY SITE_CD,ADD_POINT_ID ";
			string sSql = "SELECT " +
								"SITE_CD												," +
								"ADD_POINT_ID											," +
								"ADD_POINT												," +
								"CODE_NM						AS APPLY_COUNT_TYPE_NM	," +
								"REMARKS												," +
								"DEF_POINT_NM											," +
								"APPLY_START_TIME										," +
								"APPLY_END_TIME											," +
								"NA_FLAG												 " +
							"FROM(" +
							" SELECT "+
								"T_USER_DEF_POINT.SITE_CD								," +
								"T_USER_DEF_POINT.ADD_POINT_ID							," +
								"T_USER_DEF_POINT.ADD_POINT								," +
								"T_USER_DEF_POINT.REMARKS								," +
								"T_USER_DEF_POINT.DEF_POINT_NM							," +
								"T_CODE_DTL.CODE_NM										," +
								"T_USER_DEF_POINT.APPLY_START_TIME						," +
								"T_USER_DEF_POINT.APPLY_END_TIME						," +
								"T_USER_DEF_POINT.NA_FLAG								," +
								"ROW_NUMBER()											 " +
								" OVER (" + sOrder + ") AS RNUM FROM T_USER_DEF_POINT,T_CODE_DTL  ";

			string sWhere = string.Empty;
			OracleParameter[] objParms = CreateWhere(pSiteCd,pAddPointId,ref sWhere);
			sSql = sSql + sWhere;
			
			if(!string.IsNullOrEmpty(sWhere)){
				sSql = sSql + " AND ";
			}
			sSql = sSql + " T_CODE_DTL.CODE_TYPE='24' AND T_CODE_DTL.CODE = T_USER_DEF_POINT.APPLY_COUNT_TYPE";

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.BindByName = true;
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add(":FIRST_ROW",startRowIndex);
				cmd.Parameters.Add(":LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_USER_DEF_POINT");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	protected OracleParameter[] CreateWhere(string pSiteCd,string pAddPointId,ref string pWhere) {
		pWhere = string.Empty;

		ArrayList list = new ArrayList();
		pWhere = pWhere + " WHERE SITE_CD = :SITE_CD ";
		list.Add(new OracleParameter(":SITE_CD",pSiteCd));

		if(!string.IsNullOrEmpty(pAddPointId)){
			pWhere = pWhere + " AND ADD_POINT_ID = :ADD_POINT_ID";
			list.Add(new OracleParameter(":ADD_POINT_ID",pAddPointId));
		}
		
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
		
	}
	
	public bool IsExistAddPointId(string pSiteCd,string pAddPointId){

		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect();
			string sSql = "SELECT " +
								"ADD_POINT_ID		 " +
							"FROM " +
								"T_USER_DEF_POINT " +
							"WHERE " +
								" SITE_CD = :SITE_CD AND " +
								" ADD_POINT_ID = :ADD_POINT_ID ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":ADD_POINT_ID",pAddPointId);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_USER_DEF_POINT");
					if (ds.Tables["T_USER_DEF_POINT"].Rows.Count != 0) {
						dr = ds.Tables["T_USER_DEF_POINT"].Rows[0];
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}
}
