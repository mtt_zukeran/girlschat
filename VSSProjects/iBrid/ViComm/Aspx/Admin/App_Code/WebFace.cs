﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ＷＥＢデザイン設定
--	Progaram ID		: WebFace
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class WebFace:DbSession {

	public string colorBack;
	public string colorChar;
	public string colorLink;

	public WebFace() {
	}

	public int GetPageCount() {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_WEB_FACE ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}


	public DataSet GetPageCollection(int startRowIndex,int maximumRows) {
		DataSet ds;
		conn = DbConnect();
		ds = new DataSet();             

		string sOrder = "ORDER BY WEB_FACE_SEQ";
		string sSql = "SELECT " +
						"WEB_FACE_SEQ		," +
						"HTML_FACE_NAME		," +
						"SIZE_LINE			," +
						"SIZE_CHAR			," +
						"COLOR_LINE			," +
						"COLOR_INDEX		," +
						"COLOR_CHAR			," +
						"COLOR_BACK			," +
						"COLOR_LINK			," +
						"SIZE_LINE_WOMAN	," +
						"SIZE_CHAR_WOMAN	," +
						"COLOR_LINE_WOMAN	," +
						"COLOR_INDEX_WOMAN	," +
						"COLOR_CHAR_WOMAN	," +
						"COLOR_BACK_WOMAN	," +
						"COLOR_LINK_WOMAN	," +
						"UPDATE_DATE " +
						"FROM(" +
						" SELECT T_WEB_FACE.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM T_WEB_FACE  ";

		sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
		sSql = sSql + sOrder;

		using (cmd = CreateSelectCommand(sSql,conn)) {
			cmd.Parameters.Add("FIRST_ROW",startRowIndex);
			cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_WEB_FACE");
			}
		}
		conn.Close();
		return ds;
	}


	public DataSet GetList() {
		DataSet ds;

		conn = DbConnect();
		ds = new DataSet();

		string sSql = "SELECT WEB_FACE_SEQ,HTML_FACE_NAME FROM T_WEB_FACE ORDER BY WEB_FACE_SEQ ";

		using (cmd = CreateSelectCommand(sSql,conn)) {
			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_WEB_FACE");
			}
		}
		conn.Close();
		return ds;
	}

	public bool GetOne(string pWebFaceSeq) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;

		conn = DbConnect();

		string sSql = "SELECT " +
						"COLOR_LINE		," +
						"COLOR_CHAR		," +
						"COLOR_BACK		," +
						"COLOR_LINK		 " +
					"FROM " +
						"T_WEB_FACE " +
					"WHERE " +
						"WEB_FACE_SEQ = :WEB_FACE_SEQ";

		using (cmd = CreateSelectCommand(sSql,conn))
		using (ds = new DataSet()) {
			cmd.Parameters.Add("WEB_FACE_SEQ",pWebFaceSeq);

			using (da = new OracleDataAdapter(cmd)) {
				da.Fill(ds,"T_WEB_FACE");

				if (ds.Tables["T_WEB_FACE"].Rows.Count != 0) {
					dr = ds.Tables["T_WEB_FACE"].Rows[0];
					colorBack = dr["COLOR_BACK"].ToString();
					colorChar = dr["COLOR_CHAR"].ToString();
					colorLink = dr["COLOR_LINK"].ToString();
					bExist = true;
				}
			}
		}
		conn.Close();
		return bExist;
	}
}
