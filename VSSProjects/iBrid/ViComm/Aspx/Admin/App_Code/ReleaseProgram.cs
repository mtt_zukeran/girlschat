﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: リリース機能
--	Progaram ID		: RereaseProgram
--
--  Creation Date	: 2010.08.17
--  Creater			: iBrid(Y.Igarashi)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

/// <summary>
/// ReleaseProgram の概要の説明です
/// </summary>
public class ReleaseProgram:DbSession
{
    public string releaseProgramNm;

    public ReleaseProgram() {
	}

	public int GetPageCount() {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_RELEASE_PROGRAM ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY RELEASE_PROGRAM_SUB_ID,RELEASE_PROGRAM_ID ";
			string sSql = "SELECT " +
							"RELEASE_PROGRAM_ID			," +
							"RELEASE_PROGRAM_SUB_ID		," +
							"RELEASE_PROGRAM_NM			," +
							"RELEASE_PROGRAM_SUMMARY	," +
							"REQUEST_COMPANY        	"  +
							"FROM(" +
							" SELECT T_RELEASE_PROGRAM.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM T_RELEASE_PROGRAM  ";

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_RELEASE_PROGRAM");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
}
