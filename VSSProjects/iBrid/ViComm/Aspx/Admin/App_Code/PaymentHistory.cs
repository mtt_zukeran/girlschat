﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 支払履歴

--	Progaram ID		: PaymentHistory
--
--  Creation Date	: 2010.12.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class PaymentHistory:DbSession {
	public PaymentHistory() {
	}

	public int GetPageCount(string pUserSeq,string pReportDayFrom,string pReportDayTo) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			DateTime dtFrom = DateTime.Parse(pReportDayFrom);
			DateTime dtTo = DateTime.Parse(pReportDayTo).AddDays(1);

			string sSql = " SELECT COUNT(*) AS ROW_COUNT FROM T_PAYMENT_HISTORY WHERE USER_SEQ = :USER_SEQ AND PAYMENT_DATE >= :PAYMENT_DATE_FROM AND PAYMENT_DATE < :PAYMENT_DATE_TO";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("PAYMENT_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input);
				cmd.Parameters.Add("PAYMENT_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input);
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pUserSeq,string pReportDayFrom,string pReportDayTo,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT").AppendLine();
			sSql.Append("USER_SEQ		,").AppendLine();
			sSql.Append("PAYMENT_AMT	,").AppendLine();
			sSql.Append("PAYMENT_DATE	 ").AppendLine();
			sSql.Append("FROM(SELECT T_PAYMENT_HISTORY.*, ROW_NUMBER() OVER (ORDER BY PAYMENT_DATE DESC) AS RNUM FROM T_PAYMENT_HISTORY").AppendLine();
			sSql.Append("WHERE").AppendLine();
			sSql.Append("USER_SEQ = :USER_SEQ AND PAYMENT_DATE >= :PAYMENT_DATE_FROM AND PAYMENT_DATE < :PAYMENT_DATE_TO").AppendLine();
			sSql.Append(") WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ORDER BY PAYMENT_DATE DESC").AppendLine();

			DateTime dtFrom = DateTime.Parse(pReportDayFrom);
			DateTime dtTo = DateTime.Parse(pReportDayTo).AddDays(1);

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("PAYMENT_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input);
				cmd.Parameters.Add("PAYMENT_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input);
				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_PAYMENT_HISTORY");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;

	}

	public int GetPageCountByLoginId(string pLoginId,string pReportDayFrom,string pReportDayTo,string pPaymentType,string pProductionSeq,string pManagerSeq,string pPaymentMethod) {
	DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			DateTime dtFrom = DateTime.Parse(pReportDayFrom);
			DateTime dtTo = DateTime.Parse(pReportDayTo).AddDays(1);
			string sSql = "SELECT															 " +
							"	COUNT(*)		AS	ROW_COUNT								 " +
							"FROM															 " +
							"	VW_PAYMENT_HISTORY01										 " ;

			// ギフトコード管理テーブルを連結
			sSql += this.GetLeftJoinGiftCodeManage();

			string sTmpSql = string.Empty;
			OracleParameter[] oWhereParamAry = CreateWhere(sSql.ToString(),pLoginId,dtFrom,dtTo,pPaymentType,pProductionSeq,pManagerSeq,pPaymentMethod,out sTmpSql);
			sSql = sTmpSql;


			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				cmd.BindByName = true;

				for (int i = 0;i < oWhereParamAry.Length;i++) {
					cmd.Parameters.Add(oWhereParamAry[i]);
				}
				
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}

		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public int GetTotalAmount(string pLoginId,string pReportDayFrom,string pReportDayTo,string pPaymentType,string pProductionSeq,string pManagerSeq,string pPaymentMethod) {
		DataSet ds;
		DataRow dr;
		int iTotalAmount = 0;
		try {
			conn = DbConnect();

			DateTime dtFrom = DateTime.Parse(pReportDayFrom);
			DateTime dtTo = DateTime.Parse(pReportDayTo).AddDays(1);
			string sSql = "SELECT															 " +
							"	SUM(PAYMENT_AMT)		AS	PAYMENT_SUM						 " +
							"FROM															 " +
							"	VW_PAYMENT_HISTORY01										 " ;

			// ギフトコード管理テーブルを連結
			sSql += this.GetLeftJoinGiftCodeManage();

			string sTmpSql = string.Empty;
			OracleParameter[] oWhereParamAry = CreateWhere(sSql.ToString(),pLoginId,dtFrom,dtTo,pPaymentType,pProductionSeq,pManagerSeq,pPaymentMethod,out sTmpSql);
			sSql = sTmpSql;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				cmd.BindByName = true;

				for (int i = 0;i < oWhereParamAry.Length;i++) {
					cmd.Parameters.Add(oWhereParamAry[i]);
				}
				
				da.Fill(ds);
				if (!ds.Tables[0].Rows[0].IsNull("PAYMENT_SUM")) {
					dr = ds.Tables[0].Rows[0];
					iTotalAmount = int.Parse(dr["PAYMENT_SUM"].ToString());
				}
			}

		} finally {
			conn.Close();
		}
		return iTotalAmount;
	}
	public DataSet GetPageCollectionByLoginId(string pLoginId,string pReportDayFrom,string pReportDayTo,string pPaymentType,string pProductionSeq,string pManagerSeq,string pPaymentMethod,int startRowIndex,int maximumRows) {
	
		DataSet ds;
		try {
			conn = DbConnect();

			DateTime dtFrom = DateTime.Parse(pReportDayFrom);
			DateTime dtTo = DateTime.Parse(pReportDayTo).AddDays(1);


			StringBuilder sSql = new StringBuilder();
			sSql.Append("		SELECT																	 ").AppendLine();
			sSql.Append("			VW_PAYMENT_HISTORY01.USER_SEQ					USER_SEQ					,").AppendLine();
			sSql.Append("			VW_PAYMENT_HISTORY01.PAYMENT_DATE				PAY_DAY						,").AppendLine();
			sSql.Append("			VW_PAYMENT_HISTORY01.PAYMENT_CLOSE_DAY			PAYMENT_CLOSE_DAY			,").AppendLine();
			sSql.Append("			VW_PAYMENT_HISTORY01.PAYMENT_CLOSE_HOUR			PAYMENT_CLOSE_HOUR			,").AppendLine();
			sSql.Append("			VW_PAYMENT_HISTORY01.CSV_SEQ_NO					CSV_SEQ_NO					,").AppendLine();
			sSql.Append("			VW_PAYMENT_HISTORY01.LOGIN_ID					LOGIN_ID					,").AppendLine();
			sSql.Append("			VW_PAYMENT_HISTORY01.CAST_NM					CAST_NM						,").AppendLine();
			sSql.Append("			VW_PAYMENT_HISTORY01.PAYMENT_AMT				PAYMENT_AMT					,").AppendLine();
			sSql.Append("			VW_PAYMENT_HISTORY01.PAYMENT_TYPE_NM			PAYMENT_TYPE_NM				,").AppendLine();
			sSql.Append("			VW_PAYMENT_HISTORY01.STAFF_ID					STAFF_ID					,").AppendLine();
			sSql.Append("			VW_PAYMENT_HISTORY01.BANK_NM					BANK_NM						,").AppendLine();
			sSql.Append("			VW_PAYMENT_HISTORY01.BANK_OFFICE_NM				BANK_OFFICE_NM				,").AppendLine();
			sSql.Append("			VW_PAYMENT_HISTORY01.BANK_OFFICE_KANA_NM		BANK_OFFICE_KANA_NM			,").AppendLine();
			sSql.Append("			VW_PAYMENT_HISTORY01.BANK_ACCOUNT_TYPE2			BANK_ACCOUNT_TYPE2			,").AppendLine();
			sSql.Append("			VW_PAYMENT_HISTORY01.BANK_ACCOUNT_TYPE			BANK_ACCOUNT_TYPE			,").AppendLine();
			sSql.Append("			VW_PAYMENT_HISTORY01.BANK_ACCOUNT_NO			BANK_ACCOUNT_NO				,").AppendLine();
			sSql.Append("			VW_PAYMENT_HISTORY01.BANK_ACCOUNT_HOLDER_NM		BANK_ACCOUNT_HOLDER_NM		,").AppendLine();
			sSql.Append("			VW_PAYMENT_HISTORY01.BANK_ACCOUNT_INVALID_FLAG	BANK_ACCOUNT_INVALID_FLAG	 ").AppendLine();
			sSql.Append("			,NVL(GCM.GIFT_CODE_TYPE_NM,'現金振込') PAYMENT_METHOD_NM").AppendLine();
			sSql.Append("		FROM																			 ").AppendLine();
			sSql.Append("			VW_PAYMENT_HISTORY01														 ").AppendLine();

			// ギフトコード管理テーブルを連結
			sSql.Append(this.GetLeftJoinGiftCodeManage()).AppendLine();

			string sTmpSql = string.Empty;
			OracleParameter[] oWhereParamAry = CreateWhere(sSql.ToString(),pLoginId,dtFrom,dtTo,pPaymentType,pProductionSeq,pManagerSeq,pPaymentMethod,out sTmpSql);
			
			string sOutSql = string.Empty;
			OracleParameter[] oOracleParamAry = ViCommPrograms.CreatePagingSql(sTmpSql.ToString()," ORDER BY PAY_DAY, LOGIN_ID ",startRowIndex,maximumRows,out sOutSql);

			using (cmd = CreateSelectCommand(sOutSql.ToString(),conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {

				cmd.BindByName = true;

				for(int i = 0; i < oWhereParamAry.Length;i++) {
					cmd.Parameters.Add(oWhereParamAry[i]);
				}
				for(int i = 0; i< oOracleParamAry.Length;i++){
					cmd.Parameters.Add(oOracleParamAry[i]);
				}


				da.Fill(ds);

			}
		} finally {
			conn.Close();
		}
		return ds;
	}
	private OracleParameter[] CreateWhere(string pBaseSql,string pLoginId,DateTime dtFrom,DateTime dtTo,string pPaymentType,string pProductionSeq,string pManagerSeq,string pPaymentMethod,out string pOutSql) {

		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder sSql = new StringBuilder();
		sSql.AppendFormat("{0}",pBaseSql).AppendLine();
		sSql.Append("		WHERE																			 ").AppendLine();
		sSql.Append("				VW_PAYMENT_HISTORY01.PAYMENT_DATE		>= :PAYMENT_DATE_FROM			 ").AppendLine();
		sSql.Append("			AND	VW_PAYMENT_HISTORY01.PAYMENT_DATE		< :PAYMENT_DATE_TO				 ").AppendLine();

		oParamList.Add(new OracleParameter(":PAYMENT_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
		oParamList.Add(new OracleParameter(":PAYMENT_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		if (!pLoginId.Equals(string.Empty)) {
			sSql.Append("			AND	VW_PAYMENT_HISTORY01.LOGIN_ID		 LIKE :LOGIN_ID || '%'				 ").AppendLine();
			oParamList.Add(new OracleParameter(":LOGIN_ID",pLoginId));
		}

		if (!pPaymentType.Equals(string.Empty)) {
			sSql.Append("			AND	VW_PAYMENT_HISTORY01.PAYMENT_TYPE		= :PAYMENT_TYPE					 ").AppendLine();
			oParamList.Add(new OracleParameter(":PAYMENT_TYPE",pPaymentType));

		}

		if (!string.IsNullOrEmpty(pProductionSeq)) {
			sSql.Append("			AND VW_PAYMENT_HISTORY01.PRODUCTION_SEQ		= :PRODUCTION_SEQ				").AppendLine();
			oParamList.Add(new OracleParameter(":PRODUCTION_SEQ",pProductionSeq));
		}

		if (!string.IsNullOrEmpty(pManagerSeq)) {
			sSql.Append("			AND VW_PAYMENT_HISTORY01.MANAGER_SEQ		= :MANAGER_SEQ					").AppendLine();
			oParamList.Add(new OracleParameter(":MANAGER_SEQ",pManagerSeq));
		}

		// 支払方法
		if (!string.IsNullOrEmpty(pPaymentMethod)) {
			sSql.Append("			AND NVL(GCM.GIFT_CODE_TYPE,0) = :PAYMENT_METHOD").AppendLine();
			oParamList.Add(new OracleParameter(":PAYMENT_METHOD",pPaymentMethod));
		}

		pOutSql = sSql.ToString();
		return oParamList.ToArray();
	}

	/// <summary>
	/// ギフトコード管理テーブルの結合クエリを取得
	/// </summary>
	/// <returns></returns>
	private string GetLeftJoinGiftCodeManage() {
		StringBuilder sSql = new StringBuilder();
		sSql.Append("			LEFT JOIN (").AppendLine();
		sSql.Append("				SELECT DISTINCT").AppendLine();
		sSql.Append("					GCT.GIFT_CODE_TYPE_NM").AppendLine();
		sSql.Append("					,GCT.GIFT_CODE_TYPE").AppendLine();
		sSql.Append("					,GC.USER_SEQ").AppendLine();
		sSql.Append("					,GC.PAYMENT_DATE").AppendLine();
		sSql.Append("				FROM T_GIFT_CODE_TYPE_MANAGE GCT").AppendLine();
		sSql.Append("					INNER JOIN T_GIFT_CODE_MANAGE GC").AppendLine();
		sSql.Append("						ON GC.GIFT_CODE_TYPE=GCT.GIFT_CODE_TYPE").AppendLine();
		sSql.Append("						AND GC.AMOUNT=GCT.AMOUNT").AppendLine();
		sSql.Append("				WHERE").AppendLine();
		sSql.Append("					GCT.PUBLISH_FLAG=1").AppendLine();
		sSql.Append("					AND GC.USER_SEQ IS NOT NULL").AppendLine();
		sSql.Append("			) GCM").AppendLine();
		sSql.Append("				ON VW_PAYMENT_HISTORY01.USER_SEQ=GCM.USER_SEQ").AppendLine();
		sSql.Append("				AND VW_PAYMENT_HISTORY01.PAYMENT_DATE=GCM.PAYMENT_DATE").AppendLine();
		return sSql.ToString();
	}
}
