﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: OmikujiLog
--	Title			: おみくじ履歴

--	Progaram ID		: OmikujiLog
--
--  Creation Date	: 2011.04.05
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater   Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Text;
using iBridCommLib;
using ViComm;
using System.Collections.Generic;

/// <summary>
/// OmikujiLog の概要の説明です
/// </summary>
public class OmikujiLog : DbSession
{
    public OmikujiLog() {
    }

    public int GetPageCount(string pSiteCd, string pSexCd, string pLoginId, string pReportDayFrom, string pReportDayTo) {
        DataSet ds;
        DataRow dr;
        int iPageCount = 0;
        try {
            conn = DbConnect();

            string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_OMIKUJI_LOG TOL, T_USER TU ";

            string sWhere = "";
            OracleParameter[] objParms = CreateWhere(pSiteCd, pSexCd, pLoginId, pReportDayFrom, pReportDayTo, ref sWhere);
            sSql = sSql + sWhere;

            using (cmd = CreateSelectCommand(sSql, conn))
            using (da = new OracleDataAdapter(cmd))
            using (ds = new DataSet()) {
                for (int i = 0; i < objParms.Length; i++) {
                    cmd.Parameters.Add((OracleParameter)objParms[i]);
                }

                da.Fill(ds);
                if (ds.Tables[0].Rows.Count != 0) {
                    dr = ds.Tables[0].Rows[0];
                    iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
                }
            }
        } finally {
            conn.Close();
        }
        return iPageCount;
    }

    public DataSet GetPageCollection(string pSiteCd, string pSexCd, string pLoginId, string pReportDayFrom, string pReportDayTo, int startRowIndex, int maximumRows) {
        DataSet ds;
        try {
            conn = DbConnect();
            ds = new DataSet();

            string sOrder = " ORDER BY CREATE_DATE DESC ";
            string sSql = "SELECT " +
                            "TOL.SITE_CD			        	," +
                            "TU.LOGIN_ID                		," +
                            "TOL.OMIKUJI_POINT             		," +
                            "TOL.CREATE_DATE			         " +
                          "FROM                                  " +
                            " T_OMIKUJI_LOG     TOL             ," + 
                            " T_USER            TU               " ;

            string sWhere = "";
            OracleParameter[] objParms = CreateWhere(pSiteCd,pSexCd,pLoginId,pReportDayFrom,pReportDayTo, ref sWhere);
			sSql += sWhere;
			string sPagingSql = string.Empty;
			OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(sSql,sOrder,startRowIndex,maximumRows,out sPagingSql);


            using (cmd = CreateSelectCommand(sPagingSql, conn)) {
                for (int i = 0; i < objParms.Length; i++) {
                    cmd.Parameters.Add((OracleParameter)objParms[i]);
                }
				cmd.Parameters.AddRange(oPagingParams);

                using (da = new OracleDataAdapter(cmd)) {
                    da.Fill(ds);
                }
            }
        } finally {
            conn.Close();
        }
        return ds;
    }


    private OracleParameter[] CreateWhere(string pSiteCd, string pSexCd, string pLoginId, string pReportDayFrom, string pReportDayTo, ref string pWhere) {
        pWhere = string.Empty;

        ArrayList list = new ArrayList();
        if (!string.IsNullOrEmpty(pSiteCd)) {
            SysPrograms.SqlAppendWhere(" TOL.SITE_CD = :SITE_CD", ref pWhere);
            list.Add(new OracleParameter("SITE_CD", pSiteCd));
        }
		
        if (!string.IsNullOrEmpty(pSexCd)) {
            SysPrograms.SqlAppendWhere(" TU.SEX_CD = :SEX_CD", ref pWhere);
            list.Add(new OracleParameter("SEX_CD", pSexCd));
        }

		if (!string.IsNullOrEmpty(pLoginId)) {
			SysPrograms.SqlAppendWhere(" TU.LOGIN_ID = :LOGIN_ID",ref pWhere);
			list.Add(new OracleParameter("LOGIN_ID",pLoginId));
		}

        if (!string.IsNullOrEmpty(pReportDayFrom) && !string.IsNullOrEmpty(pReportDayTo)) {
            SysPrograms.SqlAppendWhere(" TOL.REPORT_DAY >= :REPORT_DAY_FROM AND TOL.REPORT_DAY <= :REPORT_DAY_TO ", ref pWhere);
            list.Add(new OracleParameter("REPORT_DAY_FROM", pReportDayFrom));
            list.Add(new OracleParameter("REPORT_DAY_TO", pReportDayTo));
        }

        SysPrograms.SqlAppendWhere(" TU.USER_SEQ = TOL.USER_SEQ ", ref pWhere);

        return (OracleParameter[])list.ToArray(typeof(OracleParameter));
    }

}
