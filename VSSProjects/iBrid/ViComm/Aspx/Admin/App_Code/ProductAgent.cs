﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 商品

--	Progaram ID		: Product
--
--  Creation Date	: 2010.12.06
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class ProductAgent : DbSession {
	public ProductAgent() { }

	public DataSet GetList() {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT ");
		oSqlBuilder.Append("	T_PRODUCT_AGENT.PRODUCT_AGENT_CD	,   ");
		oSqlBuilder.Append("	T_PRODUCT_AGENT.PRODUCT_AGENT_NM		");
		oSqlBuilder.Append(" FROM   ");
		oSqlBuilder.Append("	T_PRODUCT_AGENT		    ");

		DataSet oDataSet = new DataSet();
		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
				}

			}
		} finally {
			conn.Close();
		}
		return oDataSet;
	}

	public DataSet GetList(object pOuterAgentFlag) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT ");
		oSqlBuilder.Append("	T_PRODUCT_AGENT.PRODUCT_AGENT_CD	,   ");
		oSqlBuilder.Append("	T_PRODUCT_AGENT.PRODUCT_AGENT_NM		");
		oSqlBuilder.Append(" FROM   ");
		oSqlBuilder.Append("	T_PRODUCT_AGENT		    ");

		DataSet oDataSet = new DataSet();
		try {
			this.conn = this.DbConnect();

			using (this.cmd = new OracleCommand()) {
                int? iOuterAgentFlag = pOuterAgentFlag as int?;
                if (iOuterAgentFlag.HasValue) {
                    oSqlBuilder.Append(" WHERE ");
                    oSqlBuilder.Append("    OUTER_AGENT_FLAG = :OUTER_AGENT_FLAG ");
                    this.cmd.Parameters.Add(":OUTER_AGENT_FLAG", iOuterAgentFlag.Value);
                }

				this.cmd.BindByName = true;
				this.cmd.Connection = this.conn;
				this.cmd.CommandText = oSqlBuilder.ToString();

				using (this.da = new OracleDataAdapter(this.cmd)) {
					this.da.Fill(oDataSet);
				}
			}
		} finally {
			this.conn.Close();
		}
		return oDataSet;
	}
	
	public DataSet GetOne(string pProductAgentCd){
		string sSql =
@"
SELECT 
	T_PRODUCT_AGENT.PRODUCT_AGENT_CD		,
	T_PRODUCT_AGENT.PRODUCT_AGENT_NM        ,
	T_PRODUCT_AGENT.PRODUCT_AGENT_DOMAIN    ,
	T_PRODUCT_AGENT.PRODUCT_AGENT_UID       ,
	T_PRODUCT_AGENT.PRODUCT_AGENT_PWD       ,
	T_PRODUCT_AGENT.LAST_LINK_DATE          ,
	T_PRODUCT_AGENT.REVISION_NO             ,
	T_PRODUCT_AGENT.OUTER_AGENT_FLAG        ,
	T_PRODUCT_AGENT.PRODUCT_TYPE	
FROM
	T_PRODUCT_AGENT
WHERE
	T_PRODUCT_AGENT.PRODUCT_AGENT_CD = :PRODUCT_AGENT_CD
";		

		DataSet oDataSet = new DataSet();
		try {
			this.conn = this.DbConnect();

			using (this.cmd = CreateSelectCommand(sSql,conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.Add(":PRODUCT_AGENT_CD", pProductAgentCd);

				using (this.da = new OracleDataAdapter(this.cmd)) {
					this.da.Fill(oDataSet);
				}
			}
		} finally {
			this.conn.Close();
		}
		return oDataSet;
	
	}
}
