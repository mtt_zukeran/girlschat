﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: WEB利用明細
--	Progaram ID		: WebUsedLog
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;
using ViComm;

public class WebUsedLog : DbSession {

	public WebUsedLog() {
	}

	public int GetPageCount(string pSiteCd, string pUserSeq, string pReportDayFrom, string pReportDayTo, string pPartnerLoginId, string pPartnerHandleNm) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_WEB_USED_LOG01 ";

			string sWhere = "";
			OracleParameter[] oParms = CreateWhereBase(pSiteCd, pUserSeq, pReportDayFrom, pReportDayTo, pPartnerLoginId, pPartnerHandleNm, ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql, conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				cmd.Parameters.AddRange(oParms);

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd, string pUserSeq, string pReportDayFrom, string pReportDayTo, string pPartnerLoginId, string pPartnerHandleNm, int startRowIndex, int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sWhere = "";
			OracleParameter[] oParms = CreateWhereBase(pSiteCd, pUserSeq, pReportDayFrom, pReportDayTo, pPartnerLoginId, pPartnerHandleNm, ref sWhere);

			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT * FROM(				 	 ").AppendLine();
			sSql.Append("	SELECT				 		 ").AppendLine();
			sSql.Append("		INNER.*			,		 ").AppendLine();
			sSql.Append("		ROWNUM AS RNUM			 ").AppendLine();
			sSql.Append("	FROM						 ").AppendLine();
			sSql.Append("	(							 ").AppendLine();
			sSql.Append("		SELECT					 ").AppendLine();
			sSql.Append("			ROWNUM				,").AppendLine();
			sSql.Append("			CHARGE_START_DATE	,").AppendLine();
			sSql.Append("			CAST_SITE_CD		,").AppendLine();
			sSql.Append("			CAST_LOGIN_ID		,").AppendLine();
			sSql.Append("			USER_CHAR_NO		,").AppendLine();
			sSql.Append("			HANDLE_NM			,").AppendLine();
			sSql.Append("			CHARGE_TYPE			,").AppendLine();
			sSql.Append("			CHARGE_TYPE_NM		,").AppendLine();
			sSql.Append("			CHARGE_POINT		,").AppendLine();
			sSql.Append("			MAN_LOGIN_ID		 ").AppendLine();
			sSql.Append("		FROM					 ").AppendLine();
			sSql.Append("			VW_WEB_USED_LOG01	 ").AppendLine();
			sSql.Append(sWhere);
			sSql.Append("		ORDER BY CHARGE_START_DATE DESC").AppendLine();
			sSql.Append("	)INNER WHERE ROWNUM <= :MAX_ROW").AppendLine();
			sSql.Append(") WHERE RNUM > :FIRST_ROW AND RNUM <=:LAST_ROW").AppendLine();

			using (cmd = CreateSelectCommand(sSql.ToString(), conn)) {
				for (int i = 0; i < oParms.Length; i++) {
					cmd.Parameters.Add((OracleParameter)oParms[i]);
				}
				cmd.Parameters.Add("MAX_ROW", startRowIndex + maximumRows);
				cmd.Parameters.Add("FIRST_ROW", startRowIndex);
				cmd.Parameters.Add("LAST_ROW", startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds, "VW_WEB_USED_LOG01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public void GetTotal(string pSiteCd, string pUserSeq, string pReportDayFrom, string pReportDayTo, ref int pMin, ref int pPoint) {
		DataSet ds;
		DataRow dr;
		pMin = 0;
		pPoint = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT NVL(SUM(CHARGE_MIN),0) AS MIN, NVL(SUM(CHARGE_POINT),0) AS POINT FROM VW_WEB_USED_LOG01";
			string sWhere = "";

			OracleParameter[] objParms = CreateWhere(pSiteCd, pUserSeq, pReportDayFrom, pReportDayTo, ref sWhere);

			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql, conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0; i < objParms.Length; i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					pMin = int.Parse(dr["MIN"].ToString());
					pPoint = int.Parse(dr["POINT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
	}

	private OracleParameter[] CreateWhere(string pSiteCd, string pUserSeq, string pReportDayFrom, string pReportDayTo, ref string pWhere) {
		return CreateWhereBase(pSiteCd, pUserSeq, pReportDayFrom, pReportDayTo, string.Empty, string.Empty, ref pWhere);
	}
	private OracleParameter[] CreateWhereBase(string pSiteCd, string pUserSeq, string pReportDayFrom, string pReportDayTo, string pPartnerLoginId, string pPartnerHandleNm, ref string pWhere) {
		ArrayList list = new ArrayList();

		iBridCommLib.SysPrograms.SqlAppendWhere("MAN_USER_SITE_CD = :SITE_CD", ref pWhere);
		list.Add(new OracleParameter("SITE_CD", pSiteCd));

		iBridCommLib.SysPrograms.SqlAppendWhere("MAN_USER_SEQ = :USER_SEQ", ref pWhere);
		list.Add(new OracleParameter("USER_SEQ", pUserSeq));

		if (!string.IsNullOrEmpty(pPartnerLoginId)) {
			iBridCommLib.SysPrograms.SqlAppendWhere("CAST_LOGIN_ID LIKE :CAST_LOGIN_ID||'%'", ref pWhere);
			list.Add(new OracleParameter("CAST_LOGIN_ID", pPartnerLoginId));
		}

		if (!string.IsNullOrEmpty(pPartnerHandleNm)) {
			iBridCommLib.SysPrograms.SqlAppendWhere("HANDLE_NM LIKE :HANDLE_NM||'%'", ref pWhere);
			list.Add(new OracleParameter("HANDLE_NM", pPartnerHandleNm));
		}

		if (!string.IsNullOrEmpty(pReportDayFrom) && !string.IsNullOrEmpty(pReportDayTo)) {
			DateTime dtFrom = System.DateTime.ParseExact(pReportDayFrom + " 00:00:00", "yyyy/MM/dd HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo, System.Globalization.DateTimeStyles.None);
			DateTime dtTo = System.DateTime.ParseExact(pReportDayTo + " 00:00:00", "yyyy/MM/dd HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo, System.Globalization.DateTimeStyles.None);
			iBridCommLib.SysPrograms.SqlAppendWhere("CHARGE_START_DATE >= :CHARGE_START_DATE_FROM AND CHARGE_START_DATE < :CHARGE_START_DATE_TO", ref pWhere);
			list.Add(new OracleParameter("CHARGE_START_DATE_FROM", OracleDbType.Date, dtFrom, ParameterDirection.Input));
			list.Add(new OracleParameter("CHARGE_START_DATE_TO", OracleDbType.Date, dtTo, ParameterDirection.Input));
		}
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public int GetCastPageCount(string pSiteCd, string pUserSeq, string pUserCharNo, string pReportDayFrom, string pReportDayTo, string pManLoginId) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	VW_WEB_USED_LOG02");

		string sWhare = string.Empty;
		OracleParameter[] oWhereParams = CreateWhereCast(pSiteCd, pUserSeq, pUserCharNo, pReportDayFrom, pReportDayTo, pManLoginId, ref sWhare);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhare).ToString(), conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetCastPageCollection(string pSiteCd, string pUserSeq, string pUserCharNo, string pReportDayFrom, string pReportDayTo, string pManLoginId, int startRowIndex, int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	CAST_SITE_CD			,");
		oSqlBuilder.AppendLine("	CHARGE_START_DATE		,");
		oSqlBuilder.AppendLine("	CHARGE_TYPE				,");
		oSqlBuilder.AppendLine("	MAN_USER_SITE_CD		,");
		oSqlBuilder.AppendLine("	MAN_LOGIN_ID			,");
		oSqlBuilder.AppendLine("	MAN_HANDLE_NM			,");
		oSqlBuilder.AppendLine("	PAYMENT_POINT");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	VW_WEB_USED_LOG02");

		string sWhare = string.Empty;
		string sPagingSql;
		string sSortExpression = "ORDER BY CHARGE_START_DATE DESC";

		OracleParameter[] oWhereParams = CreateWhereCast(pSiteCd, pUserSeq, pUserCharNo, pReportDayFrom, pReportDayTo, pManLoginId, ref sWhare);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhare).ToString(), sSortExpression, startRowIndex, maximumRows, out sPagingSql);


		StringBuilder oOuterSqlBuilder = new StringBuilder();
		oOuterSqlBuilder.AppendLine("SELECT ");
		oOuterSqlBuilder.AppendLine("	INNER.*						,	");
		oOuterSqlBuilder.AppendLine("	T_CHARGE_TYPE.CHARGE_TYPE_NM	");
		oOuterSqlBuilder.AppendLine("FROM	");
		oOuterSqlBuilder.AppendFormat("	({0}) INNER, ", sPagingSql);
		oOuterSqlBuilder.AppendLine("	T_CHARGE_TYPE");
		oOuterSqlBuilder.AppendLine("WHERE	");
		oOuterSqlBuilder.AppendLine("	DECODE(INNER.CHARGE_TYPE,'11','08','12','17','13','18',INNER.CHARGE_TYPE) = T_CHARGE_TYPE.CHARGE_TYPE(+)");
		oOuterSqlBuilder.AppendLine("ORDER BY INNER.CHARGE_START_DATE DESC");

		DataSet oDataSet = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oOuterSqlBuilder.ToString(), conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
				}
			}
		} finally {
			conn.Close();
		}
		return oDataSet;
	}

	private OracleParameter[] CreateWhereCast(string pSiteCd, string pUserSeq, string pUserCharNo, string pReportDayFrom, string pReportDayTo, string pManLoginId, ref string pWhere) {
		ArrayList list = new ArrayList();

		if (!string.IsNullOrEmpty(pSiteCd)) {
			iBridCommLib.SysPrograms.SqlAppendWhere("CAST_SITE_CD = :SITE_CD", ref pWhere);
			list.Add(new OracleParameter("SITE_CD", pSiteCd));
		}

		if (!string.IsNullOrEmpty(pUserSeq)) {
			iBridCommLib.SysPrograms.SqlAppendWhere("CAST_USER_SEQ = :USER_SEQ", ref pWhere);
			list.Add(new OracleParameter("USER_SEQ", pUserSeq));
		}

		if (!string.IsNullOrEmpty(pUserCharNo)) {
			iBridCommLib.SysPrograms.SqlAppendWhere("CAST_CHAR_NO = :CHAR_NO", ref pWhere);
			list.Add(new OracleParameter("CHAR_NO", pUserCharNo));
		}

		if (!string.IsNullOrEmpty(pManLoginId)) {
			iBridCommLib.SysPrograms.SqlAppendWhere("MAN_LOGIN_ID = :MAN_LOGIN_ID", ref pWhere);
			list.Add(new OracleParameter("MAN_LOGIN_ID", pManLoginId));
		}

		if (!string.IsNullOrEmpty(pReportDayFrom) && !string.IsNullOrEmpty(pReportDayTo)) {
			DateTime dtFrom = DateTime.Parse(string.Format("{0} 00:00:00", pReportDayFrom));
			DateTime dtTo = DateTime.Parse(string.Format("{0} 23:59:59", pReportDayTo)).AddSeconds(1);
			iBridCommLib.SysPrograms.SqlAppendWhere("CHARGE_START_DATE >= :CHARGE_START_DATE_FROM AND CHARGE_START_DATE < :CHARGE_START_DATE_TO", ref pWhere);
			list.Add(new OracleParameter("CHARGE_START_DATE_FROM", OracleDbType.Date, dtFrom, ParameterDirection.Input));
			list.Add(new OracleParameter("CHARGE_START_DATE_TO", OracleDbType.Date, dtTo, ParameterDirection.Input));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public DataSet GetPointSummaryList(string pSiteCd, string pUserSeq, string pReportDayFrom, string pReportDayTo, string pPartnerLoginId, string pPartnerHandleNm) {
		string sWhere = string.Empty;
		OracleParameter[] objParms = CreateWhereBase(pSiteCd, pUserSeq, pReportDayFrom, pReportDayTo, pPartnerLoginId, pPartnerHandleNm, ref sWhere);

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	CHARGE_TYPE					,");
		oSqlBuilder.AppendLine("	CHARGE_TYPE_NM				,");
		oSqlBuilder.AppendLine("	SUM(CHARGE_POINT)	SUMMARY	,");
		oSqlBuilder.AppendLine("	DISPLAY_ORDER				");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_WEB_USED_LOG01		");
		oSqlBuilder.AppendLine(sWhere);
		oSqlBuilder.AppendLine("GROUP BY");
		oSqlBuilder.AppendLine("	CHARGE_TYPE				,");
		oSqlBuilder.AppendLine("	CHARGE_TYPE_NM			,");
		oSqlBuilder.AppendLine("	DISPLAY_ORDER			");
		oSqlBuilder.AppendLine("ORDER BY");
		oSqlBuilder.AppendLine("	DISPLAY_ORDER			");

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(), conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(objParms);

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}

	public DataSet GetCastPointSummaryList(string pSiteCd, string pUserSeq, string pUserCharNo, string pReportDayFrom, string pReportDayTo, string pManLoginId) {
		string sWhere = string.Empty;
		OracleParameter[] objParms = CreateWhereCast(pSiteCd, pUserSeq, pUserCharNo, pReportDayFrom, pReportDayTo, pManLoginId, ref sWhere);

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	CHARGE_TYPE					,");
		oSqlBuilder.AppendLine("	CHARGE_TYPE_NM				,");
		oSqlBuilder.AppendLine("	SUM(PAYMENT_POINT)	SUMMARY	,");
		oSqlBuilder.AppendLine("	DISPLAY_ORDER				");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_WEB_USED_LOG02		");
		oSqlBuilder.AppendLine(sWhere);
		oSqlBuilder.AppendLine("GROUP BY");
		oSqlBuilder.AppendLine("	CHARGE_TYPE				,");
		oSqlBuilder.AppendLine("	CHARGE_TYPE_NM			,");
		oSqlBuilder.AppendLine("	DISPLAY_ORDER			");
		oSqlBuilder.AppendLine("ORDER BY");
		oSqlBuilder.AppendLine("	DISPLAY_ORDER			");

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(), conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(objParms);

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}	
}
