/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ユーザーランク
--	Progaram ID		: UserRank
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class UserRank:DbSession {

	public string colorBack;
	public string colorChar;
	public string colorLink;

	public UserRank() {
	}

	public int GetPageCount(string pSiteCd) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_USER_RANK01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY SITE_CD,TOTAL_RECEIPT_AMT ";
			string sSql = "SELECT " +
							"SITE_CD				," +
							"SITE_NM				," +
							"USER_RANK				," +
							"USER_RANK_NM			," +
							"LIMIT_POINT			," +
							"TOTAL_RECEIPT_AMT		," +
							"WEB_FACE_SEQ			," +
							"HTML_FACE_NAME			," +
							"USE_OTHER_SYS_INFO_FLAG," +
							"INTRO_POINT_RATE		," +
							"USER_RANK_KEEP_AMT		," +
							"UPDATE_DATE			," +
							"BINGO_BALL_RATE		" +
							"FROM(" +
							" SELECT VW_USER_RANK01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_USER_RANK01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USER_RANK01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		pWhere = pWhere + " WHERE SITE_CD = :SITE_CD AND USER_RANK != :USER_RANK ";
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		list.Add(new OracleParameter("USER_RANK",ViComm.ViCommConst.DEFAUL_USER_RANK));
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}


	public DataSet GetList(string pSiteCd) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT * FROM VW_USER_RANK01 WHERE SITE_CD = :SITE_CD AND USER_RANK != :USER_RANK ";
			sSql = sSql + " ORDER BY SITE_CD,USER_RANK";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_RANK",ViComm.ViCommConst.DEFAUL_USER_RANK);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetListIncDefault(string pSiteCd) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT * FROM VW_USER_RANK01 WHERE SITE_CD = :SITE_CD ";
			sSql = sSql + " ORDER BY SITE_CD,USER_RANK";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public bool GetOne(string pSiteCd,string pUserRank) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;
		try {
			conn = DbConnect();

			string sSql = "SELECT " +
							"COLOR_CHAR			," +
							"COLOR_BACK			," +
							"COLOR_LINK			," +
							"USER_RANK_NM		 " +
						"FROM " +
							"VW_USER_RANK01 " +
						"WHERE " +
							"SITE_CD = :SITE_CD AND USER_RANK = :USER_RANK";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_RANK",pUserRank);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USER_RANK01");

					if (ds.Tables["VW_USER_RANK01"].Rows.Count != 0) {
						dr = ds.Tables["VW_USER_RANK01"].Rows[0];
						colorBack = dr["COLOR_BACK"].ToString();
						colorChar = dr["COLOR_CHAR"].ToString();
						colorLink = dr["COLOR_LINK"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public bool IsExistPointAffliateUserRank(string pSiteCd,string pCurrentRank) {
		DataSet ds;
		bool bExist = false;
		try {
			conn = DbConnect();
			ds = new DataSet();
			string sSql = "SELECT USER_RANK FROM T_USER_RANK WHERE SITE_CD = :SITE_CD AND USER_RANK != :USER_RANK AND POINT_AFFILIATE_RANK_FLAG = :POINT_AFFILIATE_RANK_FLAG";
			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_RANK",pCurrentRank);
				cmd.Parameters.Add("POINT_AFFILIATE_RANK_FLAG",ViComm.ViCommConst.FLAG_ON);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_USER_RANK");
					bExist = (ds.Tables["T_USER_RANK"].Rows.Count != 0);
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}
}
