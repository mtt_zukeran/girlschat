﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 所持アイテム
--	Progaram ID		: PossessionGameItem
--
--  Creation Date	: 2011.08.18
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;


public class PossessionGameItem : DbSession {

	public PossessionGameItem() {
	}

	public int GetPageCount(string pSiteCd,string pUserSeq,string pUserCharNo) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	COUNT(*)					");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_POSSESSION_GAME_ITEM	PGI	");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	PGI.SITE_CD					= :SITE_CD							AND");
		oSqlBuilder.AppendLine("	PGI.USER_SEQ				= :USER_SEQ							AND");
		oSqlBuilder.AppendLine("	PGI.USER_CHAR_NO			= :USER_CHAR_NO						");

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(new OracleParameter("SITE_CD",pSiteCd));
				cmd.Parameters.Add(new OracleParameter("USER_SEQ",pUserSeq));
				cmd.Parameters.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pUserSeq,string pUserCharNo,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	PGI.SITE_CD					,");
		oSqlBuilder.AppendLine("	PGI.GAME_ITEM_SEQ			,");
		oSqlBuilder.AppendLine("	GIC.GAME_ITEM_CATEGORY_NM	,");
		oSqlBuilder.AppendLine("	GI.GAME_ITEM_NM				,");
		oSqlBuilder.AppendLine("	PGI.POSSESSION_COUNT		,");
		oSqlBuilder.AppendLine("	PGI.NOW_ENDURANCE			,");
		oSqlBuilder.AppendLine("	GI.ENDURANCE				");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_POSSESSION_GAME_ITEM	PGI	,");
		oSqlBuilder.AppendLine("	T_GAME_ITEM				GI	,");
		oSqlBuilder.AppendLine("	T_GAME_ITEM_CATEGORY	GIC	");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	GI.SITE_CD					= GIC.SITE_CD					(+)	AND");
		oSqlBuilder.AppendLine("	GI.GAME_ITEM_CATEGORY_TYPE	= GIC.GAME_ITEM_CATEGORY_TYPE	(+)	AND");
		oSqlBuilder.AppendLine("	GI.SITE_CD					= PGI.SITE_CD						AND");
		oSqlBuilder.AppendLine("	GI.GAME_ITEM_SEQ			= PGI.GAME_ITEM_SEQ					AND");
		oSqlBuilder.AppendLine("	PGI.SITE_CD					= :SITE_CD							AND");
		oSqlBuilder.AppendLine("	PGI.USER_SEQ				= :USER_SEQ							AND");
		oSqlBuilder.AppendLine("	PGI.USER_CHAR_NO			= :USER_CHAR_NO						");

		string sPagingSql;
		string sSortExpression = "ORDER BY PGI.SITE_CD, GI.GAME_ITEM_CATEGORY_TYPE, PGI.GAME_ITEM_SEQ	";

		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(new OracleParameter("SITE_CD",pSiteCd));
				cmd.Parameters.Add(new OracleParameter("USER_SEQ",pUserSeq));
				cmd.Parameters.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));
				cmd.Parameters.AddRange(oPagingParams);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}
}
