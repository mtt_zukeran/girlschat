﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: キャストキャラクター拡張


--	Progaram ID		: CastCharacterEx
--  Creation Date	: 2011.04.04
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class CastCharacterEx : DbSession {

	public CastCharacterEx() {
	}

	public DataSet GetOne(string pSiteCd, string pUserSeq, string pUserCharNo) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	SITE_CD					,");
		oSqlBuilder.AppendLine("	USER_SEQ				,");
		oSqlBuilder.AppendLine("	USER_CHAR_NO			,");
		oSqlBuilder.AppendLine("	ENABLED_BLOG_FLAG		,");
        oSqlBuilder.AppendLine("	BUY_POINT_MAIL_RX_TYPE	,");
        oSqlBuilder.AppendLine("	SITE_USE_STATUS     	,");
		oSqlBuilder.AppendLine("	UPDATE_DATE				,");
		oSqlBuilder.AppendLine("	REVISION_NO				");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER_EX");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD			AND");
		oSqlBuilder.AppendLine("	USER_SEQ			= :USER_SEQ			AND");
		oSqlBuilder.AppendLine("	USER_CHAR_NO		= :USER_CHAR_NO		");

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(), conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(new OracleParameter("SITE_CD", pSiteCd));
				cmd.Parameters.Add(new OracleParameter("USER_SEQ", pUserSeq));
				cmd.Parameters.Add(new OracleParameter("USER_CHAR_NO", pUserCharNo));

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}
}
