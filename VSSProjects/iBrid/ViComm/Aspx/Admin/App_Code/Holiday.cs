﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 休日
--	Progaram ID		: Holiday
--
--  Creation Date	: 2009.12.22
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class Holiday:DbSession {

	public Holiday() {
	}

	public int GetPageCount() {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_HOLIDAY ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY HOLIDAY ";
			string sSql = "SELECT " +
							"HOLIDAY			," +
							"REMARKS			" +
							"FROM(" +
							" SELECT T_HOLIDAY.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM T_HOLIDAY  ";

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_HOLIDAY");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
}
