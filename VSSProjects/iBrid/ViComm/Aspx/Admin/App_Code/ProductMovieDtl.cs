﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 商品動画詳細

--	Progaram ID		: ProductMovieDtl
--
--  Creation Date	: 2010.12.10
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class ProductMovieDtl : DbSession {
	public ProductMovieDtl() {}
	
	public DataSet GetSummary(string pSiteCd,string pProductAgentCd,string pProductSeq,string pProductMovieType){

		System.Text.StringBuilder oSqlBuilder = new System.Text.StringBuilder();
		oSqlBuilder.AppendLine(" SELECT                                                                          ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL01.SITE_CD					,                            ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL01.PRODUCT_AGENT_CD         ,                            ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL01.PRODUCT_SEQ              ,                            ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL01.OBJ_SEQ                  ,                            ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL01.CP_SIZE_TYPE             ,                            ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL01.CP_FILE_FORMAT           ,                            ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL01.CP_TARGET_CARRIER        ,                            ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL01.FILE_COUNT               ,                            ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL01.FILE_SIZE                ,                            ");
		oSqlBuilder.AppendLine(" 	VW_PRODUCT_MOVIE_DTL01.PRODUCT_MOVIE_TYPE                                    ");
		oSqlBuilder.AppendLine(" FROM                                                                            ");
		oSqlBuilder.AppendLine("   VW_PRODUCT_MOVIE_DTL01                                                        ");
		oSqlBuilder.AppendLine(" WHERE                                                                           ");
		oSqlBuilder.AppendLine("   VW_PRODUCT_MOVIE_DTL01.SITE_CD 				= :SITE_CD 				AND          ");
		oSqlBuilder.AppendLine("   VW_PRODUCT_MOVIE_DTL01.PRODUCT_AGENT_CD		= :PRODUCT_AGENT_CD 	AND          ");
		oSqlBuilder.AppendLine("   VW_PRODUCT_MOVIE_DTL01.PRODUCT_SEQ 			= :PRODUCT_SEQ 			AND          ");
		oSqlBuilder.AppendLine("   VW_PRODUCT_MOVIE_DTL01.PRODUCT_MOVIE_TYPE	= :PRODUCT_MOVIE_TYPE                ");
		oSqlBuilder.AppendLine(" ORDER BY                                                                        ");
		oSqlBuilder.AppendLine("   VW_PRODUCT_MOVIE_DTL01.CP_TARGET_CARRIER 	,");
		oSqlBuilder.AppendLine("   VW_PRODUCT_MOVIE_DTL01.CP_SIZE_TYPE			,");
		oSqlBuilder.AppendLine("   VW_PRODUCT_MOVIE_DTL01.CP_TARGET_CARRIER 	 ");
		
		DataSet oDs = new DataSet();
		try{
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD", pSiteCd);
				cmd.Parameters.Add(":PRODUCT_AGENT_CD", pProductAgentCd);
				cmd.Parameters.Add(":PRODUCT_SEQ", pProductSeq);
				cmd.Parameters.Add(":PRODUCT_MOVIE_TYPE", pProductMovieType);
				
				using(da = new OracleDataAdapter(cmd)){
					da.Fill(oDs);
				}
			}
		}finally{
			conn.Close();
		}
		return oDs;		
	}
}
