﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ピックアップ

--	Progaram ID		: Pickup
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System.Text.RegularExpressions;
using iBridCommLib;
using ViComm;

public class PickupObjs :DbSession{
	public PickupObjs() {}
	
	public DataSet GetList(string pSiteCd,string pPickupId,string pPickupType){
		// 主となるViewの決定
		string sMainView = string.Empty;
		
		//各Viewで必要になるﾌｨｰﾙﾄﾞ
		string sAddFirldList = "	,''	PIC_TYPE	";
		switch(pPickupType){
			case ViCommConst.PicupTypes.PROFILE_PIC:
			case ViCommConst.PicupTypes.BBS_PIC:
				sMainView = "VW_PICKUP_OBJS01";
				sAddFirldList = "	,	PIC_TYPE	";
				break;
			case ViCommConst.PicupTypes.PROFILE_MOVIE:
			case ViCommConst.PicupTypes.BBS_MOVIE:
				sMainView = "VW_PICKUP_OBJS02";
				break;
			case ViCommConst.PicupTypes.CAST_CHARACTER:
				sMainView = "VW_PICKUP_OBJS03";
				break;				
			default:
				throw new NotSupportedException(string.Format("不正なピックアップ区分 {0}", pPickupType));
		}
		
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT ").AppendLine();
		oSqlBuilder.Append("	SITE_CD					, 	").AppendLine();
		oSqlBuilder.Append("	PICKUP_ID				, 	").AppendLine();
		oSqlBuilder.Append("	PICKUP_FLAG				, 	").AppendLine();
		oSqlBuilder.Append("	OBJ_SEQ					, 	").AppendLine();
		oSqlBuilder.Append("	COMMENT_PICKUP			, 	").AppendLine();
		oSqlBuilder.Append("	PICKUP_START_PUB_DAY	, 	").AppendLine();
		oSqlBuilder.Append("	PICKUP_END_PUB_DAY		, 	").AppendLine();
		oSqlBuilder.Append("	PRIORITY				, 	").AppendLine();
		oSqlBuilder.Append("	PICKUP_FLAG				, 	").AppendLine();
		oSqlBuilder.Append("	UPDATE_DATE				, 	").AppendLine();
		oSqlBuilder.Append("	REVISION_NO				, 	").AppendLine();
		oSqlBuilder.Append("	PICKUP_TYPE				, 	").AppendLine();
		oSqlBuilder.Append("	OBJ_TITLE				, 	").AppendLine();
		oSqlBuilder.Append("	LOGIN_ID				, 	").AppendLine();
		oSqlBuilder.Append("	CAST_NM					, 	").AppendLine();
		oSqlBuilder.Append("	HANDLE_NM 				,	").AppendLine();
		oSqlBuilder.Append("	OBJ_NOT_APPROVE_FLAG	,	").AppendLine();
		oSqlBuilder.Append("	OBJ_NOT_PUBLISH_FLAG	,	").AppendLine();
		oSqlBuilder.Append("	SMALL_PHOTO_IMG_PATH 		").AppendLine();
		oSqlBuilder.AppendFormat("	{0}	",sAddFirldList).AppendLine();
		oSqlBuilder.AppendFormat(" FROM {0}", sMainView).AppendLine();
		oSqlBuilder.Append(" WHERE				").AppendLine();
		oSqlBuilder.Append("	SITE_CD					=	:SITE_CD					AND	").AppendLine();
		oSqlBuilder.Append("	PICKUP_ID				=	:PICKUP_ID					AND	").AppendLine();
		oSqlBuilder.Append("	USER_STATUS				=	:USER_STATUS				AND ").AppendLine();
		oSqlBuilder.Append("	NA_FLAG					=	:NA_FLAG						").AppendLine();
		oSqlBuilder.Append("ORDER BY PRIORITY ASC, CREATE_DATE DESC, OBJ_SEQ ASC").AppendLine();
		
		DataSet oDs = new DataSet();
		try{
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn))
			using(da = new OracleDataAdapter(cmd)){
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD"				, pSiteCd);
				cmd.Parameters.Add(":PICKUP_ID"				,pPickupId);
				cmd.Parameters.Add(":USER_STATUS"			,ViCommConst.USER_WOMAN_NORMAL);
				cmd.Parameters.Add(":NA_FLAG"				,ViCommConst.FLAG_OFF);
				da.Fill(oDs);					
			}			
		}finally{
			conn.Close();
		}
		AppendExtendInfo(oDs.Tables[0]);

		return oDs;
	}

	public DataSet GetPageCollection(string pSiteCd, string pPickupId, string pPickupType, string pLoginId, string pHandleNm, string pPickupFlag, string pSortType, int startRowIndex, int maximumRows) {
		return GetPageCollection(pSiteCd, pPickupId, pPickupType, pLoginId, pHandleNm, pPickupFlag, true, true, pSortType, startRowIndex, maximumRows);
	}
	public DataSet GetPageCollection(string pSiteCd,string pPickupId,string pPickupType,string pLoginId,string pHandleNm,string pPickupFlag,bool pCheckUserStatus,bool pCheckNaFlag,string pSortType,int startRowIndex,int maximumRows) {
		int iUserStatusMask = pCheckUserStatus ? ViCommConst.MASK_WOMAN_NORMAL : 0;
		string sNaFlag = pCheckNaFlag ? ViCommConst.FLAG_OFF_STR : string.Empty;
		return GetPageCollection(pSiteCd,pPickupId,pPickupType,pLoginId,pHandleNm,pPickupFlag,iUserStatusMask,sNaFlag,pSortType,startRowIndex,maximumRows);
	}
	public DataSet GetPageCollection(string pSiteCd, string pPickupId, string pPickupType, string pLoginId, string pHandleNm, string pPickupFlag, int pUserStatusMask, string pNaFlag, string pSortType, int startRowIndex, int maximumRows)
	{

		// 主となるViewの決定

		string sMainView = string.Empty;

		//各Viewで必要になるﾌｨｰﾙﾄﾞ

		string sAddFirldList = "	,''	PIC_TYPE	,	'' VOTE_COUNT	,	'' COMMENT_COUNT	";
		switch (pPickupType) {
			case ViCommConst.PicupTypes.PROFILE_PIC:
			case ViCommConst.PicupTypes.BBS_PIC:
				sMainView = "VW_PICKUP_OBJS01";
				sAddFirldList = "	,	PIC_TYPE	,	VOTE_COUNT	,	COMMENT_COUNT	";
				break;
			case ViCommConst.PicupTypes.PROFILE_MOVIE:
			case ViCommConst.PicupTypes.BBS_MOVIE:
				sMainView = "VW_PICKUP_OBJS02";
				sAddFirldList = "	,'' PIC_TYPE	,	VOTE_COUNT	,	COMMENT_COUNT	";
				break;
			case ViCommConst.PicupTypes.CAST_CHARACTER:
				sMainView = "VW_PICKUP_OBJS03";
				break;
			default:
				throw new NotSupportedException(string.Format("不正なピックアップ区分 {0}", pPickupType));
		}

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT ");
		oSqlBuilder.AppendLine("	SITE_CD					, 	");
		oSqlBuilder.AppendLine("	PICKUP_ID				, 	");
		oSqlBuilder.AppendLine("	PICKUP_FLAG				, 	");
		oSqlBuilder.AppendLine("	OBJ_SEQ					, 	");
		oSqlBuilder.AppendLine("	COMMENT_PICKUP			, 	");
		oSqlBuilder.AppendLine("	PICKUP_START_PUB_DAY	, 	");
		oSqlBuilder.AppendLine("	PICKUP_END_PUB_DAY		, 	");
		oSqlBuilder.AppendLine("	PRIORITY				, 	");
		//oSqlBuilder.AppendLine("	PICKUP_FLAG				, 	");
		oSqlBuilder.AppendLine("	UPDATE_DATE				, 	");
		oSqlBuilder.AppendLine("	REVISION_NO				, 	");
		oSqlBuilder.AppendLine("	PICKUP_TYPE				, 	");
		oSqlBuilder.AppendLine("	OBJ_TITLE				, 	");
		oSqlBuilder.AppendLine("	LOGIN_ID				, 	");
		oSqlBuilder.AppendLine("	CAST_NM					, 	");
		oSqlBuilder.AppendLine("	HANDLE_NM 				,	");
		oSqlBuilder.AppendLine("	READING_COUNT			,	");
		oSqlBuilder.AppendLine("	OBJ_NOT_APPROVE_FLAG	,	");
		oSqlBuilder.AppendLine("	OBJ_NOT_PUBLISH_FLAG	,	");
		oSqlBuilder.AppendLine("	SMALL_PHOTO_IMG_PATH 	,	");
		oSqlBuilder.AppendLine("	USER_STATUS				,	");
		oSqlBuilder.AppendLine("	NA_FLAG						");
		oSqlBuilder.AppendFormat("	{0}	", sAddFirldList).AppendLine();
		oSqlBuilder.AppendFormat(" FROM {0}", sMainView).AppendLine();

		string sSortExpression = "ORDER BY PRIORITY ASC, CREATE_DATE DESC, OBJ_SEQ ASC";
		string sWhere;
		OracleParameter[] oParameters = this.CreateWhere(pSiteCd, pPickupId, pLoginId, pHandleNm, pPickupFlag, pUserStatusMask, pNaFlag, out sWhere);
		string sPagingSql;
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhere).ToString(), sSortExpression, startRowIndex, maximumRows, out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql, conn))
			using (da = new OracleDataAdapter(cmd)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oParameters);
				cmd.Parameters.AddRange(oPagingParams);
				//da.Fill(oDs);
				this.Fill(da, oDs);
			}
		} finally {
			conn.Close();
		}
		AppendExtendInfo(oDs.Tables[0]);

		return oDs;
	}

	public int GetPageCount(string pSiteCd,string pPickupId,string pPickupType,string pLoginId,string pHandleNm,string pPickupFlag, string pSortType) {
		return GetPageCount(pSiteCd, pPickupId, pPickupType, pLoginId, pHandleNm, pPickupFlag, true, true, pSortType);
	}
	public int GetPageCount(string pSiteCd,string pPickupId,string pPickupType,string pLoginId,string pHandleNm,string pPickupFlag,bool pCheckUserStatus,bool pCheckNaFlag,string pSortType) {
		int iUserStatusMask = pCheckUserStatus ? ViCommConst.MASK_WOMAN_NORMAL : 0;
		string sNaFlag = pCheckNaFlag ? ViCommConst.FLAG_OFF_STR : string.Empty;
		return GetPageCount(pSiteCd,pPickupId,pPickupType,pLoginId,pHandleNm,pPickupFlag,iUserStatusMask,sNaFlag,pSortType);
	}
	public int GetPageCount(string pSiteCd, string pPickupId, string pPickupType, string pLoginId, string pHandleNm, string pPickupFlag, int pUserStatusMask, string pNaFlag, string pSortType)
	{
		// 主となるViewの決定

		string sMainView = string.Empty;

		switch (pPickupType) {
			case ViCommConst.PicupTypes.PROFILE_PIC:
			case ViCommConst.PicupTypes.BBS_PIC:
				sMainView = "VW_PICKUP_OBJS01";
				break;
			case ViCommConst.PicupTypes.PROFILE_MOVIE:
			case ViCommConst.PicupTypes.BBS_MOVIE:
				sMainView = "VW_PICKUP_OBJS02";
				break;
			case ViCommConst.PicupTypes.CAST_CHARACTER:
				sMainView = "VW_PICKUP_OBJS03";
				break;
			default:
				throw new NotSupportedException(string.Format("不正なピックアップ区分 {0}", pPickupType));
		}

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT ");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendFormat(" FROM {0}", sMainView).AppendLine();

		string sWhere;
		OracleParameter[] oParameters = this.CreateWhere(pSiteCd, pPickupId, pLoginId, pHandleNm, pPickupFlag, pUserStatusMask, pNaFlag, out sWhere);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhere).ToString(), conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oParameters);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	private OracleParameter[] CreateWhere(string pSiteCd, string pPickupId, string pLoginId, string pHandleNm, string pPickupFlag, int pUserStatusMask, string pNaFlag, out string pWhere) {
		pWhere = string.Empty;
		List<OracleParameter> parameterList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD	= :SITE_CD", ref pWhere);
			parameterList.Add(new OracleParameter("SITE_CD", pSiteCd));
		}
		if (!string.IsNullOrEmpty(pPickupId)) {
			SysPrograms.SqlAppendWhere("PICKUP_ID = :PICKUP_ID", ref pWhere);
			parameterList.Add(new OracleParameter("PICKUP_ID", pPickupId));
		}
		if (!string.IsNullOrEmpty(pLoginId)) {
			string sLoginQuery = string.Empty;

			if (Regex.IsMatch(pLoginId,",|\r?\n")) {
				string sConjunction = "OR ";
				string[] sLoginIdArray = Regex.Split(pLoginId,",|\r?\n");
				StringBuilder oLoginIdWhere = new StringBuilder();
				for (int iIndex = 0; iIndex < sLoginIdArray.Length; iIndex++) {
					if (string.IsNullOrEmpty(sLoginIdArray[iIndex].Trim())) {
						continue;
					}
					oLoginIdWhere.AppendFormat("LOGIN_ID = :LOGIN_ID{0} OR ",iIndex);
					parameterList.Add(new OracleParameter(string.Concat("LOGIN_ID",iIndex),sLoginIdArray[iIndex].Trim()));
				}

				if (oLoginIdWhere.Length > 0) {
					sLoginQuery = string.Format("({0})",oLoginIdWhere.Remove(oLoginIdWhere.Length - sConjunction.Length,sConjunction.Length));
					SysPrograms.SqlAppendWhere(sLoginQuery,ref pWhere);
				}
			} else {
				SysPrograms.SqlAppendWhere("LOGIN_ID = :LOGIN_ID",ref pWhere);
				parameterList.Add(new OracleParameter("LOGIN_ID",pLoginId));
			}
		}
		if (!string.IsNullOrEmpty(pHandleNm)) {
			SysPrograms.SqlAppendWhere("HANDLE_NM LIKE :HANDLE_NM || '%'", ref pWhere);
			parameterList.Add(new OracleParameter("HANDLE_NM", pHandleNm));
		}
		if (!string.IsNullOrEmpty(pPickupFlag)) {
			SysPrograms.SqlAppendWhere("PICKUP_FLAG = :PICKUP_FLAG", ref pWhere);
			parameterList.Add(new OracleParameter("PICKUP_FLAG", pPickupFlag));
		}

		string[] sValues = new string[6];
		int iCnt = 0;
		string sWhere = string.Empty;

		if (pUserStatusMask != 0) {
			if ((pUserStatusMask & ViCommConst.MASK_WOMAN_AUTH_WAIT) != 0) {
				sValues[iCnt++] = ViCommConst.USER_WOMAN_AUTH_WAIT;
			}
			if ((pUserStatusMask & ViCommConst.MASK_WOMAN_NORMAL) != 0) {
				sValues[iCnt++] = ViCommConst.USER_WOMAN_NORMAL;
			}
			if ((pUserStatusMask & ViCommConst.MASK_WOMAN_STOP) != 0) {
				sValues[iCnt++] = ViCommConst.USER_WOMAN_STOP;
			}
			if ((pUserStatusMask & ViCommConst.MASK_WOMAN_HOLD) != 0) {
				sValues[iCnt++] = ViCommConst.USER_WOMAN_HOLD;
			}
			if ((pUserStatusMask & ViCommConst.MASK_WOMAN_RESIGNED) != 0) {
				sValues[iCnt++] = ViCommConst.USER_WOMAN_RESIGNED;
			}
			if ((pUserStatusMask & ViCommConst.MASK_WOMAN_BAN) != 0) {
				sValues[iCnt++] = ViCommConst.USER_WOMAN_BAN;
			}
			sWhere = "(USER_STATUS IN (";
			for (int i = 0; i < iCnt; i++) {
				sWhere = sWhere + ":USER_STATUS" + i.ToString() + ",";
				parameterList.Add(new OracleParameter("USER_STATUS" + i.ToString(),int.Parse(sValues[i])));
			}
			sWhere = sWhere.Substring(0,sWhere.Length - 1) + ")) ";
			SysPrograms.SqlAppendWhere(sWhere,ref pWhere);
		}
		
		if (!string.IsNullOrEmpty(pNaFlag)) {
			SysPrograms.SqlAppendWhere("NA_FLAG	= :NA_FLAG", ref pWhere);
			parameterList.Add(new OracleParameter("NA_FLAG", pNaFlag));
		}

		return parameterList.ToArray();
	}

	public bool Excists(string pSiteCd, string pPickupId, string pObjSeq) {

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT				").AppendLine();
		oSqlBuilder.Append("	COUNT(*)		").AppendLine();
		oSqlBuilder.Append(" FROM				").AppendLine();
		oSqlBuilder.Append("	T_PICKUP_OBJS	").AppendLine();
		oSqlBuilder.Append(" WHERE				").AppendLine();
		oSqlBuilder.Append("	SITE_CD		=	:SITE_CD		AND	").AppendLine();
		oSqlBuilder.Append("	PICKUP_ID	=	:PICKUP_ID		AND	").AppendLine();
		oSqlBuilder.Append("	OBJ_SEQ		=	:OBJ_SEQ			").AppendLine();

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD", pSiteCd);
				cmd.Parameters.Add(":PICKUP_ID", pPickupId);
				cmd.Parameters.Add(":OBJ_SEQ", pObjSeq);
				
				return ((decimal)cmd.ExecuteScalar()>0);
			}
		} finally {
			conn.Close();
		}
	}
	
	public List<string> GetPickupIdList(string pSiteCd, string sObjSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT ").AppendLine();
		oSqlBuilder.Append("	PICKUP_ID				 	").AppendLine();
		oSqlBuilder.Append(" FROM							").AppendLine();
		oSqlBuilder.Append("	T_PICKUP_OBJS				").AppendLine();
		oSqlBuilder.Append(" WHERE									").AppendLine();
		oSqlBuilder.Append("	SITE_CD			=	:SITE_CD		AND	").AppendLine();
		oSqlBuilder.Append("	OBJ_SEQ			=	:OBJ_SEQ			").AppendLine();
		oSqlBuilder.Append(" ORDER BY									").AppendLine();
		oSqlBuilder.Append("	SITE_CD,PICKUP_ID,OBJ_SEQ				").AppendLine();

		List<string> oOtherPickupList = new List<string>();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD", pSiteCd);
				cmd.Parameters.Add(":OBJ_SEQ", sObjSeq);

				using (OracleDataReader oDataReader = cmd.ExecuteReader()) {
					while (oDataReader.Read()) {
						string sOtherPickupId = iBridUtil.GetStringValue(oDataReader["PICKUP_ID"]);

						oOtherPickupList.Add(sOtherPickupId);
					}
				}
			}
		} finally {
			conn.Close();
		}

		return oOtherPickupList;
	}

	private void AppendExtendInfo(DataTable pPickupObjDataTable) {

		pPickupObjDataTable.Columns.Add(new DataColumn("OTHER_PICKUP", typeof(string)));

		if (pPickupObjDataTable.Rows.Count == 0) return;

		foreach (DataRow pPickupObjDataRow in pPickupObjDataTable.Rows) {
			string sSiteCd = iBridUtil.GetStringValue(pPickupObjDataRow["SITE_CD"]);
			string sPickupId = iBridUtil.GetStringValue(pPickupObjDataRow["PICKUP_ID"]);
			string sObjSeq = iBridUtil.GetStringValue(pPickupObjDataRow["OBJ_SEQ"]);

			string sOtherPickup = string.Empty;
			foreach (string sOtherPickupId in this.GetPickupIdList(sSiteCd, sObjSeq)) {
				if (sOtherPickupId.Equals(sPickupId)) continue;
				if (sOtherPickup.Length != 0) {
					sOtherPickup += ",";
				}
				sOtherPickup += sOtherPickupId;
			}
			pPickupObjDataRow["OTHER_PICKUP"] = sOtherPickup;
		}
	}

}
