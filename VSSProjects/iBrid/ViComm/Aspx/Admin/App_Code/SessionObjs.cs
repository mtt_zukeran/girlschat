﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public class SessionObjs {
	public Site site;
	
	public static string AdminId{
		get { return GetSessionValue("AdminId"); }
	}
	public static string MenuSite{
		get{ return GetSessionValue("MENU_SITE"); }
	}
	
	private static string GetSessionValue(string pKey){
		string sValue = string.Empty;
		if (HttpContext.Current != null && HttpContext.Current.Session != null) {
			sValue = HttpContext.Current.Session[pKey] as string ?? string.Empty;
		}
		return sValue;
	}

	public SessionObjs() {
		site = new Site();
	}
}
