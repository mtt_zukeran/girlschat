﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 一括会員取込データ

--	Progaram ID		: BulkImpManData
--
--  Creation Date	: 2010.10.20
--  Creater			: K.Itoh
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Web;
using System.Data;
using System.Data.Common;
using System.Text;
using iBridCommLib;

public class BulkImpManData :DbSession{
	public BulkImpManData() {}
	
	public DataSet GetErrorList(decimal dSeq){
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.Append("SELECT").AppendLine();
		oSqlBuilder.Append("	TEXT_LINE_NO	,").AppendLine();
		oSqlBuilder.Append("	TEXT_DTL		,").AppendLine();
		oSqlBuilder.Append("	ERROR_MSG		 ").AppendLine();
		oSqlBuilder.Append("FROM").AppendLine();
		oSqlBuilder.Append("	T_BULK_IMP_MAN_DATA").AppendLine();
		oSqlBuilder.Append("WHERE").AppendLine();
		oSqlBuilder.Append("	BULK_IMP_MAN_HISTORY_SEQ	=:BULK_IMP_MAN_HISTORY_SEQ	AND ").AppendLine();
		oSqlBuilder.Append("	BULK_PROC_STATUS			=:BULK_PROC_STATUS		").AppendLine();
		
		DataSet oDs = new DataSet();
		try{
			conn = DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(new OracleParameter(":BULK_IMP_MAN_HISTORY_SEQ", dSeq));
				cmd.Parameters.Add(new OracleParameter(":BULK_PROC_STATUS", ViComm.ViCommConst.BulkProcStatus.ERROR));
				
				using(da = new OracleDataAdapter(cmd)){
					da.Fill(oDs);
				}				
			}
		}finally{
			conn.Close();
		}
		
		return oDs;
	}
}
