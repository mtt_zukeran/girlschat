﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 1日1回プレゼントアイテム
--	Progaram ID		: OnceDayPresent
--
--  Creation Date	: 2011.12.02
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;


public class OnceDayPresent : DbSession {
	public OnceDayPresent() {
	}

	public int GetPageCount(string pSiteCd,string pSexCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT NVL(COUNT(*),0) FROM VW_ONCE_DAY_PRESENT01");

		string sWhereClause;
		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pSexCd,out sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pSexCd,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT						");
		oSqlBuilder.AppendLine("	SITE_CD					,");
		oSqlBuilder.AppendLine("	SEX_CD					,");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ			,");
		oSqlBuilder.AppendLine("	GAME_ITEM_NM			,");
		oSqlBuilder.AppendLine("	PUBLISH_FLAG			,");
		oSqlBuilder.AppendLine("	PUBLISH_DATE			,");
		oSqlBuilder.AppendLine("	CREATE_DATE				,");
		oSqlBuilder.AppendLine("	GAME_ITEM_CATEGORY_TYPE ,");
		oSqlBuilder.AppendLine("	GAME_ITEM_CATEGORY_NM	,");
		oSqlBuilder.AppendLine("	REVISION_NO				,");
		oSqlBuilder.AppendLine("	UPDATE_DATE				");
		oSqlBuilder.AppendLine("FROM						");
		oSqlBuilder.AppendLine("	VW_ONCE_DAY_PRESENT01	");

		string sWhereClause;
		string sPagingSql;
		string sSortExpression = "ORDER BY SITE_CD, GAME_ITEM_SEQ";

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pSexCd,out sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pSexCd,out string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}
		if (!string.IsNullOrEmpty(pSexCd)) {
			SysPrograms.SqlAppendWhere("SEX_CD = :SEX_CD",ref pWhere);
			list.Add(new OracleParameter("SEX_CD",pSexCd));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
	
	public bool IsDuplicate(string pSiteCd,string pGameItemSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.Append(" SELECT										").AppendLine();
		oSqlBuilder.Append(" 	NVL(COUNT(*),0)							").AppendLine();
		oSqlBuilder.Append(" FROM										").AppendLine();
		oSqlBuilder.Append(" 	T_ONCE_DAY_PRESENT						").AppendLine();
		oSqlBuilder.Append(" WHERE										").AppendLine();
		oSqlBuilder.Append(" 	SITE_CD			= :SITE_CD			AND	").AppendLine();
		oSqlBuilder.Append(" 	GAME_ITEM_SEQ	= :GAME_ITEM_SEQ	").AppendLine();

		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":GAME_ITEM_SEQ",pGameItemSeq);

				int iCount = int.Parse(cmd.ExecuteScalar().ToString());
				return iCount > 0;
			}
		} finally {
			conn.Close();
		}
	}

}
