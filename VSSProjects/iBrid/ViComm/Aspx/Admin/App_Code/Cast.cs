/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者
--	Progaram ID		: Cast
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;
using System.Text;

public class Cast:DbSession {
	public string userSeq;
	public string castNm;

	public Cast() {
	}

	public int GetPageCount(
		string pOnline,
		string pLoginId,
		string pProductionSeq,
		string pCastNm,
		string pSiteCd,
		string pEmailAddr,
		string pUserStatus,
		string pRegistDayFrom,
		string pRegistDayTo,
		string pLastLoginDayFrom,
		string pLastLoginDayTo,
		string pTotalPaymentAmtFrom,
		string pTotalPaymentAmtTo,
		bool pBankAccountInvalidFlag,
		string pNonExistMailAddrFlag
	) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(USER_SEQ) AS ROW_COUNT FROM VW_CAST07  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(
												pOnline,
												pLoginId,
												pProductionSeq,
												pCastNm,
												pSiteCd,
												pUserStatus,
												pEmailAddr,
												pRegistDayFrom,
												pRegistDayTo,
												pLastLoginDayFrom,
												pLastLoginDayTo,
												pTotalPaymentAmtFrom,
												pTotalPaymentAmtTo,
												pBankAccountInvalidFlag,
												pNonExistMailAddrFlag,
												ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				this.Fill(da,ds,"VW_CAST07");
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(
		string pOnline,
		string pLoginId,
		string pProductionSeq,
		string pCastNm,
		string pSiteCd,
		string pUserStatus,
		string pEmailAddr,
		string pRegistDayFrom,
		string pRegistDayTo,
		string pLastLoginDayFrom,
		string pLastLoginDayTo,
		string pTotalPaymentAmtFrom,
		string pTotalPaymentAmtTo,
		bool pBankAccountInvalidFlag,
		string pNonExistMailAddrFlag,
		int startRowIndex,
		int maximumRows
	) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY LOGIN_ID ";
			StringBuilder sSql = new StringBuilder();

			sSql.Append("SELECT ").AppendLine();
			sSql.Append("USER_SEQ					,").AppendLine();
			sSql.Append("PRODUCTION_SEQ				,").AppendLine();
			sSql.Append("MANAGER_SEQ				,").AppendLine();
			sSql.Append("CAST_NM					,").AppendLine();
			sSql.Append("CAST_KANA_NM				,").AppendLine();
			sSql.Append("USER_STATUS				,").AppendLine();
			sSql.Append("CONTACT_TEL				,").AppendLine();
			sSql.Append("EMAIL_ADDR					,").AppendLine();
			sSql.Append("LOGIN_ID					,").AppendLine();
			sSql.Append("LOGIN_PASSWORD				,").AppendLine();
			sSql.Append("STAFF_ID					,").AppendLine();
			sSql.Append("BANK_ACCOUNT_INVALID_FLAG	,").AppendLine();
			sSql.Append("TALKING_FLAG				,").AppendLine();
			sSql.Append("TALK_LOCK_FLAG				,").AppendLine();
			sSql.Append("TALK_LOCK_DATE				,").AppendLine();
			sSql.Append("CALL_STATUS_SEQ			,").AppendLine();
			sSql.Append("REGIST_DATE				,").AppendLine();
			sSql.Append("UPDATE_DATE				,").AppendLine();
			sSql.Append("REVISION_NO				,").AppendLine();
			sSql.Append("INTRODUCER_FRIEND_CD		,").AppendLine();
			sSql.Append("START_DATE					,").AppendLine();
			sSql.Append("START_YM					,").AppendLine();
			sSql.Append("START_DD					,").AppendLine();
			sSql.Append("REPORT_DAY					,").AppendLine();
			sSql.Append("LOGOFF_DATE				,").AppendLine();
			sSql.Append("MANAGER_NM					,").AppendLine();
			sSql.Append("PRODUCTION_NM				,").AppendLine();
			sSql.Append("USER_STATUS_NM				,").AppendLine();
			sSql.Append("CHARACTER_ONLINE_STATUS1	,").AppendLine();
			sSql.Append("CHARACTER_ONLINE_STATUS2	,").AppendLine();
			sSql.Append("CHARACTER_ONLINE_STATUS3	,").AppendLine();
			sSql.Append("CHARACTER_ONLINE_STATUS4	,").AppendLine();
			sSql.Append("CHARACTER_ONLINE_STATUS5	,").AppendLine();
			sSql.Append("CHARACTER_ONLINE_STATUS6	,").AppendLine();
			sSql.Append("CHARACTER_ONLINE_STATUS7	,").AppendLine();
			sSql.Append("CHARACTER_ONLINE_STATUS8	,").AppendLine();
			sSql.Append("CHARACTER_ONLINE_STATUS9   ,").AppendLine();
			sSql.Append("CASE WHEN CHARACTER_ONLINE_STATUS1 IS NOT NULL THEN GET_SYS_ONLINE_IMG_PATH(CHARACTER_ONLINE_STATUS1,IVP_REQUEST_SEQ,DUMMY_TALK_FLAG,MONITOR_ENABLE_FLAG,TALK_LOCK_FLAG,TALK_LOCK_DATE) ELSE NULL END AS SYS_ONLINE_IMG_PATH1,").AppendLine();
			sSql.Append("CASE WHEN CHARACTER_ONLINE_STATUS2 IS NOT NULL THEN GET_SYS_ONLINE_IMG_PATH(CHARACTER_ONLINE_STATUS2,IVP_REQUEST_SEQ,DUMMY_TALK_FLAG,MONITOR_ENABLE_FLAG,TALK_LOCK_FLAG,TALK_LOCK_DATE) ELSE NULL END AS SYS_ONLINE_IMG_PATH2,").AppendLine();
			sSql.Append("CASE WHEN CHARACTER_ONLINE_STATUS3 IS NOT NULL THEN GET_SYS_ONLINE_IMG_PATH(CHARACTER_ONLINE_STATUS3,IVP_REQUEST_SEQ,DUMMY_TALK_FLAG,MONITOR_ENABLE_FLAG,TALK_LOCK_FLAG,TALK_LOCK_DATE) ELSE NULL END AS SYS_ONLINE_IMG_PATH3,").AppendLine();
			sSql.Append("CASE WHEN CHARACTER_ONLINE_STATUS4 IS NOT NULL THEN GET_SYS_ONLINE_IMG_PATH(CHARACTER_ONLINE_STATUS4,IVP_REQUEST_SEQ,DUMMY_TALK_FLAG,MONITOR_ENABLE_FLAG,TALK_LOCK_FLAG,TALK_LOCK_DATE) ELSE NULL END AS SYS_ONLINE_IMG_PATH4,").AppendLine();
			sSql.Append("CASE WHEN CHARACTER_ONLINE_STATUS5 IS NOT NULL THEN GET_SYS_ONLINE_IMG_PATH(CHARACTER_ONLINE_STATUS5,IVP_REQUEST_SEQ,DUMMY_TALK_FLAG,MONITOR_ENABLE_FLAG,TALK_LOCK_FLAG,TALK_LOCK_DATE) ELSE NULL END AS SYS_ONLINE_IMG_PATH5,").AppendLine();
			sSql.Append("CASE WHEN CHARACTER_ONLINE_STATUS6 IS NOT NULL THEN GET_SYS_ONLINE_IMG_PATH(CHARACTER_ONLINE_STATUS6,IVP_REQUEST_SEQ,DUMMY_TALK_FLAG,MONITOR_ENABLE_FLAG,TALK_LOCK_FLAG,TALK_LOCK_DATE) ELSE NULL END AS SYS_ONLINE_IMG_PATH6,").AppendLine();
			sSql.Append("CASE WHEN CHARACTER_ONLINE_STATUS7 IS NOT NULL THEN GET_SYS_ONLINE_IMG_PATH(CHARACTER_ONLINE_STATUS7,IVP_REQUEST_SEQ,DUMMY_TALK_FLAG,MONITOR_ENABLE_FLAG,TALK_LOCK_FLAG,TALK_LOCK_DATE) ELSE NULL END AS SYS_ONLINE_IMG_PATH7,").AppendLine();
			sSql.Append("CASE WHEN CHARACTER_ONLINE_STATUS8 IS NOT NULL THEN GET_SYS_ONLINE_IMG_PATH(CHARACTER_ONLINE_STATUS8,IVP_REQUEST_SEQ,DUMMY_TALK_FLAG,MONITOR_ENABLE_FLAG,TALK_LOCK_FLAG,TALK_LOCK_DATE) ELSE NULL END AS SYS_ONLINE_IMG_PATH8,").AppendLine();
			sSql.Append("CASE WHEN CHARACTER_ONLINE_STATUS9 IS NOT NULL THEN GET_SYS_ONLINE_IMG_PATH(CHARACTER_ONLINE_STATUS9,IVP_REQUEST_SEQ,DUMMY_TALK_FLAG,MONITOR_ENABLE_FLAG,TALK_LOCK_FLAG,TALK_LOCK_DATE) ELSE NULL END AS SYS_ONLINE_IMG_PATH9").AppendLine();
			sSql.Append("FROM(").AppendLine();
			sSql.Append(" SELECT VW_CAST07.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_CAST07  ").AppendLine();
			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(
												pOnline,
												pLoginId,
												pProductionSeq,
												pCastNm,
												pSiteCd,
												pUserStatus,
												pEmailAddr,
												pRegistDayFrom,
												pRegistDayTo,
												pLastLoginDayFrom,
												pLastLoginDayTo,
												pTotalPaymentAmtFrom,
												pTotalPaymentAmtTo,
												pBankAccountInvalidFlag,
												pNonExistMailAddrFlag,
												ref sWhere);
			sSql.Append(sWhere).AppendLine();
			sSql.Append(")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ").AppendLine();
			sSql.Append(sOrder);

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
                    this.Fill(da,ds,"VW_CAST07");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(
		string pOnline,
		string pLoginId,
		string pProductionSeq,
		string pCastNm,
		string pSiteCd,
		string pUserStatus,
		string pEmailAddr,
		string pRegistDayFrom,
		string pRegistDayTo,
		string pLastLoginDayFrom,
		string pLastLoginDayTo,
		string pTotalPaymentAmtFrom,
		string pTotalPaymentAmtTo,
		bool pBankAccountInvalidFlag,
		string pNonExistMailAddrFlag,
		ref string pWhere
	) {
		pWhere = "";

		ArrayList list = new ArrayList();

		if (pSiteCd.Equals("") == false) {
			SysPrograms.SqlAppendWhere("EXISTS( SELECT 'X' FROM T_CAST_CHARACTER WHERE SITE_CD = :SITE_CD AND USER_SEQ = VW_CAST07.USER_SEQ AND USER_CHAR_NO = :USER_CHAR_NO AND NA_FLAG = :NA_FLAG )",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
			list.Add(new OracleParameter("USER_CHAR_NO",ViCommConst.MAIN_CHAR_NO));
			list.Add(new OracleParameter("NA_FLAG",ViCommConst.FLAG_OFF));
		}

		if (pOnline.Equals("") == false) {
			if (!pOnline.Equals("*")) {
				SysPrograms.SqlAppendWhere(
					"((CHARACTER_ONLINE_STATUS1 = :CHARACTER_ONLINE_STATUS1) OR " +
					"(CHARACTER_ONLINE_STATUS2 = :CHARACTER_ONLINE_STATUS2) OR " +
					"(CHARACTER_ONLINE_STATUS3 = :CHARACTER_ONLINE_STATUS3) OR " +
					"(CHARACTER_ONLINE_STATUS4 = :CHARACTER_ONLINE_STATUS4) OR " +
					"(CHARACTER_ONLINE_STATUS5 = :CHARACTER_ONLINE_STATUS5) OR " +
					"(CHARACTER_ONLINE_STATUS6 = :CHARACTER_ONLINE_STATUS6) OR " +
					"(CHARACTER_ONLINE_STATUS7 = :CHARACTER_ONLINE_STATUS7) OR " +
					"(CHARACTER_ONLINE_STATUS8 = :CHARACTER_ONLINE_STATUS8) OR " +
					"(CHARACTER_ONLINE_STATUS9 = :CHARACTER_ONLINE_STATUS9))",ref pWhere
				);
				list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS1",pOnline));
				list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS2",pOnline));
				list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS3",pOnline));
				list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS4",pOnline));
				list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS5",pOnline));
				list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS6",pOnline));
				list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS7",pOnline));
				list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS8",pOnline));
				list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS9",pOnline));
			} else {
				SysPrograms.SqlAppendWhere(
				   "((CHARACTER_ONLINE_STATUS1 IN (:CHARACTER_ONLINE_STATUS11,:CHARACTER_ONLINE_STATUS21)) OR " +
					"(CHARACTER_ONLINE_STATUS2 IN (:CHARACTER_ONLINE_STATUS12,:CHARACTER_ONLINE_STATUS22)) OR " +
					"(CHARACTER_ONLINE_STATUS3 IN (:CHARACTER_ONLINE_STATUS13,:CHARACTER_ONLINE_STATUS23)) OR " +
					"(CHARACTER_ONLINE_STATUS4 IN (:CHARACTER_ONLINE_STATUS14,:CHARACTER_ONLINE_STATUS24)) OR " +
					"(CHARACTER_ONLINE_STATUS5 IN (:CHARACTER_ONLINE_STATUS15,:CHARACTER_ONLINE_STATUS25)) OR " +
					"(CHARACTER_ONLINE_STATUS6 IN (:CHARACTER_ONLINE_STATUS16,:CHARACTER_ONLINE_STATUS26)) OR " +
					"(CHARACTER_ONLINE_STATUS7 IN (:CHARACTER_ONLINE_STATUS17,:CHARACTER_ONLINE_STATUS27)) OR " +
					"(CHARACTER_ONLINE_STATUS8 IN (:CHARACTER_ONLINE_STATUS18,:CHARACTER_ONLINE_STATUS28)) OR " +
					"(CHARACTER_ONLINE_STATUS9 IN (:CHARACTER_ONLINE_STATUS19,:CHARACTER_ONLINE_STATUS29)))",ref pWhere
				);
				list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS11",ViCommConst.USER_WAITING));
				list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS21",ViCommConst.USER_TALKING));
				list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS12",ViCommConst.USER_WAITING));
				list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS22",ViCommConst.USER_TALKING));
				list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS13",ViCommConst.USER_WAITING));
				list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS23",ViCommConst.USER_TALKING));
				list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS14",ViCommConst.USER_WAITING));
				list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS24",ViCommConst.USER_TALKING));
				list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS15",ViCommConst.USER_WAITING));
				list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS25",ViCommConst.USER_TALKING));
				list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS16",ViCommConst.USER_WAITING));
				list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS26",ViCommConst.USER_TALKING));
				list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS17",ViCommConst.USER_WAITING));
				list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS27",ViCommConst.USER_TALKING));
				list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS18",ViCommConst.USER_WAITING));
				list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS28",ViCommConst.USER_TALKING));
				list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS19",ViCommConst.USER_WAITING));
				list.Add(new OracleParameter("CHARACTER_ONLINE_STATUS29",ViCommConst.USER_TALKING));
			}
		}

		if (pUserStatus.Equals("") == false) {
			SysPrograms.SqlAppendWhere("USER_STATUS = :USER_STATUS",ref pWhere);
			list.Add(new OracleParameter("USER_STATUS",pUserStatus));
		}

		if (pBankAccountInvalidFlag) {
			SysPrograms.SqlAppendWhere("BANK_ACCOUNT_INVALID_FLAG = :BANK_ACCOUNT_INVALID_FLAG",ref pWhere);
			list.Add(new OracleParameter("BANK_ACCOUNT_INVALID_FLAG","1"));
		}

		if (pLoginId.Equals("") == false) {
			SysPrograms.SqlAppendWhere("LOGIN_ID LIKE :LOGIN_ID||'%'",ref pWhere);
			list.Add(new OracleParameter("LOGIN_ID",pLoginId));
		}

		if (pProductionSeq.Equals("") == false) {
			SysPrograms.SqlAppendWhere("PRODUCTION_SEQ = :PRODUCTION_SEQ",ref pWhere);
			list.Add(new OracleParameter("PRODUCTION_SEQ",pProductionSeq));
		}

		if (pCastNm.Equals("") == false) {
			SysPrograms.SqlAppendWhere("CAST_NM LIKE '%'||:CAST_NM||'%'",ref pWhere);
			list.Add(new OracleParameter("CAST_NM",pCastNm));
		}

		if (pEmailAddr.Equals("") == false) {
			SysPrograms.SqlAppendWhere("EMAIL_ADDR LIKE '%' || :EMAIL_ADDR || '%'",ref pWhere);
			list.Add(new OracleParameter("EMAIL_ADDR",pEmailAddr.ToLower()));
		}

		if (!pRegistDayFrom.Equals("")) {
			DateTime dtFrom = DateTime.Parse(pRegistDayFrom + " " + "00:00:00");
			DateTime dtTo = DateTime.Parse(pRegistDayTo + " " + "23:59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere("(REGIST_DATE >= :REGIST_DATE_FROM AND REGIST_DATE < :REGIST_DATE_TO)",ref pWhere);
			list.Add(new OracleParameter("REGIST_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("REGIST_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (!pLastLoginDayFrom.Equals("")) {
			DateTime dtFrom = DateTime.Parse(pLastLoginDayFrom + " " + "00:00:00");
			DateTime dtTo = DateTime.Parse(pLastLoginDayTo + " " + "23:59:59").AddSeconds(1);
			SysPrograms.SqlAppendWhere("(LAST_LOGIN_DATE >= :LAST_LOGIN_DATE_FROM AND LAST_LOGIN_DATE < :LAST_LOGIN_DATE_TO)",ref pWhere);
			list.Add(new OracleParameter("LAST_LOGIN_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("LAST_LOGIN_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (!pTotalPaymentAmtFrom.Equals("")) {
			SysPrograms.SqlAppendWhere("(TOTAL_PAYMENT_AMT >= :TOTAL_PAYMENT_AMT_FROM AND TOTAL_PAYMENT_AMT <= :TOTAL_PAYMENT_AMT_TO)",ref pWhere);
			list.Add(new OracleParameter("TOTAL_PAYMENT_AMT_FROM",int.Parse(pTotalPaymentAmtFrom)));
			list.Add(new OracleParameter("TOTAL_PAYMENT_AMT_TO",int.Parse(pTotalPaymentAmtTo)));
		}

		if (!string.IsNullOrEmpty(pNonExistMailAddrFlag)) {
			SysPrograms.SqlAppendWhere("NON_EXIST_MAIL_ADDR_FLAG = :NON_EXIST_MAIL_ADDR_FLAG",ref pWhere);
			list.Add(new OracleParameter("NON_EXIST_MAIL_ADDR_FLAG",pNonExistMailAddrFlag));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public DataSet GetOne(string pLoginID) {
		return GetOne(pLoginID,string.Empty);
	}

	public DataSet GetOne(string pLoginID,string pUserSeq) {
		if (string.IsNullOrEmpty(pLoginID) && string.IsNullOrEmpty(pUserSeq)) {
			throw new ArgumentException();
		}

		string sWhereClause = string.Empty;
		StringBuilder sSql = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pLoginID)) {
			if ((pLoginID.StartsWith("070") || pLoginID.StartsWith("080") || pLoginID.StartsWith("090")) && (pLoginID.IndexOf('@') < 0) && (pLoginID.Length == 11)) {
				SysPrograms.SqlAppendWhere("TEL = :ID",ref sWhereClause);
			} else if (pLoginID.IndexOf('@') >= 0) {
				SysPrograms.SqlAppendWhere("EMAIL_ADDR LIKE :ID ||'%'",ref sWhereClause);
				pLoginID = pLoginID.ToLower();
				if (pLoginID.EndsWith("@")) {
					pLoginID = pLoginID.Substring(0,pLoginID.Length - 1);
				}
			} else {
				SysPrograms.SqlAppendWhere("(LOGIN_ID = :ID OR PREVIOUS_LOGIN_ID = :ID)",ref sWhereClause);
			}

			oParamList.Add(new OracleParameter(":ID",pLoginID));
		}

		if (!string.IsNullOrEmpty(pUserSeq)) {
			SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref sWhereClause);
			oParamList.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		}

		sSql.AppendLine("SELECT ");
		sSql.AppendLine("USER_SEQ			,");
		sSql.AppendLine("PRODUCTION_SEQ		,");
		sSql.AppendLine("MANAGER_SEQ		,");
		sSql.AppendLine("CAST_NM			,");
		sSql.AppendLine("CAST_KANA_NM		,");
		sSql.AppendLine("LOGIN_ID			,");
		sSql.AppendLine("LOGIN_PASSWORD		,");
		sSql.AppendLine("USER_STATUS_NM		,");
		sSql.AppendLine("EMAIL_ADDR			,");
		sSql.AppendLine("NON_EXIST_MAIL_ADDR_FLAG,");
		sSql.AppendLine("TX_MULLER3_NG_MAIL_ADDR_FLAG,");
		sSql.AppendLine("SUBSTRB(MOBILE_TERMINAL_NM,1,20) MOBILE_TERMINAL_SHORT_NM,");
		sSql.AppendLine("MOBILE_CARRIER_CD,");
		sSql.AppendLine("MOBILE_TERMINAL_NM,");
		sSql.AppendLine("INTRODUCER_FRIEND_CD,");
		sSql.AppendLine("BEFORE_SYSTEM_ID,");
		sSql.AppendLine("USE_TERMINAL_TYPE_NM,");
		sSql.AppendLine("URI				,");
		sSql.AppendLine("TEL				,");
		sSql.AppendLine("IMODE_ID			,");
		sSql.AppendLine("TERMINAL_UNIQUE_ID	,");
		sSql.AppendLine("PRODUCTION_NM		,");
		sSql.AppendLine("MANAGER_NM			,");
		sSql.AppendLine("GET_PAYMENT_POINT(USER_SEQ) * POINT_PRICE			TOTAL_PAYMENT_AMT,");
		sSql.AppendLine("GET_PAYMENT_POINT_NOT_WORK(USER_SEQ) * POINT_PRICE	TOTAL_PAYMENT_AMT_NOT_WORK,");
		sSql.AppendLine("GET_NOT_PAYMENT_POINT(USER_SEQ) * POINT_PRICE		NOT_PAYMENT_AMT,");
		sSql.AppendLine("GET_NOT_PAYMENT_POINT(USER_SEQ)	NOT_PAYMENT_POINT,");
		sSql.AppendLine("REQ_PAYMENT_DATE	,");
		sSql.AppendLine("USER_RANK          ,");
		sSql.AppendLine("STAFF_ID           ,");
		sSql.AppendLine("UPDATE_DATE		,");
		sSql.AppendLine("REGIST_DATE		,");
		sSql.AppendLine("LAST_LOGIN_DATE	,");
		sSql.AppendLine("DUMMY_TALK_FLAG	,");
		sSql.AppendLine("CROSMILE_LAST_USED_VERSION	,");
		sSql.AppendLine("USER_STATUS		,");
		sSql.AppendLine("DEVICE_TOKEN		,");
		sSql.AppendLine("GCAPP_LAST_LOGIN_VERSION,");
		sSql.AppendLine("GCAPP_LAST_LOGIN_DATE");
		sSql.AppendLine("FROM VW_CAST06");
		sSql.AppendLine(sWhereClause);

		DataSet ds = ExecuteSelectQueryBase(sSql.ToString(),oParamList.ToArray());

		return ds;
	}

	public DataSet GetSamePhoneUser(string pSiteCd,string pOnLine,string pTerminalId,bool pPC) {
		DataSet ds;
		string sSiteCd = iBridUtil.GetStringValue(pSiteCd);
		string sOnline = iBridUtil.GetStringValue(pOnLine);
		string sTerminalId = iBridUtil.GetStringValue(pTerminalId);
		string sWhere = "";
		try {
			conn = DbConnect();
			ds = new DataSet();
			string sType = "TEL";
			int iLen = 11;
			if (pPC) {
				sType = "URI";
				iLen = 6;
			}
			StringBuilder sSql = new StringBuilder();

			sSql.Append("SELECT ").AppendLine();
			sSql.Append("	LOGIN_ID					,").AppendLine();
			sSql.Append("	USER_SEQ					,").AppendLine();
			sSql.Append("	TERM_ID						,").AppendLine();
			sSql.Append("	PRODUCTION_NM				,").AppendLine();
			sSql.Append("	MANAGER_NM					,").AppendLine();
			sSql.Append("	CAST_NM						,").AppendLine();
			sSql.Append("	EMAIL_ADDR					,").AppendLine();
			sSql.Append("	USER_STATUS					,").AppendLine();
			sSql.Append("	USER_STATUS_NM				,").AppendLine();
			sSql.Append("	CHARACTER_ONLINE_STATUS1	,").AppendLine();
			sSql.Append("	CHARACTER_ONLINE_STATUS2	,").AppendLine();
			sSql.Append("	CHARACTER_ONLINE_STATUS3	,").AppendLine();
			sSql.Append("	CHARACTER_ONLINE_STATUS4	,").AppendLine();
			sSql.Append("	CHARACTER_ONLINE_STATUS5	,").AppendLine();
			sSql.Append("	CHARACTER_ONLINE_STATUS6	,").AppendLine();
			sSql.Append("	CHARACTER_ONLINE_STATUS7	,").AppendLine();
			sSql.Append("	CHARACTER_ONLINE_STATUS8	,").AppendLine();
			sSql.Append("	CHARACTER_ONLINE_STATUS9	,").AppendLine();
			sSql.Append("	CASE WHEN CHARACTER_ONLINE_STATUS1 IS NOT NULL THEN GET_SYS_ONLINE_IMG_PATH(CHARACTER_ONLINE_STATUS1,IVP_REQUEST_SEQ,DUMMY_TALK_FLAG,MONITOR_ENABLE_FLAG,TALK_LOCK_FLAG,TALK_LOCK_DATE) ELSE NULL END AS SYS_ONLINE_IMG_PATH1,").AppendLine();
			sSql.Append("	CASE WHEN CHARACTER_ONLINE_STATUS2 IS NOT NULL THEN GET_SYS_ONLINE_IMG_PATH(CHARACTER_ONLINE_STATUS2,IVP_REQUEST_SEQ,DUMMY_TALK_FLAG,MONITOR_ENABLE_FLAG,TALK_LOCK_FLAG,TALK_LOCK_DATE) ELSE NULL END AS SYS_ONLINE_IMG_PATH2,").AppendLine();
			sSql.Append("	CASE WHEN CHARACTER_ONLINE_STATUS3 IS NOT NULL THEN GET_SYS_ONLINE_IMG_PATH(CHARACTER_ONLINE_STATUS3,IVP_REQUEST_SEQ,DUMMY_TALK_FLAG,MONITOR_ENABLE_FLAG,TALK_LOCK_FLAG,TALK_LOCK_DATE) ELSE NULL END AS SYS_ONLINE_IMG_PATH3,").AppendLine();
			sSql.Append("	CASE WHEN CHARACTER_ONLINE_STATUS4 IS NOT NULL THEN GET_SYS_ONLINE_IMG_PATH(CHARACTER_ONLINE_STATUS4,IVP_REQUEST_SEQ,DUMMY_TALK_FLAG,MONITOR_ENABLE_FLAG,TALK_LOCK_FLAG,TALK_LOCK_DATE) ELSE NULL END AS SYS_ONLINE_IMG_PATH4,").AppendLine();
			sSql.Append("	CASE WHEN CHARACTER_ONLINE_STATUS5 IS NOT NULL THEN GET_SYS_ONLINE_IMG_PATH(CHARACTER_ONLINE_STATUS5,IVP_REQUEST_SEQ,DUMMY_TALK_FLAG,MONITOR_ENABLE_FLAG,TALK_LOCK_FLAG,TALK_LOCK_DATE) ELSE NULL END AS SYS_ONLINE_IMG_PATH5,").AppendLine();
			sSql.Append("	CASE WHEN CHARACTER_ONLINE_STATUS6 IS NOT NULL THEN GET_SYS_ONLINE_IMG_PATH(CHARACTER_ONLINE_STATUS6,IVP_REQUEST_SEQ,DUMMY_TALK_FLAG,MONITOR_ENABLE_FLAG,TALK_LOCK_FLAG,TALK_LOCK_DATE) ELSE NULL END AS SYS_ONLINE_IMG_PATH6,").AppendLine();
			sSql.Append("	CASE WHEN CHARACTER_ONLINE_STATUS7 IS NOT NULL THEN GET_SYS_ONLINE_IMG_PATH(CHARACTER_ONLINE_STATUS7,IVP_REQUEST_SEQ,DUMMY_TALK_FLAG,MONITOR_ENABLE_FLAG,TALK_LOCK_FLAG,TALK_LOCK_DATE) ELSE NULL END AS SYS_ONLINE_IMG_PATH7,").AppendLine();
			sSql.Append("	CASE WHEN CHARACTER_ONLINE_STATUS8 IS NOT NULL THEN GET_SYS_ONLINE_IMG_PATH(CHARACTER_ONLINE_STATUS8,IVP_REQUEST_SEQ,DUMMY_TALK_FLAG,MONITOR_ENABLE_FLAG,TALK_LOCK_FLAG,TALK_LOCK_DATE) ELSE NULL END AS SYS_ONLINE_IMG_PATH8,").AppendLine();
			sSql.Append("	CASE WHEN CHARACTER_ONLINE_STATUS9 IS NOT NULL THEN GET_SYS_ONLINE_IMG_PATH(CHARACTER_ONLINE_STATUS9,IVP_REQUEST_SEQ,DUMMY_TALK_FLAG,MONITOR_ENABLE_FLAG,TALK_LOCK_FLAG,TALK_LOCK_DATE) ELSE NULL END AS SYS_ONLINE_IMG_PATH9").AppendLine();
			sSql.Append("FROM (").AppendLine();
			sSql.Append("	SELECT	DISTINCT").AppendLine();
			sSql.Append("		LOGIN_ID				,").AppendLine();
			sSql.Append("		USER_SEQ				,").AppendLine();
			sSql.Append(string.Format("SUBSTRB({0},1,{1}) TERM_ID,",sType,iLen)).AppendLine();
			sSql.Append("		PRODUCTION_NM			,").AppendLine();
			sSql.Append("		MANAGER_NM				,").AppendLine();
			sSql.Append("		CAST_NM					,").AppendLine();
			sSql.Append("		SUBSTRB(EMAIL_ADDR,1,30) EMAIL_ADDR,").AppendLine();
			sSql.Append("		USER_STATUS				,").AppendLine();
			sSql.Append("		USER_STATUS_NM			,").AppendLine();
			sSql.Append("		IVP_REQUEST_SEQ			,").AppendLine();
			sSql.Append("		DUMMY_TALK_FLAG			,").AppendLine();
			sSql.Append("		MONITOR_ENABLE_FLAG		,").AppendLine();
			sSql.Append("		TALK_LOCK_FLAG			,").AppendLine();
			sSql.Append("		TALK_LOCK_DATE			,").AppendLine();
			sSql.Append("		CHARACTER_ONLINE_STATUS1,").AppendLine();
			sSql.Append("		CHARACTER_ONLINE_STATUS2,").AppendLine();
			sSql.Append("		CHARACTER_ONLINE_STATUS3,").AppendLine();
			sSql.Append("		CHARACTER_ONLINE_STATUS4,").AppendLine();
			sSql.Append("		CHARACTER_ONLINE_STATUS5,").AppendLine();
			sSql.Append("		CHARACTER_ONLINE_STATUS6,").AppendLine();
			sSql.Append("		CHARACTER_ONLINE_STATUS7,").AppendLine();
			sSql.Append("		CHARACTER_ONLINE_STATUS8,").AppendLine();
			sSql.Append("		CHARACTER_ONLINE_STATUS9").AppendLine();
			sSql.Append("FROM VW_CAST07 ").AppendLine();

			sWhere = string.Format(" WHERE ({0} IN (SELECT {0} FROM T_USER WHERE SEX_CD = '3' AND USER_STATUS = :USER_STATUS GROUP BY {0} HAVING COUNT(*) > 1 )) ",sType);

			if (sOnline.Equals("") == false) {
				SysPrograms.SqlAppendWhere(
				   "((CHARACTER_ONLINE_STATUS1 = :CHARACTER_ONLINE_STATUS1) OR " +
					"(CHARACTER_ONLINE_STATUS2 = :CHARACTER_ONLINE_STATUS2) OR " +
					"(CHARACTER_ONLINE_STATUS3 = :CHARACTER_ONLINE_STATUS3) OR " +
					"(CHARACTER_ONLINE_STATUS4 = :CHARACTER_ONLINE_STATUS4) OR " +
					"(CHARACTER_ONLINE_STATUS5 = :CHARACTER_ONLINE_STATUS5) OR " +
					"(CHARACTER_ONLINE_STATUS6 = :CHARACTER_ONLINE_STATUS6) OR " +
					"(CHARACTER_ONLINE_STATUS7 = :CHARACTER_ONLINE_STATUS7) OR " +
					"(CHARACTER_ONLINE_STATUS8 = :CHARACTER_ONLINE_STATUS8) OR " +
					"(CHARACTER_ONLINE_STATUS9 = :CHARACTER_ONLINE_STATUS9))",ref sWhere);
			}

			if (sTerminalId.Equals("") == false) {
				SysPrograms.SqlAppendWhere(string.Format("({0} LIKE :TERMINAL_ID) ",sType),ref sWhere);
			}

			if (sSiteCd.Equals("") == false) {
				SysPrograms.SqlAppendWhere(
				   "((CHARACTER_SITE_CD1 = :CHARACTER_SITE_CD1) OR " +
					"(CHARACTER_SITE_CD2 = :CHARACTER_SITE_CD2) OR " +
					"(CHARACTER_SITE_CD3 = :CHARACTER_SITE_CD3) OR " +
					"(CHARACTER_SITE_CD4 = :CHARACTER_SITE_CD4) OR " +
					"(CHARACTER_SITE_CD5 = :CHARACTER_SITE_CD5) OR " +
					"(CHARACTER_SITE_CD6 = :CHARACTER_SITE_CD6) OR " +
					"(CHARACTER_SITE_CD7 = :CHARACTER_SITE_CD7) OR " +
					"(CHARACTER_SITE_CD8 = :CHARACTER_SITE_CD8) OR " +
					"(CHARACTER_SITE_CD9 = :CHARACTER_SITE_CD9))",ref sWhere);
			}

			sSql.Append(sWhere).AppendLine();
			sSql.Append(" ) ORDER BY TERM_ID,USER_SEQ").AppendLine();

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				cmd.Parameters.Add("USER_STATUS",ViCommConst.USER_WOMAN_NORMAL);

				if (sOnline.Equals("") == false) {
					cmd.Parameters.Add("CHARACTER_ONLINE_STATUS1",sOnline);
					cmd.Parameters.Add("CHARACTER_ONLINE_STATUS2",sOnline);
					cmd.Parameters.Add("CHARACTER_ONLINE_STATUS3",sOnline);
					cmd.Parameters.Add("CHARACTER_ONLINE_STATUS4",sOnline);
					cmd.Parameters.Add("CHARACTER_ONLINE_STATUS5",sOnline);
					cmd.Parameters.Add("CHARACTER_ONLINE_STATUS6",sOnline);
					cmd.Parameters.Add("CHARACTER_ONLINE_STATUS7",sOnline);
					cmd.Parameters.Add("CHARACTER_ONLINE_STATUS8",sOnline);
					cmd.Parameters.Add("CHARACTER_ONLINE_STATUS9",sOnline);
				}

				if (sTerminalId.Equals("") == false) {
					cmd.Parameters.Add("TERMINAL_ID",sTerminalId + "%");
				}
				if (sSiteCd.Equals("") == false) {
					cmd.Parameters.Add("CHARACTER_SITE_CD1",sSiteCd);
					cmd.Parameters.Add("CHARACTER_SITE_CD2",sSiteCd);
					cmd.Parameters.Add("CHARACTER_SITE_CD3",sSiteCd);
					cmd.Parameters.Add("CHARACTER_SITE_CD4",sSiteCd);
					cmd.Parameters.Add("CHARACTER_SITE_CD5",sSiteCd);
					cmd.Parameters.Add("CHARACTER_SITE_CD6",sSiteCd);
					cmd.Parameters.Add("CHARACTER_SITE_CD7",sSiteCd);
					cmd.Parameters.Add("CHARACTER_SITE_CD8",sSiteCd);
					cmd.Parameters.Add("CHARACTER_SITE_CD9",sSiteCd);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	public bool IsUniqueID(string pLoginID,string pUserSeq) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "";
			if (pUserSeq.Equals("")) {
				sSql = "SELECT COUNT(*) CNT FROM VW_CAST00 WHERE (LOGIN_ID= :LOGIN_ID) OR (PREVIOUS_LOGIN_ID = :PREVIOUS_LOGIN_ID) ";
			} else {
				sSql = "SELECT COUNT(*) CNT FROM VW_CAST00 WHERE (LOGIN_ID= :LOGIN_ID OR PREVIOUS_LOGIN_ID = :PREVIOUS_LOGIN_ID) AND USER_SEQ != :USER_SEQ";
			}

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("LOGIN_ID",pLoginID);
				cmd.Parameters.Add("PREVIOUS_LOGIN_ID",pLoginID);
				if (!pUserSeq.Equals("")) {
					cmd.Parameters.Add("USER_SEQ",pUserSeq);
				}

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST00");

					if (ds.Tables["VW_CAST00"].Rows.Count != 0) {
						dr = ds.Tables["VW_CAST00"].Rows[0];
						int iCnt = int.Parse(dr["CNT"].ToString());
						if (iCnt > 0) {
							bExist = true;
						}
					}
				}
			}
		} finally {
			conn.Close();
		}
		return !bExist;
	}

	public bool GetValue(string pUserSeq,string pItem,ref string pValue) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect();
			string sSql = "SELECT " +
								"CAST_NM	," +
								"LOGIN_ID	" +
							"FROM " +
								"VW_CAST00 " +
							"WHERE " +
								"USER_SEQ = :USER_SEQ ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("VW_CAST00",pUserSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST00");
					if (ds.Tables["VW_CAST00"].Rows.Count != 0) {
						dr = ds.Tables["VW_CAST00"].Rows[0];
						pValue = dr[pItem].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}


	public bool IsExistLoginId(string pLoginID) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT USER_SEQ,CAST_NM FROM VW_CAST00 WHERE LOGIN_ID= :LOGIN_ID ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("LOGIN_ID",pLoginID);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST00");

					if (ds.Tables["VW_CAST00"].Rows.Count != 0) {
						dr = ds.Tables["VW_CAST00"].Rows[0];
						userSeq = dr["USER_SEQ"].ToString();
						castNm = dr["CAST_NM"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public DataSet GetCSVList(
		string pOnline,
		string pLoginId,
		string pProductionSeq,
		string pCastNm,
		string pSiteCd,
		string pUserStatus,
		string pEmailAddr,
		string pRegistDayFrom,
		string pRegistDayTo,
		string pLastLoginDayFrom,
		string pLastLoginDayTo,
		string pTotalPaymentAmtFrom,
		string pTotalPaymentAmtTo,
		bool pBankAccountInvalidFlag,
		string pNonExistMailAddrFlag,
		bool pUseNotPaymentAmt
	) {
		DataSet oDataSet = new DataSet();

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		if (pUseNotPaymentAmt) {
			oSqlBuilder.AppendLine("	GET_NOT_PAYMENT_AMT(USER_SEQ)	AS NOT_PAYMENT_AMT	,");
		}
		oSqlBuilder.AppendLine("	LOGIN_ID		,");
		oSqlBuilder.AppendLine("	LOGIN_PASSWORD	,");
		oSqlBuilder.AppendLine("	EMAIL_ADDR");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	VW_CAST07");

		string sWhere = "";
		OracleParameter[] objParms = CreateWhere(
											pOnline,
											pLoginId,
											pProductionSeq,
											pCastNm,
											pSiteCd,
											pUserStatus,
											pEmailAddr,
											pRegistDayFrom,
											pRegistDayTo,
											pLastLoginDayFrom,
											pLastLoginDayTo,
											pTotalPaymentAmtFrom,
											pTotalPaymentAmtTo,
											pBankAccountInvalidFlag,
											pNonExistMailAddrFlag,
											ref sWhere);

		oSqlBuilder.AppendLine(sWhere);
		oSqlBuilder.AppendLine("ORDER BY LOGIN_ID");

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.AddRange(objParms);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet,"VW_CAST07");
				}
			}
		} finally {
			conn.Close();
		}
		return oDataSet;
	}


	public DataSet GetOneIdPicSeq(string pUserSeq) {
		StringBuilder oSql = new StringBuilder();
		oSql.AppendLine("SELECT	");
		oSql.AppendLine("	ID_PIC_SEQ1		,");
		oSql.AppendLine("	ID_PIC_SEQ2		,");
		oSql.AppendLine("	ID_PIC_SEQ3		,");
		oSql.AppendLine("	ID_PIC_SEQ4		,");
		oSql.AppendLine("	ID_PIC_SEQ5		,");
		oSql.AppendLine("	ID_PIC_SEQ6		,");
		oSql.AppendLine("	ID_PIC_SEQ7		,");
		oSql.AppendLine("	ID_PIC_SEQ8		,");
		oSql.AppendLine("	ID_PIC_SEQ9		,");
		oSql.AppendLine("	ID_PIC_SEQ10	");
		oSql.AppendLine("FROM	");
		oSql.AppendLine("	T_CAST	");
		oSql.AppendLine("WHERE ");
		oSql.AppendLine("	USER_SEQ = :USER_SEQ ");

		DataSet oDataSet = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSql.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add("USER_SEQ",pUserSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
				}
				return oDataSet;
			}
		} finally {
			conn.Close();
		}
	}

	/// <summary>
	/// 生年月日が重複する出演者の数を取得する
	/// </summary>
	/// <param name="pUserSeq"></param>
	/// <param name="pBirthday"></param>
	/// <returns></returns>
	public int GetDupCastPageCount(
		string pUserSeq,
		string pBirthday
	) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(T_CAST.USER_SEQ) AS ROW_COUNT FROM T_CAST, T_CAST_CHARACTER, T_USER  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateDupCastWhere(
												pUserSeq,
												pBirthday,
												ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				this.Fill(da,ds,"T_CAST");
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	/// <summary>
	/// 生年月日が重複する出演者の一覧を取得する
	/// </summary>
	/// <param name="pUserSeq"></param>
	/// <param name="pBirthday"></param>
	/// <param name="startRowIndex"></param>
	/// <param name="maximumRows"></param>
	/// <returns></returns>
	public DataSet GetDupCastPageCollection(
		string pUserSeq,
		string pBirthday,
		int startRowIndex,
		int maximumRows
	) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY LOGIN_ID ";
			StringBuilder sSql = new StringBuilder();

			sSql.Append("SELECT ").AppendLine();
			sSql.Append("USER_SEQ,").AppendLine();
			sSql.Append("LOGIN_ID,").AppendLine();
			sSql.Append("CAST_NM,").AppendLine();
			sSql.Append("CAST_KANA_NM,").AppendLine();
			sSql.Append("ID_PIC_SEQ1,").AppendLine();
			sSql.Append("ID_PIC_SEQ2,").AppendLine();
			sSql.Append("ID_PIC_SEQ3,").AppendLine();
			sSql.Append("ID_PIC_SEQ4,").AppendLine();
			sSql.Append("ID_PIC_SEQ5,").AppendLine();
			sSql.Append("ID_PIC_SEQ6,").AppendLine();
			sSql.Append("ID_PIC_SEQ7,").AppendLine();
			sSql.Append("ID_PIC_SEQ8,").AppendLine();
			sSql.Append("ID_PIC_SEQ9,").AppendLine();
			sSql.Append("ID_PIC_SEQ10").AppendLine();
			sSql.Append("FROM(").AppendLine();
			sSql.Append(" SELECT T_CAST.*, T_USER.LOGIN_ID, ROW_NUMBER() OVER (");
			sSql.Append(sOrder);
			sSql.Append(") AS RNUM FROM T_CAST, T_CAST_CHARACTER, T_USER  ").AppendLine();
			string sWhere = "";
			OracleParameter[] objParms = CreateDupCastWhere(
												pUserSeq,
												pBirthday,
												ref sWhere);
			sSql.Append(sWhere).AppendLine();
			sSql.Append(")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ").AppendLine();
			sSql.Append(sOrder);

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					this.Fill(da,ds,"T_CAST");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	/// <summary>
	/// 生年月日が重複する出演者の検索用の条件を生成する
	/// </summary>
	/// <param name="pUserSeq"></param>
	/// <param name="pBirthday"></param>
	/// <param name="pWhere"></param>
	/// <returns></returns>
	private OracleParameter[] CreateDupCastWhere(
		string pUserSeq,
		string pBirthday,
		ref string pWhere
	) {
		pWhere = "";

		ArrayList list = new ArrayList();

		SysPrograms.SqlAppendWhere("T_USER.USER_SEQ = T_CAST_CHARACTER.USER_SEQ",ref pWhere);
		SysPrograms.SqlAppendWhere("T_USER.USER_SEQ = T_CAST.USER_SEQ",ref pWhere);

		if (pUserSeq.Equals("") == false) {
			SysPrograms.SqlAppendWhere("T_CAST.USER_SEQ <> :USER_SEQ",ref pWhere);
			list.Add(new OracleParameter("USER_SEQ",pUserSeq));
		}

		if (pBirthday.Equals("") == false) {
			SysPrograms.SqlAppendWhere("T_CAST.BIRTHDAY = :BIRTHDAY",ref pWhere);
			list.Add(new OracleParameter("BIRTHDAY",pBirthday));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

}
