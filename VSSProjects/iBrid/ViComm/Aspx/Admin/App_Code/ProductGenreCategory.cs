﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 商品ジャンルカテゴリ

--	Progaram ID		: ProductGenreCategory
--
--  Creation Date	: 2010.12.08
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class ProductGenreCategory:DbSession {
	public ProductGenreCategory() {
	}

	public DataSet GetList(string pSiteCd,string pProductType) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine(" SELECT						");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY.PRODUCT_GENRE_CATEGORY_CD    ,	");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY.PRODUCT_GENRE_CATEGORY_NM    	");
		oSqlBuilder.AppendLine(" FROM				");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY ");
		oSqlBuilder.AppendLine(" WHERE				");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY.SITE_CD		= :SITE_CD		AND	");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY.PRODUCT_TYPE	= :PRODUCT_TYPE		");
		oSqlBuilder.AppendLine(" ORDER BY						");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY.PRODUCT_GENRE_CATEGORY_CD	");

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":PRODUCT_TYPE",pProductType);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}

	public DataSet GetList2(string pSiteCd,string pProductType) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine(" SELECT ");
		oSqlBuilder.AppendLine("    GC.PRODUCT_GENRE_CATEGORY_NM || ' - ' || G.PRODUCT_GENRE_NM AS GENRE_NM,        ");
		oSqlBuilder.AppendLine("    GC.PRODUCT_GENRE_CATEGORY_CD || ';'   || G.PRODUCT_GENRE_CD AS GENRE_CATEGORY_CD ");
		oSqlBuilder.AppendLine(" FROM ");
		oSqlBuilder.AppendLine("    T_PRODUCT_GENRE G INNER JOIN T_PRODUCT_GENRE_CATEGORY GC       ");
		oSqlBuilder.AppendLine("    ON  G.PRODUCT_GENRE_CATEGORY_CD = GC.PRODUCT_GENRE_CATEGORY_CD ");
		oSqlBuilder.AppendLine("    AND G.SITE_CD = GC.SITE_CD                                     ");
		oSqlBuilder.AppendLine(" WHERE ");
		oSqlBuilder.AppendLine("	GC.SITE_CD		= :SITE_CD		AND	");
		oSqlBuilder.AppendLine("	GC.PRODUCT_TYPE	= :PRODUCT_TYPE		");
		oSqlBuilder.AppendLine(" ORDER BY ");
		oSqlBuilder.AppendLine("	G.PRODUCT_GENRE_CATEGORY_CD, G.PRODUCT_GENRE_CD");

		DataSet ds = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":PRODUCT_TYPE",pProductType);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public int GetPageCount(string pSiteCd,string pProductType) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT						");
		oSqlBuilder.AppendLine("	COUNT(*)		").AppendLine();
		oSqlBuilder.AppendLine(" FROM				");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY ");
		oSqlBuilder.AppendLine(" WHERE				");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY.SITE_CD      = :SITE_CD      AND ");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY.PRODUCT_TYPE = :PRODUCT_TYPE ");

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":PRODUCT_TYPE",pProductType);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}
	public DataSet GetPageCollection(string pSiteCd,string pProductType, int startRowIndex, int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine(" SELECT						        ");
		oSqlBuilder.AppendLine("	SITE_CD				        ,	");
		oSqlBuilder.AppendLine("	PRODUCT_GENRE_CATEGORY_CD   ,	");
		oSqlBuilder.AppendLine("	PRODUCT_GENRE_CATEGORY_NM   ,	");
		oSqlBuilder.AppendLine("	PRODUCT_TYPE                 	");
		oSqlBuilder.AppendLine(" FROM				");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY ");
		oSqlBuilder.AppendLine(" WHERE				");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY.SITE_CD      = :SITE_CD      AND ");
		oSqlBuilder.AppendLine("	T_PRODUCT_GENRE_CATEGORY.PRODUCT_TYPE = :PRODUCT_TYPE ");

		string sPagingSql = string.Empty;
		string sSortExpression = "ORDER BY T_PRODUCT_GENRE_CATEGORY.PRODUCT_GENRE_CATEGORY_CD, T_PRODUCT_GENRE_CATEGORY.PRODUCT_TYPE";
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":PRODUCT_TYPE",pProductType);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}

}
