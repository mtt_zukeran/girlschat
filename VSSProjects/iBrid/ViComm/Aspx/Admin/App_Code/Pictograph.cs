﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using MobileLib.Pictograph;
using iBridCommLib;
using ViComm;

public class Pictograph {
	private static string sFilterExpression;
	private static DataTable oDataSource;

	public Pictograph() {
	}

	public int GetPageCount(int pColumnCount, bool pDocomoFlag, bool pKddiFlag, bool pSoftbankFlag) {
		StringBuilder oFilterBuilder = new StringBuilder();
		if (pDocomoFlag) {
			oFilterBuilder.Append("DocomoShiftJIS <> '' ");
		}

		if (pKddiFlag) {
			oFilterBuilder.AppendFormat("{0}KddiShiftJIS <> '' ", oFilterBuilder.Length > 0 ? "AND " : string.Empty);
		}

		if (pSoftbankFlag) {
			oFilterBuilder.AppendFormat("{0}SoftbankShiftJIS <> '' ", oFilterBuilder.Length > 0 ? "AND " : string.Empty);
		}

		return PictographDefHolder.PictographDef.Pictograph.Select(Pictograph.sFilterExpression).Length / pColumnCount;
	}

	public DataTable GetPageCollection(int pColumnCount, bool pDocomoFlag, bool pKddiFlag, bool pSoftbankFlag, int startRowIndex, int maximumRows) {
		StringBuilder oFilterBuilder = new StringBuilder();
		if (pDocomoFlag) {
			oFilterBuilder.Append("DocomoShiftJIS <> '' ");
		}

		if (pKddiFlag) {
			oFilterBuilder.AppendFormat("{0}KddiShiftJIS <> '' ", oFilterBuilder.Length > 0 ? "AND " : string.Empty);
		}

		if (pSoftbankFlag) {
			oFilterBuilder.AppendFormat("{0}SoftbankShiftJIS <> '' ", oFilterBuilder.Length > 0 ? "AND " : string.Empty);
		}

		if (Pictograph.oDataSource == null || !sFilterExpression.Equals(oFilterBuilder.ToString())) {
			Pictograph.sFilterExpression = oFilterBuilder.ToString();
			this.CreateDataSource(pColumnCount, maximumRows);
		}

		DataTable oTable = Pictograph.oDataSource.Clone();

		int iMaximumRowIndex = startRowIndex + maximumRows < Pictograph.oDataSource.Rows.Count ? startRowIndex + maximumRows : Pictograph.oDataSource.Rows.Count;
		for (int iRowIndex = startRowIndex; iRowIndex < iMaximumRowIndex; iRowIndex++) {
			oTable.Rows.Add(Pictograph.oDataSource.Rows[iRowIndex].ItemArray);
		}

		return oTable;
	}

	private void CreateDataSource(int pColumnCount, int pRowsPerPage) {
		Pictograph.oDataSource = new DataTable();
		for (int iColumnIndex = 0; iColumnIndex < pColumnCount; iColumnIndex++) {
			foreach (DataColumn oColumn in new DataColumn[] {	PictographDefHolder.PictographDef.Pictograph.CommonCodeColumn,
																PictographDefHolder.PictographDef.Pictograph.TextColumn,
																PictographDefHolder.PictographDef.Pictograph.DocomoShiftJISColumn,
																PictographDefHolder.PictographDef.Pictograph.KddiShiftJISColumn,
																PictographDefHolder.PictographDef.Pictograph.SoftbankShiftJISColumn}) {
				Pictograph.oDataSource.Columns.Add(string.Concat(oColumn.ColumnName, iColumnIndex), oColumn.DataType);
			}
		}

		DataRow[] oPictgraphRowArray = PictographDefHolder.PictographDef.Pictograph.Select(Pictograph.sFilterExpression, "CommonCode ASC");
		int iPageIndex = 1;

		for (int iFirstColumnMaximumRows = iPageIndex * pRowsPerPage; (iPageIndex - 1) * pRowsPerPage * pColumnCount <= oPictgraphRowArray.Length; iFirstColumnMaximumRows += pRowsPerPage * pColumnCount) {
			for (int iDataSourceRowIndex = (iPageIndex - 1) * pRowsPerPage * pColumnCount; iDataSourceRowIndex < iFirstColumnMaximumRows; iDataSourceRowIndex++) {
				DataRow oRow = Pictograph.oDataSource.NewRow();
				for (int iColumnIndex = 0; iColumnIndex < pColumnCount; iColumnIndex++) {
					int iRowIndex = iDataSourceRowIndex +  pRowsPerPage * iColumnIndex;
					if (iRowIndex < oPictgraphRowArray.Length) {
						oRow[string.Concat("CommonCode", iColumnIndex)] = string.Concat("$x", oPictgraphRowArray[iRowIndex]["CommonCode"], ";");
						oRow[string.Concat("Text", iColumnIndex)] = oPictgraphRowArray[iRowIndex]["Text"];
						oRow[string.Concat("DocomoShiftJIS", iColumnIndex)] = oPictgraphRowArray[iRowIndex]["DocomoShiftJIS"];
						oRow[string.Concat("KddiShiftJIS", iColumnIndex)] = oPictgraphRowArray[iRowIndex]["KddiShiftJIS"];
						oRow[string.Concat("SoftbankShiftJIS", iColumnIndex)] = oPictgraphRowArray[iRowIndex]["SoftbankShiftJIS"];
					}
				}

				oDataSource.Rows.Add(oRow);
			}
			iPageIndex++;
		}
	}
}
