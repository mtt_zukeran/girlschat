﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 抽選獲得アイテム
--	Progaram ID		: GameItemSale
--
--  Creation Date	: 2011.07.25
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;


public class GameItemSale : DbSession {

	public GameItemSale() {
	}

	public int GetPageCount(string pSiteCd,string pSaleScheduleSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_GAME_ITEM_SALE	GIT	,");
		oSqlBuilder.AppendLine("	T_GAME_ITEM			GI	");

		string sWhereClause;
		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pSaleScheduleSeq,out sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		}
		finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pSaleScheduleSeq,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	GIT.SITE_CD					,");
		oSqlBuilder.AppendLine("	GIT.SALE_SCHEDULE_SEQ		,");
		oSqlBuilder.AppendLine("	GIT.GAME_ITEM_SEQ			,");
		oSqlBuilder.AppendLine("	GI.GAME_ITEM_NM				,");
		oSqlBuilder.AppendLine("	GIT.DISCOUNT_RATE			,");
		oSqlBuilder.AppendLine("	GIT.NOTICE_DISP_FLAG		");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_GAME_ITEM_SALE	GIT	,");
		oSqlBuilder.AppendLine("	T_GAME_ITEM			GI	");

		string sWhereClause;
		string sPagingSql;
		string sSortExpression = "ORDER BY GIT.SITE_CD, GIT.SALE_SCHEDULE_SEQ, GIT.GAME_ITEM_SEQ";

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pSaleScheduleSeq,out sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		}
		finally {
			conn.Close();
		}
		return oDs;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pSaleScheduleSeq,out string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();

		SysPrograms.SqlAppendWhere("GIT.SITE_CD = GI.SITE_CD AND GIT.GAME_ITEM_SEQ = GI.GAME_ITEM_SEQ",ref pWhere);

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("GIT.SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}
		if (!string.IsNullOrEmpty(pSaleScheduleSeq)) {
			SysPrograms.SqlAppendWhere("GIT.SALE_SCHEDULE_SEQ = :SALE_SCHEDULE_SEQ",ref pWhere);
			list.Add(new OracleParameter("SALE_SCHEDULE_SEQ",pSaleScheduleSeq));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public bool IsDupulicateItem(string pSiteCd,string pSaleScheduleSeq,string pGameItemSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_GAME_ITEM_SALE	");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	SITE_CD				= :pSITE_CD				AND");
		oSqlBuilder.AppendLine("	SALE_SCHEDULE_SEQ	= :pSALE_SCHEDULE_SEQ	AND");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ		= :pGAME_ITEM_SEQ	");

		try {
			int iCount = 0;
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(new OracleParameter("SITE_CD",pSiteCd));
				cmd.Parameters.Add(new OracleParameter("SALE_SCHEDULE_SEQ",pSaleScheduleSeq));
				cmd.Parameters.Add(new OracleParameter("GAME_ITEM_SEQ",pGameItemSeq));
				iCount = Convert.ToInt32(cmd.ExecuteScalar());
				return iCount >= 1;
			}
		}
		finally {
			conn.Close();
		}
	}
}
