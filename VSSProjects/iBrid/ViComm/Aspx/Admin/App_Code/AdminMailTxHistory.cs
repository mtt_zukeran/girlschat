﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 管理者ﾒｰﾙ送信履歴

--	Progaram ID		: AdminMailTxHistory
--
--  Creation Date	: 2011.02.14
--  Creater			: K.Itoh
--
**************************************************************************/

using System.Web;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using ViComm;
using iBridCommLib;

public class AdminMailTxHistory : DbSession {
	public AdminMailTxHistory() { }
	
	/// <summary>
	/// ｽﾃｰﾀｽが処理中の履歴を取得検索します。
	/// </summary>
	/// <param name="pSiteCd">サイトコード</param>
	/// <param name="pAvailablePeriod">省略可:有効期間(分数)</param>
	/// <param name="pCount">省略可:有効件数</param>
	/// <returns>ｽﾃｰﾀｽが処理中の履歴</returns>
	public int GetProcessingCount(string pSiteCd, int? pAvailablePeriod,int? pCount) {

		// =======================
		//  Build Sql
		// =======================        						  		
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT							");
		oSqlBuilder.AppendLine(" 	COUNT(*)					");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine(" 	T_ADMIN_MAIL_TX_HISTORY	P	");
		
		string sWhere = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();
				
		SysPrograms.SqlAppendWhere(" P.SITE_CD = :SITE_CD ", ref sWhere);
		SysPrograms.SqlAppendWhere(" P.BULK_PROC_STATUS = :BULK_PROC_STATUS ", ref sWhere);
		oParamList.Add(new OracleParameter(":SITE_CD",pSiteCd));
		oParamList.Add(new OracleParameter(":BULK_PROC_STATUS",ViCommConst.BulkProcStatus.PROCESSING));
		if(pAvailablePeriod != null){
			SysPrograms.SqlAppendWhere(" P.START_DATE >= (SYSDATE - :AVAILABLE_PERIOD/1440) ", ref sWhere);
			oParamList.Add(new OracleParameter(":AVAILABLE_PERIOD", pAvailablePeriod));
		}
		if(pCount != null){
			SysPrograms.SqlAppendWhere(" ROWNUM <= :COUNT ", ref sWhere);
			oParamList.Add(new OracleParameter(":COUNT", pCount));
		}
		
		oSqlBuilder.AppendLine(sWhere);

		// =======================
		//  Execute
		// =======================        						  		
		
		return ExecuteSelectCountQuery(oSqlBuilder.ToString(),oParamList.ToArray());
	}


	public int GetPageCount(string pSiteCd, string pProcStatus) {
		StringBuilder oSqlBuilder = new StringBuilder();
	
		oSqlBuilder.AppendLine(" SELECT								");
		oSqlBuilder.AppendLine(" 	COUNT(*)                        ");
		oSqlBuilder.AppendLine(" FROM                               ");
		oSqlBuilder.AppendLine(" 	VW_ADMIN_MAIL_TX_HISTORY01 P    ");

		// where
		string sWhere = string.Empty;
		OracleParameter[] oParamList = this.CreateWhere(pSiteCd, pProcStatus, out sWhere);
		oSqlBuilder.AppendLine(sWhere);
				
		return ExecuteSelectCountQuery(oSqlBuilder,oParamList);
	}
	


	public DataSet GetPageCollection(string pSiteCd, string pProcStatus, int startRowIndex, int maximumRows) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine(" SELECT									  ");
		oSqlBuilder.AppendLine(" 	P.SITE_CD							, ");
		oSqlBuilder.AppendLine(" 	P.ADMIN_MAIL_TX_HISTORY_SEQ        	, ");
		oSqlBuilder.AppendLine(" 	P.RX_SEX_CD                        	, ");
		oSqlBuilder.AppendLine(" 	P.MAIL_TEMPLATE_NO                 	, ");
		oSqlBuilder.AppendLine(" 	P.MAIL_SEND_TYPE                   	, ");
		oSqlBuilder.AppendLine(" 	P.TX_LOGIN_ID                      	, ");
		oSqlBuilder.AppendLine(" 	P.TX_USER_CHAR_NO                  	, ");
		oSqlBuilder.AppendLine(" 	P.START_DATE                       	, ");
		oSqlBuilder.AppendLine(" 	P.END_DATE                         	, ");
		oSqlBuilder.AppendLine(" 	P.BULK_PROC_STATUS                 	, ");
		oSqlBuilder.AppendLine(" 	P.REGIST_ADMIN_ID                  	, ");
		oSqlBuilder.AppendLine(" 	P.UPDATE_DATE                      	, ");
		oSqlBuilder.AppendLine(" 	P.TEMPLATE_NM                      	, ");
		oSqlBuilder.AppendLine(" 	P.MAIL_SEND_TYPE_NM                	, ");
		oSqlBuilder.AppendLine(" 	P.BULK_PROC_STATUS_NM              	  "); 
		oSqlBuilder.AppendLine(" FROM									  ");
		oSqlBuilder.AppendLine(" 	VW_ADMIN_MAIL_TX_HISTORY01 P		  ");
		// where
		string sWhere = string.Empty;
		oParamList.AddRange(this.CreateWhere(pSiteCd, pProcStatus, out sWhere));
		oSqlBuilder.AppendLine(sWhere);

		// order by
		string sSortExpression = "ORDER BY P.START_DATE DESC, P.ADMIN_MAIL_TX_HISTORY_SEQ DESC ";
		
		string sPagingSql = string.Empty;
		oParamList.AddRange(ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(), sSortExpression, startRowIndex, maximumRows, out sPagingSql));


		return ExecuteSelectQuery(sPagingSql, oParamList.ToArray());
	}
	
	private OracleParameter[] CreateWhere(string pSiteCd, string pProcStatus,out string pWhere){
		pWhere = string.Empty;
		List<OracleParameter> oParamList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere(" P.SITE_CD = :SITE_CD ", ref pWhere);
		oParamList.Add(new OracleParameter(":SITE_CD", pSiteCd));
		if(!string.IsNullOrEmpty(pProcStatus)){
			SysPrograms.SqlAppendWhere(" P.BULK_PROC_STATUS = :BULK_PROC_STATUS ", ref pWhere);
			oParamList.Add(new OracleParameter(":BULK_PROC_STATUS", pProcStatus));
		}			
				
		return oParamList.ToArray(); 
	}
	
	private DataSet ExecuteSelectQuery(StringBuilder pSqlBuilder, OracleParameter[] pParamList) {	
		return ExecuteSelectQuery(pSqlBuilder.ToString(),pParamList);
	}
	private DataSet ExecuteSelectQuery(string pSql, OracleParameter[] pParamList){
		DataSet oDataSet = new DataSet();
		try{
			this.conn = this.DbConnect();
			
			using (this.cmd = this.CreateSelectCommand(pSql, this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.AddRange(pParamList);

				using(da = new OracleDataAdapter(this.cmd)){
					da.Fill(oDataSet);
				}
			}

		}finally{
			this.conn.Close();
		}
		return oDataSet;
	}


	private int ExecuteSelectCountQuery(StringBuilder pSqlBuilder, OracleParameter[] pParamList) {
		return ExecuteSelectCountQuery(pSqlBuilder.ToString(), pParamList);
	}
	private int ExecuteSelectCountQuery(string pSql, OracleParameter[] pParamList) {
		int iCount = 0;

		try {
			this.conn = this.DbConnect();
			using (this.cmd = this.CreateSelectCommand(pSql, this.conn)) {
				this.cmd.BindByName = true;
				this.cmd.Parameters.AddRange(pParamList);

				iCount = int.Parse(iBridUtil.GetStringValue(this.cmd.ExecuteScalar()));
			}
		} finally {
			this.conn.Close();
		}

		return iCount;
	}
}
