/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 男性会員
--	Progaram ID		: UserMan
--
--  Creation Date	: 2009.12.22
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class UserMan:DbSession {

	public UserMan() {
	}

	public int GetPageCount(
		string pSiteCd,
		string pLoginID,
		string pReceiptLimitDayFrom,
		string pReceiptLimitDayTo,
		string pUrgeLevel,
		string pLastBlackDayFrom,
		string pLastBlackDayTo
	) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_USER_MAN03 ";

			string sWhere = "";
			OracleParameter[] oParms = CreateWhere(
											pSiteCd,
											pLoginID,
											pReceiptLimitDayFrom,
											pReceiptLimitDayTo,
											pUrgeLevel,
											pLastBlackDayFrom,
											pLastBlackDayTo,
											ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < oParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)oParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(
		string pSiteCd,
		string pLoginID,
		string pReceiptLimitDayFrom,
		string pReceiptLimitDayTo,
		string pUrgeLevel,
		string pLastBlackDayFrom,
		string pLastBlackDayTo,
		int startRowIndex,
		int maximumRows
	) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = " ORDER BY SITE_CD,RECEIPT_LIMIT_DAY,USER_SEQ ";
			string sSql = " SELECT " +
								" SITE_CD				, " +
								" USER_SEQ				, " +
								" RECEIPT_LIMIT_DAY		, " +
								" BILL_AMT				, " +
								" TOTAL_RECEIPT_AMT		, " +
								" LOGIN_ID				, " +
								" URGE_LEVEL			, " +
								" URGE_EXEC_LAST_DATE	, " +
								" URGE_EXEC_COUNT " +
							" FROM( " +
								" SELECT VW_USER_MAN03.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_USER_MAN03 ";
			string sWhere = "";
			OracleParameter[] oParms = CreateWhere(
											pSiteCd,
											pLoginID,
											pReceiptLimitDayFrom,
											pReceiptLimitDayTo,
											pUrgeLevel,
											pLastBlackDayFrom,
											pLastBlackDayTo,
											ref sWhere);

			sSql = sSql + sWhere;
			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < oParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)oParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USER_MAN03");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(
		string pSiteCd,
		string pLoginID,
		string pReceiptLimitDayFrom,
		string pReceiptLimitDayTo,
		string pUrgeLevel,
		string pLastBlackDayFrom,
		string pLastBlackDayTo,
		ref string pWhere
	) {
		ArrayList list = new ArrayList();

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
		list.Add(new OracleParameter("SITE_CD",pSiteCd));

		SysPrograms.SqlAppendWhere("(USER_STATUS IN (:USER_STATUS1,:USER_STATUS2,:USER_STATUS3,:USER_STATUS4,:USER_STATUS5,:USER_STATUS6))",ref pWhere);
		list.Add(new OracleParameter("USER_STATUS1",ViCommConst.USER_MAN_NORMAL));
		list.Add(new OracleParameter("USER_STATUS2",ViCommConst.USER_MAN_TEL_NO_AUTH));
		list.Add(new OracleParameter("USER_STATUS3",ViCommConst.USER_MAN_NEW));
		list.Add(new OracleParameter("USER_STATUS4",ViCommConst.USER_MAN_NO_RECEIPT));
		list.Add(new OracleParameter("USER_STATUS5",ViCommConst.USER_MAN_NO_USED));
		list.Add(new OracleParameter("USER_STATUS6",ViCommConst.USER_MAN_DELAY));

		if (!pLoginID.Equals("")) {
			SysPrograms.SqlAppendWhere("LOGIN_ID = :LOGIN_ID",ref pWhere);
			list.Add(new OracleParameter("LOGIN_ID",pLoginID));
		}

		if ((!pReceiptLimitDayFrom.Equals("")) && (!pReceiptLimitDayTo.Equals(""))) {
			SysPrograms.SqlAppendWhere("(RECEIPT_LIMIT_DAY >= :RECEIPT_LIMIT_DAY_FROM AND RECEIPT_LIMIT_DAY <= :RECEIPT_LIMIT_DAY_TO)",ref pWhere);
			list.Add(new OracleParameter("RECEIPT_LIMIT_DAY_FROM",pReceiptLimitDayFrom));
			list.Add(new OracleParameter("RECEIPT_LIMIT_DAY_TO",pReceiptLimitDayTo));
		}

		if (!pUrgeLevel.Equals("")) {
			SysPrograms.SqlAppendWhere("URGE_LEVEL = :URGE_LEVEL",ref pWhere);
			list.Add(new OracleParameter("URGE_LEVEL",pUrgeLevel));
		}

		if ((!pLastBlackDayFrom.Equals("")) && (!pLastBlackDayTo.Equals(""))) {
			SysPrograms.SqlAppendWhere("(LAST_BLACK_DAY >= :LAST_BLACK_DAY_FROM AND LAST_BLACK_DAY <= :LAST_BLACK_DAY_TO)",ref pWhere);
			list.Add(new OracleParameter("LAST_BLACK_DAY_FROM",pLastBlackDayFrom));
			list.Add(new OracleParameter("LAST_BLACK_DAY_TO",pLastBlackDayTo));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public void GetTotalPaymentAfter(
		string pSiteCd,
		string pLoginID,
		string pReceiptLimitDayFrom,
		string pReceiptLimitDayTo,
		string pUrgeLevel,
		string pLastBlackDayFrom,
		string pLastBlackDayTo,
		out int pCount,
		out int pAmount
	) {
		DataSet ds;
		DataRow dr;
		pCount = 0;
		pAmount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT " +
								"NVL(COUNT(*),0) AS CNT , " +
								"NVL(SUM(BILL_AMT),0) AS AMT " +
							"FROM " +
								"VW_USER_MAN03 ";
			string sWhere = "";

			OracleParameter[] oParms = CreateWhere(
											pSiteCd,
											pLoginID,
											pReceiptLimitDayFrom,
											pReceiptLimitDayTo,
											pUrgeLevel,
											pLastBlackDayFrom,
											pLastBlackDayTo,
											ref sWhere);

			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < oParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)oParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					pCount = int.Parse(dr["CNT"].ToString());
					pAmount = int.Parse(dr["AMT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
	}
}
