﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者動画属性種別
--	Progaram ID		: CastMovieAttrType
--
--  Creation Date	: 2010.05.17
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using ViComm;
using iBridCommLib;

public class CastMovieAttrType:DbSession {

	public CastMovieAttrType() {
	}

	public int GetPageCount(string pSiteCd) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_CAST_MOVIE_ATTR_TYPE ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();
			string sOrder = " ORDER BY SITE_CD,PRIORITY";

			string sSql = "SELECT " +
							"SITE_CD					," +
							"CAST_MOVIE_ATTR_TYPE_SEQ	," +
							"CAST_MOVIE_ATTR_TYPE_NM	," +
							"PRIORITY					," +
							"ITEM_NO					" +
							"FROM(" +
							" SELECT T_CAST_MOVIE_ATTR_TYPE.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM T_CAST_MOVIE_ATTR_TYPE ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql += sWhere;
			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql += sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_CAST_MOVIE_ATTR_TYPE");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		pWhere = pWhere + " WHERE  SITE_CD = :SITE_CD AND CAST_MOVIE_ATTR_TYPE_SEQ <> :CAST_MOVIE_ATTR_TYPE_SEQ ";
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		list.Add(new OracleParameter("CAST_MOVIE_ATTR_TYPE_SEQ",ViCommConst.DEFAULT_CAST_MOVIE_ATTR_TYPE_SEQ));
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public DataSet GetList(string pSiteCd) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();
			
			string sOrder = " ORDER BY SITE_CD,PRIORITY ";
			string sSql = "SELECT CAST_MOVIE_ATTR_TYPE_SEQ,CAST_MOVIE_ATTR_TYPE_NM FROM T_CAST_MOVIE_ATTR_TYPE ";
			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);

			sSql += sWhere;
			sSql += sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_CAST_MOVIE_ATTR_TYPE");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
	
	public DataSet GetListIncDefault(string pSiteCd) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT CAST_MOVIE_ATTR_TYPE_SEQ,CAST_MOVIE_ATTR_TYPE_NM FROM T_CAST_MOVIE_ATTR_TYPE WHERE SITE_CD = :SITE_CD ";
			sSql = sSql + " ORDER BY SITE_CD,PRIORITY";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetListByItemNo(string pSiteCd, string pItemNo) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT CAST_MOVIE_ATTR_TYPE_SEQ,CAST_MOVIE_ATTR_TYPE_NM FROM T_CAST_MOVIE_ATTR_TYPE WHERE SITE_CD = :SITE_CD ";
			if (string.IsNullOrEmpty(pItemNo)) {
				sSql = sSql + "AND PLANNING_TYPE = 0";
			} else {
				sSql = sSql + "AND PLANNING_TYPE <> 0";
			}
			sSql = sSql + " ORDER BY SITE_CD,PRIORITY";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetOneByItemNo(string pSiteCd,string pItemNo) {
		System.Text.StringBuilder oSqlBuilder = new System.Text.StringBuilder();
		oSqlBuilder.AppendLine(" SELECT							");
		oSqlBuilder.AppendLine("	CAST_MOVIE_ATTR_TYPE_SEQ	");
		oSqlBuilder.AppendLine(" FROM							");
		oSqlBuilder.AppendLine("	T_CAST_MOVIE_ATTR_TYPE		");
		oSqlBuilder.AppendLine(" WHERE ");
		oSqlBuilder.AppendLine("	SITE_CD		= :SITE_CD	AND ");
		oSqlBuilder.AppendLine("	ITEM_NO		= :ITEM_NO     ");
		
		DataSet ds = new DataSet();
		try{
			conn = DbConnect();
			using(cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)){
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);
				cmd.Parameters.Add(":ITEM_NO",pItemNo);
				
				using(da = new OracleDataAdapter(cmd)){
					da.Fill(ds);
				}
			}
		}finally{
			conn.Close();
		}
		return ds;
	}
}

