﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 10分以内に返事できる女の子
--	Progaram ID		: QuickResInoutHis
--  Creation Date	: 2011.06.03
--  Creater			: iBrid
--
**************************************************************************/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using iBridCommLib;
using ViComm;

public class QuickResInoutHis : DbSession {
	public QuickResInoutHis() {
	}

	public int GetPageCount(string pSiteCd, string pLoginId, string pExitStatus, string pSortExpression, string pSortDirection) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	COUNT(*)		");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_QUICK_RES_INOUT_HIS QRIH	,");
		oSqlBuilder.AppendLine("	T_USER U					,");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER CC			");

		StringBuilder oWhereBuilder = new StringBuilder();
		oWhereBuilder.AppendLine("WHERE	");
		oWhereBuilder.AppendLine("	QRIH.SITE_CD			= CC.SITE_CD			AND");
		oWhereBuilder.AppendLine("	QRIH.USER_SEQ			= CC.USER_SEQ			AND");
		oWhereBuilder.AppendLine("	QRIH.USER_CHAR_NO		= CC.USER_CHAR_NO		AND");
		oWhereBuilder.AppendLine("	QRIH.USER_SEQ			= U.USER_SEQ			AND");
		oWhereBuilder.AppendLine("	SYSDATE - 3/24 < QRIH.QUICK_RES_OUT_SCH_TIME	");

		string sWhereClause = oWhereBuilder.ToString();
		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd, pLoginId, pExitStatus, ref sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(), conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}


	public DataSet GetPageCollection(string pSiteCd, string pLoginId, string pExitStatus, string pSortExpression, string pSortDirection, int startRowIndex, int maximumRows) {
		string sSortExpression = string.Format("ORDER BY {0}",
			string.IsNullOrEmpty(pSortExpression) || string.IsNullOrEmpty(pSortDirection) ?
				"QRIH.SITE_CD, QRIH.QUICK_RES_IN_TIME DESC, QRIH.USER_SEQ" :
				string.Format("{0} {1}", pSortExpression, pSortDirection));

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	QRIH.QUICK_RES_SEQ				,");
		oSqlBuilder.AppendLine("	QRIH.SITE_CD					,");
		oSqlBuilder.AppendLine("	QRIH.USER_SEQ					,");
		oSqlBuilder.AppendLine("	QRIH.USER_CHAR_NO				,");
		oSqlBuilder.AppendLine("	QRIH.QUICK_RES_IN_TIME			,");
		oSqlBuilder.AppendLine("	QRIH.QUICK_RES_OUT_TIME			,");
		oSqlBuilder.AppendLine("	QRIH.QUICK_RES_OUT_SCH_TIME		,");
		oSqlBuilder.AppendLine("	QRIH.QUICK_RES_FLAG				,");
		oSqlBuilder.AppendLine("	QRIH.RX_MAIL_COUNT				,");
		oSqlBuilder.AppendLine("	CASE							");
		oSqlBuilder.AppendLine("		WHEN QRIH.QUICK_RES_OUT_TIME IS NULL AND QRIH.QUICK_RES_OUT_SCH_TIME < SYSDATE THEN '自動退室済'");
		oSqlBuilder.AppendLine("		WHEN QRIH.QUICK_RES_OUT_TIME IS NOT NULL THEN '退室済'");
		oSqlBuilder.AppendLine("		ELSE '強制退室'				");
		oSqlBuilder.AppendLine("	END EXIT_STATUS					,");
		oSqlBuilder.AppendLine("	U.LOGIN_ID						,");
		oSqlBuilder.AppendLine("	CC.HANDLE_NM					");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_QUICK_RES_INOUT_HIS QRIH	,");
		oSqlBuilder.AppendLine("	T_USER U					,");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER CC			");

		StringBuilder oWhereBuilder = new StringBuilder();
		oWhereBuilder.AppendLine("WHERE	");
		oWhereBuilder.AppendLine("	QRIH.SITE_CD			= CC.SITE_CD			AND");
		oWhereBuilder.AppendLine("	QRIH.USER_SEQ			= CC.USER_SEQ			AND");
		oWhereBuilder.AppendLine("	QRIH.USER_CHAR_NO		= CC.USER_CHAR_NO		AND");
		oWhereBuilder.AppendLine("	QRIH.USER_SEQ			= U.USER_SEQ			AND");
		oWhereBuilder.AppendLine("	SYSDATE - 3/24 < QRIH.QUICK_RES_OUT_SCH_TIME	");

		string sWhereClause = oWhereBuilder.ToString();
		string sPagingSql;

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd, pLoginId, pExitStatus, ref sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(), sSortExpression, startRowIndex, maximumRows, out sPagingSql);

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql, conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}

	private OracleParameter[] CreateWhere(string pSiteCd, string pLoginId, string pExitStatus, ref string pWhere) {
		List<OracleParameter> oOracleParameterList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("QRIH.SITE_CD = :SITE_CD", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("SITE_CD", pSiteCd));
		}

		if (!string.IsNullOrEmpty(pLoginId)) {
			SysPrograms.SqlAppendWhere("U.LOGIN_ID = :LOGIN_ID", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("LOGIN_ID", pLoginId));
		}

		switch (pExitStatus) {
			case ViCommConst.QuickResponse.IN:
				SysPrograms.SqlAppendWhere("QRIH.QUICK_RES_OUT_TIME IS NULL AND SYSDATE < QRIH.QUICK_RES_OUT_SCH_TIME", ref pWhere);
				break;
			case ViCommConst.QuickResponse.OUT:
				SysPrograms.SqlAppendWhere("(QRIH.QUICK_RES_OUT_TIME IS NOT NULL OR (QRIH.QUICK_RES_OUT_TIME IS NULL AND QRIH.QUICK_RES_OUT_SCH_TIME < SYSDATE))", ref pWhere);
				break;
			default:
				break;
		}
		return oOracleParameterList.ToArray();
	}
}