﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: リッチーノランク
--	Progaram ID		: RichinoRank
--
--  Creation Date	: 2011.04.18
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class RichinoRank : DbSession {

	public RichinoRank() {
	}

	public int GetPageCount(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_RICHINO_RANK	");

		string sWhereClause;
		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd, out sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(), conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}


	public DataSet GetPageCollection(string pSiteCd, int startRowIndex, int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	SITE_CD						,");
		oSqlBuilder.AppendLine("	RICHINO_RANK				,");
		oSqlBuilder.AppendLine("	RICHINO_RANK_NM				,");
		oSqlBuilder.AppendLine("	RICHINO_MIN_RECEIPT_AMT		,");
		oSqlBuilder.AppendLine("	RICHINO_RANK_KEEP_AMT		,");
		oSqlBuilder.AppendLine("	SPECIAL_MOVIE_COUNT			,");
		oSqlBuilder.AppendLine("	SPECIAL_PIC_COUNT			,");
		oSqlBuilder.AppendLine("	MAIL_REQUEST_LIMIT_COUNT	,");
		oSqlBuilder.AppendLine("	MAN_TWEET_DAILY_COUNT		,");
		oSqlBuilder.AppendLine("	MAN_TWEET_INTERVAL_MIN		,");
		oSqlBuilder.AppendLine("	UPDATE_DATE");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	T_RICHINO_RANK	");

		string sWhereClause;
		string sSortExpression = "ORDER BY RICHINO_RANK";
		string sPagingSql;

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd, out sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(), sSortExpression, startRowIndex, maximumRows, out sPagingSql);

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql, conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}

	private OracleParameter[] CreateWhere(string pSiteCd, out string pWhere) {
		if (string.IsNullOrEmpty(pSiteCd)) {
			throw new ArgumentNullException();
		}

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	SITE_CD				= :SITE_CD		AND");
		oSqlBuilder.AppendLine("	RICHINO_RANK		<> :RICHINO_RANK	");

		pWhere = oSqlBuilder.ToString();

		List<OracleParameter> oOracleParameterList = new List<OracleParameter>();
		oOracleParameterList.Add(new OracleParameter("SITE_CD", pSiteCd));
		oOracleParameterList.Add(new OracleParameter("RICHINO_RANK", "*"));

		return oOracleParameterList.ToArray();
	}

	/// <summary>
	/// セレクトボックス表示用リッチーノランク取得
	/// </summary>
	/// <returns></returns>
	public DataSet GetList() {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSiteCd = iBridUtil.GetStringValue(HttpContext.Current.Session["SiteCd"]);
			string sSql = "SELECT RICHINO_RANK,RICHINO_RANK_NM FROM T_RICHINO_RANK ";

			if (!sSiteCd.Equals("")) {
				sSql += " WHERE SITE_CD =: SITE_CD ";
			}
			sSql += " ORDER BY RICHINO_RANK ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				if (!sSiteCd.Equals("")) {
					cmd.Parameters.Add("SITE_CD",sSiteCd);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_RICHINO_RANK");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
}