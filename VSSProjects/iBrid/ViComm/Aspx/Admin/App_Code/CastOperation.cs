﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者稼働状況
--	Progaram ID		: CastOperation
--
--  Creation Date	: 2010.05.14
--  Creater			: Nakano
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using Oracle.DataAccess.Client;
using System.Collections;
using System.Data;
using iBridCommLib;

/// <summary>
/// 出演者稼働状況

/// </summary>
public class CastOperation:DbSession {
	public int GetPageCount(
		string pSiteCd,
		string pRegistDayFrom,
		string pRegistDayTo,
		string pLoginDayFrom,
		string pLoginDayTo
	) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM ( " +
						  "SELECT VW_CAST_OPERATION01.* FROM  VW_CAST_OPERATION01, " +
						  "(SELECT " +
						  "T_LOGIN_CHARACTER.SITE_CD ," +
						  "T_LOGIN_CHARACTER.USER_SEQ , " +
						  "T_LOGIN_CHARACTER.USER_CHAR_NO " +
						  "FROM " +
						  "T_LOGIN_USER, " +
						  "T_LOGIN_CHARACTER " +
						  "WHERE " +
						  "T_LOGIN_CHARACTER.LOGIN_SEQ = T_LOGIN_USER.LOGIN_SEQ AND " +
						  "(T_LOGIN_USER.START_DATE >= :LOGIN_DATE_FROM AND " +
						  "T_LOGIN_USER.START_DATE <= :LOGIN_DATE_TO) " +
						  "GROUP BY " +
						  "T_LOGIN_CHARACTER.SITE_CD , " +
						  "T_LOGIN_CHARACTER.USER_SEQ , " +
						  "T_LOGIN_CHARACTER.USER_CHAR_NO " +
						  ") OPERATION ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pRegistDayFrom,pRegistDayTo,pLoginDayFrom,pLoginDayTo,ref sWhere);
			sSql = sSql + sWhere + " ) ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}



	public DataSet GetPageCollection(
	string pSiteCd,
	string pRegistDayFrom,
	string pRegistDayTo,
	string pLoginDayFrom,
	string pLoginDayTo,
	int startRowIndex,
	int maximumRows
) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY LOGIN_ID ";

			string sSql = "SELECT " +
								"SITE_CD			," +
								"USER_SEQ			," +
								"USER_CHAR_NO		," +
								"LOGIN_ID			," +
								"USER_STATUS		," +
								"USER_STATUS_NM		," +
								"LAST_ACTION		," +
								"LAST_ACTION_DATE	," +
								"TOTAL_POINT		," +
								"REGIST_DATE		," +
								"LOGIN_DAYS			," +
								"RNUM               " +
							  "FROM (" +
									"SELECT VW_CAST_OPERATION01.*                           ," +
									"ROW_NUMBER() OVER (" + sOrder + ") AS RNUM	" +
									"FROM  VW_CAST_OPERATION01, " +
									"(SELECT " +
									"T_LOGIN_CHARACTER.SITE_CD ," +
									"T_LOGIN_CHARACTER.USER_SEQ , " +
									"T_LOGIN_CHARACTER.USER_CHAR_NO " +
									"FROM " +
									"T_LOGIN_USER, " +
									"T_LOGIN_CHARACTER " +
									"WHERE " +
									"T_LOGIN_CHARACTER.LOGIN_SEQ = T_LOGIN_USER.LOGIN_SEQ AND " +
									"(T_LOGIN_USER.START_DATE >= :LOGIN_DATE_FROM AND " +
									"T_LOGIN_USER.START_DATE <= :LOGIN_DATE_TO) " +
									"GROUP BY " +
									"T_LOGIN_CHARACTER.SITE_CD , " +
									"T_LOGIN_CHARACTER.USER_SEQ , " +
									"T_LOGIN_CHARACTER.USER_CHAR_NO " +
									") OPERATION ";



			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pRegistDayFrom,pRegistDayTo,pLoginDayFrom,pLoginDayTo,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_OPERATION01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(
										string pSiteCd,
										string pRegistDateFrom,
										string pRegistDateTo,
										string pLoginDateFrom,
										string pLoginDateTo,
										ref string pWhere
										) {
		pWhere = "";
		SysPrograms.SqlAppendWhere("VW_CAST_OPERATION01.SITE_CD = OPERATION.SITE_CD",ref pWhere);
		SysPrograms.SqlAppendWhere("VW_CAST_OPERATION01.USER_SEQ = OPERATION.USER_SEQ",ref pWhere);
		SysPrograms.SqlAppendWhere("VW_CAST_OPERATION01.USER_CHAR_NO = OPERATION.USER_CHAR_NO",ref pWhere);

		ArrayList list = new ArrayList();

		DateTime loginDtFrom = DateTime.Parse(pLoginDateFrom + ":00:00");
		DateTime loginDtTo = DateTime.Parse(pLoginDateTo + ":59:59").AddSeconds(1);
		list.Add(new OracleParameter("LOGIN_DATE_FROM",OracleDbType.Date,loginDtFrom,ParameterDirection.Input));
		list.Add(new OracleParameter("LOGIN_DATE_TO",OracleDbType.Date,loginDtTo,ParameterDirection.Input));

		if (pSiteCd.Equals("") == false) {
			SysPrograms.SqlAppendWhere("VW_CAST_OPERATION01.SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		DateTime dtFrom = DateTime.Parse(pRegistDateFrom + ":00:00");
		DateTime dtTo = DateTime.Parse(pRegistDateTo + ":59:59").AddSeconds(1);
		SysPrograms.SqlAppendWhere("(VW_CAST_OPERATION01.REGIST_DATE >= :REGIST_DATE_FROM AND VW_CAST_OPERATION01.REGIST_DATE <= :REGIST_DATE_TO)",ref pWhere);
		list.Add(new OracleParameter("REGIST_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
		list.Add(new OracleParameter("REGIST_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}
