/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: サイト別広告グループ
--	Progaram ID		: SiteAdGroup
--
--  Creation Date	: 2009.09.11
--  Creater			: iBrid(Y.Inoue)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class SiteAdGroup:DbSession {

	public SiteAdGroup() {
	}

	public DataSet GetList(string pSiteCd,string pAdGroupCd) {
		DataSet ds;
		if (pAdGroupCd.Equals(ViCommConst.DEFAUL_AD_GROUP_CD)) {
			ds = GetNotAssignedAdList(pSiteCd);
		} else if (pSiteCd.Equals("") || pAdGroupCd.Equals("")) {
			using (Ad oAd = new Ad()) {
				ds = oAd.GetList();
			}
		} else {
			try{
				conn = DbConnect();
				ds = new DataSet();

				string sSql = "SELECT " +
								"AD_CD ," +
								"AD_NM " +
							  "FROM " +
								" VW_SITE_AD_GROUP02 ";

				sSql += " WHERE SITE_CD = :SITE_CD ";
				if (!pAdGroupCd.Equals("")) {
					sSql += " AND AD_GROUP_CD = :AD_GROUP_CD ";
				}

				sSql = sSql + " ORDER BY AD_CD";

				using (cmd = CreateSelectCommand(sSql,conn)) {
					cmd.Parameters.Add("SITE_CD",pSiteCd);
					if (!pAdGroupCd.Equals("")) {
						cmd.Parameters.Add("AD_GROUP_CD",pAdGroupCd);
					}
					using (da = new OracleDataAdapter(cmd)) {
						da.Fill(ds);
					}
				}
			} finally {
				conn.Close();
			}
		}
		return ds;
	}

	private DataSet GetNotAssignedAdList(string pSiteCd) {
		DataSet ds;
		try{
		
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT " +
							"AD_CD			," +
							"AD_NM			" +
						  "FROM " +
							" T_AD " +
						  "WHERE " +
							" AD_CD NOT IN ( " +
								" SELECT " +
									" AD_CD " +
								  "FROM " +
									"T_SITE_AD_GROUP " +
								  "WHERE " +
									  "SITE_CD = :SITE_CD " +
								  ") ";

			sSql = sSql + " ORDER BY AD_CD";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

}