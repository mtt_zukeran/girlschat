﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 日別ユーザー振分け実績


--	Progaram ID		: DailyDispatchReport
--
--  Creation Date	: 2010.03.18
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;
using System;

public class DailyDispatchReport:DbSession {

	private int iTotalRegist = 0;
	private int iTotalRefuse = 0;

	public DailyDispatchReport() {
	}

	public DataSet DailyDispatchReportInquiry(string pYYYY,string pMM) {

		DataColumn col;
		DataSet ds = new DataSet();
		DataTable dt = new DataTable();
		DataSet dsRtn = new DataSet();
		DataTable dtRtn = new DataTable();

		string sReportDayFrom = pYYYY + "/" + pMM + "/" + "01";
		string sReportDayTo;
		try {
			conn = DbConnect();

			if (DateTime.Today.ToString("yyyy/MM") == pYYYY + "/" + pMM) {
				sReportDayTo = DateTime.Today.ToString("yyyy/MM/dd");
			} else {
				sReportDayTo = (DateTime.Parse(sReportDayFrom).AddMonths(1)).AddDays(-1).ToString("yyyy/MM/dd");
			}

			//データ検索
			string sSql = "SELECT " +
								"PARTNER_SITE_SEQ		," +
								"PARTNER_SITE_NM		," +
								"PARTNER_SITE_INITIAL	," +
								"REPORT_DAY				," +
								"REGIST_COUNT			AS REGIST," +
								"REGIST_REFUSE_COUNT	AS REFUSE," +
								"STOP_FLAG				" +
							"FROM (" +
								"SELECT " +
									"T_PARTNER_SITE.PARTNER_SITE_SEQ				," +
									"T_PARTNER_SITE.PARTNER_SITE_NM					," +
									"T_PARTNER_SITE.PARTNER_SITE_INITIAL			," +
									"T_DISPATCH_REGIST_REPORT.REPORT_DAY			," +
									"T_DISPATCH_REGIST_REPORT.REGIST_COUNT			," +
									"T_DISPATCH_REGIST_REPORT.REGIST_REFUSE_COUNT	," +
									"T_PARTNER_SITE.STOP_FLAG						," +
									"T_DISPATCH_REGIST_REPORT.HISTORY_SEQ			" +
								"FROM " +
									"T_PARTNER_SITE " +
								"LEFT OUTER JOIN (" +
									"SELECT * FROM T_DISPATCH_REGIST_REPORT " +
									"WHERE REPORT_DAY BETWEEN :REPORT_DAY_FROM AND :REPORT_DAY_TO ) T_DISPATCH_REGIST_REPORT " +
								"ON	T_PARTNER_SITE.PARTNER_SITE_SEQ = T_DISPATCH_REGIST_REPORT.PARTNER_SITE_SEQ ) " +
							"UNION ALL " +
							"SELECT " +
								"PARTNER_SITE_SEQ					," +
								"PARTNER_SITE_NM					," +
								"PARTNER_SITE_INITIAL				," +
								"'9999/99/99'						," +
								"NVL(SUM(REGIST_COUNT), 0)			," +
								"NVL(SUM(REGIST_REFUSE_COUNT), 0)	," +
								"STOP_FLAG					" +
							"FROM (" +
								"SELECT " +
									"T_PARTNER_SITE.PARTNER_SITE_SEQ				," +
									"T_PARTNER_SITE.PARTNER_SITE_NM					," +
									"T_PARTNER_SITE.PARTNER_SITE_INITIAL			," +
									"T_DISPATCH_REGIST_REPORT.REPORT_DAY			," +
									"T_DISPATCH_REGIST_REPORT.REGIST_COUNT			," +
									"T_DISPATCH_REGIST_REPORT.REGIST_REFUSE_COUNT	," +
									"T_PARTNER_SITE.STOP_FLAG						," +
									"T_DISPATCH_REGIST_REPORT.HISTORY_SEQ			" +
								"FROM " +
									"T_PARTNER_SITE " +
								"LEFT OUTER JOIN (" +
									"SELECT * FROM T_DISPATCH_REGIST_REPORT " +
									"WHERE REPORT_DAY BETWEEN :REPORT_DAY_FROM AND :REPORT_DAY_TO ) T_DISPATCH_REGIST_REPORT " +
								"ON	T_PARTNER_SITE.PARTNER_SITE_SEQ = T_DISPATCH_REGIST_REPORT.PARTNER_SITE_SEQ ) " +
							"GROUP BY " +
								"PARTNER_SITE_SEQ			," +
								"PARTNER_SITE_NM			," +
								"PARTNER_SITE_INITIAL		," +
								"STOP_FLAG					" +
							"ORDER BY PARTNER_SITE_SEQ, REPORT_DAY ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("REPORT_DAY_FROM",sReportDayFrom);
				cmd.Parameters.Add("REPORT_DAY_TO",sReportDayTo);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		dt = ds.Tables[0];

		//カラム追加
		col = new DataColumn("REPORT_DAY",System.Type.GetType("System.String"));
		dtRtn.Columns.Add(col);
		col = new DataColumn("TOTAL_REGIST",System.Type.GetType("System.String"));
		dtRtn.Columns.Add(col);
		col = new DataColumn("TOTAL_REFUSE",System.Type.GetType("System.String"));
		dtRtn.Columns.Add(col);

		DataRow[] drArr = dt.Select("REPORT_DAY = '9999/99/99'","PARTNER_SITE_SEQ");
		if (drArr.Length > 0) {
			foreach (DataRow dr in drArr) {
				if (int.Parse(dr["REGIST"].ToString()) != 0 ||
					int.Parse(dr["REFUSE"].ToString()) != 0 ||
					dr["STOP_FLAG"].ToString().Equals("0")) {

					//パートナーサイトの 停止フラグが0 または、合計登録数が0以外 または、合計登録拒否数が0以外 の場合はカラム追加
					col = new DataColumn(dr["PARTNER_SITE_SEQ"] + ":" + dr["PARTNER_SITE_INITIAL"] + ":REGIST",System.Type.GetType("System.String"));
					dtRtn.Columns.Add(col);
					col = new DataColumn(dr["PARTNER_SITE_SEQ"] + ":" + dr["PARTNER_SITE_INITIAL"] + ":REFUSE",System.Type.GetType("System.String"));
					dtRtn.Columns.Add(col);
				}
			}
		}
		dtRtn.AcceptChanges();

		//レコード追加
		for (int i = 1;i <= int.Parse(sReportDayTo.Split('/')[2]);i++) {
			string sReportDay = string.Format("{0}/{1}/{2}",pYYYY,pMM,string.Format("{0:D2}",i));

			iTotalRegist = 0;
			iTotalRefuse = 0;

			DataRow drNew = dtRtn.NewRow();
			drNew["REPORT_DAY"] = sReportDay;

			foreach (DataColumn dc in dtRtn.Columns) {
				if (dc.ColumnName.Split(':').Length == 3) {
					drNew[dc.ColumnName] = GetCount(dt,dc,sReportDay);
				}
			}
			drNew["TOTAL_REGIST"] = iTotalRegist;
			drNew["TOTAL_REFUSE"] = iTotalRefuse;
			dtRtn.Rows.Add(drNew);
		}

		//合計行

		iTotalRegist = 0;
		iTotalRefuse = 0;

		DataRow drTotal = dtRtn.NewRow();
		drTotal["REPORT_DAY"] = "合計";

		foreach (DataColumn dc in dtRtn.Columns) {
			if (dc.ColumnName.Split(':').Length == 3) {
				drTotal[dc.ColumnName] = GetCount(dt,dc,"9999/99/99");
			}
		}
		drTotal["TOTAL_REGIST"] = iTotalRegist;
		drTotal["TOTAL_REFUSE"] = iTotalRefuse;
		dtRtn.Rows.Add(drTotal);
		dtRtn.AcceptChanges();

		dsRtn.Tables.Add(dtRtn);
		return dsRtn;
	}

	private string GetCount(DataTable dt,DataColumn dc, string sDay) {
		string sCount = "0";
		DataRow[] dr = dt.Select(string.Format("REPORT_DAY='{0}' AND PARTNER_SITE_SEQ='{1}'",sDay,dc.ColumnName.Split(':')[0]));

		if (dr.Length != 0) {
			sCount = dr[0][dc.ColumnName.Split(':')[2]].ToString();
			if (dc.ColumnName.IndexOf("REGIST") > 0) {
				iTotalRegist += int.Parse(sCount);
			}
			if (dc.ColumnName.IndexOf("REFUSE") > 0) {
				iTotalRefuse += int.Parse(sCount);
			}
		}
		return sCount;
	}
}
