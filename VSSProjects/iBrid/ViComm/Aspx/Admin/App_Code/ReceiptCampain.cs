﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: User
--	Title			: 入金キャンペーン設定--	Progaram ID		: ReceiptCampain
--
--  Creation Date	: 2008.10.21
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;


public class ReceiptCampain : DbSession {

	public ReceiptCampain() {
	}

	public int GetPageCount(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT	COUNT(*)");
		oSqlBuilder.AppendLine("FROM (	");
		oSqlBuilder.AppendLine("	SELECT	");
		oSqlBuilder.AppendLine("		ROW_NUMBER() OVER(PARTITION BY P.SITE_CD, P.SETTLE_TYPE, D.CODE_NM ORDER BY P.SETTLE_TYPE) AS ROW_NUMBER");
		oSqlBuilder.AppendLine("	FROM	");
		oSqlBuilder.AppendLine("		T_RECEIPT_SERVICE_POINT	P,");
		oSqlBuilder.AppendLine("		T_CODE_DTL				D");
		oSqlBuilder.AppendLine("	WHERE	");
		oSqlBuilder.AppendLine("		P.SITE_CD		= :SITE_CD			AND");
		oSqlBuilder.AppendLine("		'74'			= D.CODE_TYPE	(+)	AND");
		oSqlBuilder.AppendLine("		P.SETTLE_TYPE	= D.CODE		(+)	)");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	ROW_NUMBER	= 1");
		
		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(), conn)) {
				cmd.Parameters.Add(":SITE_CD", pSiteCd);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd, int startRowIndex, int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	SITE_CD						,");
		oSqlBuilder.AppendLine("	SETTLE_TYPE					,");
		oSqlBuilder.AppendLine("	SETTLE_TYPE_NM				,");
		oSqlBuilder.AppendLine("	CAMPAIN_APPLICATION_FLAG	,");
		oSqlBuilder.AppendLine("	APPLICATION_START_DATE		,");
		oSqlBuilder.AppendLine("	APPLICATION_END_DATE");
		oSqlBuilder.AppendLine("FROM (	");
		oSqlBuilder.AppendLine("	SELECT	");
		oSqlBuilder.AppendLine("		P.SITE_CD									,");
		oSqlBuilder.AppendLine("		P.SETTLE_TYPE								,");
		oSqlBuilder.AppendLine("		D.CODE_NM					SETTLE_TYPE_NM	,");
		oSqlBuilder.AppendLine("		P.CAMPAIN_APPLICATION_FLAG					,");
		oSqlBuilder.AppendLine("		P.APPLICATION_START_DATE					,");
		oSqlBuilder.AppendLine("		P.APPLICATION_END_DATE						,");
		oSqlBuilder.AppendLine("		ROW_NUMBER() OVER(PARTITION BY P.SITE_CD, P.SETTLE_TYPE, D.CODE_NM ORDER BY P.SETTLE_TYPE) AS ROW_NUMBER");
		oSqlBuilder.AppendLine("	FROM	");
		oSqlBuilder.AppendLine("		T_RECEIPT_SERVICE_POINT	P,");
		oSqlBuilder.AppendLine("		T_CODE_DTL				D");
		oSqlBuilder.AppendLine("	WHERE	");
		oSqlBuilder.AppendLine("		P.SITE_CD		= :SITE_CD			AND");
		oSqlBuilder.AppendLine("		'74'			= D.CODE_TYPE	(+)	AND");
		oSqlBuilder.AppendLine("		P.SETTLE_TYPE	= D.CODE		(+)	)");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	ROW_NUMBER	= 1");

		string sPagingSql = string.Empty;
		string sSortExpression = "ORDER BY SITE_CD, SETTLE_TYPE";

		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.ToString(), sSortExpression, startRowIndex, maximumRows, out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql, conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD", pSiteCd);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}

}