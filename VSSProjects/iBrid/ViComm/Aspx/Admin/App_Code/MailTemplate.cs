/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メールテンプレート
--	Progaram ID		: MailTemplate
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX
  2010/12/07  ttakahashi GetOne()の取得対象にTEXT_MAIL_FLAGを追加し、その値をプロパティIsTextMailとして保持するよう修正

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Text;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class MailTemplate:DbSession {

	public string htmlDoc;
	public string textDoc;
	public string mailTitle;
	public string mailTemplateType;
	public string sexCd;

	private bool isTextMail;
	/// <summary>
	/// GetOne()で取得したデータがテキストメールであるか否かの値を取得します。

	/// </summary>
	public bool IsTextMail {
		get {
			return isTextMail;
		}
	}


	public MailTemplate() {
		htmlDoc = string.Empty;
		textDoc = string.Empty;
		mailTitle = string.Empty;
		mailTemplateType = string.Empty;
	}

	public int GetPageCount(string pSiteCd,string pMailTemplateType) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_MAIL_TEMPLATE02 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pMailTemplateType,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pMailTemplateType,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY SITE_CD,MAIL_TEMPLATE_NO ";
			string sSql = "SELECT " +
							 "SITE_CD			," +
							"MAIL_TEMPLATE_NO	," +
							"MAIL_TEMPLATE_TYPE	," +
							"TEMPLATE_NM		," +
							"MAIL_TITLE			," +
							"MIME_SIZE			," +
							"SEX_CD				," +
							"HTML_DOC_SEQ		," +
							"WEB_FACE_SEQ		," +
							"UPDATE_DATE		," +
							"REVISION_NO		," +
							"HTML_DOC_TITLE		," +
							"SUBSTR(HTML_DOC1,1,20) DOC," +
							"HTML_DOC1			," +
							"HTML_DOC2			," +
							"HTML_DOC3			," +
							"HTML_DOC4			," +
							"MAIL_TEMPLATE_TYPE_NM," +
							"SITE_NM,       	" +
							"SEX_NM	" +
							"FROM(" +
							" SELECT VW_MAIL_TEMPLATE02.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_MAIL_TEMPLATE02  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pMailTemplateType,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_MAIL_TEMPLATE02");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetCsvData(string pSiteCd,string pMailTemplateType) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = " ORDER BY SITE_CD,MAIL_TEMPLATE_NO ";
			string sSql = "SELECT " +
							"SITE_CD				," +
							"MAIL_TEMPLATE_NO		," +
							"MAIL_TEMPLATE_TYPE		," +
							"CAST_CREATE_ORG_SUB_SEQ," +
							"WEB_FACE_SEQ			," +
							"MOBILE_MAIL_FONT_SIZE	," +
							"MOBILE_MAIL_BACK_COLOR	," +
							"MAIL_TITLE				," +
							"EMAIL_ADDR				," +
							"TEMPLATE_NM			," +
							"MAIL_ATTACHED_OBJ_TYPE	," +
							"MAIL_ATTACHED_METHOD	," +
							"TEXT_MAIL_FLAG			," +
							"NON_DISP_IN_MAIL_BOX	," +
							"NON_DISP_IN_DECOMAIL	," +
							"SEX_CD					," +
							"FORCE_TX_MOBILE_MAIL_FLAG," +
							"HTML_DOC_SUB_SEQ		," +
							"HTML_DOC_TITLE			," +
							"HTML_DOC1				," +
							"HTML_DOC2				," +
							"HTML_DOC3				," +
							"HTML_DOC4				," +
							"HTML_DOC5				," +
							"HTML_DOC6				," +
							"HTML_DOC7				," +
							"HTML_DOC8				," +
							"HTML_DOC9				," +
							"HTML_DOC10				," +
							"HTML_DOC11				," +
							"HTML_DOC12				," +
							"HTML_DOC13				," +
							"HTML_DOC14				," +
							"HTML_DOC15				," +
							"HTML_DOC16				," +
							"HTML_DOC17				," +
							"HTML_DOC18				," +
							"HTML_DOC19				," +
							"HTML_DOC20				 " +
							"FROM VW_MAIL_TEMPLATE04 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pMailTemplateType,ref sWhere);
			sSql = sSql + sWhere;
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_MAIL_TEMPLATE04");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pMailTemplateType,ref string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD ",ref pWhere);
		list.Add(new OracleParameter("SITE_CD",pSiteCd));

		if (!pMailTemplateType.Equals("")) {
			SysPrograms.SqlAppendWhere("MAIL_TEMPLATE_TYPE = :MAIL_TEMPLATE_TYPE ",ref pWhere);
			list.Add(new OracleParameter("MAIL_TEMPLATE_TYPE",pMailTemplateType));
		}
		SysPrograms.SqlAppendWhere("DEL_FLAG = :DEL_FLAG ",ref pWhere);
		list.Add(new OracleParameter("DEL_FLAG",ViCommConst.FLAG_OFF));

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public DataSet GetCastCreateOriginal(string pSiteCd) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT " +
								"MAIL_TEMPLATE_NO	," +
								"TEMPLATE_NM		" +
							"FROM " +
								"VW_MAIL_TEMPLATE01 " +
							"WHERE " +
								"SITE_CD =:SITE_CD AND CAST_CREATE_ORG_SUB_SEQ != 0 AND CAST_CREATE_ORG_SUB_SEQ = HTML_DOC_SUB_SEQ " +
								"AND SEX_CD IN ('3', '1') " + 
							"ORDER BY SITE_CD,TEMPLATE_NM,MAIL_TEMPLATE_NO DESC ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_MAIL_TEMPLATE01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	public string GetModifiableSubSeq(string pSiteCd,string pMailTemplateNo) {
		string sSubSeq = "";
		DataSet ds;
		DataRow dr;
		try {
			conn = DbConnect();

			string sSql = "SELECT " +
							"CAST_CREATE_ORG_SUB_SEQ " +
						"FROM " +
							"T_MAIL_TEMPLATE " +
						"WHERE " +
							"SITE_CD = :SITE_CD AND MAIL_TEMPLATE_NO = :MAIL_TEMPLATE_NO";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("MAIL_TEMPLATE_NO",pMailTemplateNo);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MAIL_TEMPLATE");

					if (ds.Tables["T_MAIL_TEMPLATE"].Rows.Count != 0) {
						dr = ds.Tables["T_MAIL_TEMPLATE"].Rows[0];
						sSubSeq = dr["CAST_CREATE_ORG_SUB_SEQ"].ToString();
					}
				}
			}
		} finally {
			conn.Close();
		}
		return sSubSeq;
	}

	public DataSet GetListByTemplateType(string pSiteCd,string pMailTemplateType, string pSexCd) {
		return GetListByTemplateType(pSiteCd,pMailTemplateType,string.Empty,pSexCd);
	}

	public DataSet GetListByTemplateType(string pSiteCd,string pMailTemplateType,string pMailTemplateType2,string pSexCd) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			StringBuilder oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine("SELECT	");
			oSqlBuilder.AppendLine("	MT.MAIL_TEMPLATE_NO		,");
			oSqlBuilder.AppendLine("	MT.TEMPLATE_NM			,");
			oSqlBuilder.AppendLine("	SHM.SEX_CD");
			oSqlBuilder.AppendLine("FROM	");
			oSqlBuilder.AppendLine("	T_MAIL_TEMPLATE		MT	,");
			oSqlBuilder.AppendLine("	T_SITE_HTML_MANAGE	SHM");
			oSqlBuilder.AppendLine("WHERE	");
			oSqlBuilder.AppendLine("	MT.SITE_CD				= SHM.SITE_CD			(+)	AND");
			oSqlBuilder.AppendLine("	MT.HTML_DOC_SEQ			= SHM.HTML_DOC_SEQ		(+)	AND");
			oSqlBuilder.AppendLine("	MT.SITE_CD				= :SITE_CD					AND");
			if (!string.IsNullOrEmpty(pMailTemplateType2)) {
				oSqlBuilder.AppendLine("	MT.MAIL_TEMPLATE_TYPE	IN (:MAIL_TEMPLATE_TYPE,:MAIL_TEMPLATE_TYPE2)		AND");
			} else {
				oSqlBuilder.AppendLine("	MT.MAIL_TEMPLATE_TYPE	= :MAIL_TEMPLATE_TYPE		AND");
			}
			oSqlBuilder.AppendLine("	MT.DEL_FLAG				= :DEL_FLAG					");
			oSqlBuilder.AppendFormat("	{0}", string.IsNullOrEmpty(pSexCd) ? string.Empty : "AND SHM.SEX_CD IN ('3', :SEX_CD)").AppendLine();
			oSqlBuilder.AppendLine("ORDER BY MT.TEMPLATE_NM,MT.MAIL_TEMPLATE_NO");

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("MAIL_TEMPLATE_TYPE",pMailTemplateType);
				if (!string.IsNullOrEmpty(pMailTemplateType2)) {
					cmd.Parameters.Add("MAIL_TEMPLATE_TYPE2",pMailTemplateType2);
				}
				cmd.Parameters.Add("DEL_FLAG",ViCommConst.FLAG_OFF);
				if (!string.IsNullOrEmpty(pSexCd)) {
					cmd.Parameters.Add("SEX_CD", pSexCd);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetNewVerList(string pSiteCd, string pSexCd) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			StringBuilder oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine("SELECT	");
			oSqlBuilder.AppendLine("	MAIL_TEMPLATE_NO		,");
			oSqlBuilder.AppendLine("	TEMPLATE_NM				,");
			oSqlBuilder.AppendLine("	MAIL_TEMPLATE_TYPE		,");
			oSqlBuilder.AppendLine("	SEX_CD					");
			oSqlBuilder.AppendLine("FROM	");
			oSqlBuilder.AppendLine("	T_MAIL_TEMPLATE			,");
			oSqlBuilder.AppendLine("	T_SITE_HTML_MANAGE		");
			oSqlBuilder.AppendLine("WHERE	");
			oSqlBuilder.AppendLine("	T_MAIL_TEMPLATE.SITE_CD			= T_SITE_HTML_MANAGE.SITE_CD		(+)	AND");
			oSqlBuilder.AppendLine("	T_MAIL_TEMPLATE.HTML_DOC_SEQ	= T_SITE_HTML_MANAGE.HTML_DOC_SEQ	(+) AND");
			oSqlBuilder.AppendLine("	T_MAIL_TEMPLATE.SITE_CD			= :SITE_CD								AND");
			oSqlBuilder.AppendLine("	T_MAIL_TEMPLATE.DEL_FLAG		= :DEL_FLAG								");
			oSqlBuilder.AppendFormat("	{0}", string.IsNullOrEmpty(pSexCd) ? string.Empty : "AND T_SITE_HTML_MANAGE.SEX_CD IN ('3', :SEX_CD)").AppendLine();
			oSqlBuilder.AppendLine("ORDER BY T_MAIL_TEMPLATE.TEMPLATE_NM,T_MAIL_TEMPLATE.MAIL_TEMPLATE_NO");

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("DEL_FLAG",ViCommConst.FLAG_OFF);
				if (!string.IsNullOrEmpty(pSexCd)) {
					cmd.Parameters.Add("SEX_CD", pSexCd);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MAIL_TEMPLATE");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public bool GetOne(string pSiteCd,string pMailTemplateNo) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect();

			StringBuilder sSql = new StringBuilder();

			sSql.Append("SELECT ").AppendLine();
			sSql.Append("	MAIL_TITLE			,").AppendLine();
			sSql.Append("	MAIL_TEMPLATE_TYPE	,").AppendLine();
			sSql.Append("   TEXT_MAIL_FLAG      ,").AppendLine();
			sSql.Append("	HTML_DOC1			,").AppendLine();
			sSql.Append("	HTML_DOC2			,").AppendLine();
			sSql.Append("	HTML_DOC3			,").AppendLine();
			sSql.Append("	HTML_DOC4			,").AppendLine();
			sSql.Append("	HTML_DOC5			,").AppendLine();
			sSql.Append("	HTML_DOC6			,").AppendLine();
			sSql.Append("	HTML_DOC7			,").AppendLine();
			sSql.Append("	HTML_DOC8			,").AppendLine();
			sSql.Append("	HTML_DOC9			,").AppendLine();
			sSql.Append("	HTML_DOC10			,").AppendLine();
			sSql.Append("	HTML_DOC11			,").AppendLine();
			sSql.Append("	HTML_DOC12			,").AppendLine();
			sSql.Append("	HTML_DOC13			,").AppendLine();
			sSql.Append("	HTML_DOC14			,").AppendLine();
			sSql.Append("	HTML_DOC15			,").AppendLine();
			sSql.Append("	HTML_DOC16			,").AppendLine();
			sSql.Append("	HTML_DOC17			,").AppendLine();
			sSql.Append("	HTML_DOC18			,").AppendLine();
			sSql.Append("	HTML_DOC19			,").AppendLine();
			sSql.Append("	HTML_DOC20			,").AppendLine();
			sSql.Append("	SEX_CD				 ").AppendLine();
			sSql.Append("FROM VW_MAIL_TEMPLATE01 ").AppendLine();
			sSql.Append("WHERE  ").AppendLine();
			sSql.Append("	SITE_CD				=:SITE_CD			AND").AppendLine();
			sSql.Append("	MAIL_TEMPLATE_NO	=:MAIL_TEMPLATE_NO	AND").AppendLine();
			sSql.Append("	HTML_DOC_SUB_SEQ	=:HTML_DOC_SUB_SEQ ").AppendLine();

			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("MAIL_TEMPLATE_NO",pMailTemplateNo);
				cmd.Parameters.Add("HTML_DOC_SUB_SEQ",1);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_MAIL_TEMPLATE01");
					if (ds.Tables["VW_MAIL_TEMPLATE01"].Rows.Count != 0) {
						dr = ds.Tables["VW_MAIL_TEMPLATE01"].Rows[0];
						mailTitle = dr["MAIL_TITLE"].ToString();
						htmlDoc = dr["HTML_DOC1"].ToString() +
									dr["HTML_DOC2"].ToString() +
									dr["HTML_DOC3"].ToString() +
									dr["HTML_DOC4"].ToString() +
									dr["HTML_DOC5"].ToString() +
									dr["HTML_DOC6"].ToString() +
									dr["HTML_DOC7"].ToString() +
									dr["HTML_DOC8"].ToString() +
									dr["HTML_DOC9"].ToString() +
									dr["HTML_DOC10"].ToString();
						textDoc = dr["HTML_DOC11"].ToString() +
									dr["HTML_DOC12"].ToString() +
									dr["HTML_DOC13"].ToString() +
									dr["HTML_DOC14"].ToString() +
									dr["HTML_DOC15"].ToString() +
									dr["HTML_DOC16"].ToString() +
									dr["HTML_DOC17"].ToString() +
									dr["HTML_DOC18"].ToString() +
									dr["HTML_DOC19"].ToString() +
									dr["HTML_DOC20"].ToString();

						mailTemplateType = dr["MAIL_TEMPLATE_TYPE"].ToString();
						sexCd = dr["SEX_CD"].ToString();
						this.isTextMail = int.Parse(dr["TEXT_MAIL_FLAG"].ToString()) > 0 ? true : false;
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public long GetMimeSize(string pSiteCd, string pMailTemplateNo) {
		try {
			conn = DbConnect();

			StringBuilder sSql = new StringBuilder();

			string sMimeSize = string.Empty;

			sSql.AppendLine("SELECT	");
			sSql.AppendLine("	MIME_SIZE				");
			sSql.AppendLine("FROM T_MAIL_TEMPLATE ");
			sSql.AppendLine("WHERE	");
			sSql.AppendLine("	SITE_CD				=:SITE_CD			AND");
			sSql.AppendLine("	MAIL_TEMPLATE_NO	=:MAIL_TEMPLATE_NO	");

			using (cmd = CreateSelectCommand(sSql.ToString(), conn)){
				cmd.Parameters.Add("SITE_CD", pSiteCd);
				cmd.Parameters.Add("MAIL_TEMPLATE_NO", pMailTemplateNo);
				sMimeSize =iBridUtil.GetStringValue(cmd.ExecuteScalar());
				if(string.IsNullOrEmpty(sMimeSize)){
					return 0;
				}
				return long.Parse(sMimeSize);
			}
		} finally {
			conn.Close();
		}
	}

	public DataSet GetListByTemplateNo(string pSiteCd, string pMailTemplateNo) {
		try {
			conn = DbConnect();

			StringBuilder sSql = new StringBuilder();

			sSql.AppendLine("SELECT	");
			sSql.AppendLine("	MAIL_TITLE			,");
			sSql.AppendLine("	MAIL_TEMPLATE_TYPE	,");
			sSql.AppendLine("	TEXT_MAIL_FLAG		,");
			sSql.AppendLine("	HTML_DOC1			,");
			sSql.AppendLine("	HTML_DOC2			,");
			sSql.AppendLine("	HTML_DOC3			,");
			sSql.AppendLine("	HTML_DOC4			,");
			sSql.AppendLine("	HTML_DOC5			,");
			sSql.AppendLine("	HTML_DOC6			,");
			sSql.AppendLine("	HTML_DOC7			,");
			sSql.AppendLine("	HTML_DOC8			,");
			sSql.AppendLine("	HTML_DOC9			,");
			sSql.AppendLine("	HTML_DOC10			,");
			sSql.AppendLine("	HTML_DOC11			,");
			sSql.AppendLine("	HTML_DOC12			,");
			sSql.AppendLine("	HTML_DOC13			,");
			sSql.AppendLine("	HTML_DOC14			,");
			sSql.AppendLine("	HTML_DOC15			,");
			sSql.AppendLine("	HTML_DOC16			,");
			sSql.AppendLine("	HTML_DOC17			,");
			sSql.AppendLine("	HTML_DOC18			,");
			sSql.AppendLine("	HTML_DOC19			,");
			sSql.AppendLine("	HTML_DOC20			");
			sSql.AppendLine("FROM VW_MAIL_TEMPLATE01 ");
			sSql.AppendLine("WHERE	");
			sSql.AppendLine("	SITE_CD				=:SITE_CD			AND");
			sSql.AppendLine("	MAIL_TEMPLATE_NO	=:MAIL_TEMPLATE_NO	");
			sSql.AppendLine("ORDER BY SITE_CD, MAIL_TEMPLATE_NO, HTML_DOC_SUB_SEQ");

			using (cmd = CreateSelectCommand(sSql.ToString(), conn))
			using (DataSet ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD", pSiteCd);
				cmd.Parameters.Add("MAIL_TEMPLATE_NO", pMailTemplateNo);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds, "VW_MAIL_TEMPLATE01");
					return ds;
				}
			}

		} finally {
			conn.Close();
		}
	}
}
