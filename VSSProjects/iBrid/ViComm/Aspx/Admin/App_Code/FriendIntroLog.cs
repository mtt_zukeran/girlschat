﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 友達紹介ポイント履歴

--	Progaram ID		: FriendIntroLog
--
--  Creation Date	: 2010.11.28
--  Creater			: R.Suzuki
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Web;
using System;
using System.Data;
using System.Data.Common;
using System.Text;
using iBridCommLib;

public class FriendIntroLog:DbSession {
	public FriendIntroLog() {
	}

	public int GetPageCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pCreateDayFrom,string pCreateDayTo,string pFriendIntroType,string pLoginId) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			StringBuilder oSqlBuilder = new StringBuilder();
			oSqlBuilder.Append("SELECT									").AppendLine();
			oSqlBuilder.Append("	COUNT(FRIEND_INTRO_SEQ) AS ROW_COUNT").AppendLine();
			oSqlBuilder.Append("FROM									").AppendLine();
			oSqlBuilder.Append("	VW_FRIEND_INTRO_LOG01				").AppendLine();

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pCreateDayFrom,pCreateDayTo,pFriendIntroType,pLoginId,ref sWhere);
			oSqlBuilder.Append(sWhere);

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}


	public DataSet GetPageCollection(string pSiteCd,string pUserSeq,string pUserCharNo,string pCreateDayFrom,string pCreateDayTo,string pFriendIntroType,string pLoginid,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY FRIEND_INTRO_SEQ DESC ";
			StringBuilder oSqlBuilder = new StringBuilder();
			oSqlBuilder.Append("SELECT								").AppendLine();
			oSqlBuilder.Append("	SITE_CD						,	").AppendLine();
			oSqlBuilder.Append("	USER_SEQ					,	").AppendLine();
			oSqlBuilder.Append("	USER_CHAR_NO				,	").AppendLine();
			oSqlBuilder.Append("	PARTNER_SITE_CD				,	").AppendLine();
			oSqlBuilder.Append("	PARTNER_USER_SEQ			,	").AppendLine();
			oSqlBuilder.Append("	PARTNER_USER_CHAR_NO		,	").AppendLine();
			oSqlBuilder.Append("	FRIEND_INTRO_POINT			,	").AppendLine();
			oSqlBuilder.Append("	BEFORE_FRIEND_INTRO_POINT	,	").AppendLine();
			oSqlBuilder.Append("	AFTER_FRIEND_INTRO_POINT	,	").AppendLine();
			oSqlBuilder.Append("	FRIEND_INTRO_TYPE			,	").AppendLine();
			oSqlBuilder.Append("	FRIEND_INTRO_TYPE_NM		,	").AppendLine();
			oSqlBuilder.Append("	CREATE_DATE					,	").AppendLine();
			oSqlBuilder.Append("	LOGIN_ID					,	").AppendLine();
			oSqlBuilder.Append("	SEX_CD						,	").AppendLine();
			oSqlBuilder.Append("	PARTNER_LOGIN_ID			,	").AppendLine();
			oSqlBuilder.Append("	PARTNER_SEX_CD					").AppendLine();
			oSqlBuilder.Append("FROM(								").AppendLine();
			oSqlBuilder.Append("	SELECT							").AppendLine();
			oSqlBuilder.Append("		VW_FRIEND_INTRO_LOG01.*	,	").AppendLine();
			oSqlBuilder.Append("		ROW_NUMBER() OVER (			").AppendLine();
			oSqlBuilder.Append(sOrder).AppendLine();
			oSqlBuilder.Append("	) AS RNUM						").AppendLine();
			oSqlBuilder.Append("	FROM							").AppendLine();
			oSqlBuilder.Append("		VW_FRIEND_INTRO_LOG01		").AppendLine();

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pCreateDayFrom,pCreateDayTo,pFriendIntroType,pLoginid,ref sWhere);

			oSqlBuilder.Append(sWhere).AppendLine();
			oSqlBuilder.Append(")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW").AppendLine();
			oSqlBuilder.Append(sOrder).AppendLine();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_FRIEND_INTRO_LOG01");
				}
			}
		} finally {
			conn.Close();
		}

		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,string pUserSeq,string pUserCharNo,string pCreateDayFrom,string pCreateDayTo,string pFriendIntroType,string pLoginId,ref string pWhere) {
		ArrayList list = new ArrayList();

		if (!pSiteCd.Equals("")) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!pUserSeq.Equals("")) {
			SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhere);
			list.Add(new OracleParameter("USER_SEQ",pUserSeq));
			if (!pUserCharNo.Equals("")) {
				SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhere);
				list.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));
			}
		}

		if (!pLoginId.Equals("")) {
			SysPrograms.SqlAppendWhere("LOGIN_ID = :LOGIN_ID",ref pWhere);
			list.Add(new OracleParameter("LOGIN_ID",pLoginId));
		}

		if ((!pCreateDayFrom.Equals("")) && (!pCreateDayTo.Equals(""))) {
			DateTime dtFrom = DateTime.Parse(pCreateDayFrom + " " + "00:00:00");
			DateTime dtTo = DateTime.Parse(pCreateDayTo + " " + "23:59:59").AddSeconds(1);
			iBridCommLib.SysPrograms.SqlAppendWhere("CREATE_DATE >= :CREATE_DATE_FROM AND CREATE_DATE < :CREATE_DATE_TO",ref pWhere);
			list.Add(new OracleParameter("CREATE_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("CREATE_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (!pFriendIntroType.Equals("")) {
			SysPrograms.SqlAppendWhere("FRIEND_INTRO_TYPE = :FRIEND_INTRO_TYPE",ref pWhere);
			list.Add(new OracleParameter("FRIEND_INTRO_TYPE",pFriendIntroType));
		}
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}
