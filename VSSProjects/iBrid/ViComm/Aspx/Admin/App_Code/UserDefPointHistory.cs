﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ユーザー定義ポイント
--	Progaram ID		: UserDefPoint
--
--  Creation Date	: 2010.04.05
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Web;
using System.Data;
using System.Data.Common;
using System.Text;
using iBridCommLib;
using ViComm;
using System.Collections.Generic;


public class UserDefPointHistory:DbSession {
	public UserDefPointHistory() {
	}

	public int GetPageCount(string pSiteCd,string pUserSeq) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(ADD_POINT_ID) AS ROW_COUNT FROM T_USER_DEF_POINT_HISTORY ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}

		} finally {
			conn.Close();
		}

		return iPageCount;

	}

	public DataSet GetPageCollection(string pSiteCd,string pUserSeq,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY ADD_POINT_DATE DESC,SITE_CD,USER_SEQ,ADD_POINT_ID ";
			string sSql = "SELECT																 " +
								"SITE_CD														," +
								"USER_SEQ														," +
								"ADD_POINT_ID													," +
								"ADD_POINT														," +
								"ADD_POINT_DATE													," +
								"REMARKS														"  +
							"FROM(																 " +
							" SELECT															 " +
								"T_USER_DEF_POINT_HISTORY.SITE_CD								," +
								"T_USER_DEF_POINT_HISTORY.USER_SEQ								," +
								"T_USER_DEF_POINT_HISTORY.ADD_POINT_ID							," +
								"T_USER_DEF_POINT_HISTORY.ADD_POINT								," +
								"T_USER_DEF_POINT_HISTORY.ADD_POINT_DATE						," +
								"T_USER_DEF_POINT_HISTORY.REMARKS								," +
								"ROW_NUMBER()													 " +
								" OVER (" + sOrder + ") AS RNUM FROM T_USER_DEF_POINT_HISTORY	 ";

			string sWhere = string.Empty;
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.BindByName = true;
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add(":FIRST_ROW",startRowIndex);
				cmd.Parameters.Add(":LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_USER_DEF_POINT_HISTORY");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	protected OracleParameter[] CreateWhere(string pSiteCd,string pUserSeq,ref string pWhere) {
		pWhere = string.Empty;

		ArrayList list = new ArrayList();
		pWhere = pWhere + " WHERE SITE_CD = :SITE_CD ";
		list.Add(new OracleParameter(":SITE_CD",pSiteCd));

		if (!string.IsNullOrEmpty(pUserSeq)) {
			pWhere = pWhere + " AND USER_SEQ = :USER_SEQ";
			list.Add(new OracleParameter(":USER_SEQ",pUserSeq));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));

	}

	public bool IsAddedPoint(string pSiteCd,string pUserSeq,string pAddPointId) {
		DataSet ds;
		bool bAddedFlag = false;
		try {
			conn = DbConnect();

			StringBuilder oSql = new StringBuilder();
			oSql.AppendLine("SELECT	");
			oSql.AppendLine("	REVISION_NO	");
			oSql.AppendLine("FROM	");
			oSql.AppendLine("	T_USER_DEF_POINT_HISTORY	");
			oSql.AppendLine("WHERE	");
			oSql.AppendLine("	SITE_CD			= :SITE_CD		AND	");
			oSql.AppendLine("	USER_SEQ		= :USER_SEQ		AND	");
			oSql.AppendLine("	ADD_POINT_ID	= :ADD_POINT_ID	");

			using (cmd = CreateSelectCommand(oSql.ToString(),conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("ADD_POINT_ID",pAddPointId);

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					bAddedFlag = true;
				}
			}

		} finally {
			conn.Close();
		}

		return bAddedFlag;

	}

	public int GetListPageCount(string pSiteCd, string pLoginId, string pDayFrom, string pDayTo, string pSexCd, string pContainsCastSite) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_USER_DEF_POINT_HISTORY PH	,");
		oSqlBuilder.AppendLine("	T_USER U					");

		StringBuilder oWhereBuilder = new StringBuilder();
		oWhereBuilder.AppendLine("WHERE	");
		oWhereBuilder.AppendLine("	PH.USER_SEQ				= U.USER_SEQ			");

		string sWhereClause = oWhereBuilder.ToString();
		OracleParameter[] oWhereParams = this.CreateWhereList(pSiteCd, pLoginId, pDayFrom, pDayTo, pSexCd, pContainsCastSite, ref sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(), conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
				return iPageCount;
			}
		} finally {
			conn.Close();
		}
	}

	public DataSet GetListPageCollection(string pSiteCd, string pLoginId, string pDayFrom, string pDayTo, string pSexCd, string pContainsCastSite, int startRowIndex, int maximumRows) {
		StringBuilder oWhereBuilder = new StringBuilder();
		oWhereBuilder.AppendLine("WHERE	");
		oWhereBuilder.AppendLine("	PH.SITE_CD				= C.SITE_CD			(+) AND");
		oWhereBuilder.AppendLine("	PH.USER_SEQ				= C.USER_SEQ		(+) AND");
		oWhereBuilder.AppendLine("	'00'					= C.USER_CHAR_NO	(+) AND");
		oWhereBuilder.AppendLine("	PH.USER_SEQ				= U.USER_SEQ			");

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	PH.SITE_CD					,");
		oSqlBuilder.AppendLine("	PH.ADD_POINT_DATE			,");
		oSqlBuilder.AppendLine("	PH.ADD_POINT_ID				,");
		oSqlBuilder.AppendLine("	PH.ADD_POINT				,");
		oSqlBuilder.AppendLine("	PH.REMARKS					,");
		oSqlBuilder.AppendLine("	U.LOGIN_ID					,");
		oSqlBuilder.AppendLine("	C.HANDLE_NM					,");
		oSqlBuilder.AppendFormat("	{0} USER_NM	,", ViCommConst.MAN.Equals(pSexCd) ? "''" : "T_CAST.CAST_NM").AppendLine();
		oSqlBuilder.AppendLine("	U.SEX_CD					,");
		oSqlBuilder.AppendLine("	CASE WHEN U.SEX_CD = '1' THEN '男性' ELSE '女性' END SEX_NM	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_USER_DEF_POINT_HISTORY PH	,");
		oSqlBuilder.AppendLine("	T_USER U					,");
		if (ViCommConst.MAN.Equals(pSexCd)) {
			oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER C		");
		} else {
			oSqlBuilder.AppendLine("	T_CAST_CHARACTER C			,");
			oSqlBuilder.AppendLine("	T_CAST						");

			oWhereBuilder.AppendLine("AND PH.USER_SEQ			= T_CAST.USER_SEQ		");
		}

		string sWhereClause = oWhereBuilder.ToString();
		string sSortExpression = "ORDER BY PH.ADD_POINT_DATE DESC,PH.SITE_CD,PH.USER_SEQ,PH.ADD_POINT_ID";
		string sPagingSql;

		OracleParameter[] oWhereParams = this.CreateWhereList(pSiteCd, pLoginId, pDayFrom, pDayTo, pSexCd, pContainsCastSite, ref sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(), sSortExpression, startRowIndex, maximumRows, out sPagingSql);

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql, conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}

	private OracleParameter[] CreateWhereList(string pSiteCd, string pLoginId, string pDayFrom, string pDayTo, string pSexCd, string pContainsCastSite, ref string pWhere) {

		List<OracleParameter> oOracleParameterList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSiteCd)) {
			if (!string.IsNullOrEmpty(pSexCd) && !string.IsNullOrEmpty(pContainsCastSite)) {
				if (ViCommConst.OPERATOR.Equals(pSexCd) && ViCommConst.FLAG_ON_STR.Equals(pContainsCastSite)) {
					SysPrograms.SqlAppendWhere("(PH.SITE_CD = :CAST_SITE_CD OR PH.SITE_CD = :SITE_CD)", ref pWhere);
					oOracleParameterList.Add(new OracleParameter("CAST_SITE_CD", ViCommConst.CAST_SITE_CD));
					oOracleParameterList.Add(new OracleParameter("SITE_CD", pSiteCd));
				} else {
					SysPrograms.SqlAppendWhere("PH.SITE_CD = :SITE_CD", ref pWhere);
					oOracleParameterList.Add(new OracleParameter("SITE_CD", pSiteCd));
				}
			} else {
				SysPrograms.SqlAppendWhere("PH.SITE_CD = :SITE_CD", ref pWhere);
				oOracleParameterList.Add(new OracleParameter("SITE_CD", pSiteCd));
			}
		}

		if (!string.IsNullOrEmpty(pLoginId)) {
			SysPrograms.SqlAppendWhere("U.LOGIN_ID = :LOGIN_ID", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("LOGIN_ID", pLoginId));
		}

		if (!string.IsNullOrEmpty(pDayFrom) && !string.IsNullOrEmpty(pDayTo)) {
			DateTime dtFrom = DateTime.Parse(pDayFrom);
			DateTime dtTo = DateTime.Parse(pDayTo).AddDays(1);

			SysPrograms.SqlAppendWhere("(PH.ADD_POINT_DATE >= :ADD_POINT_DATE_FROM AND PH.ADD_POINT_DATE < :ADD_POINT_DATE_TO)", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("ADD_POINT_DATE_FROM", OracleDbType.Date, dtFrom, ParameterDirection.Input));
			oOracleParameterList.Add(new OracleParameter("ADD_POINT_DATE_TO", OracleDbType.Date, dtTo, ParameterDirection.Input));
		}

		if (!string.IsNullOrEmpty(pSexCd)) {
			SysPrograms.SqlAppendWhere("U.SEX_CD = :SEX_CD", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("SEX_CD", pSexCd));
		}

		return oOracleParameterList.ToArray();
	}

	public DataSet GetDailyList(string pSiteCd,string pYear,string pMonth,string pSexCd,string pContainsCastSite) {
		List<OracleParameter> oParamList = new List<OracleParameter>();
		StringBuilder oSqlBuilder = new StringBuilder();
		
		oSqlBuilder.AppendLine("SELECT																							");
		oSqlBuilder.AppendLine("	C.DAYS																					,	");
		oSqlBuilder.AppendLine("	CASE																						");
		oSqlBuilder.AppendLine("		WHEN C.DAYS > TO_CHAR(SYSDATE,'YYYY/MM/DD')												");
		oSqlBuilder.AppendLine("		THEN NULL																				");
		oSqlBuilder.AppendLine("		ELSE NVL(P.ADD_POINT,0)																	");
		oSqlBuilder.AppendLine("	END AS ADD_POINT																			");
		oSqlBuilder.AppendLine("FROM																							");
		oSqlBuilder.AppendLine("	(																							");
		oSqlBuilder.AppendLine("		SELECT																					");
		oSqlBuilder.AppendLine("			TO_CHAR(STARTDATE + ROWNUM - 1,'YYYY/MM/DD') AS DAYS								");
		oSqlBuilder.AppendLine("		FROM																					");
		oSqlBuilder.AppendLine("			(																					");
		oSqlBuilder.AppendLine("				SELECT																			");
		oSqlBuilder.AppendLine("					TRUNC(:ADD_POINT_DATE_START) AS STARTDATE								,	");
		oSqlBuilder.AppendLine("					TO_NUMBER(TO_CHAR(LAST_DAY(:ADD_POINT_DATE_START),'DD')) AS DAYS			");
		oSqlBuilder.AppendLine("				FROM																			");
		oSqlBuilder.AppendLine("					DUAL																		");
		oSqlBuilder.AppendLine("			)																				,	");
		oSqlBuilder.AppendLine("			ALL_CATALOG																			");
		oSqlBuilder.AppendLine("		WHERE																					");
		oSqlBuilder.AppendLine("			ROWNUM	<= DAYS																		");
		oSqlBuilder.AppendLine("	) C																						,	");
		oSqlBuilder.AppendLine("	(																							");
		oSqlBuilder.AppendLine("		SELECT																					");
		oSqlBuilder.AppendLine("			TO_CHAR(ADD_POINT_DATE,'YYYY/MM/DD') AS REPORT_DAY								,	");
		oSqlBuilder.AppendLine("			SUM(ADD_POINT) AS ADD_POINT															");
		oSqlBuilder.AppendLine("		FROM																					");
		oSqlBuilder.AppendLine("			T_USER_DEF_POINT_HISTORY P														,	");
		oSqlBuilder.AppendLine("			T_USER U																			");
		oSqlBuilder.AppendLine("		WHERE																					");
		oSqlBuilder.AppendLine("			P.USER_SEQ	= U.USER_SEQ														AND	");
		if (ViCommConst.OPERATOR.Equals(pSexCd) && ViCommConst.FLAG_ON_STR.Equals(pContainsCastSite)) {
			oSqlBuilder.AppendLine("		(P.SITE_CD = :CAST_SITE_CD OR P.SITE_CD = :SITE_CD)								AND	");
			oParamList.Add(new OracleParameter("CAST_SITE_CD",ViCommConst.CAST_SITE_CD));
			oParamList.Add(new OracleParameter("SITE_CD",pSiteCd));
		} else {
			oSqlBuilder.AppendLine("		P.SITE_CD	= :SITE_CD															AND	");
			oParamList.Add(new OracleParameter("SITE_CD",pSiteCd));
		}
		oSqlBuilder.AppendLine("			U.SEX_CD	= :SEX_CD															AND	");
		oSqlBuilder.AppendLine("			P.ADD_POINT_DATE BETWEEN :ADD_POINT_DATE_START AND :ADD_POINT_DATE_END				");
		oSqlBuilder.AppendLine("		GROUP BY TO_CHAR(ADD_POINT_DATE,'YYYY/MM/DD')											");
		oSqlBuilder.AppendLine("	) P																							");
		oSqlBuilder.AppendLine("WHERE																							");
		oSqlBuilder.AppendLine("	C.DAYS	= P.REPORT_DAY																	(+)	");
		
		DateTime dtReportDayStart = DateTime.Parse(string.Format("{0}/{1}/01 00:00:00",pYear,pMonth));
		DateTime dtReportDayEnd = DateTime.Parse(string.Format("{0} 23:59:59",dtReportDayStart.AddMonths(1).AddDays(-1).ToString("yyyy/MM/dd")));
		oParamList.Add(new OracleParameter(":ADD_POINT_DATE_START",OracleDbType.Date,dtReportDayStart,ParameterDirection.Input));
		oParamList.Add(new OracleParameter(":ADD_POINT_DATE_END",OracleDbType.Date,dtReportDayEnd,ParameterDirection.Input));
		oParamList.Add(new OracleParameter(":SEX_CD",pSexCd));

		DataSet oDataSet = ExecuteSelectQueryBase(oSqlBuilder.ToString(),oParamList.ToArray());
		
		return oDataSet;
	}

}
