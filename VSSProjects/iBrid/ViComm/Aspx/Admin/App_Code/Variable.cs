﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 変数

--	Progaram ID		: Variable
--
--  Creation Date	: 2010.08.16
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class Variable:DbSession {
	public Variable() {
	}

	public int GetPageCount(string pUsableType,string pVariableType,string pVariableId) {
		DataSet ds;
		DataRow dr;
		ds = new DataSet();
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_VARIABLE01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pUsableType,pVariableType,pVariableId,ref sWhere);
			sSql += sWhere;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
					if (ds.Tables[0].Rows.Count != 0) {
						dr = ds.Tables[0].Rows[0];
						iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
					}
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pUsableType,string pVariableType,string pVariableId,int startRowIndex, int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY USABLE_TYPE, VARIABLE_TYPE, VARIABLE_ID";
			string sSql = "SELECT " +
							"VARIABLE_ID			," +
							"VARIABLE_NM			," +
							"VARIABLE_SUMMARY		," +
							"VARIABLE_SUMMARY_PART	," +
							"VARIABLE_PARM			," +
							"USABLE_TYPE			," +
							"USABLE_TYPE_NM			," +
							"VARIABLE_TYPE			," +
							"VARIABLE_TYPE_NM		," +
							"UPDATE_DATE			" +
							"FROM(" +
							" SELECT VW_VARIABLE01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_VARIABLE01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pUsableType,pVariableType,pVariableId,ref sWhere);
			sSql += sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_VARIABLE01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pUsableType,string pVariableType,string pVariableId,ref string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();

		if (!string.IsNullOrEmpty(pUsableType)) {
			SysPrograms.SqlAppendWhere("USABLE_TYPE = :USABLE_TYPE",ref pWhere);
			list.Add(new OracleParameter("USABLE_TYPE",pUsableType));
		}
		if (!string.IsNullOrEmpty(pVariableType)) {
			SysPrograms.SqlAppendWhere("VARIABLE_TYPE = :VARIABLE_TYPE",ref pWhere);
			list.Add(new OracleParameter("VARIABLE_TYPE",pVariableType));
		}
		if (!string.IsNullOrEmpty(pVariableId)) {
			SysPrograms.SqlAppendWhere("VARIABLE_ID LIKE '%'||:VARIABLE_ID||'%'",ref pWhere);
			list.Add(new OracleParameter("VARIABLE_ID",pVariableId));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public DataSet GetSampleList(string pUsableType,string pVariableType) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT " +
							"VARIABLE_ID			," +
							"VARIABLE_NM			," +
							"VARIABLE_SUMMARY		," +
							"VARIABLE_SUMMARY_PART	," +
							"VARIABLE_PARM			," +
							"USABLE_TYPE			," +
							"USABLE_TYPE_NM			," +
							"VARIABLE_TYPE			," +
							"VARIABLE_TYPE_NM		" +
							"FROM VW_VARIABLE01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhereSampleList(pUsableType,pVariableType,ref sWhere);
			sSql += sWhere;

			sSql += " ORDER BY VARIABLE_TYPE, VARIABLE_ID ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_VARIABLE01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhereSampleList(string pUsableType,string pVariableType,ref string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();

		if (!string.IsNullOrEmpty(pUsableType)) {
			SysPrograms.SqlAppendWhere("USABLE_TYPE IN (:USABLE_TYPE1,:USABLE_TYPE2)",ref pWhere);
			list.Add(new OracleParameter("USABLE_TYPE1",pUsableType));
			list.Add(new OracleParameter("USABLE_TYPE2",ViCommConst.UsableSexCd.COMMON));
		}
		if (!string.IsNullOrEmpty(pVariableType)) {
			SysPrograms.SqlAppendWhere("VARIABLE_TYPE = :VARIABLE_TYPE",ref pWhere);
			list.Add(new OracleParameter("VARIABLE_TYPE",pVariableType));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public DataSet GetCSVData(string pUsableType,string pVariableType, string pVariableId) {
		DataSet ds;
		conn = DbConnect();
		ds = new DataSet();
		try {
			string sOrder = " ORDER BY USABLE_TYPE, VARIABLE_TYPE, VARIABLE_ID ";
			string sSql = "SELECT   " +
								"VARIABLE_ID        , " +
								"USABLE_TYPE        , " +
								"VARIABLE_TYPE      , " +
								"VARIABLE_NM        , " +
								"VARIABLE_SUMMARY   , " +
								"VARIABLE_PARM		, " +
								"UPDATE_DATE        , " +
								"REVISION_NO          " +
							"FROM   " +
								"T_VARIABLE ";
			string sWhere = "";
			OracleParameter[] objParams = CreateWhere(pUsableType,pVariableType,pVariableId,ref sWhere);
			sSql += sWhere + sOrder;
			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParams.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParams[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_VARIABLE");
				}
				//conn.Close();
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
}
