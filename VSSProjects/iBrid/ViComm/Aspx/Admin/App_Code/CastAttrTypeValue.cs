﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者属性種別値
--	Progaram ID		: CastAttrTypeValue
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  2009/07/09  iBrid(Y.Inoue)    データ表示用にアイテムCDを追加

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;	

using ViComm;
using iBridCommLib;

public class CastAttrTypeValue:DbSession {

	public CastAttrTypeValue() {
	}

	public int GetPageCount(string pSiteCd) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_CAST_ATTR_TYPE_VALUE01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();
			string sOrder = "ORDER BY SITE_CD,CAST_ATTR_TYPE_SEQ,CAST_ATTR_TYPE_PRIORITY,PRIORITY";

			string sSql = "SELECT " +
							"SITE_CD				," +
							"CAST_ATTR_TYPE_SEQ		," +
							"CAST_ATTR_SEQ			," +
							"CAST_ATTR_NM			," +
							"PRIORITY				," +
							"ITEM_CD				," +
							"INPUT_TYPE				," +
							"NA_FLAG				," +
							"GROUPING_CATEGORY_CD	," +
							"GROUPING_CD			," +
							"CAST_ATTR_TYPE_PRIORITY," +
							"CAST_ATTR_TYPE_NM " +
							"FROM(" +
							" SELECT VW_CAST_ATTR_TYPE_VALUE01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_CAST_ATTR_TYPE_VALUE01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_CAST_ATTR_TYPE_VALUE01");
				}
			}
		} finally {
			conn.Close();
		}


		ds.Tables[0].Columns.Add(new DataColumn("GROUPING_NM",System.Type.GetType("System.String")));

		using (CodeDtl oCodeDtl = new CodeDtl()) {
			foreach (DataRow dr in ds.Tables[0].Rows) {
				dr["GROUPING_NM"] = "";
				if ((!dr["GROUPING_CATEGORY_CD"].ToString().Equals("")) && (!dr["GROUPING_CD"].ToString().Equals(""))) {
					if (oCodeDtl.GetOne(dr["GROUPING_CATEGORY_CD"].ToString(),dr["GROUPING_CD"].ToString())) {
						dr["GROUPING_NM"] = oCodeDtl.codeNm;
					}
				}
			}
		}

		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		pWhere = pWhere + " WHERE SITE_CD = :SITE_CD AND NA_FLAG = :NA_FLAG AND INPUT_TYPE <> :INPUT_TYPE ";
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		list.Add(new OracleParameter("NA_FLAG",ViCommConst.FLAG_OFF));
		list.Add(new OracleParameter("INPUT_TYPE",ViCommConst.INPUT_TYPE_TEXT));
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public DataSet GetList(string pSiteCd,string pCastAttrTypeSeq) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT * FROM T_CAST_ATTR_TYPE_VALUE WHERE  SITE_CD =:SITE_CD AND CAST_ATTR_TYPE_SEQ = :CAST_ATTR_TYPE_SEQ "
					+ " ORDER BY SITE_CD,CAST_ATTR_TYPE_SEQ,PRIORITY";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("CAST_ATTR_TYPE_SEQ",pCastAttrTypeSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public void AppendCastAttr(DataSet pDS) {

		if (pDS.Tables[0].Rows.Count <= 0) {
			return;
		}
		
		DataTable oDataTable = pDS.Tables[0];
		for (int i = 1; i <= ViCommConst.MAX_ATTR_COUNT; i++) {
			oDataTable.Columns.Add(new DataColumn(string.Format("CAST_ATTR_VALUE{0:D2}", i), typeof(string)));
		}


		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine(" SELECT										");
		oSqlBuilder.AppendLine(" 	DISPLAY_VALUE		,                   ");
		oSqlBuilder.AppendLine(" 	ITEM_NO                                 ");
		oSqlBuilder.AppendLine(" FROM                                       ");
		oSqlBuilder.AppendLine(" 	VW_CAST_ATTR_VALUE01                    ");
		oSqlBuilder.AppendLine(" WHERE                                      ");
		oSqlBuilder.AppendLine(" 	SITE_CD			= :SITE_CD			AND ");
		oSqlBuilder.AppendLine(" 	USER_SEQ		= :USER_SEQ 		AND ");
		oSqlBuilder.AppendLine(" 	USER_CHAR_NO	= :USER_CHAR_NO         ");

		try{
			this.conn = this.DbConnect();
			foreach (DataRow dr in oDataTable.Rows) {
				for (int i = 1; i <= ViCommConst.MAX_ATTR_COUNT; i++) {
					dr[string.Format("CAST_ATTR_VALUE{0:D2}", i)] = string.Empty;
				}
				using (DataSet dsSub = new DataSet()) {
					using (cmd = CreateSelectCommand(oSqlBuilder.ToString(), conn)) {
						cmd.Parameters.Add("SITE_CD", dr["SITE_CD"].ToString());
						cmd.Parameters.Add("USER_SEQ", dr["USER_SEQ"].ToString());
						cmd.Parameters.Add("USER_CHAR_NO", dr["USER_CHAR_NO"].ToString());
						using (da = new OracleDataAdapter(cmd)) {
							da.Fill(dsSub, "VW_CAST_ATTR_VALUE01");
						}
					}
					foreach (DataRow drSub in dsSub.Tables[0].Rows) {
						dr[string.Format("CAST_ATTR_VALUE{0}", drSub["ITEM_NO"].ToString())] = drSub["DISPLAY_VALUE"].ToString();
					}
				}
			}
		}finally{
			this.conn.Close();
		}
	}
}
