﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 男性会員属性種別
--	Progaram ID		: UserManAttrTypeValue
--
--  Creation Date	: 2009.07.09
--  Creater			: iBrid(Y.Inoue)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using ViComm;
using iBridCommLib;

public class UserManAttrTypeValue:DbSession {

	public UserManAttrTypeValue() {
	}

	public int GetPageCount(string pSiteCd) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_USER_MAN_ATTR_TYPE_VALUE01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}

		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();
			string sOrder = "ORDER BY SITE_CD,MAN_ATTR_TYPE_SEQ,MAN_ATTR_TYPE_PRIORITY,PRIORITY";

			string sSql = "SELECT " +
							"SITE_CD				," +
							"MAN_ATTR_TYPE_SEQ		," +
							"MAN_ATTR_SEQ			," +
							"MAN_ATTR_NM			," +
							"PRIORITY				," +
							"ITEM_CD				," +
							"INPUT_TYPE				," +
							"GROUPING_CATEGORY_CD	," +
							"GROUPING_CD			," +
							"NA_FLAG				," +
							"MAN_ATTR_TYPE_PRIORITY," +
							"MAN_ATTR_TYPE_NM " +
							"FROM(" +
							" SELECT VW_USER_MAN_ATTR_TYPE_VALUE01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_USER_MAN_ATTR_TYPE_VALUE01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USER_MAN_ATTR_TYPE_VALUE01");
				}
			}
		} finally {
			conn.Close();
		}

		ds.Tables[0].Columns.Add(new DataColumn("GROUPING_NM",System.Type.GetType("System.String")));

		using (CodeDtl oCodeDtl = new CodeDtl()) {
			foreach (DataRow dr in ds.Tables[0].Rows) {
				dr["GROUPING_NM"] = "";
				if ((!dr["GROUPING_CATEGORY_CD"].ToString().Equals("")) && (!dr["GROUPING_CD"].ToString().Equals(""))) {
					if (oCodeDtl.GetOne(dr["GROUPING_CATEGORY_CD"].ToString(),dr["GROUPING_CD"].ToString())) {
						dr["GROUPING_NM"] = oCodeDtl.codeNm;
					}
				}
			}
		}

		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		pWhere = pWhere + " WHERE SITE_CD = :SITE_CD AND NA_FLAG = :NA_FLAG AND INPUT_TYPE <> :INPUT_TYPE ";
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		list.Add(new OracleParameter("NA_FLAG",ViCommConst.FLAG_OFF));
		list.Add(new OracleParameter("INPUT_TYPE",ViCommConst.INPUT_TYPE_TEXT));
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public DataSet GetList(string pSiteCd,string pUserManAttrTypeSeq) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT * FROM T_MAN_ATTR_TYPE_VALUE WHERE  SITE_CD =:SITE_CD AND MAN_ATTR_TYPE_SEQ = :MAN_ATTR_TYPE_SEQ "
					+ " ORDER BY SITE_CD,MAN_ATTR_TYPE_SEQ,PRIORITY";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("MAN_ATTR_TYPE_SEQ",pUserManAttrTypeSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

}
