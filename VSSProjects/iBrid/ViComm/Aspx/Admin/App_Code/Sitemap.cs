﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: サイトマップ

--	Progaram ID		: Urge
--
--  Creation Date	: 2010.08.03
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;
using iBridCommLib;
using ViComm;

public class Sitemap {
	readonly string menuTile;

	public Sitemap(string pMenuTitle) {
		menuTile = pMenuTitle;
	}
}

public class SitemapList:DbSession {

	Hashtable items;

	public SitemapList(string pAdminType) {
		items = new Hashtable();
		Sitemap oItem;

		using (DataSet ds = GetList(pAdminType)) {
			foreach (DataRow dr in ds.Tables[0].Rows) {
				oItem = new Sitemap(dr["MENU_TITLE"].ToString());
				items.Add(dr["MENU_TITLE"].ToString(),oItem);
			}
		}

	}

	private DataSet GetList(string pAdminType) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT * FROM T_SITEMAP WHERE ADMIN_TYPE = :ADMIN_TYPE ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("ADMIN_TYPE",pAdminType);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public bool IsAvailableMenu(string pMenuTitle) {
		Sitemap oItem = (Sitemap)items[pMenuTitle];
		return (oItem != null);
	}
}