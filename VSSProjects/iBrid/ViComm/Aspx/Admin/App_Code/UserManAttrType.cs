﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 男性会員属性種別
--	Progaram ID		: UserManAttrType
--
--  Creation Date	: 2009.07.09
--  Creater			: iBrid(Y.Inoue)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using ViComm;
using iBridCommLib;

public class UserManAttrType:DbSession {

	public decimal manAttrTypeSeq;
	public string manAttrTypeNm;
	public int priority;
	public string inputType;
	public bool naFlag;
	public string groupingCategoryCd;
	public string itemNo;
	public bool profileReqItemFlag;
	public int rowCount;

	public UserManAttrType() {
	}

	public int GetPageCount(string pSiteCd) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_USER_MAN_ATTR_TYPE01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();
			string sOrder = "ORDER BY SITE_CD,PRIORITY";

			string sSql =   "SELECT " +
							"SITE_CD			    ," +
							"MAN_ATTR_TYPE_SEQ	    ," +
							"MAN_ATTR_TYPE_NM	    ," +
							"MAN_ATTR_TYPE_FIND_NM	," +
							"PRIORITY			    ," +
							"INPUT_TYPE			    ," +
							"NA_FLAG			    ," +
							"ITEM_NO			    ," +
							"PROFILE_REQ_ITEM_FLAG	," +
							"NA_MARK                ," +
							"PROFILE_REQ_ITEM_MARK	," +
							"REGIST_INPUT_FLAG		," +
							"CODE_NM				" +
							"FROM(" +
							" SELECT VW_USER_MAN_ATTR_TYPE01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_USER_MAN_ATTR_TYPE01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql += sWhere;

			sSql += ") ATTR, T_CODE_DTL " +
					"WHERE ATTR.INPUT_TYPE = T_CODE_DTL.CODE(+) " +
					"AND   '47'            = T_CODE_DTL.CODE_TYPE " +
					"AND   RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";

			sSql += sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds, "VW_USER_MAN_ATTR_TYPE01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		pWhere = pWhere + " WHERE  SITE_CD = :SITE_CD";
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public DataSet GetList(string pSiteCd) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT MAN_ATTR_TYPE_SEQ,MAN_ATTR_TYPE_NM,INPUT_TYPE FROM T_MAN_ATTR_TYPE " +
								" WHERE SITE_CD = :SITE_CD AND NA_FLAG = 0 AND INPUT_TYPE <> :INPUT_TYPE " +
								" ORDER BY SITE_CD,PRIORITY ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("INPUT_TYPE",ViCommConst.INPUT_TYPE_TEXT);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds, "T_MAN_ATTR_TYPE");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public bool GetOne(string pSiteCd,string pAttrTypeSeq) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;
		try{
			conn = DbConnect();

			string sSql = "SELECT " +
							"MAN_ATTR_TYPE_SEQ		," +
							"MAN_ATTR_TYPE_NM		," +
							"PRIORITY				," +
							"INPUT_TYPE				," +
							"NA_FLAG				," +
							"GROUPING_CATEGORY_CD	," +
							"ITEM_NO				," +
							"PROFILE_REQ_ITEM_FLAG	," +
							"ROW_COUNT				 " +
						"FROM " +
							"T_MAN_ATTR_TYPE " +
						"WHERE " +
							"SITE_CD			= :SITE_CD	AND " +
							"MAN_ATTR_TYPE_SEQ	= :MAN_ATTR_TYPE_SEQ	";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("MAN_ATTR_TYPE_SEQ",pAttrTypeSeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MAN_ATTR_TYPE");

					if (ds.Tables["T_MAN_ATTR_TYPE"].Rows.Count != 0) {
						dr = ds.Tables["T_MAN_ATTR_TYPE"].Rows[0];
						manAttrTypeSeq = decimal.Parse(dr["MAN_ATTR_TYPE_SEQ"].ToString());
						manAttrTypeNm = dr["MAN_ATTR_TYPE_NM"].ToString();
						priority = int.Parse(dr["PRIORITY"].ToString());
						inputType = dr["INPUT_TYPE"].ToString();
						if (dr["NA_FLAG"].ToString().Equals("1")) {
							naFlag = true;
						} else {
							naFlag = false;
						}
						groupingCategoryCd = dr["GROUPING_CATEGORY_CD"].ToString();
						itemNo = dr["ITEM_NO"].ToString();
						if (dr["PROFILE_REQ_ITEM_FLAG"].ToString().Equals("1")) {
							profileReqItemFlag = true;
						} else {
							profileReqItemFlag = false;
						}
						if (string.IsNullOrEmpty(dr["ROW_COUNT"].ToString())) {
							rowCount = 0;
						} else {
							rowCount = int.Parse(dr["ROW_COUNT"].ToString());
						}
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public DataSet GetInqAdminManAttrType(string pSiteCd) {
		string sSql = "SELECT " +
						"MAN_ATTR_TYPE_SEQ1			," +
						"MAN_ATTR_TYPE_SEQ2			," +
						"MAN_ATTR_TYPE_SEQ3			," +
						"MAN_ATTR_TYPE_SEQ4			," +
						"MAN_ATTR_TYPE_NM1			," +
						"MAN_ATTR_TYPE_NM2	  		," +
						"MAN_ATTR_TYPE_NM3	  		," +
						"MAN_ATTR_TYPE_NM4	  		," +
						"GROUPING_CATEGORY_CD1		," +
						"GROUPING_CATEGORY_CD2		," +
						"GROUPING_CATEGORY_CD3		," +
						"GROUPING_CATEGORY_CD4		" +
					  "FROM	" +
						"VW_USER_MAN_ATTR_TYPE03	 " +
					  "WHERE " +
						"SITE_CD =: SITE_CD";

		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
}
