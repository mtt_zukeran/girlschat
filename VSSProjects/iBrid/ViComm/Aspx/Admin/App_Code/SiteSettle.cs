﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Settle
--	Title			: サイト別決済方法
--	Progaram ID		: SiteSettle
--
--  Creation Date	: 2008.11.06
--  Creater			: i-Brid(S.Ohtahara)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data;
using System.Data.Common;
using System.Text;

public class SiteSettle:DbSession {


	public string cpIdNo;
	public string cpPassword;
	public string settleUrl;
	public string continueSettleUrl;
	public string backUrl;

	public SiteSettle() {
		cpIdNo = "";
		cpPassword = "";
		settleUrl = "";
		continueSettleUrl = "";
		backUrl = "";
	}

	public bool GetOne(string pSiteCd,string pSettleCompanyCd,string pSettleType) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try{
			conn = DbConnect("SiteSettle.GetOne");

			using (cmd = CreateSelectCommand("SELECT CP_ID_NO,CP_PASSWORD,SETTLE_URL,CONTINUE_SETTLE_URL,BACK_URL FROM T_SITE_SETTLE WHERE SITE_CD = :SITE_CD AND SETTLE_COMPANY_CD =:SETTLE_COMPANY_CD AND SETTLE_TYPE =:SETTLE_TYPE  ",conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("SETTLE_COMPANY_CD",pSettleCompanyCd);
				cmd.Parameters.Add("SETTLE_TYPE",pSettleType);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_SETTLE_COMPANY");
					if (ds.Tables["T_SETTLE_COMPANY"].Rows.Count != 0) {
						dr = ds.Tables["T_SETTLE_COMPANY"].Rows[0];
						cpIdNo = dr["CP_ID_NO"].ToString();
						cpPassword = dr["CP_PASSWORD"].ToString();
						settleUrl = dr["SETTLE_URL"].ToString();
						continueSettleUrl = dr["CONTINUE_SETTLE_URL"].ToString();
						backUrl = dr["BACK_URL"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public DataSet GetPageCollection(string pSiteCd,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT " +
							"SITE_CD			," +
							"SITE_NM			," +
							"SETTLE_COMPANY_CD  ," +
							"SETTLE_COMPANY_NM	," +
							"SETTLE_TYPE        ," +
							"SETTLE_TYPE_NM		," +
							"UPDATE_DATE		" +
						  "FROM " +
							"VW_SITE_SETTLE01 " +
							" WHERE SITE_CD = :SITE_CD " +
							"ORDER BY SETTLE_COMPANY_CD";

			using (cmd = CreateSelectCommand(sSql,conn)) {

				using (da = new OracleDataAdapter(cmd)) {
					cmd.Parameters.Add("SITE_CD",pSiteCd);
					da.Fill(ds,"VW_SITE_SETTLE01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}



	public int GetPageCount(string pSiteCd) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_SITE_SETTLE  WHERE SITE_CD = :SITE_CD ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public bool MailTemplateExistance(string pSiteCd) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_MAIL_TEMPLATE WHERE SITE_CD = :SITE_CD";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}

		if (iPageCount > 0) {
			return true;
		} else
			return false;

	}

	public DataSet GetList(string pSiteCd, string pSettleType) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT SETTLE_COMPANY_CD,SETTLE_COMPANY_NM ");
			sSql.Append("FROM VW_SITE_SETTLE01 ");
			sSql.Append("WHERE SITE_CD = :SITE_CD ");
			if (!string.IsNullOrEmpty(pSettleType)) {
				sSql.Append("AND SETTLE_TYPE = :SETTLE_TYPE ");
			}
			sSql.Append("ORDER BY SETTLE_COMPANY_CD ");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) 
			using (da = new OracleDataAdapter(cmd)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				if (!string.IsNullOrEmpty(pSettleType)) {
					cmd.Parameters.Add("SETTLE_TYPE",pSettleType);
				}
				da.Fill(ds,"VW_SITE_SETTLE01");
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

}
