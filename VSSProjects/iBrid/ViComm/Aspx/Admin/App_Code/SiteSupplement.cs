﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: サイト補足区分
--	Progaram ID		: SiteSupplement
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class SiteSupplement:DbSession {

	public string supplementValue;
	
	public SiteSupplement() {
		supplementValue = "";
	}

	public int GetPageCount(string pSiteCd) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_SITE_SUPPLEMENT01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY SITE_CD,SUPPLEMENT_CD";
			string sSql = "SELECT " +
							"SITE_CD		," +
							"SUPPLEMENT_CD	," +
							"SUPPLEMENT_NM	," +
							"SUPPLEMENT_VALUE " +
							"FROM(" +
							" SELECT VW_SITE_SUPPLEMENT01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_SITE_SUPPLEMENT01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_SITE_SUPPLEMENT01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		pWhere = pWhere + " WHERE SITE_CD = :SITE_CD";
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public bool GetOne(string pSiteCd,string pSupplementCd) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try{
			conn = DbConnect();
			string sSql = "SELECT " +
								"SUPPLEMENT_VALUE " +
							"FROM " +
								"T_SITE_SUPPLEMENT " +
							"WHERE " +
								"SITE_CD		= :SITE_CD	AND " +
								"SUPPLEMENT_CD	= :SUPPLEMENT_CD ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {

				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("SUPPLEMENT_CD",pSupplementCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"DS_SITE_SUPPLEMENT");
					if (ds.Tables["DS_SITE_SUPPLEMENT"].Rows.Count != 0) {
						dr = ds.Tables["DS_SITE_SUPPLEMENT"].Rows[0];
						supplementValue = dr["SUPPLEMENT_VALUE"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}
}
