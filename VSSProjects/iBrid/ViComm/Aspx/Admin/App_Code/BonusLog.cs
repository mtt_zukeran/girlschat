﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ボーナス履歴

--	Progaram ID		: BonusLog
--
--  Creation Date	: 2009.10.14
--  Creater			: iBrid(sohtahara)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class BonusLog:DbSession {
	public BonusLog() {
	}

	public int GetPageCount(string pUserSeq,string pReportDayFrom,string pReportDayTo) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			DateTime dtFrom = DateTime.Parse(pReportDayFrom);
			DateTime dtTo = DateTime.Parse(pReportDayTo).AddDays(1);

			string sSql = " SELECT COUNT(*) AS ROW_COUNT FROM T_BONUS_LOG ";
			sSql += " WHERE (USER_SEQ  = :USER_SEQ) ";
			sSql += " AND (CREATE_DATE >= :CREATE_DATE_FROM AND CREATE_DATE < :CREATE_DATE_TO)";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("CREATE_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input);
				cmd.Parameters.Add("CREATE_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input);
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pUserSeq,string pReportDayFrom,string pReportDayTo,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = " ORDER BY CREATE_DATE DESC";
			string sSql = " SELECT " +
							" USER_SEQ       , " +
							" BONUS_SEQ      , " +
							" CREATE_DATE    , " +
							" BONUS_POINT    , " +
							" REMARKS        , " +
							" CASE WHEN PAYMENT_FLAG != 1 THEN '未精算' ELSE '精算済み' END AS PAYMENT_FLAG " +
							" FROM(            " +
							" SELECT T_BONUS_LOG.*, ROW_NUMBER() OVER ( " + sOrder +
							" ) AS RNUM FROM T_BONUS_LOG WHERE (USER_SEQ = :USER_SEQ) " +
							" AND (CREATE_DATE >= :CREATE_DATE_FROM AND CREATE_DATE < :CREATE_DATE_TO)";

			sSql = sSql + " ) WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW";

			sSql = sSql + sOrder;

			DateTime dtFrom = DateTime.Parse(pReportDayFrom);
			DateTime dtTo = DateTime.Parse(pReportDayTo).AddDays(1);

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("CREATE_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input);
				cmd.Parameters.Add("CREATE_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input);
				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_BONUS_LOG");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;

	}

	public void CancelBonusLog(string pBonusSeq) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CANCEL_BONUS_LOG");
			db.ProcedureInParm("PBONUS_SEQ",DbSession.DbType.VARCHAR2,pBonusSeq);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}
