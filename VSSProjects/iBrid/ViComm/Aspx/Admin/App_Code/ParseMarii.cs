﻿using System;
using System.IO;
using System.Web;
using System.Net;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Collections.Generic;
using iBridCommLib;
using MobileLib;
using ViComm;


public class ParseViComm:MobileLib.ParseMobile {

	static Dictionary<string,int> dispatchFunc;
	private SessionObjs userObjs;

	public ParseViComm(string pPattern,RegexOptions pOption,string pCarrierCd,SessionObjs pSessionObjs)
		: base(pPattern,false,"","","",pOption,pCarrierCd) {
		userObjs = pSessionObjs;
		if (dispatchFunc == null) {
			dispatchFunc = new Dictionary<string,int>();
			dispatchFunc.Add("$HR",1);
			dispatchFunc.Add("$FOOTER",2);
			dispatchFunc.Add("$IMG_PATH",3);
			dispatchFunc.Add("$CLEAR_LEFT",4);
			dispatchFunc.Add("$BUTTON",5);
		}
	}

	public override byte[] Perser(string pTag,string pArgument) {
		string sValue = "";

		if (pTag.StartsWith("$x")) {
			return ConvertEmoji(pTag);
		} else {

			pTag = pTag.Replace(";","");

			Encoding encSjis = Encoding.GetEncoding(932); // shift-jis
			if (dispatchFunc.ContainsKey(pTag)) {
				int iFuncNo = dispatchFunc[pTag];
				switch (pTag) {
					case "$HR":
						sValue = Mobile.HrTag(userObjs.site.sizeLine,userObjs.site.colorLine);
						break;
					case "$WIDTH":
						sValue = "width";
						break;
					case "$FOOTER":
						sValue = parseContainer.Parse(userObjs.site.footerHtmlDoc);
						break;
					case "$IMG_PATH":
						sValue = "../Image/" + userObjs.site.siteCd + "/";
						break;
					case "$CLEAR_LEFT":
						sValue = "<br clear=\"left\" />";
						break;
					case "$BUTTON":
						sValue = GetButtonTag(pArgument);
						break;
				}

			} else {
				string sTag = pTag.ToUpper();
				if (sTag.StartsWith("$HREF_")) {
					sValue = ParseHRef(pTag);
				}
			}
			// Originalタグ変換
			byte[] byteArray = encSjis.GetBytes(sValue);
			return byteArray;
		}
	}


	private string ParseHRef(string pTag) {
		string sHRef = "",sText = "";
		Regex rgxHRef = new Regex(@"^\$\w{1,}");
		Match rgxHRefMatch = rgxHRef.Match(pTag);
		if (rgxHRefMatch.Success) {
			sHRef = rgxHRefMatch.Value;
		}

		Regex rgxA = new Regex(@"\(.+?\);");
		Match rgxAMatch = rgxA.Match(pTag);
		if (rgxAMatch.Success) {
			sText = rgxAMatch.Value;
			sText = sText.Replace("(","");
			sText = sText.Replace(");","");
			sText = parseContainer.Parse(sText);
		}
		return string.Format("<a href=\"\">{0}</a>",sText);
	}

	private string GetButtonTag(string pArgument) {
		string sText = "";
		string sName = "";
		string[] sValues = pArgument.Split(',');
		sText = sValues[0];
		return string.Format("<input value=\"{0}\" type=\"submit\" name=\"{1}\" />",sText,sName);
	}
}
