﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 共同ライブ売上明細
--	Progaram ID		: ShareLiveSalesDtl
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class ShareLiveSalesDtl:DbSession {

	public ShareLiveSalesDtl() {
	}

	public int GetPageCount(string pShareLiveTalkKey,string pLoginId,string pIvpSiteNm,string pReportDayFrom,string pReportDayTo) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_SHARE_LIVE_SALES_DTL01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pShareLiveTalkKey,pLoginId,pIvpSiteNm,pReportDayFrom,pReportDayTo,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pShareLiveTalkKey,string pLoginId,string pIvpSiteNm,string pReportDayFrom,string pReportDayTo,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY USER_SEQ,SHARE_LIVE_TALK_KEY,IVP_SITE_NM,CHARGE_SEQ ";
			string sSql = "SELECT " +
							"LOGIN_ID			," +
							"USER_SEQ			," +
							"SHARE_LIVE_TALK_KEY," +
							"CHARGE_SEQ			," +
							"CHARGE_START_DATE	," +
							"CHARGE_END_DATE	," +
							"CHARGE_SEC			," +
							"IVP_SITE_NM		," +
							"IVP_ACCEPT_SEQ		," +
							"CAST_NM			," +
							"LIVE_NM			," +
							"START_DATE			," +
							"END_DATE			" +
						"FROM(" +
							" SELECT VW_SHARE_LIVE_SALES_DTL01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_SHARE_LIVE_SALES_DTL01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pShareLiveTalkKey,pLoginId,pIvpSiteNm,pReportDayFrom,pReportDayTo,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_SHARE_LIVE_SALES_DTL01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(string pShareLiveTalkKey,string pLoginId,string pIvpSiteNm,string pReportDayFrom,string pReportDayTo,ref string pWhere) {
		ArrayList list = new ArrayList();

		if (!pShareLiveTalkKey.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("SHARE_LIVE_TALK_KEY = :SHARE_LIVE_TALK_KEY",ref pWhere);
			list.Add(new OracleParameter("SHARE_LIVE_TALK_KEY",pShareLiveTalkKey));
		}

		if (!pLoginId.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("LOGIN_ID LIKE :LOGIN_ID||'%'",ref pWhere);
			list.Add(new OracleParameter("LOGIN_ID",pLoginId ));
		}

		if (!pIvpSiteNm.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("IVP_SITE_NM LIKE :IVP_SITE_NM||'%'",ref pWhere);
			list.Add(new OracleParameter("IVP_SITE_NM",pIvpSiteNm ));
		}

		if ((!pReportDayFrom.Equals("")) && (!pReportDayTo.Equals(""))) {
			DateTime dtFrom = System.DateTime.ParseExact(pReportDayFrom + " 00:00:00","yyyy/MM/dd HH:mm:ss",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);
			DateTime dtTo = System.DateTime.ParseExact(pReportDayTo + " 00:00:00","yyyy/MM/dd HH:mm:ss",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);
			iBridCommLib.SysPrograms.SqlAppendWhere("CHARGE_START_DATE >= :CHARGE_START_DATE_FROM AND CHARGE_START_DATE < :CHARGE_START_DATE_TO",ref pWhere);
			list.Add(new OracleParameter("CHARGE_START_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("CHARGE_START_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

}
