/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者カテゴリー
--	Progaram ID		: ActCategory
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class ActCategory:DbSession {

	public string colorBack;
	public string colorChar;
	public string colorLink;

	public ActCategory() {
	}

	public int GetPageCount(string pSiteCd) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_ACT_CATEGORY01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();
			string sOrder = "ORDER BY SITE_CD,PRIORITY";
			string sSql = "SELECT " +
							"SITE_CD				," +
							"SITE_NM				," +
							"ACT_CATEGORY_SEQ		," +
							"ACT_CATEGORY_NM		," +
							"PRIORITY				," +
							"DISPLAY_FLAG			," +
							"HTML_DOC				," +
							"ACT_COUNT				," +
							"NA_FLAG				," +
							"DISPLAY_MARK			," +
							"NA_MARK " +
							"FROM(" +
							" SELECT VW_ACT_CATEGORY02.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_ACT_CATEGORY02  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_ACT_CATEGORY02");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		pWhere = pWhere + " WHERE SITE_CD = :SITE_CD AND ACT_CATEGORY_SEQ != :ACT_CATEGORY_SEQ ";
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		list.Add(new OracleParameter("ACT_CATEGORY_SEQ",ViComm.ViCommConst.DEFAULT_ACT_CATEGORY_SEQ));
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}


	public DataSet GetList(string pSiteCd) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT * FROM VW_ACT_CATEGORY01 WHERE SITE_CD = :SITE_CD AND NA_FLAG = 0 AND ACT_CATEGORY_SEQ != :ACT_CATEGORY_SEQ";
			 
			sSql = sSql + " ORDER BY SITE_CD,PRIORITY";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("ACT_CATEGORY_SEQ",ViComm.ViCommConst.DEFAULT_ACT_CATEGORY_SEQ);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetListIncDefault(string pSiteCd) {
		DataSet ds;

		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT * FROM VW_ACT_CATEGORY01 WHERE SITE_CD = :SITE_CD AND NA_FLAG = 0 ";

			sSql = sSql + " ORDER BY SITE_CD,PRIORITY";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public bool GetOne(string pSiteCd,string pCategorySeq) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;
		try{
			conn = DbConnect();

			string sSql = "SELECT " +
							"COLOR_CHAR			," +
							"COLOR_BACK			," +
							"COLOR_LINK			," +
							"ACT_CATEGORY_NM	 " +
						"FROM " +
							"VW_ACT_CATEGORY01 " +
						"WHERE " +
							"SITE_CD = :SITE_CD AND ACT_CATEGORY_SEQ = :ACT_CATEGORY_SEQ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("ACT_CATEGORY_SEQ",pCategorySeq);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_ACT_CATEGORY");

					if (ds.Tables["T_ACT_CATEGORY"].Rows.Count != 0) {
						dr = ds.Tables["T_ACT_CATEGORY"].Rows[0];
						colorBack = dr["COLOR_BACK"].ToString();
						colorChar = dr["COLOR_CHAR"].ToString();
						colorLink = dr["COLOR_LINK"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}
}
