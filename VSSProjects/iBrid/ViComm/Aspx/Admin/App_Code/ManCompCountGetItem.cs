﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 男性用お宝規定回数ｺﾝﾌﾟﾘｰﾄ時獲得ｱｲﾃﾑ--	Progaram ID		: ManCompCountGetItem
--
--  Creation Date	: 2011.07.25
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;


public class ManCompCountGetItem : DbSession {

	public ManCompCountGetItem() {
	}

	public int GetPageCount(string pSiteCd,string pDisplayMonth) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_MAN_COMP_COUNT_GET_ITEM		MCCGI	,");
		oSqlBuilder.AppendLine("	T_GAME_ITEM						GI	");

		string sWhereClause;
		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pDisplayMonth,out sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pDisplayMonth,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	MCCGI.SITE_CD					,");
		oSqlBuilder.AppendLine("	MCCGI.DISPLAY_MONTH				,");
		oSqlBuilder.AppendLine("	MCCGI.COMP_COUNT_GET_ITEM_SEQ	,");
		oSqlBuilder.AppendLine("	MCCGI.COMPLETE_COUNT			,");
		oSqlBuilder.AppendLine("	MCCGI.GAME_ITEM_SEQ				,");
		oSqlBuilder.AppendLine("	GI.GAME_ITEM_NM					,");
		oSqlBuilder.AppendLine("	MCCGI.ITEM_COUNT				,");
		oSqlBuilder.AppendLine("	MCCGI.REVISION_NO				,");
		oSqlBuilder.AppendLine("	MCCGI.UPDATE_DATE				");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_MAN_COMP_COUNT_GET_ITEM		MCCGI	,");
		oSqlBuilder.AppendLine("	T_GAME_ITEM						GI	");

		string sWhereClause;
		string sPagingSql;
		string sSortExpression = "ORDER BY MCCGI.SITE_CD, MCCGI.DISPLAY_MONTH DESC, MCCGI.GAME_ITEM_SEQ";

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pDisplayMonth,out sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		} finally {
			conn.Close();
		}
		return oDs;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pDisplayMonth,out string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();

		SysPrograms.SqlAppendWhere("MCCGI.SITE_CD = GI.SITE_CD AND MCCGI.GAME_ITEM_SEQ = GI.GAME_ITEM_SEQ",ref pWhere);

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("MCCGI.SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}
		if (!string.IsNullOrEmpty(pDisplayMonth)) {
			SysPrograms.SqlAppendWhere("MCCGI.DISPLAY_MONTH = :DISPLAY_MONTH",ref pWhere);
			list.Add(new OracleParameter("DISPLAY_MONTH",pDisplayMonth));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public bool IsDupulicateItem(string pSiteCd,string pDisplayMonth,string pGameItemSeq,string pCompleteCount) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_MAN_COMP_COUNT_GET_ITEM	");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	SITE_CD			= :SITE_CD				AND");
		oSqlBuilder.AppendLine("	DISPLAY_MONTH	= :DISPLAY_MONTH		AND");
		oSqlBuilder.AppendLine("	GAME_ITEM_SEQ	= :GAME_ITEM_SEQ		AND");
		oSqlBuilder.AppendLine("	COMPLETE_COUNT	= :COMPLETE_COUNT		");

		try {
			int iCount = 0;
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add(new OracleParameter("SITE_CD",pSiteCd));
				cmd.Parameters.Add(new OracleParameter("DISPLAY_MONTH",pDisplayMonth));
				cmd.Parameters.Add(new OracleParameter("GAME_ITEM_SEQ",pGameItemSeq));
				cmd.Parameters.Add(new OracleParameter("COMPLETE_COUNT",pCompleteCount));
				iCount = Convert.ToInt32(cmd.ExecuteScalar());
				return iCount > 0;
			}
		} finally {
			conn.Close();
		}
	}
}
