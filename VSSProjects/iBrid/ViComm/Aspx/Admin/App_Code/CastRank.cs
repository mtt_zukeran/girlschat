/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: キャストランク
--	Progaram ID		: CastRank
--
--  Creation Date	: 2010.08.05
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using System.Text;

public class CastRank : DbSession {

	public CastRank() {
	}

	public int GetPageCount() {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_CAST_RANK ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql, conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0; i < objParms.Length; i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(int startRowIndex, int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY USER_RANK ";
			string sSql = "SELECT " +
							"USER_RANK				," +
							"USER_RANK_NM			," +
							"INTRO_POINT_RATE		," +
							"UPDATE_DATE			," +
							"BINGO_BALL_RATE		" +
							"FROM(" +
							" SELECT T_CAST_RANK.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM T_CAST_RANK ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql, conn)) {
				for (int i = 0; i < objParms.Length; i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW", startRowIndex);
				cmd.Parameters.Add("LAST_ROW", startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds, "T_CAST_RANK");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(ref string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public DataSet GetList() {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY USER_RANK ";
			string sSql = "SELECT " +
							"USER_RANK		," +
							"USER_RANK_NM " +
							"FROM " +
							"T_CAST_RANK ";

			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql, conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds, "T_CAST_RANK");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public int? GetPointPrice(string pUserRank) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	POINT_PRICE");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_CAST_RANK");
		oSqlBuilder.AppendLine("WHERE");
		oSqlBuilder.AppendLine("	USER_RANK	= :USER_RANK");

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(), conn)) {
				cmd.Parameters.Add(new OracleParameter("USER_RANK", pUserRank));
				object oPointPrice = cmd.ExecuteScalar();

				int iPointPrice;
				if (int.TryParse(iBridUtil.GetStringValue(oPointPrice), out iPointPrice)) {
					return iPointPrice;
				} else {
					return null;
				}

				//return cmd.ExecuteScalar() ?? null;
			}
		} finally {
			conn.Clone();
		}
	}
}