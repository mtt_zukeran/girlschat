﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 商品ﾋﾟｯｸｱｯﾌﾟ管理
--	Progaram ID		: ProductPickupManage
--
--  Creation Date	: 2010.12.15
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;

public class ProductPickupManage : DbSession {
	public ProductPickupManage() { }
	
	public DataSet GetList(string pSiteCd, string pProductType){
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine(" SELECT																");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_PICKUP_MANAGE.SITE_CD			,                       ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_PICKUP_MANAGE.PRODUCT_TYPE   	,                       ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_PICKUP_MANAGE.PICKUP_ID      	,                       ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_PICKUP_MANAGE.PICKUP_TITLE   	,                       ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_PICKUP_MANAGE.PICKUP_MASK                             ");
		oSqlBuilder.AppendLine(" FROM                                                               ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_PICKUP_MANAGE                                         ");
		oSqlBuilder.AppendLine(" WHERE                                                              ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_PICKUP_MANAGE.SITE_CD 		= :SITE_CD		AND     ");
		oSqlBuilder.AppendLine(" 	T_PRODUCT_PICKUP_MANAGE.PRODUCT_TYPE 	= :PRODUCT_TYPE	        ");
		
		DataSet oDataSet = new DataSet();
		try{
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD", pSiteCd);
				cmd.Parameters.Add(":PRODUCT_TYPE", pProductType);
				using(da = new OracleDataAdapter(cmd)){
					da.Fill(oDataSet);
				}
			}
		}finally{
			conn.Close();
		}	
		return oDataSet;
	}
}
