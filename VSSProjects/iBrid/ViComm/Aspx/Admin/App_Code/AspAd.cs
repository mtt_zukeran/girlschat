/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ASP広告コード
--	Progaram ID		: AspAd
--
--  Creation Date	: 2010.09.24
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class AspAd:DbSession {

	public AspAd() {
	}

	public int GetPageCount(string pSiteCd,string pSettleCompanyCd,string pAdCd,string pAdNm,string pAffiliatePointFrom,string pAffiliatePointTo,string pWomanAffiliatePointFrom,string pWomanAffiliatePointTo,string pApplyStartDayFrom,string pApplyStartDayTo,string pMonthlyFeeFrom,string pMonthlyFeeTo,string pApplyDocomoFlag,string pApplySoftbankFlag,string pApplyAuFlag,string pApplyAndroidFlag,string pApplyIphoneFlag,string pApplySexCd,string pAspAdStatus) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT COUNT(*) AS ROW_COUNT FROM VW_ASP_AD01 ");

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pSettleCompanyCd,pAdCd,pAdNm,pAffiliatePointFrom,pAffiliatePointTo,pWomanAffiliatePointFrom,pWomanAffiliatePointTo,pApplyStartDayFrom,pApplyStartDayTo,pMonthlyFeeFrom,pMonthlyFeeTo,pApplyDocomoFlag,pApplySoftbankFlag,pApplyAuFlag,pApplyAndroidFlag,pApplyIphoneFlag,pApplySexCd,pAspAdStatus,ref sWhere);

			sSql.Append(sWhere);

			using (cmd = CreateSelectCommand(sSql.ToString(),conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pSettleCompanyCd,string pAdCd,string pAdNm,string pAffiliatePointFrom,string pAffiliatePointTo,string pWomanAffiliatePointFrom,string pWomanAffiliatePointTo,string pApplyStartDayFrom,string pApplyStartDayTo,string pMonthlyFeeFrom,string pMonthlyFeeTo,string pApplyDocomoFlag,string pApplySoftbankFlag,string pApplyAuFlag,string pApplyAndroidFlag,string pApplyIphoneFlag,string pApplySexCd,string pAspAdStatus,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			StringBuilder sSql = new StringBuilder();
			string sOrder = "ORDER BY SETTLE_COMPANY_CD, ASP_AD_CD ";

			sSql.Append("SELECT ");
			sSql.Append("	SITE_CD					, ");
			sSql.Append("	SETTLE_COMPANY_CD		, ");
			sSql.Append("	SETTLE_TYPE				, ");
			sSql.Append("	ASP_AD_CD				, ");
			sSql.Append("	AD_NM					, ");
			sSql.Append("	AD_DESCRIPTION			, ");
			sSql.Append("	AD_DESCRIPTION_SUB		, ");
			sSql.Append("	MONTHLY_FEE				, ");
			sSql.Append("	AFFILIATE_POINT			, ");
			sSql.Append("	AFFILIATE_AMT			, ");
			sSql.Append("	WOMAN_AFFILIATE_POINT	, ");
			sSql.Append("	WOMAN_AFFILIATE_AMT		, ");
			sSql.Append("	APPLY_START_DAY			, ");
			sSql.Append("	APPLY_END_DAY			, ");
			sSql.Append("	APPLY_SEX_CD			, ");
			sSql.Append("	APPLY_DOCOMO_FLAG		, ");
			sSql.Append("	APPLY_SOFTBANK_FLAG		, ");
			sSql.Append("	APPLY_AU_FLAG			, ");
			sSql.Append("	APPLY_ANDROID_FLAG		, ");
			sSql.Append("	APPLY_IPHONE_FLAG		, ");
			sSql.Append("	AD_CATEGORY				, ");
			sSql.Append("	AD_PUBLICATION_TOPIC	, ");
			sSql.Append("	SELF_AD_CD				, ");
			sSql.Append("	NA_FLAG					, ");
			sSql.Append("	UPDATE_DATE				, ");
			sSql.Append("	SITE_NM					, ");
			sSql.Append("	SETTLE_COMPANY_NM		, ");
			sSql.Append("	SELF_AD_NM				, ");
			sSql.Append("	SETTLE_TYPE_NM			, ");
			sSql.Append("	AD_CATEGORY_NM			, ");
			sSql.Append("	AD_PUBLICATION_TOPIC_NM	, ");
			sSql.Append("	APPLY_SEX_NM			");
			sSql.Append("FROM( ");
			sSql.Append("	SELECT VW_ASP_AD01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_ASP_AD01 ");

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pSettleCompanyCd,pAdCd,pAdNm,pAffiliatePointFrom,pAffiliatePointTo,pWomanAffiliatePointFrom,pWomanAffiliatePointTo,pApplyStartDayFrom,pApplyStartDayTo,pMonthlyFeeFrom,pMonthlyFeeTo,pApplyDocomoFlag,pApplySoftbankFlag,pApplyAuFlag,pApplyAndroidFlag,pApplyIphoneFlag,pApplySexCd,pAspAdStatus,ref sWhere);

			sSql.Append(sWhere);
			sSql.Append(") ");
			sSql.Append("WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ");
			sSql.Append(sOrder);

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_ASP_AD01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,string pSettleCompanyCd,string pAspAdCd,string pAdNm,string pAffiliatePointFrom,string pAffiliatePointTo,string pWomanAffiliatePointFrom,string pWomanAffiliatePointTo,string pApplyStartDayFrom,string pApplyStartDayTo,string pMonthlyFeeFrom,string pMonthlyFeeTo,string pApplyDocomoFlag,string pApplySoftbankFlag,string pApplyAuFlag,string pApplyAndroidFlag,string pApplyIphoneFlag,string pApplySexCd,string pAspAdStatus,ref string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD ",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!string.IsNullOrEmpty(pSettleCompanyCd)) {
			SysPrograms.SqlAppendWhere("SETTLE_COMPANY_CD = :SETTLE_COMPANY_CD ",ref pWhere);
			list.Add(new OracleParameter("SETTLE_COMPANY_CD",pSettleCompanyCd));
		}

		if (!string.IsNullOrEmpty(pAspAdCd)) {
			SysPrograms.SqlAppendWhere("ASP_AD_CD LIKE :ASP_AD_CD||'%' ",ref pWhere);
			list.Add(new OracleParameter("ASP_AD_CD",pAspAdCd));
		}

		if (!string.IsNullOrEmpty(pAdNm)) {
			SysPrograms.SqlAppendWhere("AD_NM LIKE '%'||:AD_NM ||'%'",ref pWhere);
			list.Add(new OracleParameter("AD_NM",pAdNm));
		}

		if (!string.IsNullOrEmpty(pAffiliatePointFrom)) {
			SysPrograms.SqlAppendWhere("AFFILIATE_POINT >= :AFFILIATE_POINT_FROM ",ref pWhere);
			list.Add(new OracleParameter("AFFILIATE_POINT_FROM",pAffiliatePointFrom));
		}

		if (!string.IsNullOrEmpty(pAffiliatePointTo)) {
			SysPrograms.SqlAppendWhere("AFFILIATE_POINT <= :AFFILIATE_POINT_TO ",ref pWhere);
			list.Add(new OracleParameter("AFFILIATE_POINT_TO",pAffiliatePointTo));
		}

		if (!string.IsNullOrEmpty(pWomanAffiliatePointFrom)) {
			SysPrograms.SqlAppendWhere("WOMAN_AFFILIATE_POINT >= :WOMAN_AFFILIATE_POINT_FROM ",ref pWhere);
			list.Add(new OracleParameter("WOMAN_AFFILIATE_POINT_FROM",pWomanAffiliatePointFrom));
		}

		if (!string.IsNullOrEmpty(pWomanAffiliatePointTo)) {
			SysPrograms.SqlAppendWhere("WOMAN_AFFILIATE_POINT <= :WOMAN_AFFILIATE_POINT_TO ",ref pWhere);
			list.Add(new OracleParameter("WOMAN_AFFILIATE_POINT_TO",pWomanAffiliatePointTo));
		}

		if (!string.IsNullOrEmpty(pApplyStartDayFrom)) {
			SysPrograms.SqlAppendWhere("APPLY_START_DAY >= :APPLY_START_DAY_FROM ",ref pWhere);
			list.Add(new OracleParameter("APPLY_START_DAY_FROM",pApplyStartDayFrom));
		}

		if (!string.IsNullOrEmpty(pApplyStartDayTo)) {
			SysPrograms.SqlAppendWhere("APPLY_START_DAY <= :APPLY_START_DAY_TO ",ref pWhere);
			list.Add(new OracleParameter("APPLY_START_DAY_TO",pApplyStartDayTo));
		}

		if (!string.IsNullOrEmpty(pMonthlyFeeFrom)) {
			SysPrograms.SqlAppendWhere("MONTHLY_FEE >= :MONTHLY_FEE_FROM ",ref pWhere);
			list.Add(new OracleParameter("MONTHLY_FEE_FROM",pMonthlyFeeFrom));
		}

		if (!string.IsNullOrEmpty(pMonthlyFeeTo)) {
			SysPrograms.SqlAppendWhere("MONTHLY_FEE <= :MONTHLY_FEE_TO ",ref pWhere);
			list.Add(new OracleParameter("MONTHLY_FEE_TO",pMonthlyFeeTo));
		}

		/*
		if (pApplyDocomoFlag.Equals(ViCommConst.FLAG_ON_STR) && pApplySoftbankFlag.Equals(ViCommConst.FLAG_ON_STR) && pApplyAuFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			SysPrograms.SqlAppendWhere("(APPLY_DOCOMO_FLAG = :APPLY_DOCOMO_FLAG OR APPLY_SOFTBANK_FLAG = :APPLY_SOFTBANK_FLAG OR APPLY_AU_FLAG = :APPLY_AU_FLAG) ",ref pWhere);
			list.Add(new OracleParameter("APPLY_DOCOMO_FLAG",pApplyDocomoFlag));
			list.Add(new OracleParameter("APPLY_SOFTBANK_FLAG",pApplySoftbankFlag));
			list.Add(new OracleParameter("APPLY_AU_FLAG",pApplyAuFlag));

		} else if (pApplyDocomoFlag.Equals(ViCommConst.FLAG_ON_STR) && pApplySoftbankFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			SysPrograms.SqlAppendWhere("(APPLY_DOCOMO_FLAG = :APPLY_DOCOMO_FLAG OR APPLY_SOFTBANK_FLAG = :APPLY_SOFTBANK_FLAG) ",ref pWhere);
			list.Add(new OracleParameter("APPLY_DOCOMO_FLAG",pApplyDocomoFlag));
			list.Add(new OracleParameter("APPLY_SOFTBANK_FLAG",pApplySoftbankFlag));

		} else if (pApplyDocomoFlag.Equals(ViCommConst.FLAG_ON_STR) && pApplyAuFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			SysPrograms.SqlAppendWhere("(APPLY_DOCOMO_FLAG = :APPLY_DOCOMO_FLAG OR APPLY_AU_FLAG = :APPLY_AU_FLAG) ",ref pWhere);
			list.Add(new OracleParameter("APPLY_DOCOMO_FLAG",pApplyDocomoFlag));
			list.Add(new OracleParameter("APPLY_AU_FLAG",pApplyAuFlag));

		} else if (pApplySoftbankFlag.Equals(ViCommConst.FLAG_ON_STR) && pApplyAuFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			SysPrograms.SqlAppendWhere("(APPLY_SOFTBANK_FLAG = :APPLY_SOFTBANK_FLAG OR APPLY_AU_FLAG = :APPLY_AU_FLAG) ",ref pWhere);
			list.Add(new OracleParameter("APPLY_SOFTBANK_FLAG",pApplySoftbankFlag));
			list.Add(new OracleParameter("APPLY_AU_FLAG",pApplyAuFlag));

		} else {
			if (pApplyDocomoFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				SysPrograms.SqlAppendWhere("APPLY_DOCOMO_FLAG = :APPLY_DOCOMO_FLAG ",ref pWhere);
				list.Add(new OracleParameter("APPLY_DOCOMO_FLAG",pApplyDocomoFlag));
			}

			if (pApplySoftbankFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				SysPrograms.SqlAppendWhere("APPLY_SOFTBANK_FLAG = :APPLY_SOFTBANK_FLAG ",ref pWhere);
				list.Add(new OracleParameter("APPLY_SOFTBANK_FLAG",pApplySoftbankFlag));
			}

			if (pApplyAuFlag.Equals(ViCommConst.FLAG_ON_STR)) {
				SysPrograms.SqlAppendWhere("APPLY_AU_FLAG = :APPLY_AU_FLAG ",ref pWhere);
				list.Add(new OracleParameter("APPLY_AU_FLAG",pApplyAuFlag));
			}
		}
		*/

		StringBuilder oApplyCarrier = new StringBuilder();
		string sTemp = " ( ";
		if (pApplyDocomoFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			oApplyCarrier.AppendFormat("{0}APPLY_DOCOMO_FLAG = :APPLY_DOCOMO_FLAG ", sTemp);
			list.Add(new OracleParameter("APPLY_DOCOMO_FLAG", pApplyDocomoFlag));
			sTemp = "OR ";
		}
		if (pApplySoftbankFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			oApplyCarrier.AppendFormat("{0}APPLY_SOFTBANK_FLAG = :APPLY_SOFTBANK_FLAG ", sTemp);
			list.Add(new OracleParameter("APPLY_SOFTBANK_FLAG", pApplySoftbankFlag));
			sTemp = "OR ";
		}
		if (pApplyAuFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			oApplyCarrier.AppendFormat("{0}APPLY_AU_FLAG = :APPLY_AU_FLAG ", sTemp);
			list.Add(new OracleParameter("APPLY_Au_FLAG", pApplyAuFlag));
			sTemp = "OR ";
		}
		if (pApplyAndroidFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			oApplyCarrier.AppendFormat("{0}APPLY_ANDROID_FLAG = :APPLY_ANDROID_FLAG ", sTemp);
			list.Add(new OracleParameter("APPLY_ANDROID_FLAG", pApplyAndroidFlag));
			sTemp = "OR ";
		}
		if (pApplyIphoneFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			oApplyCarrier.AppendFormat("{0}APPLY_IPHONE_FLAG = :APPLY_IPHONE_FLAG ", sTemp);
			list.Add(new OracleParameter("APPLY_IPHONE_FLAG", pApplyIphoneFlag));
		}

		if (!string.Empty.Equals(oApplyCarrier.ToString())) {
			oApplyCarrier.Append(") ");
			SysPrograms.SqlAppendWhere(oApplyCarrier.ToString(), ref pWhere);
		}

		if (!string.IsNullOrEmpty(pApplySexCd)) {
			SysPrograms.SqlAppendWhere("APPLY_SEX_CD = :APPLY_SEX_CD ",ref pWhere);
			list.Add(new OracleParameter("APPLY_SEX_CD",pApplySexCd));
		}

		switch (pAspAdStatus) {
			case "1":
				SysPrograms.SqlAppendWhere("(APPLY_START_DAY<=SYSDATE AND NVL(APPLY_END_DAY,'9999-12-31') >=SYSDATE)",ref pWhere);
				SysPrograms.SqlAppendWhere("NA_FLAG = :NA_FLAG ",ref pWhere);
				list.Add(new OracleParameter("NA_FLAG",ViCommConst.FLAG_OFF));
				break;
			case "2":
				SysPrograms.SqlAppendWhere("( ( NA_FLAG = :NA_FLAG ) OR NOT (APPLY_START_DAY<=SYSDATE AND NVL(APPLY_END_DAY,'9999-12-31') >=SYSDATE) ) ",ref pWhere);
				list.Add(new OracleParameter("NA_FLAG",ViCommConst.FLAG_ON));
				break;
			default:
				// 処理しない

				break;
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}
