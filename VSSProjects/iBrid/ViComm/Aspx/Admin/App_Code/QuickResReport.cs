﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: QuickResReport
--	Title			: ｸｲｯｸﾚｽﾎﾟﾝｽ稼動集計 
--	Progaram ID		: QuickResReport
--
--  Creation Date	: 2009.12.11
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater   Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Text;
using ViComm;
using iBridCommLib;

public class QuickResReport:DbSession {

	public QuickResReport() {
	}

	public int GetPageCount(string pSiteCd,string pReportDay) {
/*
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("T_QUICK_RES_REPORT	");

		string sWhere;
		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pReportDay,out sWhere);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.AppendLine(sWhere).ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
 */
		return 24;
	}


	public DataSet GetPageCollection(string pSiteCd,string pReportDay,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	*	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("T_QUICK_RES_REPORT	");

		string sWhere;
		string sSortExpression = "ORDER BY SITE_CD,REPORT_DAY,REPORT_HOUR ";
		string sPagingSql;

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pReportDay,out sWhere);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhere).ToString(),sSortExpression,startRowIndex,24,out sPagingSql);
		DataSet oDataSet = new DataSet();

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
				}
			}
		} finally {
			conn.Close();
		}


		DataColumn col;
		DataSet ds = new DataSet();
		DataTable dt = new DataTable();

		col = new DataColumn("SITE_CD",System.Type.GetType("System.String"));
		dt.Columns.Add(col);

		col = new DataColumn("REPORT_DAY",System.Type.GetType("System.String"));
		dt.Columns.Add(col);

		col = new DataColumn("REPORT_HOUR",System.Type.GetType("System.String"));
		dt.Columns.Add(col);

		col = new DataColumn("MAX_ROOM_IN_COUNT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);

		col = new DataColumn("RX_MAIL_COUNT",System.Type.GetType("System.String"));
		dt.Columns.Add(col);

		DataRow[] oCurRows;
		for (int i = 0;i < 24;i++) {
			DataRow newRow = dt.NewRow();
			newRow["SITE_CD"] = pSiteCd;
			newRow["REPORT_DAY"] = pReportDay;
			newRow["REPORT_HOUR"] = string.Format("{0:D2}",i);
			oCurRows = oDataSet.Tables[0].Select(string.Format("REPORT_HOUR = '{0:D2}'",i));
			if (oCurRows.Length >= 1) {
				newRow["MAX_ROOM_IN_COUNT"] = oCurRows[0]["MAX_ROOM_IN_COUNT"].ToString();
				newRow["RX_MAIL_COUNT"] = oCurRows[0]["RX_MAIL_COUNT"].ToString();
			} else {
				newRow["MAX_ROOM_IN_COUNT"] = "0";
				newRow["RX_MAIL_COUNT"] = "0";
			}
			dt.Rows.Add(newRow);
		}
		ds.Tables.Add(dt);

		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pReportDay,out string pWhere) {
		pWhere = string.Empty;
		ArrayList list = new ArrayList();

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!string.IsNullOrEmpty(pReportDay)) {
			SysPrograms.SqlAppendWhere("REPORT_DAY = :REPORT_DAY",ref pWhere);
			list.Add(new OracleParameter("REPORT_DAY",pReportDay));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}
