﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 動画ﾌｧｲﾙ種別

--	Progaram ID		: MovieFileType
--
--  Creation Date	: 2010.08.11
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using iBridCommLib;

public class MovieFileType:DbSession
{
    public MovieFileType()
    {
    }

    public DataSet GetList()
    {
        DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();
			
			StringBuilder oSqlBuilder = new StringBuilder();
			oSqlBuilder.AppendLine(" SELECT											");
			oSqlBuilder.AppendLine(" 	T_MOVIE_FILE_TYPE.MOVIE_FILE_TYPE		,   ");
			oSqlBuilder.AppendLine(" 	T_MOVIE_FILE_TYPE.SIZE_TYPE             ,   ");
			oSqlBuilder.AppendLine(" 	T_MOVIE_FILE_TYPE.FILE_FORMAT           ,   ");
			oSqlBuilder.AppendLine(" 	T_MOVIE_FILE_TYPE.TARGET_CARRIER        ,   ");
			oSqlBuilder.AppendLine(" 	T_MOVIE_FILE_TYPE.TARGET_USE_TYPE       ,   ");
			oSqlBuilder.AppendLine(" 	T_MOVIE_FILE_TYPE.MOVIE_FILE_TYPE_NM        ");
			oSqlBuilder.AppendLine(" FROM                                           ");
			oSqlBuilder.AppendLine(" 	T_MOVIE_FILE_TYPE                           ");
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn))
			{
				using (da = new OracleDataAdapter(cmd))
				{
					da.Fill(ds, "T_MOVIE_FILE_TYPE");
				}
			}
		} finally {
			conn.Close();
		}
        return ds;
    }
}
