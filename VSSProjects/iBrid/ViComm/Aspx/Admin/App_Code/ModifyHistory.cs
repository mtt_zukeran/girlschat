﻿/*************************************************************************
--	System			: ViComm System
--	Sub System Name	: Admin
--	Title			: 変更履歴
--	Progaram ID		: ModifyHistory
--
--  Creation Date	: 2010.03.30
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;

public class ModifyHistory:DbSession {

	public ModifyHistory() {
	}

	public int GetPageCount(string pStartPubDayFrom,string pStartPubDayTo) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM T_MODIFY_HISTORY ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pStartPubDayFrom,pStartPubDayTo,ref sWhere);
			sSql += sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pStartPubDayFrom,string pStartPubDayTo,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY START_PUB_DAY DESC,DOC_SEQ DESC";
			string sSql = "SELECT " +
								"DOC_SEQ			," +
								"DOC_TITLE			," +
								"START_PUB_DAY		," +
								"END_PUB_DAY		," +
								"MODIFY_USER_NM		," +
								"APPROVE_DAY		," +
								"APPROVE_USER_NM	," +
								"HTML_DOC1			," +
								"HTML_DOC2			," +
								"HTML_DOC3			," +
								"HTML_DOC4			," +
								"HTML_DOC5			," +
								"UPDATE_DATE " +
							"FROM(" +
							" SELECT T_MODIFY_HISTORY.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM T_MODIFY_HISTORY ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pStartPubDayFrom,pStartPubDayTo,ref sWhere);
			sSql += sWhere +
					")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW " +
					sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MODIFY_HISTORY");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetCsvData(string pStartPubDayFrom,string pStartPubDayTo) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY START_PUB_DAY,DOC_SEQ ";
			string sSql = "SELECT " +
								"DOC_SEQ			," +
								"DOC_TITLE			," +
								"START_PUB_DAY		," +
								"END_PUB_DAY		," +
								"MODIFY_USER_NM		," +
								"APPROVE_DAY		," +
								"APPROVE_USER_NM	," +
								"HTML_DOC1			," +
								"HTML_DOC2			," +
								"HTML_DOC3			," +
								"HTML_DOC4			," +
								"HTML_DOC5			," +
								"UPDATE_DATE " +
							"FROM " +
								"T_MODIFY_HISTORY  ";
			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pStartPubDayFrom,pStartPubDayTo,ref sWhere);
			sSql += sWhere +
					sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_MODIFY_HISTORY");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pStartPubDayFrom,string pStartPubDayTo,ref string pWhere) {
		ArrayList list = new ArrayList();
		pWhere = "";

		if ((!pStartPubDayFrom.Equals("")) && (!pStartPubDayTo.Equals(""))) {
			SysPrograms.SqlAppendWhere("(START_PUB_DAY >= :START_PUB_DAY_FROM AND START_PUB_DAY <= :START_PUB_DAY_TO)",ref pWhere);
			list.Add(new OracleParameter("START_PUB_DAY_FROM",pStartPubDayFrom));
			list.Add(new OracleParameter("START_PUB_DAY_TO",pStartPubDayTo));
		}
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}
