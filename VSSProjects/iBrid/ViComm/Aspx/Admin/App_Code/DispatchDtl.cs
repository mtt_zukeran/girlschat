﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 振分け明細

--	Progaram ID		: DispatchDtl
--
--  Creation Date	: 2010.03.19
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;

/// <summary>
/// DispatchDtl の概要の説明です

/// </summary>
public class DispatchDtl:DbSession {
	public DispatchDtl() {
	}

	public int GetPageCount(string pPartnerSiteCd,string pRegistDateFrom,string pRegistDateTo,string pRegistResult) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_DISPATCH_REGIST01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pPartnerSiteCd,pRegistDateFrom,pRegistDateTo,pRegistResult,ref sWhere);
			sSql += sWhere;

			using (cmd =  CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetCount(string pPartnerSiteCd,string pRegistDateFrom,string pRegistDateTo,string pRegistResult) {
		DataSet ds = new DataSet();
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(REGIST_RESULT) AS CNT, REGIST_RESULT"
				+ " FROM VW_DISPATCH_REGIST01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pPartnerSiteCd,pRegistDateFrom,pRegistDateTo,pRegistResult,ref sWhere);
			sSql += sWhere + " GROUP BY REGIST_RESULT";

			using (cmd =  CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
			}
		} finally {
			conn.Close();
		}

		return ds;
	}

	public DataSet GetPageCollection(string pPartnerSiteCd,string pRegistDateFrom,string pRegistDateTo,string pRegistResult,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = " ORDER BY USER_REGIST_DATE,USER_SEQ,PARTNER_SITE_SEQ";

			string sSql = "SELECT "
							+ "LOGIN_ID						,"
							+ "LOGIN_PASSWORD				,"
							+ "TEL							,"
							+ "TO_CHAR(USER_REGIST_DATE,'YYYY/MM/DD HH24:MI:SS') AS USER_REGIST_DATE,"
							+ "PARTNER_SITE_INITIAL			,"
							+ "TO_CHAR(REGIST_DATE,'YYYY/MM/DD HH24:MI:SS') AS REGIST_DATE          ,"
							+ "REGIST_RESULT				,"
							+ "REGIST_RESULT_NM				,"
							+ "PARTNER_SITE_LOGIN_ID		,"
							+ "USER_SEQ						,"
							+ "HISTORY_SEQ					,"
							+ "AD_CD						,"
							+ "AD_NM						,"
							+ "SITE_CD						,"
							+ "HOST_NM						"
							+ " FROM "
							+ " (SELECT VW_DISPATCH_REGIST01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_DISPATCH_REGIST01 "
							;

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pPartnerSiteCd,pRegistDateFrom,pRegistDateTo,pRegistResult,ref sWhere);
			sSql = sSql + sWhere;
			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd =  CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				da.Fill(ds,"VW_DISPATCH_REGIST01");
			}
		} finally {
			conn.Close();
		}

		return ds;
	}

	private OracleParameter[] CreateWhere(string pPartnerSiteCd,string pRegistDateFrom,string pRegistDateTo,string pRegistResult,ref string pWhere) {

		ArrayList list = new ArrayList();
		pWhere = " WHERE ";

		if (!pPartnerSiteCd.Equals("")) {
			pWhere += " PARTNER_SITE_SEQ = :PARTNER_SITE_SEQ AND ";
			list.Add(new OracleParameter("PARTNER_SITE_SEQ",pPartnerSiteCd));
		}

		pWhere += "USER_REGIST_DATE >= TO_DATE(:USER_REGIST_DATE_FROM,'YYYY/MM/DD HH24:MI:SS')"
				+ " AND USER_REGIST_DATE <= TO_DATE(:USER_REGIST_DATE_TO,'YYYY/MM/DD HH24:MI:SS')";
		list.Add(new OracleParameter("USER_REGIST_DATE_FROM",pRegistDateFrom + " 00:00:00"));
		list.Add(new OracleParameter("USER_REGIST_DATE_TO",pRegistDateTo + " 23:59:59"));

		if (!pRegistResult.Equals("")) {
			pWhere += " AND REGIST_RESULT = :REGIST_RESULT ";
			list.Add(new OracleParameter("REGIST_RESULT",pRegistResult));
		}
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}
