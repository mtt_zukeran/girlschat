﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 曜日別ランダム表示広告
--	Progaram ID		: RandomAdDayOfWeek
--
--  Creation Date	: 2010.03.25
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class RandomAdDayOfWeek:DbSession {

	public RandomAdDayOfWeek() {
	}

	public int GetPageCount(string pSiteCd, string pSexCd) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;

		string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_RANDOM_AD_DAYOFWEEK01 ";

		string sWhere = "";
		OracleParameter[] objParms = CreateWhere(pSiteCd,pSexCd,ref sWhere);
		sSql += sWhere;
		try{
			conn = DbConnect();

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd, string pSexCd) {
		DataSet ds;
		ds = new DataSet();
		string sViewNm = "VW_RANDOM_AD_DAYOFWEEK01";
		string sOrder = " ORDER BY SITE_CD,DAY_CD";
		string sSql = "SELECT " +
							"SITE_CD		," +
							"DAY_CD			," +
							"DAY_NM			," +
							"HTML_AD1		," +
							"HTML_AD2		," +
							"HTML_AD3		," +
							"HTML_AD4		," +
							"HTML_AD5		," +
							"HTML_AD6		," +
							"HTML_AD7		," +
							"HTML_AD8		," +
							"HTML_AD9		," +
							"HTML_AD10		," +
							"UPDATE_DATE	" +
						"FROM	" +
							"VW_RANDOM_AD_DAYOFWEEK01	";

		string sWhere = "";
		OracleParameter[] objParms = CreateWhere(pSiteCd, pSexCd,ref sWhere);
		sSql += sWhere;
		sSql += sOrder;

		try{
			conn = DbConnect();

			using (cmd = CreateSelectCommand(sSql,conn)) {
					cmd.Parameters.AddRange(objParms);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,sViewNm);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public DataSet GetCsvData(string pSiteCd, string pSexCd) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = " ORDER BY SITE_CD,DAY_CD ";
			string sSql = "SELECT " +
								"SITE_CD		," +
								"DAY_NM			," +
								"HTML_AD1		," +
								"HTML_AD2		," +
								"HTML_AD3		," +
								"HTML_AD4		," +
								"HTML_AD5		," +
								"HTML_AD6		," +
								"HTML_AD7		," +
								"HTML_AD8		," +
								"HTML_AD9		," +
								"HTML_AD10		" +
							"FROM " +
							 "VW_RANDOM_AD_DAYOFWEEK01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pSexCd,ref sWhere);
			sSql = sSql + sWhere;
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd, string pSexCd,ref string pWhere) {
		pWhere = "";
		List<OracleParameter> oOracleParameterList = new List<OracleParameter>();

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD", ref pWhere);
		oOracleParameterList.Add(new OracleParameter("SITE_CD", pSiteCd));
		SysPrograms.SqlAppendWhere("SEX_CD = :SEX_CD", ref pWhere);
		oOracleParameterList.Add(new OracleParameter("SEX_CD", pSexCd));

		return oOracleParameterList.ToArray();
	}

	public DataSet GetOne(string pSiteCd,string pDayCd,string pSexCd,string pAdNo) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT * FROM T_RANDOM_AD_DAYOFWEEK WHERE SITE_CD =:SITE_CD AND DAY_CD =:DAY_CD AND SEX_CD =: SEX_CD";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("DAY_CD",pDayCd);
				cmd.Parameters.Add("SEX_CD",pSexCd);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}

		//引数で指定された広告のみを返す
		DataSet dsRtn = new DataSet();
		DataTable dtRtn = dsRtn.Tables.Add();
		DataColumn col = new DataColumn("HTML_AD",System.Type.GetType("System.String"));
		dtRtn.Columns.Add(col);

		DataTable dt = ds.Tables[0];
		DataRow drRtn = dtRtn.NewRow();
		if (dt.Rows.Count > 0) {
			drRtn["HTML_AD"] = dt.Rows[0][string.Format("HTML_AD{0}",pAdNo)].ToString();
		}
		dtRtn.Rows.Add(drRtn);

		return dsRtn;
	}

	public bool IsExistBySite(string pSiteCd,string pSexCd) {
		DataSet ds;

		bool bExist = false;
		try{
			conn = DbConnect();

			string sSql = "SELECT * FROM T_RANDOM_AD_DAYOFWEEK WHERE SITE_CD = :SITE_CD AND SEX_CD = :SEX_CD";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("SEX_CD",pSexCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);

					if (ds.Tables[0].Rows.Count != 0) {
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}
}
