﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Cast PC
--	Title			: 足あと
--	Progaram ID		: Marking
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class Marking:DbSession {
	public string refuseType;

	public Marking() {
		refuseType = "";
	}

	public int GetPageCount(string pSiteCd,string pUserSeq,string pUserCharNo) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_MARKING01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}


	public DataSet GetPageCollection(string pSiteCd,string pUserSeq,string pUserCharNo,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY MARKING_DATE DESC,SITE_CD,PARTNER_USER_SEQ,PARTNER_USER_CHAR_NO ";
			string sSql = "SELECT " +
								"SITE_CD				," +
								"USER_SEQ				," +
								"USER_CHAR_NO			," +
								"CHARACTER_ONLINE_STATUS," +
								"DUMMY_TALK_FLAG		," +
								"MARKING_DATE			," +
								"LOGIN_ID				," +
								"PARTNER_LOGIN_ID		," +
								"ACT_CATEGORY_NM		," +
								"HANDLE_NM	" +
							"FROM(" +
							" SELECT VW_MARKING01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_MARKING01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_MARKING01");
				}
			}
		} finally {
			conn.Close();
		}

		ds.Tables[0].Columns.Add(new DataColumn("LAST_TALK_DATE",System.Type.GetType("System.String")));
		ds.Tables[0].Columns.Add(new DataColumn("TALK_COUNT",System.Type.GetType("System.String")));

		int iTalkCount;
		string sLastTalkDay;

		using (TalkHistory oTalkHis = new TalkHistory()) {
			foreach (DataRow dr in ds.Tables[0].Rows) {
				oTalkHis.GetTalkCount(pSiteCd,pUserSeq,pUserCharNo,dr["USER_SEQ"].ToString(),dr["USER_CHAR_NO"].ToString(),out iTalkCount,out sLastTalkDay);
				dr["LAST_TALK_DATE"] = sLastTalkDay;
				dr["TALK_COUNT"] = iTalkCount;
			}
		}
		return ds;
	}

	public int GetCastPageCount(string pSiteCd,string pUserSeq,string pUserCharNo) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_MARKING02 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}


	public DataSet GetCastPageCollection(string pSiteCd,string pUserSeq,string pUserCharNo,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY MARKING_DATE DESC,SITE_CD,PARTNER_USER_SEQ,PARTNER_USER_CHAR_NO ";
			string sSql = "SELECT " +
								"SITE_CD				," +
								"USER_SEQ				," +
								"USER_CHAR_NO			," +
								"CHARACTER_ONLINE_STATUS," +
								"MARKING_DATE			," +
								"LOGIN_ID				," +
								"HANDLE_NM	" +
							"FROM(" +
							" SELECT VW_MARKING02.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_MARKING02  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_MARKING02");
				}
			}
		} finally {
			conn.Close();
		}

		ds.Tables[0].Columns.Add(new DataColumn("LAST_TALK_DATE",System.Type.GetType("System.String")));
		ds.Tables[0].Columns.Add(new DataColumn("TALK_COUNT",System.Type.GetType("System.String")));

		int iTalkCount;
		string sLastTalkDay;

		using (TalkHistory oTalkHis = new TalkHistory()) {
			foreach (DataRow dr in ds.Tables[0].Rows) {
				oTalkHis.GetTalkCount(pSiteCd,pUserSeq,pUserCharNo,dr["USER_SEQ"].ToString(),dr["USER_CHAR_NO"].ToString(),out iTalkCount,out sLastTalkDay);
				dr["LAST_TALK_DATE"] = sLastTalkDay;
				dr["TALK_COUNT"] = iTalkCount;
			}
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pUserSeq,string pUserCharNo,ref string pWhere) {
		ArrayList list = new ArrayList();

		if (!pSiteCd.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!pUserSeq.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("PARTNER_USER_SEQ = :PARTNER_USER_SEQ",ref pWhere);
			list.Add(new OracleParameter("PARTNER_USER_SEQ",pUserSeq));
			if (!pUserCharNo.Equals("")) {
				iBridCommLib.SysPrograms.SqlAppendWhere("PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO",ref pWhere);
				list.Add(new OracleParameter("PARTNER_USER_CHAR_NO",pUserCharNo));
			}
		}
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

}
