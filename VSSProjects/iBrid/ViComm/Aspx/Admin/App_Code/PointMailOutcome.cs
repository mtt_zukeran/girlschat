﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: ポイントメール成果
--	Progaram ID		: PointMailOutcome
--  Creation Date	: 2012.03.16
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class PointMailOutcome : DbSession {
	public PointMailOutcome() {
	}

	public DataSet GetList(string pSiteCd,string pReportDayFrom,string pReportDayTo,string pRemarks) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	POINT_MAIL_OUTCOME_SEQ		,");
		oSqlBuilder.AppendLine("	SITE_CD						,");
		oSqlBuilder.AppendLine("	REQUEST_TX_DATE				,");
		oSqlBuilder.AppendLine("	REQUEST_TX_MAIL_SEQ			,");
		oSqlBuilder.AppendLine("	REQUEST_TX_TARGET_COUNT		,");
		oSqlBuilder.AppendLine("	SERVICE_POINT				,");
		oSqlBuilder.AppendLine("	COME_BACK_COUNT				,");
		oSqlBuilder.AppendLine("	COME_BACK_SETTLE_COUNT		,");
		oSqlBuilder.AppendLine("	OUTCOME_COUNT_DAYS			,");
		oSqlBuilder.AppendLine("	REMARKS						,");
		oSqlBuilder.AppendLine("	UPDATE_DATE					,");
		oSqlBuilder.AppendLine("	MAN_LOGIN_COUNT				,");
		oSqlBuilder.AppendLine("	MAN_TRANSFER_POINT			");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	VW_POINT_MAIL_OUTCOME01		");

		string sWhereClause = string.Empty;
		string sSortExpression = "ORDER BY REQUEST_TX_DATE DESC, MAN_LOGIN_COUNT, SERVICE_POINT, MAN_TRANSFER_POINT";

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pReportDayFrom,pReportDayTo,pRemarks,ref sWhereClause);

		oSqlBuilder.AppendLine(sWhereClause);
		oSqlBuilder.AppendLine(sSortExpression);

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(), conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pReportDayFrom,string pReportDayTo,string pRemarks,ref string pWhere) {
		ArrayList list = new ArrayList();

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!string.IsNullOrEmpty(pReportDayFrom) && !string.IsNullOrEmpty(pReportDayTo)) {
			DateTime dtFrom = DateTime.Parse(pReportDayFrom + " 00:00:00");
			DateTime dtTo = DateTime.Parse(pReportDayTo + " 23:59:59").AddSeconds(1);
			iBridCommLib.SysPrograms.SqlAppendWhere("REQUEST_TX_DATE >= :REQUEST_TX_DATE_FROM AND REQUEST_TX_DATE < :REQUEST_TX_DATE_TO",ref pWhere);
			list.Add(new OracleParameter("REQUEST_TX_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("REQUEST_TX_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (!string.IsNullOrEmpty(pRemarks)) {
			SysPrograms.SqlAppendWhere("REMARKS LIKE '%' || :REMARKS || '%'",ref pWhere);
			list.Add(new OracleParameter("REMARKS",pRemarks));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}
