﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: キャラクターログイン履歴
--	Progaram ID		: LoginCharacter
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class LoginCharacter:DbSession {
	public int GetPageCount(string pUserSeq,string pReportDayFrom,string pReportDayTo) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_LOGIN_CHARACTER01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pUserSeq,pReportDayFrom,pReportDayTo,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pUserSeq,string pReportDayFrom,string pReportDayTo,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY LOGIN_SEQ DESC,SITE_CD ";
			string sSql = "SELECT " +
							"LOGIN_SEQ						," +
							"START_DATE						," +
							"LOGOFF_DATE					," +
							"SITE_NM						," +
							"SITE_CD						," +
							"PRV_TV_TALK_COUNT				," +
							"PRV_TV_TALK_MIN				," +
							"PUB_TV_TALK_COUNT				," +
							"PUB_TV_TALK_MIN				," +
							"PRV_VOICE_TALK_COUNT			," +
							"PRV_VOICE_TALK_MIN				," +
							"PUB_VOICE_TALK_COUNT			," +
							"PUB_VOICE_TALK_MIN				," +
							"VIEW_TALK_MIN					," +
							"VIEW_TALK_COUNT				," +
							"WIRETAP_MIN					," +
							"WIRETAP_COUNT					," +
							"VIEW_BROADCAST_MIN				," +
							"VIEW_BROADCAST_COUNT			," +
							"CAST_PRV_TV_TALK_CALL_COUNT	," +
							"CAST_PRV_TV_TALK_COUNT			," +
							"CAST_PRV_TV_TALK_MIN			," +
							"CAST_PUB_TV_TALK_CALL_COUNT	," +
							"CAST_PUB_TV_TALK_COUNT			," +
							"CAST_PUB_TV_TALK_MIN			," +
							"CAST_PRV_VOICE_TALK_CALL_COUNT	," +
							"CAST_PRV_VOICE_TALK_COUNT		," +
							"CAST_PRV_VOICE_TALK_MIN		," +
							"CAST_PUB_VOICE_TALK_CALL_COUNT	," +
							"CAST_PUB_VOICE_TALK_COUNT		," +
							"CAST_PUB_VOICE_TALK_MIN " +
						"FROM(" +
							" SELECT VW_LOGIN_CHARACTER01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_LOGIN_CHARACTER01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pUserSeq,pReportDayFrom,pReportDayTo,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd =  CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_LOGIN_CHARACTER01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(string pUserSeq,string pReportDayFrom,string pReportDayTo,ref string pWhere) {
		ArrayList list = new ArrayList();

		if (!pUserSeq.Equals("")) {
			SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhere);
			list.Add(new OracleParameter("USER_SEQ",pUserSeq));
		}

		if ((!pReportDayFrom.Equals("")) && (!pReportDayTo.Equals(""))) {
			DateTime dtForm = DateTime.ParseExact(pReportDayFrom + " 00:00:00","yyyy/MM/dd HH:mm:ss",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);
			DateTime dtTo = DateTime.ParseExact(pReportDayTo + " 00:00:00","yyyy/MM/dd HH:mm:ss",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);
			SysPrograms.SqlAppendWhere("START_DATE >= :START_DATE_FROM AND START_DATE < :START_DATE_TO",ref pWhere);
			list.Add(new OracleParameter("START_DATE_FROM",OracleDbType.Date,dtForm,ParameterDirection.Input));
			list.Add(new OracleParameter("START_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public int GetSitePageCount(string pSiteCd,string pReportDayFrom,string pReportDayTo,int pSummaryFlag,string pLoginId,string pHandleNm,string pProductionSeq,string pManagerSeq) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_LOGIN_CHARACTER01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateSiteWhere(pSiteCd,pReportDayFrom,pReportDayTo,pLoginId,pHandleNm,pProductionSeq,pManagerSeq,0,0,ref sWhere);
			sSql = sSql + sWhere;

			if (pSummaryFlag != 0) {
				sSql += " GROUP BY SITE_CD,USER_SEQ,USER_CHAR_NO ";
				sSql = "SELECT COUNT(*) AS ROW_COUNT FROM (" + sSql + ")";
			}

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetSitePageCollection(string pSiteCd,string pReportDayFrom,string pReportDayTo,int pSummaryFlag,string pLoginId,string pHandleNm,string pProductionSeq,string pManagerSeq,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();
			string sOrder = "ORDER BY LOGIN_SEQ DESC,SITE_CD ";
			string sWhere0 = "";
			string sWhere1 = "";
			OracleParameter[] oParms0 = CreateSiteWhere(pSiteCd,pReportDayFrom,pReportDayTo,pLoginId,pHandleNm,pProductionSeq,pManagerSeq,0,pSummaryFlag,ref sWhere0);
			OracleParameter[] oParms1 = CreateSiteWhere(pSiteCd,pReportDayFrom,pReportDayTo,pLoginId,pHandleNm,pProductionSeq,pManagerSeq,1,pSummaryFlag,ref sWhere1);

			StringBuilder sSql = new StringBuilder();
			if (pSummaryFlag == 0) {
				sSql.Append("SELECT ").AppendLine();
				sSql.Append("PRV_TV_TALK_COUNT				,").AppendLine();
				sSql.Append("PRV_TV_TALK_MIN				,").AppendLine();
				sSql.Append("PUB_TV_TALK_COUNT				,").AppendLine();
				sSql.Append("PUB_TV_TALK_MIN				,").AppendLine();
				sSql.Append("PRV_VOICE_TALK_COUNT			,").AppendLine();
				sSql.Append("PRV_VOICE_TALK_MIN				,").AppendLine();
				sSql.Append("PUB_VOICE_TALK_COUNT			,").AppendLine();
				sSql.Append("PUB_VOICE_TALK_MIN				,").AppendLine();
				sSql.Append("VIEW_TALK_MIN					,").AppendLine();
				sSql.Append("VIEW_TALK_COUNT				,").AppendLine();
				sSql.Append("WIRETAP_MIN					,").AppendLine();
				sSql.Append("WIRETAP_COUNT					,").AppendLine();
				sSql.Append("VIEW_BROADCAST_MIN				,").AppendLine();
				sSql.Append("VIEW_BROADCAST_COUNT			,").AppendLine();
				sSql.Append("CAST_PRV_TV_TALK_CALL_COUNT	,").AppendLine();
				sSql.Append("CAST_PRV_TV_TALK_COUNT			,").AppendLine();
				sSql.Append("CAST_PRV_TV_TALK_MIN			,").AppendLine();
				sSql.Append("CAST_PUB_TV_TALK_CALL_COUNT	,").AppendLine();
				sSql.Append("CAST_PUB_TV_TALK_COUNT			,").AppendLine();
				sSql.Append("CAST_PUB_TV_TALK_MIN			,").AppendLine();
				sSql.Append("CAST_PRV_VOICE_TALK_CALL_COUNT	,").AppendLine();
				sSql.Append("CAST_PRV_VOICE_TALK_COUNT		,").AppendLine();
				sSql.Append("CAST_PRV_VOICE_TALK_MIN		,").AppendLine();
				sSql.Append("CAST_PUB_VOICE_TALK_CALL_COUNT	,").AppendLine();
				sSql.Append("CAST_PUB_VOICE_TALK_COUNT		,").AppendLine();
				sSql.Append("CAST_PUB_VOICE_TALK_MIN		,").AppendLine();
				sSql.Append("TOTAL_CALL_COUNT				,").AppendLine();
				sSql.Append("TOTAL_TALK_COUNT				,").AppendLine();
				sSql.Append("TOTAL_TALK_MIN					,").AppendLine();
				sSql.Append("TOTAL_VIEW_COUNT				,").AppendLine();
				sSql.Append("TOTAL_VIEW_MIN					,").AppendLine();
				sSql.Append("LOGIN_SEQ						,").AppendLine();
				sSql.Append("START_DATE						,").AppendLine();
				sSql.Append("LOGOFF_DATE					,").AppendLine();
				sSql.Append("SITE_NM						,").AppendLine();
				sSql.Append("SITE_CD						,").AppendLine();
				sSql.Append("LOGIN_ID						,").AppendLine();
				sSql.Append("HANDLE_NM						").AppendLine();
				sSql.Append("FROM(").AppendLine();
				sSql.Append(" SELECT VW_LOGIN_CHARACTER01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_LOGIN_CHARACTER01  ").AppendLine();
				sSql.Append(sWhere0).AppendLine();
				sSql.Append(sOrder);
				sSql.Append(")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ").AppendLine();
			} else {
				sSql.Append("SELECT * FROM( ").AppendLine();
				sSql.Append("SELECT T_USER.LOGIN_ID,T1.*,T_CAST_CHARACTER.HANDLE_NM,ROWNUM AS RNUM,					").AppendLine();
				sSql.Append("		(																				").AppendLine();
				sSql.Append("			SELECT NVL(SUM(CEIL(WORK_SEC/60)),0) FROM VW_LOGIN_CHARACTER00 T2			").AppendLine();
				sSql.Append(sWhere1 + " AND T2.DUMMY_TALK_FLAG = :DUMMY_TALK_FLAG	AND								").AppendLine();
				sSql.Append("					T1.SITE_CD		= T2.SITE_CD		AND								").AppendLine();
				sSql.Append("					T1.USER_SEQ		= T2.USER_SEQ		AND								").AppendLine();
				sSql.Append("					T1.USER_CHAR_NO = T2.USER_CHAR_NO 									").AppendLine();
				sSql.Append("		) AS  DUMMY_MIN																	").AppendLine();
				sSql.Append("		FROM(																			").AppendLine();
				sSql.Append("			SELECT																		").AppendLine();
				sSql.Append("				SUM(PRV_TV_TALK_COUNT				) AS PRV_TV_TALK_COUNT				,").AppendLine();
				sSql.Append("				SUM(PRV_TV_TALK_MIN					) AS PRV_TV_TALK_MIN				,").AppendLine();
				sSql.Append("				SUM(PUB_TV_TALK_COUNT				) AS PUB_TV_TALK_COUNT				,").AppendLine();
				sSql.Append("				SUM(PUB_TV_TALK_MIN					) AS PUB_TV_TALK_MIN				,").AppendLine();
				sSql.Append("				SUM(PRV_VOICE_TALK_COUNT			) AS PRV_VOICE_TALK_COUNT			,").AppendLine();
				sSql.Append("				SUM(PRV_VOICE_TALK_MIN				) AS PRV_VOICE_TALK_MIN				,").AppendLine();
				sSql.Append("				SUM(PUB_VOICE_TALK_COUNT			) AS PUB_VOICE_TALK_COUNT			,").AppendLine();
				sSql.Append("				SUM(PUB_VOICE_TALK_MIN				) AS PUB_VOICE_TALK_MIN				,").AppendLine();
				sSql.Append("				SUM(VIEW_TALK_MIN					) AS VIEW_TALK_MIN					,").AppendLine();
				sSql.Append("				SUM(VIEW_TALK_COUNT					) AS VIEW_TALK_COUNT				,").AppendLine();
				sSql.Append("				SUM(WIRETAP_MIN						) AS WIRETAP_MIN					,").AppendLine();
				sSql.Append("				SUM(WIRETAP_COUNT					) AS WIRETAP_COUNT					,").AppendLine();
				sSql.Append("				SUM(VIEW_BROADCAST_MIN				) AS VIEW_BROADCAST_MIN				,").AppendLine();
				sSql.Append("				SUM(VIEW_BROADCAST_COUNT			) AS VIEW_BROADCAST_COUNT			,").AppendLine();
				sSql.Append("				SUM(CAST_PRV_TV_TALK_CALL_COUNT		) AS CAST_PRV_TV_TALK_CALL_COUNT	,").AppendLine();
				sSql.Append("				SUM(CAST_PRV_TV_TALK_COUNT			) AS CAST_PRV_TV_TALK_COUNT			,").AppendLine();
				sSql.Append("				SUM(CAST_PRV_TV_TALK_MIN			) AS CAST_PRV_TV_TALK_MIN			,").AppendLine();
				sSql.Append("				SUM(CAST_PUB_TV_TALK_CALL_COUNT		) AS CAST_PUB_TV_TALK_CALL_COUNT	,").AppendLine();
				sSql.Append("				SUM(CAST_PUB_TV_TALK_COUNT			) AS CAST_PUB_TV_TALK_COUNT			,").AppendLine();
				sSql.Append("				SUM(CAST_PUB_TV_TALK_MIN			) AS CAST_PUB_TV_TALK_MIN			,").AppendLine();
				sSql.Append("				SUM(CAST_PRV_VOICE_TALK_CALL_COUNT	) AS CAST_PRV_VOICE_TALK_CALL_COUNT	,").AppendLine();
				sSql.Append("				SUM(CAST_PRV_VOICE_TALK_COUNT		) AS CAST_PRV_VOICE_TALK_COUNT		,").AppendLine();
				sSql.Append("				SUM(CAST_PRV_VOICE_TALK_MIN			) AS CAST_PRV_VOICE_TALK_MIN		,").AppendLine();
				sSql.Append("				SUM(CAST_PUB_VOICE_TALK_CALL_COUNT	) AS CAST_PUB_VOICE_TALK_CALL_COUNT	,").AppendLine();
				sSql.Append("				SUM(CAST_PUB_VOICE_TALK_COUNT		) AS CAST_PUB_VOICE_TALK_COUNT		,").AppendLine();
				sSql.Append("				SUM(CAST_PUB_VOICE_TALK_MIN			) AS CAST_PUB_VOICE_TALK_MIN		,").AppendLine();
				sSql.Append("				SUM(TOTAL_CALL_COUNT				) AS TOTAL_CALL_COUNT				,").AppendLine();
				sSql.Append("				SUM(TOTAL_TALK_COUNT				) AS TOTAL_TALK_COUNT				,").AppendLine();
				sSql.Append("				SUM(TOTAL_TALK_MIN					) AS TOTAL_TALK_MIN					,").AppendLine();
				sSql.Append("				SUM(TOTAL_VIEW_COUNT				) AS TOTAL_VIEW_COUNT				,").AppendLine();
				sSql.Append("				SUM(TOTAL_VIEW_MIN					) AS TOTAL_VIEW_MIN					,").AppendLine();
				sSql.Append("				SITE_CD				,							").AppendLine();
				sSql.Append("				USER_SEQ			,							").AppendLine();
				sSql.Append("				USER_CHAR_NO		,							").AppendLine();
				sSql.Append("				SUM(CEIL(WORK_SEC/60)) 	AS WORK_MIN				").AppendLine();
				sSql.Append("			FROM												").AppendLine();
				sSql.Append("				VW_LOGIN_CHARACTER00							").AppendLine();
				sSql.Append(sWhere0).AppendLine();
				sSql.Append("			GROUP BY SITE_CD,USER_SEQ,USER_CHAR_NO				").AppendLine();
				sSql.Append("		)T1,T_USER,T_CAST_CHARACTER,T_CAST						").AppendLine();
				sSql.Append("		WHERE													").AppendLine();
				sSql.Append("			T1.USER_SEQ		= T_USER.USER_SEQ				AND	").AppendLine();
				sSql.Append("			T1.SITE_CD		= T_CAST_CHARACTER.SITE_CD		AND	").AppendLine();
				sSql.Append("			T1.USER_SEQ		= T_CAST_CHARACTER.USER_SEQ		AND	").AppendLine();
				sSql.Append("			T1.USER_CHAR_NO = T_CAST_CHARACTER.USER_CHAR_NO	AND	").AppendLine();
				sSql.Append("			T1.USER_SEQ		= T_CAST.USER_SEQ					").AppendLine();

				if (pLoginId.Equals("") == false) {
					sSql.Append("AND T_USER.LOGIN_ID LIKE :LOGIN_ID||'%'").AppendLine();
				}

				if (pHandleNm.Equals("") == false) {
					sSql.Append("AND HANDLE_NM LIKE '%' || :HANDLE_NM ||'%'").AppendLine();
				}

				if (!string.IsNullOrEmpty(pProductionSeq)) {
					sSql.AppendLine("AND PRODUCTION_SEQ = :PRODUCTION_SEQ");
				}

				if (!string.IsNullOrEmpty(pManagerSeq)) {
					sSql.AppendLine("AND MANAGER_SEQ = :MANAGER_SEQ");
				}
				sSql.Append("		ORDER BY LOGIN_ID										").AppendLine();
				sSql.Append(")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ").AppendLine();
			}

			using (cmd =  CreateSelectCommand(sSql.ToString(),conn)) {
				if (pSummaryFlag != 0) {
					for (int i = 0;i < oParms1.Length;i++) {
						cmd.Parameters.Add(oParms1[i]);
					}
					cmd.Parameters.Add("DUMMY_TALK_FLAG",ViComm.ViCommConst.FLAG_ON);
				}
				for (int i = 0;i < oParms0.Length;i++) {
					cmd.Parameters.Add(oParms0[i]);
				}
				if (pSummaryFlag != 0) {
					if (pLoginId.Equals("") == false) {
						cmd.Parameters.Add(new OracleParameter("LOGIN_ID",pLoginId));
					}
					if (pHandleNm.Equals("") == false) {
						cmd.Parameters.Add(new OracleParameter("HANDLE_NM",pHandleNm));
					}
					if (!string.IsNullOrEmpty(pProductionSeq)) {
						cmd.Parameters.Add(new OracleParameter("PRODUCTION_SEQ",pProductionSeq));
					}
					if (!string.IsNullOrEmpty(pManagerSeq)) {
						cmd.Parameters.Add(new OracleParameter("MANAGER_SEQ",pManagerSeq));
					}
				}
				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_LOGIN_CHARACTER01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateSiteWhere(string pSiteCd,string pReportDayFrom,string pReportDayTo,string pLoginId,string pHandleNm,string pProductionSeq,string pManagerSeq,int pIdx,int pSummaryFlag,ref string pWhere) {
		ArrayList list = new ArrayList();

		SysPrograms.SqlAppendWhere(string.Format("SITE_CD = :SITE_CD{0}",pIdx),ref pWhere);
		list.Add(new OracleParameter("SITE_CD",pSiteCd));

		if ((!pReportDayFrom.Equals("")) && (!pReportDayTo.Equals(""))) {
			DateTime dtForm = DateTime.ParseExact(pReportDayFrom + ":00:00","yyyy/MM/dd HH:mm:ss",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);
			DateTime dtTo = DateTime.ParseExact(pReportDayTo + ":00:00","yyyy/MM/dd HH:mm:ss",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);
			SysPrograms.SqlAppendWhere(string.Format("START_DATE >= :START_DATE_FROM{0} AND START_DATE < :START_DATE_TO{0}",pIdx),ref pWhere);
			list.Add(new OracleParameter("START_DATE_FROM",OracleDbType.Date,dtForm,ParameterDirection.Input));
			list.Add(new OracleParameter("START_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (pSummaryFlag == 0) {
			if (pLoginId.Equals("") == false) {
				SysPrograms.SqlAppendWhere("LOGIN_ID LIKE :LOGIN_ID ||'%'",ref pWhere);
				list.Add(new OracleParameter("LOGIN_ID",pLoginId));
			}

			if (pHandleNm.Equals("") == false) {
				SysPrograms.SqlAppendWhere("HANDLE_NM LIKE '%'||:HANDLE_NM||'%'",ref pWhere);
				list.Add(new OracleParameter("HANDLE_NM",pHandleNm));
			}

			if (!string.IsNullOrEmpty(pProductionSeq)) {
				SysPrograms.SqlAppendWhere("PRODUCTION_SEQ = :PRODUCTION_SEQ",ref pWhere);
				list.Add(new OracleParameter("PRODUCTION_SEQ",pProductionSeq));
			}

			if (!string.IsNullOrEmpty(pManagerSeq)) {
				SysPrograms.SqlAppendWhere("MANAGER_SEQ = :MANAGER_SEQ",ref pWhere);
				list.Add(new OracleParameter("MANAGER_SEQ",pManagerSeq));
			}
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}
