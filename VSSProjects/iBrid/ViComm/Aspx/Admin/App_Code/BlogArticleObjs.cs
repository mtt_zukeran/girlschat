﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: ブログ投稿オブジェクト

--	Progaram ID		: BlogArticleObjs
--  Creation Date	: 2011.04.04
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class BlogArticleObjs : DbSession {

	public BlogArticleObjs() {
	}

	public DataSet GetList(string pSiteCd, string pUserSeq, string pUserCharNo, string pBlogSeq, string pBlogArticleSeq) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	U.LOGIN_ID				,");
		oSqlBuilder.AppendLine("	BAO.SITE_CD				,");
		oSqlBuilder.AppendLine("	BAO.USER_SEQ			,");
		oSqlBuilder.AppendLine("	BAO.USER_CHAR_NO		,");
		oSqlBuilder.AppendLine("	BAO.BLOG_SEQ			,");
		oSqlBuilder.AppendLine("	BAO.BLOG_ARTICLE_SEQ	,");
		oSqlBuilder.AppendLine("	BAO.OBJ_SEQ				,");
		oSqlBuilder.AppendLine("	BO.BLOG_FILE_TYPE		,");
		oSqlBuilder.AppendLine("	CASE WHEN BO.BLOG_FILE_TYPE = '1' THEN GET_PHOTO_IMG_PATH(BAO.SITE_CD, U.LOGIN_ID, BAO.OBJ_SEQ) END PHOTO_IMG_PATH");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_BLOG_ARTICLE_OBJS BAO	,");
		oSqlBuilder.AppendLine("	T_BLOG_OBJS			BO	,");
		oSqlBuilder.AppendLine("	T_USER				U	");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	BAO.SITE_CD				= BO.SITE_CD				AND");
		oSqlBuilder.AppendLine("	BAO.USER_SEQ			= BO.USER_SEQ				AND");
		oSqlBuilder.AppendLine("	BAO.USER_CHAR_NO		= BO.USER_CHAR_NO			AND");
		oSqlBuilder.AppendLine("	BAO.OBJ_SEQ				= BO.OBJ_SEQ				AND");
		oSqlBuilder.AppendLine("	BAO.USER_SEQ			= U.USER_SEQ				AND");
		oSqlBuilder.AppendLine("	BAO.SITE_CD				= :SITE_CD					AND");
		oSqlBuilder.AppendLine("	BAO.USER_SEQ			= :USER_SEQ					AND");
		oSqlBuilder.AppendLine("	BAO.USER_CHAR_NO		= :USER_CHAR_NO				AND");
		oSqlBuilder.AppendLine("	BAO.BLOG_SEQ			= :BLOG_SEQ					AND");
		oSqlBuilder.AppendLine("	BAO.BLOG_ARTICLE_SEQ	= :BLOG_ARTICLE_SEQ			");
		oSqlBuilder.AppendLine("ORDER BY	");
		oSqlBuilder.AppendLine("	BAO.REGIST_DATE");

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(), conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(new OracleParameter("SITE_CD", pSiteCd));
				cmd.Parameters.Add(new OracleParameter("USER_SEQ", pUserSeq));
				cmd.Parameters.Add(new OracleParameter("USER_CHAR_NO", pUserCharNo));
				cmd.Parameters.Add(new OracleParameter("BLOG_SEQ", pBlogSeq));
				cmd.Parameters.Add(new OracleParameter("BLOG_ARTICLE_SEQ", pBlogArticleSeq));

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}
}
