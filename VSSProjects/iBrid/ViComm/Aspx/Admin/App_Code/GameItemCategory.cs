﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: ゲームアイテムカテゴリ
--	Progaram ID		: GameItemCategory
--
--  Creation Date	: 2011.07.19
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;


public class GameItemCategory : DbSession {

	public GameItemCategory() {
	}

	public DataSet GetList(string pSiteCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	SITE_CD					,");
		oSqlBuilder.AppendLine("	GAME_ITEM_CATEGORY_NM	,");
		oSqlBuilder.AppendLine("	GAME_ITEM_CATEGORY_TYPE	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_GAME_ITEM_CATEGORY	");
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	SITE_CD = :SITE_CD");
		oSqlBuilder.AppendLine("ORDER BY	");
		oSqlBuilder.AppendLine("	SITE_CD, GAME_ITEM_CATEGORY_GROUP_TYPE, GAME_ITEM_CATEGORY_TYPE");

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.Add(":SITE_CD",pSiteCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		}
		finally {
			conn.Close();
		}
		return oDs;
	}

}
