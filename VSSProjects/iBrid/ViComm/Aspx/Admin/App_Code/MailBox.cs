﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メールBOX
--	Progaram ID		: MailBox
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using iBridCommLib;
using ViComm;

public class MailBox:DbSession {


	public MailBox() {
	}

	public int GetPageCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerLoginId,string pPartnerUserCharNo,string pSexCd,string pReportDayTimeFrom,string pReportDayTimeTo,string pAttachedType,bool pWithBatchMail,string pTxRxType) {

		string sWithBatchMail = string.Empty;
		if (!pWithBatchMail) {
			sWithBatchMail = ViCommConst.FLAG_OFF_STR;
		}

		return GetPageCount(pSiteCd,pUserSeq,pUserCharNo,pPartnerLoginId,pPartnerUserCharNo,pSexCd,pReportDayTimeFrom,pReportDayTimeTo,pAttachedType,sWithBatchMail,pTxRxType);
	}
	public int GetPageCount(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerLoginId,string pPartnerUserCharNo,string pSexCd,string pReportDayTimeFrom,string pReportDayTimeTo,string pAttachedType,string pWithBatchMailFlag,string pTxRxType) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		try {
			conn = DbConnect();

			StringBuilder oSqlBuilder = new StringBuilder();
			oSqlBuilder.Append("SELECT COUNT(*) AS ROW_COUNT FROM VW_MAIL_LOG00").AppendLine();
			string sWhere = "";

			OracleParameter[] oParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pPartnerLoginId,pPartnerUserCharNo,pReportDayTimeFrom,pReportDayTimeTo,pAttachedType,pWithBatchMailFlag,pTxRxType,ref sWhere);
			oSqlBuilder.Append(sWhere).AppendLine();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < oParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)oParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPages = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPages;
	}

	public DataSet GetPageCollection(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerLoginId,string pPartnerUserCharNo,string pSexCd,string pReportDayTimeFrom,string pReportDayTimeTo,string pAttachedType,bool pWithBatchMail,string pTxRxType,int startRowIndex,int maximumRows) {

		string sWithBatchMail = string.Empty;
		if (!pWithBatchMail) {
			sWithBatchMail = ViCommConst.FLAG_OFF_STR;
		}
		return GetPageCollection(pSiteCd,
									pUserSeq,
									pUserCharNo,
									pPartnerLoginId,
									pPartnerUserCharNo,
									pSexCd,
									pReportDayTimeFrom,
									pReportDayTimeTo,
									pAttachedType,
									sWithBatchMail,
									pTxRxType,
									startRowIndex,
									maximumRows);
	}
	public DataSet GetPageCollection(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerLoginId,string pPartnerUserCharNo,string pSexCd,string pReportDayTimeFrom,string pReportDayTimeTo,string pAttachedType,string pWithBatchMailFlag,string pTxRxType,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();
			string sJoinTable;
			string sColumnJoin;
			
			if (pSexCd.Equals(ViCommConst.MAN)) {
				sJoinTable = "T_CAST_CHARACTER";
				sColumnJoin = "";
			} else {
				sJoinTable = "T_USER_MAN_CHARACTER";
				sColumnJoin = ",CASE WHEN (J1.REGIST_DATE >= TO_CHAR(SYSDATE - S1.MAN_NEW_FACE_DAYS,'YYYY/MM/DD')) THEN 'NEW' ELSE ''       END AS STR_NEW";
			}
			string sOrder = "ORDER BY CREATE_DATE DESC";
			string sOrderLast = "ORDER BY T1.CREATE_DATE DESC";

			StringBuilder oSqlBuilder = new StringBuilder();

			oSqlBuilder.Append("SELECT T1.*,J1.HANDLE_NM" + sColumnJoin).AppendLine();
			oSqlBuilder.Append("	FROM (				 ").AppendLine();
			oSqlBuilder.Append("	SELECT						").AppendLine();
			oSqlBuilder.Append("		CASE WHEN INNER.TX_USER_SEQ = :ONW_USER_SEQ1 THEN '1' ELSE '2' 										END	AS TXRX_TYPE			,").AppendLine();
			oSqlBuilder.Append("		CASE WHEN INNER.TX_USER_SEQ = :ONW_USER_SEQ2 THEN INNER.RX_LOGIN_ID		ELSE INNER.TX_LOGIN_ID		END	AS PARTNER_LOGIN_ID		,").AppendLine();
			oSqlBuilder.Append("		CASE WHEN INNER.TX_USER_SEQ = :ONW_USER_SEQ3 THEN INNER.RX_SEX_CD		ELSE INNER.TX_SEX_CD 		END	AS PARTNER_SEX_CD		,").AppendLine();
			oSqlBuilder.Append("		CASE WHEN INNER.TX_USER_SEQ = :ONW_USER_SEQ4 THEN INNER.RX_USER_SEQ		ELSE INNER.TX_USER_SEQ 		END	AS PARTNER_USER_SEQ		,").AppendLine();
			oSqlBuilder.Append("		CASE WHEN INNER.TX_USER_SEQ = :ONW_USER_SEQ5 THEN INNER.RX_USER_CHAR_NO	ELSE INNER.TX_USER_CHAR_NO	END	AS PARTNER_USER_CHAR_NO	,").AppendLine();
			oSqlBuilder.Append("		CASE WHEN INNER.TX_USER_SEQ = :ONW_USER_SEQ6 THEN INNER.TX_LOGIN_ID		ELSE INNER.RX_LOGIN_ID		END	AS LOGIN_ID				,").AppendLine();
			oSqlBuilder.Append("		CASE WHEN INNER.TX_USER_SEQ = :ONW_USER_SEQ7 THEN INNER.TX_SITE_CD		ELSE INNER.RX_SITE_CD		END	AS SITE_CD				,").AppendLine();
			oSqlBuilder.Append("		ROWNUM AS RNUM	,").AppendLine();
			oSqlBuilder.Append("		INNER.*			 ").AppendLine();
			oSqlBuilder.Append("	FROM (				 ").AppendLine();
			if (pTxRxType.Equals(ViCommConst.TXRX)) {
				oSqlBuilder.Append("		SELECT /*+ INDEX(VW_MAIL_LOG00.T_MAIL_LOG IX_MAIL_LOG_7_05 */").AppendLine();
			} else {
				oSqlBuilder.Append("		SELECT ").AppendLine();
			}
			oSqlBuilder.Append("			MAIL_SEQ						,").AppendLine();
			oSqlBuilder.Append("			MAIL_TYPE						,").AppendLine();
			oSqlBuilder.Append("			CREATE_DATE						,").AppendLine();
			oSqlBuilder.Append("			MAIL_TITLE						,").AppendLine();
			oSqlBuilder.Append("			MAIL_DOC1						,").AppendLine();
			oSqlBuilder.Append("			MAIL_DOC2						,").AppendLine();
			oSqlBuilder.Append("			MAIL_DOC3						,").AppendLine();
			oSqlBuilder.Append("			MAIL_DOC4						,").AppendLine();
			oSqlBuilder.Append("			MAIL_DOC5						,").AppendLine();
			oSqlBuilder.Append("			ORIGINAL_TITLE					,").AppendLine();
			oSqlBuilder.Append("			ORIGINAL_DOC1					,").AppendLine();
			oSqlBuilder.Append("			ORIGINAL_DOC2					,").AppendLine();
			oSqlBuilder.Append("			ORIGINAL_DOC3					,").AppendLine();
			oSqlBuilder.Append("			ORIGINAL_DOC4					,").AppendLine();
			oSqlBuilder.Append("			ORIGINAL_DOC5					,").AppendLine();
			oSqlBuilder.Append("			READ_FLAG						,").AppendLine();
			oSqlBuilder.Append("			TX_DEL_FLAG						,").AppendLine();
			oSqlBuilder.Append("			RX_DEL_FLAG						,").AppendLine();
			oSqlBuilder.Append("			TX_SITE_CD						,").AppendLine();
			oSqlBuilder.Append("			TX_USER_SEQ						,").AppendLine();
			oSqlBuilder.Append("			TX_USER_CHAR_NO					,").AppendLine();
			oSqlBuilder.Append("			TX_MAIL_BOX_TYPE				,").AppendLine();
			oSqlBuilder.Append("			TX_SEX_CD						,").AppendLine();
			oSqlBuilder.Append("			TX_LOGIN_ID						,").AppendLine();
			oSqlBuilder.Append("			RX_SITE_CD						,").AppendLine();
			oSqlBuilder.Append("			RX_USER_SEQ						,").AppendLine();
			oSqlBuilder.Append("			RX_USER_CHAR_NO					,").AppendLine();
			oSqlBuilder.Append("			RX_MAIL_BOX_TYPE				,").AppendLine();
			oSqlBuilder.Append("			RX_SEX_CD						,").AppendLine();
			oSqlBuilder.Append("			RX_LOGIN_ID						,").AppendLine();
			oSqlBuilder.Append("			SERVICE_POINT					,").AppendLine();
			oSqlBuilder.Append("			SERVICE_POINT_EFFECTIVE_DATE	,").AppendLine();
			oSqlBuilder.Append("			SERVICE_POINT_TRANSFER_FLAG		,").AppendLine();
			oSqlBuilder.Append("			BATCH_MAIL_LOG_SEQ				,").AppendLine();
			oSqlBuilder.Append("			BATCH_MAIL_FLAG					,").AppendLine();
			oSqlBuilder.Append("			ATTACHED_OBJ_TYPE				,").AppendLine();
			oSqlBuilder.Append("			MOVIE_SEQ						,").AppendLine();
			oSqlBuilder.Append("			PIC_SEQ							,").AppendLine();
			oSqlBuilder.Append("			WAIT_TX_FLAG					,").AppendLine();
			oSqlBuilder.Append("			ATTACHED_OBJ_OPEN_FLAG			,").AppendLine();
			oSqlBuilder.Append("			RETURN_MAIL_FLAG				,").AppendLine();
			oSqlBuilder.Append("			DEL_PROTECT_FLAG				").AppendLine();

			string sWhere = string.Empty;
			OracleParameter[] oParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pPartnerLoginId,pPartnerUserCharNo,pReportDayTimeFrom,pReportDayTimeTo,pAttachedType,pWithBatchMailFlag,pTxRxType,ref sWhere);

			oSqlBuilder.Append(" FROM VW_MAIL_LOG00").AppendLine();
			oSqlBuilder.Append(sWhere).AppendLine();
			oSqlBuilder.Append(sOrder).AppendLine();
			oSqlBuilder.Append(")INNER WHERE ROWNUM <= :MAX_ROW ").AppendLine();
			oSqlBuilder.Append(")T1 ");
			oSqlBuilder.Append("," + sJoinTable + " J1").AppendLine();
			oSqlBuilder.Append(",T_SITE_MANAGEMENT S1").AppendLine();
			oSqlBuilder.Append("WHERE RNUM >= :FIRST_ROW AND RNUM <= :LAST_ROW ").AppendLine();
			oSqlBuilder.Append("AND T1.SITE_CD = J1.SITE_CD AND T1.PARTNER_USER_SEQ = J1.USER_SEQ AND T1.PARTNER_USER_CHAR_NO = J1.USER_CHAR_NO AND T1.SITE_CD = S1.SITE_CD").AppendLine();
			oSqlBuilder.Append(sOrderLast).AppendLine();

			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				for (int i = 0;i < 7;i++) {
					cmd.Parameters.Add(new OracleParameter(string.Format("ONW_USER_SEQ{0}",i + 1),pUserSeq));
				}

				for (int i = 0;i < oParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)oParms[i]);
				}
				cmd.Parameters.Add("MAX_ROW",startRowIndex + maximumRows);
				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"MAIL_BOX");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pPartnerLoginId,
		string pPartnerUserCharNo,
		string pReportDayTimeFrom,
		string pReportDayTimeTo,
		string pAttachedType,
		string pWithBatchMail,
		string pTxRxType,
		ref string pWhere
	) {
		ArrayList list = new ArrayList();
		if (pTxRxType.Equals(ViCommConst.RX)) {
			CreateRxMailWhere(pSiteCd,pUserSeq,pUserCharNo,pPartnerLoginId,pPartnerUserCharNo,pReportDayTimeFrom,pReportDayTimeTo,pAttachedType,ref list,ref pWhere);
		} else if (pTxRxType.Equals(ViCommConst.TX)) {
			CreateTxMailWhere(pSiteCd,pUserSeq,pUserCharNo,pPartnerLoginId,pPartnerUserCharNo,pReportDayTimeFrom,pReportDayTimeTo,pAttachedType,pWithBatchMail,ref list,ref pWhere);
		} else {
			CreateBothMailWhere(pSiteCd,pUserSeq,pUserCharNo,pPartnerLoginId,pPartnerUserCharNo,pReportDayTimeFrom,pReportDayTimeTo,pAttachedType,pWithBatchMail,ref list,ref pWhere);
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	private void CreateRxMailWhere(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerLoginId,string pPartnerUserCharNo,string pReportDayTimeFrom,string pReportDayTimeTo,string pAttachedType,ref ArrayList pParmList,ref string pWhere) {
		StringBuilder sWhere = new StringBuilder();
		sWhere.Append("RX_SITE_CD = :RX_SITE_CD AND RX_USER_SEQ = :RX_USER_SEQ AND RX_USER_CHAR_NO = :RX_USER_CHAR_NO AND RX_MAIL_BOX_TYPE	= :RX_MAIL_BOX_TYPE AND WAIT_TX_FLAG = :WAIT_TX_FLAG ").AppendLine();
		sWhere.Append("AND CREATE_DATE >= :CREATE_DATE_FROM AND CREATE_DATE < :CREATE_DATE_TO").AppendLine();

		pParmList.Add(new OracleParameter("RX_SITE_CD",pSiteCd));
		pParmList.Add(new OracleParameter("RX_USER_SEQ",pUserSeq));
		pParmList.Add(new OracleParameter("RX_USER_CHAR_NO",pUserCharNo));
		pParmList.Add(new OracleParameter("RX_MAIL_BOX_TYPE",ViCommConst.MAIL_BOX_USER));
		pParmList.Add(new OracleParameter("WAIT_TX_FLAG",ViCommConst.FLAG_OFF));

		DateTime dtFrom = DateTime.Parse(pReportDayTimeFrom + ":00:00");
		DateTime dtTo = DateTime.Parse(pReportDayTimeTo + ":59:59").AddSeconds(1);
		pParmList.Add(new OracleParameter("CREATE_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
		pParmList.Add(new OracleParameter("CREATE_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));

		if (!pAttachedType.Equals(string.Empty)) {
			sWhere.Append("AND ATTACHED_OBJ_TYPE IN (");
			int iIndex = 0;
			foreach (string sValue in pAttachedType.Split(',')) {
				string sParamName = string.Format(":ATTACHED_OBJ_TYPE{0}",iIndex);
				if (iIndex != 0) {
					sWhere.Append(",");
				}
				sWhere.Append(sParamName);
				pParmList.Add(new OracleParameter(sParamName,sValue));
				iIndex += 1;
			}
			sWhere.Append(")").AppendLine();
		}

		if (!pPartnerLoginId.Equals(string.Empty)) {
			sWhere.Append("AND(TX_LOGIN_ID = :TX_PARTNER_LOGIN_ID)");
			pParmList.Add(new OracleParameter("TX_PARTNER_LOGIN_ID",pPartnerLoginId));
		}

		SysPrograms.SqlAppendWhere(sWhere.ToString(),ref pWhere);
	}

	private void CreateTxMailWhere(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerLoginId,string pPartnerUserCharNo,string pReportDayTimeFrom,string pReportDayTimeTo,string pAttachedType,string pWithBatchMail,ref ArrayList pParmList,ref string pWhere) {
		StringBuilder sWhere = new StringBuilder();
		sWhere.Append("TX_SITE_CD = :TX_SITE_CD AND TX_USER_SEQ = :TX_USER_SEQ AND TX_USER_CHAR_NO = :TX_USER_CHAR_NO AND TX_MAIL_BOX_TYPE	= :TX_MAIL_BOX_TYPE ").AppendLine();
		if (pWithBatchMail != string.Empty) {
			sWhere.Append("AND BATCH_MAIL_FLAG = :BATCH_MAIL_FLAG ");
		} else {
			sWhere.Append("AND (BATCH_MAIL_FLAG IN (:BATCH_MAIL_FLAG_OFF,:BATCH_MAIL_FLAG_ON)) ");
		}

		sWhere.Append("AND CREATE_DATE >= :CREATE_DATE_FROM AND CREATE_DATE < :CREATE_DATE_TO").AppendLine();

		pParmList.Add(new OracleParameter("TX_SITE_CD",pSiteCd));
		pParmList.Add(new OracleParameter("TX_USER_SEQ",pUserSeq));
		pParmList.Add(new OracleParameter("TX_USER_CHAR_NO",pUserCharNo));
		pParmList.Add(new OracleParameter("TX_MAIL_BOX_TYPE",ViCommConst.MAIL_BOX_USER));

		if (pWithBatchMail != string.Empty) {
			pParmList.Add(new OracleParameter("BATCH_MAIL_FLAG",pWithBatchMail));
		} else {
			pParmList.Add(new OracleParameter("BATCH_MAIL_FLAG_OFF",ViCommConst.FLAG_OFF));
			pParmList.Add(new OracleParameter("BATCH_MAIL_FLAG_ON",ViCommConst.FLAG_ON));
		}

		DateTime dtFrom = DateTime.Parse(pReportDayTimeFrom + ":00:00");
		DateTime dtTo = DateTime.Parse(pReportDayTimeTo + ":59:59").AddSeconds(1);
		pParmList.Add(new OracleParameter("CREATE_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
		pParmList.Add(new OracleParameter("CREATE_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));

		if (!pAttachedType.Equals(string.Empty)) {
			sWhere.Append("AND ATTACHED_OBJ_TYPE IN (");
			int iIndex = 0;
			foreach (string sValue in pAttachedType.Split(',')) {
				string sParamName = string.Format(":ATTACHED_OBJ_TYPE{0}",iIndex);
				if (iIndex != 0) {
					sWhere.Append(",");
				}
				sWhere.Append(sParamName);
				pParmList.Add(new OracleParameter(sParamName,sValue));
				iIndex += 1;
			}
			sWhere.Append(")").AppendLine();
		}


		if (!pPartnerLoginId.Equals(string.Empty)) {
			sWhere.Append("AND(RX_LOGIN_ID = :RX_PARTNER_LOGIN_ID)");
			pParmList.Add(new OracleParameter("RX_PARTNER_LOGIN_ID",pPartnerLoginId));
		}

		SysPrograms.SqlAppendWhere(sWhere.ToString(),ref pWhere);
	}

	private void CreateBothMailWhere(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerLoginId,string pPartnerUserCharNo,string pReportDayTimeFrom,string pReportDayTimeTo,string pAttachedType,string pWithBatchMail,ref ArrayList pParmList,ref string pWhere) {

		StringBuilder sWhere = new StringBuilder();
		sWhere.Append("(").AppendLine();
		sWhere.Append("(TX_SITE_CD = :TX_SITE_CD AND TX_USER_SEQ = :TX_USER_SEQ AND TX_USER_CHAR_NO = :TX_USER_CHAR_NO AND TX_MAIL_BOX_TYPE	= :TX_MAIL_BOX_TYPE ").AppendLine();
		if (pWithBatchMail != string.Empty) {
			sWhere.Append("AND BATCH_MAIL_FLAG = :BATCH_MAIL_FLAG ) OR");
		} else {
			sWhere.Append("AND (BATCH_MAIL_FLAG IN (:BATCH_MAIL_FLAG_OFF,:BATCH_MAIL_FLAG_ON))) OR ");
		}
		sWhere.Append("(RX_SITE_CD = :RX_SITE_CD AND RX_USER_SEQ = :RX_USER_SEQ AND RX_USER_CHAR_NO = :RX_USER_CHAR_NO AND RX_MAIL_BOX_TYPE	= :RX_MAIL_BOX_TYPE )").AppendLine();
		sWhere.Append(") AND CREATE_DATE >= :CREATE_DATE_FROM AND CREATE_DATE < :CREATE_DATE_TO").AppendLine();

		pParmList.Add(new OracleParameter("TX_SITE_CD",pSiteCd));
		pParmList.Add(new OracleParameter("TX_USER_SEQ",pUserSeq));
		pParmList.Add(new OracleParameter("TX_USER_CHAR_NO",pUserCharNo));
		pParmList.Add(new OracleParameter("TX_MAIL_BOX_TYPE",ViCommConst.MAIL_BOX_USER));
		if (pWithBatchMail != string.Empty) {
			pParmList.Add(new OracleParameter("BATCH_MAIL_FLAG",pWithBatchMail));
		} else {
			pParmList.Add(new OracleParameter("BATCH_MAIL_FLAG_OFF",ViCommConst.FLAG_OFF));
			pParmList.Add(new OracleParameter("BATCH_MAIL_FLAG_ON",ViCommConst.FLAG_ON));
		}


		pParmList.Add(new OracleParameter("RX_SITE_CD",pSiteCd));
		pParmList.Add(new OracleParameter("RX_USER_SEQ",pUserSeq));
		pParmList.Add(new OracleParameter("RX_USER_CHAR_NO",pUserCharNo));
		pParmList.Add(new OracleParameter("RX_MAIL_BOX_TYPE",ViCommConst.MAIL_BOX_USER));

		DateTime dtFrom = DateTime.Parse(pReportDayTimeFrom + ":00:00");
		DateTime dtTo = DateTime.Parse(pReportDayTimeTo + ":59:59").AddSeconds(1);
		pParmList.Add(new OracleParameter("CREATE_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
		pParmList.Add(new OracleParameter("CREATE_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));

		if (!pAttachedType.Equals(string.Empty)) {
			sWhere.Append("AND ATTACHED_OBJ_TYPE IN (");
			int iIndex = 0;
			foreach (string sValue in pAttachedType.Split(',')) {
				string sParamName = string.Format(":ATTACHED_OBJ_TYPE{0}",iIndex);
				if (iIndex != 0) {
					sWhere.Append(",");
				}
				sWhere.Append(sParamName);
				pParmList.Add(new OracleParameter(sParamName,sValue));
				iIndex += 1;
			}
			sWhere.Append(")").AppendLine();
		}

		if (!pPartnerLoginId.Equals(string.Empty)) {
			sWhere.Append("AND(RX_LOGIN_ID = :RX_PARTNER_LOGIN_ID OR TX_LOGIN_ID = :TX_PARTNER_LOGIN_ID)");
			pParmList.Add(new OracleParameter("RX_PARTNER_LOGIN_ID",pPartnerLoginId));
			pParmList.Add(new OracleParameter("TX_PARTNER_LOGIN_ID",pPartnerLoginId));
		}

		SysPrograms.SqlAppendWhere(sWhere.ToString(),ref pWhere);
	}

	public void UpdateMailDel(string pMailSeq,string pTxRxType) {
		string[] mailSeqList = new string[] { pMailSeq };
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_MAIL_DEL");
			db.ProcedureInArrayParm("PMAIL_SEQ",DbSession.DbType.VARCHAR2,mailSeqList.Length,mailSeqList);
			db.ProcedureInParm("PMAIL_SEQ_COUNT",DbSession.DbType.NUMBER,mailSeqList.Length);
			db.ProcedureInParm("PTX_RX_TYPE",DbSession.DbType.VARCHAR2,pTxRxType);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}
}
