﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: ﾋﾞﾝｺﾞｴﾝﾄﾘｰ

--	Progaram ID		: BingoEntry
--
--  Creation Date	: 2011.07.06
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater   Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using iBridCommLib;
using ViComm;

public class BingoEntry:DbSession {

	public int bingoCardNo;
	public int bingoCharangeCount;
	public int bingoCompliateJackpot;
	public int bingoElectionAmt;
	public int bingoElectionPoint;
	public int bingoPointPaymentFlag;
	public int bingoTermSeq;
	public string bingoEntryDate;
	public string bingoApplicationDate;

	public BingoEntry() {
		bingoCardNo = 0;
		bingoCharangeCount = 0;
		bingoCompliateJackpot = 0;
		bingoElectionAmt = 0;
		bingoElectionPoint = 0;
		bingoPointPaymentFlag = 0;
		bingoTermSeq = 0;
		bingoEntryDate = string.Empty;
		bingoApplicationDate = string.Empty;
	}

	public DataSet GetPageCollection(string pSiteCd,string pBingoTermSeq,string pLoginId,string pSexCd,string pSortExpression,string pSortDirection,int startRowIndex,int maximumRows) {

		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			StringBuilder oSql = new StringBuilder();

			string sOrder = string.Format("ORDER BY SITE_CD,BINGO_TERM_SEQ,{0}BINGO_APPLICATION_DATE DESC",
				string.IsNullOrEmpty(pSortExpression) || string.IsNullOrEmpty(pSortDirection) ? string.Empty : string.Format("{0} {1},",pSortExpression,pSortDirection));
			oSql.AppendLine("SELECT ");
			oSql.AppendLine("	SITE_CD						,	");
			oSql.AppendLine("	USER_SEQ					,	");
			oSql.AppendLine("	USER_CHAR_NO				,	");
			oSql.AppendLine("	SEX_CD						,	");
			oSql.AppendLine("	HANDLE_NM					,	");
			oSql.AppendLine("	LOGIN_ID					,	");
			oSql.AppendLine("	BINGO_TERM_SEQ				,	");
			oSql.AppendLine("	BINGO_ENTRY_SEQ				,	");
			oSql.AppendLine("	BINGO_POINT_PAYMENT_FLAG	,	");
			oSql.AppendLine("	BINGO_CARD_NO				,	");
			oSql.AppendLine("	BINGO_ELECTION_POINT		,	");
			oSql.AppendLine("	BINGO_APPLICATION_DATE		,	");
			oSql.AppendLine("	BINGO_BALL_FLAG				,	");
			oSql.AppendLine("	RNUM							");
			oSql.AppendLine("FROM(");
			oSql.AppendFormat(" SELECT {0}.*, ROW_NUMBER() OVER ({1}) AS RNUM FROM {0} ",GetViewName(pSexCd),sOrder).AppendLine();

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pBingoTermSeq,pLoginId,ref sWhere);
			oSql.AppendLine(sWhere);

			oSql.AppendLine(")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ");
			oSql.AppendLine(sOrder);

			using (cmd = CreateSelectCommand(oSql.ToString(),conn)) {
				cmd.BindByName = true;
				for (int i = 0; i < objParms.Length; i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,GetViewName(pSexCd));
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pBingoTermSeq,string pLoginId,ref string pWhere) {
		pWhere = "";

		ArrayList list = new ArrayList();
		SysPrograms.SqlAppendWhere(" SITE_CD = :SITE_CD ",ref pWhere);
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		SysPrograms.SqlAppendWhere(" BINGO_TERM_SEQ = :BINGO_TERM_SEQ ",ref pWhere);
		list.Add(new OracleParameter("BINGO_TERM_SEQ",pBingoTermSeq));
		SysPrograms.SqlAppendWhere(" BINGO_APPLICATION_FLAG = :BINGO_APPLICATION_FLAG ",ref pWhere);
		list.Add(new OracleParameter("BINGO_APPLICATION_FLAG",ViCommConst.FLAG_ON));

		if (!pLoginId.Equals(string.Empty)) {
			SysPrograms.SqlAppendWhere(" LOGIN_ID = :LOGIN_ID ",ref pWhere);
			list.Add(new OracleParameter("LOGIN_ID",pLoginId));
		}
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public int GetPageCount(string pSiteCd,string pBingoTermSeq,string pLoginId,string pSexCd,string pSortExpression,string pSortDirection) {

		int iPageCount = 0;
		try {
			conn = DbConnect();
			DataSet ds;

			StringBuilder sSql = new StringBuilder();

			sSql.AppendLine("SELECT ");
			sSql.AppendLine("   COUNT(*) AS ROW_COUNT ");
			sSql.AppendLine("FROM ");
			sSql.AppendFormat("   {0} ",this.GetViewName(pSexCd)).AppendLine();
			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pBingoTermSeq,pLoginId,ref sWhere);
			sSql.AppendLine(sWhere);

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					using (ds = new DataSet()) {
						for (int i = 0;i < objParms.Length;i++) {
							cmd.Parameters.Add((OracleParameter)objParms[i]);
						}
						da.Fill(ds);
						iPageCount = Convert.ToInt32(ds.Tables[0].Rows[0]["ROW_COUNT"]);
					}
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	private string GetViewName(string pSexCd) {
		string sViewName = string.Empty;
		if (pSexCd.Equals(ViCommConst.MAN)) {
			sViewName = "VW_BINGO_ENTRY01";
		} else {
			sViewName = "VW_BINGO_ENTRY02";
		}
		return sViewName;
	}

	public int GetEntryCount(string pSiteCd,string pBingoTermSeq) {

		int iPageCount = 0;
		try {
			conn = DbConnect();
			DataSet ds;

			StringBuilder sSql = new StringBuilder();

			sSql.AppendLine("SELECT	");
			sSql.AppendLine("   COUNT(*) AS ROW_COUNT ");
			sSql.AppendLine("FROM	");
			sSql.AppendLine("   T_BINGO_ENTRY	");
			sSql.AppendLine("WHERE	");
			sSql.AppendLine("	SITE_CD			= :SITE_CD			AND	");
			sSql.AppendLine("	BINGO_TERM_SEQ	= :BINGO_TERM_SEQ	AND	");
			sSql.AppendLine("	BINGO_CARD_NO	= :BINGO_CARD_NO		");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					using (ds = new DataSet()) {
						cmd.Parameters.Add("SITE_CD",pSiteCd);
						cmd.Parameters.Add("BINGO_TERM_SEQ",pBingoTermSeq);
						cmd.Parameters.Add("BINGO_CARD_NO",1);

						da.Fill(ds);
						iPageCount = Convert.ToInt32(ds.Tables[0].Rows[0]["ROW_COUNT"]);
					}
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public void GetSumElection(string pSiteCd,string pBingoTermSeq,out int pElectionAmt,out int pElectionPoint) {
		pElectionAmt = 0;
		pElectionPoint = 0;
		try {
			conn = DbConnect();
			DataSet ds;

			StringBuilder sSql = new StringBuilder();

			sSql.AppendLine("SELECT	");
			sSql.AppendLine("   NVL(SUM(BINGO_ELECTION_AMT),0)		AS BINGO_ELECTION_AMT	,");
			sSql.AppendLine("   NVL(SUM(BINGO_ELECTION_POINT),0)	AS BINGO_ELECTION_POINT	 ");
			sSql.AppendLine("FROM	");
			sSql.AppendLine("   T_BINGO_ENTRY	");
			sSql.AppendLine("WHERE	");
			sSql.AppendLine("	SITE_CD			= :SITE_CD			AND	");
			sSql.AppendLine("	BINGO_TERM_SEQ	= :BINGO_TERM_SEQ		");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					using (ds = new DataSet()) {
						cmd.Parameters.Add("SITE_CD",pSiteCd);
						cmd.Parameters.Add("BINGO_TERM_SEQ",pBingoTermSeq);

						da.Fill(ds);

						if (ds.Tables[0].Rows.Count != 0) {
							pElectionAmt = Convert.ToInt32(ds.Tables[0].Rows[0]["BINGO_ELECTION_AMT"]);
							pElectionPoint = Convert.ToInt32(ds.Tables[0].Rows[0]["BINGO_ELECTION_POINT"]);
						}
					}
				}
			}
		} finally {
			conn.Close();
		}
	}

	public bool GetOne(string pBingoEntrySeq) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect("BingoEntry.GetOne");

			using (cmd = CreateSelectCommand("SELECT * FROM T_BINGO_ENTRY WHERE BINGO_ENTRY_SEQ = :BINGO_ENTRY_SEQ",conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("BINGO_ENTRY_SEQ",pBingoEntrySeq);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_BINGO_ENTRY");
					if (ds.Tables["T_BINGO_ENTRY"].Rows.Count != 0) {
						dr = ds.Tables["T_BINGO_ENTRY"].Rows[0];
						bingoCardNo = int.Parse(dr["BINGO_CARD_NO"].ToString());
						bingoCharangeCount = int.Parse(dr["BINGO_CHARANGE_COUNT"].ToString());
						bingoCompliateJackpot = int.Parse(dr["BINGO_COMPLIATE_JACKPOT"].ToString());
						bingoElectionAmt = int.Parse(dr["BINGO_ELECTION_AMT"].ToString());
						bingoElectionPoint = int.Parse(dr["BINGO_ELECTION_POINT"].ToString());
						bingoPointPaymentFlag = int.Parse(dr["BINGO_POINT_PAYMENT_FLAG"].ToString());
						bingoTermSeq = int.Parse(dr["BINGO_TERM_SEQ"].ToString());
						bingoEntryDate = dr["BINGO_ENTRY_DATE"].ToString();
						bingoApplicationDate = dr["BINGO_APPLICATION_DATE"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	/// <summary>
	/// CSV出力用データ取得
	/// </summary>
	/// <param name="pSiteCd"></param>
	/// <param name="pBingoTermSeq"></param>
	/// <param name="pLoginId"></param>
	/// <param name="pSexCd"></param>
	/// <param name="pSortExpression"></param>
	/// <param name="pSortDirection"></param>
	/// <returns></returns>
	public DataSet GetCSVList(
		string pSiteCd
		,string pBingoTermSeq
		,string pLoginId
		,string pSexCd
		,string pSortExpression
		,string pSortDirection
	) {
		string sWhereClause = "";
		StringBuilder oSql = new StringBuilder();
		List<OracleParameter> oParamList = new List<OracleParameter>();

		oSql.AppendLine("SELECT ");
		oSql.AppendLine("	SITE_CD");
		oSql.AppendLine("	,USER_SEQ");
		oSql.AppendLine("	,USER_CHAR_NO");
		oSql.AppendLine("	,SEX_CD");
		oSql.AppendLine("	,HANDLE_NM");
		oSql.AppendLine("	,LOGIN_ID");
		oSql.AppendLine("	,BINGO_TERM_SEQ");
		oSql.AppendLine("	,BINGO_ENTRY_SEQ");
		oSql.AppendLine("	,BINGO_POINT_PAYMENT_FLAG");
		oSql.AppendLine("	,BINGO_CHARANGE_COUNT");
		oSql.AppendLine("	,BINGO_CARD_NO");
		oSql.AppendLine("	,BINGO_ELECTION_POINT");
		oSql.AppendLine("	,BINGO_APPLICATION_DATE");
		oSql.AppendLine("	,BINGO_BALL_FLAG");
		oSql.AppendLine("FROM");
		oSql.AppendFormat("   {0}",this.GetViewName(pSexCd)).AppendLine();

		// 検索条件
		oParamList.AddRange(CreateWhere(pSiteCd,pBingoTermSeq,pLoginId,ref sWhereClause));
		oSql.AppendLine(sWhereClause);

		// ソート
		oSql.AppendLine(string.Format(
			"ORDER BY SITE_CD,BINGO_TERM_SEQ,{0}BINGO_APPLICATION_DATE DESC"
			,string.IsNullOrEmpty(pSortExpression) || string.IsNullOrEmpty(pSortDirection)
				? string.Empty : string.Format("{0} {1},",pSortExpression,pSortDirection)
		));

		return ExecuteSelectQueryBase(oSql.ToString(),oParamList.ToArray());
	}
}
