﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 携帯機種別対応動画ﾌｧｲﾙ種別

--	Progaram ID		: ModelMovieType
--
--  Creation Date	: 2010.08.10
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class ModelMovieType:DbSession
{
    public ModelMovieType()
    {
	}

    public int GetPageCount()
    {
        DataSet ds;
        DataRow dr;
        int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_MODEL_MOVIE_TYPE01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet())
			{
				for (int i = 0; i < objParms.Length; i++)
				{
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0)
				{
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
        return iPageCount;
    }

    public DataSet GetPageCollection(int startRowIndex, int maximumRows)
    {
        DataSet ds;
        try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY MODE_MOVIE_TYPE_SEQ";
			string sSql = "SELECT " +
							"MODE_MOVIE_TYPE_SEQ    ," +
							"MOVIE_FILE_TYPE	    ," +
							"MOBILE_CARRIER_CD	    ," +
							"TARGET_USE_TYPE	    ," +
							"TARGET_USE_TYPE_NM	    ," +
							"MOBILE_MODEL		    ," +
							"MOBILE_MODEL_LOWER	    ," +
							"MOVIE_FILE_TYPE_NM	    ," +
							"MOBILE_CARRIER_NM	" +
							"FROM(" +
							" SELECT VW_MODEL_MOVIE_TYPE01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_MODEL_MOVIE_TYPE01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn))
			{
				for (int i = 0; i < objParms.Length; i++)
				{
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW", startRowIndex);
				cmd.Parameters.Add("LAST_ROW", startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd))
				{
					da.Fill(ds, "VW_MODEL_MOVIE_TYPE01");
				}
			}
		} finally {
			conn.Close();
		}
        return ds;
    }

    private OracleParameter[] CreateWhere(ref string pWhere)
    {
        ArrayList list = new ArrayList();
        return (OracleParameter[])list.ToArray(typeof(OracleParameter));
    }

	public DataSet GetList() {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT " +
							"MODE_MOVIE_TYPE_SEQ    ," +
							"MOVIE_FILE_TYPE        ," +
							"MOBILE_CARRIER_CD      ," +
							"TARGET_USE_TYPE        ," +
							"MOBILE_MODEL           ," +
							"MOBILE_MODEL_LOWER " +
							"FROM T_MODEL_MOVIE_TYPE " +
							" ORDER BY MODE_MOVIE_TYPE_SEQ ";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds, "T_MODEL_MOVIE_TYPE");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}
}
