﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: 掲示板スレッド
--	Progaram ID		: BbsThread
--  Creation Date	: 2011.04.08
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class BbsThread : DbSession {
	public BbsThread() {
	}

	public int GetPageCount(string pSiteCd, string pLoginId, string pTitle, string pBbsThreadSeq, string pAdminThreadFlag) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_BBS_THREAD			BT	,");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER		CC	,");
		oSqlBuilder.AppendLine("	T_USER					U	,");
		oSqlBuilder.AppendLine("	T_CODE_DTL				CD	");

		string sWhereClause;
		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd, pLoginId, pTitle, pBbsThreadSeq, pAdminThreadFlag, out sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(), conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}


	public DataSet GetPageCollection(string pSiteCd, string pLoginId, string pTitle, string pBbsThreadSeq, string pAdminThreadFlag, int startRowIndex, int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	BT.SITE_CD					,");
		oSqlBuilder.AppendLine("	BT.USER_SEQ					,");
		oSqlBuilder.AppendLine("	BT.USER_CHAR_NO				,");
		oSqlBuilder.AppendLine("	BT.BBS_THREAD_SEQ			,");
		oSqlBuilder.AppendLine("	BT.BBS_THREAD_HANDLE_NM		,");
		oSqlBuilder.AppendLine("	BT.BBS_THREAD_TITLE			,");
		oSqlBuilder.AppendLine("	BT.BBS_THREAD_DOC1			,");
		oSqlBuilder.AppendLine("	BT.ADMIN_THREAD_FLAG		,");
		oSqlBuilder.AppendLine("	BT.THREAD_TYPE				,");
		oSqlBuilder.AppendLine("	BT.DEL_FLAG					,");
		oSqlBuilder.AppendLine("	BT.CREATE_DATE				,");
		oSqlBuilder.AppendLine("	BT.UPDATE_DATE				,");
		oSqlBuilder.AppendLine("	BT.REVISION_NO				,");
		oSqlBuilder.AppendLine("	CC.HANDLE_NM				,");
		oSqlBuilder.AppendLine("	U.LOGIN_ID					,");
		oSqlBuilder.AppendLine("	CD.CODE_NM	THREAD_TYPE_NM	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_BBS_THREAD			BT	,");
		oSqlBuilder.AppendLine("	T_CAST_CHARACTER		CC	,");
		oSqlBuilder.AppendLine("	T_USER					U	,");
		oSqlBuilder.AppendLine("	T_CODE_DTL				CD	");

		string sWhereClause;
		string sSortExpression = "ORDER BY BT.SITE_CD, NVL(BT.UPDATE_DATE,'1900/01/01') DESC, BT.CREATE_DATE DESC";
		string sPagingSql;

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd, pLoginId, pTitle, pBbsThreadSeq, pAdminThreadFlag, out sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(), sSortExpression, startRowIndex, maximumRows, out sPagingSql);

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql, conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}

	private OracleParameter[] CreateWhere(string pSiteCd, string pLoginId, string pTitle, string pBbsThreadSeq, string pAdminThreadFlag, out string pWhere) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("WHERE	");
		oSqlBuilder.AppendLine("	BT.SITE_CD			= CC.SITE_CD			AND");
		oSqlBuilder.AppendLine("	BT.USER_SEQ			= CC.USER_SEQ			AND");
		oSqlBuilder.AppendLine("	BT.USER_CHAR_NO		= CC.USER_CHAR_NO		AND");
		oSqlBuilder.AppendLine("	BT.USER_SEQ			= U.USER_SEQ			AND");
		oSqlBuilder.AppendLine("	BT.THREAD_TYPE		= CD.CODE			(+)	AND");
		oSqlBuilder.AppendLine("	CD.CODE_TYPE		= '09'					");

		pWhere = oSqlBuilder.ToString();

		List<OracleParameter> oOracleParameterList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("BT.SITE_CD = :SITE_CD", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("SITE_CD", pSiteCd));
		}

		if (!string.IsNullOrEmpty(pLoginId)) {
			SysPrograms.SqlAppendWhere("U.LOGIN_ID = :LOGIN_ID", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("LOGIN_ID", pLoginId));
		}

		if (!string.IsNullOrEmpty(pTitle)) {
			SysPrograms.SqlAppendWhere("BT.BBS_THREAD_TITLE LIKE '%' || :BBS_THREAD_TITLE || '%'", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("BBS_THREAD_TITLE", pTitle));
		}

		if (!string.IsNullOrEmpty(pBbsThreadSeq)) {
			SysPrograms.SqlAppendWhere("BT.BBS_THREAD_SEQ = :BBS_THREAD_SEQ", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("BBS_THREAD_SEQ", pBbsThreadSeq));
		}

		if (!string.IsNullOrEmpty(pAdminThreadFlag) && ViCommConst.FLAG_ON_STR.Equals(pAdminThreadFlag)) {
			SysPrograms.SqlAppendWhere("BT.ADMIN_THREAD_FLAG = :ADMIN_THREAD_FLAG", ref pWhere);
			oOracleParameterList.Add(new OracleParameter("ADMIN_THREAD_FLAG", pAdminThreadFlag));
		}

		return oOracleParameterList.ToArray();
	}
}
