﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メール文章ドメイン設定
--	Progaram ID		: MailBodyDomainSettings
--
--  Creation Date	: 2010.07.05
--  Creater			: Kazuaki.Itoh@iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using iBridCommLib;
using ViComm;

/// <summary>MAIL_BODY_DOMAIN_SETTINGS テーブルへのデータアクセスを提供するクラス。</summary>
public class MailBodyDomainSettings:DbSession {
	public MailBodyDomainSettings() {
	}

	/// <summary>ｻｲﾄｺｰﾄﾞに該当するメール文章ドメイン設定の件数を取得する</summary>
	/// <param name="pSiteCd">ｻｲﾄｺｰﾄﾞ</param>
	/// <returns>ｻｲﾄｺｰﾄﾞに該当するメール文章ドメイン設定の件数</returns>
	public int GetPageCount(string pSiteCd) {
		int iPageCount = 0;

		string sSql = "SELECT COUNT(*) FROM T_MAIL_BODY_DOMAIN_SETTINGS ";
		string sWhere = "";

		OracleParameter[] objParms = this.CreateWhere(pSiteCd,ref sWhere);
		sSql = sSql + sWhere;

		using (this.cmd = CreateSelectCommand(sSql,conn)) {
			foreach (OracleParameter objParam in objParms) {
				this.cmd.Parameters.Add(objParam);
			}

			try {
				this.conn = DbConnect();
				iPageCount = Convert.ToInt32((this.cmd.ExecuteScalar() as decimal?) ?? decimal.Zero);
			} finally {
				this.conn.Close();
			}
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,int startRowIndex,int maximumRows) {


		string sSql = string.Empty;
		string sWhere = string.Empty;
		string sSortExpression = "ORDER BY SITE_CD,MAIL_BODY_DOMAIN_SETTINGS_SEQ";

		StringBuilder objSqlBuilder = new StringBuilder();
		objSqlBuilder.Append(" SELECT                                                            ").AppendLine();
		objSqlBuilder.Append("     SITE_CD,                                                      ").AppendLine();
		objSqlBuilder.Append("     MAIL_BODY_DOMAIN_SETTINGS_SEQ,                                ").AppendLine();
		objSqlBuilder.Append("     REPLACE_DOMAIN,                                               ").AppendLine();
		objSqlBuilder.Append("     USED_NOW_FLAG,                                                ").AppendLine();
		objSqlBuilder.Append("     USED_RANDOM_FLAG,                                             ").AppendLine();
		objSqlBuilder.Append("     UPDATE_DATE,                                                  ").AppendLine();
		objSqlBuilder.Append("     REVISION_NO                                                   ").AppendLine();
		objSqlBuilder.Append(" FROM                                                              ").AppendLine();
		objSqlBuilder.Append("     T_MAIL_BODY_DOMAIN_SETTINGS                                   ").AppendLine();

		OracleParameter[] objParams = this.CreateWhere(pSiteCd,ref sWhere);
		objSqlBuilder.Append(sWhere);

		OracleParameter[] objPagingParams = ViCommPrograms.CreatePagingSql(objSqlBuilder.ToString(),sSortExpression,startRowIndex,maximumRows,out sSql);

		DataSet objDs = new DataSet();

		try {

			this.conn = this.DbConnect();

			using (this.cmd = CreateSelectCommand(sSql,conn))
			using (this.da = new OracleDataAdapter(this.cmd)) {

				foreach (OracleParameter objParam in objParams) {
					this.cmd.Parameters.Add(objParam);
				}
				foreach (OracleParameter objPagingParam in objPagingParams) {
					this.cmd.Parameters.Add(objPagingParam);
				}

				this.da.Fill(objDs,"T_MAIL_BODY_DOMAIN_SETTINGS");
			}
		} finally {
			this.conn.Close();
		}

		return objDs;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,ref string pWhere) {

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);

		List<OracleParameter> oParamList = new List<OracleParameter>();
		oParamList.Add(new OracleParameter("SITE_CD",pSiteCd));

		return (OracleParameter[])oParamList.ToArray();
	}


	public bool Exists(string pSiteCd,string pReplaceDomain) {

		bool bExists = false;

		StringBuilder objSqlBuilder = new StringBuilder();
		objSqlBuilder.Append(" SELECT                                  ").AppendLine();
		objSqlBuilder.Append("   COUNT(*)                              ").AppendLine();
		objSqlBuilder.Append(" FROM                                    ").AppendLine();
		objSqlBuilder.Append("   T_MAIL_BODY_DOMAIN_SETTINGS           ").AppendLine();
		objSqlBuilder.Append(" WHERE                                   ").AppendLine();
		objSqlBuilder.Append("   SITE_CD         =:SITE_CD	      AND  ").AppendLine();
		objSqlBuilder.Append("   REPLACE_DOMAIN  =:REPLACE_DOMAIN AND  ").AppendLine();
		objSqlBuilder.Append("   ROWNUM<=1                             ").AppendLine();

		try {
			conn = DbConnect();

			using (cmd = CreateSelectCommand(objSqlBuilder.ToString(),conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("REPLACE_DOMAIN",pReplaceDomain);

				bExists = ((cmd.ExecuteScalar() as decimal?) ?? decimal.Zero) > decimal.Zero;
			}
		} finally {
			conn.Close();
		}
		return bExists;
	}

	public DataSet GetList(string pSiteCd) {


		string sSql = string.Empty;
		string sWhere = string.Empty;
		string sSortExpression = " ORDER BY SITE_CD,MAIL_BODY_DOMAIN_SETTINGS_SEQ";

		StringBuilder objSqlBuilder = new StringBuilder();
		objSqlBuilder.Append(" SELECT                              ").AppendLine();
		objSqlBuilder.Append("     SITE_CD,                        ").AppendLine();
		objSqlBuilder.Append("     MAIL_BODY_DOMAIN_SETTINGS_SEQ,  ").AppendLine();
		objSqlBuilder.Append("     REPLACE_DOMAIN,                 ").AppendLine();
		objSqlBuilder.Append("     USED_NOW_FLAG,                  ").AppendLine();
		objSqlBuilder.Append("     UPDATE_DATE,                    ").AppendLine();
		objSqlBuilder.Append("     REVISION_NO                     ").AppendLine();
		objSqlBuilder.Append(" FROM                                ").AppendLine();
		objSqlBuilder.Append("     T_MAIL_BODY_DOMAIN_SETTINGS     ").AppendLine();

		OracleParameter[] objParams = this.CreateWhere(pSiteCd,ref sWhere);
		objSqlBuilder.Append(sWhere);
		objSqlBuilder.Append(sSortExpression);

		DataSet objDs = new DataSet();

		try {

			this.conn = this.DbConnect();

			using (this.cmd = CreateSelectCommand(objSqlBuilder.ToString(),conn))
			using (this.da = new OracleDataAdapter(this.cmd)) {

				foreach (OracleParameter objParam in objParams) {
					this.cmd.Parameters.Add(objParam);
				}

				this.da.Fill(objDs,"T_MAIL_BODY_DOMAIN_SETTINGS");
			}
		} finally {
			this.conn.Close();
		}

		return objDs;
	}
}
