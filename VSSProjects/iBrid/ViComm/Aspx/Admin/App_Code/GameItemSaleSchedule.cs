﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: ゲームアイテムセールスケジュール
--	Progaram ID		: GameItemSaleSchedule
--
--  Creation Date	: 2011.07.19
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;

using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using iBridCommLib;
using ViComm;


public class GameItemSaleSchedule : DbSession {

	public GameItemSaleSchedule() {
	}

	public int GetPageCount(string pSiteCd,string pSexCd) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_GAME_ITEM_SALE_SCHEDULE");

		string sWhereClause;
		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pSexCd,out sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		}
		finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pSexCd,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();

		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	SITE_CD							,");
		oSqlBuilder.AppendLine("	SALE_SCHEDULE_SEQ				,");
		oSqlBuilder.AppendLine("	SEX_CD							,");
		oSqlBuilder.AppendLine("	SALE_COMMENT					,");
		oSqlBuilder.AppendLine("	SALE_NOTICE_COMMENT				,");
		oSqlBuilder.AppendLine("	SALE_START_DATE					,");
		oSqlBuilder.AppendLine("	SALE_END_DATE					,");
		oSqlBuilder.AppendLine("	REMARKS							,");
		oSqlBuilder.AppendLine("	REVISION_NO						,");
		oSqlBuilder.AppendLine("	CREATE_DATE						,");
		oSqlBuilder.AppendLine("	UPDATE_DATE						");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_GAME_ITEM_SALE_SCHEDULE	");

		string sWhereClause;
		string sPagingSql;
		string sSortExpression = "ORDER BY SITE_CD, SALE_START_DATE DESC";

		OracleParameter[] oWhereParams = this.CreateWhere(pSiteCd,pSexCd,out sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		DataSet oDs = new DataSet();
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDs);
				}
			}
		}
		finally {
			conn.Close();
		}
		return oDs;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pSexCd,out string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();

		if (!string.IsNullOrEmpty(pSiteCd)) {
			SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}
		if (!string.IsNullOrEmpty(pSexCd)) {
			SysPrograms.SqlAppendWhere("SEX_CD = :SEX_CD",ref pWhere);
			list.Add(new OracleParameter("SEX_CD",pSexCd));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public bool IsDuplicateDate(string pSiteCd,string pSaleScheduleSeq,string pSexCd,DateTime pSaleStartDate,DateTime pSaleEndDate) {
		bool bDuplicate = false;
		try {
			conn = DbConnect();
			DataSet ds;
			StringBuilder sSql = new StringBuilder();
			sSql.AppendLine("SELECT ");
			sSql.AppendLine("   SALE_SCHEDULE_SEQ ");
			sSql.AppendLine("FROM ");
			sSql.AppendLine("   T_GAME_ITEM_SALE_SCHEDULE ");
			sSql.AppendLine("WHERE ");
			sSql.AppendLine("	SITE_CD				= :SITE_CD				AND ");
			sSql.AppendLine("	SEX_CD      		= :SEX_CD   			AND ");
			sSql.AppendLine("	((SALE_START_DATE>= :SALE_START_DATE1		AND	");
			sSql.AppendLine("	 SALE_START_DATE <= :SALE_END_DATE1)		OR	");
			sSql.AppendLine("	(SALE_END_DATE	>= :SALE_START_DATE2		AND	");
			sSql.AppendLine("	 SALE_END_DATE	<= :SALE_END_DATE2)			OR	");
			sSql.AppendLine("	(SALE_START_DATE <= :SALE_START_DATE3		AND	");
			sSql.AppendLine("	 SALE_END_DATE	>= :SALE_END_DATE3))			");
			if (!string.IsNullOrEmpty(pSaleScheduleSeq)) {
				sSql.AppendLine("	AND SALE_SCHEDULE_SEQ	!= :SALE_SCHEDULE_SEQ		"); 
			}
			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				using (da = new OracleDataAdapter(cmd)) {
					using (ds = new DataSet()) {
						cmd.Parameters.Add("SITE_CD",pSiteCd);
						cmd.Parameters.Add("SEX_CD",pSexCd);
						cmd.Parameters.Add("SALE_START_DATE1",OracleDbType.Date,pSaleStartDate,ParameterDirection.Input);
						cmd.Parameters.Add("SALE_END_DATE1",OracleDbType.Date,pSaleEndDate,ParameterDirection.Input);
						cmd.Parameters.Add("SALE_START_DATE2",OracleDbType.Date,pSaleStartDate,ParameterDirection.Input);
						cmd.Parameters.Add("SALE_END_DATE2",OracleDbType.Date,pSaleEndDate,ParameterDirection.Input);
						cmd.Parameters.Add("SALE_START_DATE3",OracleDbType.Date,pSaleStartDate,ParameterDirection.Input);
						cmd.Parameters.Add("SALE_END_DATE3",OracleDbType.Date,pSaleEndDate,ParameterDirection.Input);
						if (!string.IsNullOrEmpty(pSaleScheduleSeq)) {
							cmd.Parameters.Add("SALE_SCHEDULE_SEQ",pSaleScheduleSeq); 
						}
						da.Fill(ds);
						if (ds.Tables[0].Rows.Count != 0) {
							bDuplicate = true;
						}
					}
				}
			}
		}
		finally {
			conn.Close();
		}
		return bDuplicate;
	}
}
