﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 利用明細
--	Progaram ID		: UsedLog
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;
using System.Text;

public class UsedLog:DbSession {

	public UsedLog() {
	}

	public int GetPageCount(string pSiteCd,string pSexCd,string pLoginId,string pUserCharNo,string pPartnerLoginId,string pPartnerUserCharNo,string pTel,string pReportDayFrom,string pReportDayTo,string pChargeType,string pReportTimeFrom,string pReportTimeTo,string pCallResult,int pTalkSubType) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_USED_LOG01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pSexCd,pLoginId,pUserCharNo,pPartnerLoginId,pPartnerUserCharNo,pTel,pReportDayFrom,pReportDayTo,pChargeType,pReportTimeFrom,pReportTimeTo,pCallResult,pTalkSubType,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pSexCd,string pLoginId,string pUserCharNo,string pPartnerLoginId,string pPartnerUserCharNo,string pTel,string pReportDayFrom,string pReportDayTo,string pChargeType,string pReportTimeFrom,string pReportTimeTo,string pCallResult,int pTalkSubType,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY CHARGE_START_DATE DESC,CHARGE_END_DATE DESC ";
			string sSql = "SELECT " +
							"MAN_USER_SITE_CD		," +
							"CHARGE_START_DATE		," +
							"CHARGE_END_DATE		," +
							"MAN_LOGIN_ID			," +
							"CAST_LOGIN_ID			," +
							"USER_CHAR_NO			," +
							"CHARGE_SEC				," +
							"CHARGE_TYPE			," +
							"CHARGE_TYPE_NM			," +
							"CHARGE_POINT			," +
							"CHARGE_POINT_FREE_DIAL	," +
							"USE_FREE_DIAL_FLAG		," +
							"CONNECT_TYPE			," +
							"CONNECT_TYPE_NM		," +
							"TALK_TYPE				," +
							"TALK_TYPE_NM			," +
							"CHARGE_MIN				," +
							"PAYMENT_POINT			," +
							"INVITE_TALK_SERVICE_POINT," +
							"INVITE_TALK_SERVICE_MIN," +
							"OUTGOING_TERMINAL_ID	," +
							"USE_OTHER_SYS_INFO_FLAG," +
							"HANDLE_NM				," +
							"MAN_HANDLE_NM			," +
							"CASE					" +
				//男性待機 
							"	WHEN (CHARGE_TYPE IN(:CHARGE_TYPE1,:CHARGE_TYPE2,:CHARGE_TYPE3,:CHARGE_TYPE4) AND CALL_RESULT IN(:CALL_RESULT1,:CALL_RESULT2)) THEN" +
							"		REPLACE(T_ERROR.ERROR_ABBR,'{0}','女性会員') " +
							"	WHEN (CHARGE_TYPE IN(:CHARGE_TYPE5,:CHARGE_TYPE6,:CHARGE_TYPE7,:CHARGE_TYPE8) AND CALL_RESULT IN(:CALL_RESULT3,:CALL_RESULT4,:CALL_RESULT5,:CALL_RESULT11)) THEN" +
							"		REPLACE(T_ERROR.ERROR_ABBR,'{0}','男性会員') " +
				//女性待機 
							"	WHEN CALL_RESULT IN(:CALL_RESULT6,:CALL_RESULT7) THEN " +
							"		REPLACE(T_ERROR.ERROR_ABBR,'{0}','男性会員') " +
							"	WHEN CALL_RESULT IN(:CALL_RESULT8,:CALL_RESULT9,:CALL_RESULT10,:CALL_RESULT12) THEN " +
							"		REPLACE(T_ERROR.ERROR_ABBR,'{0}','女性会員') " +
							"	ELSE					" +
							"		T_ERROR.ERROR_ABBR " +
							"END AS CALL_RESULT_NM		" +
						"FROM(" +
							" SELECT VW_USED_LOG01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_USED_LOG01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pSexCd,pLoginId,pUserCharNo,pPartnerLoginId,pPartnerUserCharNo,pTel,pReportDayFrom,pReportDayTo,pChargeType,pReportTimeFrom,pReportTimeTo,pCallResult,pTalkSubType,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + "), T_ERROR WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW AND CAST_SITE_CD = T_ERROR.SITE_CD (+) AND 'CR' || CALL_RESULT = T_ERROR.ERROR_CD (+) ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("CHARGE_TYPE1",ViCommConst.CHARGE_CAST_TALK_PUBLIC);
				cmd.Parameters.Add("CHARGE_TYPE2",ViCommConst.CHARGE_CAST_TALK_WSHOT);
				cmd.Parameters.Add("CHARGE_TYPE3",ViCommConst.CHARGE_CAST_TALK_VOICE_WSHOT);
				cmd.Parameters.Add("CHARGE_TYPE4",ViCommConst.CHARGE_CAST_TALK_VOICE_PUBLIC);
				cmd.Parameters.Add("CALL_RESULT1",ViCommConst.CALL_RESULT_REQUESTER_DISCONN);
				cmd.Parameters.Add("CALL_RESULT2",ViCommConst.CALL_RESULT_NO_MACH);
				cmd.Parameters.Add("CHARGE_TYPE5",ViCommConst.CHARGE_CAST_TALK_PUBLIC);
				cmd.Parameters.Add("CHARGE_TYPE6",ViCommConst.CHARGE_CAST_TALK_WSHOT);
				cmd.Parameters.Add("CHARGE_TYPE7",ViCommConst.CHARGE_CAST_TALK_VOICE_WSHOT);
				cmd.Parameters.Add("CHARGE_TYPE8",ViCommConst.CHARGE_CAST_TALK_VOICE_PUBLIC);
				cmd.Parameters.Add("CALL_RESULT3",ViCommConst.CALL_RESULT_PARTNER_DISCONN);
				cmd.Parameters.Add("CALL_RESULT4",ViCommConst.CALL_RESULT_NO_ANSWER);
				cmd.Parameters.Add("CALL_RESULT5",ViCommConst.CALL_RESULT_BUSY);
				cmd.Parameters.Add("CALL_RESULT11",ViCommConst.CALL_RESULT_REFUSE);
				cmd.Parameters.Add("CALL_RESULT6",ViCommConst.CALL_RESULT_REQUESTER_DISCONN);
				cmd.Parameters.Add("CALL_RESULT7",ViCommConst.CALL_RESULT_NO_MACH);
				cmd.Parameters.Add("CALL_RESULT8",ViCommConst.CALL_RESULT_PARTNER_DISCONN);
				cmd.Parameters.Add("CALL_RESULT9",ViCommConst.CALL_RESULT_NO_ANSWER);
				cmd.Parameters.Add("CALL_RESULT10",ViCommConst.CALL_RESULT_BUSY);
				cmd.Parameters.Add("CALL_RESULT12",ViCommConst.CALL_RESULT_REFUSE);

				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_USED_LOG01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public void GetTotal(string pSiteCd,string pSexCd,string pLoginId,string pUserCharNo,string pPartnerLoginId,string pPartnerUserCharNo,string pTel,string pReportDayFrom,string pReportDayTo,string pChargeType,string pReportTimeFrom,string pReportTimeTo,string pCallResult,int pTalkSubType,
									out int pMin,out int pPoint,out int pFreeDialPoint,out int pInvitePoint,out int pPaymentPoint) {
		DataSet ds;
		DataRow dr;
		pMin = 0;
		pPoint = 0;
		pFreeDialPoint = 0;
		pInvitePoint = 0;
		pPaymentPoint = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT " +
								"NVL(SUM(CHARGE_MIN),0) AS MIN							, " +
								"NVL(SUM(CHARGE_POINT),0) AS POINT						, " +
								"NVL(SUM(CHARGE_POINT_FREE_DIAL),0) AS POINT_FREE_DIAL	, " +
								"NVL(SUM(PAYMENT_POINT),0) AS PAY_POINT					, " +
								"NVL(SUM(INVITE_TALK_SERVICE_POINT),0) AS INVITE_POINT " +
							"FROM " +
								"VW_USED_LOG01";
			string sWhere = "";

			OracleParameter[] objParms = CreateWhere(pSiteCd,pSexCd,pLoginId,pUserCharNo,pPartnerLoginId,pPartnerUserCharNo,pTel,pReportDayFrom,pReportDayTo,pChargeType,pReportTimeFrom,pReportTimeTo,pCallResult,pTalkSubType,ref sWhere);

			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					pMin = int.Parse(dr["MIN"].ToString());
					pPoint = int.Parse(dr["POINT"].ToString());
					pFreeDialPoint = int.Parse(dr["POINT_FREE_DIAL"].ToString());
					pPaymentPoint = int.Parse(dr["PAY_POINT"].ToString());
					pInvitePoint = int.Parse(dr["INVITE_POINT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pSexCd,string pLoginId,string pUserCharNo,string pPartnerLoginId,string pPartnerUserCharNo,string pTel,string pReportDayFrom,string pReportDayTo,string pChargeType,string pReportTimeFrom,string pReportTimeTo,string pCallResult,int pTalkSubType,ref string pWhere) {
		ArrayList list = new ArrayList();

		if (!pSiteCd.Equals("")) {
			if (pSexCd.Equals(ViComm.ViCommConst.OPERATOR)) {
				iBridCommLib.SysPrograms.SqlAppendWhere("CAST_SITE_CD = :SITE_CD",ref pWhere);
			} else {
				iBridCommLib.SysPrograms.SqlAppendWhere("MAN_USER_SITE_CD = :SITE_CD",ref pWhere);
			}
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!pLoginId.Equals("")) {
			if (pSexCd.Equals(ViComm.ViCommConst.OPERATOR)) {
				iBridCommLib.SysPrograms.SqlAppendWhere("CAST_LOGIN_ID = :LOGIN_ID",ref pWhere);
			} else {
				iBridCommLib.SysPrograms.SqlAppendWhere("MAN_LOGIN_ID = :LOGIN_ID",ref pWhere);
			}
			list.Add(new OracleParameter("LOGIN_ID",pLoginId));
			if (!pUserCharNo.Equals("")) {
				if (pSexCd.Equals(ViComm.ViCommConst.OPERATOR)) {
					iBridCommLib.SysPrograms.SqlAppendWhere("CAST_CHAR_NO = :CHAR_NO",ref pWhere);
				} else {
					iBridCommLib.SysPrograms.SqlAppendWhere("MAN_CHAR_NO = :CHAR_NO",ref pWhere);
				}
				list.Add(new OracleParameter("CHAR_NO",pUserCharNo));
			}
		}
		if (!string.IsNullOrEmpty(pPartnerLoginId)) {
			if (pSexCd.Equals(ViComm.ViCommConst.OPERATOR)) {
				iBridCommLib.SysPrograms.SqlAppendWhere("MAN_LOGIN_ID = :PARTNER_LOGIN_ID",ref pWhere);
			} else {
				iBridCommLib.SysPrograms.SqlAppendWhere("CAST_LOGIN_ID = :PARTNER_LOGIN_ID",ref pWhere);
			}
			list.Add(new OracleParameter("PARTNER_LOGIN_ID",pPartnerLoginId));
			if (!string.IsNullOrEmpty(pPartnerUserCharNo)) {
				if (pSexCd.Equals(ViComm.ViCommConst.OPERATOR)) {
					iBridCommLib.SysPrograms.SqlAppendWhere("MAN_CHAR_NO = :CHAR_NO",ref pWhere);
				} else {
					iBridCommLib.SysPrograms.SqlAppendWhere("CAST_CHAR_NO = :CHAR_NO",ref pWhere);
				}
				list.Add(new OracleParameter("CHAR_NO",pPartnerUserCharNo));
			}
		}
		if (!pChargeType.Equals("")) {
			StringBuilder oClauseBuilder = new StringBuilder();

			oClauseBuilder.Append("CHARGE_TYPE IN (");
			int iIndex = 1;
			foreach (string sValue in pChargeType.Split(',')) {
				string sParamName = string.Format(":CHARGE_TYPE{0}",iIndex);
				if (iIndex != 1) {
					oClauseBuilder.Append(",");
				}
				oClauseBuilder.Append(sParamName);
				list.Add(new OracleParameter(sParamName,sValue));
				iIndex += 1;
			}
			oClauseBuilder.Append(") ");

			iBridCommLib.SysPrograms.SqlAppendWhere(oClauseBuilder.ToString(),ref pWhere);
		}

		if (!pTel.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("OUTGOING_TERMINAL_ID LIKE :OUTGOING_TERMINAL_ID||'%'",ref pWhere);
			list.Add(new OracleParameter("OUTGOING_TERMINAL_ID",pTel));
		}

		if (string.IsNullOrEmpty(pReportTimeFrom)) {
			pReportTimeFrom = "0";
		}
		if (string.IsNullOrEmpty(pReportTimeTo)) {
			pReportTimeTo = "23";
		}

		if ((!pReportDayFrom.Equals("")) && (!pReportDayTo.Equals(""))) {
			DateTime dtFrom = DateTime.Parse(pReportDayFrom + " " + pReportTimeFrom + ":00:00");
			DateTime dtTo = DateTime.Parse(pReportDayTo + " " + pReportTimeTo + ":59:59").AddSeconds(1);
			iBridCommLib.SysPrograms.SqlAppendWhere("CHARGE_START_DATE >= :CHARGE_START_DATE_FROM AND CHARGE_START_DATE < :CHARGE_START_DATE_TO",ref pWhere);
			list.Add(new OracleParameter("CHARGE_START_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("CHARGE_START_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (!pCallResult.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("CALL_RESULT = :CALL_RESULT",ref pWhere);
			list.Add(new OracleParameter("CALL_RESULT",pCallResult));
		}

		if (pTalkSubType != ViCommConst.TALK_SUB_TYPE_ALL) {
			iBridCommLib.SysPrograms.SqlAppendWhere("TALK_SUB_TYPE = :TALK_SUB_TYPE",ref pWhere);
			list.Add(new OracleParameter("TALK_SUB_TYPE",pTalkSubType));
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public int GetMonitorPageCount(string pSiteCd,string pSexCd,string pUserSeq,string pUserCharNo,string pReportDayFrom,string pReportDayTo,string pPartnerLoginId) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_USED_LOG01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhereMonitor(pSiteCd,pSexCd,pUserSeq,pUserCharNo,pReportDayFrom,pReportDayTo,pPartnerLoginId,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				this.Fill(da,ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetMonitorPageCollection(string pSiteCd,string pSexCd,string pUserSeq,string pUserCharNo,string pReportDayFrom,string pReportDayTo,string pPartnerLoginId,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	MAN_USER_SITE_CD			,");
		oSqlBuilder.AppendLine("	CHARGE_START_DATE			,");
		oSqlBuilder.AppendLine("	CHARGE_END_DATE				,");
		oSqlBuilder.AppendLine("	MAN_LOGIN_ID				,");
		oSqlBuilder.AppendLine("	CAST_LOGIN_ID				,");
		oSqlBuilder.AppendLine("	CAST_CHAR_NO				,");
		oSqlBuilder.AppendLine("	USER_CHAR_NO				,");
		oSqlBuilder.AppendLine("	CHARGE_SEC					,");
		oSqlBuilder.AppendLine("	CHARGE_TYPE					,");
		oSqlBuilder.AppendLine("	CHARGE_TYPE_NM				,");
		oSqlBuilder.AppendLine("	CHARGE_POINT				,");
		oSqlBuilder.AppendLine("	CHARGE_POINT_FREE_DIAL		,");
		oSqlBuilder.AppendLine("	USE_FREE_DIAL_FLAG			,");
		oSqlBuilder.AppendLine("	CONNECT_TYPE				,");
		oSqlBuilder.AppendLine("	CONNECT_TYPE_NM				,");
		oSqlBuilder.AppendLine("	TALK_TYPE					,");
		oSqlBuilder.AppendLine("	TALK_TYPE_NM				,");
		oSqlBuilder.AppendLine("	CHARGE_MIN					,");
		oSqlBuilder.AppendLine("	PAYMENT_POINT				,");
		oSqlBuilder.AppendLine("	INVITE_TALK_SERVICE_POINT	,");
		oSqlBuilder.AppendLine("	INVITE_TALK_SERVICE_MIN		,");
		oSqlBuilder.AppendLine("	OUTGOING_TERMINAL_ID		,");
		oSqlBuilder.AppendLine("	USE_OTHER_SYS_INFO_FLAG		,");
		oSqlBuilder.AppendLine("	HANDLE_NM					,");
		oSqlBuilder.AppendLine("	MAN_HANDLE_NM				");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	VW_USED_LOG01	");

		string sSortExpression = "ORDER BY CHARGE_START_DATE DESC,CHARGE_END_DATE DESC ";

		string sWhere = string.Empty;
		OracleParameter[] oWhereParams = CreateWhereMonitor(pSiteCd,pSexCd,pUserSeq,pUserCharNo,pReportDayFrom,pReportDayTo,pPartnerLoginId,ref sWhere);
		string sPagingSql;
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhere).ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					this.Fill(da,oDataSet);
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}
	public DataSet GetTalkPointSummaryList(string pSiteCd,string pSexCd,string pLoginId,string pUserCharNo,string pPartnerLoginId,string pPartnerUserCharNo,string pTel,string pReportDayFrom,string pReportDayTo,string pChargeType,string pReportTimeFrom,string pReportTimeTo,string pCallResult,int pTalkSubType) {
		string sWhere = string.Empty;
		OracleParameter[] objParms = CreateWhere(pSiteCd,pSexCd,pLoginId,pUserCharNo,pPartnerLoginId,pPartnerUserCharNo,pTel,pReportDayFrom,pReportDayTo,pChargeType,pReportTimeFrom,pReportTimeTo,pCallResult,pTalkSubType,ref sWhere);

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	CHARGE_TYPE							,");
		oSqlBuilder.AppendLine("	CHARGE_TYPE_NM						,");
		oSqlBuilder.AppendLine("	SUM(CHARGE_POINT)	SUMMARY			,");
		oSqlBuilder.AppendLine("	SUM(PAYMENT_POINT)	SUMMARY_PAYMENT	,");
		oSqlBuilder.AppendLine("	SUM(INVITE_TALK_SERVICE_POINT) INVITE_SUMMARY ,");
		oSqlBuilder.AppendLine("	DISPLAY_ORDER						");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_USED_LOG01		");
		oSqlBuilder.AppendLine(sWhere);
		oSqlBuilder.AppendLine("GROUP BY");
		oSqlBuilder.AppendLine("	CHARGE_TYPE				,");
		oSqlBuilder.AppendLine("	CHARGE_TYPE_NM			,");
		oSqlBuilder.AppendLine("	DISPLAY_ORDER			");
		oSqlBuilder.AppendLine("ORDER BY");
		oSqlBuilder.AppendLine("	DISPLAY_ORDER			");

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(objParms);

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					this.Fill(da,oDataSet);
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}

	public DataSet GetMonitorPointSummaryList(string pSiteCd,string pSexCd,string pUserSeq,string pUserCharNo,string pReportDayFrom,string pReportDayTo,string pPartnerLoginId) {
		string sWhere = string.Empty;
		OracleParameter[] objParms = CreateWhereMonitor(pSiteCd,pSexCd,pUserSeq,pUserCharNo,pReportDayFrom,pReportDayTo,pPartnerLoginId,ref sWhere);

		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT");
		oSqlBuilder.AppendLine("	CHARGE_TYPE							,");
		oSqlBuilder.AppendLine("	CHARGE_TYPE_NM						,");
		oSqlBuilder.AppendLine("	SUM(CHARGE_POINT)	SUMMARY			,");
		oSqlBuilder.AppendLine("	SUM(PAYMENT_POINT)	SUMMARY_PAYMENT	,");
		oSqlBuilder.AppendLine("	DISPLAY_ORDER						");
		oSqlBuilder.AppendLine("FROM");
		oSqlBuilder.AppendLine("	VW_USED_LOG01		");
		oSqlBuilder.AppendLine(sWhere);
		oSqlBuilder.AppendLine("GROUP BY");
		oSqlBuilder.AppendLine("	CHARGE_TYPE				,");
		oSqlBuilder.AppendLine("	CHARGE_TYPE_NM			,");
		oSqlBuilder.AppendLine("	DISPLAY_ORDER			");
		oSqlBuilder.AppendLine("ORDER BY");
		oSqlBuilder.AppendLine("	DISPLAY_ORDER			");

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.ToString(),conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(objParms);

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					this.Fill(da,oDataSet);
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}

	private OracleParameter[] CreateWhereMonitor(string pSiteCd,string pSexCd,string pUserSeq,string pUserCharNo,string pReportDayFrom,string pReportDayTo,string pPartnerLoginId,ref string pWhere) {
		ArrayList list = new ArrayList();

		if (!string.IsNullOrEmpty(pSiteCd)) {
			iBridCommLib.SysPrograms.SqlAppendWhere(string.Format("{0}_SITE_CD = :SITE_CD",ViCommConst.OPERATOR.Equals(pSexCd) ? "CAST" : "MAN_USER"),ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!string.IsNullOrEmpty(pUserSeq)) {
			iBridCommLib.SysPrograms.SqlAppendWhere(string.Format("{0}_USER_SEQ = :USER_SEQ",ViCommConst.OPERATOR.Equals(pSexCd) ? "CAST" : "MAN"),ref pWhere);
			list.Add(new OracleParameter("USER_SEQ",pUserSeq));
			if (!string.IsNullOrEmpty(pUserCharNo)) {
				iBridCommLib.SysPrograms.SqlAppendWhere(string.Format("{0}_CHAR_NO = :CHAR_NO",ViCommConst.OPERATOR.Equals(pSexCd) ? "CAST" : "MAN"),ref pWhere);
				list.Add(new OracleParameter("CHAR_NO",pUserCharNo));
			}
		}

		if ((!string.IsNullOrEmpty(pReportDayFrom)) && (!string.IsNullOrEmpty(pReportDayTo))) {
			DateTime dtFrom = DateTime.Parse(pReportDayFrom + " 00:00:00");
			DateTime dtTo = DateTime.Parse(pReportDayTo + " 23:59:59").AddSeconds(1);
			iBridCommLib.SysPrograms.SqlAppendWhere("CHARGE_START_DATE >= :CHARGE_START_DATE_FROM AND CHARGE_START_DATE < :CHARGE_START_DATE_TO",ref pWhere);
			list.Add(new OracleParameter("CHARGE_START_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("CHARGE_START_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (!pPartnerLoginId.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere(string.Format("{0}_LOGIN_ID LIKE :PARTNER_LOGIN_ID||'%'",ViCommConst.OPERATOR.Equals(pSexCd) ? "MAN" : "CAST"),ref pWhere);
			list.Add(new OracleParameter("PARTNER_LOGIN_ID",pPartnerLoginId));
		}

		string sChargeTypesIn = "CHARGE_TYPE IN (:CHARGE_TYPE1, :CHARGE_TYPE2, :CHARGE_TYPE3, :CHARGE_TYPE4, :CHARGE_TYPE5, :CHARGE_TYPE6)";
		if (ViCommConst.OPERATOR.Equals(pSexCd)) {
			sChargeTypesIn = "CHARGE_TYPE IN (:CHARGE_TYPE1, :CHARGE_TYPE2, :CHARGE_TYPE3, :CHARGE_TYPE4)";
		}
		iBridCommLib.SysPrograms.SqlAppendWhere(sChargeTypesIn,ref pWhere);
		list.Add(new OracleParameter("CHARGE_TYPE1",ViCommConst.CHARGE_VIEW_LIVE));
		list.Add(new OracleParameter("CHARGE_TYPE2",ViCommConst.CHARGE_VIEW_ONLINE));
		list.Add(new OracleParameter("CHARGE_TYPE3",ViCommConst.CHARGE_VIEW_TALK));
		list.Add(new OracleParameter("CHARGE_TYPE4",ViCommConst.CHARGE_WIRETAPPING));
		if (!ViCommConst.OPERATOR.Equals(pSexCd)) {
			list.Add(new OracleParameter("CHARGE_TYPE5",ViCommConst.CHARGE_PLAY_PF_VOICE));
			list.Add(new OracleParameter("CHARGE_TYPE6",ViCommConst.CHARGE_REC_PF_VOICE));
		}
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

}
