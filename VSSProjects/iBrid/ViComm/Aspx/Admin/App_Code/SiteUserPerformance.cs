﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ユーザー別稼動状況集計
--	Progaram ID		: SiteUserPerformance
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class SiteUserPerformance:DbSession {

	public int GetPageCount(string pSiteCd,string pReportDayFrom,string pReportDayTo,string pSexCd,string pOrder) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_SITE_USER_PERFORMANCE00  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateSiteWhere(pSiteCd,pReportDayFrom,pReportDayTo,pSexCd,ref sWhere);
			sSql = sSql + sWhere;
			sSql += " GROUP BY SITE_CD,USER_SEQ,USER_CHAR_NO ";
			sSql = "SELECT COUNT(*) AS ROW_COUNT FROM (" + sSql + ")";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add(objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pReportDayFrom,string pReportDayTo,string pSexCd,string pOrder,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();
			if (pOrder.Equals("")) {
				pOrder = "T1.RX_MAIL_COUNT DESC,T1.SITE_CD,T1.USER_SEQ,T1.USER_CHAR_NO";
			}
			string sWhere = "";
			OracleParameter[] oParms = CreateSiteWhere(pSiteCd,pReportDayFrom,pReportDayTo,pSexCd,ref sWhere);

			StringBuilder sSql = new StringBuilder();
			sSql.Append("SELECT * FROM( ").AppendLine();
			sSql.Append("SELECT T2.*,ROWNUM AS RNUM FROM( ").AppendLine();
			if (pSexCd.Equals(ViComm.ViCommConst.MAN)) {
				sSql.Append("	SELECT T_USER.LOGIN_ID,T_USER.SEX_CD,T1.*,T_USER_MAN_CHARACTER.HANDLE_NM	").AppendLine();
			} else {
				sSql.Append("	SELECT T_USER.LOGIN_ID,T_USER.SEX_CD,T1.*,T_CAST_CHARACTER.HANDLE_NM	").AppendLine();
			}
			sSql.Append("		FROM(														").AppendLine();
			sSql.Append("			SELECT													").AppendLine();
			sSql.Append("				SITE_CD												,").AppendLine();
			sSql.Append("				USER_SEQ											,").AppendLine();
			sSql.Append("				USER_CHAR_NO										,").AppendLine();
			sSql.Append("				SUM(TALK_COUNT)				AS TALK_COUNT			,").AppendLine();
			sSql.Append("				SUM(TALK_MIN)				AS TALK_MIN				,").AppendLine();
			sSql.Append("				SUM(RX_MAIL_COUNT)			AS RX_MAIL_COUNT		,").AppendLine();
			sSql.Append("				SUM(TX_MAIL_COUNT)			AS TX_MAIL_COUNT		,").AppendLine();
			sSql.Append("				SUM(TX_BATCH_MAIL_COUNT)	AS TX_BATCH_MAIL_COUNT	").AppendLine();
			sSql.Append("			FROM												").AppendLine();
			sSql.Append("				VW_SITE_USER_PERFORMANCE00						").AppendLine();
			sSql.Append(sWhere).AppendLine();
			sSql.Append("			GROUP BY SITE_CD,USER_SEQ,USER_CHAR_NO				").AppendLine();
			if (pSexCd.Equals(ViComm.ViCommConst.MAN)) {
				sSql.Append("		)T1,T_USER,T_USER_MAN_CHARACTER						").AppendLine();
			} else {
				sSql.Append("		)T1,T_USER,T_CAST_CHARACTER							").AppendLine();
			}
			sSql.Append("		WHERE													").AppendLine();
			sSql.Append("			T1.USER_SEQ		= T_USER.USER_SEQ				AND	").AppendLine();
			if (pSexCd.Equals(ViComm.ViCommConst.MAN)) {
				sSql.Append("		T1.SITE_CD		= T_USER_MAN_CHARACTER.SITE_CD	AND	").AppendLine();
				sSql.Append("		T1.USER_SEQ		= T_USER_MAN_CHARACTER.USER_SEQ	AND	").AppendLine();
				sSql.Append("		T1.USER_CHAR_NO = T_USER_MAN_CHARACTER.USER_CHAR_NO	").AppendLine();
			} else {
				sSql.Append("		T1.SITE_CD		= T_CAST_CHARACTER.SITE_CD		AND	").AppendLine();
				sSql.Append("		T1.USER_SEQ		= T_CAST_CHARACTER.USER_SEQ		AND	").AppendLine();
				sSql.Append("		T1.USER_CHAR_NO = T_CAST_CHARACTER.USER_CHAR_NO		").AppendLine();
			}
			sSql.Append("			ORDER BY " + pOrder).AppendLine();
			sSql.Append(")T2 )WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ").AppendLine();

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				for (int i = 0;i < oParms.Length;i++) {
					cmd.Parameters.Add(oParms[i]);
				}
				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_SITE_USER_PERFORMANCE");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateSiteWhere(string pSiteCd,string pReportDayFrom,string pReportDayTo,string pSexCd,ref string pWhere) {
		ArrayList list = new ArrayList();

		SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
		list.Add(new OracleParameter("SITE_CD",pSiteCd));

		if ((!pReportDayFrom.Equals("")) && (!pReportDayTo.Equals(""))) {
			SysPrograms.SqlAppendWhere("REPORT_DAY >= :REPORT_DAY_FROM AND REPORT_DAY <= :REPORT_DAY_TO",ref pWhere);
			list.Add(new OracleParameter("REPORT_DAY_FROM",pReportDayFrom));
			list.Add(new OracleParameter("REPORT_DAY_TO",pReportDayTo));
		}

		if (!pSexCd.Equals("")) {
			SysPrograms.SqlAppendWhere("SEX_CD = :SEX_CD",ref pWhere);
			list.Add(new OracleParameter("SEX_CD",pSexCd));
		}
		
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}
