﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 拒否
--	Progaram ID		: Refuse
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;
using ViComm;


public class Refuse:DbSession {
	public string refuseType;

	public Refuse() {
		refuseType = "";
	}

	public int GetPageCount(string pSiteCd, string pUserSeq, string pUserCharNo, int pRefuseMeFlag) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql;
			if (ViCommConst.FLAG_OFF == pRefuseMeFlag) {
				sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_REFUSE02 ";
			} else {
				sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_REFUSE05 ";
			}

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pRefuseMeFlag,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}


	public DataSet GetPageCollection(string pSiteCd, string pUserSeq, string pUserCharNo, int pRefuseMeFlag, int startRowIndex, int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sViewNm;
			if (ViCommConst.FLAG_OFF == pRefuseMeFlag) {
				sViewNm = "VW_REFUSE02";
			} else {
				sViewNm = "VW_REFUSE05";
			}
			
			string sOrder = "ORDER BY REFUSE_DATE DESC,SITE_CD,USER_SEQ,USER_CHAR_NO ";
			string sSql = "SELECT " +
								"SITE_CD				," +
								"USER_SEQ				," +
								"USER_CHAR_NO			," +
								"PARTNER_USER_SEQ		," +
								"PARTNER_USER_CHAR_NO	," +
								"CHARACTER_ONLINE_STATUS," +
								"REFUSE_DATE			," +
								"HANDLE_NM				," +
								"LOGIN_ID				," +
								"LAST_LOGIN_DATE		" +
							"FROM(" +
							string.Format(" SELECT {0}.*, ROW_NUMBER() OVER ({1}) AS RNUM FROM {0} ", sViewNm, sOrder);

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pUserSeq,pUserCharNo,pRefuseMeFlag,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_REFUSE");
				}
			}
		} finally {
			conn.Close();
		}

		ds.Tables[0].Columns.Add(new DataColumn("LAST_TALK_DATE",System.Type.GetType("System.String")));
		ds.Tables[0].Columns.Add(new DataColumn("TALK_COUNT",System.Type.GetType("System.String")));

		int iTalkCount;
		string sLastTalkDay;

		using (TalkHistory oTalkHis = new TalkHistory()) {
			foreach (DataRow dr in ds.Tables[0].Rows) {
				oTalkHis.GetTalkCount(pSiteCd,dr["PARTNER_USER_SEQ"].ToString(),dr["PARTNER_USER_CHAR_NO"].ToString(),pUserSeq,pUserCharNo,out iTalkCount,out sLastTalkDay);
				dr["LAST_TALK_DATE"] = sLastTalkDay;
				dr["TALK_COUNT"] = iTalkCount;
			}
		}

		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd, string pUserSeq, string pUserCharNo, int pRefuseMeFlag, ref string pWhere) {
		ArrayList list = new ArrayList();

		if (!pSiteCd.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (ViCommConst.FLAG_OFF == pRefuseMeFlag) {
			if (!pUserSeq.Equals("")) {
				iBridCommLib.SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ", ref pWhere);
				list.Add(new OracleParameter("USER_SEQ", pUserSeq));
				if (!pUserCharNo.Equals("")) {
					iBridCommLib.SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO", ref pWhere);
					list.Add(new OracleParameter("USER_CHAR_NO", pUserCharNo));
				}
			}
		} else {
			if (!pUserSeq.Equals("")) {
				iBridCommLib.SysPrograms.SqlAppendWhere("PARTNER_USER_SEQ = :PARTNER_USER_SEQ", ref pWhere);
				list.Add(new OracleParameter("PARTNER_USER_SEQ", pUserSeq));
				if (!pUserCharNo.Equals("")) {
					iBridCommLib.SysPrograms.SqlAppendWhere("PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO", ref pWhere);
					list.Add(new OracleParameter("PARTNER_USER_CHAR_NO", pUserCharNo));
				}
			}
		}

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	public bool GetOne(string pSiteCd,string pUserSeq,string pUserCharNo,string pPartnerUserSeq,string pPartnerUserCharNo) {
		DataSet ds;
		DataRow dr;
		bool bExist = false;
		try {
			conn = DbConnect();

			string sSql = "SELECT " +
								"REFUSE_TYPE " +
							 "FROM T_REFUSE " +
								"WHERE " +
									"SITE_CD				= :SITE_CD			AND " +
									"USER_SEQ				= :USER_SEQ			AND " +
									"USER_CHAR_NO			= :USER_CHAR_NO		AND " +
									"PARTNER_USER_SEQ		= :PARTNER_USER_SEQ AND	" +
									"PARTNER_USER_CHAR_NO	= :PARTNER_USER_CHAR_NO ";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("USER_SEQ",pUserSeq);
				cmd.Parameters.Add("USER_CHAR_NO",pUserCharNo);
				cmd.Parameters.Add("PARTNER_USER_SEQ",pPartnerUserSeq);
				cmd.Parameters.Add("PARTNER_USER_CHAR_NO",pPartnerUserCharNo);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"T_REFUSE");
					if (ds.Tables["T_REFUSE"].Rows.Count != 0) {
						dr = ds.Tables["T_REFUSE"].Rows[0];
						refuseType = dr["REFUSE_TYPE"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}

	public int GetManRefuseCastPageCount(string pSiteCd,string pUserSeq,string pUserCharNo,int pRefuseMeFlag) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try {
			conn = DbConnect();

			string sSql;
			if (pRefuseMeFlag != 0) {
				sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_REFUSE04 ";
			} else {
				sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_REFUSE03 ";
			}

			string sWhere = "";
			OracleParameter[] objParms = CreateManRefuseCastWhere(pSiteCd,pUserSeq,pUserCharNo,pRefuseMeFlag,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}


	public DataSet GetManRefuseCastPageCollection(string pSiteCd,string pUserSeq,string pUserCharNo,int pRefuseMeFlag,int startRowIndex,int maximumRows) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();
			string sViewNm;
			if (pRefuseMeFlag != 0) {
				sViewNm = " VW_REFUSE04 ";
			} else {
				sViewNm = " VW_REFUSE03 ";
			}

			string sOrder = "ORDER BY FAVORIT_REGIST_DATE DESC,SITE_CD,PARTNER_USER_SEQ ";
			string sSql = "SELECT " +
								"SITE_CD				," +
								"USER_SEQ				," +
								"USER_CHAR_NO			," +
								"PARTNER_USER_SEQ		," +
								"PARTNER_USER_CHAR_NO	," +
								"CHARACTER_ONLINE_STATUS," +
								"DUMMY_TALK_FLAG		," +
								"FAVORIT_REGIST_DATE	," +
								"REFUSE_COMMENT			," +
								"LOGIN_ID				," +
								"PARTNER_LOGIN_ID		," +
								"HANDLE_NM				," +
								"ACT_CATEGORY_NM		" +
							"FROM(" +
							" SELECT " + sViewNm + ".*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM " + sViewNm;

			string sWhere = "";
			OracleParameter[] objParms = CreateManRefuseCastWhere(pSiteCd,pUserSeq,pUserCharNo,pRefuseMeFlag,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_REFUSE");
				}
			}
		} finally {
			conn.Close();
		}

		ds.Tables[0].Columns.Add(new DataColumn("LAST_TALK_DATE",System.Type.GetType("System.String")));
		ds.Tables[0].Columns.Add(new DataColumn("TALK_COUNT",System.Type.GetType("System.String")));

		int iTalkCount;
		string sLastTalkDay;

		using (TalkHistory oTalkHis = new TalkHistory()) {
			foreach (DataRow dr in ds.Tables[0].Rows) {
				if (pRefuseMeFlag != 0) {
					oTalkHis.GetTalkCount(pSiteCd,pUserSeq,pUserCharNo,dr["USER_SEQ"].ToString(),dr["USER_CHAR_NO"].ToString(),out iTalkCount,out sLastTalkDay);
				} else {
					oTalkHis.GetTalkCount(pSiteCd,pUserSeq,pUserCharNo,dr["PARTNER_USER_SEQ"].ToString(),dr["PARTNER_USER_CHAR_NO"].ToString(),out iTalkCount,out sLastTalkDay);
				}
				dr["LAST_TALK_DATE"] = sLastTalkDay;
				dr["TALK_COUNT"] = iTalkCount;
			}
		}
		return ds;
	}


	private OracleParameter[] CreateManRefuseCastWhere(string pSiteCd,string pUserSeq,string pUserCharNo,int pRefuseMeFlag,ref string pWhere) {
		ArrayList list = new ArrayList();

		if (!pSiteCd.Equals("")) {
			iBridCommLib.SysPrograms.SqlAppendWhere("SITE_CD = :SITE_CD",ref pWhere);
			list.Add(new OracleParameter("SITE_CD",pSiteCd));
		}

		if (!pUserSeq.Equals("")) {
			if (pRefuseMeFlag != 0) {
				iBridCommLib.SysPrograms.SqlAppendWhere("PARTNER_USER_SEQ = :PARTNER_USER_SEQ",ref pWhere);
				list.Add(new OracleParameter("PARTNER_USER_SEQ",pUserSeq));
				iBridCommLib.SysPrograms.SqlAppendWhere("PARTNER_USER_CHAR_NO = :PARTNER_USER_CHAR_NO",ref pWhere);
				list.Add(new OracleParameter("PARTNER_USER_CHAR_NO",pUserCharNo));
			} else {
				iBridCommLib.SysPrograms.SqlAppendWhere("USER_SEQ = :USER_SEQ",ref pWhere);
				list.Add(new OracleParameter("USER_SEQ",pUserSeq));
				iBridCommLib.SysPrograms.SqlAppendWhere("USER_CHAR_NO = :USER_CHAR_NO",ref pWhere);
				list.Add(new OracleParameter("USER_CHAR_NO",pUserCharNo));
			}
		}
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}
}
