﻿/*************************************************************************
--	System			: ViComm
--	Sub System Name	: Admin
--	Title			: ブログコメント


--	Progaram ID		: BlogComment
--  Creation Date	: 2012.12.29
--  Creater			: PW Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using System.Text;
using System;

using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public class BlogComment:DbSession {

	#region SearchCondition
	public class SearchCondition {
		private string siteCd;
		public string SiteCd {
			get {
				return this.siteCd;
			}
			set {
				this.siteCd = value;
			}
		}
		private string loginId;
		public string LoginId {
			get {
				return this.loginId;
			}
			set {
				this.loginId = value;
			}
		}
		private string keyword;
		public string Keyword {
			get {
				return this.keyword;
			}
			set {
				this.keyword = value;
			}
		}
		private string blogArticleSeq;
		public string BlogArticleSeq {
			get {
				return this.blogArticleSeq;
			}
			set {
				this.blogArticleSeq = value;
			}
		}
	}
	#endregion SearchCondition

	public BlogComment() {
	}

	public int GetPageCount(object pSearchCondition) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	COUNT(*)	");
		oSqlBuilder.AppendLine("FROM	");
		oSqlBuilder.AppendLine("	T_BLOG_COMMENT				BC	,");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER		UMC	,");
		oSqlBuilder.AppendLine("	T_USER						U	");

		string sWhereClause;
		OracleParameter[] oWhereParams = this.CreateWhere((SearchCondition)pSearchCondition,out sWhereClause);

		int iPageCount = 0;
		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(oSqlBuilder.Append(sWhereClause).ToString(),conn)) {
				cmd.Parameters.AddRange(oWhereParams);
				iPageCount = Convert.ToInt32(cmd.ExecuteScalar());
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(object pSearchCondition,int startRowIndex,int maximumRows) {
		StringBuilder oSqlBuilder = new StringBuilder();
		oSqlBuilder.AppendLine("SELECT	");
		oSqlBuilder.AppendLine("	U.LOGIN_ID				,");
		oSqlBuilder.AppendLine("	UMC.HANDLE_NM			,");
		oSqlBuilder.AppendLine("	BC.SITE_CD				,");
		oSqlBuilder.AppendLine("	BC.USER_SEQ				,");
		oSqlBuilder.AppendLine("	BC.USER_CHAR_NO			,");
		oSqlBuilder.AppendLine("	BC.BLOG_ARTICLE_SEQ		,");
		oSqlBuilder.AppendLine("	BC.COMMENT_DOC			,");
		oSqlBuilder.AppendLine("	BC.CREATE_DATE			,");
		oSqlBuilder.AppendLine("	CASE					");
		oSqlBuilder.AppendLine("		WHEN DEL_FLAG = 1	");
		oSqlBuilder.AppendLine("		THEN '削除'			");
		oSqlBuilder.AppendLine("		ELSE '公開'			");
		oSqlBuilder.AppendLine("	END AS DEL_STATUS		");
		oSqlBuilder.AppendLine("FROM						");
		oSqlBuilder.AppendLine("	T_BLOG_COMMENT				BC	,");
		oSqlBuilder.AppendLine("	T_USER_MAN_CHARACTER		UMC	,");
		oSqlBuilder.AppendLine("	T_USER						U	");

		string sWhereClause;
		string sSortExpression = this.GetOrder((SearchCondition)pSearchCondition);
		string sPagingSql;

		OracleParameter[] oWhereParams = this.CreateWhere((SearchCondition)pSearchCondition,out sWhereClause);
		OracleParameter[] oPagingParams = ViCommPrograms.CreatePagingSql(oSqlBuilder.Append(sWhereClause).ToString(),sSortExpression,startRowIndex,maximumRows,out sPagingSql);

		try {
			conn = DbConnect();
			using (cmd = CreateSelectCommand(sPagingSql,conn)) {
				cmd.BindByName = true;
				cmd.Parameters.AddRange(oWhereParams);
				cmd.Parameters.AddRange(oPagingParams);

				DataSet oDataSet = new DataSet();
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(oDataSet);
					return oDataSet;
				}
			}
		} finally {
			conn.Close();
		}
	}

	private string GetOrder(SearchCondition pSearchCondition) {
		return "ORDER BY BC.CREATE_DATE DESC, U.LOGIN_ID";
	}

	private OracleParameter[] CreateWhere(SearchCondition pSearchCondition,out string pWhere) {
		StringBuilder oWhereBuilder = new StringBuilder();
		oWhereBuilder.AppendLine("WHERE	");
		oWhereBuilder.AppendLine("	BC.SITE_CD				= UMC.SITE_CD				AND");
		oWhereBuilder.AppendLine("	BC.USER_SEQ				= UMC.USER_SEQ				AND");
		oWhereBuilder.AppendLine("	BC.USER_CHAR_NO			= UMC.USER_CHAR_NO			AND");
		oWhereBuilder.AppendLine("	BC.USER_SEQ				= U.USER_SEQ				");
		pWhere = oWhereBuilder.ToString();

		List<OracleParameter> oOracleParameterList = new List<OracleParameter>();

		if (!string.IsNullOrEmpty(pSearchCondition.SiteCd)) {
			SysPrograms.SqlAppendWhere("BC.SITE_CD = :SITE_CD",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("SITE_CD",pSearchCondition.SiteCd));
		}
		
		if (!string.IsNullOrEmpty(pSearchCondition.BlogArticleSeq)) {
			SysPrograms.SqlAppendWhere("BC.BLOG_ARTICLE_SEQ = :BLOG_ARTICLE_SEQ",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("BLOG_ARTICLE_SEQ",pSearchCondition.BlogArticleSeq));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.LoginId)) {
			SysPrograms.SqlAppendWhere("U.LOGIN_ID = :LOGIN_ID",ref pWhere);
			oOracleParameterList.Add(new OracleParameter("LOGIN_ID",pSearchCondition.LoginId));
		}

		if (!string.IsNullOrEmpty(pSearchCondition.Keyword)) {
			StringBuilder oWhereKeywords = new StringBuilder("(");
			string[] sKeywordArray = pSearchCondition.Keyword.Split(' ','　');
			for (int i = 0;i < sKeywordArray.Length;i++) {
				oWhereKeywords.AppendFormat("(BC.COMMENT_DOC LIKE :COMMENT_DOC{0})",i);
				oOracleParameterList.Add(new OracleParameter(string.Format("COMMENT_DOC{0}",i),string.Format("%{0}%",sKeywordArray[i])));

				if (i < sKeywordArray.Length - 1) {
					oWhereKeywords.AppendLine(" AND ");
				}
			}
			oWhereKeywords.Append(")");
			SysPrograms.SqlAppendWhere(oWhereKeywords.ToString(),ref pWhere);
		}

		return oOracleParameterList.ToArray();
	}
}
