/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 広告グループ
--	Progaram ID		: AdGroup
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Web;
using System.Web.UI;
using iBridCommLib;

public class AdGroup:DbSession {

	public string colorBack;
	public string colorChar;
	public string colorLink;

	public AdGroup() {
	}

	public int GetPageCount(string pSiteCd,string pPrepaidAdGroupFlag) {
		DataSet ds;
		DataRow dr;
		int iPageCount = 0;
		try{
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_AD_GROUP01 ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pPrepaidAdGroupFlag,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPageCount = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPageCount;
	}

	public DataSet GetPageCollection(string pSiteCd,string pPrepaidAdGroupFlag,int startRowIndex,int maximumRows) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = "ORDER BY SITE_CD,AD_GROUP_CD ";
			string sSql = "SELECT " +
							"SITE_CD			," +
							"SITE_NM			," +
							"AD_GROUP_CD		," +
							"AD_GROUP_NM		," +
							"WEB_FACE_SEQ		," +
							"HTML_FACE_NAME		," +
							"UPDATE_DATE		" +
							"FROM(" +
							" SELECT VW_AD_GROUP01.*, ROW_NUMBER() OVER (" + sOrder + ") AS RNUM FROM VW_AD_GROUP01  ";

			string sWhere = "";
			OracleParameter[] objParms = CreateWhere(pSiteCd,pPrepaidAdGroupFlag,ref sWhere);
			sSql = sSql + sWhere;

			sSql = sSql + ")WHERE RNUM > :FIRST_ROW AND RNUM <= :LAST_ROW ";
			sSql = sSql + sOrder;

			using (cmd = CreateSelectCommand(sSql,conn)) {
				for (int i = 0;i < objParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)objParms[i]);
				}

				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_AD_GROUP01");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	private OracleParameter[] CreateWhere(string pSiteCd,string pPrepaidAdGroupFlag,ref string pWhere) {
		pWhere = "";
		ArrayList list = new ArrayList();
		pWhere = pWhere + " WHERE SITE_CD = :SITE_CD AND AD_GROUP_CD != :AD_GROUP_CD ";
		list.Add(new OracleParameter("SITE_CD",pSiteCd));
		list.Add(new OracleParameter("AD_GROUP_CD",ViComm.ViCommConst.DEFAUL_AD_GROUP_CD));

		if (!string.IsNullOrEmpty(pPrepaidAdGroupFlag)) {
			SysPrograms.SqlAppendWhere("PREPAID_GROUP_FLAG = :PREPAID_GROUP_FLAG",ref pWhere);
			list.Add(new OracleParameter("PREPAID_GROUP_FLAG",pPrepaidAdGroupFlag));
		}
		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}


	public DataSet GetList(string pSiteCd) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT AD_GROUP_CD,AD_GROUP_NM FROM T_AD_GROUP WHERE SITE_CD = :SITE_CD  AND AD_GROUP_CD != :AD_GROUP_CD";
			sSql = sSql + " ORDER BY SITE_CD,AD_GROUP_CD";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("AD_GROUP_CD",ViComm.ViCommConst.DEFAUL_AD_GROUP_CD);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}


	public DataSet GetListIncDefault(string pSiteCd) {
		DataSet ds;
		try{
			conn = DbConnect();
			ds = new DataSet();

			string sSql = "SELECT AD_GROUP_CD,AD_GROUP_NM FROM T_AD_GROUP WHERE SITE_CD = :SITE_CD ";
			sSql = sSql + " ORDER BY SITE_CD,AD_GROUP_CD";

			using (cmd = CreateSelectCommand(sSql,conn)) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds);
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	public bool GetOne(string pSiteCd,string pAdGroupCd) {
		DataSet ds;
		DataRow dr;

		bool bExist = false;
		try{
			conn = DbConnect();

			string sSql = "SELECT " +
							"COLOR_LINE			," +
							"COLOR_CHAR			," +
							"COLOR_BACK			," +
							"COLOR_LINK			," +
							"AD_GROUP_NM		 " +
						"FROM " +
							"VW_AD_GROUP01 " +
						"WHERE " +
							"SITE_CD = :SITE_CD AND AD_GROUP_CD = :AD_GROUP_CD";

			using (cmd = CreateSelectCommand(sSql,conn))
			using (ds = new DataSet()) {
				cmd.Parameters.Add("SITE_CD",pSiteCd);
				cmd.Parameters.Add("AD_GROUP_CD",pAdGroupCd);

				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_AD_GROUP01");

					if (ds.Tables["VW_AD_GROUP01"].Rows.Count != 0) {
						dr = ds.Tables["VW_AD_GROUP01"].Rows[0];
						colorBack = dr["COLOR_BACK"].ToString();
						colorChar = dr["COLOR_CHAR"].ToString();
						colorLink = dr["COLOR_LINK"].ToString();
						bExist = true;
					}
				}
			}
		} finally {
			conn.Close();
		}
		return bExist;
	}
}

