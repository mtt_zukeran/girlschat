﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メール配信記録
--	Progaram ID		: ReqTxMailDtl
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater		Update Explain
  2010/06/15  KazuakiItoh	一括送信メールのデフォルト非表示対応

-------------------------------------------------------------------------*/
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using iBridCommLib;
using ViComm;

public class ReqTxMailDtl:DbSession {


	public ReqTxMailDtl() {
	}

	public int GetPageCount(string pSiteCd,string pMailType,string pReportDayFrom,string pReportTimeFrom,string pReportDayTo,string pReportTimeTo,string pTxLoginId,string pTxUserCharNo,string pRxLoginId,string pRxUserCharNo,string pRxEMailAddr) {
		return GetPageCount(pSiteCd,pMailType,pReportDayFrom,pReportTimeFrom,pReportDayTo,pReportTimeTo,pTxLoginId,pTxUserCharNo,pRxLoginId,pRxUserCharNo,pRxEMailAddr,string.Empty,string.Empty,string.Empty);
	}

	public int GetPageCount(string pSiteCd,string pMailType,string pReportDayFrom,string pReportTimeFrom,string pReportDayTo,string pReportTimeTo,string pTxLoginId,string pTxUserCharNo,string pRxLoginId,string pRxUserCharNo,string pRxEMailAddr,string pWithBatchMail,string pSortExpression,string pSortDirection) {
		DataSet ds;
		DataRow dr;
		int iPages = 0;
		try {
			conn = DbConnect();

			string sSql = "SELECT COUNT(*) AS ROW_COUNT FROM VW_REQ_TX_MAIL_DTL00 ";
			string sWhere = "";

			OracleParameter[] oParms = CreateWhere(pSiteCd,pMailType,pReportDayFrom,pReportTimeFrom,pReportDayTo,pReportTimeTo,pTxLoginId,pTxUserCharNo,pRxLoginId,pRxUserCharNo,pRxEMailAddr,pWithBatchMail,ref sWhere);
			sSql = sSql + sWhere;

			using (cmd = CreateSelectCommand(sSql,conn))
			using (da = new OracleDataAdapter(cmd))
			using (ds = new DataSet()) {
				for (int i = 0;i < oParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)oParms[i]);
				}

				da.Fill(ds);
				if (ds.Tables[0].Rows.Count != 0) {
					dr = ds.Tables[0].Rows[0];
					iPages = int.Parse(dr["ROW_COUNT"].ToString());
				}
			}
		} finally {
			conn.Close();
		}
		return iPages;
	}
	public DataSet GetPageCollection(string pSiteCd,string pMailType,string pReportDayFrom,string pReportTimeFrom,string pReportDayTo,string pReportTimeTo,string pTxLoginId,string pTxUserCharNo,string pRxLoginId,string pRxUserCharNo,string pRxEMailAddr,int startRowIndex,int maximumRows) {
		return GetPageCollection(pSiteCd,pMailType,pReportDayFrom,pReportTimeFrom,pReportDayTo,pReportTimeTo,pTxLoginId,pTxUserCharNo,pRxLoginId,pRxUserCharNo,pRxEMailAddr,startRowIndex,maximumRows,string.Empty,string.Empty,string.Empty);
	}
	public DataSet GetPageCollection(string pSiteCd,string pMailType,string pReportDayFrom,string pReportTimeFrom,string pReportDayTo,string pReportTimeTo,string pTxLoginId,string pTxUserCharNo,string pRxLoginId,string pRxUserCharNo,string pRxEMailAddr,int startRowIndex,int maximumRows,string pWithBatchMail,string pSortExpression,string pSortDirection) {
		DataSet ds;
		try {
			conn = DbConnect();
			ds = new DataSet();

			string sOrder = GetInnerOrder(pSortExpression,pSortDirection);

			StringBuilder sSql = new StringBuilder();

			sSql.Append("SELECT * FROM(").AppendLine();
			sSql.Append("	SELECT").AppendLine();
			sSql.Append("		ROWNUM AS RNUM					,").AppendLine();
			sSql.Append("		INNER.*							,").AppendLine();
			sSql.Append("		M.MAIL_SEQ						,").AppendLine();
			sSql.Append("		M.SERVICE_POINT					,").AppendLine();
			sSql.Append("		M.SERVICE_POINT_EFFECTIVE_DATE	,").AppendLine();
			sSql.Append("		M.SERVICE_POINT_TRANSFER_FLAG	,").AppendLine();
			sSql.Append("		M.MIME_SIZE						,").AppendLine();
			sSql.Append("		M.INVITE_TALK_CAST_CHARGE_FLAG	,").AppendLine();
			sSql.Append("		NVL(E.CRYPT_VALUE,INNER.TX_USER_CHAR_NO) AS CRYPT_VALUE").AppendLine();
			sSql.Append("	FROM T_MAIL_LOG M,T_EX_CHAR_NO E,(").AppendLine();
			sSql.Append("		SELECT ").AppendLine();
			sSql.Append("			REQUEST_TX_MAIL_SEQ		,").AppendLine();
			sSql.Append("			REQUEST_TX_MAIL_SUBSEQ	,").AppendLine();
			sSql.Append("			REQUEST_TX_DATE			,").AppendLine();
			sSql.Append("			MAIL_TEMPLATE_SITE_CD	,").AppendLine();
			sSql.Append("			MAIL_TEMPLATE_NO		,").AppendLine();
			sSql.Append("			RX_EMAIL_ADDR			,").AppendLine();
			sSql.Append("			RX_SITE_CD				,").AppendLine();
			sSql.Append("			RX_LOGIN_ID				,").AppendLine();
			sSql.Append("			RX_HANDLE_NM			,").AppendLine();
			sSql.Append("			RX_USER_SEQ				,").AppendLine();
			sSql.Append("			RX_SEX_CD				,").AppendLine();
			sSql.Append("			TX_SITE_CD				,").AppendLine();
			sSql.Append("			TX_HANDLE_NM			,").AppendLine();
			sSql.Append("			TX_LOGIN_ID				,").AppendLine();
			sSql.Append("			TX_USER_CHAR_NO			,").AppendLine();
			sSql.Append("			TX_SEX_CD				,").AppendLine();
			sSql.Append("			MAIL_TITLE				,").AppendLine();
			sSql.Append("			TEMPLATE_NM				,").AppendLine();
			sSql.Append("			MAIL_TYPE_NM			,").AppendLine();
			sSql.Append("			CASE					 ").AppendLine();
			sSql.Append("			WHEN TX_SEX_CD = '1' THEN").AppendLine();
			sSql.Append("				'http://' || SUB_HOST_NM || '/user/vicomm/man/' || TX_IMG_PATH").AppendLine();
			sSql.Append("			WHEN TX_SEX_CD = '3' THEN").AppendLine();
			sSql.Append("				'http://' || SUB_HOST_NM || '/user/vicomm/woman/'|| TX_IMG_PATH").AppendLine();
			sSql.Append("			ELSE NULL				 ").AppendLine();
			sSql.Append("			END AS TX_USER_IMG_FULL_PATH").AppendLine();
			sSql.Append("		FROM VW_REQ_TX_MAIL_DTL00 ").AppendLine();

			string sWhere = "";
			OracleParameter[] oParms = CreateWhere(pSiteCd,pMailType,pReportDayFrom,pReportTimeFrom,pReportDayTo,pReportTimeTo,pTxLoginId,pTxUserCharNo,pRxLoginId,pRxUserCharNo,pRxEMailAddr,pWithBatchMail,ref sWhere);

			sSql.Append(sWhere).AppendLine();
			sSql.Append(sOrder).AppendLine();
			sSql.Append(")INNER WHERE ROWNUM <= :MAX_ROW AND").AppendLine();
			sSql.Append("INNER.REQUEST_TX_MAIL_SEQ		= M.REQUEST_TX_MAIL_SEQ		(+)	AND").AppendLine();
			sSql.Append("INNER.REQUEST_TX_MAIL_SUBSEQ	= M.REQUEST_TX_MAIL_SUBSEQ	(+)	AND").AppendLine();
			sSql.Append("INNER.TX_SITE_CD				= E.SITE_CD					(+)	AND").AppendLine();
			sSql.Append("INNER.TX_USER_CHAR_NO			= E.USER_CHAR_NO			(+)	AND").AppendLine();
			sSql.Append(":LATEST_FLAG					= E.LATEST_FLAG				(+) ").AppendLine();
			sSql.Append(")WHERE ").AppendLine();

			sSql.Append("RNUM >= :FIRST_ROW AND RNUM <= :LAST_ROW ");

			using (cmd = CreateSelectCommand(sSql.ToString(),conn)) {
				for (int i = 0;i < oParms.Length;i++) {
					cmd.Parameters.Add((OracleParameter)oParms[i]);
				}
				cmd.Parameters.Add("MAX_ROW",startRowIndex + maximumRows);
				cmd.Parameters.Add("LATEST_FLAG",ViCommConst.FLAG_ON);
				cmd.Parameters.Add("FIRST_ROW",startRowIndex);
				cmd.Parameters.Add("LAST_ROW",startRowIndex + maximumRows);
				using (da = new OracleDataAdapter(cmd)) {
					da.Fill(ds,"VW_REQ_TX_MAIL_DTL00");
				}
			}
		} finally {
			conn.Close();
		}
		return ds;
	}

	private OracleParameter[] CreateWhere(string pSiteCd,string pMailType,string pReportDayFrom,string pReportTimeFrom,string pReportDayTo,string pReportTimeTo,string pTxLoginId,string pTxUserCharNo,string pRxLoginId,string pRxUserCharNo,string pRxEMailAddr,string pWithBatchMail,ref string pWhere) {
		ArrayList list = new ArrayList();
		SetDefaultTimeValue(ref pReportTimeFrom,ref pReportTimeTo);

		if (pSiteCd.Equals("") == false) {
			SysPrograms.SqlAppendWhere("TX_SITE_CD = :TX_SITE_CD",ref pWhere);
			list.Add(new OracleParameter("TX_SITE_CD",pSiteCd));
		}

		if (!string.IsNullOrEmpty(pWithBatchMail)) {
			if (ViCommConst.FLAG_ON_STR.Equals(pWithBatchMail)) {
				SysPrograms.SqlAppendWhere("BATCH_MAIL_FLAG = :BATCH_MAIL_FLAG", ref pWhere);
				list.Add(new OracleParameter("BATCH_MAIL_FLAG", ViCommConst.FLAG_ON));
			} else {
				SysPrograms.SqlAppendWhere("BATCH_MAIL_FLAG = :BATCH_MAIL_FLAG", ref pWhere);
				list.Add(new OracleParameter("BATCH_MAIL_FLAG", ViCommConst.FLAG_OFF));
			}
		}

		SysPrograms.SqlAppendWhere("WAIT_TX_FLAG = :WAIT_TX_FLAG",ref pWhere);
		list.Add(new OracleParameter("WAIT_TX_FLAG",ViCommConst.FLAG_OFF_STR));

		if ((!pReportDayFrom.Equals("")) && (!pReportDayTo.Equals(""))) {
			DateTime dtFrom = DateTime.Parse(pReportDayFrom + " " + pReportTimeFrom + ":00:00");
			DateTime dtTo = DateTime.Parse(pReportDayTo + " " + pReportTimeTo + ":59:59").AddSeconds(1);
			iBridCommLib.SysPrograms.SqlAppendWhere("REQUEST_TX_DATE >= :REQUEST_TX_DATE_FROM AND REQUEST_TX_DATE < :REQUEST_TX_DATE_TO",ref pWhere);
			list.Add(new OracleParameter("REQUEST_TX_DATE_FROM",OracleDbType.Date,dtFrom,ParameterDirection.Input));
			list.Add(new OracleParameter("REQUEST_TX_DATE_TO",OracleDbType.Date,dtTo,ParameterDirection.Input));
		}

		if (pMailType.Equals("") == false) {
			SysPrograms.SqlAppendWhere("MAIL_TYPE = :MAIL_TYPE",ref pWhere);
			list.Add(new OracleParameter("MAIL_TYPE",pMailType));
		}


		if (pTxLoginId.Equals("") == false) {
			SysPrograms.SqlAppendWhere("TX_LOGIN_ID LIKE :TX_LOGIN_ID||'%'",ref pWhere);
			list.Add(new OracleParameter("TX_LOGIN_ID",pTxLoginId));

			if (pTxUserCharNo.Equals("") == false) {
				SysPrograms.SqlAppendWhere("TX_USER_CHAR_NO = :TX_USER_CHAR_NO",ref pWhere);
				list.Add(new OracleParameter("TX_USER_CHAR_NO",pTxUserCharNo));
			}
		}

		if (pRxLoginId.Equals("") == false) {
			SysPrograms.SqlAppendWhere("RX_LOGIN_ID LIKE :RX_LOGIN_ID||'%'",ref pWhere);
			list.Add(new OracleParameter("RX_LOGIN_ID",pRxLoginId));

			if (pRxUserCharNo.Equals("") == false) {
				SysPrograms.SqlAppendWhere("RX_USER_CHAR_NO = :RX_USER_CHAR_NO",ref pWhere);
				list.Add(new OracleParameter("RX_USER_CHAR_NO",pRxUserCharNo));
			}
		}

		if (pRxEMailAddr.Equals("") == false) {
			SysPrograms.SqlAppendWhere("RX_EMAIL_ADDR LIKE :RX_EMAIL_ADDR||'%'",ref pWhere);
			list.Add(new OracleParameter("RX_EMAIL_ADDR",pRxEMailAddr.ToLower()));
		}

		SysPrograms.SqlAppendWhere("REFUSE_TX_FLAG = :REFUSE_TX_FLAG",ref pWhere);
		list.Add(new OracleParameter("REFUSE_TX_FLAG",ViCommConst.FLAG_OFF_STR));

		return (OracleParameter[])list.ToArray(typeof(OracleParameter));
	}

	private void SetDefaultTimeValue(ref string pTimeFrom,ref string pTimeTo) {
		if (string.IsNullOrEmpty(pTimeFrom)) {
			pTimeFrom = "00";
		}
		if (string.IsNullOrEmpty(pTimeTo)) {
			pTimeTo = "23";
		}
	}

	private string GetInnerOrder(string pSortExpression,string pSortDirection) {
		if (string.IsNullOrEmpty(pSortExpression) || string.IsNullOrEmpty(pSortDirection)) {
			return "ORDER BY REQUEST_TX_DATE DESC";
		}

		return string.Format("ORDER BY {0} {1}",pSortExpression,pSortDirection);
	}

}
