﻿/*************************************************************************
--	System			: ViComm System
--	Sub System Name	: Admin
--	Title			: 変更履歴
--	Progaram ID		: ModifyHistoryList
--
--  Creation Date	: 2010.03.30
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using iBridCommLib;
using ViComm;

public partial class System_ModifyHistoryList:System.Web.UI.Page {
	private string recCount = "";
	private Stream filter;

	private bool DisablePinEdit {
		get {
			return (bool)this.ViewState["DisablePinEdit"];
		}
		set {
			this.ViewState["DisablePinEdit"] = value;
		}
	}

	private string HtmlDoc {
		get {
			if (this.DisablePinEdit) {
				return this.txtHtmlDocText.Text;
			} else {
				return this.txtHtmlDoc.Text;
			}
		}
		set {
			if (this.DisablePinEdit) {
				this.txtHtmlDocText.Text = value;
			} else {
				this.txtHtmlDoc.Text = value;
			}
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		txtHtmlDoc.EditorDirectory = string.Format("http://{0}:{1}/pinEdit/editor/",Request.UrlReferrer.Host,Request.UrlReferrer.Port);
		filter = Response.Filter;
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);

		if (!IsPostBack) {
			SysPrograms.SetupFromToDay(lstFromYYYY,lstFromMM,lstFromDD,lstToYYYY,lstToMM,lstToDD,true);
			DateTime sDayFrom = DateTime.Today;
			DateTime sDayTo = DateTime.Today;
			lstFromYYYY.SelectedValue = sDayFrom.ToString("yyyy");
			lstToYYYY.SelectedValue = sDayTo.ToString("yyyy");
			lstFromMM.SelectedValue = sDayFrom.ToString("MM");
			lstToMM.SelectedValue = sDayTo.ToString("MM");
			lstFromDD.SelectedValue = sDayFrom.ToString("dd");
			lstToDD.SelectedValue = sDayTo.ToString("dd");
			using (ManageCompany oManageCompany = new ManageCompany()) {
				this.DisablePinEdit = oManageCompany.IsAvailableService(ViCommConst.RELEASE_DISABLE_PINEDIT,2);
				this.txtHtmlDoc.Visible = !this.DisablePinEdit;
				this.txtHtmlDocText.Visible = this.DisablePinEdit;
			}
			InitPage();
		}
	}

	protected void dsModifyHistory_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		if (Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_SYS_OWNER)) {
			pnlSeek.Visible = true;
		} else {
			pnlSeek.Visible = false;
		}
		grdModifyHistory.PageSize = int.Parse(Session["PageSize"].ToString());
		ClearField();
		pnlMainte.Visible = false;
		DataBind();
	}

	private void ClearField() {
		txtDocTitle.Text = "";
		this.HtmlDoc = "";
		txtModifyUserNm.Text = "";
		txtApproveUserNm.Text = "";
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		lblDocSeq.Text = "0";
		GetData();
		txtStartPubDay.Text = DateTime.Now.ToString("yyyy/MM/dd");
		txtApproveDay.Text = DateTime.Now.ToString("yyyy/MM/dd");
	}

	protected void lnkDocSeq_Command(object sender,CommandEventArgs e) {
		lblDocSeq.Text = e.CommandArgument.ToString();
		GetData();
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		ExportCSV();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void dsModifyHistory_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		if (Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_SYS_OWNER)) {
			e.InputParameters[0] = string.Format("{0}/{1}/{2}",lstFromYYYY.SelectedValue,lstFromMM.SelectedValue,lstFromDD.SelectedValue);
			e.InputParameters[1] = string.Format("{0}/{1}/{2}",lstToYYYY.SelectedValue,lstToMM.SelectedValue,lstToDD.SelectedValue);
		} else {
			e.InputParameters[0] = "";
			e.InputParameters[1] = "";
		}
	}

	protected void grdModifyHistory_DataBound(object sender,EventArgs e) {
		//履歴フッター(編集・変更・承認)の表示切替
		foreach (GridViewRow gvr in grdModifyHistory.Rows) {
			LinkButton lb = (LinkButton)gvr.FindControl("lnkHtmlDocType") as LinkButton;
			Label lblModify = (Label)gvr.FindControl("lblModify") as Label;
			Label lblApprove = (Label)gvr.FindControl("lblApprove") as Label;
			if (Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_SYS_OWNER)) {
				lb.Visible = true;
				lblModify.Visible = true;
				lblApprove.Visible = true;
			} else {
				lb.Visible = false;
				lblModify.Visible = false;
				lblApprove.Visible = false;
			}
		}
	}

	private void GetList() {
		grdModifyHistory.PageIndex = 0;
		grdModifyHistory.DataBind();
		pnlCount.DataBind();
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MODIFY_HISTORY_GET");
			db.ProcedureInParm("PDOC_SEQ",DbSession.DbType.VARCHAR2,lblDocSeq.Text);
			db.ProcedureOutParm("PDOC_TITLE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTART_PUB_DAY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMODIFY_USER_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PAPPROVE_DAY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PAPPROVE_USER_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutArrayParm("PHTML_DOC",DbSession.DbType.VARCHAR2,SysConst.MAX_HTML_BLOCKS);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtDocTitle.Text = db.GetStringValue("PDOC_TITLE");
				txtStartPubDay.Text = db.GetStringValue("PSTART_PUB_DAY");
				txtModifyUserNm.Text = db.GetStringValue("PMODIFY_USER_NM");
				txtApproveDay.Text = db.GetStringValue("PAPPROVE_DAY");
				txtApproveUserNm.Text = db.GetStringValue("PAPPROVE_USER_NM");
				this.HtmlDoc = "";
				for (int i = 0;i < ViCommConst.MODIFY_HISTORY_HTML_DOC_MAX ;i++) {
					this.HtmlDoc += db.GetArryStringValue("PHTML_DOC",i);
				}
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
	}

	private void UpdateData(int pDelFlag) {
		string[] sDoc;
		int iDocCount;

		SysPrograms.SeparateHtml(HttpUtility.HtmlDecode(this.HtmlDoc),ViCommConst.MODIFY_HISTORY_HTML_DOC_MAX,out sDoc,out iDocCount);

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MODIFY_HISTORY_MAINTE");
			db.ProcedureInParm("PDOC_SEQ",DbSession.DbType.VARCHAR2,lblDocSeq.Text);
			db.ProcedureInParm("PDOC_TITLE",DbSession.DbType.VARCHAR2,txtDocTitle.Text);
			db.ProcedureInParm("PSTART_PUB_DAY",DbSession.DbType.VARCHAR2,txtStartPubDay.Text);
			db.ProcedureInParm("PMODIFY_USER_NM",DbSession.DbType.VARCHAR2,txtModifyUserNm.Text);
			db.ProcedureInParm("PAPPROVE_DAY",DbSession.DbType.VARCHAR2,txtApproveDay.Text);
			db.ProcedureInParm("PAPPROVE_USER_NM",DbSession.DbType.VARCHAR2,txtApproveUserNm.Text);
			db.ProcedureInArrayParm("PHTML_DOC",DbSession.DbType.VARCHAR2,iDocCount,sDoc);
			db.ProcedureInParm("PHTML_DOC_COUNT",DbSession.DbType.NUMBER,iDocCount);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,ViewState["REVISION_NO"].ToString());
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		InitPage();
		GetList();
	}

	private void ExportCSV() {
		Response.Filter = filter;
		Response.AddHeader("Content-Disposition","attachment;filename=ModifyHistoryList.csv");
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		using (ModifyHistory oModify = new ModifyHistory()) {
			DataSet ds = oModify.GetCsvData(
							string.Format("{0}/{1}/{2}",lstFromYYYY.SelectedValue,lstFromMM.SelectedValue,lstFromDD.SelectedValue),
							string.Format("{0}/{1}/{2}",lstToYYYY.SelectedValue,lstToMM.SelectedValue,lstToDD.SelectedValue));
			SetCsvData(ds);
			Response.End();
		}
	}

	private void SetCsvData(DataSet pDataSet) {

		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");
		string sDtl = "";

		DataRow dr = null;
		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];

			sDtl = string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}",
							dr["DOC_TITLE"].ToString(),
							dr["START_PUB_DAY"].ToString(),
							dr["END_PUB_DAY"].ToString(),
							dr["MODIFY_USER_NM"].ToString(),
							dr["APPROVE_DAY"].ToString(),
							dr["APPROVE_USER_NM"].ToString(),
							dr["HTML_DOC1"].ToString(),
							dr["HTML_DOC2"].ToString(),
							dr["HTML_DOC3"].ToString(),
							dr["HTML_DOC4"].ToString(),
							dr["HTML_DOC5"].ToString());
			sDtl = sDtl.Replace("\r","");
			sDtl = sDtl.Replace("\n","");
			sDtl += "\r\n";
			Response.BinaryWrite(encoding.GetBytes(sDtl));
		}
	}
	protected string GetHtmlDoc() {
		string sHtmlDoc = "";
		
		sHtmlDoc += Eval("HTML_DOC1");
		sHtmlDoc += Eval("HTML_DOC2");
		sHtmlDoc += Eval("HTML_DOC3");
		sHtmlDoc += Eval("HTML_DOC4");
		sHtmlDoc += Eval("HTML_DOC5");
		
		return sHtmlDoc;
	}

	protected string GetModifyStr() {
		string sModify = "";
		sModify = string.Format("{0} {1} 変更",Eval("MODIFY_USER_NM"),Eval("START_PUB_DAY"));
		return sModify;
	}

	protected string GetApproveStr() {
		string sApprove = "";
		sApprove = string.Format("{0} {1} 承認",Eval("APPROVE_USER_NM"),Eval("APPROVE_DAY"));
		return sApprove;
	}
}
