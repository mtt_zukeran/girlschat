﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 管理者メンテナンス
--	Progaram ID		: SystAdminList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class System_AdminList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	protected void dsAdmin_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		grdAdmin.PageSize = int.Parse(Session["PageSize"].ToString());
		grdAdmin.PageIndex = 0;
		txtAdminId.Text = "";
		ClearField();
		pnlMainte.Visible = false;
		DataBind();
		if (!IsPostBack) {
			if (Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_SITE_OWNER)) {
				lstAdminType.SelectedValue = ViCommConst.RIGHT_AD_MANAGE;
				lstAdminType.Enabled = false;
				lblPgmTitle.Text = "代理店向けID・PASS発行";
			} else {
				lstAdminType.SelectedValue = "";
				lstAdminType.Enabled = true;
			}

			lstSiteCd.Items.Insert(0,new ListItem("",""));
			lstSiteCd.DataSourceID = "";
			lstSiteCd.SelectedValue = "";

			lstManagerSeq.Items.Insert(0,new ListItem("",""));
			lstManagerSeq.DataSourceID = "";
			lstManagerSeq.SelectedValue = "";

			InitAdGroup();
		}
	}

	private void InitAdGroup() {
		lstAdGroupCd.DataSourceID = "dsAdGroup";
		lstAdGroupCd.DataBind();
		lstAdGroupCd.Items.Insert(0,new ListItem("",""));
		lstAdGroupCd.SelectedIndex = 0;
	}
	private void ClearField() {
		txtAdminNm.Text = "";
		txtAdminPassword.Text = "";
		chkViewSalesFlag.Checked = false;
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		if (IsValid) {
			GetData();
		}
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		pnlMainte.Visible = true;
		pnlKey.Enabled = true;
		pnlDtl.Visible = false;
	}

	protected void lnkAdminId_Command(object sender,CommandEventArgs e) {
		txtAdminId.Text = e.CommandArgument.ToString();
		GetData();
	}

	private void GetData() {
		int iRecordCount = 0;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ADMIN_GET");
			db.ProcedureInParm("PADMIN_ID",DbSession.DbType.VARCHAR2,txtAdminId.Text);
			db.ProcedureOutParm("PADMIN_PASSWORD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PADMIN_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PADMIN_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSITE_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMANAGER_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PAD_GROUP_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PVIEW_SALES_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			iRecordCount = int.Parse(db.GetStringValue("PRECORD_COUNT"));

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtAdminNm.Text = db.GetStringValue("PADMIN_NM");
				txtAdminPassword.Text = db.GetStringValue("PADMIN_PASSWORD");
				lstAdminType.SelectedValue = db.GetStringValue("PADMIN_TYPE");

				if (db.GetStringValue("PSITE_CD").Equals("") == false) {
					lstSiteCd.SelectedValue = db.GetStringValue("PSITE_CD");
				} else {
					lstSiteCd.SelectedIndex = 0;
				}
				InitAdGroup();
				if (db.GetStringValue("PMANAGER_SEQ").Equals("") == false) {
					lstManagerSeq.SelectedValue = db.GetStringValue("PMANAGER_SEQ");
				} else {
					lstManagerSeq.SelectedIndex = 0;
				}
				if (db.GetStringValue("PAD_GROUP_CD").Equals("") == false) {
					lstAdGroupCd.SelectedValue = db.GetStringValue("PAD_GROUP_CD");
				} else {
					lstAdGroupCd.SelectedIndex = 0;
				}
				chkViewSalesFlag.Checked = db.GetStringValue("PVIEW_SALES_FLAG").Equals(ViCommConst.FLAG_ON_STR);
			} else {
				ClearField();
				InitAdGroup();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;

	}

	private void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("ADMIN_MAINTE");
			db.ProcedureInParm("PADMIN_ID",DbSession.DbType.VARCHAR2,txtAdminId.Text);
			db.ProcedureInParm("PADMIN_PASSWORD",DbSession.DbType.VARCHAR2,txtAdminPassword.Text);
			db.ProcedureInParm("PADMIN_NM",DbSession.DbType.VARCHAR2,txtAdminNm.Text);
			db.ProcedureInParm("PADMIN_TYPE",DbSession.DbType.VARCHAR2,lstAdminType.SelectedValue);
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PMANAGER_SEQ",DbSession.DbType.VARCHAR2,lstManagerSeq.SelectedValue);
			db.ProcedureInParm("PAD_GROUP_CD",DbSession.DbType.VARCHAR2,lstAdGroupCd.SelectedValue);
			db.ProcedureInParm("PVIEW_SALES_FLAG",DbSession.DbType.NUMBER,chkViewSalesFlag.Checked);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		InitPage();
	}
	protected void vdcLoginId_ServerValidate(object source,ServerValidateEventArgs args) {
		using (Manager oManager = new Manager()) {
			args.IsValid = !oManager.IsExistLoginId(txtAdminId.Text);
		}
	}

	protected void dsAdGroup_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
	}

	protected void dsAdmin_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		if (Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_SITE_OWNER)) {
			e.InputParameters[0] = ViCommConst.RIGHT_AD_MANAGE;
		} else {
			e.InputParameters[0] = "";
		}
	}

	protected void lstSiteCd_SelectedIndexChanged(object sender,EventArgs e) {
		InitAdGroup();
	}
}
