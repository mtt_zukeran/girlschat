﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="FromAddressAliasList.aspx.cs" Inherits="SystemAdmin_FromAddressAliasList" Title="Fromアドレスエイリアス設定" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="Fromアドレスエイリアス設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" Runat="Server">
	<div class="admincontent">
		<fieldset>
			<legend>[追加]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<asp:Button runat="server" ID="Button1" Text="追加" CssClass="seekbutton" OnClick="btnRegist_Click" CausesValidation="False" />
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Label ID="lblErrorMessage" runat="server" Text="" ForeColor="red"></asp:Label>
				<table border="0" style="width: 640px; margin-bottom:3px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							エイリアス名
						</td>
						<td class="tdDataStyle">
							<asp:TextBox runat="server" ID="txtAliasName" MaxLength="63" Width="200px"></asp:TextBox>
							<asp:RequiredFieldValidator ID="vdrAliasName" runat="server" ErrorMessage="エイリアス名を入力して下さい。" ControlToValidate="txtAliasName" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							利用中
						</td>
						<td class="tdDataStyle">
							<asp:CheckBox ID="chkUsedNowFlag" runat="server" />&nbsp;
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
				<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
			</fieldset>
		</asp:Panel>
		<fieldset class="fieldset">
			<legend>[一覧]</legend>
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto">
				<asp:GridView ID="grdData" runat="server" DataSourceID="dsData" AllowSorting="false" AllowPaging="True" AutoGenerateColumns="False" SkinID="GridView" PageSize="100">
					<Columns>
						<asp:TemplateField HeaderText="エイリアス名">
							<ItemStyle Wrap="true" />
							<ItemTemplate>
								<asp:LinkButton ID="lnkEdit" runat="server" Text='<%# Eval("ALIAS_NAME") %>' CommandArgument='<%# Eval("FROM_ADDRESS_ALIAS_SEQ") %>' OnCommand="lnkEdit_Command" CausesValidation="False">
								</asp:LinkButton>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="利用中">
							<ItemTemplate>
								<asp:Label ID="lblUsedNowFlag" runat="server" Text='<%# CheckUsedFlag(Eval("USED_NOW_FLAG")) %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle Width="50px" HorizontalAlign="Center" VerticalAlign="Top" />
						</asp:TemplateField>
					</Columns>
				    <PagerSettings Mode="NumericFirstLast" />
				</asp:GridView>
			</asp:Panel>
		</fieldset>
	</div>
	<asp:ObjectDataSource ID="dsData" runat="server" TypeName="FromAddressAlias" SelectMethod="GetPageCollection" SelectCountMethod="GetPageCount" EnablePaging="True">
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrAliasName" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
