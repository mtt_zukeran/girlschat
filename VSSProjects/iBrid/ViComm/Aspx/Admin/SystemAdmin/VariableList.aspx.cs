﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 変数管理
--	Progaram ID		: VariableList
--
--  Creation Date	: 2010.08.16
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;
using System.IO;

public partial class SystemAdmin_VariableList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
	}

	protected void dsVariable_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void FirstLoad() {
		DataBind();
		lstSeekUsableType.Items.Insert(0,new ListItem("",""));
		lstSeekUsableType.SelectedIndex = 0;
		lstSeekVariableType.Items.Insert(0,new ListItem("",""));
		lstSeekVariableType.SelectedIndex = 0;
		lstSeekUsableType.DataSourceID = "";
		lstSeekVariableType.DataSourceID = "";
		lstUsableType.DataSourceID = "";
		lstVariableType.DataSourceID = "";
	}

	private void InitPage() {
		grdVariable.PageSize = int.Parse(Session["PageSize"].ToString());
		grdVariable.PageIndex = 0;
		txtVariableId.Text = "";
		lstUsableType.SelectedIndex = -1;
		lstVariableType.SelectedIndex = -1;
		ClearField();
		pnlMainte.Visible = false;

		DataBind();
	}

	private void ClearField() {
		txtSeekVariableId.Text = "";
		txtVariableNm.Text = "";
		txtVariableSummary.Text = "";
		txtVariableParm.Text = "";
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		if (IsValid) {
			GetData();
		}
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		pnlMainte.Visible = true;
		pnlKey.Enabled = true;
		pnlDtl.Visible = false;
	}

    protected void lnkVariableId_Command(object sender, CommandEventArgs e){
		string[] sKey = e.CommandArgument.ToString().Split(':');
		txtVariableId.Text = sKey[0];
		lstUsableType.SelectedValue = sKey[1];
		lstVariableType.SelectedValue = sKey[2];
		GetData();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	private void GetList() {
		grdVariable.PageIndex = 0;
		grdVariable.DataBind();
		pnlCount.DataBind();
	}

	private void GetData() {
		int iRecordCount = 0;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("VARIABLE_GET");
            db.ProcedureInParm("PVARIABLE_ID", DbSession.DbType.VARCHAR2, txtVariableId.Text);
			db.ProcedureInParm("PUSABLE_TYPE",DbSession.DbType.VARCHAR2,lstUsableType.SelectedValue);
			db.ProcedureInParm("PVARIABLE_TYPE",DbSession.DbType.VARCHAR2,lstVariableType.SelectedValue);
			db.ProcedureOutParm("PVARIABLE_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PVARIABLE_SUMMARY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PVARIABLE_PARM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PUPDATE_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			iRecordCount = int.Parse(db.GetStringValue("PRECORD_COUNT"));

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
                txtVariableNm.Text = db.GetStringValue("PVARIABLE_NM");
				txtVariableSummary.Text = db.GetStringValue("PVARIABLE_SUMMARY");
				txtVariableParm.Text = db.GetStringValue("PVARIABLE_PARM");
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;

	}

	private void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("VARIABLE_MAINTE");
            db.ProcedureInParm("PVARIABLE_ID", DbSession.DbType.VARCHAR2, txtVariableId.Text);
			db.ProcedureInParm("PUSABLE_TYPE",DbSession.DbType.VARCHAR2,lstUsableType.SelectedValue);
			db.ProcedureInParm("PVARIABLE_TYPE",DbSession.DbType.VARCHAR2,lstVariableType.SelectedValue);
			db.ProcedureInParm("PVARIABLE_NM",DbSession.DbType.VARCHAR2,txtVariableNm.Text);
			db.ProcedureInParm("PVARIABLE_SUMMARY",DbSession.DbType.VARCHAR2,txtVariableSummary.Text);
			db.ProcedureInParm("PVARIABLE_PARM",DbSession.DbType.VARCHAR2,txtVariableParm.Text);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		InitPage();
	}

	protected void dsVariable_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekUsableType.SelectedValue;
		e.InputParameters[1] = lstSeekVariableType.SelectedValue;
		e.InputParameters[2] = txtSeekVariableId.Text;
	}

    protected void btnCSV_Click(object sender, EventArgs e)
    {
        ExportCSV();
    }

    private void ExportCSV()
    {
        Response.AddHeader("Content-Disposition","attachment;filename=VariableList.csv");
        Response.ContentType = "application/octet-stream-dummy";
        System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

        using (Variable oVariable = new Variable())
        {
            DataSet ds = oVariable.GetCSVData(lstSeekUsableType.SelectedValue, lstSeekVariableType.SelectedValue, txtSeekVariableId.Text);
            SetCSVData(ds);
            Response.End();
        }
    }

    private void SetCSVData(DataSet pDataSet)
    {
        System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");
        string sDtl = "";

        DataRow dr = null;
        for (int i = 0; i < pDataSet.Tables[0].Rows.Count; i++)
        {
            dr = pDataSet.Tables[0].Rows[i];

			sDtl = string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}",
                            dr["VARIABLE_ID"].ToString()    ,
                            dr["USABLE_TYPE"].ToString()    ,
                            dr["VARIABLE_TYPE"].ToString()  ,
                            dr["VARIABLE_NM"].ToString()    ,
							dr["VARIABLE_SUMMARY"].ToString(),
							dr["VARIABLE_PARM"].ToString());
            sDtl = sDtl.Replace("\r", "");
            sDtl = sDtl.Replace("\n", "");
            sDtl += "\r\n";
            Response.BinaryWrite(encoding.GetBytes(sDtl));
        }
    }
}
