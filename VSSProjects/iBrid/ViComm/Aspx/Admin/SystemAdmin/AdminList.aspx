<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AdminList.aspx.cs" Inherits="System_AdminList" Title="管理者設定" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="管理者設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 480px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									管理者ID
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtAdminId" runat="server" MaxLength="32" Width="160px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrAdminId" runat="server" ErrorMessage="管理者IDを入力して下さい。" ControlToValidate="txtAdminId" ValidationGroup="Key">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeAdminId" runat="server" ErrorMessage="管理者IDは6桁以上の英数字で入力して下さい。" ValidationExpression="\w{6,32}" ControlToValidate="txtAdminId"
										ValidationGroup="Key">*</asp:RegularExpressionValidator>
									<asp:CustomValidator ID="vdcLoginId" runat="server" ErrorMessage="担当IDと重複しています" OnServerValidate="vdcLoginId_ServerValidate" ValidationGroup="Key"></asp:CustomValidator></td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[管理者内容]</legend>
						<table border="0" style="width: 500px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									管理者名
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtAdminNm" runat="server" MaxLength="30" Width="160px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrAdminNm" runat="server" ErrorMessage="管理者名を入力して下さい。" ControlToValidate="txtAdminNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									パスワード
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtAdminPassword" runat="server" MaxLength="32" Width="160px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrAdminPassword" runat="server" ErrorMessage="パスワードを入力して下さい。" ControlToValidate="txtAdminPassword" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeAdminPassword" runat="server" ErrorMessage="パスワードは4桁以上の英数字で入力して下さい。" ValidationExpression="\w{4,32}" ControlToValidate="txtAdminPassword"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									管理者区分
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstAdminType" runat="server" Width="206px" DataSourceID="dsAdminType" DataTextField="CODE_NM" DataValueField="CODE">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									担当サイト
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px" AutoPostBack="True"
										OnSelectedIndexChanged="lstSiteCd_SelectedIndexChanged">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									担当
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstManagerSeq" runat="server" Width="206px" DataSourceID="dsManager" DataTextField="MANAGER_NM" DataValueField="MANAGER_SEQ">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									広告グループ
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstAdGroupCd" runat="server" Width="240px" DataSourceID="dsAdGroup" DataTextField="AD_GROUP_NM" DataValueField="AD_GROUP_CD">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									売上閲覧許可
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkViewSalesFlag" runat="server" />ﾒﾃﾞｨｱ用管理画面で、売上を閲覧できるようにする
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[管理者一覧]</legend>
			<asp:GridView ID="grdAdmin" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsAdmin" AllowSorting="True" SkinID="GridViewColor"
				PageSize="3">
				<Columns>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkAdminId" runat="server" Text='<%# Eval("ADMIN_ID") %>' CommandArgument='<%# Eval("ADMIN_ID") %>' OnCommand="lnkAdminId_Command"
								CausesValidation="False"></asp:LinkButton>
						</ItemTemplate>
						<HeaderTemplate>
							管理者ID
						</HeaderTemplate>
					</asp:TemplateField>
					<asp:BoundField DataField="ADMIN_NM" HeaderText="管理者名"></asp:BoundField>
					<asp:BoundField DataField="ADMIN_PASSWORD" HeaderText="パスワード"></asp:BoundField>
					<asp:BoundField DataField="ADMIN_TYPE_NM" HeaderText="権限"></asp:BoundField>
					<asp:BoundField DataField="SITE_NM" HeaderText="担当サイト"></asp:BoundField>
					<asp:BoundField DataField="MANAGER_NM" HeaderText="担当"></asp:BoundField>
					<asp:TemplateField HeaderText="担当広告グループ">
						<ItemTemplate>
							<asp:HyperLink ID="lnkAccessDay" runat="server" NavigateUrl='<%# string.Format("~/AdManage/AdList.aspx?&adgroupcd={0}",Eval("AD_GROUP_CD")) %>' Text='<%# Eval("AD_GROUP_NM") %>'></asp:HyperLink>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField DataField="AD_GROUP_NM" HeaderText="担当広告グループ"></asp:BoundField>
					<asp:BoundField DataField="UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<a class="reccount">Record Count
				<%#GetRecCount() %>
			</a>
			<br>
			<a class="reccount">Current viewing page
				<%=grdAdmin.PageIndex + 1%>
				of
				<%=grdAdmin.PageCount%>
			</a>
			<div class="button">
				<asp:Button ID="btnRegist" runat="server" Text="管理者追加" OnClick="btnRegist_Click" />
			</div>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsManager" runat="server" SelectMethod="GetList" TypeName="Manager"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsAdmin" runat="server" SelectMethod="GetPageCollection" TypeName="Admin" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsAdmin_Selected" OnSelecting="dsAdmin_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pAdminType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsAdminType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="59" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsAdGroup" runat="server" SelectMethod="GetList" TypeName="AdGroup" OnSelecting="dsAdGroup_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrAdminId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeAdminId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrAdminNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdrAdminPassword" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdeAdminPassword" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
