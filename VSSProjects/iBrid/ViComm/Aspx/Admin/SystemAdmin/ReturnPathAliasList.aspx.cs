﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: Return-Pathエイリアス設定--	Progaram ID		: ReturnPathAliasList
--  Creation Date	: 2015.07.09
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using iBridCommLib;
using ViComm;

public partial class SystemAdmin_ReturnPathAliasList:Page {
	private string ReturnPathAliasSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["ReturnPathAliasSeq"]);
		}
		set {
			this.ViewState["ReturnPathAliasSeq"] = value;
		}
	}

	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.txtAliasName.Style.Add("ime-mode","disabled");

		if (!this.IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		ClearFields();
		pnlKey.Enabled = true;
		pnlMainte.Visible = false;
	}

	private void ClearFields() {
		this.lblErrorMessage.Text = string.Empty;
		this.txtAliasName.Text = string.Empty;
		this.chkUsedNowFlag.Checked = false;
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		this.ReturnPathAliasSeq = string.Empty;
		this.RevisionNo = string.Empty;
		ClearFields();
		pnlKey.Enabled = false;
		btnDelete.Enabled = false;
		pnlMainte.Visible = true;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			if (CheckInput()) {
				UpdateData(0);
			}
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void lnkEdit_Command(object sender,CommandEventArgs e) {
		this.ReturnPathAliasSeq = e.CommandArgument.ToString();
		btnDelete.Enabled = true;
		ClearFields();
		GetData();
	}

	private bool CheckInput() {
		this.lblErrorMessage.Text = string.Empty;

		switch (this.txtAliasName.Text) {
			case "returnmail":
			case "cleaning":
			case "registfailed":
			case "checking":
			case "credit":
			case "gcm":
			case "info":
			case "job":
			case "kanri":
			case "kari2_mail":
			case "kessai":
			case "keyword":
			case "mail_list":
			case "mail_member":
			case "member":
			case "ranking":
			case "support":
			case "t_ozawa":
			case "tsumori":
			case "vicomm":
				this.lblErrorMessage.Text = "入力されたエイリアス名は利用できません";
				break;
		}

		if (this.txtAliasName.Text.Contains("@")) {
			this.lblErrorMessage.Text = "エイリアス名に @ は利用できません";
		}

		if (string.IsNullOrEmpty(this.lblErrorMessage.Text)) {
			return true;
		} else {
			return false;
		}
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("RETURN_PATH_ALIAS_GET");
			db.ProcedureInParm("pRETURN_PATH_ALIAS_SEQ",DbSession.DbType.NUMBER,this.ReturnPathAliasSeq);
			db.ProcedureOutParm("pALIAS_NAME",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pUSED_NOW_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			this.txtAliasName.Text = db.GetStringValue("pALIAS_NAME");
			this.chkUsedNowFlag.Checked = db.GetIntValue("pUSED_NOW_FLAG").Equals(1);
			this.RevisionNo = db.GetIntValue("pREVISION_NO").ToString();
		}

		pnlMainte.Visible = true;
		pnlKey.Enabled = false;
	}

	private void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("RETURN_PATH_ALIAS_MAINTE");
			db.ProcedureInParm("pRETURN_PATH_ALIAS_SEQ",DbSession.DbType.NUMBER,this.ReturnPathAliasSeq);
			db.ProcedureInParm("pALIAS_NAME",DbSession.DbType.VARCHAR2,this.txtAliasName.Text.Trim());
			db.ProcedureInParm("pUSED_NOW_FLAG",DbSession.DbType.NUMBER,this.chkUsedNowFlag.Checked);
			db.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
			db.ProcedureInParm("pDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		InitPage();
		grdData.DataBind();
	}

	protected string CheckUsedFlag(object pFlag) {
		if (pFlag.ToString().Equals(ViCommConst.FLAG_ON.ToString())) {
			return "○";
		} else {
			return "×";
		}
	}
}
