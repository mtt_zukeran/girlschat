﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メール文章ドメイン設定
--	Progaram ID		: MailBodyDomainList
--
--  Creation Date	: 2010.07.05
--  Creater			: Kazuaki.Itoh@iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;

public partial class Mail_MailBodyDomainList:System.Web.UI.Page {
	private string recCount = "0";
	protected string RecCount {
		get {
			return this.recCount;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	/// <summary>画面の初期化を行う</summary>
	private void InitPage() {
		this.grdMailBodyDomainSettings.PageSize = 999;
		this.pnlMainte.Visible = false;
		this.lstSiteCd.DataBind();
		this.lstSeekSiteCd.DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		//権限によって制限
		switch (Session["AdminType"].ToString()) {
			case ViCommConst.RIGHT_SYS_OWNER:
				break;
			default:
				btnDelete.Enabled = false;
				btnRegist.Enabled = false;
				txtReplaceDomain.Enabled = false;
				break;
		}

		this.pnlList.DataBind();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		this.GetList();
	}
	protected void lnkEdit_Command(object sender,CommandEventArgs e) {
		ViewState["SEQ"] = e.CommandArgument.ToString();
		GetData();
	}
	protected void dsMailBodyDomainSettings_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
	}
	protected void dsMailBodyDomainSettings_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null)
			this.recCount = e.ReturnValue.ToString();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		this.lstSiteCd.SelectedIndex = lstSeekSiteCd.SelectedIndex;
		this.txtReplaceDomain.Text = string.Empty;
		this.chkUsedNowFlag.Checked = false;
		this.chkUsedRandomFlag.Checked = false;
		this.chkDocomo.Checked = false;
		this.chkSoftbank.Checked = false;
		this.chkAu.Checked = false;
		ViewState["SEQ"] = decimal.Zero.ToString();
		ViewState["ROWID"] = null;
		GetData();
	}
	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!IsValid)
			return;

		this.UpdateData(0);
	}
	protected void btnDelete_Click(object sender,EventArgs e) {
		if (!IsValid)
			return;

		this.UpdateData(1);
	}
	protected void btnCancel_Click(object sender,EventArgs e) {
		Response.Redirect(this.Request.Url.PathAndQuery);
	}
	protected void vdcReplaceDomain_ServerValidate(object source,ServerValidateEventArgs args) {
		string sHostNm;
		if (args.IsValid) {
			bool bHasSeq = !((string)(ViewState["SEQ"] as string ?? "0")).Equals("0");
			if (!string.IsNullOrEmpty(txtReplaceDomain.Text.TrimEnd()) && !bHasSeq) {
				using (MailBodyDomainSettings objMailBodyDomainSettings = new MailBodyDomainSettings()) {
					args.IsValid = !objMailBodyDomainSettings.Exists(lstSiteCd.SelectedValue,txtReplaceDomain.Text.TrimEnd());
				}
			}
			using (Site oSite = new Site()) {
				oSite.GetOne(lstSiteCd.SelectedValue);
				sHostNm = oSite.hostNm;
			}
			if (txtReplaceDomain.Text.Equals(sHostNm)) {
				args.IsValid = false;
			}
		}
	}


	private void GetList() {
		this.pnlList.DataBind();
	}

	private void GetData() {
		decimal dSeq = decimal.Parse(ViewState["SEQ"] as string ?? "0");
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MAIL_BD_DOMAIN_SETTINGS_GET");
			db.ProcedureInParm("pMAIL_BODY_DOMAIN_SETTINGS_SEQ",DbSession.DbType.NUMBER,dSeq);
			db.ProcedureOutParm("pSITE_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pREPLACE_DOMAIN",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pUSED_NOW_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pUSED_RANDOM_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pCARRIER_USED_MASK",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("pREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("pROWID");

			if (int.Parse(db.GetStringValue("pRECORD_COUNT")) > 0) {
				lstSiteCd.SelectedValue = db.GetStringValue("pSITE_CD");
				txtReplaceDomain.Text = db.GetStringValue("pREPLACE_DOMAIN");
				chkUsedNowFlag.Checked = db.GetStringValue("pUSED_NOW_FLAG").Equals("1");
				chkUsedRandomFlag.Checked = db.GetStringValue("pUSED_RANDOM_FLAG").Equals("1");
				chkDocomo.Checked = (db.GetIntValue("pCARRIER_USED_MASK") & ViCommConst.MASK_DOCOMO) != 0;
				chkSoftbank.Checked = (db.GetIntValue("pCARRIER_USED_MASK") & ViCommConst.MASK_SOFTBANK) != 0;
				chkAu.Checked = (db.GetIntValue("pCARRIER_USED_MASK") & ViCommConst.MASK_KDDI) != 0;
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;

	}
	private void UpdateData(int pDelFlag) {
		decimal dSeq = decimal.Parse(string.IsNullOrEmpty(ViewState["SEQ"] as string) ? "0" : ViewState["SEQ"] as string);

		int iCarrierUsedMask = 0;
		if (chkDocomo.Checked) {
			iCarrierUsedMask += ViCommConst.MASK_DOCOMO;
		}
		if (chkSoftbank.Checked) {
			iCarrierUsedMask += ViCommConst.MASK_SOFTBANK;
		}
		if (chkAu.Checked) {
			iCarrierUsedMask += ViCommConst.MASK_KDDI;
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MAIL_BD_DOMAIN_SETTINGS_MAINTE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("pMAIL_BODY_DOMAIN_SETTINGS_SEQ",DbSession.DbType.NUMBER,dSeq);
			db.ProcedureInParm("pREPLACE_DOMAIN",DbSession.DbType.VARCHAR2,txtReplaceDomain.Text);
			db.ProcedureInParm("pUSED_NOW_FLAG",DbSession.DbType.NUMBER,chkUsedNowFlag.Checked);
			db.ProcedureInParm("pUSED_RANDOM_FLAG",DbSession.DbType.NUMBER,chkUsedRandomFlag.Checked);
			db.ProcedureInParm("pCARRIER_USED_MASK",DbSession.DbType.NUMBER,iCarrierUsedMask);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		int iIdx = lstSiteCd.SelectedIndex;
		InitPage();
		lstSeekSiteCd.SelectedIndex = iIdx;
		GetList();

		//自動化はやめ手動での追加に変更
		/*
		DataSet ds;
		string sHostHeader = "";
		string sGlobalIp;
		string sIisSiteIdentifer;
		string sHostNm;

		using (Site oSite = new Site()) {
			oSite.GetOne(lstSiteCd.SelectedValue);
			sGlobalIp = oSite.globalIpAddr;
			sIisSiteIdentifer = oSite.iisSiteIdentifier;
			sHostNm = oSite.hostNm;
		}

		using (MailBodyDomainSettings oMailBodyDomainSettings = new MailBodyDomainSettings()) {
			ds = oMailBodyDomainSettings.GetList(lstSiteCd.SelectedValue);
		}
		//sHostHeader = string.Format("{0}:80:{1}",sGlobalIp,sHostNm);
		bool bFirstFlag = true;
		foreach (DataRow dr in ds.Tables[0].Rows) {
			if (bFirstFlag) {
				sHostHeader += string.Format("{0}:80:{1}",sGlobalIp,dr["REPLACE_DOMAIN"].ToString());
			} else {
				sHostHeader += string.Format(",{0}:80:{1}",sGlobalIp,dr["REPLACE_DOMAIN"].ToString());
			}
			bFirstFlag = false;
		}
		using (localhost.Service objWebSrv = new localhost.Service()) {
			objWebSrv.IISReplaceHostHeader(sHostHeader,sIisSiteIdentifer);
		}
		*/
	}

	protected string CheckUsedFlag(object pFlag) {
		if (pFlag.ToString().Equals(ViCommConst.FLAG_ON.ToString())) {
			return "○";
		} else {
			return "×";
		}
	}
}
