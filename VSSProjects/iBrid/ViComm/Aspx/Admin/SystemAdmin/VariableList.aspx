﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="VariableList.aspx.cs" Inherits="SystemAdmin_VariableList" Title="変数管理" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="変数管理"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 600px" class="tableStyle">
							<tr>
								<td class="tdHeaderSmallStyle">
                                    変数ID
								</td>
								<td class="tdDataStyle" colspan="3">
									<asp:TextBox ID="txtVariableId" runat="server" MaxLength="256" Width="500px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrVariableId" runat="server" ErrorMessage="変数IDを入力して下さい。" ControlToValidate="txtVariableId" ValidationGroup="Key">*</asp:RequiredFieldValidator>
<%--									<asp:RegularExpressionValidator ID="vdeVariableId" runat="server" ErrorMessage="変数IDは英数字で入力して下さい。" ValidationExpression="(\w|[$]){1,64}" ControlToValidate="txtVariableId"
										ValidationGroup="Key">*</asp:RegularExpressionValidator>--%>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderSmallStyle2">
                                    使用種別
                                </td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstUsableType" runat="server" Width="135px" DataSourceID="dsUsableType" DataTextField="CODE_NM" DataValueField="CODE">
									</asp:DropDownList>
								</td>
								<td class="tdHeaderSmallStyle2">
                                    変数種別</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstVariableType" runat="server" Width="135px" DataSourceID="dsVariableType" DataTextField="CODE_NM" DataValueField="CODE">
									</asp:DropDownList>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[変数内容]</legend>
						<table border="0" style="width: 600px" class="tableStyle">
							<tr>
								<td class="tdHeaderSmallStyle">
                                    変数名
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtVariableNm" runat="server" MaxLength="170" Width="500px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrVariableNm" runat="server" ErrorMessage="変数名を入力して下さい。" ControlToValidate="txtVariableNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderSmallStyle">
                                    概要</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtVariableSummary" runat="server" MaxLength="682" Width="500px" TextMode="MultiLine" Height="80px"></asp:TextBox>
                                    &nbsp;
								</td>
							</tr>
							<tr>
								<td class="tdHeaderSmallStyle2">
                                    ﾊﾟﾗﾒｰﾀ</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtVariableParm" runat="server" MaxLength="682" Width="500px" TextMode="MultiLine" Height="80px"></asp:TextBox>
                                    &nbsp;
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[変数一覧]</legend>
			<table border="0" style="width: 650px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle">
						使用種別
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekUsableType" runat="server" DataSourceID="dsUsableType" DataTextField="CODE_NM" DataValueField="CODE" Width="135px">
						</asp:DropDownList>
					</td>
					<td class="tdHeaderStyle">
						変数種別
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekVariableType" runat="server" DataSourceID="dsVariableType" DataTextField="CODE_NM" DataValueField="CODE" Width="135px">
						</asp:DropDownList>
					</td>
				</tr>
				<tr>
					<td class="tdHeaderStyle2">
						変数ID
					</td>
					<td class="tdDataStyle" colspan="3">
						<asp:TextBox ID="txtSeekVariableId" runat="server" MaxLength="64" Width="240px"></asp:TextBox>
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnRegist" Text="変数追加" CssClass="seekbutton" OnClick="btnRegist_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" OnClick="btnCSV_Click" CausesValidation="False" />
			<br />
			<br />
			<asp:GridView ID="grdVariable" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsVariable" AllowSorting="True" SkinID="GridViewColor">
				<Columns>
					<asp:TemplateField HeaderText="変数ID">
						<ItemTemplate>
							<asp:LinkButton ID="lnkVariableId" runat="server" Text='<%# Eval("VARIABLE_ID") %>' CommandArgument='<%# string.Format("{0}:{1}:{2}",Eval("VARIABLE_ID"), Eval("USABLE_TYPE"), Eval("VARIABLE_TYPE")) %>' OnCommand="lnkVariableId_Command"
								CausesValidation="False"></asp:LinkButton>
						</ItemTemplate>
                        <ItemStyle Wrap="True" />
					</asp:TemplateField>
					<asp:TemplateField HeaderText="使用<br />種別">
						<ItemTemplate>
							<asp:Label ID="lblUsableTypeNm" runat="server" Text='<%# Eval("USABLE_TYPE_NM") %>'></asp:Label>
						</ItemTemplate>
                        <ItemStyle Wrap="False" />
                    </asp:TemplateField>
					<asp:BoundField DataField="VARIABLE_TYPE_NM" HeaderText="変数種別">
                        <ItemStyle Wrap="False" />
                    </asp:BoundField>
					<asp:BoundField DataField="VARIABLE_NM" HeaderText="変数名">
                        <ControlStyle Width="240px" />
                        <HeaderStyle Wrap="False" />
                        <ItemStyle Wrap="False" />
                    </asp:BoundField>
					<asp:BoundField DataField="VARIABLE_SUMMARY_PART" HeaderText="概要">
                        <ItemStyle Wrap="True" />
                    </asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br>
				<a class="reccount">Current viewing page
					<%=grdVariable.PageIndex + 1%>
					of
					<%=grdVariable.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsUsableType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="36" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsVariableType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="35" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsVariable" runat="server" SelectMethod="GetPageCollection" TypeName="Variable" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelected="dsVariable_Selected" OnSelecting="dsVariable_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pUsableType" Type="string" />
			<asp:Parameter Name="pVariableType" Type="string" />
			<asp:Parameter Name="pVariableId" Type="string" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrVariableId" HighlightCssClass="validatorCallout" />
<%--	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeVariableId" HighlightCssClass="validatorCallout" />
--%>	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrVariableNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
