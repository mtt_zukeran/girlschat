﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MailBodyDomainList.aspx.cs" Inherits="Mail_MailBodyDomainList"
	Title="Untitled Page" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="メール文章ドメイン設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									サイトコード
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
									</asp:DropDownList>
								</td>
							</tr>
						</table>
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[メール文章ドメイン内容]</legend>
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									置換後ドメイン
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtReplaceDomain" runat="server" MaxLength="255" Width="400px"></asp:TextBox>&nbsp;
									<asp:CustomValidator ID="vdcReplaceDomain" runat="server" ControlToValidate="txtReplaceDomain" ErrorMessage="既に登録済みのドメインです。" OnServerValidate="vdcReplaceDomain_ServerValidate"
										ValidationGroup="Detail" Display="Dynamic">*</asp:CustomValidator>
									<asp:RequiredFieldValidator ID="vdrReplaceDomain" runat="server" ErrorMessage="置換後ドメインを入力して下さい。" ControlToValidate="txtReplaceDomain" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									利用中
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkUsedNowFlag" runat="server" />&nbsp;
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ランダム利用中
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkUsedRandomFlag" runat="server" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									利用可能キャリア
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkDocomo" runat="server" Text="ﾄﾞｺﾓ" />
									<asp:CheckBox ID="chkSoftbank" runat="server" Text="ｿﾌﾄﾊﾞﾝｸ" />
									<asp:CheckBox ID="chkAu" runat="server" Text="au" />
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[メール文章ドメイン設定一覧]</legend>
			<table border="0" style="width: 640px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle2">
						サイトコード
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
						</asp:DropDownList>
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnRegist" Text="メール文章ドメイン追加" CssClass="seekbutton" CausesValidation="False" OnClick="btnRegist_Click" />
			<br /><br />
			<asp:Label ID="Label1" runat="server" Text="ドメイン追加の際はドメイン取得後、技術担当までご連絡下さい。" ForeColor="Red"></asp:Label><br />
			<asp:Panel ID="pnlList" runat="server">
				<br />
				<asp:GridView ID="grdMailBodyDomainSettings" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsMailBodyDomainSettings" AllowSorting="True"
					SkinID="GridViewColor">
					<Columns>
						<asp:TemplateField HeaderText="置換後ドメイン">
							<ItemStyle Wrap="true" />
							<ItemTemplate>
								<asp:LinkButton ID="lnkEdit" runat="server" Text='<%# Eval("REPLACE_DOMAIN") %>' CommandArgument='<%# Eval("MAIL_BODY_DOMAIN_SETTINGS_SEQ") %>' OnCommand="lnkEdit_Command"
									CausesValidation="False">
								</asp:LinkButton>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="利用中">
							<ItemTemplate>
								<asp:Label ID="lblUsedNowFlag" runat="server" Text='<%# CheckUsedFlag(Eval("USED_NOW_FLAG")) %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle Width="50px" HorizontalAlign="Center" VerticalAlign="Top" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ﾗﾝﾀﾞﾑ利用中">
							<ItemTemplate>
								<asp:Label ID="lblUsedRandomFlag" runat="server" Text='<%# CheckUsedFlag(Eval("USED_RANDOM_FLAG")) %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
						</asp:TemplateField>
					</Columns>
					<PagerSettings Mode="NumericFirstLast" />
				</asp:GridView>
				<br />
				<div>
					<a class="reccount">Record Count
						<%# RecCount %>
					</a>
					<br />
					<a class="reccount">Current viewing page
						<%= grdMailBodyDomainSettings.PageIndex + 1%>
						of
						<%= grdMailBodyDomainSettings.PageCount%>
					</a>
				</div>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsMailBodyDomainSettings" runat="server" SelectMethod="GetPageCollection" TypeName="MailBodyDomainSettings" SelectCountMethod="GetPageCount"
		EnablePaging="True" OnSelected="dsMailBodyDomainSettings_Selected" OnSelecting="dsMailBodyDomainSettings_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Custom, Numbers, LowercaseLetters, UppercaseLetters"
		ValidChars=".-" TargetControlID="txtReplaceDomain" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdcReplaceDomain" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdrReplaceDomain" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
