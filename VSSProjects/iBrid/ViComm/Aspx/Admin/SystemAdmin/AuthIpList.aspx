﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" 
    CodeFile="AuthIpList.aspx.cs" Inherits="SystemAdmin_AuthIpList" Title="アクセス許可IP設定"
    ValidateRequest="false" %>
    
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="アクセス許可IP設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 480px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									アクセス許可IP
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtIpAddr" runat="server" MaxLength="32" Width="160px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrIpAddr" runat="server" ErrorMessage="IPアドレスを入力して下さい。" ControlToValidate="txtIpAddr" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeIpAddr" runat="server" ErrorMessage="IPアドレスを正しく入力して下さい。" ValidationExpression="^(\d|[01]?\d\d|2[0-4]\d|25[0-5])\.(\d|[01]?\d\d|2[0-4]\d|25[0-5])\.(\d|[01]?\d\d|2[0-4]\d|25[0-5])\.(\d|[01]?\d\d|2[0-4]\d|25[0-5])$" ControlToValidate="txtIpAddr"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									<asp:CustomValidator ID="vdcIpAddr" runat="server" ErrorMessage="既に存在しているIPアドレスです。" ControlToValidate="txtIpAddr" OnServerValidate="vdcIpAddr_ServerValidate" ValidationGroup="Detail">*</asp:CustomValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									PCｻｲﾄへ強制ﾘﾀﾞｲﾚｸﾄ
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkRedirectPcPageFlag" runat="server" Width="160px"></asp:CheckBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									携帯ｻｲﾄの閲覧を不許可
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkNaMobilePageFlag" runat="server" Width="160px"></asp:CheckBox>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="追加" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
   		<fieldset>
		    <legend>[ｱｸｾｽ許可IP一覧]</legend>
            <asp:GridView ID="grdAuthIp" runat="server" AllowPaging="true" DataSourceID="dsAuthIp" PageSize="20" AutoGenerateColumns="false" SkinID="GridViewColor" >
                <Columns>
                    <asp:BoundField DataField="IP_ADDR" HeaderText="IP" >
                    <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label runat="server" Text="PCｻｲﾄへ強制ﾘﾀﾞｲﾚｸﾄ"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label runat="server" Text='<%# IsChecked(Eval("REDIRECT_PC_PAGE_FLAG"))%>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label runat="server" Text="携帯ｻｲﾄの閲覧を不許可"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label runat="server" Text='<%# IsChecked(Eval("NA_MOBILE_PAGE_FLAG"))%>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label runat="server" Text="削除"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkAuthIpDelete" runat="server" Text="IP削除" CommandArgument='<%# Eval("IP_ADDR") %>' OnCommand="lnkAuthIpDelete_OnCommand" />
	                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="lnkAuthIpDelete" ConfirmText="IPの削除を実行しますか？" ConfirmOnFormSubmit="true" />
                    </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
			<a class="reccount">Record Count
				<%#GetRecCount() %>
			</a>
			<br />
			<a class="reccount">Current viewing page
				<%=grdAuthIp.PageIndex + 1%>
				of
				<%=grdAuthIp.PageCount%>
			</a>
			<div class="button">
				<asp:Button ID="btnRegist" runat="server" Text="IP追加" OnClick="btnRegist_Click" />
			</div>
        </fieldset>
    </div>
    <asp:ObjectDataSource ID="dsAuthIp" runat="server" EnablePaging="True"
        SelectCountMethod="GetPageCount" SelectMethod="GetPageCollection" TypeName="AuthIp" OnSelected="dsAuthIp_Selected">
    </asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrIpAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeIpAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdcIpAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="IPの追加を行ないますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
