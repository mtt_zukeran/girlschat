﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: サイト別決済設定
--	Progaram ID		: SiteSettleManage
--
--  Creation Date	: 2009.11.06
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web.UI.WebControls;
using iBridCommLib;
using ViComm;

public partial class System_SiteSettleManage:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
	}

	private void FirstLoad() {
		grdSiteSettle.PageSize = 999;
		string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		if (sSiteCd.Equals("")) {
			if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
				lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			} else {
				lstSeekSiteCd.SelectedIndex = 0;
			}
		} else {
			lstSeekSiteCd.SelectedValue = sSiteCd;
		}
		DataBind();

		lstSiteCd.DataSourceID = "";
		lstSiteCd.SelectedIndex = 0;

		lstSettleCompanyCd.DataSourceID = "";
		lstSettleCompanyCd.SelectedIndex = 0;

		lstSettleType.DataSourceID = "";
		lstSettleType.SelectedIndex = 0;

		lstMailTemplate.DataSourceID = "";
		lstMailTemplate.SelectedValue = "";

		// 権限によって表示項目を編集します。 
		switch (Session["AdminType"].ToString()) {
			case ViCommConst.RIGHT_SYS_OWNER:
				break;
			default:
				btnRegist.Enabled = false;
				btnDelete.Enabled = false;
				plcOnlyOwner.Visible = false;
				break;
		}
	}

	private void InitPage() {
		ClearField();
		pnlMainte.Visible = false;

	}

	private void ClearField() {
		if (lstMailTemplate.Items.Count > 1) {
			lstMailTemplate.Items.Clear();
		}
		recCount = "0";
		txtCpIdNo.Text = "";
		txtCpPassword.Text = "";
		txtSettleUrl.Text = "";
		txtBackUrl.Text = "";
		txtContinueSettleUrl.Text = "";
		txtHostNm.Text = "";
		txtHostNm2.Text = "";
		chkAffiliSalesAddReceiptFlag.Checked = false;
		chkNotCreateSettleReqLogFlag.Checked = false;
	}

	protected string GetRecCount() {
		return recCount;
	}

	private void GetList() {
		grdSiteSettle.PageIndex = 0;
		grdSiteSettle.DataBind();
		pnlCount.DataBind();
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		ClearField();
		GetData();
	}


	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		pnlMainte.Visible = true;
		pnlKey.Enabled = true;
		pnlDtl.Visible = false;
		lstSiteCd.Enabled = false;
		lstSiteCd.SelectedIndex = lstSeekSiteCd.SelectedIndex;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void lnkSettleType_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		lstSiteCd.SelectedValue = sKeys[0];
		lstSettleCompanyCd.SelectedValue = sKeys[1];
		lstSettleType.SelectedValue = sKeys[2];
		GetData();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
		lstSiteCd.SelectedValue = lstSeekSiteCd.SelectedValue;
	}

	protected void dsSiteSettle_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsSiteSettle_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
	}

	protected void dsMailTemplate_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
	}

	protected bool UrlCheck(string sUrl) {
		if (sUrl.IndexOf("\n") >= 0) {
			return false;
		} else {
			return true;
		}
	}

	protected void vdcSettleUrl_ServerValidate(object source,ServerValidateEventArgs args) {
		args.IsValid = UrlCheck(txtSettleUrl.Text);
	}
	protected void vdcBackUrl_ServerValidate(object source,ServerValidateEventArgs args) {
		args.IsValid = UrlCheck(txtBackUrl.Text);
	}
	protected void vdcContinueSettleUrl_ServerValidate(object source,ServerValidateEventArgs args) {
		args.IsValid = UrlCheck(txtContinueSettleUrl.Text);
	}
	protected void vdcSubSettleUrl_ServerValidate(object source,ServerValidateEventArgs args) {
		args.IsValid = UrlCheck(txtSubSettleUrl.Text);
	}

	private void GetData() {
		string sMailTemplateNo = "";

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SITE_SETTLE_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PSETTLE_COMPANY_CD",DbSession.DbType.VARCHAR2,lstSettleCompanyCd.SelectedValue);
			db.ProcedureInParm("PSETTLE_TYPE",DbSession.DbType.VARCHAR2,lstSettleType.SelectedValue);
			db.ProcedureOutParm("PCP_ID_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCP_PASSWORD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSETTLE_URL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PBACK_URL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCONTINUE_SETTLE_URL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSUB_SETTLE_URL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREGEX_LOGIN_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREGEX_ASP_AD_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREGEX_POINT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREGEX_AMT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREGEX_SETTLE_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHOST_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHOST_NM2",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHOST_NM3",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PAFFILI_ADD_RECEIPT_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PNOT_CREATE_SETTLE_REQ_LOG_FLG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtCpIdNo.Text = db.GetStringValue("PCP_ID_NO");
				txtCpPassword.Text = db.GetStringValue("PCP_PASSWORD");
				txtSettleUrl.Text = db.GetStringValue("PSETTLE_URL");
				txtBackUrl.Text = db.GetStringValue("PBACK_URL");
				txtContinueSettleUrl.Text = db.GetStringValue("PCONTINUE_SETTLE_URL");
				txtSubSettleUrl.Text = db.GetStringValue("PSUB_SETTLE_URL");
				txtHostNm.Text = db.GetStringValue("PHOST_NM");
				txtHostNm2.Text = db.GetStringValue("PHOST_NM2");
				txtHostNm3.Text = db.GetStringValue("PHOST_NM3");
				txtRegexAspCd.Text = db.GetStringValue("PREGEX_ASP_AD_CD");
				txtRegexLoginId.Text = db.GetStringValue("PREGEX_LOGIN_ID");
				txtRegexPoint.Text = db.GetStringValue("PREGEX_POINT");
				txtRegexAmt.Text = db.GetStringValue("PREGEX_AMT");
				txtRegexSettleSeq.Text = db.GetStringValue("PREGEX_SETTLE_SEQ");
				chkAffiliSalesAddReceiptFlag.Checked = db.GetStringValue("PAFFILI_ADD_RECEIPT_FLAG").Equals("1");
				chkNotCreateSettleReqLogFlag.Checked = db.GetStringValue("PNOT_CREATE_SETTLE_REQ_LOG_FLG").Equals("1");
				if (!db.GetStringValue("PMAIL_TEMPLATE_NO").Equals("")) {
					sMailTemplateNo = db.GetStringValue("PMAIL_TEMPLATE_NO");
				}

			} else {
				ClearField();
			}

			using (Site oSite = new Site()) {
				if (oSite.GetOne(lstSiteCd.SelectedValue)) {
					lblSiteNm.Text = oSite.siteNm;
				}
			}

			using (SettleCompany oSettleCompany = new SettleCompany()) {
				if (oSettleCompany.GetOne(lstSettleCompanyCd.SelectedValue)) {
					lblSettleCompanyNm.Text = oSettleCompany.settleCompanyNm;
				}
			}

			using (CodeDtl oCodeDtl = new CodeDtl()) {
				if (oCodeDtl.GetOne("74",lstSettleType.SelectedValue)) {
					lblSettleType.Text = oCodeDtl.codeNm;
				}
			}

			using (SiteSettle oSiteSettle = new SiteSettle()) {
				if (oSiteSettle.MailTemplateExistance(lstSiteCd.SelectedValue)) {
					lstMailTemplate.DataSourceID = "dsMailTemplate";
				}
				lstMailTemplate.DataBind();
				lstMailTemplate.Items.Insert(0,new ListItem("",""));

				if (!sMailTemplateNo.Equals("")) {
					lstMailTemplate.SelectedValue = sMailTemplateNo;
				} else {
					lstMailTemplate.SelectedIndex = 0;
				}
			}

		}

		if (!lstSettleType.SelectedValue.Equals(ViCommConst.SETTLE_POINT_AFFILIATE)) {
			plcPointAffiliate.Visible = false;
		} else {
			plcPointAffiliate.Visible = true;
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		lstSeekSiteCd.SelectedIndex = lstSiteCd.SelectedIndex;
		pnlKey.Enabled = false;
	}

	private void UpdateData(int pDelFlag) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SITE_SETTLE_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PSETTLE_COMPANY_CD",DbSession.DbType.VARCHAR2,lstSettleCompanyCd.SelectedValue);
			db.ProcedureInParm("PSETTLE_TYPE",DbSession.DbType.VARCHAR2,lstSettleType.SelectedValue);
			db.ProcedureInParm("PCP_ID_NO",DbSession.DbType.VARCHAR2,txtCpIdNo.Text);
			db.ProcedureInParm("PCP_PASSWORD",DbSession.DbType.VARCHAR2,txtCpPassword.Text);
			db.ProcedureInParm("PSETTLE_URL",DbSession.DbType.VARCHAR2,txtSettleUrl.Text);
			db.ProcedureInParm("PBACK_URL",DbSession.DbType.VARCHAR2,txtBackUrl.Text);
			db.ProcedureInParm("PCONTINUE_SETTLE_URL",DbSession.DbType.VARCHAR2,txtContinueSettleUrl.Text);
			db.ProcedureInParm("PSUB_SETTLE_URL",DbSession.DbType.VARCHAR2,txtSubSettleUrl.Text);
			db.ProcedureInParm("PMAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,lstMailTemplate.SelectedValue);
			db.ProcedureInParm("PREGEX_LOGIN_ID",DbSession.DbType.VARCHAR2,txtRegexLoginId.Text);
			db.ProcedureInParm("PREGEX_ASP_AD_CD",DbSession.DbType.VARCHAR2,txtRegexAspCd.Text);
			db.ProcedureInParm("PREGEX_POINT",DbSession.DbType.VARCHAR2,txtRegexPoint.Text);
			db.ProcedureInParm("PREGEX_AMT",DbSession.DbType.VARCHAR2,txtRegexAmt.Text);
			db.ProcedureInParm("PREGEX_SETTLE_SEQ",DbSession.DbType.VARCHAR2,txtRegexSettleSeq.Text);
			db.ProcedureInParm("PHOST_NM",DbSession.DbType.VARCHAR2,txtHostNm.Text);
			db.ProcedureInParm("PHOST_NM2",DbSession.DbType.VARCHAR2,txtHostNm2.Text);
			db.ProcedureInParm("PHOST_NM3",DbSession.DbType.VARCHAR2,txtHostNm3.Text);
			db.ProcedureInParm("PAFFILI_ADD_RECEIPT_FLAG",DbSession.DbType.NUMBER,chkAffiliSalesAddReceiptFlag.Checked);
			db.ProcedureInParm("PNOT_CREATE_SETTLE_REQ_LOG_FLG",DbSession.DbType.NUMBER,chkNotCreateSettleReqLogFlag.Checked);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		int iIdx = lstSiteCd.SelectedIndex;
		InitPage();
		GetList();
		lstSeekSiteCd.SelectedIndex = iIdx;
	}
}
