<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ModifyHistoryList.aspx.cs" Inherits="System_ModifyHistoryList" Title="変更履歴"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="変更履歴"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<asp:Label ID="lblDocSeq" runat="server" Text="" Visible="false"></asp:Label>
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[変更内容]</legend>
						<table border="0" style="width: 850px" class="tableStyle">
							<tr>
								<td class="tdHeaderSmallStyle">
									タイトル
								</td>
								<td class="tdDataStyle" colspan="3">
									<asp:TextBox ID="txtDocTitle" runat="server" MaxLength="80" Width="350px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderSmallStyle">
									変更日
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtStartPubDay" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrStartPubDay" runat="server" ErrorMessage="変更日を入力して下さい。" ControlToValidate="txtStartPubDay" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RangeValidator ID="vdeStartPubDay" runat="server" ControlToValidate="txtStartPubDay" ErrorMessage="変更日を正しく入力して下さい。" MaximumValue="2099/12/31" MinimumValue="1900/01/01"
										Type="Date" SetFocusOnError="True" ValidationGroup="Detail">*</asp:RangeValidator>
								</td>
								<td class="tdHeaderSmallStyle">
									変更者
								</td>
								<td class="tdDataStyle" width="450px">
									<asp:TextBox ID="txtModifyUserNm" runat="server" MaxLength="20" Width="130px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrModifyUserNm" runat="server" ErrorMessage="変更者を入力して下さい。" ControlToValidate="txtModifyUserNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderSmallStyle">
									承認日
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtApproveDay" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrApproveDay" runat="server" ErrorMessage="承認日を入力して下さい。" ControlToValidate="txtApproveDay" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RangeValidator ID="vdeApproveDay" runat="server" ControlToValidate="txtApproveDay" ErrorMessage="承認日を正しく入力して下さい。" MaximumValue="2099/12/31" MinimumValue="1900/01/01"
										Type="Date" SetFocusOnError="True" ValidationGroup="Detail">*</asp:RangeValidator>
								</td>
								<td class="tdHeaderSmallStyle2">
									承認者
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtApproveUserNm" runat="server" MaxLength="20" Width="130px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrApproveUserNm" runat="server" ErrorMessage="承認者を入力して下さい。" ControlToValidate="txtApproveUserNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderSmallStyle2">
									変更内容
								</td>
								<td class="tdDataStyle" colspan="3">
									<pin:pinEdit ID="txtHtmlDoc" runat="server" UserMode="HTML" DisplayStyle="Style5" DocumentView="True" HtmlMode="Body" Ruler="False" ReturnMode="BR" SpellMode="Inline"
										ToolbarConfiguration="ToolbarDesigner" Toolbar="T09SE1011601213626722232427282930313233343536;B53SE54SE55" FormatMode="Advanced" Optimizer="True"
										BorderWidth="1px" Height="500px" Width="780px" DocumentWidth="700" Font-Bold="False" FontSizeType="px" RelativeBaseImageUrl="/" RelativeBaseLinkUrl="/">
									</pin:pinEdit>
									<asp:TextBox ID="txtHtmlDocText" runat="server" TextMode="MultiLine" Height="500px" Width="780px" Visible="false"></asp:TextBox>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlSeek">
			<fieldset>
				<legend>[検索条件]</legend>
					<table border="0" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle2">
								変更日
							</td>
							<td class="tdDataStyle" colspan="5">
								<asp:DropDownList ID="lstFromYYYY" runat="server">
								</asp:DropDownList>年
								<asp:DropDownList ID="lstFromMM" runat="server">
								</asp:DropDownList>月

								<asp:DropDownList ID="lstFromDD" runat="server">
								</asp:DropDownList>日〜

								<asp:DropDownList ID="lstToYYYY" runat="server">
								</asp:DropDownList>年
								<asp:DropDownList ID="lstToMM" runat="server">
								</asp:DropDownList>月

								<asp:DropDownList ID="lstToDD" runat="server">
								</asp:DropDownList>日
							</td>
						</tr>
					</table>
					<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" CausesValidation="False" />
					<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" CausesValidation="False" />
					<asp:Button runat="server" ID="btnRegist" Text="変更履歴追加" CssClass="seekbutton" OnClick="btnRegist_Click" CausesValidation="False" />
					<asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" OnClick="btnCSV_Click" CausesValidation="False" />
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[変更履歴一覧]</legend>
			<asp:GridView ID="grdModifyHistory" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsModifyHistory" AllowSorting="True" SkinID="GridView" ShowHeader="false" OnDataBound="grdModifyHistory_DataBound">
				<Columns>
					<asp:TemplateField>
						<ItemTemplate>
							<table width="100%">
								<tr>
									<td class="HeaderStyle" style="text-align:left;" colspan="2">
										<asp:Label ID="lblStartPubDay" runat="server" Text='<%# Eval("START_PUB_DAY") %>'></asp:Label>&nbsp;&nbsp;
										<asp:Label Font-Bold="true" ID="lblDocTitle" runat="server" Text='<%# Eval("DOC_TITLE")%>'></asp:Label>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<asp:Literal ID="lblHtmlDoc" runat="server" Text='<%# GetHtmlDoc()%>'></asp:Literal><br />
									</td>
								</tr>
								<tr style="vertical-align:bottom;">
									<td>
										<asp:LinkButton ID="lnkHtmlDocType" runat="server" Text="編集" CommandArgument='<%#Eval("DOC_SEQ")%>' OnCommand="lnkDocSeq_Command"
											CausesValidation="False"></asp:LinkButton>
									</td>
									<td style="text-align:right;">
										<asp:Label ID="lblModify" runat="server" Text='<%# GetModifyStr()%>'></asp:Label><br />
										<asp:Label ID="lblApprove" runat="server" Text='<%# GetApproveStr()%>'></asp:Label>
									</td>
								</tr>
							</table>
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
			</asp:GridView>
			&nbsp;
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br>
				<a class="reccount">Current viewing page
					<%=grdModifyHistory.PageIndex + 1%>
					of
					<%=grdModifyHistory.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsModifyHistory" runat="server" SelectMethod="GetPageCollection" TypeName="ModifyHistory" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsModifyHistory_Selected" OnSelecting="dsModifyHistory_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pStartPubDayFrom" Type="String" />
			<asp:Parameter Name="pStartPubDayTo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrStartPubDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeStartPubDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrApproveDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdeApproveDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdrModifyUserNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdrApproveUserNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskStartPubDay" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtStartPubDay">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskApproveDay" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtApproveDay">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
