﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SiteSettleManage.aspx.cs" Inherits="System_SiteSettleManage"
	Title="サイト別決済設定" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="サイト別決済設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 800px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2" style="height: 26px">
									サイトコード
								</td>
								<td class="tdDataStyle" style="height: 26px">
									<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="140px">
									</asp:DropDownList>
								</td>
								<td class="tdHeaderStyle2" style="height: 26px">
									決済会社コード
								</td>
								<td class="tdDataStyle" style="height: 26px">
									<asp:DropDownList ID="lstSettleCompanyCd" runat="server" DataSourceID="dsSettleCompany" DataTextField="SETTLE_COMPANY_NM" DataValueField="SETTLE_COMPANY_CD"
										Width="200px">
									</asp:DropDownList>
								</td>
								<td class="tdHeaderStyle2" style="height: 26px">
									決済種別
								</td>
								<td class="tdDataStyle" style="height: 26px">
									<asp:DropDownList ID="lstSettleType" runat="server" DataSourceID="dsSettleType" DataTextField="CODE_NM" DataValueField="CODE" Width="150px">
									</asp:DropDownList>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnSeekCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[サイト別決済設定]</legend>
						<table border="0" style="width: 800px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									サイト名
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblSiteNm" runat="server" Text="Label"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									決済会社名
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblSettleCompanyCd" runat="server" Text="" Visible="false"></asp:Label>
									<asp:Label ID="lblSettleCompanyNm" runat="server" Text="Label"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									決済種別
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblSettleType" runat="server" Text="Label"></asp:Label>
								</td>
							</tr>
							<asp:PlaceHolder ID="plcOnlyOwner" runat="server">
								<tr>
									<td class="tdHeaderStyle">
										CP識別番号
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCpIdNo" runat="server" MaxLength="60" Width="60px"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										CPパスワード
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCpPassword" runat="server" MaxLength="60" Width="60px"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										決済URL
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtSettleUrl" runat="server" MaxLength="100" Width="600px" Rows="2" TextMode="MultiLine"></asp:TextBox>
										<asp:CustomValidator ID="vdcSettleUrl" runat="server" ControlToValidate="txtSettleUrl" ErrorMessage="URL中に改行が含まれています" OnServerValidate="vdcSettleUrl_ServerValidate"
											ValidationGroup="Detail">*</asp:CustomValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										戻りURL
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtBackUrl" runat="server" MaxLength="100" Width="600px" Rows="2" TextMode="MultiLine"></asp:TextBox>
										<asp:CustomValidator ID="vdcBackUrl" runat="server" ControlToValidate="txtBackUrl" ErrorMessage="URL中に改行が含まれています" OnServerValidate="vdcBackUrl_ServerValidate"
											ValidationGroup="Detail">*</asp:CustomValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										継続決済URL
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtContinueSettleUrl" runat="server" MaxLength="100" Width="600px" Rows="2" TextMode="MultiLine"></asp:TextBox>
										<asp:CustomValidator ID="vdcContinueSettleUrl" runat="server" ControlToValidate="txtContinueSettleUrl" ErrorMessage="URL中に改行が含まれています" OnServerValidate="vdcContinueSettleUrl_ServerValidate"
											ValidationGroup="Detail">*</asp:CustomValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										サブ決済URL
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtSubSettleUrl" runat="server" MaxLength="100" Width="600px" Rows="2" TextMode="MultiLine"></asp:TextBox>
										<asp:CustomValidator ID="vdcSubSettleUrl" runat="server" ControlToValidate="txtSubSettleUrl" ErrorMessage="URL中に改行が含まれています" OnServerValidate="vdcSubSettleUrl_ServerValidate"
											ValidationGroup="Detail">*</asp:CustomValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										決済結果送信元ﾎｽﾄ名
									</td>
									<td class="tdDataStyle" colspan="4">
										<asp:TextBox ID="txtHostNm" runat="server" Width="140px" MaxLength="60"></asp:TextBox>
										<asp:TextBox ID="txtHostNm2" runat="server" Width="140px" MaxLength="60"></asp:TextBox>
										<asp:TextBox ID="txtHostNm3" runat="server" Width="140px" MaxLength="60"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrHostNm" runat="server" ErrorMessage="決済結果送信ﾎｽﾄ名を入力して下さい。" ControlToValidate="txtHostNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
								</tr>
								<asp:PlaceHolder ID="plcPointAffiliate" runat="server">
									<tr>
										<td class="tdHeaderStyle">
											ﾎﾟｲﾝﾄｱﾌﾘ入金累計加算
										</td>
										<td class="tdDataStyle">
											<asp:CheckBox ID="chkAffiliSalesAddReceiptFlag" runat="server" Text="ﾎﾟｲﾝﾄｱﾌﾘｴｰﾄ入金をﾕｰｻﾞｰの入金累計に加算する">
											</asp:CheckBox>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											ﾛｸﾞｲﾝID取得正規表現
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtRegexLoginId" runat="server" MaxLength="100" Width="600px"></asp:TextBox>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											ASP広告ｺｰﾄﾞ取得正規表現
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtRegexAspCd" runat="server" MaxLength="100" Width="600px"></asp:TextBox>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											成果ﾎﾟｲﾝﾄ取得正規表現
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtRegexPoint" runat="server" MaxLength="100" Width="600px"></asp:TextBox>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											売上取得用正規表現
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtRegexAmt" runat="server" MaxLength="100" Width="600px"></asp:TextBox>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											決済番号取得正規表現
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtRegexSettleSeq" runat="server" MaxLength="100" Width="600px"></asp:TextBox>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											決済要求ログを作成しない
										</td>
										<td class="tdDataStyle">
											<asp:CheckBox ID="chkNotCreateSettleReqLogFlag" runat="server"></asp:CheckBox>
										</td>
									</tr>
								</asp:PlaceHolder>
							</asp:PlaceHolder>
							<tr>
								<td class="tdHeaderStyle2">
									メールテンプレート番号
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstMailTemplate" runat="server" Width="309px" DataSourceID="dsMailTemplate" DataTextField="TEMPLATE_NM" DataValueField="MAIL_TEMPLATE_NO">
									</asp:DropDownList>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[サイト別決済設定一覧]</legend>
			<table border="0" style="width: 700px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle2">
						サイトコード
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="120px">
						</asp:DropDownList>
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnRegist" Text="追加" CssClass="seekbutton" CausesValidation="False" OnClick="btnRegist_Click" />
			<br />
			<br />
			<asp:GridView ID="grdSiteSettle" runat="server" AutoGenerateColumns="False" DataSourceID="dsSiteSettle" AllowSorting="True" SkinID="GridViewColor">
				<Columns>
					<asp:BoundField DataField="SETTLE_COMPANY_CD" HeaderText="ID" SortExpression="SETTLE_COMPANY_CD" />
					<asp:BoundField DataField="SETTLE_COMPANY_NM" HeaderText="決済会社名" SortExpression="SETTLE_COMPANY_NM" />
					<asp:TemplateField SortExpression="SETTLE_TYPE_NM" HeaderText="決済種別">
						<ItemTemplate>
							<asp:LinkButton ID="lnkSettleType" runat="server" Text='<%# Eval("SETTLE_TYPE_NM") %>' CommandArgument='<%# string.Format("{0}:{1}:{2}",Eval("SITE_CD"),Eval("SETTLE_COMPANY_CD"),Eval("SETTLE_TYPE")) %>'
								OnCommand="lnkSettleType_Command" CausesValidation="False">
							</asp:LinkButton>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField DataField="UPDATE_DATE" HeaderText="更新日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
						<ItemStyle HorizontalAlign="Left" />
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br />
				<a class="reccount">Current viewing page
					<%=grdSiteSettle.PageIndex + 1%>
					of
					<%=grdSiteSettle.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsSiteSettle" runat="server" SelectMethod="GetPageCollection" TypeName="SiteSettle" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsSiteSettle_Selected" OnSelecting="dsSiteSettle_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSettleCompany" runat="server" SelectMethod="GetList" TypeName="SettleCompany"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSettleType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="83" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsMailTemplate" runat="server" SelectMethod="GetNewVerList" TypeName="MailTemplate" OnSelecting="dsMailTemplate_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter DefaultValue="1" Name="pSexCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdcSettleUrl" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdcBackUrl" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdcContinueSettleUrl" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdcSubSettleUrl" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Custom" TargetControlID="txtSettleUrl" FilterMode="InvalidChars"
		InvalidChars="&#13;&#10;">
	</ajaxToolkit:FilteredTextBoxExtender>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="true" FilterType="Custom" TargetControlID="txtBackUrl" FilterMode="InvalidChars"
		InvalidChars="&#13;&#10;" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" Enabled="true" FilterType="Custom" TargetControlID="txtContinueSettleUrl"
		FilterMode="InvalidChars" InvalidChars="&#13;&#10;" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9" TargetControlID="vdrHostNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
