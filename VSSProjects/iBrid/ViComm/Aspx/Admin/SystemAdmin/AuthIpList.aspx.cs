﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;


public partial class SystemAdmin_AuthIpList : System.Web.UI.Page {

	private string recCount = string.Empty;

	protected void Page_Load(object sender, EventArgs e) {
		if (!IsPostBack) {
			FirstLoad();
		}
	}
	protected void FirstLoad() {
		InitPage();
	}

	protected void InitPage() {
		ClearFirld();
		DataBind();
	}
	protected void ClearFirld() {
		pnlMainte.Visible = false;
		recCount = string.Empty;
		txtIpAddr.Text = string.Empty;
		chkRedirectPcPageFlag.Checked = false;
		chkNaMobilePageFlag.Checked = false;
	}

	protected void UpdateData(string pIpAddr,string pRedirectPcPageFlag,string pNaMobilePageFlag,int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("AUTH_IP_MAINTE");
			db.ProcedureInParm("PIP_ADDR"				, DbSession.DbType.VARCHAR2	, pIpAddr);
			db.ProcedureInParm("PREDIRECT_PC_PAGE_FLAG"	, DbSession.DbType.VARCHAR2	, pRedirectPcPageFlag);
			db.ProcedureInParm("PNA_MOBILE_PAGE_FLAG"	, DbSession.DbType.VARCHAR2	, pNaMobilePageFlag);
			db.ProcedureInParm("PDEL_FLAG"				, DbSession.DbType.NUMBER	, pDelFlag);
			db.ProcedureOutParm("PSTATUS"				, DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	protected string IsChecked(object pFirld) { 
		if(pFirld.ToString().Equals(ViComm.ViCommConst.FLAG_ON_STR)) {
			return "○";
		} else {
			return "×";
		}
	}

	#region === Event Methods ===

	protected void btnRegist_Click(object sender, EventArgs e) {
		pnlMainte.Visible = true;
	}

	protected void btnUpdate_Click(object sender, EventArgs e) {
		if (IsValid) {
			string sChkRedirectPcPageFlag = (chkRedirectPcPageFlag.Checked) ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
			string sChkNaMobilePageFlag = (chkNaMobilePageFlag.Checked) ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
			UpdateData(txtIpAddr.Text,sChkRedirectPcPageFlag,sChkNaMobilePageFlag,0);
			InitPage();
		}
	}

	protected void btnCancel_Click(object sender, EventArgs e) {
		ClearFirld();
	}

	protected void lnkAuthIpDelete_OnCommand(object sender, CommandEventArgs e) {
		UpdateData(e.CommandArgument.ToString(),string.Empty,string.Empty, 1);
		InitPage();
	}

	protected void dsAuthIp_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void vdcIpAddr_ServerValidate(object sender, ServerValidateEventArgs e) {
		using (AuthIP oAuthIp = new AuthIP()){
			e.IsValid = !oAuthIp.IsExist(txtIpAddr.Text);
		}
	}
	#endregion

}
