﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class MasterPage:System.Web.UI.MasterPage {

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
		}
		form1.Attributes["onkeydown"] = "if(event.keyCode==13){if(window.event.srcElement.type!='submit' && window.event.srcElement.type!='textarea'){return false;}}";
	}
}
