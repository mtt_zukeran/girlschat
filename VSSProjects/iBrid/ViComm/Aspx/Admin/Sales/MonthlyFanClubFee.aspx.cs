﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 月別FC会費売上
--	Progaram ID		: MonthlyFanClubFee
--
--  Creation Date	: 2013.03.06
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Sales_MonthlyFanClubFee:System.Web.UI.Page {
	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			SysPrograms.SetupYear(lstYearFrom);
			SysPrograms.SetupYear(lstYearTo);

			lstYearFrom.SelectedValue = DateTime.Now.AddMonths(-5).ToString("yyyy");
			lstMonthFrom.SelectedValue = DateTime.Now.AddMonths(-5).ToString("MM");
			lstYearTo.SelectedValue = DateTime.Now.ToString("yyyy");
			lstMonthTo.SelectedValue = DateTime.Now.ToString("MM");
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			lstSiteCd.DataSourceID = "";
			DataBind();
		}
	}

	protected void vdcFromTo_ServerValidate(object source,ServerValidateEventArgs args) {
		if (this.IsValid) {
			DateTime dtCreateDateFrom = DateTime.Parse(string.Format("{0}/{1}/01",lstYearFrom.SelectedValue,lstMonthFrom.SelectedValue));
			DateTime dtCreateDateTo = DateTime.Parse(string.Format("{0}/{1}/01",lstYearTo.SelectedValue,lstMonthTo.SelectedValue));

			if (dtCreateDateFrom > dtCreateDateTo) {
				args.IsValid = false;
			}
		}
	}

	protected void dsMonthlyFanClubFee_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		string sYearFrom = lstYearFrom.SelectedValue;
		string sMonthFrom = lstMonthFrom.SelectedValue;
		string sYearTo = lstYearTo.SelectedValue;
		string sMonthTo = lstMonthTo.SelectedValue;
		string sDayTo = string.Format("{0:D2}",DateTime.DaysInMonth(int.Parse(sYearTo),int.Parse(sMonthTo)));

		MonthlyFanClubFee.SearchCondition oSearchCondition = new MonthlyFanClubFee.SearchCondition();
		oSearchCondition.SiteCd = lstSiteCd.SelectedValue;
		oSearchCondition.CreateDateFrom = string.Format("{0}/{1}/01 00:00:00",sYearFrom,sMonthFrom);
		oSearchCondition.CreateDateTo = string.Format("{0}/{1}/{2} 23:59:59",sYearTo,sMonthTo,sDayTo);
		e.InputParameters[0] = oSearchCondition;
	}

	protected string GetSettleAmtUrl(object pCreateMonth) {
		string sUrl = string.Empty;
		string sSiteCd = lstSiteCd.SelectedValue;
		string sCreateMonth = iBridUtil.GetStringValue(pCreateMonth);
		string sReportDayFrom = string.Format("{0}/01",sCreateMonth);
		string sReportDayTo = DateTime.Parse(string.Format("{0}/01",sCreateMonth)).AddMonths(1).AddDays(-1).ToString("yyyy/MM/dd");

		sUrl = string.Format(
			"~/Sales/SettleInquiry.aspx?sitecd={0}&reportdayfrom={1}&reportdayto={2}&settlestatus=0&fanclubfeeflag=1&return=MonthlyFanClubFee.aspx",
			sSiteCd,
			sReportDayFrom,
			sReportDayTo
		);

		return sUrl;
	}
}
