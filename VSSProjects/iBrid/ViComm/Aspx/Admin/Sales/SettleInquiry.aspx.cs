﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 決済記録
--	Progaram ID		: SettleInquiry
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Sales_SettleInquiry:System.Web.UI.Page {

	private string recCount = "";
	private int totalAmt = 0;
	private int totalCount = 0;

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			FisrtLoad();
			InitPage();
			if (!Request.QueryString.ToString().Equals("")) {
				GetList();
			}
		}
	}

	private void FisrtLoad() {
		grdSettle.PageSize = int.Parse(Session["PageSize"].ToString());
		grdSettle.DataSourceID = "";
		DataBind();
		//		if (Session["SiteCd"].ToString().Equals("")) {
		//			lstSiteCd.Items.Insert(0,new ListItem("",""));
		//		}
		lstSiteCd.DataSourceID = "";
		lstSettleType.Items.Insert(0,new ListItem("",""));
		lstSettleType.DataSourceID = "";
		lstSettleStatus.Items.Insert(0,new ListItem("",""));
		lstSettleStatus.DataSourceID = "";
		this.btnCSV.Visible = true;
	}

	private void InitPage() {
		//grdSettle.AllowSorting = false;
		grdSettle.PageSize = 1;
		pnlInfo.Visible = false;
		ClearField();
		if (!iBridUtil.GetStringValue(Request.QueryString["sitecd"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		} else if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		if (!iBridUtil.GetStringValue(Request.QueryString["reportdayfrom"]).Equals("")) {
			txtReportDayFrom.Text = iBridUtil.GetStringValue(Request.QueryString["reportdayfrom"]);
		}
		if (!iBridUtil.GetStringValue(Request.QueryString["reportdayto"]).Equals("")) {
			txtReportDayTo.Text = iBridUtil.GetStringValue(Request.QueryString["reportdayto"]);
		}
		if (!iBridUtil.GetStringValue(Request.QueryString["settleType"]).Equals("")) {
			lstSettleType.SelectedValue = iBridUtil.GetStringValue(Request.QueryString["settleType"]);
		}
		if (!iBridUtil.GetStringValue(Request.QueryString["settlestatus"]).Equals("")) {
			lstSettleStatus.SelectedValue = iBridUtil.GetStringValue(Request.QueryString["settlestatus"]);
		}
		if (!iBridUtil.GetStringValue(Request.QueryString["adcd"]).Equals("")) {
			txtAdCd.Text = iBridUtil.GetStringValue(Request.QueryString["adcd"]);
		}
		if (iBridUtil.GetStringValue(Request.QueryString["fanclubfeeflag"]).Equals(ViCommConst.FLAG_ON_STR)) {
			chkFanClubFeeFlag.Checked = true;
		}
		if (!iBridUtil.GetStringValue(Request.QueryString["settlecompany"]).Equals("")) {
			ViewState["SETTLE_COMPNAY_CD"] = iBridUtil.GetStringValue(Request.QueryString["settlecompany"]);
		}
	}

	private void ClearField() {
		totalAmt = 0;
		totalCount = 0;
		txtLoginId.Text = "";
		txtTel.Text = "";
		txtReportDayFrom.Text = DateTime.Now.ToString("yyyy/MM/dd");
		txtReportDayTo.Text = DateTime.Now.ToString("yyyy/MM/dd");
		//txtAnswerDayFrom.Text = DateTime.Now.ToString("yyyy/MM/dd");
		//txtAnswerDayTo.Text = DateTime.Now.ToString("yyyy/MM/dd");
		txtAdCd.Text = "";
		chkPagingOff.Checked = true;
		chkWithoutCredit300Flag.Checked = true;
		chkFanClubFeeFlag.Checked = false;
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		// 画面を初期化するために自画面へリダイレクト


		Response.Redirect(Request.Url.PathAndQuery);
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		if (grdSettle.Rows.Count == 0) {
			return;
		}

		DataTable oCsvData;
		using (SettleLog oSettleLog = new SettleLog()) {
			DateTime oToDate;
			if (string.IsNullOrEmpty(this.txtReportDayTo.Text)) {
				oToDate = DateTime.Parse(this.txtReportDayFrom.Text);
			} else {
				oToDate = DateTime.Parse(this.txtReportDayTo.Text);
			}
			if (!string.IsNullOrEmpty(this.txtAnswerDayFrom.Text) && string.IsNullOrEmpty(this.txtAnswerDayTo.Text)) {
				this.txtAnswerDayTo.Text = this.txtAnswerDayFrom.Text;
			}
			if (string.IsNullOrEmpty(this.txtAnswerDayFrom.Text) && !string.IsNullOrEmpty(this.txtAnswerDayTo.Text)) {
				this.txtAnswerDayFrom.Text = this.txtAnswerDayTo.Text;
			}

			using (DataSet oDataSet = oSettleLog.GetPageCollection(
				this.lstSiteCd.SelectedValue,
				this.txtReportDayFrom.Text,
				oToDate.AddDays(1).ToString("yyyy/MM/dd"),
				this.lstSettleType.SelectedValue,
				this.txtLoginId.Text.TrimEnd(),
				this.txtTel.Text.TrimEnd(),
				this.lstSettleStatus.SelectedValue,
				this.txtAdCd.Text.TrimEnd(),
				iBridUtil.GetStringValue(ViewState["SETTLE_COMPNAY_CD"]),
				this.txtAnswerDayFrom.Text,
				string.IsNullOrEmpty(this.txtAnswerDayTo.Text) ? string.Empty : DateTime.Parse(this.txtAnswerDayTo.Text).AddDays(1).ToString("yyyy/MM/dd"),
				chkFanClubFeeFlag.Checked ? ViCommConst.FLAG_ON_STR : string.Empty,
				chkWithoutCredit300Flag.Checked ? ViCommConst.FLAG_ON_STR : string.Empty,
				0,
				SysConst.DB_MAX_ROWS)) {

				if (oDataSet.Tables[0].Rows.Count > 0) {
					oCsvData = oDataSet.Tables[0];
				} else {
					return;
				}
			}
		}

		//ヘッダ作成
		string sHeader = "要求日時,決済種別,決済種別名称,決済会社,ﾕｰｻﾞｰID,決済ID,電話番号,決済額,Pt,応答日時,結果,結果名称";

		//CSV出力 
		Response.ContentType = "application/download";
		Response.AppendHeader("Content-Disposition","attachment;filename=SettleList.csv");
		Response.ContentEncoding = System.Text.Encoding.GetEncoding("Shift_JIS");

		Response.Write(sHeader + "\r\n");
		foreach (DataRow oCsvRow in oCsvData.Rows) {
			DateTime oCreateDate;
			DateTime oAnswerDate;
			string sData =
				SetCsvString(DateTime.TryParse(oCsvRow["CREATE_DATE"].ToString(),out oCreateDate) ? oCreateDate.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty) + "," +
				SetCsvString(oCsvRow["SETTLE_TYPE"].ToString()) + "," +
				SetCsvString(oCsvRow["SETTLE_TYPE_NM"].ToString()) + "," +
				SetCsvString(oCsvRow["SETTLE_COMPANY_NM"].ToString()) + "," +
				SetCsvString(oCsvRow["LOGIN_ID"].ToString()) + "," +
				SetCsvString(oCsvRow["SID"].ToString()) + "," +
				string.Empty + "," +
				SetCsvString(oCsvRow["SETTLE_AMT"].ToString()) + "," +
				SetCsvString(oCsvRow["SETTLE_POINT"].ToString()) + "," +
				SetCsvString(DateTime.TryParse(oCsvRow["ANSWER_DATE"].ToString(),out oAnswerDate) ? oAnswerDate.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty) + "," +
				SetCsvString(oCsvRow["SETTLE_STATUS"].ToString()) + "," +
				SetCsvString(oCsvRow["SETTLE_STATUS_NM"].ToString());

			Response.Write(sData + "\r\n");
		}
		Response.End();
	}

	private string SetCsvString(string pData) {
		if (pData.IndexOf('"') > -1 ||
			pData.IndexOf(',') > -1 ||
			pData.IndexOf('\r') > -1 ||
			pData.IndexOf('\n') > -1 ||
			pData.StartsWith(" ") || pData.StartsWith("\t") ||
			pData.EndsWith(" ") || pData.EndsWith("\t")) {
			if (pData.IndexOf('"') > -1) {
				//"を""とする
				pData = pData.Replace("\"","\"\"");
			}
			if (pData.IndexOf('\r') > -1 || pData.IndexOf('\n') > -1) {
				//改行しない 
				pData = pData.Replace("\r","");
				pData = pData.Replace("\n","");
			}
			pData = "\"" + pData + "\"";
		}
		return pData;
	}

	protected string GetTotalAmount() {
		return totalAmt.ToString();
	}

	protected string GetTotalCount() {
		return totalCount.ToString();
	}

	protected string CheckAdminLevel(object pText) {
		if (Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_SITE_OWNER)) {
			return pText.ToString();
		} else {
			return "***********";
		}
	}

	private void GetList() {
		if (string.IsNullOrEmpty(txtReportDayTo.Text)) {
			txtReportDayTo.Text = txtReportDayFrom.Text;
		}
		DateTime dtTo = System.DateTime.ParseExact(txtReportDayTo.Text,"yyyy/MM/dd",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);
		if (!string.IsNullOrEmpty(this.txtAnswerDayFrom.Text) && string.IsNullOrEmpty(this.txtAnswerDayTo.Text)) {
			this.txtAnswerDayTo.Text = this.txtAnswerDayFrom.Text;
		}
		if (string.IsNullOrEmpty(this.txtAnswerDayFrom.Text) && !string.IsNullOrEmpty(this.txtAnswerDayTo.Text)) {
			this.txtAnswerDayFrom.Text = this.txtAnswerDayTo.Text;
		}

		using (SettleLog oLog = new SettleLog()) {
			oLog.GetTotal(
				lstSiteCd.SelectedValue,
				txtReportDayFrom.Text,
				dtTo.AddDays(1).ToString("yyyy/MM/dd"),
				lstSettleType.SelectedValue,
				txtLoginId.Text,
				txtTel.Text,
				lstSettleStatus.SelectedValue,
				txtAdCd.Text,
				iBridUtil.GetStringValue(ViewState["SETTLE_COMPNAY_CD"]),
				txtAnswerDayFrom.Text,
				string.IsNullOrEmpty(this.txtAnswerDayTo.Text) ? string.Empty : DateTime.Parse(this.txtAnswerDayTo.Text).AddDays(1).ToString("yyyy/MM/dd"),
				chkFanClubFeeFlag.Checked ? ViCommConst.FLAG_ON_STR : string.Empty,
				chkWithoutCredit300Flag.Checked ? ViCommConst.FLAG_ON_STR : string.Empty,
				out totalCount,
				out totalAmt
			);
		}

		if (chkPagingOff.Checked) {
			grdSettle.PageSize = 99999;
		} else {
			grdSettle.PageSize = 50;
		}
		grdSettle.DataSourceID = "dsSettleLog";

		DataBind();
		pnlInfo.Visible = true;
	}

	protected void dsSettleLog_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = txtReportDayFrom.Text;
		if (txtReportDayTo.Text.Equals("")) {
			txtReportDayTo.Text = txtReportDayFrom.Text;
		}
		DateTime dtTo = System.DateTime.ParseExact(txtReportDayTo.Text,"yyyy/MM/dd",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);
		e.InputParameters[2] = dtTo.AddDays(1).ToString("yyyy/MM/dd");
		e.InputParameters[3] = lstSettleType.SelectedValue;
		e.InputParameters[4] = txtLoginId.Text;
		e.InputParameters[5] = txtTel.Text;
		e.InputParameters[6] = lstSettleStatus.SelectedValue;
		e.InputParameters[7] = txtAdCd.Text;
		e.InputParameters[8] = iBridUtil.GetStringValue(ViewState["SETTLE_COMPNAY_CD"]);
		e.InputParameters[9] = txtAnswerDayFrom.Text;
		e.InputParameters[10] = string.IsNullOrEmpty(this.txtAnswerDayTo.Text) ? string.Empty : DateTime.Parse(this.txtAnswerDayTo.Text).AddDays(1).ToString("yyyy/MM/dd");
		e.InputParameters[11] = chkFanClubFeeFlag.Checked ? ViCommConst.FLAG_ON_STR : string.Empty;
		e.InputParameters[12] = chkWithoutCredit300Flag.Checked ? ViCommConst.FLAG_ON_STR : string.Empty;
	}

	protected void dsSettleLog_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}
}
