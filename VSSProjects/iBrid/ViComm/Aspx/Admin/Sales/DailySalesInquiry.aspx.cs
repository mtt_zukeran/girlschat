﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 日別売上状況
--	Progaram ID		: DailySalesInquiry
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Sales_DailySalesInquiry:System.Web.UI.Page {

	public class colInfo {
		public string title;
		public string settleType;
		public string countField;
		public string amtField;

		public colInfo(string pSettleType,string pTitle,string pCountFiled,string pAmtField) {
			title = pTitle;
			settleType = pSettleType;
			countField = pCountFiled;
			amtField = pAmtField;
		}
	}


	public class SalesCountTemplate:System.Web.UI.ITemplate {
		private ListItemType templateType;
		private string bindField;

		public SalesCountTemplate(ListItemType pItemtype,string pBindField) {
			templateType = pItemtype;
			bindField = pBindField;
		}

		public void InstantiateIn(System.Web.UI.Control container) {
			PlaceHolder ph = new PlaceHolder();
			Label item1 = new Label();
			item1.ID = bindField;
			switch (templateType) {
				case ListItemType.Item:
					ph.Controls.Add(item1);
					ph.DataBinding += new EventHandler(Item_DataBinding);
					container.Controls.Add(ph);
					break;
			}
		}

		public void Item_DataBinding(object sender,EventArgs e) {
			PlaceHolder ph = (PlaceHolder)sender;
			GridViewRow container = (GridViewRow)ph.NamingContainer;
			((Label)ph.FindControl(bindField)).Text = ((DataRowView)container.DataItem)[this.bindField].ToString();
		}
	}

	public class SalesAmtTemplate:System.Web.UI.ITemplate {
		private ListItemType templateType;
		private string bindField;
		private string settleType;
		private string adminType;

		public SalesAmtTemplate(ListItemType pItemtype,string pBindField,string pSettleType,string pAdminType) {
			templateType = pItemtype;
			bindField = pBindField;
			settleType = pSettleType;
			adminType = pAdminType;
		}

		public void InstantiateIn(System.Web.UI.Control container) {
			PlaceHolder ph = new PlaceHolder();
			HyperLink item1 = new HyperLink();
			item1.ID = bindField;
			switch (templateType) {
				case ListItemType.Item:
					ph.Controls.Add(item1);
					ph.DataBinding += new EventHandler(Item_DataBinding);
					container.Controls.Add(ph);
					break;
			}
		}

		public void Item_DataBinding(object sender,EventArgs e) {
			PlaceHolder ph = (PlaceHolder)sender;
			GridViewRow container = (GridViewRow)ph.NamingContainer;
			HyperLink link = (HyperLink)ph.FindControl(bindField);

			link.Text = ((DataRowView)container.DataItem)[this.bindField].ToString();

			string sSiteCd = ((DataRowView)container.DataItem)["SITE_CD"].ToString();
			string sSalesDay = ((DataRowView)container.DataItem)["SALES_DAY"].ToString();

			link.NavigateUrl = string.Format("~/Sales/ReceiptInquiry.aspx?sitecd={0}&reportdayfrom={1}&reportdayto={1}&settletype={2}&without0yen=0&without300yen=0&return={3}",sSiteCd,sSalesDay,this.settleType,"DailySalesInquiry.aspx");
			link.Enabled = (string.Compare(this.adminType, ViCommConst.RIGHT_STAFF) > 0);
		}
	}

	private List<colInfo> colList;
	private int cashSalesCount;
	private int cashSalesAmt;
	private int prepaidSalesCount;
	private int prepaidSalesAmt;
	private int creditPackSalesCount;
	private int creditPackSalesAmt;
	private int ccheckSalesCount;
	private int ccheckSalesAmt;
	private int bitcashSalesCount;
	private int bitcashSalesAmt;
	private int smoneySalesCount;
	private int smoneySalesAmt;
	private int conviniSalesCount;
	private int conviniSalesAmt;
	private int creditSalesCount;
	private int creditSalesAmt;
	private int pointAffiliateSalesCount;
	private int pointAffiliateSalesAmt;
	private int adultIdSalesCount;
	private int adultIdSalesAmt;
	private int conviniDirectSalesCount;
	private int conviniDirectSalesAmt;
	private int edySalesCount;
	private int edySalesAmt;
	private int gmoneySalesCount;
	private int gmoneySalesAmt;
	private int mobileCashSalesCount;
	private int mobileCashSalesAmt;
	private int payAfterPackSalesCount;
	private int payAfterPackSalesAmt;
	private int zeroSettleSalesCount;
	private int zeroSettleSalesAmt;
	private int gigaPointSalesCount;
	private int gigaPointSalesAmt;
	private int autoReceiptSalesCount;
	private int autoReceiptSalesAmt;
	private int rakutenSalesCount;
	private int rakutenSalesAmt;
	private int totalSalesCount;
	private int totalSalesAmt;
	private int settleCompanyMask;

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		grdSales.DataSourceID = "";
		ClearField();
		DataBind();

		if (!IsPostBack) {
            SysPrograms.SetupYear(lstYYYY);

			lstMM.SelectedValue = DateTime.Now.ToString("MM");
			lstSiteCd.DataSourceID = "";
			GetList();
		}
	}

	private void ClearField() {
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		Response.Redirect(Request.Url.PathAndQuery);
	}

	protected void grdSales_RowDataBound(object sender,GridViewRowEventArgs e) {

		if (e.Row.RowType == DataControlRowType.DataRow) {
			totalSalesCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TOTAL_SALES_COUNT"));
			totalSalesAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TOTAL_SALES_AMT"));
			cashSalesCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CASH_SALES_COUNT"));
			cashSalesAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CASH_SALES_AMT"));
			prepaidSalesCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PREPAID_SALES_COUNT"));
			prepaidSalesAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PREPAID_SALES_AMT"));
			creditPackSalesCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CREDIT_PACK_SALES_COUNT"));
			creditPackSalesAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CREDIT_PACK_SALES_AMT"));
			ccheckSalesCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CCHECK_SALES_COUNT"));
			ccheckSalesAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CCHECK_SALES_AMT"));
			bitcashSalesCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"BITCASH_SALES_COUNT"));
			bitcashSalesAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"BITCASH_SALES_AMT"));
			smoneySalesCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"SMONEY_SALES_COUNT"));
			smoneySalesAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"SMONEY_SALES_AMT"));
			conviniSalesCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CONVINI_SALES_COUNT"));
			conviniSalesAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CONVINI_SALES_AMT"));
			creditSalesCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CREDIT_SALES_COUNT"));
			creditSalesAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CREDIT_SALES_AMT"));
			pointAffiliateSalesCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"POINT_AFFILIATE_SALES_COUNT"));
			pointAffiliateSalesAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"POINT_AFFILIATE_SALES_AMT"));
			adultIdSalesCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"ADULTID_SALES_COUNT"));
			adultIdSalesAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"ADULTID_SALES_AMT"));
			conviniDirectSalesCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CONVINI_DIRECT_SALES_COUNT"));
			conviniDirectSalesAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CONVINI_DIRECT_SALES_AMT"));
			edySalesCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"EDY_SALES_COUNT"));
			edySalesAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"EDY_SALES_AMT"));
			gmoneySalesCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"GMONEY_SALES_COUNT"));
			gmoneySalesAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"GMONEY_SALES_AMT"));
			payAfterPackSalesCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PAY_AFTER_PACK_SALES_COUNT"));
			payAfterPackSalesAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PAY_AFTER_PACK_SALES_AMT"));
			mobileCashSalesCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"MOBILE_CASH_SALES_COUNT"));
			mobileCashSalesAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"MOBILE_CASH_SALES_AMT"));
			zeroSettleSalesCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"ZERO_SETTLE_SALES_COUNT"));
			zeroSettleSalesAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"ZERO_SETTLE_SALES_AMT"));
			gigaPointSalesCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"GIGA_POINT_SALES_COUNT"));
			gigaPointSalesAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"GIGA_POINT_SALES_AMT"));
			autoReceiptSalesCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"AUTO_RECEIPT_SALES_COUNT"));
			autoReceiptSalesAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"AUTO_RECEIPT_SALES_AMT"));
			rakutenSalesCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"RAKUTEN_SALES_COUNT"));
			rakutenSalesAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"RAKUTEN_SALES_AMT"));
		} else if (e.Row.RowType == DataControlRowType.Footer) {

			e.Row.Cells[2].Text = totalSalesCount.ToString();
			e.Row.Cells[3].Text = totalSalesAmt.ToString();

			int iPos = 4;
			foreach (colInfo oInfo in colList) {
				int iCount = 0;
				int iAmt = 0;

				switch (oInfo.settleType) {
					case ViCommConst.SETTLE_CASH:
						iCount = cashSalesCount;
						iAmt = cashSalesAmt;
						break;
					case ViCommConst.SETTLE_PREPAID:
						iCount = prepaidSalesCount;
						iAmt = prepaidSalesAmt;
						break;
					case ViCommConst.SETTLE_CREDIT_PACK:
						iCount = creditPackSalesCount;
						iAmt = creditPackSalesAmt;
						break;
					case ViCommConst.SETTLE_C_CHECK:
						iCount = ccheckSalesCount;
						iAmt = ccheckSalesAmt;
						break;
					case ViCommConst.SETTLE_BITCASH:
						iCount = bitcashSalesCount;
						iAmt = bitcashSalesAmt;
						break;
					case ViCommConst.SETTLE_S_MONEY:
						iCount = smoneySalesCount;
						iAmt = smoneySalesAmt;
						break;
					case ViCommConst.SETTLE_CREDIT:
						iCount = creditSalesCount;
						iAmt = creditSalesAmt;
						break;
					case ViCommConst.SETTLE_POINT_AFFILIATE:
						iCount = pointAffiliateSalesCount;
						iAmt = pointAffiliateSalesAmt;
						break;
					case ViCommConst.SETTLE_CVSDL:
						iCount = conviniSalesCount;
						iAmt = conviniSalesAmt;
						break;
					case ViCommConst.SETTLE_CVS_DIRECT:
						iCount = conviniDirectSalesCount;
						iAmt = conviniDirectSalesAmt;
						break;
					case ViCommConst.SETTLE_ADULTID:
					case ViCommConst.SETTLE_SPS:
						iCount = adultIdSalesCount;
						iAmt = adultIdSalesAmt;
						break;
					case ViCommConst.SETTLE_EDY:
						iCount = edySalesCount;
						iAmt = edySalesAmt;
						break;
					case ViCommConst.SETTLE_G_MONEY:
						iCount = gmoneySalesCount;
						iAmt = gmoneySalesAmt;
						break;
					case ViCommConst.SETTLE_MOBILE_CASH:
						iCount = mobileCashSalesCount;
						iAmt = mobileCashSalesAmt;
						break;
					case ViCommConst.SETTLE_GIGA_POINT:
						iCount = gigaPointSalesCount;
						iAmt = gigaPointSalesAmt;
						break;
					case ViCommConst.SETTLE_AUTO_RECEIPT:
						iCount = autoReceiptSalesCount;
						iAmt = autoReceiptSalesAmt;
						break;
					case ViCommConst.SETTLE_CREDIT_AUTH:
						iCount = zeroSettleSalesCount;
						iAmt = zeroSettleSalesAmt;
						break;
					case ViCommConst.SETTLE_RAKUTEN:
						iCount = rakutenSalesCount;
						iAmt = rakutenSalesAmt;
						break;
					case ViCommConst.SETTLE_PAYMENT_AFTER_PACK:
						iCount = payAfterPackSalesCount;
						iAmt = payAfterPackSalesAmt;
						break;
				}
				e.Row.Cells[iPos].Text = iCount.ToString();
				e.Row.Cells[iPos + 1].Text = iAmt.ToString();
				iPos += 2;
			}
		}
	}

	private void GetList() {
		cashSalesCount = 0;
		cashSalesAmt = 0;
		prepaidSalesCount = 0;
		prepaidSalesCount = 0;
		creditPackSalesCount = 0;
		creditPackSalesAmt = 0;
		ccheckSalesCount = 0;
		ccheckSalesAmt = 0;
		bitcashSalesCount = 0;
		bitcashSalesAmt = 0;
		smoneySalesCount = 0;
		smoneySalesAmt = 0;
		conviniSalesCount = 0;
		conviniSalesAmt = 0;
		creditSalesCount = 0;
		creditSalesAmt = 0;
		pointAffiliateSalesCount = 0;
		pointAffiliateSalesAmt = 0;
		adultIdSalesCount = 0;
		adultIdSalesAmt = 0;
		conviniDirectSalesCount = 0;
		conviniDirectSalesAmt = 0;
		edySalesCount = 0;
		edySalesAmt = 0;
		gmoneySalesCount = 0;
		gmoneySalesAmt = 0;
		payAfterPackSalesCount = 0;
		payAfterPackSalesAmt = 0;
		mobileCashSalesCount = 0;
		mobileCashSalesAmt = 0;
		zeroSettleSalesCount = 0;
		zeroSettleSalesAmt = 0;
		gigaPointSalesCount = 0;
		gigaPointSalesAmt = 0;
		rakutenSalesCount = 0;
		rakutenSalesAmt = 0;
		autoReceiptSalesCount = 0;
		autoReceiptSalesAmt = 0;
		totalSalesCount = 0;
		totalSalesAmt = 0;

		grdSales.PageIndex = 0;
		grdSales.DataSourceID = "dsDailySales";
		DataBind();
		AddHeader();
	}


	protected Color GetBackColor(object pDayOfWeek) {
		if (pDayOfWeek.ToString().Equals("Sat")) {
			return Color.FromArgb(0xCCCCFF);
		} else {
			if (pDayOfWeek.ToString().Equals("Sun")) {
				return Color.FromArgb(0xFFCCCC);
			} else {
				return Color.Empty;
			}
		}
	}

	private void AddHeader() {

		GridViewRow row = new GridViewRow(-1,-1,DataControlRowType.Header,DataControlRowState.Normal);
		TableCell cell;
		cell = new TableCell();
		cell.ColumnSpan = 2;
		cell.RowSpan = 1;
		cell.Text = "";
		row.Cells.Add(cell);

		cell = new TableCell();
		cell.ColumnSpan = 2;
		cell.Text = "日計";
		cell.HorizontalAlign = HorizontalAlign.Center;
		row.Cells.Add(cell);

		foreach (colInfo oInfo in colList) {
			cell = new TableCell();
			cell.ColumnSpan = 2;
			cell.Text = oInfo.title;
			cell.HorizontalAlign = HorizontalAlign.Center;
			row.Cells.Add(cell);
		}
		grdSales.Controls[0].Controls.AddAt(0,row);
	}

	protected void dsDailySales_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = lstYYYY.SelectedValue;
		e.InputParameters[2] = lstMM.SelectedValue;
		e.InputParameters[3] = settleCompanyMask;
	}

	protected void grdSales_Init(object sender,EventArgs e) {
		using (ManageCompany oManageCompany = new ManageCompany()) {
			if (oManageCompany.GetOne()) {
				settleCompanyMask = oManageCompany.settleCompanyMask;
			}
		}
		colList = new List<colInfo>();

		if ((settleCompanyMask & ViCommConst.MSK_SETTLE_CASH) > 0) {
			colList.Add(new colInfo(ViCommConst.SETTLE_CASH,"現金","CASH_SALES_COUNT","CASH_SALES_AMT"));
		}
		if ((settleCompanyMask & ViCommConst.MSK_SETTLE_MOBILE_CASH) > 0) {
			colList.Add(new colInfo(ViCommConst.SETTLE_MOBILE_CASH,"携帯送金","MOBILE_CASH_SALES_COUNT","MOBILE_CASH_SALES_AMT"));
		}
		if ((settleCompanyMask & ViCommConst.MSK_SETTLE_PREPAID) > 0) {
			colList.Add(new colInfo(ViCommConst.SETTLE_PREPAID,"ﾌﾟﾘﾍﾟｰﾄﾞ","PREPAID_SALES_COUNT","PREPAID_SALES_AMT"));
		}
		if ((settleCompanyMask & ViCommConst.MSK_SETTLE_CREDIT_PACK) > 0) {
			colList.Add(new colInfo(ViCommConst.SETTLE_CREDIT_PACK,"ｸﾚｼﾞｯﾄ","CREDIT_PACK_SALES_COUNT","CREDIT_PACK_SALES_AMT"));
		}
		if ((settleCompanyMask & ViCommConst.MSK_SETTLE_CREDIT) > 0) {
			colList.Add(new colInfo(ViCommConst.SETTLE_CREDIT,"ｸﾚｼﾞｯﾄ従量","CREDIT_SALES_COUNT","CREDIT_SALES_AMT"));
		}
		if ((settleCompanyMask & ViCommConst.MSK_SETTLE_CREDIT_AUTH) > 0) {
			colList.Add(new colInfo(ViCommConst.SETTLE_CREDIT_AUTH,"ｸﾚｼﾞｯﾄ0円","ZERO_SETTLE_SALES_COUNT","ZERO_SETTLE_SALES_AMT"));
		}
		if ((settleCompanyMask & ViCommConst.MSK_SETTLE_C_CHECK) > 0) {
			colList.Add(new colInfo(ViCommConst.SETTLE_C_CHECK,"C-CHECK","CCHECK_SALES_COUNT","CCHECK_SALES_AMT"));
		}
		if ((settleCompanyMask & ViCommConst.MSK_SETTLE_BITCASH) > 0) {
			colList.Add(new colInfo(ViCommConst.SETTLE_BITCASH,"BITCASH","BITCASH_SALES_COUNT","BITCASH_SALES_AMT"));
		}
		if ((settleCompanyMask & ViCommConst.MSK_SETTLE_S_MONEY) > 0) {
			colList.Add(new colInfo(ViCommConst.SETTLE_S_MONEY,"S-MONEY","SMONEY_SALES_COUNT","SMONEY_SALES_AMT"));
		}
		if ((settleCompanyMask & ViCommConst.MSK_SETTLE_G_MONEY) > 0) {
			colList.Add(new colInfo(ViCommConst.SETTLE_G_MONEY,"G-MONEY","GMONEY_SALES_COUNT","GMONEY_SALES_AMT"));
		}
		if ((settleCompanyMask & ViCommConst.MSK_SETTLE_CVSDL) > 0) {
			colList.Add(new colInfo(ViCommConst.SETTLE_CVSDL,"CVSﾀﾞｳﾝﾛｰﾄﾞ","CONVINI_SALES_COUNT","CONVINI_SALES_AMT"));
		}
		if ((settleCompanyMask & ViCommConst.MSK_SETTLE_CVS_DIRECT) > 0) {
			colList.Add(new colInfo(ViCommConst.SETTLE_CVS_DIRECT,"CVSﾀﾞｲﾚｸﾄ","CONVINI_DIRECT_SALES_COUNT","CONVINI_DIRECT_SALES_AMT"));
		}
		if ((settleCompanyMask & ViCommConst.MSK_SETTLE_EDY) > 0) {
			colList.Add(new colInfo(ViCommConst.SETTLE_EDY,"EDY","EDY_SALES_COUNT","EDY_SALES_AMT"));
		}
		if ((settleCompanyMask & ViCommConst.MSK_SETTLE_GIGA_POINT) > 0) {
			colList.Add(new colInfo(ViCommConst.SETTLE_GIGA_POINT,"GIGAﾎﾟｲﾝﾄ","GIGA_POINT_SALES_COUNT","GIGA_POINT_SALES_AMT"));
		}
		if ((settleCompanyMask & ViCommConst.MSK_SETTLE_AUTO_RECEIPT) > 0) {
			colList.Add(new colInfo(ViCommConst.SETTLE_AUTO_RECEIPT,"自動入金","AUTO_RECEIPT_SALES_COUNT","AUTO_RECEIPT_SALES_AMT"));
		}
		if ((settleCompanyMask & ViCommConst.MSK_SETTLE_POINT_AFFILIATE) > 0) {
			colList.Add(new colInfo(ViCommConst.SETTLE_POINT_AFFILIATE,"ｱﾌﾘｴｰﾄ","POINT_AFFILIATE_SALES_COUNT","POINT_AFFILIATE_SALES_AMT"));
		}
		if ((settleCompanyMask & ViCommConst.MSK_SETTLE_ADULTID) > 0) {
			//colList.Add(new colInfo(ViCommConst.SETTLE_ADULTID,"大人ID","ADULTID_SALES_COUNT","ADULTID_SALES_AMT"));
			colList.Add(new colInfo(ViCommConst.SETTLE_SPS,"SPS","ADULTID_SALES_COUNT","ADULTID_SALES_AMT"));
		}
		if ((settleCompanyMask & ViCommConst.MSK_SETTLE_PAY_AFLTER_PACK) > 0) {
			colList.Add(new colInfo(ViCommConst.SETTLE_PAYMENT_AFTER_PACK,"後払ﾊﾟｯｸ","PAY_AFTER_PACK_SALES_COUNT","PAY_AFTER_PACK_SALES_AMT"));
		}
		if ((settleCompanyMask & ViCommConst.MSK_SETTLE_RAKUTEN) > 0) {
			colList.Add(new colInfo(ViCommConst.SETTLE_RAKUTEN,"楽天","RAKUTEN_SALES_COUNT","RAKUTEN_SALES_AMT"));
		}
		foreach (colInfo oInfo in colList) {
			TemplateField tmpCount = new TemplateField();
			TemplateField tmpAmt = new TemplateField();
			tmpCount.HeaderText = "件数";
			tmpCount.HeaderStyle.CssClass = "HeaderStyle";
			tmpCount.HeaderStyle.Font.Size = FontUnit.XSmall;
			tmpCount.FooterStyle.Font.Size = FontUnit.XSmall;
			tmpCount.FooterStyle.HorizontalAlign = HorizontalAlign.Right;
			tmpCount.ItemStyle.Width = 30;
			tmpCount.ItemStyle.CssClass = "RowStylePad";
			tmpCount.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
			tmpCount.ItemStyle.Font.Size = FontUnit.XSmall;
			tmpCount.ItemTemplate = new SalesCountTemplate(ListItemType.Item,oInfo.countField);
			tmpCount.FooterStyle.HorizontalAlign = HorizontalAlign.Right;

			tmpAmt.HeaderText = "金額";
			tmpAmt.HeaderStyle.CssClass = "HeaderStyle";
			tmpAmt.HeaderStyle.Font.Size = FontUnit.XSmall;
			tmpAmt.FooterStyle.Font.Size = FontUnit.XSmall;
			tmpAmt.FooterStyle.HorizontalAlign = HorizontalAlign.Right;
			tmpAmt.ItemStyle.Width = 60;
			tmpAmt.ItemStyle.CssClass = "RowStylePad";
			tmpAmt.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
			tmpAmt.ItemStyle.VerticalAlign = VerticalAlign.Middle;
			tmpAmt.ItemStyle.Font.Size = FontUnit.XSmall;
			tmpAmt.ItemTemplate = new SalesAmtTemplate(ListItemType.Item, oInfo.amtField, oInfo.settleType, Session["AdminType"].ToString());

			grdSales.Columns.Add(tmpCount);
			grdSales.Columns.Add(tmpAmt);
		}
		grdSales.Width = 120 + colList.Count * 100;
	}
}