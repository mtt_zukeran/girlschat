<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PaymentAfter.aspx.cs" Inherits="Sales_PaymentAfter" Title="後払請求一覧"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="後払請求一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 740px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px" AutoPostBack="True"
								OnSelectedIndexChanged="lstSiteCd_SelectedIndexChanged">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle">
							入金期日
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtReceiptLimitDayFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RequiredFieldValidator ID="vdrReceiptLimitDayFrom" runat="server" ErrorMessage="入金期日Fromを入力して下さい。" ControlToValidate="txtReceiptLimitDayFrom" ValidationGroup="Key"
								Display="Dynamic">*</asp:RequiredFieldValidator>
							<asp:RangeValidator ID="vdeReceiptLimitDayFrom" runat="server" ErrorMessage="入金期日Fromを正しく入力して下さい。" ControlToValidate="txtReceiptLimitDayFrom" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							〜&nbsp;
							<asp:TextBox ID="txtReceiptLimitDayTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RangeValidator ID="vdeReceiptLimitDayTo" runat="server" ErrorMessage="入金期日Toを正しく入力して下さい。" ControlToValidate="txtReceiptLimitDayTo" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:CompareValidator ID="vdcReceiptLimitDayFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtReceiptLimitDayFrom" ControlToValidate="txtReceiptLimitDayTo"
								Operator="GreaterThanEqual" ValidationGroup="Key">*</asp:CompareValidator>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							督促レベル
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtUrgeLevel" runat="server" MaxLength="1" Width="30px"></asp:TextBox>
						</td>
						<td class="tdHeaderStyle">
							停止日
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtLastBlackDayFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RangeValidator ID="vdeLastBlackDayFrom" runat="server" ErrorMessage="停止日Fromを正しく入力して下さい。" ControlToValidate="txtLastBlackDayFrom" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>&nbsp; 〜&nbsp;
							<asp:TextBox ID="txtLastBlackDayTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RangeValidator ID="vdeLastBlackDayTo" runat="server" ErrorMessage="停止日Toを正しく入力して下さい。" ControlToValidate="txtLastBlackDayTo" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:CompareValidator ID="vdcLastBlackDayFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtLastBlackDayFrom" ControlToValidate="txtLastBlackDayTo"
								Operator="GreaterThanEqual" ValidationGroup="Key">*</asp:CompareValidator>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							ユーザーＩＤ
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtLoginId" runat="server" MaxLength="15" Width="70px"></asp:TextBox>
						</td>
					    <td class="tdHeaderStyle2">
						    Paging Off
					    </td>
					    <td class="tdDataStyle">
						    <asp:CheckBox ID="chkPagingOff" runat="server" />
					    </td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[後払請求記録]</legend>後払請求件数&nbsp;<%#GetTotalCount()%>
				件&nbsp;&nbsp; 後払請求金額&nbsp;<%#GetTotalAmount()%>
				円
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="430px">
					<asp:GridView ID="grdPaymentAfter" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsUserMan" AllowSorting="True" SkinID="GridViewColor">
						<Columns>
							<asp:BoundField DataField="URGE_LEVEL" HeaderText="督促レベル">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="ユーザーＩＤ">
								<ItemTemplate>
									<asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# string.Format("../Man/ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>'
										Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" />
							</asp:TemplateField>
							<asp:BoundField DataField="RECEIPT_LIMIT_DAY" HeaderText="入金期日">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
							<asp:BoundField DataField="BILL_AMT" HeaderText="請求金額">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:BoundField DataField="TOTAL_RECEIPT_AMT" HeaderText="入金累計額">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:BoundField DataField="URGE_EXEC_COUNT" HeaderText="督促実行済回数">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:BoundField DataField="URGE_EXEC_LAST_DATE" HeaderText="督促最終実行日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
								<ItemStyle HorizontalAlign="Center" Width="180px" />
							</asp:BoundField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
				&nbsp;
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count
						<%#GetRecCount() %>
					</a>
					<br>
					<a class="reccount">Current viewing page
						<%=grdPaymentAfter.PageIndex + 1%>
						<%=grdPaymentAfter.PageCount%>
					</a>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsUserMan" runat="server" SelectMethod="GetPageCollection" TypeName="UserMan" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsUserMan_Selected" OnSelecting="dsUserMan_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pLoginID" Type="String" />
			<asp:Parameter Name="pReceiptLimitDayFrom" Type="String" />
			<asp:Parameter Name="pReceiptLimitDayTo" Type="String" />
			<asp:Parameter Name="pUrgeLevel" Type="String" />
			<asp:Parameter Name="pLastBlackDayFrom" Type="String" />
			<asp:Parameter Name="pLastBlackDayTo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtUrgeLevel" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrReceiptLimitDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdeReceiptLimitDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdeReceiptLimitDayTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskReceiptLimitDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="False"
		TargetControlID="txtReceiptLimitDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskReceiptLimitDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="False"
		TargetControlID="txtReceiptLimitDayTo">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskLastBlackDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="True"
		TargetControlID="txtLastBlackDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskLastBlackDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="True"
		TargetControlID="txtLastBlackDayTo">
	</ajaxToolkit:MaskedEditExtender>
</asp:Content>
