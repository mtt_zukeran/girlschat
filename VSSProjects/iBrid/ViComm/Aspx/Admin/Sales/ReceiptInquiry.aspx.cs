﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Progaram ID		: ReceiptInquiry
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Sales_ReceiptInquiry:System.Web.UI.Page {

	private string recCount = "";
	private int totalAmt = 0;
	private int totalCount = 0;

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			FisrtLoad();
			InitPage();
			if (!Request.QueryString.ToString().Equals("")) {
				GetList();
			}
		}
	}

	private void FisrtLoad() {
		grdReceipt.PageSize = (iBridUtil.GetStringValue(Request.QueryString["pagingoff"]).Equals("1")) ? 99999 : 50;
		grdReceipt.DataSourceID = "";
		DataBind();
		lstSiteCd.DataSourceID = "";
		lstSettleType.Items.Insert(0,new ListItem("",""));
		lstSettleType.DataSourceID = "";
		this.btnCSV.Visible = true;
	}

	private void InitPage() {
		pnlInfo.Visible = false;
		ClearField();
		if (!iBridUtil.GetStringValue(Request.QueryString["sitecd"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		} else if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		if (!iBridUtil.GetStringValue(Request.QueryString["reportdayfrom"]).Equals("")) {
			txtReportDayFrom.Text = iBridUtil.GetStringValue(Request.QueryString["reportdayfrom"]);
		}
		if (!iBridUtil.GetStringValue(Request.QueryString["reportdayto"]).Equals("")) {
			txtReportDayTo.Text = iBridUtil.GetStringValue(Request.QueryString["reportdayto"]);
		}
		if (!iBridUtil.GetStringValue(Request.QueryString["adcd"]).Equals("")) {
			txtAdCd.Text = iBridUtil.GetStringValue(Request.QueryString["adcd"]);
		}
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["settletype"]))) {
			lstSettleType.SelectedValue = iBridUtil.GetStringValue(Request.QueryString["settletype"]);
		}
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["without0yen"]))) {
			chkWithoutCredit0Yen.Checked = iBridUtil.GetStringValue(Request.QueryString["without0yen"]).Equals(ViCommConst.FLAG_ON_STR);
		}
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["without300yen"]))) {
			chkWithoutCredit300Yen.Checked = iBridUtil.GetStringValue(Request.QueryString["withuot300yen"]).Equals(ViCommConst.FLAG_ON_STR);
		}
	}

	private void ClearField() {
		totalAmt = 0;
		totalCount = 0;
		txtLoginId.Text = "";
		txtTel.Text = "";
		txtAdCd.Text = "";
		txtReportDayFrom.Text = DateTime.Now.ToString("yyyy/MM/dd");
		txtReportDayTo.Text = DateTime.Now.ToString("yyyy/MM/dd");
		chkWithoutCredit0Yen.Checked = true;
		chkWithoutCredit300Yen.Checked = true;
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		Response.Redirect(Request.Url.PathAndQuery);
	}

	protected void btnCSV_Click(object sender, EventArgs e) {
		if (grdReceipt.Rows.Count == 0) {
			return;
		}

		DataTable oCsvData;
		using (Receipt oReceipt = new Receipt()) {
			using (DataSet oDataSet = oReceipt.GetPageCollection(
				this.lstSiteCd.SelectedValue, 
				string.Empty, 
				this.txtReportDayFrom.Text, 
				string.IsNullOrEmpty(this.txtReportDayTo.Text) ? this.txtReportDayFrom.Text : this.txtReportDayTo.Text,
				this.lstSettleType.SelectedValue,
				this.txtLoginId.Text.TrimEnd(),
				this.txtTel.Text.TrimEnd(),
				this.txtAdCd.Text.TrimEnd(),
				ViCommConst.FLAG_OFF_STR,
				this.chkWithoutCredit0Yen.Checked,
				this.chkWithoutCredit300Yen.Checked,
				0,
				SysConst.DB_MAX_ROWS)) {

				if (oDataSet.Tables[0].Rows.Count > 0) {
					oCsvData = oDataSet.Tables[0];
				} else {
					return;
				}
			}
		}

		//ヘッダ作成
		string sHeader = "入金日時,ユーザーＩＤ,電話番号,入金額,入金前P,入金P,ｻｰﾋﾞｽP,入金前請求額,入金後P,入金後請求額,入金№,種別,種別名称,会員登録日時,初回入金日,広告コード";

		//CSV出力 
		Response.ContentType = "application/download";
		Response.AppendHeader("Content-Disposition", "attachment;filename=ReceiptList.csv");
		Response.ContentEncoding = System.Text.Encoding.GetEncoding("Shift_JIS");

		Response.Write(sHeader + "\r\n");
		foreach (DataRow oCsvRow in oCsvData.Rows) {
			DateTime oCreateDate;
			StringBuilder oDataStr = new StringBuilder();

			oDataStr.Append(SetCsvString(DateTime.TryParse(oCsvRow["CREATE_DATE"].ToString(),out oCreateDate) ? oCreateDate.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty));
			oDataStr.Append(",").Append(SetCsvString(oCsvRow["LOGIN_ID"].ToString()));
			oDataStr.Append(",");
			oDataStr.Append(",").Append(SetCsvString(oCsvRow["RECEIPT_AMT"].ToString()));
			oDataStr.Append(",").Append(SetCsvString(oCsvRow["BEFORE_RECEIPT_POINT"].ToString()));
			oDataStr.Append(",").Append(SetCsvString(oCsvRow["EX_POINT"].ToString()));
			oDataStr.Append(",").Append(SetCsvString(oCsvRow["SERVICE_POINT"].ToString()));
			oDataStr.Append(",").Append(SetCsvString(oCsvRow["BEFORE_RECEIPT_BILL_AMT"].ToString()));
			oDataStr.Append(",").Append(SetCsvString(oCsvRow["AFTER_RECEIPT_POINT"].ToString()));
			oDataStr.Append(",").Append(SetCsvString(oCsvRow["AFTER_RECEIPT_BILL_AMT"].ToString()));
			oDataStr.Append(",").Append(SetCsvString(oCsvRow["RECEIPT_PERSON"].ToString()));
			oDataStr.Append(",").Append(SetCsvString(oCsvRow["SETTLE_TYPE"].ToString()));
			oDataStr.Append(",").Append(SetCsvString(oCsvRow["SETTLE_TYPE_NM"].ToString()));
			oDataStr.Append(",").Append(SetCsvString(DateTime.TryParse(oCsvRow["REGIST_DATE"].ToString(),out oCreateDate) ? oCreateDate.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty));
			oDataStr.Append(",").Append(SetCsvString(oCsvRow["FIRST_RECEIPT_DAY"].ToString()));
			oDataStr.Append(",").Append(SetCsvString(oCsvRow["AD_CD"].ToString()));

			Response.Write(oDataStr.ToString() + "\r\n");
		}
		Response.End();
	}

	private string SetCsvString(string pData) {
		if (pData.IndexOf('"') > -1 ||
			pData.IndexOf(',') > -1 ||
			pData.IndexOf('\r') > -1 ||
			pData.IndexOf('\n') > -1 ||
			pData.StartsWith(" ") || pData.StartsWith("\t") ||
			pData.EndsWith(" ") || pData.EndsWith("\t")) {
			if (pData.IndexOf('"') > -1) {
				//"を""とする
				pData = pData.Replace("\"", "\"\"");
			}
			if (pData.IndexOf('\r') > -1 || pData.IndexOf('\n') > -1) {
				//改行しない 
				pData = pData.Replace("\r", "");
				pData = pData.Replace("\n", "");
			}
			pData = "\"" + pData + "\"";
		}
		return pData;
	}

	protected string GetTotalAmount() {
		return totalAmt.ToString();
	}

	protected string GetTotalCount() {
		return totalCount.ToString();
	}

	protected string CheckAdminLevel(object pText) {
		if (Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_SITE_OWNER)) {
			return pText.ToString();
		} else {
			return "***********";
		}
	}

	private void GetList() {
		if (txtReportDayTo.Text.Equals("")) {
			txtReportDayTo.Text = txtReportDayFrom.Text;
		}

		using (Receipt oLog = new Receipt()) {
			oLog.GetTotal(
					lstSiteCd.SelectedValue,
					"",
					txtReportDayFrom.Text,
					txtReportDayTo.Text,
					lstSettleType.SelectedValue,
					txtLoginId.Text,
					txtTel.Text,
					txtAdCd.Text,
					chkWithoutCredit0Yen.Checked,
					chkWithoutCredit300Yen.Checked,
					out totalCount,
					out totalAmt);
		}
		grdReceipt.DataSourceID = "dsReceipt";
		DataBind();
		pnlInfo.Visible = true;
	}

	protected void dsReceipt_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = "";
		e.InputParameters[2] = txtReportDayFrom.Text;
		if (txtReportDayTo.Text.Equals("")) {
			txtReportDayTo.Text = txtReportDayFrom.Text;
		}
		e.InputParameters[3] = txtReportDayTo.Text;
		e.InputParameters[4] = lstSettleType.SelectedValue;
		e.InputParameters[5] = txtLoginId.Text;
		e.InputParameters[6] = txtTel.Text;
		e.InputParameters[7] = txtAdCd.Text;

		e.InputParameters[9] = chkWithoutCredit0Yen.Checked;
		e.InputParameters[10] = chkWithoutCredit300Yen.Checked;
	}

	protected void dsReceipt_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}
}
