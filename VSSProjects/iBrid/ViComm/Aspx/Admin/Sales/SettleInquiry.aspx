<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SettleInquiry.aspx.cs" Inherits="Sales_SettleInquiry" Title="決済記録"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="決済記録"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 740px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle">
							決済日
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtReportDayFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RequiredFieldValidator ID="vdrReportDayFrom" runat="server" ErrorMessage="決済日Fromを入力して下さい。" ControlToValidate="txtReportDayFrom" ValidationGroup="Key"
								Display="Dynamic">*</asp:RequiredFieldValidator>
							<asp:RangeValidator ID="vdeReportDayFrom" runat="server" ErrorMessage="決済日Fromを正しく入力して下さい。" ControlToValidate="txtReportDayFrom" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							〜&nbsp;
							<asp:TextBox ID="txtReportDayTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RangeValidator ID="vdeReportDayTo" runat="server" ErrorMessage="決済日Toを正しく入力して下さい。" ControlToValidate="txtReportDayTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
								Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							決済種別
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSettleType" runat="server" DataSourceID="dsSettleType" DataTextField="CODE_NM" DataValueField="CODE" Width="180px">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle">
							決済結果
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSettleStatus" runat="server" DataSourceID="dsSettleStatus" DataTextField="CODE_NM" DataValueField="CODE" Width="180px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							ユーザーＩＤ
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtLoginId" runat="server" Width="70px"></asp:TextBox>
						</td>
						<td class="tdHeaderStyle">
							電話番号
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtTel" runat="server" Width="90px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							広告コード
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtAdCd" runat="server" MaxLength="32" Width="200px"></asp:TextBox>
						</td>
						<td class="tdHeaderStyle">
							応答日
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtAnswerDayFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RangeValidator ID="vdeAnswerDayFrom" runat="server" ErrorMessage="応答日Fromを正しく入力して下さい。" ControlToValidate="txtAnswerDayFrom" MaximumValue="2099/12/31" 
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator> 
							〜&nbsp;
							<asp:TextBox ID="txtAnswerDayTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RangeValidator ID="vdeAnswerDayTo" runat="server" ErrorMessage="応答日Toを正しく入力して下さい。" ControlToValidate="txtAnswerDayTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
								Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
						</td>
					</tr>
					<tr>
                        <td class="tdHeaderStyle">
                            Paging Off
                        </td>
                        <td class="tdDataStyle">
                            <asp:CheckBox ID="chkPagingOff" runat="server" />
                        </td>
                        <td class="tdHeaderStyle">
							FC会費のみ
                        </td>
                        <td class="tdDataStyle">
                            <asp:CheckBox ID="chkFanClubFeeFlag" runat="server" />
                        </td>
                    </tr>
					<tr>
                        <td class="tdHeaderStyle2">
                            ｸﾚｼﾞｯﾄ300円決済を表示しない
                        </td>
                        <td class="tdDataStyle" colspan="3">
                            <asp:CheckBox ID="chkWithoutCredit300Flag" runat="server" />
                        </td>
                    </tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
    			<asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" CausesValidation="False" OnClick="btnCSV_Click" />
            </asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[決済記録]</legend>決済完了件数&nbsp;<%#GetTotalCount()%>
				件&nbsp;&nbsp; 決済完了金額&nbsp;<%#GetTotalAmount()%>
				円
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="430px">
					<asp:GridView ID="grdSettle" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsSettleLog" AllowSorting="True" SkinID="GridViewColor" Font-Size="X-Small">
						<Columns>
							<asp:BoundField DataField="CREATE_DATE" HeaderText="要求日時" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False" SortExpression="CREATE_DATE">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="決済種別" SortExpression="SETTLE_TYPE">
								<ItemTemplate>
									<asp:Label ID="lblSettleType" runat="server" Text='<%# Eval("SETTLE_TYPE_NM")%>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="SETTLE_COMPANY_NM" HeaderText="決済会社" SortExpression="SETTLE_COMPANY_CD">
								<ItemStyle HorizontalAlign="Left" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="ﾕｰｻﾞｰID" SortExpression="LOGIN_ID">
								<ItemTemplate>
									<asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# string.Format("../Man/ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>'
										Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="SID" HeaderText="決済ID" SortExpression="SID">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
							<asp:BoundField DataField="SETTLE_AMT" HeaderText="決済額" SortExpression="SETTLE_AMT">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:BoundField DataField="SETTLE_POINT" HeaderText="pt" SortExpression="SETTLE_POINT">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:BoundField DataField="ANSWER_DATE" HeaderText="応答日時" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False" SortExpression="ANSWER_DATE">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="結果" SortExpression="SETTLE_STATUS">
								<ItemTemplate>
									<asp:Label ID="lblSettleAmt" runat="server" Text='<%# Eval("SETTLE_STATUS_NM")%>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
				&nbsp;
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count
						<%#GetRecCount() %>
					</a>
					<br />
					<a class="reccount">Current viewing page
						<%=grdSettle.PageIndex + 1%>
						of
						<%=grdSettle.PageCount%>
					</a>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsSettleLog" runat="server" SelectMethod="GetPageCollection" TypeName="SettleLog" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsSettleLog_Selected" OnSelecting="dsSettleLog_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
			<asp:Parameter Name="pSettleType" Type="String" />
			<asp:Parameter Name="pLoginId" Type="String" />
			<asp:Parameter Name="pTel" Type="String" />
			<asp:Parameter Name="pSettleStatus" Type="String" />
			<asp:Parameter Name="pAdCd" Type="String" />
			<asp:Parameter Name="pSettleCompanyCd" Type="String" />
            <asp:Parameter Name="pAnswerDayFrom" Type="String" />
            <asp:Parameter Name="pAnswerDayTo" Type="String" />
            <asp:Parameter Name="pFanClubFeeFlag" Type="String" />
            <asp:Parameter Name="pWithoutCredit300Flag" Type="String" />
        </SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSettleType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="83" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSettleStatus" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="84" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrReportDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdeReportDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdeReportDayTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskReportDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtReportDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskReportDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtReportDayTo">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdeAnswerDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdeAnswerDayTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskAnswerDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true" 
		TargetControlID="txtAnswerDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskAnswerDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtAnswerDayTo">
	</ajaxToolkit:MaskedEditExtender>
</asp:Content>
