<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DailyAffiliateSalesInquiry.aspx.cs" Inherits="Sales_DailyAffiliateSalesInquiry"
	Title="日別売上状況" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="日別アフリエート別売上状況"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							<asp:Label ID="lblSalesUnit" runat="server" Text="報告年月"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstYYYY" runat="server" Width="54px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstMM" runat="server" Width="43px">
								<asp:ListItem Value="">--</asp:ListItem>
								<asp:ListItem Value="01">01</asp:ListItem>
								<asp:ListItem Value="02">02</asp:ListItem>
								<asp:ListItem Value="03">03</asp:ListItem>
								<asp:ListItem Value="04">04</asp:ListItem>
								<asp:ListItem Value="05">05</asp:ListItem>
								<asp:ListItem Value="06">06</asp:ListItem>
								<asp:ListItem Value="07">07</asp:ListItem>
								<asp:ListItem Value="08">08</asp:ListItem>
								<asp:ListItem Value="09">09</asp:ListItem>
								<asp:ListItem Value="10">10</asp:ListItem>
								<asp:ListItem Value="11">11</asp:ListItem>
								<asp:ListItem Value="12">12</asp:ListItem>
							</asp:DropDownList>
							<asp:Label ID="lblMonth" runat="server" Text="月"></asp:Label>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
			</asp:Panel>
		</fieldset>
		<fieldset>
			<legend>[日別売上状況]</legend>
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="630px">
				<asp:GridView ID="grdSales" runat="server" AutoGenerateColumns="False" DataSourceID="dsDailySales" ShowFooter="True" OnRowDataBound="grdSales_RowDataBound"
					SkinID="GridViewFreeRowStyle">
					<Columns>
						<asp:TemplateField HeaderText="日付">
							<ItemStyle HorizontalAlign="Center" Font-Size="X-Small" CssClass="RowStyleNoPad" />
							<ItemTemplate>
								<asp:Label ID="Label1" runat="server" Text='<%# Eval("PRINT_DAY") %>' BackColor='<%# GetBackColor(Eval("SALES_DAY_OF_WEEK")) %>' Width="38px"></asp:Label>
							</ItemTemplate>
							<HeaderStyle Height="30px" CssClass="HeaderStyle" Font-Size="X-Small" />
							<FooterStyle Height="20px" HorizontalAlign="Center" Font-Size="X-Small" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="曜日" FooterText="合計">
							<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="X-Small" CssClass="RowStyleNoPad" />
							<ItemTemplate>
								<asp:Label ID="Label2" runat="server" Text='<%# Eval("SALES_DAY_OF_WEEK") %>' BackColor='<%# GetBackColor(Eval("SALES_DAY_OF_WEEK")) %>' Width="38px"></asp:Label>
							</ItemTemplate>
							<FooterStyle HorizontalAlign="Center" Font-Size="X-Small" />
							<HeaderStyle CssClass="HeaderStyle" Font-Size="X-Small" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="件" ItemStyle-CssClass="RowStylePad">
							<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Font-Size="X-Small" />
							<ItemTemplate>
								<asp:Label ID="lblConviniCount" runat="server" Text='<%# Eval("TOTAL_SALES_COUNT") %>' Width="30px"></asp:Label>
							</ItemTemplate>
							<FooterStyle HorizontalAlign="Right" Font-Size="X-Small" />
							<HeaderStyle CssClass="HeaderStyle" Font-Size="X-Small" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="金額" ItemStyle-CssClass="RowStylePad">
							<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Font-Size="X-Small" />
							<ItemTemplate>
								<asp:Label ID="lblConviniAmt" runat="server" Text='<%# Eval("TOTAL_SALES_AMT") %>' Width="60px"></asp:Label>
							</ItemTemplate>
							<FooterStyle HorizontalAlign="Right" Font-Size="X-Small" />
							<HeaderStyle CssClass="HeaderStyle" Font-Size="X-Small" />
						</asp:TemplateField>
					</Columns>
					<FooterStyle ForeColor="Black" BackColor="LightYellow" />
				</asp:GridView>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsDailyAffiliateSales" runat="server" SelectMethod="DailyAffiliateSalesInquiry" TypeName="DailyAffiliateSales" OnSelecting="dsDailyAffiliateSales_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pYYYY" Type="String" />
			<asp:Parameter Name="pMM" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
</asp:Content>
