﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MonthlyFanClubFee.aspx.cs" Inherits="Sales_MonthlyFanClubFee" Title="月別FC会費売上" ValidateRequest="false" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="月別FC会費売上"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" Runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
                        <td class="tdHeaderStyle2">
							集計期間
                        </td>
                        <td class="tdDataStyle">
							<asp:DropDownList ID="lstYearFrom" runat="server" Width="54px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstMonthFrom" runat="server" Width="43px">
								<asp:ListItem Value="">--</asp:ListItem>
								<asp:ListItem Value="01">01</asp:ListItem>
								<asp:ListItem Value="02">02</asp:ListItem>
								<asp:ListItem Value="03">03</asp:ListItem>
								<asp:ListItem Value="04">04</asp:ListItem>
								<asp:ListItem Value="05">05</asp:ListItem>
								<asp:ListItem Value="06">06</asp:ListItem>
								<asp:ListItem Value="07">07</asp:ListItem>
								<asp:ListItem Value="08">08</asp:ListItem>
								<asp:ListItem Value="09">09</asp:ListItem>
								<asp:ListItem Value="10">10</asp:ListItem>
								<asp:ListItem Value="11">11</asp:ListItem>
								<asp:ListItem Value="12">12</asp:ListItem>
							</asp:DropDownList>月&nbsp;～&nbsp;
							<asp:DropDownList ID="lstYearTo" runat="server" Width="54px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstMonthTo" runat="server" Width="43px">
								<asp:ListItem Value="">--</asp:ListItem>
								<asp:ListItem Value="01">01</asp:ListItem>
								<asp:ListItem Value="02">02</asp:ListItem>
								<asp:ListItem Value="03">03</asp:ListItem>
								<asp:ListItem Value="04">04</asp:ListItem>
								<asp:ListItem Value="05">05</asp:ListItem>
								<asp:ListItem Value="06">06</asp:ListItem>
								<asp:ListItem Value="07">07</asp:ListItem>
								<asp:ListItem Value="08">08</asp:ListItem>
								<asp:ListItem Value="09">09</asp:ListItem>
								<asp:ListItem Value="10">10</asp:ListItem>
								<asp:ListItem Value="11">11</asp:ListItem>
								<asp:ListItem Value="12">12</asp:ListItem>
							</asp:DropDownList>月
                        </td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click"  ValidationGroup="Seek" />
				<asp:CustomValidator ID="vdcFromTo" runat="server" ErrorMessage="集計期間の大小関係が不正です。" OnServerValidate="vdcFromTo_ServerValidate" ValidationGroup="Seek"></asp:CustomValidator>
            </asp:Panel>
		</fieldset>
		<fieldset>
			<legend>[月別FC会費売上]</legend>
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto">
				<asp:GridView ID="grdMonthlyFanClubFee" runat="server" AutoGenerateColumns="False" DataSourceID="dsMonthlyFanClubFee" SkinID="GridViewColor">
					<Columns>
						<asp:BoundField DataField="CREATE_MONTH" HeaderText="年月" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" />
						</asp:BoundField>
						<asp:BoundField DataField="SETTLE_COUNT" HeaderText="件数" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Right" />
						</asp:BoundField>
						<asp:TemplateField HeaderText="金額">
							<ItemTemplate>
								<asp:HyperLink ID="lnkSettleAmt" runat="server" NavigateUrl='<%# GetSettleAmtUrl(Eval("CREATE_MONTH")) %>' Text='<%# Eval("SETTLE_AMT") %>' Width="50px"	CssClass="Warp"></asp:HyperLink>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="right" />
						</asp:TemplateField>
					</Columns>
				</asp:GridView>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsMonthlyFanClubFee" runat="server" SelectMethod="GetList" TypeName="MonthlyFanClubFee" OnSelecting="dsMonthlyFanClubFee_Selecting">
        <SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
        </SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>

