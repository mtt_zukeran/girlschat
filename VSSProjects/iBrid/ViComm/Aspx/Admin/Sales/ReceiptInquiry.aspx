<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReceiptInquiry.aspx.cs" Inherits="Sales_ReceiptInquiry" Title="入金記録"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="入金記録"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 740px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle">
							入金日
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtReportDayFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RequiredFieldValidator ID="vdrReportDayFrom" runat="server" ErrorMessage="入金日Fromを入力して下さい。" ControlToValidate="txtReportDayFrom" ValidationGroup="Key"
								Display="Dynamic">*</asp:RequiredFieldValidator>
							<asp:RangeValidator ID="vdeReportDayFrom" runat="server" ErrorMessage="入金日Fromを正しく入力して下さい。" ControlToValidate="txtReportDayFrom" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							〜&nbsp;
							<asp:TextBox ID="txtReportDayTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RangeValidator ID="vdeReportDayTo" runat="server" ErrorMessage="入金日Toを正しく入力して下さい。" ControlToValidate="txtReportDayTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
								Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							ユーザーＩＤ
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtLoginId" runat="server" Width="70px"></asp:TextBox>
						</td>
						<td class="tdHeaderStyle">
							電話番号
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtTel" runat="server" Width="90px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							広告コード
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtAdCd" runat="server" MaxLength="32" Width="200px"></asp:TextBox>
						</td>
						<td class="tdHeaderStyle">
							0円決済を非表示
						</td>
						<td class="tdDataStyle">
							<asp:CheckBox ID="chkWithoutCredit0Yen" runat="server" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							決済種別
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSettleType" runat="server" DataSourceID="dsSettleType" DataTextField="CODE_NM" DataValueField="CODE">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle2">
							300円決済を非表示
						</td>
						<td class="tdDataStyle">
							<asp:CheckBox ID="chkWithoutCredit300Yen" runat="server" />
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
				<asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" CausesValidation="False" OnClick="btnCSV_Click" />
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[入金記録]</legend>入金件数&nbsp;<%#GetTotalCount()%>
				件&nbsp;&nbsp; 入金額&nbsp;<%#GetTotalAmount()%>
				円
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="430px">
					<asp:GridView ID="grdReceipt" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsSettleLog" AllowSorting="True" SkinID="GridViewColor">
						<Columns>
							<asp:BoundField DataField="CREATE_DATE" HeaderText="入金日時" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False" SortExpression="CREATE_DATE">
								<ItemStyle HorizontalAlign="Center" />
								<ItemStyle Font-Size="Small" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="ユーザーＩＤ" SortExpression="LOGIN_ID">
								<ItemTemplate>
									<asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# string.Format("../Man/ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>'
										Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="RECEIPT_AMT" HeaderText="入金額" SortExpression="RECEIPT_AMT">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:BoundField DataField="BEFORE_RECEIPT_POINT" HeaderText="入金前P" SortExpression="BEFORE_RECEIPT_POINT">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:BoundField DataField="EX_POINT" HeaderText="入金P" SortExpression="EX_POINT">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:BoundField DataField="SERVICE_POINT" HeaderText="ｻｰﾋﾞｽP" SortExpression="SERVICE_POINT">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:BoundField DataField="BEFORE_RECEIPT_BILL_AMT" HeaderText="入金前請求額"  SortExpression="BEFORE_RECEIPT_BILL_AMT">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:BoundField DataField="AFTER_RECEIPT_POINT" HeaderText="入金後P"  SortExpression="AFTER_RECEIPT_POINT">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:BoundField DataField="AFTER_RECEIPT_BILL_AMT" HeaderText="入金後請求額" SortExpression="AFTER_RECEIPT_BILL_AMT">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:BoundField DataField="RECEIPT_PERSON" HeaderText="入金��" SortExpression="RECEIPT_PERSON">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
							<asp:BoundField DataField="SETTLE_TYPE_NM" HeaderText="種別" SortExpression="SETTLE_TYPE">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
							<asp:BoundField DataField="REGIST_DATE" HeaderText="会員登録日時" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False" SortExpression="REGIST_DATE">
								<ItemStyle HorizontalAlign="Center" />
								<ItemStyle Font-Size="Small" />
							</asp:BoundField>
							<asp:BoundField DataField="FIRST_RECEIPT_DAY" HeaderText="初回入金日" SortExpression="FIRST_RECEIPT_DAY">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
				&nbsp;
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count of
						<%#GetRecCount() %>
					</a>
					<br />
					<a class="reccount">Current viewing page
						<%=grdReceipt.PageIndex + 1%>
						of
						<%=grdReceipt.PageCount%>
					</a>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsReceipt" runat="server" SelectMethod="GetPageCollection" TypeName="Receipt" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelecting="dsReceipt_Selecting" OnSelected="dsReceipt_Selected">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
			<asp:Parameter Name="pSettleType" Type="String" />
			<asp:Parameter Name="pLoginId" Type="String" />
			<asp:Parameter Name="pTel" Type="String" />
			<asp:Parameter Name="pAdCd" />
            <asp:Parameter Name="pReceiptCancelFlag" Type="String" DefaultValue="0" />
            <asp:Parameter Name="pWithoutCredit0Yen" Type="Boolean" />
            <asp:Parameter Name="pWithoutCredit300Yen" Type="Boolean" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSettleType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="74" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrReportDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdeReportDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdeReportDayTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskReportDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="True"
		TargetControlID="txtReportDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskReportDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="True"
		TargetControlID="txtReportDayTo">
	</ajaxToolkit:MaskedEditExtender>
</asp:Content>
