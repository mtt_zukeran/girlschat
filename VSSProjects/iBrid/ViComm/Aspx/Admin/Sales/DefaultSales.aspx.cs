﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;

public partial class Sales_DefaultSales:System.Web.UI.Page {

	protected void Page_Load(object sender, EventArgs e) {
		DataBind();
		if (Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_STAFF)) {
			Response.Redirect("DailySalesInquiry.aspx");
		} else {
			Response.Redirect("SettleInquiry.aspx");
		}
	}
}
