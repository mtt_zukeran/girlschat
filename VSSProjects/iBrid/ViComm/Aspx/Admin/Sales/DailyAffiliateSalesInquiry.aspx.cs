﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 日別アフリエート別売上状況
--	Progaram ID		: DailyAffiliateSalesInquiry
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Sales_DailyAffiliateSalesInquiry:System.Web.UI.Page {

	public class colInfo {
		public int index;
		public string title;
		public string settleCompnayCd;
		public string countField;
		public string amtField;

		public colInfo(int pIndex,string pSettleCompnayCd,string pTitle,string pCountFiled,string pAmtField) {
			index = pIndex;
			title = pTitle;
			settleCompnayCd = pSettleCompnayCd;
			countField = pCountFiled;
			amtField = pAmtField;
		}
	}


	public class SalesCountTemplate:System.Web.UI.ITemplate {
		private ListItemType templateType;
		private string bindField;

		public SalesCountTemplate(ListItemType pItemtype,string pBindField) {
			templateType = pItemtype;
			bindField = pBindField;
		}

		public void InstantiateIn(System.Web.UI.Control container) {
			PlaceHolder ph = new PlaceHolder();
			Label item1 = new Label();
			item1.ID = bindField;
			switch (templateType) {
				case ListItemType.Item:
					ph.Controls.Add(item1);
					ph.DataBinding += new EventHandler(Item_DataBinding);
					container.Controls.Add(ph);
					break;
			}
		}

		public void Item_DataBinding(object sender,EventArgs e) {
			PlaceHolder ph = (PlaceHolder)sender;
			GridViewRow container = (GridViewRow)ph.NamingContainer;
			((Label)ph.FindControl(bindField)).Text = ((DataRowView)container.DataItem)[this.bindField].ToString();
		}
	}

	public class SalesAmtTemplate:System.Web.UI.ITemplate {
		private ListItemType templateType;
		private string bindField;
		private string settleCompanyCd;

		public SalesAmtTemplate(ListItemType pItemtype,string pBindField,string pSettleCompanyCd) {
			templateType = pItemtype;
			bindField = pBindField;
			settleCompanyCd = pSettleCompanyCd;
		}

		public void InstantiateIn(System.Web.UI.Control container) {
			PlaceHolder ph = new PlaceHolder();
			HyperLink item1 = new HyperLink();
			item1.ID = bindField;
			switch (templateType) {
				case ListItemType.Item:
					ph.Controls.Add(item1);
					ph.DataBinding += new EventHandler(Item_DataBinding);
					container.Controls.Add(ph);
					break;
			}
		}

		public void Item_DataBinding(object sender,EventArgs e) {
			PlaceHolder ph = (PlaceHolder)sender;
			GridViewRow container = (GridViewRow)ph.NamingContainer;
			HyperLink link = (HyperLink)ph.FindControl(bindField);

			link.Text = ((DataRowView)container.DataItem)[this.bindField].ToString();

			string sSiteCd = ((DataRowView)container.DataItem)["SITE_CD"].ToString();
			string sSalesDay = ((DataRowView)container.DataItem)["SALES_DAY"].ToString();
			link.NavigateUrl =
				string.Format("~/Sales/SettleInquiry.aspx?sitecd={0}&reportdayfrom={1}&reportdayto={1}&settletype={2}&settlestatus={3}&return={4}&settlecompany={5}",
					sSiteCd,sSalesDay,ViCommConst.SETTLE_POINT_AFFILIATE,ViCommConst.SETTLE_STAT_OK,"DailyAffiliateSalesInquiry.aspx",this.settleCompanyCd);
		}
	}

	private List<colInfo> colList;
	private int[] salesCount = new int[ViCommConst.MAX_AF_SETTLE_COMPANY];
	private int[] salesAmt = new int[ViCommConst.MAX_AF_SETTLE_COMPANY];
	private int totalSalesCount;
	private int totalSalesAmt;

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		grdSales.DataSourceID = "";
		ClearField();
		DataBind();

		if (!IsPostBack) {

            SysPrograms.SetupYear(lstYYYY);

			lstMM.SelectedValue = DateTime.Now.ToString("MM");
			lstSiteCd.DataSourceID = "";

			if (!iBridUtil.GetStringValue(Request.QueryString["site"]).Equals(string.Empty)) {
				lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Request.QueryString["site"]);
			}
			if (!iBridUtil.GetStringValue(Request.QueryString["yyyy"]).Equals(string.Empty)) {
				lstYYYY.SelectedValue = iBridUtil.GetStringValue(Request.QueryString["yyyy"]);
			}
			if (!iBridUtil.GetStringValue(Request.QueryString["mm"]).Equals(string.Empty)) {
				lstMM.SelectedValue = iBridUtil.GetStringValue(Request.QueryString["mm"]);
			}
			GetList();
		}
	}

	private void ClearField() {
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		// PostBackで検索を複数回実行するとCol0～3のDateFiledが空になってしまうため
		Response.Redirect(Request.Url.AbsolutePath + string.Format("?site={0}&yyyy={1}&mm={2}",lstSiteCd.SelectedValue,lstYYYY.SelectedValue,lstMM.SelectedValue));
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		Response.Redirect(Request.Url.PathAndQuery);
	}

	protected void grdSales_RowDataBound(object sender,GridViewRowEventArgs e) {

		if (e.Row.RowType == DataControlRowType.DataRow) {
			totalSalesCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TOTAL_SALES_COUNT"));
			totalSalesAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TOTAL_SALES_AMT"));

			for (int i = 0;i < ViCommConst.MAX_AF_SETTLE_COMPANY;i++) {
				salesCount[i] += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,string.Format("SALES_COUNT{0}",i + 1)));
				salesAmt[i] += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,string.Format("SALES_AMT{0}",i + 1)));
			}

		} else if (e.Row.RowType == DataControlRowType.Footer) {

			e.Row.Cells[2].Text = totalSalesCount.ToString();
			e.Row.Cells[3].Text = totalSalesAmt.ToString();

			int iPos = 4;
			int iCount = 0;
			int iAmt = 0;

			foreach (colInfo oInfo in colList) {
				iCount = salesCount[oInfo.index];
				iAmt = salesAmt[oInfo.index];
				e.Row.Cells[iPos].Text = iCount.ToString();
				e.Row.Cells[iPos + 1].Text = iAmt.ToString();
				iPos += 2;
			}
		}
	}

	private void GetList() {
		CreateGrid();
		for (int i = 0;i < ViCommConst.MAX_AF_SETTLE_COMPANY;i++) {
			salesCount[i] = 0;
			salesAmt[i] = 0;
		}
		totalSalesCount = 0;
		totalSalesAmt = 0;

		grdSales.PageIndex = 0;
		grdSales.DataSourceID = "dsDailyAffiliateSales";
		DataBind();
		AddHeader();
	}


	protected Color GetBackColor(object pDayOfWeek) {
		if (pDayOfWeek.ToString().Equals("Sat")) {
			return Color.FromArgb(0xCCCCFF);
		} else {
			if (pDayOfWeek.ToString().Equals("Sun")) {
				return Color.FromArgb(0xFFCCCC);
			} else {
				return Color.Empty;
			}
		}
	}

	private void AddHeader() {

		GridViewRow row = new GridViewRow(-1,-1,DataControlRowType.Header,DataControlRowState.Normal);
		TableCell cell;
		cell = new TableCell();
		cell.ColumnSpan = 2;
		cell.RowSpan = 1;
		cell.Text = "";
		row.Cells.Add(cell);

		cell = new TableCell();
		cell.ColumnSpan = 2;
		cell.Text = "日計";
		cell.HorizontalAlign = HorizontalAlign.Center;
		row.Cells.Add(cell);

		foreach (colInfo oInfo in colList) {
			cell = new TableCell();
			cell.ColumnSpan = 2;
			cell.Text = oInfo.title;
			cell.HorizontalAlign = HorizontalAlign.Center;
			cell.Wrap = true;
			cell.Width = 120;
			row.Cells.Add(cell);
		}
		grdSales.Controls[0].Controls.AddAt(0,row);
	}

	protected void dsDailyAffiliateSales_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = lstYYYY.SelectedValue;
		e.InputParameters[2] = lstMM.SelectedValue;
	}


	private void CreateGrid() {
		colList = new List<colInfo>();

		while (true) {
			if (grdSales.Columns.Count > 4) {
				grdSales.Columns.RemoveAt(4);
			} else {
				break;
			}
		}

		using (SiteSettle oSiteSettle = new SiteSettle()) {
			DataSet ds = oSiteSettle.GetList(lstSiteCd.SelectedValue,ViCommConst.SETTLE_POINT_AFFILIATE);
			int i = 0;
			foreach (DataRow dr in ds.Tables[0].Rows) {
				colList.Add(new colInfo(i,dr["SETTLE_COMPANY_CD"].ToString(),dr["SETTLE_COMPANY_NM"].ToString(),string.Format("SALES_COUNT{0}",i + 1),string.Format("SALES_AMT{0}",i + 1)));
				i++;
			}
		}

		foreach (colInfo oInfo in colList) {
			TemplateField tmpCount = new TemplateField();
			TemplateField tmpAmt = new TemplateField();
			tmpCount.HeaderText = "件数";
			tmpCount.HeaderStyle.CssClass = "HeaderStyle";
			tmpCount.HeaderStyle.Font.Size = FontUnit.XSmall;
			tmpCount.FooterStyle.Font.Size = FontUnit.XSmall;
			tmpCount.FooterStyle.HorizontalAlign = HorizontalAlign.Right;
			tmpCount.ItemStyle.Width = 30;
			tmpCount.ItemStyle.CssClass = "RowStylePad";
			tmpCount.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
			tmpCount.ItemStyle.Font.Size = FontUnit.XSmall;
			tmpCount.ItemTemplate = new SalesCountTemplate(ListItemType.Item,oInfo.countField);
			tmpCount.FooterStyle.HorizontalAlign = HorizontalAlign.Right;

			tmpAmt.HeaderText = "金額";
			tmpAmt.HeaderStyle.CssClass = "HeaderStyle";
			tmpAmt.HeaderStyle.Font.Size = FontUnit.XSmall;
			tmpAmt.FooterStyle.Font.Size = FontUnit.XSmall;
			tmpAmt.FooterStyle.HorizontalAlign = HorizontalAlign.Right;
			tmpAmt.ItemStyle.Width = 60;
			tmpAmt.ItemStyle.CssClass = "RowStylePad";
			tmpAmt.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
			tmpAmt.ItemStyle.VerticalAlign = VerticalAlign.Middle;
			tmpAmt.ItemStyle.Font.Size = FontUnit.XSmall;
			tmpAmt.ItemTemplate = new SalesAmtTemplate(ListItemType.Item,oInfo.amtField,oInfo.settleCompnayCd);

			grdSales.Columns.Add(tmpCount);
			grdSales.Columns.Add(tmpAmt);
		}
		grdSales.Width = 120 + colList.Count * 120;
	}

	protected void grdSales_Load(object sender,EventArgs e) {

	}
}