﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 後払請求一覧
--	Progaram ID		: PaymentAfter
--
--  Creation Date	: 2009.12.21
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Sales_PaymentAfter:System.Web.UI.Page {

	private string recCount = "";
	private int totalAmt = 0;
	private int totalCount = 0;

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			FisrtLoad();
			InitPage();
		}
	}

	private void FisrtLoad() {
		grdPaymentAfter.PageSize = int.Parse(Session["PageSize"].ToString());
		grdPaymentAfter.DataSourceID = "";
		DataBind();
		lstSiteCd.DataSourceID = "";
	}

	private void InitPage() {
		pnlInfo.Visible = false;
		ClearField();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void ClearField() {
		totalAmt = 0;
		totalCount = 0;
		txtLoginId.Text = "";
		txtUrgeLevel.Text = "";
		DefaultDays();
	}

	private void DefaultDays() {
		int iDays = 0;
		int iUrgeDays = 0;
		int iReceiptSight = 0;
		
		using (Site oSite = new Site()){
			oSite.GetOne(lstSiteCd.SelectedValue);
			iUrgeDays = oSite.urgeDays;
			iReceiptSight = oSite.receiptSight;
			iDays -= (iUrgeDays + iReceiptSight);
		}
		txtReceiptLimitDayFrom.Text = DateTime.Now.AddDays(iDays).ToString("yyyy/MM/dd");
		txtReceiptLimitDayTo.Text = DateTime.Now.ToString("yyyy/MM/dd");

		txtLastBlackDayFrom.Text = "";
		txtLastBlackDayTo.Text = "";
	}
	
	protected string GetRecCount() {
		return recCount;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		Response.Redirect(Request.Url.PathAndQuery);
	}

	protected string GetTotalAmount() {
		return totalAmt.ToString();
	}

	protected string GetTotalCount() {
		return totalCount.ToString();
	}

	private void GetList() {
		if (txtReceiptLimitDayTo.Text.Equals("")) {
			txtReceiptLimitDayTo.Text = txtReceiptLimitDayFrom.Text;
		}
		if ((!txtLastBlackDayFrom.Text.Equals("")) || (!txtLastBlackDayTo.Text.Equals(""))) {
			if (txtLastBlackDayFrom.Text.Equals("")) {
				txtLastBlackDayFrom.Text = txtLastBlackDayTo.Text;
			} else if (txtLastBlackDayTo.Text.Equals("")) {
				txtLastBlackDayTo.Text = txtLastBlackDayFrom.Text;
			}
		}
		
		using (UserMan oUserMan = new UserMan()) {

			oUserMan.GetTotalPaymentAfter(
					lstSiteCd.SelectedValue,
					txtLoginId.Text,
					txtReceiptLimitDayFrom.Text,
					txtReceiptLimitDayTo.Text,
					txtUrgeLevel.Text,
					txtLastBlackDayFrom.Text,
					txtLastBlackDayTo.Text,
					out totalCount,
					out totalAmt);
		}
		if (chkPagingOff.Checked) {
			grdPaymentAfter.PageSize = 99999;
			grdPaymentAfter.AllowSorting = false;
			grdPaymentAfter.PageIndex = 0;
		} else {
			grdPaymentAfter.PageSize = 50;
			grdPaymentAfter.AllowSorting = true;
		}
		grdPaymentAfter.DataSourceID = "dsUserMan";
		DataBind();
		pnlInfo.Visible = true;
	}

	protected void dsUserMan_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = txtLoginId.Text;
		e.InputParameters[2] = txtReceiptLimitDayFrom.Text;
		if (txtReceiptLimitDayTo.Text.Equals("")) {
			txtReceiptLimitDayTo.Text = txtReceiptLimitDayFrom.Text;
		}
		e.InputParameters[3] = txtReceiptLimitDayTo.Text;
		e.InputParameters[4] = txtUrgeLevel.Text;

		if ((!txtLastBlackDayFrom.Text.Equals("")) || (!txtLastBlackDayTo.Text.Equals(""))) {
			if (txtLastBlackDayFrom.Text.Equals("")) {
				txtLastBlackDayFrom.Text = txtLastBlackDayTo.Text;
			} else if (txtLastBlackDayTo.Text.Equals("")) {
				txtLastBlackDayTo.Text = txtLastBlackDayFrom.Text;
			}
		}
		e.InputParameters[5] = txtLastBlackDayFrom.Text;
		e.InputParameters[6] = txtLastBlackDayTo.Text;
	}

	protected void dsUserMan_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}
	protected void lstSiteCd_SelectedIndexChanged(object sender,EventArgs e) {
		DefaultDays();
	}
}
