﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メニュー画面
--	Progaram ID		: TreeMenu
--
--  Creation Date	: 2010.04.06
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class TreeMenu:System.Web.UI.Page {
	private SitemapList siteMaplist;

	protected void Page_PreInit(object sender,EventArgs e) {
		string sTheme = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["theme"]);
		if (!sTheme.Equals(string.Empty)) {
			Page.Theme = sTheme;
		}
	}
	
	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {

			int iCompanyMask,iAvailableMaqiaAffiliateFlag;
			
			int.TryParse(Session["CompanyMask"].ToString(),out iCompanyMask);
			int.TryParse(Session["AvailableMaqiaAffiliate"].ToString(),out iAvailableMaqiaAffiliateFlag);

			SiteMapDataSource1.SiteMapProvider = "ViCommSiteOwnerProvider";

			if (Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_SYS_OWNER)) {
				SiteMapDataSource1.SiteMapProvider = "ViCommSysOwnerProvider";
			}
			siteMaplist = new SitemapList(Session["AdminType"].ToString());

			DataBind();

			foreach (TreeNode oTn in this.trvMenu.Nodes) {
				this.ReplaceDisplayWord(oTn);
			}

			Session["MENU_SITE"] = lstSiteCd.SelectedValue;

		}
	}

	private void ReplaceDisplayWord(TreeNode tn) {
		tn.Text = DisplayWordUtil.Replace(tn.Text);

		foreach (TreeNode oChildNode in tn.ChildNodes) {
			ReplaceDisplayWord(oChildNode);
		}
	}

	protected void lstSiteCd_SelectedIndexChanged(object sender,EventArgs e) {
		Session["MENU_SITE"] = lstSiteCd.SelectedValue;
	}

	protected void trvMenu_TreeNodeDataBound(object sender,TreeNodeEventArgs e) {
		string sTile = ((System.Web.SiteMapNode)(e.Node.DataItem)).Title;
		if (siteMaplist.IsAvailableMenu(sTile) == false) {
			if (e.Node.Parent != null) {
				e.Node.Parent.ChildNodes.Remove(e.Node);
			} else {
				this.trvMenu.Nodes.Remove(e.Node);
			}
		}

		if (sTile.Equals("ネット投票設定(ｴﾝﾄﾘｰ制)")) {
			if (!Session["AdminId"].Equals("itomichiko") && e.Node.Parent != null) {
				e.Node.Parent.ChildNodes.Remove(e.Node);
			}
		}
	}
}
