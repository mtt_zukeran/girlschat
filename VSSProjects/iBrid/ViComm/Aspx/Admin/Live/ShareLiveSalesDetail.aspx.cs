﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: MODEL ASP売上明細
--	Progaram ID		: ShareLiveSalesDetail
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Live_ShareLiveSalesDetail:System.Web.UI.Page {

	private string recCount = "";
	private int totalMin = 0;
	private int totalPoint = 0;
	private int totalPointFreeDial = 0;
	private int totalInvitePoint = 0;
	private int totalPaymentPoint = 0;

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			FisrtLoad();
			InitPage();
		}
	}

	private void FisrtLoad() {
		grdSalesDetail.PageSize = int.Parse(Session["PageSize"].ToString());
		grdSalesDetail.DataSourceID = "";
		DataBind();
	}

	private void InitPage() {
		ClearField();
		if (!IsPostBack) {
			string sDayFrom = iBridUtil.GetStringValue(Request.QueryString["dayfrom"]);
			string sDayTo = iBridUtil.GetStringValue(Request.QueryString["dayTo"]);
			string sShKey = iBridUtil.GetStringValue(Request.QueryString["shkey"]);
			if ((!sDayFrom.Equals("")) && (!sDayTo.Equals(""))) {
				txtShareLiveTalkKey.Text = sShKey;
				txtReportDayFrom.Text = sDayFrom;
				txtReportDayTo.Text = sDayTo;
				GetList();
			}
		}
	}

	private void ClearField() {
		totalMin = 0;
		totalPoint = 0;
		totalPointFreeDial = 0;
		totalInvitePoint = 0;
		totalPaymentPoint = 0;
		txtShareLiveTalkKey.Text = "";
		txtLoginId.Text = "";
		txtIvpSiteNm.Text = "";
		txtReportDayFrom.Text = DateTime.Now.ToString("yyyy/MM/dd");
		txtReportDayTo.Text = DateTime.Now.ToString("yyyy/MM/dd");
		GetList();
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void dsShareLiveSalesDtl_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	private void GetList() {
		if (txtReportDayTo.Text.Equals("")) {
			txtReportDayTo.Text = txtReportDayFrom.Text;
		}
		grdSalesDetail.DataSourceID = "dsShareLiveSalesDtl";
		DataBind();
	}

	protected string GetTotalMin() {
		return totalMin.ToString();
	}

	protected string GetTotalPoint() {
		return totalPoint.ToString();
	}

	protected string GetTotalPointFreeDial() {
		return totalPointFreeDial.ToString();
	}

	protected string GetTotalInvitePoint() {
		return totalInvitePoint.ToString();
	}

	protected string GetTotalPaymentPoint() {
		return totalPaymentPoint.ToString();
	}

	protected void dsShareLiveSalesDtl_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = txtShareLiveTalkKey.Text;
		e.InputParameters[1] = txtLoginId.Text;
		e.InputParameters[2] = txtIvpSiteNm.Text;
		e.InputParameters[3] = txtReportDayFrom.Text;
		if (txtReportDayTo.Text.Equals("")) {
			txtReportDayTo.Text = txtReportDayFrom.Text;
		}
		DateTime dtTo = System.DateTime.ParseExact(txtReportDayTo.Text,"yyyy/MM/dd",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);
		e.InputParameters[4] = dtTo.AddDays(1).ToString("yyyy/MM/dd");
	}

	protected void grdSalesDetail_DataBound(object sender,EventArgs e) {
		JoinCells(grdSalesDetail,0);
	}

	protected void JoinCells(GridView pGrd,int pCol) {
		int iRow = pGrd.Rows.Count;
		int iBaseidx = 0;

		while (iBaseidx < iRow) {
			int iNextIdx = iBaseidx + 1;
			TableCell celBase = pGrd.Rows[iBaseidx].Cells[pCol];

			while (iNextIdx < iRow) {

				TableCell celNext = pGrd.Rows[iNextIdx].Cells[pCol];

				if (GetText(celBase).Equals(GetText(celNext))) {
					if (celBase.RowSpan == 0) {
						celBase.RowSpan = 2;
					} else {
						celBase.RowSpan++;
					}
					pGrd.Rows[iNextIdx].Cells.Remove(celNext);
					iNextIdx++;
				} else {
					break;
				}
			}
			iBaseidx = iNextIdx;
		}
	}


	private string GetText(TableCell tc) {
		Label dblc =
		  (Label)tc.Controls[1];
		return dblc.Text;
	}


	protected void btnCSV_Click(object sender,EventArgs e) {
		string fileName = ViCommConst.CSV_FILE_NM_SHARE_LIVE_SALES;
		Response.AddHeader("Content-Disposition","attachment;filename=" + fileName);
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		using (ShareLiveSalesDtl oShareLiveSales = new ShareLiveSalesDtl()) {
			DateTime dtTo = System.DateTime.ParseExact(txtReportDayTo.Text,"yyyy/MM/dd",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);
			DataSet ds = oShareLiveSales.GetPageCollection(
								txtShareLiveTalkKey.Text,
								txtLoginId.Text,
								txtIvpSiteNm.Text,
								txtReportDayFrom.Text,
								dtTo.AddDays(1).ToString("yyyy/MM/dd"),
								0,
								SysConst.DB_MAX_ROWS);
			SetCsvData(ds);
			Response.End();
		}
	}

	public void SetCsvData(DataSet pDataSet) {

		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");
		string sDtl = "";

		sDtl += "ライブキー,";
		sDtl += "ライブ名称,";
		sDtl += DisplayWordUtil.Replace("出演者ID,");
		sDtl += DisplayWordUtil.Replace("出演者名,");
		sDtl += "ライブ開始時間,";
		sDtl += "ライブ終了時間,";
		sDtl += "利用ｻｲﾄ名,";
		sDtl += "照会№,";
		sDtl += "課金開始日時,";
		sDtl += "課金終了日時,";
		sDtl += "課金秒数\r\n";

		Response.BinaryWrite(encoding.GetBytes(sDtl));

		DataRow dr = null;
		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];
			sDtl = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}\r\n",
							dr["SHARE_LIVE_TALK_KEY"].ToString(),
							dr["LIVE_NM"].ToString(),
							dr["LOGIN_ID"].ToString(),
							dr["CAST_NM"].ToString(),
							dr["START_DATE"].ToString(),
							dr["END_DATE"].ToString(),
							dr["IVP_SITE_NM"].ToString(),
							dr["IVP_ACCEPT_SEQ"].ToString(),
							dr["CHARGE_START_DATE"].ToString(),
							dr["CHARGE_END_DATE"].ToString(),
							dr["CHARGE_SEC"].ToString());
			Response.BinaryWrite(encoding.GetBytes(sDtl));
		}
	}
}
