﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ライブ配信メンテナンス
--	Progaram ID		: LiveBroadCastList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Live_LiveBroadCastList:System.Web.UI.Page {
	private string recCount = "";
	private int MAX_CAMERA = 3;

	protected void Page_Load(object sender,EventArgs e) {
		txtOpenHtmlDoc1.EditorDirectory = string.Format("http://{0}:{1}/pinEdit/editor/",Request.UrlReferrer.Host,Request.UrlReferrer.Port);
		txtOpenHtmlDoc2.EditorDirectory = string.Format("http://{0}:{1}/pinEdit/editor/",Request.UrlReferrer.Host,Request.UrlReferrer.Port);
		txtCloseHtmlDoc1.EditorDirectory = string.Format("http://{0}:{1}/pinEdit/editor/",Request.UrlReferrer.Host,Request.UrlReferrer.Port);
		txtCloseHtmlDoc2.EditorDirectory = string.Format("http://{0}:{1}/pinEdit/editor/",Request.UrlReferrer.Host,Request.UrlReferrer.Port);

		if (!IsPostBack) {
			if (CalledByOtherPrograms() == false) {
				InitPage();
				//lstSiteCd.SelectedIndex = 0;
			}
		}
	}

	protected void dsLiveBroadcast_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		grdLive.PageSize = int.Parse(Session["PageSize"].ToString());
		ClearField();
		pnlList.Visible = true;
		pnlMainte.Visible = false;
		DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		DataBind();
		if (!IsPostBack) {
			for (int i = 0;i < MAX_CAMERA;i++) {
				DropDownList lstCameraId = (DropDownList)plcHolder.FindControl(string.Format("lstCameraId{0}",i)) as DropDownList;
				lstCameraId.Items.Insert(0,new ListItem("",""));
				lstCameraId.DataSourceID = "";
				lstCameraId.SelectedValue = "";

				DropDownList lstUseTerminalType = (DropDownList)plcHolder.FindControl(string.Format("lstUseTerminalType{0}",i)) as DropDownList;
				lstUseTerminalType.Items.Insert(0,new ListItem("",""));
				lstUseTerminalType.DataSourceID = "";
				lstUseTerminalType.SelectedValue = "";
			}
		}
		
		DisplayWordUtil.ReplaceValidatorErrorMessage(this.Page.Validators);
	}

	private void ClearField() {
		txtLiveNm.Text = "";
		txtWebScreenId.Text = "";
		txtChargePoint.Text = "";
		txtChargeUnitSec.Text = "";
		chkPublicFlag.Checked = false;
		txtOpenHtmlDoc1.Text = "";
		txtOpenHtmlDoc2.Text = "";
		txtCloseHtmlDoc1.Text = "";
		txtCloseHtmlDoc2.Text = "";
		txtLoginId.Text = "";
		lnkHandleNm.Text = "";

		for (int i = 0;i < MAX_CAMERA;i++) {
			DropDownList lstCameraId = (DropDownList)plcHolder.FindControl(string.Format("lstCameraId{0}",i)) as DropDownList;
			lstCameraId.SelectedIndex = 0;

			DropDownList lstUseTerminalType = (DropDownList)plcHolder.FindControl(string.Format("lstUseTerminalType{0}",i)) as DropDownList;
			lstUseTerminalType.SelectedIndex = 0;

			TextBox txtTerminalId = (TextBox)plcHolder.FindControl(string.Format("txtTerminalId{0}",i)) as TextBox;
			txtTerminalId.Text = "";

			TextBox txtLiveCameraNm = (TextBox)plcHolder.FindControl(string.Format("txtLiveCameraNm{0}",i)) as TextBox;
			txtLiveCameraNm.Text = "";

			TextBox txtCameraOperationDTMF = (TextBox)plcHolder.FindControl(string.Format("txtCameraOperationDTMF{0}",i)) as TextBox;
			txtCameraOperationDTMF.Text = "";

			TextBox txtCameraTitle = (TextBox)plcHolder.FindControl(string.Format("txtCameraTitle{0}",i)) as TextBox;
			txtCameraTitle.Text = "";

			CheckBox chkMainCameraFlag = (CheckBox)plcHolder.FindControl(string.Format("chkMainCameraFlag{0}",i)) as CheckBox;
			chkMainCameraFlag.Checked = false;
		}
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		lstSiteCd.SelectedIndex = lstSeekSiteCd.SelectedIndex;
		lblLiveSeq.Text = "";
		GetData();
	}

	protected void lnkLive_Command(object sender,CommandEventArgs e) {
		string sKey = e.CommandArgument.ToString();
		int idx = sKey.IndexOf(":");
		if (idx >= 0) {
			lstSiteCd.SelectedValue = sKey.Substring(0,idx);
			lblLiveSeq.Text = sKey.Substring(idx + 1,sKey.Length - idx - 1);
			GetData();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void vdcLoginId_ServerValidate(object source,ServerValidateEventArgs args) {
		using (CastCharacter oCharacter = new CastCharacter()) {
			args.IsValid = oCharacter.IsExistLoginId(lstSiteCd.SelectedValue,txtLoginId.Text,ViCommConst.MAIN_CHAR_NO);
			if (args.IsValid) {
				ViewState["USER_SEQ"] = oCharacter.userSeq;
			} else {
				ViewState["USER_SEQ"] = "";
				lnkHandleNm.Text = "";
			}
		}

		if (args.IsValid) {
			using (CastCharacter oCharcter = new CastCharacter()) {
				DataSet ds = oCharcter.GetOne(lstSiteCd.SelectedValue,txtLoginId.Text,ViCommConst.MAIN_CHAR_NO); //とりあえずﾋﾞﾙﾄﾞ通すためにｷｬﾗｸﾀｰNoはﾒｲﾝ　DAMMY
				if (ds.Tables[0].Rows.Count > 0) {
					DataRow dr = ds.Tables[0].Rows[0];
					lnkHandleNm.Text = dr["HANDLE_NM"].ToString();
					lnkHandleNm.NavigateUrl = string.Format("~/Cast/CastView.aspx?loginid={0}&return=LiveBroadCastList.aspx",txtLoginId.Text);
				}
			}
		}
	}

	private void GetList() {
		pnlList.Visible = true;
		grdLive.DataBind();
		pnlCount.DataBind();
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("LIVE_BROADCAST_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PLIVE_SEQ",DbSession.DbType.VARCHAR2,lblLiveSeq.Text);
			db.ProcedureOutParm("PLIVE_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHTML_DOC1",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHTML_DOC2",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHTML_DOC3",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHTML_DOC4",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPUBLISH_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PNA_HTML_DOC1",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PNA_HTML_DOC2",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PNA_HTML_DOC3",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PNA_HTML_DOC4",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PWEB_SCREEN_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCHARGE_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCHARGE_UNIT_SEC",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PUSER_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PLOGIN_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHANDLE_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);

			db.ProcedureOutArrayParm("PCAMERA_ID",DbSession.DbType.VARCHAR2,MAX_CAMERA);
			db.ProcedureOutArrayParm("PCAMERA_TERMINAL_TYPE",DbSession.DbType.VARCHAR2,MAX_CAMERA);
			db.ProcedureOutArrayParm("PCAMERA_TERMINAL_ID",DbSession.DbType.VARCHAR2,MAX_CAMERA);
			db.ProcedureOutArrayParm("PCAMERA_LIVE_NM",DbSession.DbType.VARCHAR2,MAX_CAMERA);
			db.ProcedureOutArrayParm("PCAMERA_DTMF",DbSession.DbType.VARCHAR2,MAX_CAMERA);
			db.ProcedureOutArrayParm("PCAMERA_TITLE",DbSession.DbType.VARCHAR2,MAX_CAMERA);
			db.ProcedureOutArrayParm("PCAMERA_MAIN_FLAG",DbSession.DbType.VARCHAR2,MAX_CAMERA);

			db.ProcedureOutParm("PCAMERA_RECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtLiveNm.Text = db.GetStringValue("PLIVE_NM");
				txtWebScreenId.Text = db.GetStringValue("PWEB_SCREEN_ID");
				txtChargePoint.Text = db.GetStringValue("PCHARGE_POINT");
				txtChargeUnitSec.Text = db.GetStringValue("PCHARGE_UNIT_SEC");
				chkPublicFlag.Checked = (db.GetStringValue("PPUBLISH_FLAG").Equals("1") == true);
				txtLoginId.Text = db.GetStringValue("PLOGIN_ID");
				lnkHandleNm.Text = db.GetStringValue("PHANDLE_NM");
				lnkHandleNm.NavigateUrl = string.Format("~/Cast/CastView.aspx?loginid={0}&return=LiveBroadCastList.aspx",txtLoginId.Text);
				txtOpenHtmlDoc1.Text = db.GetStringValue("PHTML_DOC1") + db.GetStringValue("PHTML_DOC2");
				txtOpenHtmlDoc2.Text = db.GetStringValue("PHTML_DOC3") + db.GetStringValue("PHTML_DOC4");
				txtCloseHtmlDoc1.Text = db.GetStringValue("PNA_HTML_DOC1") + db.GetStringValue("PNA_HTML_DOC2");
				txtCloseHtmlDoc2.Text = db.GetStringValue("PNA_HTML_DOC3") + db.GetStringValue("PNA_HTML_DOC4");
			} else {
				ClearField();
			}

			for (int i = 0;i < MAX_CAMERA;i++) {
				DropDownList lstCameraId = (DropDownList)plcHolder.FindControl(string.Format("lstCameraId{0}",i)) as DropDownList;
				DropDownList lstUseTerminalType = (DropDownList)plcHolder.FindControl(string.Format("lstUseTerminalType{0}",i)) as DropDownList;
				TextBox txtTerminalId = (TextBox)plcHolder.FindControl(string.Format("txtTerminalId{0}",i)) as TextBox;
				TextBox txtLiveCameraNm = (TextBox)plcHolder.FindControl(string.Format("txtLiveCameraNm{0}",i)) as TextBox;
				TextBox txtCameraOperationDTMF = (TextBox)plcHolder.FindControl(string.Format("txtCameraOperationDTMF{0}",i)) as TextBox;
				TextBox txtCameraTitle = (TextBox)plcHolder.FindControl(string.Format("txtCameraTitle{0}",i)) as TextBox;
				CheckBox chkMainCameraFlag = (CheckBox)plcHolder.FindControl(string.Format("chkMainCameraFlag{0}",i)) as CheckBox;
				if (i < db.GetIntValue("PCAMERA_RECORD_COUNT")) {
					lstCameraId.SelectedValue = db.GetArryStringValue("PCAMERA_ID",i);
					lstUseTerminalType.SelectedValue = db.GetArryStringValue("PCAMERA_TERMINAL_TYPE",i);
					txtTerminalId.Text = db.GetArryStringValue("PCAMERA_TERMINAL_ID",i);
					txtLiveCameraNm.Text = db.GetArryStringValue("PCAMERA_LIVE_NM",i);
					txtCameraOperationDTMF.Text = db.GetArryStringValue("PCAMERA_DTMF",i);
					txtCameraTitle.Text = db.GetArryStringValue("PCAMERA_TITLE",i);
					chkMainCameraFlag.Checked = (db.GetArryStringValue("PCAMERA_MAIN_FLAG",i).Equals("1") == true);
				} else {
					lstCameraId.SelectedIndex = 0;
					lstUseTerminalType.SelectedIndex = 0;
					txtTerminalId.Text = "";
					txtLiveCameraNm.Text = "";
					txtCameraOperationDTMF.Text = "";
					txtCameraTitle.Text = "";
					chkMainCameraFlag.Checked = false;
				}
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;
		pnlList.Visible = false;
	}

	private void UpdateData(int pDelFlag) {
		int iPublicFlag = 0;

		if (chkPublicFlag.Checked) {
			iPublicFlag = 1;
		}

		string[] sOpenDoc1,sOpenDoc2,sCloseDoc1, sCloseDoc2;
		int iOpenDocCount1,iOpenDocCount2,iCloseDocCount1,iCloseDocCount2;

		SysPrograms.SeparateHtml(HttpUtility.HtmlDecode(txtOpenHtmlDoc1.Text),SysConst.MAX_HTML_BLOCKS,out sOpenDoc1,out iOpenDocCount1);
		SysPrograms.SeparateHtml(HttpUtility.HtmlDecode(txtOpenHtmlDoc2.Text),SysConst.MAX_HTML_BLOCKS,out sOpenDoc2,out iOpenDocCount2);
		SysPrograms.SeparateHtml(HttpUtility.HtmlDecode(txtCloseHtmlDoc1.Text),SysConst.MAX_HTML_BLOCKS,out sCloseDoc1,out iCloseDocCount1);
		SysPrograms.SeparateHtml(HttpUtility.HtmlDecode(txtCloseHtmlDoc2.Text),SysConst.MAX_HTML_BLOCKS,out sCloseDoc2,out iCloseDocCount2);

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("LIVE_BROADCAST_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PLIVE_SEQ",DbSession.DbType.VARCHAR2,lblLiveSeq.Text);
			db.ProcedureInParm("PLIVE_NM",DbSession.DbType.VARCHAR2,txtLiveNm.Text);
			db.ProcedureInParm("PHTML_DOC1",DbSession.DbType.VARCHAR2,sOpenDoc1[0]);
			db.ProcedureInParm("PHTML_DOC2",DbSession.DbType.VARCHAR2,sOpenDoc1[1]);
			db.ProcedureInParm("PHTML_DOC3",DbSession.DbType.VARCHAR2,sOpenDoc2[0]);
			db.ProcedureInParm("PHTML_DOC4",DbSession.DbType.VARCHAR2,sOpenDoc2[1]);
			db.ProcedureInParm("PPUBLISH_FLAG",DbSession.DbType.NUMBER,iPublicFlag);
			db.ProcedureInParm("PNA_HTML_DOC1",DbSession.DbType.VARCHAR2,sCloseDoc1[0]);
			db.ProcedureInParm("PNA_HTML_DOC2",DbSession.DbType.VARCHAR2,sCloseDoc1[1]);
			db.ProcedureInParm("PNA_HTML_DOC3",DbSession.DbType.VARCHAR2,sCloseDoc2[0]);
			db.ProcedureInParm("PNA_HTML_DOC4",DbSession.DbType.VARCHAR2,sCloseDoc2[1]);
			db.ProcedureInParm("PWEB_SCREEN_ID",DbSession.DbType.VARCHAR2,txtWebScreenId.Text);
			db.ProcedureInParm("PCHARGE_POINT",DbSession.DbType.NUMBER,txtChargePoint.Text);
			db.ProcedureInParm("PCHARGE_UNIT_SEC",DbSession.DbType.NUMBER,txtChargeUnitSec.Text);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["USER_SEQ"]));
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,ViewState["REVISION_NO"].ToString());
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);

			string[] sCameraId = new string[MAX_CAMERA];
			string[] sUseTerminalType = new string[MAX_CAMERA];
			string[] sUseTerminalId = new string[MAX_CAMERA];
			string[] sLiveCameraNm = new string[MAX_CAMERA];
			string[] sDtmf = new string[MAX_CAMERA];
			string[] sTitle = new string[MAX_CAMERA];
			string[] sMainFlag = new string[MAX_CAMERA];

			for (int i = 0;i < MAX_CAMERA;i++) {
				DropDownList lstCameraId = (DropDownList)plcHolder.FindControl(string.Format("lstCameraId{0}",i)) as DropDownList;
				DropDownList lstUseTerminalType = (DropDownList)plcHolder.FindControl(string.Format("lstUseTerminalType{0}",i)) as DropDownList;
				TextBox txtTerminalId = (TextBox)plcHolder.FindControl(string.Format("txtTerminalId{0}",i)) as TextBox;
				TextBox txtLiveCameraNm = (TextBox)plcHolder.FindControl(string.Format("txtLiveCameraNm{0}",i)) as TextBox;
				TextBox txtCameraOperationDTMF = (TextBox)plcHolder.FindControl(string.Format("txtCameraOperationDTMF{0}",i)) as TextBox;
				TextBox txtCameraTitle = (TextBox)plcHolder.FindControl(string.Format("txtCameraTitle{0}",i)) as TextBox;
				CheckBox chkMainCameraFlag = (CheckBox)plcHolder.FindControl(string.Format("chkMainCameraFlag{0}",i)) as CheckBox;

				sCameraId[i] = lstCameraId.SelectedValue;
				sUseTerminalType[i] = lstUseTerminalType.SelectedValue;
				sUseTerminalId[i] = txtTerminalId.Text;
				sLiveCameraNm[i] = txtLiveCameraNm.Text;
				sDtmf[i] = txtCameraOperationDTMF.Text;
				sTitle[i] = txtCameraTitle.Text;

				if (chkMainCameraFlag.Checked) {
					sMainFlag[i] = "1";
				} else {
					sMainFlag[i] = "0";
				}
			}

			db.ProcedureInArrayParm("PCAMERA_ID",DbSession.DbType.VARCHAR2,MAX_CAMERA,sCameraId);
			db.ProcedureInArrayParm("PCAMERA_TERMINAL_TYPE",DbSession.DbType.VARCHAR2,MAX_CAMERA,sUseTerminalType);
			db.ProcedureInArrayParm("PCAMERA_TERMINAL_ID",DbSession.DbType.VARCHAR2,MAX_CAMERA,sUseTerminalId);
			db.ProcedureInArrayParm("PCAMERA_LIVE_NM",DbSession.DbType.VARCHAR2,MAX_CAMERA,sLiveCameraNm);
			db.ProcedureInArrayParm("PCAMERA_DTMF",DbSession.DbType.VARCHAR2,MAX_CAMERA,sDtmf);
			db.ProcedureInArrayParm("PCAMERA_TITLE",DbSession.DbType.VARCHAR2,MAX_CAMERA,sTitle);
			db.ProcedureInArrayParm("PCAMERA_MAIN_FLAG",DbSession.DbType.VARCHAR2,MAX_CAMERA,sMainFlag);

			db.ProcedureInParm("PCAMERA_RECORD_COUNT",DbSession.DbType.NUMBER,MAX_CAMERA);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		int iIdx = lstSiteCd.SelectedIndex;
		InitPage();
		lstSeekSiteCd.SelectedIndex = iIdx;
		GetList();
	}

	private bool CalledByOtherPrograms() {
		string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		string sLiveSeq = iBridUtil.GetStringValue(Request.QueryString["liveseq"]);

		if ((!sSiteCd.Equals("")) && (!sLiveSeq.Equals(""))) {
			InitPage();
			lstSiteCd.SelectedValue = sSiteCd;
			lblLiveSeq.Text = sLiveSeq;
			GetData();
			return true;
		} else {
			return false;
		}
	}
	protected void dsLiveBroadcast_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
	}
}
