<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
	CodeFile="CallCamera.aspx.cs" Inherits="Live_CallCamera" Title="ライブカメラ呼出" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="ライブカメラ呼出"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset>
			<legend>[ライブカメラ一覧]</legend>
			<asp:GridView ID="grdCamera" runat="server" AllowPaging="True" AutoGenerateColumns="False"
				DataSourceID="dsLiveCamera" AllowSorting="True" SkinID="GridView" DataKeyNames="CAMERA_ID"
				OnRowCreated="grdCamera_RowCreated">
				<Columns>
					<asp:TemplateField HeaderText="呼出">
						<ItemTemplate>
							<asp:CheckBox ID="chkCall" runat="server" />
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField  HeaderText="録画">
						<ItemTemplate>
							<asp:CheckBox ID="chkRec" runat="server" />
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="ID/種別/呼出先">
						<ItemTemplate>
							<asp:Label ID="lblCameraId" runat="server" Text='<%# Eval("CAMERA_ID") %>'></asp:Label>
							<asp:Label ID="lblTerminalTypeNm" runat="server" Text='<%# Eval("USE_TERMINAL_TYPE_NM") %>'></asp:Label><br />
							<asp:Label ID="Label5" runat="server" Text='<%# Eval("TERMINAL_ID") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField DataField="CONNECTED_CAMERA_MARK" HeaderText="接続">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
					<asp:TemplateField HeaderText="利用サイト">
						<ItemTemplate>
							<asp:GridView ID="grdLive" runat="server" AutoGenerateColumns="False" GridLines="None"
								DataSource='<%# GetChildDataSource() %>' SkinID="GridView" Width="600px">
								<Columns>
									<asp:TemplateField HeaderText="サイト名">
										<ItemTemplate>
											<asp:Label ID="Label1" runat="server" Text='<%# Bind("SITE_NM") %>'></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="ライブ名">
										<ItemTemplate>
											<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# string.Format("LiveBroadCastList.aspx?sitecd={0}&liveseq={1}",Eval("SITE_CD"),Eval("LIVE_SEQ")) %>'
												Text='<%# Eval("LIVE_NM") %>'></asp:HyperLink>
										</ItemTemplate>
										<ItemStyle Width="180px" />
									</asp:TemplateField>
									<asp:TemplateField HeaderText="カメラ名">
										<ItemTemplate>
											<asp:Label ID="Label3" runat="server" Text='<%# Bind("LIVE_CAMERA_NM") %>'></asp:Label>
										</ItemTemplate>
										<ItemStyle Width="180px" />
									</asp:TemplateField>
								</Columns>
							</asp:GridView>
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Button runat="server" ID="btnUpdate" Text="呼出実行" CssClass="seekbutton" OnClick="btnUpdate_Click" />
			<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" />
			<br />
			<br />
			<a class="reccount">Record Count
				<%#GetRecCount() %>
			</a>
			<br>
			<a class="reccount">Current viewing page
				<%=grdCamera.PageIndex + 1%>
				of
				<%=grdCamera.PageCount%>
			</a>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsLiveCamera" runat="server" SelectMethod="GetPageCollection"
		TypeName="LiveCamera" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelected="dsLiveCamera_Selected">
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsBroadcastCamera" runat="server" SelectMethod="GetLiveList"
		TypeName="LiveBroadcastCamera">
		<SelectParameters>
			<asp:SessionParameter Name="pCameraId" SessionField="CurCameraId" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
