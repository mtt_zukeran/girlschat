<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ShareLiveTalkList.aspx.cs" Inherits="Live_ShareLiveTalkList"
	Title="Model ASP設定" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="Model ASP設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[Model ASP内容]</legend>
						<table border="0" style="width: 800px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									ライブキー
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblKey" runat="server" Text=""></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ライブ内容
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLiveNm" runat="server" MaxLength="100" Width="300px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrLiveNm" runat="server" ErrorMessage="ライブ内容を入力して下さい。" ControlToValidate="txtLiveNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("出演者ＩＤ") %>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLoginId" runat="server" MaxLength="8" Width="80px"></asp:TextBox>
									<asp:HyperLink ID="lnkCastNm" runat="server"></asp:HyperLink>
									<asp:RequiredFieldValidator ID="vdrLoginId" runat="server" ErrorMessage="出演者ＩＤを入力して下さい。" ControlToValidate="txtLoginId" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:CustomValidator ID="vdcLoginId" runat="server" ControlToValidate="txtLoginId" ErrorMessage="この出演者ＩＤは存在しません。" OnServerValidate="vdcLoginId_ServerValidate"
										ValidationGroup="Detail">*</asp:CustomValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ライブ開始日時
								</td>
								<td class="tdDataStyle">
									日付(YYYY/MM/DD)
									<asp:TextBox ID="txtStartDay" runat="server" MaxLength="10" Width="64px"></asp:TextBox>
									時刻(HH:MM)
									<asp:TextBox ID="txtStartTime" runat="server" MaxLength="5" Width="34px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrStartDay" runat="server" ErrorMessage="ライブ開始日を入力して下さい。" ControlToValidate="txtStartDay" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RangeValidator ID="vddStartDay" runat="server" ErrorMessage="ライブ開始日を正しく入力して下さい。" ControlToValidate="txtStartDay" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
										Type="Date" ValidationGroup="Detail">*</asp:RangeValidator>
									<asp:RequiredFieldValidator ID="vdrStartTime" runat="server" ErrorMessage="ライブ開始時刻を入力して下さい。" ControlToValidate="txtStartTime" ValidationGroup="Detail">*</asp:RequiredFieldValidator>&nbsp;
									<asp:RegularExpressionValidator ID="vdeStartTime" runat="server" ErrorMessage="ライブ開始時刻を正しく入力して下さい。" ValidationExpression="([0-9]|([0-1][0-9]|[2][0-3]))[:][0-5][0-9]"
										ControlToValidate="txtStartTime" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									ライブ終了日時
								</td>
								<td class="tdDataStyle">
									日付(YYYY/MM/DD)
									<asp:TextBox ID="txtEndDay" runat="server" MaxLength="10" Width="64px"></asp:TextBox>
									時刻(HH:MM)
									<asp:TextBox ID="txtEndTime" runat="server" MaxLength="5" Width="34px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrEndDay" runat="server" ErrorMessage="ライブ終了日を入力して下さい。" ControlToValidate="txtEndDay" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RangeValidator ID="vddEndDay" runat="server" ErrorMessage="ライブ終了日を正しく入力して下さい。" ControlToValidate="txtEndDay" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
										Type="Date" ValidationGroup="Detail">*</asp:RangeValidator>
									<asp:RequiredFieldValidator ID="vdrEndTime" runat="server" ErrorMessage="ライブ終了時刻を入力して下さい。" ControlToValidate="txtEndTime" ValidationGroup="Detail">*</asp:RequiredFieldValidator>&nbsp;
									<asp:RegularExpressionValidator ID="vdeEndTime" runat="server" ErrorMessage="ライブ終了時刻を正しく入力して下さい。" ValidationExpression="([0-9]|([0-1][0-9]|[2][0-3]))[:][0-5][0-9]"
										ControlToValidate="txtEndTime" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlList">
			<fieldset>
				<legend>[Model ASP一覧]</legend>
				<table border="0" style="width: 340px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle2">
							ライブ開始日
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtReportDayFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RequiredFieldValidator ID="vdrReportDayFrom" runat="server" ErrorMessage="ライブ開始Fromを入力して下さい。" ControlToValidate="txtReportDayFrom" ValidationGroup="Key"
								Display="Dynamic">*</asp:RequiredFieldValidator>
							<asp:RangeValidator ID="vdeReportDayFrom" runat="server" ErrorMessage="ライブ開始Fromを正しく入力して下さい。" ControlToValidate="txtReportDayFrom" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							〜&nbsp;
							<asp:TextBox ID="txtReportDayTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RangeValidator ID="vdeReportDayTo" runat="server" ErrorMessage="ライブ開始Toを正しく入力して下さい。" ControlToValidate="txtReportDayTo" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnRegist" Text="ライブ追加" CssClass="seekbutton" CausesValidation="False" OnClick="btnRegist_Click" />
				<br />
				<br />
				<asp:GridView ID="grdShareLive" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsShareLiveTalk" AllowSorting="True" SkinID="GridViewColor"
					Width="880px">
					<Columns>
						<asp:TemplateField HeaderText="開始日時">
							<ItemTemplate>
								<asp:HyperLink ID="lnkStartDate" runat="server" NavigateUrl='<%# string.Format("ShareLiveSalesDetail.aspx?shkey={0}&dayfrom={1}&dayto={2}",Eval("SHARE_LIVE_TALK_KEY"),Eval("START_DATE","{0:yyyy/MM/dd}"),Eval("END_DATE","{0:yyyy/MM/dd}")) %>'
									Text='<%# Eval("START_DATE", "{0:yyyy/MM/dd HH:mm}") %>'></asp:HyperLink>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Center" Width="140px" />
						</asp:TemplateField>
						<asp:BoundField DataField="END_DATE" HeaderText="終了日時" DataFormatString="{0:yyyy/MM/dd HH:mm}" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" Width="140px" />
						</asp:BoundField>
						<asp:TemplateField HeaderText="出演者ID" SortExpression="LOGIN_ID">
							<ItemTemplate>
								<asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}",Eval("LOGIN_ID"),"ShareLiveTalkList.aspx") %>'
									Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Center" />
						</asp:TemplateField>
						<asp:BoundField DataField="CAST_NM" HeaderText="出演者名"></asp:BoundField>
						<asp:BoundField DataField="LIVE_NM" HeaderText="ライブ内容"></asp:BoundField>
						<asp:TemplateField HeaderText="ライブキー">
							<ItemTemplate>
								<asp:LinkButton ID="lnkShareLive" runat="server" Text='<%# Eval("SHARE_LIVE_TALK_KEY") %>' CommandArgument='<%# Eval("SHARE_LIVE_TALK_KEY")%>' OnCommand="lnkShareLive_Command"
									CausesValidation="False">
								</asp:LinkButton>
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
					<PagerSettings Mode="NumericFirstLast" />
				</asp:GridView>
				&nbsp;
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count
						<%#GetRecCount()%>
					</a>
					<br>
					<a class="reccount">Current viewing page
						<%=grdShareLive.PageIndex + 1%>
						of
						<%=grdShareLive.PageCount%>
					</a>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsShareLiveTalk" runat="server" SelectMethod="GetPageCollection" TypeName="ShareLiveTalk" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsShareLiveTalk_Selected" OnSelecting="dsShareLiveTalk_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrLiveNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender13" TargetControlID="vdrLoginId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrReportDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdeReportDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeReportDayTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdrStartDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vddStartDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9" TargetControlID="vdrStartTime" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10" TargetControlID="vdeStartTime" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vdrEndDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8" TargetControlID="vddEndDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11" TargetControlID="vdrEndTime" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender12" TargetControlID="vdeEndTime" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskReportDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="False"
		TargetControlID="txtReportDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskReportDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="False"
		TargetControlID="txtReportDayTo">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskStartDay" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtStartDay">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskEndDay" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtEndDay">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskStartTime" runat="server" MaskType="Time" Mask="99:99" UserTimeFormat="TwentyFourHour" ClearMaskOnLostFocus="true"
		TargetControlID="txtStartTime">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskEndTime" runat="server" MaskType="Time" Mask="99:99" UserTimeFormat="TwentyFourHour" ClearMaskOnLostFocus="true"
		TargetControlID="txtEndTime">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
