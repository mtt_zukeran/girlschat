<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ShareLiveSalesDetail.aspx.cs" Inherits="Live_ShareLiveSalesDetail"
	Title="Model ASP売上明細" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="Model ASP売上明細"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							課金日
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtReportDayFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RequiredFieldValidator ID="vdrReportDayFrom" runat="server" ErrorMessage="課金日Fromを入力して下さい。" ControlToValidate="txtReportDayFrom" ValidationGroup="Key"
								Display="Dynamic">*</asp:RequiredFieldValidator>
							<asp:RangeValidator ID="vdeReportDayFrom" runat="server" ErrorMessage="課金日Fromを正しく入力して下さい。" ControlToValidate="txtReportDayFrom" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							〜&nbsp;
							<asp:TextBox ID="txtReportDayTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RangeValidator ID="vdeReportDayTo" runat="server" ErrorMessage="課金日Toを正しく入力して下さい。" ControlToValidate="txtReportDayTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
								Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
						</td>
						<td class="tdHeaderStyle">
							<%= DisplayWordUtil.Replace("出演者ＩＤ") %>
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtLoginId" runat="server" Width="70px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							利用者サイト名
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtIvpSiteNm" runat="server" Width="100px"></asp:TextBox>
						</td>
						<td class="tdHeaderStyle2">
							ライブキー
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtShareLiveTalkKey" runat="server" Width="100px"></asp:TextBox>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
				<asp:Button runat="server" ID="btnCsv" Text="ＣＳＶ出力" CssClass="seekbutton" OnClick="btnCSV_Click" />
			</asp:Panel>
		</fieldset>
		<fieldset>
			<legend>[売上明細]</legend>
			<asp:GridView ID="grdSalesDetail" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsShareLiveSalesDtl" AllowSorting="True"
				SkinID="GridViewColor" OnDataBound="grdSalesDetail_DataBound">
				<Columns>
					<asp:TemplateField HeaderText="ライブ内容">
						<ItemTemplate>
							<asp:Label ID="Label1" runat="server" Text='<%# Eval("SHARE_LIVE_TALK_KEY") %>'></asp:Label>
							<asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}",Eval("LOGIN_ID"),"ShareLiveSalesDetail.aspx") %>'
								Text='<%# string.Format("{0} {1}",Eval("LOGIN_ID"),Eval("CAST_NM")) %>'></asp:HyperLink>
							<br />
							<asp:Label ID="Label3" runat="server" Text='<%# Eval("START_DATE", "{0:MM/dd HH:mm}") %>'></asp:Label>〜
							<asp:Label ID="Label4" runat="server" Text='<%# Bind("END_DATE", "{0:MM/dd HH:mm}") %>'></asp:Label>
							<br />
							<asp:Label ID="Label2" runat="server" Text='<%# Eval("LIVE_NM") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField DataField="CHARGE_START_DATE" HeaderText="課金開始時間" DataFormatString="{0:MM/dd HH:mm:ss}" HtmlEncode="False" SortExpression="CHARGE_START_DATE">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
					<asp:BoundField DataField="CHARGE_END_DATE" HeaderText="課金終了時間" DataFormatString="{0:MM/dd HH:mm:ss}" HtmlEncode="False" SortExpression="CHARGE_END_DATE">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
					<asp:BoundField DataField="IVP_SITE_NM" HeaderText="利用ｻｲﾄ名" />
					<asp:BoundField DataField="IVP_ACCEPT_SEQ" HeaderText="照会��">
					<itemstyle horizontalalign="Center" />
					</asp:BoundField>
					<asp:BoundField DataField="CHARGE_SEC" HeaderText="秒数" DataFormatString="{0}秒 " SortExpression="CHARGE_SEC">
						<ItemStyle HorizontalAlign="Right" Width="50px" />
						<FooterStyle HorizontalAlign="Center" />
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			&nbsp;
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br>
				<a class="reccount">Current viewing page
					<%=grdSalesDetail.PageIndex + 1%>
					of
					<%=grdSalesDetail.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsShareLiveSalesDtl" runat="server" SelectMethod="GetPageCollection" TypeName="ShareLiveSalesDtl" SelectCountMethod="GetPageCount"
		EnablePaging="True" OnSelected="dsShareLiveSalesDtl_Selected" OnSelecting="dsShareLiveSalesDtl_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pShareLiveTalkKey" Type="String" />
			<asp:Parameter Name="pLoginId" Type="String" />
			<asp:Parameter Name="pIvpSiteNm" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrReportDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdeReportDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdeReportDayTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskReportDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="False"
		TargetControlID="txtReportDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskReportDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="False"
		TargetControlID="txtReportDayTo">
	</ajaxToolkit:MaskedEditExtender>
</asp:Content>
