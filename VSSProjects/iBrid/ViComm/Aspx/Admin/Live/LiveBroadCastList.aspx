<%@ Import Namespace="ViComm" %>
<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="LiveBroadCastList.aspx.cs" Inherits="Live_LiveBroadCastList"
	Title="ライブ設定" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="ライブ設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									サイトコード
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
									</asp:DropDownList>
								</td>
							</tr>
						</table>
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[ライブ内容]</legend>
						<table border="0" style="width: 800px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									ライブ名称
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblLiveSeq" runat="server" Text="" Visible="false"></asp:Label>
									<asp:TextBox ID="txtLiveNm" runat="server" MaxLength="100" Width="200px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrLiveNm" runat="server" ErrorMessage="ライブ名称を入力して下さい。" ControlToValidate="txtLiveNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									Ｗｅｂ画面ＩＤ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtWebScreenId" runat="server" MaxLength="2" Width="32px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrWebScreenId" runat="server" ErrorMessage="Ｗｅｂ画面ＩＤを入力して下さい。" ControlToValidate="txtWebScreenId" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeWebScreenId" runat="server" ErrorMessage="Ｗｅｂ画面ＩＤは２桁の数字で入力して下さい。" ValidationExpression="\d{2}" ControlToValidate="txtWebScreenId"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									課金ポイント
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtChargePoint" runat="server" MaxLength="5" Width="32px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrChargePoint" runat="server" ErrorMessage="課金ポイントを入力して下さい。" ControlToValidate="txtChargePoint" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									課金単位秒数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtChargeUnitSec" runat="server" MaxLength="3" Width="32px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrChargeUnitSec" runat="server" ErrorMessage="課金単位秒数を入力して下さい。" ControlToValidate="txtChargeUnitSec" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									公開フラグ
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkPublicFlag" runat="server" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("出演者ＩＤ") %>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLoginId" runat="server" MaxLength="8" Width="80px"></asp:TextBox>
									<asp:HyperLink ID="lnkHandleNm" runat="server"></asp:HyperLink>
									<asp:CustomValidator ID="vdcLoginId" runat="server" ControlToValidate="txtLoginId" ErrorMessage="この出演者ＩＤは存在しません。" OnServerValidate="vdcLoginId_ServerValidate"
										ValidationGroup="Detail">*</asp:CustomValidator>
								</td>
							</tr>
						</table>
						「ライブカメラ設定」
						<asp:PlaceHolder ID="plcHolder" runat="server">
							<table border="0" style="width: 800px" class="tableStyle">
								<tr>
									<td class="tdHeaderSmallStyle">
										カメラ1
									</td>
									<td class="tdDataStyle">
										ｶﾒﾗID<br />
										<asp:DropDownList ID="lstCameraId0" runat="server" DataSourceID="dsLiveCamera" DataTextField="CAMERA_NM" DataValueField="CAMERA_ID" Width="100px">
										</asp:DropDownList>
									</td>
									<td class="tdDataStyle">
										発信種別<br />
										<asp:DropDownList ID="lstUseTerminalType0" runat="server" DataSourceID="dsTerminalType" DataTextField="CODE_NM" DataValueField="CODE" Width="80px">
										</asp:DropDownList>
									</td>
									<td class="tdDataStyle">
										発信先<br />
										<asp:TextBox ID="txtTerminalId0" runat="server" MaxLength="64" Width="200px"></asp:TextBox>
									</td>
									<td class="tdDataStyle">
										ﾀｲﾄﾙ<br />
										<asp:TextBox ID="txtLiveCameraNm0" runat="server" MaxLength="20" Width="100px"></asp:TextBox>
									</td>
									<td class="tdDataStyle">
										DTMF<br />
										<asp:TextBox ID="txtCameraOperationDTMF0" runat="server" MaxLength="1" Width="10px"></asp:TextBox>
									</td>
									<td class="tdDataStyle">
										ｵｰﾊﾞｰﾚｲ<br />
										<asp:TextBox ID="txtCameraTitle0" runat="server" MaxLength="20" Width="100px"></asp:TextBox>
										<asp:RegularExpressionValidator ID="vdeCameraTitle0" runat="server" ErrorMessage="ﾀｲﾄﾙは20桁以内の英数字で入力して下さい。" ValidationExpression="[\W\da-zA-Z]{1,20}" ControlToValidate="txtCameraTitle0"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										ﾒｲﾝｶﾒﾗ<br />
										<asp:CheckBox ID="chkMainCameraFlag0" runat="server" />
									</td>
								</tr>
								<tr>
									<td class="tdHeaderSmallStyle">
										カメラ２
									</td>
									<td class="tdDataStyle">
										<asp:DropDownList ID="lstCameraId1" runat="server" DataSourceID="dsLiveCamera" DataTextField="CAMERA_NM" DataValueField="CAMERA_ID" Width="100px">
										</asp:DropDownList>
									</td>
									<td class="tdDataStyle">
										<asp:DropDownList ID="lstUseTerminalType1" runat="server" DataSourceID="dsTerminalType" DataTextField="CODE_NM" DataValueField="CODE" Width="80px">
										</asp:DropDownList>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtTerminalId1" runat="server" MaxLength="64" Width="200px"></asp:TextBox>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtLiveCameraNm1" runat="server" MaxLength="20" Width="100px"></asp:TextBox>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCameraOperationDTMF1" runat="server" MaxLength="1" Width="10px"></asp:TextBox>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCameraTitle1" runat="server" MaxLength="20" Width="100px"></asp:TextBox>
										<asp:RegularExpressionValidator ID="vdeCameraTitle1" runat="server" ErrorMessage="ﾀｲﾄﾙは20桁以内の英数字で入力して下さい。" ValidationExpression="[\W\da-zA-Z]{1,20}" ControlToValidate="txtCameraTitle1"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:CheckBox ID="chkMainCameraFlag1" runat="server" />
									</td>
								</tr>
								<tr>
									<td class="tdHeaderSmallStyle">
										カメラ３
									</td>
									<td class="tdDataStyle">
										<asp:DropDownList ID="lstCameraId2" runat="server" DataSourceID="dsLiveCamera" DataTextField="CAMERA_NM" DataValueField="CAMERA_ID" Width="100px">
										</asp:DropDownList>
									</td>
									<td class="tdDataStyle">
										<asp:DropDownList ID="lstUseTerminalType2" runat="server" DataSourceID="dsTerminalType" DataTextField="CODE_NM" DataValueField="CODE" Width="80px">
										</asp:DropDownList>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtTerminalId2" runat="server" MaxLength="64" Width="200px"></asp:TextBox>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtLiveCameraNm2" runat="server" MaxLength="20" Width="100px"></asp:TextBox>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCameraOperationDTMF2" runat="server" MaxLength="1" Width="10px"></asp:TextBox>
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtCameraTitle2" runat="server" MaxLength="20" Width="100px"></asp:TextBox>
										<asp:RegularExpressionValidator ID="vdeCameraTitle2" runat="server" ErrorMessage="ﾀｲﾄﾙは20桁以内の英数字で入力して下さい。" ValidationExpression="[\W\da-zA-Z]{1,20}" ControlToValidate="txtCameraTitle2"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
									<td class="tdDataStyle">
										<asp:CheckBox ID="chkMainCameraFlag2" runat="server" />
									</td>
								</tr>
							</table>
						</asp:PlaceHolder>
						<br>
						「上段ＨＴＭＬ」
						<table border="0" style="width: 780px" class="tableStyle">
							<tr>
								<td class="tdHeaderSmallStyle">
									公開時
								</td>
								<td class="tdDataStyle">
									<pin:pinEdit ID="txtOpenHtmlDoc1" runat="server" UserMode="HTML" DisplayStyle="Style5" DocumentView="True" HtmlMode="Body" Ruler="False" ReturnMode="BR"
										SpellMode="Inline" ToolbarConfiguration="ToolbarDesigner" Toolbar="T09SE10116012136267;T22232427282930313233343536;B53SE54SE55" FormatMode="Advanced"
										Optimizer="True" BorderWidth="1px" Height="450px" Width="310px" DocumentWidth="230" Font-Bold="False" FontSizeType="px" RelativeBaseImageUrl="/" RelativeBaseLinkUrl="/">
									</pin:pinEdit>
								</td>
								<td class="tdHeaderSmallStyle">
									非公開時
								</td>
								<td class="tdDataStyle">
									<pin:pinEdit ID="txtCloseHtmlDoc1" runat="server" UserMode="HTML" DisplayStyle="Style5" DocumentView="True" HtmlMode="Body" Ruler="False" ReturnMode="BR"
										SpellMode="Inline" ToolbarConfiguration="ToolbarDesigner" Toolbar="T09SE10116012136267;T22232427282930313233343536;B53SE54SE55" FormatMode="Advanced"
										Optimizer="True" BorderWidth="1px" Height="450px" Width="310px" DocumentWidth="230" Font-Bold="False" FontSizeType="px" RelativeBaseImageUrl="/" RelativeBaseLinkUrl="/">
									</pin:pinEdit>
								</td>
							</tr>
						</table>
						<br>
						「下段ＨＴＭＬ」
						<table border="0" style="width: 780px" class="tableStyle">
							<tr>
								<td class="tdHeaderSmallStyle">
									公開時
								</td>
								<td class="tdDataStyle">
									<pin:pinEdit ID="txtOpenHtmlDoc2" runat="server" UserMode="HTML" DisplayStyle="Style5" DocumentView="True" HtmlMode="Body" Ruler="False" ReturnMode="BR"
										SpellMode="Inline" ToolbarConfiguration="ToolbarDesigner" Toolbar="T09SE10116012136267;T22232427282930313233343536;B53SE54SE55" FormatMode="Advanced"
										Optimizer="True" BorderWidth="1px" Height="240px" Width="310px" DocumentWidth="230" Font-Bold="False" FontSizeType="px" RelativeBaseImageUrl="/" RelativeBaseLinkUrl="/">
									</pin:pinEdit>
								</td>
								<td class="tdHeaderSmallStyle">
									非公開時
								</td>
								<td class="tdDataStyle">
									<pin:pinEdit ID="txtCloseHtmlDoc2" runat="server" UserMode="HTML" DisplayStyle="Style5" DocumentView="True" HtmlMode="Body" Ruler="False" ReturnMode="BR"
										SpellMode="Inline" ToolbarConfiguration="ToolbarDesigner" Toolbar="T09SE10116012136267;T22232427282930313233343536;B53SE54SE55" FormatMode="Advanced"
										Optimizer="True" BorderWidth="1px" Height="240px" Width="310px" DocumentWidth="230" Font-Bold="False" FontSizeType="px" RelativeBaseImageUrl="/" RelativeBaseLinkUrl="/">
									</pin:pinEdit>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlList">
			<fieldset>
				<legend>[ライブ一覧]</legend>
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle2">
							サイトコード
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
							</asp:DropDownList>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
				<asp:Button runat="server" ID="btnRegist" Text="ライブ追加" CssClass="seekbutton" CausesValidation="False" OnClick="btnRegist_Click" />
				<br />
				<br />
				<asp:GridView ID="grdLive" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsLiveBroadcast" AllowSorting="True" SkinID="GridView"
					Width="260px" ShowHeader="False">
					<Columns>
						<asp:TemplateField>
							<ItemTemplate>
								<asp:LinkButton ID="lnkLiveOpen" runat="server" Text='<%# string.Format("{1}(WebID={0})",Eval("WEB_SCREEN_ID"),Eval("LIVE_NM")) %>' CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("LIVE_SEQ"))%>'
									OnCommand="lnkLive_Command" CausesValidation="False" Font-Size="Medium"></asp:LinkButton>
								<br />
								<asp:Literal ID="lblHtmlNm1" runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM"))%>'></asp:Literal>
								<asp:Literal ID="lblHtmlOpenDoc1" runat="server" Text='<%# string.Format("{0}{1}{2}{3}",Eval("HTML_DOC1"), Eval("HTML_DOC2"), Eval("HTML_DOC3"), Eval("HTML_DOC4"))%>'></asp:Literal>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
						</asp:TemplateField>
						<asp:TemplateField>
							<ItemTemplate>
								<asp:LinkButton ID="lnkLiveClose" runat="server" Text='<%# string.Format("{1}(WebID={0})",Eval("WEB_SCREEN_ID"),Eval("LIVE_NM")) %>' CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("LIVE_SEQ"))%>'
									OnCommand="lnkLive_Command" CausesValidation="False" Font-Size="Medium"></asp:LinkButton>
								<br />
								<asp:Literal ID="lblHtmlNm2" runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM"))%>'></asp:Literal>
								<asp:Literal ID="lblHtmlCloseDoc1" runat="server" Text='<%# string.Format("{0}{1}{2}{3}",Eval("NA_HTML_DOC1"), Eval("NA_HTML_DOC2"), Eval("NA_HTML_DOC3"), Eval("NA_HTML_DOC4"))%>'></asp:Literal>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
						</asp:TemplateField>
					</Columns>
					<PagerSettings Mode="NumericFirstLast" />
				</asp:GridView>
				&nbsp;
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count
						<%#GetRecCount() %>
					</a>
					<br>
					<a class="reccount">Current viewing page
						<%=grdLive.PageIndex + 1%>
						of
						<%=grdLive.PageCount%>
					</a>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsLiveBroadcast" runat="server" SelectMethod="GetPageCollection" TypeName="LiveBroadcast" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsLiveBroadcast_Selected" OnSelecting="dsLiveBroadcast_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsLiveCamera" runat="server" SelectMethod="GetList" TypeName="LiveCamera"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsTerminalType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="55" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtChargePoint" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtChargeUnitSec" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrLiveNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrWebScreenId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdeWebScreenId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdrChargePoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdrChargeUnitSec" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdcLoginId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8" TargetControlID="vdeCameraTitle0" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9" TargetControlID="vdeCameraTitle1" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10" TargetControlID="vdeCameraTitle2" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
