﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 共有ライブトークメンテナンス
--	Progaram ID		: ShareLiveTalkList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Live_ShareLiveTalkList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {

		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		grdShareLive.PageSize = int.Parse(Session["PageSize"].ToString());
		ClearField();
		pnlList.Visible = true;
		pnlMainte.Visible = false;
		DataBind();
		
		DisplayWordUtil.ReplaceValidatorErrorMessage(this.Page.Validators);
		DisplayWordUtil.ReplaceGridColumnHeader(this.grdShareLive);
	}

	private void ClearField() {
		txtLiveNm.Text = "";
		txtLoginId.Text = "";
		lnkCastNm.Text = "";
		txtStartDay.Text = "";
		txtStartTime.Text = "";
		txtEndDay.Text = "";
		txtEndTime.Text = "";
		txtReportDayFrom.Text = DateTime.Now.AddDays(-7).ToString("yyyy/MM/dd");
		txtReportDayTo.Text = DateTime.Now.AddDays(7).ToString("yyyy/MM/dd");
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		lblKey.Text = "";
		GetData();
	}

	protected void lnkShareLive_Command(object sender,CommandEventArgs e) {
		string sKey = e.CommandArgument.ToString();
		lblKey.Text = sKey;
		GetData();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void vdcLoginId_ServerValidate(object source,ServerValidateEventArgs args) {
		using (Cast oCast = new Cast()) {
			args.IsValid = oCast.IsExistLoginId(txtLoginId.Text);
			if (args.IsValid) {
				ViewState["USER_SEQ"] = oCast.userSeq;
				lnkCastNm.Text = oCast.castNm;
				lnkCastNm.NavigateUrl = string.Format("~/Cast/CastView.aspx?loginid={0}&return=ShareLiveTalkList.aspx",txtLoginId.Text);
			} else {
				ViewState["USER_SEQ"] = "";
				lnkCastNm.Text = "";
			}
		}
	}

	private void GetList() {
		pnlList.Visible = true;
		grdShareLive.DataBind();
		pnlCount.DataBind();
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SHARE_LIVE_TALK_GET");
			db.ProcedureInParm("PSHARE_LIVE_TALK_KEY",DbSession.DbType.VARCHAR2,lblKey.Text);
			db.ProcedureOutParm("PLIVE_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PUSER_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PLOGIN_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTART_DAY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTART_TIME",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PEND_DAY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PEND_TIME",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				ViewState["USER_SEQ"] = db.GetStringValue("PUSER_SEQ");
				txtLiveNm.Text = db.GetStringValue("PLIVE_NM");
				txtLoginId.Text = db.GetStringValue("PLOGIN_ID");
				lnkCastNm.Text = db.GetStringValue("PCAST_NM");
				lnkCastNm.NavigateUrl = string.Format("~/Cast/CastView.aspx?loginid={0}&return=ShareLiveTalkList.aspx",txtLoginId.Text);
				txtStartDay.Text = db.GetStringValue("PSTART_DAY");
				txtStartTime.Text = db.GetStringValue("PSTART_TIME");
				txtEndDay.Text = db.GetStringValue("PEND_DAY");
				txtEndTime.Text = db.GetStringValue("PEND_TIME");
			} else {
				ClearField();
			}

		}

		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlList.Visible = false;
	}

	private void UpdateData(int pDelFlag) {
		if (lblKey.Text.Equals("")) {
			lblKey.Text = IssueShareLiveTalkKey(txtLoginId.Text);
		}
		if (!lblKey.Text.Equals("")) {
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("SHARE_LIVE_TALK_MAINTE");
				db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["USER_SEQ"]));
				db.ProcedureInParm("PSHARE_LIVE_TALK_KEY",DbSession.DbType.VARCHAR2,lblKey.Text);
				db.ProcedureInParm("PLIVE_NM",DbSession.DbType.VARCHAR2,txtLiveNm.Text);
				db.ProcedureInParm("PSTART_DAY",DbSession.DbType.VARCHAR2,txtStartDay.Text);
				db.ProcedureInParm("PSTART_TIME",DbSession.DbType.VARCHAR2,txtStartTime.Text + ":00");
				db.ProcedureInParm("PEND_DAY",DbSession.DbType.VARCHAR2,txtEndDay.Text);
				db.ProcedureInParm("PEND_TIME",DbSession.DbType.VARCHAR2,txtEndTime.Text + ":00");
				db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
				db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,ViewState["REVISION_NO"].ToString());
				db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
			}
		}

		InitPage();
		GetList();
	}


	protected void dsShareLiveTalk_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsShareLiveTalk_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = txtReportDayFrom.Text;
		e.InputParameters[1] = txtReportDayTo.Text;
	}


	public string IssueShareLiveTalkKey(string pLoginId) {
		string sUrl = "",sLocCd,sSiteCd,sRequestUrl,sKey;

		using (Sys oSys = new Sys()) {
			oSys.GetValue("SIP_IVP_LOC_CD",out sLocCd);
			oSys.GetValue("SIP_IVP_SITE_CD",out sSiteCd);
			oSys.GetValue("IVP_ISSUE_LIVE_KEY_URL",out sRequestUrl);
		}

		sUrl = string.Format("{0}?req={1}&loc={2}&site={3}&remarks={4}",sRequestUrl,ViCommConst.SHARE_LIVE_KEY_CREATE,sLocCd,sSiteCd,pLoginId);

		if (ViCommInterface.ShareLiveTalkKey(sUrl,out sKey).Equals("0")) {
			return sKey;
		} else {
			return "";
		}
	}
}
