﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ライブカメラ呼出
--	Progaram ID		: CallCamera
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Live_CallCamera:System.Web.UI.Page {
	private string recCount = "";
	private string currentCameraId;

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	protected void dsLiveCamera_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		grdCamera.PageSize = int.Parse(Session["PageSize"].ToString());
		grdCamera.PageIndex = 0;
		ClearField();
		DataBind();
	}

	private void ClearField() {
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		CheckCells();
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void grdCamera_RowCreated(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			if (e.Row.DataItem != null) {
				currentCameraId = DataBinder.Eval(e.Row.DataItem,"CAMERA_ID").ToString();
			}
		}
	}

	protected DataTable GetChildDataSource() {
		DataSet ds;
		using (LiveBroadcastCamera oCamera = new LiveBroadcastCamera()) {
			ds = oCamera.GetLiveList(currentCameraId);
		}
		return ds.Tables["VW_LIVE_BROADCAST_CAMERA01"];
	}


	private void CheckCells() {
		int iRec = 0;
		int iRow = grdCamera.Rows.Count;
		using (LiveBroadcastCamera oCamera = new LiveBroadcastCamera()) {
			for (int i = 0;i < iRow;i++) {
				TableCell oChkCel = grdCamera.Rows[i].Cells[0];
				CheckBox oChk = (CheckBox)oChkCel.Controls[1];

				TableCell oChkRec = grdCamera.Rows[i].Cells[1];
				CheckBox oRec = (CheckBox)oChkRec.Controls[1];

				TableCell oIDCel = grdCamera.Rows[i].Cells[2];
				Label oId = (Label)oIDCel.Controls[1];

				if (oChk.Checked) {
					if (oRec.Checked) {
						iRec = 1;
					} else {
						iRec = 0;
					}
					oCamera.ExecuteCameraCall(oId.Text,iRec);
				}
			}
		}
	}
}
