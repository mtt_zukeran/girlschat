<%@ Import Namespace="ViComm" %>
<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastPerformance.aspx.cs" Inherits="Status_CastPerformance"
	Title="出演者別稼動集計" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="出演者別稼動集計"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 800px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px" AutoPostBack="True"
								OnSelectedIndexChanged="lstSiteCd_SelectedIndexChanged">
							</asp:DropDownList>
						</td>
					</tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            プロダクション
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstProductionSeq" runat="server" DataSourceID="dsProduction"
                                DataTextField="PRODUCTION_NM" DataValueField="PRODUCTION_SEQ" Width="180px" OnSelectedIndexChanged="lstProductionSeq_SelectedIndexChanged"
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
					</tr>
					<tr>
                        <td class="tdHeaderStyle">
                            担当
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstManager" runat="server" DataSourceID="dsManager" DataTextField="MANAGER_NM"
                                DataValueField="MANAGER_SEQ" Width="180px">
                            </asp:DropDownList>
                        </td>
                    </tr>
					<tr>
						<td class="tdHeaderStyle">
							集計開始日
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstFromYYYY" runat="server" Width="60px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstFromMM" runat="server" Width="40px">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstFromDD" runat="server" Width="40px">
							</asp:DropDownList>日
							<asp:DropDownList ID="lstFromHH" runat="server" Width="40px">
							</asp:DropDownList>時
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							集計終了日
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstToYYYY" runat="server" Width="60px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstToMM" runat="server" Width="40px">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstToDD" runat="server" Width="40px">
							</asp:DropDownList>日
							<asp:DropDownList ID="lstToHH" runat="server" Width="40px">
							</asp:DropDownList>時
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							登録日
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstRegistFromYYYY" runat="server" Width="60px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstRegistFromMM" runat="server" Width="40px">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstRegistFromDD" runat="server" Width="40px">
							</asp:DropDownList>日 &nbsp;〜&nbsp;
							<asp:DropDownList ID="lstRegistToYYYY" runat="server" Width="60px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstRegistToMM" runat="server" Width="40px">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstRegistToDD" runat="server" Width="40px">
							</asp:DropDownList>日
							<asp:CustomValidator runat="server" ID="vdcFrom" ErrorMessage="登録日が不正です。" OnServerValidate="vdcRegistDay_ServerValidate" ValidationGroup="Seek"></asp:CustomValidator>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							<%= DisplayWordUtil.Replace("出演者ID(前方一致検索)") %>
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtLoginId" runat="server" Width="80px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							広告グループ
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstAdGroup" runat="server" DataSourceID="dsAdGroup" DataTextField="AD_GROUP_NM" DataValueField="AD_GROUP_CD" Width="232px" AutoPostBack="true"
								OnSelectedIndexChanged="lstAdGroup_SelectedIndexChanged">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							広告コード
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstAd" runat="server" DataSourceID="dsAdGroupMember" DataTextField="AD_NM" DataValueField="AD_CD" Width="232px" AutoPostBack="True"
								OnSelectedIndexChanged="lstAd_SelectedIndexChanged">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							<asp:Label ID="Label9" runat="server" Text="表示明細"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:RadioButton ID="rdoTel" runat="server" Checked="true" GroupName="DisplayType" AutoPostBack="true" Text="電話利用明細">
							</asp:RadioButton>
							<asp:RadioButton ID="rdoManTel" runat="server" GroupName="DisplayType" AutoPostBack="true" Text="男性待機・電話利用明細">
							</asp:RadioButton>
							<asp:RadioButton ID="rdoWeb" runat="server" GroupName="DisplayType" AutoPostBack="true" Text="WEB利用明細">
							</asp:RadioButton>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							<asp:Label ID="Label14" runat="server" Text="表示項目"></asp:Label>
						</td>
						<td class="tdDataStyle">
							上から順番に
							<asp:CheckBox ID="chkCount" runat="server" Text="分数/件数" Checked="true">
							</asp:CheckBox>
							<asp:CheckBox ID="chkPoint" runat="server" Text="稼動Pt" Checked="true">
							</asp:CheckBox>
							<asp:CheckBox ID="chkPayPoint" runat="server" Text="支払Pt" Checked="true">
							</asp:CheckBox>
							を表示する。
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							Paging Off
						</td>
						<td class="tdDataStyle">
							<asp:CheckBox ID="chkPagingOff" runat="server" />
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Seek" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
				<asp:Button runat="server" ID="btnCsv" Text="ＣＳＶ出力" CssClass="seekbutton" OnClick="btnCSV_Click" ValidationGroup="Seek" />
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[稼動集計]</legend>
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="Auto" Height="430px" Width="100%">
					<asp:GridView ID="grdPerformace" runat="server" AllowPaging="true" AllowSorting="True" AutoGenerateColumns="False" SkinID="GridViewColor" EnableViewState="false"
						Font-Size="X-Small" DataSourceID="dsTimeOperation" ShowFooter="True" OnSorting="grdPerformace_Sorting" OnDataBound="grdPerformace_DataBound" OnLoad="grdPerformace_Load">
						<PagerSettings Mode="NumericFirstLast" />
						<Columns>
							<asp:TemplateField HeaderText="ID番号" FooterText="合計" SortExpression="LOGIN_ID">
								<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="60px" />
								<ItemTemplate>
									<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}",Eval("LOGIN_ID"),"CastPerformance.aspx") %>'
										Text='<%# Eval("LOGIN_ID") %>'>
									</asp:HyperLink>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Center" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="氏名&lt;br/&gt;ﾊﾝﾄﾞﾙ名" SortExpression="CAST_NM">
								<ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="80px" />
								<ItemTemplate>
									<asp:Label ID="Label2" runat="server" Text='<%# GetCastNm(Eval("CAST_NM"),ViCommPrograms.DefHandleName(Eval("HANDLE_NM"))) %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Center" />
								<FooterTemplate>
									<asp:Label ID="lblTotalCountNm" runat="server" Text="分数/件数" Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblTotalPointNm" runat="server" Text="稼動Pt" Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblTotalPayPointNm" runat="server" Text="支払Pt" Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="TV" SortExpression="PRV_TV_TALK_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label01" runat="server" Text='<%# Eval("PRV_TV_TALK_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label21" runat="server" Text='<%# Eval("PRV_TV_TALK_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label02" runat="server" Text='<%# Eval("PRV_TV_TALK_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblPrvTvTalkMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label22" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblPrvTvTalkPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label64" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblPrvTvTalkPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="TVﾁｬｯﾄ" SortExpression="PUB_TV_TALK_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label03" runat="server" Text='<%# Eval("PUB_TV_TALK_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label25" runat="server" Text='<%# Eval("PUB_TV_TALK_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label04" runat="server" Text='<%# Eval("PUB_TV_TALK_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblPubTvTalkMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label23" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblPubTvTalkPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label65" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblPubTvTalkPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="音声" SortExpression="PRV_VOICE_TALK_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label05" runat="server" Text='<%# Eval("PRV_VOICE_TALK_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label26" runat="server" Text='<%# Eval("PRV_VOICE_TALK_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label06" runat="server" Text='<%# Eval("PRV_VOICE_TALK_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblPrvVoiceTalkMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label24" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblPrvVoiceTalkPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label66" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblPrvVoiceTalkPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="音声ﾁｬｯﾄ" SortExpression="PUB_VOICE_TALK_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label05" runat="server" Text='<%# Eval("PUB_VOICE_TALK_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label26" runat="server" Text='<%# Eval("PUB_VOICE_TALK_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label06" runat="server" Text='<%# Eval("PUB_VOICE_TALK_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblPubVoiceTalkMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblPubVoiceTalkPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label67" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblPubVoiceTalkPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="会話ﾓﾆﾀ" SortExpression="VIEW_TALK_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label07" runat="server" Text='<%# Eval("VIEW_TALK_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label27" runat="server" Text='<%# Eval("VIEW_TALK_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label08" runat="server" Text='<%# Eval("VIEW_TALK_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblViewTalkMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label37" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblViewTalkPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label68" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblViewTalkPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="音声ﾓﾆﾀ" SortExpression="WIRETAP_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label09" runat="server" Text='<%# Eval("WIRETAP_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("WIRETAP_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("WIRETAP_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblWiretapMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label38" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblWiretapPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label69" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblWiretapPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="部屋ﾓﾆﾀ" SortExpression="VIEW_BROADCAST_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label09" runat="server" Text='<%# Eval("VIEW_BROADCAST_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("VIEW_BROADCAST_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("VIEW_BROADCAST_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblViewBroadcastMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label39" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblViewBroadcastPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label70" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblViewBroadcastPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾗｲﾌﾞ視聴" SortExpression="LIVE_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label09" runat="server" Text='<%# Eval("LIVE_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("LIVE_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("LIVE_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblLiveMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label40" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblLivePoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label71" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblLivePayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="PF再生" SortExpression="PLAY_PROFILE_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label09" runat="server" Text='<%# Eval("PLAY_PROFILE_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("PLAY_PROFILE_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("PLAY_PROFILE_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblPlayProfileMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label41" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblPlayProfilePoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label72" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblPlayProfilePayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="PF録音" SortExpression="REC_PROFILE_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label09" runat="server" Text='<%# Eval("REC_PROFILE_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("REC_PROFILE_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("REC_PROFILE_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblRecProfileMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label42" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblRecProfilePoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label73" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblRecProfilePayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="GP会話" SortExpression="GPF_TALK_VOICE_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label09" runat="server" Text='<%# Eval("GPF_TALK_VOICE_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("GPF_TALK_VOICE_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("GPF_TALK_VOICE_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblGpfTalkVoiceMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label43" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblGpfTalkVoicePoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label74" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblGpfTalkVoicePayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾌﾟﾗｲﾍﾞｰﾄ&lt;br/&gt;伝言再生" SortExpression="PLAY_PV_MSG_COUNT">
								<ItemTemplate>
									<asp:Label ID="Label09" runat="server" Text='<%# Eval("PLAY_PV_MSG_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label28" runat="server" Text='　' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='　' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblPlayPvMsgMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label44" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblPlayPvMsgPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label75" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblPlayPvMsgPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="男性待機&lt;br/&gt;TV" SortExpression="CAST_PRV_TV_TALK_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label09" runat="server" Text='<%# Eval("CAST_PRV_TV_TALK_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("CAST_PRV_TV_TALK_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("CAST_PRV_TV_TALK_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblCastPrvTvTalkMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label45" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblCastPrvTvTalkPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label76" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblCastPrvTvTalkPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="男性待機&lt;br/&gt;TVﾁｬｯﾄ" SortExpression="CAST_PUB_TV_TALK_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label09" runat="server" Text='<%# Eval("CAST_PUB_TV_TALK_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("CAST_PUB_TV_TALK_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("CAST_PUB_TV_TALK_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblCastPubTvTalkMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label46" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblCastPubTvTalkPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label77" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblCastPubTvTalkPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="男性待機&lt;br/&gt;音声" SortExpression="CAST_PRV_VOICE_TALK_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label09" runat="server" Text='<%# Eval("CAST_PRV_VOICE_TALK_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("CAST_PRV_VOICE_TALK_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("CAST_PRV_VOICE_TALK_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblCastPrvVoiceTalkMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label47" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblCastPrvVoiceTalkPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label78" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblCastPrvVoiceTalkPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="男性待機&lt;br/&gt;音声ﾁｬｯﾄ" SortExpression="CAST_PUB_VOICE_TALK_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label09" runat="server" Text='<%# Eval("CAST_PUB_VOICE_TALK_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("CAST_PUB_VOICE_TALK_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("CAST_PUB_VOICE_TALK_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblCastPubVoiceTalkMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label48" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblCastPubVoiceTalkPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label79" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblCastPubVoiceTalkPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="電話計" SortExpression="TALK_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label13" runat="server" Text='<%# Eval("TALK_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label30" runat="server" Text='<%# Eval("TALK_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label14" runat="server" Text='<%# Eval("TALK_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" BackColor="Beige" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" BackColor="Beige" />
								<FooterTemplate>
									<asp:Label ID="lblTalkMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label49" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblTalkPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label81" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblTalkPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾒｰﾙ受信" SortExpression="USER_MAIL_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label15" runat="server" Text='<%# Eval("USER_MAIL_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label31" runat="server" Text='<%# Eval("USER_MAIL_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label80" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label16" runat="server" Text='<%# Eval("USER_MAIL_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblUserMailCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label50" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblUserMailPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label82" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblUserMailPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="写真添付&lt;br/&gt;ﾒｰﾙ受信" SortExpression="USER_MAIL_PIC_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label15" runat="server" Text='<%# Eval("USER_MAIL_PIC_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label31" runat="server" Text='<%# Eval("USER_MAIL_PIC_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label83" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label16" runat="server" Text='<%# Eval("USER_MAIL_PIC_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblUserMailPicCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label51" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblUserMailPicPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblUserMailPicPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="動画添付&lt;br/&gt;ﾒｰﾙ受信" SortExpression="USER_MAIL_MOVIE_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label15" runat="server" Text='<%# Eval("USER_MAIL_MOVIE_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label31" runat="server" Text='<%# Eval("USER_MAIL_MOVIE_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label85" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label16" runat="server" Text='<%# Eval("USER_MAIL_MOVIE_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblUserMailMovieCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label52" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblUserMailMoviePoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label86" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblUserMailMoviePayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾒｰﾙ添付&lt;br&gt;写真閲覧" SortExpression="OPEN_PIC_MAIL_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label17" runat="server" Text='<%# Eval("OPEN_PIC_MAIL_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label32" runat="server" Text='<%# Eval("OPEN_PIC_MAIL_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label18" runat="server" Text='<%# Eval("OPEN_PIC_MAIL_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblOpenPicMailCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label53" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblOpenPicMailPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label87" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblOpenPicMailPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾒｰﾙ添付&lt;br&gt;動画閲覧" SortExpression="OPEN_MOVIE_MAIL_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label19" runat="server" Text='<%# Eval("OPEN_MOVIE_MAIL_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label33" runat="server" Text='<%# Eval("OPEN_MOVIE_MAIL_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label20" runat="server" Text='<%# Eval("OPEN_MOVIE_MAIL_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblOpenMovieMailCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label54" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblOpenMovieMailPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label88" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblOpenMovieMailPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="掲示板&lt;br&gt;写真閲覧" SortExpression="OPEN_PIC_BBS_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label17" runat="server" Text='<%# Eval("OPEN_PIC_BBS_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label32" runat="server" Text='<%# Eval("OPEN_PIC_BBS_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label18" runat="server" Text='<%# Eval("OPEN_PIC_BBS_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblOpenPicBbsCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label55" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblOpenPicBbsPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label89" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblOpenPicBbsPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="掲示板&lt;br&gt;動画閲覧" SortExpression="OPEN_MOVIE_BBS_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label19" runat="server" Text='<%# Eval("OPEN_MOVIE_BBS_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label33" runat="server" Text='<%# Eval("OPEN_MOVIE_BBS_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label20" runat="server" Text='<%# Eval("OPEN_MOVIE_BBS_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblOpenMovieBbsCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label56" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblOpenMovieBbsPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label90" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblOpenMovieBbsPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾌﾟﾗｲﾍﾞｰﾄ&lt;br&gt;伝言受信" SortExpression="PRV_MESSAGE_RX_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label19" runat="server" Text='<%# Eval("PRV_MESSAGE_RX_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label33" runat="server" Text='<%# Eval("PRV_MESSAGE_RX_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label20" runat="server" Text='<%# Eval("PRV_MESSAGE_RX_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblPrvMessageRxCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label57" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblPrvMessageRxPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label91" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblPrvMessageRxPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾌﾟﾗｲﾍﾞｰﾄ&lt;br&gt;PF受信" SortExpression="PRV_PROFILE_RX_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label19" runat="server" Text='<%# Eval("PRV_PROFILE_RX_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label33" runat="server" Text='<%# Eval("PRV_PROFILE_RX_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label20" runat="server" Text='<%# Eval("PRV_PROFILE_RX_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblPrvProfileRxCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label58" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblPrvProfileRxPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label92" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblPrvProfileRxPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾀﾞｳﾝﾛｰﾄﾞ<br>ﾑｰﾋﾞｰ" SortExpression="DOWNLOAD_MOVIE_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("DOWNLOAD_MOVIE_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("DOWNLOAD_MOVIE_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label11" runat="server" Text='<%# Eval("DOWNLOAD_MOVIE_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblDownloadMovieCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label59" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblDownloadMoviePoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label93" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblDownloadMoviePayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="定時<br>ｺｰﾙ" SortExpression="TIME_CALL_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("TIME_CALL_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("TIME_CALL_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label12" runat="server" Text='<%# Eval("TIME_CALL_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblTimeCallCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label60" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblTimeCallPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label94" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblTimeCallPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾀﾞｳﾝﾛｰﾄﾞ<br>着ﾎﾞｲｽ" SortExpression="DOWNLOAD_VOICE_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("DOWNLOAD_VOICE_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("DOWNLOAD_VOICE_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label11" runat="server" Text='<%# Eval("DOWNLOAD_VOICE_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblDownloadVoiceCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label61" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblDownloadVoicePoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label95" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblDownloadVoicePayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ｿｰｼｬﾙ<br>ｹﾞｰﾑ" SortExpression="SOCIAL_GAME_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("SOCIAL_GAME_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("SOCIAL_GAME_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label11" runat="server" Text='<%# Eval("SOCIAL_GAME_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblSocialGameCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label99" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblSocialGamePoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label98" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblSocialGamePayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="野球拳<br />FC会費" SortExpression="YAKYUKEN_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("YAKYUKEN_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("YAKYUKEN_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label11" runat="server" Text='<%# Eval("YAKYUKEN_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblYakyukenCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="lblYakyukenBrCount" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblYakyukenPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="lblYakyukenBrPoint" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblYakyukenPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾌﾟﾚｾﾞﾝﾄﾒｰﾙ" SortExpression="PRESENT_MAIL_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("PRESENT_MAIL_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("PRESENT_MAIL_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label11" runat="server" Text='<%# Eval("PRESENT_MAIL_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" />
								<FooterTemplate>
									<asp:Label ID="lblPresentMailCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="lblPresentMailBrCount" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblPresentMailPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="lblPresentMailBrPoint" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblPresentMailPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="WEB計" SortExpression="WEB_TOTAL_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label13" runat="server" Text='<%# Eval("WEB_TOTAL_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label34" runat="server" Text='<%# Eval("WEB_TOTAL_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label14" runat="server" Text='<%# Eval("WEB_TOTAL_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" BackColor="Beige" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" BackColor="Beige" />
								<FooterTemplate>
									<asp:Label ID="lblWebTotalCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label62" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblWebTotalPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label96" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblWebTotalPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="合計" SortExpression="TOTAL_PAY_POINT">
								<ItemTemplate>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label35" runat="server" Text='<%#string.Format("{0}",CalcSum(Eval("TALK_POINT"),Eval("WEB_TOTAL_POINT"))) %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label16" runat="server" Text='<%#string.Format("{0}",CalcSum(Eval("TALK_PAY_POINT"),Eval("WEB_TOTAL_PAY_POINT"))) %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" BackColor="BlanchedAlmond" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="52px" BackColor="BlanchedAlmond" />
								<FooterTemplate>
									<asp:Label ID="Label63" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblTotalPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label97" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblTotalPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
						</Columns>
						<FooterStyle ForeColor="Black" BackColor="LightYellow" />
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
					<br />
					<asp:Panel runat="server" ID="pnlCount">
						<a class="reccount">Record Count
							<asp:Label ID="lblTotalRecordCount" runat="server" Text="0"></asp:Label>
						</a>
						<br />
						<a class="reccount">Current viewing page
							<%=grdPerformace.PageIndex + 1%>
							of
							<%=grdPerformace.PageCount%>
						</a>
					</asp:Panel>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsTimeOperation" runat="server" TypeName="TimeOperation" SelectMethod="GetList" OnSelected="dsTimeOperation_Selected" OnSelecting="dsTimeOperation_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pFromYYYY" Type="String" />
			<asp:Parameter Name="pFromMM" Type="String" />
			<asp:Parameter Name="pFromDD" Type="String" />
			<asp:Parameter Name="pFromHH" Type="String" />
			<asp:Parameter Name="pToYYYY" Type="String" />
			<asp:Parameter Name="pToMM" Type="String" />
			<asp:Parameter Name="pToDD" Type="String" />
			<asp:Parameter Name="pToHH" Type="String" />
			<asp:Parameter Name="pFromRegistDay" Type="String" />
			<asp:Parameter Name="pToRegistDay" Type="String" />
			<asp:Parameter Name="pLoginId" Type="String" />
			<asp:Parameter Name="pAdGroupCd" Type="String" />
			<asp:Parameter Name="pAdCd" Type="String" />
			<asp:Parameter Name="pProductionSeq" Type="String" />
			<asp:Parameter Name="pManagerSeq" Type="String" />
			<asp:Parameter Name="pOutputHandleNmFlag" Type="Boolean" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsProduction" runat="server" SelectMethod="GetList" TypeName="Production"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsManager" runat="server" SelectMethod="GetProductionList" TypeName="Manager" OnSelecting="dsManager_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pProductionSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsAdGroup" runat="server" SelectMethod="GetList" TypeName="AdGroup" OnSelecting="dsAdGroup_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsAdGroupMember" runat="server" SelectMethod="GetList" TypeName="SiteAdGroup" OnSelecting="dsAdGroupMember_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pAdGroupCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
