﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master" CodeFile="PaymentHistorySummary.aspx.cs" Inherits="StatusManager_PaymentHistorySummary"
	Title="支払履歴一覧" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="支払履歴一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 740px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							支払日
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtReportDayFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RequiredFieldValidator ID="vdrReportDayFrom" runat="server" ErrorMessage="支払日Fromを入力して下さい。" ControlToValidate="txtReportDayFrom" ValidationGroup="Key"
								Display="Dynamic">*</asp:RequiredFieldValidator>
							<asp:RangeValidator ID="vdeReportDayFrom" runat="server" ErrorMessage="支払日Fromを正しく入力して下さい。" ControlToValidate="txtReportDayFrom" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							～&nbsp;
							<asp:TextBox ID="txtReportDayTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RangeValidator ID="vdeReportDayTo" runat="server" ErrorMessage="支払日Toを正しく入力して下さい。" ControlToValidate="txtReportDayTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
								Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
						</td>
						<td class="tdHeaderStyle">
							支払方法
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstPaymentMethod" runat="server" Width="180px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							出演者ID(前方一致検索)
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtLoginId" runat="server" Width="70px"></asp:TextBox>
						</td>
						<td class="tdHeaderStyle">
							精算種別
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstPaymentType" runat="server" DataSourceID="dsPaymentType" DataTextField="CODE_NM" DataValueField="CODE" Width="78px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							プロダクション
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstProductionSeq" runat="server" DataSourceID="dsProduction" DataTextField="PRODUCTION_NM" DataValueField="PRODUCTION_SEQ" Width="170px"
								OnSelectedIndexChanged="lstProductionSeq_SelectedIndexChanged" AutoPostBack="True">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle2">
							担当
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstManager" runat="server" DataSourceID="dsManager" DataTextField="MANAGER_NM" DataValueField="MANAGER_SEQ" Width="180px">
							</asp:DropDownList>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
				<asp:Button runat="server" ID="btnCsv" Text="CSV出力" CssClass="seekbutton" OnClick="btnCsv_Click" />
				<asp:Button runat="server" ID="btnSendMail" Text="ﾒｰﾙ再送" CssClass="seekbutton" OnClick="btnSendMail_Click" />
				<asp:Label ID="lblSendMailSuccess" runat="server" Text="ﾒｰﾙの送信が完了致しました。" ForeColor="red" Visible="false"></asp:Label>
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlSendMail">
			<fieldset>
				<legend>[送信メール選択]</legend>
				<table border="0" style="width: 740px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle2">
							ﾒｰﾙ種別
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstMailTemplateNo" runat="server" DataSourceID="dsCastMailMagazine" DataTextField="TEMPLATE_NM" DataValueField="MAIL_TEMPLATE_NO">
							</asp:DropDownList>
							<asp:RequiredFieldValidator ID="vdrMailTemplateNo" runat="server" ErrorMessage="ﾒｰﾙ種別" ControlToValidate="lstMailTemplateNo" ValidationGroup="SendMail"
								Display="Dynamic">*</asp:RequiredFieldValidator>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnSendMailCommit" Text="送信する" CssClass="seekbutton" OnClick="btnSendMailCommit_Click" ValidationGroup="SendMail" />
				<asp:Button runat="server" ID="btnSendMailCancel" Text="中止" CssClass="seekbutton" OnClick="btnSendMailCancel_Click" />
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[支払一覧履歴]</legend>
				<asp:Panel runat="server" ID="pnlPaymentCount">
					合計支払件数&nbsp;<%#GetTotalCount()%>件&nbsp;&nbsp; 合計支払額&nbsp;<%#GetTotalAmount()%>円
				</asp:Panel>
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="430px">
					<asp:GridView ID="grdPaymentHistory" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="" AllowSorting="True" SkinID="GridViewColor">
						<Columns>
							<asp:BoundField DataField="PAY_DAY" HeaderText="支払日時" DataFormatString="{0:yyyy/MM/dd HH:mm}" HtmlEncode="False">
								<ItemStyle HorizontalAlign="Center" />
								<ItemStyle Font-Size="Small" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="出演者ID">
								<ItemTemplate>
									<asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# string.Format("../Cast/CastView.aspx?loginid={0}",Eval("LOGIN_ID")) %>' Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="氏名">
								<ItemTemplate>
									<asp:Label ID="txtCastNm" runat="server" Text='<%# Eval("CAST_NM") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="PAYMENT_AMT" HeaderText="支払金額">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:BoundField DataField="PAYMENT_TYPE_NM" HeaderText="支払種別">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:BoundField DataField="PAYMENT_METHOD_NM" HeaderText="支払方法">
								<ItemStyle HorizontalAlign="Right" />
							</asp:BoundField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
				&nbsp;
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count of
						<%#GetRecCount() %>
					</a>
					<br />
					<a class="reccount">Current viewing page
						<%=grdPaymentHistory.PageIndex + 1%>
						of
						<%=grdPaymentHistory.PageCount%>
					</a>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsPaymentHistory" runat="server" SelectMethod="GetPageCollectionByLoginId" TypeName="PaymentHistory" SelectCountMethod="GetPageCountByLoginId"
		EnablePaging="True" OnSelecting="dsPaymentHistory_Selecting" OnSelected="dsPaymentHistory_Selected">
		<SelectParameters>
			<asp:Parameter Name="pLoginId" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
			<asp:Parameter Name="pPaymentType" Type="string" />
			<asp:Parameter Name="pProductionSeq" Type="string" />
			<asp:Parameter Name="pManagerSeq" Type="string" />
			<asp:Parameter Name="pPaymentMethod" Type="string" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsProduction" runat="server" SelectMethod="GetList" TypeName="Production"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsManager" runat="server" SelectMethod="GetProductionList" TypeName="Manager" OnSelecting="dsManager_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pProductionSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsPaymentType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="18" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastMailMagazine" runat="server" SelectMethod="GetListByTemplateType" TypeName="MailTemplate">
		<SelectParameters>
			<asp:Parameter DefaultValue="Z001" Name="pSiteCd" Type="String" />
			<asp:Parameter DefaultValue="24" Name="pMailTemplateType" Type="String" />
			<asp:Parameter DefaultValue="2" Name="pSexCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrReportDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdeReportDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdeReportDayTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrMailTemplateNo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskReportDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="True"
		TargetControlID="txtReportDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskReportDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="True"
		TargetControlID="txtReportDayTo">
	</ajaxToolkit:MaskedEditExtender>
</asp:Content>
