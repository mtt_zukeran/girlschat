﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者別精算集計(管理会社別)

--	Progaram ID		: CastManageCompanySettlement
--
--  Creation Date	: 2009.10.01
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.IO;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Status_CastManageCompanySettlement:System.Web.UI.Page {
	private int talkPayAmt;
	private int bonusAmt;
	private int webTotalPayAmt;
	private int friendIntroPayAmt;

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
			SetLastMonth();
			using (ManageCompany oManageCompany = new ManageCompany()) {
				if (oManageCompany.GetOne()) {
					txtCastMinimumPayment.Text = oManageCompany.castSelfMinimumPayment;
				}
			}

			this.grdPwCsv.Visible = true;

			if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["InvalidBankCastDispFlag"]).Equals(ViCommConst.FLAG_ON_STR)) {
				chkBankInfoInvalid.Checked = true;
			} else {
				chkBankInfoInvalid.Checked = false;
			}
			SetQuery();
			//		pnlFrom.Visible = false;
			grdCsv.PageSize = 26;
			grdPwCsv.PageSize = 26;
			SetDownloadCsv();
		}
	}

	private void InitPage() {
		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);

		int iCompanyMask = int.Parse(Session["CompanyMask"].ToString());

		grdPerformace.DataSourceID = "";
		pnlInfo.Visible = false;
		ClearField();
		DataBind();
		if (!IsPostBack) {
			SysPrograms.SetupDay(lstToYYYY,lstToMM,lstToDD,true);
			SysPrograms.SetupTime(null,lstToHH);
			SysPrograms.SetupDay(lstWaitPaymentYYYY,lstWaitPaymentMM,lstWaitPaymentDD,false);

			if (!iBridUtil.GetStringValue(Session["PRODUCTION_SEQ"]).Equals("")) {
				lstProductionSeq.DataSourceID = "";
				lstProductionSeq.SelectedValue = Session["PRODUCTION_SEQ"].ToString();
				lstProductionSeq.Enabled = false;
			} else {
				lstProductionSeq.Items.Insert(0,new ListItem("",""));
				lstProductionSeq.DataSourceID = "";
				lstProductionSeq.SelectedIndex = 0;
			}
		}
		btnCheckAll.Visible = false;
		btnSettle.Visible = false;
		btnCsv.Visible = false;
		lblCsv.Visible = false;

		chkWaitPaymentOnly.Checked = true;
		if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["InvalidBankCastDispFlag"]).Equals(ViCommConst.FLAG_ON_STR)) {
			chkBankInfoInvalid.Checked = true;
		} else {
			chkBankInfoInvalid.Checked = false;
		}

		bool bIsImpact = false;
		chkAuthWait.Checked = false;
		chkNormal.Checked = true;
		chkStop.Checked = false;
		chkHold.Checked = bIsImpact;
		chkResigned.Checked = bIsImpact;
		chkBan.Checked = false;
	}

	private void SetQuery() {
		string sMinPayment = iBridUtil.GetStringValue(Request.QueryString["minpayment"]);
		string sLoginId = iBridUtil.GetStringValue(Request.QueryString["loginid"]);
		string sToDate = iBridUtil.GetStringValue(Request.QueryString["to"]);
		string sInvalidBank = iBridUtil.GetStringValue(Request.QueryString["bank"]);

		if (!sMinPayment.Equals(string.Empty)) {
			txtCastMinimumPayment.Text = sMinPayment;
		}
		if (!sLoginId.Equals(string.Empty)) {
			txtLoginId.Text = sLoginId;
		}
		if (!sToDate.Equals(string.Empty)) {
			string[] sKeys = sToDate.Split('/');
			lstToYYYY.SelectedValue = sKeys[0];
			lstToMM.SelectedValue = sKeys[1];
			lstToDD.SelectedValue = sKeys[2];
			lstToHH.SelectedValue = sKeys[3];
		}
		if (!sInvalidBank.Equals(string.Empty)) {
			if (sInvalidBank.Equals(ViCommConst.FLAG_ON_STR)) {
				chkBankInfoInvalid.Checked = true;
			} else {
				chkBankInfoInvalid.Checked = false;
			}
		}
		if (!sMinPayment.Equals(string.Empty) && !sLoginId.Equals(string.Empty) && !sToDate.Equals(string.Empty)) {
			GetList();
		}
	}
	private void ClearField() {
		talkPayAmt = 0;
		webTotalPayAmt = 0;
		bonusAmt = 0;
		lblError.Visible = false;
	}

	private void SetLastMonth() {
		DateTime dtYesterDay = DateTime.Now.AddDays(-1);
		lstToYYYY.SelectedValue = dtYesterDay.ToString("yyyy");
		lstToMM.SelectedValue = dtYesterDay.ToString("MM");
		lstToDD.SelectedValue = dtYesterDay.ToString("dd");
		lstWaitPaymentYYYY.SelectedValue = dtYesterDay.ToString("yyyy");
		lstWaitPaymentMM.SelectedValue = dtYesterDay.ToString("MM");
		lstWaitPaymentDD.SelectedValue = dtYesterDay.ToString("dd");

		//運営会社設定よりFromの日付を設定 
		using (ManageCompany oManageCompany = new ManageCompany()) {
			if (oManageCompany.GetOne()) {
				if (oManageCompany.notPayExpireMonth == 0) {
					lblFromYYYY.Text = "1900";
					lblFromMM.Text = "01";
					lblFromDD.Text = "01";
					lblFromHH.Text = "00";
				} else {
					DateTime dtTo = DateTime.Parse(string.Format("{0}/{1}/{2}",lstToYYYY.SelectedValue,lstToMM.SelectedValue,lstToDD.SelectedValue));
					DateTime dtFrom = DateTime.Now.AddMonths(-1 * (oManageCompany.notPayExpireMonth));
					lblFromYYYY.Text = dtFrom.ToString("yyyy");
					lblFromMM.Text = dtFrom.ToString("MM");
					lblFromDD.Text = "01";
					lblFromHH.Text = "00";
				}
			}
		}
	}

	private void SetDownloadCsv() {
		string sInstallDir = "";
		DataSet ds = new DataSet();
		DataTable dt = new DataTable();

		using (Sys oSys = new Sys()) {
			oSys.GetValue("INSTALL_DIR",out sInstallDir);
		}

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,string.Empty,ViCommConst.FILE_UPLOAD_PASSWORD)) {
			string[] files = Directory.GetFiles(string.Format("{0}\\Text\\",sInstallDir),"CastTimeOperationLog*.txt");

			dt.Columns.Add("DATE_TIME",Type.GetType("System.String"));
			dt.Columns.Add("FILE_PATH",Type.GetType("System.String"));

			for (int i = files.Length - 1;i >= 0;i--) {
				try {
					int iLength = files[i].Length;
					iLength -= 21;
					string sDateTime = files[i].Substring(iLength,12);
					DateTime dateTime = DateTime.ParseExact(sDateTime,"yyyyMMddHHmm",null);

					dt.Rows.Add(dateTime.ToString("yyyy/MM/dd HH:mm"),files[i]);
				} catch {
				}
			}
		}

		ds.Tables.Add(dt);
		grdCsv.DataSource = ds;
		grdCsv.DataBind();

		int iCompare = string.Compare(Session["AdminType"].ToString(),ViCommConst.RIGHT_SITE_OWNER);
		grdCsv.Enabled = (iCompare >= 0);

		ds.Clear();
		dt.Clear();

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,string.Empty,ViCommConst.FILE_UPLOAD_PASSWORD)) {
			string[] files = Directory.GetFiles(string.Format("{0}\\Text\\",sInstallDir),"CastPwildPaymentLog*.txt");

			for (int i = files.Length - 1;i >= 0;i--) {
				try {
					int iLength = files[i].Length;
					iLength -= 21;
					string sDateTime = files[i].Substring(iLength,12);
					DateTime dateTime = DateTime.ParseExact(sDateTime,"yyyyMMddHHmm",null);

					dt.Rows.Add(dateTime.ToString("yyyy/MM/dd HH:mm"),files[i]);
				} catch {
				}
			}
		}

		grdPwCsv.DataSource = ds;
		grdPwCsv.DataBind();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (IsValid) {
			GetList();
		}
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
		SetDownloadCsv();
	}

	protected void btnSettle_Click(object sender,EventArgs e) {
		if (IsValid == true) {
			string sCsvSeqNo,sUserSeqNo,sResult,sNgLoginId;
			SetSettle(ViCommConst.FLAG_OFF,out sCsvSeqNo,out sUserSeqNo,out sResult,out sNgLoginId);
			if (!sResult.Equals("1")) {
				lblError.Visible = false;
				SendMail(sUserSeqNo,sCsvSeqNo);
				ExportCSV(ViCommConst.FLAG_OFF,sCsvSeqNo);
			} else {
				lblError.Visible = true;
				lblError.Text = "<br/><br/>会員ID " + sNgLoginId + "が会話中のデータが精算範囲内にある為、精算できませんでした。<br/><br/>しばらくお待ち頂いてから再度実行して下さい。";
			}
		}
	}

	protected void btnCsv_Click(object sender,EventArgs e) {
		if (IsValid == true) {
			string sCsvSeqNo,sUserSeqNo,sResult,sNgLoginId;
			SetSettle(ViCommConst.FLAG_ON,out sCsvSeqNo,out sUserSeqNo,out sResult,out sNgLoginId);
			if (!sResult.Equals("1")) {
				lblError.Visible = false;
				ExportCSV(ViCommConst.FLAG_ON,sCsvSeqNo);
			} else {
				lblError.Visible = true;
				lblError.Text = "<br/><br/>会員ID " + sNgLoginId + "が会話中のデータが精算範囲内にある為、精算できませんでした。<br/><br/>しばらくお待ち頂いてから再度実行して下さい。";
			}
		}
	}

	protected void btnCheckAll_Click(object sender,EventArgs e) {
		bool bCheck = true;
		if (btnCheckAll.Text.IndexOf("ON") > 0) {
			bCheck = true;
			btnCheckAll.Text = "全ﾁｪｯｸOFF";
		} else {
			bCheck = false;
			btnCheckAll.Text = "全ﾁｪｯｸON";
		}
		foreach (GridViewRow gvr in grdPerformace.Rows) {
			CheckBox chk = (CheckBox)gvr.FindControl("chkSettle");
			chk.Checked = bCheck;
		}
	}

	private void SetSettle(int pNoUpdatePaymentFlag,out string pCsvSeqNo,out string pUserSeqNo,out string pResult,out string pNgLoginId) {
		pCsvSeqNo = "";
		pUserSeqNo = "";
		pResult = "0";
		pNgLoginId = string.Empty;

		if (grdPerformace.Rows.Count == 0) {
			return;
		}

		string[] sLoginIdWk = new string[grdPerformace.Rows.Count];
		int i = 0;
		foreach (GridViewRow gvr in grdPerformace.Rows) {
			CheckBox chk = (CheckBox)gvr.FindControl("chkSettle");
			HyperLink lnkLoginId = (HyperLink)gvr.FindControl("lnkLoginId");
			if (chk.Checked) {
				sLoginIdWk[i] = lnkLoginId.Text;
				i++;
			}
		}
		if (i == 0) {
			return;
		}
		string[] sLoginId = new string[i];
		for (int j = 0;j < sLoginId.Length;j++) {
			sLoginId[j] = sLoginIdWk[j];
		}
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_MANAGE_COMPANY_SETTLE_ID");
			db.ProcedureInArrayParm("PLOGIN_ID",DbSession.DbType.VARCHAR2,sLoginId.Length,sLoginId);
			db.ProcedureInParm("PLOGIN_ID_COUNT",DbSession.DbType.NUMBER,sLoginId.Length);
			db.ProcedureInParm("PREPORT_DAY_TIME_FROM",DbSession.DbType.VARCHAR2,CreateFromDateTime());
			db.ProcedureInParm("PREPORT_DAY_TIME_TO",DbSession.DbType.VARCHAR2,CreateToDateTime());
			db.ProcedureInParm("PREC_CAST_MINIMUM_PAYMENT",DbSession.DbType.NUMBER,int.Parse(txtCastMinimumPayment.Text));
			db.ProcedureInParm("PPAYMENT_TYPE",DbSession.DbType.VARCHAR2,ViCommConst.PaymentType.SELF_PAYMENT);
			db.ProcedureOutParm("PCSV_SEQ_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PUSER_SEQ_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PNG_LOGIN_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ProcedureInParm("PNO_UPDATE_PAYMENT_FLAG",DbSession.DbType.NUMBER,pNoUpdatePaymentFlag);
			db.ExecuteProcedure();
			pCsvSeqNo = db.GetStringValue("PCSV_SEQ_NO");
			pUserSeqNo = db.GetStringValue("PUSER_SEQ_NO");
			pResult = db.GetStringValue("PRESULT");
			pNgLoginId = db.GetStringValue("PNG_LOGIN_ID");
		}
	}

	private string CreateFromDateTime() {
		string yyyy = lblFromYYYY.Text;
		string mm = lblFromMM.Text;
		string dd = lblFromDD.Text;
		string hh = lblFromHH.Text;
		return yyyy + mm + dd + hh;
	}

	private string CreateToDateTime() {
		string yyyy = lstToYYYY.SelectedValue;
		string mm = lstToMM.SelectedValue;
		string dd = lstToDD.SelectedValue;
		string hh = lstToHH.SelectedValue;
		return yyyy + mm + dd + hh;
	}

	private void SendMail(string pUserSeqNo,string pCsvSeqNo) {
		if (!lstMailTemplateNo.SelectedValue.Equals("") && chkSendMail.Checked) {

			using (Site oSite = new Site())
			using (TextWork oTextWork = new TextWork()) {
				oSite.GetOne(ViCommConst.CAST_SITE_CD);
				DataSet ds = oTextWork.GetList(pUserSeqNo);
				DataRow dr = null;

				string[] sUserSeq = new string[ds.Tables[0].Rows.Count];
				string[] sTotalPoint = new string[ds.Tables[0].Rows.Count];
				string[] sReportDayTimeTo = new string[ds.Tables[0].Rows.Count];
				string[] sCsvSeqNo = new string[ds.Tables[0].Rows.Count];

				for (int i = 0;i < ds.Tables[0].Rows.Count;i++) {
					dr = ds.Tables[0].Rows[i];
					string[] sTextDtl = dr["TEXT_DTL"].ToString().Split(',');

					int iTotalPoint = 0;
					if (!int.TryParse(sTextDtl[1],out iTotalPoint)) {
						iTotalPoint = 0;
					}

					sUserSeq[i] = sTextDtl[0];
					sTotalPoint[i] = ((int)Math.Max(0,iTotalPoint - oSite.transferFee)).ToString();
					sReportDayTimeTo[i] = CreateToDateTime();
					sCsvSeqNo[i] = pCsvSeqNo;
				}

				string[] sDoc;
				int iDocCount;

				SysPrograms.SeparateHtml("",ViCommConst.MAX_ORG_MAIL_BLOCKS,out sDoc,out iDocCount);

				using (DbSession db = new DbSession()) {
					db.PrepareProcedure("TX_PAYOUT_TO_CAST_MAIL");
					db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,ViCommConst.CAST_SITE_CD);
					db.ProcedureInParm("PMAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,lstMailTemplateNo.SelectedValue);
					db.ProcedureInArrayParm("PCAST_USER_SEQ",DbSession.DbType.VARCHAR2,ds.Tables[0].Rows.Count,sUserSeq);
					db.ProcedureInParm("PCAST_USER_COUNT",DbSession.DbType.NUMBER,ds.Tables[0].Rows.Count);
					db.ProcedureInArrayParm("PGENERAL1",DbSession.DbType.VARCHAR2,ds.Tables[0].Rows.Count,sTotalPoint);
					db.ProcedureInArrayParm("PREPORT_DAY_TIME_TO",DbSession.DbType.VARCHAR2,ds.Tables[0].Rows.Count,sReportDayTimeTo);
					db.ProcedureInArrayParm("PCSV_SEQ_NO",DbSession.DbType.VARCHAR2,ds.Tables[0].Rows.Count,sCsvSeqNo);
					db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
					db.ExecuteProcedure();
				}
			}
		}
	}

	private void ExportCSV(int pNoUpdatePaymentFlag,string pSeqNo) {
		string fileName = ViCommConst.CSV_FILE_NM_CAST_SETTLEMENT;
		string sSaveCsvFileNm = string.Empty;
		string sInstallDir;
		bool bStaffIdFlag = false,bCastInputBankCd = false,bDispAddress = false;
		string sTransferFee = string.Empty;

		using (Site oSite = new Site()) {
			oSite.GetValue(ViCommConst.CAST_SITE_CD,"TRANSFER_FEE",ref sTransferFee);
		}
		using (Sys oSys = new Sys()) {
			oSys.GetValue("INSTALL_DIR",out sInstallDir);
		}
		if (pNoUpdatePaymentFlag == 0) {
			sSaveCsvFileNm = "CastTimeOperationLog";
		} else {
			sSaveCsvFileNm = "CastPwildPaymentLog";
		}
		string sSaveFileNm = string.Format("{0}\\Text\\{2}{1}.txt",sInstallDir,DateTime.Now.ToString("yyyyMMddHHmmssfff"),sSaveCsvFileNm);

		Response.AddHeader("Content-Disposition","attachment;filename=" + fileName);
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		// Available Serive?
		using (ManageCompany oCompany = new ManageCompany()) {
			bStaffIdFlag = oCompany.IsAvailableService(ViCommConst.RELEASE_SUPPORT_STAFF_ID);
			bCastInputBankCd = oCompany.IsAvailableService(ViCommConst.RELEASE_CAST_INPUT_BANK_CD,2);
		}

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,string.Empty,ViCommConst.FILE_UPLOAD_PASSWORD)) {
			using (StreamWriter writer = new StreamWriter(sSaveFileNm,false,System.Text.Encoding.GetEncoding("Shift_JIS"))) {
				string sDtl = "";

				if (pNoUpdatePaymentFlag == 0) {
					sDtl += "ログインID,";
					sDtl += "氏名,";
					if (bStaffIdFlag) {
						sDtl += "事業者コード,";
					}
					sDtl += "金額合計,";
					if (!sTransferFee.Equals("0")) {
						sDtl += "金額-手数料,";
					}
					sDtl += "銀行名,";
					if (bCastInputBankCd) {
						sDtl += "金融機関ｺｰﾄﾞ,";
					}
					sDtl += "支店名,";
					sDtl += "支店名カナ,";
					if (bCastInputBankCd) {
						sDtl += "支店ｺｰﾄﾞ,";
					}
					sDtl += "口座タイプ,";
					sDtl += "口座番号,";
					sDtl += "口座名義,";
					if (bDispAddress) {
						sDtl += "郵便番号,都道府県,住所1,住所2,";
					}
					sDtl += "精算対象開始日時...終了日時,";
					sDtl += "前回精算処理実行日時";
				} else {
					sDtl += "申請日時,〆日,精算ポイント,出演者ID,氏名,口座名義,銀行名,支店名,支店名カナ,口座種類,口座番号";
				}
				writer.WriteLine(sDtl);
				sDtl += "\r\n";

				Response.BinaryWrite(encoding.GetBytes(sDtl));
				sDtl = "";

				using (TextWork oTextWork = new TextWork()) {
					DataSet ds = oTextWork.GetList(pSeqNo);
					foreach (DataRow dr in ds.Tables[0].Rows) {
						sDtl += dr["TEXT_DTL"].ToString();
						writer.WriteLine(sDtl);
						sDtl += "\r\n";
						Response.BinaryWrite(encoding.GetBytes(sDtl));
						sDtl = "";
					}
				}
				writer.Flush();
				writer.Close();
			}
		}
		Response.End();
	}

	protected void vdrToDate_ServerValidate(object source,ServerValidateEventArgs args) {
		string sToDate = lstToYYYY.SelectedValue + lstToMM.SelectedValue + lstToDD.SelectedValue + lstToHH.SelectedValue;
		string sNowDate = DateTime.Now.ToString("yyyyMMddHH");

		if (int.Parse(sToDate) >= int.Parse(sNowDate)) {
			args.IsValid = false;
		} else {
			args.IsValid = true;
		}
	}

	protected void lnkCsvDownload_Command(object sender,CommandEventArgs e) {
		string sFilePath = e.CommandArgument.ToString();
		string fileName = sFilePath.Substring(sFilePath.Length - 41,32) + ".csv";

		Response.AddHeader("Content-Disposition","attachment;filename=" + fileName);
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		System.IO.StreamReader sr = new System.IO.StreamReader(sFilePath,System.Text.Encoding.GetEncoding("Shift-JIS"));

		string sCsv = sr.ReadToEnd();
		sr.Close();

		Response.BinaryWrite(encoding.GetBytes(sCsv));
		Response.End();
	}
	protected void grd_PageIndexChanging(object sender,GridViewPageEventArgs e) {
		GridView grd = sender as GridView;
		if (grd == null) {
			return;
		}
		grd.PageIndex = e.NewPageIndex;
		SetDownloadCsv();
	}

	private void GetList() {
		ClearField();
		grdPerformace.DataSourceID = "dsTimeOperation";
		grdPerformace.DataBind();
		if (grdPerformace.Rows.Count > 0) {
			btnCheckAll.Text = "全ﾁｪｯｸOFF";
			btnCheckAll.Visible = true;
			btnSettle.Visible = true;
			this.btnCsv.Visible = true;
			this.lblCsv.Visible = true;
		} else {
			btnCheckAll.Visible = false;
			btnSettle.Visible = false;
			//pnlKey.Enabled = true;
			btnCsv.Visible = false;
			lblCsv.Visible = false;
		}
		SetDownloadCsv();
		pnlInfo.Visible = true;
	}

	protected string CalcSum(object pSum1,object pSum2,object pSum3) {
		int iSum = int.Parse(pSum1.ToString()) + int.Parse(pSum2.ToString()) + int.Parse(pSum3.ToString());
		return iSum.ToString();
	}

	protected void dsTimeOperation_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[9] = lstProductionSeq.SelectedValue;
		e.InputParameters[11] = chkBankInfoInvalid.Checked ? "1" : "0";		// BankInfo未設定者も含む
		e.InputParameters[12] = chkBankInfoInvalid.Checked ? "1" : "0";		// BankInfo不正者も含む

		if (chkNotNormal.Checked) {
			e.InputParameters[13] = "1";
		} else {
			e.InputParameters[13] = "0";
		}
		if (chkWaitPaymentOnly.Checked) {
			e.InputParameters[14] = ViCommConst.FLAG_ON_STR;
			e.InputParameters[15] = lstWaitPaymentYYYY.SelectedValue + "/" + lstWaitPaymentMM.SelectedValue + "/" + lstWaitPaymentDD.SelectedValue;
		} else {
			e.InputParameters[14] = string.Empty;
			e.InputParameters[15] = string.Empty;
		}

		int iStatus = 0;
		if (chkAuthWait.Checked) {
			iStatus += ViCommConst.MASK_WOMAN_AUTH_WAIT;
		}
		if (chkNormal.Checked) {
			iStatus += ViCommConst.MASK_WOMAN_NORMAL;
		}
		if (chkStop.Checked) {
			iStatus += ViCommConst.MASK_WOMAN_STOP;
		}
		if (chkHold.Checked) {
			iStatus += ViCommConst.MASK_WOMAN_HOLD;
		}
		if (chkResigned.Checked) {
			iStatus += ViCommConst.MASK_WOMAN_RESIGNED;
		}
		if (chkBan.Checked) {
			iStatus += ViCommConst.MASK_WOMAN_BAN;
		}
		e.InputParameters[16] = iStatus;
	}

	protected void grdPerformace_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			talkPayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TALK_PAY_AMT"));
			bonusAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"BONUS_AMT"));
			webTotalPayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"WEB_TOTAL_PAY_AMT"));
			friendIntroPayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"FRIEND_INTRO_PAY_AMT"));

		} else if (e.Row.RowType == DataControlRowType.Footer) {
			Label lblTalkPayAmt = (Label)e.Row.FindControl("lblTalkPayAmt");
			Label lblWebTotalPayAmt = (Label)e.Row.FindControl("lblWebTotalPayAmt");
			Label lblbonusAmt = (Label)e.Row.FindControl("lblbonusAmt");
			Label lblTotalPayAmt = (Label)e.Row.FindControl("lblTotalPayAmt");
			Label lblFriendIntroPayAmt = (Label)e.Row.FindControl("lblFriendIntroPayAmt");

			int iTotalPayAmt = talkPayAmt + webTotalPayAmt + bonusAmt + friendIntroPayAmt;
			lblTalkPayAmt.Text = talkPayAmt.ToString();
			lblWebTotalPayAmt.Text = webTotalPayAmt.ToString();
			lblbonusAmt.Text = bonusAmt.ToString();
			lblFriendIntroPayAmt.Text = friendIntroPayAmt.ToString();
			lblTotalPayAmt.Text = iTotalPayAmt.ToString();

		} else if (e.Row.RowType == DataControlRowType.Header) {
			ClearField();
		}
	}

	protected string GetWaitPaymentMark(object pWaitPaymentFlag) {
		if (pWaitPaymentFlag.ToString().Equals(ViCommConst.FLAG_ON_STR)) {
			return "★";
		} else {
			return string.Empty;
		}
	}
}
