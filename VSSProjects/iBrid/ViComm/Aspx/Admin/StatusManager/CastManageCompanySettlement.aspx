<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastManageCompanySettlement.aspx.cs" Inherits="Status_CastManageCompanySettlement"
	Title="出演者別支払集計" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="出演者別支払集計"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 900px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							最低精算金額
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtCastMinimumPayment" runat="server" MaxLength="5" Width="80px"></asp:TextBox>
							<asp:RequiredFieldValidator ID="vdrCastMinimumPayment" runat="server" ControlToValidate="txtCastMinimumPayment" ErrorMessage="最低精算金額を指定してください" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
						</td>
						<td class="tdHeaderStyle">
							精算対象者にメールを送る
						</td>
						<td class="tdDataStyle">
							<asp:CheckBox ID="chkSendMail" runat="server" Checked="true">
							</asp:CheckBox></td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							ﾒｰﾙ種別
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstMailTemplateNo" runat="server" Width="309px" DataSourceID="dsCastMailMagazine" DataTextField="TEMPLATE_NM" DataValueField="MAIL_TEMPLATE_NO">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle">
							集計終了日
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstToYYYY" runat="server" Width="60px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstToMM" runat="server" Width="40px">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstToDD" runat="server" Width="40px">
							</asp:DropDownList>日
							<asp:DropDownList ID="lstToHH" runat="server" Width="40px">
							</asp:DropDownList>時
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							<%= DisplayWordUtil.Replace("出演者ID(前方一致検索)")%>
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtLoginId" runat="server" Width="80px"></asp:TextBox>
						</td>
						<td class="tdHeaderStyle">
							<%= DisplayWordUtil.Replace("出演者状態")%>
						</td>
						<td class="tdDataStyle">
							<asp:CheckBox ID="chkNotNormal" runat="server" Checked="true" Visible="false"/>
							<asp:CheckBox ID="chkNormal" runat="server" Text="通常" />
							<asp:CheckBox ID="chkAuthWait" runat="server" Text="認証待ち" ForeColor="SeaGreen" Visible="false" />
							<asp:CheckBox ID="chkHold" runat="server" Text="保留" ForeColor="Blue" />
							<asp:CheckBox ID="chkResigned" runat="server" Text="退会" ForeColor="Maroon" />
							<asp:CheckBox ID="chkStop" runat="server" Text="禁止" ForeColor="Red" />
							<asp:CheckBox ID="chkBan" runat="server" Text="停止" ForeColor="Red" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							精算申請中のみ出力
						</td>
						<td class="tdDataStyle">
							<asp:CheckBox ID="chkWaitPaymentOnly" runat="server" Checked="true">
							</asp:CheckBox>
						</td>
						<td class="tdHeaderStyle2">
							<asp:Label ID="lblBankInfoInvalid" runat="server" Text="銀行情報未確定者も出力"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:CheckBox ID="chkBankInfoInvalid" runat="server" Checked="false" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							プロダクション
						</td>
						<td class="tdDataStyle" colspan="3">
							<asp:DropDownList ID="lstProductionSeq" runat="server" DataSourceID="dsProduction" DataTextField="PRODUCTION_NM" DataValueField="PRODUCTION_SEQ" Width="170px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							精算申請日
						</td>
						<td class="tdDataStyle" colspan="3">
							<asp:DropDownList ID="lstWaitPaymentYYYY" runat="server" Width="60px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstWaitPaymentMM" runat="server" Width="40px">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstWaitPaymentDD" runat="server" Width="40px">
							</asp:DropDownList>日 より以前 ※精算申請中のみにﾁｪｯｸが入っている時に有効
						</td>
					</tr>
					<%-- dummy start --%>
					<asp:Panel runat="server" ID="pnlFrom" Visible="false">
						<tr>
							<td class="tdHeaderStyle">
								集計開始日
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblFromYYYY" runat="server" Text="" Visible="false"></asp:Label>
								<asp:Label ID="lblFromMM" runat="server" Text="" Visible="false"></asp:Label>
								<asp:Label ID="lblFromDD" runat="server" Text="" Visible="false"></asp:Label>
								<asp:Label ID="lblFromHH" runat="server" Text="" Visible="false"></asp:Label>
							</td>
						</tr>
					</asp:Panel>
					<%-- dummy end --%>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Detail" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
				<asp:Button runat="server" ID="btnSettle" Text="精算" CssClass="seekbutton" OnClick="btnSettle_Click" ValidationGroup="Detail" />
				<asp:Button runat="server" ID="btnCsv" Text="CSV出力" CssClass="seekbutton" OnClick="btnCsv_Click" ValidationGroup="Detail" />
				<asp:Label runat="server" ID="lblCsv" Text="※CSV出力を実行しても対象は精算済になりません。"></asp:Label>
				<asp:CustomValidator ID="vdrToDate" runat="server" ErrorMessage="集計終了日は現在時刻より前の日時を指定してください。" ValidationGroup="Detail" OnServerValidate="vdrToDate_ServerValidate"></asp:CustomValidator>
				<asp:Label runat="server" ID="lblError" Font-Size="X-Large" ForeColor="red"></asp:Label>
			</asp:Panel>
			<br />
			<table border="0">
				<tr>
					<td valign="top">
						<fieldset class="fieldset-inner">
							<legend>[CSV履歴]</legend>
							<asp:GridView ID="grdCsv" runat="server" AutoGenerateColumns="false" SkinID="GridViewColor" AllowSorting="false" Font-Size="X-Small" AllowPaging="True"
								OnPageIndexChanging="grd_PageIndexChanging">
								<PagerSettings Mode="NumericFirstLast" />
								<Columns>
									<asp:TemplateField HeaderText="CSV再ﾀﾞｳﾝﾛｰﾄﾞ">
										<ItemTemplate>
											<asp:LinkButton ID="lnkCsvDownload" runat="server" Text='<%# Eval("DATE_TIME") %>' CommandArgument='<%# Eval("FILE_PATH") %>' OnCommand="lnkCsvDownload_Command"
												CausesValidation="False">
											</asp:LinkButton>
										</ItemTemplate>
										<ItemStyle HorizontalAlign="Center" />
									</asp:TemplateField>
								</Columns>
							</asp:GridView>
							<asp:GridView ID="grdPwCsv" runat="server" AutoGenerateColumns="false" SkinID="GridViewColor" AllowSorting="false" Font-Size="X-Small" AllowPaging="True"
								OnPageIndexChanging="grd_PageIndexChanging">
								<PagerSettings Mode="NumericFirstLast" />
								<Columns>
									<asp:TemplateField HeaderText="PWILD様向けCSV再ﾀﾞｳﾝﾛｰﾄﾞ">
										<ItemTemplate>
											<asp:LinkButton ID="lnkCsvDownload" runat="server" Text='<%# Eval("DATE_TIME") %>' CommandArgument='<%# Eval("FILE_PATH") %>' OnCommand="lnkCsvDownload_Command"
												CausesValidation="False">
											</asp:LinkButton>
										</ItemTemplate>
										<ItemStyle HorizontalAlign="Center" />
									</asp:TemplateField>
								</Columns>
							</asp:GridView>
						</fieldset>
					</td>
					<td valign="top">
						<asp:Panel runat="server" ID="pnlInfo">
							<fieldset class="fieldset-inner">
								<legend>[精算対象]</legend>
								<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="550px" Width="650px">
									<asp:GridView ID="grdPerformace" runat="server" AutoGenerateColumns="False" DataSourceID="dsTimeOperation" ShowFooter="True" AllowSorting="True" SkinID="GridViewColor"
										OnRowDataBound="grdPerformace_RowDataBound" Font-Size="X-Small">
										<Columns>
											<asp:TemplateField HeaderText="選択">
												<ItemTemplate>
													<asp:CheckBox ID="chkSettle" runat="server" Checked="true" />
												</ItemTemplate>
												<ItemStyle HorizontalAlign="Center" />
											</asp:TemplateField>
											<asp:TemplateField HeaderText="ログインID" SortExpression="LOGIN_ID">
												<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
												<ItemTemplate>
													<asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# string.Format("../Cast/CastView.aspx?loginid={0}&return={1}",Eval("LOGIN_ID"),iBridCommLib.iBridUtil.GetStringValue(ViewState["RETURN"])) %>'
														Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
												</ItemTemplate>
												<FooterStyle HorizontalAlign="Center" />
												<FooterTemplate>
													合計
												</FooterTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="氏名" SortExpression="CAST_NM">
												<ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
												<ItemTemplate>
													<asp:Label ID="Label01" runat="server" Text='<%# Eval("CAST_NM") %>'></asp:Label>
												</ItemTemplate>
												<FooterStyle HorizontalAlign="Center" />
												<FooterTemplate>
													支払金額
												</FooterTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="電話計" SortExpression="TALK_PAY_AMT">
												<ItemTemplate>
													<asp:Label ID="Label02" runat="server" Text='<%# Eval("TALK_PAY_AMT", "{0}") %>'></asp:Label>
												</ItemTemplate>
												<FooterStyle HorizontalAlign="Right" BackColor="#F5F5DC" />
												<HeaderStyle HorizontalAlign="Center" />
												<ItemStyle HorizontalAlign="Right" BackColor="#F5F5DC" />
												<FooterTemplate>
													<asp:Label ID="lblTalkPayAmt" runat="server" Text=''></asp:Label>
												</FooterTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="WEB計" SortExpression="WEB_TOTAL_PAY_AMT">
												<ItemTemplate>
													<asp:Label ID="Label03" runat="server" Text='<%# Eval("WEB_TOTAL_PAY_AMT", "{0}") %>'></asp:Label>
												</ItemTemplate>
												<FooterStyle HorizontalAlign="Right" BackColor="#F5F5DC" />
												<HeaderStyle HorizontalAlign="Center" />
												<ItemStyle HorizontalAlign="Right" BackColor="#F5F5DC" />
												<FooterTemplate>
													<asp:Label ID="lblWebTotalPayAmt" runat="server" Text=''></asp:Label>
												</FooterTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="友達紹介" SortExpression="FRIEND_INTRO_PAY_AMT">
												<ItemTemplate>
													<asp:Label ID="Label04" runat="server" Text='<%# Eval("FRIEND_INTRO_PAY_AMT", "{0}") %>'></asp:Label>
												</ItemTemplate>
												<FooterStyle HorizontalAlign="Right" BackColor="#F5F5DC" />
												<HeaderStyle HorizontalAlign="Center" />
												<ItemStyle HorizontalAlign="Right" BackColor="#F5F5DC" />
												<FooterTemplate>
													<asp:Label ID="lblFriendIntroPayAmt" runat="server" Text=''></asp:Label>
												</FooterTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="ﾎﾞｰﾅｽ" SortExpression="BONUS_AMT">
												<ItemTemplate>
													<asp:Label ID="Label05" runat="server" Text='<%# Eval("BONUS_AMT", "{0}") %>'></asp:Label>
												</ItemTemplate>
												<FooterStyle HorizontalAlign="Right" BackColor="#FFEBCD" />
												<HeaderStyle HorizontalAlign="Center" />
												<ItemStyle HorizontalAlign="Right" BackColor="#FFEBCD" />
												<FooterTemplate>
													<asp:Label ID="lblBonusAmt" runat="server" Text=''></asp:Label>
												</FooterTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="合計" SortExpression="TOTAL_PAY_AMT">
												<ItemTemplate>
													<asp:Label ID="Label06" runat="server" Text='<%# Eval("TOTAL_PAY_AMT","{0}") %>'></asp:Label>
												</ItemTemplate>
												<FooterStyle HorizontalAlign="Right" BackColor="#FFEBCD" />
												<HeaderStyle HorizontalAlign="Center" />
												<ItemStyle HorizontalAlign="Right" BackColor="#FFEBCD" />
												<FooterTemplate>
													<asp:Label ID="lblTotalPayAmt" runat="server" Text=''></asp:Label>
												</FooterTemplate>
											</asp:TemplateField>
											<asp:BoundField DataField="REQ_PAYMENT_DATE" HeaderText="精算申請日付" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False"></asp:BoundField>
										</Columns>
										<FooterStyle ForeColor="Black" BackColor="LightYellow" />
									</asp:GridView>
								</asp:Panel>
								<asp:Button runat="server" ID="btnCheckAll" Text="全ﾁｪｯｸOFF" CssClass="seekbutton" OnClick="btnCheckAll_Click" />
							</fieldset>
						</asp:Panel>
					</td>
				</tr>
			</table>
		</fieldset>
	</div>
	<asp:ObjectDataSource ID="dsCastMailMagazine" runat="server" SelectMethod="GetListByTemplateType" TypeName="MailTemplate">
		<SelectParameters>
			<asp:Parameter DefaultValue="Z001" Name="pSiteCd" Type="String" />
			<asp:Parameter DefaultValue="24" Name="pMailTemplateType" Type="String" />
			<asp:Parameter DefaultValue="2" Name="pSexCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsTimeOperation" runat="server" SelectMethod="GetNotPaymentList" TypeName="TimeOperation" OnSelecting="dsTimeOperation_Selecting">
		<SelectParameters>
			<asp:ControlParameter ControlID="lblFromYYYY" Name="pFromYYYY" PropertyName="Text" Type="String" />
			<asp:ControlParameter ControlID="lblFromMM" Name="pFromMM" PropertyName="Text" Type="String" />
			<asp:ControlParameter ControlID="lblFromDD" Name="pFromDD" PropertyName="Text" Type="String" />
			<asp:ControlParameter ControlID="lblFromHH" Name="pFromHH" PropertyName="Text" Type="String" />
			<asp:ControlParameter ControlID="lstToYYYY" Name="pToYYYY" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstToMM" Name="pToMM" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstToDD" Name="pToDD" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstToHH" Name="pToHH" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="txtLoginId" Name="pLoginId" PropertyName="Text" Type="String" />
			<asp:Parameter Name="pProductionSeq" Type="String" />
			<asp:ControlParameter ControlID="txtCastMinimumPayment" Name="pTotalPayAmt" PropertyName="Text" Type="String" />
			<asp:Parameter DefaultValue="0" Name="pNoBankCastDispFlag" Type="String" />
			<asp:Parameter DefaultValue="0" Name="pInvalidBankCastDispFlag" Type="String" />
			<asp:Parameter Name="pNotNormalCastDispFlag" Type="String" />
			<asp:Parameter Name="pWaitPaymentFlag" Type="String" />
			<asp:Parameter Name="pReqPaymentDate" Type="String" />
			<asp:Parameter Name="pUserStatusMask" Type="int32" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsProduction" runat="server" SelectMethod="GetList" TypeName="Production"></asp:ObjectDataSource>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtCastMinimumPayment" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmOnFormSubmit="True" ConfirmText="支払集計を行ないますか？" TargetControlID="btnSettle">
	</ajaxToolkit:ConfirmButtonExtender>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrCastMinimumPayment" HighlightCssClass="validatorCallout" />
</asp:Content>
