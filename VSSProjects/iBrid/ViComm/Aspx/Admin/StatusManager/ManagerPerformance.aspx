<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ManagerPerformance.aspx.cs" Inherits="Status_ManagerPerformance"
	Title="担当別稼動集計" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="担当別稼動集計"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 632px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="232px">
							</asp:DropDownList>
						</td>
					</tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            プロダクション
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstProductionSeq" runat="server" DataSourceID="dsProduction"
                                DataTextField="PRODUCTION_NM" DataValueField="PRODUCTION_SEQ" Width="180px" OnSelectedIndexChanged="lstProductionSeq_SelectedIndexChanged"
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
					</tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            担当
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstManager" runat="server" DataSourceID="dsManager" DataTextField="MANAGER_NM"
                                DataValueField="MANAGER_SEQ" Width="180px">
                            </asp:DropDownList>
                        </td>
                    </tr>
					<tr>
						<td class="tdHeaderStyle">
							集計開始日
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstFromYYYY" runat="server" Width="60px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstFromMM" runat="server" Width="40px">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstFromDD" runat="server" Width="40px">
							</asp:DropDownList>日
							<asp:DropDownList ID="lstFromHH" runat="server" Width="40px">
							</asp:DropDownList>時
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							集計終了日
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstToYYYY" runat="server" Width="60px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstToMM" runat="server" Width="40px">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstToDD" runat="server" Width="40px">
							</asp:DropDownList>日
							<asp:DropDownList ID="lstToHH" runat="server" Width="40px">
							</asp:DropDownList>時
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							<asp:Label ID="Label9" runat="server" Text="表示明細"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:RadioButton ID="rdoTel" runat="server" Checked="True" GroupName="DisplayType" AutoPostBack="True" Text="電話利用明細">
							</asp:RadioButton>
							<asp:RadioButton ID="rdoWeb" runat="server" GroupName="DisplayType" AutoPostBack="True" Text="WEB利用明細">
							</asp:RadioButton>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							<asp:Label ID="Label14" runat="server" Text="表示項目"></asp:Label>
						</td>
						<td class="tdDataStyle">
							上から順番に
							<asp:CheckBox ID="chkCount" runat="server" Text="分数/件数" Checked="true">
							</asp:CheckBox>
							<asp:CheckBox ID="chkPoint" runat="server" Text="稼動Pt" Checked="true">
							</asp:CheckBox>
							<asp:CheckBox ID="chkPayPoint" runat="server" Text="支払Pt" Checked="true">
							</asp:CheckBox>
							を表示する。
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
				<asp:Button runat="server" ID="btnCsv" Text="ＣＳＶ出力" CssClass="seekbutton" OnClick="btnCSV_Click" />
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[稼動集計]</legend>
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="Auto" Height="430px" Width="100%">
					<asp:GridView ID="grdPoint" runat="server" AutoGenerateColumns="False" DataSourceID="dsTimeOperation" ShowFooter="True" OnRowDataBound="grdPoint_RowDataBound"
						SkinID="GridViewColor" Font-Size="X-Small" OnRowCreated="grdPoint_RowCreated">
						<Columns>
							<asp:TemplateField HeaderText="担当">
								<ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
								<ItemTemplate>
									<asp:Label ID="Label2" runat="server" Text='<%# Eval("MANAGER_NM") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Center" />
								<FooterTemplate>
									<asp:Label ID="lblTotalCountNm" runat="server" Text="分数/件数" Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblTotalPointNm" runat="server" Text="稼動Pt" Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblTotalPayPointNm" runat="server" Text="支払Pt" Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="TV">
								<ItemTemplate>
									<asp:Label ID="Label01" runat="server" Text='<%# Eval("PRV_TV_TALK_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label21" runat="server" Text='<%# Eval("PRV_TV_TALK_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label02" runat="server" Text='<%# Eval("PRV_TV_TALK_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblPrvTvTalkMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label22" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblPrvTvTalkPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label63" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblPrvTvTalkPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="TVﾁｬｯﾄ">
								<ItemTemplate>
									<asp:Label ID="Label03" runat="server" Text='<%# Eval("PUB_TV_TALK_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label25" runat="server" Text='<%# Eval("PUB_TV_TALK_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label04" runat="server" Text='<%# Eval("PUB_TV_TALK_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblPubTvTalkMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label23" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblPubTvTalkPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label64" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblPubTvTalkPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="音声">
								<ItemTemplate>
									<asp:Label ID="Label05" runat="server" Text='<%# Eval("PRV_VOICE_TALK_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label26" runat="server" Text='<%# Eval("PRV_VOICE_TALK_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label06" runat="server" Text='<%# Eval("PRV_VOICE_TALK_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblPrvVoiceTalkMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label24" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblPrvVoiceTalkPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label65" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblPrvVoiceTalkPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="音声ﾁｬｯﾄ">
								<ItemTemplate>
									<asp:Label ID="Label05" runat="server" Text='<%# Eval("PUB_VOICE_TALK_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label26" runat="server" Text='<%# Eval("PUB_VOICE_TALK_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label06" runat="server" Text='<%# Eval("PUB_VOICE_TALK_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblPubVoiceTalkMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblPubVoiceTalkPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label66" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblPubVoiceTalkPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="会話ﾓﾆﾀ">
								<ItemTemplate>
									<asp:Label ID="Label07" runat="server" Text='<%# Eval("VIEW_TALK_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label27" runat="server" Text='<%# Eval("VIEW_TALK_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label08" runat="server" Text='<%# Eval("VIEW_TALK_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblViewTalkMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label37" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblViewTalkPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label67" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblViewTalkPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="音声ﾓﾆﾀ">
								<ItemTemplate>
									<asp:Label ID="Label09" runat="server" Text='<%# Eval("WIRETAP_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("WIRETAP_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("WIRETAP_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblWiretapMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label38" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblWiretapPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label68" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblWiretapPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="部屋ﾓﾆﾀ">
								<ItemTemplate>
									<asp:Label ID="Label09" runat="server" Text='<%# Eval("VIEW_BROADCAST_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("VIEW_BROADCAST_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("VIEW_BROADCAST_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblViewBroadcastMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label39" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblViewBroadcastPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label69" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblViewBroadcastPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾗｲﾌﾞ視聴">
								<ItemTemplate>
									<asp:Label ID="Label09" runat="server" Text='<%# Eval("LIVE_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("LIVE_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("LIVE_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblLiveMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label40" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblLivePoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label70" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblLivePayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="PF再生">
								<ItemTemplate>
									<asp:Label ID="Label09" runat="server" Text='<%# Eval("PLAY_PROFILE_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("PLAY_PROFILE_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("PLAY_PROFILE_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblPlayProfileMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label41" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblPlayProfilePoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label71" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblPlayProfilePayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="PF録音">
								<ItemTemplate>
									<asp:Label ID="Label09" runat="server" Text='<%# Eval("REC_PROFILE_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("REC_PROFILE_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("REC_PROFILE_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblRecProfileMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label42" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblRecProfilePoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label72" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblRecProfilePayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="GP会話">
								<ItemTemplate>
									<asp:Label ID="Label09" runat="server" Text='<%# Eval("GPF_TALK_VOICE_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("GPF_TALK_VOICE_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("GPF_TALK_VOICE_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblGpfTalkVoiceMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label43" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblGpfTalkVoicePoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label73" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblGpfTalkVoicePayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾌﾟﾗｲﾍﾞｰﾄ<br/>伝言再生">
								<ItemTemplate>
									<asp:Label ID="Label09" runat="server" Text='<%# Eval("PLAY_PV_MSG_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label28" runat="server" Text='　' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='　' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblPlayPvMsgMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label44" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblPlayPvMsgPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label74" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblPlayPvMsgPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="男性待機<br/>TV">
								<ItemTemplate>
									<asp:Label ID="Label09" runat="server" Text='<%# Eval("CAST_PRV_TV_TALK_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("CAST_PRV_TV_TALK_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("CAST_PRV_TV_TALK_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblCastPrvTvTalkMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label45" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblCastPrvTvTalkPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label75" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblCastPrvTvTalkPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="男性待機<br/>TVﾁｬｯﾄ">
								<ItemTemplate>
									<asp:Label ID="Label09" runat="server" Text='<%# Eval("CAST_PUB_TV_TALK_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("CAST_PUB_TV_TALK_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("CAST_PUB_TV_TALK_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblCastPubTvTalkMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label46" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblCastPubTvTalkPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label76" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblCastPubTvTalkPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="男性待機<br/>音声">
								<ItemTemplate>
									<asp:Label ID="Label09" runat="server" Text='<%# Eval("CAST_PRV_VOICE_TALK_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("CAST_PRV_VOICE_TALK_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("CAST_PRV_VOICE_TALK_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblCastPrvVoiceTalkMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label47" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblCastPrvVoiceTalkPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label77" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblCastPrvVoiceTalkPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="男性待機<br/>音声ﾁｬｯﾄ">
								<ItemTemplate>
									<asp:Label ID="Label09" runat="server" Text='<%# Eval("CAST_PUB_VOICE_TALK_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("CAST_PUB_VOICE_TALK_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("CAST_PUB_VOICE_TALK_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblCastPubVoiceTalkMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label48" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblCastPubVoiceTalkPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label78" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblCastPubVoiceTalkPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="電話計">
								<ItemTemplate>
									<asp:Label ID="Label13" runat="server" Text='<%# Eval("TALK_MIN", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label30" runat="server" Text='<%# Eval("TALK_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label14" runat="server" Text='<%# Eval("TALK_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" BackColor="#F5F5DC" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" BackColor="#F5F5DC" />
								<FooterTemplate>
									<asp:Label ID="lblTalkMin" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label49" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblTalkPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label79" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblTalkPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾒｰﾙ受信">
								<ItemTemplate>
									<asp:Label ID="Label15" runat="server" Text='<%# Eval("USER_MAIL_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label31" runat="server" Text='<%# Eval("USER_MAIL_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label80" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label16" runat="server" Text='<%# Eval("USER_MAIL_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblUserMailCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label50" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblUserMailPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label81" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblUserMailPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="写真添付<br/>ﾒｰﾙ受信">
								<ItemTemplate>
									<asp:Label ID="Label15" runat="server" Text='<%# Eval("USER_MAIL_PIC_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label31" runat="server" Text='<%# Eval("USER_MAIL_PIC_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label82" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label16" runat="server" Text='<%# Eval("USER_MAIL_PIC_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblUserMailPicCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label51" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblUserMailPicPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label83" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblUserMailPicPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="動画添付<br/>ﾒｰﾙ受信">
								<ItemTemplate>
									<asp:Label ID="Label15" runat="server" Text='<%# Eval("USER_MAIL_MOVIE_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label31" runat="server" Text='<%# Eval("USER_MAIL_MOVIE_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label16" runat="server" Text='<%# Eval("USER_MAIL_MOVIE_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblUserMailMovieCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label52" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblUserMailMoviePoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label85" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblUserMailMoviePayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾒｰﾙ添付<br>写真閲覧">
								<ItemTemplate>
									<asp:Label ID="Label17" runat="server" Text='<%# Eval("OPEN_PIC_MAIL_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label32" runat="server" Text='<%# Eval("OPEN_PIC_MAIL_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label18" runat="server" Text='<%# Eval("OPEN_PIC_MAIL_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblOpenPicMailCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label53" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblOpenPicMailPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label86" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblOpenPicMailPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾒｰﾙ添付<br>動画閲覧">
								<ItemTemplate>
									<asp:Label ID="Label19" runat="server" Text='<%# Eval("OPEN_MOVIE_MAIL_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label33" runat="server" Text='<%# Eval("OPEN_MOVIE_MAIL_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label20" runat="server" Text='<%# Eval("OPEN_MOVIE_MAIL_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblOpenMovieMailCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label54" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblOpenMovieMailPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label87" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblOpenMovieMailPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="掲示板<br>写真閲覧">
								<ItemTemplate>
									<asp:Label ID="Label17" runat="server" Text='<%# Eval("OPEN_PIC_BBS_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label32" runat="server" Text='<%# Eval("OPEN_PIC_BBS_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label18" runat="server" Text='<%# Eval("OPEN_PIC_BBS_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblOpenPicBbsCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label55" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblOpenPicBbsPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label88" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblOpenPicBbsPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="掲示板<br>動画閲覧">
								<ItemTemplate>
									<asp:Label ID="Label19" runat="server" Text='<%# Eval("OPEN_MOVIE_BBS_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label33" runat="server" Text='<%# Eval("OPEN_MOVIE_BBS_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label20" runat="server" Text='<%# Eval("OPEN_MOVIE_BBS_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblOpenMovieBbsCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label56" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblOpenMovieBbsPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label89" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblOpenMovieBbsPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾌﾟﾗｲﾍﾞｰﾄ<br>伝言受信">
								<ItemTemplate>
									<asp:Label ID="Label19" runat="server" Text='<%# Eval("PRV_MESSAGE_RX_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label33" runat="server" Text='<%# Eval("PRV_MESSAGE_RX_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label20" runat="server" Text='<%# Eval("PRV_MESSAGE_RX_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblPrvMessageRxCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label57" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblPrvMessageRxPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label90" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblPrvMessageRxPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾌﾟﾗｲﾍﾞｰﾄ<br>PF受信">
								<ItemTemplate>
									<asp:Label ID="Label19" runat="server" Text='<%# Eval("PRV_PROFILE_RX_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label33" runat="server" Text='<%# Eval("PRV_PROFILE_RX_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label20" runat="server" Text='<%# Eval("PRV_PROFILE_RX_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblPrvProfileRxCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label58" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblPrvProfileRxPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label91" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblPrvProfileRxPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾀﾞｳﾝﾛｰﾄﾞ<br>ﾑｰﾋﾞｰ">
								<ItemTemplate>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("DOWNLOAD_MOVIE_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("DOWNLOAD_MOVIE_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label11" runat="server" Text='<%# Eval("DOWNLOAD_MOVIE_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblDownloadMovieCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label59" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblDownloadMoviePoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label92" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblDownloadMoviePayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="定時<br>ｺｰﾙ">
								<ItemTemplate>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("TIME_CALL_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("TIME_CALL_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label12" runat="server" Text='<%# Eval("TIME_CALL_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblTimeCallCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label60" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblTimeCallPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label93" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblTimeCallPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾀﾞｳﾝﾛｰﾄﾞ<br>着ﾎﾞｲｽ">
								<ItemTemplate>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("DOWNLOAD_VOICE_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("DOWNLOAD_VOICE_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label11" runat="server" Text='<%# Eval("DOWNLOAD_VOICE_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblDownloadVoiceCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label61" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblDownloadVoicePoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label94" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblDownloadVoicePayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ｿｰｼｬﾙ<br>ｹﾞｰﾑ">
								<ItemTemplate>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("SOCIAL_GAME_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("SOCIAL_GAME_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label11" runat="server" Text='<%# Eval("SOCIAL_GAME_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblSocialGameCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label99" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblSocialGamePoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label98" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblSocialGamePayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="野球拳<br />FC会費">
								<ItemTemplate>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("YAKYUKEN_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("YAKYUKEN_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label11" runat="server" Text='<%# Eval("YAKYUKEN_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblYakyukenCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="lblYakyukenBrCount" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblYakyukenPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="lblYakyukenBrPoint" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblYakyukenPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾌﾟﾚｾﾞﾝﾄ<br />ﾒｰﾙ">
								<ItemTemplate>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("PRESENT_MAIL_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("PRESENT_MAIL_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label11" runat="server" Text='<%# Eval("PRESENT_MAIL_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblPresentMailCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="lblPresentMailBrCount" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblPresentMailPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="lblPresentMailBrPoint" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblPresentMailPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="WEB計">
								<ItemTemplate>
									<asp:Label ID="Label13" runat="server" Text='<%# Eval("WEB_TOTAL_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label34" runat="server" Text='<%# Eval("WEB_TOTAL_POINT", "{0}") %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label14" runat="server" Text='<%# Eval("WEB_TOTAL_PAY_POINT", "{0}") %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" BackColor="#F5F5DC" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" BackColor="#F5F5DC" />
								<FooterTemplate>
									<asp:Label ID="lblWebTotalCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label62" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblWebTotalPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label95" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblWebTotalPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="合計">
								<ItemTemplate>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label35" runat="server" Text='<%#string.Format("{0}",CalcSum(Eval("TALK_POINT"),Eval("WEB_TOTAL_POINT"))) %>' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="Label16" runat="server" Text='<%#string.Format("{0}",CalcSum(Eval("TALK_PAY_POINT"),Eval("WEB_TOTAL_PAY_POINT"))) %>' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" BackColor="#FFEBCD" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" BackColor="#FFEBCD" />
								<FooterTemplate>
									<asp:Label ID="Label97" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblTotalPoint" runat="server" Text='' Visible='<%# CheckedPoint() %>'></asp:Label>
									<asp:Label ID="Label96" runat="server" Text='<br />' Visible='<%# GetBrPoint() %>'></asp:Label>
									<asp:Label ID="lblTotalPayPoint" runat="server" Text='' Visible='<%# CheckedPayPoint() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
						</Columns>
						<FooterStyle ForeColor="Black" BackColor="LightYellow" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsTimeOperation" runat="server" SelectMethod="GetManagerSummaryList" TypeName="TimeOperation" OnSelecting="dsTimeOperation_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pFromYYYY" Type="String" />
			<asp:Parameter Name="pFromMM" Type="String" />
			<asp:Parameter Name="pFromDD" Type="String" />
			<asp:Parameter Name="pFromHH" Type="String" />
			<asp:Parameter Name="pToYYYY" Type="String" />
			<asp:Parameter Name="pToMM" Type="String" />
			<asp:Parameter Name="pToDD" Type="String" />
			<asp:Parameter Name="pToHH" Type="String" />
			<asp:Parameter Name="pProductionSeq" Type="String" />
			<asp:Parameter Name="pManagerSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsProduction" runat="server" SelectMethod="GetList" TypeName="Production"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsManager" runat="server" SelectMethod="GetProductionList" TypeName="Manager" OnSelecting="dsManager_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pProductionSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
