﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者別未精算報酬額一覧

--	Progaram ID		: CastNotSettlePerformance
--
--  Creation Date	: 2010.03.25
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Status_CastNotSettlePerformance:System.Web.UI.Page {

	private int prvTvTalkAmt;
	private int pubTvTalkAmt;
	private int prvVoiceTalkAmt;
	private int pubVoiceTalkAmt;
	private int viewTalkPayAmt;
	private int viewBroadcastPayAmt;
	private int livePayAmt;
	private int playProfilePayAmt;
	private int recProfilePayAmt;
	private int wiretapPayAmt;
	private int gpfTalkVoicePayAmt;
	private int playPvMsgPayAmt;
	//private int moviePayAmt;
	private int castPrvTvTalkAmt;
	private int castPubTvTalkAmt;
	private int castPrvVoiceTalkAmt;
	private int castPubVoiceTalkAmt;
	private int talkPayAmt;
	private int mailPayAmt;
	private int mailPicPayAmt;
	private int mailMoviePayAmt;
	private int openPicMailPayAmt;
	private int openMovieMailPayAmt;
	private int openPicBbsPayAmt;
	private int openMovieBbsPayAmt;
	private int messageRxPayAmt;
	private int profileRxPayAmt;
	private int downloadMoviePayAmt;
	private int timeCallPayAmt;
	private int downloadVoicePayAmt;
	private int socialGamePayAmt;
	private int yakyukenPayAmt;
	private int presentMailPayAmt;
	private int friendIntroPayAmt;

	private int bonusAmt;
	private int webTotalPayAmt;

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			txtLoginId.Text = "";
			if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["InvalidBankCastDispFlag"]).Equals(ViCommConst.FLAG_ON_STR)) {
				chkBankInfoInvalid.Checked = true;
			} else {
				chkBankInfoInvalid.Checked = false;
			}
			btnCsv.Visible = false;
			InitPage();

			lstProductionSeq.Items.Insert(0,new ListItem("",""));
			lstProductionSeq.DataSourceID = "";
			if (!iBridUtil.GetStringValue(Session["PRODUCTION_SEQ"]).Equals("")) {
				lstProductionSeq.SelectedValue = Session["PRODUCTION_SEQ"].ToString();
				lstProductionSeq.Enabled = false;
			} else {
				lstProductionSeq.SelectedIndex = 0;
			}
		} else {
			DataBind();
		}
		DisplayWordUtil.ReplaceGridColumnHeader(this.grdPerformace);
	}

	private void InitPage() {
		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);

		grdPerformace.DataSourceID = "";
		pnlInfo.Visible = false;
		ClearField();
		DataBind();
		if (!IsPostBack) {
			txtCastMinimumPayment.Text = "0";
			using (ManageCompany oManageCompany = new ManageCompany()) {
				if (oManageCompany.GetOne()) {
					txtCastMinimumPayment.Text = oManageCompany.castSelfMinimumPayment;
				}
			}
			SysPrograms.SetupFromToDayTime(lstFromYYYY,lstFromMM,lstFromDD,lstFromHH,lstToYYYY,lstToMM,lstToDD,lstToHH,true);
			lstFromYYYY.Items.Insert(0,new ListItem("----","1900"));
			lstToYYYY.Items.Insert(0,new ListItem("----",DateTime.Now.AddYears(1).ToString("yyyy")));
		}

		bool bIsImpact = false;
		chkAuthWait.Checked = false;
		chkNormal.Checked = true;
		chkStop.Checked = false;
		chkHold.Checked = bIsImpact;
		chkResigned.Checked = bIsImpact;
		chkBan.Checked = false;
	}

	private void ClearField() {
		ClearTotal();
		lblMsg.Visible = false;
		lstFromYYYY.SelectedIndex = 0;
		lstFromMM.SelectedIndex = 0;
		lstFromDD.SelectedIndex = 0;
		lstFromHH.SelectedIndex = 0;

		lstToYYYY.SelectedIndex = 0;
		lstToMM.SelectedIndex = 0;
		lstToDD.SelectedIndex = 0;
		lstToHH.SelectedIndex = 0;

		if (iBridUtil.GetStringValue(ConfigurationManager.AppSettings["InvalidBankCastDispFlag"]).Equals(ViCommConst.FLAG_ON_STR)) {
			chkBankInfoInvalid.Checked = true;
		} else {
			chkBankInfoInvalid.Checked = false;
		}
	}

	private void ClearTotal() {
		prvTvTalkAmt = 0;
		pubTvTalkAmt = 0;
		prvVoiceTalkAmt = 0;
		pubVoiceTalkAmt = 0;
		viewTalkPayAmt = 0;
		viewBroadcastPayAmt = 0;
		livePayAmt = 0;
		playProfilePayAmt = 0;
		recProfilePayAmt = 0;
		wiretapPayAmt = 0;
		gpfTalkVoicePayAmt = 0;
		playPvMsgPayAmt = 0;
		//moviePayAmt = 0;
		castPrvTvTalkAmt = 0;
		castPubTvTalkAmt = 0;
		castPrvVoiceTalkAmt = 0;
		castPubVoiceTalkAmt = 0;
		talkPayAmt = 0;
		mailPayAmt = 0;
		mailPicPayAmt = 0;
		mailMoviePayAmt = 0;
		openPicMailPayAmt = 0;
		openMovieMailPayAmt = 0;
		openPicBbsPayAmt = 0;
		openMovieBbsPayAmt = 0;
		messageRxPayAmt = 0;
		profileRxPayAmt = 0;
		downloadMoviePayAmt = 0;
		timeCallPayAmt = 0;
		downloadVoicePayAmt = 0;
		socialGamePayAmt = 0;
		yakyukenPayAmt = 0;
		presentMailPayAmt = 0;
		friendIntroPayAmt = 0;
		bonusAmt = 0;
		webTotalPayAmt = 0;

	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (IsValid) {
			GetList();
		}
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		btnCsv.Visible = false;
		InitPage();
	}

	protected void grdPerformace_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			prvTvTalkAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRV_TV_TALK_PAY_AMT"));
			pubTvTalkAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PUB_TV_TALK_PAY_AMT"));
			prvVoiceTalkAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRV_VOICE_TALK_PAY_AMT"));
			pubVoiceTalkAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PUB_VOICE_TALK_PAY_AMT"));
			viewTalkPayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"VIEW_TALK_PAY_AMT"));
			wiretapPayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"WIRETAP_PAY_AMT"));
			viewBroadcastPayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"VIEW_BROADCAST_PAY_AMT"));
			livePayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"LIVE_PAY_AMT"));
			playProfilePayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PLAY_PROFILE_PAY_AMT"));
			recProfilePayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"REC_PROFILE_PAY_AMT"));
			gpfTalkVoicePayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"GPF_TALK_VOICE_PAY_AMT"));
			playPvMsgPayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PLAY_PV_MSG_PAY_AMT"));
			//moviePayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"MOVIE_PAY_AMT"));
			castPrvTvTalkAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PRV_TV_TALK_PAY_AMT"));
			castPubTvTalkAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PUB_TV_TALK_PAY_AMT"));
			castPrvVoiceTalkAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PRV_VOICE_TALK_PAY_AMT"));
			castPubVoiceTalkAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PUB_VOICE_TALK_PAY_AMT"));
			talkPayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TALK_PAY_AMT"));
			mailPayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"USER_MAIL_PAY_AMT"));
			mailPicPayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"USER_MAIL_PIC_PAY_AMT"));
			mailMoviePayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"USER_MAIL_MOVIE_PAY_AMT"));
			openPicMailPayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"OPEN_PIC_MAIL_PAY_AMT"));
			openMovieMailPayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"OPEN_MOVIE_MAIL_PAY_AMT"));
			openPicBbsPayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"OPEN_PIC_BBS_PAY_AMT"));
			openMovieBbsPayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"OPEN_MOVIE_BBS_PAY_AMT"));
			messageRxPayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRV_MESSAGE_RX_PAY_AMT"));
			profileRxPayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRV_PROFILE_RX_PAY_AMT"));
			downloadMoviePayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"DOWNLOAD_MOVIE_PAY_AMT"));
			timeCallPayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TIME_CALL_PAY_AMT"));
			downloadVoicePayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"DOWNLOAD_VOICE_PAY_AMT"));
			socialGamePayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"SOCIAL_GAME_PAY_AMT"));
			yakyukenPayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"YAKYUKEN_PAY_AMT"));
			presentMailPayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRESENT_MAIL_PAY_AMT"));
			friendIntroPayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"FRIEND_INTRO_PAY_AMT"));
			bonusAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"BONUS_AMT"));
			webTotalPayAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"WEB_TOTAL_PAY_AMT"));

		} else if (e.Row.RowType == DataControlRowType.Footer) {
			Label lblPrvTvTalkPayAmt = (Label)e.Row.FindControl("lblPrvTvTalkPayAmt");
			Label lblPubTvTalkPayAmt = (Label)e.Row.FindControl("lblPubTvTalkPayAmt");
			Label lblPrvVoiceTalkPayAmt = (Label)e.Row.FindControl("lblPrvVoiceTalkPayAmt");
			Label lblPubVoiceTalkPayAmt = (Label)e.Row.FindControl("lblPubVoiceTalkPayAmt");
			Label lblViewTalkPayAmt = (Label)e.Row.FindControl("lblViewTalkPayAmt");
			Label lblWiretapPayAmt = (Label)e.Row.FindControl("lblWiretapPayAmt");
			Label lblViewBroadcastPayAmt = (Label)e.Row.FindControl("lblViewBroadcastPayAmt");
			Label lblLivePayAmt = (Label)e.Row.FindControl("lblLivePayAmt");
			Label lblPlayProfilePayAmt = (Label)e.Row.FindControl("lblPlayProfilePayAmt");
			Label lblRecProfilePayAmt = (Label)e.Row.FindControl("lblRecProfilePayAmt");
			Label lblGpfTalkVoicePayAmt = (Label)e.Row.FindControl("lblGpfTalkVoicePayAmt");
			Label lblPlayPvMsgPayAmt = (Label)e.Row.FindControl("lblPlayPvMsgPayAmt");
			//Label lblMoviePayAmt = (Label)e.Row.FindControl("lblMoviePayAmt");
			Label lblCastPrvTvTalkPayAmt = (Label)e.Row.FindControl("lblCastPrvTvTalkPayAmt");
			Label lblCastPubTvTalkPayAmt = (Label)e.Row.FindControl("lblCastPubTvTalkPayAmt");
			Label lblCastPrvVoiceTalkPayAmt = (Label)e.Row.FindControl("lblCastPrvVoiceTalkPayAmt");
			Label lblCastPubVoiceTalkPayAmt = (Label)e.Row.FindControl("lblCastPubVoiceTalkPayAmt");
			Label lblTalkPayAmt = (Label)e.Row.FindControl("lblTalkPayAmt");
			Label lblUserMailPayAmt = (Label)e.Row.FindControl("lblUserMailPayAmt");
			Label lblUserMailPicPayAmt = (Label)e.Row.FindControl("lblUserMailPicPayAmt");
			Label lblUserMailMoviePayAmt = (Label)e.Row.FindControl("lblUserMailMoviePayAmt");
			Label lblOpenPicMailPayAmt = (Label)e.Row.FindControl("lblOpenPicMailPayAmt");
			Label lblOpenMovieMailPayAmt = (Label)e.Row.FindControl("lblOpenMovieMailPayAmt");
			Label lblOpenPicBbsPayAmt = (Label)e.Row.FindControl("lblOpenPicBbsPayAmt");
			Label lblOpenMovieBbsPayAmt = (Label)e.Row.FindControl("lblOpenMovieBbsPayAmt");
			Label lblPrvMessageRxPayAmt = (Label)e.Row.FindControl("lblPrvMessageRxPayAmt");
			Label lblPrvProfileRxPayAmt = (Label)e.Row.FindControl("lblPrvProfileRxPayAmt");
			Label lblDownloadMoviePayAmt = (Label)e.Row.FindControl("lblDownloadMoviePayAmt");
			Label lblTimeCallPayAmt = (Label)e.Row.FindControl("lblTimeCallPayAmt");
			Label lblDownloadVoicePayAmt = (Label)e.Row.FindControl("lblDownloadVoicePayAmt");
			Label lblSocialGamePayAmt = (Label)e.Row.FindControl("lblSocialGamePayAmt");
			Label lblYakyukenPayAmt = (Label)e.Row.FindControl("lblYakyukenPayAmt");
			Label lblPresentMailPayAmt = (Label)e.Row.FindControl("lblPresentMailPayAmt");
			Label lblFriendIntroPayAmt = (Label)e.Row.FindControl("lblFriendIntroPayAmt");
			Label lblWebTotalPayAmt = (Label)e.Row.FindControl("lblWebTotalPayAmt");
			Label lblBonusAmt = (Label)e.Row.FindControl("lblBonusAmt");
			Label lblTotalPayAmt = (Label)e.Row.FindControl("lblTotalPayAmt");

			int iTotalPayAmt = talkPayAmt + webTotalPayAmt + bonusAmt + friendIntroPayAmt;

			lblPrvTvTalkPayAmt.Text = prvTvTalkAmt.ToString();
			lblPubTvTalkPayAmt.Text = pubTvTalkAmt.ToString();
			lblPrvVoiceTalkPayAmt.Text = prvVoiceTalkAmt.ToString();
			lblPubVoiceTalkPayAmt.Text = pubVoiceTalkAmt.ToString();
			lblViewTalkPayAmt.Text = viewTalkPayAmt.ToString();
			lblWiretapPayAmt.Text = wiretapPayAmt.ToString();
			lblViewBroadcastPayAmt.Text = viewBroadcastPayAmt.ToString();
			lblLivePayAmt.Text = livePayAmt.ToString();
			lblPlayProfilePayAmt.Text = playProfilePayAmt.ToString();
			lblRecProfilePayAmt.Text = recProfilePayAmt.ToString();
			lblGpfTalkVoicePayAmt.Text = gpfTalkVoicePayAmt.ToString();
			lblPlayPvMsgPayAmt.Text = playPvMsgPayAmt.ToString();
			//lblMoviePayAmt.Text = moviePayAmt.ToString();
			lblCastPrvTvTalkPayAmt.Text = castPrvTvTalkAmt.ToString();
			lblCastPubTvTalkPayAmt.Text = castPubTvTalkAmt.ToString();
			lblCastPrvVoiceTalkPayAmt.Text = castPrvVoiceTalkAmt.ToString();
			lblCastPubVoiceTalkPayAmt.Text = castPubVoiceTalkAmt.ToString();
			lblTalkPayAmt.Text = talkPayAmt.ToString();
			lblUserMailPayAmt.Text = mailPayAmt.ToString();
			lblUserMailPicPayAmt.Text = mailPicPayAmt.ToString();
			lblUserMailMoviePayAmt.Text = mailMoviePayAmt.ToString();
			lblOpenPicMailPayAmt.Text = openPicMailPayAmt.ToString();
			lblOpenMovieMailPayAmt.Text = openMovieMailPayAmt.ToString();
			lblOpenPicBbsPayAmt.Text = openPicBbsPayAmt.ToString();
			lblOpenMovieBbsPayAmt.Text = openMovieBbsPayAmt.ToString();
			lblPrvMessageRxPayAmt.Text = messageRxPayAmt.ToString();
			lblPrvProfileRxPayAmt.Text = profileRxPayAmt.ToString();
			lblDownloadMoviePayAmt.Text = downloadMoviePayAmt.ToString();
			lblTalkPayAmt.Text = talkPayAmt.ToString();
			lblDownloadVoicePayAmt.Text = downloadVoicePayAmt.ToString();
			lblSocialGamePayAmt.Text = socialGamePayAmt.ToString();
			lblYakyukenPayAmt.Text = yakyukenPayAmt.ToString();
			lblPresentMailPayAmt.Text = presentMailPayAmt.ToString();
			lblFriendIntroPayAmt.Text = friendIntroPayAmt.ToString();
			lblWebTotalPayAmt.Text = webTotalPayAmt.ToString();
			lblBonusAmt.Text = bonusAmt.ToString();
			lblTotalPayAmt.Text = iTotalPayAmt.ToString();
		}
	}

	private void GetList() {
		ClearTotal();

		grdPerformace.Columns[10].Visible = false;
		grdPerformace.Columns[11].Visible = false;
		grdPerformace.Columns[12].Visible = false;
		grdPerformace.Columns[13].Visible = false;
		grdPerformace.Columns[26].Visible = false;
		grdPerformace.Columns[27].Visible = false;
		grdPerformace.Columns[28].Visible = IsAvailableService(ViCommConst.RELEASE_SALE_MOVIE);
		grdPerformace.Columns[29].Visible = IsAvailableService(ViCommConst.RELEASE_TIME_CALL);
		grdPerformace.Columns[30].Visible = IsAvailableService(ViCommConst.RELEASE_SALE_VOICE);
		grdPerformace.DataSourceID = "dsTimeOperation";
		DataBind();

		int iCompare = string.Compare(Session["AdminType"].ToString(),ViCommConst.RIGHT_SITE_OWNER);
		btnCsv.Visible = (iCompare >= 0);

		pnlInfo.Visible = true;
	}

	public static bool IsAvailableService(ulong pService) {
		using (ManageCompany oInstance = new ManageCompany()) {
			return oInstance.IsAvailableService(pService);
		}
	}

	protected string CalcSum(object pSum1,object pSum2,object pSum3) {
		int iSum = int.Parse(pSum1.ToString()) + int.Parse(pSum2.ToString()) + int.Parse(pSum3.ToString());
		return iSum.ToString();
	}

	protected void grdPerformace_RowCreated(object sender,GridViewRowEventArgs e) {
		if ((e.Row.RowType == DataControlRowType.Header) ||
			(e.Row.RowType == DataControlRowType.DataRow) ||
			(e.Row.RowType == DataControlRowType.Footer)) {
			if (rdoTel.Checked) {
				e.Row.Cells[19].Visible = false;
				e.Row.Cells[20].Visible = false;
				e.Row.Cells[21].Visible = false;
				e.Row.Cells[22].Visible = false;
				e.Row.Cells[23].Visible = false;
				e.Row.Cells[24].Visible = false;
				e.Row.Cells[25].Visible = false;
				e.Row.Cells[26].Visible = false;
				e.Row.Cells[27].Visible = false;
				e.Row.Cells[28].Visible = false;
				e.Row.Cells[29].Visible = false;
				e.Row.Cells[30].Visible = false;
				e.Row.Cells[31].Visible = false;
				e.Row.Cells[32].Visible = false;
				e.Row.Cells[33].Visible = false;
			} else {
				e.Row.Cells[2].Visible = false;
				e.Row.Cells[3].Visible = false;
				e.Row.Cells[4].Visible = false;
				e.Row.Cells[5].Visible = false;
				e.Row.Cells[6].Visible = false;
				e.Row.Cells[7].Visible = false;
				e.Row.Cells[8].Visible = false;
				e.Row.Cells[9].Visible = false;
				e.Row.Cells[10].Visible = false;
				e.Row.Cells[11].Visible = false;
				e.Row.Cells[12].Visible = false;
				e.Row.Cells[13].Visible = false;
				e.Row.Cells[14].Visible = false;
				e.Row.Cells[15].Visible = false;
				e.Row.Cells[16].Visible = false;
				e.Row.Cells[17].Visible = false;
			}
		}
	}

	private string GetCsvString(DataSet pDataSet) {
		bool bStaffIdFlag = false;
		bool bValidateAccountFlag = false;
		bool bBankTypeRadio = false;
		string sTransferFee = string.Empty;
		int iTransferFee = 0;

		// Available Serive?
		using (ManageCompany oCompany = new ManageCompany()) {
			bStaffIdFlag = oCompany.IsAvailableService(ViCommConst.RELEASE_SUPPORT_STAFF_ID);
			bValidateAccountFlag = oCompany.IsAvailableService(ViCommConst.RELEASE_VALIDATE_ACCOUNT_INFO);
			bBankTypeRadio = oCompany.IsAvailableService(ViCommConst.RELEASE_BANK_TYPE_RADIO);
		}

		using (Site oSite = new Site()) {
			oSite.GetValue(ViCommConst.CAST_SITE_CD,"TRANSFER_FEE",ref sTransferFee);
		}
		int.TryParse(sTransferFee,out iTransferFee);

		string sCsv = "";

		sCsv += "ログインID,";
		sCsv += "氏名,";
		if (bStaffIdFlag) {
			sCsv += "事業者コード,";
		}
		sCsv += "金額合計,";
		if (iTransferFee != 0) {
			sCsv += "金額-手数料,";
		}
		sCsv += "銀行名,";
		sCsv += "支店名,";
		sCsv += "支店名カナ,";
		sCsv += "口座タイプ,";
		sCsv += "口座番号,";
		if (bValidateAccountFlag) {
			sCsv += "口座名義,";
			sCsv += "口座状態";
		} else {
			sCsv += "口座名義";
		}

		sCsv += "\r\n";

		DataRow dr;

		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];

			sCsv += dr["LOGIN_ID"] + ",";
			sCsv += dr["CAST_NM"] + ",";
			if (bStaffIdFlag) {
				sCsv += dr["STAFF_ID"] + ",";
			}
			sCsv += dr["TOTAL_PAY_AMT"] + ",";
			if (iTransferFee != 0) {
				sCsv += (int.Parse(dr["TOTAL_PAY_AMT"].ToString()) - iTransferFee).ToString() + ",";
			}
			sCsv += dr["BANK_NM"] + ",";
			sCsv += dr["BANK_OFFICE_NM"] + ",";
			sCsv += dr["BANK_OFFICE_KANA_NM"] + ",";
			if (bBankTypeRadio) {
				if (dr["BANK_ACCOUNT_TYPE2"].Equals(ViCommConst.BankAccountType.NORMAL_ACCOUNT)) {
					sCsv += "普通,";
				} else if (dr["BANK_ACCOUNT_TYPE2"].Equals(ViCommConst.BankAccountType.CHECKING_ACCOUNT)) {
					sCsv += "当座,";
				} else if (dr["BANK_ACCOUNT_TYPE2"].Equals(ViCommConst.BankAccountType.SAVINGS_ACCOUNTS)) {
					sCsv += "貯蓄,";
				} else {
					sCsv += ",";
				}
			} else {
				sCsv += dr["BANK_ACCOUNT_TYPE"] + ",";
			}
			sCsv += dr["BANK_ACCOUNT_NO"] + ",";
			if (bValidateAccountFlag) {
				sCsv += dr["BANK_ACCOUNT_HOLDER_NM"] + ",";
				if (dr["BANK_ACCOUNT_INVALID_FLAG"].ToString().Equals("1")) {
					sCsv += "×";
				}
			} else {
				sCsv += dr["BANK_ACCOUNT_HOLDER_NM"];
			}

			sCsv += "\r\n";
		}
		return sCsv;
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		string fileName = ViCommConst.CSV_FILE_NM_NOT_SETTLE_PERFORMANCE;

		Response.AddHeader("Content-Disposition","attachment;filename=" + fileName);
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		string sYYYY = lstFromYYYY.SelectedValue;
		string sMM = lstFromMM.SelectedValue;
		string sDD = lstFromDD.SelectedValue;
		string sNoBankCastDispFlag = string.Empty;

		//運営会社設定よりFromの日付を設定 
		if (lstFromYYYY.SelectedValue.Equals("1900")) {
			using (ManageCompany oManageCompany = new ManageCompany()) {
				if (oManageCompany.GetOne()) {
					if (oManageCompany.notPayExpireMonth > 0) {
						DateTime dtFrom = DateTime.Now.AddMonths(-1 * (oManageCompany.notPayExpireMonth));
						sYYYY = dtFrom.ToString("yyyy");
						sMM = dtFrom.ToString("MM");
						sDD = "01";
					}
				}
			}
		}

		sNoBankCastDispFlag = chkBankInfoInvalid.Checked ? "1" : "0";		// BankInfo未設定者も含む

		using (TimeOperation oTimeOperation = new TimeOperation()) {
			DataSet ds = oTimeOperation.GetNotPaymentList(
					sYYYY,
					sMM,
					sDD,
					lstFromHH.SelectedValue,
					lstToYYYY.SelectedValue,
					lstToMM.SelectedValue,
					lstToDD.SelectedValue,
					lstToHH.SelectedValue,
					txtLoginId.Text,
					lstProductionSeq.SelectedValue,
					txtCastMinimumPayment.Text,
					sNoBankCastDispFlag,						// BankInfo未設定者も含む
					chkBankInfoInvalid.Checked ? "1" : "0",		// BankInfo不正者も含む
					chkNotNormal.Checked ? "1" : "0",			// ﾕｰｻﾞｰ状態が正常以外も含む
					string.Empty,
					string.Empty,
					this.GetUserStatusMask());

			string sCsvRec = GetCsvString(ds);

			Response.BinaryWrite(encoding.GetBytes(sCsvRec));
			Response.End();
		}
	}
	protected void dsTimeOperation_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {

		string sYYYY = lstFromYYYY.SelectedValue;
		string sMM = lstFromMM.SelectedValue;
		string sDD = lstFromDD.SelectedValue;
		lblMsg.Visible = false;

		//運営会社設定よりFromの日付を設定 
		if (lstFromYYYY.SelectedValue.Equals("1900")) {
			using (ManageCompany oManageCompany = new ManageCompany()) {
				if (oManageCompany.GetOne()) {
					if (oManageCompany.notPayExpireMonth > 0) {
						DateTime dtFrom = DateTime.Now.AddMonths(-1 * (oManageCompany.notPayExpireMonth));
						sYYYY = dtFrom.ToString("yyyy");
						sMM = dtFrom.ToString("MM");
						sDD = "01";
						lblMsg.Visible = true;
						lblMsg.Text = string.Format("* 集計開始年が----の場合,未精算報酬有効月数の月初日({0}/{1}/01)からが対象となっています。",sYYYY,sDD);
					}
				}
			}
		}

		e.InputParameters[0] = sYYYY;
		e.InputParameters[1] = sMM;
		e.InputParameters[2] = sDD;
		e.InputParameters[3] = lstFromHH.SelectedValue;
		e.InputParameters[4] = lstToYYYY.SelectedValue;
		e.InputParameters[5] = lstToMM.SelectedValue;
		e.InputParameters[6] = lstToDD.SelectedValue;
		e.InputParameters[7] = lstToHH.SelectedValue;
		e.InputParameters[8] = txtLoginId.Text;
		e.InputParameters[9] = lstProductionSeq.SelectedValue;
		e.InputParameters[10] = txtCastMinimumPayment.Text;
		e.InputParameters[11] = chkBankInfoInvalid.Checked ? "1" : "0";		// BankInfo未設定者も含む
		e.InputParameters[12] = chkBankInfoInvalid.Checked ? "1" : "0";		// BankInfo不正者も含む
		e.InputParameters[13] = chkNotNormal.Checked ? "1" : "0";			// ﾕｰｻﾞｰ状態が正常以外も含む
		e.InputParameters[14] = string.Empty;
		e.InputParameters[15] = string.Empty;
		e.InputParameters[16] = this.GetUserStatusMask();
	}

	protected string GetSelectToDate() {
		DateTime oSelectToDate = DateTime.Now.AddDays(-1);

		return string.Format("{0}/{1}/{2}/{3}",oSelectToDate.Year,oSelectToDate.Month,oSelectToDate.Day,24);
	}

	private int GetUserStatusMask() {
		int iStatus = 0;
		if (chkAuthWait.Checked) {
			iStatus += ViCommConst.MASK_WOMAN_AUTH_WAIT;
		}
		if (chkNormal.Checked) {
			iStatus += ViCommConst.MASK_WOMAN_NORMAL;
		}
		if (chkStop.Checked) {
			iStatus += ViCommConst.MASK_WOMAN_STOP;
		}
		if (chkHold.Checked) {
			iStatus += ViCommConst.MASK_WOMAN_HOLD;
		}
		if (chkResigned.Checked) {
			iStatus += ViCommConst.MASK_WOMAN_RESIGNED;
		}
		if (chkBan.Checked) {
			iStatus += ViCommConst.MASK_WOMAN_BAN;
		}
		return iStatus;
	}
}
