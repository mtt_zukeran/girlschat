﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者支払履歴一覧
--	Progaram ID		: PaymentHistorySummary
--
--  Creation Date	: 2011.01.05
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Drawing;
using System.Web.Security;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class StatusManager_PaymentHistorySummary:System.Web.UI.Page {

	private string recCount = string.Empty;
	private int totalAmt = 0;

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["PRODUCTION_SEQ"]))) {
				lstProductionSeq.SelectedValue = iBridUtil.GetStringValue(Session["PRODUCTION_SEQ"]);
				lstProductionSeq.Enabled = false;
			}
			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MANAGER_SEQ"]))) {
				lstManager.SelectedValue = iBridUtil.GetStringValue(Session["MANAGER_SEQ"]);
				lstManager.Enabled = false;
			}

			// パラメータが存在する場合、検索結果を表示
			if (!string.IsNullOrEmpty(Request.QueryString.ToString())) {
				this.Validate();
				btnListSeek_Click(sender,e);
			}
		}
	}
	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (IsValid) {
			if (txtReportDayTo.Text.Equals(string.Empty)) {
				txtReportDayTo.Text = txtReportDayFrom.Text;
			}
			grdPaymentHistory.PageSize = 50;
			grdPaymentHistory.PageIndex = 0;
			grdPaymentHistory.DataSourceID = "dsPaymentHistory";
			grdPaymentHistory.DataBind();
			pnlCount.DataBind();
			pnlPaymentCount.DataBind();
			pnlInfo.Visible = true;
			btnCsv.Visible = true;
			btnSendMail.Visible = true;
			pnlSendMail.Visible = false;
			lblSendMailSuccess.Visible = false;
		}
	}
	protected void btnClear_Click(object sender,EventArgs e) {
		ClearField();
		if (lstProductionSeq.Enabled) {
			lstProductionSeq.SelectedValue = string.Empty;
		}
		if (lstManager.Enabled) {
			lstManager.SelectedValue = string.Empty;
		}
	}
	protected void btnCsv_Click(object sender,EventArgs e) {

		using (PaymentHistory oPayment = new PaymentHistory()) {
			PaymentHistoryParameters oParameter = GetPaymentHistoryParameters();
			string fileName = ViCommConst.CSV_FILE_NM_PAYMENT_HISTORY_SUMMARY;

			Response.AddHeader("Content-Disposition","attachment;filename=" + fileName);
			Response.ContentType = "application/octet-stream-dummy";
			System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");
			DataSet ds = oPayment.GetPageCollectionByLoginId(oParameter.sLoginId,
															oParameter.sReportDayFrom,
															oParameter.sReportDayTo,
															oParameter.sPaymentType,
															oParameter.sProductionSeq,
															oParameter.sManagerSeq,
															oParameter.sPaymentMethod,
															0,
															SysConst.DB_MAX_ROWS);
			string sCsvRec = GetCsvRec(ds);

			Response.BinaryWrite(encoding.GetBytes(sCsvRec));
			Response.End();

		}
	}
	protected void btnSendMail_Click(object sender,EventArgs e) {
		pnlSendMail.Visible = true;
		pnlSendMail.Enabled = true;
		pnlKey.Enabled = false;
		lblSendMailSuccess.Visible = false;
	}
	protected void btnSendMailCancel_Click(object sender,EventArgs e) {
		pnlSendMail.Visible = false;
		pnlSendMail.Enabled = false;
		pnlKey.Enabled = true;
	}
	protected void btnSendMailCommit_Click(object sender,EventArgs e) {

		using (Site oSite = new Site())
		using (PaymentHistory oPaymentHistory = new PaymentHistory()) {
			oSite.GetOne(ViCommConst.CAST_SITE_CD);
			PaymentHistoryParameters oParameter = GetPaymentHistoryParameters();
			DataSet ds = oPaymentHistory.GetPageCollectionByLoginId(oParameter.sLoginId,
														oParameter.sReportDayFrom,
														oParameter.sReportDayTo,
														oParameter.sPaymentType,
														oParameter.sProductionSeq,
														oParameter.sManagerSeq,
														oParameter.sPaymentMethod,
														0,
														int.MaxValue);
			DataRow dr = null;

			string[] sUserSeq = new string[ds.Tables[0].Rows.Count];
			string[] sTotalPoint = new string[ds.Tables[0].Rows.Count];
			string[] sReportDayTimeTo = new string[ds.Tables[0].Rows.Count];
			string[] sCsvSeqNo = new string[ds.Tables[0].Rows.Count];

			for (int i = 0;i < ds.Tables[0].Rows.Count;i++) {
				dr = ds.Tables[0].Rows[i];
				
				int iTotalPoint = 0;
				if (!int.TryParse(dr["PAYMENT_AMT"].ToString(),out iTotalPoint)) {
					iTotalPoint = 0;
				}

				sUserSeq[i] = dr["USER_SEQ"].ToString();
				sTotalPoint[i] = ((int)Math.Max(0,iTotalPoint - oSite.transferFee)).ToString();
				sReportDayTimeTo[i] = dr["PAYMENT_CLOSE_DAY"].ToString().Replace("/",string.Empty) + dr["PAYMENT_CLOSE_HOUR"].ToString();
				sCsvSeqNo[i] = dr["CSV_SEQ_NO"].ToString();
			}

			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("TX_PAYOUT_TO_CAST_MAIL");
				db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,ViCommConst.CAST_SITE_CD);
				db.ProcedureInParm("PMAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,lstMailTemplateNo.SelectedValue);
				db.ProcedureInArrayParm("PCAST_USER_SEQ",DbSession.DbType.VARCHAR2,ds.Tables[0].Rows.Count,sUserSeq);
				db.ProcedureInParm("PCAST_USER_COUNT",DbSession.DbType.NUMBER,ds.Tables[0].Rows.Count);
				db.ProcedureInArrayParm("PGENERAL1",DbSession.DbType.VARCHAR2,ds.Tables[0].Rows.Count,sTotalPoint);
				db.ProcedureInArrayParm("PREPORT_DAY_TIME_TO",DbSession.DbType.VARCHAR2,ds.Tables[0].Rows.Count,sReportDayTimeTo);
				db.ProcedureInArrayParm("PCSV_SEQ_NO",DbSession.DbType.VARCHAR2,ds.Tables[0].Rows.Count,sCsvSeqNo);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
			}
		}

		pnlSendMail.Visible = false;
		pnlKey.Enabled = true;
		lblSendMailSuccess.Visible = true;
	}
	protected string GetCsvRec(DataSet pDataSet) {
		bool bStaffIdFlag = false;
		bool bValidateAccountFlag = false;
		bool bBankTypeRadio = false;
		string sTransferFee = string.Empty;
		int iTransferFee = 0;

		// Available Serive?
		using (ManageCompany oCompany = new ManageCompany()) {
			bStaffIdFlag = oCompany.IsAvailableService(ViCommConst.RELEASE_SUPPORT_STAFF_ID);
			bValidateAccountFlag = oCompany.IsAvailableService(ViCommConst.RELEASE_VALIDATE_ACCOUNT_INFO);
			bBankTypeRadio = oCompany.IsAvailableService(ViCommConst.RELEASE_BANK_TYPE_RADIO);
		}

		using (Site oSite = new Site()) {
			oSite.GetValue(ViCommConst.CAST_SITE_CD,"TRANSFER_FEE",ref sTransferFee);
		}
		int.TryParse(sTransferFee,out iTransferFee);

		string sCsv = string.Empty;
		sCsv += "ログインID,";
		sCsv += "氏名,";
		if (bStaffIdFlag) {
			sCsv += "事業者コード,";
		}
		sCsv += "金額合計,";
		if (iTransferFee != 0) {
			sCsv += "金額-手数料,";
		}
		sCsv += "銀行名,";
		sCsv += "支店名,";
		sCsv += "支店名カナ,";
		sCsv += "口座タイプ,";
		sCsv += "口座番号,";
		if (bValidateAccountFlag) {
			sCsv += "口座名義,";
			sCsv += "口座状態";
		} else {
			sCsv += "口座名義";
		}
		sCsv += "\r\n";

		DataRow dr;

		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];

			sCsv += dr["LOGIN_ID"] + ",";
			sCsv += dr["CAST_NM"] + ",";
			if (bStaffIdFlag) {
				sCsv += dr["STAFF_ID"] + ",";
			}
			sCsv += dr["PAYMENT_AMT"] + ",";
			if (iTransferFee != 0) {
				sCsv += (int.Parse(dr["PAYMENT_AMT"].ToString()) - iTransferFee).ToString() + ",";
			}
			sCsv += dr["BANK_NM"] + ",";
			sCsv += dr["BANK_OFFICE_NM"] + ",";
			sCsv += dr["BANK_OFFICE_KANA_NM"] + ",";
			if (bBankTypeRadio) {
				if (dr["BANK_ACCOUNT_TYPE2"].Equals(ViCommConst.BankAccountType.NORMAL_ACCOUNT)) {
					sCsv += "普通,";
				} else if (dr["BANK_ACCOUNT_TYPE2"].Equals(ViCommConst.BankAccountType.CHECKING_ACCOUNT)) {
					sCsv += "当座,";
				} else if (dr["BANK_ACCOUNT_TYPE2"].Equals(ViCommConst.BankAccountType.SAVINGS_ACCOUNTS)) {
					sCsv += "貯蓄,";
				} else {
					sCsv += ",";
				}
			} else {
				sCsv += dr["BANK_ACCOUNT_TYPE"] + ",";
			}
			sCsv += dr["BANK_ACCOUNT_NO"] + ",";
			if (bValidateAccountFlag) {
				sCsv += dr["BANK_ACCOUNT_HOLDER_NM"] + ",";
				if (dr["BANK_ACCOUNT_INVALID_FLAG"].ToString().Equals("1")) {
					sCsv += "×";
				}
			} else {
				sCsv += dr["BANK_ACCOUNT_HOLDER_NM"];
			}
			sCsv += "\r\n";
		}
		return sCsv;
	}
	protected void InitPage() {
		ClearField();

		// 支払方法の選択肢を設定
		GiftCode oGiftCode = new GiftCode();
		oGiftCode.SetPaymentMethod(lstPaymentMethod);

		lstPaymentType.DataBind();
		lstPaymentType.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		lstPaymentType.DataSourceID = string.Empty;
		lstPaymentType.SelectedIndex = 0;
		lstProductionSeq.DataBind();
		lstProductionSeq.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		lstProductionSeq.DataSourceID = string.Empty;
		lstManager.DataBind();
		lstManager.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		lstManager.DataSourceID = string.Empty;

		// パラメータによる初期値設定
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["pdate"]))) {
			// 支払日時
			DateTime oDateTime = new DateTime();
			if (DateTime.TryParse(iBridUtil.GetStringValue(Request.QueryString["pdate"]),out oDateTime)) {
				txtReportDayFrom.Text = oDateTime.ToString("yyyy/MM/dd");
				txtReportDayTo.Text = txtReportDayFrom.Text;
			}
		}
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["pmethod"]))) {
			// 支払方法
			lstPaymentMethod.SelectedValue = iBridUtil.GetStringValue(Request.QueryString["pmethod"]);
		}
	}
	protected void ClearField() {
		totalAmt = 0;
		txtLoginId.Text = "";
		txtReportDayFrom.Text = DateTime.Now.ToString("yyyy/MM/dd");
		txtReportDayTo.Text = DateTime.Now.ToString("yyyy/MM/dd");
		lstPaymentMethod.SelectedIndex = 0;
		lstPaymentType.SelectedIndex = 0;
		pnlInfo.Visible = false;
		btnCsv.Visible = false;
		btnSendMail.Visible = false;
		pnlSendMail.Visible = false;
		lblSendMailSuccess.Visible = false;
	}
	protected string GetTotalAmount() {
		using (PaymentHistory oPayment = new PaymentHistory()) {
			PaymentHistoryParameters oParameter = GetPaymentHistoryParameters();
			totalAmt = oPayment.GetTotalAmount(oParameter.sLoginId,
												oParameter.sReportDayFrom,
												oParameter.sReportDayTo,
												oParameter.sPaymentType,
												oParameter.sProductionSeq,
												oParameter.sManagerSeq,
												oParameter.sPaymentMethod);
		}
		return totalAmt.ToString();
	}

	protected string GetTotalCount() {
		return recCount.ToString();
	}
	protected string GetRecCount() {
		return recCount;
	}

	protected void dsPaymentHistory_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		PaymentHistoryParameters oParameter = GetPaymentHistoryParameters();
		e.InputParameters[0] = oParameter.sLoginId;
		e.InputParameters[1] = oParameter.sReportDayFrom;
		e.InputParameters[2] = oParameter.sReportDayTo;
		e.InputParameters[3] = oParameter.sPaymentType;
		e.InputParameters[4] = oParameter.sProductionSeq;
		e.InputParameters[5] = oParameter.sManagerSeq;
		e.InputParameters[6] = oParameter.sPaymentMethod;
	}
	protected void dsPaymentHistory_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsManager_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {

		if (lstProductionSeq.Items.Count == 0) {
			e.InputParameters[0] = "";
		} else if (lstProductionSeq.Items.Count > 0 && lstProductionSeq.SelectedIndex == 0 && !string.IsNullOrEmpty(lstProductionSeq.Items[0].Value)) {
			e.InputParameters[0] = "";
		} else {
			e.InputParameters[0] = lstProductionSeq.SelectedValue;
		}
	}
	protected void lstProductionSeq_SelectedIndexChanged(object sender,EventArgs e) {
		lstManager.DataSourceID = "dsManager";
		lstManager.DataBind();
		lstManager.Items.Insert(0,new ListItem("",""));
		lstManager.DataSourceID = "";
	}

	protected PaymentHistoryParameters GetPaymentHistoryParameters() {
		PaymentHistoryParameters oParamters = new PaymentHistoryParameters();
		oParamters.sLoginId = txtLoginId.Text.TrimEnd();
		oParamters.sReportDayFrom = txtReportDayFrom.Text.ToString();
		if (txtReportDayTo.Text.Equals(string.Empty)) {
			txtReportDayTo.Text = txtReportDayFrom.Text.ToString();
		}
		oParamters.sReportDayTo = txtReportDayTo.Text.ToString();
		oParamters.sPaymentType = lstPaymentType.SelectedValue;
		oParamters.sProductionSeq = lstProductionSeq.SelectedValue;
		oParamters.sManagerSeq = lstManager.SelectedValue;
		oParamters.sPaymentMethod = lstPaymentMethod.SelectedValue;

		return oParamters;

	}
	public class PaymentHistoryParameters {

		public string sLoginId;
		public string sReportDayFrom;
		public string sReportDayTo;
		public string sPaymentMethod;
		public string sPaymentType;
		public string sProductionSeq;
		public string sManagerSeq;
		public PaymentHistoryParameters() {
			this.sLoginId = string.Empty;
			this.sReportDayFrom = string.Empty;
			this.sReportDayTo = string.Empty;
			this.sPaymentMethod = string.Empty;
			this.sPaymentType = string.Empty;
			this.sProductionSeq = string.Empty;
			this.sManagerSeq = string.Empty;
		}

	}
}
