﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者別稼動集計
--	Progaram ID		: CastPerformance
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.IO;
using iBridCommLib;
using ViComm;

public partial class Status_CastPerformance:System.Web.UI.Page {
	private Stream filter;
	private DataSet _dataSource = null;


	public string SiteCd {
		get {
			return Session["SiteCd"] as string ?? string.Empty;
		}
		private set {
			Session["SiteCd"] = value;
		}
	}
	public string MenuSite {
		get {
			return Session["MENU_SITE"] as string ?? string.Empty;
		}
		private set {
			Session["MENU_SITE"] = value;
		}
	}
	public string ProductionSeq {
		get {
			return Session["PRODUCTION_SEQ"] as string ?? string.Empty;
		}
		private set {
			Session["PRODUCTION_SEQ"] = value;
		}
	}
	public string ManagerSeq {
		get {
			return Session["MANAGER_SEQ"] as string ?? string.Empty;
		}
		private set {
			Session["MANAGER_SEQ"] = value;
		}
	}

	#region □■□　Page イベント □■□ ==========================================================

	protected void Page_Load(object sender,EventArgs e) {
		filter = Response.Filter;

		if (!IsPostBack) {

			InitPage();

			// ===========================
			// サイトDropDownListの初期化 
			// ===========================
			if (string.IsNullOrEmpty(this.SiteCd)) {
				lstSiteCd.Items.Insert(0,new ListItem(string.Empty,string.Empty));
			}
			lstSiteCd.DataSourceID = string.Empty;
			if (!string.IsNullOrEmpty(this.MenuSite)) {
				lstSiteCd.SelectedValue = this.MenuSite;
			}

			// ===========================
			// プロダクションDropDownListの初期化 
			// ===========================
			lstProductionSeq.Items.Insert(0,new ListItem(string.Empty,string.Empty));
			lstProductionSeq.DataSourceID = string.Empty;
			if (!string.IsNullOrEmpty(this.ProductionSeq)) {
				lstProductionSeq.SelectedValue = this.ProductionSeq;
				lstProductionSeq.Enabled = false;
			} else {
				lstProductionSeq.SelectedIndex = 0;
			}

			FirstLoad();
		}
		DisplayWordUtil.ReplaceGridColumnHeader(this.grdPerformace);
	}
	#endregion ====================================================================================


	#region □■□　Button イベント □■□ ========================================================

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			GetList();
		}
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
		if (string.IsNullOrEmpty(this.ProductionSeq)) {
			lstProductionSeq.SelectedIndex = 0;
		}
		if (string.IsNullOrEmpty(this.ManagerSeq)) {
			lstManager.SelectedIndex = 0;
		}
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			string fileName = ViCommConst.CSV_FILE_NM_CAST_TALK_LOG;

			Response.AddHeader("Content-Disposition","attachment;filename=" + fileName);
			Response.ContentType = "application/octet-stream-dummy";
			string sRegistFromDay = string.Empty;
			string sRegistToDay = string.Empty;

			if (!lstRegistFromYYYY.SelectedValue.Equals(string.Empty)) {
				sRegistFromDay = lstRegistFromYYYY.SelectedValue + "/" + lstRegistFromMM.SelectedValue + "/" + lstRegistFromDD.SelectedValue;
			}
			if (!lstRegistToYYYY.SelectedValue.Equals(string.Empty)) {
				sRegistToDay = lstRegistToYYYY.SelectedValue + "/" + lstRegistToMM.SelectedValue + "/" + lstRegistToDD.SelectedValue;
			}
			using (TimeOperation oTimePerformance = new TimeOperation()) {
				DataSet ds = oTimePerformance.GetList(
									lstSiteCd.SelectedValue,
									lstFromYYYY.SelectedValue,
									lstFromMM.SelectedValue,
									lstFromDD.SelectedValue,
									lstFromHH.SelectedValue,
									lstToYYYY.SelectedValue,
									lstToMM.SelectedValue,
									lstToDD.SelectedValue,
									lstToHH.SelectedValue,
									sRegistFromDay,
									sRegistToDay,
									txtLoginId.Text,
									lstProductionSeq.SelectedValue,
									lstManager.SelectedValue,
									lstAdGroup.SelectedValue,
									lstAd.SelectedValue,
									true
								);

				WriteCsvString(ds);
				Response.End();
			}
		}
	}

	#endregion ====================================================================================


	#region □■□　GridView イベント □■□ ======================================================

	protected void grdPerformace_DataBound(object sender,EventArgs e) {
		// 合計行の出力



		this.DisplaySummaryRow();
	}
	protected void grdPerformace_Sorting(object sender,GridViewSortEventArgs e) {
		if (e.SortDirection == SortDirection.Ascending) {
			ViewState["SORT"] = e.SortExpression + " ASC";
		} else {
			ViewState["SORT"] = e.SortExpression + " DESC";
		}
	}
	protected void grdPerformace_Load(object sender,EventArgs e) {
		this.InitializeGridViewColumns();
	}
	#endregion ====================================================================================


	#region □■□　ObjectDataSource イベント □■□ ==============================================

	protected void dsTimeOperation_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		string sRegistFromDay = string.Empty;
		string sRegistToDay = string.Empty;

		if (!lstRegistFromYYYY.SelectedValue.Equals(string.Empty)) {
			sRegistFromDay = lstRegistFromYYYY.SelectedValue + "/" + lstRegistFromMM.SelectedValue + "/" + lstRegistFromDD.SelectedValue;
		}
		if (!lstRegistToYYYY.SelectedValue.Equals(string.Empty)) {
			sRegistToDay = lstRegistToYYYY.SelectedValue + "/" + lstRegistToMM.SelectedValue + "/" + lstRegistToDD.SelectedValue;
		}
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = lstFromYYYY.SelectedValue;
		e.InputParameters[2] = lstFromMM.SelectedValue;
		e.InputParameters[3] = lstFromDD.SelectedValue;
		e.InputParameters[4] = lstFromHH.SelectedValue;
		e.InputParameters[5] = lstToYYYY.SelectedValue;
		e.InputParameters[6] = lstToMM.SelectedValue;
		e.InputParameters[7] = lstToDD.SelectedValue;
		e.InputParameters[8] = lstToHH.SelectedValue;
		e.InputParameters[9] = sRegistFromDay;
		e.InputParameters[10] = sRegistToDay;
		e.InputParameters[11] = txtLoginId.Text;
		e.InputParameters[12] = lstAdGroup.SelectedValue;
		e.InputParameters[13] = lstAd.SelectedValue;
		e.InputParameters[14] = lstProductionSeq.SelectedValue;
		e.InputParameters[15] = lstManager.SelectedValue;
		e.InputParameters[16] = true;
	}

	protected void dsTimeOperation_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue is DataSet) {
			this._dataSource = (DataSet)e.ReturnValue;
			this.lblTotalRecordCount.Text = "0";
			if (this._dataSource.Tables.Count > 0) {
				this.lblTotalRecordCount.Text = this._dataSource.Tables[0].Rows.Count.ToString();
			}
		}
	}

	protected void dsAdGroup_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
	}

	protected void dsAdGroupMember_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = lstAdGroup.SelectedValue;
	}

	#endregion ====================================================================================


	protected void lstAdGroup_SelectedIndexChanged(object sender,EventArgs e) {
		ChanegAdMember();
	}

	protected void lstAd_SelectedIndexChanged(object sender,EventArgs e) {
	}

	protected void lstSiteCd_SelectedIndexChanged(object sender,EventArgs e) {
		SetAdGroupCd();
		ChanegAdMember();
	}

	protected void dsManager_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {

		if (lstProductionSeq.Items.Count == 0) {
			e.InputParameters[0] = "";
		} else if (lstProductionSeq.Items.Count > 0 && lstProductionSeq.SelectedIndex == 0 && !string.IsNullOrEmpty(lstProductionSeq.Items[0].Value)) {
			e.InputParameters[0] = "";
		} else {
			e.InputParameters[0] = lstProductionSeq.SelectedValue;
		}
	}
	
	protected void lstProductionSeq_SelectedIndexChanged(object sender,EventArgs e) {
		lstManager.DataSourceID = "dsManager";
		lstManager.DataBind();
		lstManager.Items.Insert(0,new ListItem("",""));
		lstManager.DataSourceID = "";
	}

	private void FirstLoad() {
		string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		string sAdGroupCd = iBridUtil.GetStringValue(Request.QueryString["adgroup"]);
		string sAdCd = iBridUtil.GetStringValue(Request.QueryString["adcd"]);
		string sFrom = iBridUtil.GetStringValue(Request.QueryString["from"]);
		string sTo = iBridUtil.GetStringValue(Request.QueryString["to"]);

		if (!sSiteCd.Equals(string.Empty)) {
			lstSiteCd.SelectedValue = sSiteCd;
		}
		lstSiteCd.DataSourceID = string.Empty;

		SetAdGroupCd();
		if (!sAdGroupCd.Equals(string.Empty) && !sAdGroupCd.Equals(ViCommConst.DEFAUL_AD_GROUP_CD)) {
			lstAdGroup.SelectedValue = sAdGroupCd;
		} else {
			lstAdGroup.SelectedIndex = 0;
		}

		ChanegAdMember();
		if (!sAdCd.Equals(string.Empty)) {
			lstAd.SelectedValue = sAdCd;
		} else {
			lstAd.SelectedIndex = 0;
		}

		if (!sFrom.Equals(string.Empty)) {
			string[] sArrFrom = sFrom.Split('/');
			lstFromYYYY.SelectedValue = sArrFrom[0];
			lstFromMM.SelectedValue = sArrFrom[1];
			lstFromDD.SelectedValue = sArrFrom[2];
		}
		if (!sTo.Equals(string.Empty)) {
			string[] sArrTo = sTo.Split('/');
			lstToYYYY.SelectedValue = sArrTo[0];
			lstToMM.SelectedValue = sArrTo[1];
			lstToDD.SelectedValue = sArrTo[2];
		}

		if (!sSiteCd.Equals(string.Empty)) {
			GetList();
		}
	}

	private void InitPage() {

		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);

		grdPerformace.DataSourceID = string.Empty;
		pnlInfo.Visible = false;
		ClearField();
		DataBind();
		SetAdGroupCd();
		ChanegAdMember();
		if (!IsPostBack) {
			SysPrograms.SetupFromToDayTime(lstFromYYYY,lstFromMM,lstFromDD,lstFromHH,lstToYYYY,lstToMM,lstToDD,lstToHH,true);
			lstFromYYYY.SelectedIndex = 0;
			lstToYYYY.SelectedIndex = 0;
			lstFromMM.SelectedValue = DateTime.Now.ToString("MM");
			lstToMM.SelectedValue = DateTime.Now.ToString("MM");
			lstFromDD.SelectedValue = DateTime.Now.ToString("dd");
			lstToDD.SelectedValue = DateTime.Now.ToString("dd");
			lstFromHH.SelectedIndex = 0;
			lstToHH.SelectedIndex = 0;
			SysPrograms.SetupFromToDay(lstRegistFromYYYY,lstRegistFromMM,lstRegistFromDD,lstRegistToYYYY,lstRegistToMM,lstRegistToDD,false);
			lstRegistFromYYYY.Items.Insert(0,new ListItem("----",""));
			lstRegistFromMM.Items.Insert(0,new ListItem("---",""));
			lstRegistFromDD.Items.Insert(0,new ListItem("---",""));
			lstRegistFromYYYY.SelectedIndex = 0;
			lstRegistFromMM.SelectedIndex = 0;
			lstRegistFromDD.SelectedIndex = 0;
			lstRegistToYYYY.Items.Insert(0,new ListItem("----",""));
			lstRegistToMM.Items.Insert(0,new ListItem("---",""));
			lstRegistToDD.Items.Insert(0,new ListItem("---",""));
			lstRegistToYYYY.SelectedIndex = 0;
			lstRegistToMM.SelectedIndex = 0;
			lstRegistToDD.SelectedIndex = 0;

			lstManager.Items.Insert(0,new ListItem(string.Empty,string.Empty));
			lstManager.DataSourceID = string.Empty;
			if (!string.IsNullOrEmpty(this.ManagerSeq)) {
				lstManager.SelectedValue = this.ManagerSeq;
				lstManager.Enabled = false;
			} else {
				lstManager.SelectedIndex = 0;
			}
		}
	}

	private void ClearField() {
		btnCsv.Visible = false;
		this.lblTotalRecordCount.Text = "0";
	}


	private void GetList() {
		if (!chkCount.Checked && !chkPoint.Checked && !chkPayPoint.Checked) {
			chkCount.Checked = true;
			chkPoint.Checked = true;
			chkPayPoint.Checked = true;
		}
		this.ClearField();
		this.grdPerformace.AllowPaging = !this.chkPagingOff.Checked;
		this.grdPerformace.DataBind();
		int iCompare = string.Compare(Session["AdminType"].ToString(),ViCommConst.RIGHT_SITE_MANAGER);
		btnCsv.Visible = (iCompare >= 0);
		pnlInfo.Visible = true;
	}

	public static bool IsAvailableService(ulong pService) {
		using (ManageCompany oInstance = new ManageCompany()) {
			return oInstance.IsAvailableService(pService);
		}
	}

	private void WriteCsvString(DataSet pDataSet) {
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");
		int iCompanyMask = int.Parse(Session["CompanyMask"].ToString());
		bool bReleaseSaleMovie = IsAvailableService(ViCommConst.RELEASE_SALE_MOVIE);
		bool bReleaseTimeCall = IsAvailableService(ViCommConst.RELEASE_TIME_CALL);
		bool bReleaseSaleVoice = IsAvailableService(ViCommConst.RELEASE_SALE_VOICE);
		bool bReleaseSocialGame = true;

		string sCsv = string.Empty;
		sCsv += "ＩＤ番号,";
		sCsv += "氏名,";

		sCsv += "TV分数,";
		sCsv += "TV稼動Pt,";
		sCsv += "TV支払Pt,";
		sCsv += "TVﾁｬｯﾄ分数,";
		sCsv += "TVﾁｬｯﾄ稼動Pt,";
		sCsv += "TVﾁｬｯﾄ支払Pt,";
		sCsv += "音声分数,";
		sCsv += "音声稼動Pt,";
		sCsv += "音声支払Pt,";
		sCsv += "音声ﾁｬｯﾄ分数,";
		sCsv += "音声ﾁｬｯﾄ稼動Pt,";
		sCsv += "音声ﾁｬｯﾄ支払Pt,";
		sCsv += "会話ﾓﾆﾀ分数,";
		sCsv += "会話ﾓﾆﾀ稼動Pt,";
		sCsv += "会話ﾓﾆﾀ支払Pt,";
		sCsv += "音声ﾓﾆﾀ分数,";
		sCsv += "音声ﾓﾆﾀ稼動Pt,";
		sCsv += "音声ﾓﾆﾀ支払Pt,";
		sCsv += "部屋ﾓﾆﾀ分数,";
		sCsv += "部屋ﾓﾆﾀ稼動Pt,";
		sCsv += "部屋ﾓﾆﾀ支払Pt,";
		sCsv += "ﾗｲﾌﾞ視聴分数,";
		sCsv += "ﾗｲﾌﾞ視聴稼動Pt,";
		sCsv += "ﾗｲﾌﾞ視聴支払Pt,";
		sCsv += "男性待機TV分数,";
		sCsv += "男性待機TV稼動Pt,";
		sCsv += "男性待機TV支払Pt,";
		sCsv += "男性待機TVﾁｬｯﾄ分数,";
		sCsv += "男性待機TVﾁｬｯﾄ稼動Pt,";
		sCsv += "男性待機TVﾁｬｯﾄ支払Pt,";
		sCsv += "男性待機音声分数,";
		sCsv += "男性待機音声稼動Pt,";
		sCsv += "男性待機音声支払Pt,";
		sCsv += "男性待機音声ﾁｬｯﾄ分数,";
		sCsv += "男性待機音声ﾁｬｯﾄ稼動Pt,";
		sCsv += "男性待機音声ﾁｬｯﾄ支払Pt,";
		sCsv += "電話分数計,";
		sCsv += "電話稼動Pt計,";
		sCsv += "電話支払Pt計,";
		sCsv += "ﾒｰﾙ受信件数,";
		sCsv += "ﾒｰﾙ受信稼動Pt,";
		sCsv += "ﾒｰﾙ受信支払Pt,";
		sCsv += "写真添付ﾒｰﾙ受信件数,";
		sCsv += "写真添付ﾒｰﾙ稼動Pt,";
		sCsv += "写真添付ﾒｰﾙ支払Pt,";
		sCsv += "動画添付ﾒｰﾙ受信件数,";
		sCsv += "動画添付ﾒｰﾙ稼動Pt,";
		sCsv += "動画添付ﾒｰﾙ支払Pt,";
		sCsv += "ﾒｰﾙ添付写真閲覧件数,";
		sCsv += "ﾒｰﾙ添付写真閲覧稼動Pt,";
		sCsv += "ﾒｰﾙ添付写真閲覧支払Pt,";
		sCsv += "ﾒｰﾙ添付動画閲覧件数,";
		sCsv += "ﾒｰﾙ添付動画閲覧稼動Pt,";
		sCsv += "ﾒｰﾙ添付動画閲覧支払Pt,";
		sCsv += "BBS写真閲覧件数,";
		sCsv += "BBS写真閲覧稼動Pt,";
		sCsv += "BBS写真閲覧支払Pt,";
		sCsv += "BBS動画閲覧件数,";
		sCsv += "BBS動画閲覧稼動Pt,";
		sCsv += "BBS動画閲覧支払Pt,";
		if (bReleaseSaleMovie) {
			sCsv += "ﾀﾞｳﾝﾛｰﾄﾞﾑｰﾋﾞｰ件数,";
			sCsv += "ﾀﾞｳﾝﾛｰﾄﾞﾑｰﾋﾞｰ稼動Pt,";
			sCsv += "ﾀﾞｳﾝﾛｰﾄﾞﾑｰﾋﾞｰ支払Pt,";
		}
		if (bReleaseTimeCall) {
			sCsv += "定時ｺｰﾙ件数,";
			sCsv += "定時ｺｰﾙ稼動Pt,";
			sCsv += "定時ｺｰﾙ支払Pt,";
		}
		if (bReleaseSaleVoice) {
			sCsv += "ﾀﾞｳﾝﾛｰﾄﾞﾎﾞｲｽ件数,";
			sCsv += "ﾀﾞｳﾝﾛｰﾄﾞﾎﾞｲｽ稼動Pt,";
			sCsv += "ﾀﾞｳﾝﾛｰﾄﾞﾎﾞｲｽ支払Pt,";
		}
		if (bReleaseSocialGame) {
			sCsv += "ｿｰｼｬﾙｹﾞｰﾑ件数,";
			sCsv += "ｿｰｼｬﾙｹﾞｰﾑ稼動Pt,";
			sCsv += "ｿｰｼｬﾙｹﾞｰﾑ支払Pt,";
		}
		sCsv += "野球拳/FC会費件数,";
		sCsv += "野球拳/FC会費稼動Pt,";
		sCsv += "野球拳/FC会費支払Pt,";
		sCsv += "ﾌﾟﾚｾﾞﾝﾄﾒｰﾙ件数,";
		sCsv += "ﾌﾟﾚｾﾞﾝﾄﾒｰﾙ稼動Pt,";
		sCsv += "ﾌﾟﾚｾﾞﾝﾄﾒｰﾙ支払Pt,";
		sCsv += "WEB利用件数計,";
		sCsv += "WEB利用稼動Pt計,";
		sCsv += "WEB利用支払Pt計,";
		sCsv += "合計稼動Pt,";
		sCsv += "合計支払Pt";
		sCsv += "\r\n";

		sCsv = DisplayWordUtil.Replace(sCsv);

		Response.BinaryWrite(encoding.GetBytes(sCsv));

		DataRow dr = null;
		int iTotalPoint,iTotalPayPoint;

		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];
			sCsv = string.Empty;

			iTotalPoint = int.Parse(dr["TALK_POINT"].ToString()) + int.Parse(dr["WEB_TOTAL_POINT"].ToString());
			iTotalPayPoint = int.Parse(dr["TALK_PAY_POINT"].ToString()) + int.Parse(dr["WEB_TOTAL_PAY_POINT"].ToString());

			sCsv += dr["LOGIN_ID"] + ",";
			sCsv += dr["CAST_NM"] + ",";

			sCsv += dr["PRV_TV_TALK_MIN"] + ",";
			sCsv += dr["PRV_TV_TALK_POINT"] + ",";
			sCsv += dr["PRV_TV_TALK_PAY_POINT"] + ",";
			sCsv += dr["PUB_TV_TALK_MIN"] + ",";
			sCsv += dr["PUB_TV_TALK_POINT"] + ",";
			sCsv += dr["PUB_TV_TALK_PAY_POINT"] + ",";
			sCsv += dr["PRV_VOICE_TALK_MIN"] + ",";
			sCsv += dr["PRV_VOICE_TALK_POINT"] + ",";
			sCsv += dr["PRV_VOICE_TALK_PAY_POINT"] + ",";
			sCsv += dr["PUB_VOICE_TALK_MIN"] + ",";
			sCsv += dr["PUB_VOICE_TALK_POINT"] + ",";
			sCsv += dr["PUB_VOICE_TALK_PAY_POINT"] + ",";
			sCsv += dr["VIEW_TALK_MIN"] + ",";
			sCsv += dr["VIEW_TALK_POINT"] + ",";
			sCsv += dr["VIEW_TALK_PAY_POINT"] + ",";
			sCsv += dr["WIRETAP_MIN"] + ",";
			sCsv += dr["WIRETAP_POINT"] + ",";
			sCsv += dr["WIRETAP_PAY_POINT"] + ",";
			sCsv += dr["VIEW_BROADCAST_MIN"] + ",";
			sCsv += dr["VIEW_BROADCAST_POINT"] + ",";
			sCsv += dr["VIEW_BROADCAST_PAY_POINT"] + ",";
			sCsv += dr["LIVE_MIN"] + ",";
			sCsv += dr["LIVE_POINT"] + ",";
			sCsv += dr["LIVE_PAY_POINT"] + ",";
			sCsv += dr["CAST_PRV_TV_TALK_MIN"] + ",";
			sCsv += dr["CAST_PRV_TV_TALK_POINT"] + ",";
			sCsv += dr["CAST_PRV_TV_TALK_PAY_POINT"] + ",";
			sCsv += dr["CAST_PUB_TV_TALK_MIN"] + ",";
			sCsv += dr["CAST_PUB_TV_TALK_POINT"] + ",";
			sCsv += dr["CAST_PUB_TV_TALK_PAY_POINT"] + ",";
			sCsv += dr["CAST_PRV_VOICE_TALK_MIN"] + ",";
			sCsv += dr["CAST_PRV_VOICE_TALK_POINT"] + ",";
			sCsv += dr["CAST_PRV_VOICE_TALK_PAY_POINT"] + ",";
			sCsv += dr["CAST_PUB_VOICE_TALK_MIN"] + ",";
			sCsv += dr["CAST_PUB_VOICE_TALK_POINT"] + ",";
			sCsv += dr["CAST_PUB_VOICE_TALK_PAY_POINT"] + ",";
			sCsv += dr["TALK_MIN"] + ",";
			sCsv += dr["TALK_POINT"] + ",";
			sCsv += dr["TALK_PAY_POINT"] + ",";
			sCsv += dr["USER_MAIL_COUNT"] + ",";
			sCsv += dr["USER_MAIL_POINT"] + ",";
			sCsv += dr["USER_MAIL_PAY_POINT"] + ",";
			sCsv += dr["USER_MAIL_PIC_COUNT"] + ",";
			sCsv += dr["USER_MAIL_PIC_POINT"] + ",";
			sCsv += dr["USER_MAIL_PIC_PAY_POINT"] + ",";
			sCsv += dr["USER_MAIL_MOVIE_COUNT"] + ",";
			sCsv += dr["USER_MAIL_MOVIE_POINT"] + ",";
			sCsv += dr["USER_MAIL_MOVIE_PAY_POINT"] + ",";
			sCsv += dr["OPEN_PIC_MAIL_COUNT"] + ",";
			sCsv += dr["OPEN_PIC_MAIL_POINT"] + ",";
			sCsv += dr["OPEN_PIC_MAIL_PAY_POINT"] + ",";
			sCsv += dr["OPEN_MOVIE_MAIL_COUNT"] + ",";
			sCsv += dr["OPEN_MOVIE_MAIL_POINT"] + ",";
			sCsv += dr["OPEN_MOVIE_MAIL_PAY_POINT"] + ",";
			sCsv += dr["OPEN_PIC_BBS_COUNT"] + ",";
			sCsv += dr["OPEN_PIC_BBS_POINT"] + ",";
			sCsv += dr["OPEN_PIC_BBS_PAY_POINT"] + ",";
			sCsv += dr["OPEN_MOVIE_BBS_COUNT"] + ",";
			sCsv += dr["OPEN_MOVIE_BBS_POINT"] + ",";
			sCsv += dr["OPEN_MOVIE_BBS_PAY_POINT"] + ",";
			if (bReleaseSaleMovie) {
				sCsv += dr["DOWNLOAD_MOVIE_COUNT"] + ",";
				sCsv += dr["DOWNLOAD_MOVIE_POINT"] + ",";
				sCsv += dr["DOWNLOAD_MOVIE_PAY_POINT"] + ",";
			}
			if (bReleaseTimeCall) {
				sCsv += dr["TIME_CALL_COUNT"] + ",";
				sCsv += dr["TIME_CALL_POINT"] + ",";
				sCsv += dr["TIME_CALL_PAY_POINT"] + ",";
			}
			if (bReleaseSaleVoice) {
				sCsv += dr["DOWNLOAD_VOICE_COUNT"] + ",";
				sCsv += dr["DOWNLOAD_VOICE_POINT"] + ",";
				sCsv += dr["DOWNLOAD_VOICE_PAY_POINT"] + ",";
			}
			if (bReleaseSocialGame) {
				sCsv += dr["SOCIAL_GAME_COUNT"] + ",";
				sCsv += dr["SOCIAL_GAME_POINT"] + ",";
				sCsv += dr["SOCIAL_GAME_PAY_POINT"] + ",";
			}
			sCsv += dr["YAKYUKEN_COUNT"] + ",";
			sCsv += dr["YAKYUKEN_POINT"] + ",";
			sCsv += dr["YAKYUKEN_PAY_POINT"] + ",";
			sCsv += dr["PRESENT_MAIL_COUNT"] + ",";
			sCsv += dr["PRESENT_MAIL_POINT"] + ",";
			sCsv += dr["PRESENT_MAIL_PAY_POINT"] + ",";
			sCsv += dr["WEB_TOTAL_COUNT"] + ",";
			sCsv += dr["WEB_TOTAL_POINT"] + ",";
			sCsv += dr["WEB_TOTAL_PAY_POINT"] + ",";
			sCsv += iTotalPoint.ToString() + ",";
			sCsv += iTotalPayPoint.ToString();
			sCsv += "\r\n";
			Response.BinaryWrite(encoding.GetBytes(sCsv));
		}
	}


	protected string CalcSum(object pSum1,object pSum2) {
		int iSum = int.Parse(pSum1.ToString()) + int.Parse(pSum2.ToString());
		return iSum.ToString();
	}

	/// <summary>
	/// GridViewの列を初期化する



	/// </summary>
	private void InitializeGridViewColumns() {
		foreach (DataControlField oColumn in this.grdPerformace.Columns) {
			oColumn.Visible = true;
		}

		if (this.rdoTel.Checked) {
			//男性待機 
			this.grdPerformace.Columns[14].Visible = false;
			this.grdPerformace.Columns[15].Visible = false;
			this.grdPerformace.Columns[16].Visible = false;
			this.grdPerformace.Columns[17].Visible = false;
			//WEB
			this.grdPerformace.Columns[19].Visible = false;
			this.grdPerformace.Columns[20].Visible = false;
			this.grdPerformace.Columns[21].Visible = false;
			this.grdPerformace.Columns[22].Visible = false;
			this.grdPerformace.Columns[23].Visible = false;
			this.grdPerformace.Columns[24].Visible = false;
			this.grdPerformace.Columns[25].Visible = false;
			this.grdPerformace.Columns[26].Visible = false;
			this.grdPerformace.Columns[27].Visible = false;
			this.grdPerformace.Columns[28].Visible = false;
			this.grdPerformace.Columns[29].Visible = false;
			this.grdPerformace.Columns[30].Visible = false;
			this.grdPerformace.Columns[31].Visible = false;
			this.grdPerformace.Columns[32].Visible = false;
			this.grdPerformace.Columns[33].Visible = false;
		} else if (this.rdoManTel.Checked) {
			//TEL
			this.grdPerformace.Columns[2].Visible = false;
			this.grdPerformace.Columns[3].Visible = false;
			this.grdPerformace.Columns[4].Visible = false;
			this.grdPerformace.Columns[5].Visible = false;
			this.grdPerformace.Columns[6].Visible = false;
			this.grdPerformace.Columns[7].Visible = false;
			this.grdPerformace.Columns[8].Visible = false;
			this.grdPerformace.Columns[9].Visible = false;
			this.grdPerformace.Columns[10].Visible = false;
			this.grdPerformace.Columns[11].Visible = false;
			this.grdPerformace.Columns[12].Visible = false;
			this.grdPerformace.Columns[13].Visible = false;
			//WEB
			this.grdPerformace.Columns[19].Visible = false;
			this.grdPerformace.Columns[20].Visible = false;
			this.grdPerformace.Columns[21].Visible = false;
			this.grdPerformace.Columns[22].Visible = false;
			this.grdPerformace.Columns[23].Visible = false;
			this.grdPerformace.Columns[24].Visible = false;
			this.grdPerformace.Columns[25].Visible = false;
			this.grdPerformace.Columns[26].Visible = false;
			this.grdPerformace.Columns[27].Visible = false;
			this.grdPerformace.Columns[28].Visible = false;
			this.grdPerformace.Columns[29].Visible = false;
			this.grdPerformace.Columns[30].Visible = false;
			this.grdPerformace.Columns[31].Visible = false;
			this.grdPerformace.Columns[32].Visible = false;
			this.grdPerformace.Columns[33].Visible = false;
		} else {
			//TEL
			this.grdPerformace.Columns[2].Visible = false;
			this.grdPerformace.Columns[3].Visible = false;
			this.grdPerformace.Columns[4].Visible = false;
			this.grdPerformace.Columns[5].Visible = false;
			this.grdPerformace.Columns[6].Visible = false;
			this.grdPerformace.Columns[7].Visible = false;
			this.grdPerformace.Columns[8].Visible = false;
			this.grdPerformace.Columns[9].Visible = false;
			this.grdPerformace.Columns[10].Visible = false;
			this.grdPerformace.Columns[11].Visible = false;
			this.grdPerformace.Columns[12].Visible = false;
			this.grdPerformace.Columns[13].Visible = false;
			//男性待機 
			this.grdPerformace.Columns[14].Visible = false;
			this.grdPerformace.Columns[15].Visible = false;
			this.grdPerformace.Columns[16].Visible = false;
			this.grdPerformace.Columns[17].Visible = false;
		}

		grdPerformace.Columns[10].Visible = false;
		grdPerformace.Columns[11].Visible = false;
		grdPerformace.Columns[12].Visible = false;
		grdPerformace.Columns[13].Visible = false;
		grdPerformace.Columns[26].Visible = false;
		grdPerformace.Columns[27].Visible = false;
		grdPerformace.Columns[28].Visible = IsAvailableService(ViCommConst.RELEASE_SALE_MOVIE);
		grdPerformace.Columns[29].Visible = IsAvailableService(ViCommConst.RELEASE_TIME_CALL);
		grdPerformace.Columns[30].Visible = IsAvailableService(ViCommConst.RELEASE_SALE_VOICE);
	}

	private void SetAdGroupCd() {
		lstAdGroup.DataSourceID = "dsAdGroup";
		lstAdGroup.Items.Clear();
		lstAdGroup.DataBind();
		lstAdGroup.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		lstAdGroup.DataSourceID = string.Empty;
	}

	private void ChanegAdMember() {
		lstAd.DataSourceID = "dsAdGroupMember";
		lstAd.Items.Clear();
		lstAd.DataBind();
		lstAd.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		lstAd.DataSourceID = string.Empty;
	}

	/// <summary>
	/// 合計行を表示する
	/// </summary>
	private void DisplaySummaryRow() {
		if (this.grdPerformace.Rows.Count == 0)
			return;

		if (_dataSource != null && _dataSource.Tables.Count > 0) {
			DataTable oDt = _dataSource.Tables[0];

			Dictionary<string,string> oSumFieldMap = new Dictionary<string,string>();
			// =================
			//  電話
			// =================
			oSumFieldMap.Add("lblPrvTvTalkMin","PRV_TV_TALK_MIN");
			oSumFieldMap.Add("lblPrvTvTalkPoint","PRV_TV_TALK_POINT");
			oSumFieldMap.Add("lblPrvTvTalkPayPoint","PRV_TV_TALK_PAY_POINT");
			oSumFieldMap.Add("lblPubTvTalkMin","PUB_TV_TALK_MIN");
			oSumFieldMap.Add("lblPubTvTalkPoint","PUB_TV_TALK_POINT");
			oSumFieldMap.Add("lblPubTvTalkPayPoint","PUB_TV_TALK_PAY_POINT");
			oSumFieldMap.Add("lblPrvVoiceTalkMin","PRV_VOICE_TALK_MIN");
			oSumFieldMap.Add("lblPrvVoiceTalkPoint","PRV_VOICE_TALK_POINT");
			oSumFieldMap.Add("lblPrvVoiceTalkPayPoint","PRV_VOICE_TALK_PAY_POINT");
			oSumFieldMap.Add("lblPubVoiceTalkMin","PUB_VOICE_TALK_MIN");
			oSumFieldMap.Add("lblPubVoiceTalkPoint","PUB_VOICE_TALK_POINT");
			oSumFieldMap.Add("lblPubVoiceTalkPayPoint","PUB_VOICE_TALK_PAY_POINT");
			oSumFieldMap.Add("lblViewTalkMin","VIEW_TALK_MIN");
			oSumFieldMap.Add("lblViewTalkPoint","VIEW_TALK_POINT");
			oSumFieldMap.Add("lblViewTalkPayPoint","VIEW_TALK_PAY_POINT");
			oSumFieldMap.Add("lblWiretapMin","WIRETAP_MIN");
			oSumFieldMap.Add("lblWiretapPoint","WIRETAP_POINT");
			oSumFieldMap.Add("lblWiretapPayPoint","WIRETAP_PAY_POINT");
			oSumFieldMap.Add("lblViewBroadcastMin","VIEW_BROADCAST_MIN");
			oSumFieldMap.Add("lblViewBroadcastPoint","VIEW_BROADCAST_POINT");
			oSumFieldMap.Add("lblViewBroadcastPayPoint","VIEW_BROADCAST_PAY_POINT");
			oSumFieldMap.Add("lblLiveMin","LIVE_MIN");
			oSumFieldMap.Add("lblLivePoint","LIVE_POINT");
			oSumFieldMap.Add("lblLivePayPoint","LIVE_PAY_POINT");
			oSumFieldMap.Add("lblPlayProfileMin","PLAY_PROFILE_MIN");
			oSumFieldMap.Add("lblPlayProfilePoint","PLAY_PROFILE_POINT");
			oSumFieldMap.Add("lblPlayProfilePayPoint","PLAY_PROFILE_PAY_POINT");
			oSumFieldMap.Add("lblRecProfileMin","REC_PROFILE_MIN");
			oSumFieldMap.Add("lblRecProfilePoint","REC_PROFILE_POINT");
			oSumFieldMap.Add("lblRecProfilePayPoint","REC_PROFILE_PAY_POINT");
			oSumFieldMap.Add("lblGpfTalkVoiceMin","GPF_TALK_VOICE_MIN");
			oSumFieldMap.Add("lblGpfTalkVoicePoint","GPF_TALK_VOICE_POINT");
			oSumFieldMap.Add("lblGpfTalkVoicePayPoint","GPF_TALK_VOICE_PAY_POINT");

			oSumFieldMap.Add("lblPlayPvMsgMin","PLAY_PV_MSG_MIN");
			oSumFieldMap.Add("lblPlayPvMsgPoint","PLAY_PV_MSG_POINT");
			oSumFieldMap.Add("lblPlayPvMsgPayPoint","PLAY_PV_MSG_PAY_POINT");

			oSumFieldMap.Add("lblCastPrvTvTalkMin","CAST_PRV_TV_TALK_MIN");
			oSumFieldMap.Add("lblCastPrvTvTalkPoint","CAST_PRV_TV_TALK_POINT");
			oSumFieldMap.Add("lblCastPrvTvTalkPayPoint","CAST_PRV_TV_TALK_PAY_POINT");

			oSumFieldMap.Add("lblCastPubTvTalkMin","CAST_PUB_TV_TALK_MIN");
			oSumFieldMap.Add("lblCastPubTvTalkPoint","CAST_PUB_TV_TALK_POINT");
			oSumFieldMap.Add("lblCastPubTvTalkPayPoint","CAST_PUB_TV_TALK_PAY_POINT");

			oSumFieldMap.Add("lblCastPrvVoiceTalkMin","CAST_PRV_VOICE_TALK_MIN");
			oSumFieldMap.Add("lblCastPrvVoiceTalkPoint","CAST_PRV_VOICE_TALK_POINT");
			oSumFieldMap.Add("lblCastPrvVoiceTalkPayPoint","CAST_PRV_VOICE_TALK_PAY_POINT");

			oSumFieldMap.Add("lblCastPubVoiceTalkMin","CAST_PUB_VOICE_TALK_MIN");
			oSumFieldMap.Add("lblCastPubVoiceTalkPoint","CAST_PUB_VOICE_TALK_POINT");
			oSumFieldMap.Add("lblCastPubVoiceTalkPayPoint","CAST_PUB_VOICE_TALK_PAY_POINT");
			// 電話計 
			oSumFieldMap.Add("lblTalkMin","TALK_MIN");
			oSumFieldMap.Add("lblTalkPoint","TALK_POINT");
			oSumFieldMap.Add("lblTalkPayPoint","TALK_PAY_POINT");

			// =================
			//  Web
			// =================
			oSumFieldMap.Add("lblUserMailCount","USER_MAIL_COUNT");
			oSumFieldMap.Add("lblUserMailPoint","USER_MAIL_POINT");
			oSumFieldMap.Add("lblUserMailPayPoint","USER_MAIL_PAY_POINT");
			oSumFieldMap.Add("lblUserMailPicCount","USER_MAIL_PIC_COUNT");
			oSumFieldMap.Add("lblUserMailPicPoint","USER_MAIL_PIC_POINT");
			oSumFieldMap.Add("lblUserMailPicPayPoint","USER_MAIL_PIC_PAY_POINT");
			oSumFieldMap.Add("lblUserMailMovieCount","USER_MAIL_MOVIE_COUNT");
			oSumFieldMap.Add("lblUserMailMoviePoint","USER_MAIL_MOVIE_POINT");
			oSumFieldMap.Add("lblUserMailMoviePayPoint","USER_MAIL_MOVIE_PAY_POINT");
			oSumFieldMap.Add("lblOpenPicMailCount","OPEN_PIC_MAIL_COUNT");
			oSumFieldMap.Add("lblOpenPicMailPoint","OPEN_PIC_MAIL_POINT");
			oSumFieldMap.Add("lblOpenPicMailPayPoint","OPEN_PIC_MAIL_PAY_POINT");
			oSumFieldMap.Add("lblOpenMovieMailCount","OPEN_MOVIE_MAIL_COUNT");
			oSumFieldMap.Add("lblOpenMovieMailPoint","OPEN_MOVIE_MAIL_POINT");
			oSumFieldMap.Add("lblOpenMovieMailPayPoint","OPEN_MOVIE_MAIL_PAY_POINT");
			oSumFieldMap.Add("lblOpenPicBbsCount","OPEN_PIC_BBS_COUNT");
			oSumFieldMap.Add("lblOpenPicBbsPoint","OPEN_PIC_BBS_POINT");
			oSumFieldMap.Add("lblOpenPicBbsPayPoint","OPEN_PIC_BBS_PAY_POINT");
			oSumFieldMap.Add("lblOpenMovieBbsCount","OPEN_MOVIE_BBS_COUNT");
			oSumFieldMap.Add("lblOpenMovieBbsPoint","OPEN_MOVIE_BBS_POINT");
			oSumFieldMap.Add("lblOpenMovieBbsPayPoint","OPEN_MOVIE_BBS_PAY_POINT");
			oSumFieldMap.Add("lblPrvMessageRxCount","PRV_MESSAGE_RX_COUNT");
			oSumFieldMap.Add("lblPrvMessageRxPoint","PRV_MESSAGE_RX_POINT");
			oSumFieldMap.Add("lblPrvMessageRxPayPoint","PRV_MESSAGE_RX_PAY_POINT");
			oSumFieldMap.Add("lblPrvProfileRxCount","PRV_PROFILE_RX_COUNT");
			oSumFieldMap.Add("lblPrvProfileRxPoint","PRV_PROFILE_RX_POINT");
			oSumFieldMap.Add("lblPrvProfileRxPayPoint","PRV_PROFILE_RX_PAY_POINT");
			oSumFieldMap.Add("lblDownloadMovieCount","DOWNLOAD_MOVIE_COUNT");
			oSumFieldMap.Add("lblDownloadMoviePoint","DOWNLOAD_MOVIE_POINT");
			oSumFieldMap.Add("lblDownloadMoviePayPoint","DOWNLOAD_MOVIE_PAY_POINT");
			oSumFieldMap.Add("lblTimeCallCount","TIME_CALL_COUNT");
			oSumFieldMap.Add("lblTimeCallPoint","TIME_CALL_POINT");
			oSumFieldMap.Add("lblTimeCallPayPoint","TIME_CALL_PAY_POINT");
			oSumFieldMap.Add("lblDownloadVoiceCount","DOWNLOAD_VOICE_COUNT");
			oSumFieldMap.Add("lblDownloadVoicePoint","DOWNLOAD_VOICE_POINT");
			oSumFieldMap.Add("lblDownloadVoicePayPoint","DOWNLOAD_VOICE_PAY_POINT");
			oSumFieldMap.Add("lblSocialGameCount","SOCIAL_GAME_COUNT");
			oSumFieldMap.Add("lblSocialGamePoint","SOCIAL_GAME_POINT");
			oSumFieldMap.Add("lblSocialGamePayPoint","SOCIAL_GAME_PAY_POINT");
			oSumFieldMap.Add("lblYakyukenCount","YAKYUKEN_COUNT");
			oSumFieldMap.Add("lblYakyukenPoint","YAKYUKEN_POINT");
			oSumFieldMap.Add("lblYakyukenPayPoint","YAKYUKEN_PAY_POINT");
			oSumFieldMap.Add("lblPresentMailCount","PRESENT_MAIL_COUNT");
			oSumFieldMap.Add("lblPresentMailPoint","PRESENT_MAIL_POINT");
			oSumFieldMap.Add("lblPresentMailPayPoint","PRESENT_MAIL_PAY_POINT");

			// WEB計 
			oSumFieldMap.Add("lblWebTotalCount","WEB_TOTAL_COUNT");
			oSumFieldMap.Add("lblWebTotalPoint","WEB_TOTAL_POINT");
			oSumFieldMap.Add("lblWebTotalPayPoint","WEB_TOTAL_PAY_POINT");

			foreach (string sSumFiledName in oSumFieldMap.Keys) {
				string sDataFieldName = oSumFieldMap[sSumFiledName];
				Label lblSumFiled = (Label)this.grdPerformace.FooterRow.FindControl(sSumFiledName);
				lblSumFiled.Text = oDt.Compute(string.Format("SUM({0})",sDataFieldName),string.Empty).ToString();
			}

			// 総計 
			Label lblTotalPoint = (Label)this.grdPerformace.FooterRow.FindControl("lblTotalPoint");
			Label lblTotalPayPoint = (Label)this.grdPerformace.FooterRow.FindControl("lblTotalPayPoint");

			lblTotalPoint.Text = oDt.Compute("SUM(TALK_POINT) + SUM(WEB_TOTAL_POINT)",string.Empty).ToString();
			lblTotalPayPoint.Text = oDt.Compute("SUM(TALK_PAY_POINT) + SUM(WEB_TOTAL_PAY_POINT)",string.Empty).ToString();

		}
	}

	protected string GetCastNm(object pCastNm,object pHandleNm) {


		System.Text.StringBuilder oStr = new System.Text.StringBuilder();
		oStr.AppendFormat("{0}<br />{1}",pCastNm.ToString(),pHandleNm.ToString());
		return oStr.ToString();

	}

	protected void vdcRegistDay_ServerValidate(object source,ServerValidateEventArgs args) {
		if (this.IsValid) {
			if (lstRegistFromYYYY.SelectedValue.Equals(string.Empty) &&
				lstRegistFromMM.SelectedValue.Equals(string.Empty) &&
				lstRegistFromDD.SelectedValue.Equals(string.Empty) &&
				lstRegistToYYYY.SelectedValue.Equals(string.Empty) &&
				lstRegistToMM.SelectedValue.Equals(string.Empty) &&
				lstRegistToDD.SelectedValue.Equals(string.Empty)) {
				//全て未選択ならばOK
			} else {
				DateTime dt;
				if (!DateTime.TryParse(string.Format("{0}/{1}/{2}",lstRegistFromYYYY.SelectedValue,lstRegistFromMM.SelectedValue,lstRegistFromDD.SelectedValue),out dt)) {
					args.IsValid = false;
				}
				if (!DateTime.TryParse(string.Format("{0}/{1}/{2}",lstRegistToYYYY.SelectedValue,lstRegistToMM.SelectedValue,lstRegistToDD.SelectedValue),out dt)) {
					args.IsValid = false;
				}
			}
		}
	}

	protected bool GetBrCount() {
		if (chkCount.Checked) {
			if (chkPoint.Checked || chkPayPoint.Checked) {
				return true;
			}
		}
		return false;
	}

	protected bool GetBrPoint() {
		if (chkPoint.Checked) {
			if (chkPayPoint.Checked) {
				return true;
			}
		}
		return false;
	}

	protected bool CheckedCount() {
		return chkCount.Checked;
	}

	protected bool CheckedPoint() {
		return chkPoint.Checked;
	}

	protected bool CheckedPayPoint() {
		return chkPayPoint.Checked;
	}
}
