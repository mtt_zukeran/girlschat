﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者待機履歴
--	Progaram ID		: CastLoginHistory
--
--  Creation Date	: 2010.05.12
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using iBridCommLib;
using ViComm;

public partial class Status_CastLoginHistory:Page {
	private Stream filter;

	protected void Page_Load(object sender,EventArgs e) {

		filter = Response.Filter;
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);

		if (!IsPostBack) {
			FisrtLoad();
			InitPage();
			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["PRODUCTION_SEQ"]))) {
				lstProductionSeq.SelectedValue = iBridUtil.GetStringValue(Session["PRODUCTION_SEQ"]);
				lstProductionSeq.Enabled = false;
			}
			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MANAGER_SEQ"]))) {
				lstManager.SelectedValue = iBridUtil.GetStringValue(Session["MANAGER_SEQ"]);
				lstManager.Enabled = false;
			}
		}
	}

	private void FisrtLoad() {
		grdLogin.PageSize = 100;
		lstSiteCd.DataBind();
		if (Session["SiteCd"].ToString().Equals("")) {
			lstSiteCd.Items.Insert(0,new ListItem("",""));
		}
		lstSiteCd.DataSourceID = "";
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
		if (lstProductionSeq.Enabled) {
			lstProductionSeq.SelectedIndex = 0;
		}
		if (lstManager.Enabled) {
			lstManager.SelectedIndex = 0;
		}
	}

	protected void dsLoginCharacter_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = CreateDateString(txtReportDayFrom.Text,lstReportTimeFrom.Text);

		if (txtReportDayTo.Text.Equals("")) {
			txtReportDayTo.Text = txtReportDayFrom.Text;
		}
		e.InputParameters[2] = CreateDateString(txtReportDayTo.Text,lstReportTimeTo.SelectedValue);
		if (rdoDetail.Checked) {
			e.InputParameters[3] = 0;
		} else {
			e.InputParameters[3] = 1;
		}
		e.InputParameters[4] = txtLoginId.Text;
		e.InputParameters[5] = txtHandelNm.Text;
		e.InputParameters[6] = lstProductionSeq.SelectedValue;
		e.InputParameters[7] = lstManager.SelectedValue;
	}

	private void InitPage() {
		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);
		grdLogin.PageSize = 1;
		if (!IsPostBack) {
			SysPrograms.SetupTime(lstReportTimeFrom,lstReportTimeTo);
			lstProductionSeq.DataBind();
			lstProductionSeq.Items.Insert(0,new ListItem(string.Empty,string.Empty));
			lstProductionSeq.DataSourceID = string.Empty;

			lstManager.DataBind();
			lstManager.Items.Insert(0,new ListItem(string.Empty,string.Empty));
			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MANAGER_SEQ"]))) {
				lstManager.DataSourceID = string.Empty;
				lstManager.SelectedValue = iBridUtil.GetStringValue(Session["MANAGER_SEQ"]);
				lstManager.Enabled = false;
			} else {
				lstManager.SelectedIndex = 0;
			}
		}
		pnlInfo.Visible = false;
		ClearField();
	}

	private void ClearField() {
		txtReportDayFrom.Text = DateTime.Now.ToString("yyyy/MM/dd");
		txtReportDayTo.Text = DateTime.Now.ToString("yyyy/MM/dd");
		lstReportTimeFrom.SelectedIndex = 0;
		lstReportTimeTo.SelectedIndex = 0;
		txtLoginId.Text = "";
		txtHandelNm.Text = "";
	}

	private void GetList() {
		if (rdoDetail.Checked) {
			grdLogin.PageSize = 100;
			grdLogin.Columns[0].Visible = true;
			grdLogin.Columns[1].Visible = true;
			grdLogin.Columns[4].Visible = false;
			grdLogin.Columns[5].Visible = false;
			grdLogin.Sort("LOGIN_SEQ",SortDirection.Descending);
		} else {
			grdLogin.PageSize = 99999;
			grdLogin.Columns[0].Visible = false;
			grdLogin.Columns[1].Visible = false;
			grdLogin.Columns[4].Visible = true;
			grdLogin.Columns[5].Visible = true;
			grdLogin.Sort("LOGIN_ID",SortDirection.Ascending);
		}

		if (chkPagingOff.Checked) {
			grdLogin.PageSize = 99999;
			grdLogin.AllowSorting = false;
		} else {
			grdLogin.PageSize = 100;
			grdLogin.AllowSorting = true;
		}

		grdLogin.DataSourceID = "dsLoginCharacter";
		grdLogin.DataBind();
		pnlInfo.Visible = true;
	}

	private string CreateDateString(string pDate,string pHh) {
		return string.Format("{0} {1}",pDate,pHh);
	}

	protected void rdoList_CheckedChanged(object sender,EventArgs e) {
		GetList();
	}

	protected void dsManager_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {

		if (lstProductionSeq.Items.Count == 0) {
			e.InputParameters[0] = "";
		} else if (lstProductionSeq.Items.Count > 0 && lstProductionSeq.SelectedIndex == 0 && !string.IsNullOrEmpty(lstProductionSeq.Items[0].Value)) {
			e.InputParameters[0] = "";
		} else {
			e.InputParameters[0] = lstProductionSeq.SelectedValue;
		}
	}

	protected void lstProductionSeq_SelectedIndexChanged(object sender,EventArgs e) {
		lstManager.DataSourceID = "dsManager";
		lstManager.DataBind();
		lstManager.Items.Insert(0,new ListItem("",""));
		lstManager.DataSourceID = "";
	}

	protected void grdLogin_Sorting(object sender,GridViewSortEventArgs e) {
//		e.SortDirection = SortDirection.Descending;
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		string sFileNm = string.Empty;
		
		if (!rdoSummay.Checked) {
			sFileNm = string.Format("CAST_LOGIN_HISTORY_DETAIL_{0}",lstSiteCd.SelectedValue);
		} else {
			sFileNm = string.Format("CAST_LOGIN_HISTORY_SUMMARY_{0}",lstSiteCd.SelectedValue);
		}
		
		Response.Filter = filter;
		Response.AddHeader("Content-Disposition",string.Format("attachment;filename={0}.CSV",sFileNm));
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		DataSet ds;
		using (LoginCharacter oLoginCharacter = new LoginCharacter()) {
			if (txtReportDayTo.Text.Equals("")) {
				txtReportDayTo.Text = txtReportDayFrom.Text;
			}
			
			ds = oLoginCharacter.GetSitePageCollection(
					lstSiteCd.SelectedValue,
					CreateDateString(txtReportDayFrom.Text,lstReportTimeFrom.Text),
					CreateDateString(txtReportDayTo.Text,lstReportTimeTo.SelectedValue),
					rdoDetail.Checked == true ? ViCommConst.FLAG_OFF : ViCommConst.FLAG_ON,
					txtLoginId.Text,
					txtHandelNm.Text,
					lstProductionSeq.SelectedValue,
					lstManager.SelectedValue,
					0,
					99999
				);
			
			if (!rdoSummay.Checked) {
				SetCsvData(ds);
			} else {
				SetCsvDataSummary(ds);
			}
			Response.End();
		}
	}

	public void SetCsvData(DataSet pDataSet) {

		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		string sHeaderString = "開始時間,終了時間,ﾊﾝﾄﾞﾙ名,合計(分),合計(回),TV(分),TV(回),音声(分),音声(回)\r\n";
		Response.BinaryWrite(encoding.GetBytes(sHeaderString));

		string sDtl = "";

		DataRow dr = null;
		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];

			sDtl = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8}",
							dr["START_DATE"].ToString(),
							dr["LOGOFF_DATE"].ToString(),
							dr["LOGIN_ID"].ToString(),
							dr["HANDLE_NM"].ToString(),
							dr["TOTAL_TALK_MIN"].ToString(),
							dr["TOTAL_TALK_COUNT"].ToString(),
							dr["PRV_TV_TALK_MIN"].ToString(),
							dr["PRV_TV_TALK_COUNT"].ToString(),
							dr["PRV_VOICE_TALK_MIN"].ToString(),
							dr["PRV_VOICE_TALK_COUNT"].ToString()
							);
			sDtl += "\r\n";
			Response.BinaryWrite(encoding.GetBytes(sDtl));
		}
	}

	public void SetCsvDataSummary(DataSet pDataSet) {

		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		string sHeaderString = "ﾛｸﾞｲﾝID,ﾊﾝﾄﾞﾙ名,待機分,ﾀﾞﾐｰ分,合計(分),合計(回),TV(分),TV(回),音声(分),音声(回)\r\n";
		Response.BinaryWrite(encoding.GetBytes(sHeaderString));

		string sDtl = "";

		DataRow dr = null;
		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];

			sDtl = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8}",
							dr["LOGIN_ID"].ToString(),
							dr["HANDLE_NM"].ToString(),
							dr["WORK_MIN"].ToString(),
							dr["DUMMY_MIN"].ToString(),
							dr["TOTAL_TALK_MIN"].ToString(),
							dr["TOTAL_TALK_COUNT"].ToString(),
							dr["PRV_TV_TALK_MIN"].ToString(),
							dr["PRV_TV_TALK_COUNT"].ToString(),
							dr["PRV_VOICE_TALK_MIN"].ToString(),
							dr["PRV_VOICE_TALK_COUNT"].ToString()
							);
			sDtl += "\r\n";
			Response.BinaryWrite(encoding.GetBytes(sDtl));
		}
	}
}
