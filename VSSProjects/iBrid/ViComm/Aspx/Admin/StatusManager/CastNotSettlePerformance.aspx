<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastNotSettlePerformance.aspx.cs" Inherits="Status_CastNotSettlePerformance"
	Title="出演者別未精算報酬額一覧" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="出演者別未精算報酬額一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							プロダクション
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstProductionSeq" runat="server" DataSourceID="dsProduction" DataTextField="PRODUCTION_NM" DataValueField="PRODUCTION_SEQ" Width="180px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							最低精算金額
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtCastMinimumPayment" runat="server" MaxLength="5" Width="80px"></asp:TextBox>
							<asp:RequiredFieldValidator ID="vdrCastMinimumPayment" runat="server" ControlToValidate="txtCastMinimumPayment" ErrorMessage="最低精算金額を指定してください" ValidationGroup="Key">*</asp:RequiredFieldValidator>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							集計開始日
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstFromYYYY" runat="server" Width="60px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstFromMM" runat="server" Width="40px">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstFromDD" runat="server" Width="40px">
							</asp:DropDownList>日
							<asp:DropDownList ID="lstFromHH" runat="server" Width="40px">
							</asp:DropDownList>時<br />
							<asp:Label ID="lblMsg" Visible="false" runat="server" Text="* 集計開始年が----の場合,未精算報酬有効月数の月初日からが対象となります。" Font-Size="X-Small" ForeColor="Blue"></asp:Label>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							集計終了日
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstToYYYY" runat="server" Width="60px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstToMM" runat="server" Width="40px">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstToDD" runat="server" Width="40px">
							</asp:DropDownList>日
							<asp:DropDownList ID="lstToHH" runat="server" Width="40px">
							</asp:DropDownList>時
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							<%= DisplayWordUtil.Replace("出演者ID(前方一致検索)") %>
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtLoginId" runat="server" Width="80px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							<%= DisplayWordUtil.Replace("出演者状態")%>
						</td>
						<td class="tdDataStyle">
							<asp:CheckBox ID="chkNotNormal" runat="server" Checked="true" Visible="false"/>
							<asp:CheckBox ID="chkNormal" runat="server" Text="通常" />
							<asp:CheckBox ID="chkAuthWait" runat="server" Text="認証待ち" ForeColor="SeaGreen" Visible="false" />
							<asp:CheckBox ID="chkHold" runat="server" Text="保留" ForeColor="Blue" />
							<asp:CheckBox ID="chkResigned" runat="server" Text="退会" ForeColor="Maroon" />
							<asp:CheckBox ID="chkStop" runat="server" Text="禁止" ForeColor="Red" />
							<asp:CheckBox ID="chkBan" runat="server" Text="停止" ForeColor="Red" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
						    <asp:Label ID="lblBankInfoInvalid" runat="server" Text="銀行情報未確定者も出力"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:CheckBox ID="chkBankInfoInvalid" runat="server" Checked="false" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							<asp:Label ID="Label9" runat="server" Text="表示明細"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:RadioButton ID="rdoTel" runat="server" Checked="True" GroupName="DisplayType" AutoPostBack="True" Text="電話利用明細" ValidationGroup="Key">
							</asp:RadioButton>
							<asp:RadioButton ID="rdoWeb" runat="server" GroupName="DisplayType" AutoPostBack="True" Text="WEB利用明細" ValidationGroup="Key">
							</asp:RadioButton>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
				<asp:Button runat="server" ID="btnCsv" Text="ＣＳＶ出力" CssClass="seekbutton" OnClick="btnCSV_Click" />
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[稼動集計]</legend>
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="Auto" Height="430px">
					<asp:GridView ID="grdPerformace" runat="server" AutoGenerateColumns="False" DataSourceID="dsTimeOperation" ShowFooter="True" AllowSorting="True" SkinID="GridViewColor"
						OnRowDataBound="grdPerformace_RowDataBound" Font-Size="XX-Small" OnRowCreated="grdPerformace_RowCreated">
						<Columns>
							<asp:TemplateField HeaderText="ログインID" SortExpression="LOGIN_ID">
								<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="48px" />
								<ItemTemplate>
									<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# string.Format("~/StatusManager/CastManageCompanySettlement.aspx?loginid={0}&to={1}&minpayment={2}&bank={3}",Eval("LOGIN_ID"),GetSelectToDate(),txtCastMinimumPayment.Text,chkBankInfoInvalid.Checked ? "1" : "0") %>'
										Text='<%# Eval("LOGIN_ID") %>'>
									</asp:HyperLink>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Center" />
								<FooterTemplate>
									合計
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="氏名" SortExpression="CAST_NM">
								<ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="70px" />
								<ItemTemplate>
									<asp:Label ID="Label2" runat="server" Text='<%# Eval("CAST_NM") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Center" />
								<FooterTemplate>
									支払Pt
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="TV" SortExpression="PRV_TV_TALK_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label02" runat="server" Text='<%# Eval("PRV_TV_TALK_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblPrvTvTalkPayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="TV<br/>　ﾁｬｯﾄ" SortExpression="PUB_TV_TALK_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label04" runat="server" Text='<%# Eval("PUB_TV_TALK_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblPubTvTalkPayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="音声" SortExpression="PRV_VOICE_TALK_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label06" runat="server" Text='<%# Eval("PRV_VOICE_TALK_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblPrvVoiceTalkPayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="音声<br/>　ﾁｬｯﾄ" SortExpression="PUB_VOICE_TALK_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label06" runat="server" Text='<%# Eval("PUB_VOICE_TALK_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblPubVoiceTalkPayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="会話<br>　ﾓﾆﾀ" SortExpression="VIEW_TALK_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label08" runat="server" Text='<%# Eval("VIEW_TALK_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblViewTalkPayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="音声<br>　ﾓﾆﾀ" SortExpression="WIRETAP_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("WIRETAP_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblWiretapPayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="部屋<br>　ﾓﾆﾀ" SortExpression="VIEW_BROADCAST_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("VIEW_BROADCAST_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblViewBroadcastPayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾗｲﾌﾞ<br/>　視聴" SortExpression="LIVE_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("LIVE_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblLivePayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="PF再生" SortExpression="PLAY_PROFILE_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("PLAY_PROFILE_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblPlayProfilePayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="PF録音" SortExpression="REC_PROFILE_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("REC_PROFILE_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblRecProfilePayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="逆PF<br/>会話" SortExpression="GPF_TALK_VOICE_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("GPF_TALK_VOICE_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblGpfTalkVoicePayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾌﾟﾗｲﾍﾞｰﾄ<br/>伝言再生" SortExpression="PLAY_PV_MSG_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("PLAY_PV_MSG_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblPlayPvMsgPayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="待機<br/>　TV" SortExpression="CAST_PRV_TV_TALK_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label02" runat="server" Text='<%# Eval("CAST_PRV_TV_TALK_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblCastPrvTvTalkPayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="待機<br/>TV<br/>　ﾁｬｯﾄ" SortExpression="CAST_PUB_TV_TALK_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label04" runat="server" Text='<%# Eval("CAST_PUB_TV_TALK_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblCastPubTvTalkPayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="待機<br/>　音声" SortExpression="CAST_PRV_VOICE_TALK_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label06" runat="server" Text='<%# Eval("CAST_PRV_VOICE_TALK_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblCastPrvVoiceTalkPayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="待機<br/>音声<br/>　ﾁｬｯﾄ" SortExpression="CAST_PUB_VOICE_TALK_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label06" runat="server" Text='<%# Eval("CAST_PUB_VOICE_TALK_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblCastPubVoiceTalkPayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="電話計" SortExpression="TALK_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label14" runat="server" Text='<%# Eval("TALK_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" BackColor="#F5F5DC" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" BackColor="#F5F5DC" />
								<FooterTemplate>
									<asp:Label ID="lblTalkPayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾒｰﾙ受信" SortExpression="USER_MAIL_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label16" runat="server" Text='<%# Eval("USER_MAIL_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblUserMailPayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="写真添付<br/>ﾒｰﾙ受信" SortExpression="USER_MAIL_PIC_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label16" runat="server" Text='<%# Eval("USER_MAIL_PIC_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblUserMailPicPayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="動画添付<br/>ﾒｰﾙ受信" SortExpression="USER_MAIL_MOVIE_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label16" runat="server" Text='<%# Eval("USER_MAIL_MOVIE_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblUserMailMoviePayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾒｰﾙ添付<br>写真閲覧" SortExpression="OPEN_PIC_MAIL_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label18" runat="server" Text='<%# Eval("OPEN_PIC_MAIL_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblOpenPicMailPayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾒｰﾙ添付<br>動画閲覧" SortExpression="OPEN_MOVIE_MAIL_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label20" runat="server" Text='<%# Eval("OPEN_MOVIE_MAIL_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblOpenMovieMailPayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="掲示板<br>写真閲覧" SortExpression="OPEN_PIC_BBS_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label18" runat="server" Text='<%# Eval("OPEN_PIC_BBS_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblOpenPicBbsPayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="掲示板<br>動画閲覧" SortExpression="OPEN_MOVIE_BBS_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label20" runat="server" Text='<%# Eval("OPEN_MOVIE_BBS_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblOpenMovieBbsPayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾌﾟﾗｲﾍﾞｰﾄ<br>伝言受信" SortExpression="PRV_MESSAGE_RX_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label20" runat="server" Text='<%# Eval("PRV_MESSAGE_RX_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblPrvMessageRxPayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾌﾟﾗｲﾍﾞｰﾄ<br>PF受信" SortExpression="PRV_PROFILE_RX_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label20" runat="server" Text='<%# Eval("PRV_PROFILE_RX_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblPrvProfileRxPayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾀﾞｳﾝﾛｰﾄﾞ<br>ﾑｰﾋﾞｰ" SortExpression="DOWNLOAD_MOVIE_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("DOWNLOAD_MOVIE_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblDownloadMoviePayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="定時<br>ｺｰﾙ" SortExpression="TIME_CALL_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("TIME_CALL_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblTimeCallPayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾀﾞｳﾝﾛｰﾄﾞ<br>着ﾎﾞｲｽ" SortExpression="DOWNLOAD_VOICE_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("DOWNLOAD_VOICE_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblDownloadVoicePayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ｿｰｼｬﾙ<br>ｹﾞｰﾑ" SortExpression="SOCIAL_GAME_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("SOCIAL_GAME_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblSocialGamePayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="野球拳<br />FC会費" SortExpression="YAKYUKEN_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("YAKYUKEN_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblYakyukenPayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾌﾟﾚｾﾞﾝﾄﾒｰﾙ" SortExpression="PRESENT_MAIL_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("PRESENT_MAIL_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" />
								<FooterTemplate>
									<asp:Label ID="lblPresentMailPayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="友達紹介" SortExpression="FRIEND_INTRO_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("FRIEND_INTRO_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" BackColor="#F5F5DC" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" BackColor="#F5F5DC" />
								<FooterTemplate>
									<asp:Label ID="lblFriendIntroPayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="WEB計" SortExpression="WEB_TOTAL_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label14" runat="server" Text='<%# Eval("WEB_TOTAL_PAY_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" BackColor="#F5F5DC" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" BackColor="#F5F5DC" />
								<FooterTemplate>
									<asp:Label ID="lblWebTotalPayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾎﾞｰﾅｽ" SortExpression="BONUS_AMT">
								<ItemTemplate>
									<asp:Label ID="Label16" runat="server" Text='<%# Eval("BONUS_AMT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" BackColor="#FFEBCD" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" BackColor="#FFEBCD" />
								<FooterTemplate>
									<asp:Label ID="lblBonusAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="合計" SortExpression="TOTAL_PAY_AMT">
								<ItemTemplate>
									<asp:Label ID="Label16" runat="server" Text='<%# Eval("TOTAL_PAY_AMT","{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="48px" BackColor="#FFEBCD" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="48px" BackColor="#FFEBCD" />
								<FooterTemplate>
									<asp:Label ID="lblTotalPayAmt" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
						</Columns>
						<FooterStyle ForeColor="Black" BackColor="LightYellow" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsTimeOperation" runat="server" SelectMethod="GetNotPaymentList" TypeName="TimeOperation" OnSelecting="dsTimeOperation_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pFromYYYY" Type="String" />
			<asp:Parameter Name="pFromMM" Type="String" />
			<asp:Parameter Name="pFromDD" Type="String" />
			<asp:Parameter Name="pFromHH" Type="String" />
			<asp:Parameter Name="pToYYYY" Type="String" />
			<asp:Parameter Name="pToMM" Type="String" />
			<asp:Parameter Name="pToDD" Type="String" />
			<asp:Parameter Name="pToHH" Type="String" />
			<asp:Parameter Name="pLoginId" Type="String" />
			<asp:Parameter Name="pProductionSeq" Type="String" />
			<asp:Parameter Name="pTotalPayAmt" Type="String" />
			<asp:Parameter Name="pNoBankCastDispFlag" Type="String" />
			<asp:Parameter Name="pInvalidBankCastDispFlag" Type="String" />
			<asp:Parameter Name="pNotNormalCastDispFlag" Type="String" />
			<asp:Parameter Name="pWaitPaymentFlag" Type="String" />
			<asp:Parameter Name="pReqPaymentDate" Type="String" />
			<asp:Parameter Name="pUserStatusMask" Type="int32" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsProduction" runat="server" SelectMethod="GetList" TypeName="Production"></asp:ObjectDataSource>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtCastMinimumPayment" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrCastMinimumPayment" HighlightCssClass="validatorCallout" />
</asp:Content>
