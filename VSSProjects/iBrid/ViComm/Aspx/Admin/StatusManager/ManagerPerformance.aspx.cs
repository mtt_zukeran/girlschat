﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 担当別稼動集計
--	Progaram ID		: ManagerPerformance
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Status_ManagerPerformance:System.Web.UI.Page {

	private int prvTvTalkPoint;
	private int prvTvTalkMin;
	private int prvTvTalkPayPoint;

	private int pubTvTalkPoint;
	private int pubTvTalkMin;
	private int pubTvTalkPayPoint;

	private int prvVoiceTalkPoint;
	private int prvVoiceTalkMin;
	private int prvVoiceTalkPayPoint;

	private int pubVoiceTalkPoint;
	private int pubVoiceTalkMin;
	private int pubVoiceTalkPayPoint;

	private int viewTalkPoint;
	private int viewTalkMin;
	private int viewTalkPayPoint;

	private int wiretapPoint;
	private int wiretapMin;
	private int wiretapPayPoint;

	private int viewBroadcastPoint;
	private int viewBroadcastMin;
	private int viewBroadcastPayPoint;

	private int livePoint;
	private int liveMin;
	private int livePayPoint;

	private int playProfilePoint;
	private int playProfileMin;
	private int playProfilePayPoint;

	private int recProfilePoint;
	private int recProfileMin;
	private int recProfilePayPoint;

	private int gpfTalkVoicePoint;
	private int gpfTalkVoiceMin;
	private int gpfTalkVoicePayPoint;

	private int playPvMsgPoint;
	private int playPvMsgMin;
	private int playPvMsgPayPoint;

	//private int moviePoint;
	//private int movieMin;
	//private int moviePayPoint;

	private int castPrvTvTalkPoint;
	private int castPrvTvTalkMin;
	private int castPrvTvTalkPayPoint;

	private int castPubTvTalkPoint;
	private int castPubTvTalkMin;
	private int castPubTvTalkPayPoint;

	private int castPrvVoiceTalkPoint;
	private int castPrvVoiceTalkMin;
	private int castPrvVoiceTalkPayPoint;

	private int castPubVoiceTalkPoint;
	private int castPubVoiceTalkMin;
	private int castPubVoiceTalkPayPoint;

	private int talkPoint;
	private int talkMin;
	private int talkPayPoint;

	private int mailPoint;
	private int mailCount;
	private int mailPayPoint;
	private int mailPicPoint;
	private int mailPicCount;
	private int mailPicPayPoint;
	private int mailMoviePoint;
	private int mailMovieCount;
	private int mailMoviePayPoint;
	private int openPicMailPoint;
	private int openPicMailCount;
	private int openPicMailPayPoint;
	private int openMovieMailPoint;
	private int openMovieMailCount;
	private int openMovieMailPayPoint;
	private int openPicBbsPoint;
	private int openPicBbsCount;
	private int openPicBbsPayPoint;
	private int openMovieBbsPoint;
	private int openMovieBbsCount;
	private int openMovieBbsPayPoint;
	private int messageRxCount;
	private int messageRxPoint;
	private int messageRxPayPoint;
	private int profileRxCount;
	private int profileRxPoint;
	private int profileRxPayPoint;
	private int downloadMovieCount;
	private int downloadMoviePoint;
	private int downloadMoviePayPoint;
	private int timeCallCount;
	private int timeCallPoint;
	private int timeCallPayPoint;
	private int downloadVoiceCount;
	private int downloadVoicePoint;
	private int downloadVoicePayPoint;
	private int socialGameCount;
	private int socialGamePoint;
	private int socialGamePayPoint;
	private int yakyukenCount;
	private int yakyukenPoint;
	private int yakyukenPayPoint;
	private int presentMailCount;
	private int presentMailPoint;
	private int presentMailPayPoint;

	private int webTotalPoint;
	private int webTotalCount;
	private int webTotalPayPoint;



	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
			if (Session["SiteCd"].ToString().Equals("")) {
				lstSiteCd.Items.Insert(0,new ListItem("",""));
			}
			lstSiteCd.DataSourceID = "";
			if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
				lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}
		} else {
			DataBind();
		}
		DisplayWordUtil.ReplaceGridColumnHeader(this.grdPoint);
	}

	private void InitPage() {
		grdPoint.DataSourceID = "";
		pnlInfo.Visible = false;
		ClearField();
		DataBind();
		if (!IsPostBack) {
			SysPrograms.SetupFromToDayTime(lstFromYYYY,lstFromMM,lstFromDD,lstFromHH,lstToYYYY,lstToMM,lstToDD,lstToHH,true);
			lstFromYYYY.SelectedIndex = 0;
			lstToYYYY.SelectedIndex = 0;
			lstFromMM.SelectedValue = DateTime.Now.ToString("MM");
			lstToMM.SelectedValue = DateTime.Now.ToString("MM");
			lstFromDD.SelectedValue = DateTime.Now.ToString("dd");
			lstToDD.SelectedValue = DateTime.Now.ToString("dd");
			lstFromHH.SelectedIndex = 0;
			lstToHH.SelectedIndex = 0;

			lstProductionSeq.Items.Insert(0,new ListItem("",""));
			lstProductionSeq.DataSourceID = "";
			if (!iBridUtil.GetStringValue(Session["PRODUCTION_SEQ"]).Equals("")) {
				lstProductionSeq.SelectedValue = Session["PRODUCTION_SEQ"].ToString();
				lstProductionSeq.Enabled = false;
			} else {
				lstProductionSeq.SelectedIndex = 0;
			}

			lstManager.Items.Insert(0,new ListItem(string.Empty,string.Empty));
			lstManager.DataSourceID = string.Empty;
			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MANAGER_SEQ"]))) {
				lstManager.SelectedValue = iBridUtil.GetStringValue(Session["MANAGER_SEQ"]);
				lstManager.Enabled = false;
			} else {
				lstManager.SelectedIndex = 0;
			}
		}
	}

	private void ClearField() {
		btnCsv.Visible = false;
		prvTvTalkPoint = 0;
		prvTvTalkMin = 0;
		prvTvTalkPayPoint = 0;

		pubTvTalkPoint = 0;
		pubTvTalkMin = 0;
		pubTvTalkPayPoint = 0;

		prvVoiceTalkPoint = 0;
		prvVoiceTalkMin = 0;
		prvVoiceTalkPayPoint = 0;

		pubVoiceTalkPoint = 0;
		pubVoiceTalkMin = 0;
		pubVoiceTalkPayPoint = 0;

		viewTalkPoint = 0;
		viewTalkMin = 0;
		viewTalkPayPoint = 0;

		wiretapPoint = 0;
		wiretapMin = 0;
		wiretapPayPoint = 0;

		viewBroadcastPoint = 0;
		viewBroadcastMin = 0;
		viewBroadcastPayPoint = 0;

		livePoint = 0;
		liveMin = 0;
		livePayPoint = 0;

		playProfilePoint = 0;
		playProfileMin = 0;
		playProfilePayPoint = 0;

		recProfilePoint = 0;
		recProfileMin = 0;
		recProfilePayPoint = 0;

		gpfTalkVoicePoint = 0;
		gpfTalkVoiceMin = 0;
		gpfTalkVoicePayPoint = 0;

		playPvMsgPoint = 0;
		playPvMsgMin = 0;
		playPvMsgPayPoint = 0;

		//moviePoint = 0;
		//movieMin = 0;
		//moviePayPoint = 0;

		castPrvTvTalkPoint = 0;
		castPrvTvTalkMin = 0;
		castPrvTvTalkPayPoint = 0;

		castPubTvTalkPoint = 0;
		castPubTvTalkMin = 0;
		castPubTvTalkPayPoint = 0;

		castPrvVoiceTalkPoint = 0;
		castPrvVoiceTalkMin = 0;
		castPrvVoiceTalkPayPoint = 0;

		castPubVoiceTalkPoint = 0;
		castPubVoiceTalkMin = 0;
		castPubVoiceTalkPayPoint = 0;

		talkPoint = 0;
		talkMin = 0;
		talkPayPoint = 0;

		mailPoint = 0;
		mailCount = 0;
		mailPayPoint = 0;
		mailPicPoint = 0;
		mailPicCount = 0;
		mailPicPayPoint = 0;
		mailMoviePoint = 0;
		mailMovieCount = 0;
		mailMoviePayPoint = 0;

		openPicMailPoint = 0;
		openPicMailCount = 0;
		openPicMailPayPoint = 0;
		openMovieMailPoint = 0;
		openMovieMailCount = 0;
		openMovieMailPayPoint = 0;
		openPicBbsPoint = 0;
		openPicBbsCount = 0;
		openPicBbsPayPoint = 0;
		openMovieBbsPoint = 0;
		openMovieBbsCount = 0;
		openMovieBbsPayPoint = 0;
		messageRxCount = 0;
		messageRxPoint = 0;
		messageRxPayPoint = 0;
		profileRxCount = 0;
		profileRxPoint = 0;
		profileRxPayPoint = 0;
		downloadMovieCount = 0;
		downloadMoviePoint = 0;
		downloadMoviePayPoint = 0;
		timeCallCount = 0;
		timeCallPoint = 0;
		timeCallPayPoint = 0;
		downloadVoiceCount = 0;
		downloadVoicePoint = 0;
		downloadVoicePayPoint = 0;
		socialGameCount = 0;
		socialGamePoint = 0;
		socialGamePayPoint = 0;
		yakyukenCount = 0;
		yakyukenPoint = 0;
		yakyukenPayPoint = 0;

		webTotalPoint = 0;
		webTotalCount = 0;
		webTotalPayPoint = 0;
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
		if (string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["PRODUCTION_SEQ"]))) {
			lstProductionSeq.SelectedIndex = 0;
		}
		if (string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MANAGER_SEQ"]))) {
			lstManager.SelectedIndex = 0;
		}
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		string fileName = ViCommConst.CSV_FILE_NM_MANAGER_PERFORMANCE_LOG;

		Response.AddHeader("Content-Disposition","attachment;filename=" + fileName);
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		using (TimeOperation oTimePerformance = new TimeOperation()) {
			DataSet ds = oTimePerformance.GetManagerSummaryList(
								lstSiteCd.SelectedValue,
								lstFromYYYY.SelectedValue,
								lstFromMM.SelectedValue,
								lstFromDD.SelectedValue,
								lstFromHH.SelectedValue,
								lstToYYYY.SelectedValue,
								lstToMM.SelectedValue,
								lstToDD.SelectedValue,
								lstToHH.SelectedValue,
								lstProductionSeq.SelectedValue,
								lstManager.SelectedValue
							);

			string sCsvRec = GetCsvString(ds);

			Response.BinaryWrite(encoding.GetBytes(sCsvRec));
			Response.End();
		}
	}


	protected void grdPoint_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			prvTvTalkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRV_TV_TALK_POINT"));
			prvTvTalkMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRV_TV_TALK_MIN"));
			prvTvTalkPayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRV_TV_TALK_PAY_POINT"));

			pubTvTalkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PUB_TV_TALK_POINT"));
			pubTvTalkMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PUB_TV_TALK_MIN"));
			pubTvTalkPayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PUB_TV_TALK_PAY_POINT"));

			prvVoiceTalkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRV_VOICE_TALK_POINT"));
			prvVoiceTalkMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRV_VOICE_TALK_MIN"));
			prvVoiceTalkPayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRV_VOICE_TALK_PAY_POINT"));

			pubVoiceTalkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PUB_VOICE_TALK_POINT"));
			pubVoiceTalkMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PUB_VOICE_TALK_MIN"));
			pubVoiceTalkPayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PUB_VOICE_TALK_PAY_POINT"));

			viewTalkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"VIEW_TALK_POINT"));
			viewTalkMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"VIEW_TALK_MIN"));
			viewTalkPayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"VIEW_TALK_PAY_POINT"));

			wiretapPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"WIRETAP_POINT"));
			wiretapMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"WIRETAP_MIN"));
			wiretapPayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"WIRETAP_PAY_POINT"));

			viewBroadcastPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"VIEW_BROADCAST_POINT"));
			viewBroadcastMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"VIEW_BROADCAST_MIN"));
			viewBroadcastPayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"VIEW_BROADCAST_PAY_POINT"));

			livePoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"LIVE_POINT"));
			liveMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"LIVE_MIN"));
			livePayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"LIVE_PAY_POINT"));

			playProfilePoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PLAY_PROFILE_POINT"));
			playProfileMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PLAY_PROFILE_MIN"));
			playProfilePayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PLAY_PROFILE_PAY_POINT"));

			recProfilePoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"REC_PROFILE_POINT"));
			recProfileMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"REC_PROFILE_MIN"));
			recProfilePayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"REC_PROFILE_PAY_POINT"));

			gpfTalkVoicePoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"GPF_TALK_VOICE_POINT"));
			gpfTalkVoiceMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"GPF_TALK_VOICE_MIN"));
			gpfTalkVoicePayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"GPF_TALK_VOICE_PAY_POINT"));

			playPvMsgPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PLAY_PV_MSG_POINT"));
			playPvMsgMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PLAY_PV_MSG_MIN"));
			playPvMsgPayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PLAY_PV_MSG_PAY_POINT"));

			//moviePoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"MOVIE_POINT"));
			//movieMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"MOVIE_MIN"));
			//moviePayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"MOVIE_PAY_POINT"));

			castPrvTvTalkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PRV_TV_TALK_POINT"));
			castPrvTvTalkMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PRV_TV_TALK_MIN"));
			castPrvTvTalkPayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PRV_TV_TALK_PAY_POINT"));

			castPubTvTalkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PUB_TV_TALK_POINT"));
			castPubTvTalkMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PUB_TV_TALK_MIN"));
			castPubTvTalkPayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PUB_TV_TALK_PAY_POINT"));

			castPrvVoiceTalkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PRV_VOICE_TALK_POINT"));
			castPrvVoiceTalkMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PRV_VOICE_TALK_MIN"));
			castPrvVoiceTalkPayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PRV_VOICE_TALK_PAY_POINT"));

			castPubVoiceTalkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PUB_VOICE_TALK_POINT"));
			castPubVoiceTalkMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PUB_VOICE_TALK_MIN"));
			castPubVoiceTalkPayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PUB_VOICE_TALK_PAY_POINT"));

			talkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TALK_POINT"));
			talkMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TALK_MIN"));
			talkPayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TALK_PAY_POINT"));

			mailPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"USER_MAIL_POINT"));
			mailCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"USER_MAIL_COUNT"));
			mailPayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"USER_MAIL_PAY_POINT"));

			mailPicPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"USER_MAIL_PIC_POINT"));
			mailPicCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"USER_MAIL_PIC_COUNT"));
			mailPicPayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"USER_MAIL_PIC_PAY_POINT"));

			mailMoviePoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"USER_MAIL_MOVIE_POINT"));
			mailMovieCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"USER_MAIL_MOVIE_COUNT"));
			mailMoviePayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"USER_MAIL_MOVIE_PAY_POINT"));

			openPicMailPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"OPEN_PIC_MAIL_POINT"));
			openPicMailCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"OPEN_PIC_MAIL_COUNT"));
			openPicMailPayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"OPEN_PIC_MAIL_PAY_POINT"));

			openMovieMailPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"OPEN_MOVIE_MAIL_POINT"));
			openMovieMailCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"OPEN_MOVIE_MAIL_COUNT"));
			openMovieMailPayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"OPEN_MOVIE_MAIL_PAY_POINT"));

			openPicBbsPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"OPEN_PIC_BBS_POINT"));
			openPicBbsCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"OPEN_PIC_BBS_COUNT"));
			openPicBbsPayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"OPEN_PIC_BBS_PAY_POINT"));

			openMovieBbsPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"OPEN_MOVIE_BBS_POINT"));
			openMovieBbsCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"OPEN_MOVIE_BBS_COUNT"));
			openMovieBbsPayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"OPEN_MOVIE_BBS_PAY_POINT"));

			messageRxPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRV_MESSAGE_RX_POINT"));
			messageRxCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRV_MESSAGE_RX_COUNT"));
			messageRxPayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRV_MESSAGE_RX_PAY_POINT"));

			profileRxPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRV_PROFILE_RX_POINT"));
			profileRxCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRV_PROFILE_RX_COUNT"));
			profileRxPayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRV_PROFILE_RX_PAY_POINT"));

			downloadMovieCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"DOWNLOAD_MOVIE_COUNT"));
			downloadMoviePoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"DOWNLOAD_MOVIE_POINT"));
			downloadMoviePayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"DOWNLOAD_MOVIE_PAY_POINT"));

			timeCallCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TIME_CALL_COUNT"));
			timeCallPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TIME_CALL_POINT"));
			timeCallPayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TIME_CALL_PAY_POINT"));

			downloadVoiceCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"DOWNLOAD_VOICE_COUNT"));
			downloadVoicePoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"DOWNLOAD_VOICE_POINT"));
			downloadVoicePayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"DOWNLOAD_VOICE_PAY_POINT"));

			socialGameCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"SOCIAL_GAME_COUNT"));
			socialGamePoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"SOCIAL_GAME_POINT"));
			socialGamePayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"SOCIAL_GAME_PAY_POINT"));

			yakyukenCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"YAKYUKEN_COUNT"));
			yakyukenPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"YAKYUKEN_POINT"));
			yakyukenPayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"YAKYUKEN_PAY_POINT"));

			presentMailCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRESENT_MAIL_COUNT"));
			presentMailPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRESENT_MAIL_POINT"));
			presentMailPayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRESENT_MAIL_PAY_POINT"));

			webTotalPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"WEB_TOTAL_POINT"));
			webTotalCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"WEB_TOTAL_COUNT"));
			webTotalPayPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"WEB_TOTAL_PAY_POINT"));

		} else if (e.Row.RowType == DataControlRowType.Footer) {
			Label lblPrvTvTalkMin = (Label)e.Row.FindControl("lblPrvTvTalkMin");
			Label lblPrvTvTalkPoint = (Label)e.Row.FindControl("lblPrvTvTalkPoint");
			Label lblPrvTvTalkPayPoint = (Label)e.Row.FindControl("lblPrvTvTalkPayPoint");

			Label lblPubTvTalkMin = (Label)e.Row.FindControl("lblPubTvTalkMin");
			Label lblPubTvTalkPoint = (Label)e.Row.FindControl("lblPubTvTalkPoint");
			Label lblPubTvTalkPayPoint = (Label)e.Row.FindControl("lblPubTvTalkPayPoint");

			Label lblPrvVoiceTalkMin = (Label)e.Row.FindControl("lblPrvVoiceTalkMin");
			Label lblPrvVoiceTalkPoint = (Label)e.Row.FindControl("lblPrvVoiceTalkPoint");
			Label lblPrvVoiceTalkPayPoint = (Label)e.Row.FindControl("lblPrvVoiceTalkPayPoint");

			Label lblPubVoiceTalkMin = (Label)e.Row.FindControl("lblPubVoiceTalkMin");
			Label lblPubVoiceTalkPoint = (Label)e.Row.FindControl("lblPubVoiceTalkPoint");
			Label lblPubVoiceTalkPayPoint = (Label)e.Row.FindControl("lblPubVoiceTalkPayPoint");

			Label lblViewTalkMin = (Label)e.Row.FindControl("lblViewTalkMin");
			Label lblViewTalkPoint = (Label)e.Row.FindControl("lblViewTalkPoint");
			Label lblViewTalkPayPoint = (Label)e.Row.FindControl("lblViewTalkPayPoint");

			Label lblWiretapMin = (Label)e.Row.FindControl("lblWiretapMin");
			Label lblWiretapPoint = (Label)e.Row.FindControl("lblWiretapPoint");
			Label lblWiretapPayPoint = (Label)e.Row.FindControl("lblWiretapPayPoint");

			Label lblViewBroadcastMin = (Label)e.Row.FindControl("lblViewBroadcastMin");
			Label lblViewBroadcastPoint = (Label)e.Row.FindControl("lblViewBroadcastPoint");
			Label lblViewBroadcastPayPoint = (Label)e.Row.FindControl("lblViewBroadcastPayPoint");

			Label lblLiveMin = (Label)e.Row.FindControl("lblLiveMin");
			Label lblLivePoint = (Label)e.Row.FindControl("lblLivePoint");
			Label lblLivePayPoint = (Label)e.Row.FindControl("lblLivePayPoint");

			Label lblPlayProfileMin = (Label)e.Row.FindControl("lblPlayProfileMin");
			Label lblPlayProfilePoint = (Label)e.Row.FindControl("lblPlayProfilePoint");
			Label lblPlayProfilePayPoint = (Label)e.Row.FindControl("lblPlayProfilePayPoint");

			Label lblRecProfileMin = (Label)e.Row.FindControl("lblRecProfileMin");
			Label lblRecProfilePoint = (Label)e.Row.FindControl("lblRecProfilePoint");
			Label lblRecProfilePayPoint = (Label)e.Row.FindControl("lblRecProfilePayPoint");

			Label lblGpfTalkVoiceMin = (Label)e.Row.FindControl("lblGpfTalkVoiceMin");
			Label lblGpfTalkVoicePoint = (Label)e.Row.FindControl("lblGpfTalkVoicePoint");
			Label lblGpfTalkVoicePayPoint = (Label)e.Row.FindControl("lblGpfTalkVoicePayPoint");

			Label lblPlayPvMsgMin = (Label)e.Row.FindControl("lblPlayPvMsgMin");
			Label lblPlayPvMsgPoint = (Label)e.Row.FindControl("lblPlayPvMsgPoint");
			Label lblPlayPvMsgPayPoint = (Label)e.Row.FindControl("lblPlayPvMsgPayPoint");

			//Label lblMovieMin = (Label)e.Row.FindControl("lblMovieMin");
			//Label lblMoviePoint = (Label)e.Row.FindControl("lblMoviePoint");
			//Label lblMoviePayPoint = (Label)e.Row.FindControl("lblMoviePayPoint");

			Label lblCastPrvTvTalkMin = (Label)e.Row.FindControl("lblCastPrvTvTalkMin");
			Label lblCastPrvTvTalkPoint = (Label)e.Row.FindControl("lblCastPrvTvTalkPoint");
			Label lblCastPrvTvTalkPayPoint = (Label)e.Row.FindControl("lblCastPrvTvTalkPayPoint");

			Label lblCastPubTvTalkMin = (Label)e.Row.FindControl("lblCastPubTvTalkMin");
			Label lblCastPubTvTalkPoint = (Label)e.Row.FindControl("lblCastPubTvTalkPoint");
			Label lblCastPubTvTalkPayPoint = (Label)e.Row.FindControl("lblCastPubTvTalkPayPoint");

			Label lblCastPrvVoiceTalkMin = (Label)e.Row.FindControl("lblCastPrvVoiceTalkMin");
			Label lblCastPrvVoiceTalkPoint = (Label)e.Row.FindControl("lblCastPrvVoiceTalkPoint");
			Label lblCastPrvVoiceTalkPayPoint = (Label)e.Row.FindControl("lblCastPrvVoiceTalkPayPoint");

			Label lblCastPubVoiceTalkMin = (Label)e.Row.FindControl("lblCastPubVoiceTalkMin");
			Label lblCastPubVoiceTalkPoint = (Label)e.Row.FindControl("lblCastPubVoiceTalkPoint");
			Label lblCastPubVoiceTalkPayPoint = (Label)e.Row.FindControl("lblCastPubVoiceTalkPayPoint");

			Label lblTalkMin = (Label)e.Row.FindControl("lblTalkMin");
			Label lblTalkPoint = (Label)e.Row.FindControl("lblTalkPoint");
			Label lblTalkPayPoint = (Label)e.Row.FindControl("lblTalkPayPoint");

			Label lblUserMailCount = (Label)e.Row.FindControl("lblUserMailCount");
			Label lblUserMailPoint = (Label)e.Row.FindControl("lblUserMailPoint");
			Label lblUserMailPayPoint = (Label)e.Row.FindControl("lblUserMailPayPoint");

			Label lblUserMailPicCount = (Label)e.Row.FindControl("lblUserMailPicCount");
			Label lblUserMailPicPoint = (Label)e.Row.FindControl("lblUserMailPicPoint");
			Label lblUserMailPicPayPoint = (Label)e.Row.FindControl("lblUserMailPicPayPoint");

			Label lblUserMailMovieCount = (Label)e.Row.FindControl("lblUserMailMovieCount");
			Label lblUserMailMoviePoint = (Label)e.Row.FindControl("lblUserMailMoviePoint");
			Label lblUserMailMoviePayPoint = (Label)e.Row.FindControl("lblUserMailMoviePayPoint");

			Label lblOpenPicMailCount = (Label)e.Row.FindControl("lblOpenPicMailCount");
			Label lblOpenPicMailPoint = (Label)e.Row.FindControl("lblOpenPicMailPoint");
			Label lblOpenPicMailPayPoint = (Label)e.Row.FindControl("lblOpenPicMailPayPoint");

			Label lblOpenMovieMailCount = (Label)e.Row.FindControl("lblOpenMovieMailCount");
			Label lblOpenMovieMailPoint = (Label)e.Row.FindControl("lblOpenMovieMailPoint");
			Label lblOpenMovieMailPayPoint = (Label)e.Row.FindControl("lblOpenMovieMailPayPoint");

			Label lblOpenPicBbsCount = (Label)e.Row.FindControl("lblOpenPicBbsCount");
			Label lblOpenPicBbsPoint = (Label)e.Row.FindControl("lblOpenPicBbsPoint");
			Label lblOpenPicBbsPayPoint = (Label)e.Row.FindControl("lblOpenPicBbsPayPoint");

			Label lblOpenMovieBbsCount = (Label)e.Row.FindControl("lblOpenMovieBbsCount");
			Label lblOpenMovieBbsPoint = (Label)e.Row.FindControl("lblOpenMovieBbsPoint");
			Label lblOpenMovieBbsPayPoint = (Label)e.Row.FindControl("lblOpenMovieBbsPayPoint");

			Label lblPrvMessageRxCount = (Label)e.Row.FindControl("lblPrvMessageRxCount");
			Label lblPrvMessageRxPoint = (Label)e.Row.FindControl("lblPrvMessageRxPoint");
			Label lblPrvMessageRxPayPoint = (Label)e.Row.FindControl("lblPrvMessageRxPayPoint");

			Label lblPrvProfileRxCount = (Label)e.Row.FindControl("lblPrvProfileRxCount");
			Label lblPrvProfileRxPoint = (Label)e.Row.FindControl("lblPrvProfileRxPoint");
			Label lblPrvProfileRxPayPoint = (Label)e.Row.FindControl("lblPrvProfileRxPayPoint");

			Label lblDownloadMovieCount = (Label)e.Row.FindControl("lblDownloadMovieCount");
			Label lblDownloadMoviePoint = (Label)e.Row.FindControl("lblDownloadMoviePoint");
			Label lblDownloadMoviePayPoint = (Label)e.Row.FindControl("lblDownloadMoviePayPoint");

			Label lblTimeCallCount = (Label)e.Row.FindControl("lblTimeCallCount");
			Label lblTimeCallPoint = (Label)e.Row.FindControl("lblTimeCallPoint");
			Label lblTimeCallPayPoint = (Label)e.Row.FindControl("lblTimeCallPayPoint");

			Label lblDownloadVoiceCount = (Label)e.Row.FindControl("lblDownloadVoiceCount");
			Label lblDownloadVoicePoint = (Label)e.Row.FindControl("lblDownloadVoicePoint");
			Label lblDownloadVoicePayPoint = (Label)e.Row.FindControl("lblDownloadVoicePayPoint");

			Label lblSocialGameCount = (Label)e.Row.FindControl("lblSocialGameCount");
			Label lblSocialGamePoint = (Label)e.Row.FindControl("lblSocialGamePoint");
			Label lblSocialGamePayPoint = (Label)e.Row.FindControl("lblSocialGamePayPoint");

			Label lblYakyukenCount = (Label)e.Row.FindControl("lblYakyukenCount");
			Label lblYakyukenPoint = (Label)e.Row.FindControl("lblYakyukenPoint");
			Label lblYakyukenPayPoint = (Label)e.Row.FindControl("lblYakyukenPayPoint");

			Label lblWebTotalCount = (Label)e.Row.FindControl("lblWebTotalCount");
			Label lblWebTotalPoint = (Label)e.Row.FindControl("lblWebTotalPoint");
			Label lblWebTotalPayPoint = (Label)e.Row.FindControl("lblWebTotalPayPoint");

			Label lblTotalPoint = (Label)e.Row.FindControl("lblTotalPoint");
			Label lblTotalPayPoint = (Label)e.Row.FindControl("lblTotalPayPoint");

			int iTotalPoint = talkPoint + webTotalPoint;
			int iTotalPayPoint = talkPayPoint + webTotalPayPoint;

			lblPrvTvTalkMin.Text = prvTvTalkMin.ToString();
			lblPrvTvTalkPoint.Text = prvTvTalkPoint.ToString();
			lblPrvTvTalkPayPoint.Text = prvTvTalkPayPoint.ToString();

			lblPubTvTalkMin.Text = pubTvTalkMin.ToString();
			lblPubTvTalkPoint.Text = pubTvTalkPoint.ToString();
			lblPubTvTalkPayPoint.Text = pubTvTalkPayPoint.ToString();

			lblPrvVoiceTalkMin.Text = prvVoiceTalkMin.ToString();
			lblPrvVoiceTalkPoint.Text = prvVoiceTalkPoint.ToString();
			lblPrvVoiceTalkPayPoint.Text = prvVoiceTalkPayPoint.ToString();

			lblPubVoiceTalkMin.Text = pubVoiceTalkMin.ToString();
			lblPubVoiceTalkPoint.Text = pubVoiceTalkPoint.ToString();
			lblPubVoiceTalkPayPoint.Text = pubVoiceTalkPayPoint.ToString();

			lblViewTalkMin.Text = viewTalkMin.ToString();
			lblViewTalkPoint.Text = viewTalkPoint.ToString();
			lblViewTalkPayPoint.Text = viewTalkPayPoint.ToString();

			lblViewBroadcastMin.Text = viewBroadcastMin.ToString();
			lblViewBroadcastPoint.Text = viewBroadcastPoint.ToString();
			lblViewBroadcastPayPoint.Text = viewBroadcastPayPoint.ToString();

			lblLiveMin.Text = liveMin.ToString();
			lblLivePoint.Text = livePoint.ToString();
			lblLivePayPoint.Text = livePayPoint.ToString();

			lblPlayProfileMin.Text = playProfileMin.ToString();
			lblPlayProfilePoint.Text = playProfilePoint.ToString();
			lblPlayProfilePayPoint.Text = playProfilePayPoint.ToString();

			lblRecProfileMin.Text = recProfileMin.ToString();
			lblRecProfilePoint.Text = recProfilePoint.ToString();
			lblRecProfilePayPoint.Text = recProfilePayPoint.ToString();

			lblWiretapMin.Text = wiretapMin.ToString();
			lblWiretapPoint.Text = wiretapPoint.ToString();
			lblWiretapPayPoint.Text = wiretapPayPoint.ToString();

			lblGpfTalkVoiceMin.Text = gpfTalkVoiceMin.ToString();
			lblGpfTalkVoicePoint.Text = gpfTalkVoicePoint.ToString();
			lblGpfTalkVoicePayPoint.Text = gpfTalkVoicePayPoint.ToString();

			lblPlayPvMsgMin.Text = playPvMsgMin.ToString();
			lblPlayPvMsgPoint.Text = playPvMsgPoint.ToString();
			lblPlayPvMsgPayPoint.Text = playPvMsgPayPoint.ToString();

			//lblMovieMin.Text = movieMin.ToString();
			//lblMoviePoint.Text = moviePoint.ToString();
			//lblMoviePayPoint.Text = moviePayPoint.ToString();

			lblCastPrvTvTalkMin.Text = castPrvTvTalkMin.ToString();
			lblCastPrvTvTalkPoint.Text = castPrvTvTalkPoint.ToString();
			lblCastPrvTvTalkPayPoint.Text = castPrvTvTalkPayPoint.ToString();

			lblCastPubTvTalkMin.Text = castPubTvTalkMin.ToString();
			lblCastPubTvTalkPoint.Text = castPubTvTalkPoint.ToString();
			lblCastPubTvTalkPayPoint.Text = castPubTvTalkPayPoint.ToString();

			lblCastPrvVoiceTalkMin.Text = castPrvVoiceTalkMin.ToString();
			lblCastPrvVoiceTalkPoint.Text = castPrvVoiceTalkPoint.ToString();
			lblCastPrvVoiceTalkPayPoint.Text = castPrvVoiceTalkPayPoint.ToString();

			lblCastPubVoiceTalkMin.Text = castPrvVoiceTalkMin.ToString();
			lblCastPubVoiceTalkPoint.Text = castPrvVoiceTalkPoint.ToString();
			lblCastPubVoiceTalkPayPoint.Text = castPrvVoiceTalkPayPoint.ToString();

			lblTalkMin.Text = talkMin.ToString();
			lblTalkPoint.Text = talkPoint.ToString();
			lblTalkPayPoint.Text = talkPayPoint.ToString();

			lblUserMailCount.Text = mailCount.ToString();
			lblUserMailPoint.Text = mailPoint.ToString();
			lblUserMailPayPoint.Text = mailPayPoint.ToString();

			lblUserMailPicCount.Text = mailPicCount.ToString();
			lblUserMailPicPoint.Text = mailPicPoint.ToString();
			lblUserMailPicPayPoint.Text = mailPicPayPoint.ToString();

			lblUserMailMovieCount.Text = mailMovieCount.ToString();
			lblUserMailMoviePoint.Text = mailMoviePoint.ToString();
			lblUserMailMoviePayPoint.Text = mailMoviePayPoint.ToString();

			lblOpenPicMailCount.Text = openPicMailCount.ToString();
			lblOpenPicMailPoint.Text = openPicMailPoint.ToString();
			lblOpenPicMailPayPoint.Text = openPicMailPayPoint.ToString();

			lblOpenMovieMailCount.Text = openMovieMailCount.ToString();
			lblOpenMovieMailPoint.Text = openMovieMailPoint.ToString();
			lblOpenMovieMailPayPoint.Text = openMovieMailPayPoint.ToString();

			lblOpenPicBbsCount.Text = openPicBbsCount.ToString();
			lblOpenPicBbsPoint.Text = openPicBbsPoint.ToString();
			lblOpenPicBbsPayPoint.Text = openPicBbsPayPoint.ToString();

			lblOpenMovieBbsCount.Text = openMovieBbsCount.ToString();
			lblOpenMovieBbsPoint.Text = openMovieBbsPoint.ToString();
			lblOpenMovieBbsPayPoint.Text = openMovieBbsPayPoint.ToString();

			lblPrvMessageRxCount.Text = messageRxCount.ToString();
			lblPrvMessageRxPoint.Text = messageRxPoint.ToString();
			lblPrvMessageRxPayPoint.Text = messageRxPayPoint.ToString();

			lblPrvProfileRxCount.Text = profileRxCount.ToString();
			lblPrvProfileRxPoint.Text = profileRxPoint.ToString();
			lblPrvProfileRxPayPoint.Text = profileRxPayPoint.ToString();

			lblDownloadMovieCount.Text = downloadMovieCount.ToString();
			lblDownloadMoviePoint.Text = downloadMoviePoint.ToString();
			lblDownloadMoviePayPoint.Text = downloadMoviePayPoint.ToString();

			lblTimeCallCount.Text = timeCallCount.ToString();
			lblTimeCallPoint.Text = timeCallPoint.ToString();
			lblTimeCallPayPoint.Text = timeCallPayPoint.ToString();

			lblDownloadVoiceCount.Text = downloadVoiceCount.ToString();
			lblDownloadVoicePoint.Text = downloadVoicePoint.ToString();
			lblDownloadVoicePayPoint.Text = downloadVoicePayPoint.ToString();

			lblSocialGameCount.Text = socialGameCount.ToString();
			lblSocialGamePoint.Text = socialGamePoint.ToString();
			lblSocialGamePayPoint.Text = socialGamePayPoint.ToString();

			lblYakyukenCount.Text = yakyukenCount.ToString();
			lblYakyukenPoint.Text = yakyukenPoint.ToString();
			lblYakyukenPayPoint.Text = yakyukenPayPoint.ToString();

			lblWebTotalCount.Text = webTotalCount.ToString();
			lblWebTotalPoint.Text = webTotalPoint.ToString();
			lblWebTotalPayPoint.Text = webTotalPayPoint.ToString();

			lblTotalPoint.Text = iTotalPoint.ToString();
			lblTotalPayPoint.Text = iTotalPayPoint.ToString();
		}
	}

	private void GetList() {
		if (!chkCount.Checked && !chkPoint.Checked && !chkPayPoint.Checked) {
			chkCount.Checked = true;
			chkPoint.Checked = true;
			chkPayPoint.Checked = true;
		}
		ClearField();

		grdPoint.Columns[9].Visible = false;
		grdPoint.Columns[10].Visible = false;
		grdPoint.Columns[11].Visible = false;
		grdPoint.Columns[12].Visible = false;
		grdPoint.Columns[25].Visible = false;
		grdPoint.Columns[26].Visible = false;
		grdPoint.Columns[27].Visible = IsAvailableService(ViCommConst.RELEASE_SALE_MOVIE);
		grdPoint.Columns[28].Visible = IsAvailableService(ViCommConst.RELEASE_TIME_CALL);
		grdPoint.Columns[29].Visible = IsAvailableService(ViCommConst.RELEASE_SALE_VOICE);

		grdPoint.DataSourceID = "dsTimeOperation";
		DataBind();

		switch (Session["AdminType"].ToString()) {
			case ViCommConst.RIGHT_PRODUCTION:
			case ViCommConst.RIGHT_SITE_OWNER:
				btnCsv.Visible = true;
				break;
			default:
				btnCsv.Visible = false;
				break;
		}

		pnlInfo.Visible = true;
	}

	public static bool IsAvailableService(ulong pService) {
		using (ManageCompany oInstance = new ManageCompany()) {
			return oInstance.IsAvailableService(pService);
		}
	}

	private string GetCsvString(DataSet pDataSet) {
		int iCompanyMask = int.Parse(Session["CompanyMask"].ToString());

		bool bReleaseSaleMovie = IsAvailableService(ViCommConst.RELEASE_SALE_MOVIE);
		bool bReleaseTimeCall = IsAvailableService(ViCommConst.RELEASE_TIME_CALL);
		bool bReleaseSaleVoice = IsAvailableService(ViCommConst.RELEASE_SALE_VOICE);
		bool bReleaseSocialGame = true;

		string sCsv = "";
		sCsv += "担当名,";
		sCsv += "TV分数,";
		sCsv += "TV稼動Pt,";
		sCsv += "TV支払Pt,";
		sCsv += "TVﾁｬｯﾄ分数,";
		sCsv += "TVﾁｬｯﾄ稼動Pt,";
		sCsv += "TVﾁｬｯﾄ支払Pt,";
		sCsv += "音声分数,";
		sCsv += "音声稼動Pt,";
		sCsv += "音声支払Pt,";
		sCsv += "音声ﾁｬｯﾄ分数,";
		sCsv += "音声ﾁｬｯﾄ稼動Pt,";
		sCsv += "音声ﾁｬｯﾄ支払Pt,";
		sCsv += "会話ﾓﾆﾀ分数,";
		sCsv += "会話ﾓﾆﾀ稼動Pt,";
		sCsv += "会話ﾓﾆﾀ支払Pt,";
		sCsv += "音声ﾓﾆﾀ分数,";
		sCsv += "音声ﾓﾆﾀ稼動Pt,";
		sCsv += "音声ﾓﾆﾀ支払Pt,";
		sCsv += "部屋ﾓﾆﾀ分数,";
		sCsv += "部屋ﾓﾆﾀ稼動Pt,";
		sCsv += "部屋ﾓﾆﾀ支払Pt,";
		sCsv += "ﾗｲﾌﾞ視聴分数,";
		sCsv += "ﾗｲﾌﾞ視聴稼動Pt,";
		sCsv += "ﾗｲﾌﾞ視聴支払Pt,";
		sCsv += "男性待機TV分数,";
		sCsv += "男性待機TV稼動Pt,";
		sCsv += "男性待機TV支払Pt,";
		sCsv += "男性待機TVﾁｬｯﾄ分数,";
		sCsv += "男性待機TVﾁｬｯﾄ稼動Pt,";
		sCsv += "男性待機TVﾁｬｯﾄ支払Pt,";
		sCsv += "男性待機音声分数,";
		sCsv += "男性待機音声稼動Pt,";
		sCsv += "男性待機音声支払Pt,";
		sCsv += "男性待機音声ﾁｬｯﾄ分数,";
		sCsv += "男性待機音声ﾁｬｯﾄ稼動Pt,";
		sCsv += "男性待機音声ﾁｬｯﾄ支払Pt,";
		sCsv += "電話分数計,";
		sCsv += "電話稼動Pt計,";
		sCsv += "電話支払Pt計,";
		sCsv += "ﾒｰﾙ受信件数,";
		sCsv += "ﾒｰﾙ受信稼動Pt,";
		sCsv += "ﾒｰﾙ受信支払Pt,";
		sCsv += "写真添付ﾒｰﾙ受信件数,";
		sCsv += "写真添付ﾒｰﾙ稼動Pt,";
		sCsv += "写真添付ﾒｰﾙ支払Pt,";
		sCsv += "動画添付ﾒｰﾙ受信件数,";
		sCsv += "動画添付ﾒｰﾙ稼動Pt,";
		sCsv += "動画添付ﾒｰﾙ支払Pt,";
		sCsv += "ﾒｰﾙ添付写真閲覧件数,";
		sCsv += "ﾒｰﾙ添付写真閲覧稼動Pt,";
		sCsv += "ﾒｰﾙ添付写真閲覧支払Pt,";
		sCsv += "ﾒｰﾙ添付動画閲覧件数,";
		sCsv += "ﾒｰﾙ添付動画閲覧稼動Pt,";
		sCsv += "ﾒｰﾙ添付動画閲覧支払Pt,";
		sCsv += "BBS写真閲覧件数,";
		sCsv += "BBS写真閲覧稼動Pt,";
		sCsv += "BBS写真閲覧支払Pt,";
		sCsv += "BBS動画閲覧件数,";
		sCsv += "BBS動画閲覧稼動Pt,";
		sCsv += "BBS動画閲覧支払Pt,";
		if (bReleaseSaleMovie) {
			sCsv += "ﾀﾞｳﾝﾛｰﾄﾞﾑｰﾋﾞｰ件数,";
			sCsv += "ﾀﾞｳﾝﾛｰﾄﾞﾑｰﾋﾞｰ稼動Pt,";
			sCsv += "ﾀﾞｳﾝﾛｰﾄﾞﾑｰﾋﾞｰ支払Pt,";
		}
		if (bReleaseTimeCall) {
			sCsv += "定時ｺｰﾙ件数,";
			sCsv += "定時ｺｰﾙ稼動Pt,";
			sCsv += "定時ｺｰﾙ支払Pt,";
		}
		if (bReleaseSaleVoice) {
			sCsv += "ﾀﾞｳﾝﾛｰﾄﾞﾎﾞｲｽ件数,";
			sCsv += "ﾀﾞｳﾝﾛｰﾄﾞﾎﾞｲｽ稼動Pt,";
			sCsv += "ﾀﾞｳﾝﾛｰﾄﾞﾎﾞｲｽ支払Pt,";
		}
		if (bReleaseSocialGame) {
			sCsv += "ｿｰｼｬﾙｹﾞｰﾑ件数,";
			sCsv += "ｿｰｼｬﾙｹﾞｰﾑ稼動Pt,";
			sCsv += "ｿｰｼｬﾙｹﾞｰﾑ支払Pt,";
		}
		sCsv += "野球拳/FC会費件数,";
		sCsv += "野球拳/FC会費稼動Pt,";
		sCsv += "野球拳/FC会費支払Pt,";
		sCsv += "ﾌﾟﾚｾﾞﾝﾄﾒｰﾙ件数,";
		sCsv += "ﾌﾟﾚｾﾞﾝﾄﾒｰﾙ稼動Pt,";
		sCsv += "ﾌﾟﾚｾﾞﾝﾄﾒｰﾙ支払Pt,";
		sCsv += "WEB利用件数計,";
		sCsv += "WEB利用稼動Pt計,";
		sCsv += "WEB利用支払Pt計,";
		sCsv += "合計稼動Pt,";
		sCsv += "合計支払Pt";
		sCsv += "\r\n";
		DataRow dr = null;
		int iTotalPoint,iTotalPayPoint;

		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];

			iTotalPoint = int.Parse(dr["TALK_POINT"].ToString()) + int.Parse(dr["WEB_TOTAL_POINT"].ToString());
			iTotalPayPoint = int.Parse(dr["TALK_PAY_POINT"].ToString()) + int.Parse(dr["WEB_TOTAL_PAY_POINT"].ToString());

			sCsv += dr["MANAGER_NM"] + ",";
			sCsv += dr["PRV_TV_TALK_MIN"] + ",";
			sCsv += dr["PRV_TV_TALK_POINT"] + ",";
			sCsv += dr["PRV_TV_TALK_PAY_POINT"] + ",";
			sCsv += dr["PUB_TV_TALK_MIN"] + ",";
			sCsv += dr["PUB_TV_TALK_POINT"] + ",";
			sCsv += dr["PUB_TV_TALK_PAY_POINT"] + ",";
			sCsv += dr["PRV_VOICE_TALK_MIN"] + ",";
			sCsv += dr["PRV_VOICE_TALK_POINT"] + ",";
			sCsv += dr["PRV_VOICE_TALK_PAY_POINT"] + ",";
			sCsv += dr["PUB_VOICE_TALK_MIN"] + ",";
			sCsv += dr["PUB_VOICE_TALK_POINT"] + ",";
			sCsv += dr["PUB_VOICE_TALK_PAY_POINT"] + ",";
			sCsv += dr["VIEW_TALK_MIN"] + ",";
			sCsv += dr["VIEW_TALK_POINT"] + ",";
			sCsv += dr["VIEW_TALK_PAY_POINT"] + ",";
			sCsv += dr["WIRETAP_MIN"] + ",";
			sCsv += dr["WIRETAP_POINT"] + ",";
			sCsv += dr["WIRETAP_PAY_POINT"] + ",";
			sCsv += dr["VIEW_BROADCAST_MIN"] + ",";
			sCsv += dr["VIEW_BROADCAST_POINT"] + ",";
			sCsv += dr["VIEW_BROADCAST_PAY_POINT"] + ",";
			sCsv += dr["LIVE_MIN"] + ",";
			sCsv += dr["LIVE_POINT"] + ",";
			sCsv += dr["LIVE_PAY_POINT"] + ",";
			sCsv += dr["CAST_PRV_TV_TALK_MIN"] + ",";
			sCsv += dr["CAST_PRV_TV_TALK_POINT"] + ",";
			sCsv += dr["CAST_PRV_TV_TALK_PAY_POINT"] + ",";
			sCsv += dr["CAST_PUB_TV_TALK_MIN"] + ",";
			sCsv += dr["CAST_PUB_TV_TALK_POINT"] + ",";
			sCsv += dr["CAST_PUB_TV_TALK_PAY_POINT"] + ",";
			sCsv += dr["CAST_PRV_VOICE_TALK_MIN"] + ",";
			sCsv += dr["CAST_PRV_VOICE_TALK_POINT"] + ",";
			sCsv += dr["CAST_PRV_VOICE_TALK_PAY_POINT"] + ",";
			sCsv += dr["CAST_PUB_VOICE_TALK_MIN"] + ",";
			sCsv += dr["CAST_PUB_VOICE_TALK_POINT"] + ",";
			sCsv += dr["CAST_PUB_VOICE_TALK_PAY_POINT"] + ",";
			sCsv += dr["TALK_MIN"] + ",";
			sCsv += dr["TALK_POINT"] + ",";
			sCsv += dr["TALK_PAY_POINT"] + ",";
			sCsv += dr["USER_MAIL_COUNT"] + ",";
			sCsv += dr["USER_MAIL_POINT"] + ",";
			sCsv += dr["USER_MAIL_PAY_POINT"] + ",";
			sCsv += dr["USER_MAIL_PIC_COUNT"] + ",";
			sCsv += dr["USER_MAIL_PIC_POINT"] + ",";
			sCsv += dr["USER_MAIL_PIC_PAY_POINT"] + ",";
			sCsv += dr["USER_MAIL_MOVIE_COUNT"] + ",";
			sCsv += dr["USER_MAIL_MOVIE_POINT"] + ",";
			sCsv += dr["USER_MAIL_MOVIE_PAY_POINT"] + ",";
			sCsv += dr["OPEN_PIC_MAIL_COUNT"] + ",";
			sCsv += dr["OPEN_PIC_MAIL_POINT"] + ",";
			sCsv += dr["OPEN_PIC_MAIL_PAY_POINT"] + ",";
			sCsv += dr["OPEN_MOVIE_MAIL_COUNT"] + ",";
			sCsv += dr["OPEN_MOVIE_MAIL_POINT"] + ",";
			sCsv += dr["OPEN_MOVIE_MAIL_PAY_POINT"] + ",";
			sCsv += dr["OPEN_PIC_BBS_COUNT"] + ",";
			sCsv += dr["OPEN_PIC_BBS_POINT"] + ",";
			sCsv += dr["OPEN_PIC_BBS_PAY_POINT"] + ",";
			sCsv += dr["OPEN_MOVIE_BBS_COUNT"] + ",";
			sCsv += dr["OPEN_MOVIE_BBS_POINT"] + ",";
			sCsv += dr["OPEN_MOVIE_BBS_PAY_POINT"] + ",";
			if (bReleaseSaleMovie) {
				sCsv += dr["DOWNLOAD_MOVIE_COUNT"] + ",";
				sCsv += dr["DOWNLOAD_MOVIE_POINT"] + ",";
				sCsv += dr["DOWNLOAD_MOVIE_PAY_POINT"] + ",";
			}
			if (bReleaseTimeCall) {
				sCsv += dr["TIME_CALL_COUNT"] + ",";
				sCsv += dr["TIME_CALL_POINT"] + ",";
				sCsv += dr["TIME_CALL_PAY_POINT"] + ",";
			}
			if (bReleaseSaleVoice) {
				sCsv += dr["DOWNLOAD_VOICE_COUNT"] + ",";
				sCsv += dr["DOWNLOAD_VOICE_POINT"] + ",";
				sCsv += dr["DOWNLOAD_VOICE_PAY_POINT"] + ",";
			}
			if (bReleaseSocialGame) {
				sCsv += dr["SOCIAL_GAME_COUNT"] + ",";
				sCsv += dr["SOCIAL_GAME_POINT"] + ",";
				sCsv += dr["SOCIAL_GAME_PAY_POINT"] + ",";
			}
			sCsv += dr["YAKYUKEN_COUNT"] + ",";
			sCsv += dr["YAKYUKEN_POINT"] + ",";
			sCsv += dr["YAKYUKEN_PAY_POINT"] + ",";
			sCsv += dr["PRESENT_MAIL_COUNT"] + ",";
			sCsv += dr["PRESENT_MAIL_POINT"] + ",";
			sCsv += dr["PRESENT_MAIL_PAY_POINT"] + ",";
			sCsv += dr["WEB_TOTAL_COUNT"] + ",";
			sCsv += dr["WEB_TOTAL_POINT"] + ",";
			sCsv += dr["WEB_TOTAL_PAY_POINT"] + ",";
			sCsv += iTotalPoint.ToString() + ",";
			sCsv += iTotalPayPoint.ToString();
			sCsv += "\r\n";
		}
		return DisplayWordUtil.Replace(sCsv);
	}

	protected string CalcSum(object pSum1,object pSum2) {
		int iSum = int.Parse(pSum1.ToString()) + int.Parse(pSum2.ToString());
		return iSum.ToString();
	}
	protected void grdPoint_RowCreated(object sender,GridViewRowEventArgs e) {
		if ((e.Row.RowType == DataControlRowType.Header) ||
			(e.Row.RowType == DataControlRowType.DataRow) ||
			(e.Row.RowType == DataControlRowType.Footer)) {
			if (rdoTel.Checked) {
				e.Row.Cells[18].Visible = false;
				e.Row.Cells[19].Visible = false;
				e.Row.Cells[20].Visible = false;
				e.Row.Cells[21].Visible = false;
				e.Row.Cells[22].Visible = false;
				e.Row.Cells[23].Visible = false;
				e.Row.Cells[24].Visible = false;
				e.Row.Cells[25].Visible = false;
				e.Row.Cells[26].Visible = false;
				e.Row.Cells[27].Visible = false;
				e.Row.Cells[28].Visible = false;
				e.Row.Cells[29].Visible = false;
				e.Row.Cells[30].Visible = false;
				e.Row.Cells[31].Visible = false;
				e.Row.Cells[32].Visible = false;
			} else {
				e.Row.Cells[1].Visible = false;
				e.Row.Cells[2].Visible = false;
				e.Row.Cells[3].Visible = false;
				e.Row.Cells[4].Visible = false;
				e.Row.Cells[5].Visible = false;
				e.Row.Cells[6].Visible = false;
				e.Row.Cells[7].Visible = false;
				e.Row.Cells[8].Visible = false;
				e.Row.Cells[9].Visible = false;
				e.Row.Cells[10].Visible = false;
				e.Row.Cells[11].Visible = false;
				e.Row.Cells[12].Visible = false;
				e.Row.Cells[13].Visible = false;
				e.Row.Cells[14].Visible = false;
				e.Row.Cells[15].Visible = false;
				e.Row.Cells[16].Visible = false;
			}
		}

	}

	protected void dsManager_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {

		if (lstProductionSeq.Items.Count == 0) {
			e.InputParameters[0] = "";
		} else if (lstProductionSeq.Items.Count > 0 && lstProductionSeq.SelectedIndex == 0 && !string.IsNullOrEmpty(lstProductionSeq.Items[0].Value)) {
			e.InputParameters[0] = "";
		} else {
			e.InputParameters[0] = lstProductionSeq.SelectedValue;
		}
	}

	protected void lstProductionSeq_SelectedIndexChanged(object sender,EventArgs e) {
		lstManager.DataSourceID = "dsManager";
		lstManager.DataBind();
		lstManager.Items.Insert(0,new ListItem("",""));
		lstManager.DataSourceID = "";
	}

	protected void dsTimeOperation_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = lstFromYYYY.SelectedValue;
		e.InputParameters[2] = lstFromMM.SelectedValue;
		e.InputParameters[3] = lstFromDD.SelectedValue;
		e.InputParameters[4] = lstFromHH.SelectedValue;
		e.InputParameters[5] = lstToYYYY.SelectedValue;
		e.InputParameters[6] = lstToMM.SelectedValue;
		e.InputParameters[7] = lstToDD.SelectedValue;
		e.InputParameters[8] = lstToHH.SelectedValue;
		e.InputParameters[9] = lstProductionSeq.SelectedValue;
		e.InputParameters[10] = lstManager.SelectedValue;
	}
	protected bool GetBrCount() {
		if (chkCount.Checked) {
			if (chkPoint.Checked || chkPayPoint.Checked) {
				return true;
			}
		}
		return false;
	}

	protected bool GetBrPoint() {
		if (chkPoint.Checked) {
			if (chkPayPoint.Checked) {
				return true;
			}
		}
		return false;
	}

	protected bool CheckedCount() {
		return chkCount.Checked;
	}

	protected bool CheckedPoint() {
		return chkPoint.Checked;
	}

	protected bool CheckedPayPoint() {
		return chkPayPoint.Checked;
	}
}
