﻿/*************************************************************************
--	System			: ViComm Sales
--	Sub System Name	: Admin
--	Title			: リリース機能設定
--	Progaram ID		: ReleaseProgramList
--
--  Creation Date	: 10.08.18
--  Creater			: iBrid(Y.Igarashi)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class System_ReleaseProgramList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	protected void dsReleaseProgram_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		grdReleaseProgram.PageSize = 999;
		txtReleaseProgramNm.Text = "";
		txtReleaseProgramSummary.Text = "";
		ClearField();
		pnlMainte.Visible = false;
		DataBind();
	}

	private void ClearField() {
		txtReleaseProgramNm.Text = "";
		txtReleaseProgramSummary.Text = "";
		txtRequestCompany.Text = "";
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		if (IsValid) {
			GetData();
		}
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void lnkReleaseProgram_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		txtReleaseProgramId.Text = sKeys[0];
		txtReleaseProgramSubId.Text = sKeys[1];
		GetData();
		pnlKey.Visible = true;
		btnDelete.Enabled = true;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	private void GetList() {
		grdReleaseProgram.DataBind();
		pnlCount.DataBind();
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("RELEASE_PROGRAM_GET");
			db.ProcedureInParm("PRELEASE_PROGRAM_ID",DbSession.DbType.NUMBER,txtReleaseProgramId.Text);
			db.ProcedureInParm("PRELEASE_PROGRAM_SUB_ID",DbSession.DbType.NUMBER,txtReleaseProgramSubId.Text);
			db.ProcedureOutParm("PRELEASE_PROGRAM_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PRELEASE_PROGRAM_SUMMARY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREQUEST_COMPANY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtReleaseProgramNm.Text = db.GetStringValue("PRELEASE_PROGRAM_NM");
				txtReleaseProgramSummary.Text = db.GetStringValue("PRELEASE_PROGRAM_SUMMARY");
				txtRequestCompany.Text = db.GetStringValue("PREQUEST_COMPANY");
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;

	}

	private void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("RELEASE_PROGRAM_MAINTE");
			db.ProcedureInParm("PRELEASE_PROGRAM_ID",DbSession.DbType.NUMBER,txtReleaseProgramId.Text);
			db.ProcedureInParm("PRELEASE_PROGRAM_SUB_ID",DbSession.DbType.NUMBER,txtReleaseProgramSubId.Text);
			db.ProcedureInParm("PRELEASE_PROGRAM_NM",DbSession.DbType.VARCHAR2,txtReleaseProgramNm.Text);
			db.ProcedureInParm("PRELEASE_PROGRAM_SUMMARY",DbSession.DbType.VARCHAR2,txtReleaseProgramSummary.Text);
			db.ProcedureInParm("PREQUEST_COMPANY",DbSession.DbType.VARCHAR2,txtRequestCompany.Text);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		InitPage();
	}
}
