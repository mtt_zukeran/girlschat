﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: サイト情報メンテナンス
--	Progaram ID		: SiteList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;

public partial class System_SiteList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
	}

	protected void dsSite_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void FirstLoad() {
		lstOtherWebSysId.DataBind();
		lstOtherWebSysId.Items.Insert(0,new ListItem("",""));
		lstOtherWebSysId.DataSourceID = "";
		lstOtherWebSysId.SelectedValue = "";
	}

	private void InitPage() {
		grdSite.PageSize = int.Parse(Session["PageSize"].ToString());
		grdSite.PageIndex = 0;
		txtSiteCd.Text = "";
		ClearField();
		pnlMainte.Visible = false;
		DataBind();
		PlcVisible();
	}

	private void ClearField() {
		txtSiteNm.Text = string.Empty;
		txtSiteMark.Text = string.Empty;
		txtUrl.Text = string.Empty;
		txtWebPhisicalDir.Text = string.Empty;
		txtHostNm.Text = string.Empty;
		txtSubHostNm.Text = string.Empty;
		txtMailHost.Text = string.Empty;
		txtJobOfferSiteHostNm.Text = string.Empty;
		txtJobOfferStartDoc.Text = string.Empty;
		txtJobOfferSiteType.Text = string.Empty;
		txtLoMailerIP.Text = string.Empty;
		txtHiMailerIP.Text = string.Empty;
		txtLocalIpAddr.Text = string.Empty;
		txtLocalPort.Text = string.Empty;
		txtGlobalIpAddr.Text = string.Empty;
		txtIisSiteIdentifier.Text = string.Empty;
		txtIvpLocCd.Text = string.Empty;
		txtIvpSiteCd.Text = string.Empty;
		txtSiteType.Text = string.Empty;
		txtIvpObjectStoreFolder.Text = string.Empty;
		txtPriority.Text = string.Empty;
		txtNextBusinessTime.Text = string.Empty;
		txtWshotTalkDialoutNo.Text = string.Empty;
		txtWShotTalkStartGuidance.Text = string.Empty;
		txtPublicTalkDialoutNo.Text = string.Empty;
		txtPublicTalkStartGuidance.Text = string.Empty;
		txtWhitePlanNo.Text = string.Empty;
		txtPointPrice.Text = string.Empty;
		txtPointTax.Text = string.Empty;
		txtUserMailDelDays.Text = string.Empty;
		txtNotifyMailDelDays.Text = string.Empty;
		txtBlogMailDelDays.Text = string.Empty;
		txtMarkingDelDays.Text = "0";
		txtUserLogDelDays.Text = "0";
		txtTalkHistoryDelDays.Text = "0";
		txtBbsWriteDelDays.Text = "0";
		txtBbsPicDelDays.Text = "0";
		txtBbsMovieDelDays.Text = "0";
		txtUrgeDelDays.Text = "0";
		txtDiaryDelDays.Text = "0";
		txtManTweetDelDays.Text = "0";
		txtSummaryReportDelDays.Text = "0";
		txtReceiptDelDays.Text = "0";
		txtImportUserDelDays.Text = "0";
		txtManTempRegistDelDays.Text = "0";
		txtCastTempRegistDelDays.Text = "0";
		txtMailerFtpId.Text = string.Empty;
		txtMailerFtpPw.Text = string.Empty;
		txtPcRedirectUrl.Text = string.Empty;
		txtPageDescription.Text = string.Empty;
		txtPageKeyword.Text = string.Empty;
		txtPageTitle.Text = string.Empty;
		txtUrgeDays.Text = string.Empty;
		txtReceiptSight.Text = string.Empty;
		txtChargeStartPoint.Text = string.Empty;
		txtVcsWebLocCd.Text = string.Empty;
		txtVcsTenantCd.Text = string.Empty;
		txtUserTopId.Text = string.Empty;
		txtNonUserTopId.Text = string.Empty;
		txtCastToManBatchMailLimit.Text = string.Empty;
		txtInviteMailLimit.Text = string.Empty;
		txtGoogleVerification.Text = string.Empty;
		txtStampFileNm.Text = string.Empty;
		txtStampSampleFileNm.Text = string.Empty;
		txtPicSizeLarge.Text = string.Empty;
		txtPicSizeSmall.Text = string.Empty;
		txtPicBackColor.Text = string.Empty;
		txtTransferFee.Text = string.Empty;
		txtFirstTimeFreeTalkSec.Text = string.Empty;
		chkEnablePrivateTalkMenuFlag.Checked = false;
		chkMultiCharFlag.Checked = false;
		chkUseOtherSysInfoFlag.Checked = false;
		chkSupportPremiumTalkFlag.Checked = false;
		chkSupportKddiTvTelFlag.Checked = false;
		chkSupportChgMonitorToTalk.Checked = false;
		chkNaFlag.Checked = false;
		chkAvailablePayAfterFlag.Checked = false;
		chkUsedChargeSettleFlag.Checked = false;
		chkMobileNotAutoLogin.Checked = false;
		chkRegPrepaidAddSvPtFlag.Checked = false;
		chkCastPcModifyNaFlag.Checked = false;
		chkViewCallCountFlag.Checked = false;
		chkMailDocNaWebFaceFlag.Checked = false;
		txtManRegistFuncLimitAdCd.Text = string.Empty;
		txtMovieSiteUrl.Text = string.Empty;
		rdoScaleType.SelectedIndex = 0;
		lstLightChConnectType.SelectedIndex = 0;
		lstCastRegistReportTiming.SelectedIndex = 0;
		lstManRegistReportTiming.SelectedIndex = 0;
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(ViCommConst.FLAG_OFF);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(ViCommConst.FLAG_ON);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		pnlMainte.Visible = true;
		pnlKey.Enabled = true;
		pnlDtl.Visible = false;
	}

	protected void lnkSiteCd_Command(object sender,CommandEventArgs e) {
		txtSiteCd.Text = e.CommandArgument.ToString();
		GetData();
	}

	protected void vdcPriority_ServerValidate(object source,ServerValidateEventArgs args) {

		if (args.IsValid) {
			using (Site oSite = new Site()) {
				args.IsValid = !oSite.IsExistPriority(txtSiteCd.Text,txtPriority.Text);
			}
		}
	}

	protected void vdcPicBackColor_ServerValidate(object source,ServerValidateEventArgs args) {

		if (args.IsValid) {
			if (ViCommConst.ScaleType.SQUARE.Equals(this.rdoScaleType.SelectedValue)) {
				if (txtPicBackColor.Text.Equals(string.Empty)) {
					args.IsValid = false;
				}
			}
		}
	}

	protected void vdcOtherWebSysId_ServerValidate(object source,ServerValidateEventArgs args) {
		if (chkUseOtherSysInfoFlag.Checked) {
			if (lstOtherWebSysId.SelectedValue.Equals("")) {
				args.IsValid = false;
			}
		}
	}

	private void GetData() {
		this.ClearField();

		int iOtherWebSysIdCount = lstOtherWebSysId.Items.Count;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SITE_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,txtSiteCd.Text);
			db.ProcedureOutParm("PSITE_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSITE_MARK",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PURL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PWEB_PHISICAL_DIR",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHOST_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSUB_HOST_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMAIL_HOST",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PJOB_OFFER_SITE_HOST_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PJOB_OFFER_START_DOC",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PJOB_OFFER_SITE_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PTX_LO_MAILER_IP",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PTX_HI_MAILER_IP",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMAILER_FTP_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMAILER_FTP_PW",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PLOCAL_IP_ADDR",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PLOCAL_PORT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PGLOBAL_IP_ADDR",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PIIS_SITE_IDENTIFIER",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PIVP_LOC_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PIVP_SITE_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PIVP_OBJECT_STORE_FOLDER",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSITE_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPRIORITY",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PNEXT_BUSINESS_TIME",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PWSHOT_TALK_DIALOUT_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PWSHOT_TALK_START_GUIDANCE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPUB_TALK_DIALOUT_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPUB_TALK_START_GUIDANCE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PWHITE_PLAN_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PENABLE_PRIVATE_TALK_MENU_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PPOINT_PRICE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PPOINT_TAX",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PUSER_MAIL_DEL_DAYS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PNOTIFY_MAIL_DEL_DAYS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PBLOG_MAIL_DEL_DAYS", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMARKING_DEL_DAYS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PUSED_LOG_DEL_DAYS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PTALK_HISTORY_DEL_DAYS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PBBS_WRITE_DEL_DAYS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PBBS_PIC_DEL_DAYS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PBBS_MOVIE_DEL_DAYS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PURGE_DEL_DAYS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PDIARY_DEL_DAYS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMAN_TWEET_DEL_DAYS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSUMMARY_REPORT_DEL_DAYS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECEIPT_DEL_DAYS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PIMPORT_USER_DEL_DAYS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMAN_TEMP_REGIST_DEL_DAYS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_TEMP_REGIST_DEL_DAYS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PUSE_OTHER_SYS_INFO_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("POTHER_WEB_SYS_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPC_REDIRECT_URL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPAGE_TITLE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPAGE_KEYWORD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPAGE_DESCRIPTION",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCSS_DOCOMO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCSS_SOFTBANK",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCSS_AU",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCSS_IPHONE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCSS_ANDROID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMULTI_CHAR_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSUPPORT_PREMIUM_TALK_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSUPPORT_KDDI_TV_TEL_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSUPPORT_CHG_MONITOR_TO_TALK",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PNA_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PAVAILABLE_PAY_AFTER_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PUSED_CHARGE_SETTLE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCHARGE_START_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECEIPT_SIGHT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PURGE_DAYS",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PLIGHT_CH_CONNECT_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PVCS_TENANT_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PVCS_WEB_LOC_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PUSER_TOP_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PNON_USER_TOP_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_TO_MAN_BATCH_MAIL_LIMIT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PINVITE_MAIL_LIMIT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMOBILE_NOT_AUTO_LOGIN",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("REG_PREPAID_ADD_SV_PT_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_PC_MODIFY_NA_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PVIEW_CALL_COUNT_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PGOOGLE_VERIFICATION",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTAMP_FILE_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTAMP_SAMPLE_FILE_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPIC_SIZE_LARGE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PPIC_SIZE_SMALL",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PPIC_BACK_COLOR",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSCALE_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMAIL_SEARCH_ENABLED",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMAIL_SEARCH_LIMIT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PTRANSFER_FEE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_REGIST_REPORT_TIMING",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMAN_REGIST_REPORT_TIMING",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PFIRST_TIME_FREE_TALK_SEC",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMAIL_DOC_NA_WEB_FACE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMAN_REGIST_FUNC_LIMIT_AD_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMOVIE_SITE_URL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtSiteNm.Text = db.GetStringValue("PSITE_NM");
				txtSiteMark.Text = db.GetStringValue("PSITE_MARK");
				txtUrl.Text = db.GetStringValue("PURL");
				txtWebPhisicalDir.Text = db.GetStringValue("PWEB_PHISICAL_DIR");
				txtHostNm.Text = db.GetStringValue("PHOST_NM");
				txtSubHostNm.Text = db.GetStringValue("PSUB_HOST_NM");
				txtMailHost.Text = db.GetStringValue("PMAIL_HOST");
				txtJobOfferSiteHostNm.Text = db.GetStringValue("PJOB_OFFER_SITE_HOST_NM");
				txtJobOfferStartDoc.Text = db.GetStringValue("PJOB_OFFER_START_DOC");
				txtJobOfferSiteType.Text = db.GetStringValue("PJOB_OFFER_SITE_TYPE");
				txtLoMailerIP.Text = db.GetStringValue("PTX_LO_MAILER_IP");
				txtHiMailerIP.Text = db.GetStringValue("PTX_HI_MAILER_IP");
				txtMailerFtpId.Text = db.GetStringValue("PMAILER_FTP_ID");
				txtMailerFtpPw.Text = db.GetStringValue("PMAILER_FTP_PW");
				txtLocalIpAddr.Text = db.GetStringValue("PLOCAL_IP_ADDR");
				txtLocalPort.Text = db.GetStringValue("PLOCAL_PORT");
				txtGlobalIpAddr.Text = db.GetStringValue("PGLOBAL_IP_ADDR");
				txtIisSiteIdentifier.Text = db.GetStringValue("PIIS_SITE_IDENTIFIER");
				txtIvpLocCd.Text = db.GetStringValue("PIVP_LOC_CD");
				txtIvpSiteCd.Text = db.GetStringValue("PIVP_SITE_CD");
				txtSiteType.Text = db.GetStringValue("PSITE_TYPE");
				txtIvpObjectStoreFolder.Text = db.GetStringValue("PIVP_OBJECT_STORE_FOLDER");
				txtPriority.Text = db.GetStringValue("PPRIORITY");
				txtNextBusinessTime.Text = db.GetStringValue("PNEXT_BUSINESS_TIME");
				txtWshotTalkDialoutNo.Text = db.GetStringValue("PWSHOT_TALK_DIALOUT_NO");
				txtWShotTalkStartGuidance.Text = db.GetStringValue("PWSHOT_TALK_START_GUIDANCE");
				txtPublicTalkDialoutNo.Text = db.GetStringValue("PPUB_TALK_DIALOUT_NO");
				txtPublicTalkStartGuidance.Text = db.GetStringValue("PPUB_TALK_START_GUIDANCE");
				txtWhitePlanNo.Text = db.GetStringValue("PWHITE_PLAN_NO");
				txtPointPrice.Text = db.GetStringValue("PPOINT_PRICE");
				txtPointTax.Text = db.GetDecimalValue("PPOINT_TAX").ToString("f2");
				txtUserMailDelDays.Text = db.GetStringValue("PUSER_MAIL_DEL_DAYS");
				txtNotifyMailDelDays.Text = db.GetStringValue("PNOTIFY_MAIL_DEL_DAYS");
				txtBlogMailDelDays.Text = db.GetStringValue("PBLOG_MAIL_DEL_DAYS");
				txtMarkingDelDays.Text = db.GetStringValue("PMARKING_DEL_DAYS");
				txtUserLogDelDays.Text = db.GetStringValue("PUSED_LOG_DEL_DAYS");
				txtTalkHistoryDelDays.Text = db.GetStringValue("PTALK_HISTORY_DEL_DAYS");
				txtBbsWriteDelDays.Text = db.GetStringValue("PBBS_WRITE_DEL_DAYS");
				txtBbsPicDelDays.Text = db.GetStringValue("PBBS_PIC_DEL_DAYS");
				txtBbsMovieDelDays.Text = db.GetStringValue("PBBS_MOVIE_DEL_DAYS");
				txtUrgeDelDays.Text = db.GetStringValue("PURGE_DEL_DAYS");
				txtDiaryDelDays.Text = db.GetStringValue("PDIARY_DEL_DAYS");
				txtManTweetDelDays.Text = db.GetStringValue("PMAN_TWEET_DEL_DAYS");
				txtSummaryReportDelDays.Text = db.GetStringValue("PSUMMARY_REPORT_DEL_DAYS");
				txtReceiptDelDays.Text = db.GetStringValue("PRECEIPT_DEL_DAYS");
				txtImportUserDelDays.Text = db.GetStringValue("PIMPORT_USER_DEL_DAYS");
				txtManTempRegistDelDays.Text = db.GetStringValue("PMAN_TEMP_REGIST_DEL_DAYS");
				txtCastTempRegistDelDays.Text = db.GetStringValue("PCAST_TEMP_REGIST_DEL_DAYS");
				txtPcRedirectUrl.Text = db.GetStringValue("PPC_REDIRECT_URL");
				txtPageTitle.Text = db.GetStringValue("PPAGE_TITLE");
				txtPageKeyword.Text = db.GetStringValue("PPAGE_KEYWORD");
				txtPageDescription.Text = db.GetStringValue("PPAGE_DESCRIPTION");
				txtCssDocomo.Text = db.GetStringValue("PCSS_DOCOMO");
				txtCssSoftBank.Text = db.GetStringValue("PCSS_SOFTBANK");
				txtCssAU.Text = db.GetStringValue("PCSS_AU");
				txtCssiPhone.Text = db.GetStringValue("PCSS_IPHONE");
				txtCssAndroid.Text = db.GetStringValue("PCSS_ANDROID");
				txtUrgeDays.Text = db.GetStringValue("PURGE_DAYS");
				txtReceiptSight.Text = db.GetStringValue("PRECEIPT_SIGHT");
				txtChargeStartPoint.Text = db.GetStringValue("PCHARGE_START_POINT");
				txtVcsWebLocCd.Text = db.GetStringValue("PVCS_WEB_LOC_CD");
				txtVcsTenantCd.Text = db.GetStringValue("PVCS_TENANT_CD");
				txtUserTopId.Text = db.GetStringValue("PUSER_TOP_ID");
				txtNonUserTopId.Text = db.GetStringValue("PNON_USER_TOP_ID");
				txtCastToManBatchMailLimit.Text = db.GetStringValue("PCAST_TO_MAN_BATCH_MAIL_LIMIT");
				txtInviteMailLimit.Text = db.GetStringValue("PINVITE_MAIL_LIMIT");
				txtGoogleVerification.Text = db.GetStringValue("PGOOGLE_VERIFICATION");
				txtStampFileNm.Text = db.GetStringValue("PSTAMP_FILE_NM");
				txtStampSampleFileNm.Text = db.GetStringValue("PSTAMP_SAMPLE_FILE_NM");
				txtPicSizeLarge.Text = db.GetStringValue("PPIC_SIZE_LARGE");
				txtPicSizeSmall.Text = db.GetStringValue("PPIC_SIZE_SMALL");
				txtPicBackColor.Text = db.GetStringValue("PPIC_BACK_COLOR");
				chkMultiCharFlag.Checked = db.GetStringValue("PMULTI_CHAR_FLAG").ToString().Equals("1");
				chkSupportPremiumTalkFlag.Checked = db.GetStringValue("PSUPPORT_PREMIUM_TALK_FLAG").ToString().Equals("1");
				chkSupportKddiTvTelFlag.Checked = db.GetStringValue("PSUPPORT_KDDI_TV_TEL_FLAG").ToString().Equals("1");
				chkEnablePrivateTalkMenuFlag.Checked = db.GetStringValue("PENABLE_PRIVATE_TALK_MENU_FLAG").ToString().Equals("1");
				chkUseOtherSysInfoFlag.Checked = db.GetStringValue("PUSE_OTHER_SYS_INFO_FLAG").ToString().Equals("1");
				chkSupportChgMonitorToTalk.Checked = db.GetStringValue("PSUPPORT_CHG_MONITOR_TO_TALK").ToString().Equals("1");
				chkNaFlag.Checked = db.GetStringValue("PNA_FLAG").ToString().Equals("1");
				chkAvailablePayAfterFlag.Checked = db.GetStringValue("PAVAILABLE_PAY_AFTER_FLAG").ToString().Equals("1");
				chkUsedChargeSettleFlag.Checked = db.GetStringValue("PUSED_CHARGE_SETTLE_FLAG").ToString().Equals("1");
				chkMobileNotAutoLogin.Checked = db.GetStringValue("PMOBILE_NOT_AUTO_LOGIN").ToString().Equals("1");
				chkRegPrepaidAddSvPtFlag.Checked = db.GetStringValue("REG_PREPAID_ADD_SV_PT_FLAG").ToString().Equals("1");
				chkCastPcModifyNaFlag.Checked = db.GetStringValue("PCAST_PC_MODIFY_NA_FLAG").ToString().Equals("1");
				chkViewCallCountFlag.Checked = db.GetStringValue("PVIEW_CALL_COUNT_FLAG").ToString().Equals("1");
				chkMailSearchEnabled.Checked = db.GetIntValue("PMAIL_SEARCH_ENABLED").ToString().Equals("1");
				chkMailDocNaWebFaceFlag.Checked = db.GetIntValue("PMAIL_DOC_NA_WEB_FACE_FLAG").ToString().Equals("1");
				txtMailSearchLimit.Text = db.GetIntValue("PMAIL_SEARCH_LIMIT").ToString();
				txtTransferFee.Text = db.GetStringValue("PTRANSFER_FEE").ToString();
				txtManRegistFuncLimitAdCd.Text = db.GetStringValue("PMAN_REGIST_FUNC_LIMIT_AD_CD");

				this.rdoScaleType.SelectedValue = db.GetStringValue("PSCALE_TYPE");

				lstLightChConnectType.SelectedValue = db.GetStringValue("PLIGHT_CH_CONNECT_TYPE");
				if (!db.GetStringValue("PCAST_REGIST_REPORT_TIMING").Equals(string.Empty)) {
					lstCastRegistReportTiming.SelectedValue = db.GetStringValue("PCAST_REGIST_REPORT_TIMING");
				}
				if (!db.GetStringValue("PMAN_REGIST_REPORT_TIMING").Equals(string.Empty)) {
					lstManRegistReportTiming.SelectedValue = db.GetStringValue("PMAN_REGIST_REPORT_TIMING");
				}
				if (db.GetStringValue("POTHER_WEB_SYS_ID").Equals("") == false) {
					using (OtherWebSite oOtherWebSite = new OtherWebSite()) {
						if (oOtherWebSite.GetUnusedPageCount() < iOtherWebSysIdCount - 1) {
							lstOtherWebSysId.Items.RemoveAt(1);
						}
						string sOtherWebSiteNm = "";
						if (oOtherWebSite.GetOne(db.GetStringValue("POTHER_WEB_SYS_ID"))) {
							sOtherWebSiteNm = oOtherWebSite.siteNm;
						}
						lstOtherWebSysId.Items.Insert(1,new ListItem(sOtherWebSiteNm,db.GetStringValue("POTHER_WEB_SYS_ID")));
					}
					lstOtherWebSysId.SelectedIndex = 1;
				} else {
					using (OtherWebSite oOtherWebSite = new OtherWebSite()) {
						if (oOtherWebSite.GetUnusedPageCount() < iOtherWebSysIdCount - 1) {
							lstOtherWebSysId.Items.RemoveAt(1);
						}
					}
					lstOtherWebSysId.SelectedIndex = 0;
				}
				txtFirstTimeFreeTalkSec.Text = db.GetStringValue("PFIRST_TIME_FREE_TALK_SEC");
				txtMovieSiteUrl.Text = db.GetStringValue("PMOVIE_SITE_URL");

			}
		}

		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;

	}

	private void UpdateData(int pDelFlag) {

		int iEnablePrivateTalkMenuFlag = 0;
		int iUseOtherSysInfoFlag = 0;
		int iNaFlag = 0;
		int iMailSearchEnabled = (this.chkMailSearchEnabled.Checked) ? 1 : 0;

		if (chkEnablePrivateTalkMenuFlag.Checked) {
			iEnablePrivateTalkMenuFlag = 1;
		}

		if (chkUseOtherSysInfoFlag.Checked) {
			iUseOtherSysInfoFlag = 1;
		}

		if (chkNaFlag.Checked) {
			iNaFlag = 1;
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SITE_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,txtSiteCd.Text);
			db.ProcedureInParm("PSITE_NM",DbSession.DbType.VARCHAR2,txtSiteNm.Text);
			db.ProcedureInParm("PSITE_MARK",DbSession.DbType.VARCHAR2,txtSiteMark.Text);
			db.ProcedureInParm("PURL",DbSession.DbType.VARCHAR2,txtUrl.Text);
			db.ProcedureInParm("PWEB_PHISICAL_DIR",DbSession.DbType.VARCHAR2,txtWebPhisicalDir.Text);
			db.ProcedureInParm("PHOST_NM",DbSession.DbType.VARCHAR2,txtHostNm.Text);
			db.ProcedureInParm("PSUB_HOST_NM",DbSession.DbType.VARCHAR2,txtSubHostNm.Text);
			db.ProcedureInParm("PMAIL_HOST",DbSession.DbType.VARCHAR2,txtMailHost.Text);
			db.ProcedureInParm("PJOB_OFFER_SITE_HOST_NM",DbSession.DbType.VARCHAR2,txtJobOfferSiteHostNm.Text);
			db.ProcedureInParm("PJOB_OFFER_START_DOC",DbSession.DbType.VARCHAR2,txtJobOfferStartDoc.Text);
			db.ProcedureInParm("PJOB_OFFER_SITE_TYPE",DbSession.DbType.VARCHAR2,txtJobOfferSiteType.Text);
			db.ProcedureInParm("PTX_LO_MAILER_IP",DbSession.DbType.VARCHAR2,txtLoMailerIP.Text);
			db.ProcedureInParm("PTX_HI_MAILER_IP",DbSession.DbType.VARCHAR2,txtHiMailerIP.Text);
			db.ProcedureInParm("PMAILER_FTP_ID",DbSession.DbType.VARCHAR2,txtMailerFtpId.Text);
			db.ProcedureInParm("PMAILER_FTP_PW",DbSession.DbType.VARCHAR2,txtMailerFtpPw.Text);
			db.ProcedureInParm("PLOCAL_IP_ADDR",DbSession.DbType.VARCHAR2,txtLocalIpAddr.Text);
			db.ProcedureInParm("PLOCAL_PORT",DbSession.DbType.NUMBER,int.Parse(txtLocalPort.Text));
			db.ProcedureInParm("PGLOBAL_IP_ADDR",DbSession.DbType.VARCHAR2,txtGlobalIpAddr.Text);
			db.ProcedureInParm("PIIS_SITE_IDENTIFIER",DbSession.DbType.VARCHAR2,txtIisSiteIdentifier.Text);
			db.ProcedureInParm("PIVP_LOC_CD",DbSession.DbType.VARCHAR2,txtIvpLocCd.Text);
			db.ProcedureInParm("PIVP_SITE_CD",DbSession.DbType.VARCHAR2,txtIvpSiteCd.Text);
			db.ProcedureInParm("PIVP_OBJECT_STORE_FOLDER",DbSession.DbType.VARCHAR2,txtIvpObjectStoreFolder.Text);
			db.ProcedureInParm("PSITE_TYPE",DbSession.DbType.VARCHAR2,txtSiteType.Text);
			db.ProcedureInParm("PPRIORITY",DbSession.DbType.VARCHAR2,txtPriority.Text);
			db.ProcedureInParm("PNEXT_BUSINESS_TIME",DbSession.DbType.VARCHAR2,txtNextBusinessTime.Text);
			db.ProcedureInParm("PWSHOT_TALK_DIALOUT_NO",DbSession.DbType.VARCHAR2,txtWshotTalkDialoutNo.Text);
			db.ProcedureInParm("PWSHOT_TALK_START_GUIDANCE",DbSession.DbType.VARCHAR2,txtWShotTalkStartGuidance.Text);
			db.ProcedureInParm("PPUB_TALK_DIALOUT_NO",DbSession.DbType.VARCHAR2,txtPublicTalkDialoutNo.Text);
			db.ProcedureInParm("PPUB_TALK_START_GUIDANCE",DbSession.DbType.VARCHAR2,txtPublicTalkStartGuidance.Text);
			db.ProcedureInParm("PWHITE_PLAN_NO",DbSession.DbType.VARCHAR2,txtWhitePlanNo.Text);
			db.ProcedureInParm("PENABLE_PRIVATE_TALK_MENU_FLAG",DbSession.DbType.NUMBER,iEnablePrivateTalkMenuFlag);
			db.ProcedureInParm("PPOINT_PRICE",DbSession.DbType.NUMBER,int.Parse(txtPointPrice.Text));
			db.ProcedureInParm("PPOINT_TAX",DbSession.DbType.NUMBER,float.Parse(txtPointTax.Text));
			db.ProcedureInParm("PUSER_MAIL_DEL_DAYS",DbSession.DbType.NUMBER,int.Parse(txtUserMailDelDays.Text));
			db.ProcedureInParm("PNOTIFY_MAIL_DEL_DAYS", DbSession.DbType.NUMBER, int.Parse(txtNotifyMailDelDays.Text));
			db.ProcedureInParm("PBLOG_MAIL_DEL_DAYS", DbSession.DbType.NUMBER, int.Parse(txtBlogMailDelDays.Text));
			db.ProcedureInParm("PMARKING_DEL_DAYS",DbSession.DbType.NUMBER,int.Parse(txtMarkingDelDays.Text));
			db.ProcedureInParm("PUSED_LOG_DEL_DAYS",DbSession.DbType.NUMBER,int.Parse(txtUserLogDelDays.Text));
			db.ProcedureInParm("PTALK_HISTORY_DEL_DAYS",DbSession.DbType.NUMBER,int.Parse(txtTalkHistoryDelDays.Text));
			db.ProcedureInParm("PBBS_WRITE_DEL_DAYS",DbSession.DbType.NUMBER,int.Parse(txtBbsWriteDelDays.Text));
			db.ProcedureInParm("PBBS_PIC_DEL_DAYS",DbSession.DbType.NUMBER,int.Parse(txtBbsPicDelDays.Text));
			db.ProcedureInParm("PBBS_MOVIE_DEL_DAYS",DbSession.DbType.NUMBER,int.Parse(txtBbsMovieDelDays.Text));
			db.ProcedureInParm("PURGE_DEL_DAYS",DbSession.DbType.NUMBER,int.Parse(txtUrgeDelDays.Text));
			db.ProcedureInParm("PDIARY_DEL_DAYS",DbSession.DbType.NUMBER,int.Parse(txtDiaryDelDays.Text));
			db.ProcedureInParm("PMAN_TWEET_DEL_DAYS",DbSession.DbType.NUMBER,int.Parse(txtManTweetDelDays.Text));
			db.ProcedureInParm("PSUMMARY_REPORT_DEL_DAYS",DbSession.DbType.NUMBER,int.Parse(txtSummaryReportDelDays.Text));
			db.ProcedureInParm("PRECEIPT_DEL_DAYS",DbSession.DbType.NUMBER,int.Parse(txtReceiptDelDays.Text));
			db.ProcedureInParm("PIMPORT_USER_DEL_DAYS",DbSession.DbType.NUMBER,int.Parse(txtImportUserDelDays.Text));
			db.ProcedureInParm("PMAN_TEMP_REGIST_DEL_DAYS",DbSession.DbType.NUMBER,int.Parse(txtManTempRegistDelDays.Text));
			db.ProcedureInParm("PCAST_TEMP_REGIST_DEL_DAYS",DbSession.DbType.NUMBER,int.Parse(txtCastTempRegistDelDays.Text));
			db.ProcedureInParm("PUSE_OTHER_SYS_INFO_FLAG",DbSession.DbType.NUMBER,iUseOtherSysInfoFlag);
			db.ProcedureInParm("POTHER_WEB_SYS_ID",DbSession.DbType.VARCHAR2,lstOtherWebSysId.SelectedValue);
			db.ProcedureInParm("PPC_REDIRECT_URL",DbSession.DbType.VARCHAR2,txtPcRedirectUrl.Text);
			db.ProcedureInParm("PPAGE_TITLE",DbSession.DbType.VARCHAR2,txtPageTitle.Text);
			db.ProcedureInParm("PPAGE_KEYWORD",DbSession.DbType.VARCHAR2,txtPageKeyword.Text);
			db.ProcedureInParm("PPAGE_DESCRIPTION",DbSession.DbType.VARCHAR2,txtPageDescription.Text);
			db.ProcedureInParm("PCSS_DOCOMO",DbSession.DbType.VARCHAR2,txtCssDocomo.Text);
			db.ProcedureInParm("PCSS_SOFTBANK",DbSession.DbType.VARCHAR2,txtCssSoftBank.Text);
			db.ProcedureInParm("PCSS_AU",DbSession.DbType.VARCHAR2,txtCssAU.Text);
			db.ProcedureInParm("PCSS_IPHONE",DbSession.DbType.VARCHAR2,txtCssiPhone.Text);
			db.ProcedureInParm("PCSS_ANDROID",DbSession.DbType.VARCHAR2,txtCssAndroid.Text);
			db.ProcedureInParm("PMULTI_CHAR_FLAG",DbSession.DbType.NUMBER,chkMultiCharFlag.Checked);
			db.ProcedureInParm("PSUPPORT_PREMIUM_TALK_FLAG",DbSession.DbType.NUMBER,chkSupportPremiumTalkFlag.Checked);
			db.ProcedureInParm("PSUPPORT_KDDI_TV_TEL_FLAG",DbSession.DbType.NUMBER,chkSupportKddiTvTelFlag.Checked);
			db.ProcedureInParm("PSUPPORT_CHG_MONITOR_TO_TALK",DbSession.DbType.NUMBER,chkSupportChgMonitorToTalk.Checked);
			db.ProcedureInParm("PNA_FLAG",DbSession.DbType.NUMBER,iNaFlag);
			db.ProcedureInParm("PAVAILABLE_PAY_AFTER_FLAG",DbSession.DbType.NUMBER,chkAvailablePayAfterFlag.Checked);
			db.ProcedureInParm("PUSED_CHARGE_SETTLE_FLAG",DbSession.DbType.NUMBER,chkUsedChargeSettleFlag.Checked);
			db.ProcedureInParm("PCHARGE_START_POINT",DbSession.DbType.NUMBER,int.Parse(txtChargeStartPoint.Text));
			db.ProcedureInParm("PRECEIPT_SIGHT",DbSession.DbType.NUMBER,int.Parse(txtReceiptSight.Text));
			db.ProcedureInParm("PURGE_DAYS",DbSession.DbType.NUMBER,int.Parse(txtUrgeDays.Text));
			db.ProcedureInParm("PLIGHT_CH_CONNECT_TYPE",DbSession.DbType.VARCHAR2,lstLightChConnectType.SelectedValue);
			db.ProcedureInParm("PVCS_TENANT_CD",DbSession.DbType.VARCHAR2,txtVcsTenantCd.Text);
			db.ProcedureInParm("PVCS_WEB_LOC_CD",DbSession.DbType.VARCHAR2,txtVcsWebLocCd.Text);
			db.ProcedureInParm("PUSER_TOP_ID",DbSession.DbType.VARCHAR2,txtUserTopId.Text);
			db.ProcedureInParm("PNON_USER_TOP_ID",DbSession.DbType.VARCHAR2,txtNonUserTopId.Text);
			db.ProcedureInParm("PCAST_TO_MAN_BATCH_MAIL_LIMIT",DbSession.DbType.NUMBER,int.Parse(txtCastToManBatchMailLimit.Text));
			db.ProcedureInParm("PINVITE_MAIL_LIMIT",DbSession.DbType.NUMBER,int.Parse(txtInviteMailLimit.Text));
			db.ProcedureInParm("PMOBILE_NOT_AUTO_LOGIN",DbSession.DbType.NUMBER,chkMobileNotAutoLogin.Checked);
			db.ProcedureInParm("PREG_PREPAID_ADD_SV_PT_FLAG",DbSession.DbType.NUMBER,chkRegPrepaidAddSvPtFlag.Checked);
			db.ProcedureInParm("PCAST_PC_MODIFY_NA_FLAG",DbSession.DbType.NUMBER,chkCastPcModifyNaFlag.Checked);
			db.ProcedureInParm("PVIEW_CALL_COUNT_FLAG",DbSession.DbType.NUMBER,chkViewCallCountFlag.Checked);
			db.ProcedureInParm("PGOOGLE_VERIFICATION",DbSession.DbType.VARCHAR2,txtGoogleVerification.Text);
			db.ProcedureInParm("PSTAMP_FILE_NM",DbSession.DbType.VARCHAR2,txtStampFileNm.Text);
			db.ProcedureInParm("PSTAMP_SAMPLE_FILE_NM",DbSession.DbType.VARCHAR2,txtStampSampleFileNm.Text);
			db.ProcedureInParm("PPIC_SIZE_LARGE",DbSession.DbType.NUMBER,txtPicSizeLarge.Text);
			db.ProcedureInParm("PPIC_SIZE_SMALL",DbSession.DbType.NUMBER,txtPicSizeSmall.Text);
			db.ProcedureInParm("PPIC_BACK_COLOR",DbSession.DbType.VARCHAR2,txtPicBackColor.Text);
			db.ProcedureInParm("PSCALE_TYPE",DbSession.DbType.VARCHAR2,this.rdoScaleType.SelectedValue);
			db.ProcedureInParm("PMAIL_SEARCH_ENABLED",DbSession.DbType.VARCHAR2,iMailSearchEnabled);
			db.ProcedureInParm("PMAIL_SEARCH_LIMIT",DbSession.DbType.VARCHAR2,txtMailSearchLimit.Text);
			db.ProcedureInParm("PTRANSFER_FEE",DbSession.DbType.NUMBER,int.Parse(txtTransferFee.Text));
			db.ProcedureInParm("PCAST_REGIST_REPORT_TIMING",DbSession.DbType.VARCHAR2,lstCastRegistReportTiming.SelectedValue);
			db.ProcedureInParm("PMAN_REGIST_REPORT_TIMING",DbSession.DbType.VARCHAR2,lstManRegistReportTiming.SelectedValue);
			db.ProcedureInParm("PFIRST_TIME_FREE_TALK_SEC",DbSession.DbType.NUMBER,int.Parse(txtFirstTimeFreeTalkSec.Text));
			db.ProcedureInParm("PMAIL_DOC_NA_WEB_FACE_FLAG",DbSession.DbType.NUMBER,chkMailDocNaWebFaceFlag.Checked);
			db.ProcedureInParm("pMAN_REGIST_FUNC_LIMIT_AD_CD",DbSession.DbType.VARCHAR2,txtManRegistFuncLimitAdCd.Text);
			db.ProcedureInParm("pMOVIE_SITE_URL",DbSession.DbType.VARCHAR2,txtMovieSiteUrl.Text);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		if (lstOtherWebSysId.SelectedValue != "") {
			lstOtherWebSysId.Items.RemoveAt(lstOtherWebSysId.SelectedIndex);
		}

		InitPage();
	}

	private void PlcVisible() {
		plcVisible1.Visible = false;
		plcVisible2.Visible = false;
		plcVisible3.Visible = false;
		plcVisible4.Visible = false;
		plcVisible5.Visible = false;
	}

	protected void lst_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}
		oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}
}
