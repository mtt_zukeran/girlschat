<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="LiveCameraList.aspx.cs" Inherits="System_LiveCameraList" Title="ライブカメラ設定" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="ライブカメラ設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 600px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									カメラID
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCameraId" runat="server" MaxLength="2" Width="32px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrCameraId" runat="server" ErrorMessage="カメラIDを入力して下さい。" ControlToValidate="txtCameraId" ValidationGroup="Key">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeCameraId" runat="server" ErrorMessage="カメラIDは2桁の数字で入力して下さい。" ValidationExpression="\d{2}" ControlToValidate="txtCameraId" ValidationGroup="Key">*</asp:RegularExpressionValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[カメラ内容]</legend>
						<table border="0" style="width: 500px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									カメラ名称
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCameraNm" runat="server" MaxLength="30" Width="160px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrCameraNm" runat="server" ErrorMessage="カメラ名称を入力して下さい。" ControlToValidate="txtCameraNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									シェアードメディア名
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtSharedMediaNm" runat="server" MaxLength="12" Width="160px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrSharedMediaNm" runat="server" ErrorMessage="シェアードメディア名を入力して下さい。" ControlToValidate="txtSharedMediaNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[ライブカメラ一覧]</legend>
			<asp:GridView ID="grdCamera" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsLiveCamera" AllowSorting="True" SkinID="GridViewColor" >
				<Columns>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkCameraId" runat="server" Text='<%# Eval("CAMERA_ID") %>' CommandArgument='<%# Eval("CAMERA_ID") %>' OnCommand="lnkCameraId_Command" CausesValidation="False"></asp:LinkButton>
						</ItemTemplate>
						<HeaderTemplate>
							カメラID
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Center" />
					</asp:TemplateField>
					<asp:BoundField DataField="CAMERA_NM" HeaderText="カメラ名称"></asp:BoundField>
					<asp:BoundField DataField="SHARED_MEDIA_NM" HeaderText="シェアードメディア名"></asp:BoundField>
					<asp:BoundField DataField="UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<a class="reccount">Record Count
				<%#GetRecCount() %>
			</a>
			<br>
			<a class="reccount">Current viewing page
				<%=grdCamera.PageIndex + 1%>
				of
				<%=grdCamera.PageCount%>
			</a>
			<div class="button">
				<asp:Button ID="btnRegist" runat="server" Text="ライブカメラ追加" OnClick="btnRegist_Click" />
			</div>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsLiveCamera" runat="server" SelectMethod="GetPageCollection" TypeName="LiveCamera" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelected="dsLiveCamera_Selected"></asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrCameraId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeCameraId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrCameraNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdrSharedMediaNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
