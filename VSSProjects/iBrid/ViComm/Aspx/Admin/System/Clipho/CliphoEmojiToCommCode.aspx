<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CliphoEmojiToCommCode.aspx.cs" Inherits="System_Clipho_CliphoEmojiToCommCode"
	Title="男性会員取得（ジャストテレコム）" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="クリフォ・絵文字変換"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[ディレクト指定]</legend>
				<table border="0" style="width: 400px" class="tableStyle">
					<tr>
						<td>
							<asp:TextBox ID="txtDir" Text="D:\CNV" runat="server" Width="374px"></asp:TextBox>
						</td>
					</tr>
		
				</table>
				<br />
				<asp:Button ID="btnExec" runat="server" Text="実行" ValidationGroup="Upload" Width="150px" OnClick="btnExec_Click" />
				<asp:CheckBox ID="chkMediaMode" runat="server" Text="メディア モード"></asp:CheckBox>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnExec" ConfirmText="絵文字変換を行ないますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
