﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: クリフォ・絵文字変換

--	Progaram ID		: CliphoEmojiToCommCode
--
--  Creation Date	: 2010.06.09
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.IO;
using System.Text;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using iBridCommLib;
using MobileLib;
using ViComm;

public partial class System_Clipho_CliphoEmojiToCommCode:System.Web.UI.Page {


	private static Regex mediaEmojiTagRegex = new Regex("(?<start>[<]{1}EMJ)(?<index>[0-9]+)(?<end>[>]{1})", RegexOptions.Compiled);
	private static readonly string[] mediaEmojiArray = new string[]{string.Empty,
			"$xE63E;","$xE63F;","$xE640;","$xE641;","$xE642;","$xE643;","$xE644;","$xE645;","$xE646;","$xE647;",
			"$xE648;","$xE649;","$xE64A;","$xE64B;","$xE64C;","$xE64D;","$xE64E;","$xE64F;","$xE650;","$xE651;",
			"$xE652;","$xE653;","$xE654;","$xE655;","$xE656;","$xE657;","$xE658;","$xE659;","$xE65A;","$xE65B;",
			"$xE65C;","$xE65D;","$xE65E;","$xE65F;","$xE660;","$xE661;","$xE662;","$xE663;","$xE664;","$xE665;",
			"$xE666;","$xE667;","$xE668;","$xE669;","$xE66A;","$xE66B;","$xE66C;","$xE66D;","$xE66E;","$xE66F;",
			"$xE670;","$xE671;","$xE672;","$xE673;","$xE674;","$xE675;","$xE676;","$xE677;","$xE678;","$xE679;",
			"$xE67A;","$xE67B;","$xE67C;","$xE67D;","$xE67E;","$xE67F;","$xE680;","$xE681;","$xE682;","$xE683;",
			"$xE684;","$xE685;","$xE686;","$xE687;","$xE688;","$xE689;","$xE68A;","$xE68B;","$xE68C;","$xE68D;",
			"$xE68E;","$xE68F;","$xE690;","$xE691;","$xE692;","$xE693;","$xE694;","$xE695;","$xE696;","$xE697;",
			"$xE698;","$xE699;","$xE69A;","$xE69B;","$xE69C;","$xE69D;","$xE69E;","$xE69F;","$xE6A0;","$xE6A1;",
			"$xE6A2;","$xE6A3;","$xE6A4;","$xE6A5;","$xE6CE;","$xE6CF;","$xE6D0;","$xE6D1;","$xE6D2;","$xE6D3;",
			"$xE6D4;","$xE6D5;","$xE6D6;","$xE6D7;","$xE6D8;","$xE6D9;","$xE6DA;","$xE6DB;","$xE6DC;","$xE6DD;",
			"$xE6DE;","$xE6DF;","$xE6E0;","$xE6E1;","$xE6E2;","$xE6E3;","$xE6E4;","$xE6E5;","$xE6E6;","$xE6E7;",
			"$xE6E8;","$xE6E9;","$xE6EA;","$xE6EB;","$xE70B;","$xE6EC;","$xE6ED;","$xE6EE;","$xE6EF;","$xE6F0;",
			"$xE6F1;","$xE6F2;","$xE6F3;","$xE6F4;","$xE6F5;","$xE6F6;","$xE6F7;","$xE6F8;","$xE6F9;","$xE6FA;",
			"$xE6FB;","$xE6FC;","$xE6FD;","$xE6FE;","$xE6FF;","$xE700;","$xE701;","$xE702;","$xE703;","$xE704;",
			"$xE705;","$xE706;","$xE707;","$xE708;","$xE709;","$xE70A;","$xE6AC;","$xE6AD;","$xE6AE;","$xE6B1;",
			"$xE6B2;","$xE6B3;","$xE6B7;","$xE6B8;","$xE6B9;","$xE6BA;","$xE70C;","$xE70D;","$xE70E;","$xE70F;",
			"$xE710;","$xE711;","$xE712;","$xE713;","$xE714;","$xE715;","$xE716;","$xE717;","$xE718;","$xE719;",
			"$xE71A;","$xE71B;","$xE71C;","$xE71D;","$xE71E;","$xE71F;","$xE720;","$xE721;","$xE722;","$xE723;",
			"$xE724;","$xE725;","$xE726;","$xE727;","$xE728;","$xE729;","$xE72A;","$xE72B;","$xE72C;","$xE72D;",
			"$xE72E;","$xE72F;","$xE730;","$xE731;","$xE732;","$xE733;","$xE734;","$xE735;","$xE736;","$xE737;",
			"$xE738;","$xE739;","$xE73A;","$xE73B;","$xE73C;","$xE73D;","$xE73E;","$xE73F;","$xE740;","$xE741;",
			"$xE742;","$xE743;","$xE744;","$xE745;","$xE746;","$xE747;","$xE748;","$xE749;","$xE74A;","$xE74B;",
			"$xE74C;","$xE74D;","$xE74E;","$xE74F;","$xE750;","$xE751;","$xE752;","$xE753;","$xE754;","$xE755;",
			"$xE756;","$xE757;"		
		};
		
	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
	}


	protected void btnExec_Click(object sender,EventArgs e) {

		string[] filelist = System.IO.Directory.GetFiles(txtDir.Text,"*.csv",System.IO.SearchOption.AllDirectories);
		string sLine,sReg;
		bool bSep;
		int[] iSepPos = new int[3];
		int[] iMaxCnt = new int[3];
		MatchEvaluator oMatch = new MatchEvaluator(RegexMatchEvaluator1);

		string sNewFileNm = "";
		foreach (string sFileNm in filelist) {
			sNewFileNm = sFileNm + ".new";

			iSepPos[0] = -1;
			iSepPos[1] = -1;
			iSepPos[2] = -1;

			iMaxCnt[0] = -1;
			iMaxCnt[1] = -1;
			iMaxCnt[2] = -1;
			bSep = false;
			
			if(this.chkMediaMode.Checked){
				if (sFileNm.ToLower().Equals(txtDir.Text.ToLower() + "\\maillog.csv")) {
					bSep = true;
					iSepPos[0] = 8;
					iMaxCnt[0] = 4;
				} else if (sFileNm.ToLower().Equals(txtDir.Text.ToLower() + "\\middleprocess.csv")) {
					bSep = true;
					iSepPos[0] = 7;
					iMaxCnt[0] = 4;
				}
			}else{
				if (sFileNm.Equals(txtDir.Text + "\\man_information.csv")) {
					bSep = true;
					iSepPos[0] = 2;
					iMaxCnt[0] = 5;
				} else if (sFileNm.Equals(txtDir.Text + "\\woman_information.csv")) {
					bSep = true;
					iSepPos[0] = 2;
					iMaxCnt[0] = 5;
				} else if (sFileNm.Equals(txtDir.Text + "\\woman_mailbox.csv")) {
					bSep = true;
					iSepPos[0] = 12;
					iMaxCnt[0] = 4;
				} else if (sFileNm.Equals(txtDir.Text + "\\woman_manysendmsg_hist.csv")) {
					bSep = true;
					iSepPos[0] = 3;
					iMaxCnt[0] = 4;
				} else if (sFileNm.Equals(txtDir.Text + "\\man_draft_box.csv")) {
					bSep = true;
					iSepPos[0] = 7;
					iMaxCnt[0] = 4;
				} else if (sFileNm.Equals(txtDir.Text + "\\woman_draft_box.csv")) {
					bSep = true;
					iSepPos[0] = 7;
					iMaxCnt[0] = 4;
				} else if (sFileNm.Equals(txtDir.Text + "\\man_mailbox.csv")) {
					bSep = true;
					iSepPos[0] = 12;
					iMaxCnt[0] = 4;
				} else if (sFileNm.Equals(txtDir.Text + "\\man_ownermail_history.csv")) {
					bSep = true;
					iSepPos[0] = 5;
					iMaxCnt[0] = 4;
				} else if (sFileNm.Equals(txtDir.Text + "\\woman_ownermail_history.csv")) {
					bSep = true;
					iSepPos[0] = 5;
					iMaxCnt[0] = 4;
				} else if (sFileNm.Equals(txtDir.Text + "\\man_mailbox_blog.csv")) {
					bSep = true;
					iSepPos[0] = 12;
					iMaxCnt[0] = 4;
				} else if (sFileNm.Equals(txtDir.Text + "\\man_mailbox_cache.csv")) {
					bSep = true;
					iSepPos[0] = 6;
					iMaxCnt[0] = 4;
				} else if (sFileNm.Equals(txtDir.Text + "\\woman_blog_article.csv")) {
					bSep = true;
					iSepPos[0] = 8;
					iMaxCnt[0] = 4;
				} else if (sFileNm.Equals(txtDir.Text + "\\woman_mailbox_cache.csv")) {
					bSep = true;
					iSepPos[0] = 6;
					iMaxCnt[0] = 4;
				} else if (sFileNm.Equals(txtDir.Text + "\\girls_w_wbbs2_comment.csv")) {
					bSep = true;
					iSepPos[0] = 5;
					iMaxCnt[0] = 4;
				} 
			}
			
			
						

			

			Regex regex1 = new Regex(@"(\$.\&amp;|\$.\&gt;|\$.\&lt;|\$.\&quot;|\$.\&#039;)");

			StreamWriter writer = null;
			try{
				writer = new StreamWriter(sNewFileNm,false,Encoding.GetEncoding("Shift_JIS"));
				
				using (StreamReader sr = new StreamReader(sFileNm,Encoding.GetEncoding("Shift_JIS"))) {
					
					long lCycle = 1 * 1024 * 1024 * 1024; // 1G
					long lStreamsize = lCycle;
					
					string sRow = string.Empty;
					
					while ((sLine = sr.ReadLine()) != null) {
						
						sRow += sLine;
						
						if(!this.chkMediaMode.Checked || sRow.EndsWith("[[END]]")){
							sReg = regex1.Replace(sRow, oMatch);
							sRow = Mobile.CliphoEmojiToCommTag(sReg);
							if(this.chkMediaMode.Checked){
								sRow = mediaEmojiTagRegex.Replace(sRow, new MatchEvaluator(delegate(Match pMatch) {
									string sIndex = pMatch.Groups["index"].Value;
									int iIndex = 0;
									if (!int.TryParse(sIndex, out iIndex)) {
										return string.Empty;
									}

									if (mediaEmojiArray.Length <= iIndex) {
										return string.Empty;
									}

									return mediaEmojiArray[iIndex];
								}));
							}
							
							if (bSep) {
								SeparateStr(sRow, iSepPos, iMaxCnt, writer);
							} else {
								writer.WriteLine(sRow);
							}

							if (writer.BaseStream.Position > lStreamsize) {
								lStreamsize += lCycle;
								writer.Flush();
								writer.Close();
								writer.Dispose();
								// ファイルに追記する
								writer = new StreamWriter(sNewFileNm, true, Encoding.GetEncoding("Shift_JIS"));
							}
							
							sRow = string.Empty;
						}else{
							sRow += Environment.NewLine;
						}
					}
					writer.Flush();
					writer.Close();

				}
			}finally{
				if(writer != null) {
					writer.Dispose();
				}
			}

		}
	}
	public static string RegexMatchEvaluator1(Match match) {
		string sHead = match.Value.Substring(0,2);
		string sValue = match.Value.Substring(2);

		switch (sValue) {
			case "&amp;":
				return sHead + "&";
			case "&gt;":
				return sHead + ">";
			case "&lt;":
				return sHead + "<";
			case "&quot;":
				return sHead + "\"";
			case "&#039;":
				return sHead + "'";
		}
		return "";
	}

	private void SeparateStr(string pOriginalDoc,int[] pSepPos,int[] pMaxCnt,StreamWriter writer) {
		string[] sNewDoc = new string[100];
		string[] sSepDoc;
		int iDocCount;
		int i,j,k,iMaxCnt;
		
		string sSeparator = ",";
		if(this.chkMediaMode.Checked){
			sSeparator = "#,#";
		}

		string[] sDoc = pOriginalDoc.Split(new string[] { sSeparator }, StringSplitOptions.None);
		int iNewCnt = 0;
		bool bSep;

		for (i = 0;i < sDoc.Length;i++) {
			bSep = false;
			iMaxCnt = 0;
			for (k = 0;k < pSepPos.Length;k++) {
				if (pSepPos[k] == i) {
					bSep = true;
					iMaxCnt = pMaxCnt[k];
					break;
				}
			}
			if (sDoc[i].Length >= 2) {
				if (sDoc[i].Substring(0,1).Equals("\"")) {
					sDoc[i] = sDoc[i].Substring(1);
				}

				if (sDoc[i].Substring(sDoc[i].Length - 1,1).Equals("\"")) {
					sDoc[i] = sDoc[i].Substring(0,sDoc[i].Length - 1);
				}
			}

			if (bSep) {
				this.SeparateHtml(sDoc[i],iMaxCnt,out sSepDoc,out iDocCount);
				for (j = 0;j < iDocCount;j++) {
					sNewDoc[iNewCnt++] = sSepDoc[j];
				}
				for (j = iDocCount;j < iMaxCnt;j++) {
					sNewDoc[iNewCnt++] = "";
				}
			} else {
				sNewDoc[iNewCnt++] = sDoc[i];
			}
		}
		writer.WriteLine(string.Join(sSeparator, sNewDoc, 0, iNewCnt));
	}

	#region □■□ SysProgramから移管 □■□ ======================================================
	
	private static readonly Regex regex1 = new Regex(@"(\$\w{1,}\(.+?\);\s|\$\w{1,};\s)", RegexOptions.Compiled);
	private static readonly Regex regex2 = new Regex(@"(\s</.+?>)", RegexOptions.Compiled);

	public void SeparateHtml(string pDoc, int pMaxAarrySize, out string[] pHtmlDoc, out int pBlockCount) {
		pHtmlDoc = new string[pMaxAarrySize];
		pDoc = pDoc.Replace("middle", "center");
		pDoc = pDoc.Replace("<TBODY>", "");
		pDoc = pDoc.Replace("</TBODY>", "");
		pDoc = pDoc.Replace("utn=\"\">", "utn>");
		pBlockCount = pDoc.Length / 994;
		if ((pDoc.Length % 994) > 0) {
			pBlockCount++;
		}

		if (pBlockCount > pMaxAarrySize) {
			pBlockCount = pMaxAarrySize;
		}
		int iPos = 0;
		pDoc = iBridUtil.GetStringValue(pDoc);

		for (int i = 0; i < pBlockCount; i++) {
			if ((iPos + 1006) <= pDoc.Length) {
				pHtmlDoc[i] = SysPrograms.Substring(pDoc.Substring(iPos, 1006), 994);
				iPos += pHtmlDoc[i].Length;
			} else {
				if ((pDoc.Length - iPos) <= 0) {
					pBlockCount = i - 1;
					break;
				}
				pHtmlDoc[i] = pDoc.Substring(iPos, pDoc.Length - iPos);
			}
			pHtmlDoc[i] = regex1.Replace(pHtmlDoc[i], new MatchEvaluator(RegexMatchEvaluator3));
			pHtmlDoc[i] = regex2.Replace(pHtmlDoc[i], new MatchEvaluator(RegexMatchEvaluator4));
		}
	}

	public static string RegexMatchEvaluator3(Match match) {
		return match.Value.Trim();
	}

	public static string RegexMatchEvaluator4(Match match) {
		return match.Value.Trim();
	}
	
	#endregion ====================================================================================



}
