﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReleaseProgramList.aspx.cs" Inherits="System_ReleaseProgramList"
	Title="リリース機能設定" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="リリース機能設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[リリース機能内容]</legend>
						<table border="0" style="width: 600px" class="tableStyle">
							<asp:Panel runat="server" ID="pnlKey">
								<tr>
									<td class="tdHeaderStyle">
										ﾘﾘｰｽ機能ｺｰﾄﾞ
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtReleaseProgramId" runat="server" MaxLength="20" Width="100px"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										ｻﾌﾞｺｰﾄﾞ
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtReleaseProgramSubId" runat="server" MaxLength="1" Width="20px"></asp:TextBox>
									</td>
								</tr>
							</asp:Panel>
							<tr>
								<td class="tdHeaderStyle">
									ﾘﾘｰｽ機能名
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtReleaseProgramNm" runat="server" MaxLength="64" Width="360px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrReleaseProgramNm" runat="server" ErrorMessage="リリース機能名を入力して下さい。" ControlToValidate="txtReleaseProgramNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾘﾘｰｽ機能概要
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtReleaseProgramSummary" runat="server" MaxLength="512" Width="360px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrReleaseProgramSummary" runat="server" ErrorMessage="リリース機能概要を入力して下さい。" ControlToValidate="txtReleaseProgramSummary"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									依頼元
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtRequestCompany" runat="server" MaxLength="128" Width="360px"></asp:TextBox>
								</td>
							</tr>
						</table>
					</fieldset>
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
					<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
					<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset class="fieldset">
			<legend>[リリース機能一覧]</legend>
			<asp:GridView ID="grdReleaseProgram" AllowPaging="True" runat="server" AutoGenerateColumns="False" DataSourceID="dsReleaseProgram" AllowSorting="True"
				SkinID="GridViewColor">
				<Columns>
					<asp:TemplateField>
						<HeaderTemplate>
							ﾘﾘｰｽ機能ｺｰﾄﾞ
						</HeaderTemplate>
						<ItemTemplate>
							<asp:LinkButton ID="lnkReleaseProgram" runat="server" Text='<%# Eval("RELEASE_PROGRAM_ID") %>' CommandArgument='<%# string.Format("{0}:{1}",Eval("RELEASE_PROGRAM_ID"),Eval("RELEASE_PROGRAM_SUB_ID")) %>'
								OnCommand="lnkReleaseProgram_Command" CausesValidation="False">
							</asp:LinkButton>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:BoundField DataField="RELEASE_PROGRAM_SUB_ID" HeaderText="ｻﾌﾞｺｰﾄﾞ">
						<ItemStyle HorizontalAlign="Left" />
					</asp:BoundField>
					<asp:BoundField DataField="RELEASE_PROGRAM_NM" HeaderText="ﾘﾘｰｽ機能名">
						<ItemStyle HorizontalAlign="Left" />
					</asp:BoundField>
					<asp:BoundField DataField="RELEASE_PROGRAM_SUMMARY" HeaderText="ﾘﾘｰｽ機能概要">
						<ItemStyle HorizontalAlign="Left" />
					</asp:BoundField>
					<asp:BoundField DataField="REQUEST_COMPANY" HeaderText="依頼元">
						<ItemStyle HorizontalAlign="Left" />
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br />
				<a class="reccount">Current viewing page
					<%=grdReleaseProgram.PageIndex + 1%>
					of
					<%=grdReleaseProgram.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
	</div>
	<asp:ObjectDataSource ID="dsReleaseProgram" runat="server" SelectMethod="GetPageCollection" TypeName="ReleaseProgram" SelectCountMethod="GetPageCount"
		OnSelected="dsReleaseProgram_Selected" EnablePaging="True"></asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrReleaseProgramNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrReleaseProgramSummary" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
