﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="VariableConvert.aspx.cs" Inherits="System_VariableConvert" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="変数変換"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HolderContent" Runat="Server">
    <div class="admincontent">
        <asp:Panel ID="pnlMainte" runat="server">
            <fieldset class="fieldset">
                <legend>[ファイル指定]</legend>
                <asp:Label ID="lblStatus1" runat="server" Text="変換対象ファイルの変数変換を行います。"></asp:Label><br />
                <asp:Label ID="lblStatus2" runat="server" Text="下記のフォームに変数変換表ファイルを指定してください。"></asp:Label><br />
                <asp:FileUpload ID="uplVariableFile" runat="server" Width="500px" />
                <asp:RequiredFieldValidator ID="valrVariableFile" runat="server" ControlToValidate="uplVariableFile"
                    ErrorMessage="変数変換表ファイルを入力して下さい。" Text="*" ValidationGroup="Convert"></asp:RequiredFieldValidator>
                <br />
                <asp:Label ID="lblStatus3" runat="server" Text="下記のフォームに変換対象ファイルを指定してください。"></asp:Label><br />
                <asp:FileUpload ID="uplHtmlCsvFile" runat="server" Width="500px" />
                <asp:RequiredFieldValidator ID="valrHtmlCsvFile" runat="server" ControlToValidate="uplHtmlCsvFile"
                    ErrorMessage="サイト構成文章ファイルを入力して下さい。" Text="*" ValidationGroup="Convert"></asp:RequiredFieldValidator><br />
                <asp:Button ID="btnConvert" runat="server" OnClick="btnConvert_Click" Text="変換実行" ValidationGroup="Convert"
                    Width="150px" />
            </fieldset>
        </asp:Panel>
        <br />
    </div>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmOnFormSubmit="true"
        ConfirmText="変数変換を行ないますか？" TargetControlID="btnConvert">
    </ajaxToolkit:ConfirmButtonExtender>
</asp:Content>

