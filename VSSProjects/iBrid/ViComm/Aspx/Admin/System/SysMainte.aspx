<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SysMainte.aspx.cs" Inherits="System_SysMainte" Title="システム設定" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="システム設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[システム設定内容]</legend>
						<table border="0" style="width: 680px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle" style="width:200px">
									SIP登録用IVP管理拠点ｺｰﾄﾞ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtSipIvpLocCd" runat="server" Width="60px" MaxLength="2"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrSipIvpLocCd" runat="server" ErrorMessage="ＳＩＰ登録用ＩＶＰ管理拠点コードを入力して下さい。" ControlToValidate="txtSipIvpLocCd" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeSipIvpLocCd" runat="server" ErrorMessage="ＳＩＰ登録用ＩＶＰ管理拠点コードは2桁の英数字で入力して下さい。" ValidationExpression="\w{2}" ControlToValidate="txtSipIvpLocCd"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									SIP登録用IVPｻｲﾄｺｰﾄﾞ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtSipIvpSiteCd" runat="server" Width="140px" MaxLength="12"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrSipIvpSiteCd" runat="server" ErrorMessage="ＳＩＰ登録用ＩＶＰサイトコードを入力して下さい。" ControlToValidate="txtSipIvpSiteCd" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeSipIvpSiteCd" runat="server" ErrorMessage="ＳＩＰ登録用ＩＶＰサイトコードは2〜12桁の英数字で入力して下さい。" ValidationExpression="\w{2,12}" ControlToValidate="txtSipIvpSiteCd"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									SIP登録用URL
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtSipRegistUrl" runat="server" Width="420px" MaxLength="256"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrSipRegistUrl" runat="server" ErrorMessage="ＳＩＰ登録用ＵＲＬを入力して下さい。" ControlToValidate="txtSipRegistUrl" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeSipRegistUrl" runat="server" ErrorMessage="ＳＩＰ登録用ＵＲＬを正しく入力して下さい。" ValidationExpression="(\S+)://([^:/]+)(:(\d+))?(/[^#\s]*)(#(\S+))?"
										ControlToValidate="txtSipRegistUrl" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									SIPﾄﾞﾒｲﾝ(Global接続)
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtSipDomain" runat="server" Width="140px" MaxLength="64"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrSipDomain" runat="server" ErrorMessage="ＳＩＰドメイン(Global接続)を入力して下さい。" ControlToValidate="txtSipDomain" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeSipDomain" runat="server" ErrorMessage="ＳＩＰドメイン(Global接続)は64桁以内の英数字で入力して下さい。" ValidationExpression="[a-z0-9A-z\._-]+"
										ControlToValidate="txtSipDomain" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									SIPﾄﾞﾒｲﾝ(Local接続)
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtSipDomainLocal" runat="server" Width="140px" MaxLength="64"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrSipDomainLocal" runat="server" ErrorMessage="ＳＩＰドメイン(Local接続)を入力して下さい。" ControlToValidate="txtSipDomainLocal" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeSipDomainLocal" runat="server" ErrorMessage="ＳＩＰドメイン(Local接続)は64桁以内の英数字で入力して下さい。" ValidationExpression="[a-z0-9A-z\._-]+"
										ControlToValidate="txtSipDomainLocal" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									IVP ADMIN用URL
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtIvpAdminUrl" runat="server" Width="420px" MaxLength="256"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrIvpAdminUrl" runat="server" ErrorMessage="IVP ADMIN用URLを入力して下さい。" ControlToValidate="txtIvpAdminUrl" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeIvpAdminUrl" runat="server" ErrorMessage="IVP ADMIN用URLを正しく入力して下さい。" ValidationExpression="(\S+)://([^:/]+)(:(\d+))?(/[^#\s]*)(#(\S+))?"
										ControlToValidate="txtIvpAdminUrl" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									IVP要求URL
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtIvpRequestUrl" runat="server" Width="420px" MaxLength="256"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrIvpRequestUrl" runat="server" ErrorMessage="IVP要求URLを入力して下さい。" ControlToValidate="txtIvpRequestUrl" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeIvpRequestUrl" runat="server" ErrorMessage="IVP要求URLを正しく入力して下さい。" ValidationExpression="(\S+)://([^:/]+)(:(\d+))?(/[^#\s]*)(#(\S+))?"
										ControlToValidate="txtIvpRequestUrl" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									共有ﾗｲﾌﾞﾄｰｸｷｰ発行URL
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtIvpIssueLiveKeyUrl" runat="server" Width="420px" MaxLength="256"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrIvpIssueLiveKeyUrl" runat="server" ErrorMessage="共有ﾗｲﾌﾞﾄｰｸｷｰ発行URLを入力して下さい。" ControlToValidate="txtIvpIssueLiveKeyUrl"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeIvpIssueLiveKeyUrl" runat="server" ErrorMessage="共有ﾗｲﾌﾞﾄｰｸｷｰ発行URLを正しく入力して下さい。" ValidationExpression="(\S+)://([^:/]+)(:(\d+))?(/[^#\s]*)(#(\S+))?"
										ControlToValidate="txtIvpIssueLiveKeyUrl" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾗｲﾌﾞｶﾒﾗ起動URL
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtIvpStartupCamera" runat="server" Width="420px" MaxLength="256"></asp:TextBox>
									<asp:RegularExpressionValidator ID="vdeIvpStartupCamera" runat="server" ErrorMessage="ライブカメラ起動URLを正しく入力して下さい。" ValidationExpression="(\S+)://([^:/]+)(:(\d+))?(/[^#\s]*)(#(\S+))?"
										ControlToValidate="txtIvpStartupCamera" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									IVPﾗｲﾌﾞｷｰ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLiveKey" runat="server" MaxLength="12" Width="140px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrLiveKey" runat="server" ErrorMessage="ＩＶＰライブキーを入力して下さい。" ControlToValidate="txtLiveKey" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ｲﾝｽﾄｰﾙﾃﾞｨﾚｸﾄﾘ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtInstallDir" runat="server" Width="420px" MaxLength="256"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrInstallDir" runat="server" ErrorMessage="インストールディレクトリを入力して下さい。" ControlToValidate="txtInstallDir" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle" style="height: 26px">
									ﾃﾞｺﾒｰﾙｻｰﾊﾞｰ種別
								</td>
								<td class="tdDataStyle" style="height: 26px">
									<asp:DropDownList ID="lstDecomMailServerType" runat="server" DataSourceID="dsServerType" DataTextField="CODE_NM" DataValueField="CODE" Width="150px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									1POSTあたりのﾒｰﾙ配信上限数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtMailTxUnit" runat="server" Width="40px" MaxLength="3"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrMailTxUnit" runat="server" ErrorMessage="メール配信上限数を入力して下さい。" ControlToValidate="txtMailTxUnit" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeMailTxUnit" runat="server" ErrorMessage="メール配信上限数は2〜5桁の数字で入力して下さい。" ValidationExpression="\d{2,5}" ControlToValidate="txtMailTxUnit"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									1POST後のﾒｰﾙ配信待機秒数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtMailTxCycleSec" runat="server" Width="40px" MaxLength="3"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrMailTxCycleSec" runat="server" ErrorMessage="メール配信待機秒数を入力して下さい。" ControlToValidate="txtMailTxCycleSec" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeMailTxCycleSec" runat="server" ErrorMessage="メール配信待機秒数は2〜3桁の数字で入力して下さい。" ValidationExpression="\d{2,3}" ControlToValidate="txtMailTxCycleSec"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾒｰﾙ配信不可対象NGﾒｰﾙ数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtMarkMailAddrNgCount" runat="server" Width="40px" MaxLength="3"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrMarkMailAddrNgCount" runat="server" ErrorMessage="NGメール数を入力して下さい。" ControlToValidate="txtMarkMailAddrNgCount" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeMarkMailAddrNgCount" runat="server" ErrorMessage="NGメール数は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtMarkMailAddrNgCount"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									VICOMM ﾒｰﾙ要求受付URL
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtMailIFUrl" runat="server" Width="420px" MaxLength="256"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrMailIFUrl" runat="server" ErrorMessage="メール振分用URLを入力して下さい。" ControlToValidate="txtMailIFUrl" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeMailIFUrl" runat="server" ErrorMessage="メール振分用URLを正しく入力して下さい。" ValidationExpression="(\S+)://([^:/]+)(:(\d+))?(/[^#\s]*)(#(\S+))?"
										ControlToValidate="txtMailIFUrl" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾃｷｽﾄﾒｰﾙﾘﾚｰ先
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkTextMailOtherRelayFlag" runat="server" Text="ﾃｷｽﾄﾒｰﾙの送信時に他社ﾘﾚｰを使用する。" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ｱﾄﾞﾚｽｸﾘｰﾆﾝｸﾞ許可
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkAvaCheckExistAddrFlag" runat="server" Text="ﾒｰﾙｱﾄﾞﾚｽのｸﾘｰﾆﾝｸﾞ処理を許可する。" /><br>
									<asp:Label ID="lblGuide" runat="server" Text="ACCELﾒｰﾙの設定及びxmailｱｶｳﾝﾄcleaning@が必要" ForeColor="red"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									VICOMM VCS要求受付URL
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtVcsIFUrl" runat="server" Width="420px" MaxLength="256"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrVcsIFUrl" runat="server" ErrorMessage="VCS要求振分用URLを入力して下さい。" ControlToValidate="txtVcsIFUrl" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeVcsIFUrl" runat="server" ErrorMessage="VCS要求振分用URLを正しく入力して下さい。" ValidationExpression="(\S+)://([^:/]+)(:(\d+))?(/[^#\s]*)(#(\S+))?"
										ControlToValidate="txtVcsIFUrl" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<asp:PlaceHolder ID="plcMaqiaAffiliate" runat="server" Visible="false">
								<tr>
									<td class="tdHeaderStyle">
										VICOMM ｱﾌﾘｴｰﾄ利用可能
									</td>
									<td class="tdDataStyle">
										<asp:CheckBox ID="chkAvailableMaqiaAffiliateFlag" runat="server" />
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										VICOMM ｱﾌﾘｴｰﾄURL
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtAffiliIFUrl" runat="server" Width="420px" MaxLength="256"></asp:TextBox>
										<asp:RegularExpressionValidator ID="vdeAffiliIFUrl" runat="server" ErrorMessage="ｱﾌﾘｴｰﾄURLを正しく入力して下さい。" ValidationExpression="(\S+)://([^:/]+)(:(\d+))?(/[^#\s]*)(#(\S+))?"
											ControlToValidate="txtAffiliIFUrl" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
							</asp:PlaceHolder>
							<tr>
								<td class="tdHeaderStyle">
									ｸﾚｼﾞｯﾄ従量制決済要求受付URL
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCreditMeasuredRateIFUrl" runat="server" Width="420px" MaxLength="256"></asp:TextBox>
									<asp:RegularExpressionValidator ID="vdeCreditMeasuredRateIFUrl" runat="server" ErrorMessage="ｸﾚｼﾞｯﾄ従量制決済要求受付URLを正しく入力して下さい。" ValidationExpression="(\S+)://([^:/]+)(:(\d+))?(/[^#\s]*)(#(\S+))?"
										ControlToValidate="txtCreditMeasuredRateIFUrl" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									VCS要求URL
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtVcsRequestUrl" runat="server" Width="420px" MaxLength="256"></asp:TextBox>
									<asp:RegularExpressionValidator ID="vdeVcsRequestUrl" runat="server" ErrorMessage="VCS-LIGHTチャネル要求URLを正しく入力して下さい。" ValidationExpression="(\S+)://([^:/]+)(:(\d+))?(/[^#\s]*)(#(\S+))?"
										ControlToValidate="txtVcsRequestUrl" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									VCS FTP IP
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtVcsFtpIP" runat="server" MaxLength="15"></asp:TextBox>
									<asp:RegularExpressionValidator ID="vdeVcsFtpIP" runat="server" ErrorMessage="VCS IPを正しく入力して下さい。" ValidationExpression="^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$"
										ValidationGroup="Detail" ControlToValidate="txtVcsFtpIP">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									VCS FTP ID
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtVcsFtpId" runat="server" Width="100px" MaxLength="32"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									VCS FTP PW
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtVcsFtpPw" runat="server" Width="100px" MaxLength="32"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾎﾜｲﾄﾌﾟﾗﾝ利用不可時刻
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtWhitePlanNaTime" runat="server" Width="30px" MaxLength="5"></asp:TextBox>
									〜 01:00の間ﾎﾜｲﾄﾌﾟﾗﾝを利用しての通話を許可しない
									<asp:RegularExpressionValidator ID="vdeWhitePlanNaTime" runat="server" ErrorMessage="ﾎﾜｲﾄﾌﾟﾗﾝ利用不可開始時刻は99:99の形式で入力して下さい。" ValidationExpression="^([01]+[0-9]|2[0-3]):([0-5][0-9])$"
										ControlToValidate="txtWhitePlanNaTime" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾎﾜｲﾄﾌﾟﾗﾝ自動切断時刻
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtWhitePlanDisconnTime" runat="server" Width="30px" MaxLength="5"></asp:TextBox>
									<asp:RegularExpressionValidator ID="vdeWhitePlanDisconnTime" runat="server" ErrorMessage="ﾎﾜｲﾄﾌﾟﾗﾝ自動切断時刻は99:99の形式で入力して下さい。" ValidationExpression="^([01]+[0-9]|2[0-3]):([0-5][0-9])$"
										ControlToValidate="txtWhitePlanDisconnTime" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									<asp:CompareValidator ID="vdcWhitePlanDisconnTime" runat="server" ErrorMessage="ﾎﾜｲﾄﾌﾟﾗﾝ自動切断時刻がﾎﾜｲﾄﾌﾟﾗﾝ利用不可時刻以前になっています。" ControlToCompare="txtWhitePlanNaTime"
										ControlToValidate="txtWhitePlanDisconnTime" ValidationGroup="Detail" Operator="GreaterThanEqual" Display="Dynamic">*</asp:CompareValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									出演者報酬ｸﾘｱ日数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtNoLoginClearPayDays" runat="Server" MaxLength="3" Width="25px"></asp:TextBox>日間ログインしなかったら報酬を全てクリアする
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtNoLoginClearPayDays">
									</ajaxToolkit:FilteredTextBoxExtender>
									<asp:RegularExpressionValidator ID="vdeNoLoginClearPayDays" runat="server" ErrorMessage="報酬クリア日数を正しく入力して下さい" ControlToValidate="txtNoLoginClearPayDays"
										ValidationGroup="Detail" ValidationExpression="0|[1-9]\d{0,2}">*</asp:RegularExpressionValidator>
									<asp:RequiredFieldValidator ID="vdrNoLoginClearPayDays" runat="server" ErrorMessage="報酬クリア日数を正しく入力して下さい" ControlToValidate="txtNoLoginClearPayDays" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									CROSMILE SIPURI獲得要求URL
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCrosmileSipuriGetIFUrl" runat="server" Width="420px" MaxLength="256"></asp:TextBox>
									<asp:RegularExpressionValidator ID="vdeCrosmileSipuriGetIFUrl" runat="server" ErrorMessage="CROSMILE SIPURI獲得要求URLを正しく入力して下さい。" ValidationExpression="(\S+)://([^:/]+)(:(\d+))?(/[^#\s]*)(#(\S+))?"
										ControlToValidate="txtCrosmileSipuriGetIFUrl" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									CROSMILE SIPURI存在確認URL
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCrosmileSipuriExistIFUrl" runat="server" Width="420px" MaxLength="256"></asp:TextBox>
									<asp:RegularExpressionValidator ID="vdeCrosmileSipuriExistIFUrl" runat="server" ErrorMessage="CROSMILE SIPURI存在確認URLを正しく入力して下さい。" ValidationExpression="(\S+)://([^:/]+)(:(\d+))?(/[^#\s]*)(#(\S+))?"
										ControlToValidate="txtCrosmileSipuriExistIFUrl" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ｽﾏｰﾄﾀﾞｲﾚｸﾄIVP利用
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkTalkSmartDirectUseIvpFlag" runat="server" Text="ｸﾛｽﾏｲﾙ同士のVIDEO2Shot時にIVPを経由する。" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									XSM音声電話回線利用
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkXsmVoicePhoneLineFlag" runat="server" Text="XSM音声通話でも電話回線を利用する。" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									TwilioｱｶｳﾝﾄSID
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtTwilioAccountSid" runat="server" Width="420px" MaxLength="256"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									Twilio認証ﾄｰｸﾝ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtTwilioAuthToken" runat="server" Width="420px" MaxLength="256"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									Twilio電話番号
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtTwilioTelNo" runat="server" Width="100px" MaxLength="15"></asp:TextBox>※国番号なしで入力してください
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									Twilioﾌﾘｰﾀﾞｲｱﾙ番号
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtTwilioFreedialTelNo" runat="server" Width="100px" MaxLength="15"></asp:TextBox>※国番号なしで入力してください
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									Twilioﾎﾜｲﾄﾌﾟﾗﾝ番号
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtTwilioWhiteplanTelNo" runat="server" Width="100px" MaxLength="15"></asp:TextBox>※国番号なしで入力してください
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									TwilioSMS電話番号
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtTwilioSmsTelNo" runat="server" Width="100px" MaxLength="15"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									音声通話ｱﾌﾟﾘ状態確認URL
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtVoiceappStatusUrl" runat="server" Width="420px" MaxLength="256"></asp:TextBox>
									<asp:RegularExpressionValidator ID="vdeVoiceappStatusUrl" runat="server" ErrorMessage="音声通話ｱﾌﾟﾘ状態確認URLを正しく入力して下さい。" ValidationExpression="(\S+)://([^:/]+)(:(\d+))?(/[^#\s]*)(#(\S+))?" ControlToValidate="txtVoiceappStatusUrl" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsServerType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="92" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrSipIvpLocCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeSipIvpLocCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrSipIvpSiteCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdeSipIvpSiteCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdrSipRegistUrl" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdeSipRegistUrl" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vdrSipDomain" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8" TargetControlID="vdeSipDomain" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender14" TargetControlID="vdrSipDomainLocal" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender15" TargetControlID="vdeSipDomainLocal" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9" TargetControlID="vdrMailIFUrl" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10" TargetControlID="vdeMailIFUrl" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender17" TargetControlID="vdrVcsIFUrl" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender18" TargetControlID="vdeVcsIFUrl" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender32" TargetControlID="vdeAffiliIFUrl" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender12" TargetControlID="vdrIvpAdminUrl" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender13" TargetControlID="vdeIvpAdminUrl" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11" TargetControlID="vdrInstallDir" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender16" TargetControlID="vdrLiveKey" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender30" TargetControlID="vdrIvpRequestUrl" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender31" TargetControlID="vdeIvpRequestUrl" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender26" TargetControlID="vdrIvpIssueLiveKeyUrl" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender27" TargetControlID="vdeIvpIssueLiveKeyUrl" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender29" TargetControlID="vdeIvpStartupCamera" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender20" TargetControlID="vdeVcsRequestUrl" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender65" TargetControlID="vdeVcsFtpIP" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender19" TargetControlID="vdeMailTxUnit" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender21" TargetControlID="vdrMailTxUnit" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender22" TargetControlID="vdeMailTxCycleSec" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender23" TargetControlID="vdrMailTxCycleSec" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender24" TargetControlID="vdrMarkMailAddrNgCount" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender25" TargetControlID="vdeMarkMailAddrNgCount" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender33" TargetControlID="vdeWhitePlanDisconnTime" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender35" TargetControlID="vdeWhitePlanNaTime" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender28" TargetControlID="vdcWhitePlanDisconnTime" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender34" TargetControlID="vdeNoLoginClearPayDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender36" TargetControlID="vdrNoLoginClearPayDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender37" TargetControlID="vdeCrosmileSipuriGetIFUrl" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender38" TargetControlID="vdeCrosmileSipuriExistIFUrl" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender39" TargetControlID="vdeVoiceappStatusUrl" HighlightCssClass="validatorCallout" />
</asp:Content>
