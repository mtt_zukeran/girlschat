<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RenameDecoMailPic.aspx.cs" Inherits="System_RenameDecoMailPic" Title="デコメール写真一括ファイル名変更"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="デコメール写真一括ファイル名変更"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<asp:Button ID="btnRename" runat="server" Text="ファイル名一括変更開始" ValidationGroup="Upload" Width="150px" OnClick="btnRename_Click" />
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnRename" ConfirmText="ファイル名変更を行ないますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
