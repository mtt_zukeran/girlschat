<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="OtherWebSiteList.aspx.cs" Inherits="System_OtherWebSiteList" Title="他システムＷｅｂサイト設定" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="他システムＩ／Ｆ設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 600px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									他システムＩＤ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtOtherWebId" runat="server" MaxLength="2" Width="32px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrOtherWebId" runat="server" ErrorMessage="他システムＩＤを入力して下さい。" ControlToValidate="txtOtherWebId" ValidationGroup="Key">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeOtherWebId" runat="server" ErrorMessage="他システムＩＤは2桁の数字で入力して下さい。" ValidationExpression="\d{2}" ControlToValidate="txtOtherWebId" ValidationGroup="Key">*</asp:RegularExpressionValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[他システム設定内容]</legend>
						<table border="0" style="width: 700px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									システム名称
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtSiteNm" runat="server" MaxLength="30" Width="200px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrSiteNm" runat="server" ErrorMessage="システム名称を入力して下さい。" ControlToValidate="txtSiteNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾄｯﾌﾟﾍﾟｰｼﾞURL
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtTopPageUrl" runat="server" MaxLength="256" Width="500px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									会員情報取得URL
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUserInfoUrl" runat="server" MaxLength="256" Width="500px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									他ｼｽﾃﾑﾛｸﾞｲﾝURL
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLoginUrl" runat="server" MaxLength="256" Width="500px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									MY PAGE URL
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtMyPageUrl" runat="server" MaxLength="256" Width="500px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									自ｼｽﾃﾑﾛｸﾞｲﾝURL
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtViCommLoginUrl" runat="server" MaxLength="256" Width="500px"></asp:TextBox>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[他システム一覧]</legend>
			<asp:GridView ID="grdParentWeb" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsParentWeb" AllowSorting="True" SkinID="GridViewColor">
				<Columns>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkOtherWebId" runat="server" Text='<%# Eval("OTHER_WEB_SYS_ID") %>' CommandArgument='<%# Eval("OTHER_WEB_SYS_ID") %>' OnCommand="lnkOtherWebId_Command" CausesValidation="False"></asp:LinkButton>
						</ItemTemplate>
						<HeaderTemplate>
							ＩＤ
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Center" Width="36px" />
					</asp:TemplateField>
					<asp:BoundField DataField="SITE_NM" HeaderText="システム名称">
						<ItemStyle Width="120px" />
					</asp:BoundField>
					<asp:TemplateField HeaderText="ﾛｸﾞｲﾝURL">
						<ItemTemplate>
							<asp:Label ID="Label2" runat="server" Text='<%# Bind("LOGIN_URL") %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<a class="reccount">Record Count
				<%#GetRecCount() %>
			</a>
			<br>
			<a class="reccount">Current viewing page
				<%=grdParentWeb.PageIndex + 1%>
				of
				<%=grdParentWeb.PageCount%>
			</a>
			<div class="button">
				<asp:Button ID="btnRegist" runat="server" Text="他システム追加" OnClick="btnRegist_Click" />
			</div>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsParentWeb" runat="server" SelectMethod="GetPageCollection" TypeName="OtherWebSite" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelected="dsParentWeb_Selected"></asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrOtherWebId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeOtherWebId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrSiteNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
