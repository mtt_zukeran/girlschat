﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 障害検索
--	Progaram ID		: TroubleInquiry
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;

public partial class System_TroubleInquiry:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
			DataBind();
		}
	}

	protected void dsProgramTrouble_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		grdTrouble.PageIndex = 0;
		DataBind();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	private void InitPage() {
		grdTrouble.PageSize = int.Parse(Session["PageSize"].ToString());

		SysPrograms.SetupFromToDay(lstFromYYYY,lstFromMM,lstFromDD,lstToYYYY,lstToMM,lstToDD,true);
		lstFromYYYY.SelectedIndex = 0;
		lstToYYYY.SelectedIndex = 0;
		lstFromMM.SelectedValue = DateTime.Now.ToString("MM");
		lstToMM.SelectedValue = DateTime.Now.ToString("MM");
		lstFromDD.SelectedValue = DateTime.Now.ToString("dd");
		lstToDD.SelectedValue = DateTime.Now.ToString("dd");
		recCount = "";
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void dsProgramTrouble_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = string.Format("{0}/{1}/{2}",lstFromYYYY.SelectedValue,lstFromMM.SelectedValue,lstFromDD.SelectedValue);
		e.InputParameters[1] = string.Format("{0}/{1}/{2}",lstToYYYY.SelectedValue,lstToMM.SelectedValue,lstToDD.SelectedValue);
	}
}
