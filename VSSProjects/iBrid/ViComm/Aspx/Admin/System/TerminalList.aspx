<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TerminalList.aspx.cs" Inherits="System_TerminalList" Title="端末設定"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="端末設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 680px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									端末ＩＰアドレス
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtTerminalIPAddr" runat="server" MaxLength="15"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrTerminalIPAddr" runat="server" ErrorMessage="端末ＩＰアドレスを入力して下さい。" ControlToValidate="txtTerminalIPAddr" ValidationGroup="Key">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeTerminalIPAddr" runat="server" ErrorMessage="端末ＩＰアドレスを正しく入力して下さい。" ValidationExpression="^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$"
										ValidationGroup="Key" ControlToValidate="txtTerminalIPAddr">*</asp:RegularExpressionValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[端末設定内容]</legend>
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									端末種別
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstTerminalType" runat="server" Width="206px" DataSourceID="dsTerminalType" DataTextField="CODE_NM" DataValueField="CODE">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ＵＲＩ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtUri" runat="server" MaxLength="64" Width="400px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrUri" runat="server" ErrorMessage="ＵＲＩを入力して下さい。" ControlToValidate="txtUri" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:CustomValidator ID="vdcUri" runat="server" ControlToValidate="txtUri" ErrorMessage="このURIはすでに他の端末で使用されています。" ValidationGroup="Detail" OnServerValidate="vdcUri_ServerValidate">*</asp:CustomValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									備考
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtRemarks" runat="server" MaxLength="80" Width="400px"></asp:TextBox>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[端末一覧]</legend>
			<asp:GridView ID="grdTerminal" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsTerminal" AllowSorting="True" SkinID="GridViewColor">
				<Columns>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkTerminalIpAddr" runat="server" Text='<%# Eval("TERMINAL_IP_ADDR") %>' CommandArgument='<%# Eval("TERMINAL_IP_ADDR") %>' OnCommand="lnkTerminalIpAddr_Command"
								CausesValidation="False"></asp:LinkButton>
						</ItemTemplate>
						<HeaderTemplate>
							ＩＰ
						</HeaderTemplate>
					</asp:TemplateField>
					<asp:BoundField DataField="TERMINAL_TYPE_NM" HeaderText="端末種別"></asp:BoundField>
					<asp:BoundField DataField="URI" HeaderText="ＵＲＩ"></asp:BoundField>
					<asp:BoundField DataField="REMARKS" HeaderText="備考"></asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br>
				<a class="reccount">Current viewing page
					<%=grdTerminal.PageIndex + 1%>
					of
					<%=grdTerminal.PageCount%>
				</a>
				<div class="button">
					<asp:Button ID="btnRegist" runat="server" Text="端末追加" OnClick="btnRegist_Click" />
				</div>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsTerminal" runat="server" SelectMethod="GetPageCollection" TypeName="Terminal" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsTerminal_Selected"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsTerminalType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="55" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrTerminalIPAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeTerminalIPAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrUri" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdcUri" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
