﻿/*************************************************************************
--	System			: ViComm Sales
--	Sub System Name	: Admin
--	Title			: 決済会社設定
--	Progaram ID		: SettleCompanyList
--
--  Creation Date	: 2009.11.05
--  Creater			: iBrid(S.Ohtahara)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class System_SettleCompanyList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		grdSettleCompany.PageSize = 100;
		grdSettleCompany.PageIndex = 0;
		txtSettleCompanyCd.Text = "";
		ClearField();
		pnlMainte.Visible = false;
		DataBind();

	}

	private void ClearField() {
		txtSettleCompanyNm.Text = "";
	}


	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
		InitPage();
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void lnkSettleCompanyCd_Command(object sender,CommandEventArgs e) {
		txtSettleCompanyCd.Text = e.CommandArgument.ToString();
		GetData();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		pnlMainte.Visible = true;
		pnlKey.Enabled = true;
		pnlDtl.Visible = false;
	}


	protected void dsSettleCompany_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SETTLE_COMPANY_GET");
			db.ProcedureInParm("PSETTLE_COMPANY_CD",DbSession.DbType.VARCHAR2,txtSettleCompanyCd.Text);
			db.ProcedureOutParm("PSETTLE_COMPANY_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PUPDATE_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtSettleCompanyCd.Text = db.GetStringValue("PSETTLE_COMPANY_CD");
				txtSettleCompanyNm.Text = db.GetStringValue("PSETTLE_COMPANY_NM");
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;
	}

	private void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SETTLE_COMPANY_MAINTE");
			db.ProcedureInParm("PMANAGE_COMPANY_CD",DbSession.DbType.VARCHAR2,txtSettleCompanyCd.Text);
			db.ProcedureInParm("PMANAGE_COMPANY_NM",DbSession.DbType.VARCHAR2,txtSettleCompanyNm.Text);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		InitPage();
	}
}
