﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TroubleInquiry.aspx.cs" Inherits="System_TroubleInquiry" Title="障害履歴検索" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="障害履歴検索"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset>
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlSeek">
				<table border="0" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle2">
							発生日時
						</td>
						<td class="tdDataStyle" colspan="5">
							<asp:DropDownList ID="lstFromYYYY" runat="server">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstFromMM" runat="server">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstFromDD" runat="server">
							</asp:DropDownList>日～
							<asp:DropDownList ID="lstToYYYY" runat="server">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstToMM" runat="server">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstToDD" runat="server">
							</asp:DropDownList>日
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
			</asp:Panel>
		</fieldset>
		<fieldset>
			<legend>[障害履歴一覧]</legend>
			<asp:GridView ID="grdTrouble" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsProgramTrouble" AllowSorting="True" SkinID="GridViewColor">
				<Columns>
					<asp:BoundField DataField="TROUBLE_DATE" HeaderText="障害発生日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}"></asp:BoundField>
					<asp:BoundField DataField="PROGRAM_NM" HeaderText="発生プログラム" />
					<asp:BoundField DataField="TROUBLE_MSG" HeaderText="エラーメッセージ">
						<ItemStyle Width="500px" />
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<a class="reccount">Record Count
				<%#GetRecCount() %>
			</a><a class="viewpage">current viewing page
				<%=grdTrouble.PageIndex + 1%>
				of
				<%=grdTrouble.PageCount%>
			</a>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsProgramTrouble" runat="server" SelectMethod="GetPageCollection" TypeName="ProgramTrouble" SelectCountMethod="GetPageCount"
		EnablePaging="True" OnSelected="dsProgramTrouble_Selected" OnSelecting="dsProgramTrouble_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
