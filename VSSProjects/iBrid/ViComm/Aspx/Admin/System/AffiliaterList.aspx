﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AffiliaterList.aspx.cs" Inherits="System_AffiliaterList" Title="アフリエーター設定" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="アフリエーター設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 800px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									アフリエーターコード
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtAffiliaterCd" runat="server" MaxLength="12" Width="20px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrAffiliaterCd" runat="server" ErrorMessage="アフリエーターコードを入力して下さい。" ControlToValidate="txtAffiliaterCd" ValidationGroup="Key">*</asp:RequiredFieldValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[アフリエーター内容]</legend>
						<table border="0" style="width: 800px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									アフリエーター名称
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtAffiliaterNm" runat="server" MaxLength="30" Width="160px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrAffiliater" runat="server" ErrorMessage="アフリエータ名称を入力して下さい。" ControlToValidate="txtAffiliaterNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle" style="height: 26px">
									AS情報識別
								</td>
								<td class="tdDataStyle" style="height: 26px">
									<asp:TextBox ID="txtAsInfoPrefix" runat="server" MaxLength="32" Width="200px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrAsInfoPrefix" runat="server" ErrorMessage="識別文字列を入力して下さい。" ControlToValidate="txtAsInfoPrefix" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:CustomValidator ID="vdcAsInfoPrefix" runat="server" ControlToValidate="txtAsInfoPrefix" ErrorMessage='<%# string.Format("{0}バイトまで入力可能です。",txtAsInfoPrefix.MaxLength) %>'
										OnServerValidate="vdcAsInfoPrefix_ServerValidate" ValidationGroup="Detail">*</asp:CustomValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle" style="height: 26px">
									AS情報識別補足
								</td>
								<td class="tdDataStyle" style="height: 26px">
									<asp:TextBox ID="txtAsInfoSubPrefix" runat="server" MaxLength="12" Width="100px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle" style="height: 26px">
									AS情報識別補足値
								</td>
								<td class="tdDataStyle" style="height: 26px">
									<asp:TextBox ID="txtAsInfoSubValue" runat="server" MaxLength="32" Width="200px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾄﾗｯｷﾝｸﾞURL
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtTrackingUrl" runat="server" MaxLength="100" Width="600px" Rows="2" TextMode="MultiLine"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrTrackingUrl" runat="server" ErrorMessage="トラッキングURLを入力して下さい。" ControlToValidate="txtTrackingUrl" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:CustomValidator ID="vdcTrackingUrl" runat="server" ControlToValidate="txtTrackingUrl" ErrorMessage="URL中に改行が含まれています" OnServerValidate="vdcTrackingUrl_ServerValidate"
										ValidationGroup="Detail">*</asp:CustomValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾄﾗｯｷﾝｸﾞHTTP送信ﾌﾗｸﾞ
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkTrackingHttpFlag" runat="server">
									</asp:CheckBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									追加情報
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtAddOnInfo" runat="server" Width="160px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾄﾗｯｷﾝｸﾞJavaScript
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtTrackingJs" runat="server" MaxLength="100" Width="600px" Rows="10" TextMode="MultiLine"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrTrackingJs" runat="server" ErrorMessage="トラッキングJavaScriptを入力して下さい。" ControlToValidate="txtTrackingJs" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									認証送信方式
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstAuthResultTransType" runat="server" Width="206px" DataSourceID="dsAuthResultTransType" DataTextField="CODE_NM" DataValueField="CODE">
									</asp:DropDownList>
									<asp:RequiredFieldValidator ID="vdrAuthResultTransType" runat="server" ControlToValidate="lstAuthResultTransType" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:CustomValidator ID="vdcAuthResultTransType" runat="server" ControlToValidate="lstAuthResultTransType" OnServerValidate="vdcAuthResultTransType_ServerValidate"
										ValidationGroup="Detail">*</asp:CustomValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[アフリエーター一覧]</legend>
			<asp:GridView ID="grdAffiliater" runat="server" AllowPaging="True" DataSourceID="dsAffiliater" AllowSorting="True" SkinID="GridViewColor" AutoGenerateColumns="False">
				<Columns>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkAffiliaterCd" runat="server" Text='<%# Eval("AFFILIATER_CD") %>' CommandArgument='<%# Eval("AFFILIATER_CD") %>' OnCommand="lnkAffiliaterCd_Command"
								CausesValidation="False"></asp:LinkButton>
							<asp:Label ID="lblAffiliaterNm" runat="server" Text='<%# Bind("AFFILIATER_NM") %>'></asp:Label>
						</ItemTemplate>
						<HeaderTemplate>
							アフリエーターコード
						</HeaderTemplate>
					</asp:TemplateField>
					<asp:BoundField DataField="AS_INFO_PREFIX" HeaderText="AS情報識別"></asp:BoundField>
					<asp:BoundField DataField="AS_INFO_SUB_PREFIX" HeaderText="AS情報識別補足"></asp:BoundField>
					<asp:BoundField DataField="AS_INFO_SUB_VALUE" HeaderText="AS情報識別補足値"></asp:BoundField>
					<asp:BoundField DataField="UPDATE_DATE" HeaderText="更新日時"></asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<a class="reccount">Record Count
				<%#GetRecCount() %>
			</a>
			<br>
			<a class="reccount">Current viewing page
				<%=grdAffiliater.PageIndex + 1%>
				of
				<%=grdAffiliater.PageCount%>
			</a>
			<div class="button">
				<asp:Button ID="btnRegist" runat="server" Text="アフリエーター追加" OnClick="btnRegist_Click" />
			</div>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsManRegistReportTiming" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="16" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsAffiliater" runat="server" SelectMethod="GetPageCollection" SelectCountMethod="GetPageCount" OnSelected="dsAffiliater_Selected"
		EnablePaging="True" TypeName="Affiliater"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsAuthResultTransType" runat="server" SelectMethod="GetAuthResultTransType" TypeName="Affiliater"></asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrAffiliaterCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdrAsInfoPrefix" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdcAsInfoPrefix" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrTrackingUrl" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdcTrackingUrl" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdrTrackingJs" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vdrAuthResultTransType" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8" TargetControlID="vdcAuthResultTransType" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Custom" TargetControlID="txtTrackingUrl" FilterMode="InvalidChars"
		InvalidChars="&#13;&#10;" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
