﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 他システムＷｅｂサイト設定
--	Progaram ID		: OtherWebSiteList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;

public partial class System_OtherWebSiteList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	protected void dsParentWeb_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		grdParentWeb.PageSize = int.Parse(Session["PageSize"].ToString());
		grdParentWeb.PageIndex = 0;
		txtOtherWebId.Text = "";
		ClearField();
		pnlMainte.Visible = false;
		DataBind();
	}

	private void ClearField() {
		txtSiteNm.Text = "";
		txtTopPageUrl.Text = "";
		txtUserInfoUrl.Text = "";
		txtLoginUrl.Text = "";
		txtMyPageUrl.Text = "";
		txtViCommLoginUrl.Text = "";
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		pnlMainte.Visible = true;
		pnlKey.Enabled = true;
		pnlDtl.Visible = false;
	}

	protected void lnkOtherWebId_Command(object sender,CommandEventArgs e) {
		txtOtherWebId.Text = e.CommandArgument.ToString();
		GetData();
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("OTHER_WEB_SITE_GET");
			db.ProcedureInParm("POTHER_WEB_SYS_ID",DbSession.DbType.VARCHAR2,txtOtherWebId.Text);
			db.ProcedureOutParm("PSITE_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PTOP_PAGE_URL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PUSER_INFO_URL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PLOGIN_URL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMY_PAGE_URL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PVIOCMM_LOGIN_URL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtSiteNm.Text = db.GetStringValue("PSITE_NM");
				txtTopPageUrl.Text = db.GetStringValue("PTOP_PAGE_URL");
				txtUserInfoUrl.Text = db.GetStringValue("PUSER_INFO_URL");
				txtLoginUrl.Text = db.GetStringValue("PLOGIN_URL");
				txtMyPageUrl.Text = db.GetStringValue("PMY_PAGE_URL");
				txtViCommLoginUrl.Text = db.GetStringValue("PVIOCMM_LOGIN_URL");
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;

	}

	private void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("OTHER_WEB_SITE_MAINTE");
			db.ProcedureInParm("POTHER_WEB_SYS_ID",DbSession.DbType.VARCHAR2,txtOtherWebId.Text);
			db.ProcedureInParm("PSITE_NM",DbSession.DbType.VARCHAR2,txtSiteNm.Text);
			db.ProcedureInParm("PTOP_PAGE_URL",DbSession.DbType.VARCHAR2,txtTopPageUrl.Text);
			db.ProcedureInParm("PUSER_INFO_URL",DbSession.DbType.VARCHAR2,txtUserInfoUrl.Text);
			db.ProcedureInParm("PLOGIN_URL",DbSession.DbType.VARCHAR2,txtLoginUrl.Text);
			db.ProcedureInParm("PMY_PAGE_URL",DbSession.DbType.VARCHAR2,txtMyPageUrl.Text);
			db.ProcedureInParm("PVIOCMM_LOGIN_URL",DbSession.DbType.VARCHAR2,txtViCommLoginUrl.Text);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		InitPage();
	}
}
