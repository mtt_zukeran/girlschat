﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 変数変換

--	Progaram ID		: VariableConvert
--
--  Creation Date	: 2010.08.23
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class System_VariableConvert:System.Web.UI.Page {
	private string ReplacementBase;
	private string pattern;
	StreamWriter writer;

	protected void Page_Load(object sender,EventArgs e) {

	}
	protected void btnConvert_Click(object sender,EventArgs e) {
		if (!uplVariableFile.HasFile || !uplHtmlCsvFile.HasFile) {
			return;
		}

		Encoding enc = Encoding.GetEncoding("Shift-JIS");
		Dictionary<string,string> dicVariable = new Dictionary<string,string>();

		using (StreamReader sr = new StreamReader(uplVariableFile.FileContent,enc)) {
			string sLine;
			while ((sLine = sr.ReadLine()) != null) {
				string[] sValues = sLine.Split('\t');
				if (sValues.Length >= 2 && sValues[0] != string.Empty) {

					string sKey = sValues[0].Replace("\"\"","\"");
					string sValue = sValues[1].Replace("\"\"","\"");

					if (sKey.StartsWith("$DIV_")) {
						sKey = sKey.Replace(";",@"\(?(;?|[^;]*;?)?\)?;");
					} else {
						if (sValue.IndexOf("%") > -1) {
							sKey = sKey.Replace(";",@"\(?(;?|[^\)]*;?)?\)?;");
						}
					}
					try {
						dicVariable.Add(sKey,sValue);
					} catch (ArgumentException) {
						//Key重複は無視

					}
				}
			}
			dicVariable.Add(@"<div id=""<div id=""([^""]*)"">"">",@"<div id=""%0"">");
		}

		string sNewFileNm = Path.GetFileNameWithoutExtension(uplHtmlCsvFile.FileName) + "_new" +
			Path.GetExtension(uplHtmlCsvFile.FileName);

		string sInstallDir;
		using (Sys oSys = new Sys()) {
			oSys.GetValue("INSTALL_DIR",out sInstallDir);
		}

		string sLogFile = string.Format("{0}\\Text\\ChangeVariable{1}.txt",sInstallDir,DateTime.Now.ToString("yyyyMMddHHmmssfff"));


		String sNewFileContent = "";
		using (writer = new StreamWriter(sLogFile))
		using (StreamReader sr = new StreamReader(uplHtmlCsvFile.FileContent,enc)) {
			sNewFileContent = sr.ReadToEnd();

			foreach (KeyValuePair<string,string> kvp in dicVariable) {
				sNewFileContent = ConvertByRegex(sNewFileContent,kvp);
			}
			writer.Flush();
			writer.Close();
		}

		if (Request.Browser.Browser == "IE") {
			Response.HeaderEncoding = enc;
		}

		Response.AddHeader("Content-Disposition","attachment;filename=" + sNewFileNm);
		Response.ContentType = "application/octet-stream-dummy";
		Response.BinaryWrite(enc.GetBytes(sNewFileContent));
		Response.End();
	}

	private string ConvertByRegex(string sInput,KeyValuePair<string,string> kvp) {
		string sPattern = kvp.Key.Replace("$","\\$");
		ReplacementBase = kvp.Value;
		Regex regex1 = new Regex(sPattern);
		//Match rgxMatch = regex1.Match(sInput);
		//		while (rgxMatch.Success) {
		//			log += sPattern + " " + rgxMatch.Value + "0:" + rgxMatch.Groups[0].Value + " 1:" + rgxMatch.Groups[1].Value + "\r\n";
		//			rgxMatch = rgxMatch.NextMatch();
		//		}

		pattern = sPattern;
		writer.WriteLine(pattern);
		if (kvp.Key.StartsWith(@"$HREF_")) {
			sInput = regex1.Replace(sInput,new MatchEvaluator(Evaluator1));
		} else {
			sInput = regex1.Replace(sInput,new MatchEvaluator(Evaluator2));

		}
		return sInput;
	}

	private string Evaluator1(Match m) {
		string str = ReplacementBase;
		string[] sParams = m.Groups[1].Value.Split(',');
		if (ReplacementBase.IndexOf("%1") > -1) {
			switch (sParams.Length) {
				case 1:
					str = str.Replace(@"%0",sParams[0]);
					str = str.Replace(@"%1",null);
					str = str.Replace(@"accesskey=""""",null); //accesskeyが空なので削除
					writer.WriteLine("   " + m.Value + " CAP0:" + sParams[0] + "\r\n      CNV:" + str);
					break;
				case 2:
					str = str.Replace(@"%0",sParams[0]);
					str = str.Replace(@"%1",sParams[1]);
					writer.WriteLine("   " + m.Value + " CAP0:" + sParams[0] + " CAP1:" + sParams[1] + "\r\n      CNV:" + str);
					break;
			}
		} else {
			str = str.Replace(@"%0",m.Groups[1].Value);
			str = str.Replace(@"()",null); //()内が空なので削除
			writer.WriteLine("   " + m.Value + " CAP0:" + m.Groups[1].Value + "\r\n      CNV:" + str);
		}

		return str;
	}

	private string Evaluator2(Match m) {
		string str = ReplacementBase;
		string sParam = m.Groups[1].Value;
		str = str.Replace(@"%0",sParam);
		str = str.Replace(@"()",null); //()内が空なので削除
		writer.WriteLine("   " + m.Value + " CAP0:" + sParam + "\r\n      CNV:" + str);
		return str;
	}
}
