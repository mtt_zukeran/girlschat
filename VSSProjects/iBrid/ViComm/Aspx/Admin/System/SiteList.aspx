<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SiteList.aspx.cs" Inherits="System_SiteList" Title="サイト情報設定" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="サイト基本情報設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									サイトコード
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtSiteCd" runat="server" MaxLength="4" Width="60px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrSiteCd" runat="server" ErrorMessage="サイトコードを入力して下さい。" ControlToValidate="txtSiteCd" ValidationGroup="Key">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeSiteCd" runat="server" ErrorMessage="サイトコードは4桁の英数字で入力して下さい。" ValidationExpression="\w{4}" ControlToValidate="txtSiteCd"
										ValidationGroup="Key">*</asp:RegularExpressionValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[サイト内容]</legend>
						<asp:Button runat="server" ID="Button1" Text="更新" CssClass="seektopbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="Button2" Text="削除" CssClass="seektopbutton" OnClick="btnDelete_Click" ValidationGroup="Key" Visible="false" />
						<asp:Button runat="server" ID="Button3" Text="キャンセル" CssClass="seektopbutton" OnClick="btnCancel_Click" CausesValidation="False" />
						<br clear="all" />
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									ｻｲﾄ名
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtSiteNm" runat="server" MaxLength="40" Width="200px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrSiteNm" runat="server" ErrorMessage="ｻｲﾄ名を入力して下さい。" ControlToValidate="txtSiteNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ｻｲﾄ記号
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtSiteMark" runat="server" MaxLength="2" Width="20px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrSiteMark" runat="server" ErrorMessage="ｻｲﾄ記号を入力して下さい。" ControlToValidate="txtSiteMark" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeSiteMark" runat="server" ErrorMessage="ｻｲﾄ記号は英数字2文字で入力して下さい。" ValidationExpression="[a-zA-Z\d.\-_]{2}" ControlToValidate="txtSiteMark"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ｻｲﾄﾀｲﾌﾟ
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtSiteType" runat="server" MaxLength="32" Width="200px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrSiteType" runat="server" ErrorMessage="ｻｲﾄﾀｲﾌﾟを入力して下さい。" ControlToValidate="txtSiteType" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									URL
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtUrl" runat="server" Width="420px" MaxLength="256"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrUrl" runat="server" ErrorMessage="URLを入力して下さい。" ControlToValidate="txtUrl" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeUrl" runat="server" ErrorMessage="URLを正しく入力して下さい。" ValidationExpression="(\S+)://([^:/]+)(:(\d+))?" ControlToValidate="txtUrl"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									WEB配置ﾃﾞｨﾚｸﾄﾘ
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtWebPhisicalDir" runat="server" Width="420px" MaxLength="256"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrWebPhisicalDir" runat="server" ErrorMessage="WEB配置ﾃﾞｨﾚｸﾄﾘを入力して下さい。" ControlToValidate="txtWebPhisicalDir" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾎｽﾄ名
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtHostNm" runat="server" Width="140px" MaxLength="60"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrHostNm" runat="server" ErrorMessage="ﾎｽﾄ名を入力して下さい。" ControlToValidate="txtHostNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeHostNm" runat="server" ErrorMessage="ﾎｽﾄ名は英数字で入力して下さい。" ValidationExpression="[a-zA-Z\d.\-_]{1,60}" ControlToValidate="txtHostNm"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ｻﾌﾞﾎｽﾄ名
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtSubHostNm" runat="server" Width="140px" MaxLength="60"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrSubHostNm" runat="server" ErrorMessage="ｻﾌﾞﾎｽﾄ名を入力して下さい。" ControlToValidate="txtSubHostNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeSubHostNm" runat="server" ErrorMessage="ｻﾌﾞﾎｽﾄ名は英数字で入力して下さい。" ValidationExpression="[a-zA-Z\d.\-_]{1,60}" ControlToValidate="txtSubHostNm"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾒｰﾙﾎｽﾄ名
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtMailHost" runat="server" Width="140px" MaxLength="60"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrMailHost" runat="server" ErrorMessage="ﾒｰﾙﾎｽﾄ名を入力して下さい。" ControlToValidate="txtMailHost" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeMailHost" runat="server" ErrorMessage="ﾒｰﾙﾎｽﾄ名は英数字で入力して下さい。" ValidationExpression="[a-zA-Z\d.\-_]{1,60}" ControlToValidate="txtMailHost"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									求人ｻｲﾄﾎｽﾄ名
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtJobOfferSiteHostNm" runat="server" Width="140px" MaxLength="60"></asp:TextBox>
									<asp:RegularExpressionValidator ID="vdeJobOfferSiteHostNm" runat="server" ErrorMessage="求人ｻｲﾄﾎｽﾄ名は英数字で入力して下さい。" ValidationExpression="[a-zA-Z\d.\-_]{1,60}"
										ControlToValidate="txtJobOfferSiteHostNm" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									求人ﾎｽﾄﾀｲﾌﾟ
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtJobOfferSiteType" runat="server" MaxLength="32" Width="200px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									求人ｻｲﾄTOPﾍﾟｰｼﾞ文章��
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtJobOfferStartDoc" runat="server" Width="40px" MaxLength="3"></asp:TextBox>
									<asp:RegularExpressionValidator ID="vdeJobOfferStartDoc" runat="server" ErrorMessage="求人ｻｲﾄTOPﾍﾟｰｼﾞ文章�ｂﾍ3桁の数字で入力して下さい。" ValidationExpression="\d{3}" ControlToValidate="txtJobOfferStartDoc"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<asp:PlaceHolder ID="plcVisible1" runat="server">
								<tr>
									<td class="tdHeaderStyle">
										ﾒｰﾙ配信機IP(ﾊﾞｯﾁ用)
									</td>
									<td class="tdDataStyle" colspan="4">
										<asp:TextBox ID="txtLoMailerIP" runat="server" MaxLength="15"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrLoMailerIP" runat="server" ErrorMessage="ﾒｰﾙ配信機IP(ﾊﾞｯﾁ用)を入力して下さい。" ControlToValidate="txtLoMailerIP" ValidationGroup="Detail"
											Enabled="False">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeLoMailerIP" runat="server" ErrorMessage="ﾒｰﾙ配信機IP(ﾊﾞｯﾁ用)を正しく入力して下さい。" ValidationExpression="^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$"
											ValidationGroup="Detail" ControlToValidate="txtLoMailerIP">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										ﾒｰﾙ配信機IP(即時用)
									</td>
									<td class="tdDataStyle" colspan="4">
										<asp:TextBox ID="txtHiMailerIP" runat="server" MaxLength="15"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrHiMailerIP" runat="server" ErrorMessage="ﾒｰﾙ配信機IP(即時用)を入力して下さい。" ControlToValidate="txtHiMailerIP" ValidationGroup="Detail"
											Enabled="False">*</asp:RequiredFieldValidator>
										<asp:RegularExpressionValidator ID="vdeHiMailerIP" runat="server" ErrorMessage="ﾒｰﾙ配信機IP(即時用)を正しく入力して下さい。" ValidationExpression="^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$"
											ValidationGroup="Detail" ControlToValidate="txtHiMailerIP">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										ﾒｰﾙ配信機FTP ID
									</td>
									<td class="tdDataStyle" colspan="4">
										<asp:TextBox ID="txtMailerFtpId" runat="server" Width="100px" MaxLength="32"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										ﾒｰﾙ配信機FTP PW
									</td>
									<td class="tdDataStyle" colspan="4">
										<asp:TextBox ID="txtMailerFtpPw" runat="server" Width="100px" MaxLength="32"></asp:TextBox>
									</td>
								</tr>
							</asp:PlaceHolder>
							<tr>
								<td class="tdHeaderStyle">
									ﾛｰｶﾙIPｱﾄﾞﾚｽ
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtLocalIpAddr" runat="server" MaxLength="15"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrLocalIpAddr" runat="server" ErrorMessage="ﾛｰｶﾙIPｱﾄﾞﾚｽを入力して下さい。" ControlToValidate="txtLocalIpAddr" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeLocalIpAddr" runat="server" ErrorMessage="ﾛｰｶﾙIPｱﾄﾞﾚｽを正しく入力して下さい。" ValidationExpression="^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$"
										ValidationGroup="Detail" ControlToValidate="txtLocalIpAddr">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾛｰｶﾙﾎﾟｰﾄ
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtLocalPort" runat="server" MaxLength="5" Width="32px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrLocalPort" runat="server" ErrorMessage="ﾛｰｶﾙﾎﾟｰﾄを入力して下さい。" ControlToValidate="txtLocalPort" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ｸﾞﾛｰﾊﾞﾙIPｱﾄﾞﾚｽ
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtGlobalIpAddr" runat="server" MaxLength="15"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrGlobalIpAddr" runat="server" ErrorMessage="ｸﾞﾛｰﾊﾞﾙIPｱﾄﾞﾚｽを入力して下さい。" ControlToValidate="txtGlobalIpAddr" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeGlobalIpAddr" runat="server" ErrorMessage="ｸﾞﾛｰﾊﾞﾙIPｱﾄﾞﾚｽを正しく入力して下さい。" ValidationExpression="^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$"
										ValidationGroup="Detail" ControlToValidate="txtGlobalIpAddr">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<asp:PlaceHolder ID="plcVisible2" runat="server">
								<tr>
									<td class="tdHeaderStyle">
										IISｻｲﾄ識別子<br />
										（Dispatch）
									</td>
									<td class="tdDataStyle" colspan="4">
										<asp:TextBox ID="txtIisSiteIdentifier" runat="server" MaxLength="15"></asp:TextBox>
										<asp:RequiredFieldValidator ID="vdrIisSiteIdentifier" runat="server" ErrorMessage="DipatchのIISｻｲﾄ識別子を入力して下さい。" ControlToValidate="txtIisSiteIdentifier"
											ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									</td>
								</tr>
							</asp:PlaceHolder>
							<tr>
								<td class="tdHeaderStyle">
									IVP管理拠点ｺｰﾄﾞ
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtIvpLocCd" runat="server" Width="60px" MaxLength="2"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrIvpLocCd" runat="server" ErrorMessage="IVP管理拠点ｺｰﾄﾞを入力して下さい。" ControlToValidate="txtIvpLocCd" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeIvpLocCd" runat="server" ErrorMessage="IVP管理拠点ｺｰﾄﾞは2桁の英数字で入力して下さい。" ValidationExpression="\w{2}" ControlToValidate="txtIvpLocCd"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									IVPｻｲﾄｺｰﾄﾞ
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtIvpSiteCd" runat="server" Width="140px" MaxLength="12"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrIvpSiteCd" runat="server" ErrorMessage="IVPｻｲﾄｺｰﾄﾞを入力して下さい。" ControlToValidate="txtIvpSiteCd" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeIvpSiteCd" runat="server" ErrorMessage="IVPｻｲﾄｺｰﾄﾞは4〜12桁の英数字で入力して下さい。" ValidationExpression="\w{4,12}" ControlToValidate="txtIvpSiteCd"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									IVPｵﾌﾞｼﾞｪｸﾄ保管ﾌｫﾙﾀﾞ
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtIvpObjectStoreFolder" runat="server" Width="140px" MaxLength="64"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrIvpObjectStoreFolder" runat="server" ErrorMessage="IVPｵﾌﾞｼﾞｪｸﾄ保管ﾌｫﾙﾀﾞを入力して下さい。" ControlToValidate="txtIvpObjectStoreFolder"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeIvpObjectStoreFolder" runat="server" ErrorMessage="IVPｵﾌﾞｼﾞｪｸﾄ保管ﾌｫﾙﾀﾞは64桁以内の英数字で入力して下さい。" ValidationExpression="\w{1,64}"
										ControlToValidate="txtIvpObjectStoreFolder" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle" style="height: 26px">
									LIGHTﾁｬﾈﾙ接続方式
								</td>
								<td class="tdDataStyle" style="height: 26px">
									<asp:DropDownList ID="lstLightChConnectType" runat="server" DataSourceID="dsLightChConnectType" DataTextField="CODE_NM" DataValueField="CODE" Width="150px"
										OnDataBound="lst_DataBound">
									</asp:DropDownList>
									<asp:RequiredFieldValidator ID="vdrLightChConnectType" runat="server" ErrorMessage="LIGHTﾁｬﾈﾙ接続方式を選択して下さい。" ControlToValidate="lstLightChConnectType" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<asp:PlaceHolder ID="plcVisible3" runat="server">
								<tr>
									<td class="tdHeaderStyle">
										VCS接続先<br />
										ﾃﾅﾝﾄｺｰﾄﾞ
									</td>
									<td class="tdDataStyle" colspan="4">
										<asp:TextBox ID="txtVcsTenantCd" runat="server" Width="24px" MaxLength="2"></asp:TextBox>
										<asp:RegularExpressionValidator ID="vdeVcsTenantCd" runat="server" ErrorMessage="VCS接続先ﾃﾅﾝﾄｺｰﾄﾞは2桁の英数字で入力して下さい。" ValidationExpression="\w{2,2}" ControlToValidate="txtVcsTenantCd"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										VCS接続先<br />
										WEB管理拠点ｺｰﾄﾞ
									</td>
									<td class="tdDataStyle">
										<asp:TextBox ID="txtVcsWebLocCd" runat="server" Width="24px" MaxLength="2"></asp:TextBox>
										<asp:RegularExpressionValidator ID="vdeVcsWebLocCd" runat="server" ErrorMessage="VCS接続先WEB管理拠点ｺｰﾄﾞは2桁の英数字で入力して下さい。" ValidationExpression="\w{2}" ControlToValidate="txtVcsWebLocCd"
											ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									</td>
								</tr>
							</asp:PlaceHolder>
							<tr>
								<td class="tdHeaderStyle">
									表示順位
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtPriority" runat="server" MaxLength="2" Width="32px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrPriority" runat="server" ErrorMessage="表示順位を入力して下さい。" ControlToValidate="txtPriority" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:CustomValidator ID="vdcPriority" runat="server" ControlToValidate="txtPriority" ErrorMessage="既に登録済みの表示順位です。" OnServerValidate="vdcPriority_ServerValidate"
										ValidationGroup="Detail">*</asp:CustomValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									営業開始時刻
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtNextBusinessTime" runat="server" Width="80px" MaxLength="8"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrNextBusinessTime" runat="server" ErrorMessage="営業開始時刻を入力して下さい。" ControlToValidate="txtNextBusinessTime" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeNextBusinessTime" runat="server" ErrorMessage="営業開始時刻は99:99:99の形式で入力して下さい。" ValidationExpression="^([01]+[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$"
										ControlToValidate="txtNextBusinessTime" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									2SHOTﾁｬｯﾄ<br />
									ﾀﾞｲｱﾙｱｳﾄ番号
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtWshotTalkDialoutNo" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
									<asp:RegularExpressionValidator ID="vdeWshotTalkDialoutNo" runat="server" ControlToValidate="txtWshotTalkDialoutNo" ErrorMessage="2SHOTﾁｬｯﾄﾀﾞｲｱﾙｱｳﾄを正しく入力して下さい。"
										ValidationExpression="(0\d{9})" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									2SHOTﾁｬｯﾄ<br />
									開始時ｶﾞｲﾀﾞﾝｽ
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtWShotTalkStartGuidance" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾊﾟｰﾃｨﾁｬｯﾄ<br />
									ﾀﾞｲｱﾙｱｳﾄ番号
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtPublicTalkDialoutNo" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
									<asp:RegularExpressionValidator ID="vdePublicTalkDialoutNo" runat="server" ControlToValidate="txtPublicTalkDialoutNo" ErrorMessage="ﾊﾟｰﾃｨﾁｬｯﾄﾀﾞｲｱﾙｱｳﾄ番号を正しく入力して下さい。"
										ValidationExpression="(0\d{9})" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾊﾟｰﾃｨﾁｬｯﾄ<br />
									開始時ｶﾞｲﾀﾞﾝｽ
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtPublicTalkStartGuidance" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾎﾜｲﾄﾌﾟﾗﾝ転送電話番号
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtWhitePlanNo" runat="server" MaxLength="11" Width="100px"></asp:TextBox>
									<asp:RegularExpressionValidator ID="vdeWhitePlanNo" runat="server" ControlToValidate="txtWhitePlanNo" ErrorMessage="ﾎﾜｲﾄﾌﾟﾗﾝ転送電話番号を正しく入力して下さい。" ValidationExpression="(0\d{10})"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾊﾟｰﾃｨﾁｬｯﾄ利用可能
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:CheckBox ID="chkSupportPremiumTalkFlag" runat="server" Text="ﾊﾟｰﾃｨﾁｬｯﾄを利用する。" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									AU-TV電話利用可能
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:CheckBox ID="chkSupportKddiTvTelFlag" runat="server" Text="AU-TV電話を利用する。" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									2SHOTﾁｬｯﾄ利用可能
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:CheckBox ID="chkEnablePrivateTalkMenuFlag" runat="server" Text="2SHOTﾁｬｯﾄを利用する。" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ONLINEﾓﾆﾀｰから<br />
									会話への変更を許可
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:CheckBox ID="chkSupportChgMonitorToTalk" runat="server" Text="ﾓﾆﾀｰ状態から会話への変更を許可する。" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									初回通話相手無料秒数
								</td>
								<td class="tdDataStyle" colspan="4">
									通話相手が初回の場合、<asp:TextBox ID="txtFirstTimeFreeTalkSec" runat="server" Width="24px" MaxLength="3" />秒間を無料会話とする。
									<asp:RequiredFieldValidator ID="vdrFirstTimeFreeTalkSec" runat="server" ControlToValidate="txtFirstTimeFreeTalkSec" ErrorMessage="初回無料通話秒数を入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									単価／1ﾎﾟｲﾝﾄあたり
								</td>
								<td class="tdDataStyle" colspan="4">
									1ﾎﾟｲﾝﾄあたり<asp:TextBox ID="txtPointPrice" runat="server" MaxLength="3" Width="40px"></asp:TextBox>÷<asp:TextBox ID="txtPointTax" runat="server" MaxLength="4" Width="40px"></asp:TextBox>円。
									<asp:RequiredFieldValidator ID="vdrPointPrice" runat="server" ErrorMessage="単価を入力して下さい。" ControlToValidate="txtPointPrice" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdePointPrice" runat="server" ErrorMessage="単価は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtPointPrice"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									<asp:RequiredFieldValidator ID="vdrPointTax" runat="server" ErrorMessage="消費税率を入力して下さい。" ControlToValidate="txtPointTax" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdePointTax" runat="server" ErrorMessage="消費税率を正しく入力して下さい。" ValidationExpression="^[0-9]+(\.[0-9]{1,2})?$" ControlToValidate="txtPointTax" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾕｰｻﾞｰﾒｰﾙ記録<br />
									削除日数
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtUserMailDelDays" runat="server" MaxLength="3" Width="40px"></asp:TextBox>日
									<asp:RequiredFieldValidator ID="vdrUserMailDelDays" runat="server" ErrorMessage="ﾕｰｻﾞｰﾒｰﾙ記録削除日数を入力して下さい。" ControlToValidate="txtUserMailDelDays" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeUserMailDelDays" runat="server" ErrorMessage="ﾕｰｻﾞｰﾒｰﾙ記録削除日数は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtUserMailDelDays"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									お知らせﾒｰﾙ記録<br />
									削除日数
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtNotifyMailDelDays" runat="server" MaxLength="3" Width="40px"></asp:TextBox>日
									<asp:RequiredFieldValidator ID="vdrNotifyMailDelDays" runat="server" ErrorMessage="お知らせﾒｰﾙ記録削除日数を入力して下さい。" ControlToValidate="txtNotifyMailDelDays" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeNotifyMailDelDays" runat="server" ErrorMessage="お知らせﾒｰﾙ記録削除日数は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtNotifyMailDelDays"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾌﾞﾛｸﾞ更新通知ﾒｰﾙ記録<br />
									削除日数
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtBlogMailDelDays" runat="server" MaxLength="3" Width="40px"></asp:TextBox>日
									<asp:RequiredFieldValidator ID="vdrBlogMailDelDays" runat="server" ErrorMessage="ﾌﾞﾛｸﾞ更新通知ﾒｰﾙ記録削除日数を入力して下さい。" ControlToValidate="txtBlogMailDelDays" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeBlogMailDelDays" runat="server" ErrorMessage="ﾌﾞﾛｸﾞ更新通知ﾒｰﾙ記録削除日数は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtBlogMailDelDays"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									足あと削除日数
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtMarkingDelDays" runat="server" MaxLength="3" Width="40px"></asp:TextBox>日
									<asp:RequiredFieldValidator ID="vdrMarkingDelDays" runat="server" ErrorMessage="足あと削除日数を入力して下さい。" ControlToValidate="txtMarkingDelDays" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeMarkingDelDays" runat="server" ErrorMessage="足あと削除日数は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtMarkingDelDays"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									WEB利用明細削除日数
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtUserLogDelDays" runat="server" MaxLength="3" Width="40px"></asp:TextBox>日
									<asp:RequiredFieldValidator ID="vdrUserLogDelDays" runat="server" ErrorMessage="利用明細削除日数を入力して下さい。" ControlToValidate="txtUserLogDelDays" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeUserLogDelDays" runat="server" ErrorMessage="利用明細削除日数は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtUserLogDelDays"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									会話利用明細・履歴<br />
									削除日数
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtTalkHistoryDelDays" runat="server" MaxLength="3" Width="40px"></asp:TextBox>日
									<asp:RequiredFieldValidator ID="vdrTalkHistoryDelDays" runat="server" ErrorMessage="会話利用明細・履歴削除日数を入力して下さい。" ControlToValidate="txtTalkHistoryDelDays" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeTalkHistoryDelDays" runat="server" ErrorMessage="会話利用明細・履歴削除日数は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}"
										ControlToValidate="txtTalkHistoryDelDays" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									掲示板削除日数
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtBbsWriteDelDays" runat="server" MaxLength="4" Width="40px"></asp:TextBox>日
									<asp:RequiredFieldValidator ID="vdrBbsWriteDelDays" runat="server" ErrorMessage="掲示板削除日数を入力して下さい。" ControlToValidate="txtBbsWriteDelDays" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeBbsWriteDelDays" runat="server" ErrorMessage="掲示板削除日数は1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtBbsWriteDelDays"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									掲示板写真削除日数
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtBbsPicDelDays" runat="server" MaxLength="3" Width="40px"></asp:TextBox>日
									<asp:RequiredFieldValidator ID="vdrBbsPicDelDays" runat="server" ErrorMessage="掲示板写真削除日数を入力して下さい。" ControlToValidate="txtBbsPicDelDays" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeBbsPicDelDays" runat="server" ErrorMessage="掲示板写真削除日数は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtBbsPicDelDays"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									掲示板動画削除日数
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtBbsMovieDelDays" runat="server" MaxLength="3" Width="40px"></asp:TextBox>日
									<asp:RequiredFieldValidator ID="vdrBbsMovieDelDays" runat="server" ErrorMessage="掲示板動画削除日数を入力して下さい。" ControlToValidate="txtBbsMovieDelDays" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeBbsMovieDelDays" runat="server" ErrorMessage="掲示板動画削除日数は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtBbsMovieDelDays"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									督促削除日数
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtUrgeDelDays" runat="server" MaxLength="3" Width="40px"></asp:TextBox>日
									<asp:RequiredFieldValidator ID="vdrUrgeDelDays" runat="server" ErrorMessage="督促削除日数を入力して下さい。" ControlToValidate="txtUrgeDelDays" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeUrgeDelDays" runat="server" ErrorMessage="督促削除日数は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtUrgeDelDays"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									日記削除日数
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtDiaryDelDays" runat="server" MaxLength="4" Width="40px"></asp:TextBox>日
									<asp:RequiredFieldValidator ID="vdrDiaryDelDays" runat="server" ErrorMessage="日記削除日数を入力して下さい。" ControlToValidate="txtDiaryDelDays" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeDiaryDelDays" runat="server" ErrorMessage="日記削除日数は1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtDiaryDelDays"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									会員つぶやき削除日数
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtManTweetDelDays" runat="server" MaxLength="4" Width="40px"></asp:TextBox>日
									<asp:RequiredFieldValidator ID="vdrManTweetDelDays" runat="server" ErrorMessage="会員つぶやき削除日数を入力して下さい。" ControlToValidate="txtManTweetDelDays" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeManTweetDelDays" runat="server" ErrorMessage="会員つぶやき削除日数は1〜4桁の数字で入力して下さい。" ValidationExpression="\d{1,4}" ControlToValidate="txtManTweetDelDays"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									集計・稼動ﾃﾞｰﾀ削除日数
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtSummaryReportDelDays" runat="server" MaxLength="3" Width="40px"></asp:TextBox>日
									<asp:RequiredFieldValidator ID="vdrSummaryReportDelDays" runat="server" ErrorMessage="集計記録削除日数を入力して下さい。" ControlToValidate="txtSummaryReportDelDays" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeSummaryReportDelDays" runat="server" ErrorMessage="集計記録削除日数は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtSummaryReportDelDays"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									決済記録削除日数
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtReceiptDelDays" runat="server" MaxLength="3" Width="40px"></asp:TextBox>日
									<asp:RequiredFieldValidator ID="vdrReceiptDelDays" runat="server" ErrorMessage="入金記録削除日数を入力して下さい。" ControlToValidate="txtReceiptDelDays" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeReceiptDelDays" runat="server" ErrorMessage="入金記録削除日数は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtReceiptDelDays"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ｲﾝﾎﾟｰﾄﾕｰｻﾞ削除日数
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtImportUserDelDays" runat="server" MaxLength="3" Width="40px"></asp:TextBox>日
									<asp:RequiredFieldValidator ID="vdrImportUserDelDays" runat="server" ErrorMessage="ｲﾝﾎﾟｰﾄﾕｰｻﾞ削除日数を入力して下さい。" ControlToValidate="txtImportUserDelDays" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeImportUserDelDays" runat="server" ErrorMessage="ｲﾝﾎﾟｰﾄﾕｰｻﾞ削除日数は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}"
										ControlToValidate="txtImportUserDelDays" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									男性仮登録削除日数
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtManTempRegistDelDays" runat="server" MaxLength="3" Width="40px"></asp:TextBox>日
									<asp:RequiredFieldValidator ID="vdrManTempRegistDelDays" runat="server" ErrorMessage="男性仮登録削除日数を入力して下さい。" ControlToValidate="txtManTempRegistDelDays" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeManTempRegistDelDays" runat="server" ErrorMessage="男性仮登録削除日数は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtManTempRegistDelDays"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									女性仮登録削除日数
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtCastTempRegistDelDays" runat="server" MaxLength="3" Width="40px"></asp:TextBox>日
									<asp:RequiredFieldValidator ID="vdrCastTempRegistDelDays" runat="server" ErrorMessage="女性仮登録削除日数を入力して下さい。" ControlToValidate="txtCastTempRegistDelDays"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeCastTempRegistDelDays" runat="server" ErrorMessage="女性仮登録削除日数は1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtCastTempRegistDelDays"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									出演者ﾏﾙﾁｷｬﾗｸﾀｰ<br />
									対応ｻｲﾄ
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:CheckBox ID="chkMultiCharFlag" runat="server" Text="出演者にてﾏﾙﾁｷｬﾗｸﾀｰを利用する。" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									他ｼｽﾃﾑDB利用ｻｲﾄ
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:CheckBox ID="chkUseOtherSysInfoFlag" runat="server" Text="他ｼｽﾃﾑのDBを利用する。" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									他ｼｽﾃﾑID
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:DropDownList ID="lstOtherWebSysId" runat="server" Width="206px" DataSourceID="dsOtherWebSite" DataTextField="OTHER_WEB_SYS_NM" DataValueField="OTHER_WEB_SYS_ID">
									</asp:DropDownList>
									<asp:CustomValidator ID="vdcOtherWebSysId" runat="server" ErrorMessage="他ｼｽﾃﾑIDを選択してください" OnServerValidate="vdcOtherWebSysId_ServerValidate" ValidationGroup="Detail"
										Display="Dynamic"></asp:CustomValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									後払利用可能ｻｲﾄ
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:CheckBox ID="chkAvailablePayAfterFlag" runat="server" Text="後払ﾊﾟｯｸを利用する。" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									従量制決済利用ｻｲﾄ
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:CheckBox ID="chkUsedChargeSettleFlag" runat="server" Text="従量制決済を利用する。" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									課金開始ﾎﾟｲﾝﾄ
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtChargeStartPoint" runat="server" MaxLength="3" Width="40px"></asp:TextBox>ﾎﾟｲﾝﾄ分請求ﾎﾟｲﾝﾄが存在していたら、請求対象とする。
									<asp:RequiredFieldValidator ID="vdrChargeStartPoint" runat="server" ErrorMessage="課金開始ﾎﾟｲﾝﾄを入力して下さい。" ControlToValidate="txtChargeStartPoint" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeChargeStartPoint" runat="server" ErrorMessage="課金開始ﾎﾟｲﾝﾄは1〜3桁の数字で入力して下さい。" ValidationExpression="\d{1,3}" ControlToValidate="txtChargeStartPoint"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									入金期日
								</td>
								<td class="tdDataStyle" colspan="4">
									請求対象となった会員の入金期日を最終入金から<asp:TextBox ID="txtReceiptSight" runat="server" MaxLength="2" Width="40px"></asp:TextBox>日後にする。
									<asp:RequiredFieldValidator ID="vdrReceiptSight" runat="server" ErrorMessage="入金ｻｲﾄを入力して下さい。" ControlToValidate="txtReceiptSight" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeReceiptSight" runat="server" ErrorMessage="入金ｻｲﾄは1〜2桁の数字で入力して下さい。" ValidationExpression="\d{1,2}" ControlToValidate="txtReceiptSight"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									督促日数
								</td>
								<td class="tdDataStyle" colspan="4">
									入金期日から<asp:TextBox ID="txtUrgeDays" runat="server" MaxLength="2" Width="40px"></asp:TextBox>日経過したらﾌﾞﾗｯｸ会員にする。
									<asp:RequiredFieldValidator ID="vdrUrgeDays" runat="server" ErrorMessage="督促日数を入力して下さい。" ControlToValidate="txtUrgeDays" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeUrgeDays" runat="server" ErrorMessage="督促日数は1〜2桁の数字で入力して下さい。" ValidationExpression="\d{1,2}" ControlToValidate="txtUrgeDays"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾕｰｻﾞｰﾄｯﾌﾟID
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtUserTopId" runat="server" Width="150px" MaxLength="32"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrUserTopId" runat="server" ErrorMessage="ﾕｰｻﾞｰﾄｯﾌﾟIDを入力して下さい。" ControlToValidate="txtUserTopId" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾉﾝﾕｰｻﾞｰﾄｯﾌﾟID
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtNonUserTopId" runat="server" Width="150px" MaxLength="32"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrNonUserTopId" runat="server" ErrorMessage="ﾉﾝﾕｰｻﾞｰﾄｯﾌﾟIDを入力して下さい。" ControlToValidate="txtNonUserTopId" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									出演者から男性への<br />
									ﾒｰﾙ一括送信上限数
								</td>
								<td class="tdDataStyle" colspan="4">
									1日<asp:TextBox ID="txtCastToManBatchMailLimit" runat="server" Width="50px" MaxLength="6"></asp:TextBox>通まで送信可能。
									<asp:RequiredFieldValidator ID="vdrCastToManBatchMailLimit" runat="server" ErrorMessage="出演者から男性へのﾒｰﾙ一括送信上限数を入力して下さい。" ControlToValidate="txtCastToManBatchMailLimit"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									出演者から男性への<br />
									会話招待ﾒｰﾙ送信上限数
								</td>
								<td class="tdDataStyle" colspan="4">
									1日<asp:TextBox ID="txtInviteMailLimit" runat="server" Width="50px" MaxLength="6"></asp:TextBox>通まで送信可能。
									<asp:RequiredFieldValidator ID="vdrInviteMailLimit" runat="server" ErrorMessage="出演者から男性への会話招待ﾒｰﾙ上限数を入力して下さい。" ControlToValidate="txtInviteMailLimit" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<asp:PlaceHolder ID="plcVisible4" runat="server">
								<tr>
									<td class="tdHeaderStyle">
										携帯で待機開始した時に、<br />
										このｻｲﾄのｷｬﾗｸﾀｰは<br />
										待機しない
									</td>
									<td class="tdDataStyle" colspan="4">
										<asp:CheckBox ID="chkMobileNotAutoLogin" runat="server" Text="出演者が携帯電話で待機開始した場合、待機状態にならない。" />
									</td>
								</tr>
								<tr>
									<td class="tdHeaderStyle">
										ﾌﾟﾘﾍﾟｰﾄﾞ新規登録時
									</td>
									<td class="tdDataStyle" colspan="4">
										<asp:CheckBox ID="chkRegPrepaidAddSvPtFlag" runat="server" Text="ｻｰﾋﾞｽﾎﾟｲﾝﾄを付与する。" />
									</td>
								</tr>
							</asp:PlaceHolder>
							<tr>
								<td class="tdHeaderStyle">
									出演者PCｷｬﾗｸﾀｰ設定<br />
									編集不可
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:CheckBox ID="chkCastPcModifyNaFlag" runat="server" Text="出演者PCのｷｬﾗｸﾀｰ設定でｺﾒﾝﾄ以外編集不可にする。" />
								</td>
							</tr>
							<asp:PlaceHolder ID="plcVisible5" runat="server">
								<tr>
									<td class="tdHeaderStyle">
										出演者ｷｬﾗｸﾀｰ検索画面<br />
										指名数表示
									</td>
									<td class="tdDataStyle" colspan="4">
										<asp:CheckBox ID="chkViewCallCountFlag" runat="server" Text="出演者ｷｬﾗｸﾀｰ検索画面で指名数を表示する。" />
									</td>
								</tr>
							</asp:PlaceHolder>
							<tr>
								<td class="tdHeaderStyle">
									ｽﾀﾝﾌﾟﾌｧｲﾙ名
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtStampFileNm" runat="server" Width="120px" MaxLength="32"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾓｻﾞｲｸ画像用ｽﾀﾝﾌﾟﾌｧｲﾙ名
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtStampSampleFileNm" runat="server" Width="120px" MaxLength="32"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									写真ｻｲｽﾞ大
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtPicSizeLarge" runat="server" Width="30px" MaxLength="3"></asp:TextBox>px
									<asp:RequiredFieldValidator ID="vdrPicSizeLarge" runat="server" ControlToValidate="txtPicSizeLarge" ErrorMessage="写真ｻｲｽﾞ大を入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdePicSizeLarge" runat="server" ControlToValidate="txtPicSizeLarge" ErrorMessage="写真ｻｲｽﾞ大は数字3桁以内で入力して下さい。" ValidationExpression="[0-9]{1,3}"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									写真ｻｲｽﾞ小
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtPicSizeSmall" runat="server" Width="30px" MaxLength="3"></asp:TextBox>px
									<asp:RequiredFieldValidator ID="vdrPicSizeSmall" runat="server" ControlToValidate="txtPicSizeSmall" ErrorMessage="写真ｻｲｽﾞ小を入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdePicSizeSmall" runat="server" ControlToValidate="txtPicSizeSmall" ErrorMessage="写真ｻｲｽﾞ小は数字3桁以内で入力して下さい。" ValidationExpression="[0-9]{1,3}"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									写真背景色
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtPicBackColor" runat="server" Width="70px" MaxLength="7"></asp:TextBox>
									<asp:CustomValidator ID="vdcPicBackColor" runat="server" ControlToValidate="txtPicBackColor" ErrorMessage="写真背景色を入力してください。" OnServerValidate="vdcPicBackColor_ServerValidate"
										ValidateEmptyText="true" ValidationGroup="Detail">写真背景色を入力してください。</asp:CustomValidator>
									<asp:RegularExpressionValidator ID="vdePicBackColor" runat="server" ErrorMessage="#で始まる6桁のｶﾗｰｺｰﾄﾞを入力して下さい。" ValidationExpression="#[a-fA-F0-9]{6}$" ControlToValidate="txtPicBackColor"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									縮尺方式
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:RadioButtonList ID="rdoScaleType" runat="server" RepeatDirection="Horizontal">
										<asp:ListItem Selected="true" Text="縦横比" Value="1"></asp:ListItem>
										<asp:ListItem Text="縮尺比" Value="2"></asp:ListItem>
										<asp:ListItem Text="正方形" Value="3"></asp:ListItem>
									</asp:RadioButtonList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									HTMLﾒｰﾙ作成時<br />
									WEB FACE未適用
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:CheckBox ID="chkMailDocNaWebFaceFlag" runat="server" Checked="false" Text="HTMLﾒｰﾙの作成時にWEB FACEを適用しない。(絵文字色やﾕｰｻﾞｰ文章のﾌｫﾝﾄｻｲｽﾞが指定できるので推奨)" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾒｰﾙ検索利用許可
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:CheckBox ID="chkMailSearchEnabled" runat="server" Checked="false" Text="ﾒｰﾙ検索機能を利用可能にする。" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾒｰﾙ検索上限件数
								</td>
								<td class="tdDataStyle" colspan="4">
									ﾒｰﾙ検索後の一覧には最大<asp:TextBox ID="txtMailSearchLimit" runat="server" Width="50px" MaxLength="5" />件まで表示する。
									<asp:RequiredFieldValidator ID="vdrMailSearchLimit" runat="server" ControlToValidate="txtMailSearchLimit" ErrorMessage="ﾒｰﾙ検索上限件数を入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeMailSearchLimit" runat="server" ControlToValidate="txtMailSearchLimit" ErrorMessage="ﾒｰﾙ検索上限件数は数字5桁以内で入力して下さい。"
										ValidationExpression="[0-9]{1,5}" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									出演者報酬振込手数料
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtTransferFee" runat="server" Width="50px" MaxLength="5" />円
									<asp:RequiredFieldValidator ID="vdrTransferFee" runat="server" ControlToValidate="txtTransferFee" ErrorMessage="出演者報酬振込手数料を入力して下さい。" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeTransferFee" runat="server" ControlToValidate="txtTransferFee" ErrorMessage="出演者報酬振込手数料は数字5桁以内で入力して下さい。" ValidationExpression="[0-9]{1,5}"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									出演者ﾃﾞﾌｫﾙﾄ<br />
									成果発生ﾀｲﾐﾝｸﾞ
								</td>
								<td class="tdDataStyle" colspan="4">
									登録成果発生ﾀｲﾐﾝｸﾞのﾃﾞﾌｫﾙﾄ値を<asp:DropDownList ID="lstCastRegistReportTiming" runat="server" DataSourceID="dsCastRegistReportTiming" DataTextField="CODE_NM" DataValueField="CODE"
										OnDataBound="lst_DataBound">
									</asp:DropDownList>にする。
									<asp:RequiredFieldValidator ID="vdrCastRegistReportTiming" runat="server" ErrorMessage="出演者ﾃﾞﾌｫﾙﾄ成果発生ﾀｲﾐﾝｸﾞを選択して下さい。" ControlToValidate="lstCastRegistReportTiming"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									男性会員ﾃﾞﾌｫﾙﾄ<br />
									成果発生ﾀｲﾐﾝｸﾞ
								</td>
								<td class="tdDataStyle" colspan="4">
									登録成果発生ﾀｲﾐﾝｸﾞのﾃﾞﾌｫﾙﾄ値を<asp:DropDownList ID="lstManRegistReportTiming" runat="server" DataSourceID="dsManRegistReportTiming" DataTextField="CODE_NM" DataValueField="CODE"
										OnDataBound="lst_DataBound">
									</asp:DropDownList>にする。
									<asp:RequiredFieldValidator ID="vdrManRegistReportTiming" runat="server" ErrorMessage="男性会員ﾃﾞﾌｫﾙﾄ成果発生ﾀｲﾐﾝｸﾞを選択して下さい。" ControlToValidate="lstManRegistReportTiming"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									PCﾘﾀﾞｲﾚｸﾄ先URL
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtPcRedirectUrl" runat="server" Width="420px" MaxLength="256"></asp:TextBox>
									<asp:RegularExpressionValidator ID="vdePcRedirectUrl" runat="server" ErrorMessage="PCﾘﾀﾞｲﾚｸﾄ先URLを正しく入力して下さい。" ValidationExpression="(\S+)://([^:/]+)(:(\d+))?(/[^#\s]*)(#(\S+))?"
										ControlToValidate="txtPcRedirectUrl" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									HTMLﾍﾟｰｼﾞﾀｲﾄﾙ
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtPageTitle" runat="server" Width="420px" MaxLength="60"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrPageTitle" runat="server" ErrorMessage="HTMLﾍﾟｰｼﾞﾀｲﾄﾙを入力して下さい。" ControlToValidate="txtPageTitle" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									HTMLﾍﾟｰｼﾞｷｰﾜｰﾄﾞ
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtPageKeyword" runat="server" Width="420px" MaxLength="60"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrPageKeyword" runat="server" ErrorMessage="HTMLﾍﾟｰｼﾞｷｰﾜｰﾄﾞを入力して下さい。" ControlToValidate="txtPageKeyword" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									HTMLﾍﾟｰｼﾞ説明
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtPageDescription" runat="server" Width="420px" MaxLength="60"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrPageDescription" runat="server" ErrorMessage="HTMLﾍﾟｰｼﾞ説明を入力して下さい。" ControlToValidate="txtPageDescription" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾄﾞｺﾓ用CSSﾌｧｲﾙ名
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtCssDocomo" runat="server" Width="200px" MaxLength="48"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ｿﾌﾄﾊﾞﾝｸ用CSSﾌｧｲﾙ名
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtCssSoftBank" runat="server" Width="200px" MaxLength="48"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									AU用CSSﾌｧｲﾙ名
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtCssAU" runat="server" Width="200px" MaxLength="48"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									Android用CSSﾌｧｲﾙ名
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtCssAndroid" runat="server" Width="200px" MaxLength="48"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									iPhone用CSSﾌｧｲﾙ名
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtCssiPhone" runat="server" Width="200px" MaxLength="48"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									Google認証ｷｰ
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtGoogleVerification" runat="server" Width="420px" MaxLength="60"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									男性会員登録時<br />
									機能制限広告コード
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtManRegistFuncLimitAdCd" runat="server" MaxLength="40" Width="200px"></asp:TextBox>が含まれている広告コードから登録があった場合、機能を制限する。
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									外部動画ｻｲﾄURL
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:TextBox ID="txtMovieSiteUrl" runat="server" MaxLength="80" Width="200px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									利用不可
								</td>
								<td class="tdDataStyle" colspan="4">
									<asp:CheckBox ID="chkNaFlag" runat="server" Text="ｻｲﾄを利用不可にする。" />
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" Visible="false" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[サイト一覧]</legend>
			<asp:GridView ID="grdSite" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsSite" AllowSorting="True" SkinID="GridViewColor">
				<Columns>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkSiteCd" runat="server" Text='<%# Eval("SITE_CD") %>' CommandArgument='<%# Eval("SITE_CD") %>' OnCommand="lnkSiteCd_Command" CausesValidation="False">
							</asp:LinkButton>
							<asp:Label ID="lblConnect" runat="server" Text='<%# Bind("SITE_NM") %>'></asp:Label>
						</ItemTemplate>
						<HeaderTemplate>
							サイトコード
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:BoundField DataField="SITE_MARK" HeaderText="略号"></asp:BoundField>
					<asp:BoundField DataField="PRIORITY" HeaderText="INDEX"></asp:BoundField>
					<asp:BoundField DataField="HOST_NM" HeaderText="HOST"></asp:BoundField>
					<asp:BoundField DataField="SUB_HOST_NM" HeaderText="SUB.HOST"></asp:BoundField>
					<asp:BoundField DataField="URL" HeaderText="URL"></asp:BoundField>
					<asp:BoundField DataField="LOCAL_IP_ADDR" HeaderText="Local IP"></asp:BoundField>
					<asp:BoundField DataField="LOCAL_PORT" HeaderText="Port"></asp:BoundField>
					<asp:BoundField DataField="UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<a class="reccount">Record Count
				<%#GetRecCount() %>
			</a>
			<br />
			<a class="reccount">Current viewing page
				<%=grdSite.PageIndex + 1%>
				of
				<%=grdSite.PageCount%>
			</a>
			<div class="button">
				<asp:Button ID="btnRegist" runat="server" Text="サイト追加" OnClick="btnRegist_Click" />
			</div>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetPageCollection" TypeName="Site" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsSite_Selected"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsOtherWebSite" runat="server" SelectMethod="GetUnusedList" TypeName="OtherWebSite"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsLightChConnectType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="95" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsManRegistReportTiming" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="16" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastRegistReportTiming" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="20" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPriority" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtLocalPort" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtChargeStartPoint" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtUrgeDays" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtReceiptSight" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtIisSiteIdentifier" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" Enabled="true" FilterType="Custom, Numbers, LowercaseLetters, UppercaseLetters"
		ValidChars=".&?=" TargetControlID="txtUserTopId" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" Enabled="true" FilterType="Custom, Numbers, LowercaseLetters, UppercaseLetters"
		ValidChars=".&?=" TargetControlID="txtNonUserTopId" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtCastToManBatchMailLimit" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtInviteMailLimit" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtFirstTimeFreeTalkSec" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrSiteCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeSiteCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrSiteNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender48" TargetControlID="vdrSiteMark" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender49" TargetControlID="vdeSiteMark" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdrSiteType" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10" TargetControlID="vdrUrl" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11" TargetControlID="vdeUrl" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8" TargetControlID="vdrWebPhisicalDir" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9" TargetControlID="vdrHostNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdeHostNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender54" TargetControlID="vdrSubHostNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender55" TargetControlID="vdeSubHostNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender64" TargetControlID="vdrGlobalIpAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender70" TargetControlID="vdeGlobalIpAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender71" TargetControlID="vdrIisSiteIdentifier" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender12" TargetControlID="vdrMailHost" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdeMailHost" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender28" TargetControlID="vdeJobOfferSiteHostNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender29" TargetControlID="vdeJobOfferStartDoc" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender13" TargetControlID="vdrIvpLocCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender15" TargetControlID="vdeIvpLocCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender16" TargetControlID="vdrIvpSiteCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender17" TargetControlID="vdeIvpSiteCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vdrIvpObjectStoreFolder" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender14" TargetControlID="vdeIvpObjectStoreFolder" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender18" TargetControlID="vdrPriority" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender59" TargetControlID="vdcPriority" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender19" TargetControlID="vdrLocalIpAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender20" TargetControlID="vdeLocalIpAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender25" TargetControlID="vdrNextBusinessTime" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender26" TargetControlID="vdeNextBusinessTime" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender32" TargetControlID="vdeWshotTalkDialoutNo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender33" TargetControlID="vdePublicTalkDialoutNo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender79" TargetControlID="vdeWhitePlanNo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender34" TargetControlID="vdrPointPrice" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender35" TargetControlID="vdePointPrice" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender95" TargetControlID="vdrPointTax" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender96" TargetControlID="vdePointTax" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender36" TargetControlID="vdrUserMailDelDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender37" TargetControlID="vdeUserMailDelDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender30" TargetControlID="vdrNotifyMailDelDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender31" TargetControlID="vdeNotifyMailDelDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender93" TargetControlID="vdrBlogMailDelDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender94" TargetControlID="vdeBlogMailDelDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender38" TargetControlID="vdrMarkingDelDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender39" TargetControlID="vdeMarkingDelDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender80" TargetControlID="vdrUserLogDelDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender81" TargetControlID="vdeUserLogDelDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender82" TargetControlID="vdrTalkHistoryDelDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender83" TargetControlID="vdeTalkHistoryDelDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender84" TargetControlID="vdrBbsWriteDelDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender85" TargetControlID="vdeBbsWriteDelDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender86" TargetControlID="vdrBbsPicDelDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender87" TargetControlID="vdeBbsPicDelDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender88" TargetControlID="vdrBbsMovieDelDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender89" TargetControlID="vdeBbsMovieDelDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender90" TargetControlID="vdrUrgeDelDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender91" TargetControlID="vdeUrgeDelDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender42" TargetControlID="vdrDiaryDelDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender43" TargetControlID="vdeDiaryDelDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender97" TargetControlID="vdrManTweetDelDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender98" TargetControlID="vdeManTweetDelDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender44" TargetControlID="vdrSummaryReportDelDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender45" TargetControlID="vdeSummaryReportDelDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender46" TargetControlID="vdrReceiptDelDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender47" TargetControlID="vdeReceiptDelDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender50" TargetControlID="vdrHiMailerIP" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender51" TargetControlID="vdeHiMailerIP" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender52" TargetControlID="vdrLoMailerIP" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender53" TargetControlID="vdeLoMailerIP" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender56" TargetControlID="vdrPageTitle" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender57" TargetControlID="vdrPageKeyword" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender58" TargetControlID="vdrPageDescription" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender27" TargetControlID="vdcOtherWebSysId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender40" TargetControlID="vdrChargeStartPoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender41" TargetControlID="vdeChargeStartPoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender60" TargetControlID="vdrReceiptSight" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender61" TargetControlID="vdeReceiptSight" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender62" TargetControlID="vdrUrgeDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender63" TargetControlID="vdeUrgeDays" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender66" TargetControlID="vdeVcsTenantCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender69" TargetControlID="vdeVcsWebLocCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender67" TargetControlID="vdrUserTopId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender68" TargetControlID="vdrNonUserTopId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender65" TargetControlID="vdrCastToManBatchMailLimit" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender21" TargetControlID="vdrInviteMailLimit" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender72" TargetControlID="vdrPicSizeLarge" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender73" TargetControlID="vdePicSizeLarge" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender74" TargetControlID="vdrPicSizeSmall" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender75" TargetControlID="vdePicSizeSmall" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender76" TargetControlID="vdrMailSearchLimit" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender77" TargetControlID="vdeMailSearchLimit" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender78" TargetControlID="vdePicBackColor" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender22" TargetControlID="vdrFirstTimeFreeTalkSec" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender23" TargetControlID="vdrCastRegistReportTiming" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender24" TargetControlID="vdrManRegistReportTiming" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender92" TargetControlID="vdrLightChConnectType" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender3" runat="server" TargetControlID="Button1" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender4" runat="server" TargetControlID="Button2" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
