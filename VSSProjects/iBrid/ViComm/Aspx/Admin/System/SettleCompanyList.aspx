﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SettleCompanyList.aspx.cs" Inherits="System_SettleCompanyList"
	Title="決済会社設定" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="決済会社設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 400px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									決済会社コード
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtSettleCompanyCd" runat="server" MaxLength="2" Width="20px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrSettleCompanyCd" runat="server" ErrorMessage="決済会社コードを入力して下さい。" ControlToValidate="txtSettleCompanyCd" ValidationGroup="Key">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeSettleCompanyCd" runat="server" ErrorMessage="決済会社コードは2桁の数字で入力して下さい。" ValidationExpression="\d{2}" ControlToValidate="txtSettleCompanyCd"
										ValidationGroup="Key">*</asp:RegularExpressionValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[決済会社内容]</legend>
						<table border="0" style="width: 400px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									決済会社名
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtSettleCompanyNm" runat="server" MaxLength="30" Width="180px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrSettleCompanyNm" runat="server" ErrorMessage="決済会社名を入力して下さい。" ControlToValidate="txtSettleCompanyNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
						</table>
					</fieldset>
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
					<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
					<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset class="fieldset">
			<legend>[決済会社一覧]</legend>
			<asp:GridView ID="grdSettleCompany" AllowPaging="True" runat="server" AutoGenerateColumns="False" DataSourceID="dsSettleCompany" AllowSorting="True" SkinID="GridViewColor">
				<Columns>
					<asp:TemplateField>
						<HeaderTemplate>
							決済会社コード
						</HeaderTemplate>
						<ItemTemplate>
							<asp:LinkButton ID="lnkSettleCompanyCd" runat="server" Text='<%# Bind("SETTLE_COMPANY_CD") %>' CommandArgument='<%# Bind("SETTLE_COMPANY_CD") %>' OnCommand="lnkSettleCompanyCd_Command"
								CausesValidation="False">
							</asp:LinkButton>
							<asp:Label ID="lblConnect" runat="server" Text='<%# Bind("SETTLE_COMPANY_NM") %>'></asp:Label>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:BoundField DataField="UPDATE_DATE" HeaderText="更新日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br>
				<a class="reccount">Current viewing page
					<%=grdSettleCompany.PageIndex + 1%>
					of
					<%=grdSettleCompany.PageCount%>
				</a>
			</asp:Panel>
			<div class="button">
				<asp:Button ID="btnRegist" runat="server" Text="決済会社追加" OnClick="btnRegist_Click" />
			</div>
		</fieldset>
	</div>
	<asp:ObjectDataSource ID="dsSettleCompany" runat="server" SelectMethod="GetPageCollection" TypeName="SettleCompany" SelectCountMethod="GetPageCount" OnSelected="dsSettleCompany_Selected"
		EnablePaging="True"></asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrSettleCompanyCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdeSettleCompanyCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrSettleCompanyNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
