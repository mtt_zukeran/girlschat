﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: システム設定メンテナンス
--	Progaram ID		: SysMainte
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;

public partial class System_SysMainte:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			plcMaqiaAffiliate.Visible = true;
			InitPage();
		}
	}

	protected void dsSite_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		ClearField();
		GetData();
	}

	private void ClearField() {
		txtSipIvpLocCd.Text = "";
		txtSipIvpSiteCd.Text = "";
		txtSipRegistUrl.Text = "";
		txtSipDomain.Text = "";
		txtSipDomainLocal.Text = "";
		txtMailIFUrl.Text = "";
		txtVcsIFUrl.Text = "";
		chkAvailableMaqiaAffiliateFlag.Checked = false;
		chkTextMailOtherRelayFlag.Checked = false;
		chkAvaCheckExistAddrFlag.Checked = false;
		txtAffiliIFUrl.Text = "";
		txtCreditMeasuredRateIFUrl.Text = "";
		txtInstallDir.Text = "";
		txtIvpAdminUrl.Text = "";
		txtIvpRequestUrl.Text = "";
		txtIvpIssueLiveKeyUrl.Text = "";
		txtIvpStartupCamera.Text = "";
		txtLiveKey.Text = "";
		txtVcsFtpId.Text = "";
		txtVcsFtpPw.Text = "";
		txtVcsFtpIP.Text = "";
		txtVcsRequestUrl.Text = "";
		txtMailTxCycleSec.Text = "";
		txtMailTxUnit.Text = "";
		txtWhitePlanDisconnTime.Text = "";
		txtWhitePlanNaTime.Text = "";
		txtNoLoginClearPayDays.Text = "";
		chkTalkSmartDirectUseIvpFlag.Checked = false;
		chkXsmVoicePhoneLineFlag.Checked = false;
		txtTwilioAccountSid.Text = "";
		txtTwilioAuthToken.Text = "";
		txtTwilioTelNo.Text = "";
		txtTwilioFreedialTelNo.Text = "";
		txtTwilioWhiteplanTelNo.Text = "";
		txtTwilioSmsTelNo.Text = "";
		txtVoiceappStatusUrl.Text = "";
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SYS_GET");
			db.ProcedureOutParm("PSIP_IVP_LOC_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSIP_IVP_SITE_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSIP_REGIST_URL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMAIL_IF_URL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PVCS_IF_URL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMAQIA_AFFILIATE_IF_URL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCREDIT_MEASURED_RATE_IF_URL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSIP_DOMAIN",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSIP_DOMAIN_LOCAL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PIVP_ADMIN_URL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PIVP_REQUEST_URL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PIVP_ISSUE_LIVE_KEY_URL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PIVP_STARTUP_CAMERA_URL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PLIVE_KEY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PINSTALL_DIR",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PDECOMAIL_SERVER_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PVCS_REQUEST_URL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PVCS_FTP_IP",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PVCS_FTP_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PVCS_FTP_PW",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMAIL_TX_UNIT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMAIL_TX_CYCLE_SEC",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMARK_MAIL_ADDR_NG_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PAVAILABLE_MAQIA_AFFILIATE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PTEXT_MAIL_OTHER_RELAY_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PWHITE_PLAN_DISCONN_TIME",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PWHITE_PLAN_NA_TIME",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PNO_LOGIN_CLEAR_PAY_DAYS",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PAVA_CHECK_EXIST_ADDR_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pCROSMILE_SIPURI_GET_IF_URL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pCROSMILE_SIPURI_EXIST_IF_URL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pTALK_SMART_DIRECT_USE_IVP_FLG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pXSM_VOICE_PHONE_LINE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pTWILIO_ACCOUNT_SID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pTWILIO_AUTH_TOKEN",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pTWILIO_TEL_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pTWILIO_FREEDIAL_TEL_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pTWILIO_WHITEPLAN_TEL_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pTWILIO_SMS_TEL_NO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pVOICEAPP_STATUS_URL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtSipIvpLocCd.Text = db.GetStringValue("PSIP_IVP_LOC_CD");
				txtSipIvpSiteCd.Text = db.GetStringValue("PSIP_IVP_SITE_CD");
				txtSipRegistUrl.Text = db.GetStringValue("PSIP_REGIST_URL");
				txtSipDomain.Text = db.GetStringValue("PSIP_DOMAIN");
				txtSipDomainLocal.Text = db.GetStringValue("PSIP_DOMAIN_LOCAL");
				txtMailIFUrl.Text = db.GetStringValue("PMAIL_IF_URL");
				txtVcsIFUrl.Text = db.GetStringValue("PVCS_IF_URL");
				txtAffiliIFUrl.Text = db.GetStringValue("PMAQIA_AFFILIATE_IF_URL");
				txtCreditMeasuredRateIFUrl.Text = db.GetStringValue("PCREDIT_MEASURED_RATE_IF_URL");
				txtIvpAdminUrl.Text = db.GetStringValue("PIVP_ADMIN_URL");
				txtInstallDir.Text = db.GetStringValue("PINSTALL_DIR");
				txtIvpRequestUrl.Text = db.GetStringValue("PIVP_REQUEST_URL");
				txtIvpIssueLiveKeyUrl.Text = db.GetStringValue("PIVP_ISSUE_LIVE_KEY_URL");
				txtIvpStartupCamera.Text = db.GetStringValue("PIVP_STARTUP_CAMERA_URL");
				txtLiveKey.Text = db.GetStringValue("PLIVE_KEY");
				lstDecomMailServerType.SelectedValue = db.GetStringValue("PDECOMAIL_SERVER_TYPE");
				txtVcsRequestUrl.Text = db.GetStringValue("PVCS_REQUEST_URL");
				txtVcsFtpIP.Text = db.GetStringValue("PVCS_FTP_IP");
				txtVcsFtpPw.Text = db.GetStringValue("PVCS_FTP_ID");
				txtVcsFtpId.Text = db.GetStringValue("PVCS_FTP_PW");
				txtMailTxUnit.Text = db.GetStringValue("PMAIL_TX_UNIT");
				txtMailTxCycleSec.Text = db.GetStringValue("PMAIL_TX_CYCLE_SEC");
				txtMarkMailAddrNgCount.Text = db.GetStringValue("PMARK_MAIL_ADDR_NG_COUNT");
				chkAvailableMaqiaAffiliateFlag.Checked = db.GetStringValue("PAVAILABLE_MAQIA_AFFILIATE").Equals("1");
				chkTextMailOtherRelayFlag.Checked = db.GetStringValue("PTEXT_MAIL_OTHER_RELAY_FLAG").Equals("1");
				chkAvaCheckExistAddrFlag.Checked = db.GetStringValue("PAVA_CHECK_EXIST_ADDR_FLAG").Equals("1");
				txtWhitePlanDisconnTime.Text = db.GetStringValue("PWHITE_PLAN_DISCONN_TIME");
				txtWhitePlanNaTime.Text = db.GetStringValue("PWHITE_PLAN_NA_TIME");
				txtNoLoginClearPayDays.Text = db.GetStringValue("PNO_LOGIN_CLEAR_PAY_DAYS");
				txtCrosmileSipuriGetIFUrl.Text = db.GetStringValue("pCROSMILE_SIPURI_GET_IF_URL");
				txtCrosmileSipuriExistIFUrl.Text = db.GetStringValue("pCROSMILE_SIPURI_EXIST_IF_URL");
				chkTalkSmartDirectUseIvpFlag.Checked = db.GetStringValue("pTALK_SMART_DIRECT_USE_IVP_FLG").Equals("1");
				chkXsmVoicePhoneLineFlag.Checked = db.GetStringValue("pXSM_VOICE_PHONE_LINE_FLAG").Equals("1");
				txtTwilioAccountSid.Text = db.GetStringValue("pTWILIO_ACCOUNT_SID");
				txtTwilioAuthToken.Text = db.GetStringValue("pTWILIO_AUTH_TOKEN");
				txtTwilioTelNo.Text = db.GetStringValue("pTWILIO_TEL_NO");
				txtTwilioFreedialTelNo.Text = db.GetStringValue("pTWILIO_FREEDIAL_TEL_NO");
				txtTwilioWhiteplanTelNo.Text = db.GetStringValue("pTWILIO_WHITEPLAN_TEL_NO");
				txtTwilioSmsTelNo.Text = db.GetStringValue("pTWILIO_SMS_TEL_NO");
				txtVoiceappStatusUrl.Text = db.GetStringValue("pVOICEAPP_STATUS_URL");
			} else {
				ClearField();
				lstDecomMailServerType.SelectedIndex = 0;
			}
		}
	}

	private void UpdateData(int pDelFlag) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SYS_MAINTE");
			db.ProcedureInParm("PSIP_IVP_LOC_CD",DbSession.DbType.VARCHAR2,txtSipIvpLocCd.Text);
			db.ProcedureInParm("PSIP_IVP_SITE_CD",DbSession.DbType.VARCHAR2,txtSipIvpSiteCd.Text);
			db.ProcedureInParm("PSIP_REGIST_URL",DbSession.DbType.VARCHAR2,txtSipRegistUrl.Text);
			db.ProcedureInParm("PSIP_DOMAIN",DbSession.DbType.VARCHAR2,txtSipDomain.Text);
			db.ProcedureInParm("PSIP_DOMAIN_LOCAL",DbSession.DbType.VARCHAR2,txtSipDomainLocal.Text);
			db.ProcedureInParm("PMAIL_IF_URL",DbSession.DbType.VARCHAR2,txtMailIFUrl.Text);
			db.ProcedureInParm("PVCS_IF_URL",DbSession.DbType.VARCHAR2,txtVcsIFUrl.Text);
			db.ProcedureInParm("PMAQIA_AFFILIATE_IF_URL",DbSession.DbType.VARCHAR2,txtAffiliIFUrl.Text);
			db.ProcedureInParm("PCREDIT_MEASURED_RATE_IF_URL",DbSession.DbType.VARCHAR2,txtCreditMeasuredRateIFUrl.Text);
			db.ProcedureInParm("PIVP_ADMIN_URL",DbSession.DbType.VARCHAR2,txtIvpAdminUrl.Text);
			db.ProcedureInParm("PIVP_REQUEST_URL",DbSession.DbType.VARCHAR2,txtIvpRequestUrl.Text);
			db.ProcedureInParm("PIVP_ISSUE_LIVE_KEY_URL",DbSession.DbType.VARCHAR2,txtIvpIssueLiveKeyUrl.Text);
			db.ProcedureInParm("PIVP_STARTUP_CAMERA_URL",DbSession.DbType.VARCHAR2,txtIvpStartupCamera.Text);
			db.ProcedureInParm("PLIVE_KEY",DbSession.DbType.VARCHAR2,txtLiveKey.Text);
			db.ProcedureInParm("PINSTALL_DIR",DbSession.DbType.VARCHAR2,txtInstallDir.Text);
			db.ProcedureInParm("PDECOMAIL_SERVER_TYPE",DbSession.DbType.VARCHAR2,lstDecomMailServerType.SelectedValue);
			db.ProcedureInParm("PVCS_REQUEST_URL",DbSession.DbType.VARCHAR2,txtVcsRequestUrl.Text);
			db.ProcedureInParm("PVCS_FTP_IP",DbSession.DbType.VARCHAR2,txtVcsFtpIP.Text);
			db.ProcedureInParm("PVCS_FTP_ID",DbSession.DbType.VARCHAR2,txtVcsFtpId.Text);
			db.ProcedureInParm("PVCS_FTP_PW",DbSession.DbType.VARCHAR2,txtVcsFtpPw.Text);
			db.ProcedureInParm("PMAIL_TX_UNIT",DbSession.DbType.NUMBER,int.Parse(txtMailTxUnit.Text));
			db.ProcedureInParm("PMAIL_TX_CYCLE_SEC",DbSession.DbType.NUMBER,int.Parse(txtMailTxCycleSec.Text));
			db.ProcedureInParm("PMARK_MAIL_ADDR_NG_COUNT",DbSession.DbType.NUMBER,int.Parse(txtMarkMailAddrNgCount.Text));
			db.ProcedureInParm("PAVAILABLE_MAQIA_AFFILIATE",DbSession.DbType.NUMBER,chkAvailableMaqiaAffiliateFlag.Checked);
			db.ProcedureInParm("PTEXT_MAIL_OTHER_RELAY_FLAG",DbSession.DbType.NUMBER,chkTextMailOtherRelayFlag.Checked);
			db.ProcedureInParm("PWHITE_PLAN_DISCONN_TIME",DbSession.DbType.VARCHAR2,txtWhitePlanDisconnTime.Text);
			db.ProcedureInParm("PWHITE_PLAN_NA_TIME",DbSession.DbType.VARCHAR2,txtWhitePlanNaTime.Text);
			db.ProcedureInParm("PNO_LOGIN_CLEAR_PAY_DAYS",DbSession.DbType.NUMBER,int.Parse(txtNoLoginClearPayDays.Text));
			db.ProcedureInParm("PAVA_CHECK_EXIST_ADDR_FLAG",DbSession.DbType.NUMBER,chkAvaCheckExistAddrFlag.Checked);
			db.ProcedureInParm("pCROSMILE_SIPURI_GET_IF_URL",DbSession.DbType.VARCHAR2,txtCrosmileSipuriGetIFUrl.Text);
			db.ProcedureInParm("pCROSMILE_SIPURI_EXIST_IF_URL",DbSession.DbType.VARCHAR2,txtCrosmileSipuriExistIFUrl.Text);
			db.ProcedureInParm("pTALK_SMART_DIRECT_USE_IVP_FLG",DbSession.DbType.NUMBER,chkTalkSmartDirectUseIvpFlag.Checked);
			db.ProcedureInParm("pXSM_VOICE_PHONE_LINE_FLAG",DbSession.DbType.NUMBER,chkXsmVoicePhoneLineFlag.Checked);
			db.ProcedureInParm("pTWILIO_ACCOUNT_SID",DbSession.DbType.VARCHAR2,txtTwilioAccountSid.Text);
			db.ProcedureInParm("pTWILIO_AUTH_TOKEN",DbSession.DbType.VARCHAR2,txtTwilioAuthToken.Text);
			db.ProcedureInParm("pTWILIO_TEL_NO",DbSession.DbType.VARCHAR2,txtTwilioTelNo.Text);
			db.ProcedureInParm("pTWILIO_FREEDIAL_TEL_NO",DbSession.DbType.VARCHAR2,txtTwilioFreedialTelNo.Text);
			db.ProcedureInParm("pTWILIO_WHITEPLAN_TEL_NO",DbSession.DbType.VARCHAR2,txtTwilioWhiteplanTelNo.Text);
			db.ProcedureInParm("pTWILIO_SMS_TEL_NO",DbSession.DbType.VARCHAR2,txtTwilioSmsTelNo.Text);
			db.ProcedureInParm("pVOICEAPP_STATUS_URL",DbSession.DbType.VARCHAR2,txtVoiceappStatusUrl.Text);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		InitPage();
	}
}
