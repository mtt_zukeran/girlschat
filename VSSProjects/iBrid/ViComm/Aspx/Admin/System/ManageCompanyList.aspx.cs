﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 運営会社設定
--	Progaram ID		: System_ManageCompanyList
--
--  Creation Date	: 2009.09.07
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class System_ManageCompanyList:System.Web.UI.Page {
	private string recCount = "";
	private const int MAX_SETTLE_TYPE = 23;

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	protected void dsManageCompany_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		int iPageCount;
		grdCompany.PageSize = int.Parse(Session["PageSize"].ToString());
		grdCompany.PageIndex = 0;
		txtManageCompanyCd.Text = "";
		ClearField();
		pnlMainte.Visible = false;
		DataBind();
		using (ManageCompany oManageCompany = new ManageCompany()) {
			iPageCount = oManageCompany.GetPageCount();
			if (iPageCount > 0) {
				btnRegist.Enabled = false;
			} else {
				btnRegist.Enabled = true;
			}
		}
	}

	private void ClearField() {
		txtManageCompanyNm.Text = "";
		txtManagePersonNm.Text = "";
		txtTel.Text = "";
		txtEMailAddr.Text = "";
		txtCastSelfMinimumPayment.Text = "";
		txtCastAutoMinimumPayment.Text = "";
		txtWomanOnlineMin.Text = "";
		txtNotPayExpireMonth.Text = "";
		txtCallLockTimeoutSec.Text = "";
		txtCallTimeoutSec.Text = "";
		chkAutoGetTelInfoFlag.Checked = false;
		chkWomanUsedEasyLogin.Checked = false;
		chkManUsedEasyLogin.Checked = false;
		chkDupSubscrSetDummyFlag.Checked = false;
		chkExecMonthlyAutoPaymentFlag.Checked = false;
		for (int i = 0;i < MAX_SETTLE_TYPE;i++) {
			CheckBox chkSettle = (CheckBox)plcSettleList.FindControl(string.Format("chkSettleFlag{0}",i)) as CheckBox;
			chkSettle.Checked = false;
		}
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();

		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlDtl2.Visible = false;
		pnlKey.Enabled = false;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
		InitPage();
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		pnlMainte.Visible = true;
		pnlKey.Enabled = true;
		pnlDtl.Visible = false;
		pnlDtl2.Visible = false;
	}

	protected void lnkAdminId_Command(object sender,CommandEventArgs e) {
		txtManageCompanyCd.Text = e.CommandArgument.ToString();
		GetData();

		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlDtl2.Visible = false;
		pnlKey.Enabled = false;
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MANAGE_COMPANY_GET");
			db.ProcedureInParm("PMANAGE_COMPANY_CD",DbSession.DbType.VARCHAR2,txtManageCompanyCd.Text);
			db.ProcedureOutParm("PMANAGE_COMPANY_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PTEL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMANAGE_PERSON_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PEMAIL_ADDR",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSETTLE_COMPANY_MASK",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_SELF_MINIMUM_PAYMENT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_AUTO_MINIMUM_PAYMENT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PAUTO_GET_TEL_INFO_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PAD_MANAGE_HOST",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PWOMAN_ONLINE_MIN",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PWOMAN_USED_EASY_LOGIN",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMAN_USED_EASY_LOGIN",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PWOMAN_GET_TERM_ID_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMAN_GET_TERM_ID_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PNOT_PAY_EXPIRE_MONTH",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCALL_LOCK_TIMEOUT_SEC",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCALL_TIMEOUT_SEC",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PDUP_SUBSCR_SET_DUMMY_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PEXEC_MONTHLY_AUTO_PAYMENT_FLG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PUPDATE_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtManageCompanyCd.Text = db.GetStringValue("PMANAGE_COMPANY_CD");
				txtManageCompanyNm.Text = db.GetStringValue("PMANAGE_COMPANY_NM");
				txtTel.Text = db.GetStringValue("PTEL");
				txtManagePersonNm.Text = db.GetStringValue("PMANAGE_PERSON_NM");
				txtEMailAddr.Text = db.GetStringValue("PEMAIL_ADDR");
				txtCastSelfMinimumPayment.Text = db.GetStringValue("PCAST_SELF_MINIMUM_PAYMENT");
				txtCastAutoMinimumPayment.Text = db.GetStringValue("PCAST_AUTO_MINIMUM_PAYMENT");
				txtAdManageHost.Text = db.GetStringValue("PAD_MANAGE_HOST");
				txtWomanOnlineMin.Text = db.GetStringValue("PWOMAN_ONLINE_MIN");
				txtNotPayExpireMonth.Text = db.GetStringValue("PNOT_PAY_EXPIRE_MONTH");
				txtCallLockTimeoutSec.Text = db.GetStringValue("PCALL_LOCK_TIMEOUT_SEC");
				txtCallTimeoutSec.Text = db.GetStringValue("PCALL_TIMEOUT_SEC");
				int iSettleCompanyMask = db.GetIntValue("PSETTLE_COMPANY_MASK");
				int iMaskValue = 1;
				chkAutoGetTelInfoFlag.Checked = db.GetStringValue("PAUTO_GET_TEL_INFO_FLAG").Equals("1");
				chkWomanUsedEasyLogin.Checked = db.GetStringValue("PWOMAN_USED_EASY_LOGIN").Equals("1");
				chkManUsedEasyLogin.Checked = db.GetStringValue("PMAN_USED_EASY_LOGIN").Equals("1");
				chkWomanGetTermIdFlag.Checked = db.GetStringValue("PWOMAN_GET_TERM_ID_FLAG").Equals("1");
				chkManGetTermIdFlag.Checked = db.GetStringValue("PMAN_GET_TERM_ID_FLAG").Equals("1");
				chkDupSubscrSetDummyFlag.Checked = db.GetStringValue("PDUP_SUBSCR_SET_DUMMY_FLAG").Equals("1");
				chkExecMonthlyAutoPaymentFlag.Checked = db.GetStringValue("PEXEC_MONTHLY_AUTO_PAYMENT_FLG").Equals("1");

				for (int i = 0;i < MAX_SETTLE_TYPE;i++) {
					CheckBox chkSettle = (CheckBox)plcSettleList.FindControl(string.Format("chkSettleFlag{0}",i)) as CheckBox;
					if ((iMaskValue & iSettleCompanyMask) != 0) {
						chkSettle.Checked = true;
					}
					iMaskValue = iMaskValue << 1;
				}
			} else {
				ClearField();
			}
		}

		using (ManageCompany oManageCompany = new ManageCompany()) {
			DataTable dt = new DataTable();
			dt.Columns.Add("RELEASE_PROGRAM_BIT",typeof(bool));
			dt.Columns.Add("RELEASE_PROGRAM_SUMMARY");
			dt.Columns.Add("RELEASE_PROGRAM_ID");
			dt.Columns.Add("RELEASE_PROGRAM_SUB_ID");
			dt.Columns.Add("REQUEST_COMPANY");

			DataSet dsReleaseProgramMask = oManageCompany.GetReleaseProgramMask(txtManageCompanyCd.Text);

			foreach (DataRow dr in dsReleaseProgramMask.Tables[0].Rows) {
				if (dr["RELEASE_PROGRAM_BIT"].ToString() == "1") {
					dt.Rows.Add(true,dr["RELEASE_PROGRAM_SUMMARY"].ToString(),dr["RELEASE_PROGRAM_ID"],dr["RELEASE_PROGRAM_SUB_ID"],dr["REQUEST_COMPANY"]);
				} else {
					dt.Rows.Add(false,dr["RELEASE_PROGRAM_SUMMARY"].ToString(),dr["RELEASE_PROGRAM_ID"],dr["RELEASE_PROGRAM_SUB_ID"],dr["REQUEST_COMPANY"]);
				}
			}

			grdReleaseProgramMask.DataSource = dt;
			grdReleaseProgramMask.DataBind();
		}
	}

	private void UpdateData(int pDelFlag) {
		int iSettleCompanyMask = 0;
		int iMaskValue = 1;

		for (int i = 0;i < MAX_SETTLE_TYPE;i++) {
			CheckBox chkSettle = (CheckBox)plcSettleList.FindControl(string.Format("chkSettleFlag{0}",i)) as CheckBox;
			if (chkSettle.Checked) {
				iSettleCompanyMask += iMaskValue;
			}
			iMaskValue = iMaskValue << 1;
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MANAGE_COMPANY_MAINTE");
			db.ProcedureInParm("PMANAGE_COMPANY_CD",DbSession.DbType.VARCHAR2,txtManageCompanyCd.Text);
			db.ProcedureInParm("PMANAGE_COMPANY_NM",DbSession.DbType.VARCHAR2,txtManageCompanyNm.Text);
			db.ProcedureInParm("PTEL",DbSession.DbType.VARCHAR2,txtTel.Text);
			db.ProcedureInParm("PMANAGE_PERSON_NM",DbSession.DbType.VARCHAR2,txtManagePersonNm.Text);
			db.ProcedureInParm("PEMAIL_ADDR",DbSession.DbType.VARCHAR2,txtEMailAddr.Text);
			db.ProcedureInParm("PSETTLE_COMPANY_MASK",DbSession.DbType.NUMBER,iSettleCompanyMask);
			db.ProcedureInParm("PCAST_SELF_MINIMUM_PAYMENT",DbSession.DbType.NUMBER,int.Parse(txtCastSelfMinimumPayment.Text));
			db.ProcedureInParm("PCAST_AUTO_MINIMUM_PAYMENT",DbSession.DbType.NUMBER,int.Parse(txtCastAutoMinimumPayment.Text));
			db.ProcedureInParm("PAUTO_GET_TEL_INFO_FLAG",DbSession.DbType.NUMBER,chkAutoGetTelInfoFlag.Checked);
			db.ProcedureInParm("PAD_MANAGE_HOST",DbSession.DbType.VARCHAR2,txtAdManageHost.Text);
			db.ProcedureInParm("PWOMAN_ONLINE_MIN",DbSession.DbType.NUMBER,txtWomanOnlineMin.Text);
			db.ProcedureInParm("PWOMAN_USED_EASY_LOGIN",DbSession.DbType.NUMBER,chkWomanUsedEasyLogin.Checked);
			db.ProcedureInParm("PMAN_USED_EASY_LOGIN",DbSession.DbType.NUMBER,chkManUsedEasyLogin.Checked);
			db.ProcedureInParm("PWOMAN_GET_TERM_ID_FLAG",DbSession.DbType.NUMBER,chkWomanGetTermIdFlag.Checked);
			db.ProcedureInParm("PMAN_GET_TERM_ID_FLAG",DbSession.DbType.NUMBER,chkManGetTermIdFlag.Checked);
			db.ProcedureInParm("PNOT_PAY_EXPIRE_MONTH",DbSession.DbType.NUMBER,int.Parse(txtNotPayExpireMonth.Text));
			db.ProcedureInParm("PCALL_LOCK_TIMEOUT_SEC",DbSession.DbType.NUMBER,int.Parse(txtCallLockTimeoutSec.Text));
			db.ProcedureInParm("PCALL_TIMEOUT_SEC",DbSession.DbType.NUMBER,int.Parse(txtCallTimeoutSec.Text));
			db.ProcedureInParm("PDUP_SUBSCR_SET_DUMMY_FLAG",DbSession.DbType.NUMBER,chkDupSubscrSetDummyFlag.Checked);
			db.ProcedureInParm("PEXEC_MONTHLY_AUTO_PAYMENT_FLG",DbSession.DbType.NUMBER,chkExecMonthlyAutoPaymentFlag.Checked);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		InitPage();
	}

	private void UpdateData2() {
		long iReleaseProgramMask = 0;
		long iReleaseProgramMask2 = 0;

		foreach (GridViewRow gvr in grdReleaseProgramMask.Rows) {
			CheckBox chk = (CheckBox)gvr.FindControl("chkReleaseProgramBit") as CheckBox;
			Label lbl = (Label)gvr.FindControl("lblReleaseProgramID") as Label;
			Label lblSubId = (Label)gvr.FindControl("lblReleaseProgramSubID") as Label;

			if (chk.Checked) {
				if (lblSubId.Text.Equals("1")) {
					iReleaseProgramMask += long.Parse(lbl.Text);
				} else if (lblSubId.Text.Equals("2")) {
					iReleaseProgramMask2 += long.Parse(lbl.Text);
				}
			}
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MANAGE_COMPANY_UPDATE");
			db.ProcedureInParm("PMANAGE_COMPANY_CD",DbSession.DbType.VARCHAR2,txtManageCompanyCd.Text);
			db.ProcedureInParm("PRELEASE_PROGRAM_MASK",DbSession.DbType.NUMBER,iReleaseProgramMask);
			db.ProcedureInParm("PRELEASE_PROGRAM_MASK2",DbSession.DbType.NUMBER,iReleaseProgramMask2);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		InitPage();
	}

	protected void lnkReleaseProgramMask_Command(object sender,CommandEventArgs e) {
		txtManageCompanyCd.Text = e.CommandArgument.ToString();
		GetData();

		pnlMainte.Visible = true;
		pnlDtl.Visible = false;
		pnlDtl2.Visible = true;
		pnlKey.Enabled = false;
	}

	protected void btnUpdate2_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData2();
		}
	}

	protected void btnCancel2_Click(object sender,EventArgs e) {
		InitPage();
	}
}