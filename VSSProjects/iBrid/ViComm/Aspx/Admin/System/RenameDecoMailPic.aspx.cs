﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: デコメール写真一括ファイル名変更

--	Progaram ID		: RenameDecoMailPic
--
--  Creation Date	: 2009.12.14
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.IO;
using System.Text;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class System_RenameDecoMailPic:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
	}


	protected void btnRename_Click(object sender,EventArgs e) {
		DataSet ds = new DataSet();
		string sSiteCd = "";
		string sUserSeq = "";
		string sUserCharNo = "";
		string sLoginId = "";
		string sPicSeq = "";

		using (CastPic oCastPic = new CastPic()) {
			ds = oCastPic.GetAllProfilePic();
		}
		for (int i = 0;i < ds.Tables[0].Rows.Count;i++) {
			sSiteCd = iBridUtil.GetStringValue(ds.Tables[0].Rows[i]["SITE_CD"]);
			sUserSeq = iBridUtil.GetStringValue(ds.Tables[0].Rows[i]["USER_SEQ"]);
			sUserCharNo = iBridUtil.GetStringValue(ds.Tables[0].Rows[i]["USER_CHAR_NO"]);
			sLoginId = iBridUtil.GetStringValue(ds.Tables[0].Rows[i]["LOGIN_ID"]);
			sPicSeq = iBridUtil.GetStringValue(ds.Tables[0].Rows[i]["PIC_SEQ"]);
			FTPUpload(sSiteCd,sUserSeq,sUserCharNo,sLoginId,sPicSeq);
		}
	}

	private void FTPUpload(string pSiteCd,string pUserSeq,string pUserCharNo,string pLoginId,string pFileSeq) {
		string sWebPhisicalDir = "";
		string sMailerIP = "";
		string sMailerFtpId = "";
		string sMailerFtpPw = "";
		string sFileNm;
		using (Site oSite = new Site()) {
			oSite.GetValue(pSiteCd,"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
			oSite.GetValue(pSiteCd,"TX_HI_MAILER_IP",ref sMailerIP);
			oSite.GetValue(pSiteCd,"MAILER_FTP_ID",ref sMailerFtpId);
			oSite.GetValue(pSiteCd,"MAILER_FTP_PW",ref sMailerFtpPw);
		}

		sFileNm = iBridUtil.addZero(pFileSeq,ViCommConst.OBJECT_NM_LENGTH);

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
			string sFullPath = sWebPhisicalDir +
								ViCommConst.PIC_DIRECTRY + "\\" + pSiteCd + "\\Operator\\" + pLoginId + "\\" +
								sFileNm + ViCommConst.PIC_FOODER;
			//
			//SysInterface.FTPUpload(sFullPath,sMailerIP,pSiteCd + "_" + pLoginId + pUserCharNo + ".jpg",sMailerFtpId,sMailerFtpPw);
		}
	}

}
