<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="HolidayList.aspx.cs" Inherits="System_HolidayList" Title="休日設定"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="休日設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 680px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									休日
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtHoliday" runat="server" MaxLength="10"></asp:TextBox>
									<asp:RangeValidator ID="vddHoliday" runat="server" ErrorMessage="休日を正しく入力して下さい。" ControlToValidate="txtHoliday" MaximumValue="2099/12/31" MinimumValue="2000/01/01"
										Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
									<asp:RegularExpressionValidator ID="vdeHoliday" runat="server" ErrorMessage="休日を正しく入力して下さい。" ControlToValidate="txtHoliday" ValidationExpression="\d{4}/\d{2}/\d{2}"
										ValidationGroup="Key" Display="Dynamic">*</asp:RegularExpressionValidator>
									<asp:RequiredFieldValidator ID="vdrHoliday" runat="server" ErrorMessage="休日を入力して下さい。" ControlToValidate="txtHoliday" ValidationGroup="Key">*</asp:RequiredFieldValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[休日設定内容]</legend>
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									備考
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtRemarks" runat="server" MaxLength="80" Width="400px"></asp:TextBox>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[休日一覧]</legend>
			<asp:GridView ID="grdHoliday" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsHoliday" AllowSorting="True" SkinID="GridViewColor">
				<Columns>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkHoliday" runat="server" Text='<%# Eval("HOLIDAY") %>' CommandArgument='<%# Eval("HOLIDAY") %>' OnCommand="lnkHoliday_Command" CausesValidation="False"></asp:LinkButton>
						</ItemTemplate>
						<HeaderTemplate>
							休日
						</HeaderTemplate>
					</asp:TemplateField>
					<asp:BoundField DataField="REMARKS" HeaderText="備考"></asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br>
				<a class="reccount">Current viewing page
					<%=grdHoliday.PageIndex + 1%>
					of
					<%=grdHoliday.PageCount%>
				</a>
				<div class="button">
					<asp:Button ID="btnRegist" runat="server" Text="休日追加" OnClick="btnRegist_Click" />
				</div>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsHoliday" runat="server" SelectMethod="GetPageCollection" TypeName="Holiday" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsHoliday_Selected"></asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vddHoliday" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeHoliday" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrHoliday" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskHoliday" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtHoliday">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
