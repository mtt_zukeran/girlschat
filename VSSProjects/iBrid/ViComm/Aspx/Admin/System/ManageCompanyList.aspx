<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ManageCompanyList.aspx.cs" Inherits="System_ManageCompanyList"
	Title="運営会社設定" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="運営会社設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 400px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									運営会社コード
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtManageCompanyCd" runat="server" MaxLength="12" Width="80px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrManageCompanyCd" runat="server" ErrorMessage="運営会社コードを入力して下さい。" ControlToValidate="txtManageCompanyCd" ValidationGroup="Key">*</asp:RequiredFieldValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[運営会社内容]</legend>
						<table border="0" style="width: 700px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									運営会社名
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtManageCompanyNm" runat="server" MaxLength="30" Width="160px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrManageCompanyNm" runat="server" ErrorMessage="運営会社名を入力して下さい。" ControlToValidate="txtManageCompanyNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle" style="height: 26px;">
									電話番号
								</td>
								<td class="tdDataStyle" style="height: 26px">
									<asp:TextBox ID="txtTel" runat="server" MaxLength="12" Width="160px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrTel" runat="server" ErrorMessage="電話番号を入力して下さい。" ControlToValidate="txtTel" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeTel" runat="server" ErrorMessage="電話番号を正しく入力して下さい。" ValidationExpression="(0\d{9,10})" ControlToValidate="txtTel"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									担当者名
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtManagePersonNm" runat="server" MaxLength="12" Width="160px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrManagePersonNm" runat="server" ErrorMessage="担当者名を入力して下さい。" ControlToValidate="txtManagePersonNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle" style="width: 128px">
									メールアドレス
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtEMailAddr" runat="server" MaxLength="120" Width="160px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrEMailAddr" runat="server" ErrorMessage="メールアドレスを入力して下さい。" ControlToValidate="txtEMailAddr" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeEMailAddr" runat="server" ErrorMessage="メールアドレスを正しく入力して下さい。" ValidationExpression="(\w)+([\w\._-])*@([\w_-])+([\w\._-]+)+"
										ControlToValidate="txtEMailAddr" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									キャスト手動精算<br />
									最低支払額
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCastSelfMinimumPayment" runat="server" MaxLength="5" Width="80px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrCastSelfMinimumPayment" runat="server" ErrorMessage="キャスト手動精算最低支払額" ControlToValidate="txtCastSelfMinimumPayment" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									キャスト月末自動精算<br />
									最低支払額
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCastAutoMinimumPayment" runat="server" MaxLength="5" Width="80px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrCastAutoMinimumPayment" runat="server" ErrorMessage="キャスト月末自動精算最低支払額" ControlToValidate="txtCastAutoMinimumPayment"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									未精算報酬有効月数
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtNotPayExpireMonth" runat="server" MaxLength="2" Width="30px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrNotPayExpireMonth" runat="server" ErrorMessage="未精算報酬有効月数を入力してください。" ControlToValidate="txtNotPayExpireMonth" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RangeValidator ID="vdaNotPayExpireMonth" runat="server" ErrorMessage="未精算報酬有効月数は0〜24の間で入力してください。" ControlToValidate="txtNotPayExpireMonth" MaximumValue="24"
										MinimumValue="0" ValidationGroup="Detail" Type="Integer">*</asp:RangeValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									月末自動精算処理
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkExecMonthlyAutoPaymentFlag" runat="server" Text="月末自動精算処理を行う" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									初回利用時に電話番号を<br />
									自動取得する
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkAutoGetTelInfoFlag" runat="server" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									発呼タイムアウト
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCallTimeoutSec" runat="server" Width="40px" MaxLength="3"></asp:TextBox>秒
									<asp:RequiredFieldValidator ID="vdrCallTimeoutSec" runat="server" ErrorMessage="発呼タイムアウト秒数を入力して下さい。" ControlToValidate="txtCallTimeoutSec" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeCallTimeoutSec" runat="server" ErrorMessage="発呼タイムは2〜3桁の数字で入力して下さい。" ValidationExpression="\d{2,3}" ControlToValidate="txtCallTimeoutSec"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									発呼ロックタイムアウト
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtCallLockTimeoutSec" runat="server" Width="40px" MaxLength="3"></asp:TextBox>秒
									<asp:RequiredFieldValidator ID="vdrCallLockTimeoutSec" runat="server" ErrorMessage="発呼ロックタイムアウト秒数を入力して下さい。" ControlToValidate="txtCallLockTimeoutSec" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeCallLockTimeoutSec" runat="server" ErrorMessage="発呼ロックタイムアウト秒数は2〜3桁の数字で入力して下さい。" ValidationExpression="\d{2,3}"
										ControlToValidate="txtCallLockTimeoutSec" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾛｸﾞｲﾝ済み女性会員
								</td>
								<td class="tdDataStyle">
									最終操作から<asp:TextBox ID="txtWomanOnlineMin" runat="server" MaxLength="3" Width="24px"></asp:TextBox>分後にｵﾌﾗｲﾝに変更する
									<asp:RequiredFieldValidator ID="vdrWomanOnlineMin" runat="server" ErrorMessage="女性会員最終操作から何分後にオフラインにするかを入力してください。" ControlToValidate="txtWomanOnlineMin"
										ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									女性会員簡単ﾛｸﾞｲﾝ
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkWomanUsedEasyLogin" runat="server" Text="iModeID/契約番号(AU,SoftBank)により自動ﾛｸﾞｲﾝを行う" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									女性会員固体識別取得
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkWomanGetTermIdFlag" runat="server" Text="DOCOMO利用の女性会員登録時及びiModeIDの再設定時に固体識別を取得する" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									男性会員簡単ﾛｸﾞｲﾝ
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkManUsedEasyLogin" runat="server" Text="iModeID/契約番号(AU,SoftBank)により自動ﾛｸﾞｲﾝを行う" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									男性会員固体識別取得
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkManGetTermIdFlag" runat="server" Text="DOCOMO利用の男性会員登録時及びiModeIDの再設定時に固体識別を取得する" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ｻﾌﾞｽｸﾗｲﾊﾞNo重複時<br>ﾀﾞﾐｰ設定
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkDupSubscrSetDummyFlag" runat="server" Text="ｻﾌﾞｽｸﾗｲﾊﾞNoの重複が発覚した場合、重複したﾕｰｻﾞｰにﾀﾞﾐｰｻﾌﾞｽｸﾗｲﾊﾞNoを設定する" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									広告管理用HOST名
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtAdManageHost" runat="server" Width="120px" MaxLength="60"></asp:TextBox>
								</td>
							</tr>
						</table>
						<table border="0" style="width: 700px; margin: 6px 0 6px 0" class="tableStyle">
							<asp:PlaceHolder ID="plcSettleList" runat="server">
								<tr>
									<td class="tdHeaderStyle2" rowspan="6">
										表示決済会社
									</td>
									<td>
										<asp:CheckBox ID="chkSettleFlag0" runat="server" />
										現金
									</td>
									<td>
										<asp:CheckBox ID="chkSettleFlag1" runat="server" />
										プリペード
									</td>
									<td>
										<asp:CheckBox ID="chkSettleFlag2" runat="server" />
										クレジットパック
									</td>
									<td>
										<asp:CheckBox ID="chkSettleFlag3" runat="server" />
										C-CHECK
									</td>
								</tr>
								<tr>
									<td>
										<asp:CheckBox ID="chkSettleFlag4" runat="server" />
										BITCASH
									</td>
									<td>
										<asp:CheckBox ID="chkSettleFlag5" runat="server" />
										S-MONEY
									</td>
									<td>
										<asp:CheckBox ID="chkSettleFlag6" runat="server" />
										ｺﾝﾋﾞﾆﾀﾞｳﾝﾛｰﾄﾞ
									</td>
									<td>
										<asp:CheckBox ID="chkSettleFlag7" runat="server" />
										ﾎﾟｲﾝﾄｱﾌﾘｰﾄ
									</td>
								</tr>
								<tr>
									<td>
										<asp:CheckBox ID="chkSettleFlag8" runat="server" />
										Softbank Payment Service
									</td>
									<td>
										<asp:CheckBox ID="chkSettleFlag9" runat="server" />
										ｺﾝﾋﾞﾆﾀﾞｲﾚｸﾄ
									</td>
									<td>
										<asp:CheckBox ID="chkSettleFlag10" runat="server" />
										EDY
									</td>
									<td>
										<asp:CheckBox ID="chkSettleFlag11" runat="server" />
										自動入金
									</td>
								</tr>
								<tr>
									<td>
										<asp:CheckBox ID="chkSettleFlag12" runat="server" />
										G-MONEY
									</td>
									<td>
										<asp:CheckBox ID="chkSettleFlag13" runat="server" />
										後払パック
									</td>
									<td>
										<asp:CheckBox ID="chkSettleFlag14" runat="server" />
										予備02
									</td>
									<td>
										<asp:CheckBox ID="chkSettleFlag15" runat="server" />
										予備03
									</td>
								</tr>
								<tr>
									<td>
										<asp:CheckBox ID="chkSettleFlag16" runat="server" />
										ｸﾚｼﾞｯﾄ従量精算
									</td>
									<td>
										<asp:CheckBox ID="chkSettleFlag17" runat="server" />
										予備04
									</td>
									<td>
										<asp:CheckBox ID="chkSettleFlag18" runat="server" />
										携帯送金
									</td>
									<td>
										<asp:CheckBox ID="chkSettleFlag19" runat="server" />
										クレジット０円決済
									</td>
								</tr>
								<tr>
									<td>
										<asp:CheckBox ID="chkSettleFlag20" runat="server" />
										予備05
									</td>
									<td>
										<asp:CheckBox ID="chkSettleFlag21" runat="server" />
										GIGA POINT
									</td>
									<td>
										<asp:CheckBox ID="chkSettleFlag22" runat="server" />
										楽天決済			
									</td>
									<td>
									</td>
								</tr>
							</asp:PlaceHolder>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl2">
					<fieldset class="fieldset-inner">
						<legend>[リリース機能マスク]</legend>
						<asp:GridView ID="grdReleaseProgramMask" runat="server" AutoGenerateColumns="False" ShowHeader="True" SkinID="GridViewColor">
							<Columns>
								<asp:TemplateField>
									<ItemTemplate>
										<asp:CheckBox ID="chkReleaseProgramBit" runat="server" Checked='<%# (bool)Eval("RELEASE_PROGRAM_BIT") %>' Text='<%# Eval("RELEASE_PROGRAM_SUMMARY") %>' />
										<asp:Label ID="lblReleaseProgramID" runat="server" Text='<%# Eval("RELEASE_PROGRAM_ID") %>' Visible="False"></asp:Label>
										<asp:Label ID="lblReleaseProgramSubID" runat="server" Text='<%# Eval("RELEASE_PROGRAM_SUB_ID") %>' Visible="False"></asp:Label>
									</ItemTemplate>
									<HeaderTemplate>
										リリース機能
									</HeaderTemplate>
									<ItemStyle Width="400px" />
								</asp:TemplateField>
								<asp:BoundField DataField="REQUEST_COMPANY" HeaderText="依頼元"></asp:BoundField>
							</Columns>
						</asp:GridView>
						<asp:Button runat="server" ID="btnUpdate2" Text="更新" CssClass="seekbutton" OnClick="btnUpdate2_Click" ValidationGroup="Detail2" />
						<asp:Button runat="server" ID="btnCancel2" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel2_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[運営会社一覧]</legend>
			<asp:GridView ID="grdCompany" runat="server" AllowPaging="True" DataSourceID="dsManageCompany" AllowSorting="True" SkinID="GridViewColor" AutoGenerateColumns="False">
				<Columns>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkAdminId" runat="server" Text='<%# Eval("MANAGE_COMPANY_CD") %>' CommandArgument='<%# Eval("MANAGE_COMPANY_CD") %>' OnCommand="lnkAdminId_Command"
								CausesValidation="False"></asp:LinkButton>
						</ItemTemplate>
						<HeaderTemplate>
							運営会社コード
						</HeaderTemplate>
					</asp:TemplateField>
					<asp:BoundField DataField="MANAGE_COMPANY_NM" HeaderText="運営会社名"></asp:BoundField>
					<asp:BoundField DataField="TEL" HeaderText="電話番号"></asp:BoundField>
					<asp:BoundField DataField="MANAGE_PERSON_NM" HeaderText="担当者名"></asp:BoundField>
					<asp:BoundField DataField="EMAIL_ADDR" HeaderText="メールアドレス"></asp:BoundField>
					<asp:TemplateField HeaderText="リリース機能マスク">
						<EditItemTemplate>
							<asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("RELEASE_PROGRAM_MASK") %>'></asp:TextBox>
						</EditItemTemplate>
						<ItemTemplate>
							<asp:LinkButton ID="lnkReleaseProgramMask" runat="server" OnCommand="lnkReleaseProgramMask_Command" CommandArgument='<%# Eval("MANAGE_COMPANY_CD") %>'
								Text='リリース機能設定'></asp:LinkButton>
						</ItemTemplate>
						<HeaderTemplate>
							リリース機能マスク
						</HeaderTemplate>
					</asp:TemplateField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<a class="reccount">Record Count
				<%#GetRecCount() %>
			</a>
			<br>
			<a class="reccount">Current viewing page
				<%=grdCompany.PageIndex + 1%>
				of
				<%=grdCompany.PageCount%>
			</a>
			<div class="button">
				<asp:Button ID="btnRegist" runat="server" Text="運営会社追加" OnClick="btnRegist_Click" />
			</div>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsManageCompany" runat="server" SelectMethod="GetPageCollection" SelectCountMethod="GetPageCount" OnSelected="dsManageCompany_Selected"
		EnablePaging="True" TypeName="ManageCompany"></asp:ObjectDataSource>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtCastSelfMinimumPayment" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtCastAutoMinimumPayment" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtWomanOnlineMin" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender21" TargetControlID="vdrWomanOnlineMin" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrManageCompanyCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrManageCompanyNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrTel" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdeTel" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdrManagePersonNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdrEMailAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vdeEMailAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8" TargetControlID="vdrCastSelfMinimumPayment" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9" TargetControlID="vdrCastAutoMinimumPayment" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender33" TargetControlID="vdrNotPayExpireMonth" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender34" TargetControlID="vdaNotPayExpireMonth" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10" TargetControlID="vdrCallTimeoutSec" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender22" TargetControlID="vdeCallTimeoutSec" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender23" TargetControlID="vdrCallLockTimeoutSec" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender24" TargetControlID="vdeCallLockTimeoutSec" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender3" runat="server" TargetControlID="btnUpdate2" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
