﻿/*************************************************************************
--	System			: ViComm Admin
--	Sub System Name	: Admin
--	Title			: アフリエーター設定
--	Progaram ID		: AdManage_AffiliaterList
--
--  Creation Date	: 2009.11.06
--  Creater			: iBrid(S.Ohtahara)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class System_AffiliaterList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	protected void dsAffiliater_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		grdAffiliater.PageSize = 50;
		grdAffiliater.PageIndex = 0;
		txtAffiliaterCd.Text = "";
		ClearField();
		pnlMainte.Visible = false;
		DataBind();
	}

	private void ClearField() {
		txtAffiliaterNm.Text = "";
		txtAsInfoPrefix.Text = "";
		txtTrackingUrl.Text = "";
		chkTrackingHttpFlag.Checked = false;
		txtAddOnInfo.Text = "";

	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
		InitPage();
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		pnlMainte.Visible = true;
		pnlKey.Enabled = true;
		pnlDtl.Visible = false;
	}

	protected void lnkAffiliaterCd_Command(object sender,CommandEventArgs e) {
		txtAffiliaterCd.Text = e.CommandArgument.ToString();
		GetData();
	}

	protected bool UrlCheck(string sUrl) {
		if (sUrl.IndexOf("\n") >= 0) {
			return false;
		} else {
			return true;
		}
	}

	/// <summary>
	/// 選択している認証送信方式により、必須入力項目をチェック
	/// </summary>
	/// <param name="source"></param>
	/// <param name="args"></param>
	protected void vdcAuthResultTransType_ServerValidate(object source,ServerValidateEventArgs args) {
		int iAuthResultTransType = int.Parse(lstAuthResultTransType.SelectedValue);
		args.IsValid = true;

		// 選択している認証送信方式
		switch (iAuthResultTransType) {
			// JavaScriptタグによる送信
			case ViCommConst.AUTH_AFFILIATE_TRASN_JS_TAG:
				vdrTrackingUrl.Enabled = false;
				vdrTrackingJs.Enabled = true;
				txtTrackingJs.Text = txtTrackingJs.Text.Trim();
				// ﾄﾗｯｷﾝｸﾞJavaScriptが未入力
				if (string.IsNullOrEmpty(txtTrackingJs.Text)) {
					vdrTrackingJs.IsValid = false;
				}
				break;
			// Httpリクエストによる送信、Imageタグによる送信
			case ViCommConst.AUTH_AFFILIATE_TRASN_HTTP:
			case ViCommConst.AUTH_AFFILIATE_TRASN_IMAGE_TAG:
			default:
				vdrTrackingUrl.Enabled = true;
				vdrTrackingJs.Enabled = false;
				txtTrackingUrl.Text = txtTrackingUrl.Text.Trim();
				// ﾄﾗｯｷﾝｸﾞURLが未入力
				if (string.IsNullOrEmpty(txtTrackingUrl.Text)) {
					vdrTrackingUrl.IsValid = false;
				}
				break;
		}
	}
	protected void vdcTrackingUrl_ServerValidate(object source,ServerValidateEventArgs args) {
		args.IsValid = UrlCheck(txtTrackingUrl.Text);
	}
	protected void vdcAsInfoPrefix_ServerValidate(object source,ServerValidateEventArgs args) {
		if (System.Text.Encoding.GetEncoding(65001).GetByteCount(txtAsInfoPrefix.Text) <= txtAsInfoPrefix.MaxLength) {
			args.IsValid = true;
		} else {
			args.IsValid = false;
		}

	}

	//必須になった場合には開放
	//protected void vdcAsInfoSubPrefix_ServerValidate(object source,ServerValidateEventArgs args) {
	//    if (System.Text.Encoding.GetEncoding(65001).GetByteCount(txtAsInfoSubPrefix.Text) <= txtAsInfoSubPrefix.MaxLength) {
	//        args.IsValid = true;
	//    } else {
	//        args.IsValid = false;
	//    }
	//}

	//protected void vdcAsInfoSubValue_ServerValidate(object source,ServerValidateEventArgs args) {
	//    if (System.Text.Encoding.GetEncoding(65001).GetByteCount(txtAsInfoSubValue.Text) <= txtAsInfoSubValue.MaxLength) {
	//        args.IsValid = true;
	//    } else {
	//        args.IsValid = false;
	//    }
	//}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("AFFILIATER_GET");
			db.ProcedureInParm("PAFFILIATER_CD",DbSession.DbType.VARCHAR2,txtAffiliaterCd.Text);
			db.ProcedureOutParm("PAFFILIATER_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PAS_INFO_PREFIX",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PAS_INFO_SUB_PREFIX",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PAS_INFO_SUB_VALUE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PTRACKING_URL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PTRACKING_HTTP_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PADD_ON_INFO",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PTRACKING_JS",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PAUTH_RESULT_TRANS_TYPE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtAffiliaterCd.Text = db.GetStringValue("PAFFILIATER_CD");
				txtAffiliaterNm.Text = db.GetStringValue("PAFFILIATER_NM");
				txtAsInfoPrefix.Text = db.GetStringValue("PAS_INFO_PREFIX");
				txtAsInfoSubPrefix.Text = db.GetStringValue("PAS_INFO_SUB_PREFIX");
				txtAsInfoSubValue.Text = db.GetStringValue("PAS_INFO_SUB_VALUE");
				txtTrackingUrl.Text = db.GetStringValue("PTRACKING_URL");
				chkTrackingHttpFlag.Checked = (db.GetStringValue("PTRACKING_HTTP_FLAG").Equals("0") == false);
				txtAddOnInfo.Text = db.GetStringValue("PADD_ON_INFO");
				txtTrackingJs.Text = db.GetStringValue("PTRACKING_JS");
				lstAuthResultTransType.SelectedValue = db.GetStringValue("PAUTH_RESULT_TRANS_TYPE");
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;

	}

	private void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("AFFILIATER_MAINTE");
			db.ProcedureInParm("PAFFILIATER_CD",DbSession.DbType.VARCHAR2,txtAffiliaterCd.Text);
			db.ProcedureInParm("PAFFILIATER_NM",DbSession.DbType.VARCHAR2,txtAffiliaterNm.Text);
			db.ProcedureInParm("PAS_INFO_PREFIX",DbSession.DbType.VARCHAR2,txtAsInfoPrefix.Text);
			db.ProcedureInParm("PAS_INFO_SUB_PREFIX",DbSession.DbType.VARCHAR2,txtAsInfoSubPrefix.Text);
			db.ProcedureInParm("PAS_INFO_SUB_VALUE",DbSession.DbType.VARCHAR2,txtAsInfoSubValue.Text);
			db.ProcedureInParm("PTRACKING_URL",DbSession.DbType.VARCHAR2,txtTrackingUrl.Text);
			db.ProcedureInParm("PTRACKING_HTTP_FLAG",DbSession.DbType.NUMBER,chkTrackingHttpFlag.Checked);
			db.ProcedureInParm("PADD_ON_INFO",DbSession.DbType.VARCHAR2,txtAddOnInfo.Text);
			db.ProcedureInParm("PTRACKING_JS",DbSession.DbType.VARCHAR2,txtTrackingJs.Text);
			db.ProcedureInParm("PAUTH_RESULT_TRANS_TYPE",DbSession.DbType.NUMBER,int.Parse(lstAuthResultTransType.SelectedValue));
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		InitPage();
	}
}

