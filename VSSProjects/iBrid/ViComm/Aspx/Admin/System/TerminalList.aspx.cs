﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 端末メンテナンス
--	Progaram ID		: TerminalList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class System_TerminalList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	protected void dsTerminal_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		grdTerminal.PageSize = 999;
		txtTerminalIPAddr.Text = "";
		ClearField();
		pnlMainte.Visible = false;
		DataBind();
	}

	private void ClearField() {
		lstTerminalType.SelectedIndex = 0;
		txtUri.Text = "";
		txtRemarks.Text = "";
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		pnlMainte.Visible = true;
		pnlKey.Enabled = true;
		pnlDtl.Visible = false;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void lnkTerminalIpAddr_Command(object sender,CommandEventArgs e) {
		txtTerminalIPAddr.Text = e.CommandArgument.ToString();
		GetData();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void vdcUri_ServerValidate(object source,ServerValidateEventArgs args) {
		using (Terminal oTerminal = new Terminal()) {
			args.IsValid = oTerminal.IsUniqueUri(txtUri.Text,txtTerminalIPAddr.Text);
		}
	}

	private void GetList() {
		grdTerminal.DataBind();
		pnlCount.DataBind();
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("TERMINAL_GET");
			db.ProcedureInParm("PTERMINAL_IP_ADDR",DbSession.DbType.VARCHAR2,txtTerminalIPAddr.Text);
			db.ProcedureOutParm("PTERMINAL_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PURI",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREMARKS",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				lstTerminalType.SelectedValue = db.GetStringValue("PTERMINAL_TYPE");
				txtUri.Text = db.GetStringValue("PURI");
				txtRemarks.Text = db.GetStringValue("PREMARKS");
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;

	}

	private void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("TERMINAL_MAINTE");
			db.ProcedureInParm("PTERMINAL_IP_ADDR",DbSession.DbType.VARCHAR2,txtTerminalIPAddr.Text);
			db.ProcedureInParm("PTERMINAL_TYPE",DbSession.DbType.VARCHAR2,lstTerminalType.SelectedValue);
			db.ProcedureInParm("PURI",DbSession.DbType.VARCHAR2,txtUri.Text);
			db.ProcedureInParm("PREMARKS",DbSession.DbType.VARCHAR2,txtRemarks.Text);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		InitPage();
	}
}
