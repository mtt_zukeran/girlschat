﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SalesByEntMonthlyList.aspx.cs" Inherits="AdManage_SalesByEntMonthlyList" Title="アクセスログ"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="アクセスログ"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
                            <td class="tdHeaderStyle">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                表示月
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstYear" runat="server" Width="60px">
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstMonth" runat="server" Width="40px">
                                </asp:DropDownList>月
                                <br />
								<asp:Label ID="lblErrorMessageReportDayFrom" runat="server" ForeColor="red" Visible="false"></asp:Label>
                            </td>
                        </tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
			</asp:Panel>
		</fieldset>
		<fieldset>
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="560px">
				<asp:GridView ID="grdSalesByEntMonthly" runat="server" AutoGenerateColumns="False" DataSourceID="dsSalesByEntMonthly" CellPadding="0" ShowFooter="True" AllowSorting="True"
					OnRowDataBound="grdSalesByEntMonthly_RowDataBound" SkinID="GridViewColorNoRowStyle">
					<Columns>
						<asp:TemplateField HeaderText="入会月" FooterText="合計">
							<ItemStyle HorizontalAlign="Center" />
							<ItemTemplate>
								<asp:HyperLink ID="lnkEntMonth" runat="server" NavigateUrl='<%# string.Format("~/AdManage/SalesByEntDailyList.aspx?sitecd={0}&entmonth={1}&year={2}&month={3}",lstSiteCd.SelectedValue,Eval("ENT_MONTH"),lstYear.SelectedValue,lstMonth.SelectedValue) %>' Text='<%# GetDisplayEntMonth(Eval("ENT_MONTH").ToString()) %>' ></asp:HyperLink>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="売上">
							<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
							<ItemTemplate>
								<asp:Label ID="Label2" runat="server" Text='<%# Eval("RECEIPT_AMT") %>' BackColor=''></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="件数">
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("RECEIPT_COUNT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
						</asp:TemplateField>
					</Columns>
					<FooterStyle ForeColor="Black" BackColor="LightYellow" />
				</asp:GridView>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsSalesByEntMonthly" runat="server" SelectMethod="GetSalesList" TypeName="SalesByEntMonthly" OnSelecting="dsSalesByEntMonthly_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
</asp:Content>
