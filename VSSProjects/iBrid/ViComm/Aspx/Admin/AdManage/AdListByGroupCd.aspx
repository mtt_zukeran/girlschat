<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AdListByGroupCd.aspx.cs" Inherits="AdManage_AdListByGroupCd"
	Title="広告グループ別広告コード" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="広告グループ別広告コード"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<table border="0" style="width: 800px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle">
						サイトコード
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px" AutoPostBack="True"
							OnSelectedIndexChanged="lstSiteCd_SelectedIndexChanged">
						</asp:DropDownList>
					</td>
					<td class="tdHeaderStyle">
						広告グループ
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstAdGroup" runat="server" DataSourceID="dsAdGroup" DataTextField="AD_GROUP_NM" DataValueField="AD_GROUP_CD" Width="240px">
						</asp:DropDownList>
					</td>
				</tr>
				<tr>
					<td class="tdHeaderStyle">
						広告名
					</td>
					<td class="tdDataStyle">
						<asp:TextBox ID="txtAdNm" runat="server" MaxLength="80" Width="150px"></asp:TextBox>
					</td>
					<td class="tdHeaderStyle2">
						広告発行日
					</td>
					<td class="tdDataStyle">
						<asp:TextBox ID="txtIssueDayFrom" runat="server" MaxLength="10" Width="62px"></asp:TextBox>&nbsp;〜
						<asp:TextBox ID="txtIssueDayTo" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
						<asp:RangeValidator ID="vdrIssueDayFrom" runat="server" ErrorMessage="広告発行日Fromを正しく入力して下さい。" ControlToValidate="txtIssueDayFrom" MaximumValue="2099/12/31"
							MinimumValue="1990/01/01" Type="Date" ValidationGroup="Detail" Display="Dynamic">*</asp:RangeValidator>
						<asp:RangeValidator ID="vdrIssueDayTo" runat="server" ErrorMessage="広告発行日Toを正しく入力して下さい。" ControlToValidate="txtIssueDayTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
							Type="Date" ValidationGroup="Detail" Display="Dynamic">*</asp:RangeValidator>
						<asp:CompareValidator ID="vdcIssueDayFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtIssueDayFrom" ControlToValidate="txtIssueDayTo"
							Operator="GreaterThanEqual" ValidationGroup="Detail">*</asp:CompareValidator>
					</td>
				</tr>
				<tr>
					<td class="tdHeaderStyle2">
						Paging Off
					</td>
					<td class="tdDataStyle">
						<asp:CheckBox ID="chkPagingOff" runat="server" />
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" ValidationGroup="Detail" OnClick="btnListSeek_Click" />
			<asp:Button runat="server" ID="btnCsv" Text="CSV出力" CssClass="seekbutton" ValidationGroup="Detail" OnClick="btnCsv_Click" />
		</fieldset>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset class="fieldset">
				<legend>[広告コード一覧]</legend>
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="500px">
					<asp:GridView ID="grdAd" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsAd" AllowSorting="True" SkinID="GridViewColor">
						<Columns>
							<asp:BoundField DataField="AD_CD" HeaderText="広告コード" SortExpression="AD_CD">
								<ItemStyle HorizontalAlign="Left" />
							</asp:BoundField>
							<asp:BoundField DataField="AD_NM" HeaderText="広告名" SortExpression="AD_NM">
								<ItemStyle HorizontalAlign="Left" />
							</asp:BoundField>
							<asp:BoundField DataField="ISSUE_DATE" HeaderText="発行日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False" SortExpression="ISSUE_DATE">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
							<asp:BoundField DataField="UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False" SortExpression="UPDATE_DATE">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
				&nbsp;
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count
						<%#GetRecCount() %>
					</a>
					<br>
					<a class="reccount">Current viewing page
						<%=grdAd.PageIndex + 1%>
						of
						<%=grdAd.PageCount%>
					</a>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrIssueDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrIssueDayTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdcIssueDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskIssueDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtIssueDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskIssueDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtIssueDayTo">
	</ajaxToolkit:MaskedEditExtender>
	<asp:ObjectDataSource ID="dsAd" runat="server" SelectMethod="GetPageCollectionBySiteGroupCd" TypeName="Ad" SelectCountMethod="GetPageCountBySiteGroupCd"
		EnablePaging="True" OnSelected="dsAd_Selected" OnSelecting="dsAd_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pAdGroupCd" Type="String" />
			<asp:Parameter Name="pAdNm" Type="String" />
			<asp:Parameter Name="pIssueDateFrom" Type="String" />
			<asp:Parameter Name="pIssueDateTo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsAdGroup" runat="server" SelectMethod="GetList" TypeName="AdGroup" OnSelecting="dsAdGroup_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
