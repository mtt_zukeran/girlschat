﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 広告別サービスポイント割当
--	Progaram ID		: SiteAdPointList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class AdManage_SiteAdPointList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
	}

	private void FirstLoad() {
		grdAd.PageSize = int.Parse(Session["PageSize"].ToString());
		grdAd.DataSourceID = "";
		DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void InitPage() {
		lstSiteCd.SelectedIndex = 0;
		txtAdCd.Text = "";
		ClearField();
		pnlMainte.Visible = false;
	}

	private void ClearField() {
		txtRegistServicePoint.Text = "";
		txtApplyEndDay.Text = "";
		txtApplyStartDay.Text = "";
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	private void GetList() {
		grdAd.DataSourceID = "dsAd";
		grdAd.PageIndex = 0;
		grdAd.DataBind();
		pnlCount.DataBind();
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		if (IsValid) {
			GetData();
		}
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		pnlMainte.Visible = true;
		pnlKey.Enabled = true;
		pnlDtl.Visible = false;
		lstSiteCd.SelectedIndex = lstSeekSiteCd.SelectedIndex;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void lnkAdCd_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		if (sKeys[0].Equals("")) {
			lstSiteCd.SelectedValue = lstSeekSiteCd.SelectedValue;
		} else {
			lstSiteCd.SelectedValue = sKeys[0];
		}
		txtAdCd.Text = sKeys[1];
		GetData();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void vdcAdCd_ServerValidate(object source,ServerValidateEventArgs args) {
		using (Ad oAd = new Ad()) {
			string sAdNm = "";
			args.IsValid = oAd.IsExist(txtAdCd.Text,ref sAdNm);
		}
	}

	protected void dsAd_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsAdGroup_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
	}

	protected void dsAd_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
		e.InputParameters[1] = txtSeekAdCd.Text;
		e.InputParameters[2] = txtSeekAdNm.Text;
		e.InputParameters[3] = txtSeekDay.Text;
		e.InputParameters[4] = chkNoAssign.Checked;
	}


	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SITE_AD_POINT_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PAD_CD",DbSession.DbType.VARCHAR2,txtAdCd.Text);
			db.ProcedureOutParm("PREGIST_SERVICE_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PAPPLY_START_DAY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PAPPLY_END_DAY",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PAD_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			lblAdNm.Text = db.GetStringValue("PAD_NM");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtRegistServicePoint.Text = db.GetStringValue("PREGIST_SERVICE_POINT");
				txtApplyStartDay.Text = db.GetStringValue("PAPPLY_START_DAY");
				txtApplyEndDay.Text = db.GetStringValue("PAPPLY_END_DAY");
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		lstSeekSiteCd.SelectedIndex = lstSiteCd.SelectedIndex;
		pnlKey.Enabled = false;
	}

	private void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SITE_AD_POINT_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PAD_CD",DbSession.DbType.VARCHAR2,txtAdCd.Text);
			db.ProcedureInParm("PREGIST_SERVICE_POINT",DbSession.DbType.NUMBER,int.Parse(txtRegistServicePoint.Text));
			db.ProcedureInParm("PAPPLY_START_DAY",DbSession.DbType.VARCHAR2,txtApplyStartDay.Text);
			db.ProcedureInParm("PAPPLY_END_DAY",DbSession.DbType.VARCHAR2,txtApplyEndDay.Text);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		int iIdx = lstSiteCd.SelectedIndex;
		InitPage();
		GetList();
		lstSeekSiteCd.SelectedIndex = iIdx;
	}
}
