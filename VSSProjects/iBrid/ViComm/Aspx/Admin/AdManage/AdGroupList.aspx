<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AdGroupList.aspx.cs" Inherits="AdManage_AdGroupList" Title="広告グループ設定"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="広告グループ設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2" style="height: 26px">
									サイトコード
								</td>
								<td class="tdDataStyle" style="height: 26px">
									<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
									</asp:DropDownList>
								</td>
								<td class="tdHeaderStyle2" style="height: 26px">
									広告グループ
								</td>
								<td class="tdDataStyle" style="height: 26px">
									<asp:TextBox ID="txtAdGroupCd" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrAdGroupCd" runat="server" ErrorMessage="広告グループを入力して下さい。" ControlToValidate="txtAdGroupCd" ValidationGroup="Key">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeAdGroupCd" runat="server" ErrorMessage="広告グループは10桁以下の英数字で入力して下さい。" ValidationExpression="\w{1,10}" ControlToValidate="txtAdGroupCd"
										ValidationGroup="Key">*</asp:RegularExpressionValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[広告グループ内容]</legend>
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									広告グループ名称
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtAdGroupNm" runat="server" MaxLength="30" Width="200px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrAdGroupNm" runat="server" ErrorMessage="広告グループ名称を入力して下さい。" ControlToValidate="txtAdGroupNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									カラーパターン
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstWebFaceSeq" runat="server" Width="206px" DataSourceID="dsWebFace" DataTextField="HTML_FACE_NAME" DataValueField="WEB_FACE_SEQ">
									</asp:DropDownList>
								</td>
							</tr>
							<tr id="trPrepaidAdGroup" runat="server">
								<td class="tdHeaderStyle2">
									プリペードグループ
								</td>
								<td class="tdDataStyle">
									<asp:CheckBox ID="chkPrepaidAdGroupFlag" runat="server" />
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[広告グループ一覧]</legend>
            <table border="0" style="width: 600px" class="tableStyle">
                <tr>
                    <td class="tdHeaderStyle2">
                        サイトコード
                    </td>
                    <td class="tdDataStyle">
                        <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                            DataValueField="SITE_CD" Width="240px">
                        </asp:DropDownList>
                    </td>
                    <td class="tdHeaderStyle2" id="tdHeaderPreapidAdGroup" runat="server">
                        プリペードグループ
                    </td>
                    <td class="tdDataStyle" id="tdDataPrepaidAdGroup" runat="server">
                        <asp:CheckBox ID="chkSeekPrepaidAdGroupFlag" runat="server" />
                    </td>
                </tr>
            </table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnRegist" Text="グループ追加" CssClass="seekbutton" CausesValidation="False" OnClick="btnRegist_Click" />
			<br />
			<br />
			<asp:GridView ID="grdAdGroup" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsAdGroup" AllowSorting="True" SkinID="GridViewColor">
				<Columns>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkAdGroupCd" runat="server" Text='<%# string.Format("{0} {1}",Eval("AD_GROUP_CD"),Eval("AD_GROUP_NM")) %>' CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("AD_GROUP_CD"))  %>'
								OnCommand="lnkAdGroupCd_Command" CausesValidation="False">
							</asp:LinkButton>
						</ItemTemplate>
						<HeaderTemplate>
							広告グループ
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:BoundField DataField="HTML_FACE_NAME" HeaderText="カラーパターン">
						<ItemStyle HorizontalAlign="Left" />
					</asp:BoundField>
					<asp:BoundField DataField="UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br />
				<a class="reccount">Current viewing page
					<%=grdAdGroup.PageIndex + 1%>
					of
					<%=grdAdGroup.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsAdGroup" runat="server" SelectMethod="GetPageCollection" TypeName="AdGroup" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsAdGroup_Selected" OnSelecting="dsAdGroup_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
            <asp:Parameter Name="pPrepaidAdGroupFlag" Type="String" DefaultValue="" />
        </SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsWebFace" runat="server" SelectMethod="GetList" TypeName="WebFace"></asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrAdGroupCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeAdGroupCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrAdGroupNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
