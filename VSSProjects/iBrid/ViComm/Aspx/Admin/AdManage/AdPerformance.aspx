<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AdPerformance.aspx.cs" Inherits="AdManage_AdPerformance" Title="広告コード別稼動集計"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="広告コード別稼動集計"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 732px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="232px" AutoPostBack="True"
								OnSelectedIndexChanged="lstSiteCd_SelectedIndexChanged">
							</asp:DropDownList>
						</td>
					</tr>
					<asp:PlaceHolder ID="plcDisplayAdColumn" runat="server">
						<tr>
							<td class="tdHeaderStyle">
								代理店向け報告数表示
							</td>
							<td class="tdDataStyle">
								<asp:CheckBox ID="chkDisplayAdColumn" runat="server" Text="代理店向け報告数を表示する">
								</asp:CheckBox>
							</td>
						</tr>
					</asp:PlaceHolder>
					<tr>
						<td class="tdHeaderStyle">
							集計開始日
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstFromYYYY" runat="server" Width="56px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstFromMM" runat="server" Width="40px">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstFromDD" runat="server" Width="40px">
							</asp:DropDownList>日
							<asp:CustomValidator runat="server" ID="vdcFrom" ErrorMessage="集計開始日が不正です。" OnServerValidate="vdcFrom_ServerValidate" ValidationGroup="Seek"></asp:CustomValidator>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							集計終了日
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstToYYYY" runat="server" Width="56px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstToMM" runat="server" Width="40px">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstToDD" runat="server" Width="40px">
							</asp:DropDownList>日
							<asp:CustomValidator ID="vdcTo" runat="server" ErrorMessage="集計終了日が不正です。" OnServerValidate="vdcTo_ServerValidate" ValidationGroup="Seek"></asp:CustomValidator>
						</td>
					</tr>
					<asp:PlaceHolder ID="plcAdCd" runat="server">
						<tr>
							<td class="tdHeaderStyle">
								広告グループ
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstAdGroup" runat="server" DataSourceID="dsAdGroup" DataTextField="AD_GROUP_NM" DataValueField="AD_GROUP_CD" Width="232px" OnDataBound="lstAdGroup_DataBound">
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								広告コード
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtAdCd" runat="server" MaxLength="32" Width="232px"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								集計方法
							</td>
							<td class="tdDataStyle">
								<asp:RadioButtonList ID="rdoSummaryType" RepeatDirection="horizontal" runat="server">
									<asp:ListItem Text="広告コード別" Value="1"></asp:ListItem>
									<asp:ListItem Text="広告グループコード別" Value="2"></asp:ListItem>
								</asp:RadioButtonList>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle2">
								利用状況
							</td>
							<td class="tdDataStyle">
								<asp:RadioButtonList ID="rdoUsedFlag" runat="server" RepeatDirection="horizontal">
									<asp:ListItem Text="すべて" Value="" Selected></asp:ListItem>
									<asp:ListItem Text="停止中" Value="0"></asp:ListItem>
									<asp:ListItem Text="利用中" Value="1"></asp:ListItem>
								</asp:RadioButtonList>
							</td>
						</tr>
					</asp:PlaceHolder>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Seek" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
				<asp:Button runat="server" ID="btnCsv" Text="ＣＳＶ出力" CssClass="seekbutton" OnClick="btnCSV_Click" />
				<asp:CustomValidator ID="vdcFromTo" runat="server" ErrorMessage="集計期間の大小関係が不正です。" OnServerValidate="vdcFromTo_ServerValidate" ValidationGroup="Seek"></asp:CustomValidator>
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[稼動集計]</legend>
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="560px">
					<asp:GridView ID="grdAd" runat="server" AutoGenerateColumns="False" DataSourceID="dsAccessPage" ShowFooter="True" OnRowDataBound="grdAd_RowDataBound" SkinID="GridViewColor"
						OnSorting="grdAd_Sorting" OnSorted="grdAd_Sorted" AllowSorting="True">
						<Columns>
							<asp:TemplateField HeaderText="ｺｰﾄﾞ" SortExpression="AD_CD">
								<HeaderStyle CssClass="HeaderStyle" Font-Size="Small" />
								<ItemStyle HorizontalAlign="Left" Font-Size="Small" Wrap="false" />
								<ItemTemplate>
									<asp:HyperLink ID="lnkAdCd" runat="server" NavigateUrl='<%# string.Format("AdPerformanceByDay.aspx?sitecd={0}&adcd={1}&yy={2}&mm={3}&sexcd={4}",Eval("SITE_CD"),Eval("AD_CD"),lstFromYYYY.SelectedValue,lstFromMM.SelectedValue,this.SexCd) %>'
										Text='<%# Eval("AD_CD") %>' Visible='<%# (!IsAdManager) %>'></asp:HyperLink>
									<asp:Label ID="lblAdCd" runat="server" Text='<%# Eval("AD_CD") %>' Visible='<%# IsAdManager %>'>
									</asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Font-Size="Small" />
							</asp:TemplateField>
							<asp:BoundField DataField="AD_NM" HeaderText="広告名" FooterText="&lt;合計&gt;" SortExpression="AD_NM">
								<ItemStyle HorizontalAlign="Left" Font-Size="Small" Wrap="false" />
								<FooterStyle HorizontalAlign="Right" Font-Size="Small" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="広告ｸﾞﾙｰﾌﾟ" SortExpression="AD_GROUP_CD">
								<HeaderStyle CssClass="HeaderStyle" Font-Size="Small" />
								<ItemStyle Font-Size="Small" HorizontalAlign="Left" Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblAdGroupCd" runat="server" Text='<%# Eval("AD_GROUP_CD") %>'>
									</asp:Label>
								</ItemTemplate>
								<FooterStyle Font-Size="Small" HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:BoundField DataField="AD_GROUP_NM" FooterText="&lt;合計&gt;" HeaderText="広告ｸﾞﾙｰﾌﾟ名" SortExpression="AD_GROUP_NM">
								<ItemStyle Font-Size="Small" HorizontalAlign="Left" Wrap="false" />
								<FooterStyle Font-Size="Small" HorizontalAlign="Right" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="ｱｸｾｽ数" SortExpression="ACCESS_COUNT_AD">
								<HeaderStyle CssClass="HeaderStyle" Font-Size="Small" Wrap="false" />
								<ItemStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblAccessCount" runat="server" Text='<%# Eval("ACCESS_COUNT_AD") %>'>
									</asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Font-Size="Small" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="登録数" SortExpression="REGIST_COUNT">
								<HeaderStyle CssClass="HeaderStyle" Font-Size="Small" Wrap="false" />
								<ItemStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblRegistCountRaw" runat="server" Text='<%# Eval("REGIST_COUNT") %>'>
									</asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Font-Size="Small" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="登録数" SortExpression="REGIST_COUNT_AD">
								<HeaderStyle CssClass="HeaderStyle" Font-Size="Small" Wrap="false" />
								<ItemStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
								<ItemTemplate>
									<asp:HyperLink ID="lnkRegistCount" runat="server" Text='<%#  Eval("REGIST_COUNT_AD") %>' Visible='<%# !IsAdManager %>'>
									</asp:HyperLink>
									<asp:Label ID="lblRegistCount" runat="server" Text='<%# Eval("REGIST_COUNT_AD") %>' Visible='<%# IsAdManager %>'>
									</asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Font-Size="Small" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="退会数">
								<ItemTemplate>
									<asp:Label ID="lblWithdrwalCount" runat="server" Text='<%# Eval("WITHDRAWAL_COUNT") %>' Enabled='<%# IsAdManager %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="再登録数">
								<ItemTemplate>
									<asp:Label ID="lblReRegistCount" runat="server" Text='<%# Eval("RE_REGIST_COUNT") %>' Enabled='<%# IsAdManager %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="登録ﾒｰﾙ不達数" SortExpression="REGIST_MAIL_SEND_FAILED_COUNT" Visible="false">
								<HeaderStyle CssClass="HeaderStyle" Font-Size="Small" Wrap="false" />
								<ItemStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
								<ItemTemplate>
									<%# Eval("REGIST_MAIL_SEND_FAILED_COUNT") %>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Font-Size="Small" />
							</asp:TemplateField>
							<asp:TemplateField SortExpression="LOGIN_COUNT">
								<HeaderStyle CssClass="HeaderStyle" Font-Size="Small" Wrap="false" />
								<ItemStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblLoginCount" runat="server" Text='<%#  GetValue(Eval("LOGIN_COUNT"),Eval("LOGIN_COUNT_AD")) %>'>
									</asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Font-Size="Small" />
							</asp:TemplateField>
							<asp:TemplateField SortExpression="LOGIN_SUCCESS_COUNT">
								<HeaderStyle CssClass="HeaderStyle" Font-Size="Small" Wrap="false" />
								<ItemStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblLoginSuccessCountRaw" runat="server" Text='<%#  Eval("LOGIN_SUCCESS_COUNT") %>'>
									</asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Font-Size="Small" />
							</asp:TemplateField>
							<asp:TemplateField SortExpression="LOGIN_SUCCESS_COUNT_AD">
								<HeaderStyle CssClass="HeaderStyle" Font-Size="Small" Wrap="false" />
								<ItemStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblLoginSuccessCount" runat="server" Text='<%#  Eval("LOGIN_SUCCESS_COUNT_AD") %>'>
									</asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Font-Size="Small" />
							</asp:TemplateField>
							<asp:TemplateField SortExpression="LOGIN_SUCCESS_COUNT_UNIQUE">
								<HeaderStyle CssClass="HeaderStyle" Font-Size="Small" Wrap="false" />
								<ItemStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblLoginSuccessCountUniqueRaw" runat="server" Text='<%#  Eval("LOGIN_SUCCESS_COUNT_UNIQUE") %>'>
									</asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Font-Size="Small" />
							</asp:TemplateField>
							<asp:TemplateField SortExpression="LOGIN_SUCCESS_COUNT_UNIQUE_AD">
								<HeaderStyle CssClass="HeaderStyle" Font-Size="Small" Wrap="false" />
								<ItemStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblLoginSuccessCountUnique" runat="server" Text='<%#  Eval("LOGIN_SUCCESS_COUNT_UNIQUE_AD") %>'>
									</asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Font-Size="Small" />
							</asp:TemplateField>
							<asp:TemplateField SortExpression="TOTAL_USED_POINT">
								<HeaderStyle CssClass="HeaderStyle" Font-Size="Small" Wrap="false" />
								<ItemStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblTotalUsedPoint" runat="server" Text='<%# GetValue(Eval("TOTAL_USED_POINT"),Eval("TOTAL_USED_POINT_AD")) %>'>
									</asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Font-Size="Small" />
							</asp:TemplateField>
							<asp:BoundField DataField="ORDINARY_SALES_AMT" HtmlEncode="False" HeaderText="通常売上" DataFormatString="{0}" SortExpression="ORDINARY_SALES_AMT">
								<HeaderStyle Wrap="false" />
								<ItemStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
								<FooterStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
							</asp:BoundField>
							<asp:BoundField DataField="POINT_BACK_SALES_AMT" HtmlEncode="False" HeaderText="ﾎﾟｲﾝﾄｱﾌﾘ売上" DataFormatString="{0}" SortExpression="POINT_BACK_SALES_AMT">
								<HeaderStyle Wrap="false" />
								<ItemStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
								<FooterStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
							</asp:BoundField>
							<asp:BoundField DataField="TOTAL_SALES_AMT" HtmlEncode="False" HeaderText="売上合計" DataFormatString="{0}" SortExpression="TOTAL_SALES_AMT">
								<HeaderStyle Wrap="false" />
								<ItemStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
								<FooterStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
							</asp:BoundField>
						</Columns>
						<FooterStyle ForeColor="Black" BackColor="LightYellow" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsAccessPage" runat="server" SelectMethod="GetAdPerformanceList" TypeName="AccessPage">
		<SelectParameters>
			<asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstFromYYYY" Name="pFromYYYY" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstFromMM" Name="pFromMM" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstFromDD" Name="pFromDD" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstToYYYY" Name="pToYYYY" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstToMM" Name="pToMM" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstToDD" Name="pToDD" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstAdGroup" Name="pAdGroupCd" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="txtAdCd" Name="pAdCd" PropertyName="Text" Type="String" />
			<asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" DefaultValue="" />
			<asp:SessionParameter Name="pAdminType" SessionField="AdminType" DefaultValue="" />
			<asp:ControlParameter ControlID="rdoSummaryType" Name="pSummaryType" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="rdoUsedFlag" Name="pUsedFlag" PropertyName="SelectedValue" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsAdGroup" runat="server" SelectMethod="GetList" TypeName="AdGroup">
		<SelectParameters>
			<asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
