﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 広告コード別日別集計(出演者)
--	Progaram ID		: AdDataCastDailyList
--
--  Creation Date	: 2014.06.18
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class AdManage_AdDataCastDailyList:System.Web.UI.Page {
	private string[] Week = { "日","月","火","水","木","金","土" };

	private double dAdCost = 0;
	private double dAccessCount = 0;
	private double dRegistCount = 0;
	private double dRegistCountAd = 0;
	private double dGetPointCastCount = 0;
	private double dStartPaymentCastCount = 0;
	private double dEntMonthPaymentCount = 0;
	private double dEntMonthPaymentAmt = 0;
	private double dRepeatPaymentCount = 0;
	private double dRepeatPaymentAmt = 0;
	private double dTotalPaymentCount = 0;
	private double dTotalPaymentAmt = 0;
	private double dAggrPaymentCastCount = 0;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SITE_CD"]);
		}
		set {
			this.ViewState["SITE_CD"] = value;
		}
	}

	private string AdCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["AD_CD"]);
		}
		set {
			this.ViewState["AD_CD"] = value;
		}
	}

	private string Year {
		get {
			return iBridUtil.GetStringValue(this.ViewState["YEAR"]);
		}
		set {
			this.ViewState["YEAR"] = value;
		}
	}

	private string Month {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MONTH"]);
		}
		set {
			this.ViewState["MONTH"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			this.AdCd = iBridUtil.GetStringValue(Request.QueryString["adcd"]);
			this.Year = iBridUtil.GetStringValue(Request.QueryString["year"]);
			this.Month = iBridUtil.GetStringValue(Request.QueryString["month"]);

			this.CreateList();
			this.InitPage();

			if (!string.IsNullOrEmpty(this.Year) && !string.IsNullOrEmpty(this.Month)) {
				this.lstYear.SelectedValue = this.Year;
				this.lstMonth.SelectedValue = this.Month;
			}
			this.GetAdDataMonthlyHeader();

			pnlGrid.DataBind();
		}
	}

	private void GetAdDataMonthlyHeader() {
		lblAdMenuNmVal.Text = "-";
		lblAggrAdCostVal.Text = "0";
		lblAggrRegistCountVal.Text = "0";
		lblAggrRegistCountAdVal.Text = "0";
		lblAggrRegistRateVal.Text = "-";
		lblAggrGetPointCastVal.Text = "0";
		lblAggrGetPointCastRateVal.Text = "-";
		lblAggrPaymentRateVal.Text = "0";
		lblAggrAdCostByPaymentCastVal.Text = "-";
		lblAggrPaymentAmtVal.Text = "0";
		lblAggrPaymentByUserVal.Text = "-";
		lblAggrAdCostRateVal.Text = "-";
		lblAggrRepeatRateVal.Text = "-";
		lblAggrReturnRateVal.Text = "-";

		AdDataCastMonthly.SearchCondition oSearchCondition = new AdDataCastMonthly.SearchCondition();
		oSearchCondition.SiteCd = this.SiteCd;
		oSearchCondition.AdCd = this.AdCd;
		oSearchCondition.ReportMonth = string.Format("{0}/{1}",this.lstYear.SelectedValue,this.lstMonth.SelectedValue);

		using (AdDataCastMonthly oAdDataCastMonthly = new AdDataCastMonthly()) {
			DataSet oDataSet = oAdDataCastMonthly.GetPageCollection(oSearchCondition,0,1);

			if (oDataSet.Tables[0].Rows.Count > 0) {
				lblAdNmVal.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["AD_NM"]);
				lblAdCdVal.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["AD_CD"]);
				lblPublishStartDateVal.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["PUBLISH_START_DATE"]);
				lblPublishEndDateVal.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["PUBLISH_END_DATE"]);
				lblAdMenuNmVal.Text = oDataSet.Tables[0].Rows[0]["AD_MENU_NM"].ToString();
				lblAggrAdCostVal.Text = oDataSet.Tables[0].Rows[0]["AGGR_AD_COST"].ToString();
				lblAggrRegistCountVal.Text = oDataSet.Tables[0].Rows[0]["AGGR_REGIST_COUNT"].ToString();
				lblAggrRegistCountAdVal.Text = oDataSet.Tables[0].Rows[0]["AGGR_REGIST_COUNT_AD"].ToString();
				lblAggrRegistRateVal.Text = oDataSet.Tables[0].Rows[0]["AGGR_REGIST_RATE"].ToString();
				lblAggrGetPointCastVal.Text = oDataSet.Tables[0].Rows[0]["AGGR_GET_POINT_CAST"].ToString();
				lblAggrGetPointCastRateVal.Text = oDataSet.Tables[0].Rows[0]["AGGR_GET_POINT_CAST_RATE"].ToString();
				lblAggrPaymentRateVal.Text = oDataSet.Tables[0].Rows[0]["AGGR_PAYMENT_RATE"].ToString();
				lblAggrAdCostByPaymentCastVal.Text = oDataSet.Tables[0].Rows[0]["AGGR_AD_COST_BY_PAYMENT_CAST"].ToString();
				lblAggrPaymentAmtVal.Text = oDataSet.Tables[0].Rows[0]["AGGR_PAYMENT_AMT"].ToString();
				lblAggrPaymentByUserVal.Text = oDataSet.Tables[0].Rows[0]["AGGR_PAYMENT_BY_CAST"].ToString();
				lblAggrAdCostRateVal.Text = oDataSet.Tables[0].Rows[0]["AGGR_AD_COST_RATE"].ToString();
				lblAggrRepeatRateVal.Text = oDataSet.Tables[0].Rows[0]["AGGR_REPEAT_RATE"].ToString();
				lblAggrReturnRateVal.Text = oDataSet.Tables[0].Rows[0]["AGGR_RETURN_RATE"].ToString();
				dAggrPaymentCastCount = int.Parse(oDataSet.Tables[0].Rows[0]["AGGR_PAYMENT_CAST_COUNT"].ToString());
			}
		}
	}

	private void InitPage() {
		this.lstYear.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstMonth.SelectedValue = DateTime.Now.ToString("MM");
	}

	private void ClearField() {
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void grdAdDataCastDaily_RowDataBound(object sender,GridViewRowEventArgs e) {

		if (e.Row.RowType == DataControlRowType.DataRow) {
			DataRowView drv = (DataRowView)e.Row.DataItem;
			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(drv["AD_COST"]))) {
				dAdCost = dAdCost + int.Parse(iBridUtil.GetStringValue(drv["AD_COST"]));
				dAccessCount = dAccessCount + int.Parse(iBridUtil.GetStringValue(drv["ACCESS_COUNT"]));
				dRegistCount = dRegistCount + int.Parse(iBridUtil.GetStringValue(drv["REGIST_COUNT"]));
				dRegistCountAd = dRegistCountAd + int.Parse(iBridUtil.GetStringValue(drv["REGIST_COUNT_AD"]));
				dGetPointCastCount = dGetPointCastCount + int.Parse(iBridUtil.GetStringValue(drv["GET_POINT_CAST_COUNT"]));
				dStartPaymentCastCount = dStartPaymentCastCount + int.Parse(iBridUtil.GetStringValue(drv["START_PAYMENT_CAST_COUNT"]));
				dEntMonthPaymentCount = dEntMonthPaymentCount + int.Parse(iBridUtil.GetStringValue(drv["ENT_MONTH_PAYMENT_COUNT"]));
				dEntMonthPaymentAmt = dEntMonthPaymentAmt + int.Parse(iBridUtil.GetStringValue(drv["ENT_MONTH_PAYMENT_AMT"]));
				dRepeatPaymentCount = dRepeatPaymentCount + int.Parse(iBridUtil.GetStringValue(drv["REPEAT_PAYMENT_COUNT"]));
				dRepeatPaymentAmt = dRepeatPaymentAmt + int.Parse(iBridUtil.GetStringValue(drv["REPEAT_PAYMENT_AMT"]));
				dTotalPaymentCount = dTotalPaymentCount + int.Parse(iBridUtil.GetStringValue(drv["TOTAL_PAYMENT_COUNT"]));
				dTotalPaymentAmt = dTotalPaymentAmt + int.Parse(iBridUtil.GetStringValue(drv["TOTAL_PAYMENT_AMT"]));
			}

			DateTime dtReportDay = DateTime.Parse(drv["DAYS"].ToString());
			int iDayOfWeek = (int)dtReportDay.DayOfWeek;

			if (iDayOfWeek == 0) {
				e.Row.BackColor = Color.FromArgb(0xFFCCCC);
			} else if (iDayOfWeek == 6) {
				e.Row.BackColor = Color.FromArgb(0xCCCCFF);
			}
		} else if (e.Row.RowType == DataControlRowType.Footer) {
			DateTime dtSelectedmonth = DateTime.Parse(string.Format("{0}/{1}/01",lstYear.SelectedValue,lstMonth.SelectedValue));
			DateTime dtNowMonth = DateTime.Parse(string.Format("{0}/{1}/01",DateTime.Now.ToString("yyyy"),DateTime.Now.ToString("MM")));

			int iDaysInMonth = DateTime.DaysInMonth(dtSelectedmonth.Year,dtSelectedmonth.Month);
			int iPastDays = 0;

			if (dtSelectedmonth == dtNowMonth) {
				iPastDays = DateTime.Now.Day - 1;
			} else {
				iPastDays = iDaysInMonth;
			}

			Label lblAdCostFooter1 = (Label)e.Row.FindControl("lblAdCostFooter1");
			Label lblAdCostFooter2 = (Label)e.Row.FindControl("lblAdCostFooter2");
			Label lblAdCostFooter3 = (Label)e.Row.FindControl("lblAdCostFooter3");
			Label lblAccessCountFooter1 = (Label)e.Row.FindControl("lblAccessCountFooter1");
			Label lblAccessCountFooter2 = (Label)e.Row.FindControl("lblAccessCountFooter2");
			Label lblAccessCountFooter3 = (Label)e.Row.FindControl("lblAccessCountFooter3");
			Label lblRegistCountFooter1 = (Label)e.Row.FindControl("lblRegistCountFooter1");
			Label lblRegistCountFooter2 = (Label)e.Row.FindControl("lblRegistCountFooter2");
			Label lblRegistCountFooter3 = (Label)e.Row.FindControl("lblRegistCountFooter3");
			Label lblRegistRateFooter1 = (Label)e.Row.FindControl("lblRegistRateFooter1");
			Label lblRegistRateFooter2 = (Label)e.Row.FindControl("lblRegistRateFooter2");
			Label lblRegistRateFooter3 = (Label)e.Row.FindControl("lblRegistRateFooter3");
			Label lblRegistCountAdFooter1 = (Label)e.Row.FindControl("lblRegistCountAdFooter1");
			Label lblRegistCountAdFooter2 = (Label)e.Row.FindControl("lblRegistCountAdFooter2");
			Label lblRegistCountAdFooter3 = (Label)e.Row.FindControl("lblRegistCountAdFooter3");
			Label lblAdCostByRegistFooter1 = (Label)e.Row.FindControl("lblAdCostByRegistFooter1");
			Label lblAdCostByRegistFooter2 = (Label)e.Row.FindControl("lblAdCostByRegistFooter2");
			Label lblAdCostByRegistFooter3 = (Label)e.Row.FindControl("lblAdCostByRegistFooter3");
			Label lblGetPointCastCountFooter1 = (Label)e.Row.FindControl("lblGetPointCastCountFooter1");
			Label lblGetPointCastCountFooter2 = (Label)e.Row.FindControl("lblGetPointCastCountFooter2");
			Label lblGetPointCastCountFooter3 = (Label)e.Row.FindControl("lblGetPointCastCountFooter3");
			Label lblGetPointCastRateFooter1 = (Label)e.Row.FindControl("lblGetPointCastRateFooter1");
			Label lblGetPointCastRateFooter2 = (Label)e.Row.FindControl("lblGetPointCastRateFooter2");
			Label lblGetPointCastRateFooter3 = (Label)e.Row.FindControl("lblGetPointCastRateFooter3");
			Label lblStartPaymentCastCountFooter1 = (Label)e.Row.FindControl("lblStartPaymentCastCountFooter1");
			Label lblStartPaymentCastCountFooter2 = (Label)e.Row.FindControl("lblStartPaymentCastCountFooter2");
			Label lblStartPaymentCastCountFooter3 = (Label)e.Row.FindControl("lblStartPaymentCastCountFooter3");
			Label lblStartPaymentCastRateFooter1 = (Label)e.Row.FindControl("lblStartPaymentCastRateFooter1");
			Label lblStartPaymentCastRateFooter2 = (Label)e.Row.FindControl("lblStartPaymentCastRateFooter2");
			Label lblStartPaymentCastRateFooter3 = (Label)e.Row.FindControl("lblStartPaymentCastRateFooter3");
			Label lblAdCostByStartPaymentFooter1 = (Label)e.Row.FindControl("lblAdCostByStartPaymentFooter1");
			Label lblAdCostByStartPaymentFooter2 = (Label)e.Row.FindControl("lblAdCostByStartPaymentFooter2");
			Label lblAdCostByStartPaymentFooter3 = (Label)e.Row.FindControl("lblAdCostByStartPaymentFooter3");
			Label lblEntMonthPaymentCountFooter1 = (Label)e.Row.FindControl("lblEntMonthPaymentCountFooter1");
			Label lblEntMonthPaymentCountFooter2 = (Label)e.Row.FindControl("lblEntMonthPaymentCountFooter2");
			Label lblEntMonthPaymentCountFooter3 = (Label)e.Row.FindControl("lblEntMonthPaymentCountFooter3");
			Label lblEntMonthPaymentAmtFooter1 = (Label)e.Row.FindControl("lblEntMonthPaymentAmtFooter1");
			Label lblEntMonthPaymentAmtFooter2 = (Label)e.Row.FindControl("lblEntMonthPaymentAmtFooter2");
			Label lblEntMonthPaymentAmtFooter3 = (Label)e.Row.FindControl("lblEntMonthPaymentAmtFooter3");
			Label lblEntMonthReturnRateFooter1 = (Label)e.Row.FindControl("lblEntMonthReturnRateFooter1");
			Label lblEntMonthReturnRateFooter2 = (Label)e.Row.FindControl("lblEntMonthReturnRateFooter2");
			Label lblEntMonthReturnRateFooter3 = (Label)e.Row.FindControl("lblEntMonthReturnRateFooter3");
			Label lblRepeatPaymentCountFooter1 = (Label)e.Row.FindControl("lblRepeatPaymentCountFooter1");
			Label lblRepeatPaymentCountFooter2 = (Label)e.Row.FindControl("lblRepeatPaymentCountFooter2");
			Label lblRepeatPaymentCountFooter3 = (Label)e.Row.FindControl("lblRepeatPaymentCountFooter3");
			Label lblRepeatPaymentAmtFooter1 = (Label)e.Row.FindControl("lblRepeatPaymentAmtFooter1");
			Label lblRepeatPaymentAmtFooter2 = (Label)e.Row.FindControl("lblRepeatPaymentAmtFooter2");
			Label lblRepeatPaymentAmtFooter3 = (Label)e.Row.FindControl("lblRepeatPaymentAmtFooter3");
			Label lblTotalPaymentCountFooter1 = (Label)e.Row.FindControl("lblTotalPaymentCountFooter1");
			Label lblTotalPaymentCountFooter2 = (Label)e.Row.FindControl("lblTotalPaymentCountFooter2");
			Label lblTotalPaymentCountFooter3 = (Label)e.Row.FindControl("lblTotalPaymentCountFooter3");
			Label lblTotalPaymentAmtFooter1 = (Label)e.Row.FindControl("lblTotalPaymentAmtFooter1");
			Label lblTotalPaymentAmtFooter2 = (Label)e.Row.FindControl("lblTotalPaymentAmtFooter2");
			Label lblTotalPaymentAmtFooter3 = (Label)e.Row.FindControl("lblTotalPaymentAmtFooter3");
			Label lblRepeatRateFooter1 = (Label)e.Row.FindControl("lblRepeatRateFooter1");
			Label lblRepeatRateFooter2 = (Label)e.Row.FindControl("lblRepeatRateFooter2");
			Label lblRepeatRateFooter3 = (Label)e.Row.FindControl("lblRepeatRateFooter3");
			Label lblTotalAmtByCastFooter1 = (Label)e.Row.FindControl("lblTotalAmtByCastFooter1");
			Label lblTotalAmtByCastFooter2 = (Label)e.Row.FindControl("lblTotalAmtByCastFooter2");
			Label lblTotalAmtByCastFooter3 = (Label)e.Row.FindControl("lblTotalAmtByCastFooter3");

			lblAdCostFooter1.Text = dAdCost.ToString();
			lblAdCostFooter2.Text = "-";
			lblAdCostFooter3.Text = "-";
			double dAdCostAvgVal = 0;
			double dAdCostEstVal = 0;

			if (dAdCost > 0) {
				dAdCostAvgVal = dAdCost / iPastDays;
				dAdCostEstVal = dAdCostAvgVal * iDaysInMonth;
				lblAdCostFooter2.Text = GetDecimalValue(dAdCostEstVal).ToString();
				lblAdCostFooter3.Text = GetDecimalValue(dAdCostAvgVal).ToString();
			}

			lblAccessCountFooter1.Text = dAccessCount.ToString();
			lblAccessCountFooter2.Text = "-";
			lblAccessCountFooter3.Text = "-";
			double dAccessCountAvgVal = 0;
			double dAccessCountEstVal = 0;

			if (dAccessCount > 0) {
				dAccessCountAvgVal = dAccessCount / iPastDays;
				dAccessCountEstVal = dAccessCountAvgVal * iDaysInMonth;
				lblAccessCountFooter2.Text = GetDecimalValue(dAccessCountEstVal).ToString();
				lblAccessCountFooter3.Text = GetDecimalValue(dAccessCountAvgVal).ToString();
			}


			lblRegistCountFooter1.Text = dRegistCount.ToString();
			lblRegistCountFooter2.Text = "-";
			lblRegistCountFooter3.Text = "-";
			double dRegistCountAvgVal = 0;
			double dRegistCountEstVal = 0;

			if (dRegistCount > 0) {
				dRegistCountAvgVal = dRegistCount / iPastDays;
				dRegistCountEstVal = dRegistCountAvgVal * iDaysInMonth;
				lblRegistCountFooter2.Text = GetDecimalValue(dRegistCountEstVal).ToString();
				lblRegistCountFooter3.Text = GetDecimalValue(dRegistCountAvgVal).ToString();
			}

			lblRegistRateFooter1.Text = "-";
			lblRegistRateFooter2.Text = "-";
			lblRegistRateFooter3.Text = "-";
			if (dAccessCount > 0) {
				lblRegistRateFooter1.Text = GetDecimalValue(dRegistCount / dAccessCount * 100).ToString();
				lblRegistRateFooter2.Text = GetDecimalValue(dRegistCountEstVal / dAccessCountEstVal * 100).ToString();
				lblRegistRateFooter3.Text = GetDecimalValue(dRegistCountAvgVal / dAccessCountAvgVal * 100).ToString();
			}

			lblRegistCountAdFooter1.Text = dRegistCountAd.ToString();
			lblRegistCountAdFooter2.Text = "-";
			lblRegistCountAdFooter3.Text = "-";
			double dRegistCountAdAvgVal = 0;
			double dRegistCountAdEstVal = 0;

			if (dRegistCountAd > 0) {
				dRegistCountAdAvgVal = dRegistCountAd / iPastDays;
				dRegistCountAdEstVal = dRegistCountAdAvgVal * iDaysInMonth;
				lblRegistCountAdFooter2.Text = GetDecimalValue(dRegistCountAdEstVal).ToString();
				lblRegistCountAdFooter3.Text = GetDecimalValue(dRegistCountAdAvgVal).ToString();
			}

			lblAdCostByRegistFooter1.Text = "-";
			lblAdCostByRegistFooter2.Text = "-";
			lblAdCostByRegistFooter3.Text = "-";
			if (dRegistCount > 0) {
				lblAdCostByRegistFooter1.Text = GetDecimalValue(dAdCost / dRegistCount * 100).ToString();
				lblAdCostByRegistFooter2.Text = GetDecimalValue(dAdCostEstVal / dRegistCountEstVal * 100).ToString();
				lblAdCostByRegistFooter3.Text = GetDecimalValue(dAdCostAvgVal / dRegistCountAvgVal * 100).ToString();
			}

			lblGetPointCastCountFooter1.Text = dGetPointCastCount.ToString();
			lblGetPointCastCountFooter2.Text = "-";
			lblGetPointCastCountFooter3.Text = "-";
			double dGetPointCastCountAvgVal = 0;
			double dGetPointCastCountEstVal = 0;

			if (dGetPointCastCount > 0) {
				dGetPointCastCountAvgVal = dGetPointCastCount / iPastDays;
				dGetPointCastCountEstVal = dGetPointCastCountAvgVal * iDaysInMonth;
				lblGetPointCastCountFooter2.Text = GetDecimalValue(dGetPointCastCountEstVal).ToString();
				lblGetPointCastCountFooter3.Text = GetDecimalValue(dGetPointCastCountAvgVal).ToString();
			}

			lblGetPointCastRateFooter1.Text = "-";
			lblGetPointCastRateFooter2.Text = "-";
			lblGetPointCastRateFooter3.Text = "-";
			if (dRegistCount > 0) {
				lblGetPointCastRateFooter1.Text = GetDecimalValue(dGetPointCastCount / dRegistCount * 100).ToString();
				lblGetPointCastRateFooter2.Text = GetDecimalValue(dGetPointCastCountEstVal / dRegistCountEstVal * 100).ToString();
				lblGetPointCastRateFooter3.Text = GetDecimalValue(dGetPointCastCountAvgVal / dRegistCountAvgVal * 100).ToString();
			}

			lblStartPaymentCastCountFooter1.Text = dStartPaymentCastCount.ToString();
			lblStartPaymentCastCountFooter2.Text = "-";
			lblStartPaymentCastCountFooter3.Text = "-";
			double dStartPaymentCastCountAvgVal = 0;
			double dStartPaymentCastCountEstVal = 0;

			if (dStartPaymentCastCount > 0) {
				dStartPaymentCastCountAvgVal = dStartPaymentCastCount / iPastDays;
				dStartPaymentCastCountEstVal = dStartPaymentCastCountAvgVal * iDaysInMonth;
				lblStartPaymentCastCountFooter2.Text = GetDecimalValue(dStartPaymentCastCountEstVal).ToString();
				lblStartPaymentCastCountFooter3.Text = GetDecimalValue(dStartPaymentCastCountAvgVal).ToString();
			}

			lblStartPaymentCastRateFooter1.Text = "-";
			lblStartPaymentCastRateFooter2.Text = "-";
			lblStartPaymentCastRateFooter3.Text = "-";
			if (dRegistCount > 0) {
				lblStartPaymentCastRateFooter1.Text = GetDecimalValue(dStartPaymentCastCount / dRegistCount * 100).ToString();
				lblStartPaymentCastRateFooter2.Text = GetDecimalValue(dStartPaymentCastCountEstVal / dRegistCountEstVal * 100).ToString();
				lblStartPaymentCastRateFooter3.Text = GetDecimalValue(dStartPaymentCastCountAvgVal / dRegistCountAvgVal * 100).ToString();
			}

			lblAdCostByStartPaymentFooter1.Text = "-";
			lblAdCostByStartPaymentFooter2.Text = "-";
			lblAdCostByStartPaymentFooter3.Text = "-";
			if (dStartPaymentCastCount > 0) {
				lblAdCostByStartPaymentFooter1.Text = GetDecimalValue(dAdCost / dStartPaymentCastCount * 100).ToString();
				lblAdCostByStartPaymentFooter2.Text = GetDecimalValue(dAdCostEstVal / dStartPaymentCastCountEstVal * 100).ToString();
				lblAdCostByStartPaymentFooter3.Text = GetDecimalValue(dAdCostAvgVal / dStartPaymentCastCountAvgVal * 100).ToString();
			}

			lblEntMonthPaymentCountFooter1.Text = dEntMonthPaymentCount.ToString();
			lblEntMonthPaymentCountFooter2.Text = "-";
			lblEntMonthPaymentCountFooter3.Text = "-";
			double dEntMonthPaymentCountAvgVal = 0;
			double dEntMonthPaymentCountEstVal = 0;

			if (dEntMonthPaymentCount > 0) {
				dEntMonthPaymentCountAvgVal = dEntMonthPaymentCount / iPastDays;
				dEntMonthPaymentCountEstVal = dEntMonthPaymentCountAvgVal * iDaysInMonth;
				lblEntMonthPaymentCountFooter2.Text = GetDecimalValue(dEntMonthPaymentCountEstVal).ToString();
				lblEntMonthPaymentCountFooter3.Text = GetDecimalValue(dEntMonthPaymentCountAvgVal).ToString();
			}

			lblEntMonthPaymentAmtFooter1.Text = dEntMonthPaymentAmt.ToString();
			lblEntMonthPaymentAmtFooter2.Text = "-";
			lblEntMonthPaymentAmtFooter3.Text = "-";
			double dEntMonthPaymentAmtAvgVal = 0;
			double dEntMonthPaymentAmtEstVal = 0;

			if (dEntMonthPaymentAmt > 0) {
				dEntMonthPaymentAmtAvgVal = dEntMonthPaymentAmt / iPastDays;
				dEntMonthPaymentAmtEstVal = dEntMonthPaymentAmtAvgVal * iDaysInMonth;
				lblEntMonthPaymentAmtFooter2.Text = GetDecimalValue(dEntMonthPaymentAmtEstVal).ToString();
				lblEntMonthPaymentAmtFooter3.Text = GetDecimalValue(dEntMonthPaymentAmtAvgVal).ToString();
			}

			lblEntMonthReturnRateFooter1.Text = "-";
			lblEntMonthReturnRateFooter2.Text = "-";
			lblEntMonthReturnRateFooter3.Text = "-";
			if (dAdCost > 0) {
				lblEntMonthReturnRateFooter1.Text = GetDecimalValue(dEntMonthPaymentAmt / dAdCost * 100).ToString();
				lblEntMonthReturnRateFooter2.Text = GetDecimalValue(dEntMonthPaymentAmtEstVal / dAdCostEstVal * 100).ToString();
				lblEntMonthReturnRateFooter3.Text = GetDecimalValue(dEntMonthPaymentAmtAvgVal / dAdCostAvgVal * 100).ToString();
			}

			lblRepeatPaymentCountFooter1.Text = dRepeatPaymentCount.ToString();
			lblRepeatPaymentCountFooter2.Text = "-";
			lblRepeatPaymentCountFooter3.Text = "-";
			double dRepeatPaymentCountAvgVal = 0;
			double dRepeatPaymentCountEstVal = 0;

			if (dRepeatPaymentCount > 0) {
				dRepeatPaymentCountAvgVal = dRepeatPaymentCount / iPastDays;
				dRepeatPaymentCountEstVal = dRepeatPaymentCountAvgVal * iDaysInMonth;
				lblRepeatPaymentCountFooter2.Text = GetDecimalValue(dRepeatPaymentCountEstVal).ToString();
				lblRepeatPaymentCountFooter3.Text = GetDecimalValue(dRepeatPaymentCountAvgVal).ToString();
			}

			lblRepeatPaymentAmtFooter1.Text = dRepeatPaymentAmt.ToString();
			lblRepeatPaymentAmtFooter2.Text = "-";
			lblRepeatPaymentAmtFooter3.Text = "-";
			double dRepeatPaymentAmtAvgVal = 0;
			double dRepeatPaymentAmtEstVal = 0;

			if (dRepeatPaymentAmt > 0) {
				dRepeatPaymentAmtAvgVal = dRepeatPaymentAmt / iPastDays;
				dRepeatPaymentAmtEstVal = dRepeatPaymentAmtAvgVal * iDaysInMonth;
				lblRepeatPaymentAmtFooter2.Text = GetDecimalValue(dRepeatPaymentAmtEstVal).ToString();
				lblRepeatPaymentAmtFooter3.Text = GetDecimalValue(dRepeatPaymentAmtAvgVal).ToString();
			}

			lblTotalPaymentCountFooter1.Text = dTotalPaymentCount.ToString();
			lblTotalPaymentCountFooter2.Text = "-";
			lblTotalPaymentCountFooter3.Text = "-";
			double dTotalPaymentCountAvgVal = 0;
			double dTotalPaymentCountEstVal = 0;

			if (dTotalPaymentCount > 0) {
				dTotalPaymentCountAvgVal = dTotalPaymentCount / iPastDays;
				dTotalPaymentCountEstVal = dTotalPaymentCountAvgVal * iDaysInMonth;
				lblTotalPaymentCountFooter2.Text = GetDecimalValue(dTotalPaymentCountEstVal).ToString();
				lblTotalPaymentCountFooter3.Text = GetDecimalValue(dTotalPaymentCountAvgVal).ToString();
			}

			lblTotalPaymentAmtFooter1.Text = dTotalPaymentAmt.ToString();
			lblTotalPaymentAmtFooter2.Text = "-";
			lblTotalPaymentAmtFooter3.Text = "-";
			double dTotalPaymentAmtAvgVal = 0;
			double dTotalPaymentAmtEstVal = 0;

			if (dTotalPaymentAmt > 0) {
				dTotalPaymentAmtAvgVal = dTotalPaymentAmt / iPastDays;
				dTotalPaymentAmtEstVal = dTotalPaymentAmtAvgVal * iDaysInMonth;
				lblTotalPaymentAmtFooter2.Text = GetDecimalValue(dTotalPaymentAmtEstVal).ToString();
				lblTotalPaymentAmtFooter3.Text = GetDecimalValue(dTotalPaymentAmtAvgVal).ToString();
			}

			lblRepeatRateFooter1.Text = "-";
			lblRepeatRateFooter2.Text = "-";
			lblRepeatRateFooter3.Text = "-";
			double dAggrPaymentCastCountAvgVal = 0;
			double dAggrPaymentCastCountEstVal = 0;
			double dAggrPaymentCastCountOrg = dAggrPaymentCastCount - dTotalPaymentCount;
			if (dAggrPaymentCastCount > 0) {
				dAggrPaymentCastCountAvgVal = dAggrPaymentCastCountOrg + dTotalPaymentCountAvgVal;
				dAggrPaymentCastCountEstVal = dAggrPaymentCastCountOrg + dTotalPaymentCountEstVal;
				lblRepeatRateFooter1.Text = GetDecimalValue(dRepeatPaymentCount / dAggrPaymentCastCount * 100).ToString();
				lblRepeatRateFooter2.Text = GetDecimalValue(dRepeatPaymentCountEstVal / dAggrPaymentCastCountEstVal * 100).ToString();
				lblRepeatRateFooter3.Text = GetDecimalValue(dRepeatPaymentCountAvgVal / dAggrPaymentCastCountAvgVal * 100).ToString();
			}

			lblTotalAmtByCastFooter1.Text = "-";
			lblTotalAmtByCastFooter2.Text = "-";
			lblTotalAmtByCastFooter3.Text = "-";
			if (dTotalPaymentCount > 0) {
				lblTotalAmtByCastFooter1.Text = GetDecimalValue(dTotalPaymentAmt / dTotalPaymentCount).ToString();
				lblTotalAmtByCastFooter2.Text = GetDecimalValue(dTotalPaymentAmtEstVal / dTotalPaymentCountEstVal).ToString();
				lblTotalAmtByCastFooter3.Text = GetDecimalValue(dTotalPaymentAmtAvgVal / dTotalPaymentCountAvgVal).ToString();
			}
		}
	}

	private double GetDecimalValue(double pValue) {
		double dTmpVal = pValue * 100;
		return Math.Floor(dTmpVal) / 100;
	}

	protected string AddPercentMark(string pValue) {
		if (!string.IsNullOrEmpty(pValue)) {
			pValue = pValue + "%";
		}

		return pValue;
	}

	private void GetList() {

		grdAdDataCastDaily.PageIndex = 0;
		grdAdDataCastDaily.DataSourceID = "dsAdDataCastDaily";
		DataBind();
	}

	protected Color GetBackColor(object pDayOfWeek) {
		if (pDayOfWeek.ToString().Equals("Sat")) {
			return Color.FromArgb(0xCCCCFF);
		} else {
			if (pDayOfWeek.ToString().Equals("Sun")) {
				return Color.FromArgb(0xFFCCCC);
			} else {
				return Color.Empty;
			}
		}
	}

	private void CreateList() {
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		int iStartYear = 2009;
		int iCurYear = DateTime.Now.Year;
		lstYear.Items.Clear();

		for (int i = 0;i + iStartYear <= iCurYear;i++) {

			lstYear.Items.Add(new ListItem(iBridUtil.GetStringValue(i + iStartYear)));
		}

		lstMonth.Items.Clear();
		for (int i = 1;i <= 12;i++) {
			lstMonth.Items.Add(new ListItem(string.Format("{0:D2}",i)));
		}
	}

	private bool IsCorrectDate() {
		return true;
	}

	protected string GetDisplayReportDay(string pReportDay) {
		DateTime dtReportDay = DateTime.Parse(pReportDay);
		return dtReportDay.Day.ToString();
	}

	protected string GetDisplayDayOfWeek(string pReportDay) {
		DateTime dtReportDay = DateTime.Parse(pReportDay);
		int iDayOfWeekNo = (int)dtReportDay.DayOfWeek;

		return Week[iDayOfWeekNo];
	}
}
