<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="IssueAdCd.aspx.cs" Inherits="AdManage_IssueAdCd" Title="広告グループ自動発行"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="広告コード自動発行"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset>
			<legend>[広告コード発行設定]</legend>
			<table border="0" style="width: 800px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle">
						サイトコード
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px" AutoPostBack="True"
							OnSelectedIndexChanged="lstSiteCd_SelectedIndexChanged">
						</asp:DropDownList>
					</td>
					<td class="tdHeaderStyle">
						広告グループ
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstAdGroup" runat="server" DataSourceID="dsAdGroup" DataTextField="AD_GROUP_NM" DataValueField="AD_GROUP_CD" Width="240px">
						</asp:DropDownList>
						<asp:RequiredFieldValidator ID="vdrAdGroup" runat="server" Display="Dynamic" ErrorMessage="広告グループを入力して下さい。" ControlToValidate="lstAdGroup" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
					</td>
				</tr>
				<tr>
					<td class="tdHeaderStyle">
						<%= DisplayWordUtil.Replace("男性会員")%>成果発生ﾀｲﾐﾝｸﾞ
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstManRegistReportTiming" runat="server" DataSourceID="dsManRegistReportTiming" DataTextField="CODE_NM" DataValueField="CODE">
						</asp:DropDownList>
					</td>
					<td class="tdHeaderStyle">
						<%= DisplayWordUtil.Replace("女性会員")%>成果発生ﾀｲﾐﾝｸﾞ
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstCastRegistReportTiming" runat="server" DataSourceID="dsCastRegistReportTiming" DataTextField="CODE_NM" DataValueField="CODE">
						</asp:DropDownList>
					</td>												
				</tr>
				<tr>
					<td class="tdHeaderStyle2">
						広告コード発行数
					</td>
					<td class="tdDataStyle">
						<asp:TextBox ID="txtIssueCount" runat="server" MaxLength="5" Width="60px"></asp:TextBox>
						<asp:RequiredFieldValidator ID="vdrIssueCount" runat="server" Display="Dynamic" ErrorMessage="広告コード発行数を入力して下さい。" ControlToValidate="txtIssueCount" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
					</td>
					<td class="tdHeaderStyle2">
						広告コード名
					</td>
					<td class="tdDataStyle">
						<asp:TextBox ID="txtAdNm" runat="server" MaxLength="60" Width="200px"></asp:TextBox>
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnIssue" Text="発行" CssClass="seekbutton" OnClick="btnIssue_Click" ValidationGroup="Detail" />
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsAdGroup" runat="server" SelectMethod="GetList" TypeName="AdGroup" OnSelecting="dsAdGroup_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsManRegistReportTiming" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="16" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastRegistReportTiming" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="20" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>		
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtIssueCount" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrAdGroup" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrIssueCount" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnIssue" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
