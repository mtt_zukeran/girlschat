﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AdDataCastDailyList.aspx.cs" Inherits="AdManage_AdDataCastDailyList" Title="広告コード別日別集計(出演者)"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="広告コード別日別集計(出演者)"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
                            <td class="tdHeaderStyle">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                表示月
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstYear" runat="server" Width="60px">
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstMonth" runat="server" Width="40px">
                                </asp:DropDownList>月
                                <br />
								<asp:Label ID="lblErrorMessageReportDayFrom" runat="server" ForeColor="red" Visible="false"></asp:Label>
                            </td>
                        </tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
			</asp:Panel>
		</fieldset>
		<fieldset>
			<asp:Panel ID="pnlHeader" runat="server" ScrollBars="none">
				<asp:PlaceHolder ID="plcHolder" runat="server">
					<asp:Table ID="Table1" runat="server" CssClass="tableStyle tdDetailsViewStyle">
						<asp:TableRow ID="row1" runat="server">
							<asp:TableCell ID="celAdNm" runat="server" CssClass="tdHeaderStyle ">
								<asp:Label ID="lblAdNm" runat="server" Text='広告名'></asp:Label>
							</asp:TableCell>
							<asp:TableCell ID="celAdNmVal" runat="server" CssClass="tdDataStyle ">
								<asp:Label ID="lblAdNmVal" runat="server" Text="Label"></asp:Label>
							</asp:TableCell>
							<asp:TableCell ID="celPublishStartDate" runat="server" CssClass="tdHeaderStyle " Width="200px">
								<asp:Label ID="lblPublishStartDate" runat="server" Text='掲載開始日'></asp:Label>
							</asp:TableCell>
							<asp:TableCell ID="celPublishStartDateVal" runat="server" CssClass="tdDataStyle ">
								<asp:Label ID="lblPublishStartDateVal" runat="server" Text="Label"></asp:Label>
							</asp:TableCell>
						</asp:TableRow>
						<asp:TableRow ID="row2" runat="server">
							<asp:TableCell ID="celAdCd" runat="server" CssClass="tdHeaderStyle ">
								<asp:Label ID="lblAdCd" runat="server" Text='広告コード'></asp:Label>
							</asp:TableCell>
							<asp:TableCell ID="celAdCdVal" runat="server" CssClass="tdDataStyle ">
								<asp:Label ID="lblAdCdVal" runat="server" Text="Label"></asp:Label>
							</asp:TableCell>
							<asp:TableCell ID="celPubishEndDate" runat="server" CssClass="tdHeaderStyle ">
								<asp:Label ID="lblPublishEndDate" runat="server" Text='広告停止日'></asp:Label>
							</asp:TableCell>
							<asp:TableCell ID="celPubishEndDateVal" runat="server" CssClass="tdDataStyle ">
								<asp:Label ID="lblPublishEndDateVal" runat="server" Text="Label"></asp:Label>
							</asp:TableCell>
						</asp:TableRow>
					</asp:Table>
					<asp:Table ID="Table2" runat="server" CssClass="tableStyle tdDetailsViewStyle" Width="1200px">
						<asp:TableRow ID="row3" runat="server">
							<asp:TableCell ID="celAdMenuNm" runat="server" CssClass="tdHeaderStyle ">
								<asp:Label ID="lblAdMenuNm" runat="server" Text='掲載ﾒﾆｭｰ'></asp:Label>
							</asp:TableCell>
							<asp:TableCell ID="celAggrAdCost" runat="server" CssClass="tdHeaderStyle ">
								<asp:Label ID="lblAggrAdCost" runat="server" Text='累計広告費'></asp:Label>
							</asp:TableCell>
							<asp:TableCell ID="celAggrRegistCount" runat="server" CssClass="tdHeaderStyle ">
								<asp:Label ID="lblAggrRegistCount" runat="server" Text='累計登録数'></asp:Label>
							</asp:TableCell>
							<asp:TableCell ID="celAggrRegistCountAd" runat="server" CssClass="tdHeaderStyle ">
								<asp:Label ID="lblAggrRegistCountAd" runat="server" Text='累計認証数'></asp:Label>
							</asp:TableCell>
							<asp:TableCell ID="celAggrRegistRate" runat="server" CssClass="tdHeaderStyle ">
								<asp:Label ID="lblAggrRegistRate" runat="server" Text='累計登録率'></asp:Label>
							</asp:TableCell>
							<asp:TableCell ID="celAggrGetPointCast" runat="server" CssClass="tdHeaderStyle ">
								<asp:Label ID="lblAggrGetPointCast" runat="server" Text='累計顧客数'></asp:Label>
							</asp:TableCell>
							<asp:TableCell ID="celAggrGetPointCastRate" runat="server" CssClass="tdHeaderStyle ">
								<asp:Label ID="lblAggrGetPointCastRate" runat="server" Text='累計ﾎﾟｲﾝﾄ獲得率'></asp:Label>
							</asp:TableCell>
							<asp:TableCell ID="celAggrPaymentRate" runat="server" CssClass="tdHeaderStyle ">
								<asp:Label ID="lblAggrPaymentRate" runat="server" Text='累計報酬率'></asp:Label>
							</asp:TableCell>
							<asp:TableCell ID="celAggrAdCostByPaymentCast" runat="server" CssClass="tdHeaderStyle ">
								<asp:Label ID="lblAggrAdCostByPaymentCast" runat="server" Text='累計顧客単価'></asp:Label>
							</asp:TableCell>
							<asp:TableCell ID="celAggrPaymentAmt" runat="server" CssClass="tdHeaderStyle ">
								<asp:Label ID="lblAggrPaymentAmt" runat="server" Text='累計報酬金額'></asp:Label>
							</asp:TableCell>
							<asp:TableCell ID="celAggrPaymentByCast" runat="server" CssClass="tdHeaderStyle ">
								<asp:Label ID="lblAggrPaymentByCast" runat="server" Text='累計報酬単価'></asp:Label>
							</asp:TableCell>
							<asp:TableCell ID="celAggrAdCostRate" runat="server" CssClass="tdHeaderStyle ">
								<asp:Label ID="lblAggrAdCostRate" runat="server" Text='累計広告比率'></asp:Label>
							</asp:TableCell>
							<asp:TableCell ID="celAggrRepeatRate" runat="server" CssClass="tdHeaderStyle ">
								<asp:Label ID="lblAggrRepeatRate" runat="server" Text='累計ﾘﾋﾟｰﾄ率'></asp:Label>
							</asp:TableCell>
							<asp:TableCell ID="celAggrReturnRate" runat="server" CssClass="tdHeaderStyle ">
								<asp:Label ID="lblAggrReturnRate" runat="server" Text='累計回収率'></asp:Label>
							</asp:TableCell>
						</asp:TableRow>
						<asp:TableRow ID="row4" runat="server">
							<asp:TableCell ID="celAdMenuNmVal" runat="server" CssClass="tdDataStyle ">
								<asp:Label ID="lblAdMenuNmVal" runat="server"></asp:Label>
							</asp:TableCell>
							<asp:TableCell ID="celAggrAdCostVal" runat="server" CssClass="tdDataStyle ">
								<asp:Label ID="lblAggrAdCostVal" runat="server"></asp:Label>
							</asp:TableCell>
							<asp:TableCell ID="celAggrRegistCountVal" runat="server" CssClass="tdDataStyle ">
								<asp:Label ID="lblAggrRegistCountVal" runat="server"></asp:Label>
							</asp:TableCell>
							<asp:TableCell ID="celAggrRegistCountAdVal" runat="server" CssClass="tdDataStyle ">
								<asp:Label ID="lblAggrRegistCountAdVal" runat="server"></asp:Label>
							</asp:TableCell>
							<asp:TableCell ID="celAggrRegistRateVal" runat="server" CssClass="tdDataStyle ">
								<asp:Label ID="lblAggrRegistRateVal" runat="server"></asp:Label>%
							</asp:TableCell>
							<asp:TableCell ID="celAggrGetPointCastVal" runat="server" CssClass="tdDataStyle ">
								<asp:Label ID="lblAggrGetPointCastVal" runat="server"></asp:Label>
							</asp:TableCell>
							<asp:TableCell ID="celAggrGetPointCastRateVal" runat="server" CssClass="tdDataStyle ">
								<asp:Label ID="lblAggrGetPointCastRateVal" runat="server"></asp:Label>%
							</asp:TableCell>
							<asp:TableCell ID="celAggrPaymentRateVal" runat="server" CssClass="tdDataStyle ">
								<asp:Label ID="lblAggrPaymentRateVal" runat="server"></asp:Label>%
							</asp:TableCell>
							<asp:TableCell ID="celAggrAdCostByPaymentCastVal" runat="server" CssClass="tdDataStyle ">
								<asp:Label ID="lblAggrAdCostByPaymentCastVal" runat="server"></asp:Label>%
							</asp:TableCell>
							<asp:TableCell ID="celAggrPaymentAmtVal" runat="server" CssClass="tdDataStyle ">
								<asp:Label ID="lblAggrPaymentAmtVal" runat="server"></asp:Label>
							</asp:TableCell>
							<asp:TableCell ID="celAggrPaymentByUserVal" runat="server" CssClass="tdDataStyle ">
								<asp:Label ID="lblAggrPaymentByUserVal" runat="server"></asp:Label>%
							</asp:TableCell>
							<asp:TableCell ID="celAggrAdCostRateVal" runat="server" CssClass="tdDataStyle ">
								<asp:Label ID="lblAggrAdCostRateVal" runat="server"></asp:Label>%
							</asp:TableCell>
							<asp:TableCell ID="celAggrRepeatRateVal" runat="server" CssClass="tdDataStyle ">
								<asp:Label ID="lblAggrRepeatRateVal" runat="server"></asp:Label>%
							</asp:TableCell>
							<asp:TableCell ID="celAggrReturnRateVal" runat="server" CssClass="tdDataStyle">
								<asp:Label ID="lblAggrReturnRateVal" runat="server"></asp:Label>%
							</asp:TableCell>
						</asp:TableRow>
					</asp:Table>
				</asp:PlaceHolder>
			</asp:Panel>
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="750px">
				<asp:GridView ID="grdAdDataCastDaily" runat="server" AutoGenerateColumns="False" DataSourceID="dsAdDataCastDaily" CellPadding="0" AllowSorting="True" ShowFooter="true"
					OnRowDataBound="grdAdDataCastDaily_RowDataBound" SkinID="GridViewColorNoRowStyle">
					<Columns>
						<asp:TemplateField HeaderText="日付">
							<ItemStyle HorizontalAlign="Center" Width="70px" />
							<ItemTemplate>
								<asp:Label ID="Label1" runat="server" Text='<%# GetDisplayReportDay(Eval("DAYS").ToString()) %>' BackColor='' Width="40px"></asp:Label>
							</ItemTemplate>
							<FooterStyle HorizontalAlign="center" Wrap="false" />
							<FooterTemplate>
								<asp:Label ID="lblReportDayFooter1" Text="合計" runat="server"></asp:Label>
								<asp:Label ID="Label4" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblReportDayFooter2" Text="最終予測" runat="server"></asp:Label>
								<asp:Label ID="Label5" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblReportDayFooter3" Text="平均" runat="server"></asp:Label>
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="曜日">
							<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
							<ItemTemplate>
								<asp:Label ID="Label2" runat="server" Text='<%# GetDisplayDayOfWeek(Eval("DAYS").ToString()) %>' BackColor='' Width="60px"></asp:Label>
							</ItemTemplate>
							<FooterTemplate>
								<asp:Label ID="lblDayOfWeekFooter1" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label6" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblDayOfWeekFooter2" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label7" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblDayOfWeekFooter3" Text="" runat="server"></asp:Label>
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="広告費">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("AD_COST") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblAdCostFooter1" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label8" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblAdCostFooter2" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label9" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblAdCostFooter3" Text="" runat="server"></asp:Label>
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="アクセス数">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("ACCESS_COUNT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblAccessCountFooter1" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label10" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblAccessCountFooter2" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label11" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblAccessCountFooter3" Text="" runat="server"></asp:Label>
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="登録数">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("REGIST_COUNT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblRegistCountFooter1" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label12" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblRegistCountFooter2" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label13" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblRegistCountFooter3" Text="" runat="server"></asp:Label>
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="登録率">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# AddPercentMark(Eval("REGIST_RATE").ToString()) %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblRegistRateFooter1" Text="" runat="server"></asp:Label>%
								<asp:Label ID="Label14" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblRegistRateFooter2" Text="" runat="server"></asp:Label>%
								<asp:Label ID="Label15" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblRegistRateFooter3" Text="" runat="server"></asp:Label>%
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="認証数">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("REGIST_COUNT_AD") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblRegistCountAdFooter1" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label16" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblRegistCountAdFooter2" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label17" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblRegistCountAdFooter3" Text="" runat="server"></asp:Label>
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="獲得単価">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("AD_COST_BY_REGIST") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblAdCostByRegistFooter1" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label18" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblAdCostByRegistFooter2" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label19" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblAdCostByRegistFooter3" Text="" runat="server"></asp:Label>
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ﾎﾟｲﾝﾄ獲得人数">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("GET_POINT_CAST_COUNT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblGetPointCastCountFooter1" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label20" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblGetPointCastCountFooter2" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label21" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblGetPointCastCountFooter3" Text="" runat="server"></asp:Label>
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ﾎﾟｲﾝﾄ消費率">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# AddPercentMark(Eval("GET_POINT_CAST_RATE").ToString()) %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblGetPointCastRateFooter1" Text="" runat="server"></asp:Label>%
								<asp:Label ID="Label22" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblGetPointCastRateFooter2" Text="" runat="server"></asp:Label>%
								<asp:Label ID="Label23" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblGetPointCastRateFooter3" Text="" runat="server"></asp:Label>%
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="初顧客人数">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("START_PAYMENT_CAST_COUNT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblStartPaymentCastCountFooter1" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label24" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblStartPaymentCastCountFooter2" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label25" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblStartPaymentCastCountFooter3" Text="" runat="server"></asp:Label>
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="初顧客報酬率">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# AddPercentMark(Eval("START_PAYMENT_CAST_RATE").ToString()) %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblStartPaymentCastRateFooter1" Text="" runat="server"></asp:Label>%
								<asp:Label ID="Label26" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblStartPaymentCastRateFooter2" Text="" runat="server"></asp:Label>%
								<asp:Label ID="Label27" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblStartPaymentCastRateFooter3" Text="" runat="server"></asp:Label>%
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="顧客単価">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("AD_COST_BY_START_PAYMENT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblAdCostByStartPaymentFooter1" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label28" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblAdCostByStartPaymentFooter2" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label29" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblAdCostByStartPaymentFooter3" Text="" runat="server"></asp:Label>
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="当月報酬人数">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("ENT_MONTH_PAYMENT_COUNT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblEntMonthPaymentCountFooter1" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label30" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblEntMonthPaymentCountFooter2" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label31" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblEntMonthPaymentCountFooter3" Text="" runat="server"></asp:Label>
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="当月報酬金額">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("ENT_MONTH_PAYMENT_AMT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblEntMonthPaymentAmtFooter1" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label32" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblEntMonthPaymentAmtFooter2" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label33" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblEntMonthPaymentAmtFooter3" Text="" runat="server"></asp:Label>
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="当月回収率">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# AddPercentMark(Eval("ENT_MONTH_RETURN_RATE").ToString()) %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblEntMonthReturnRateFooter1" Text="" runat="server"></asp:Label>%
								<asp:Label ID="Label34" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblEntMonthReturnRateFooter2" Text="" runat="server"></asp:Label>%
								<asp:Label ID="Label35" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblEntMonthReturnRateFooter3" Text="" runat="server"></asp:Label>%
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ﾘﾋﾟｰﾄ報酬人数">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("REPEAT_PAYMENT_COUNT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblRepeatPaymentCountFooter1" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label36" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblRepeatPaymentCountFooter2" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label37" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblRepeatPaymentCountFooter3" Text="" runat="server"></asp:Label>
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ﾘﾋﾟｰﾄ報酬金額">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("REPEAT_PAYMENT_AMT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblRepeatPaymentAmtFooter1" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label38" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblRepeatPaymentAmtFooter2" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label39" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblRepeatPaymentAmtFooter3" Text="" runat="server"></asp:Label>
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="合計報酬人数">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("TOTAL_PAYMENT_COUNT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblTotalPaymentCountFooter1" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label40" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblTotalPaymentCountFooter2" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label41" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblTotalPaymentCountFooter3" Text="" runat="server"></asp:Label>
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="合計報酬金額">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("TOTAL_PAYMENT_AMT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblTotalPaymentAmtFooter1" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label42" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblTotalPaymentAmtFooter2" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label43" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblTotalPaymentAmtFooter3" Text="" runat="server"></asp:Label>
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ﾘﾋﾟｰﾄ率">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# AddPercentMark(Eval("REPEAT_RATE").ToString()) %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblRepeatRateFooter1" Text="" runat="server"></asp:Label>%
								<asp:Label ID="Label44" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblRepeatRateFooter2" Text="" runat="server"></asp:Label>%
								<asp:Label ID="Label45" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblRepeatRateFooter3" Text="" runat="server"></asp:Label>%
							</FooterTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="報酬単価">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("TOTAL_AMT_BY_CAST") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							<FooterTemplate>
								<asp:Label ID="lblTotalAmtByCastFooter1" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label46" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblTotalAmtByCastFooter2" Text="" runat="server"></asp:Label>
								<asp:Label ID="Label47" runat="server" Text='<br />'></asp:Label>
								<asp:Label ID="lblTotalAmtByCastFooter3" Text="" runat="server"></asp:Label>
							</FooterTemplate>
						</asp:TemplateField>
					</Columns>
					<FooterStyle ForeColor="Black" BackColor="LightYellow" />
				</asp:GridView>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsAdDataCastDaily" runat="server" SelectMethod="GetListDaily" TypeName="AdDataCastDaily">
		<SelectParameters>
			<asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstYear" Name="pYear" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstMonth" Name="pMonth" PropertyName="SelectedValue" Type="String" />
			<asp:QueryStringParameter Name="pAdCd" QueryStringField="adcd" Type="String" DefaultValue="" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
</asp:Content>
