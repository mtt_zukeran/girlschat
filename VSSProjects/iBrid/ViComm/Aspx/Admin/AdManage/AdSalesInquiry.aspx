<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AdSalesInquiry.aspx.cs" Inherits="AdManage_AdSalesInquiry"
	Title="�L���ʔ����" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="�L���ʔ����"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[��������]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 632px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							�T�C�g
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="232px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							�L���R�[�h
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtAdCd" runat="server" Width="200px" MaxLength="32"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							�W�v�J�n��
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstFromYYYY" runat="server" Width="56px">
							</asp:DropDownList>�N
							<asp:DropDownList ID="lstFromMM" runat="server" Width="40px">
							</asp:DropDownList>��
							<asp:DropDownList ID="lstFromDD" runat="server" Width="40px">
							</asp:DropDownList>��
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							�W�v�I����
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstToYYYY" runat="server" Width="56px">
							</asp:DropDownList>�N
							<asp:DropDownList ID="lstToMM" runat="server" Width="40px">
							</asp:DropDownList>��
							<asp:DropDownList ID="lstToDD" runat="server" Width="40px">
							</asp:DropDownList>��
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="����" CssClass="seekbutton" OnClick="btnListSeek_Click" />
				<asp:Button runat="server" ID="btnClear" Text="�N���A" CssClass="seekbutton" OnClick="btnClear_Click" />
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[�ғ��W�v]</legend>
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="540px">
					<asp:GridView ID="grdAd" runat="server" AutoGenerateColumns="False" DataSourceID="dsDailySales" ShowFooter="True" AllowSorting="False" OnRowDataBound="grdAd_RowDataBound"
						CellPadding="1" SkinID="GridViewColor" UseAccessibleHeader="False" OnInit="grdAd_Init">
						<Columns>
							<asp:BoundField DataField="AD_CD" HeaderText="����">
								<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="70px" />
							</asp:BoundField>
							<asp:BoundField DataField="AD_NM" HeaderText="�L����" FooterText="&lt;���v&gt;">
								<ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="160px" />
								<FooterStyle HorizontalAlign="Right" Font-Size="X-Small" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="��">
								<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Font-Size="X-Small" />
								<ItemTemplate>
									<asp:Label ID="lblConviniCount" runat="server" Text='<%# Eval("TOTAL_SALES_COUNT") %>' Width="30px"></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Font-Size="X-Small" />
								<HeaderStyle CssClass="HeaderStyle" Font-Size="X-Small" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="���z">
								<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Font-Size="X-Small" />
								<ItemTemplate>
									<asp:Label ID="lblConviniAmt" runat="server" Text='<%# Eval("TOTAL_SALES_AMT") %>' Width="60px"></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Font-Size="X-Small" />
								<HeaderStyle CssClass="HeaderStyle" Font-Size="X-Small" />
							</asp:TemplateField>
						</Columns>
						<FooterStyle ForeColor="Black" BackColor="LightYellow" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsDailySales" runat="server" SelectMethod="DailyAdSalesInquiry" TypeName="DailySales" OnSelecting="dsDailySales_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pFromYYYY" Type="String" />
			<asp:Parameter Name="pFromMM" Type="String" />
			<asp:Parameter Name="pFromDD" Type="String" />
			<asp:Parameter Name="pToYYYY" Type="String" />
			<asp:Parameter Name="pToMM" Type="String" />
			<asp:Parameter Name="pToDD" Type="String" />
			<asp:Parameter Name="pSettleCampanyMask" Type="String" />
			<asp:Parameter Name="pAdCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
</asp:Content>
