﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AdDataUserMonthlyListByUsedFlag.aspx.cs" Inherits="AdManage_AdDataUserMonthlyListByUsedFlag" Title="媒体一覧(会員)" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="媒体一覧(会員)"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[検索条件]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 800px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                表示月
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstYear" runat="server" Width="60px">
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstMonth" runat="server" Width="40px">
                                </asp:DropDownList>月
                                <br />
								<asp:Label ID="lblErrorMessageReportDayFrom" runat="server" ForeColor="red" Visible="false"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" />
            </fieldset>
        </asp:Panel>
		<asp:Panel runat="server" ID="pnlList">
			<fieldset>
				<legend>[集計]</legend>
				<asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdAdDataUserMonthly.PageIndex + 1%>
                        of
                        <%= grdAdDataUserMonthly.PageCount%>
                    </a>
                </asp:Panel>
                &nbsp;<br />
				<asp:Panel ID="pnlGrid" runat="server">
					<asp:GridView ID="grdAdDataUserMonthly" DataSourceID="dsAdDataUserMonthly" runat="server" AllowPaging="true" AutoGenerateColumns="False" OnSorting="grdAdDataUserMonthly_Sorting" PageSize="9999"
						EnableSortingAndPagingCallbacks="false" ShowFooter="false" SkinID="GridViewColor" AllowSorting="true" OnRowDataBound="grdAdDataUserMonthly_RowDataBound">
						<Columns>
							<asp:TemplateField HeaderText="代理店名">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblAdGroupNm" runat="server" Text='<%# Eval("AD_GROUP_NM") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle Wrap="false" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="広告名">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:HyperLink ID="lnkAdNm" runat="server" NavigateUrl='<%# string.Format("~/AdManage/AdDataUserDailyList.aspx?sitecd={0}&adcd={1}&year={2}&month={3}",Eval("SITE_CD"),Eval("AD_CD"),lstYear.SelectedValue,lstMonth.SelectedValue) %>' Text='<%# Eval("AD_NM") %>' ></asp:HyperLink>&nbsp;
									<asp:HyperLink ID="lnkAd" runat="server" NavigateUrl='<%# string.Format("~/AdManage/AdList.aspx?adcd={0}",Eval("AD_CD")) %>' Text='設定' ></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle Wrap="false" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="広告費" SortExpression="AD_COST">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblAdCost" runat="server" Text='<%# Eval("AD_COST") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ｱｸｾｽ数" SortExpression="ACCESS_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblAccessCount" runat="server" Text='<%# Eval("ACCESS_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="登録数" SortExpression="REGIST_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblRegistCount" runat="server" Text='<%# Eval("REGIST_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="登録率" SortExpression="REGIST_RATE">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblRegistRate" runat="server" Text='<%# Eval("REGIST_RATE") %>'></asp:Label>%
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="認証数" SortExpression="REGIST_COUNT_AD">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblRegistCountAd" runat="server" Text='<%# Eval("REGIST_COUNT_AD") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="獲得単価" SortExpression="AD_COST_BY_REGIST">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblAdCostByRegist" runat="server" Text='<%# Eval("AD_COST_BY_REGIST") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾛｸﾞｲﾝ数" SortExpression="LOGIN_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblLoginCount" runat="server" Text='<%# Eval("LOGIN_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾎﾟｲﾝﾄ消費<br>人数" SortExpression="CHARGE_POINT_USER_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblChargePointUserCount" runat="server" Text='<%# Eval("CHARGE_POINT_USER_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾎﾟｲﾝﾄ消費<br>消費率" SortExpression="CHARGE_POINT_USER_RATE">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblChargePointUserRate" runat="server" Text='<%# Eval("CHARGE_POINT_USER_RATE") %>'></asp:Label>%
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="初顧客<br>人数" SortExpression="START_RECEIPT_USER_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblStartReceiptUserCount" runat="server" Text='<%# Eval("START_RECEIPT_USER_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="初顧客<br>課金率" SortExpression="START_RECEIPT_USER_RATE">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblStartReceiptUserRate" runat="server" Text='<%# Eval("START_RECEIPT_USER_RATE") %>'></asp:Label>%
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="顧客単価" SortExpression="AD_COST_BY_REGIST">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblAdCostByRegist" runat="server" Text='<%# Eval("AD_COST_BY_REGIST") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="当月課金<br>人数" SortExpression="ENT_MONTH_RECEIPT_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblEntMonthReceiptCount" runat="server" Text='<%# Eval("ENT_MONTH_RECEIPT_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="当月課金<br>金額" SortExpression="ENT_MONTH_RECEIPT_AMT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblEntMonthReceiptAmt" runat="server" Text='<%# Eval("ENT_MONTH_RECEIPT_AMT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="当月回収率" SortExpression="ENT_MONTH_RETURN_RATE">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblEntMonthReturnRate" runat="server" Text='<%# Eval("ENT_MONTH_RETURN_RATE") %>'></asp:Label>%
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾘﾋﾟｰﾄ課金<br>人数" SortExpression="REPEAT_RECEIPT_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblRepeatReceiptCount" runat="server" Text='<%# Eval("REPEAT_RECEIPT_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾘﾋﾟｰﾄ課金<br>金額" SortExpression="REPEAT_RECEIPT_AMT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblRepeatReceiptAmt" runat="server" Text='<%# Eval("REPEAT_RECEIPT_AMT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="合計課金<br>人数" SortExpression="TOTAL_RECEIPT_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblTotalReceiptCount" runat="server" Text='<%# Eval("TOTAL_RECEIPT_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="合計課金<br>金額" SortExpression="TOTAL_RECEIPT_AMT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblTotalReceiptAmt" runat="server" Text='<%# Eval("TOTAL_RECEIPT_AMT") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾘﾋﾟｰﾄ率" SortExpression="REPEAT_RATE">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblRepeatRate" runat="server" Text='<%# Eval("REPEAT_RATE") %>'></asp:Label>%
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="累計広告比率" SortExpression="AGGR_AD_COST_RATE">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblAggrAdCostRate" runat="server" Text='<%# Eval("AGGR_AD_COST_RATE") %>'></asp:Label>%
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
						</Columns>
					</asp:GridView>
				</asp:Panel>
			</fieldset>
			<br />
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsAdDataUserMonthly" runat="server" ConvertNullToDBNull="false" EnablePaging="true" OnSelecting="dsAdDataUserMonthly_Selecting" OnSelected="dsAdDataUserMonthly_Selected"
		SelectMethod="GetPageCollection" SelectCountMethod="GetPageCount" SortParameterName="" TypeName="AdDataUserMonthly">
		<SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
</asp:Content>

