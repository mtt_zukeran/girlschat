﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 広告ｺｰﾄﾞ別月別集計(出演者)
--	Progaram ID		: AdDataCastMonthlyList
--  Creation Date	: 2014.06.17
--  Creater			: Y.Ikemiya
**************************************************************************/
using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using ViComm.Extension.Pwild;

public partial class AdManage_AdDataCastMonthlyList:System.Web.UI.Page {

	private string recCount = string.Empty;

	private double AdCost = 0;
	private double AccessCount = 0;
	private double RegistCount = 0;
	private double RegistCountAd = 0;
	private double LoginCount = 0;
	private double GetPointCastCount = 0;
	private double StartPaymentCastCount = 0;
	private double EntMonthPaymentCount = 0;
	private double EntMonthPaymentAmt = 0;
	private double RepeatPaymentCount = 0;
	private double RepeatPaymentAmt = 0;
	private double TotalPaymentCount = 0;
	private double TotalPaymentAmt = 0;
	private double AggrPaymentCastCount = 0;
	private double AggrPaymentAmt = 0;
	private double AggrAdCost = 0;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SITE_CD"]);
		}
		set {
			this.ViewState["SITE_CD"] = value;
		}
	}

	private string AdGroupCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["AD_GROUP_CD"]);
		}
		set {
			this.ViewState["AD_GROUP_CD"] = value;
		}
	}

	private string Year {
		get {
			return iBridUtil.GetStringValue(this.ViewState["YEAR"]);
		}
		set {
			this.ViewState["YEAR"] = value;
		}
	}

	private string Month {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MONTH"]);
		}
		set {
			this.ViewState["MONTH"] = value;
		}
	}

	private string SortExpression {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortExpression"]);
		}
		set {
			this.ViewState["SortExpression"] = value;
		}
	}

	private string SortDirect {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortDirect"]);
		}
		set {
			this.ViewState["SortDirect"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			this.AdGroupCd = iBridUtil.GetStringValue(Request.QueryString["adGroupcd"]);
			this.Year = iBridUtil.GetStringValue(Request.QueryString["year"]);
			this.Month = iBridUtil.GetStringValue(Request.QueryString["month"]);

			this.CreateList();
			this.InitPage();

			if (!string.IsNullOrEmpty(this.Year) && !string.IsNullOrEmpty(this.Month)) {
				this.lstYear.SelectedValue = this.Year;
				this.lstMonth.SelectedValue = this.Month;
			}

			pnlGrid.DataBind();
		}
	}

	protected void dsAdDataCastMonthly_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {

		AdDataCastMonthly.SearchCondition oSearchCondition = new AdDataCastMonthly.SearchCondition();
		oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
		oSearchCondition.ReportMonth = string.Format("{0}/{1}",this.lstYear.SelectedValue,this.lstMonth.SelectedValue);
		oSearchCondition.AdGroupCd = this.AdGroupCd;
		oSearchCondition.SortDirection = this.SortDirect;
		oSearchCondition.SortExpression = this.SortExpression;

		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsAdDataCastMonthly_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		if (!this.IsCorrectDate()) {
			return;
		}

		this.SortDirect = string.Empty;
		this.SortExpression = string.Empty;

		this.grdAdDataCastMonthly.PageIndex = 0;
		this.GetList();
	}

	protected void grdAdDataCastMonthly_Sorting(object sender,GridViewSortEventArgs e) {
		if (this.SortExpression.Equals(e.SortExpression)) {
			if (this.SortDirect.Equals("ASC")) {
				this.SortDirect = "DESC";
			} else if (this.SortDirect.Equals("DESC")) {
				this.SortDirect = "ASC";
			}
		} else {
			this.SortDirect = "DESC";
		}

		this.SortExpression = e.SortExpression;

		e.Cancel = true;
		this.GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	private void GetList() {
		this.grdAdDataCastMonthly.PageIndex = 0;
		this.grdAdDataCastMonthly.PageSize = 9999;
		this.grdAdDataCastMonthly.DataSourceID = "dsAdDataCastMonthly";
		this.grdAdDataCastMonthly.DataBind();
		this.pnlCount.DataBind();
	}

	protected void grdAdDataCastMonthly_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			DataRowView drv = (DataRowView)e.Row.DataItem;
			AdCost = AdCost + double.Parse(iBridUtil.GetStringValue(drv["AD_COST"]));
			AccessCount = AccessCount + double.Parse(iBridUtil.GetStringValue(drv["ACCESS_COUNT"]));
			RegistCount = RegistCount + double.Parse(iBridUtil.GetStringValue(drv["REGIST_COUNT"]));
			RegistCountAd = RegistCountAd + double.Parse(iBridUtil.GetStringValue(drv["REGIST_COUNT_AD"]));
			LoginCount = LoginCount + double.Parse(iBridUtil.GetStringValue(drv["LOGIN_COUNT"]));
			GetPointCastCount = GetPointCastCount + double.Parse(iBridUtil.GetStringValue(drv["GET_POINT_CAST_COUNT"]));
			StartPaymentCastCount = StartPaymentCastCount + double.Parse(iBridUtil.GetStringValue(drv["START_PAYMENT_CAST_COUNT"]));
			EntMonthPaymentCount = EntMonthPaymentCount + double.Parse(iBridUtil.GetStringValue(drv["ENT_MONTH_PAYMENT_COUNT"]));
			EntMonthPaymentAmt = EntMonthPaymentAmt + double.Parse(iBridUtil.GetStringValue(drv["ENT_MONTH_PAYMENT_AMT"]));
			RepeatPaymentCount = RepeatPaymentCount + double.Parse(iBridUtil.GetStringValue(drv["REPEAT_PAYMENT_COUNT"]));
			RepeatPaymentAmt = RepeatPaymentAmt + double.Parse(iBridUtil.GetStringValue(drv["REPEAT_PAYMENT_AMT"]));
			TotalPaymentCount = TotalPaymentCount + double.Parse(iBridUtil.GetStringValue(drv["TOTAL_PAYMENT_COUNT"]));
			TotalPaymentAmt = TotalPaymentAmt + double.Parse(iBridUtil.GetStringValue(drv["TOTAL_PAYMENT_AMT"]));
			AggrAdCost = AggrAdCost + double.Parse(iBridUtil.GetStringValue(drv["AGGR_AD_COST"]));
			AggrPaymentCastCount = AggrPaymentCastCount + double.Parse(iBridUtil.GetStringValue(drv["AGGR_PAYMENT_CAST_COUNT"]));
			AggrPaymentAmt = AggrPaymentAmt + double.Parse(iBridUtil.GetStringValue(drv["AGGR_PAYMENT_AMT"]));

		} else if (e.Row.RowType == DataControlRowType.Footer) {
			e.Row.Cells[0].Text = "合計";
			e.Row.Cells[1].Text = iBridUtil.GetStringValue(AdCost);
			e.Row.Cells[2].Text = iBridUtil.GetStringValue(AccessCount);
			e.Row.Cells[3].Text = iBridUtil.GetStringValue(RegistCount);

			e.Row.Cells[4].Text = "-%";
			if (AccessCount > 0) {
				double dRegistRate;
				dRegistRate = GetDecimalValue(RegistCount / AccessCount * 100);
				e.Row.Cells[4].Text = iBridUtil.GetStringValue(dRegistRate) + "%";
			}

			e.Row.Cells[5].Text = iBridUtil.GetStringValue(RegistCountAd);

			e.Row.Cells[6].Text = "-";
			if (RegistCount > 0) {
				double dAdCostByRegist = GetDecimalValue(AdCost / RegistCount);
				e.Row.Cells[6].Text = iBridUtil.GetStringValue(dAdCostByRegist);
			}

			e.Row.Cells[7].Text = iBridUtil.GetStringValue(LoginCount);
			e.Row.Cells[8].Text = iBridUtil.GetStringValue(GetPointCastCount);

			e.Row.Cells[9].Text = "-%";
			if (RegistCount > 0) {
				double dGetPointCastRate = GetDecimalValue(GetPointCastCount / RegistCount * 100);
				e.Row.Cells[9].Text = iBridUtil.GetStringValue(dGetPointCastRate) + "%";
			}

			e.Row.Cells[10].Text = iBridUtil.GetStringValue(StartPaymentCastCount);

			e.Row.Cells[11].Text = "-%";
			if (RegistCount > 0) {
				double dStartPaymentCastRate = GetDecimalValue(StartPaymentCastCount / RegistCount * 100);
				e.Row.Cells[11].Text = iBridUtil.GetStringValue(dStartPaymentCastRate) + "%";
			}

			e.Row.Cells[12].Text = "-";
			if (StartPaymentCastCount > 0) {
				double dAdCostByStartPayment = GetDecimalValue(AdCost / StartPaymentCastCount);
				e.Row.Cells[12].Text = iBridUtil.GetStringValue(dAdCostByStartPayment);
			}

			e.Row.Cells[13].Text = iBridUtil.GetStringValue(EntMonthPaymentCount);
			e.Row.Cells[14].Text = iBridUtil.GetStringValue(EntMonthPaymentAmt);

			e.Row.Cells[15].Text = "-%";
			if (AdCost > 0) {
				double dReturnRate = GetDecimalValue(EntMonthPaymentAmt / AdCost * 100);
				e.Row.Cells[15].Text = iBridUtil.GetStringValue(AdCost) + "%";
			}

			e.Row.Cells[16].Text = iBridUtil.GetStringValue(RepeatPaymentCount);
			e.Row.Cells[17].Text = iBridUtil.GetStringValue(RepeatPaymentAmt);
			e.Row.Cells[18].Text = iBridUtil.GetStringValue(TotalPaymentCount);
			e.Row.Cells[19].Text = iBridUtil.GetStringValue(TotalPaymentAmt);

			e.Row.Cells[20].Text = "-%";
			if (AggrPaymentCastCount > 0) {
				double dRepeatRate = GetDecimalValue(RepeatPaymentCount / AggrPaymentCastCount * 100);
				e.Row.Cells[20].Text = iBridUtil.GetStringValue(dRepeatRate) + "%";
			}

			e.Row.Cells[21].Text = "-%";
			if (AggrPaymentAmt > 0) {
				double dAggrAdRate = GetDecimalValue(AggrAdCost / AggrPaymentAmt * 100);
				e.Row.Cells[21].Text = iBridUtil.GetStringValue(dAggrAdRate) + "%";
			}
		}
	}

	private double GetDecimalValue(double pValue) {
		double dTmpVal = pValue * 100;
		return Math.Floor(dTmpVal) / 100;
	}

	protected string AddPercentMark(string pValue) {
		if (!string.IsNullOrEmpty(pValue)) {
			pValue = pValue + "%";
		}

		return pValue;
	}

	private void InitPage() {
		this.lstYear.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstMonth.SelectedValue = DateTime.Now.ToString("MM");
	}

	private void CreateList() {
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		int iStartYear = 2009;
		int iCurYear = DateTime.Now.Year;
		lstYear.Items.Clear();

		for (int i = 0;i + iStartYear <= iCurYear;i++) {

			lstYear.Items.Add(new ListItem(iBridUtil.GetStringValue(i + iStartYear)));
		}

		lstMonth.Items.Clear();
		for (int i = 1;i <= 12;i++) {
			lstMonth.Items.Add(new ListItem(string.Format("{0:D2}",i)));
		}
	}

	private bool IsCorrectDate() {
		return true;
	}

	protected string GetRecCount() {
		return this.recCount;
	}
}