﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 広告グループ別広告コード
--	Progaram ID		: AdListByGroupCd
--
--  Creation Date	: 2010.03.18
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class AdManage_AdListByGroupCd:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
		if (!Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_SITE_OWNER)) {
			btnCsv.Visible = false;
		}
	}

	private void FirstLoad() {
		grdAd.PageSize = 100;
		grdAd.DataSourceID = "";
		DataBind();
	}

	private void InitPage() {
		pnlInfo.Visible = false;
		ClearField();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		lstAdGroup.DataBind();
	}

	private void ClearField() {
		txtIssueDayFrom.Text = "";
		txtIssueDayTo.Text = "";
		txtAdNm.Text = "";
		lstSiteCd.SelectedIndex = 0;
		lstAdGroup.SelectedIndex = 0;
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	private void GetList() {
		if (chkPagingOff.Checked) {
			grdAd.PageSize = 99999;
			grdAd.AllowSorting = true;
		} else {
			grdAd.PageSize = 50;
			grdAd.AllowSorting = false;
		}
		grdAd.DataSourceID = "dsAd";
		grdAd.PageIndex = 0;
		grdAd.DataBind();
		pnlCount.DataBind();
		pnlInfo.Visible = true;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (IsValid) {
			if ((!txtIssueDayFrom.Text.Equals("")) || (!txtIssueDayTo.Text.Equals(""))) {
				if (txtIssueDayFrom.Text.Equals("")) {
					txtIssueDayFrom.Text = txtIssueDayTo.Text;
				} else if (txtIssueDayTo.Text.Equals("")) {
					txtIssueDayTo.Text = txtIssueDayFrom.Text;
				}
			}
			GetList();
		}
	}

	protected void btnCsv_Click(object sender,EventArgs e) {
		if (IsValid) {
			if ((!txtIssueDayFrom.Text.Equals("")) || (!txtIssueDayTo.Text.Equals(""))) {
				if (txtIssueDayFrom.Text.Equals("")) {
					txtIssueDayFrom.Text = txtIssueDayTo.Text;
				} else if (txtIssueDayTo.Text.Equals("")) {
					txtIssueDayTo.Text = txtIssueDayFrom.Text;
				}
			}
			string fileName = "AdListByGroupCd" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".csv";

			Response.AddHeader("Content-Disposition","attachment;filename=" + fileName);
			Response.ContentType = "application/octet-stream-dummy";
			System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

			using (Ad oAd = new Ad()) {
				DataSet ds = oAd.GetCsvDataBySiteGroupCd(lstSiteCd.SelectedValue,lstAdGroup.SelectedValue,txtAdNm.Text,txtIssueDayFrom.Text,txtIssueDayTo.Text);

				SetCsvData(ds);
				Response.End();
			}
		}
	}

	public void SetCsvData(DataSet pDataSet) {

		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");
		string sDtl = "";

		DataRow dr = null;
		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			sDtl = "";
			dr = pDataSet.Tables[0].Rows[i];
			sDtl += dr["AD_CD"].ToString() + ",";
			sDtl += dr["AD_NM"].ToString();
			sDtl += "\r\n";
			Response.BinaryWrite(encoding.GetBytes(sDtl));
		}
	}

	protected void dsAd_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = lstAdGroup.SelectedValue;
		e.InputParameters[2] = txtAdNm.Text;
		e.InputParameters[3] = txtIssueDayFrom.Text;
		e.InputParameters[4] = txtIssueDayTo.Text;
	}

	protected void dsAd_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsAdGroup_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
	}

	protected void lstSiteCd_SelectedIndexChanged(object sender,EventArgs e) {
		lstAdGroup.DataBind();
	}
}
