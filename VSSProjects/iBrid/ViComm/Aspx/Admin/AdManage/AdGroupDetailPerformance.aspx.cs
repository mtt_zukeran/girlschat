﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 広告効果集計

--	Progaram ID		: AdGroupDetailPerformance
--
--  Creation Date	: 2009.09.08
--  Creater			: iBrid
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------
  Date			Updater     Update Explain
  2010/08/13	K.Itoh		ﾕﾆｰｸ数の対応。＋できる限りリファクタ
-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class AdManage_AdGroupDetailPerformance:System.Web.UI.Page {
	private int accessCount;
	private int loginCount;
	private int loginSuccessCount;
	private int loginSuccessCountUnique;
	private int registCount;
	private int usedPoint;
	private int ordinarySalesAmt;
	private int ocSalesAmt;
	private int totalSalesAmt;

	private int colAdGroupIndex = 0;
	private int colAdCdIndex = 1;
	private int colAdNmIndex = 2;
	private int colAccessCountIndex = 3;
	private int colRegistCountIndex = 4;
	private int colLoginCountIndex = 5;
	private int colLoginSuccessCountIndex = 6;
	private int colLoginSuccessCountUniqueIndex = 7;
	private int colTotalUsedPointIndex = 8;
	private int colOrdinarySalesAmtIndex = 9;
	private int colPointBackSalesAmtIndex = 10;
	private int colTotalSalesAmtIndex = 11;

	protected string SexCd {
		get {
			return ViewState["SEX_CD"] as string ?? string.Empty;
		}
		set {
			ViewState["SEX_CD"] = value;
		}
	}

	/// <summary>
	/// 男性表示モードかどうかを示す値を取得する。 
	/// </summary>
	protected bool IsManMode {
		get {
			return this.SexCd.Equals(ViCommConst.MAN);
		}
	}
	/// <summary>
	/// ﾛｸﾞｲﾝ中のｱｶｳﾝﾄが広告管理者かどうかを示す値を取得する。 
	/// </summary>
	protected bool IsAdManager {
		get {
			return Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_AD_MANAGE);
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.grdAdGroupDetail.Columns[this.colLoginCountIndex].Visible = !ManageCompany.IsAvailableUniqueAccessSummary();
		this.grdAdGroupDetail.Columns[this.colLoginSuccessCountIndex].Visible = ManageCompany.IsAvailableUniqueAccessSummary();
		this.grdAdGroupDetail.Columns[this.colLoginSuccessCountUniqueIndex].Visible = ManageCompany.IsAvailableUniqueAccessSummary();

		if (!IsPostBack) {
			InitPage();
			FirstLoad();
		}
	}

	private void FirstLoad() {
		lstAdGroup.Items.Clear();
		txtAdCd.Text = "";

		DataBind();

		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals(string.Empty)) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		DataBind();

		lstAdGroup.Items.Insert(0,new ListItem(string.Empty,string.Empty));

		lstAdGroup.SelectedIndex = 0;

		lstSiteCd.DataSourceID = string.Empty;

		if (this.IsAdManager) {
			if (!iBridUtil.GetStringValue(Session["AD_CD"]).Equals(string.Empty)) {
				txtAdCd.Text = Session["AD_CD"].ToString();
				rdoAdGroup.Checked = true;
				rdoAdGroupAll.Checked = false;
				plcAdGroup.Visible = false;
				lstAdGroup.DataSourceID = "";
				txtAdCd.Enabled = false;
			} else {
				lstAdGroup.SelectedValue = Session["AD_GROUP_CD"].ToString();
				lstAdGroup.Enabled = false;
			}
		}

		this.SexCd = iBridUtil.GetStringValue(Request.QueryString["sexcd"]);
		if (this.IsManMode) {
			this.Title = this.Title + DisplayWordUtil.Replace("（会員）");
			lblPgmTitle.Text = lblPgmTitle.Text + DisplayWordUtil.Replace("（会員）");
			lblFromToName.Text = "≪購入日指定≫";
			grdAdGroupDetail.Columns[this.colLoginCountIndex].HeaderText = "ﾛｸﾞｲﾝ数";
			grdAdGroupDetail.Columns[this.colTotalUsedPointIndex].HeaderText = "利用Pt.";
		} else {
			this.Title = this.Title + DisplayWordUtil.Replace("（出演者）");
			lblPgmTitle.Text = lblPgmTitle.Text + DisplayWordUtil.Replace("（出演者）");
			lblFromToName.Text = "≪稼動日指定≫";
			grdAdGroupDetail.Columns[this.colLoginCountIndex].HeaderText = "待機数";
			grdAdGroupDetail.Columns[this.colTotalUsedPointIndex].HeaderText = "獲得報酬";
		}
	}

	private void InitPage() {
		if (!lstAdGroup.DataSourceID.Equals(string.Empty)) {
			lstAdGroup.SelectedIndex = 0;
		}
		txtAdCd.Text = "";
		grdAdGroupDetail.DataSource = null;
		grdAdGroupDetail.DataBind();

		pnlInfo.Visible = false;
		ClearTotal();
		if (!IsPostBack) {
			SetupFromToDay();
		}
	}

	private void SetupFromToDay() {
		SysPrograms.SetupFromToDay(lstFromYYYYRegist,lstFromMMRegist,lstFromDDRegist,lstToYYYYRegist,lstToMMRegist,lstToDDRegist,true);
		lstFromYYYYRegist.SelectedIndex = 0;
		lstToYYYYRegist.SelectedIndex = 0;
		lstFromMMRegist.SelectedValue = DateTime.Now.ToString("MM");
		lstToMMRegist.SelectedValue = DateTime.Now.ToString("MM");
		lstFromDDRegist.SelectedValue = DateTime.Now.ToString("dd");
		lstToDDRegist.SelectedValue = DateTime.Now.ToString("dd");
		SysPrograms.SetupFromToDay(lstFromYYYYPurchase,lstFromMMPurchase,lstFromDDPurchase,lstToYYYYPurchase,lstToMMPurchase,lstToDDPurchase,true);
		lstFromYYYYPurchase.SelectedIndex = 0;
		lstToYYYYPurchase.SelectedIndex = 0;
		lstFromMMPurchase.SelectedValue = DateTime.Now.ToString("MM");
		lstToMMPurchase.SelectedValue = DateTime.Now.ToString("MM");
		lstFromDDPurchase.SelectedValue = DateTime.Now.ToString("dd");
		lstToDDPurchase.SelectedValue = DateTime.Now.ToString("dd");
	}

	private void ClearTotal() {
		btnCsv.Visible = false;
		accessCount = 0;
		loginCount = 0;
		loginSuccessCount = 0;
		loginSuccessCountUnique = 0;
		usedPoint = 0;
		registCount = 0;
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		string fileName = ViCommConst.CSV_FILE_NM_AD_GROUP_DETAIL_PERFORMANCE_LOG;

		Response.AddHeader("Content-Disposition","attachment;filename=" + fileName);
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		using (AccessPage pAccess = new AccessPage()) {
			DataSet ds = pAccess.GetAdGroupDetailPerformanceList(
								lstSiteCd.SelectedValue,
								lstFromYYYYRegist.SelectedValue,
								lstFromMMRegist.SelectedValue,
								lstFromDDRegist.SelectedValue,
								lstToYYYYRegist.SelectedValue,
								lstToMMRegist.SelectedValue,
								lstToDDRegist.SelectedValue,
								lstFromYYYYPurchase.SelectedValue,
								lstFromMMPurchase.SelectedValue,
								lstFromDDPurchase.SelectedValue,
								lstToYYYYPurchase.SelectedValue,
								lstToMMPurchase.SelectedValue,
								lstToDDPurchase.SelectedValue,
								lstAdGroup.SelectedValue,
								txtAdCd.Text,
								this.SexCd,
								rdoAdGroupAll.Checked,
								IsAdManager
								);

			string sCsvRec = GetCsvString(ds);

			Response.BinaryWrite(encoding.GetBytes(sCsvRec));
			Response.End();
		}
	}

	protected void grdAdGroupDetail_RowDataBound(object sender,GridViewRowEventArgs e) {

		if (e.Row.RowType == DataControlRowType.DataRow) {
			if (this.IsAdManager) {
				accessCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"ACCESS_COUNT_AD"));
				loginCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"LOGIN_COUNT_AD"));
				loginSuccessCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"LOGIN_SUCCESS_COUNT_AD"));
				loginSuccessCountUnique += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"LOGIN_SUCCESS_COUNT_UNIQUE_AD"));
				registCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"REGIST_COUNT_AD"));
				usedPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TOTAL_USED_POINT_AD"));
			} else {
				accessCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"ACCESS_COUNT"));
				loginCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"LOGIN_COUNT"));
				loginSuccessCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"LOGIN_SUCCESS_COUNT"));
				loginSuccessCountUnique += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"LOGIN_SUCCESS_COUNT_UNIQUE"));
				registCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"REGIST_COUNT"));
				usedPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TOTAL_USED_POINT"));
			}
			ordinarySalesAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"ORDINARY_SALES_AMT"));
			ocSalesAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"POINT_BACK_SALES_AMT"));
			totalSalesAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TOTAL_SALES_AMT"));

		} else if (e.Row.RowType == DataControlRowType.Footer) {
			e.Row.Cells[this.colAccessCountIndex].Text = accessCount.ToString();
			e.Row.Cells[this.colRegistCountIndex].Text = registCount.ToString();
			e.Row.Cells[this.colLoginCountIndex].Text = loginCount.ToString();
			e.Row.Cells[this.colLoginSuccessCountIndex].Text = loginSuccessCount.ToString();
			e.Row.Cells[this.colLoginSuccessCountUniqueIndex].Text = loginSuccessCountUnique.ToString();
			e.Row.Cells[this.colTotalUsedPointIndex].Text = usedPoint.ToString();
			e.Row.Cells[this.colOrdinarySalesAmtIndex].Text = ordinarySalesAmt.ToString();
			e.Row.Cells[this.colPointBackSalesAmtIndex].Text = ocSalesAmt.ToString();
			e.Row.Cells[this.colTotalSalesAmtIndex].Text = totalSalesAmt.ToString();
		}

		if (((DataSet)(((GridView)sender).DataSource)).Tables[0].Columns["AD_CD"] != null) {
			grdAdGroupDetail.Columns[this.colAdGroupIndex].FooterText = string.Empty;
		} else {
			grdAdGroupDetail.Columns[this.colAdGroupIndex].FooterText = "<合計>";
		}

		if (Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_PRODUCTION)) {
			e.Row.Cells[this.colTotalUsedPointIndex].Visible = false;
		}
	}

	protected void lstSiteCd_SelectedIndexChanged(object sender,EventArgs e) {
		lstAdGroup.Items.Clear();
		lstAdGroup.DataBind();
		lstAdGroup.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		btnListSeek_Click(sender,e);
	}

	protected void dsAdGroup_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
	}

	protected void dsAdGroupMember_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = lstAdGroup.SelectedValue;
	}

	protected void lstAdGroup_SelectedIndexChanged(object sender,EventArgs e) {
		//		lstAd.Items.Clear();
		//		lstAd.DataBind();
		//		lstAd.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		btnListSeek_Click(sender,e);
	}

	protected void lstAd_SelectedIndexChanged(object sender,EventArgs e) {
		btnListSeek_Click(sender,e);
	}

	private void GetList() {
		ClearTotal();
		using (AccessPage pAccess = new AccessPage()) {
			DataSet ds = pAccess.GetAdGroupDetailPerformanceList(
								lstSiteCd.SelectedValue,
								lstFromYYYYRegist.SelectedValue,
								lstFromMMRegist.SelectedValue,
								lstFromDDRegist.SelectedValue,
								lstToYYYYRegist.SelectedValue,
								lstToMMRegist.SelectedValue,
								lstToDDRegist.SelectedValue,
								lstFromYYYYPurchase.SelectedValue,
								lstFromMMPurchase.SelectedValue,
								lstFromDDPurchase.SelectedValue,
								lstToYYYYPurchase.SelectedValue,
								lstToMMPurchase.SelectedValue,
								lstToDDPurchase.SelectedValue,
								lstAdGroup.SelectedValue,
								txtAdCd.Text,
								this.SexCd,
								rdoAdGroupAll.Checked,
								IsAdManager
						);

			if (ds.Tables[0].Columns["AD_CD"] == null) {
				grdAdGroupDetail.Columns[this.colAdCdIndex].Visible = false;
				grdAdGroupDetail.Columns[this.colAdNmIndex].Visible = false;
			} else {
				grdAdGroupDetail.Columns[this.colAdCdIndex].Visible = true;
				grdAdGroupDetail.Columns[this.colAdNmIndex].Visible = true;
			}
			grdAdGroupDetail.DataSource = ds;
			grdAdGroupDetail.DataBind();
			if (this.IsManMode) {
				grdAdGroupDetail.Columns[this.colOrdinarySalesAmtIndex].Visible = true;
				grdAdGroupDetail.Columns[this.colPointBackSalesAmtIndex].Visible = true;
				grdAdGroupDetail.Columns[this.colTotalSalesAmtIndex].Visible = true;
			} else {
				grdAdGroupDetail.Columns[this.colOrdinarySalesAmtIndex].Visible = false;
				grdAdGroupDetail.Columns[this.colPointBackSalesAmtIndex].Visible = false;
				grdAdGroupDetail.Columns[this.colTotalSalesAmtIndex].Visible = false;
			}
		}
		btnCsv.Visible = (Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_SITE_OWNER) || Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_AD_MANAGE));
		pnlInfo.Visible = true;
	}

	private string GetCsvString(DataSet pDataSet) {

		DataTable oDataTable = pDataSet.Tables[0];

		GridViewCsvBuilder oCsvBuilder = new GridViewCsvBuilder(this.grdAdGroupDetail,oDataTable);

		oCsvBuilder.AppendMapping(this.colAdGroupIndex,"AD_GROUP_NM");
		if (oDataTable.Columns["AD_CD"] != null) {
			oCsvBuilder.AppendMapping(this.colAdCdIndex,"AD_CD");
			oCsvBuilder.AppendMapping(this.colAdNmIndex,"AD_NM");
		}
		oCsvBuilder.AppendMapping(this.colAccessCountIndex,this.IsAdManager ? "ACCESS_COUNT_AD" : "ACCESS_COUNT");
		oCsvBuilder.AppendMapping(this.colRegistCountIndex,this.IsAdManager ? "REGIST_COUNT_AD" : "REGIST_COUNT");
		oCsvBuilder.AppendMapping(this.colLoginCountIndex,this.IsAdManager ? "LOGIN_COUNT_AD" : "LOGIN_COUNT");
		oCsvBuilder.AppendMapping(this.colLoginSuccessCountIndex,this.IsAdManager ? "LOGIN_SUCCESS_COUNT_AD" : "LOGIN_SUCCESS_COUNT");
		oCsvBuilder.AppendMapping(this.colLoginSuccessCountUniqueIndex,this.IsAdManager ? "LOGIN_SUCCESS_COUNT_UNIQUE_AD" : "LOGIN_SUCCESS_COUNT_UNIQUE");
		oCsvBuilder.AppendMapping(this.colTotalUsedPointIndex,this.IsAdManager ? "TOTAL_USED_POINT_AD" : "TOTAL_USED_POINT");
		oCsvBuilder.AppendMapping(this.colOrdinarySalesAmtIndex,"ORDINARY_SALES_AMT");
		oCsvBuilder.AppendMapping(this.colPointBackSalesAmtIndex,"POINT_BACK_SALES_AMT");
		oCsvBuilder.AppendMapping(this.colTotalSalesAmtIndex,"TOTAL_SALES_AMT");

		return oCsvBuilder.ToString();

	}

	protected string GetAdLink() {
		if (rdoAdGroupAll.Checked) {
			return string.Empty;
		} else {
			return string.Format("~/StatusManager/CastPerformance.aspx?sitecd={0}&adgroup={1}&adcd={2}&from={3}&to={4}",lstSiteCd.SelectedValue,Eval("AD_GROUP_CD"),Eval("AD_CD"),lstFromYYYYPurchase.SelectedValue + "/" + lstFromMMPurchase.SelectedValue + "/" + lstFromDDPurchase.SelectedValue,lstToYYYYPurchase.SelectedValue + "/" + lstToMMPurchase.SelectedValue + "/" + lstToDDPurchase.SelectedValue);
		}
	}
	protected string GetAdNm() {
		if (rdoAdGroupAll.Checked) {
			return string.Empty;
		} else {
			return Eval("AD_CD").ToString();
		}
	}

	protected string GetValue(object pValue,object pAdValue) {
		object oValue = (IsAdManager ? pAdValue : pValue) ?? string.Empty;
		return oValue.ToString();
	}

	protected int GetValueWithConvertInt32(object pValue,object pAdValue) {
		string sValue = GetValue(pValue,pAdValue);
		int iResult = 0;
		return int.TryParse(sValue,out iResult) ? iResult : 0;
	}


}
