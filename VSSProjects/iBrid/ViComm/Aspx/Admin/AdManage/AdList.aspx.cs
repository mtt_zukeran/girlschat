﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 広告コードメンテナンス
--	Progaram ID		: AdList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using iBridCommLib;
using ViComm;

public partial class AdManage_AdList:System.Web.UI.Page {
	protected string[] YearArray;
	private int ListStartYear = 2009;
	private int ListEndYearRange = 10;
	protected static readonly string[] MonthArray = new string[] { "","01","02","03","04","05","06","07","08","09","10","11","12" };
	protected static readonly string[] DayArray = new string[] { "","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31" };
	private const string AD_MENU_TERM = "04";
	private string recCount = "";
	
	private string sAdStartDate;
	private string sAdEndDate;
	private string sPublishStartDate;
	private string sPublishEndDate;

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			CreateYearArray();
			FirstLoad();
			InitPage();
		}
	}

	private void FirstLoad() {
		grdAd.PageSize = 100;
		grdAd.DataSourceID = "";
		DataBind();
		lstAdGroup.DataSourceID = string.Empty;
		lstAdGroup.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		string sAdGroupCd = iBridUtil.GetStringValue(Request.QueryString["adgroupcd"]);
		if (!string.IsNullOrEmpty(sAdGroupCd)) {
			lstAdGroup.SelectedValue = sAdGroupCd;
			GetList();
		}

		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["adcd"]))) {
			txtSeekAdCd.Text = iBridUtil.GetStringValue(Request.QueryString["adcd"]);
			GetList();
		}
	}

	private void InitPage() {
		txtAdCd.Text = "";
		lstCastRegistReportTiming.DataBind();
		lstManRegistReportTiming.DataBind();
		ClearField();
		pnlMainte.Visible = false;
	}

	private void ClearField() {
		lblDuplicate.Visible = false;
		txtAdNm.Text = "";
		txtManConversionFee.Text = "0";
		txtWomanConversionFee.Text = "0";
		txtAdminId.Text = "";
		txtAdminPassword.Text = "";
		recCount = "0";
		lstCastRegistReportTiming.SelectedIndex = 0;
		lstManRegistReportTiming.SelectedIndex = 0;
		lstAdMenu.ClearSelection();
		lstSexCd.ClearSelection();
		lstAdStartDateY.SelectedIndex = 0;
		lstAdStartDateM.SelectedIndex = 0;
		lstAdStartDateD.SelectedIndex = 0;
		lstAdEndDateY.SelectedIndex = 0;
		lstAdEndDateM.SelectedIndex = 0;
		lstAdEndDateD.SelectedIndex = 0;
		lstPublishStartDateY.SelectedValue = DateTime.Now.ToString("yyyy");
		lstPublishStartDateM.SelectedValue = DateTime.Now.ToString("MM");
		lstPublishStartDateD.SelectedValue = DateTime.Now.ToString("dd");
		lstPublishEndDateY.SelectedIndex = 0;
		lstPublishEndDateM.SelectedIndex = 0;
		lstPublishEndDateD.SelectedIndex = 0;
		txtAdCost.Text = "0";
		lblErrorMessageAdTerm.Text = string.Empty;
		lblErrorMessageAdTerm.Visible = false;
		lblErrorMessagePublishStartDate.Text = string.Empty;
		lblErrorMessagePublishStartDate.Visible = false;
		lblErrorMessagePublishEndDate.Text = string.Empty;
		lblErrorMessagePublishEndDate.Visible = false;
		pnlAdTerm.Visible = false;
	}

	protected string GetRecCount() {
		return recCount;
	}

	private void GetList() {
		grdAd.DataSourceID = "dsAd";
		grdAd.PageIndex = 0;
		grdAd.DataBind();
		pnlCount.DataBind();
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		pnlMainte.Visible = true;
		pnlKey.Enabled = true;
		pnlDtl.Visible = false;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			if (CheckInput()) {
				UpdateData(0);
			}
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void lnkAdCd_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		txtAdCd.Text = sKeys[0];
		GetData();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void dsAd_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsAd_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = txtSeekAdCd.Text;
		e.InputParameters[1] = txtSeekAdNm.Text;
		e.InputParameters[2] = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		e.InputParameters[3] = lstAdGroup.SelectedValue;
		e.InputParameters[4] = usedSearch.SelectedValue;
	}

	protected void dsAdGroup_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(Session["MENU_SITE"]);
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("AD_GET");
			db.ProcedureInParm("PAD_CD",DbSession.DbType.VARCHAR2,txtAdCd.Text);
			db.ProcedureOutParm("PAD_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMAN_CONVERSION_FEE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PWOMAN_CONVERSION_FEE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PADMIN_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PADMIN_PASSWORD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PVIEW_SALES_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID_ADMIN",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO_ADMIN",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pCAST_REGIST_REPORT_TIMING",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pMAN_REGIST_REPORT_TIMING",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PUSED_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PNEED_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSIGNUP_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PAD_MENU",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSEX_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PAD_START_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PAD_END_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPUBLISH_START_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPUBLISH_END_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PAD_COST",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pUNNECESSARY_CERTIFY_FLAG",DbSession.DbType.NUMBER);

			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");
			ViewState["REVISION_NO_ADMIN"] = db.GetStringValue("PREVISION_NO_ADMIN");
			ViewState["ROWID_ADMIN"] = db.GetStringValue("PROWID_ADMIN");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtAdNm.Text = db.GetStringValue("PAD_NM");
				txtManConversionFee.Text = db.GetStringValue("PMAN_CONVERSION_FEE");
				txtWomanConversionFee.Text = db.GetStringValue("PWOMAN_CONVERSION_FEE");
				txtAdminId.Text = db.GetStringValue("PADMIN_ID");
				txtAdminPassword.Text = db.GetStringValue("PADMIN_PASSWORD");
				chkViewSalesFlag.Checked = db.GetStringValue("PVIEW_SALES_FLAG").Equals(ViCommConst.FLAG_ON_STR);
				string sCastRegistReportTiming = db.GetStringValue("pCAST_REGIST_REPORT_TIMING");
				string sManRegistReportTiming = db.GetStringValue("pMAN_REGIST_REPORT_TIMING");
				usedFlag.SelectedIndex = int.Parse(db.GetStringValue("PUSED_FLAG"));
				signUpFlag.SelectedIndex = int.Parse(db.GetStringValue("PSIGNUP_FLAG"));
				chkUnnecessaryCertifyFlag.Checked = db.GetStringValue("pUNNECESSARY_CERTIFY_FLAG").Equals(ViCommConst.FLAG_ON_STR);
				
				if (!string.IsNullOrEmpty(sCastRegistReportTiming)) {
					lstCastRegistReportTiming.SelectedValue = sCastRegistReportTiming;
				} else {
					lstCastRegistReportTiming.SelectedIndex = 0;
				}

				if (!string.IsNullOrEmpty(sManRegistReportTiming)) {
					lstManRegistReportTiming.SelectedValue = sManRegistReportTiming;
				} else {
					lstManRegistReportTiming.SelectedIndex = 0;
				}

				lstAdMenu.SelectedValue = db.GetStringValue("PAD_MENU");
				if (lstAdMenu.SelectedValue.Equals(AD_MENU_TERM)) {
					pnlAdTerm.Visible = true;
				} else {
					pnlAdTerm.Visible = false;
				}
				
				if (!db.GetStringValue("PSEX_CD").Equals("-")) {
					lstSexCd.SelectedValue = db.GetStringValue("PSEX_CD");
				}
				
				if (!string.IsNullOrEmpty(db.GetStringValue("pAD_START_DATE"))) {
					DateTime dtAdStartDate;
					DateTime.TryParse(db.GetStringValue("pAD_START_DATE"),out dtAdStartDate);
					lstAdStartDateY.SelectedValue = dtAdStartDate.Year.ToString();
					lstAdStartDateM.SelectedValue = string.Format("{0:D2}",dtAdStartDate.Month);
					lstAdStartDateD.SelectedValue = string.Format("{0:D2}",dtAdStartDate.Day);
				}

				if (!string.IsNullOrEmpty(db.GetStringValue("pAD_END_DATE"))) {
					DateTime dtAdEndDate;
					DateTime.TryParse(db.GetStringValue("pAD_END_DATE"),out dtAdEndDate);
					lstAdEndDateY.SelectedValue = dtAdEndDate.Year.ToString();
					lstAdEndDateM.SelectedValue = string.Format("{0:D2}",dtAdEndDate.Month);
					lstAdEndDateD.SelectedValue = string.Format("{0:D2}",dtAdEndDate.Day);
				}

				if (!string.IsNullOrEmpty(db.GetStringValue("pPUBLISH_START_DATE"))) {
					DateTime dtPublishStartDate;
					DateTime.TryParse(db.GetStringValue("pPUBLISH_START_DATE"),out dtPublishStartDate);
					lstPublishStartDateY.SelectedValue = dtPublishStartDate.Year.ToString();
					lstPublishStartDateM.SelectedValue = string.Format("{0:D2}",dtPublishStartDate.Month);
					lstPublishStartDateD.SelectedValue = string.Format("{0:D2}",dtPublishStartDate.Day);
				}

				if (!string.IsNullOrEmpty(db.GetStringValue("pPUBLISH_END_DATE"))) {
					DateTime dtPublishEndDate;
					DateTime.TryParse(db.GetStringValue("pPUBLISH_END_DATE"),out dtPublishEndDate);
					lstPublishEndDateY.SelectedValue = dtPublishEndDate.Year.ToString();
					lstPublishEndDateM.SelectedValue = string.Format("{0:D2}",dtPublishEndDate.Month);
					lstPublishEndDateD.SelectedValue = string.Format("{0:D2}",dtPublishEndDate.Day);
				}
				
				txtAdCost.Text = iBridUtil.GetStringValue(db.GetIntValue("PAD_COST"));

			} else {
				ClearField();
				string sCastRegistReportTiming = string.Empty;
				string sManRegistReportTiming = string.Empty;

				using (Site oSite = new Site()) {
					if (oSite.GetValue(Session["MENU_SITE"].ToString(),"CAST_REGIST_REPORT_TIMING",ref sCastRegistReportTiming)) {
						for (int i = 0;i < lstCastRegistReportTiming.Items.Count;i++) {
							if (lstCastRegistReportTiming.Items[i].Value == sCastRegistReportTiming) {
								lstCastRegistReportTiming.SelectedIndex = i;
							}
						}
					}
					if (oSite.GetValue(Session["MENU_SITE"].ToString(),"MAN_REGIST_REPORT_TIMING",ref sManRegistReportTiming)) {
						for (int i = 0;i < lstManRegistReportTiming.Items.Count;i++) {
							if (lstManRegistReportTiming.Items[i].Value == sManRegistReportTiming) {
								lstManRegistReportTiming.SelectedIndex = i;
							}
						}
					}
				}
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;
		DataBind();
	}

	private void UpdateData(int pDelFlag) {
		string sResult;
		using (DbSession db = new DbSession()) {
			
			db.PrepareProcedure("AD_MAINTE");
			db.ProcedureInParm("PAD_CD",DbSession.DbType.VARCHAR2,txtAdCd.Text);
			db.ProcedureInParm("PAD_NM",DbSession.DbType.VARCHAR2,txtAdNm.Text);
			db.ProcedureInParm("PMAN_CONVERSION_FEE",DbSession.DbType.NUMBER,int.Parse(txtManConversionFee.Text));
			db.ProcedureInParm("PWOMAN_CONVERSION_FEE",DbSession.DbType.NUMBER,int.Parse(txtWomanConversionFee.Text));
			db.ProcedureInParm("PADMIN_ID",DbSession.DbType.VARCHAR2,txtAdminId.Text);
			db.ProcedureInParm("PADMIN_PASSWORD",DbSession.DbType.VARCHAR2,txtAdminPassword.Text);
			db.ProcedureInParm("PVIEW_SALES_FLAG",DbSession.DbType.NUMBER,chkViewSalesFlag.Checked);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PROWID_ADMIN",DbSession.DbType.VARCHAR2,ViewState["ROWID_ADMIN"].ToString());
			db.ProcedureInParm("PREVISION_NO_ADMIN",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO_ADMIN"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureInParm("pCAST_REGIST_REPORT_TIMING",DbSession.DbType.VARCHAR2,lstCastRegistReportTiming.SelectedValue);
			db.ProcedureInParm("pMAN_REGIST_REPORT_TIMING",DbSession.DbType.VARCHAR2,lstManRegistReportTiming.SelectedValue);
			db.ProcedureInParm("PUSED_FLAG",DbSession.DbType.NUMBER,usedFlag.SelectedIndex);
			db.ProcedureInParm("PNEED_FLAG",DbSession.DbType.NUMBER,0);
			db.ProcedureInParm("PSIGNUP_FLAG",DbSession.DbType.NUMBER,signUpFlag.SelectedIndex);
			db.ProcedureInParm("PAD_MENU",DbSession.DbType.VARCHAR2,lstAdMenu.SelectedValue);
			db.ProcedureInParm("PSEX_CD",DbSession.DbType.VARCHAR2,lstSexCd.SelectedValue);
			db.ProcedureInParm("PAD_START_DATE",DbSession.DbType.VARCHAR2,sAdStartDate);
			db.ProcedureInParm("PAD_END_DATE",DbSession.DbType.VARCHAR2,sAdEndDate);
			db.ProcedureInParm("PPUBLISH_START_DATE",DbSession.DbType.VARCHAR2,sPublishStartDate);
			db.ProcedureInParm("PPUBLISH_END_DATE",DbSession.DbType.VARCHAR2,sPublishEndDate);
			db.ProcedureInParm("PAD_COST",DbSession.DbType.NUMBER,int.Parse(txtAdCost.Text));
			db.ProcedureInParm("pUNNECESSARY_CERTIFY_FLAG",DbSession.DbType.NUMBER,chkUnnecessaryCertifyFlag.Checked);			
			db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			sResult = db.GetStringValue("PRESULT");
		}
		if (sResult.Equals("0")) {
			InitPage();
			GetList();
		} else {
			lblDuplicate.Visible = true;
		}
	}

	protected void vdcInsert_ServerValidate(object sender,ServerValidateEventArgs e) {
		if (!string.IsNullOrEmpty(ViewState["ROWID"].ToString())) {
			return;
		}

		using (Ad oAd = new Ad()) {
			string sAdNm = "";
			e.IsValid = !oAd.IsExist(txtAdCd.Text,ref sAdNm);
		}
	}

	protected void lstAdMenu_IndexChanged(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}

		if (oDropDownList.SelectedValue.Equals(AD_MENU_TERM)) {
			pnlAdTerm.Visible = true;
		} else {
			pnlAdTerm.Visible = false;
		}
	}
	
	private void CreateYearArray() {
		List<string> listYear = new List<string>();
		
		listYear.Add("");
		for (int i = ListStartYear;i <= DateTime.Now.Year + ListEndYearRange; i++) {
			listYear.Add(i.ToString());
		}
		
		YearArray = listYear.ToArray();
	}
	
	private bool CheckInput() {
		bool bOk = true;
		DateTime dtPublishStartDate;
		DateTime dtPublishEndDate;
		bool bPublishStartInputFlag = false;

		if (lstAdMenu.SelectedValue.Equals(AD_MENU_TERM)) {
			DateTime dtAdStartDate;
			DateTime dtAdEndDate;
			sAdStartDate = string.Empty;
			sAdEndDate = string.Empty;
			sPublishStartDate = string.Empty;
			sPublishEndDate = string.Empty;
			bool bAdStartInputFlag = false;

			if (!string.IsNullOrEmpty(lstAdStartDateY.SelectedValue) || !string.IsNullOrEmpty(lstAdStartDateM.SelectedValue) || !string.IsNullOrEmpty(lstAdStartDateD.SelectedValue)) {
				bAdStartInputFlag = true;
				sAdStartDate = string.Format("{0}/{1}/{2}",lstAdStartDateY.SelectedValue,lstAdStartDateM.SelectedValue,lstAdStartDateD.SelectedValue);
				if (!DateTime.TryParse(sAdStartDate,out dtAdStartDate)) {
					bOk = false;
					lblErrorMessageAdTerm.Visible = true;
					lblErrorMessageAdTerm.Text = "期間開始を正しく入力してください。";

				} else {
					if (!string.IsNullOrEmpty(lstAdEndDateY.SelectedValue) || !string.IsNullOrEmpty(lstAdEndDateM.SelectedValue) || !string.IsNullOrEmpty(lstAdEndDateD.SelectedValue)) {
						sAdEndDate = string.Format("{0}/{1}/{2}",lstAdEndDateY.SelectedValue,lstAdEndDateM.SelectedValue,lstAdEndDateD.SelectedValue);
						if (!DateTime.TryParse(sAdEndDate,out dtAdEndDate)) {
							bOk = false;
							lblErrorMessageAdTerm.Visible = true;
							lblErrorMessageAdTerm.Text = "期間終了を正しく入力してください。";
						} else if (bAdStartInputFlag && dtAdStartDate > dtAdEndDate) {
							bOk = false;
							lblErrorMessageAdTerm.Visible = true;
							lblErrorMessageAdTerm.Text = "期間の大小関係が正しくありません。";
						}
					} else {
						bOk = false;
						lblErrorMessageAdTerm.Visible = true;
						lblErrorMessageAdTerm.Text = "期間終了を入力してください。";
					}
				}
			} else {
				bOk = false;
				lblErrorMessageAdTerm.Visible = true;
				lblErrorMessageAdTerm.Text = "期間開始を入力してください。";
			}
		}

		if (!string.IsNullOrEmpty(lstPublishStartDateY.SelectedValue) || !string.IsNullOrEmpty(lstPublishStartDateM.SelectedValue) || !string.IsNullOrEmpty(lstPublishStartDateD.SelectedValue)) {
			bPublishStartInputFlag = true;
			sPublishStartDate = string.Format("{0}/{1}/{2}",lstPublishStartDateY.SelectedValue,lstPublishStartDateM.SelectedValue,lstPublishStartDateD.SelectedValue);
			if (!DateTime.TryParse(sPublishStartDate,out dtPublishStartDate)) {
				bOk = false;
				lblErrorMessagePublishStartDate.Visible = true;
				lblErrorMessagePublishStartDate.Text = "掲載開始日を正しく入力してください。";
			}

			if (!string.IsNullOrEmpty(lstPublishEndDateY.SelectedValue) || !string.IsNullOrEmpty(lstPublishEndDateM.SelectedValue) || !string.IsNullOrEmpty(lstPublishEndDateD.SelectedValue)) {
				sPublishEndDate = string.Format("{0}/{1}/{2}",lstPublishEndDateY.SelectedValue,lstPublishEndDateM.SelectedValue,lstPublishEndDateD.SelectedValue);
				if (!DateTime.TryParse(sPublishEndDate,out dtPublishEndDate)) {
					bOk = false;
					lblErrorMessagePublishEndDate.Visible = true;
					lblErrorMessagePublishEndDate.Text = "広告停止日を正しく入力してください。";
				} else if (bPublishStartInputFlag && dtPublishStartDate > dtPublishEndDate) {
					bOk = false;
					lblErrorMessagePublishStartDate.Visible = true;
					lblErrorMessagePublishStartDate.Text = "掲載期間の大小関係が正しくありません。";
				}
			}
		}
		
		return bOk;
	}

}