﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 広告グループメンテナンス
--	Progaram ID		: AdGroupList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class AdManage_AdGroupList:System.Web.UI.Page {
	private string recCount = "";

	private int CompanyMask {
		get {
			return int.Parse(Session["CompanyMask"].ToString());
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
	}

	private void FirstLoad() {
		this.tdDataPrepaidAdGroup.Visible = false;
		this.tdHeaderPreapidAdGroup.Visible = false;
		grdAdGroup.PageSize = 999;
		string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		if (sSiteCd.Equals("")) {
			if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
				lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			} else {
				lstSeekSiteCd.SelectedIndex = 0;
			}
		} else {
			lstSeekSiteCd.SelectedValue = sSiteCd;
		}
		DataBind();

		lstSiteCd.DataSourceID = "";
		lstSiteCd.SelectedIndex = 0;

		lstWebFaceSeq.DataSourceID = "";
		lstWebFaceSeq.SelectedIndex = 0;
	}

	private void InitPage() {
		txtAdGroupCd.Text = "";
		ClearField();
		pnlMainte.Visible = false;
		chkPrepaidAdGroupFlag.Visible = false;
		trPrepaidAdGroup.Visible = false;
	}

	private void ClearField() {
		txtAdGroupNm.Text = "";
		if (lstWebFaceSeq.Items.Count > 0) {
			lstWebFaceSeq.SelectedIndex = 0;
		}
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	private void GetList() {
		grdAdGroup.PageIndex = 0;
		grdAdGroup.DataBind();
		pnlCount.DataBind();
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		pnlMainte.Visible = true;
		pnlKey.Enabled = true;
		pnlDtl.Visible = false;
		lstSiteCd.SelectedIndex = lstSeekSiteCd.SelectedIndex;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void lnkAdGroupCd_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		lstSiteCd.SelectedValue = sKeys[0];
		txtAdGroupCd.Text = sKeys[1];
		GetData();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void dsAdGroup_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}


	protected void dsAdGroup_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
		int iCompanyMask = int.Parse(Session["CompanyMask"].ToString());
	}


	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("AD_GROUP_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PAD_GROUP_CD",DbSession.DbType.VARCHAR2,txtAdGroupCd.Text);
			db.ProcedureOutParm("PAD_GROUP_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PWEB_FACE_SEQ",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PPREPAID_GROUP_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtAdGroupNm.Text = db.GetStringValue("PAD_GROUP_NM");
				if (!db.GetStringValue("PWEB_FACE_SEQ").Equals("")) {
					lstWebFaceSeq.SelectedValue = db.GetStringValue("PWEB_FACE_SEQ");
				}
				chkPrepaidAdGroupFlag.Checked = (db.GetIntValue("PPREPAID_GROUP_FLAG") != 0);
			} else {
				ClearField();
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		lstSeekSiteCd.SelectedIndex = lstSiteCd.SelectedIndex;
		pnlKey.Enabled = false;
		DataBind();
	}

	private void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("AD_GROUP_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PAD_GROUP_CD",DbSession.DbType.VARCHAR2,txtAdGroupCd.Text);
			db.ProcedureInParm("PAD_GROUP_NM",DbSession.DbType.VARCHAR2,txtAdGroupNm.Text);
			db.ProcedureInParm("PWEB_FACE_SEQ",DbSession.DbType.VARCHAR2,lstWebFaceSeq.SelectedValue);
			db.ProcedureInParm("PPREPAID_GROUP_FLAG",DbSession.DbType.VARCHAR2,chkPrepaidAdGroupFlag.Checked);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		int iIdx = lstSiteCd.SelectedIndex;
		InitPage();
		GetList();
		lstSeekSiteCd.SelectedIndex = iIdx;
	}
}
