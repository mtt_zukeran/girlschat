﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PaymentByEntDailyList.aspx.cs" Inherits="AdManage_PaymentByEntDailyList" Title="出演者入会月別報酬一覧(日別)"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="出演者入会月別報酬一覧(日別)"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
                            <td class="tdHeaderStyle">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                表示月
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstYear" runat="server" Width="60px">
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstMonth" runat="server" Width="40px">
                                </asp:DropDownList>月
                                <br />
								<asp:Label ID="lblErrorMessageReportDayFrom" runat="server" ForeColor="red" Visible="false"></asp:Label>
                            </td>
                        </tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
			</asp:Panel>
		</fieldset>
		<fieldset>
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="560px">
				<asp:GridView ID="grdPaymentByEntDaily" runat="server" AutoGenerateColumns="False" DataSourceID="dsPaymentByEntDaily" CellPadding="0" ShowFooter="True" AllowSorting="True"
					OnRowDataBound="grdPaymentByEntDaily_RowDataBound" SkinID="GridViewColorNoRowStyle">
					<Columns>
						<asp:TemplateField HeaderText="日付">
							<ItemStyle HorizontalAlign="Center" Width="40px" />
							<ItemTemplate>
								<asp:Label ID="Label1" runat="server" Text='<%# GetDisplayReportDay(Eval("DAYS").ToString()) %>' BackColor=''></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="曜日" FooterText="合計">
							<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
							<ItemTemplate>
								<asp:Label ID="Label2" runat="server" Text='<%# GetDisplayDayOfWeek(Eval("DAYS").ToString()) %>' BackColor=''></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="報酬">
							<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("PAYMENT_AMT") %>' BackColor=''></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="件数">
							<ItemTemplate>
								<asp:Label ID="Label4" runat="server" Text='<%# Eval("PAYMENT_COUNT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
						</asp:TemplateField>
					</Columns>
					<FooterStyle ForeColor="Black" BackColor="LightYellow" />
				</asp:GridView>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsPaymentByEntDaily" runat="server" SelectMethod="GetPaymentList" TypeName="PaymentByEntDaily" OnSelecting="dsPaymentByEntDaily_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
</asp:Content>
