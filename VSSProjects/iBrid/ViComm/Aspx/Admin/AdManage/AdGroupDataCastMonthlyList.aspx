﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AdGroupDataCastMonthlyList.aspx.cs" Inherits="AdManage_AdGroupDataCastMonthlyList" Title="代理店別集計(出演者)" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="代理店別集計(出演者)"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[検索条件]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 800px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                表示月
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstYear" runat="server" Width="60px">
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstMonth" runat="server" Width="40px">
                                </asp:DropDownList>月
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" />
            </fieldset>
        </asp:Panel>
		<asp:Panel runat="server" ID="pnlList">
			<fieldset>
				<legend>[集計]</legend>
				<asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdAdDataCastMonthly.PageIndex + 1%>
                        of
                        <%= grdAdDataCastMonthly.PageCount%>
                    </a>
                </asp:Panel>
                &nbsp;<br />
				<asp:Panel ID="pnlGrid" runat="server">
					<asp:GridView ID="grdAdDataCastMonthly" DataSourceID="dsAdDataCastMonthly" runat="server" AllowPaging="true" AutoGenerateColumns="False" PageSize="9999" OnSorting="grdAdDataCastMonthly_Sorting"
						EnableSortingAndPagingCallbacks="false" ShowFooter="true" SkinID="GridViewColor" AllowSorting="true" OnRowDataBound="grdAdDataCastMonthly_RowDataBound">
						<Columns>
							<asp:TemplateField HeaderText="代理店名">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:HyperLink ID="lnkAdGroupNm" runat="server" NavigateUrl='<%# string.Format("~/AdManage/AdDataCastMonthlyList.aspx?sitecd={0}&adgroupcd={1}&year={2}&month={3}",Eval("SITE_CD"),Eval("AD_GROUP_CD"),lstYear.SelectedValue,lstMonth.SelectedValue) %>' Text='<%# Eval("AD_GROUP_NM") %>' ></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle Wrap="false" />
								<FooterTemplate>
									<asp:Label ID="lblAdGroupNmFooter1" Text="グループ無し" runat="server"></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblAdGroupNmFooter2" Text="合計" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="広告費" SortExpression="AD_COST">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblAdCost" runat="server" Text='<%# Eval("AD_COST") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblAdCostFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label2" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblAdCostFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ｱｸｾｽ数" SortExpression="ACCESS_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblAccessCount" runat="server" Text='<%# Eval("ACCESS_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblAccessCountFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label3" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblAccessCountFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="登録数" SortExpression="REGIST_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblRegistCount" runat="server" Text='<%# Eval("REGIST_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblRegistCountFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label4" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblRegistCountFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="登録率" SortExpression="REGIST_RATE">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblRegistRate" runat="server" Text='<%# AddPercentMark(Eval("REGIST_RATE").ToString()) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblRegistRateFooter1" runat="server"></asp:Label>%
									<asp:Label ID="Label5" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblRegistRateFooter2" runat="server"></asp:Label>%
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="認証数" SortExpression="REGIST_COUNT_AD">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblRegistCountAd" runat="server" Text='<%# Eval("REGIST_COUNT_AD") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblRegistCountAdFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label6" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblRegistCountAdFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="獲得単価" SortExpression="AD_COST_BY_REGIST">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblAdCostByRegist" runat="server" Text='<%# Eval("AD_COST_BY_REGIST") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblAdCostByRegistFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label7" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblAdCostByRegistFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾛｸﾞｲﾝ数" SortExpression="LOGIN_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblLoginCount" runat="server" Text='<%# Eval("LOGIN_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblLoginCountFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label8" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblLoginCountFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾎﾟｲﾝﾄ獲得<br>人数" SortExpression="GET_POINT_CAST_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblGetPointCastCount" runat="server" Text='<%# Eval("GET_POINT_CAST_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblGetPointCastCountFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label9" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblGetPointCastCountFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾎﾟｲﾝﾄ獲得<br>獲得率" SortExpression="GET_POINT_CAST_RATE">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblGetPointCastRate" runat="server" Text='<%# AddPercentMark(Eval("GET_POINT_CAST_RATE").ToString()) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblGetPointCastRateFooter1" runat="server"></asp:Label>%
									<asp:Label ID="Label10" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblGetPointCastRateFooter2" runat="server"></asp:Label>%
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="初顧客<br>人数" SortExpression="START_PAYMENT_CAST_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblStartPaymentCastCount" runat="server" Text='<%# Eval("START_PAYMENT_CAST_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblStartPaymentCastCountFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label11" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblStartPaymentCastCountFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="初顧客<br>報酬率" SortExpression="START_PAYMENT_CAST_RATE">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblStartPaymentCastRate" runat="server" Text='<%# AddPercentMark(Eval("START_PAYMENT_CAST_RATE").ToString()) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblStartPaymentCastRateFooter1" runat="server"></asp:Label>%
									<asp:Label ID="Label12" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblStartPaymentCastRateFooter2" runat="server"></asp:Label>%
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="顧客単価" SortExpression="AD_COST_BY_START_PAYMENT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblAdCostByStartPayment" runat="server" Text='<%# Eval("AD_COST_BY_START_PAYMENT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblAdCostByStartPaymentFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label13" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblAdCostByStartPaymentFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="当月報酬<br>人数" SortExpression="ENT_MONTH_PAYMENT_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblEntMonthPaymentCount" runat="server" Text='<%# Eval("ENT_MONTH_PAYMENT_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblEntMonthPaymentCountFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label14" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblEntMonthPaymentCountFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="当月報酬<br>金額" SortExpression="ENT_MONTH_PAYMENT_AMT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblEntMonthPaymentAmt" runat="server" Text='<%# Eval("ENT_MONTH_PAYMENT_AMT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblEntMonthPaymentAmtFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblEntMonthPaymentAmtFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="当月回収率" SortExpression="ENT_MONTH_RETURN_RATE">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblEntMonthReturnRate" runat="server" Text='<%# AddPercentMark(Eval("ENT_MONTH_RETURN_RATE").ToString()) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblEntMonthReturnRateFooter1" runat="server"></asp:Label>%
									<asp:Label ID="Label16" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblEntMonthReturnRateFooter2" runat="server"></asp:Label>%
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾘﾋﾟｰﾄ報酬<br>人数" SortExpression="REPEAT_PAYMENT_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblRepeatPaymentCount" runat="server" Text='<%# Eval("REPEAT_PAYMENT_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblRepeatPaymentCountFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label17" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblRepeatPaymentCountFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾘﾋﾟｰﾄ報酬<br>金額" SortExpression="REPEAT_PAYMENT_AMT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblRepeatPaymentAmt" runat="server" Text='<%# Eval("REPEAT_PAYMENT_AMT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblRepeatPaymentAmtFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label18" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblRepeatPaymentAmtFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="合計報酬<br>人数" SortExpression="TOTAL_PAYMENT_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblTotalPaymentCount" runat="server" Text='<%# Eval("TOTAL_PAYMENT_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblTotalPaymentCountFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label19" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblTotalPaymentCountFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="合計報酬<br>金額" SortExpression="TOTAL_PAYMENT_AMT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblTotalPaymentAmt" runat="server" Text='<%# Eval("TOTAL_PAYMENT_AMT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblTotalPaymentAmtFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label20" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblTotalPaymentAmtFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾘﾋﾟｰﾄ率" SortExpression="REPEAT_RATE">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblRepeatRate" runat="server" Text='<%# AddPercentMark(Eval("REPEAT_RATE").ToString()) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblRepeatRateFooter1" runat="server"></asp:Label>%
									<asp:Label ID="Label21" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblRepeatRateFooter2" runat="server"></asp:Label>%
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="累計広告比率" SortExpression="AGGR_AD_COST_RATE">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblAggrAdCostRate" runat="server" Text='<%# AddPercentMark(Eval("AGGR_AD_COST_RATE").ToString()) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblAggrAdRateFooter1" runat="server"></asp:Label>%
									<asp:Label ID="Label22" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblAggrAdRateFooter2" runat="server"></asp:Label>%
								</FooterTemplate>
							</asp:TemplateField>
						</Columns>
					</asp:GridView>
				</asp:Panel>
			</fieldset>
			<br />
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsAdDataCastMonthly" runat="server" ConvertNullToDBNull="false" EnablePaging="true" OnSelecting="dsAdDataCastMonthly_Selecting" OnSelected="dsAdDataCastMonthly_Selected"
		SelectMethod="GetPageCollectionByAdGroup" SelectCountMethod="GetPageCountByAdGroup" SortParameterName="" TypeName="AdDataCastMonthly">
		<SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
</asp:Content>

