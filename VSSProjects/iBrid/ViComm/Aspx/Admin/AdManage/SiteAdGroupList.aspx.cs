﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 広告グループ割当
--	Progaram ID		: SiteAdGroupList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class AdManage_SiteAdGroupList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
		}
	}

	private void FirstLoad() {
		grdAd.PageSize = int.Parse(Session["PageSize"].ToString());
		grdAd.DataSourceID = "";
		DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void InitPage() {
		lstSiteCd.SelectedIndex = 0;
		txtAdCd.Text = "";
		ClearField();
		pnlMainte.Visible = false;
	}

	private void ClearField() {
		if (lstAdGroupCd.Items.Count > 0) {
			lstAdGroupCd.SelectedIndex = 0;
		}
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	private void GetList() {
		grdAd.DataSourceID = "dsAd";
		grdAd.PageIndex = 0;
		grdAd.DataBind();
		pnlCount.DataBind();
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		if (IsValid) {
			GetData();
		}
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		pnlMainte.Visible = true;
		pnlKey.Enabled = true;
		pnlDtl.Visible = false;
		lstSiteCd.SelectedIndex = lstSeekSiteCd.SelectedIndex;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void lnkAdCd_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		if (!sKeys[0].Equals("")) {
			lstSiteCd.SelectedValue = sKeys[0];
		} else {
			lstSiteCd.SelectedValue = lstSeekSiteCd.SelectedValue;
		}
		txtAdCd.Text = sKeys[1];
		GetData();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void vdcAdCd_ServerValidate(object source,ServerValidateEventArgs args) {
		using (Ad oAd = new Ad()) {
			string sAdNm = "";
			args.IsValid = oAd.IsExist(txtAdCd.Text,ref sAdNm);
		}
	}

	protected void dsAd_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsAdGroup_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
	}

	protected void dsAd_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		grdAd.PageSize = 100;
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
		e.InputParameters[1] = txtSeekAdCd.Text;
		e.InputParameters[2] = txtSeekAdNm.Text;
		e.InputParameters[3] = chkNoAssign.Checked;
	}


	private void GetData() {
		string sAdGroupCd = "";
		int iRecordCount = 0;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SITE_AD_GROUP_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PAD_CD",DbSession.DbType.VARCHAR2,txtAdCd.Text);
			db.ProcedureOutParm("PAD_GROUP_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PAD_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			lblAdNm.Text = db.GetStringValue("PAD_NM");

			sAdGroupCd = db.GetStringValue("PAD_GROUP_CD");
			iRecordCount = int.Parse(db.GetStringValue("PRECORD_COUNT"));


		}
		lstAdGroupCd.DataBind();
		lstAdGroupCd.Items.Insert(0, new ListItem(string.Empty, string.Empty));
		lstAdGroupCd.SelectedIndex = 0;
		if (iRecordCount > 0) {
			if (!sAdGroupCd.Equals("")) {
				lstAdGroupCd.SelectedValue = sAdGroupCd;
			}
		} else {
			ClearField();
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		lstSeekSiteCd.SelectedIndex = lstSiteCd.SelectedIndex;
		pnlKey.Enabled = false;
	}

	private void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SITE_AD_GROUP_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PAD_CD",DbSession.DbType.VARCHAR2,txtAdCd.Text);
			db.ProcedureInParm("PAD_GROUP_CD",DbSession.DbType.VARCHAR2,lstAdGroupCd.SelectedValue);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		int iIdx = lstSiteCd.SelectedIndex;
		InitPage();
		GetList();
		lstSeekSiteCd.SelectedIndex = iIdx;
	}
}
