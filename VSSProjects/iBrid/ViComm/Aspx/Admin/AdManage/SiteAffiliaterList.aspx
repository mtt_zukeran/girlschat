<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SiteAffiliaterList.aspx.cs" Inherits="AdManage_SiteAffiliaterList"
	Title="アフリエータ−広告コード割当" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="アフリエータ−広告コード割当"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2" style="height: 26px">
									サイトコード
								</td>
								<td class="tdDataStyle" style="height: 26px">
									<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
									</asp:DropDownList>
								</td>
								<td class="tdHeaderStyle2" style="height: 26px">
									アフリエター
								</td>
								<td class="tdDataStyle" style="height: 26px">
									<asp:DropDownList ID="lstAffiliaterCd" runat="server" DataSourceID="dsAffiliater" DataTextField="AFFILIATER_NM" DataValueField="AFFILIATER_CD" Width="180px">
									</asp:DropDownList>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[アフリエータ−広告コード割当]</legend>
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									広告コード
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtAdCd" runat="server" MaxLength="32" Width="60px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrAdCd" runat="server" ErrorMessage="広告コードを入力して下さい。" ControlToValidate="txtAdCd" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeAdCd" runat="server" ErrorMessage="広告コードは32桁以下の英数字で入力して下さい。" ValidationExpression="[a-zA-Z0-9-\/_]{1,32}" ControlToValidate="txtAdCd"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
									<asp:CustomValidator ID="vdcAdCd" runat="server" ControlToValidate="txtAdCd" ErrorMessage="広告コードが未登録です。" OnServerValidate="vdcAdCd_ServerValidate" ValidationGroup="Detail">*</asp:CustomValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									広告名
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblAdNm" runat="server" Text=""></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									トラッキング追加情報
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtTrackingAddtionInfo" runat="server" MaxLength="256" Width="200px"></asp:TextBox>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[割当一覧]</legend>
			<table border="0" style="width: 600px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle2">
						サイトコード
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
						</asp:DropDownList>
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnRegist" Text="広告コード割当" CssClass="seekbutton" CausesValidation="False" OnClick="btnRegist_Click" />
			<br />
			<br />
			<asp:GridView ID="grdSiteAffiliater" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsSiteAffiliater" AllowSorting="True"
				SkinID="GridViewColor">
				<Columns>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkAffiliaterCd" runat="server" Text='<%# string.Format("{0} {1}",Eval("AFFILIATER_CD"),Eval("AFFILIATER_NM")) %>' CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("AFFILIATER_CD"))  %>'
								OnCommand="lnkAffiliaterCd_Command" CausesValidation="False">
							</asp:LinkButton>
						</ItemTemplate>
						<HeaderTemplate>
							アフリエター
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:BoundField DataField="AD_CD" HeaderText="広告コード">
						<ItemStyle HorizontalAlign="Left" />
					</asp:BoundField>
					<asp:BoundField DataField="AD_NM" HeaderText="広告名">
						<ItemStyle HorizontalAlign="Left" />
					</asp:BoundField>
					<asp:BoundField DataField="UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br>
				<a class="reccount">Current viewing page
					<%=grdSiteAffiliater.PageIndex + 1%>
					of
					<%=grdSiteAffiliater.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsAffiliater" runat="server" SelectMethod="GetList" TypeName="Affiliater"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSiteAffiliater" runat="server" SelectMethod="GetPageCollection" TypeName="SiteAffiliater" SelectCountMethod="GetPageCount"
		EnablePaging="True" OnSelected="dsSiteAffiliater_Selected" OnSelecting="dsSiteAffiliater_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrAdCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeAdCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdcAdCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
