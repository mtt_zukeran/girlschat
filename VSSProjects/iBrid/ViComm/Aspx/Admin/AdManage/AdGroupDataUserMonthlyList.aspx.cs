﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者ページビュー表示(時間別)
--	Progaram ID		: AdGroupDataUserMonthlyList
--  Creation Date	: 2014.06.17
--  Creater			: Y.Ikemiya
**************************************************************************/
using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using ViComm.Extension.Pwild;

public partial class AdManage_AdGroupDataUserMonthlyList:System.Web.UI.Page {

	private string recCount = string.Empty;

	private double AdCost = 0;
	private double AccessCount = 0;
	private double RegistCount = 0;
	private double RegistCountAd = 0;
	private double LoginCount = 0;
	private double ChargePointUserCount = 0;
	private double StartReceiptUserCount = 0;
	private double EntMonthReceiptCount = 0;
	private double EntMonthReceiptAmt = 0;
	private double RepeatReceiptCount = 0;
	private double RepeatReceiptAmt = 0;
	private double TotalReceiptCount = 0;
	private double TotalReceiptAmt = 0;
	private double AggrReceiptUserCount = 0;
	private double AggrReceiptAmt = 0;
	private double AggrAdCost = 0;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SITE_CD"]);
		}
		set {
			this.ViewState["SITE_CD"] = value;
		}
	}

	private string Year {
		get {
			return iBridUtil.GetStringValue(this.ViewState["YEAR"]);
		}
		set {
			this.ViewState["YEAR"] = value;
		}
	}

	private string Month {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MONTH"]);
		}
		set {
			this.ViewState["MONTH"] = value;
		}
	}

	private string SortExpression {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortExpression"]);
		}
		set {
			this.ViewState["SortExpression"] = value;
		}
	}

	private string SortDirect {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortDirect"]);
		}
		set {
			this.ViewState["SortDirect"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			this.Year = iBridUtil.GetStringValue(Request.QueryString["year"]);
			this.Month = iBridUtil.GetStringValue(Request.QueryString["month"]);

			this.CreateList();
			this.InitPage();

			if (!string.IsNullOrEmpty(this.Year) && !string.IsNullOrEmpty(this.Month)) {
				this.lstYear.SelectedValue = this.Year;
				this.lstMonth.SelectedValue = this.Month;
			}

			pnlGrid.DataBind();
		}
	}

	protected void dsAdDataUserMonthly_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {

		AdDataUserMonthly.SearchCondition oSearchCondition = new AdDataUserMonthly.SearchCondition();
		oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
		oSearchCondition.ReportMonth = string.Format("{0}/{1}",this.lstYear.SelectedValue,this.lstMonth.SelectedValue);
		oSearchCondition.SortDirection = this.SortDirect;
		oSearchCondition.SortExpression = this.SortExpression;

		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsAdDataUserMonthly_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		if (!this.IsCorrectDate()) {
			return;
		}

		this.SortDirect = string.Empty;
		this.SortExpression = string.Empty;

		this.grdAdDataUserMonthly.PageIndex = 0;
		this.GetList();
	}

	protected void grdAdDataUserMonthly_Sorting(object sender,GridViewSortEventArgs e) {
		if (this.SortExpression.Equals(e.SortExpression)) {
			if (this.SortDirect.Equals("ASC")) {
				this.SortDirect = "DESC";
			} else if (this.SortDirect.Equals("DESC")) {
				this.SortDirect = "ASC";
			}
		} else {
			this.SortDirect = "DESC";
		}

		this.SortExpression = e.SortExpression;

		e.Cancel = true;
		this.GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	private void GetList() {
		this.grdAdDataUserMonthly.PageIndex = 0;
		this.grdAdDataUserMonthly.PageSize = 9999;
		this.grdAdDataUserMonthly.DataSourceID = "dsAdDataUserMonthly";
		this.grdAdDataUserMonthly.DataBind();
		this.pnlCount.DataBind();
	}

	protected void grdAdDataUserMonthly_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			DataRowView drv = (DataRowView)e.Row.DataItem;
			AdCost = AdCost + double.Parse(iBridUtil.GetStringValue(drv["AD_COST"]));
			AccessCount = AccessCount + double.Parse(iBridUtil.GetStringValue(drv["ACCESS_COUNT"]));
			RegistCount = RegistCount + double.Parse(iBridUtil.GetStringValue(drv["REGIST_COUNT"]));
			RegistCountAd = RegistCountAd + double.Parse(iBridUtil.GetStringValue(drv["REGIST_COUNT_AD"]));
			LoginCount = LoginCount + double.Parse(iBridUtil.GetStringValue(drv["LOGIN_COUNT"]));
			ChargePointUserCount = ChargePointUserCount + double.Parse(iBridUtil.GetStringValue(drv["CHARGE_POINT_USER_COUNT"]));
			StartReceiptUserCount = StartReceiptUserCount + double.Parse(iBridUtil.GetStringValue(drv["START_RECEIPT_USER_COUNT"]));
			EntMonthReceiptCount = EntMonthReceiptCount + double.Parse(iBridUtil.GetStringValue(drv["ENT_MONTH_RECEIPT_COUNT"]));
			EntMonthReceiptAmt = EntMonthReceiptAmt + double.Parse(iBridUtil.GetStringValue(drv["ENT_MONTH_RECEIPT_AMT"]));
			RepeatReceiptCount = RepeatReceiptCount + double.Parse(iBridUtil.GetStringValue(drv["REPEAT_RECEIPT_COUNT"]));
			RepeatReceiptAmt = RepeatReceiptAmt + double.Parse(iBridUtil.GetStringValue(drv["REPEAT_RECEIPT_AMT"]));
			TotalReceiptCount = TotalReceiptCount + double.Parse(iBridUtil.GetStringValue(drv["TOTAL_RECEIPT_COUNT"]));
			TotalReceiptAmt = TotalReceiptAmt + double.Parse(iBridUtil.GetStringValue(drv["TOTAL_RECEIPT_AMT"]));
			AggrAdCost = AggrAdCost + double.Parse(iBridUtil.GetStringValue(drv["AGGR_AD_COST"]));
			AggrReceiptUserCount = AggrReceiptUserCount + double.Parse(iBridUtil.GetStringValue(drv["AGGR_RECEIPT_USER_COUNT"]));
			AggrReceiptAmt = AggrReceiptAmt + double.Parse(iBridUtil.GetStringValue(drv["AGGR_RECEIPT_AMT"]));
			
		} else if (e.Row.RowType == DataControlRowType.Footer) {
			double dAdCost = 0;
			double dAccessCount = 0;
			double dRegistCount = 0;
			double dRegistCountAd = 0;
			double dLoginCount = 0;
			double dChargePointUserCount = 0;
			double dStartReceiptUserCount = 0;
			double dEntMonthReceiptCount = 0;
			double dEntMonthReceiptAmt = 0;
			double dRepeatReceiptCount = 0;
			double dRepeatReceiptAmt = 0;
			double dTotalReceiptCount = 0;
			double dTotalReceiptAmt = 0;
			double dAggrAdCost = 0;
			double dAggrReceiptUserCount = 0;
			double dAggrReceiptAmt = 0;
			
			Label lblAdCostFooter1 = (Label)e.Row.FindControl("lblAdCostFooter1");
			Label lblAdCostFooter2 = (Label)e.Row.FindControl("lblAdCostFooter2");
			Label lblAccessCountFooter1 = (Label)e.Row.FindControl("lblAccessCountFooter1");
			Label lblAccessCountFooter2 = (Label)e.Row.FindControl("lblAccessCountFooter2");
			Label lblRegistCountFooter1 = (Label)e.Row.FindControl("lblRegistCountFooter1");
			Label lblRegistCountFooter2 = (Label)e.Row.FindControl("lblRegistCountFooter2");
			Label lblRegistRateFooter1 = (Label)e.Row.FindControl("lblRegistRateFooter1");
			Label lblRegistRateFooter2 = (Label)e.Row.FindControl("lblRegistRateFooter2");
			Label lblRegistCountAdFooter1 = (Label)e.Row.FindControl("lblRegistCountAdFooter1");
			Label lblRegistCountAdFooter2 = (Label)e.Row.FindControl("lblRegistCountAdFooter2");
			Label lblLoginCountFooter1 = (Label)e.Row.FindControl("lblLoginCountFooter1");
			Label lblLoginCountFooter2 = (Label)e.Row.FindControl("lblLoginCountFooter2");
			Label lblAdCostByRegistFooter1 = (Label)e.Row.FindControl("lblAdCostByRegistFooter1");
			Label lblAdCostByRegistFooter2 = (Label)e.Row.FindControl("lblAdCostByRegistFooter2");
			Label lblChargePointUserCountFooter1 = (Label)e.Row.FindControl("lblChargePointUserCountFooter1");
			Label lblChargePointUserCountFooter2 = (Label)e.Row.FindControl("lblChargePointUserCountFooter2");
			Label lblChargePointUserRateFooter1 = (Label)e.Row.FindControl("lblChargePointUserRateFooter1");
			Label lblChargePointUserRateFooter2 = (Label)e.Row.FindControl("lblChargePointUserRateFooter2");
			Label lblStartReceiptUserCountFooter1 = (Label)e.Row.FindControl("lblStartReceiptUserCountFooter1");
			Label lblStartReceiptUserCountFooter2 = (Label)e.Row.FindControl("lblStartReceiptUserCountFooter2");
			Label lblStartReceiptUserRateFooter1 = (Label)e.Row.FindControl("lblStartReceiptUserRateFooter1");
			Label lblStartReceiptUserRateFooter2 = (Label)e.Row.FindControl("lblStartReceiptUserRateFooter2");
			Label lblAdCostByStartReceiptFooter1 = (Label)e.Row.FindControl("lblAdCostByStartReceiptFooter1");
			Label lblAdCostByStartReceiptFooter2 = (Label)e.Row.FindControl("lblAdCostByStartReceiptFooter2");
			Label lblEntMonthReceiptCountFooter1 = (Label)e.Row.FindControl("lblEntMonthReceiptCountFooter1");
			Label lblEntMonthReceiptCountFooter2 = (Label)e.Row.FindControl("lblEntMonthReceiptCountFooter2");
			Label lblEntMonthReceiptAmtFooter1 = (Label)e.Row.FindControl("lblEntMonthReceiptAmtFooter1");
			Label lblEntMonthReceiptAmtFooter2 = (Label)e.Row.FindControl("lblEntMonthReceiptAmtFooter2");
			Label lblEntMonthReturnRateFooter1 = (Label)e.Row.FindControl("lblEntMonthReturnRateFooter1");
			Label lblEntMonthReturnRateFooter2 = (Label)e.Row.FindControl("lblEntMonthReturnRateFooter2");
			Label lblRepeatReceiptCountFooter1 = (Label)e.Row.FindControl("lblRepeatReceiptCountFooter1");
			Label lblRepeatReceiptCountFooter2 = (Label)e.Row.FindControl("lblRepeatReceiptCountFooter2");
			Label lblRepeatReceiptAmtFooter1 = (Label)e.Row.FindControl("lblRepeatReceiptAmtFooter1");
			Label lblRepeatReceiptAmtFooter2 = (Label)e.Row.FindControl("lblRepeatReceiptAmtFooter2");
			Label lblTotalReceiptCountFooter1 = (Label)e.Row.FindControl("lblTotalReceiptCountFooter1");
			Label lblTotalReceiptCountFooter2 = (Label)e.Row.FindControl("lblTotalReceiptCountFooter2");
			Label lblTotalReceiptAmtFooter1 = (Label)e.Row.FindControl("lblTotalReceiptAmtFooter1");
			Label lblTotalReceiptAmtFooter2 = (Label)e.Row.FindControl("lblTotalReceiptAmtFooter2");
			Label lblRepeatRateFooter1 = (Label)e.Row.FindControl("lblRepeatRateFooter1");
			Label lblRepeatRateFooter2 = (Label)e.Row.FindControl("lblRepeatRateFooter2");
			Label lblAggrAdRateFooter1 = (Label)e.Row.FindControl("lblAggrAdRateFooter1");
			Label lblAggrAdRateFooter2 = (Label)e.Row.FindControl("lblAggrAdRateFooter2");
			
			DataSet oDataSet = null;
			using (AdDataUserMonthly oAdDataUserMonthly = new AdDataUserMonthly()) {
				oDataSet = oAdDataUserMonthly.GetTotalByNoAdGroup(lstSiteCd.SelectedValue,string.Format("{0}/{1}",lstYear.SelectedValue,lstMonth.SelectedValue));
			}
			
			if (oDataSet.Tables[0].Rows.Count > 0) {
				dAdCost = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["AD_COST"]));
				dAccessCount = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["ACCESS_COUNT"]));
				dRegistCount = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["REGIST_COUNT"]));
				dRegistCountAd = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["REGIST_COUNT_AD"]));
				dLoginCount = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["LOGIN_COUNT"]));
				dChargePointUserCount = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["CHARGE_POINT_USER_COUNT"]));
				dStartReceiptUserCount = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["START_RECEIPT_USER_COUNT"]));
				dEntMonthReceiptCount = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["ENT_MONTH_RECEIPT_COUNT"]));
				dEntMonthReceiptAmt = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["ENT_MONTH_RECEIPT_AMT"]));
				dRepeatReceiptCount = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["REPEAT_RECEIPT_COUNT"]));
				dRepeatReceiptAmt = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["REPEAT_RECEIPT_AMT"]));
				dTotalReceiptCount = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["TOTAL_RECEIPT_COUNT"]));
				dTotalReceiptAmt = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["TOTAL_RECEIPT_AMT"]));
				dAggrAdCost = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["AGGR_AD_COST"]));
				dAggrReceiptUserCount = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["AGGR_RECEIPT_USER_COUNT"]));
				dAggrReceiptAmt = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["AGGR_RECEIPT_AMT"]));
				
				AdCost = AdCost + dAdCost;
				AccessCount = AccessCount + dAccessCount;
				RegistCount = RegistCount + dRegistCount;
				RegistCountAd = RegistCountAd + dRegistCountAd;
				LoginCount = LoginCount + dLoginCount;
				ChargePointUserCount = ChargePointUserCount + dChargePointUserCount;
				StartReceiptUserCount = StartReceiptUserCount + dStartReceiptUserCount;
				EntMonthReceiptCount = EntMonthReceiptCount + dEntMonthReceiptCount;
				EntMonthReceiptAmt = EntMonthReceiptAmt + dEntMonthReceiptAmt;
				RepeatReceiptCount = RepeatReceiptCount + dRepeatReceiptCount;
				RepeatReceiptAmt = RepeatReceiptAmt + dRepeatReceiptAmt;
				TotalReceiptCount = TotalReceiptCount + dTotalReceiptCount;
				TotalReceiptAmt = TotalReceiptAmt + dTotalReceiptAmt;
				AggrReceiptUserCount = AggrReceiptAmt + dAggrReceiptUserCount;
				AggrReceiptAmt = AggrReceiptAmt + dAggrReceiptAmt;
				AggrAdCost = AggrAdCost + dAggrAdCost;

				lblAdCostFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["AD_COST"]);
				lblAccessCountFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["ACCESS_COUNT"]);
				lblRegistCountFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["REGIST_COUNT"]);
				lblRegistRateFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["REGIST_RATE"]);
				lblRegistCountAdFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["REGIST_COUNT_AD"]);
				lblLoginCountFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["LOGIN_COUNT"]);
				lblAdCostByRegistFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["AD_COST_BY_REGIST"]);
				lblChargePointUserCountFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["CHARGE_POINT_USER_COUNT"]);
				lblChargePointUserRateFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["CHARGE_POINT_USER_RATE"]);
				lblStartReceiptUserCountFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["START_RECEIPT_USER_COUNT"]);
				lblStartReceiptUserRateFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["START_RECEIPT_USER_RATE"]);
				lblAdCostByStartReceiptFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["AD_COST_BY_START_RECEIPT"]);
				lblEntMonthReceiptCountFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["ENT_MONTH_RECEIPT_COUNT"]);
				lblEntMonthReceiptAmtFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["ENT_MONTH_RECEIPT_AMT"]);
				lblEntMonthReturnRateFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["ENT_MONTH_RETURN_RATE"]);
				lblRepeatReceiptCountFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["REPEAT_RECEIPT_COUNT"]);
				lblRepeatReceiptAmtFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["REPEAT_RECEIPT_AMT"]);
				lblTotalReceiptCountFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["TOTAL_RECEIPT_COUNT"]);
				lblTotalReceiptAmtFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["TOTAL_RECEIPT_AMT"]);
				lblRepeatRateFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["REPEAT_RATE"]);
				lblAggrAdRateFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["AGGR_AD_COST_RATE"]);
			}

			lblAdCostFooter2.Text = iBridUtil.GetStringValue(AdCost);
			lblAccessCountFooter2.Text = iBridUtil.GetStringValue(AccessCount);
			lblRegistCountFooter2.Text = iBridUtil.GetStringValue(RegistCount);

			lblRegistRateFooter2.Text = "-";
			if (AccessCount > 0) {
				double dRegistRate;
				dRegistRate = GetDecimalValue(RegistCount / AccessCount * 100);
				lblRegistRateFooter2.Text = iBridUtil.GetStringValue(dRegistRate);
			}

			lblRegistCountAdFooter2.Text = iBridUtil.GetStringValue(RegistCountAd);

			lblAdCostByRegistFooter2.Text = "-";
			if (RegistCount > 0) {
				double dAdCostByRegist = GetDecimalValue(AdCost / RegistCount);
				lblAdCostByRegistFooter2.Text = iBridUtil.GetStringValue(dAdCostByRegist);
			}

			lblLoginCountFooter2.Text = iBridUtil.GetStringValue(LoginCount);
			lblChargePointUserCountFooter2.Text = iBridUtil.GetStringValue(ChargePointUserCount);

			lblChargePointUserRateFooter2.Text = "-";
			if (RegistCount > 0) {
				double dChargePointUserRate = GetDecimalValue(ChargePointUserCount / RegistCount * 100);
				lblChargePointUserRateFooter2.Text = iBridUtil.GetStringValue(dChargePointUserRate);
			}

			lblStartReceiptUserCountFooter2.Text = iBridUtil.GetStringValue(StartReceiptUserCount);

			lblStartReceiptUserRateFooter2.Text = "-";
			if (RegistCount > 0) {
				double dStartReceiptUserRate = GetDecimalValue(StartReceiptUserCount / RegistCount * 100);
				lblStartReceiptUserRateFooter2.Text = iBridUtil.GetStringValue(dStartReceiptUserRate);
			}

			lblAdCostByStartReceiptFooter2.Text = "-";
			if (StartReceiptUserCount > 0) {
				double dAdCostByStartReceipt = GetDecimalValue(AdCost / StartReceiptUserCount);
				lblAdCostByStartReceiptFooter2.Text = iBridUtil.GetStringValue(dAdCostByStartReceipt);
			}

			lblEntMonthReceiptCountFooter2.Text = iBridUtil.GetStringValue(EntMonthReceiptCount);
			lblEntMonthReceiptAmtFooter2.Text = iBridUtil.GetStringValue(EntMonthReceiptAmt);

			lblEntMonthReturnRateFooter2.Text = "-";
			if (AdCost > 0) {
				double dReturnRate = GetDecimalValue(EntMonthReceiptAmt / AdCost * 100);
				lblEntMonthReturnRateFooter2.Text = iBridUtil.GetStringValue(AdCost);
			}

			lblRepeatReceiptCountFooter2.Text = iBridUtil.GetStringValue(RepeatReceiptCount);
			lblRepeatReceiptAmtFooter2.Text = iBridUtil.GetStringValue(RepeatReceiptAmt);
			lblTotalReceiptCountFooter2.Text = iBridUtil.GetStringValue(TotalReceiptCount);
			lblTotalReceiptAmtFooter2.Text = iBridUtil.GetStringValue(TotalReceiptAmt);

			lblRepeatRateFooter2.Text = "-";
			if (AggrReceiptUserCount > 0) {
				double dRepeatRate = GetDecimalValue(RepeatReceiptCount / AggrReceiptUserCount * 100);
				lblRepeatRateFooter2.Text = iBridUtil.GetStringValue(dRepeatRate);
			}

			lblAggrAdRateFooter2.Text = "-";
			if (AggrReceiptAmt > 0) {
				double dAggrAdRate = GetDecimalValue(AggrAdCost / AggrReceiptAmt * 100);
				lblAggrAdRateFooter2.Text = iBridUtil.GetStringValue(dAggrAdRate);
			}
		}
	}

	private double GetDecimalValue(double pValue) {
		double dTmpVal = pValue * 100;
		return Math.Floor(dTmpVal) / 100;
	}

	protected string AddPercentMark(string pValue) {
		if (!string.IsNullOrEmpty(pValue)) {
			pValue = pValue + "%";
		}

		return pValue;
	}

	private void InitPage() {
		this.lstYear.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstMonth.SelectedValue = DateTime.Now.ToString("MM");
	}

	private void CreateList() {
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		int iStartYear = 2009;
		int iCurYear = DateTime.Now.Year;
		lstYear.Items.Clear();

		for (int i = 0;i + iStartYear <= iCurYear;i++) {

			lstYear.Items.Add(new ListItem(iBridUtil.GetStringValue(i + iStartYear)));
		}

		lstMonth.Items.Clear();
		for (int i = 1;i <= 12;i++) {
			lstMonth.Items.Add(new ListItem(string.Format("{0:D2}",i)));
		}
	}

	private bool IsCorrectDate() {
		return true;
	}

	protected string GetRecCount() {
		return this.recCount;
	}
}