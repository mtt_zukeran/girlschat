<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AdPerformanceByDay.aspx.cs" Inherits="AdManage_AdPerformanceByDay"
	Title="広告コード日別月計" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="広告コード日別月計"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							<asp:Label ID="lblAccessUnit" runat="server" Text="報告年月"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstYYYY" runat="server" Width="54px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstMM" runat="server" Width="43px">
								<asp:ListItem Value="">--</asp:ListItem>
								<asp:ListItem Value="01">01</asp:ListItem>
								<asp:ListItem Value="02">02</asp:ListItem>
								<asp:ListItem Value="03">03</asp:ListItem>
								<asp:ListItem Value="04">04</asp:ListItem>
								<asp:ListItem Value="05">05</asp:ListItem>
								<asp:ListItem Value="06">06</asp:ListItem>
								<asp:ListItem Value="07">07</asp:ListItem>
								<asp:ListItem Value="08">08</asp:ListItem>
								<asp:ListItem Value="09">09</asp:ListItem>
								<asp:ListItem Value="10">10</asp:ListItem>
								<asp:ListItem Value="11">11</asp:ListItem>
								<asp:ListItem Value="12">12</asp:ListItem>
							</asp:DropDownList>
							月
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							広告コード
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtAdCd" runat="server" MaxLength="32" Width="200px"></asp:TextBox>
							<asp:Label ID="lblAdNm" runat="server" Text=""></asp:Label>
							<asp:RequiredFieldValidator ID="vdrAdCd" runat="server" ErrorMessage="広告コードを入力して下さい。" ControlToValidate="txtAdCd" ValidationGroup="Key">*</asp:RequiredFieldValidator>
							<asp:RegularExpressionValidator ID="vdeAdCd" runat="server" ErrorMessage="広告コードは32桁以下の英数字で入力して下さい。" ValidationExpression="[a-zA-Z0-9-\/_]{1,32}" ControlToValidate="txtAdCd"
								ValidationGroup="Key">*</asp:RegularExpressionValidator>
							<asp:CustomValidator ID="vdcAdCd" runat="server" ControlToValidate="txtAdCd" ErrorMessage="広告コードが未登録です。" OnServerValidate="vdcAdCd_ServerValidate" ValidationGroup="Key">*</asp:CustomValidator>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" CausesValidation="false" />
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[稼動集計]</legend>
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="560px">
					<asp:GridView ID="grdAd" runat="server" AutoGenerateColumns="False" DataSourceID="dsAccessPage" ShowFooter="True" OnRowDataBound="grdAd_RowDataBound" SkinID="GridViewColor">
						<Columns>
							<asp:TemplateField HeaderText="日付">
								<HeaderStyle CssClass="HeaderStyle" Font-Size="Small" />
								<ItemStyle HorizontalAlign="Left" Font-Size="Small" />
								<ItemTemplate>
									<asp:HyperLink ID="lnkAccessDay" runat="server" NavigateUrl='<%# string.Format("~/StatusManager/CastPerformance.aspx?sitecd={0}&adcd={1}&from={2}&to={3}",Eval("SITE_CD"),txtAdCd.Text,Eval("ACCESS_DAY"),Eval("ACCESS_DAY")) %>'
										Text='<%# Eval("ACCESS_DAY") %>' Enabled='<%# !this.IsManMode %>'></asp:HyperLink>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Font-Size="Small" />
							</asp:TemplateField>
							<asp:BoundField DataField="ACCESS_COUNT" HeaderText="ｱｸｾｽ数" DataFormatString="{0}">
								<ItemStyle HorizontalAlign="Right" Font-Size="Small" />
								<FooterStyle HorizontalAlign="Right" Font-Size="Small" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="登録数">
								<HeaderStyle CssClass="HeaderStyle" Font-Size="Small" />
								<ItemStyle HorizontalAlign="Right" Font-Size="Small" />
								<ItemTemplate>
									<asp:Label ID="lblRegistCountRaw" runat="server" Text='<%# Eval("REGIST_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Font-Size="Small" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="登録数" SortExpression="REGIST_COUNT_AD">
								<HeaderStyle CssClass="HeaderStyle" Font-Size="Small" Wrap="false" />
								<ItemStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
								<ItemTemplate>
									<asp:HyperLink ID="lnkRegistCount" runat="server" Text='<%#  Eval("REGIST_COUNT_AD") %>' Enabled='<%# !IsAdManager %>'>
									</asp:HyperLink>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Font-Size="Small" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="退会数">
								<ItemTemplate>
									<asp:Label ID="lblWithdrwalCount" runat="server" Text='<%# Eval("WITHDRAWAL_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="再登録数">
								<ItemTemplate>
									<asp:Label ID="lblReRegistCount" runat="server" Text='<%# Eval("RE_REGIST_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="登録ﾒｰﾙ不達数" SortExpression="REGIST_MAIL_SEND_FAILED_COUNT" Visible="false">
								<HeaderStyle CssClass="HeaderStyle" Font-Size="Small" Wrap="false" />
								<ItemStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
								<ItemTemplate>
									<%# Eval("REGIST_MAIL_SEND_FAILED_COUNT") %>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Font-Size="Small" />
							</asp:TemplateField>
							<asp:TemplateField SortExpression="LOGIN_COUNT">
								<HeaderStyle CssClass="HeaderStyle" Font-Size="Small" Wrap="false" />
								<ItemStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblLoginCount" runat="server" Text='<%#  GetValue(Eval("LOGIN_COUNT"),Eval("LOGIN_COUNT_AD")) %>'>
									</asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Font-Size="Small" />
							</asp:TemplateField>
							<asp:TemplateField SortExpression="LOGIN_SUCCESS_COUNT">
								<HeaderStyle CssClass="HeaderStyle" Font-Size="Small" Wrap="false" />
								<ItemStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblLoginSuccessCountRaw" runat="server" Text='<%#  Eval("LOGIN_SUCCESS_COUNT") %>'>
									</asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Font-Size="Small" />
							</asp:TemplateField>
							<asp:TemplateField SortExpression="LOGIN_SUCCESS_COUNT_AD">
								<HeaderStyle CssClass="HeaderStyle" Font-Size="Small" Wrap="false" />
								<ItemStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblLoginSuccessCount" runat="server" Text='<%#  Eval("LOGIN_SUCCESS_COUNT_AD") %>'>
									</asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Font-Size="Small" />
							</asp:TemplateField>
							<asp:TemplateField SortExpression="LOGIN_SUCCESS_COUNT_UNIQUE">
								<HeaderStyle CssClass="HeaderStyle" Font-Size="Small" Wrap="false" />
								<ItemStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblLoginSuccessCountUniqueRaw" runat="server" Text='<%#  Eval("LOGIN_SUCCESS_COUNT_UNIQUE") %>'>
									</asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Font-Size="Small" />
							</asp:TemplateField>
							<asp:TemplateField SortExpression="LOGIN_SUCCESS_COUNT_UNIQUE_AD">
								<HeaderStyle CssClass="HeaderStyle" Font-Size="Small" Wrap="false" />
								<ItemStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblLoginSuccessCountUnique" runat="server" Text='<%#  Eval("LOGIN_SUCCESS_COUNT_UNIQUE_AD") %>'>
									</asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Font-Size="Small" />
							</asp:TemplateField>
							<asp:TemplateField SortExpression="TOTAL_USED_POINT">
								<HeaderStyle CssClass="HeaderStyle" Font-Size="Small" Wrap="false" />
								<ItemStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblTotalUsedPoint" runat="server" Text='<%# GetValue(Eval("TOTAL_USED_POINT"),Eval("TOTAL_USED_POINT_AD")) %>'>
									</asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Font-Size="Small" />
							</asp:TemplateField>
							<asp:BoundField DataField="ORDINARY_SALES_AMT" HtmlEncode="False" HeaderText="通常売上" DataFormatString="{0}" SortExpression="ORDINARY_SALES_AMT">
								<HeaderStyle Wrap="false" />
								<ItemStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
								<FooterStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
							</asp:BoundField>
							<asp:BoundField DataField="POINT_BACK_SALES_AMT" HtmlEncode="False" HeaderText="ﾎﾟｲﾝﾄｱﾌﾘ売上" DataFormatString="{0}" SortExpression="POINT_BACK_SALES_AMT">
								<HeaderStyle Wrap="false" />
								<ItemStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
								<FooterStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
							</asp:BoundField>
							<asp:BoundField DataField="TOTAL_SALES_AMT" HtmlEncode="False" HeaderText="売上合計" DataFormatString="{0}" SortExpression="TOTAL_SALES_AMT">
								<HeaderStyle Wrap="false" />
								<ItemStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
								<FooterStyle HorizontalAlign="Right" Font-Size="Small" Wrap="false" />
							</asp:BoundField>
						</Columns>
						<FooterStyle ForeColor="Black" BackColor="LightYellow" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsAccessPage" runat="server" SelectMethod="AccessAdInquiryDay" TypeName="AccessPage">
		<SelectParameters>
			<asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstYYYY" Name="pYYYY" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstMM" Name="pMM" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="txtAdCd" Name="pAdCd" PropertyName="Text" Type="String" />
			<asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" DefaultValue="" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrAdCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeAdCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdcAdCd" HighlightCssClass="validatorCallout" />
</asp:Content>
