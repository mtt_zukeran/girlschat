﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 広告コード別日別集計(会員)
--	Progaram ID		: AdDataUserDailyList
--
--  Creation Date	: 2014.06.18
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class AdManage_AdDataUserDailyList:System.Web.UI.Page {
	private string[] Week = {"日","月","火","水","木","金","土"};

	private double dAdCost = 0;
	private double dAccessCount = 0;
	private double dRegistCount = 0;
	private double dRegistCountAd = 0;
	private double dChargePointUserCount = 0;
	private double dStartReceiptUserCount = 0;
	private double dEntMonthReceiptCount = 0;
	private double dEntMonthReceiptAmt = 0;
	private double dRepeatReceiptCount = 0;
	private double dRepeatReceiptAmt = 0;
	private double dTotalReceiptCount = 0;
	private double dTotalReceiptAmt = 0;
	private double dAggrReceiptUserCount = 0;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SITE_CD"]);
		}
		set {
			this.ViewState["SITE_CD"] = value;
		}
	}

	private string AdCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["AD_CD"]);
		}
		set {
			this.ViewState["AD_CD"] = value;
		}
	}

	private string Year {
		get {
			return iBridUtil.GetStringValue(this.ViewState["YEAR"]);
		}
		set {
			this.ViewState["YEAR"] = value;
		}
	}

	private string Month {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MONTH"]);
		}
		set {
			this.ViewState["MONTH"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			this.AdCd = iBridUtil.GetStringValue(Request.QueryString["adcd"]);
			this.Year = iBridUtil.GetStringValue(Request.QueryString["year"]);
			this.Month = iBridUtil.GetStringValue(Request.QueryString["month"]);

			this.CreateList();
			this.InitPage();

			if (!string.IsNullOrEmpty(this.Year) && !string.IsNullOrEmpty(this.Month)) {
				this.lstYear.SelectedValue = this.Year;
				this.lstMonth.SelectedValue = this.Month;
			}
			this.GetAdDataMonthlyHeader();

			pnlGrid.DataBind();
		}
	}
	
	private void GetAdDataMonthlyHeader() {
		lblAdMenuNmVal.Text = "-";
		lblAggrAdCostVal.Text = "0";
		lblAggrRegistCountVal.Text = "0";
		lblAggrRegistCountAdVal.Text = "0";
		lblAggrRegistRateVal.Text = "-";
		lblAggrChargePointUserVal.Text = "0";
		lblAggrChargePointUserRateVal.Text = "-";
		lblAggrReceiptRateVal.Text = "0";
		lblAggrAdCostByReceiptUserVal.Text = "-";
		lblAggrReceiptAmtVal.Text = "0";
		lblAggrReceiptByUserVal.Text = "-";
		lblAggrAdCostRateVal.Text = "-";
		lblAggrRepeatRateVal.Text = "-";
		lblAggrReturnRateVal.Text = "-";
		
		AdDataUserMonthly.SearchCondition oSearchCondition = new AdDataUserMonthly.SearchCondition();
		oSearchCondition.SiteCd = this.SiteCd;
		oSearchCondition.AdCd = this.AdCd;
		oSearchCondition.ReportMonth = string.Format("{0}/{1}",this.lstYear.SelectedValue,this.lstMonth.SelectedValue);
		
		using (AdDataUserMonthly oAdDataUserMonthly = new AdDataUserMonthly()) {
			DataSet oDataSet = oAdDataUserMonthly.GetPageCollection(oSearchCondition,0,1);

			if (oDataSet.Tables[0].Rows.Count > 0) {
				lblAdNmVal.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["AD_NM"]);
				lblAdCdVal.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["AD_CD"]);
				lblPublishStartDateVal.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["PUBLISH_START_DATE"]);
				lblPublishEndDateVal.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["PUBLISH_END_DATE"]);
				lblAdMenuNmVal.Text = oDataSet.Tables[0].Rows[0]["AD_MENU_NM"].ToString();
				lblAggrAdCostVal.Text = oDataSet.Tables[0].Rows[0]["AGGR_AD_COST"].ToString();
				lblAggrRegistCountVal.Text = oDataSet.Tables[0].Rows[0]["AGGR_REGIST_COUNT"].ToString();
				lblAggrRegistCountAdVal.Text = oDataSet.Tables[0].Rows[0]["AGGR_REGIST_COUNT_AD"].ToString();
				lblAggrRegistRateVal.Text = oDataSet.Tables[0].Rows[0]["AGGR_REGIST_RATE"].ToString();
				lblAggrChargePointUserVal.Text = oDataSet.Tables[0].Rows[0]["AGGR_CHARGE_POINT_USER"].ToString();
				lblAggrChargePointUserRateVal.Text = oDataSet.Tables[0].Rows[0]["AGGR_CHARGE_POINT_USER_RATE"].ToString();
				lblAggrReceiptRateVal.Text = oDataSet.Tables[0].Rows[0]["AGGR_RECEIPT_RATE"].ToString();
				lblAggrAdCostByReceiptUserVal.Text = oDataSet.Tables[0].Rows[0]["AGGR_AD_COST_BY_RECEIPT_USER"].ToString();
				lblAggrReceiptAmtVal.Text = oDataSet.Tables[0].Rows[0]["AGGR_RECEIPT_AMT"].ToString();
				lblAggrReceiptByUserVal.Text = oDataSet.Tables[0].Rows[0]["AGGR_RECEIPT_BY_USER"].ToString();
				lblAggrAdCostRateVal.Text = oDataSet.Tables[0].Rows[0]["AGGR_AD_COST_RATE"].ToString();
				lblAggrRepeatRateVal.Text = oDataSet.Tables[0].Rows[0]["AGGR_REPEAT_RATE"].ToString();
				lblAggrReturnRateVal.Text = oDataSet.Tables[0].Rows[0]["AGGR_RETURN_RATE"].ToString();
				dAggrReceiptUserCount = int.Parse(oDataSet.Tables[0].Rows[0]["AGGR_RECEIPT_USER_COUNT"].ToString());
			}
		}
	}

	private void InitPage() {
		this.lstYear.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstMonth.SelectedValue = DateTime.Now.ToString("MM");
	}

	private void ClearField() {
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void grdAdDataUserDaily_RowDataBound(object sender,GridViewRowEventArgs e) {
		
		if (e.Row.RowType == DataControlRowType.DataRow) {
			DataRowView drv = (DataRowView)e.Row.DataItem;
			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(drv["AD_COST"]))) {
				dAdCost = dAdCost + int.Parse(iBridUtil.GetStringValue(drv["AD_COST"]));
				dAccessCount = dAccessCount + int.Parse(iBridUtil.GetStringValue(drv["ACCESS_COUNT"]));
				dRegistCount = dRegistCount + int.Parse(iBridUtil.GetStringValue(drv["REGIST_COUNT"]));
				dRegistCountAd = dRegistCountAd + int.Parse(iBridUtil.GetStringValue(drv["REGIST_COUNT_AD"]));
				dChargePointUserCount = dChargePointUserCount + int.Parse(iBridUtil.GetStringValue(drv["CHARGE_POINT_USER_COUNT"]));
				dStartReceiptUserCount = dStartReceiptUserCount + int.Parse(iBridUtil.GetStringValue(drv["START_RECEIPT_USER_COUNT"]));
				dEntMonthReceiptCount = dEntMonthReceiptCount + int.Parse(iBridUtil.GetStringValue(drv["ENT_MONTH_RECEIPT_COUNT"]));
				dEntMonthReceiptAmt = dEntMonthReceiptAmt + int.Parse(iBridUtil.GetStringValue(drv["ENT_MONTH_RECEIPT_AMT"]));
				dRepeatReceiptCount = dRepeatReceiptCount + int.Parse(iBridUtil.GetStringValue(drv["REPEAT_RECEIPT_COUNT"]));
				dRepeatReceiptAmt = dRepeatReceiptAmt + int.Parse(iBridUtil.GetStringValue(drv["REPEAT_RECEIPT_AMT"]));
				dTotalReceiptCount = dTotalReceiptCount + int.Parse(iBridUtil.GetStringValue(drv["TOTAL_RECEIPT_COUNT"]));
				dTotalReceiptAmt = dTotalReceiptAmt + int.Parse(iBridUtil.GetStringValue(drv["TOTAL_RECEIPT_AMT"]));
			}

			DateTime dtReportDay = DateTime.Parse(drv["DAYS"].ToString());
			int iDayOfWeek = (int)dtReportDay.DayOfWeek;
			
			if (iDayOfWeek == 0) {
				e.Row.BackColor = Color.FromArgb(0xFFCCCC);
			} else if (iDayOfWeek == 6) {
				e.Row.BackColor = Color.FromArgb(0xCCCCFF);
			}
		} else if (e.Row.RowType == DataControlRowType.Footer) {
			DateTime dtSelectedmonth = DateTime.Parse(string.Format("{0}/{1}/01",lstYear.SelectedValue,lstMonth.SelectedValue));
			DateTime dtNowMonth = DateTime.Parse(string.Format("{0}/{1}/01",DateTime.Now.ToString("yyyy"),DateTime.Now.ToString("MM")));

			int iDaysInMonth = DateTime.DaysInMonth(dtSelectedmonth.Year,dtSelectedmonth.Month);
			int iPastDays = 0;
			
			if (dtSelectedmonth == dtNowMonth) {
				iPastDays = DateTime.Now.Day - 1;
			} else {
				iPastDays = iDaysInMonth;
			}
			
			Label lblAdCostFooter1 = (Label)e.Row.FindControl("lblAdCostFooter1");
			Label lblAdCostFooter2 = (Label)e.Row.FindControl("lblAdCostFooter2");
			Label lblAdCostFooter3 = (Label)e.Row.FindControl("lblAdCostFooter3");
			Label lblAccessCountFooter1 = (Label)e.Row.FindControl("lblAccessCountFooter1");
			Label lblAccessCountFooter2 = (Label)e.Row.FindControl("lblAccessCountFooter2");
			Label lblAccessCountFooter3 = (Label)e.Row.FindControl("lblAccessCountFooter3");
			Label lblRegistCountFooter1 = (Label)e.Row.FindControl("lblRegistCountFooter1");
			Label lblRegistCountFooter2 = (Label)e.Row.FindControl("lblRegistCountFooter2");
			Label lblRegistCountFooter3 = (Label)e.Row.FindControl("lblRegistCountFooter3");
			Label lblRegistRateFooter1 = (Label)e.Row.FindControl("lblRegistRateFooter1");
			Label lblRegistRateFooter2 = (Label)e.Row.FindControl("lblRegistRateFooter2");
			Label lblRegistRateFooter3 = (Label)e.Row.FindControl("lblRegistRateFooter3");
			Label lblRegistCountAdFooter1 = (Label)e.Row.FindControl("lblRegistCountAdFooter1");
			Label lblRegistCountAdFooter2 = (Label)e.Row.FindControl("lblRegistCountAdFooter2");
			Label lblRegistCountAdFooter3 = (Label)e.Row.FindControl("lblRegistCountAdFooter3");
			Label lblAdCostByRegistFooter1 = (Label)e.Row.FindControl("lblAdCostByRegistFooter1");
			Label lblAdCostByRegistFooter2 = (Label)e.Row.FindControl("lblAdCostByRegistFooter2");
			Label lblAdCostByRegistFooter3 = (Label)e.Row.FindControl("lblAdCostByRegistFooter3");
			Label lblChargePointUserCountFooter1 = (Label)e.Row.FindControl("lblChargePointUserCountFooter1");
			Label lblChargePointUserCountFooter2 = (Label)e.Row.FindControl("lblChargePointUserCountFooter2");
			Label lblChargePointUserCountFooter3 = (Label)e.Row.FindControl("lblChargePointUserCountFooter3");
			Label lblChargePointUserRateFooter1 = (Label)e.Row.FindControl("lblChargePointUserRateFooter1");
			Label lblChargePointUserRateFooter2 = (Label)e.Row.FindControl("lblChargePointUserRateFooter2");
			Label lblChargePointUserRateFooter3 = (Label)e.Row.FindControl("lblChargePointUserRateFooter3");
			Label lblStartReceiptUserCountFooter1 = (Label)e.Row.FindControl("lblStartReceiptUserCountFooter1");
			Label lblStartReceiptUserCountFooter2 = (Label)e.Row.FindControl("lblStartReceiptUserCountFooter2");
			Label lblStartReceiptUserCountFooter3 = (Label)e.Row.FindControl("lblStartReceiptUserCountFooter3");
			Label lblStartReceiptUserRateFooter1 = (Label)e.Row.FindControl("lblStartReceiptUserRateFooter1");
			Label lblStartReceiptUserRateFooter2 = (Label)e.Row.FindControl("lblStartReceiptUserRateFooter2");
			Label lblStartReceiptUserRateFooter3 = (Label)e.Row.FindControl("lblStartReceiptUserRateFooter3");
			Label lblAdCostByStartReceiptFooter1 = (Label)e.Row.FindControl("lblAdCostByStartReceiptFooter1");
			Label lblAdCostByStartReceiptFooter2 = (Label)e.Row.FindControl("lblAdCostByStartReceiptFooter2");
			Label lblAdCostByStartReceiptFooter3 = (Label)e.Row.FindControl("lblAdCostByStartReceiptFooter3");
			Label lblEntMonthReceiptCountFooter1 = (Label)e.Row.FindControl("lblEntMonthReceiptCountFooter1");
			Label lblEntMonthReceiptCountFooter2 = (Label)e.Row.FindControl("lblEntMonthReceiptCountFooter2");
			Label lblEntMonthReceiptCountFooter3 = (Label)e.Row.FindControl("lblEntMonthReceiptCountFooter3");
			Label lblEntMonthReceiptAmtFooter1 = (Label)e.Row.FindControl("lblEntMonthReceiptAmtFooter1");
			Label lblEntMonthReceiptAmtFooter2 = (Label)e.Row.FindControl("lblEntMonthReceiptAmtFooter2");
			Label lblEntMonthReceiptAmtFooter3 = (Label)e.Row.FindControl("lblEntMonthReceiptAmtFooter3");
			Label lblEntMonthReturnRateFooter1 = (Label)e.Row.FindControl("lblEntMonthReturnRateFooter1");
			Label lblEntMonthReturnRateFooter2 = (Label)e.Row.FindControl("lblEntMonthReturnRateFooter2");
			Label lblEntMonthReturnRateFooter3 = (Label)e.Row.FindControl("lblEntMonthReturnRateFooter3");
			Label lblRepeatReceiptCountFooter1 = (Label)e.Row.FindControl("lblRepeatReceiptCountFooter1");
			Label lblRepeatReceiptCountFooter2 = (Label)e.Row.FindControl("lblRepeatReceiptCountFooter2");
			Label lblRepeatReceiptCountFooter3 = (Label)e.Row.FindControl("lblRepeatReceiptCountFooter3");
			Label lblRepeatReceiptAmtFooter1 = (Label)e.Row.FindControl("lblRepeatReceiptAmtFooter1");
			Label lblRepeatReceiptAmtFooter2 = (Label)e.Row.FindControl("lblRepeatReceiptAmtFooter2");
			Label lblRepeatReceiptAmtFooter3 = (Label)e.Row.FindControl("lblRepeatReceiptAmtFooter3");
			Label lblTotalReceiptCountFooter1 = (Label)e.Row.FindControl("lblTotalReceiptCountFooter1");
			Label lblTotalReceiptCountFooter2 = (Label)e.Row.FindControl("lblTotalReceiptCountFooter2");
			Label lblTotalReceiptCountFooter3 = (Label)e.Row.FindControl("lblTotalReceiptCountFooter3");
			Label lblTotalReceiptAmtFooter1 = (Label)e.Row.FindControl("lblTotalReceiptAmtFooter1");
			Label lblTotalReceiptAmtFooter2 = (Label)e.Row.FindControl("lblTotalReceiptAmtFooter2");
			Label lblTotalReceiptAmtFooter3 = (Label)e.Row.FindControl("lblTotalReceiptAmtFooter3");
			Label lblRepeatRateFooter1 = (Label)e.Row.FindControl("lblRepeatRateFooter1");
			Label lblRepeatRateFooter2 = (Label)e.Row.FindControl("lblRepeatRateFooter2");
			Label lblRepeatRateFooter3 = (Label)e.Row.FindControl("lblRepeatRateFooter3");
			Label lblTotalAmtByUserFooter1 = (Label)e.Row.FindControl("lblTotalAmtByUserFooter1");
			Label lblTotalAmtByUserFooter2 = (Label)e.Row.FindControl("lblTotalAmtByUserFooter2");
			Label lblTotalAmtByUserFooter3 = (Label)e.Row.FindControl("lblTotalAmtByUserFooter3");
			
			lblAdCostFooter1.Text = dAdCost.ToString();
			lblAdCostFooter2.Text = "-";
			lblAdCostFooter3.Text = "-";
			double dAdCostAvgVal = 0;
			double dAdCostEstVal = 0;
			
			if(dAdCost > 0) {
				dAdCostAvgVal = dAdCost / iPastDays;
				dAdCostEstVal = dAdCostAvgVal * iDaysInMonth;
				lblAdCostFooter2.Text = GetDecimalValue(dAdCostEstVal).ToString();
				lblAdCostFooter3.Text = GetDecimalValue(dAdCostAvgVal).ToString();
			}
			
			lblAccessCountFooter1.Text = dAccessCount.ToString();
			lblAccessCountFooter2.Text = "-";
			lblAccessCountFooter3.Text = "-";
			double dAccessCountAvgVal = 0;
			double dAccessCountEstVal = 0;

			if (dAccessCount > 0) {
				dAccessCountAvgVal = dAccessCount / iPastDays;
				dAccessCountEstVal = dAccessCountAvgVal * iDaysInMonth;
				lblAccessCountFooter2.Text = GetDecimalValue(dAccessCountEstVal).ToString();
				lblAccessCountFooter3.Text = GetDecimalValue(dAccessCountAvgVal).ToString();
			}
			
			
			lblRegistCountFooter1.Text = dRegistCount.ToString();
			lblRegistCountFooter2.Text = "-";
			lblRegistCountFooter3.Text = "-";
			double dRegistCountAvgVal = 0;
			double dRegistCountEstVal = 0;

			if (dRegistCount > 0) {
				dRegistCountAvgVal = dRegistCount / iPastDays;
				dRegistCountEstVal = dRegistCountAvgVal * iDaysInMonth;
				lblRegistCountFooter2.Text = GetDecimalValue(dRegistCountEstVal).ToString();
				lblRegistCountFooter3.Text = GetDecimalValue(dRegistCountAvgVal).ToString();
			}
			
			lblRegistRateFooter1.Text = "-";
			lblRegistRateFooter2.Text = "-";
			lblRegistRateFooter3.Text = "-";
			if (dAccessCount > 0) {
				lblRegistRateFooter1.Text = GetDecimalValue(dRegistCount / dAccessCount * 100).ToString();
				lblRegistRateFooter2.Text = GetDecimalValue(dRegistCountEstVal / dAccessCountEstVal * 100).ToString();
				lblRegistRateFooter3.Text = GetDecimalValue(dRegistCountAvgVal / dAccessCountAvgVal * 100).ToString();
			}
			
			lblRegistCountAdFooter1.Text = dRegistCountAd.ToString();
			lblRegistCountAdFooter2.Text = "-";
			lblRegistCountAdFooter3.Text = "-";
			double dRegistCountAdAvgVal = 0;
			double dRegistCountAdEstVal = 0;

			if (dRegistCountAd > 0) {
				dRegistCountAdAvgVal = dRegistCountAd / iPastDays;
				dRegistCountAdEstVal = dRegistCountAdAvgVal * iDaysInMonth;
				lblRegistCountAdFooter2.Text = GetDecimalValue(dRegistCountAdEstVal).ToString();
				lblRegistCountAdFooter3.Text = GetDecimalValue(dRegistCountAdAvgVal).ToString();
			}
			
			lblAdCostByRegistFooter1.Text = "-";
			lblAdCostByRegistFooter2.Text = "-";
			lblAdCostByRegistFooter3.Text = "-";
			if (dRegistCount > 0) {
				lblAdCostByRegistFooter1.Text = GetDecimalValue(dAdCost / dRegistCount * 100).ToString();
				lblAdCostByRegistFooter2.Text = GetDecimalValue(dAdCostEstVal / dRegistCountEstVal * 100).ToString();
				lblAdCostByRegistFooter3.Text = GetDecimalValue(dAdCostAvgVal / dRegistCountAvgVal * 100).ToString();
			}
			
			lblChargePointUserCountFooter1.Text = dChargePointUserCount.ToString();
			lblChargePointUserCountFooter2.Text = "-";
			lblChargePointUserCountFooter3.Text = "-";
			double dChargePointUserCountAvgVal = 0;
			double dChargePointUserCountEstVal = 0;

			if (dChargePointUserCount > 0) {
				dChargePointUserCountAvgVal = dChargePointUserCount / iPastDays;
				dChargePointUserCountEstVal = dChargePointUserCountAvgVal * iDaysInMonth;
				lblChargePointUserCountFooter2.Text = GetDecimalValue(dChargePointUserCountEstVal).ToString();
				lblChargePointUserCountFooter3.Text = GetDecimalValue(dChargePointUserCountAvgVal).ToString();
			}
			
			lblChargePointUserRateFooter1.Text = "-";
			lblChargePointUserRateFooter2.Text = "-";
			lblChargePointUserRateFooter3.Text = "-";
			if (dRegistCount > 0) {
				lblChargePointUserRateFooter1.Text = GetDecimalValue(dChargePointUserCount / dRegistCount * 100).ToString();
				lblChargePointUserRateFooter2.Text = GetDecimalValue(dChargePointUserCountEstVal / dRegistCountEstVal * 100).ToString();
				lblChargePointUserRateFooter3.Text = GetDecimalValue(dChargePointUserCountAvgVal / dRegistCountAvgVal * 100).ToString();
			}
			
			lblStartReceiptUserCountFooter1.Text = dStartReceiptUserCount.ToString();
			lblStartReceiptUserCountFooter2.Text = "-";
			lblStartReceiptUserCountFooter3.Text = "-";
			double dStartReceiptUserCountAvgVal = 0;
			double dStartReceiptUserCountEstVal = 0;

			if (dStartReceiptUserCount > 0) {
				dStartReceiptUserCountAvgVal = dStartReceiptUserCount / iPastDays;
				dStartReceiptUserCountEstVal = dStartReceiptUserCountAvgVal * iDaysInMonth;
				lblStartReceiptUserCountFooter2.Text = GetDecimalValue(dStartReceiptUserCountEstVal).ToString();
				lblStartReceiptUserCountFooter3.Text = GetDecimalValue(dStartReceiptUserCountAvgVal).ToString();
			}
			
			lblStartReceiptUserRateFooter1.Text = "-";
			lblStartReceiptUserRateFooter2.Text = "-";
			lblStartReceiptUserRateFooter3.Text = "-";
			if (dRegistCount > 0) {
				lblStartReceiptUserRateFooter1.Text = GetDecimalValue(dStartReceiptUserCount / dRegistCount * 100).ToString();
				lblStartReceiptUserRateFooter2.Text = GetDecimalValue(dStartReceiptUserCountEstVal / dRegistCountEstVal * 100).ToString();
				lblStartReceiptUserRateFooter3.Text = GetDecimalValue(dStartReceiptUserCountAvgVal / dRegistCountAvgVal * 100).ToString();
			}
			
			lblAdCostByStartReceiptFooter1.Text = "-";
			lblAdCostByStartReceiptFooter2.Text = "-";
			lblAdCostByStartReceiptFooter3.Text = "-";
			if (dStartReceiptUserCount > 0) {
				lblAdCostByStartReceiptFooter1.Text = GetDecimalValue(dAdCost / dStartReceiptUserCount * 100).ToString();
				lblAdCostByStartReceiptFooter2.Text = GetDecimalValue(dAdCostEstVal / dStartReceiptUserCountEstVal * 100).ToString();
				lblAdCostByStartReceiptFooter3.Text = GetDecimalValue(dAdCostAvgVal / dStartReceiptUserCountAvgVal * 100).ToString();
			}
			
			lblEntMonthReceiptCountFooter1.Text = dEntMonthReceiptCount.ToString();
			lblEntMonthReceiptCountFooter2.Text = "-";
			lblEntMonthReceiptCountFooter3.Text = "-";
			double dEntMonthReceiptCountAvgVal = 0;
			double dEntMonthReceiptCountEstVal = 0;

			if (dEntMonthReceiptCount > 0) {
				dEntMonthReceiptCountAvgVal = dEntMonthReceiptCount / iPastDays;
				dEntMonthReceiptCountEstVal = dEntMonthReceiptCountAvgVal * iDaysInMonth;
				lblEntMonthReceiptCountFooter2.Text = GetDecimalValue(dEntMonthReceiptCountEstVal).ToString();
				lblEntMonthReceiptCountFooter3.Text = GetDecimalValue(dEntMonthReceiptCountAvgVal).ToString();
			}
			
			lblEntMonthReceiptAmtFooter1.Text = dEntMonthReceiptAmt.ToString();
			lblEntMonthReceiptAmtFooter2.Text = "-";
			lblEntMonthReceiptAmtFooter3.Text = "-";
			double dEntMonthReceiptAmtAvgVal = 0;
			double dEntMonthReceiptAmtEstVal = 0;

			if (dEntMonthReceiptAmt > 0) {
				dEntMonthReceiptAmtAvgVal = dEntMonthReceiptAmt / iPastDays;
				dEntMonthReceiptAmtEstVal = dEntMonthReceiptAmtAvgVal * iDaysInMonth;
				lblEntMonthReceiptAmtFooter2.Text = GetDecimalValue(dEntMonthReceiptAmtEstVal).ToString();
				lblEntMonthReceiptAmtFooter3.Text = GetDecimalValue(dEntMonthReceiptAmtAvgVal).ToString();
			}
			
			lblEntMonthReturnRateFooter1.Text = "-";
			lblEntMonthReturnRateFooter2.Text = "-";
			lblEntMonthReturnRateFooter3.Text = "-";
			if (dAdCost > 0) {
				lblEntMonthReturnRateFooter1.Text = GetDecimalValue(dEntMonthReceiptAmt / dAdCost * 100).ToString();
				lblEntMonthReturnRateFooter2.Text = GetDecimalValue(dEntMonthReceiptAmtEstVal / dAdCostEstVal * 100).ToString();
				lblEntMonthReturnRateFooter3.Text = GetDecimalValue(dEntMonthReceiptAmtAvgVal / dAdCostAvgVal * 100).ToString();
			}
			
			lblRepeatReceiptCountFooter1.Text = dRepeatReceiptCount.ToString();
			lblRepeatReceiptCountFooter2.Text = "-";
			lblRepeatReceiptCountFooter3.Text = "-";
			double dRepeatReceiptCountAvgVal = 0;
			double dRepeatReceiptCountEstVal = 0;

			if (dRepeatReceiptCount > 0) {
				dRepeatReceiptCountAvgVal = dRepeatReceiptCount / iPastDays;
				dRepeatReceiptCountEstVal = dRepeatReceiptCountAvgVal * iDaysInMonth;
				lblRepeatReceiptCountFooter2.Text = GetDecimalValue(dRepeatReceiptCountEstVal).ToString();
				lblRepeatReceiptCountFooter3.Text = GetDecimalValue(dRepeatReceiptCountAvgVal).ToString();
			}
			
			lblRepeatReceiptAmtFooter1.Text = dRepeatReceiptAmt.ToString();
			lblRepeatReceiptAmtFooter2.Text = "-";
			lblRepeatReceiptAmtFooter3.Text = "-";
			double dRepeatReceiptAmtAvgVal = 0;
			double dRepeatReceiptAmtEstVal = 0;

			if (dRepeatReceiptAmt > 0) {
				dRepeatReceiptAmtAvgVal = dRepeatReceiptAmt / iPastDays;
				dRepeatReceiptAmtEstVal = dRepeatReceiptAmtAvgVal * iDaysInMonth;
				lblRepeatReceiptAmtFooter2.Text = GetDecimalValue(dRepeatReceiptAmtEstVal).ToString();
				lblRepeatReceiptAmtFooter3.Text = GetDecimalValue(dRepeatReceiptAmtAvgVal).ToString();
			}
			
			lblTotalReceiptCountFooter1.Text = dTotalReceiptCount.ToString();
			lblTotalReceiptCountFooter2.Text = "-";
			lblTotalReceiptCountFooter3.Text = "-";
			double dTotalReceiptCountAvgVal = 0;
			double dTotalReceiptCountEstVal = 0;

			if (dTotalReceiptCount > 0) {
				dTotalReceiptCountAvgVal = dTotalReceiptCount / iPastDays;
				dTotalReceiptCountEstVal = dTotalReceiptCountAvgVal * iDaysInMonth;
				lblTotalReceiptCountFooter2.Text = GetDecimalValue(dTotalReceiptCountEstVal).ToString();
				lblTotalReceiptCountFooter3.Text = GetDecimalValue(dTotalReceiptCountAvgVal).ToString();
			}
			
			lblTotalReceiptAmtFooter1.Text = dTotalReceiptAmt.ToString();
			lblTotalReceiptAmtFooter2.Text = "-";
			lblTotalReceiptAmtFooter3.Text = "-";
			double dTotalReceiptAmtAvgVal = 0;
			double dTotalReceiptAmtEstVal = 0;

			if (dTotalReceiptAmt > 0) {
				dTotalReceiptAmtAvgVal = dTotalReceiptAmt / iPastDays;
				dTotalReceiptAmtEstVal = dTotalReceiptAmtAvgVal * iDaysInMonth;
				lblTotalReceiptAmtFooter2.Text = GetDecimalValue(dTotalReceiptAmtEstVal).ToString();
				lblTotalReceiptAmtFooter3.Text = GetDecimalValue(dTotalReceiptAmtAvgVal).ToString();
			}
			
			lblRepeatRateFooter1.Text = "-";
			lblRepeatRateFooter2.Text = "-";
			lblRepeatRateFooter3.Text = "-";
			double dAggrReceiptUserCountAvgVal = 0;
			double dAggrReceiptUserCountEstVal = 0;
			double dAggrReceiptUserCountOrg = dAggrReceiptUserCount - dTotalReceiptCount;
			if (dAggrReceiptUserCount > 0) {
				dAggrReceiptUserCountAvgVal = dAggrReceiptUserCountOrg + dTotalReceiptCountAvgVal;
				dAggrReceiptUserCountEstVal = dAggrReceiptUserCountOrg + dTotalReceiptCountEstVal;
				lblRepeatRateFooter1.Text = GetDecimalValue(dRepeatReceiptCount / dAggrReceiptUserCount * 100).ToString();
				lblRepeatRateFooter2.Text = GetDecimalValue(dRepeatReceiptCountEstVal / dAggrReceiptUserCountEstVal * 100).ToString();
				lblRepeatRateFooter3.Text = GetDecimalValue(dRepeatReceiptCountAvgVal / dAggrReceiptUserCountAvgVal * 100).ToString();
			}

			lblTotalAmtByUserFooter1.Text = "-";
			lblTotalAmtByUserFooter2.Text = "-";
			lblTotalAmtByUserFooter3.Text = "-";
			if (dTotalReceiptCount > 0) {
				lblTotalAmtByUserFooter1.Text = GetDecimalValue(dTotalReceiptAmt / dTotalReceiptCount).ToString();
				lblTotalAmtByUserFooter2.Text = GetDecimalValue(dTotalReceiptAmtEstVal / dTotalReceiptCountEstVal).ToString();
				lblTotalAmtByUserFooter3.Text = GetDecimalValue(dTotalReceiptAmtAvgVal / dTotalReceiptCountAvgVal).ToString();
			}
		}
	}
	
	private double GetDecimalValue(double pValue) {
		double dTmpVal = pValue * 100;
		return Math.Floor(dTmpVal) / 100;
	}
	
	protected string AddPercentMark(string pValue) {
		if (!string.IsNullOrEmpty(pValue)) {
			pValue = pValue + "%";
		}
		
		return pValue;
	}

	private void GetList() {

		grdAdDataUserDaily.PageIndex = 0;
		grdAdDataUserDaily.DataSourceID = "dsAdDataUserDaily";
		DataBind();
	}

	protected Color GetBackColor(object pDayOfWeek) {
		if (pDayOfWeek.ToString().Equals("Sat")) {
			return Color.FromArgb(0xCCCCFF);
		} else {
			if (pDayOfWeek.ToString().Equals("Sun")) {
				return Color.FromArgb(0xFFCCCC);
			} else {
				return Color.Empty;
			}
		}
	}

	private void CreateList() {
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		int iStartYear = 2009;
		int iCurYear = DateTime.Now.Year;
		lstYear.Items.Clear();

		for (int i = 0;i + iStartYear <= iCurYear;i++) {

			lstYear.Items.Add(new ListItem(iBridUtil.GetStringValue(i + iStartYear)));
		}

		lstMonth.Items.Clear();
		for (int i = 1;i <= 12;i++) {
			lstMonth.Items.Add(new ListItem(string.Format("{0:D2}",i)));
		}
	}

	private bool IsCorrectDate() {
		return true;
	}
	
	protected string GetDisplayReportDay(string pReportDay) {
		DateTime dtReportDay = DateTime.Parse(pReportDay);
		return dtReportDay.Day.ToString();
	}

	protected string GetDisplayDayOfWeek(string pReportDay) {
		DateTime dtReportDay = DateTime.Parse(pReportDay);
		int iDayOfWeekNo = (int)dtReportDay.DayOfWeek;
		
		return Week[iDayOfWeekNo];
	}
}
