﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 広告コード自動発行
--	Progaram ID		: IssueAdCd
--
--  Creation Date	: 2010.03.18
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class AdManage_IssueAdCd:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			DataBind();
			if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
				lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}
			string sCastRegistReportTiming = string.Empty;
			string sManRegistReportTiming = string.Empty;
			using (Site oSite = new Site()) {
				if (oSite.GetValue(lstSiteCd.SelectedValue,"CAST_REGIST_REPORT_TIMING",ref sCastRegistReportTiming)) {
					for (int i = 0;i < lstCastRegistReportTiming.Items.Count;i++) {
						if (lstCastRegistReportTiming.Items[i].Value == sCastRegistReportTiming) {
							lstCastRegistReportTiming.SelectedIndex = i;
						}
					}
				}
				if (oSite.GetValue(lstSiteCd.SelectedValue,"MAN_REGIST_REPORT_TIMING",ref sManRegistReportTiming)) {
					for (int i = 0;i < lstManRegistReportTiming.Items.Count;i++) {
						if (lstManRegistReportTiming.Items[i].Value == sManRegistReportTiming) {
							lstManRegistReportTiming.SelectedIndex = i;
						}
					}
				}
			}
		}
	}

	protected void btnIssue_Click(object sender,EventArgs e) {
		if (IsValid) {
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("ISSUE_AD_CD");
				db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
				db.ProcedureInParm("PAD_GROUP_CD",DbSession.DbType.VARCHAR2,lstAdGroup.SelectedValue);
				db.ProcedureInParm("PAD_NM",DbSession.DbType.VARCHAR2,txtAdNm.Text);
				db.ProcedureInParm("PISSUE_COUNT",DbSession.DbType.NUMBER,int.Parse(txtIssueCount.Text));
				db.ProcedureInParm("pCAST_REGIST_REPORT_TIMING",DbSession.DbType.VARCHAR2,lstCastRegistReportTiming.SelectedValue);
				db.ProcedureInParm("pMAN_REGIST_REPORT_TIMING",DbSession.DbType.VARCHAR2,lstManRegistReportTiming.SelectedValue);
				db.ProcedureOutArrayParm("PAD_CD",DbSession.DbType.VARCHAR2,int.Parse(txtIssueCount.Text));
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

				db.ExecuteProcedure();

				string fileName = "AdCdList" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".csv";

				Response.AddHeader("Content-Disposition","attachment;filename=" + fileName);
				Response.ContentType = "application/octet-stream-dummy";
				System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");
				string sDtl = "";
				for (int i = 0;i < int.Parse(txtIssueCount.Text);i++) {
					sDtl = "";
					sDtl += db.GetArryStringValue("PAD_CD",i);
					sDtl += "\r\n";
					Response.BinaryWrite(encoding.GetBytes(sDtl));
				}
			}
			Response.End();
		}
	}

	protected void dsAdGroup_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
	}

	protected void lstSiteCd_SelectedIndexChanged(object sender,EventArgs e) {
		lstAdGroup.DataBind();
	}
}
