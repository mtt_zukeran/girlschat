﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者入会月別報酬一覧(日別)
--	Progaram ID		: PaymentByEntDailyList
--
--  Creation Date	: 2014.06.26
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class AdManage_PaymentByEntDailyList:System.Web.UI.Page {
	private string[] Week = { "日","月","火","水","木","金","土" };
	private int PaymentAmt = 0;
	private int PaymentCount = 0;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SITE_CD"]);
		}
		set {
			this.ViewState["SITE_CD"] = value;
		}
	}

	private string EntMonth {
		get {
			return iBridUtil.GetStringValue(this.ViewState["ENT_MONTH"]);
		}
		set {
			this.ViewState["ENT_MONTH"] = value;
		}
	}

	private string Year {
		get {
			return iBridUtil.GetStringValue(this.ViewState["YEAR"]);
		}
		set {
			this.ViewState["YEAR"] = value;
		}
	}

	private string Month {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MONTH"]);
		}
		set {
			this.ViewState["MONTH"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			this.EntMonth = iBridUtil.GetStringValue(Request.QueryString["entmonth"]);
			this.Year = iBridUtil.GetStringValue(Request.QueryString["year"]);
			this.Month = iBridUtil.GetStringValue(Request.QueryString["month"]);

			this.CreateList();
			this.InitPage();

			if (!string.IsNullOrEmpty(this.Year) && !string.IsNullOrEmpty(this.Month)) {
				this.lstYear.SelectedValue = this.Year;
				this.lstMonth.SelectedValue = this.Month;
			}

			pnlGrid.DataBind();
		}
	}

	private void InitPage() {
		this.lstYear.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstMonth.SelectedValue = DateTime.Now.ToString("MM");
	}

	protected void dsPaymentByEntDaily_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {

		PaymentByEntDaily.SearchCondition oSearchCondition = new PaymentByEntDaily.SearchCondition();
		oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
		oSearchCondition.EntMonth = this.EntMonth;
		oSearchCondition.ReportMonth = string.Format("{0}/{1}",this.lstYear.SelectedValue,this.lstMonth.SelectedValue);

		e.InputParameters[0] = oSearchCondition;
	}

	private void ClearField() {
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void grdPaymentByEntDaily_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			DataRowView drv = (DataRowView)e.Row.DataItem;
			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(drv["PAYMENT_AMT"]))) {
				PaymentAmt = PaymentAmt + int.Parse(iBridUtil.GetStringValue(drv["PAYMENT_AMT"]));
				PaymentCount = PaymentCount + int.Parse(iBridUtil.GetStringValue(drv["PAYMENT_COUNT"]));
			}

			DateTime dtReportDay = DateTime.Parse(drv["DAYS"].ToString());
			int iDayOfWeek = (int)dtReportDay.DayOfWeek;

			if (iDayOfWeek == 0) {
				e.Row.BackColor = Color.FromArgb(0xFFCCCC);
			} else if (iDayOfWeek == 6) {
				e.Row.BackColor = Color.FromArgb(0xCCCCFF);
			}
		} else if (e.Row.RowType == DataControlRowType.Footer) {
			e.Row.Cells[2].Text = iBridUtil.GetStringValue(PaymentAmt);
			e.Row.Cells[3].Text = iBridUtil.GetStringValue(PaymentCount);
		}
	}

	private void GetList() {

		grdPaymentByEntDaily.PageIndex = 0;
		grdPaymentByEntDaily.DataSourceID = "dsPaymentByEntDaily";
		DataBind();
	}

	private void CreateList() {
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		int iStartYear = 2009;
		int iCurYear = DateTime.Now.Year;
		lstYear.Items.Clear();

		for (int i = 0;i + iStartYear <= iCurYear;i++) {

			lstYear.Items.Add(new ListItem(iBridUtil.GetStringValue(i + iStartYear)));
		}

		lstMonth.Items.Clear();
		for (int i = 1;i <= 12;i++) {
			lstMonth.Items.Add(new ListItem(string.Format("{0:D2}",i)));
		}
	}

	private bool IsCorrectDate() {
		return true;
	}

	protected string GetDisplayReportDay(string pReportDay) {
		DateTime dtReportDay = DateTime.Parse(pReportDay);
		return dtReportDay.Day.ToString();
	}

	protected string GetDisplayDayOfWeek(string pReportDay) {
		DateTime dtReportDay = DateTime.Parse(pReportDay);
		int iDayOfWeekNo = (int)dtReportDay.DayOfWeek;

		return Week[iDayOfWeekNo];
	}
}
