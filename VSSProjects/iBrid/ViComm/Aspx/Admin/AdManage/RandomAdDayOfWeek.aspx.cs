﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 曜日別ランダム広告設定
--	Progaram ID		: RandomAdDayOfWeek
--
--  Creation Date	: 2010.03.24
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Site_RandomAdDayOfWeek:System.Web.UI.Page {
	private string SexCd {
		get { return iBridUtil.GetStringValue(this.ViewState["SexCd"]); }
		set { this.ViewState["SexCd"] = value; }
	}

	private bool DisablePinEdit {
		get {
			return (bool)this.ViewState["DisablePinEdit"];
		}
		set {
			this.ViewState["DisablePinEdit"] = value;
		}
	}

	private string HtmlDoc {
		get {
			if (this.DisablePinEdit) {
				return this.txtHtmlDocText.Text;
			} else {
				return this.txtHtmlDoc.Text;
			}
		}
		set {
			if (this.DisablePinEdit) {
				this.txtHtmlDocText.Text = value;
			} else {
				this.txtHtmlDoc.Text = value;
			}
		}
	}

	private Stream filter;

	protected void Page_Load(object sender,EventArgs e) {
		txtHtmlDoc.EditorDirectory = string.Format("http://{0}:{1}/pinEdit/editor/",Request.UrlReferrer.Host,Request.UrlReferrer.Port);
		filter = Response.Filter;
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);

		if (!IsPostBack) {
			this.SexCd = this.Request.QueryString["sexcd"];
			FirstLoad();
			InitPage();
		}

		switch (this.SexCd) {
			case ViCommConst.MAN:
				this.Title = "曜日別ランダム広告設定（会員）";
				this.lblPgmTitle.Text = "曜日別ランダム広告設定（会員）";
				break;
			case ViCommConst.OPERATOR:
				this.Title = "曜日別ランダム広告設定（出演者）";
				this.lblPgmTitle.Text = "曜日別ランダム広告設定（出演者）";
				break;
		}
	}

	private void FirstLoad() {
		grdRandomAdDayOfWeek.PageSize = 10;
		DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		lstSiteCd.DataSourceID = "";
		lstSeekSiteCd.DataSourceID = "";

		using (ManageCompany oManageCompany = new ManageCompany()) {
			this.DisablePinEdit = oManageCompany.IsAvailableService(ViCommConst.RELEASE_DISABLE_PINEDIT,2);
			this.txtHtmlDoc.Visible = !this.DisablePinEdit;
			this.txtHtmlDocText.Visible = this.DisablePinEdit;
		}
	}

	private void InitPage() {
		ClearField();
		pnlMainte.Visible = false;
		grdOutLookAd.DataSourceID = "";
		DataBind();
		SetGridColor();
	}

	private void ClearField() {
		lblDayCd.Text = "";
		lblDayNm.Text = "";
		lblAd.Text = "";
		lblAdNo.Text = "";
		this.HtmlDoc = "";
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		RegistData();
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void lnkHtmlAd_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		lstSiteCd.SelectedValue = sKeys[0];
		lblDayCd.Text = sKeys[1];
		lblDayNm.Text = string.Format("{0}曜日",sKeys[2]);
		lblAd.Text = string.Format("広告{0}",sKeys[3]);
		lblAdNo.Text = sKeys[3];
		GetData();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected string EditHtmlDoc(object pHtmlDoc) {
		return pHtmlDoc.ToString();
	}

	private void GetList() {
		grdRandomAdDayOfWeek.PageIndex = 0;
		grdRandomAdDayOfWeek.DataBind();
		SetGridColor();
	}

	private void GetData() {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("RANDOM_AD_DAYOFWEEK_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PDAY_CD",DbSession.DbType.NUMBER,lblDayCd.Text);
			db.ProcedureInParm("pSEX_CD", DbSession.DbType.VARCHAR2, this.SexCd);
			db.ProcedureOutParm("PHTML_AD1", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHTML_AD2",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHTML_AD3",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHTML_AD4",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHTML_AD5",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHTML_AD6",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHTML_AD7",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHTML_AD8",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHTML_AD9",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHTML_AD10",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();

			ViewState["PREVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["PROWID"] = db.GetStringValue("PROWID");

			this.HtmlDoc = "";
			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				this.HtmlDoc = db.GetStringValue(string.Format("PHTML_AD{0}",lblAdNo.Text));
			} else {
				this.HtmlDoc = "";
			}
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;
		SetColor();
		grdOutLookAd.DataSourceID = "dsOutLookAd";
		grdOutLookAd.DataBind();
	}

	private void UpdateData(int pDelFlag) {
		
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("RANDOM_AD_DAYOFWEEK_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PDAY_CD",DbSession.DbType.VARCHAR2,lblDayCd.Text);
			db.ProcedureInParm("pSEX_CD", DbSession.DbType.VARCHAR2, this.SexCd);
			db.ProcedureInParm("PHTML_AD1", DbSession.DbType.VARCHAR2, SetParamHtmlAd("1"));
			db.ProcedureInParm("PHTML_AD2",DbSession.DbType.VARCHAR2,SetParamHtmlAd("2"));
			db.ProcedureInParm("PHTML_AD3",DbSession.DbType.VARCHAR2,SetParamHtmlAd("3"));
			db.ProcedureInParm("PHTML_AD4",DbSession.DbType.VARCHAR2,SetParamHtmlAd("4"));
			db.ProcedureInParm("PHTML_AD5",DbSession.DbType.VARCHAR2,SetParamHtmlAd("5"));
			db.ProcedureInParm("PHTML_AD6",DbSession.DbType.VARCHAR2,SetParamHtmlAd("6"));
			db.ProcedureInParm("PHTML_AD7",DbSession.DbType.VARCHAR2,SetParamHtmlAd("7"));
			db.ProcedureInParm("PHTML_AD8",DbSession.DbType.VARCHAR2,SetParamHtmlAd("8"));
			db.ProcedureInParm("PHTML_AD9",DbSession.DbType.VARCHAR2,SetParamHtmlAd("9"));
			db.ProcedureInParm("PHTML_AD10",DbSession.DbType.VARCHAR2,SetParamHtmlAd("10"));
			db.ProcedureInParm("PREGIST_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			db.ProcedureInParm("PAD_NO",DbSession.DbType.VARCHAR2,lblAdNo.Text);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["PROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,ViewState["PREVISION_NO"].ToString());
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		int iIdx = lstSiteCd.SelectedIndex;
		InitPage();
		lstSeekSiteCd.SelectedIndex = iIdx;
		GetList();
	}

	private string SetParamHtmlAd(string pAdNo) {
		if (pAdNo.Equals(lblAdNo.Text)) {
			return HttpUtility.HtmlDecode(this.HtmlDoc);
		} else {
			return "";
		}
	}

	private void RegistData() {
		bool bExist;

		//存在チェック
		using (RandomAdDayOfWeek oRandomAd = new RandomAdDayOfWeek()) {
			bExist = oRandomAd.IsExistBySite(lstSeekSiteCd.SelectedValue,this.SexCd);
		}

		//追加
		if (!bExist) {
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("RANDOM_AD_DAYOFWEEK_MAINTE");
				db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSeekSiteCd.SelectedValue);
				db.ProcedureInParm("PDAY_CD",DbSession.DbType.VARCHAR2,DBNull.Value);
				db.ProcedureInParm("pSEX_CD", DbSession.DbType.VARCHAR2, this.SexCd);
				db.ProcedureInParm("PHTML_AD1", DbSession.DbType.VARCHAR2, DBNull.Value);
				db.ProcedureInParm("PHTML_AD2",DbSession.DbType.VARCHAR2,DBNull.Value);
				db.ProcedureInParm("PHTML_AD3",DbSession.DbType.VARCHAR2,DBNull.Value);
				db.ProcedureInParm("PHTML_AD4",DbSession.DbType.VARCHAR2,DBNull.Value);
				db.ProcedureInParm("PHTML_AD5",DbSession.DbType.VARCHAR2,DBNull.Value);
				db.ProcedureInParm("PHTML_AD6",DbSession.DbType.VARCHAR2,DBNull.Value);
				db.ProcedureInParm("PHTML_AD7",DbSession.DbType.VARCHAR2,DBNull.Value);
				db.ProcedureInParm("PHTML_AD8",DbSession.DbType.VARCHAR2,DBNull.Value);
				db.ProcedureInParm("PHTML_AD9",DbSession.DbType.VARCHAR2,DBNull.Value);
				db.ProcedureInParm("PHTML_AD10",DbSession.DbType.VARCHAR2,DBNull.Value);
				db.ProcedureInParm("PREGIST_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_ON);
				db.ProcedureInParm("PAD_NO",DbSession.DbType.VARCHAR2,DBNull.Value);
				db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,DBNull.Value);
				db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,DBNull.Value);
				db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
			}
		}
		GetList();
	}

	protected void dsRandomAdDayOfWeek_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
	}

	protected void dsRandomAdDayOfWeek_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		DataSet oDataSet = e.ReturnValue as DataSet;
		if (oDataSet != null) {
			if (oDataSet.Tables[0].Rows.Count > 0) {
				this.btnRegist.Enabled = false;
			} else {
				this.btnRegist.Enabled = true;
			}
		}
	}

	protected void dsOutLookAd_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = lblDayCd.Text;
		e.InputParameters[2] = lblAdNo.Text;
	}

	private void SetColor() {
		string sForeColor;
		string sBackColor;
		string sLinkColor;
		
		using (Site oSite = new Site()) {
			oSite.GetOne(lstSiteCd.SelectedValue);
			sForeColor = oSite.colorChar;
			sBackColor = oSite.colorBack;
			sLinkColor = oSite.colorLink;
		}

		ltlUserColor.Text = "<style type=\"text/css\">\r\n" +
							"<!--\r\n" +
							".userColor { background-color: " + sBackColor + ";}\r\n" +
							".userColor TD{ color: " + sForeColor + ";}\r\n" +
							".userColor TD A{color: " + sLinkColor + ";font-weight: bold;text-decoration: none;background-color: inherit;}\r\n" +
							"-->\r\n" +
							"</style>";
	}

	private void SetGridColor() {
		foreach (GridViewRow gvr in grdRandomAdDayOfWeek.Rows) {
			//曜日セルの色を設定
			Label lbl = (Label)gvr.FindControl("lblDayCd") as Label;
			TableCell tc = (TableCell)lbl.Parent as TableCell;
			tc.BackColor = GetBackColor(lbl.Text);
		}
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		//Response.Filter = filter;
		//Response.AddHeader("Content-Disposition","attachment;filename=RandomAdDayOfWeek.csv");
		//Response.ContentType = "application/octet-stream-dummy";
		//System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		//using (RandomAdDayOfWeek oRandomAd = new RandomAdDayOfWeek()) {
		//    DataSet ds = oRandomAd.GetCsvData(lstSeekSiteCd.SelectedValue);
		//    SetCsvData(ds);
		//    Response.End();
		//}
	}

	public void SetCsvData(DataSet pDataSet) {

		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");
		string sDtl = "";

		DataRow dr = null;
		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];

			sDtl = string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}",
							dr["SITE_CD"].ToString(),
							dr["DAY_NM"].ToString(),
							dr["HTML_AD1"].ToString(),
							dr["HTML_AD2"].ToString(),
							dr["HTML_AD3"].ToString(),
							dr["HTML_AD4"].ToString(),
							dr["HTML_AD5"].ToString(),
							dr["HTML_AD6"].ToString(),
							dr["HTML_AD7"].ToString(),
							dr["HTML_AD8"].ToString(),
							dr["HTML_AD9"].ToString(),
							dr["HTML_AD10"].ToString());
			sDtl = sDtl.Replace("\r","");
			sDtl = sDtl.Replace("\n","");
			sDtl += "\r\n";
			Response.BinaryWrite(encoding.GetBytes(sDtl));
		}
	}

	protected Color GetBackColor(object pDayCd) {
		if (pDayCd.ToString().Equals("7")) {
			//土曜日
			return Color.FromArgb(0xCCCCFF);
		} else {
			if (pDayCd.ToString().Equals("1")) {
				//日曜日
				return Color.FromArgb(0xFFCCCC);
			} else {
				return Color.Empty;
			}
		}
	}
}
