﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者代理店別広告集計
--	Progaram ID		: AdGroupDataCastMonthlyList
--  Creation Date	: 2014.06.17
--  Creater			: Y.Ikemiya
**************************************************************************/
using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using ViComm.Extension.Pwild;

public partial class AdManage_AdGroupDataCastMonthlyList:System.Web.UI.Page {

	private string recCount = string.Empty;

	private double AdCost = 0;
	private double AccessCount = 0;
	private double RegistCount = 0;
	private double RegistCountAd = 0;
	private double LoginCount = 0;
	private double GetPointCastCount = 0;
	private double StartPaymentCastCount = 0;
	private double EntMonthPaymentCount = 0;
	private double EntMonthPaymentAmt = 0;
	private double RepeatPaymentCount = 0;
	private double RepeatPaymentAmt = 0;
	private double TotalPaymentCount = 0;
	private double TotalPaymentAmt = 0;
	private double AggrPaymentCastCount = 0;
	private double AggrPaymentAmt = 0;
	private double AggrAdCost = 0;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SITE_CD"]);
		}
		set {
			this.ViewState["SITE_CD"] = value;
		}
	}

	private string Year {
		get {
			return iBridUtil.GetStringValue(this.ViewState["YEAR"]);
		}
		set {
			this.ViewState["YEAR"] = value;
		}
	}

	private string Month {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MONTH"]);
		}
		set {
			this.ViewState["MONTH"] = value;
		}
	}

	private string SortExpression {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortExpression"]);
		}
		set {
			this.ViewState["SortExpression"] = value;
		}
	}

	private string SortDirect {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortDirect"]);
		}
		set {
			this.ViewState["SortDirect"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			this.Year = iBridUtil.GetStringValue(Request.QueryString["year"]);
			this.Month = iBridUtil.GetStringValue(Request.QueryString["month"]);

			this.CreateList();
			this.InitPage();

			if (!string.IsNullOrEmpty(this.Year) && !string.IsNullOrEmpty(this.Month)) {
				this.lstYear.SelectedValue = this.Year;
				this.lstMonth.SelectedValue = this.Month;
			}

			pnlGrid.DataBind();
		}
	}

	protected void dsAdDataCastMonthly_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {

		AdDataCastMonthly.SearchCondition oSearchCondition = new AdDataCastMonthly.SearchCondition();
		oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
		oSearchCondition.ReportMonth = string.Format("{0}/{1}",this.lstYear.SelectedValue,this.lstMonth.SelectedValue);
		oSearchCondition.SortDirection = this.SortDirect;
		oSearchCondition.SortExpression = this.SortExpression;

		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsAdDataCastMonthly_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		if (!this.IsCorrectDate()) {
			return;
		}

		this.SortDirect = string.Empty;
		this.SortExpression = string.Empty;

		this.grdAdDataCastMonthly.PageIndex = 0;
		this.GetList();
	}

	protected void grdAdDataCastMonthly_Sorting(object sender,GridViewSortEventArgs e) {
		if (this.SortExpression.Equals(e.SortExpression)) {
			if (this.SortDirect.Equals("ASC")) {
				this.SortDirect = "DESC";
			} else if (this.SortDirect.Equals("DESC")) {
				this.SortDirect = "ASC";
			}
		} else {
			this.SortDirect = "DESC";
		}

		this.SortExpression = e.SortExpression;

		e.Cancel = true;
		this.GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	private void GetList() {
		this.grdAdDataCastMonthly.PageIndex = 0;
		this.grdAdDataCastMonthly.PageSize = 9999;
		this.grdAdDataCastMonthly.DataSourceID = "dsAdDataCastMonthly";
		this.grdAdDataCastMonthly.DataBind();
		this.pnlCount.DataBind();
	}

	protected void grdAdDataCastMonthly_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			DataRowView drv = (DataRowView)e.Row.DataItem;
			AdCost = AdCost + double.Parse(iBridUtil.GetStringValue(drv["AD_COST"]));
			AccessCount = AccessCount + double.Parse(iBridUtil.GetStringValue(drv["ACCESS_COUNT"]));
			RegistCount = RegistCount + double.Parse(iBridUtil.GetStringValue(drv["REGIST_COUNT"]));
			RegistCountAd = RegistCountAd + double.Parse(iBridUtil.GetStringValue(drv["REGIST_COUNT_AD"]));
			LoginCount = LoginCount + double.Parse(iBridUtil.GetStringValue(drv["LOGIN_COUNT"]));
			GetPointCastCount = GetPointCastCount + double.Parse(iBridUtil.GetStringValue(drv["GET_POINT_CAST_COUNT"]));
			StartPaymentCastCount = StartPaymentCastCount + double.Parse(iBridUtil.GetStringValue(drv["START_PAYMENT_CAST_COUNT"]));
			EntMonthPaymentCount = EntMonthPaymentCount + double.Parse(iBridUtil.GetStringValue(drv["ENT_MONTH_PAYMENT_COUNT"]));
			EntMonthPaymentAmt = EntMonthPaymentAmt + double.Parse(iBridUtil.GetStringValue(drv["ENT_MONTH_PAYMENT_AMT"]));
			RepeatPaymentCount = RepeatPaymentCount + double.Parse(iBridUtil.GetStringValue(drv["REPEAT_PAYMENT_COUNT"]));
			RepeatPaymentAmt = RepeatPaymentAmt + double.Parse(iBridUtil.GetStringValue(drv["REPEAT_PAYMENT_AMT"]));
			TotalPaymentCount = TotalPaymentCount + double.Parse(iBridUtil.GetStringValue(drv["TOTAL_PAYMENT_COUNT"]));
			TotalPaymentAmt = TotalPaymentAmt + double.Parse(iBridUtil.GetStringValue(drv["TOTAL_PAYMENT_AMT"]));
			AggrAdCost = AggrAdCost + double.Parse(iBridUtil.GetStringValue(drv["AGGR_AD_COST"]));
			AggrPaymentCastCount = AggrPaymentCastCount + double.Parse(iBridUtil.GetStringValue(drv["AGGR_PAYMENT_CAST_COUNT"]));
			AggrPaymentAmt = AggrPaymentAmt + double.Parse(iBridUtil.GetStringValue(drv["AGGR_PAYMENT_AMT"]));

		} else if (e.Row.RowType == DataControlRowType.Footer) {
			double dAdCost = 0;
			double dAccessCount = 0;
			double dRegistCount = 0;
			double dRegistCountAd = 0;
			double dLoginCount = 0;
			double dGetPointCastCount = 0;
			double dStartPaymentCastCount = 0;
			double dEntMonthPaymentCount = 0;
			double dEntMonthPaymentAmt = 0;
			double dRepeatPaymentCount = 0;
			double dRepeatPaymentAmt = 0;
			double dTotalPaymentCount = 0;
			double dTotalPaymentAmt = 0;
			double dAggrAdCost = 0;
			double dAggrPaymentCastCount = 0;
			double dAggrPaymentAmt = 0;

			Label lblAdCostFooter1 = (Label)e.Row.FindControl("lblAdCostFooter1");
			Label lblAdCostFooter2 = (Label)e.Row.FindControl("lblAdCostFooter2");
			Label lblAccessCountFooter1 = (Label)e.Row.FindControl("lblAccessCountFooter1");
			Label lblAccessCountFooter2 = (Label)e.Row.FindControl("lblAccessCountFooter2");
			Label lblRegistCountFooter1 = (Label)e.Row.FindControl("lblRegistCountFooter1");
			Label lblRegistCountFooter2 = (Label)e.Row.FindControl("lblRegistCountFooter2");
			Label lblRegistRateFooter1 = (Label)e.Row.FindControl("lblRegistRateFooter1");
			Label lblRegistRateFooter2 = (Label)e.Row.FindControl("lblRegistRateFooter2");
			Label lblRegistCountAdFooter1 = (Label)e.Row.FindControl("lblRegistCountAdFooter1");
			Label lblRegistCountAdFooter2 = (Label)e.Row.FindControl("lblRegistCountAdFooter2");
			Label lblLoginCountFooter1 = (Label)e.Row.FindControl("lblLoginCountFooter1");
			Label lblLoginCountFooter2 = (Label)e.Row.FindControl("lblLoginCountFooter2");
			Label lblAdCostByRegistFooter1 = (Label)e.Row.FindControl("lblAdCostByRegistFooter1");
			Label lblAdCostByRegistFooter2 = (Label)e.Row.FindControl("lblAdCostByRegistFooter2");
			Label lblGetPointCastCountFooter1 = (Label)e.Row.FindControl("lblGetPointCastCountFooter1");
			Label lblGetPointCastCountFooter2 = (Label)e.Row.FindControl("lblGetPointCastCountFooter2");
			Label lblGetPointCastRateFooter1 = (Label)e.Row.FindControl("lblGetPointCastRateFooter1");
			Label lblGetPointCastRateFooter2 = (Label)e.Row.FindControl("lblGetPointCastRateFooter2");
			Label lblStartPaymentCastCountFooter1 = (Label)e.Row.FindControl("lblStartPaymentCastCountFooter1");
			Label lblStartPaymentCastCountFooter2 = (Label)e.Row.FindControl("lblStartPaymentCastCountFooter2");
			Label lblStartPaymentCastRateFooter1 = (Label)e.Row.FindControl("lblStartPaymentCastRateFooter1");
			Label lblStartPaymentCastRateFooter2 = (Label)e.Row.FindControl("lblStartPaymentCastRateFooter2");
			Label lblAdCostByStartPaymentFooter1 = (Label)e.Row.FindControl("lblAdCostByStartPaymentFooter1");
			Label lblAdCostByStartPaymentFooter2 = (Label)e.Row.FindControl("lblAdCostByStartPaymentFooter2");
			Label lblEntMonthPaymentCountFooter1 = (Label)e.Row.FindControl("lblEntMonthPaymentCountFooter1");
			Label lblEntMonthPaymentCountFooter2 = (Label)e.Row.FindControl("lblEntMonthPaymentCountFooter2");
			Label lblEntMonthPaymentAmtFooter1 = (Label)e.Row.FindControl("lblEntMonthPaymentAmtFooter1");
			Label lblEntMonthPaymentAmtFooter2 = (Label)e.Row.FindControl("lblEntMonthPaymentAmtFooter2");
			Label lblEntMonthReturnRateFooter1 = (Label)e.Row.FindControl("lblEntMonthReturnRateFooter1");
			Label lblEntMonthReturnRateFooter2 = (Label)e.Row.FindControl("lblEntMonthReturnRateFooter2");
			Label lblRepeatPaymentCountFooter1 = (Label)e.Row.FindControl("lblRepeatPaymentCountFooter1");
			Label lblRepeatPaymentCountFooter2 = (Label)e.Row.FindControl("lblRepeatPaymentCountFooter2");
			Label lblRepeatPaymentAmtFooter1 = (Label)e.Row.FindControl("lblRepeatPaymentAmtFooter1");
			Label lblRepeatPaymentAmtFooter2 = (Label)e.Row.FindControl("lblRepeatPaymentAmtFooter2");
			Label lblTotalPaymentCountFooter1 = (Label)e.Row.FindControl("lblTotalPaymentCountFooter1");
			Label lblTotalPaymentCountFooter2 = (Label)e.Row.FindControl("lblTotalPaymentCountFooter2");
			Label lblTotalPaymentAmtFooter1 = (Label)e.Row.FindControl("lblTotalPaymentAmtFooter1");
			Label lblTotalPaymentAmtFooter2 = (Label)e.Row.FindControl("lblTotalPaymentAmtFooter2");
			Label lblRepeatRateFooter1 = (Label)e.Row.FindControl("lblRepeatRateFooter1");
			Label lblRepeatRateFooter2 = (Label)e.Row.FindControl("lblRepeatRateFooter2");
			Label lblAggrAdRateFooter1 = (Label)e.Row.FindControl("lblAggrAdRateFooter1");
			Label lblAggrAdRateFooter2 = (Label)e.Row.FindControl("lblAggrAdRateFooter2");

			DataSet oDataSet = null;
			using (AdDataCastMonthly oAdDataCastMonthly = new AdDataCastMonthly()) {
				oDataSet = oAdDataCastMonthly.GetTotalByNoAdGroup(lstSiteCd.SelectedValue,string.Format("{0}/{1}",lstYear.SelectedValue,lstMonth.SelectedValue));
			}

			if (oDataSet.Tables[0].Rows.Count > 0) {
				dAdCost = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["AD_COST"]));
				dAccessCount = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["ACCESS_COUNT"]));
				dRegistCount = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["REGIST_COUNT"]));
				dRegistCountAd = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["REGIST_COUNT_AD"]));
				dLoginCount = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["LOGIN_COUNT"]));
				dGetPointCastCount = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["GET_POINT_CAST_COUNT"]));
				dStartPaymentCastCount = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["START_PAYMENT_CAST_COUNT"]));
				dEntMonthPaymentCount = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["ENT_MONTH_PAYMENT_COUNT"]));
				dEntMonthPaymentAmt = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["ENT_MONTH_PAYMENT_AMT"]));
				dRepeatPaymentCount = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["REPEAT_PAYMENT_COUNT"]));
				dRepeatPaymentAmt = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["REPEAT_PAYMENT_AMT"]));
				dTotalPaymentCount = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["TOTAL_PAYMENT_COUNT"]));
				dTotalPaymentAmt = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["TOTAL_PAYMENT_AMT"]));
				dAggrAdCost = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["AGGR_AD_COST"]));
				dAggrPaymentCastCount = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["AGGR_PAYMENT_CAST_COUNT"]));
				dAggrPaymentAmt = double.Parse(iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["AGGR_PAYMENT_AMT"]));

				AdCost = AdCost + dAdCost;
				AccessCount = AccessCount + dAccessCount;
				RegistCount = RegistCount + dRegistCount;
				RegistCountAd = RegistCountAd + dRegistCountAd;
				LoginCount = LoginCount + dLoginCount;
				GetPointCastCount = GetPointCastCount + dGetPointCastCount;
				StartPaymentCastCount = StartPaymentCastCount + dStartPaymentCastCount;
				EntMonthPaymentCount = EntMonthPaymentCount + dEntMonthPaymentCount;
				EntMonthPaymentAmt = EntMonthPaymentAmt + dEntMonthPaymentAmt;
				RepeatPaymentCount = RepeatPaymentCount + dRepeatPaymentCount;
				RepeatPaymentAmt = RepeatPaymentAmt + dRepeatPaymentAmt;
				TotalPaymentCount = TotalPaymentCount + dTotalPaymentCount;
				TotalPaymentAmt = TotalPaymentAmt + dTotalPaymentAmt;
				AggrPaymentCastCount = AggrPaymentAmt + dAggrPaymentCastCount;
				AggrPaymentAmt = AggrPaymentAmt + dAggrPaymentAmt;
				AggrAdCost = AggrAdCost + dAggrAdCost;

				lblAdCostFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["AD_COST"]);
				lblAccessCountFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["ACCESS_COUNT"]);
				lblRegistCountFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["REGIST_COUNT"]);
				lblRegistRateFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["REGIST_RATE"]);
				lblRegistCountAdFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["REGIST_COUNT_AD"]);
				lblLoginCountFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["LOGIN_COUNT"]);
				lblAdCostByRegistFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["AD_COST_BY_REGIST"]);
				lblGetPointCastCountFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["GET_POINT_CAST_COUNT"]);
				lblGetPointCastRateFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["GET_POINT_CAST_RATE"]);
				lblStartPaymentCastCountFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["START_PAYMENT_CAST_COUNT"]);
				lblStartPaymentCastRateFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["START_PAYMENT_CAST_RATE"]);
				lblAdCostByStartPaymentFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["AD_COST_BY_START_PAYMENT"]);
				lblEntMonthPaymentCountFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["ENT_MONTH_PAYMENT_COUNT"]);
				lblEntMonthPaymentAmtFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["ENT_MONTH_PAYMENT_AMT"]);
				lblEntMonthReturnRateFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["ENT_MONTH_RETURN_RATE"]);
				lblRepeatPaymentCountFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["REPEAT_PAYMENT_COUNT"]);
				lblRepeatPaymentAmtFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["REPEAT_PAYMENT_AMT"]);
				lblTotalPaymentCountFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["TOTAL_PAYMENT_COUNT"]);
				lblTotalPaymentAmtFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["TOTAL_PAYMENT_AMT"]);
				lblRepeatRateFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["REPEAT_RATE"]);
				lblAggrAdRateFooter1.Text = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["AGGR_AD_COST_RATE"]);
			}

			lblAdCostFooter2.Text = iBridUtil.GetStringValue(AdCost);
			lblAccessCountFooter2.Text = iBridUtil.GetStringValue(AccessCount);
			lblRegistCountFooter2.Text = iBridUtil.GetStringValue(RegistCount);

			lblRegistRateFooter2.Text = "-";
			if (AccessCount > 0) {
				double dRegistRate;
				dRegistRate = GetDecimalValue(RegistCount / AccessCount * 100);
				lblRegistRateFooter2.Text = iBridUtil.GetStringValue(dRegistRate);
			}

			lblRegistCountAdFooter2.Text = iBridUtil.GetStringValue(RegistCountAd);

			lblAdCostByRegistFooter2.Text = "-";
			if (RegistCount > 0) {
				double dAdCostByRegist = GetDecimalValue(AdCost / RegistCount);
				lblAdCostByRegistFooter2.Text = iBridUtil.GetStringValue(dAdCostByRegist);
			}

			lblLoginCountFooter2.Text = iBridUtil.GetStringValue(LoginCount);
			lblGetPointCastCountFooter2.Text = iBridUtil.GetStringValue(GetPointCastCount);

			lblGetPointCastRateFooter2.Text = "-";
			if (RegistCount > 0) {
				double dGetPointCastRate = GetDecimalValue(GetPointCastCount / RegistCount * 100);
				lblGetPointCastRateFooter2.Text = iBridUtil.GetStringValue(dGetPointCastRate);
			}

			lblStartPaymentCastCountFooter2.Text = iBridUtil.GetStringValue(StartPaymentCastCount);

			lblStartPaymentCastRateFooter2.Text = "-";
			if (RegistCount > 0) {
				double dStartPaymentCastRate = GetDecimalValue(StartPaymentCastCount / RegistCount * 100);
				lblStartPaymentCastRateFooter2.Text = iBridUtil.GetStringValue(dStartPaymentCastRate);
			}

			lblAdCostByStartPaymentFooter2.Text = "-";
			if (StartPaymentCastCount > 0) {
				double dAdCostByStartPayment = GetDecimalValue(AdCost / StartPaymentCastCount);
				lblAdCostByStartPaymentFooter2.Text = iBridUtil.GetStringValue(dAdCostByStartPayment);
			}

			lblEntMonthPaymentCountFooter2.Text = iBridUtil.GetStringValue(EntMonthPaymentCount);
			lblEntMonthPaymentAmtFooter2.Text = iBridUtil.GetStringValue(EntMonthPaymentAmt);

			lblEntMonthReturnRateFooter2.Text = "-";
			if (AdCost > 0) {
				double dReturnRate = GetDecimalValue(EntMonthPaymentAmt / AdCost * 100);
				lblEntMonthReturnRateFooter2.Text = iBridUtil.GetStringValue(AdCost);
			}

			lblRepeatPaymentCountFooter2.Text = iBridUtil.GetStringValue(RepeatPaymentCount);
			lblRepeatPaymentAmtFooter2.Text = iBridUtil.GetStringValue(RepeatPaymentAmt);
			lblTotalPaymentCountFooter2.Text = iBridUtil.GetStringValue(TotalPaymentCount);
			lblTotalPaymentAmtFooter2.Text = iBridUtil.GetStringValue(TotalPaymentAmt);

			lblRepeatRateFooter2.Text = "-";
			if (AggrPaymentCastCount > 0) {
				double dRepeatRate = GetDecimalValue(RepeatPaymentCount / AggrPaymentCastCount * 100);
				lblRepeatRateFooter2.Text = iBridUtil.GetStringValue(dRepeatRate);
			}

			lblAggrAdRateFooter2.Text = "-";
			if (AggrPaymentAmt > 0) {
				double dAggrAdRate = GetDecimalValue(AggrAdCost / AggrPaymentAmt * 100);
				lblAggrAdRateFooter2.Text = iBridUtil.GetStringValue(dAggrAdRate);
			}
		}
	}

	private double GetDecimalValue(double pValue) {
		double dTmpVal = pValue * 100;
		return Math.Floor(dTmpVal) / 100;
	}

	protected string AddPercentMark(string pValue) {
		if (!string.IsNullOrEmpty(pValue)) {
			pValue = pValue + "%";
		}

		return pValue;
	}

	private void InitPage() {
		this.lstYear.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstMonth.SelectedValue = DateTime.Now.ToString("MM");
	}

	private void CreateList() {
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		int iStartYear = 2009;
		int iCurYear = DateTime.Now.Year;
		lstYear.Items.Clear();

		for (int i = 0;i + iStartYear <= iCurYear;i++) {

			lstYear.Items.Add(new ListItem(iBridUtil.GetStringValue(i + iStartYear)));
		}

		lstMonth.Items.Clear();
		for (int i = 1;i <= 12;i++) {
			lstMonth.Items.Add(new ListItem(string.Format("{0:D2}",i)));
		}
	}

	private bool IsCorrectDate() {
		return true;
	}

	protected string GetRecCount() {
		return this.recCount;
	}
}