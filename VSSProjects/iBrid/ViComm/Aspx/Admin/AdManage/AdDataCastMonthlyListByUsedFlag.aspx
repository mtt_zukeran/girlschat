﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AdDataCastMonthlyListByUsedFlag.aspx.cs" Inherits="AdManage_AdDataCastMonthlyListByUsedFlag" Title="媒体一覧(出演者)" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="媒体一覧(出演者)"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[検索条件]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 800px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                表示月
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstYear" runat="server" Width="60px">
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstMonth" runat="server" Width="40px">
                                </asp:DropDownList>月
                                <br />
								<asp:Label ID="lblErrorMessageReportDayFrom" runat="server" ForeColor="red" Visible="false"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" />
            </fieldset>
        </asp:Panel>
		<asp:Panel runat="server" ID="pnlList">
			<fieldset>
				<legend>[集計]</legend>
				<asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdAdDataCastMonthly.PageIndex + 1%>
                        of
                        <%= grdAdDataCastMonthly.PageCount%>
                    </a>
                </asp:Panel>
                &nbsp;<br />
				<asp:Panel ID="pnlGrid" runat="server">
					<asp:GridView ID="grdAdDataCastMonthly" DataSourceID="dsAdDataCastMonthly" runat="server" AllowPaging="true" AutoGenerateColumns="False" OnSorting="grdAdDataCastMonthly_Sorting" PageSize="9999"
						EnableSortingAndPagingCallbacks="false" ShowFooter="false" SkinID="GridViewColor" AllowSorting="true" OnRowDataBound="grdAdDataCastMonthly_RowDataBound">
						<Columns>
							<asp:TemplateField HeaderText="代理店名">
								<ItemTemplate>
									<asp:Label ID="lblAdGroupNm" runat="server" Text='<%# Eval("AD_GROUP_NM") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle Wrap="false" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="広告名">
								<ItemTemplate>
									<asp:HyperLink ID="lnkAdNm" runat="server" NavigateUrl='<%# string.Format("~/AdManage/AdDataCastDailyList.aspx?sitecd={0}&adcd={1}&year={2}&month={3}",Eval("SITE_CD"),Eval("AD_CD"),lstYear.SelectedValue,lstMonth.SelectedValue) %>' Text='<%# Eval("AD_NM") %>' ></asp:HyperLink>&nbsp;
									<asp:HyperLink ID="lnkAd" runat="server" NavigateUrl='<%# string.Format("~/AdManage/AdList.aspx?adcd={0}",Eval("AD_CD")) %>' Text='設定' ></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle Wrap="false" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="広告費" SortExpression="AD_COST">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblAdCost" runat="server" Text='<%# Eval("AD_COST") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ｱｸｾｽ数" SortExpression="ACCESS_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblAccessCount" runat="server" Text='<%# Eval("ACCESS_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="登録数" SortExpression="REGIST_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblRegistCount" runat="server" Text='<%# Eval("REGIST_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="登録率" SortExpression="REGIST_RATE">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblRegistRate" runat="server" Text='<%# Eval("REGIST_RATE") %>'></asp:Label>%
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="認証数" SortExpression="REGIST_COUNT_AD">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblRegistCountAd" runat="server" Text='<%# Eval("REGIST_COUNT_AD") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="獲得単価" SortExpression="AD_COST_BY_REGIST">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblAdCostByRegist" runat="server" Text='<%# Eval("AD_COST_BY_REGIST") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾛｸﾞｲﾝ数" SortExpression="LOGIN_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblLoginCount" runat="server" Text='<%# Eval("LOGIN_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾎﾟｲﾝﾄ獲得<br>人数" SortExpression="GET_POINT_CAST_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblGetPointCastCount" runat="server" Text='<%# Eval("GET_POINT_CAST_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾎﾟｲﾝﾄ獲得<br>獲得率" SortExpression="GET_POINT_CAST_RATECOST">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblGetPointCastRate" runat="server" Text='<%# Eval("GET_POINT_CAST_RATE") %>'></asp:Label>%
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="初顧客<br>人数" SortExpression="START_PAYMENT_CAST_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblStartPaymentCastCount" runat="server" Text='<%# Eval("START_PAYMENT_CAST_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="初顧客<br>報酬率" SortExpression="START_PAYMENT_CAST_RATE">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblStartPaymentCastRate" runat="server" Text='<%# Eval("START_PAYMENT_CAST_RATE") %>'></asp:Label>%
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="顧客単価" SortExpression="AD_COST_BY_REGIST">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblAdCostByRegist" runat="server" Text='<%# Eval("AD_COST_BY_REGIST") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="当月報酬<br>人数" SortExpression="ENT_MONTH_PAYMENT_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblEntMonthPaymentCount" runat="server" Text='<%# Eval("ENT_MONTH_PAYMENT_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="当月報酬<br>金額" SortExpression="ENT_MONTH_PAYMENT_AMT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblEntMonthPaymentAmt" runat="server" Text='<%# Eval("ENT_MONTH_PAYMENT_AMT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="当月回収率" SortExpression="ENT_MONTH_RETURN_RATE">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblEntMonthReturnRate" runat="server" Text='<%# Eval("ENT_MONTH_RETURN_RATE") %>'></asp:Label>%
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾘﾋﾟｰﾄ報酬<br>人数" SortExpression="REPEAT_PAYMENT_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblRepeatPaymentCount" runat="server" Text='<%# Eval("REPEAT_PAYMENT_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾘﾋﾟｰﾄ報酬<br>金額" SortExpression="REPEAT_PAYMENT_AMT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblRepeatPaymentAmt" runat="server" Text='<%# Eval("REPEAT_PAYMENT_AMT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="合計報酬<br>人数" SortExpression="TOTAL_PAYMENT_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblTotalPaymentCount" runat="server" Text='<%# Eval("TOTAL_PAYMENT_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="合計報酬<br>金額" SortExpression="TOTAL_PAYMENT_AMT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblTotalPaymentAmt" runat="server" Text='<%# Eval("TOTAL_PAYMENT_AMT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾘﾋﾟｰﾄ率" SortExpression="REPEAT_RATE">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblRepeatRate" runat="server" Text='<%# Eval("REPEAT_RATE") %>'></asp:Label>%
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="累計広告比率" SortExpression="AGGR_AD_COST_RATE">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblAggrAdCostRate" runat="server" Text='<%# Eval("AGGR_AD_COST_RATE") %>'></asp:Label>%
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
							</asp:TemplateField>
						</Columns>
					</asp:GridView>
				</asp:Panel>
			</fieldset>
			<br />
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsAdDataCastMonthly" runat="server" ConvertNullToDBNull="false" EnablePaging="true" OnSelecting="dsAdDataCastMonthly_Selecting" OnSelected="dsAdDataCastMonthly_Selected"
		SelectMethod="GetPageCollection" SelectCountMethod="GetPageCount" SortParameterName="" TypeName="AdDataCastMonthly">
		<SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
</asp:Content>

