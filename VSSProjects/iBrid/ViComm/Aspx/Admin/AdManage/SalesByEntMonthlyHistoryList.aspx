﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SalesByEntMonthlyHistoryList.aspx.cs" Inherits="AdManage_SalesByEntMonthlyHistoryList" Title="入会月別売上履歴(会員)"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="入会月別売上履歴(会員)"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
                            <td class="tdHeaderStyle">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
			</asp:Panel>
		</fieldset>
		<fieldset>
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="560px">
				<asp:GridView ID="grdSalesByEntMonthly" runat="server" AutoGenerateColumns="False" DataSourceID="dsSalesByEntMonthly" CellPadding="0" ShowFooter="True" AllowSorting="True"
					OnRowDataBound="grdSalesByEntMonthly_RowDataBound" SkinID="GridViewColorNoRowStyle">
					<Columns>
						<asp:TemplateField HeaderText="月間集計" FooterText="合計">
							<ItemStyle HorizontalAlign="Center" />
							<ItemTemplate>
								<asp:Label ID="Label1" runat="server" Text='<%# GetDisplayReportMonth(Eval("REPORT_MONTH").ToString()) %>' BackColor=''></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="売上">
							<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
							<ItemTemplate>
								<asp:Label ID="Label2" runat="server" Text='<%# Eval("RECEIPT_AMT") %>' BackColor=''></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="件数">
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("RECEIPT_COUNT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
						</asp:TemplateField>
					</Columns>
					<FooterStyle ForeColor="Black" BackColor="LightYellow" />
				</asp:GridView>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsSalesByEntMonthly" runat="server" SelectMethod="GetSalesList" TypeName="SalesByEntMonthly" OnSelecting="dsSalesByEntMonthly_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
</asp:Content>
