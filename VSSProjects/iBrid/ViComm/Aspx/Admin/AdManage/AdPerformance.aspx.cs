﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 広告コード別稼動集計

--	Progaram ID		: AdPerformance
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------
  Date			Updater     Update Explain
  2010/08/13	K.Itoh		ﾕﾆｰｸ数の対応。＋できる限りリファクタ
-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class AdManage_AdPerformance:System.Web.UI.Page {
	private int accessCount;
	private int registMailFaildCount;
	private int loginCount;
	private int loginSuccessCount;
	private int loginSuccessCountRaw;
	private int loginSuccessUniqueCount;
	private int loginSuccessUniqueCountRaw;
	private int registCount;
	private int registCountRaw;
	private int reRegistCnt;
	private int withdrawalCnt;
	private int usedPoint;
	private int ordinarySalesAmt;
	private int affiliateSalesAmt;
	private int totalAmt;

	/*
	 * 数値のインデクサで列の制御をする処理が散乱すると可読性が極端に下がり、 
	 * また列に増減があった場合にエンバグする可能性が高くなるので、あらかじめ 
	 * 列Indexをフィールドとして定義しておいて利用する 
	 * 
	 * ×：this.grdAd.Columns[0].Visible=true;
	 * ○：this.grdAd.Columns[colAdCdIndex].Visible=true;
	 */
	protected int colAdCdIndex = 0;
	protected int colAdNmIndex = 1;
	protected int colAdGroupCdIndex = 2;
	protected int colAdGroupNmIndex = 3;
	protected int colAccessCountIndex = 4;
	protected int colRegistCountRawIndex = 5;
	protected int colRegistCountIndex = 6;
	protected int colWithdrawalIndex = 7;
	protected int colReRegistIndex = 8;
	protected int colRegistMailFaildCountIndex = 9;
	protected int colLoginCountIndex = 10;
	protected int colLoginSuccessCountRawIndex = 11;
	protected int colLoginSuccessCountIndex = 12;
	protected int colLoginSuccessCountUniqueRawIndex = 13;
	protected int colLoginSuccessCountUniqueIndex = 14;
	protected int colTotalUsedPointIndex = 15;
	protected int colOrdinalSalesIndex = 16;
	protected int colAffiliateSalesIndex = 17;
	protected int colTotalAmtIndex = 18;


	#region □■□ プロパティ □■□ ==================================================================================
	protected string SexCd {
		get {
			return ViewState["SEX_CD"] as string ?? string.Empty;
		}
		set {
			ViewState["SEX_CD"] = value;
		}
	}
	/// <summary>
	/// 男性表示モードかどうかを示す値を取得する。 
	/// </summary>
	protected bool IsManMode {
		get {
			return this.SexCd.Equals(ViCommConst.MAN);
		}
	}
	/// <summary>
	/// ﾛｸﾞｲﾝ中のｱｶｳﾝﾄが広告管理者かどうかを示す値を取得する。 
	/// </summary>
	protected bool IsAdManager {
		get {
			return Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_AD_MANAGE);
		}
	}
	/// <summary>
	/// ﾛｸﾞｲﾝ中のｱｶｳﾝﾄがプロダクション管理者かどうかを示す値を取得する。 
	/// </summary>
	protected bool IsProductionManager {
		get {
			return Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_PRODUCTION);
		}
	}

	/// <summary>
	/// ﾛｸﾞｲﾝ中のｱｶｳﾝﾄが売上閲覧許可ｱｶｳﾝﾄかどうかを示す値を取得する。 
	/// </summary>
	protected bool IsCanViewSales {
		get {
			return Session["ViewSalesFlag"].ToString().Equals(ViCommConst.FLAG_ON_STR);
		}
	}

	protected string DaysInMonthTo {
		get {
			return this.GetDaysInMonth(this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,this.lstToDD.SelectedItem.Text);
		}
	}
	#endregion ========================================================================================================

	protected void Page_Load(object sender,EventArgs e) {

		if (!IsPostBack) {
			this.grdAd.Columns[this.colLoginCountIndex].Visible = !ManageCompany.IsAvailableUniqueAccessSummary();
			this.grdAd.Columns[this.colLoginSuccessCountIndex].Visible = ManageCompany.IsAvailableUniqueAccessSummary();
			this.grdAd.Columns[this.colLoginSuccessCountUniqueIndex].Visible = ManageCompany.IsAvailableUniqueAccessSummary();

			InitPage();
			if (Session["SiteCd"].ToString().Equals(string.Empty)) {
				lstSiteCd.Items.Insert(0,new ListItem(string.Empty,string.Empty));
			}
			lstSiteCd.DataSourceID = string.Empty;
			if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals(string.Empty)) {
				lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			} else {
				lstSiteCd.SelectedIndex = 0;
			}
			this.SexCd = iBridUtil.GetStringValue(Request.QueryString["sexcd"]);

			if (IsManMode) {
				this.Title = this.Title + DisplayWordUtil.Replace("（会員）");
				lblPgmTitle.Text = lblPgmTitle.Text + DisplayWordUtil.Replace("（会員）");
				this.grdAd.Columns[this.colOrdinalSalesIndex].Visible = true;
				this.grdAd.Columns[this.colAffiliateSalesIndex].Visible = true;
				this.grdAd.Columns[this.colTotalAmtIndex].Visible = true;
			} else {
				this.Title = this.Title + DisplayWordUtil.Replace("（出演者）");
				lblPgmTitle.Text = lblPgmTitle.Text + DisplayWordUtil.Replace("（出演者）");
				this.grdAd.Columns[this.colOrdinalSalesIndex].Visible = false;
				this.grdAd.Columns[this.colAffiliateSalesIndex].Visible = false;
				this.grdAd.Columns[this.colTotalAmtIndex].Visible = false;
				this.grdAd.Columns[this.colRegistMailFaildCountIndex].Visible = false;
			}

			this.grdAd.Columns[this.colLoginCountIndex].HeaderText = "ﾛｸﾞｲﾝ数";
			this.grdAd.Columns[this.colLoginSuccessCountRawIndex].HeaderText = "ﾛｸﾞｲﾝ数";
			this.grdAd.Columns[this.colLoginSuccessCountIndex].HeaderText = "ﾛｸﾞｲﾝ数";
			this.grdAd.Columns[this.colLoginSuccessCountUniqueRawIndex].HeaderText = "ﾛｸﾞｲﾝ数(ﾕﾆｰｸ)";
			this.grdAd.Columns[this.colLoginSuccessCountUniqueIndex].HeaderText = "ﾛｸﾞｲﾝ数(ﾕﾆｰｸ)";
			this.grdAd.Columns[this.colTotalUsedPointIndex].HeaderText = this.IsManMode ? "利用Pt." : "獲得報酬";

			if (IsAdManager) {
				this.grdAd.Columns[this.colTotalUsedPointIndex].Visible = false;
				this.grdAd.Columns[this.colOrdinalSalesIndex].Visible = false;
				this.grdAd.Columns[this.colAffiliateSalesIndex].Visible = false;
				this.grdAd.Columns[this.colTotalAmtIndex].Visible = false;

				this.grdAd.Columns[this.colRegistCountRawIndex].Visible = false;
				this.grdAd.Columns[this.colWithdrawalIndex].Visible = false;
				this.grdAd.Columns[this.colReRegistIndex].Visible = false;
				this.grdAd.Columns[this.colLoginSuccessCountRawIndex].Visible = false;
				this.grdAd.Columns[this.colLoginSuccessCountUniqueRawIndex].Visible = false;

				plcAdCd.Visible = false;

			} else {
				this.grdAd.Columns[this.colRegistCountIndex].HeaderText = "認証数";
				this.grdAd.Columns[this.colLoginSuccessCountIndex].HeaderText += "[認証数]";
				this.grdAd.Columns[this.colLoginSuccessCountUniqueIndex].HeaderText += "[認証数]";
			}
			if (IsCanViewSales && IsManMode) {
				this.grdAd.Columns[this.colTotalAmtIndex].Visible = true;
			}
		}
	}

	private void InitPage() {
		grdAd.Sort(string.Empty,SortDirection.Ascending);
		pnlInfo.Visible = false;
		ClearField();
		lstSiteCd.DataBind();
		lstAdGroup.DataBind();
		if (!IsPostBack) {
			SysPrograms.SetupFromToDay(lstFromYYYY,lstFromMM,lstFromDD,lstToYYYY,lstToMM,lstToDD,true);

			this.lstFromMM.Items.FindByText("---").Value = this.lstFromMM.Items[1].Value;
			this.lstFromDD.Items.FindByText("---").Value = this.lstFromDD.Items[1].Value;
			this.lstToMM.Items.FindByText("---").Value = this.lstToMM.Items[this.lstToMM.Items.Count - 1].Value;

			lstFromYYYY.SelectedIndex = 0;
			lstToYYYY.SelectedIndex = 0;
			lstFromMM.SelectedValue = DateTime.Now.ToString("MM");
			lstToMM.SelectedValue = DateTime.Now.ToString("MM");
			lstFromDD.SelectedValue = DateTime.Now.ToString("dd");
			lstToDD.SelectedValue = DateTime.Now.ToString("dd");
			txtAdCd.Text = iBridUtil.GetStringValue(Session["AD_CD"]);
			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["AD_GROUP_CD"]))) {
				lstAdGroup.SelectedValue = iBridUtil.GetStringValue(Session["AD_GROUP_CD"]);
				lstAdGroup.Enabled = false;
			}
			if (txtAdCd.Text.Equals(string.Empty) == false || iBridUtil.GetStringValue(Session["AD_GROUP_CD"]).Equals(string.Empty) == false) {
				plcAdCd.Visible = false;
			}
			//this.plcDisplayAdColumn.Visible = !this.IsAdManager;
			this.plcDisplayAdColumn.Visible = false;
			this.rdoSummaryType.SelectedIndex = 0;
		}
	}

	private void ClearField() {
		btnCsv.Visible = false;
		accessCount = 0;

		registMailFaildCount = 0;
		loginCount = 0;
		loginSuccessCount = 0;
		loginSuccessCountRaw = 0;
		loginSuccessUniqueCount = 0;
		loginSuccessUniqueCountRaw = 0;
		usedPoint = 0;
		registCount = 0;
		registCountRaw = 0;
		reRegistCnt = 0;
		withdrawalCnt = 0;
		ordinarySalesAmt = 0;
		affiliateSalesAmt = 0;
		totalAmt = 0;
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			GetList();
		}
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		string fileName = ViCommConst.CSV_FILE_NM_MANAGER_PERFORMANCE_LOG;

		Response.AddHeader("Content-Disposition","attachment;filename=" + fileName);
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		using (AccessPage pAccess = new AccessPage()) {
			DataSet ds = pAccess.GetAdPerformanceList(
								lstSiteCd.SelectedValue,
								lstFromYYYY.SelectedValue,
								lstFromMM.SelectedValue,
								lstFromDD.SelectedValue,
								lstToYYYY.SelectedValue,
								lstToMM.SelectedValue,
								lstToDD.SelectedValue,
								lstAdGroup.SelectedValue,
				//iBridUtil.GetStringValue(Session["AD_GROUP_CD"]),
								txtAdCd.Text,
								ViewState["SEX_CD"].ToString(),
								iBridUtil.GetStringValue(Session["AdminType"]),
								rdoSummaryType.SelectedValue,
								rdoUsedFlag.SelectedValue
								);


			string sCsvRec = GetCsvString(ds);

			Response.BinaryWrite(encoding.GetBytes(sCsvRec));
			Response.End();
		}
	}

	protected void lstSiteCd_SelectedIndexChanged(object sender,EventArgs e) {
		lstAdGroup.Items.Clear();
		lstAdGroup.DataBind();
	}

	protected void lstAdGroup_DataBound(object sender,EventArgs e) {
		lstAdGroup.Items.Insert(0,new ListItem(string.Empty,string.Empty));
	}

	protected string GetBusTime() {
		string sVal = iBridUtil.GetStringValue(ViewState["BUSSISS_TIME"]);
		if (sVal.Length >= 2) {
			return sVal.Substring(0,2);
		} else {
			return "00";
		}
	}

	protected string GetFormatDate(int pFromTo) {
		if (pFromTo == 0) {
			return SysPrograms.IsDateToLastDate(lstFromYYYY.SelectedValue + "/" + lstFromMM.SelectedValue + "/" + lstFromDD.SelectedValue);
		} else {
			string sCnvD = SysPrograms.IsDateToLastDate(lstToYYYY.SelectedValue + "/" + lstToMM.SelectedValue + "/" + lstToDD.SelectedValue) + " " + iBridUtil.GetStringValue(ViewState["BUSSISS_TIME"]);
			return DateTime.ParseExact(sCnvD,"yyyy/MM/dd HH:mm:ss",null).AddHours(23).ToString("yyyy/MM/dd");
		}
	}

	protected string GetTime(string pDate) {
		string sCnvD = SysPrograms.IsDateToLastDate(pDate.ToString()) + " " + iBridUtil.GetStringValue(ViewState["BUSSISS_TIME"]);
		return DateTime.ParseExact(sCnvD,"yyyy/MM/dd HH:mm:ss",null).AddHours(23).ToString("HH");
	}

	protected void grdAd_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			accessCount += GetValueWithConvertInt32(DataBinder.Eval(e.Row.DataItem,"ACCESS_COUNT_AD"));


			loginCount += GetValueWithConvertInt32(
				DataBinder.Eval(e.Row.DataItem,"LOGIN_COUNT"),
				DataBinder.Eval(e.Row.DataItem,"LOGIN_COUNT_AD"));

			registCount += GetValueWithConvertInt32(DataBinder.Eval(e.Row.DataItem,"REGIST_COUNT_AD"));
			registCountRaw += GetValueWithConvertInt32(DataBinder.Eval(e.Row.DataItem,"REGIST_COUNT"));
			withdrawalCnt += GetValueWithConvertInt32(DataBinder.Eval(e.Row.DataItem,"WITHDRAWAL_COUNT"));
			reRegistCnt += GetValueWithConvertInt32(DataBinder.Eval(e.Row.DataItem,"RE_REGIST_COUNT"));

			registMailFaildCount += GetValueWithConvertInt32(
				DataBinder.Eval(e.Row.DataItem,"REGIST_MAIL_SEND_FAILED_COUNT"),
				DataBinder.Eval(e.Row.DataItem,"REGIST_MAIL_SEND_FAILED_COUNT"));

			loginSuccessCount += GetValueWithConvertInt32(DataBinder.Eval(e.Row.DataItem,"LOGIN_SUCCESS_COUNT_AD"));
			loginSuccessCountRaw += GetValueWithConvertInt32(DataBinder.Eval(e.Row.DataItem,"LOGIN_SUCCESS_COUNT"));
			loginSuccessUniqueCount += GetValueWithConvertInt32(DataBinder.Eval(e.Row.DataItem,"LOGIN_SUCCESS_COUNT_UNIQUE_AD"));
			loginSuccessUniqueCountRaw += GetValueWithConvertInt32(DataBinder.Eval(e.Row.DataItem,"LOGIN_SUCCESS_COUNT_UNIQUE"));

			usedPoint += GetValueWithConvertInt32(
				DataBinder.Eval(e.Row.DataItem,"TOTAL_USED_POINT"),
				DataBinder.Eval(e.Row.DataItem,"TOTAL_USED_POINT_AD"));

			ordinarySalesAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"ORDINARY_SALES_AMT"));
			affiliateSalesAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"POINT_BACK_SALES_AMT"));
			totalAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TOTAL_SALES_AMT"));

			HyperLink lnkRegistCount = (HyperLink)e.Row.FindControl("lnkRegistCount");
			Label lblRegistCount = (Label)e.Row.FindControl("lblRegistCount");
			if (!IsAdManager) {
				if (this.rdoSummaryType.SelectedValue.Equals("2")) {
					// 集計 広告ｸﾞﾙｰﾌﾟ別
					lnkRegistCount.Visible = false;
					lblRegistCount.Visible = true;
				} else {
					lnkRegistCount.Visible = true;
					lblRegistCount.Visible = false;
				}
				if (this.IsManMode) {
					lnkRegistCount.NavigateUrl = string.Format("~/Man/ManInquiry.aspx?sitecd={0}&registdayfrom={1}&registtimefrom={2}&registdayto={3}&registtimeto={4}&adcd={5}&adnm={6}&return={7}",DataBinder.Eval(e.Row.DataItem,"SITE_CD"),GetFormatDate(0),GetBusTime(),GetFormatDate(1),GetTime(GetFormatDate(1)),DataBinder.Eval(e.Row.DataItem,"AD_CD"),HttpUtility.UrlEncodeUnicode(iBridUtil.GetStringValue(DataBinder.Eval(e.Row.DataItem,"AD_NM"))),"AdPerformance.aspx");
				} else {
					lnkRegistCount.NavigateUrl = string.Format("~/Cast/CastCharacterInquiry.aspx?sitecd={0}&adcd={1}&adnm={2}&registdayfrom={3}/{4}/{5}&registdayto={6}/{7}/{8}",DataBinder.Eval(e.Row.DataItem,"SITE_CD"),DataBinder.Eval(e.Row.DataItem,"AD_CD"),HttpUtility.UrlEncodeUnicode(iBridUtil.GetStringValue(DataBinder.Eval(e.Row.DataItem,"AD_NM"))),lstFromYYYY.SelectedValue,lstFromMM.SelectedValue,lstFromDD.SelectedValue,lstToYYYY.SelectedValue,lstToMM.SelectedValue,this.DaysInMonthTo);
				}
			}


		} else if (e.Row.RowType == DataControlRowType.Footer) {
			e.Row.Cells[this.colAccessCountIndex].Text = accessCount.ToString();
			e.Row.Cells[this.colRegistCountIndex].Text = registCount.ToString();
			e.Row.Cells[this.colRegistCountRawIndex].Text = registCountRaw.ToString();
			e.Row.Cells[this.colWithdrawalIndex].Text = withdrawalCnt.ToString();
			e.Row.Cells[this.colReRegistIndex].Text = reRegistCnt.ToString();
			e.Row.Cells[this.colRegistMailFaildCountIndex].Text = registMailFaildCount.ToString();
			e.Row.Cells[this.colLoginCountIndex].Text = loginCount.ToString();
			e.Row.Cells[this.colLoginSuccessCountIndex].Text = loginSuccessCount.ToString();
			e.Row.Cells[this.colLoginSuccessCountRawIndex].Text = loginSuccessCountRaw.ToString();
			e.Row.Cells[this.colLoginSuccessCountUniqueIndex].Text = loginSuccessUniqueCount.ToString();
			e.Row.Cells[this.colLoginSuccessCountUniqueRawIndex].Text = loginSuccessUniqueCountRaw.ToString();
			e.Row.Cells[this.colTotalUsedPointIndex].Text = usedPoint.ToString();
			e.Row.Cells[this.colOrdinalSalesIndex].Text = ordinarySalesAmt.ToString();
			e.Row.Cells[this.colAffiliateSalesIndex].Text = affiliateSalesAmt.ToString();
			e.Row.Cells[this.colTotalAmtIndex].Text = totalAmt.ToString();
		}

		if (Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_PRODUCTION)) {
			e.Row.Cells[this.colTotalUsedPointIndex].Visible = false;
		}
	}

	private void GetList() {
		if (IsAdManager) {
			this.grdAd.Columns[this.colRegistCountIndex].Visible = true;
			this.grdAd.Columns[this.colLoginSuccessCountIndex].Visible = true;
			this.grdAd.Columns[this.colLoginSuccessCountUniqueIndex].Visible = true;
		} else {
			this.grdAd.Columns[this.colRegistCountIndex].Visible = true;
			this.grdAd.Columns[this.colLoginSuccessCountIndex].Visible = false;
			this.grdAd.Columns[this.colLoginSuccessCountUniqueIndex].Visible = false;
		}

		if (this.rdoSummaryType.SelectedValue.Equals("1")) {
			this.grdAd.Columns[this.colAdCdIndex].Visible = true;
			this.grdAd.Columns[this.colAdNmIndex].Visible = true;
			this.grdAd.Columns[this.colAdGroupCdIndex].Visible = false;
			this.grdAd.Columns[this.colAdGroupNmIndex].Visible = false;
		} else {
			this.grdAd.Columns[this.colAdCdIndex].Visible = false;
			this.grdAd.Columns[this.colAdNmIndex].Visible = false;
			this.grdAd.Columns[this.colAdGroupCdIndex].Visible = true;
			this.grdAd.Columns[this.colAdGroupNmIndex].Visible = true;
		}

		ClearField();
		SetNextBusinessTime();
		grdAd.DataSourceID = "dsAccessPage";
		grdAd.DataBind();

		btnCsv.Visible = (Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_SITE_OWNER) || Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_AD_MANAGE));
		pnlInfo.Visible = true;


	}


	private string GetCsvString(DataSet pDataSet) {

		GridViewCsvBuilder oCsvBuilder = new GridViewCsvBuilder(this.grdAd,pDataSet.Tables[0]);

		oCsvBuilder.AppendMapping(this.colAdCdIndex,"AD_CD");
		oCsvBuilder.AppendMapping(this.colAdNmIndex,"AD_NM");
		oCsvBuilder.AppendMapping(this.colAccessCountIndex,"ACCESS_COUNT_AD");
		// 広告管理（IsAdManager）関係なく出力する
		oCsvBuilder.AppendMapping(this.colRegistCountRawIndex,"REGIST_COUNT");

		// 認証数
		oCsvBuilder.AppendMapping(this.colRegistCountIndex,"REGIST_COUNT_AD");
		if (this.IsAdManager) {
			// 退会数
			oCsvBuilder.AppendMapping(this.colRegistCountRawIndex,"WITHDRAWAL_COUNT");
		} else {
			oCsvBuilder.AppendMapping(this.colWithdrawalIndex,"WITHDRAWAL_COUNT");
		}
		if (this.IsAdManager) {
			// 再登録数
			oCsvBuilder.AppendMapping(this.colRegistCountRawIndex,"RE_REGIST_COUNT");
		} else {
			oCsvBuilder.AppendMapping(this.colReRegistIndex,"RE_REGIST_COUNT");
		}
		// ログイン数(出力なし）
		oCsvBuilder.AppendMapping(this.colLoginCountIndex,this.IsAdManager ? "LOGIN_COUNT_AD" : "LOGIN_COUNT");
		// ログイン成功数
		oCsvBuilder.AppendMapping(this.colLoginSuccessCountRawIndex,"LOGIN_SUCCESS_COUNT");
		
		oCsvBuilder.AppendMapping(this.colLoginSuccessCountIndex,"LOGIN_SUCCESS_COUNT_AD");
		// ログイン数ユニーク
		oCsvBuilder.AppendMapping(this.colLoginSuccessCountUniqueRawIndex,"LOGIN_SUCCESS_COUNT_UNIQUE");
		oCsvBuilder.AppendMapping(this.colLoginSuccessCountUniqueIndex,"LOGIN_SUCCESS_COUNT_UNIQUE_AD");
		// 利用ポイント
		oCsvBuilder.AppendMapping(this.colTotalUsedPointIndex,this.IsAdManager ? "TOTAL_USED_POINT_AD" : "TOTAL_USED_POINT");

		if (IsManMode) {
			oCsvBuilder.AppendMapping(this.colOrdinalSalesIndex,"ORDINARY_SALES_AMT");
			oCsvBuilder.AppendMapping(this.colAffiliateSalesIndex,"POINT_BACK_SALES_AMT");
			oCsvBuilder.AppendMapping(this.colTotalAmtIndex,"TOTAL_SALES_AMT");
		}
		return oCsvBuilder.ToString();
	}

	protected void SetNextBusinessTime() {
		string sBusinessTime;
		using (Site oSite = new Site()) {
			oSite.GetOne(lstSiteCd.SelectedValue);
			try {
				DateTime.ParseExact(oSite.nextbusinesstime,"HH:mm:ss",null);
				sBusinessTime = oSite.nextbusinesstime;
			} catch {
				sBusinessTime = "00:00:00";
			}
		}
		if (Session["AvailableMaqiaAffiliate"].ToString() == "1") {
			sBusinessTime = "00:00:00";
		}
		ViewState["BUSSISS_TIME"] = sBusinessTime;
	}

	/// <summary>
	/// ﾛｸﾞｲﾝ中の管理者のロールによって利用する値を選定し返す。


	/// </summary>
	/// <param name="pValue">広告管理者以外の場合に利用する値</param>
	/// <param name="pAdValue">広告管理者の場合に利用する値</param>
	/// <returns>選定後の値</returns>
	protected string GetValue(object pValue,object pAdValue) {
		object oValue = (IsAdManager ? pAdValue : pValue) ?? string.Empty;
		return oValue.ToString();
	}


	/// <summary>
	/// ﾛｸﾞｲﾝ中の管理者のロールによって利用する値を選定し、Int32に変換して返す。


	/// </summary>
	/// <param name="pValue">広告管理者以外の場合に利用する値</param>
	/// <param name="pAdValue">広告管理者の場合に利用する値</param>
	/// <returns>選定し、Int32に変換した値</returns>
	protected int GetValueWithConvertInt32(object pValue,object pAdValue) {
		string sValue = GetValue(pValue,pAdValue);
		int iResult = 0;
		return int.TryParse(sValue,out iResult) ? iResult : 0;
	}

	protected int GetValueWithConvertInt32(object pValue) {
		int iResult = 0;
		return int.TryParse(pValue.ToString(),out iResult) ? iResult : 0;
	}

	protected void vdcFrom_ServerValidate(object source,ServerValidateEventArgs args) {
		if (this.IsValid) {
			DateTime dtFrom;
			if (!DateTime.TryParse(string.Format("{0}/{1}/{2}",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue),out dtFrom)) {
				args.IsValid = false;
			}
		}
	}

	protected void vdcTo_ServerValidate(object source,ServerValidateEventArgs args) {
		if (this.IsValid) {
			DateTime dtTo;
			if (!DateTime.TryParse(string.Format("{0}/{1}/{2}",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,this.DaysInMonthTo),out dtTo)) {
				args.IsValid = false;
			}
		}
	}

	protected void vdcFromTo_ServerValidate(object source,ServerValidateEventArgs args) {
		if (this.IsValid) {
			DateTime dtFrom;
			DateTime dtTo;
			if (!DateTime.TryParse(string.Format("{0}/{1}/{2}",this.lstFromYYYY.SelectedValue,this.lstFromMM.SelectedValue,this.lstFromDD.SelectedValue),out dtFrom)) {
				dtFrom = DateTime.MinValue;
			}
			if (!DateTime.TryParse(string.Format("{0}/{1}/{2}",this.lstToYYYY.SelectedValue,this.lstToMM.SelectedValue,this.DaysInMonthTo),out dtTo)) {
				dtTo = DateTime.MinValue;
			}
			if (dtFrom > dtTo) {
				args.IsValid = false;
			}
		}
	}

	private string GetDaysInMonth(string pYear,string pMonth,string pDay) {
		if (!pDay.Equals("---")) {
			return pDay;
		}

		return DateTime.DaysInMonth(int.Parse(pYear),int.Parse(pMonth)).ToString();
	}

	protected void grdAd_Sorting(object sender,EventArgs e) {
	}
	protected void grdAd_Sorted(object sender,EventArgs e) {
	}
}
