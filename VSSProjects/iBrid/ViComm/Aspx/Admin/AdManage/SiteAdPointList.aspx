<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SiteAdPointList.aspx.cs" Inherits="AdManage_SiteAdPointList"
	Title="広告別サービスポイント" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="広告別サービスポイント"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2" style="height: 26px">
									サイトコード
								</td>
								<td class="tdDataStyle" style="height: 26px">
									<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
									</asp:DropDownList>
								</td>
								<td class="tdHeaderStyle2" style="height: 26px">
									広告コード
								</td>
								<td class="tdDataStyle" style="height: 26px">
									<asp:TextBox ID="txtAdCd" runat="server" MaxLength="32" Width="200px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrAdCd" runat="server" ErrorMessage="広告コードを入力して下さい。" ControlToValidate="txtAdCd" ValidationGroup="Key">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeAdCd" runat="server" ErrorMessage="広告コードは32桁以下の英数字で入力して下さい。" ValidationExpression="[a-zA-Z0-9-\/_]{1,32}" ControlToValidate="txtAdCd"
										ValidationGroup="Key">*</asp:RegularExpressionValidator>
									<asp:CustomValidator ID="vdcAdCd" runat="server" ControlToValidate="txtAdCd" ErrorMessage="広告コードが未登録です。" OnServerValidate="vdcAdCd_ServerValidate" ValidationGroup="Key">*</asp:CustomValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[広告別サービスポイント]</legend>
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									広告名
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblAdNm" runat="server" Text=""></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									サービスポイント
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtRegistServicePoint" runat="server" MaxLength="4" Width="30px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrRegistServicePoint" runat="server" ErrorMessage="サービスポイントを入力して下さい。" ControlToValidate="txtRegistServicePoint" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeRegistServicePoint" runat="server" ErrorMessage="サービスポイントは1〜4桁の数字で入力して下さい。" ValidationExpression="-?\d{1,4}" ControlToValidate="txtRegistServicePoint"
										ValidationGroup="Detail" Display="Dynamic">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									適用開始日
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtApplyStartDay" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrApplyStartDay" runat="server" ErrorMessage="適用開始日を入力して下さい。" ControlToValidate="txtApplyStartDay" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RangeValidator ID="vddApplyStartDay" runat="server" ErrorMessage="適用開始日を正しく入力して下さい。" ControlToValidate="txtApplyStartDay" MaximumValue="2099/12/31"
										MinimumValue="1990/01/01" Type="Date" ValidationGroup="Detail" Display="Dynamic">*</asp:RangeValidator>
									<asp:RegularExpressionValidator ID="vdeApplyStartDay" runat="server" ErrorMessage="適用開始日を正しく入力して下さい。" ControlToValidate="txtApplyStartDay" ValidationExpression="\d{4}/\d{2}/\d{2}"
										ValidationGroup="Detail" Display="Dynamic">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									適用終了日
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtApplyEndDay" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrApplyEndDay" runat="server" ErrorMessage="適用終了日を入力して下さい。" ControlToValidate="txtApplyEndDay" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RangeValidator ID="vddApplyEndDay" runat="server" ErrorMessage="適用終了日を正しく入力して下さい。" ControlToValidate="txtApplyEndDay" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
										Type="Date" ValidationGroup="Detail" Display="Dynamic">*</asp:RangeValidator>
									<asp:RegularExpressionValidator ID="vdeApplyEndDay" runat="server" ErrorMessage="適用終了日を正しく入力して下さい。" ControlToValidate="txtApplyEndDay" ValidationExpression="\d{4}/\d{2}/\d{2}"
										ValidationGroup="Detail" Display="Dynamic">*</asp:RegularExpressionValidator>
									<asp:CompareValidator ID="vdcApplyEndDay" runat="server" ErrorMessage="適用終了日が適用開始日以前になっています。" ControlToCompare="txtApplyStartDay" ControlToValidate="txtApplyEndDay"
										ValidationGroup="Detail" Operator="GreaterThanEqual" Display="Dynamic">*</asp:CompareValidator></td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[広告コード一覧]</legend>
			<table border="0" style="width: 900px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle">
						サイトコード
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
						</asp:DropDownList>
					</td>
					<td class="tdHeaderStyle">
						適用日
					</td>
					<td class="tdDataStyle">
						<asp:TextBox ID="txtSeekDay" runat="server" MaxLength="10" Width="80px"></asp:TextBox>を含む
						<asp:RangeValidator ID="vddSeekDay" runat="server" ErrorMessage="適用日を正しく入力して下さい。" ControlToValidate="txtSeekDay" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
							Type="Date" ValidationGroup="Detail" Display="Dynamic">*</asp:RangeValidator>
						<asp:RegularExpressionValidator ID="vdeSeekDay" runat="server" ErrorMessage="適用日を正しく入力して下さい。" ControlToValidate="txtSeekDay" ValidationExpression="\d{4}/\d{2}/\d{2}"
							ValidationGroup="Detail" Display="Dynamic">*</asp:RegularExpressionValidator>
					</td>
				</tr>
				<tr>
					<td class="tdHeaderStyle">
						広告コード
					</td>
					<td class="tdDataStyle" >
						<asp:TextBox ID="txtSeekAdCd" runat="server" MaxLength="32" Width="200px"></asp:TextBox>
					</td>
					<td class="tdHeaderStyle">
						広告名称
					</td>
					<td class="tdDataStyle">
						<asp:TextBox ID="txtSeekAdNm" runat="server" Width="200px"></asp:TextBox>
					</td>

				</tr>		
				<tr>
					<td class="tdHeaderStyle">
						未設定
					</td>
					<td class="tdDataStyle" colspan="3">
						<asp:CheckBox ID="chkNoAssign" runat="server" Text="サービスポイント未設定の広告コード" />
					</td>
				</tr>
						
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnRegist" Text="サービスポイント設定" CssClass="seekbutton" CausesValidation="False" OnClick="btnRegist_Click" />
			<br />
			<br />
			<asp:GridView ID="grdAd" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsAd" AllowSorting="True" SkinID="GridViewColor">
				<Columns>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkAdCd" runat="server" Text='<%# string.Format("{0} {1}",Eval("AD_CD"),Eval("AD_NM")) %>' CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("AD_CD"))  %>'
								OnCommand="lnkAdCd_Command" CausesValidation="False">
							</asp:LinkButton>
						</ItemTemplate>
						<HeaderTemplate>
							広告コード
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:BoundField DataField="REGIST_SERVICE_POINT" HeaderText="サービスＰ">
						<ItemStyle HorizontalAlign="Right" />
					</asp:BoundField>
					<asp:BoundField DataField="APPLY_START_DAY" HeaderText="適用開始日">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
					<asp:BoundField DataField="APPLY_END_DAY" HeaderText="適用終了日">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
					<asp:BoundField DataField="UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br>
				<a class="reccount">Current viewing page
					<%=grdAd.PageIndex + 1%>
					of
					<%=grdAd.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsAd" runat="server" SelectMethod="GetPageCollectionByServicePoint" TypeName="Ad" SelectCountMethod="GetPageCountByServicePoint"
		EnablePaging="True" OnSelected="dsAd_Selected" OnSelecting="dsAd_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pAdCd" Type="String" />
			<asp:Parameter Name="pAdNm" Type="String" />
			<asp:Parameter Name="pApplyDay" Type="String" />
			<asp:Parameter Name="pNoAssign" Type="Boolean" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrAdCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeAdCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdcAdCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdrRegistServicePoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdeRegistServicePoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdrApplyStartDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vddApplyStartDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8" TargetControlID="vdeApplyStartDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11" TargetControlID="vdrApplyEndDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender12" TargetControlID="vddApplyEndDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10" TargetControlID="vdeApplyEndDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9" TargetControlID="vdcApplyEndDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender13" TargetControlID="vddSeekDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender14" TargetControlID="vdeSeekDay" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
