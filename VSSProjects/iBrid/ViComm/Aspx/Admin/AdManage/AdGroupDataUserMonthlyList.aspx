﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AdGroupDataUserMonthlyList.aspx.cs" Inherits="AdManage_AdGroupDataUserMonthlyList" Title="代理店別集計(会員)" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="代理店別集計(会員)"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[検索条件]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 800px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                表示月
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstYear" runat="server" Width="60px">
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstMonth" runat="server" Width="40px">
                                </asp:DropDownList>月
                                <br />
								<asp:Label ID="lblErrorMessageReportDayFrom" runat="server" ForeColor="red" Visible="false"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" />
            </fieldset>
        </asp:Panel>
		<asp:Panel runat="server" ID="pnlList">
			<fieldset>
				<legend>[集計]</legend>
				<asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdAdDataUserMonthly.PageIndex + 1%>
                        of
                        <%= grdAdDataUserMonthly.PageCount%>
                    </a>
                </asp:Panel>
                &nbsp;<br />
				<asp:Panel ID="pnlGrid" runat="server">
					<asp:GridView ID="grdAdDataUserMonthly" DataSourceID="dsAdDataUserMonthly" runat="server" AllowPaging="true" AutoGenerateColumns="False" PageSize="9999" OnSorting="grdAdDataUserMonthly_Sorting"
						EnableSortingAndPagingCallbacks="false" ShowFooter="true" SkinID="GridViewColor" AllowSorting="true" OnRowDataBound="grdAdDataUserMonthly_RowDataBound">
						<Columns>
							<asp:TemplateField HeaderText="代理店名">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:HyperLink ID="lnkAdGroupNm" runat="server" NavigateUrl='<%# string.Format("~/AdManage/AdDataUserMonthlyList.aspx?sitecd={0}&adgroupcd={1}&year={2}&month={3}",Eval("SITE_CD"),Eval("AD_GROUP_CD"),lstYear.SelectedValue,lstMonth.SelectedValue) %>' Text='<%# Eval("AD_GROUP_NM") %>' ></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle Wrap="false" />
								<FooterTemplate>
									<asp:Label ID="lblAdGroupNmFooter1" Text="グループ無し" runat="server"></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblAdGroupNmFooter2" Text="合計" runat="server"></asp:Label>
								</FooterTemplate>
								<FooterStyle HorizontalAlign="center" Wrap="false" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="広告費" SortExpression="AD_COST">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblAdCost" runat="server" Text='<%# Eval("AD_COST") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblAdCostFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label2" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblAdCostFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ｱｸｾｽ数" SortExpression="ACCESS_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblAccessCount" runat="server" Text='<%# Eval("ACCESS_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblAccessCountFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label3" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblAccessCountFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="登録数" SortExpression="REGIST_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblRegistCount" runat="server" Text='<%# Eval("REGIST_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblRegistCountFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label4" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblRegistCountFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="登録率" SortExpression="REGIST_RATE">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblRegistRate" runat="server" Text='<%# AddPercentMark(Eval("REGIST_RATE").ToString()) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblRegistRateFooter1" runat="server"></asp:Label>%
									<asp:Label ID="Label5" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblRegistRateFooter2" runat="server"></asp:Label>%
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="認証数" SortExpression="REGIST_COUNT_AD">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblRegistCountAd" runat="server" Text='<%# Eval("REGIST_COUNT_AD") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblRegistCountAdFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label6" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblRegistCountAdFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="獲得単価" SortExpression="AD_COST_BY_REGIST">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblAdCostByRegist" runat="server" Text='<%# Eval("AD_COST_BY_REGIST") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblAdCostByRegistFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label7" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblAdCostByRegistFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾛｸﾞｲﾝ数" SortExpression="LOGIN_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblLoginCount" runat="server" Text='<%# Eval("LOGIN_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblLoginCountFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label8" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblLoginCountFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾎﾟｲﾝﾄ消費<br>人数" SortExpression="CHARGE_POINT_USER_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblChargePointUserCount" runat="server" Text='<%# Eval("CHARGE_POINT_USER_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblChargePointUserCountFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label9" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblChargePointUserCountFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾎﾟｲﾝﾄ消費<br>消費率" SortExpression="CHARGE_POINT_USER_RATE">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblChargePointUserRate" runat="server" Text='<%# AddPercentMark(Eval("CHARGE_POINT_USER_RATE").ToString()) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblChargePointUserRateFooter1" runat="server"></asp:Label>%
									<asp:Label ID="Label10" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblChargePointUserRateFooter2" runat="server"></asp:Label>%
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="初顧客<br>人数" SortExpression="START_RECEIPT_USER_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblStartReceiptUserCount" runat="server" Text='<%# Eval("START_RECEIPT_USER_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblStartReceiptUserCountFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label11" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblStartReceiptUserCountFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="初顧客<br>課金率" SortExpression="START_RECEIPT_USER_RATE">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblStartReceiptUserRate" runat="server" Text='<%# AddPercentMark(Eval("START_RECEIPT_USER_RATE").ToString()) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblStartReceiptUserRateFooter1" runat="server"></asp:Label>%
									<asp:Label ID="Label12" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblStartReceiptUserRateFooter2" runat="server"></asp:Label>%
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="顧客単価" SortExpression="AD_COST_BY_START_RECEIPT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblAdCostByStartReceipt" runat="server" Text='<%# Eval("AD_COST_BY_START_RECEIPT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblAdCostByStartReceiptFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label13" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblAdCostByStartReceiptFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="当月課金<br>人数" SortExpression="ENT_MONTH_RECEIPT_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblEntMonthReceiptCount" runat="server" Text='<%# Eval("ENT_MONTH_RECEIPT_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblEntMonthReceiptCountFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label14" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblEntMonthReceiptCountFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="当月課金<br>金額" SortExpression="ENT_MONTH_RECEIPT_AMT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblEntMonthReceiptAmt" runat="server" Text='<%# Eval("ENT_MONTH_RECEIPT_AMT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblEntMonthReceiptAmtFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblEntMonthReceiptAmtFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="当月回収率" SortExpression="ENT_MONTH_RETURN_RATE">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblEntMonthReturnRate" runat="server" Text='<%# AddPercentMark(Eval("ENT_MONTH_RETURN_RATE").ToString()) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblEntMonthReturnRateFooter1" runat="server"></asp:Label>%
									<asp:Label ID="Label16" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblEntMonthReturnRateFooter2" runat="server"></asp:Label>%
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾘﾋﾟｰﾄ課金<br>人数" SortExpression="REPEAT_RECEIPT_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblRepeatReceiptCount" runat="server" Text='<%# Eval("REPEAT_RECEIPT_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblRepeatReceiptCountFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label17" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblRepeatReceiptCountFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾘﾋﾟｰﾄ課金<br>金額" SortExpression="REPEAT_RECEIPT_AMT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblRepeatReceiptAmt" runat="server" Text='<%# Eval("REPEAT_RECEIPT_AMT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblRepeatReceiptAmtFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label18" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblRepeatReceiptAmtFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="合計課金<br>人数" SortExpression="TOTAL_RECEIPT_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblTotalReceiptCount" runat="server" Text='<%# Eval("TOTAL_RECEIPT_COUNT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblTotalReceiptCountFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label19" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblTotalReceiptCountFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="合計課金<br>金額" SortExpression="TOTAL_RECEIPT_AMT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblTotalReceiptAmt" runat="server" Text='<%# Eval("TOTAL_RECEIPT_AMT") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblTotalReceiptAmtFooter1" runat="server"></asp:Label>
									<asp:Label ID="Label20" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblTotalReceiptAmtFooter2" runat="server"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾘﾋﾟｰﾄ率" SortExpression="REPEAT_RATE">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblRepeatRate" runat="server" Text='<%# AddPercentMark(Eval("REPEAT_RATE").ToString()) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblRepeatRateFooter1" runat="server"></asp:Label>%
									<asp:Label ID="Label21" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblRepeatRateFooter2" runat="server"></asp:Label>%
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="累計広告比率" SortExpression="AGGR_AD_COST_RATE">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:Label ID="lblAggrAdCostRate" runat="server" Text='<%# AddPercentMark(Eval("AGGR_AD_COST_RATE").ToString()) %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" />
								<FooterTemplate>
									<asp:Label ID="lblAggrAdRateFooter1" runat="server"></asp:Label>%
									<asp:Label ID="Label22" runat="server" Text='<br />'></asp:Label>
									<asp:Label ID="lblAggrAdRateFooter2" runat="server"></asp:Label>%
								</FooterTemplate>
							</asp:TemplateField>
						</Columns>
					</asp:GridView>
				</asp:Panel>
			</fieldset>
			<br />
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsAdDataUserMonthly" runat="server" ConvertNullToDBNull="false" EnablePaging="true" OnSelecting="dsAdDataUserMonthly_Selecting" OnSelected="dsAdDataUserMonthly_Selected"
		SelectMethod="GetPageCollectionByAdGroup" SelectCountMethod="GetPageCountByAdGroup" SortParameterName="" TypeName="AdDataUserMonthly">
		<SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
</asp:Content>

