﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;


public partial class AdManage_DefaultAdManage:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		DataBind();
		if (Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_PRODUCTION)) {
			Response.Redirect("~/AdManage/AdPerformance.aspx?sexcd="+ViCommConst.MAN);

		} else if (Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_STAFF)) {
			Response.Redirect("~/AdManage/AdPerformance.aspx?sexcd=" + ViCommConst.MAN);

		} else if (Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_AD_MANAGE)) {
			Response.Redirect("~/AdManage/AdPerformance.aspx?sexcd=" + ViCommConst.MAN);

		} else {
			Response.Redirect("~/AdManage/AdGroupList.aspx");
		}
	}
}
