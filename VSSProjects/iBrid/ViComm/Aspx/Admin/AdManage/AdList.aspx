<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AdList.aspx.cs" Inherits="AdManage_AdList" Title="広告コード設定"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="広告コード設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									広告コード
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtAdCd" runat="server" MaxLength="32" Width="200px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrAdCd" runat="server" ErrorMessage="広告コードを入力して下さい。" ControlToValidate="txtAdCd" ValidationGroup="Key">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeAdCd" runat="server" ErrorMessage="広告コードは32桁以下の英数字で入力して下さい。" ValidationExpression="[a-zA-Z0-9-\/_]{1,32}" ControlToValidate="txtAdCd"
										ValidationGroup="Key">*</asp:RegularExpressionValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[広告コード内容]</legend>
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									広告名称
								</td>
								<td class="tdDataStyle" colspan="3">
									<asp:TextBox ID="txtAdNm" runat="server" MaxLength="60" Width="250px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrAdNm" runat="server" ErrorMessage="広告名称を入力して下さい。" ControlToValidate="txtAdNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									掲載メニュー
								</td>
								<td class="tdDataStyle" colspan="3">
									<asp:DropDownList ID="lstAdMenu" runat="server" DataSourceID="dsCodeDtl"
										OnSelectedIndexChanged="lstAdMenu_IndexChanged"
										DataTextField="CODE_NM" DataValueField="CODE"
										Width="170px" AutoPostBack="True">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									セグメント
								</td>
								<td class="tdDataStyle" colspan="3">
	                                <asp:DropDownList ID="lstSexCd" runat="server">
						                <asp:ListItem Value="-" Text="未選択"></asp:ListItem>
						                <asp:ListItem Value="1" Text="男性"></asp:ListItem>
				                        <asp:ListItem Value="3" Text="女性"></asp:ListItem>
					                </asp:DropDownList>
	                            </td>
							</tr>
							<asp:Panel runat="server" ID="pnlAdTerm" Visible="false">
								<tr>
									<td class="tdHeaderStyle">
										期間
									</td>
									<td class="tdDataStyle" colspan="3">
										<asp:DropDownList ID="lstAdStartDateY" runat="server" DataSource='<%# YearArray %>'>
										</asp:DropDownList>年
										<asp:DropDownList ID="lstAdStartDateM" runat="server" DataSource='<%# MonthArray %>'>
										</asp:DropDownList>月
										<asp:DropDownList ID="lstAdStartDateD" runat="server" DataSource='<%# DayArray %>'>
										</asp:DropDownList>日
										〜
										<asp:DropDownList ID="lstAdEndDateY" runat="server" DataSource='<%# YearArray %>'>
										</asp:DropDownList>年
										<asp:DropDownList ID="lstAdEndDateM" runat="server" DataSource='<%# MonthArray %>'>
										</asp:DropDownList>月
										<asp:DropDownList ID="lstAdEndDateD" runat="server" DataSource='<%# DayArray %>'>
										</asp:DropDownList>日
										<br />
										<asp:Label ID="lblErrorMessageAdTerm" runat="server" ForeColor="red" Visible="false"></asp:Label>
									</td>
								</tr>
							</asp:panel>
							<tr>
								<td class="tdHeaderStyle">
									掲載開始日
								</td>
								<td class="tdDataStyle" colspan="3">
									<asp:DropDownList ID="lstPublishStartDateY" runat="server" DataSource='<%# YearArray %>'>
									</asp:DropDownList>年
									<asp:DropDownList ID="lstPublishStartDateM" runat="server" DataSource='<%# MonthArray %>'>
									</asp:DropDownList>月
									<asp:DropDownList ID="lstPublishStartDateD" runat="server" DataSource='<%# DayArray %>'>
									</asp:DropDownList>日
									<br />
									<asp:Label ID="lblErrorMessagePublishStartDate" runat="server" ForeColor="red" Visible="false"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									広告停止日
								</td>
								<td class="tdDataStyle" colspan="3">
									<asp:DropDownList ID="lstPublishEndDateY" runat="server" DataSource='<%# YearArray %>'>
									</asp:DropDownList>年
									<asp:DropDownList ID="lstPublishEndDateM" runat="server" DataSource='<%# MonthArray %>'>
									</asp:DropDownList>月
									<asp:DropDownList ID="lstPublishEndDateD" runat="server" DataSource='<%# DayArray %>'>
									</asp:DropDownList>日
									<br />
									<asp:Label ID="lblErrorMessagePublishEndDate" runat="server" ForeColor="red" Visible="false"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									広告費
								</td>
								<td class="tdDataStyle" colspan="3">
									<asp:TextBox ID="txtAdCost" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrAdCost" runat="server" ErrorMessage="広告費を入力して下さい。" ControlToValidate="txtAdCost" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("男性会員獲得単価")%>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtManConversionFee" runat="server" MaxLength="5" Width="60px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrManConversionFee" runat="server" ErrorMessage="男性会員獲得単価を入力して下さい。" ControlToValidate="txtManConversionFee" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
								<td class="tdHeaderStyle">
									<%= DisplayWordUtil.Replace("男性会員")%>
									成果発生ﾀｲﾐﾝｸﾞ
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstManRegistReportTiming" runat="server" DataSourceID="dsManRegistReportTiming" DataTextField="CODE_NM" DataValueField="CODE">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									女性会員獲得単価
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtWomanConversionFee" runat="server" MaxLength="5" Width="60px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrWomanConversionFee" runat="server" ErrorMessage="女性会員獲得単価を入力して下さい。" ControlToValidate="txtWomanConversionFee" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
								</td>
								<td class="tdHeaderStyle2">
									<%= DisplayWordUtil.Replace("女性会員")%>
									成果発生ﾀｲﾐﾝｸﾞ
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstCastRegistReportTiming" runat="server" DataSourceID="dsCastRegistReportTiming" DataTextField="CODE_NM" DataValueField="CODE">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾒﾃﾞｨｱ管理者ID
								</td>
								<td class="tdDataStyle" colspan="3">
									<asp:TextBox ID="txtAdminId" runat="server" MaxLength="32" Width="100px"></asp:TextBox>
									<asp:Label ID="lblDuplicate" runat="server" ForeColor="Red" Text="管理者IDが他の管理者IDと重複しています。" Visible="False"></asp:Label></td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ﾒﾃﾞｨｱ管理者ﾊﾟｽﾜｰﾄﾞ
								</td>
								<td class="tdDataStyle" colspan="3">
									<asp:TextBox ID="txtAdminPassword" runat="server" MaxLength="12" Width="160px"></asp:TextBox>
									<asp:RegularExpressionValidator ID="vdeAdminPassword" runat="server" ErrorMessage="ﾒﾃﾞｨｱ管理者ﾊﾟｽﾜｰﾄﾞは4桁以上の英数字で入力して下さい。" ValidationExpression="\w{4,12}" ControlToValidate="txtAdminPassword"
										ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									売上閲覧許可
								</td>
								<td class="tdDataStyle" colspan="3">
									<asp:CheckBox ID="chkViewSalesFlag" runat="server" />ﾒﾃﾞｨｱ用管理画面で、売上を閲覧できるようにする
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									利用状況
								</td>
								<td class="tdDataStyle" colspan="3">
									<asp:RadioButtonList ID="usedFlag" runat="server" RepeatDirection="horizontal">
										<asp:ListItem Text="停止中" Value="停止中"></asp:ListItem>
										<asp:ListItem Text="利用中" Value="利用中" Selected></asp:ListItem>
									</asp:RadioButtonList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									成果報告時に認証を必要としない
								</td>
								<td class="tdDataStyle" colspan="3">
									<asp:CheckBox ID="chkUnnecessaryCertifyFlag" runat="server" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									身分証
								</td>
								<td class="tdDataStyle" colspan="3">
									<asp:RadioButtonList ID="signUpFlag" runat="server" RepeatDirection="horizontal">
										<asp:ListItem Text="不要" Value="不要" Selected></asp:ListItem>
										<asp:ListItem Text="必要" Value="必要"></asp:ListItem>
									</asp:RadioButtonList>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
						<asp:CustomValidator runat="server" ID="vdcInsert" ErrorMessage="ブラウザの戻るボタンによって開かれた可能性があります。再度検索を行って下さい。" OnServerValidate="vdcInsert_ServerValidate" ValidationGroup="Detail"></asp:CustomValidator>
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[広告コード一覧]</legend>
			<table border="0" style="width: 800px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle">
						広告コード
					</td>
					<td class="tdDataStyle">
						<asp:TextBox ID="txtSeekAdCd" runat="server" Width="200px" MaxLength="32"></asp:TextBox>
					</td>
					<td class="tdHeaderStyle">
						広告名称
					</td>
					<td class="tdDataStyle">
						<asp:TextBox ID="txtSeekAdNm" runat="server"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<td class="tdHeaderStyle2">
						広告グループ
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstAdGroup" runat="server" DataSourceID="dsAdGroup" DataTextField="AD_GROUP_NM" DataValueField="AD_GROUP_CD" Width="240px">
						</asp:DropDownList>
					</td>
					<td class="tdHeaderStyle2">
						利用状況
					</td>
					<td class="tdDataStyle">
						<asp:RadioButtonList ID="usedSearch" runat="server" RepeatDirection="horizontal">
							<asp:ListItem Text="すべて" Value="" Selected></asp:ListItem>
							<asp:ListItem Text="停止中" Value="停止中"></asp:ListItem>
							<asp:ListItem Text="利用中" Value="利用中"></asp:ListItem>
						</asp:RadioButtonList>
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnRegist" Text="広告コード追加・修正" CssClass="seekbutton" CausesValidation="False" OnClick="btnRegist_Click" />
			<br />
			<br />
			<asp:GridView ID="grdAd" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsAd" AllowSorting="True" SkinID="GridViewColor">
				<Columns>
					<asp:TemplateField SortExpression="AD_CD" HeaderText="広告ｺｰﾄﾞ">
						<ItemTemplate>
							<asp:LinkButton ID="lnkAdCd" runat="server" Text='<%# Eval("AD_CD") %>' CommandArgument='<%# Eval("AD_CD")  %>' OnCommand="lnkAdCd_Command" CausesValidation="False">
							</asp:LinkButton>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:BoundField DataField="AD_NM" HeaderText="広告名" SortExpression="AD_NM">
						<ItemStyle HorizontalAlign="Left" />
					</asp:BoundField>
					<asp:BoundField DataField="ADMIN_ID" HeaderText="管理者ID" SortExpression="ADMIN_ID">
						<ItemStyle HorizontalAlign="Left" />
					</asp:BoundField>
					<asp:BoundField DataField="ADMIN_PASSWORD" HeaderText="管理者ﾊﾟｽﾜｰﾄﾞ" SortExpression="ADMIN_PASSWORD">
						<ItemStyle HorizontalAlign="Left" />
					</asp:BoundField>
					<asp:TemplateField SortExpression="MAN_REGIST_REPORT_TIMING_NM">
						<HeaderTemplate>
							<%= DisplayWordUtil.Replace("男性会員")%>
							<br />
							成果発生ﾀｲﾐﾝｸﾞ
						</HeaderTemplate>
						<ItemTemplate>
							<%# DataBinder.Eval(Container.DataItem, "MAN_REGIST_REPORT_TIMING_NM")%>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField SortExpression="CAST_REGIST_REPORT_TIMING_NM">
						<HeaderTemplate>
							<%= DisplayWordUtil.Replace("女性会員")%>
							<br />
							成果発生ﾀｲﾐﾝｸﾞ
						</HeaderTemplate>
						<ItemTemplate>
							<%# DataBinder.Eval(Container.DataItem, "CAST_REGIST_REPORT_TIMING_NM")%>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField DataField="UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False" SortExpression="UPDATE_DATE">
						<ItemStyle HorizontalAlign="Center" />
					</asp:BoundField>
					<asp:BoundField DataField="USED_FLAG" HeaderText="利用状況" SortExpression="USED_FLAG">
						<ItemStyle HorizontalAlign="Left"/>
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br />
				<a class="reccount">Current viewing page
					<%=grdAd.PageIndex + 1%>
					of
					<%=grdAd.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:Label ID="result" Text="" runat="server" />
	<asp:ObjectDataSource ID="dsAd" runat="server" SelectMethod="GetPageCollection" TypeName="Ad" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelected="dsAd_Selected"
		OnSelecting="dsAd_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pAdCd" Type="String" />
			<asp:Parameter Name="pAdNm" Type="String" />
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pAdGroupCd" Type="String" />
			<asp:Parameter Name="pUsedSearch" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsManRegistReportTiming" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="16" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastRegistReportTiming" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="20" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsAdGroup" runat="server" SelectMethod="GetList" TypeName="AdGroup" OnSelecting="dsAdGroup_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCodeDtl" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="A7" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrAdCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeAdCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrAdNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdeAdminPassword" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vdrManConversionFee" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8" TargetControlID="vdrWomanConversionFee" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtManConversionFee" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtWomanConversionFee" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
