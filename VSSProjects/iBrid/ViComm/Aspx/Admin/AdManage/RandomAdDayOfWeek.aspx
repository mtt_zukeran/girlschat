<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RandomAdDayOfWeek.aspx.cs" Inherits="Site_RandomAdDayOfWeek" Title="曜日別ランダム広告設定"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="曜日別ランダム広告設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<asp:Literal ID="ltlUserColor" runat="server"></asp:Literal>
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<asp:Label ID="lblDayCd" runat="server" Text="" Visible="false"></asp:Label>
			<asp:Label ID="lblAdNo" runat="server" Text="" Visible="false"></asp:Label>
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									サイトコード
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
									</asp:DropDownList>
								</td>
								<td class="tdHeaderStyle2">
									曜日
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblDayNm" runat="server" Text=""></asp:Label>
								</td>
							</tr>
						</table>
					</fieldset>
				</asp:Panel>
				<table border="0">
					<tr>
						<td valign="top">
							<asp:Panel runat="server" ID="pnlSelect" Height="380px" ScrollBars="Auto">
								<fieldset class="fieldset-inner">
									<legend>[アウトルック]</legend>
									<asp:GridView ID="grdOutLookAd" runat="server" AllowPaging="False" AutoGenerateColumns="False" DataSourceID="dsOutLookAd" SkinID="GridView" Width="230px" ShowHeader="False">
										<Columns>
											<asp:TemplateField>
												<ItemTemplate>
													<asp:Literal ID="lblHtmlDoc1" runat="server" Text='<%# string.Format("<table class=\"userColor\" ><tr><td width=\"220px\">{0}</tr></td></table>",Eval("HTML_AD"))%>'></asp:Literal>
												</ItemTemplate>
												<ItemStyle HorizontalAlign="Left" />
											</asp:TemplateField>
										</Columns>
									</asp:GridView>
								</fieldset>
							</asp:Panel>
						</td>
						<td valign="top">
							<asp:Panel runat="server" ID="pnlDtl">
								<fieldset class="fieldset-inner">
									<legend>[広告内容]</legend>
									<table border="0" style="width: 548px" class="tableStyle">
										<tr>
											<td class="tdHeaderSmallStyle">
												<br />
											</td>
											<td class="tdDataStyle">
												<asp:Label ID="lblAd" runat="server" Width="400px" Enabled="false"></asp:Label>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderSmallStyle2">
												広告
											</td>
											<td class="tdDataStyle">
												$NO_TRANS_START;
												<pin:pinEdit ID="txtHtmlDoc" runat="server" UserMode="HTML" DisplayStyle="Style5" DocumentView="True" HtmlMode="Body" Ruler="False" ReturnMode="BR" SpellMode="Inline"
													ToolbarConfiguration="ToolbarDesigner" Toolbar="T09SE10116012136267;T22232427282930313233343536;B53SE54SE55" FormatMode="Classic" BorderWidth="1px"
													Height="300px" Width="500px" DocumentWidth="230" Font-Bold="False" FontSizeType="px" RelativeBaseImageUrl="/" RelativeBaseLinkUrl="/" RelativeImageRoot="/">
												</pin:pinEdit>
												<asp:TextBox ID="txtHtmlDocText" runat="server" TextMode="MultiLine" Height="300px" Width="500px" Visible="false"></asp:TextBox>
												$NO_TRANS_END;
											</td>
										</tr>
									</table>
									<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
									<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
									<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
								</fieldset>
							</asp:Panel>
						</td>
					</tr>
				</table>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[曜日別ランダム広告一覧]</legend>
			<table border="0" style="width: 640px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle2" style="height: 24px">
						サイトコード
					</td>
					<td class="tdDataStyle" style="height: 24px">
						<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
						</asp:DropDownList>
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnRegist" Text="追加" CssClass="seekbutton" OnClick="btnRegist_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" OnClick="btnCSV_Click" CausesValidation="False" Enabled="false" /><br />
			<br />
			<div style="height: 600px; width: auto; overflow: auto;">
				<asp:GridView ID="grdRandomAdDayOfWeek" runat="server" AutoGenerateColumns="False" DataSourceID="dsRandomAdDayOfWeek" AllowSorting="True" SkinID="GridView" Font-Size="X-Small">
					<Columns>
						<asp:TemplateField HeaderText="曜日">
							<ItemStyle HorizontalAlign="Center" VerticalAlign="Top" CssClass="RowStyleNoPad" Width="38px" />
							<ItemTemplate>
								<asp:Label ID="lblDayNm" runat="server" Text='<%# Eval("DAY_NM") %>' Width="38px"></asp:Label>
								<asp:Label ID="lblDayCd" runat="server" Text='<%# Eval("DAY_CD") %>' Visible="false"></asp:Label>
							</ItemTemplate>
							<HeaderStyle CssClass="HeaderStyle" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="広告1">
							<ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="RowStyleNoPad" Wrap="false" />
							<ItemTemplate>
								<asp:LinkButton ID="lnkHtmlAd1" runat="server" Text="編集" CommandArgument='<%# string.Format("{0}:{1}:{2}:{3}",Eval("SITE_CD"),Eval("DAY_CD"),Eval("DAY_NM"),"1") %>'
									OnCommand="lnkHtmlAd_Command" CausesValidation="False">
								</asp:LinkButton><br />
								<asp:Literal ID="lblHtmlAd1" runat="server" Text='<%# Eval("HTML_AD1")%>'></asp:Literal>
							</ItemTemplate>
							<HeaderStyle CssClass="HeaderStyle" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="広告2">
							<ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="RowStyleNoPad" Wrap="false" />
							<ItemTemplate>
								<asp:LinkButton ID="lnkHtmlAd2" runat="server" Text="編集" CommandArgument='<%# string.Format("{0}:{1}:{2}:{3}",Eval("SITE_CD"),Eval("DAY_CD"),Eval("DAY_NM"),"2") %>'
									OnCommand="lnkHtmlAd_Command" CausesValidation="False">
								</asp:LinkButton><br />
								<asp:Literal ID="lblHtmlAd2" runat="server" Text='<%# Eval("HTML_AD2")%>'></asp:Literal>
							</ItemTemplate>
							<HeaderStyle CssClass="HeaderStyle" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="広告3">
							<ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="RowStyleNoPad" Wrap="false" />
							<ItemTemplate>
								<asp:LinkButton ID="lnkHtmlAd3" runat="server" Text="編集" CommandArgument='<%# string.Format("{0}:{1}:{2}:{3}",Eval("SITE_CD"),Eval("DAY_CD"),Eval("DAY_NM"),"3") %>'
									OnCommand="lnkHtmlAd_Command" CausesValidation="False">
								</asp:LinkButton><br />
								<asp:Literal ID="lblHtmlAd3" runat="server" Text='<%# Eval("HTML_AD3")%>'></asp:Literal>
							</ItemTemplate>
							<HeaderStyle CssClass="HeaderStyle" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="広告4">
							<ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="RowStyleNoPad" Wrap="false" />
							<ItemTemplate>
								<asp:LinkButton ID="lnkHtmlAd4" runat="server" Text="編集" CommandArgument='<%# string.Format("{0}:{1}:{2}:{3}",Eval("SITE_CD"),Eval("DAY_CD"),Eval("DAY_NM"),"4") %>'
									OnCommand="lnkHtmlAd_Command" CausesValidation="False">
								</asp:LinkButton><br />
								<asp:Literal ID="lblHtmlAd4" runat="server" Text='<%# Eval("HTML_AD4")%>'></asp:Literal>
							</ItemTemplate>
							<HeaderStyle CssClass="HeaderStyle" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="広告5">
							<ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="RowStyleNoPad" Wrap="false" />
							<ItemTemplate>
								<asp:LinkButton ID="lnkHtmlAd5" runat="server" Text="編集" CommandArgument='<%# string.Format("{0}:{1}:{2}:{3}",Eval("SITE_CD"),Eval("DAY_CD"),Eval("DAY_NM"),"5") %>'
									OnCommand="lnkHtmlAd_Command" CausesValidation="False">
								</asp:LinkButton><br />
								<asp:Literal ID="lblHtmlAd5" runat="server" Text='<%# Eval("HTML_AD5")%>'></asp:Literal>
							</ItemTemplate>
							<HeaderStyle CssClass="HeaderStyle" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="広告6">
							<ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="RowStyleNoPad" Wrap="false" />
							<ItemTemplate>
								<asp:LinkButton ID="lnkHtmlAd6" runat="server" Text="編集" CommandArgument='<%# string.Format("{0}:{1}:{2}:{3}",Eval("SITE_CD"),Eval("DAY_CD"),Eval("DAY_NM"),"6") %>'
									OnCommand="lnkHtmlAd_Command" CausesValidation="False">
								</asp:LinkButton><br />
								<asp:Literal ID="lblHtmlAd6" runat="server" Text='<%# Eval("HTML_AD6")%>'></asp:Literal>
							</ItemTemplate>
							<HeaderStyle CssClass="HeaderStyle" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="広告7">
							<ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="RowStyleNoPad" Wrap="false" />
							<ItemTemplate>
								<asp:LinkButton ID="lnkHtmlAd7" runat="server" Text="編集" CommandArgument='<%# string.Format("{0}:{1}:{2}:{3}",Eval("SITE_CD"),Eval("DAY_CD"),Eval("DAY_NM"),"7") %>'
									OnCommand="lnkHtmlAd_Command" CausesValidation="False">
								</asp:LinkButton><br />
								<asp:Literal ID="lblHtmlAd7" runat="server" Text='<%# Eval("HTML_AD7")%>'></asp:Literal>
							</ItemTemplate>
							<HeaderStyle CssClass="HeaderStyle" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="広告8">
							<ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="RowStyleNoPad" Wrap="false" />
							<ItemTemplate>
								<asp:LinkButton ID="lnkHtmlAd8" runat="server" Text="編集" CommandArgument='<%# string.Format("{0}:{1}:{2}:{3}",Eval("SITE_CD"),Eval("DAY_CD"),Eval("DAY_NM"),"8") %>'
									OnCommand="lnkHtmlAd_Command" CausesValidation="False">
								</asp:LinkButton><br />
								<asp:Literal ID="lblHtmlAd8" runat="server" Text='<%# Eval("HTML_AD8")%>'></asp:Literal>
							</ItemTemplate>
							<HeaderStyle CssClass="HeaderStyle" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="広告9">
							<ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="RowStyleNoPad" Wrap="false" />
							<ItemTemplate>
								<asp:LinkButton ID="lnkHtmlAd9" runat="server" Text="編集" CommandArgument='<%# string.Format("{0}:{1}:{2}:{3}",Eval("SITE_CD"),Eval("DAY_CD"),Eval("DAY_NM"),"9") %>'
									OnCommand="lnkHtmlAd_Command" CausesValidation="False">
								</asp:LinkButton><br />
								<asp:Literal ID="lblHtmlAd9" runat="server" Text='<%# Eval("HTML_AD9")%>'></asp:Literal>
							</ItemTemplate>
							<HeaderStyle CssClass="HeaderStyle" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="広告10">
							<ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="RowStyleNoPad" Wrap="false" />
							<ItemTemplate>
								<asp:LinkButton ID="lnkHtmlAd10" runat="server" Text="編集" CommandArgument='<%# string.Format("{0}:{1}:{2}:{3}",Eval("SITE_CD"),Eval("DAY_CD"),Eval("DAY_NM"),"10") %>'
									OnCommand="lnkHtmlAd_Command" CausesValidation="False">
								</asp:LinkButton><br />
								<asp:Literal ID="lblHtmlAd10" runat="server" Text='<%# Eval("HTML_AD10")%>'></asp:Literal>
							</ItemTemplate>
							<HeaderStyle CssClass="HeaderStyle" Wrap="false" />
						</asp:TemplateField>
						<asp:BoundField DataField="UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yyyy/MM/dd}" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" VerticalAlign="Top" CssClass="RowStyleNoPad" Width="80px" Wrap="false" />
						</asp:BoundField>
					</Columns>
					<PagerSettings Mode="NumericFirstLast" />
				</asp:GridView>
			</div>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsRandomAdDayOfWeek" runat="server" SelectMethod="GetPageCollection" TypeName="RandomAdDayOfWeek"
		OnSelected="dsRandomAdDayOfWeek_Selected" OnSelecting="dsRandomAdDayOfWeek_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
        </SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsOutLookAd" runat="server" SelectMethod="GetOne" TypeName="RandomAdDayOfWeek" OnSelecting="dsOutLookAd_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pDayCd" Type="String" />
			<asp:Parameter Name="pAdNo" Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender3" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
