﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 入会月別売上履歴(会員)
--	Progaram ID		: SalesByEntMonthlyHistoryList
--
--  Creation Date	: 2014.06.26
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class AdManage_SalesByEntMonthlyHistoryList:System.Web.UI.Page {

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SITE_CD"]);
		}
		set {
			this.ViewState["SITE_CD"] = value;
		}
	}
	
	private string EntMonth {
		get {
			return iBridUtil.GetStringValue(this.ViewState["ENT_MONTH"]);
		}
		set {
			this.ViewState["ENT_MONTH"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			this.EntMonth = iBridUtil.GetStringValue(Request.QueryString["entmonth"]);

			this.CreateList();
			this.InitPage();

			pnlGrid.DataBind();
		}
	}

	private void InitPage() {
	}

	protected void dsSalesByEntMonthly_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {

		SalesByEntMonthly.SearchCondition oSearchCondition = new SalesByEntMonthly.SearchCondition();
		oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
		oSearchCondition.EntMonth = this.EntMonth;

		e.InputParameters[0] = oSearchCondition;
	}

	private void ClearField() {
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void grdSalesByEntMonthly_RowDataBound(object sender,GridViewRowEventArgs e) {

	}

	private void GetList() {

		grdSalesByEntMonthly.PageIndex = 0;
		grdSalesByEntMonthly.DataSourceID = "dsSalesByEntMonthly";
		DataBind();
	}

	private void CreateList() {
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}
	
	protected string GetDisplayReportMonth(string pReportMonth) {
		DateTime dtReportMonth = DateTime.Parse(string.Format("{0}/01",pReportMonth));
		return dtReportMonth.ToString("yyyy年M月集計");
	}
}
