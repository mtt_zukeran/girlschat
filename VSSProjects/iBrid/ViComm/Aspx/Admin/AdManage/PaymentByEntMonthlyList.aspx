﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PaymentByEntMonthlyList.aspx.cs" Inherits="AdManage_PaymentByEntMonthlyList" Title="出演者入会月別報酬(月別)"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="アクセスログ"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
                            <td class="tdHeaderStyle">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                表示月
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstYear" runat="server" Width="60px">
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstMonth" runat="server" Width="40px">
                                </asp:DropDownList>月
                                <br />
								<asp:Label ID="lblErrorMessageReportDayFrom" runat="server" ForeColor="red" Visible="false"></asp:Label>
                            </td>
                        </tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
			</asp:Panel>
		</fieldset>
		<fieldset>
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="560px">
				<asp:GridView ID="grdPaymentByEntMonthly" runat="server" AutoGenerateColumns="False" DataSourceID="dsPaymentByEntMonthly" CellPadding="0" ShowFooter="True" AllowSorting="True"
					OnRowDataBound="grdPaymentByEntMonthly_RowDataBound" SkinID="GridViewColorNoRowStyle">
					<Columns>
						<asp:TemplateField HeaderText="入会月" FooterText="合計">
							<ItemStyle HorizontalAlign="Center" />
							<ItemTemplate>
								<asp:HyperLink ID="lnkEntMonth" runat="server" NavigateUrl='<%# string.Format("~/AdManage/PaymentByEntDailyList.aspx?sitecd={0}&entmonth={1}&year={2}&month={3}",lstSiteCd.SelectedValue,Eval("ENT_MONTH"),lstYear.SelectedValue,lstMonth.SelectedValue) %>' Text='<%# GetDisplayEntMonth(Eval("ENT_MONTH").ToString()) %>' ></asp:HyperLink>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="報酬">
							<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
							<ItemTemplate>
								<asp:Label ID="Label2" runat="server" Text='<%# Eval("PAYMENT_AMT") %>' BackColor=''></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="件数">
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("PAYMENT_COUNT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
						</asp:TemplateField>
					</Columns>
					<FooterStyle ForeColor="Black" BackColor="LightYellow" />
				</asp:GridView>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsPaymentByEntMonthly" runat="server" SelectMethod="GetSalesList" TypeName="PaymentByEntMonthly" OnSelecting="dsPaymentByEntMonthly_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
</asp:Content>
