﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 広告集計データ登録指定
--	Progaram ID		: AdDataRegist
--  Creation Date	: 2014.06.17
--  Creater			: Y.Ikemiya
**************************************************************************/
using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using ViComm.Extension.Pwild;

public partial class AdManage_AdDataRegist:System.Web.UI.Page {

	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SITE_CD"]);
		}
		set {
			this.ViewState["SITE_CD"] = value;
		}
	}

	private string Year {
		get {
			return iBridUtil.GetStringValue(this.ViewState["YEAR"]);
		}
		set {
			this.ViewState["YEAR"] = value;
		}
	}

	private string Month {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MONTH"]);
		}
		set {
			this.ViewState["MONTH"] = value;
		}
	}

	private string Day {
		get {
			return iBridUtil.GetStringValue(this.ViewState["DAY"]);
		}
		set {
			this.ViewState["DAY"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);

			this.CreateList();
			this.InitPage();
		}
	}

	protected void btnRegistData_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		if (!this.IsCorrectDate()) {
			return;
		}

		this.RegistData();
		lblErrorMessageRegistDate.Text = "集計データを登録しました";
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}
	
	private void RegistData() {
		using (DbSession db = new DbSession()) {

			db.PrepareProcedure("EXEC_REGIST_AD_DATA");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("pREGIST_DATE",DbSession.DbType.VARCHAR2,string.Format("{0}/{1}/{2}",lstYear.SelectedValue,lstMonth.SelectedValue,lstDay.SelectedValue));
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	private void InitPage() {
		this.lstYear.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstMonth.SelectedValue = DateTime.Now.ToString("MM");
		this.lstDay.SelectedValue = DateTime.Now.ToString("dd");
	}

	private void CreateList() {
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		int iStartYear = 2009;
		int iCurYear = DateTime.Now.Year;
		lstYear.Items.Clear();

		for (int i = 0;i + iStartYear <= iCurYear;i++) {

			lstYear.Items.Add(new ListItem(iBridUtil.GetStringValue(i + iStartYear)));
		}

		lstMonth.Items.Clear();
		for (int i = 1;i <= 12;i++) {
			lstMonth.Items.Add(new ListItem(string.Format("{0:D2}",i)));
		}

		lstDay.Items.Clear();
		for (int i = 1;i <= 31;i++) {
			lstDay.Items.Add(new ListItem(string.Format("{0:D2}",i)));
		}
	}

	private bool IsCorrectDate() {
		DateTime dtChk;
		if (DateTime.TryParse(string.Format("{0}/{1}/{2}",lstYear.SelectedValue,lstMonth.SelectedValue,lstDay.SelectedValue),out dtChk)) {
			return true;
		} else {
			lblErrorMessageRegistDate.Text = "日付が正しくありません";
			return false;
		}
	}
}