﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 広告コード日別月計

--	Progaram ID		: AdPerformanceByDay
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/
// [ Update History ]
/*------------------------------------------------------------------------
  Date			Updater     Update Explain
  2010/08/13	K.Itoh		ﾕﾆｰｸ数の対応。＋できる限りリファクタ
-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class AdManage_AdPerformanceByDay:System.Web.UI.Page {
	private int accessCount;
	private int registMailFaildCount;
	private int loginCount;
	private int loginSuccessCount;
	private int loginSuccessCountRaw;
	private int loginSuccessUniqueCount;
	private int loginSuccessUniqueCountRaw;
	private int registCount;
	private int registCountRaw;
	private int reRegistCnt;
	private int withdrawalCnt;
	private int usedPoint;
	private int ordinarySalesAmt;
	private int affiliateSalesAmt;
	private int totalAmt;

	private string nextbusinesstime;

	protected int colReportDay = 0;
	protected int colAccessCountIndex = 1;
	protected int colRegistCountRawIndex = 2;
	protected int colRegistCountIndex = 3;
	protected int colWithdrawalIndex = 4;
	protected int colReRegistIndex = 5;
	protected int colRegistMailFaildCountIndex = 6;
	protected int colLoginCountIndex = 7;
	protected int colLoginSuccessCountRawIndex = 8;
	protected int colLoginSuccessCountIndex = 9;
	protected int colLoginSuccessCountUniqueRawIndex = 10;
	protected int colLoginSuccessCountUniqueIndex = 11;
	protected int colTotalUsedPointIndex = 12;
	protected int colOrdinalSalesIndex = 13;
	protected int colAffiliateSalesIndex = 14;
	protected int colTotalAmtIndex = 15;

	protected string SexCd {
		get {
			return ViewState["SEX_CD"] as string ?? string.Empty;
		}
		set {
			ViewState["SEX_CD"] = value;
		}
	}

	protected bool IsManMode {
		get {
			return this.SexCd.Equals(ViCommConst.MAN);
		}
	}

	protected bool IsAdManager {
		get {
			return Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_AD_MANAGE);
		}
	}

	protected bool IsCanViewSales {
		get {
			return Session["ViewSalesFlag"].ToString().Equals(ViCommConst.FLAG_ON_STR);
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		this.grdAd.Columns[this.colLoginCountIndex].Visible = !ManageCompany.IsAvailableUniqueAccessSummary();
		this.grdAd.Columns[this.colLoginSuccessCountIndex].Visible = ManageCompany.IsAvailableUniqueAccessSummary();
		this.grdAd.Columns[this.colLoginSuccessCountUniqueIndex].Visible = ManageCompany.IsAvailableUniqueAccessSummary();

		grdAd.DataSourceID = string.Empty;
		lblAdNm.Text = string.Empty;
		pnlInfo.Visible = false;
		ClearField();

		txtAdCd.Text = iBridUtil.GetStringValue(Request.QueryString["adcd"]);
		string sYY = iBridUtil.GetStringValue(Request.QueryString["yy"]);
		string sMM = iBridUtil.GetStringValue(Request.QueryString["mm"]);

		DataBind();
		if (!IsPostBack) {
            SysPrograms.SetupYear(lstYYYY);
			lstSiteCd.DataSourceID = string.Empty;

			if (!sYY.Equals(string.Empty)) {
				lstYYYY.SelectedIndex = lstYYYY.Items.IndexOf(lstYYYY.Items.FindByValue(sYY));
			} else {
				lstYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
			}
			if (!sMM.Equals(string.Empty)) {
				lstMM.SelectedIndex = lstMM.Items.IndexOf(lstMM.Items.FindByValue(sMM));
			} else {
				lstMM.SelectedValue = DateTime.Now.ToString("MM");
			}

			this.SexCd = iBridUtil.GetStringValue(Request.QueryString["sexcd"]);

			if (this.IsManMode) {
				this.Title = this.Title + DisplayWordUtil.Replace("（会員）");
				lblPgmTitle.Text = lblPgmTitle.Text + DisplayWordUtil.Replace("（会員）");
				this.grdAd.Columns[this.colOrdinalSalesIndex].Visible = true;
				this.grdAd.Columns[this.colAffiliateSalesIndex].Visible = true;
				this.grdAd.Columns[this.colTotalAmtIndex].Visible = true;

			} else {
				this.Title = this.Title + DisplayWordUtil.Replace("（出演者）");
				lblPgmTitle.Text = lblPgmTitle.Text + DisplayWordUtil.Replace("（出演者）");
				this.grdAd.Columns[this.colOrdinalSalesIndex].Visible = false;
				this.grdAd.Columns[this.colAffiliateSalesIndex].Visible = false;
				this.grdAd.Columns[this.colTotalAmtIndex].Visible = false;
				this.grdAd.Columns[this.colRegistMailFaildCountIndex].Visible = false;
			}

			this.grdAd.Columns[this.colLoginCountIndex].HeaderText = "ﾛｸﾞｲﾝ数";
			this.grdAd.Columns[this.colLoginSuccessCountRawIndex].HeaderText = "ﾛｸﾞｲﾝ数";
			this.grdAd.Columns[this.colLoginSuccessCountIndex].HeaderText = "ﾛｸﾞｲﾝ数";
			this.grdAd.Columns[this.colLoginSuccessCountUniqueRawIndex].HeaderText = "ﾛｸﾞｲﾝ数(ﾕﾆｰｸ)";
			this.grdAd.Columns[this.colLoginSuccessCountUniqueIndex].HeaderText = "ﾛｸﾞｲﾝ数(ﾕﾆｰｸ)";
			this.grdAd.Columns[this.colTotalUsedPointIndex].HeaderText = this.IsManMode ? "利用Pt." : "獲得報酬";

			if (IsAdManager) {
				this.grdAd.Columns[this.colTotalUsedPointIndex].Visible = false;
				this.grdAd.Columns[this.colOrdinalSalesIndex].Visible = false;
				this.grdAd.Columns[this.colAffiliateSalesIndex].Visible = false;
				this.grdAd.Columns[this.colTotalAmtIndex].Visible = false;

				this.grdAd.Columns[this.colRegistCountRawIndex].Visible = false;
				this.grdAd.Columns[this.colWithdrawalIndex].Visible = false;
				this.grdAd.Columns[this.colReRegistIndex].Visible = false;
				this.grdAd.Columns[this.colLoginSuccessCountRawIndex].Visible = false;
				this.grdAd.Columns[this.colLoginSuccessCountUniqueRawIndex].Visible = false;
			} else {
				this.grdAd.Columns[this.colRegistCountIndex].HeaderText = "認証数";
				this.grdAd.Columns[this.colLoginSuccessCountIndex].HeaderText += "[認証数]";
				this.grdAd.Columns[this.colLoginSuccessCountUniqueIndex].HeaderText += "[認証数]";
			}
			if (IsCanViewSales && IsManMode) {
				this.grdAd.Columns[this.colTotalAmtIndex].Visible = true;
			}

			if (!txtAdCd.Text.Equals(string.Empty)) {
				CheckAdNm();
				GetList();
			}
		}

		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals(string.Empty)) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void ClearField() {
		accessCount = 0;
		registMailFaildCount = 0;
		loginCount = 0;
		loginSuccessCount = 0;
		loginSuccessCountRaw = 0;
		loginSuccessUniqueCount = 0;
		loginSuccessUniqueCountRaw = 0;
		usedPoint = 0;
		registCount = 0;
		registCountRaw = 0;
		reRegistCnt = 0;
		withdrawalCnt = 0;
		ordinarySalesAmt = 0;
		affiliateSalesAmt = 0;
		totalAmt = 0;
		nextbusinesstime = "00:00:00";
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (IsValid) {
			GetList();
		}
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected string GetBusTime() {
		return nextbusinesstime.Substring(0,2);
	}

	protected string SetDateTo(object pDate) {
		string sCnvD = SysPrograms.IsDateToLastDate(pDate.ToString()) + " " + nextbusinesstime;
		return DateTime.ParseExact(sCnvD,"yyyy/MM/dd HH:mm:ss",null).AddHours(23).ToString("yyyy/MM/dd");
	}

	protected string GetTime(object pDate) {
		string sCnvD = SysPrograms.IsDateToLastDate(pDate.ToString()) + " " + nextbusinesstime;
		return DateTime.ParseExact(sCnvD,"yyyy/MM/dd HH:mm:ss",null).AddHours(23).ToString("HH");
	}

	protected void grdAd_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {

			accessCount += GetValueWithConvertInt32(DataBinder.Eval(e.Row.DataItem,"ACCESS_COUNT_AD"));


			loginCount += GetValueWithConvertInt32(
				DataBinder.Eval(e.Row.DataItem,"LOGIN_COUNT"),
				DataBinder.Eval(e.Row.DataItem,"LOGIN_COUNT_AD"));

			registCount += GetValueWithConvertInt32(DataBinder.Eval(e.Row.DataItem,"REGIST_COUNT_AD"));
			registCountRaw += GetValueWithConvertInt32(DataBinder.Eval(e.Row.DataItem,"REGIST_COUNT"));
			withdrawalCnt += GetValueWithConvertInt32(DataBinder.Eval(e.Row.DataItem,"WITHDRAWAL_COUNT"));
			reRegistCnt += GetValueWithConvertInt32(DataBinder.Eval(e.Row.DataItem,"RE_REGIST_COUNT"));

			registMailFaildCount += GetValueWithConvertInt32(
				DataBinder.Eval(e.Row.DataItem,"REGIST_MAIL_SEND_FAILED_COUNT"),
				DataBinder.Eval(e.Row.DataItem,"REGIST_MAIL_SEND_FAILED_COUNT"));

			loginSuccessCount += GetValueWithConvertInt32(DataBinder.Eval(e.Row.DataItem,"LOGIN_SUCCESS_COUNT_AD"));
			loginSuccessCountRaw += GetValueWithConvertInt32(DataBinder.Eval(e.Row.DataItem,"LOGIN_SUCCESS_COUNT"));
			loginSuccessUniqueCount += GetValueWithConvertInt32(DataBinder.Eval(e.Row.DataItem,"LOGIN_SUCCESS_COUNT_UNIQUE_AD"));
			loginSuccessUniqueCountRaw += GetValueWithConvertInt32(DataBinder.Eval(e.Row.DataItem,"LOGIN_SUCCESS_COUNT_UNIQUE"));

			usedPoint += GetValueWithConvertInt32(
				DataBinder.Eval(e.Row.DataItem,"TOTAL_USED_POINT"),
				DataBinder.Eval(e.Row.DataItem,"TOTAL_USED_POINT_AD"));

			ordinarySalesAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"ORDINARY_SALES_AMT"));
			affiliateSalesAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"POINT_BACK_SALES_AMT"));
			totalAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TOTAL_SALES_AMT"));

			HyperLink lnkRegistCount = (HyperLink)e.Row.FindControl("lnkRegistCount");
			if (ViewState["SEX_CD"].ToString().Equals(ViCommConst.MAN)) {
				lnkRegistCount.NavigateUrl = string.Format("~/Man/ManInquiry.aspx?sitecd={0}&registdayfrom={1}&registtimefrom={2}&registdayto={3}&registtimeto={4}&adcd={5}&adnm={6}&return={7}",DataBinder.Eval(e.Row.DataItem,"SITE_CD"),DataBinder.Eval(e.Row.DataItem,"ACCESS_DAY"),GetBusTime(),SetDateTo(DataBinder.Eval(e.Row.DataItem,"ACCESS_DAY")),GetTime(DataBinder.Eval(e.Row.DataItem,"ACCESS_DAY")),txtAdCd.Text,HttpUtility.UrlEncodeUnicode(lblAdNm.Text),"AdPerformanceByDay.aspx");
			} else {
				lnkRegistCount.NavigateUrl = string.Format("~/Cast/CastCharacterInquiry.aspx?sitecd={0}&adcd={1}&adnm={2}&registdayfrom={3}&registdayto={4}",DataBinder.Eval(e.Row.DataItem,"SITE_CD"),txtAdCd.Text,HttpUtility.UrlEncodeUnicode(lblAdNm.Text),DataBinder.Eval(e.Row.DataItem,"ACCESS_DAY"),DataBinder.Eval(e.Row.DataItem,"ACCESS_DAY"));
			}
		} else if (e.Row.RowType == DataControlRowType.Footer) {
			e.Row.Cells[this.colAccessCountIndex].Text = accessCount.ToString();
			e.Row.Cells[this.colRegistCountIndex].Text = registCount.ToString();
			e.Row.Cells[this.colRegistCountRawIndex].Text = registCountRaw.ToString();
			e.Row.Cells[this.colWithdrawalIndex].Text = withdrawalCnt.ToString();
			e.Row.Cells[this.colReRegistIndex].Text = reRegistCnt.ToString();
			e.Row.Cells[this.colRegistMailFaildCountIndex].Text = registMailFaildCount.ToString();
			e.Row.Cells[this.colLoginCountIndex].Text = loginCount.ToString();
			e.Row.Cells[this.colLoginSuccessCountIndex].Text = loginSuccessCount.ToString();
			e.Row.Cells[this.colLoginSuccessCountRawIndex].Text = loginSuccessCountRaw.ToString();
			e.Row.Cells[this.colLoginSuccessCountUniqueIndex].Text = loginSuccessUniqueCount.ToString();
			e.Row.Cells[this.colLoginSuccessCountUniqueRawIndex].Text = loginSuccessUniqueCountRaw.ToString();
			e.Row.Cells[this.colTotalUsedPointIndex].Text = usedPoint.ToString();
			e.Row.Cells[this.colOrdinalSalesIndex].Text = ordinarySalesAmt.ToString();
			e.Row.Cells[this.colAffiliateSalesIndex].Text = affiliateSalesAmt.ToString();
			e.Row.Cells[this.colTotalAmtIndex].Text = totalAmt.ToString();
		}

		if (Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_PRODUCTION)) {
			e.Row.Cells[this.colTotalUsedPointIndex].Visible = false;
		}
	}

	private void GetList() {
		if (IsAdManager) {
			this.grdAd.Columns[this.colRegistCountIndex].Visible = true;
			this.grdAd.Columns[this.colLoginSuccessCountIndex].Visible = true;
			this.grdAd.Columns[this.colLoginSuccessCountUniqueIndex].Visible = true;
		} else {
			this.grdAd.Columns[this.colRegistCountIndex].Visible = true;
			this.grdAd.Columns[this.colLoginSuccessCountIndex].Visible = false;
			this.grdAd.Columns[this.colLoginSuccessCountUniqueIndex].Visible = false;
		}
		ClearField();
		SetNextBusinessTime();
		grdAd.DataSourceID = "dsAccessPage";
		DataBind();
		pnlInfo.Visible = true;
	}

	protected void vdcAdCd_ServerValidate(object source,ServerValidateEventArgs args) {
		args.IsValid = CheckAdNm();
	}

	private bool CheckAdNm() {
		bool bRet;
		using (Ad oAd = new Ad()) {
			string sAdNm = string.Empty;
			bRet = oAd.IsExist(txtAdCd.Text,ref sAdNm);
			lblAdNm.Text = sAdNm;
		}
		return bRet;
	}


	protected void SetNextBusinessTime() {
		using (Site oSite = new Site()) {
			oSite.GetOne(lstSiteCd.SelectedValue);
			try {
				DateTime.ParseExact(oSite.nextbusinesstime,"HH:mm:ss",null);
				nextbusinesstime = oSite.nextbusinesstime;
			} catch {
				nextbusinesstime = "00:00:00";
			}
		}
	}
	protected string GetValue(object pValue,object pAdValue) {
		object oValue = (IsAdManager ? pAdValue : pValue) ?? string.Empty;
		return oValue.ToString();
	}

	protected int GetValueWithConvertInt32(object pValue,object pAdValue) {
		string sValue = GetValue(pValue,pAdValue);
		int iResult = 0;
		return int.TryParse(sValue,out iResult) ? iResult : 0;
	}

	protected int GetValueWithConvertInt32(object pValue) {
		int iResult = 0;
		return int.TryParse(pValue.ToString(),out iResult) ? iResult : 0;
	}

}
