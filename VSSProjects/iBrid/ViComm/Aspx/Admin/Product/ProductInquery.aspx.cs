﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using iBridCommLib;
using ViComm;
using System.Drawing;
using System.Collections.Generic;

public partial class Product_ProductInquery : System.Web.UI.Page {
	string recCount = string.Empty;
	private const string SEARCH_ID = "_PRODUCT_INQUERY_SEARCH_CONDITION_";
	private const int INDEX_PRODUCT_NM = 2;
	private const int INDEX_AUCTION_START_DATE = 5;
	private const int INDEX_GROSS_PRICE = 6;
	private const int INDEX_CHARGE_POINT = 7;
	private const int INDEX_MAX_BID_AMT = 8;
	private const int INDEX_AUCTION_STATUS = 10;
	private const int INDEX_BUY_COUNT = 12;

	protected Product.SearchCondition SearchCondition {
		get { return Session[SEARCH_ID] as Product.SearchCondition; }
		private set { Session.Add(SEARCH_ID, value); }
	}

	protected bool IsRestorable {
		get {
			if (this.SearchCondition == null) {
				return false;
			}
			return true;
		}
	}

	protected void Page_Load(object sender, EventArgs e) {
		if (!IsPostBack) {
			this.InitPage();
			if (!Request.QueryString.ToString().Equals(string.Empty)) {
				this.GetList();
			}
		}
	}

	protected void lnkCondition_Click(object sender, EventArgs e) {
		if (pnlKey.Visible) {
			pnlKey.Visible = false;
			pnlGrid.Height = 550;
			lnkCondition.Text = "[検索条件表示]";
		} else {
			pnlKey.Visible = true;
			pnlGrid.Height = 290;
			lnkCondition.Text = "[条件非表示]";
		}
	}

	protected void btnListSeek_Click(object sender, EventArgs e) {
		if (IsValid) {
			this.GetList();
		}
	}

	protected void btnClear_Click(object sender, EventArgs e) {
		if (IsValid) {
			this.SearchCondition = null;
			this.InitPage();
		}
	}

	protected void dsProduct_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		this.SetupCondition();

		Product.SearchCondition oSearchCondition = new Product.SearchCondition();
		oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
		oSearchCondition.ProductAgentCd = this.lstProductAgent.SelectedValue;
		oSearchCondition.Code = this.lstProductType.SelectedValue;

		oSearchCondition.ProductName = TrimEnd(this.txtProductNm.Text);
		oSearchCondition.ProductSeries = TrimEnd(this.txtProductSeries.Text);
		oSearchCondition.Cast = TrimEnd(this.txtCast.Text);
		oSearchCondition.ProductId = TrimEnd(this.txtProductId.Text);
		oSearchCondition.PartNumber = TrimEnd(this.txtPartNumber.Text);
		oSearchCondition.LinkId = TrimEnd(this.txtLinkId.Text);
		oSearchCondition.ProductMaker = TrimEnd(this.txtProductMaker.Text);
		oSearchCondition.PublishStartDateFrom = this.txtPublishStartDateFrom.Text;
		oSearchCondition.PublishStartDateTo = this.txtPublishStartDateTo.Text;
		oSearchCondition.PublishEndDateFrom = this.txtPublishEndDateFrom.Text;
		oSearchCondition.PublishEndDateTo = this.txtPublishEndDateTo.Text;
		oSearchCondition.SpecialChargePointFrom = this.txtSpecialChargePointFrom.Text;
		oSearchCondition.SpecialChargePointTo = this.txtSpecialChargePointTo.Text;
		oSearchCondition.GrossPriceFrom = this.txtGrossPriceFrom.Text;
		oSearchCondition.GrossPriceTo = this.txtGrossPriceTo.Text;
		oSearchCondition.ChargePointFrom = this.txtChargePointFrom.Text;
		oSearchCondition.ChargePointTo = this.txtChargePointTo.Text;
		oSearchCondition.ReleaseDateFrom = this.txtReleaseDateFrom.Text;
		oSearchCondition.ReleaseDateTo = this.txtReleaseDateTo.Text;
		oSearchCondition.PublishFlag = this.rdoPublishFlag.SelectedValue;
		oSearchCondition.DelFlag = this.chkDelete.Checked;
		oSearchCondition.ProductKeyword = TrimEnd(this.txtProductKeyWord.Text);
		oSearchCondition.HandleName = TrimEnd(this.txtHandleName.Text);
		oSearchCondition.GenreList.Add(this.lstGenreCd1.SelectedValue);
		oSearchCondition.GenreList.Add(this.lstGenreCd2.SelectedValue);
		oSearchCondition.GenreList.Add(this.lstGenreCd3.SelectedValue);
		oSearchCondition.PickupId = this.lstProductPickupManage.SelectedValue;
		oSearchCondition.RoyaltyRate = this.txtRoyaltyRate.Text;
		oSearchCondition.NotNewFlag = this.rdoNotNewFlag.SelectedValue;
		oSearchCondition.LoginId = TrimEnd(this.txtLoginId.Text);
		oSearchCondition.BuyCountFrom = TrimEnd(this.txtBuyCountFrom.Text);
		oSearchCondition.BuyCountTo = TrimEnd(this.txtBuyCountTo.Text);
		oSearchCondition.FavoriteMeCountFrom = TrimEnd(this.txtFavoriteMeCountFrom.Text);
		oSearchCondition.FavoriteMeCountTo = TrimEnd(this.txtFavoriteMeCountTo.Text);
		oSearchCondition.NoGenreFlag = this.chkNoGenre.Checked;

		//Auction
		oSearchCondition.AuctionStatus = this.rdoAuctionStatus.SelectedValue;
		oSearchCondition.AuctionStartDateFrom = this.txtAuctionStartDateFrom.Text;
		oSearchCondition.AuctionStartDateTo = this.txtAuctionStartDateTo.Text;
		oSearchCondition.AuctionEndDateFrom = this.txtAuctionEndDateFrom.Text;
		oSearchCondition.AuctionEndDateTo = this.txtAuctionEndDateTo.Text;
		oSearchCondition.BlindEndDateFrom = this.txtBlindEndDateFrom.Text;
		oSearchCondition.BlindEndDateTo = this.txtBlindEndDateTo.Text;
		oSearchCondition.AuctionStartHourFrom = this.txtAuctionStartHourFrom.Text;
		oSearchCondition.AuctionStartHourTo = this.txtAuctionStartHourTo.Text;
		oSearchCondition.AuctionEndHourFrom = this.txtAuctionEndHourFrom.Text;
		oSearchCondition.AuctionEndHourTo = this.txtAuctionEndHourTo.Text;
		oSearchCondition.BlindEndHourFrom = this.txtBlindEndHourFrom.Text;
		oSearchCondition.BlindEndHourTo = this.txtBlindEndHourTo.Text;
		oSearchCondition.ReserveAmtFrom = this.txtReserveAmtFrom.Text;
		oSearchCondition.ReserveAmtTo = this.txtReserveAmtTo.Text;
		oSearchCondition.MinimumBidAmtFrom = this.txtMinimumBidAmtFrom.Text;
		oSearchCondition.MinimumBidAmtTo = this.txtMinimumBidAmtTo.Text;
		oSearchCondition.EntryPointFrom = this.txtEntryPointFrom.Text;
		oSearchCondition.EntryPointTo = this.txtEntryPointTo.Text;
		oSearchCondition.BlindEntryPointFrom = this.txtBlindEntryPointFrom.Text;
		oSearchCondition.BlindEntryPointTo = this.txtBlindEntryPointTo.Text;
		oSearchCondition.BidPointFrom = this.txtBidPointFrom.Text;
		oSearchCondition.BidPointTo = this.txtBidPointTo.Text;
		oSearchCondition.BlindBidPointFrom = this.txtBlindBidPointFrom.Text;
		oSearchCondition.BlindBidPointTo = this.txtBlindBidPointTo.Text;
		oSearchCondition.AutoExtensionFlag = this.rdoAutoExtension.SelectedValue;

		e.InputParameters[0] = oSearchCondition;
		this.SearchCondition = oSearchCondition;
	}

	protected void dsProduct_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void rdoAuctionStatus_DataBound(object sender, EventArgs e) {
		this.rdoAuctionStatus.Items.Insert(0, new ListItem("全て", string.Empty));
	}

	private void GetList() {
		this.pnlList.Visible = true;
		this.grdProduct.PageIndex = 0;
		this.grdProduct.DataSourceID = "dsProduct";
		this.grdProduct.DataBind();
		this.pnlCount.DataBind();
	}

	private void InitPage() {
		this.pnlList.Visible = false;

		if (this.IsRestorable) {
			this.RestoreSearchCondition();
			return;
		}

		this.lstSiteCd.DataBind();
		this.lstProductType.DataBind();
		this.lstProductAgent.DataBind();
		this.lstProductAgent.SelectedIndex = 0;
		this.lstProductPickupManage.DataBind();
		this.lstGenreCd1.DataBind();
		this.lstGenreCd2.DataBind();
		this.lstGenreCd3.DataBind();
		this.rdoAuctionStatus.DataBind();
		this.rdoAuctionStatus.SelectedIndex = 0;
		this.SetupConditionVisible();
		
		this.grdProduct.DataSourceID = string.Empty;
		this.grdProduct.DataBind();

		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals(string.Empty)) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		this.txtProductNm.Text = string.Empty;
		this.txtProductSeries.Text = string.Empty;
		this.txtCast.Text = string.Empty;
		this.txtProductId.Text = string.Empty;
		this.txtPartNumber.Text = string.Empty;
		this.txtLinkId.Text = string.Empty;
		this.txtProductMaker.Text = string.Empty;
		this.txtPublishStartDateFrom.Text = string.Empty;
		this.txtPublishStartDateTo.Text = string.Empty;
		this.txtPublishEndDateFrom.Text = string.Empty;
		this.txtPublishEndDateTo.Text = string.Empty;
		this.txtSpecialChargePointFrom.Text = string.Empty;
		this.txtSpecialChargePointTo.Text = string.Empty;
		this.txtGrossPriceFrom.Text = string.Empty;
		this.txtGrossPriceTo.Text = string.Empty;
		this.txtChargePointFrom.Text = string.Empty;
		this.txtChargePointTo.Text = string.Empty;
		this.txtReleaseDateFrom.Text = string.Empty;
		this.txtReleaseDateTo.Text = string.Empty;
		this.rdoPublishFlag.SelectedIndex = 0;
		this.chkDelete.Checked = false;
		this.txtProductKeyWord.Text = string.Empty;
		this.txtHandleName.Text = string.Empty;
		this.txtRoyaltyRate.Text = string.Empty;
		this.txtLoginId.Text = string.Empty;
		this.rdoNotNewFlag.SelectedValue = ViCommConst.FLAG_OFF_STR;
		this.txtBuyCountFrom.Text = string.Empty;		
		this.txtBuyCountTo.Text = string.Empty;		
		this.txtFavoriteMeCountFrom.Text = string.Empty;
		this.txtFavoriteMeCountTo.Text = string.Empty;
		this.chkNoGenre.Checked = false;

		this.rdoAuctionStatus.SelectedValue = string.Empty;
		this.txtAuctionStartDateFrom.Text = string.Empty;
		this.txtAuctionStartDateTo.Text = string.Empty;
		this.txtAuctionEndDateFrom.Text = string.Empty;
		this.txtAuctionEndDateTo.Text = string.Empty;
		this.txtBlindEndDateFrom.Text = string.Empty;
		this.txtBlindEndDateTo.Text = string.Empty;
		this.txtAuctionStartHourFrom.Text = string.Empty;
		this.txtAuctionStartHourTo.Text = string.Empty;
		this.txtAuctionEndHourFrom.Text = string.Empty;
		this.txtAuctionEndHourTo.Text = string.Empty;
		this.txtBlindEndHourFrom.Text = string.Empty;
		this.txtBlindEndHourTo.Text = string.Empty;
		this.txtReserveAmtFrom.Text = string.Empty;
		this.txtReserveAmtTo.Text = string.Empty;
		this.txtMinimumBidAmtFrom.Text = string.Empty;
		this.txtMinimumBidAmtTo.Text = string.Empty;
		this.txtEntryPointFrom.Text = string.Empty;
		this.txtEntryPointTo.Text = string.Empty;
		this.txtBlindEntryPointFrom.Text = string.Empty;
		this.txtBlindEntryPointTo.Text = string.Empty;
		this.txtBidPointFrom.Text = string.Empty;
		this.txtBidPointTo.Text = string.Empty;
		this.txtBlindBidPointFrom.Text = string.Empty;
		this.txtBlindBidPointTo.Text = string.Empty;
		this.rdoAutoExtension.SelectedValue = string.Empty;

		this.dsProduct = null;
		this.recCount = "0";
	}

	protected string GetPublishFlagNm(object pPublishFlag) {
		switch (iBridUtil.GetStringValue(pPublishFlag)) {
			case ViCommConst.FLAG_ON_STR:
				return "公開";
			case ViCommConst.FLAG_OFF_STR:
				return "非公開";
			case ViCommConst.FLAG_WITHOUT_STR:
				return "未設定";
			default:
				return string.Empty;
		}
	}

	protected Color GetPublishFlagNmColor(object pPublishFlag) {
		switch (iBridUtil.GetStringValue(pPublishFlag)) {
			case ViCommConst.FLAG_ON_STR:
			case ViCommConst.FLAG_OFF_STR:
				return Color.Black;
			case ViCommConst.FLAG_WITHOUT_STR:
				return Color.Red;
			default:
				return Color.Black;
		}
	}

	protected bool GetProductPicVisible(object pObjSeq) {
		return !string.IsNullOrEmpty(iBridUtil.GetStringValue(pObjSeq));
	}

	protected string GenerateProductViewUrl(object pProductId) {
		return string.Format("ProductView.aspx?product_id={0}", pProductId);
	}

	protected string GenerateCastViewUrl(object pLoginId) {
		return string.Format("~/Cast/CastView.aspx?loginid={0}", pLoginId);
		//using (CastCharacter oCastCharacter = new CastCharacter()) {
		//    using (DataSet oDataSet = oCastCharacter.GetOneByUserSeq(this.lstSiteCd.SelectedValue, iBridUtil.GetStringValue(pUserSeq), iBridUtil.GetStringValue(pUserCharNo))) {
		//        if (oDataSet != null && oDataSet.Tables[0].Rows > 0) {
		//            DataRow oDataRow = oDataSet.Tables[0].Rows[0];
		//            return string.Format("~/Cast/CastView.aspx?loginid={0}", oDataRow["LOGIN_ID"]);
		//        }
		//    }
		//}
		//return string.Empty;
	}

	private void SetupCondition() {
		if (!string.IsNullOrEmpty(this.txtPublishStartDateFrom.Text) || !string.IsNullOrEmpty(this.txtPublishStartDateTo.Text)) {
			if (string.IsNullOrEmpty(this.txtPublishStartDateFrom.Text)) {
				this.txtPublishStartDateFrom.Text = this.txtPublishStartDateTo.Text;
			} else if (string.IsNullOrEmpty(this.txtPublishStartDateTo.Text)) {
				this.txtPublishStartDateTo.Text = this.txtPublishStartDateFrom.Text;
			}
		}

		if (!string.IsNullOrEmpty(this.txtPublishEndDateFrom.Text) || !string.IsNullOrEmpty(this.txtPublishEndDateTo.Text)) {
			if (string.IsNullOrEmpty(this.txtPublishEndDateFrom.Text)) {
				this.txtPublishEndDateFrom.Text = this.txtPublishEndDateTo.Text;
			} else if (string.IsNullOrEmpty(this.txtPublishEndDateTo.Text)) {
				this.txtPublishEndDateTo.Text = this.txtPublishEndDateFrom.Text;
			}
		}

		if (!string.IsNullOrEmpty(this.txtReleaseDateFrom.Text) || !string.IsNullOrEmpty(this.txtReleaseDateTo.Text)) {
			if (string.IsNullOrEmpty(this.txtReleaseDateFrom.Text)) {
				this.txtReleaseDateFrom.Text = this.txtReleaseDateTo.Text;
			} else if (string.IsNullOrEmpty(this.txtReleaseDateTo.Text)) {
				this.txtReleaseDateTo.Text = this.txtReleaseDateFrom.Text;
			}
		}

		if (!string.IsNullOrEmpty(this.txtSpecialChargePointFrom.Text) || !string.IsNullOrEmpty(this.txtSpecialChargePointTo.Text)) {
			if (string.IsNullOrEmpty(this.txtSpecialChargePointFrom.Text)) {
				this.txtSpecialChargePointFrom.Text = this.txtSpecialChargePointTo.Text;
			} else if (string.IsNullOrEmpty(this.txtSpecialChargePointTo.Text)) {
				this.txtSpecialChargePointTo.Text = this.txtSpecialChargePointFrom.Text;
			}
		}

		if (!string.IsNullOrEmpty(this.txtGrossPriceFrom.Text) || !string.IsNullOrEmpty(this.txtGrossPriceTo.Text)) {
			if (string.IsNullOrEmpty(this.txtGrossPriceFrom.Text)) {
				this.txtGrossPriceFrom.Text = this.txtGrossPriceTo.Text;
			} else if (string.IsNullOrEmpty(this.txtGrossPriceTo.Text)) {
				this.txtGrossPriceTo.Text = this.txtGrossPriceFrom.Text;
			}
		}

		if (!string.IsNullOrEmpty(this.txtChargePointFrom.Text) || !string.IsNullOrEmpty(this.txtChargePointTo.Text)) {
			if (string.IsNullOrEmpty(this.txtChargePointFrom.Text)) {
				this.txtChargePointFrom.Text = this.txtChargePointTo.Text;
			} else if (string.IsNullOrEmpty(this.txtChargePointTo.Text)) {
				this.txtChargePointTo.Text = this.txtChargePointFrom.Text;
			}
		}

		if (!string.IsNullOrEmpty(this.txtBuyCountFrom.Text) || !string.IsNullOrEmpty(this.txtBuyCountTo.Text)) {
			if (string.IsNullOrEmpty(this.txtBuyCountFrom.Text)) {
				this.txtBuyCountFrom.Text = this.txtBuyCountTo.Text;
			} else if (string.IsNullOrEmpty(this.txtBuyCountTo.Text)) {
				this.txtBuyCountTo.Text = this.txtBuyCountFrom.Text;
			}
		}

		if (!string.IsNullOrEmpty(this.txtFavoriteMeCountFrom.Text) || !string.IsNullOrEmpty(this.txtFavoriteMeCountTo.Text)) {
			if (string.IsNullOrEmpty(this.txtFavoriteMeCountFrom.Text)) {
				this.txtFavoriteMeCountFrom.Text = this.txtFavoriteMeCountTo.Text;
			} else if (string.IsNullOrEmpty(this.txtFavoriteMeCountTo.Text)) {
				this.txtFavoriteMeCountTo.Text = this.txtFavoriteMeCountFrom.Text;
			}
		}

		//Auction
		if (!string.IsNullOrEmpty(this.txtAuctionStartDateFrom.Text) || !string.IsNullOrEmpty(this.txtAuctionStartDateTo.Text)) {
			if (string.IsNullOrEmpty(this.txtAuctionStartDateFrom.Text)) {
				this.txtAuctionStartDateFrom.Text = this.txtAuctionStartDateTo.Text;
			} else if (string.IsNullOrEmpty(this.txtAuctionStartDateTo.Text)) {
				this.txtAuctionStartDateTo.Text = this.txtAuctionStartDateFrom.Text;
			}
		}

		if (!string.IsNullOrEmpty(this.txtBlindEndDateFrom.Text) || !string.IsNullOrEmpty(this.txtBlindEndDateTo.Text)) {
			if (string.IsNullOrEmpty(this.txtBlindEndDateFrom.Text)) {
				this.txtBlindEndDateFrom.Text = this.txtBlindEndDateTo.Text;
			} else if (string.IsNullOrEmpty(this.txtBlindEndDateTo.Text)) {
				this.txtBlindEndDateTo.Text = this.txtBlindEndDateFrom.Text;
			}
		}

		if (!string.IsNullOrEmpty(this.txtAuctionEndDateFrom.Text) || !string.IsNullOrEmpty(this.txtAuctionEndDateTo.Text)) {
			if (string.IsNullOrEmpty(this.txtAuctionEndDateFrom.Text)) {
				this.txtAuctionEndDateFrom.Text = this.txtAuctionEndDateTo.Text;
			} else if (string.IsNullOrEmpty(this.txtAuctionEndDateTo.Text)) {
				this.txtAuctionEndDateTo.Text = this.txtAuctionEndDateFrom.Text;
			}
		}

		if (!string.IsNullOrEmpty(this.txtReserveAmtFrom.Text) || !string.IsNullOrEmpty(this.txtReserveAmtTo.Text)) {
			if (string.IsNullOrEmpty(this.txtReserveAmtFrom.Text)) {
				this.txtReserveAmtFrom.Text = this.txtReserveAmtTo.Text;
			} else if (string.IsNullOrEmpty(this.txtReserveAmtTo.Text)) {
				this.txtReserveAmtTo.Text = this.txtReserveAmtFrom.Text;
			}
		}

		if (!string.IsNullOrEmpty(this.txtEntryPointFrom.Text) || !string.IsNullOrEmpty(this.txtEntryPointTo.Text)) {
			if (string.IsNullOrEmpty(this.txtEntryPointFrom.Text)) {
				this.txtEntryPointFrom.Text = this.txtEntryPointTo.Text;
			} else if (string.IsNullOrEmpty(this.txtEntryPointTo.Text)) {
				this.txtEntryPointTo.Text = this.txtEntryPointFrom.Text;
			}
		}

		if (!string.IsNullOrEmpty(this.txtBlindEntryPointFrom.Text) || !string.IsNullOrEmpty(this.txtBlindEntryPointTo.Text)) {
			if (string.IsNullOrEmpty(this.txtBlindEntryPointFrom.Text)) {
				this.txtBlindEntryPointFrom.Text = this.txtBlindEntryPointTo.Text;
			} else if (string.IsNullOrEmpty(this.txtBlindEntryPointTo.Text)) {
				this.txtBlindEntryPointTo.Text = this.txtBlindEntryPointFrom.Text;
			}
		}

		if (!string.IsNullOrEmpty(this.txtMinimumBidAmtFrom.Text) || !string.IsNullOrEmpty(this.txtMinimumBidAmtTo.Text)) {
			if (string.IsNullOrEmpty(this.txtMinimumBidAmtFrom.Text)) {
				this.txtMinimumBidAmtFrom.Text = this.txtMinimumBidAmtTo.Text;
			} else if (string.IsNullOrEmpty(this.txtMinimumBidAmtTo.Text)) {
				this.txtMinimumBidAmtTo.Text = this.txtMinimumBidAmtFrom.Text;
			}
		}

		if (!string.IsNullOrEmpty(this.txtBidPointFrom.Text) || !string.IsNullOrEmpty(this.txtBidPointTo.Text)) {
			if (string.IsNullOrEmpty(this.txtBidPointFrom.Text)) {
				this.txtBidPointFrom.Text = this.txtBidPointTo.Text;
			} else if (string.IsNullOrEmpty(this.txtBidPointTo.Text)) {
				this.txtBidPointTo.Text = this.txtBidPointFrom.Text;
			}
		}

		if (!string.IsNullOrEmpty(this.txtBlindBidPointFrom.Text) || !string.IsNullOrEmpty(this.txtBlindBidPointTo.Text)) {
			if (string.IsNullOrEmpty(this.txtBlindBidPointFrom.Text)) {
				this.txtBlindBidPointFrom.Text = this.txtBlindBidPointTo.Text;
			} else if (string.IsNullOrEmpty(this.txtBlindBidPointTo.Text)) {
				this.txtBlindBidPointTo.Text = this.txtBlindBidPointFrom.Text;
			}
		}

	}

	protected void dsProductGenreCategory_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = this.lstSiteCd.SelectedValue;
		e.InputParameters["pProductType"] = this.lstProductType.SelectedValue;
	}

	protected void lstSiteCd_SelectedIndexChanged(object sender, EventArgs e) {
		this.lstGenreCd1.DataBind();
		this.lstGenreCd2.DataBind();
		this.lstGenreCd3.DataBind();
		this.lstProductPickupManage.DataBind();
	}

	protected void lstProductType_SelectedIndexChanged(object sender, EventArgs e) {
		this.lstGenreCd1.DataBind();
		this.lstGenreCd2.DataBind();
		this.lstGenreCd3.DataBind();
		this.lstProductPickupManage.DataBind();

		this.SetupConditionVisible();
	}

	private void SetupConditionVisible() {
		bool bIsAuction = ViCommConst.ProductType.IsAuction(this.lstProductType.SelectedValue);
		this.plcProductBasicInfo.Visible = !bIsAuction;
		this.plcProductAuctionInfo.Visible = bIsAuction;
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void lstProductPickupManage_DataBound(object sender, EventArgs e) {
		this.lstProductPickupManage.Items.Insert(0, new ListItem(string.Empty, string.Empty));
		this.lstProductPickupManage.SelectedIndex = 0;
	}

	protected string GetPickupMask(object pPickupMask) {
		int iMask = Convert.ToInt32(pPickupMask);
		if (iMask == 0) {
			return string.Empty;
		}

		string sLabel = string.Empty;
		int iValue = 1;
		for (int i = 1; i < 10; i++) {
			if ((iMask & iValue) > 0) {
				sLabel = sLabel + i.ToString("d2") + "&nbsp;";
			}
			iValue = iValue << 1;
		}
		if (!sLabel.Equals(string.Empty)) {
			sLabel = "<BR>Pickup " + sLabel;
		}
		return sLabel;
	}

	protected void grdProduct_RowDataBound(object sender, GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.Pager) {
			return;
		}

		if (e.Row.RowType == DataControlRowType.DataRow) {
			if (!Convert.ToString(DataBinder.Eval(e.Row.DataItem, "PICKUP_MASK")).Equals("0")) {
				e.Row.Cells[1].BackColor = Color.Teal;
				e.Row.Cells[1].ForeColor = Color.White;
			}
		}
	}

	protected void grdProduct_DataBound(object sender, EventArgs e) {
		if (this.grdProduct.Rows.Count > 0) {
			if (ViCommConst.ProductType.IsAuction(this.lstProductType.SelectedValue)) {
				this.grdProduct.Columns[INDEX_AUCTION_START_DATE].Visible = true;
				this.grdProduct.Columns[INDEX_GROSS_PRICE].Visible = false;
				this.grdProduct.Columns[INDEX_CHARGE_POINT].Visible = false;
				this.grdProduct.Columns[INDEX_MAX_BID_AMT].Visible = true;
				this.grdProduct.Columns[INDEX_AUCTION_STATUS].Visible = true;
				this.grdProduct.Columns[INDEX_BUY_COUNT].Visible = false;
				//this.grdProduct.Columns[INDEX_PRODUCT_NM].ItemStyle.Width = 250;
			} else {
				this.grdProduct.Columns[INDEX_AUCTION_START_DATE].Visible = false;
				this.grdProduct.Columns[INDEX_GROSS_PRICE].Visible = true;
				this.grdProduct.Columns[INDEX_CHARGE_POINT].Visible = true;
				this.grdProduct.Columns[INDEX_MAX_BID_AMT].Visible = false;
				this.grdProduct.Columns[INDEX_AUCTION_STATUS].Visible = false;
				this.grdProduct.Columns[INDEX_BUY_COUNT].Visible = true;
				//this.grdProduct.Columns[INDEX_PRODUCT_NM].ItemStyle.Width = 300;
			}
		}
	}

	protected void lstGenreCd_DataBound(object sender, EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList == null) {
			return;
		}
		oDropDownList.Items.Insert(0, new ListItem(string.Empty, string.Empty));
	}

	private void RestoreSearchCondition() {
		if (this.SearchCondition != null) {
			this.lstSiteCd.DataBind();
			this.lstProductAgent.DataBind();
			this.lstProductType.DataBind();

			this.lstSiteCd.SelectedValue = this.SearchCondition.SiteCd;
			this.lstProductAgent.SelectedValue = this.SearchCondition.ProductAgentCd;
			this.lstProductType.SelectedValue = this.SearchCondition.Code;

			this.SetupConditionVisible();

			this.lstGenreCd1.DataBind();
			this.lstGenreCd2.DataBind();
			this.lstGenreCd3.DataBind();
			this.lstProductPickupManage.DataBind();
			this.rdoAuctionStatus.DataBind();

			this.txtProductNm.Text = this.SearchCondition.ProductName;
			this.txtProductSeries.Text = this.SearchCondition.ProductSeries;
			this.txtCast.Text = this.SearchCondition.Cast;
			this.txtProductId.Text = this.SearchCondition.ProductId;
			this.txtPartNumber.Text = this.SearchCondition.PartNumber;
			this.txtLinkId.Text = this.SearchCondition.LinkId;
			this.txtProductMaker.Text = this.SearchCondition.ProductMaker;
			this.txtPublishStartDateFrom.Text = this.SearchCondition.PublishStartDateFrom;
			this.txtPublishStartDateTo.Text = this.SearchCondition.PublishStartDateTo;
			this.txtPublishEndDateFrom.Text = this.SearchCondition.PublishEndDateFrom;
			this.txtPublishEndDateTo.Text = this.SearchCondition.PublishEndDateTo;
			this.txtSpecialChargePointFrom.Text = this.SearchCondition.SpecialChargePointFrom;
			this.txtSpecialChargePointTo.Text = this.SearchCondition.SpecialChargePointTo;
			this.txtGrossPriceFrom.Text = this.SearchCondition.GrossPriceFrom;
			this.txtGrossPriceTo.Text = this.SearchCondition.GrossPriceTo;
			this.txtChargePointFrom.Text = this.SearchCondition.ChargePointFrom;
			this.txtChargePointTo.Text = this.SearchCondition.ChargePointTo;
			this.txtReleaseDateFrom.Text = this.SearchCondition.ReleaseDateFrom;
			this.txtReleaseDateTo.Text = this.SearchCondition.ReleaseDateTo;
			this.rdoPublishFlag.SelectedValue = this.SearchCondition.PublishFlag;
			this.chkDelete.Checked = this.SearchCondition.DelFlag;
			this.txtProductKeyWord.Text = this.SearchCondition.ProductKeyword;
			this.txtHandleName.Text = this.SearchCondition.HandleName;
			this.lstProductPickupManage.DataBind();
			this.lstProductPickupManage.SelectedValue = this.SearchCondition.PickupId;
			this.lstGenreCd1.SelectedValue = this.SearchCondition.GenreList[0];
			this.lstGenreCd2.SelectedValue = this.SearchCondition.GenreList[1];
			this.lstGenreCd3.SelectedValue = this.SearchCondition.GenreList[2];
			this.txtRoyaltyRate.Text = this.SearchCondition.RoyaltyRate;
			this.rdoNotNewFlag.SelectedValue = this.SearchCondition.NotNewFlag;
			this.txtLoginId.Text = this.SearchCondition.LoginId;
			this.txtBuyCountFrom.Text = this.SearchCondition.BuyCountFrom;
			this.txtBuyCountTo.Text = this.SearchCondition.BuyCountTo;
			this.txtFavoriteMeCountFrom.Text = this.SearchCondition.FavoriteMeCountFrom;
			this.txtFavoriteMeCountTo.Text = this.SearchCondition.FavoriteMeCountTo;
			this.chkNoGenre.Checked = this.SearchCondition.NoGenreFlag;

			//Auction
			this.rdoAuctionStatus.SelectedValue = this.SearchCondition.AuctionStatus;
			this.txtAuctionStartDateFrom.Text = this.SearchCondition.AuctionStartDateFrom;
			this.txtAuctionStartDateTo.Text = this.SearchCondition.AuctionStartDateTo;
			this.txtAuctionEndDateFrom.Text = this.SearchCondition.AuctionEndDateFrom;
			this.txtAuctionEndDateTo.Text = this.SearchCondition.AuctionEndDateTo;
			this.txtBlindEndDateFrom.Text = this.SearchCondition.BlindEndDateFrom;
			this.txtBlindEndDateTo.Text = this.SearchCondition.BlindEndDateTo;
			this.txtAuctionStartHourFrom.Text = this.SearchCondition.AuctionStartHourFrom;
			this.txtAuctionStartHourTo.Text = this.SearchCondition.AuctionStartHourTo;
			this.txtAuctionEndHourFrom.Text = this.SearchCondition.AuctionEndHourFrom;
			this.txtAuctionEndHourTo.Text = this.SearchCondition.AuctionEndHourTo;
			this.txtBlindEndHourFrom.Text = this.SearchCondition.BlindEndHourFrom;
			this.txtBlindEndHourTo.Text = this.SearchCondition.BlindEndHourTo;
			this.txtReserveAmtFrom.Text = this.SearchCondition.ReserveAmtFrom;
			this.txtReserveAmtTo.Text = this.SearchCondition.ReserveAmtTo;
			this.txtMinimumBidAmtFrom.Text = this.SearchCondition.MinimumBidAmtFrom;
			this.txtMinimumBidAmtTo.Text = this.SearchCondition.MinimumBidAmtTo;
			this.txtEntryPointFrom.Text = this.SearchCondition.EntryPointFrom;
			this.txtEntryPointTo.Text = this.SearchCondition.EntryPointTo;
			this.txtBlindEntryPointFrom.Text = this.SearchCondition.BlindEntryPointFrom;
			this.txtBlindEntryPointTo.Text = this.SearchCondition.BlindEntryPointTo;
			this.txtBidPointFrom.Text = this.SearchCondition.BidPointFrom;
			this.txtBidPointTo.Text = this.SearchCondition.BidPointTo;
			this.txtBlindBidPointFrom.Text = this.SearchCondition.BlindBidPointFrom;
			this.txtBlindBidPointTo.Text = this.SearchCondition.BlindBidPointTo;
			this.rdoAutoExtension.SelectedValue = this.SearchCondition.AutoExtensionFlag;
		}
	}
	protected string TrimEnd(string pTargetStr) {
		char[] trims = { ' ' };
		string sRetStr = pTargetStr.TrimEnd(trims);

		return sRetStr;
	}
}
