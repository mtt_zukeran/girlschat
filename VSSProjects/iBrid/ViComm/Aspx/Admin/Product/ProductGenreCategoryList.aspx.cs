﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;

public partial class Product_ProductGenreCategoryList : System.Web.UI.Page {
    private string recCount = string.Empty;

	protected string Rowid {
		get { return iBridUtil.GetStringValue(this.ViewState["Rowid"]); }
		private set { this.ViewState["Rowid"] = value; }
	}
	protected string RevisionNo {
		get { return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]); }
		private set { this.ViewState["RevisionNo"] = value; }
	}
	
	protected void Page_Load(object sender, EventArgs e) {
        if (!this.IsPostBack) {
            this.InitPage();
        }
    }
    protected void btnListSeek_Click(object sender, EventArgs e) {
        this.GetList();
    }
	protected void btnRegist_Click(object sender, EventArgs e) {
		this.SyncLists();
		this.txtProductGenreCategoryCd.Text = string.Empty;
		this.GetData(string.Empty);
		this.pnlMainte.Visible = true;
		this.txtProductGenreCategoryCd.Enabled = true;
	}
	protected void btnUpdate_Click(object sender, EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.UpdateData(false);
	}
	protected void btnDelete_Click(object sender, EventArgs e) {
		this.UpdateData(true);
	}
	protected void btnCancel_Click(object sender, EventArgs e) {
		this.InitPage();
	}
	protected void lnkGenreCategory_Command(object sender, CommandEventArgs e) {
		this.SyncLists();
		this.pnlMainte.Visible = true;
		this.txtProductGenreCategoryCd.Enabled = false;
		this.GetData(e.CommandArgument);
	}
    protected void dsProductGenreCategory_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
        e.InputParameters["pSiteCd"] = this.lstSeekSiteCd.SelectedValue;
        e.InputParameters["pProductType"] = this.lstSeekProductType.SelectedValue;
    }
    protected void dsProductGenreCategory_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
        if (e.ReturnValue != null) {
            recCount = e.ReturnValue.ToString();
        }
    }

    private void InitPage() {
		this.pnlMainte.Visible = false;
        this.lstSeekSiteCd.DataBind();
        this.lstSeekProductType.DataBind();

        if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals(string.Empty)) {
            this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
        }

        this.GetList();
    }

    protected string GetRecCount() {
        return this.recCount;
    }

    private void GetList() {
        this.grdProductGenreCategory.PageSize = 999;
        this.grdProductGenreCategory.DataSourceID = "dsProductGenreCategory";
        this.grdProductGenreCategory.DataBind();
        this.pnlCount.DataBind();
    }

	private void GetData(object pProductGenreCategoryCd) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("PRODUCT_GENRE_CATEGORY_GET");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.lstSeekSiteCd.SelectedValue);
			oDbSession.ProcedureBothParm("pPRODUCT_GENRE_CATEGORY_CD", DbSession.DbType.VARCHAR2, pProductGenreCategoryCd);
			oDbSession.ProcedureOutParm("pPRODUCT_GENRE_CATEGORY_NM", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureInParm("pPRODUCT_TYPE", DbSession.DbType.VARCHAR2, this.lstSeekProductType.SelectedValue);
			oDbSession.ProcedureOutParm("pROWID", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREVISION_NO", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pRECORD_COUNT", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.Rowid = oDbSession.GetStringValue("pROWID");
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
			this.txtProductGenreCategoryCd.Text = oDbSession.GetStringValue("pPRODUCT_GENRE_CATEGORY_CD");
			this.txtProductGenreCategoryNm.Text = oDbSession.GetStringValue("pPRODUCT_GENRE_CATEGORY_NM");
		}
	}

	private void UpdateData(bool pDelFlag) {
		int iDelFlag = 0;
		if (pDelFlag) {
			iDelFlag = 1;
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("PRODUCT_GENRE_CATEGORY_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pPRODUCT_GENRE_CATEGORY_CD", DbSession.DbType.VARCHAR2, this.txtProductGenreCategoryCd.Text);
			oDbSession.ProcedureInParm("pPRODUCT_GENRE_CATEGORY_NM", DbSession.DbType.VARCHAR2, this.txtProductGenreCategoryNm.Text);
			oDbSession.ProcedureInParm("pPRODUCT_TYPE", DbSession.DbType.VARCHAR2, this.lstProductType.SelectedValue);
			oDbSession.ProcedureInParm("pROWID", DbSession.DbType.VARCHAR2, this.Rowid);
			oDbSession.ProcedureInParm("pREVISION_NO", DbSession.DbType.NUMBER, this.RevisionNo);
			oDbSession.ProcedureInParm("pDEL_FLAG", DbSession.DbType.NUMBER, iDelFlag);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		int iSiteCdIndex = this.lstSiteCd.SelectedIndex;
		int iProductTypeIndex = this.lstProductType.SelectedIndex;
		this.InitPage();
		this.lstSeekSiteCd.SelectedIndex = iSiteCdIndex;
		this.lstSeekProductType.SelectedIndex = iProductTypeIndex;
		this.GetList();
	}

	private void SyncLists() {
		this.lstSiteCd.SelectedValue = this.lstSeekSiteCd.SelectedValue;
		this.lstProductType.SelectedValue = this.lstSeekProductType.SelectedValue;
	}
}
