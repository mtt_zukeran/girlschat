﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Product_ProductGenreList : System.Web.UI.Page {
    private string recCount = string.Empty;

	protected string Rowid {
		get { return iBridUtil.GetStringValue(this.ViewState["Rowid"]); }
		private set { this.ViewState["Rowid"] = value; }
	}
	protected string RevisionNo {
		get { return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]); }
		private set { this.ViewState["RevisionNo"] = value; }
	}
	
	protected void Page_Load(object sender, EventArgs e) {
        if (!this.IsPostBack) {
            this.InitPage();

			string sSiteCd = iBridUtil.GetStringValue(this.Request.QueryString["site_cd"]);
			string sProductType = iBridUtil.GetStringValue(this.Request.QueryString["product_type"]);
			string sProductGenreCategoryCd = iBridUtil.GetStringValue(this.Request.QueryString["product_genre_category_cd"]);
			string sProductGenreCd = iBridUtil.GetStringValue(this.Request.QueryString["product_genre_cd"]);

			if (!string.IsNullOrEmpty(sSiteCd) &&
				!string.IsNullOrEmpty(sProductType) &&
				!string.IsNullOrEmpty(sProductGenreCategoryCd) &&
				!string.IsNullOrEmpty(sProductGenreCd)) {

				this.lstSeekProductType.SelectedValue = sProductType;
				this.lstSeekSiteCd.SelectedValue = sSiteCd;
				this.SyncLists();

				this.GetData(sProductGenreCategoryCd, sProductGenreCd);
				this.txtProductGenreCd.Enabled = false;
				this.lstProductGenreCagetory.Enabled = false;
				this.lstProductGenreCagetory.DataBind();
				this.pnlMainte.Visible = true;
			}
        }
		this.GetList();
    }
    protected void btnListSeek_Click(object sender, EventArgs e) {
        this.GetList();
    }
	protected void btnRegist_Click(object sender, EventArgs e) {
		this.SyncLists();
		this.lstProductGenreCagetory.DataBind();
		this.lstProductGenreCagetory.SelectedValue = string.Empty;
		this.txtProductGenreCd.Text = string.Empty;
		this.txtProductGenreNm.Text = string.Empty;
		this.txtPriority.Text = string.Empty;
		//this.GetData(string.Empty, string.Empty);
		this.GetList();

		this.txtProductGenreCd.Enabled = true;
		this.lstProductGenreCagetory.Enabled = true;
		this.pnlMainte.Visible = true;
	}
	protected void btnUpdate_Click(object sender, EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.UpdateData(false);
	}
	protected void btnDelete_Click(object sender, EventArgs e) {
		this.UpdateData(true);
	}
	protected void btnCancel_Click(object sender, EventArgs e) {
		this.InitPage();
		this.GetList();
	}
    protected void dsProductGenre_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
        e.InputParameters["pSiteCd"] = this.lstSeekSiteCd.SelectedValue;
        e.InputParameters["pProductType"] = this.lstSeekProductType.SelectedValue;
    }
    protected void dsProductGenre_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
        if (e.ReturnValue != null) {
            recCount = e.ReturnValue.ToString();
        }
    }
	protected void grdProductGenre_DataBound(object sender, EventArgs e) {
		this.JoinCells(this.grdProductGenre, 0);
	}
	protected void lstProductGenreCategory_DataBound(object sender, EventArgs e) {
		this.lstProductGenreCagetory.Items.Insert(0, new ListItem(string.Empty, string.Empty));
	}

    private void InitPage() {
		this.pnlMainte.Visible = false;
        this.lstSeekSiteCd.DataBind();
        this.lstSeekProductType.DataBind();

        if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals(string.Empty)) {
            this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
        }
		this.DataBind();
    }

    protected string GetRecCount() {
        return this.recCount;
    }

    private void GetList() {
        this.grdProductGenre.PageSize = 999;
        this.grdProductGenre.DataSourceID = "dsProductGenre";
        this.grdProductGenre.DataBind();
        this.pnlCount.DataBind();
    }

	private void GetData(string pProductGenreCategoryCd, string pProductGenreCd) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("PRODUCT_GENRE_GET");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.lstSeekSiteCd.SelectedValue);
			oDbSession.ProcedureBothParm("pPRODUCT_GENRE_CATEGORY_CD", DbSession.DbType.VARCHAR2, pProductGenreCategoryCd);
			oDbSession.ProcedureBothParm("pPRODUCT_GENRE_CD", DbSession.DbType.VARCHAR2, pProductGenreCd);
			oDbSession.ProcedureOutParm("pPRODUCT_GENRE_NM", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPRIORITY", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pPUBLISH_FLAG", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pROWID", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pREVISION_NO", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pRECORD_COUNT", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.Rowid = oDbSession.GetStringValue("pROWID");
			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
			this.lstProductGenreCagetory.SelectedValue = oDbSession.GetStringValue("pPRODUCT_GENRE_CATEGORY_CD");
			this.txtProductGenreCd.Text = oDbSession.GetStringValue("pPRODUCT_GENRE_CD");
			this.txtProductGenreNm.Text = oDbSession.GetStringValue("pPRODUCT_GENRE_NM");
			this.txtPriority.Text = oDbSession.GetStringValue("pPRIORITY");
			this.rdoPublishFlag.SelectedValue = oDbSession.GetStringValue("pPUBLISH_FLAG");
		}
	}

	protected string GetPublishFlagMark(object pPublishFlag) {
		if (ViCommConst.FLAG_ON_STR.Equals( iBridUtil.GetStringValue(pPublishFlag))) {
			return "公開";
		}
		return "非公開";
	}

	private void UpdateData(bool pDelFlag) {
		int iDelFlag = 0;
		if (pDelFlag) {
			iDelFlag = 1;
		}

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("PRODUCT_GENRE_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pPRODUCT_GENRE_CATEGORY_CD", DbSession.DbType.VARCHAR2, this.lstProductGenreCagetory.SelectedValue);
			oDbSession.ProcedureInParm("pPRODUCT_GENRE_CD", DbSession.DbType.VARCHAR2, this.txtProductGenreCd.Text);
			oDbSession.ProcedureInParm("pPRODUCT_GENRE_NM", DbSession.DbType.VARCHAR2, this.txtProductGenreNm.Text);
			oDbSession.ProcedureInParm("pPRIORITY", DbSession.DbType.NUMBER, this.txtPriority.Text);
			oDbSession.ProcedureInParm("pPUBLISH_FLAG", DbSession.DbType.NUMBER, this.rdoPublishFlag.SelectedValue);
			oDbSession.ProcedureInParm("pROWID", DbSession.DbType.VARCHAR2, this.Rowid);
			oDbSession.ProcedureInParm("pREVISION_NO", DbSession.DbType.NUMBER, this.RevisionNo);
			oDbSession.ProcedureInParm("pDEL_FLAG", DbSession.DbType.NUMBER, iDelFlag);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		int iSiteCdIndex = this.lstSiteCd.SelectedIndex;
		int iProductTypeIndex = this.lstProductType.SelectedIndex;
		this.InitPage();
		this.lstSeekSiteCd.SelectedIndex = iSiteCdIndex;
		this.lstSeekProductType.SelectedIndex = iProductTypeIndex;
		this.GetList();
	}

	private void SyncLists() {
		this.lstSiteCd.SelectedValue = this.lstSeekSiteCd.SelectedValue;
		this.lstProductType.SelectedValue = this.lstSeekProductType.SelectedValue;
	}
	
	private void JoinCells(GridView pGrd, int pCol) {
        int iRow = pGrd.Rows.Count;
        int iBaseidx = 0;

        while (iBaseidx < iRow) {
            int iNextIdx = iBaseidx + 1;
            TableCell celBase = pGrd.Rows[iBaseidx].Cells[pCol];

            while (iNextIdx < iRow) {

                TableCell celNext = pGrd.Rows[iNextIdx].Cells[pCol];

                if (this.GetText(celBase).Equals(this.GetText(celNext))) {
                    if (celBase.RowSpan == 0) {
                        celBase.RowSpan = 2;
                    } else {
                        celBase.RowSpan++;
                    }
					pGrd.Rows[iNextIdx].Cells.Remove(celNext);
                    iNextIdx++;
                } else {
                    break;
                }
            }
            iBaseidx = iNextIdx;
        }
    }

    private string GetText(TableCell tc) {
        return ((Label)tc.FindControl("lblProductGenreCategoryCd")).Text;
    }
}
