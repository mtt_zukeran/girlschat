﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ProductView.aspx.cs" Inherits="Product_ProductView" Title="Untitled Page" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="商品詳細"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <table border="0">
                <tr>
                    <td valign="top">
                        <fieldset class="fieldset" style="padding: 0px 10px 4px 10px; height: 65px">
                            <legend>[検索条件]</legend>
                            <table border="0" style="width: 540px" class="tableStyle">
                                <tr>
                                    <td class="tdHeaderStyle2">
                                        商品ID
                                    </td>
                                    <td class="tdDataStyle">
                                        <asp:TextBox ID="txtProductId" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <asp:Button ID="btnSeek" runat="server" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click"
                                ValidationGroup="Key" />
                            <asp:Button ID="btnClear" runat="server" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
                            <asp:Button ID="btnBack" runat="server" Text="戻る" CssClass="seekbutton" OnClientClick="history.back();return false;" />
                        </fieldset>
                    </td>
                    <td valign="top">
                        <asp:Panel ID="pnlProductSituation" runat="server" >
							<fieldset class="fieldset" style="padding: 0px 5px 4px 5px; height: 65px">
								<legend>[商品状況]</legend>
									<table style="line-height: 150%; text-align: left; vertical-align: top" cellspacing="0">
										<tr>
											<td>
												<asp:LinkButton ID="lnkAuctionBidLog" runat="server" Width="70px" OnClick="lnkAuctionBidLog_Click"
													ValidationGroup="Key">入札履歴</asp:LinkButton>
											</td>
											<td>
												<asp:LinkButton ID="lnkAuctionEntryLog" runat="server" Width="70px" OnClick="lnkAuctionEntryLog_Click"
													ValidationGroup="Key">参加会員</asp:LinkButton>
											</td>
										</tr>
									</table>
							</fieldset>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlExtInfo" HorizontalAlign="Left" Visible="false">
            <table border="0">
                <tr>
                    <td>
                        <asp:Panel runat="server" ID="pnlAuctionBidLog" Visible="false">
                            <fieldset class="fieldset-inner">
                                <legend>[入札履歴]</legend>
                                <asp:GridView ID="grdAuctionBidLog" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                    DataSourceID="" AllowSorting="True" SkinID="GridViewColor" PageSize="30">
                                    <Columns>
                                        <asp:BoundField DataField="REGIST_DATE" HeaderText="入札日時">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="BID_AMT" HeaderText="入札金額">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="BID_POINT" HeaderText="入札ﾎﾟｲﾝﾄ">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="ID">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# string.Format("../Man/ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>'
                                                    Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="HANDLE_NM" HeaderText="ﾊﾝﾄﾞﾙ名">
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="ﾌﾞﾗｲﾝﾄﾞ">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBlindBidFlag" runat="server" Text='<%# GetBlindBidMark(Eval("BLIND_BID_FLAG")) %>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerSettings Mode="NumericFirstLast" />
                                </asp:GridView>
                                <asp:Button runat="server" ID="btnCloseExt1" Text="閉じる" CssClass="seekbutton" OnClick="btnClose_Ext_Click" />
                            </fieldset>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlAuctionEntryLog" Visible="false">
                            <fieldset class="fieldset-inner">
                                <legend>[参加会員]</legend>
                                <asp:GridView ID="grdAuctionEntryLog" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                    DataSourceID="" AllowSorting="True" SkinID="GridViewColor" PageSize="30">
                                    <Columns>
                                        <asp:BoundField DataField="REGIST_DATE" HeaderText="参加日時">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="ID">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# string.Format("../Man/ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>'
                                                    Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="HANDLE_NM" HeaderText="ﾊﾝﾄﾞﾙ名">
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ENTRY_POINT" HeaderText="参加ﾎﾟｲﾝﾄ">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="TOTAL_BID_POINT" HeaderText="入札ﾎﾟｲﾝﾄ累計">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="REMAINING_RETURN_POINT" HeaderText="返却ﾎﾟｲﾝﾄ残">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                    </Columns>
                                    <PagerSettings Mode="NumericFirstLast" />
                                </asp:GridView>
                                <asp:Button runat="server" ID="btnCloseExt2" Text="閉じる" CssClass="seekbutton" OnClick="btnClose_Ext_Click" />
                            </fieldset>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlProductInfo" Visible="false">
            <asp:Panel runat="server" ID="pnlInfo">
                <table border="0">
                    <tr>
                        <td valign="top">
                            <fieldset class="fieldset-inner">
                                <legend>[商品情報]</legend>
                                <asp:DetailsView ID="dvwProduct" runat="server" DataSourceID="dsProduct" AutoGenerateRows="False"
                                    SkinID="DetailsView" Width="380px" OnDataBound="dvwProduct_DataBound">
                                    <Fields>
                                        <asp:TemplateField HeaderText="ID">
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("PRODUCT_ID") %>'></asp:Label>
                                                (<asp:Label ID="Label2" runat="server" Text='<%# Eval("PRODUCT_SEQ") %>'></asp:Label>)
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="PRODUCT_AGENT_NM" HeaderText="ｺﾝﾃﾝﾂﾌﾟﾛﾊﾞｲﾀﾞ" />
                                        <asp:TemplateField HeaderText="商品種別">
                                            <ItemTemplate>
                                                <asp:Label ID="lblProductType" runat="server" Text='<%# GetProductTypeMark(Eval("PRODUCT_TYPE")) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="PRODUCT_NM" HeaderText="商品名" />
                                        <asp:BoundField DataField="PART_NUMBER" HeaderText="商品番号" />
                                        <asp:BoundField DataField="PRODUCT_SERIES_SUMMARY" HeaderText="商品シリーズ" />
                                        <asp:BoundField DataField="CAST_SUMMARY" HeaderText="出演者" />
                                        <asp:BoundField DataField="PRODUCT_MAKER_SUMMARY" HeaderText="製造元" />
                                        <asp:BoundField DataField="PUBLISH_START_DATE" HeaderText="公開開始日時" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}"
                                            HtmlEncode="False" />
                                        <asp:BoundField DataField="PUBLISH_END_DATE" HeaderText="公開終了日時" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}"
                                            HtmlEncode="False" />
                                        <asp:BoundField DataField="RELEASE_DATE" HeaderText="発売日" DataFormatString="{0:yyyy/MM/dd}"
                                            HtmlEncode="False" />
                                        <asp:BoundField DataField="GROSS_PRICE" HeaderText="グロス価格" />
                                        <asp:BoundField DataField="CHARGE_POINT" HeaderText="課金ポイント" />
                                        <asp:BoundField DataField="SPECIAL_CHARGE_POINT" HeaderText="特別課金ポイント" />
                                        <asp:BoundField DataField="ROYALTY_RATE" HeaderText="ロイヤリティ率" />
                                        <asp:TemplateField HeaderText="新着表示/非表示">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNotNewFlag" runat="server" Text='<%# GetNotNewFlagMark(Eval("NOT_NEW_FLAG")) %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="関連出演者">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lnkCastView" runat="server" Text='<%# GetHandleNm(Eval("SITE_CD"), Eval("PRODUCT_SEQ"), Eval("PRODUCT_AGENT_CD")) %>'
                                                    NavigateUrl='<%# GenerateCastViewUrl(Eval("SITE_CD"), Eval("PRODUCT_SEQ"), Eval("PRODUCT_AGENT_CD")) %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="LINK_ID" HeaderText="連携ID" />
                                        <asp:BoundField DataField="LINK_DATE" HeaderText="連携日時" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}"
                                            HtmlEncode="False" />
                                        <asp:TemplateField HeaderText="ピックアップ">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPickup" runat="server" Text='<%# GetPickupMask(Eval("PICKUP_MASK")) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="公開/非公開">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPublishFlag" runat="server" Text='<%# GetPublishMark(Eval("PUBLISH_FLAG")) %>'
                                                    ForeColor='<%# GetPublishMarkColor(Eval("PUBLISH_FLAG")) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="削除">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDelFlag" runat="server" Text='<%# GetDelMark(Eval("DEL_FLAG")) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="商品基本情報更新">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="lnkProductMainte" runat="server" Text="更新実行" NavigateUrl='<%# GenerateProductMainteUrl(Container.DataItem) %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Fields>
                                    <HeaderStyle Wrap="False" />
                                </asp:DetailsView>
                                <asp:Panel runat="server" ID="pnlAuctionInfo">
                                    <legend>[オークション基本情報]</legend>
                                    <asp:DetailsView ID="dvwProductAuction" runat="server" DataSourceID="dsProductAuction"
                                        AutoGenerateRows="False" SkinID="DetailsView" Width="380px">
                                        <Fields>
                                            <asp:TemplateField HeaderText="ｵｰｸｼｮﾝ状態">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblProductType" runat="server" Text='<%# GetAuctionStatusMark(Eval("AUCTION_STATUS")) %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="AUCTION_START_DATE" HeaderText="ｵｰｸｼｮﾝ開始日時" />
                                            <asp:BoundField DataField="BLIND_END_DATE" HeaderText="ﾌﾞﾗｲﾝﾄﾞ終了日時" />
                                            <asp:BoundField DataField="AUCTION_END_DATE" HeaderText="ｵｰｸｼｮﾝ終了日時" />
                                            <asp:BoundField DataField="RESERVE_AMT" HeaderText="開始金額" DataFormatString="{0:N0}" />
                                            <asp:BoundField DataField="MINIMUM_BID_AMT" HeaderText="入札単位" DataFormatString="{0:N0}" />
                                            <asp:BoundField DataField="ENTRY_POINT" HeaderText="参加ﾎﾟｲﾝﾄ" DataFormatString="{0:N0}" />
                                            <asp:BoundField DataField="BLIND_ENTRY_POINT" HeaderText="参加ﾎﾟｲﾝﾄ ﾌﾞﾗｲﾝﾄﾞ時" DataFormatString="{0:N0}" />
                                            <asp:BoundField DataField="BID_POINT" HeaderText="入札ﾎﾟｲﾝﾄ" DataFormatString="{0:N0}" />
                                            <asp:BoundField DataField="BLIND_BID_POINT" HeaderText="入札ﾎﾟｲﾝﾄ ﾌﾞﾗｲﾝﾄﾞ時" DataFormatString="{0:N0}" />
                                            <asp:BoundField DataField="MAX_BID_AMT" DataFormatString="{0:N0}" HeaderText="最高入札額" />
                                            <asp:BoundField DataField="BID_COUNT" DataFormatString="{0:N0}" HeaderText="入札回数" />
                                            <asp:TemplateField HeaderText="自動延長">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAutoExtensionFlag" runat="server" Text='<%# GetAutoExtensionFlagMark(Eval("AUTO_EXTENSION_FLAG")) %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ｵｰｸｼｮﾝ状態更新">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="lstAuctionStatus" runat="server" Enabled='<%# GetStatusUpdateEnabled(Eval("AUCTION_STATUS")) %>'>
                                                        <asp:ListItem Text="" Value="">
                                                        </asp:ListItem>
                                                        <asp:ListItem Text="終了" Value="80">
                                                        </asp:ListItem>
                                                        <asp:ListItem Text="落札辞退" Value="70">
                                                        </asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:Button ID="btnUpdateAuctionStatus" runat="server" OnClick="btnUpdateAuctionStatus_Click"
                                                        Enabled='<%# GetStatusUpdateEnabled(Eval("AUCTION_STATUS")) %>' OnClientClick="return confirm('状態の更新を行いますか？');"
                                                        Text="更新" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Fields>
                                        <HeaderStyle Wrap="False" />
                                    </asp:DetailsView>
                                </asp:Panel>
                            </fieldset>
                            <asp:HiddenField ID="hdnSiteCd" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hdnProductAgentCd" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hdnProductSeq" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hdnProductType" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hdnLinkId" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hdnAuctionStatus" runat="server" />
                            <asp:HiddenField ID="hdnRevisionNo" runat="server" />
                        </td>
                        <td valign="top">
                            <% // ************************************	%>
                            <% //  備考									%>
                            <% // ************************************	%>							
							<table border="0">
								<tr>
									<td>
										<asp:Panel runat="server" ID="pnlRemarks">
											<fieldset class="fieldset-inner">
												<legend>[備考]</legend>
												<table border="0" style="width: 490px" class="tableStyle">
													<tr>
														<td class="tdHeaderStyle2">
															備 考
														</td>
														<td class="tdDataStyle">
															<asp:TextBox ID="txtRemarks" runat="server" MaxLength="256" Width="360px" Height="70px" TextMode="MultiLine"></asp:TextBox><br />
														</td>
													</tr>
												</table>
												<asp:Button runat="server" ID="btnRemarks" Text="備考更新" CssClass="seekbutton" OnClick="btnRemarks_Click" ValidationGroup="Remarks" />
											</fieldset>
										</asp:Panel>
									</td>
								</tr>
							</table>
                            <% // ************************************	%>
                            <% //  キーワード							%>
                            <% // ************************************	%>
                            <table border="0">
                                <tr>
                                    <td>
                                        <fieldset class="fieldset-inner">
                                            <legend>[キーワード]</legend>
                                            <table border="0">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblKeyword" runat="server"></asp:Label>
                                                        <asp:Label ID="lblEmptyKeywordMsg" runat="server" ForeColor="red">未設定</asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                            </table>
                            <% // ************************************	%>
                            <% //  ジャンル								%>
                            <% // ************************************	%>
                            <table border="0">
                                <tr>
                                    <td>
                                        <fieldset class="fieldset-inner">
                                            <legend>[ジャンル]</legend>
                                            <asp:GridView ID="grdGenre" runat="server" AutoGenerateColumns="false" SkinID="GridView">
                                                <Columns>
                                                    <asp:BoundField HeaderText="カテゴリ" DataField="PRODUCT_GENRE_CATEGORY_NM" />
                                                    <asp:BoundField HeaderText="ジャンル" DataField="PRODUCT_GENRE_NM" />
                                                </Columns>
                                            </asp:GridView>
                                            <asp:Label ID="lblEmptyGenreMsg" runat="server" ForeColor="red">未設定</asp:Label>
                                        </fieldset>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <% // ************************************	%>
            <% //  商品動画								%>
            <% // ************************************	%>
            <asp:Panel runat="server" ID="pnlProductMovie">
                <fieldset class="fieldset">
                    <legend>[動画情報]</legend>
                    <fieldset class="fieldset">
                        <legend>[商品画像]</legend>
                        <table border="0">
                            <tr>
                                <td class="tdHeaderStyle">
                                    パッケージ表
                                </td>
                                <td class="tdHeaderStyle">
                                    パッケージ裏
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button runat="server" ID="btnPackageDel" Text="削除" OnClientClick="return confirm('削除を行いますか？');"
                                        OnCommand="btnPackageDel_Command" />
                                    <asp:Button runat="server" ID="btnPackageAdd" Text="設定" OnCommand="btnAppendPic_Command" />
                                </td>
                                <td>
                                    <asp:Button runat="server" ID="btnPackage2Del" Text="削除" OnClientClick="return confirm('削除を行いますか？');"
                                        OnCommand="btnPackageDel_Command" />
                                    <asp:Button runat="server" ID="btnPackage2Add" Text="設定" OnCommand="btnAppendPic_Command" />
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle" align="center" style="width: 200px">
                                    <asp:Panel ID="pnlMoviePackageA" runat="server">
                                        <asp:Image ID="imgMoviePackageA" runat="server"></asp:Image>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlEmptyMoviePackageA" runat="server">
                                        <asp:Label ID="lblEmptyMoviePackageAMsg" runat="server" Text="未登録" ForeColor="#FF0000"
                                            Font-Bold="True"></asp:Label>
                                    </asp:Panel>
                                </td>
                                <td valign="middle" align="center" style="width: 200px">
                                    <asp:Panel ID="pnlMoviePackageB" runat="server">
                                        <asp:Image ID="imgMoviePackageB" runat="server"></asp:Image>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlEmptyMoviePackageB" runat="server">
                                        <asp:Label ID="lblEmptyMoviePackageBMsg" runat="server" Text="未登録" ForeColor="#FF0000"
                                            Font-Bold="True"></asp:Label>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                    <fieldset class="fieldset">
                        <legend>[コンテンツ動画]</legend>
                        <asp:Panel ID="pnlContentsMovie" runat="server">
                            <asp:Button ID="btnEditContentsMovie" runat="server" Text="編集" OnCommand="btnEditMovie_Command" />
                            <asp:HyperLink ID="lnkContentsMovie" runat="server" Text="確認する">
                            </asp:HyperLink>
                            <br />
                            <asp:DetailsView ID="dvwContentsMovie" runat="server" AutoGenerateRows="False" SkinID="DetailsView"
                                Width="380px">
                                <Fields>
                                    <asp:BoundField DataField="PLAY_TIME" HeaderText="再生時間" DataFormatString="{0:HH:mm:ss}"
                                        HtmlEncode="False" />
                                    <asp:BoundField DataField="UPLOAD_DATE" HeaderText="アップロード日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}"
                                        HtmlEncode="False" />
                                </Fields>
                                <HeaderStyle Wrap="False" />
                            </asp:DetailsView>
                            <br />
                            <asp:GridView ID="grdContentsMovieDtl" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                SkinID="GridView">
                                <Columns>
                                    <asp:TemplateField HeaderText="対象ｷｬﾘｱ">
                                        <ItemTemplate>
                                            <%# ConvertCarrierCd2Nm(DataBinder.Eval(Container.DataItem, "CP_TARGET_CARRIER"))%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ファイル種別">
                                        <ItemTemplate>
                                            <%# DataBinder.Eval(Container.DataItem, "CP_SIZE_TYPE").ToString() %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="フォーマット">
                                        <ItemTemplate>
                                            <%# DataBinder.Eval(Container.DataItem, "CP_FILE_FORMAT").ToString()%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ファイル数">
                                        <ItemStyle HorizontalAlign="right" />
                                        <ItemTemplate>
                                            <%# DataBinder.Eval(Container.DataItem, "FILE_COUNT").ToString() %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                        <asp:Panel ID="pnlEmptyContentsMovie" runat="server" Visible="false">
                            <asp:Button runat="server" ID="btnAppendContentsMovie" Text="設定" CssClass="seekbutton"
                                OnCommand="btnAppendMovie_Command" />
                            <br />
                            <asp:Label ID="lblEmptyContentsMovieMsg" runat="server" Text="コンテンツ動画は登録されていません。"
                                ForeColor="#FF0000" Font-Bold="True"></asp:Label>
                        </asp:Panel>
                    </fieldset>
                    <fieldset class="fieldset">
                        <legend>[サンプル動画]</legend>
                        <asp:Panel ID="pnlSampleMovie" runat="server">
                            <asp:Button ID="btnEditSampleMovie" runat="server" Text="編集" OnCommand="btnEditMovie_Command" />
                            <asp:HyperLink ID="lnkOpenSampleMovieViewer" runat="server" Text="確認する">
                            </asp:HyperLink>
                            <br />
                            <asp:GridView ID="grdSampleMovieDtl" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                SkinID="GridView">
                                <Columns>
                                    <asp:TemplateField HeaderText="対象ｷｬﾘｱ">
                                        <ItemTemplate>
                                            <%# ConvertCarrierCd2Nm(DataBinder.Eval(Container.DataItem, "CP_TARGET_CARRIER"))%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ファイル種別">
                                        <ItemTemplate>
                                            <%# DataBinder.Eval(Container.DataItem, "CP_SIZE_TYPE").ToString() %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="フォーマット">
                                        <ItemTemplate>
                                            <%# DataBinder.Eval(Container.DataItem, "CP_FILE_FORMAT").ToString() %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ファイル数">
                                        <ItemStyle HorizontalAlign="right" />
                                        <ItemTemplate>
                                            <%# DataBinder.Eval(Container.DataItem, "FILE_COUNT").ToString() %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                        <asp:Panel ID="pnlEmptySampleMovie" runat="server" Visible="false">
                            <asp:Button runat="server" ID="btnAppendSampleMovie" Text="設定" OnCommand="btnAppendMovie_Command" />
                            <br />
                            <asp:Label ID="lblEmptySampleMovieMsg" runat="server" Text="未登録" ForeColor="#FF0000"
                                Font-Bold="True"></asp:Label>
                        </asp:Panel>
                    </fieldset>
                    <fieldset class="fieldset">
                        <legend>[サンプル画像]</legend>
                        <asp:Button runat="server" ID="btnAppendMovieSamplePic" Text="設定" OnCommand="btnAppendPic_Command" />
                        <br />
                        <div>
                            <asp:Panel ID="pnlMovieSamplePic" runat="server">
                                <asp:Repeater ID="rptMovieSamplePic" runat="server">
                                    <ItemTemplate>
                                        <asp:Image ID="imgMovieSamplePic" runat="server" ImageUrl='<%# string.Format("~/{0}",Eval("PHOTO_IMG_PATH")) %>'>
                                        </asp:Image>
                                        <%# ((Container.ItemIndex+1)%4)==0?"<br />":"" %>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </asp:Panel>
                            <asp:Panel ID="pnlEmptyMovieSamplePic" runat="server">
                                <asp:Label ID="lblEmptyMovieSamplePicMsg" runat="server" Text="未登録" ForeColor="#FF0000"
                                    Font-Bold="True"></asp:Label>
                            </asp:Panel>
                        </div>
                    </fieldset>
                </fieldset>
            </asp:Panel>
            <% // ************************************	%>
            <% //  商品写真								%>
            <% // ************************************	%>
            <asp:Panel runat="server" ID="pnlProductPic">
                <fieldset class="fieldset">
                    <legend>[写真情報]</legend>
                    <table border="0">
                        <tr>
                            <td class="tdHeaderStyle">
                                商品画像
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button runat="server" ID="btnRemovePicPackagePic" Text="削除" OnClientClick="return confirm('削除を行いますか？');"
                                    OnCommand="btnPackageDel_Command" />
                                <asp:Button runat="server" ID="btnAppendPicPackagePic" Text="設定" OnCommand="btnAppendPic_Command" />
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" align="center" style="width: 200px">
                                <asp:Panel ID="pnlPicPackagePic" runat="server">
                                    <asp:Image ID="imgPicPackagePic" runat="server"></asp:Image>
                                </asp:Panel>
                                <asp:Panel ID="pnlEmptyPicPackagePic" runat="server">
                                    <asp:Label ID="lblEmptyPicPackagePicMsg" runat="server" Text="未登録" ForeColor="#FF0000"
                                        Font-Bold="True"></asp:Label>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                    <fieldset class="fieldset">
                        <legend>[コンテンツ写真]</legend>
                        <asp:Button runat="server" ID="btnAppendPicContentsPic" Text="設定" OnCommand="btnAppendPic_Command" />
                        <br />
                        <div>
                            <asp:Panel ID="pnlPicContentsPic" runat="server">
                                <asp:Repeater ID="rptPicContentsPic" runat="server">
                                    <ItemTemplate>
                                        <asp:Image ID="imgPicContentsPic" runat="server" ImageUrl='<%# string.Format("~/{0}",Eval("PHOTO_IMG_PATH")) %>'>
                                        </asp:Image>
                                        <%# ((Container.ItemIndex + 1) % 4) == 0 ? "<br />" : string.Empty%>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </asp:Panel>
                            <asp:Panel ID="pnlEmptyPicContentsPic" runat="server">
                                <asp:Label ID="lblEmptyPicContentsPicMsg" runat="server" Text="未登録" ForeColor="#FF0000"
                                    Font-Bold="True"></asp:Label>
                            </asp:Panel>
                        </div>
                    </fieldset>
                    <fieldset class="fieldset">
                        <legend>[サンプル画像(一覧表示用)]</legend>
                        <asp:Button runat="server" ID="btnAppendPicSamplePic" Text="設定" OnCommand="btnAppendPic_Command" />
                        <br />
                        <div>
                            <asp:Panel ID="pnlPicSamplePic" runat="server">
                                <asp:Repeater ID="rptPicSamplePic" runat="server">
                                    <ItemTemplate>
                                        <asp:Image ID="imgPicSamplePic" runat="server" ImageUrl='<%# string.Format("~/{0}",Eval("PHOTO_IMG_PATH")) %>'>
                                        </asp:Image>
                                        <%# ((Container.ItemIndex + 1) % 4) == 0 ? "<br />" : string.Empty%>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </asp:Panel>
                            <asp:Panel ID="pnlEmptyPicSamplePic" runat="server">
                                <asp:Label ID="lblEmptyPicSamplePicMsg" runat="server" Text="未登録" ForeColor="#FF0000"
                                    Font-Bold="True"></asp:Label>
                            </asp:Panel>
                        </div>
                    </fieldset>
                </fieldset>
            </asp:Panel>
            <% // ************************************	%>
            <% //  商品オークション						%>
            <% // ************************************	%>
            <asp:Panel runat="server" ID="pnlProductAuction">
                <fieldset class="fieldset">
                    <legend>[オークション情報]</legend>
                    <fieldset class="fieldset">
                        <legend>[商品画像]</legend>
                        <table border="0">
                            <tr>
                                <td class="tdHeaderStyle">
                                    パッケージ表
                                </td>
                                <td class="tdHeaderStyle">
                                    パッケージ裏
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button runat="server" ID="btnAuctionPackageDel" Text="削除" OnClientClick="return confirm('削除を行いますか？');"
                                        OnCommand="btnPackageDel_Command" />
                                    <asp:Button runat="server" ID="btnAuctionPackageAdd" Text="設定" OnCommand="btnAppendPic_Command" />
                                </td>
                                <td>
                                    <asp:Button runat="server" ID="btnAuctionPackage2Del" Text="削除" OnClientClick="return confirm('削除を行いますか？');"
                                        OnCommand="btnPackageDel_Command" />
                                    <asp:Button runat="server" ID="btnAuctionPackage2Add" Text="設定" OnCommand="btnAppendPic_Command" />
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle" align="center" style="width: 200px">
                                    <asp:Panel ID="pnlAuctionMoviePackageA" runat="server">
                                        <asp:Image ID="imgAuctionMoviePackageA" runat="server"></asp:Image>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlEmptyAuctionMoviePackageA" runat="server">
                                        <asp:Label ID="lblEmptyAuctionMoviePackageAMsg" runat="server" Text="未登録" ForeColor="#FF0000"
                                            Font-Bold="True"></asp:Label>
                                    </asp:Panel>
                                </td>
                                <td valign="middle" align="center" style="width: 200px">
                                    <asp:Panel ID="pnlAuctionMoviePackageB" runat="server">
                                        <asp:Image ID="imgAuctionMoviePackageB" runat="server"></asp:Image>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlEmptyAuctionMoviePackageB" runat="server">
                                        <asp:Label ID="lblEmptyAuctionMoviePackageBMsg" runat="server" Text="未登録" ForeColor="#FF0000"
                                            Font-Bold="True"></asp:Label>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                    <fieldset class="fieldset">
                        <legend>[サンプル動画]</legend>
                        <asp:Panel ID="pnlSampleAuctionMovie" runat="server">
                            <asp:Button ID="btnEditSampleAuctionMovie" runat="server" Text="編集" OnCommand="btnEditMovie_Command" />
                            <asp:HyperLink ID="lnkOpenSampleAuctionMovieViewer" runat="server" Text="確認する">
                            </asp:HyperLink>
                            <br />
                            <asp:GridView ID="grdSampleAuctionMovieDtl" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                SkinID="GridView">
                                <Columns>
                                    <asp:TemplateField HeaderText="対象ｷｬﾘｱ">
                                        <ItemTemplate>
                                            <%# ConvertCarrierCd2Nm(DataBinder.Eval(Container.DataItem, "CP_TARGET_CARRIER"))%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ファイル種別">
                                        <ItemTemplate>
                                            <%# DataBinder.Eval(Container.DataItem, "CP_SIZE_TYPE").ToString() %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="フォーマット">
                                        <ItemTemplate>
                                            <%# DataBinder.Eval(Container.DataItem, "CP_FILE_FORMAT").ToString() %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ファイル数">
                                        <ItemStyle HorizontalAlign="right" />
                                        <ItemTemplate>
                                            <%# DataBinder.Eval(Container.DataItem, "FILE_COUNT").ToString() %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                        <asp:Panel ID="pnlEmptySampleAuctionMovie" runat="server" Visible="false">
                            <asp:Button runat="server" ID="btnAppendSampleAuctionMovie" Text="設定" OnCommand="btnAppendMovie_Command" />
                            <br />
                            <asp:Label ID="lblEmptySampleAuctionMovieMsg" runat="server" Text="未登録" ForeColor="#FF0000"
                                Font-Bold="True"></asp:Label>
                        </asp:Panel>
                    </fieldset>
                    <fieldset class="fieldset">
                        <legend>[サンプル画像]</legend>
                        <asp:Button runat="server" ID="btnAppendMovieSampleAuctionPic" Text="設定" OnCommand="btnAppendPic_Command" />
                        <br />
                        <div>
                            <asp:Panel ID="pnlMovieSampleAuctionPic" runat="server">
                                <asp:Repeater ID="rptMovieSampleAuctionPic" runat="server">
                                    <ItemTemplate>
                                        <asp:Image ID="imgMovieSampleAuctionPic" runat="server" ImageUrl='<%# string.Format("~/{0}",Eval("PHOTO_IMG_PATH")) %>'>
                                        </asp:Image>
                                        <%# ((Container.ItemIndex+1)%4)==0?"<br />":"" %>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </asp:Panel>
                            <asp:Panel ID="pnlEmptyMovieSampleAuctionPic" runat="server">
                                <asp:Label ID="lblEmptyMovieSampleAuctionPicMsg" runat="server" Text="未登録" ForeColor="#FF0000"
                                    Font-Bold="True"></asp:Label>
                            </asp:Panel>
                        </div>
                    </fieldset>
                </fieldset>
            </asp:Panel>
            <% // ************************************	%>
            <% //  着せ替えﾂｰﾙ							%>
            <% // ************************************	%>
            <asp:Panel runat="server" ID="pnlProductTheme">
                <fieldset class="fieldset">
                    <legend>[着せ替えﾂｰﾙ情報]</legend>
                    <fieldset class="fieldset">
                        <legend>[商品画像]</legend>
                        <table border="0">
                            <tr>
                                <td class="tdHeaderStyle">
                                    パッケージ
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button runat="server" ID="btnDelProductThemePic" Text="削除" OnClientClick="return confirm('削除を行いますか？');"
                                        OnCommand="btnPackageDel_Command" />
                                    <asp:Button runat="server" ID="btnAddProductThemePic" Text="設定" OnCommand="btnAppendPic_Command" />
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle" align="center" style="width: 200px">
                                    <asp:Panel ID="pnlProductThemePic" runat="server">
                                        <asp:Image ID="imgProductThemePic" runat="server"></asp:Image>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlEmptyProductThemePic" runat="server">
                                        <asp:Label ID="lblEmptyProductThemePicMsg" runat="server" Text="未登録" ForeColor="#FF0000"
                                            Font-Bold="True"></asp:Label>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                    <fieldset class="fieldset">
                        <legend>[サンプル画像]</legend>
                        <asp:Button runat="server" ID="btnAddProductThemeSamplePic" Text="設定" OnCommand="btnAppendPic_Command" />
                        <br />
                        <div>
                            <asp:Panel ID="pnlProductThemeSample" runat="server">
                                <asp:Repeater ID="rptProductThemeSamplePic" runat="server">
                                    <ItemTemplate>
                                        <asp:Image ID="imgProductThemeSamplePic" runat="server" ImageUrl='<%# string.Format("~/{0}",Eval("PHOTO_IMG_PATH")) %>'>
                                        </asp:Image>
                                        <%# ((Container.ItemIndex + 1) % 4) == 0 ? "<br />" : string.Empty%>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </asp:Panel>
                            <asp:Panel ID="pnlEmptyProductThemeSamplePic" runat="server">
                                <asp:Label ID="lblEmptyProductThemeSamplePicMsg" runat="server" Text="未登録" ForeColor="#FF0000"
                                    Font-Bold="True"></asp:Label>
                            </asp:Panel>
                        </div>
                    </fieldset>
                </fieldset>
            </asp:Panel>
        </asp:Panel>
    </div>
    <% // *********************************		%>
    <% //  ObjectDataSource						%>
    <% // *********************************		%>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsProduct" runat="server" SelectMethod="GetOne" TypeName="Product"
        OnSelected="dsProduct_Selected">
        <SelectParameters>
            <asp:ControlParameter ControlID="txtProductId" PropertyName="Text" Name="pProductId"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsProductAuction" runat="server" SelectMethod="GetOne"
        TypeName="ProductAuction">
        <SelectParameters>
            <asp:ControlParameter ControlID="hdnSiteCd" PropertyName="Value" Name="pSiteCd" Type="String" />
            <asp:ControlParameter ControlID="hdnProductAgentCd" PropertyName="Value" Name="pProductAgentCd"
                Type="String" />
            <asp:ControlParameter ControlID="hdnProductSeq" PropertyName="Value" Name="pProductSeq"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsAuctionBidLog" runat="server" SelectCountMethod="GetPageCount"
        SelectMethod="GetPageCollection" TypeName="ProductAuctionBidLog" EnablePaging="True"
        OnSelecting="dsProductAuctionBidLog_Selecting">
        <SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="Object" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsAuctionEntryLog" runat="server" SelectCountMethod="GetPageCount"
        SelectMethod="GetPageCollection" TypeName="ProductAuctionEntryLog" EnablePaging="True"
        OnSelecting="dsProductAuctionEntryLog_Selecting">
        <SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="Object" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
