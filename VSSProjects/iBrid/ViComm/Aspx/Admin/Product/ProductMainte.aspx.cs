﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using iBridCommLib;
using ViComm;

public partial class Product_ProductMainte : System.Web.UI.Page {
	private const int MAX_COUNT_OF_PICKUP = 5;
	private const int MAX_COUNT_OF_KEYWORD = 20;
	private const int MAX_COUNT_OF_GENRE = 6;

	protected object[] DummyKeywordArray {
		get { return new object[MAX_COUNT_OF_KEYWORD]; }
	}
	protected object[] DummyPickupArray {
		get { return new object[MAX_COUNT_OF_PICKUP]; }
	}
	protected object[] DummyGenreArray {
		get { return new object[MAX_COUNT_OF_GENRE]; }
	}
	private string ProductId {
		get { return iBridUtil.GetStringValue(this.ViewState["ProductId"]); }
		set { this.ViewState["ProductId"] = value; }
	}
	protected List<string> ProductGenreCategoryList {
		get {
			List<string> genreCategoryList = ViewState["pPRODUCT_GENRE_CATEGORY_CD_ARR"] as List<string>;
			if (genreCategoryList == null) {
				ViewState["pPRODUCT_GENRE_CATEGORY_CD_ARR"] = new List<string>();
			}
			return (List<string>)ViewState["pPRODUCT_GENRE_CATEGORY_CD_ARR"];
		}
		private set { ViewState["pPRODUCT_GENRE_CATEGORY_CD_ARR"] = value; }
	}

	protected List<string> ProductGenreList {
		get {
			List<string> genreList = ViewState["pPRODUCT_GENRE_CD_ARR"] as List<string>;
			if (genreList == null) {
				ViewState["pPRODUCT_GENRE_CD_ARR"] = new List<string>();
			}
			return (List<string>)ViewState["pPRODUCT_GENRE_CD_ARR"];
		}
		private set { ViewState["pPRODUCT_GENRE_CD_ARR"] = value; }
	}

	protected void Page_Load(object sender, EventArgs e) {
		if (!this.IsPostBack) {
			this.InitPage();
			
			string sSiteCd = iBridUtil.GetStringValue(this.Request["site_cd"]);
			string sProductAgentCd = iBridUtil.GetStringValue(this.Request["product_agent_cd"]);
			string sProductSeq = iBridUtil.GetStringValue(this.Request["product_seq"]);

			this.GetData(sSiteCd, sProductAgentCd, sProductSeq);
			this.SetupProductAgentDropDownList();

			if (this.GetMultiCharFlag(sSiteCd)) {
				this.txtUserCharNo.Visible = true;
			}
		}
		this.SetupProductGenreTreeView();
	}

	protected void btnUpdate_Click(object sender, EventArgs e) {
		if (!this.IsValid) return;
		this.UpdateData(false);
	}
	protected void btnDelete_Click(object sender, EventArgs e) {
		this.UpdateData(true);
	}
	protected void btnCancel_Click(object sender, EventArgs e) {
		string sProductId = this.ProductId;

		UrlBuilder oUrlBuilder = new UrlBuilder("ProductView.aspx");
		oUrlBuilder.AddParameter("product_id", sProductId);
		Response.Redirect(oUrlBuilder.ToString());
	}
	protected void btnConfirmLoginId_Click(object sender, EventArgs e) {
		if (!this.IsValid) return;

		this.lblHandlNm.Text = string.Empty;

		string sSiteCd = this.lstSiteCd.SelectedValue;
		string sLoginId = this.txtLoginId.Text;
		string sUserCharNo = ViCommConst.MAIN_CHAR_NO;

		using (CastCharacter oCastCharacter = new CastCharacter()) {
			DataSet oCastCharacterDataSet = oCastCharacter.GetOne(sSiteCd, sLoginId, sUserCharNo);
			if (oCastCharacterDataSet.Tables.Count > 0 && oCastCharacterDataSet.Tables[0].Rows.Count > 0) {
				this.lblHandlNm.Text = iBridUtil.GetStringValue(oCastCharacterDataSet.Tables[0].Rows[0]["HANDLE_NM"]);
			}
		}
	}

	protected void btnCreate_Click(object sender, EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.SetupCreateMode(true);
	}

	protected void btnCreateCancel_Click(object sender, EventArgs e) {
		this.SetupCreateMode(false);
	}

	protected void lstSiteCd_SelectedIndexChanged(object sender, EventArgs e) {
		this.SetupProductGenreTreeView();
		this.lblHandlNm.Text = string.Empty;
	}

	protected void dsProductAgent_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		int? iOuterAgentFlag = null;
		if (this.hdnIsInitial.Value.Equals(true.ToString())) {
			iOuterAgentFlag = ViCommConst.FLAG_OFF;
		}
		e.InputParameters["pOuterAgentFlag"] = iOuterAgentFlag;
	}
	protected void dsProductPickupManage_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = this.lstSiteCd.SelectedValue;
		e.InputParameters["pProductType"] = this.lstProductType.SelectedValue;
	}

	#region □■□ 入力チェック関連 □■□ ============================================================================
	protected void ValidateDate(object source, ServerValidateEventArgs args) {
		DateTime? dtValue = SysPrograms.TryParseOrDafult(args.Value, (DateTime?)null);
		args.IsValid = (dtValue != null);
	}
	protected void vdcCast_ServerValidate(object source, ServerValidateEventArgs args) {
		this.lblHandlNm.Text = string.Empty;

		string sUserCharNo = this.txtUserCharNo.Text.TrimEnd();
		if (sUserCharNo.Equals(string.Empty)) {
			sUserCharNo = ViCommConst.MAIN_CHAR_NO;
		}

		bool bExistsCharacter = false;
		using (CastCharacter oCastCharacter = new CastCharacter()) {
			bExistsCharacter = oCastCharacter.IsExistLoginId(this.lstSiteCd.SelectedValue, this.txtLoginId.Text, sUserCharNo);
		}
		args.IsValid = bExistsCharacter;
	}
	#endregion ========================================================================================================


	#region □■□ 参照/更新 □■□ ===================================================================================
	private void GetData(string pSiteCd, string pProductAgentCd, string pProductSeq) {

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("PRODUCT_GET");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, pSiteCd);
			oDbSession.ProcedureInParm("pPRODUCT_AGENT_CD", DbSession.DbType.VARCHAR2, pProductAgentCd);
			oDbSession.ProcedureBothParm("pPRODUCT_SEQ", DbSession.DbType.VARCHAR2, pProductSeq);
			oDbSession.ProcedureBothParm("pLINK_ID", DbSession.DbType.VARCHAR2, null);
			oDbSession.ProcedureBothParm("pPRODUCT_ID", DbSession.DbType.VARCHAR2, null);
			oDbSession.ProcedureOutParm("pPRODUCT_TYPE", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPRODUCT_NM", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPRODUCT_DISCRIPTION", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPART_NUMBER", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPRODUCT_SERIES_SUMMARY", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pCAST_SUMMARY", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPRODUCT_MAKER_SUMMARY", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPUBLISH_START_DATE", DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pPUBLISH_END_DATE", DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pRELEASE_DATE", DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pGROSS_PRICE", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pCHARGE_POINT", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSPECIAL_CHARGE_POINT", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pPUBLISH_FLAG", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pDEL_FLAG", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pREMARKS", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pUNEDITABLE_FLAG", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pNOT_NEW_FLAG", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pROYALTY_RATE", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pREVISION_NO", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutArrayParm("pPRODUCT_GENRE_CATEGORY_CD_ARR", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutArrayParm("pPRODUCT_GENRE_CD_ARR", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutArrayParm("pPRODUCT_KEYWORD_VALUE_ARR", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutArrayParm("pUNEDITABLE_FLAG_ARR", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutArrayParm("pUSER_SEQ_ARR", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutArrayParm("pUSER_CHAR_NO_ARR", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutArrayParm("pPICKUP_ID_ARR", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pAUCTION_STATUS", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pAUCTION_START_DATE", DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pBLIND_END_DATE", DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pAUCTION_END_DATE", DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pRESERVE_AMT", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pMINIMUM_BID_AMT", DbSession.DbType.NUMBER);			
			oDbSession.ProcedureOutParm("pENTRY_POINT", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pBLIND_ENTRY_POINT", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pBID_POINT", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pBLIND_BID_POINT", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pAUTO_EXTENSION_FLAG", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pAUCTION_REVISION_NO", DbSession.DbType.NUMBER);			
			oDbSession.ProcedureOutParm("pRECORD_COUNT", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();

			if (int.Parse(oDbSession.GetStringValue("PRECORD_COUNT")) == 0) {
				this.lblProductId.Text = "新規登録";
				this.lblProductId.ForeColor = System.Drawing.Color.Red;
				this.hdnUneditableFlag.Value = ViCommConst.FLAG_OFF_STR;
				this.hdnIsInitial.Value = true.ToString();
				this.SetupCreateMode(false);
			} else {
				string sProductId = oDbSession.GetStringValue("pPRODUCT_ID");
				if (oDbSession.GetStringValue("pUNEDITABLE_FLAG").Equals(ViCommConst.FLAG_ON_STR)) {
					this.SetupUnEditable();
				}

				this.lstSiteCd.SelectedValue = pSiteCd;
				this.lstProductAgent.SelectedValue = pProductAgentCd;
				this.lstProductType.SelectedValue = oDbSession.GetStringValue("pPRODUCT_TYPE");
				this.hdnProductSeq.Value = oDbSession.GetStringValue("pPRODUCT_SEQ");
				this.lblProductId.Text = string.Format("{0} ({1})", sProductId, pProductSeq);
				this.ProductId = sProductId;
				this.txtProductNm.Text = oDbSession.GetStringValue("pPRODUCT_NM");
				this.txtPartNumber.Text = oDbSession.GetStringValue("pPART_NUMBER");
				this.txtProcuctDiscription.Text = oDbSession.GetStringValue("pPRODUCT_DISCRIPTION");
				this.txtProductSeriesSummary.Text = oDbSession.GetStringValue("pPRODUCT_SERIES_SUMMARY");
				this.txtCastSummary.Text = oDbSession.GetStringValue("pCAST_SUMMARY");
				this.txtProductMakerSummary.Text = oDbSession.GetStringValue("pPRODUCT_MAKER_SUMMARY");
				this.txtPublishStartDate.Text = string.Format("{0:yyyy/MM/dd HH:mm:ss}", oDbSession.GetDateTimeValue("pPUBLISH_START_DATE"));
				this.txtPublishEndDate.Text = string.Format("{0:yyyy/MM/dd HH:mm:ss}", oDbSession.GetDateTimeValue("pPUBLISH_END_DATE"));
				this.txtReleaseDate.Text = string.Format("{0:yyyy/MM/dd}", oDbSession.GetDateTimeValue("pRELEASE_DATE"));
				this.txtGrossPrice.Text = oDbSession.GetStringValue("pGROSS_PRICE");
				this.txtChargePoint.Text = oDbSession.GetStringValue("pCHARGE_POINT");
				this.txtSpecialChargePoint.Text = oDbSession.GetStringValue("pSPECIAL_CHARGE_POINT");
				this.txtRemarks.Text = oDbSession.GetStringValue("pREMARKS");
				this.rdoNotNewFlag.SelectedValue = oDbSession.GetStringValue("pNOT_NEW_FLAG");
				this.txtRoyaltyRate.Text = oDbSession.GetStringValue("pROYALTY_RATE");
				this.hdnUneditableFlag.Value = oDbSession.GetStringValue("pUNEDITABLE_FLAG");
				this.rdoPublish.SelectedValue = oDbSession.GetStringValue("pPUBLISH_FLAG");

				//
				// ｷｰﾜｰﾄﾞ
				//
				for (int i = 0; i < this.rptKeyword.Items.Count; i++) {
					if (oDbSession.GetArrySize("pPRODUCT_KEYWORD_VALUE_ARR") <= i) break;

					string sKeywordValue = oDbSession.GetArryStringValue("pPRODUCT_KEYWORD_VALUE_ARR", i);
					string sKeywordUnEditableFlag = oDbSession.GetArryIntValue("pUNEDITABLE_FLAG_ARR", i);

					RepeaterItem oKeywordItem = this.rptKeyword.Items[i];

					TextBox txtKeywordValue = (TextBox)oKeywordItem.FindControl("txtKeywordValue");
					HiddenField hdnKeywordUnEditableFlag = (HiddenField)oKeywordItem.FindControl("hdnKeywordUnEditableFlag");

					txtKeywordValue.Text = sKeywordValue;
					txtKeywordValue.Enabled = !sKeywordUnEditableFlag.Equals(ViCommConst.FLAG_ON_STR);
					hdnKeywordUnEditableFlag.Value = sKeywordUnEditableFlag;
				}

				//
				// ﾋﾟｯｸｱｯﾌﾟ

				//
				this.rptPickup.DataBind();
				for (int i = 0; i < this.rptPickup.Items.Count; i++) {
					if (oDbSession.GetArrySize("pPICKUP_ID_ARR") <= i) break;

					RepeaterItem oPickupItem = this.rptPickup.Items[i];

					string sPickupId = oDbSession.GetArryStringValue("pPICKUP_ID_ARR", i);

					DropDownList lstPickup = (DropDownList)oPickupItem.FindControl("lstPickup");

					lstPickup.SelectedValue = sPickupId;
				}

				//
				// ジャンル
				//
				for (int i = 0; i < oDbSession.GetArrySize("pPRODUCT_GENRE_CD_ARR"); i++) {
				    string sProductGenreCategoryCd = oDbSession.GetArryStringValue("pPRODUCT_GENRE_CATEGORY_CD_ARR", i);
				    string sProductGenreCd = oDbSession.GetArryStringValue("pPRODUCT_GENRE_CD_ARR", i);

				    this.ProductGenreCategoryList.Add(sProductGenreCategoryCd);
				    this.ProductGenreList.Add(sProductGenreCd);
				}

				//
				// 関連出演者ｷｬﾗｸﾀｰ
				//
				if (oDbSession.GetArrySize("pUSER_SEQ_ARR") > 0) {
					// MEMO:2010/12時点では1件のみ

					string sUserSeq = oDbSession.GetArryStringValue("pUSER_SEQ_ARR", 0);
					string sUserCharNo = oDbSession.GetArryStringValue("pUSER_CHAR_NO_ARR", 0);

					using (CastCharacter oCastCharacter = new CastCharacter()) {
						DataSet oCastCharacterDataSet = oCastCharacter.GetOneByUserSeq(this.lstSiteCd.SelectedValue, sUserSeq, sUserCharNo);
						if (oCastCharacterDataSet.Tables.Count > 0 && oCastCharacterDataSet.Tables[0].Rows.Count > 0) {
							DataRow oCastCharacterDataRow = oCastCharacterDataSet.Tables[0].Rows[0];

							this.txtLoginId.Text = iBridUtil.GetStringValue(oCastCharacterDataRow["LOGIN_ID"]);
							this.txtUserCharNo.Text = iBridUtil.GetStringValue(oCastCharacterDataRow["USER_CHAR_NO"]);
							this.lblHandlNm.Text = iBridUtil.GetStringValue(oCastCharacterDataRow["HANDLE_NM"]);
							if (this.lblHandlNm.Text.Equals(string.Empty)) {
								this.lblHandlNm.Text = "(未設定)";
							}
						}
					}
				}
				
				if(ViCommConst.ProductType.IsAuction(this.lstProductType.SelectedValue)){
					ProcessGetDataAuction(oDbSession);
				}

				this.SetupEditMode();		
			}
		}
	}
	
	private void ProcessGetDataAuction(DbSession pDbSession){
		string sAuctionStatus = pDbSession.GetStringValue("pAUCTION_STATUS");
		using (CodeDtl oCodeDtl = new CodeDtl()) {
			if (oCodeDtl.GetOne(ViCommConst.CODE_TYPE_AUCTION_STATUS, sAuctionStatus)) {
				lblAuctionStatus.Text = oCodeDtl.codeNm;
			}
		}
		txtAuctionStartDate.Text = string.Format("{0:yyyy/MM/dd HH:mm:ss}", pDbSession.GetDateTimeValue("pAUCTION_START_DATE"));
		txtBlindEndDate.Text = string.Format("{0:yyyy/MM/dd HH:mm:ss}", pDbSession.GetDateTimeValue("pBLIND_END_DATE"));
		txtAuctionEndDate.Text = string.Format("{0:yyyy/MM/dd HH:mm:ss}", pDbSession.GetDateTimeValue("pAUCTION_END_DATE"));
		txtReserveAmt.Text = pDbSession.GetStringValue("pRESERVE_AMT");
		txtMinimumBidAmt.Text = pDbSession.GetStringValue("pMINIMUM_BID_AMT");
		txtEntryPoint.Text = pDbSession.GetStringValue("pENTRY_POINT");
		txtBlindEntryPoint.Text = pDbSession.GetStringValue("pBLIND_ENTRY_POINT");
		txtBidPoint.Text = pDbSession.GetStringValue("pBID_POINT");
		txtBlindBidPoint.Text = pDbSession.GetStringValue("pBLIND_BID_POINT");
		chkAutoExtension.Checked = ViCommConst.FLAG_ON_STR.Equals(pDbSession.GetStringValue("pAUTO_EXTENSION_FLAG"));
		hdnAuctionRevisionNo.Value = pDbSession.GetStringValue("pAUCTION_REVISION_NO");

		bool bIsAuctionStatusNormal = ViCommConst.AuctionStatus.Normal.Equals(sAuctionStatus);
		txtAuctionStartDate.Enabled = bIsAuctionStatusNormal;
		txtBlindEndDate.Enabled = bIsAuctionStatusNormal;
		txtAuctionEndDate.Enabled = bIsAuctionStatusNormal;
		txtReserveAmt.Enabled = bIsAuctionStatusNormal;
		txtMinimumBidAmt.Enabled = bIsAuctionStatusNormal;
		txtEntryPoint.Enabled = bIsAuctionStatusNormal;
		txtBlindEntryPoint.Enabled = bIsAuctionStatusNormal;
		txtBidPoint.Enabled = bIsAuctionStatusNormal;
		txtBlindBidPoint.Enabled = bIsAuctionStatusNormal;
		chkAutoExtension.Enabled = bIsAuctionStatusNormal;
	}

	private void UpdateData(bool pIsDelete) {

		string sProductType = this.lstProductType.SelectedValue;

		DateTime? dtPublishStartDate = SysPrograms.TryParseOrDafult(this.txtPublishStartDate.Text, DateTime.MinValue);
		DateTime? dtPublishEndDate = SysPrograms.TryParseOrDafult(this.txtPublishEndDate.Text, DateTime.Parse("2050/12/31"));
		DateTime? dtReleaseDate = SysPrograms.TryParseOrDafult(this.txtReleaseDate.Text, (DateTime?)null);
		DateTime? dtLinkDate = SysPrograms.TryParseOrDafult(this.hdnLinkDate.Value, (DateTime?)null);
		int? iGrossPoint = SysPrograms.TryParseOrDafult(this.txtGrossPrice.Text, 0);
		int? iChargePoint = SysPrograms.TryParseOrDafult(this.txtChargePoint.Text, 0);
		int? iSpecialChargePoint = SysPrograms.TryParseOrDafult(this.txtSpecialChargePoint.Text, 0);
		int? iPublishFlag = SysPrograms.TryParseOrDafult(this.rdoPublish.SelectedValue, 0);
		List<string> oProductGenreCategoryCdList = new List<string>();
		List<string> oProductGenreCdList = new List<string>();
		List<string> oUserSeqList = new List<string>();
		List<string> oUserCharNoList = new List<string>();
		List<string> oUnEditableFlagList = new List<string>();
		List<string> oProductKeywordValueList = new List<string>();
		List<string> oProductPickupIdList = new List<string>();
		int iDeleteFlag = pIsDelete ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF;


		//
		// ｷｰﾜｰﾄﾞ		//
		for (int i = 0; i < this.rptKeyword.Items.Count; i++) {
			RepeaterItem oKeywordItem = this.rptKeyword.Items[i];
			TextBox txtKeywordValue = (TextBox)oKeywordItem.FindControl("txtKeywordValue");
			HiddenField hdnKeywordUnEditableFlag = (HiddenField)oKeywordItem.FindControl("hdnKeywordUnEditableFlag");

			string sKeywordValue = txtKeywordValue.Text;
			int? iKeywordUnEditableFlag = SysPrograms.TryParseOrDafult(hdnKeywordUnEditableFlag.Value, 0);

			if (sKeywordValue.Equals(string.Empty)) continue;

			if (!oProductKeywordValueList.Contains(sKeywordValue)) {
				oProductKeywordValueList.Add(sKeywordValue);
				oUnEditableFlagList.Add(iKeywordUnEditableFlag.ToString());
			}
		}
		//
		// ﾋﾟｯｸｱｯﾌﾟ		//
		for (int i = 0; i < this.rptPickup.Items.Count; i++) {
			RepeaterItem oPickupItem = this.rptPickup.Items[i];
			DropDownList lstPickup = (DropDownList)oPickupItem.FindControl("lstPickup");

			string sPickupId = lstPickup.SelectedValue;

			if (sPickupId.Equals(string.Empty)) continue;

			if (!oProductPickupIdList.Contains(sPickupId)) {
				oProductPickupIdList.Add(sPickupId);
			}
		}

		//
		// ジャンル
		//
		foreach (TreeView tvGenre in this.GetTreeViews()) {
			foreach (TreeNode tnGenreCategory in tvGenre.Nodes) {
				foreach (TreeNode tnGenre in tnGenreCategory.ChildNodes) {
					if (!tnGenre.Checked) continue;

					oProductGenreCategoryCdList.Add(tnGenreCategory.Value);
					oProductGenreCdList.Add(tnGenre.Value);
				}
			}
		}

		//
		// 関連出演者ｷｬﾗｸﾀｰ
		//
		if (!this.txtLoginId.Text.TrimEnd().Equals(string.Empty)) {
			string sLoginId = this.txtLoginId.Text.TrimEnd();
			string sUserCharNo = this.txtUserCharNo.Text.TrimEnd();
			if (sUserCharNo.Equals(string.Empty)) {
				sUserCharNo = ViCommConst.MAIN_CHAR_NO;
			}

			using (CastCharacter oCastCharacter = new CastCharacter()) {
				DataSet oCastCharacterDataSet = oCastCharacter.GetOne(this.lstSiteCd.SelectedValue, sLoginId, sUserCharNo);
				if (oCastCharacterDataSet.Tables.Count > 0 && oCastCharacterDataSet.Tables[0].Rows.Count > 0) {
					DataRow oCastCharacterDataRow = oCastCharacterDataSet.Tables[0].Rows[0];

					oUserSeqList.Add(iBridUtil.GetStringValue(oCastCharacterDataRow["USER_SEQ"]));
					oUserCharNoList.Add(sUserCharNo);
				}
			}
		}

		string sProductId = null;
		string sProductSeq = null;
		if (!this.hdnProductSeq.Value.Equals(string.Empty)) {
			sProductSeq = this.hdnProductSeq.Value;
		}
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("PRODUCT_MAINTE");
			oDbSession.cmd.BindByName = true;
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.lstSiteCd.SelectedValue);
			oDbSession.ProcedureInParm("pPRODUCT_AGENT_CD", DbSession.DbType.VARCHAR2, this.lstProductAgent.SelectedValue);
			oDbSession.ProcedureBothParm("pPRODUCT_SEQ", DbSession.DbType.NUMBER, sProductSeq);
			oDbSession.ProcedureInParm("pPRODUCT_TYPE", DbSession.DbType.VARCHAR2, sProductType);
			oDbSession.ProcedureInParm("pPRODUCT_NM", DbSession.DbType.VARCHAR2, this.txtProductNm.Text);
			oDbSession.ProcedureInParm("pPRODUCT_DISCRIPTION", DbSession.DbType.VARCHAR2, this.txtProcuctDiscription.Text);
			oDbSession.ProcedureInParm("pPART_NUMBER", DbSession.DbType.VARCHAR2, this.txtPartNumber.Text);
			oDbSession.ProcedureInParm("pPRODUCT_SERIES_SUMMARY", DbSession.DbType.VARCHAR2, this.txtProductSeriesSummary.Text);
			oDbSession.ProcedureInParm("pCAST_SUMMARY", DbSession.DbType.VARCHAR2, this.txtCastSummary.Text);
			oDbSession.ProcedureInParm("pPRODUCT_MAKER_SUMMARY", DbSession.DbType.VARCHAR2, this.txtProductMakerSummary.Text);
			oDbSession.ProcedureInParm("pPUBLISH_START_DATE", DbSession.DbType.DATE, dtPublishStartDate);
			oDbSession.ProcedureInParm("pPUBLISH_END_DATE", DbSession.DbType.DATE, dtPublishEndDate);
			oDbSession.ProcedureInParm("pRELEASE_DATE", DbSession.DbType.DATE, dtReleaseDate);
			oDbSession.ProcedureInParm("pGROSS_PRICE", DbSession.DbType.NUMBER, iGrossPoint);
			oDbSession.ProcedureInParm("pCHARGE_POINT", DbSession.DbType.NUMBER, iChargePoint);
			oDbSession.ProcedureInParm("pSPECIAL_CHARGE_POINT", DbSession.DbType.NUMBER, iSpecialChargePoint);
			oDbSession.ProcedureInParm("pPUBLISH_FLAG", DbSession.DbType.NUMBER, iPublishFlag);
			oDbSession.ProcedureInParm("pDEL_FLAG", DbSession.DbType.VARCHAR2, 0);
			oDbSession.ProcedureInParm("pREMARKS", DbSession.DbType.VARCHAR2, this.txtRemarks.Text);
			oDbSession.ProcedureInParm("pLINK_ID", DbSession.DbType.VARCHAR2, this.hdnLinkId.Value);
			oDbSession.ProcedureInParm("pLINK_DATE", DbSession.DbType.DATE, dtLinkDate);
			oDbSession.ProcedureInParm("pUNEDITABLE_FLAG", DbSession.DbType.VARCHAR2, this.hdnUneditableFlag.Value);
			oDbSession.ProcedureInParm("pNOT_NEW_FLAG", DbSession.DbType.NUMBER, this.rdoNotNewFlag.SelectedValue);
			oDbSession.ProcedureInParm("pROYALTY_RATE", DbSession.DbType.NUMBER, SysPrograms.TryParseOrDafult(this.txtRoyaltyRate.Text,0));
			oDbSession.ProcedureInParm("pREVISION_NO", DbSession.DbType.VARCHAR2, this.hdnRevisionNo.Value);
			oDbSession.ProcedureInArrayParm("pPRODUCT_GENRE_CATEGORY_CD_ARR", DbSession.DbType.VARCHAR2, oProductGenreCategoryCdList.ToArray());
			oDbSession.ProcedureInArrayParm("pPRODUCT_GENRE_CD_ARR", DbSession.DbType.VARCHAR2, oProductGenreCdList.ToArray());
			oDbSession.ProcedureInArrayParm("pPRODUCT_KEYWORD_VALUE_ARR", DbSession.DbType.VARCHAR2, oProductKeywordValueList.ToArray());
			oDbSession.ProcedureInArrayParm("pUNEDITABLE_FLAG_ARR", DbSession.DbType.NUMBER, oUnEditableFlagList.ToArray());
			oDbSession.ProcedureInArrayParm("pUSER_SEQ_ARR", DbSession.DbType.VARCHAR2, oUserSeqList.ToArray());
			oDbSession.ProcedureInArrayParm("pUSER_CHAR_NO_ARR", DbSession.DbType.VARCHAR2, oUserCharNoList.ToArray());
			oDbSession.ProcedureInArrayParm("pPICKUP_ID_ARR", DbSession.DbType.VARCHAR2, oProductPickupIdList.ToArray());
			if (ViCommConst.ProductType.IsAuction(sProductType)) {
				ProcessUpdateDataAuction(oDbSession);
			}			
			oDbSession.ProcedureInParm("pDELETE_FLAG", DbSession.DbType.NUMBER, iDeleteFlag);
			oDbSession.ProcedureOutParm("pPRODUCT_ID", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			sProductId = oDbSession.GetStringValue("pPRODUCT_ID");
		}

		if (pIsDelete) {
			Response.Redirect("ProductInquery.aspx");
		} else {
			UrlBuilder oUrlBuilder = new UrlBuilder("ProductView.aspx");
			oUrlBuilder.Parameters.Add("product_id", sProductId);
			Response.Redirect(oUrlBuilder.ToString());
		}
	}

	private void ProcessUpdateDataAuction(DbSession pDbSession) {

		DateTime? dtAuctionStartDate = SysPrograms.TryParseOrDafult(this.txtAuctionStartDate.Text,(DateTime?)null);
		DateTime? dtBlindEndDate = SysPrograms.TryParseOrDafult(this.txtBlindEndDate.Text, (DateTime?)null);
		DateTime? dtAuctionEndDate = SysPrograms.TryParseOrDafult(this.txtAuctionEndDate.Text, (DateTime?)null);
		int? iReserveAmt = SysPrograms.TryParseOrDafult(this.txtReserveAmt.Text, (int?)null); ;
		int? iMinimumBidAmt = SysPrograms.TryParseOrDafult(this.txtMinimumBidAmt.Text, (int?)null); ;
		int? iEntryPoint = SysPrograms.TryParseOrDafult(this.txtEntryPoint.Text, (int?)null); ;
		int? iBlindEntryPoint = SysPrograms.TryParseOrDafult(this.txtBlindEntryPoint.Text, (int?)null); ;
		int? iBidPoint = SysPrograms.TryParseOrDafult(this.txtBidPoint.Text, (int?)null); ;
		int? iBlindBidPoint = SysPrograms.TryParseOrDafult(this.txtBlindBidPoint.Text, (int?)null); ;

		pDbSession.ProcedureInParm("pAUCTION_START_DATE", DbSession.DbType.DATE, dtAuctionStartDate);
		pDbSession.ProcedureInParm("pBLIND_END_DATE", DbSession.DbType.DATE, dtBlindEndDate);
		pDbSession.ProcedureInParm("pAUCTION_END_DATE", DbSession.DbType.DATE, dtAuctionEndDate);
		pDbSession.ProcedureInParm("pRESERVE_AMT", DbSession.DbType.NUMBER, iReserveAmt);
		pDbSession.ProcedureInParm("pMINIMUM_BID_AMT", DbSession.DbType.NUMBER, iMinimumBidAmt);
		pDbSession.ProcedureInParm("pENTRY_POINT", DbSession.DbType.NUMBER, iEntryPoint);
		pDbSession.ProcedureInParm("pBLIND_ENTRY_POINT", DbSession.DbType.NUMBER, iBlindEntryPoint);
		pDbSession.ProcedureInParm("pBID_POINT", DbSession.DbType.NUMBER, iBidPoint);
		pDbSession.ProcedureInParm("pBLIND_BID_POINT", DbSession.DbType.NUMBER, iBlindBidPoint);
		pDbSession.ProcedureInParm("pAUTO_EXTENSION_FLAG", DbSession.DbType.NUMBER, this.chkAutoExtension.Checked ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF);
		pDbSession.ProcedureInParm("pAUCTION_REVISION_NO", DbSession.DbType.NUMBER, this.hdnAuctionRevisionNo.Value);
	}
	#endregion ========================================================================================================

	private void InitPage() {
		this.rptKeyword.DataBind();
		this.rptPickup.DataBind();
		this.lstSiteCd.DataBind();
		this.SetupProductTypeDropDownList();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void SetupEditMode() {
		this.lstSiteCd.Enabled = false;
		this.lstProductAgent.Enabled = false;
		this.lstProductType.Enabled = false;

		this.btnCancel.Visible = true;
		this.btnDelete.Visible = true;

		bool bIsAuction = ViCommConst.ProductType.IsAuction(this.lstProductType.SelectedValue);
		this.pnlProductInfoDetail.Visible = !bIsAuction;
		this.pnlAuctionInfo.Visible = bIsAuction;

		if (!bIsAuction) {
			this.pnlProductBasicInfo.Enabled = true;
			this.pnlPickup.Enabled = true;
			this.pnlProductGenre.Enabled = true;
			this.pnlKeyword.Enabled = true;
		}
	}

	private void SetupUnEditable() {
		this.lstSiteCd.Enabled = false;
		this.lstProductAgent.Enabled = false;
		this.lstProductType.Enabled = false;
		this.txtProductNm.Enabled = false;

		this.txtPartNumber.Enabled = false;
		this.txtProcuctDiscription.Enabled = false;
		this.txtProductMakerSummary.Enabled = false;
		this.txtProductSeriesSummary.Enabled = false;
		this.txtPublishStartDate.Enabled = false;
		this.txtPublishEndDate.Enabled = false;
		this.txtReleaseDate.Enabled = false;
		this.txtGrossPrice.Enabled = false;
		this.txtCastSummary.Enabled = false;

		this.btnDelete.Enabled = false;
	}
	private void SetupCreateMode(bool isPanelVisible) {
		this.pnlCreate.Visible = true;
		this.pnlProductBasicInfo.Visible = isPanelVisible;
		this.pnlAuctionInfo.Visible = isPanelVisible;
		this.pnlProductGenre.Visible = isPanelVisible;
		this.pnlKeyword.Visible = isPanelVisible;
		this.pnlCommand.Visible = isPanelVisible;
		this.pnlPickup.Visible = isPanelVisible;

		this.lstProductAgent.Enabled = !isPanelVisible;
		this.lstProductType.Enabled = !isPanelVisible;
		this.lstSiteCd.Enabled = !isPanelVisible;
		
		this.btnCreate.Enabled = !isPanelVisible;
		if (isPanelVisible) {
			this.rptPickup.DataBind();
			
			bool bIsAuction = ViCommConst.ProductType.IsAuction(this.lstProductType.SelectedValue);
			this.pnlProductInfoDetail.Visible = !bIsAuction;
			this.pnlAuctionInfo.Visible = bIsAuction;
		}
		
		
	}

	private void SetupProductTypeDropDownList() {
		this.lstProductType.Items.Clear();
		this.lstProductType.DataBind();
		this.lstProductType.Items.Insert(0, new ListItem(string.Empty, string.Empty));
	}

	private void SetupProductAgentDropDownList() {
		this.lstProductAgent.Items.Clear();
		this.lstProductAgent.DataBind();
		this.lstProductAgent.Items.Insert(0, new ListItem(string.Empty, string.Empty));
	}

	private void SetupProductGenreTreeView() {
		this.tblProductGenre.Rows.Clear();

		string pSiteCd = this.lstSiteCd.SelectedValue;
		string pProductType = this.lstProductType.SelectedValue;
		bool bHasGenre = false;
		bool bShowNotExists = false;

		TableRow row = new TableRow();
		this.tblProductGenre.Rows.Add(row);

		using (ProductGenre oProductGenre = new ProductGenre())
		using (ProductGenreCategory oProductGenreCategory = new ProductGenreCategory()) {
			using (DataSet oGenreCategoryDs = oProductGenreCategory.GetList(pSiteCd, pProductType)) {
				foreach (DataRow oGenreCategoryDr in oGenreCategoryDs.Tables[0].Rows) {
					bHasGenre = false;

					TableCell cell = new TableCell();
					cell.VerticalAlign = VerticalAlign.Top;
					row.Cells.Add(cell);

					TreeView oTreeView = new TreeView();
					oTreeView.ID = string.Format("tvGenre{0}{1}", this.tblProductGenre.Rows.Count, row.Cells.Count);

					string sGenreCategoryCd = iBridUtil.GetStringValue(oGenreCategoryDr["PRODUCT_GENRE_CATEGORY_CD"]);

					TreeNode tnGenreCategory = new TreeNode();
					tnGenreCategory.Value = sGenreCategoryCd;
					tnGenreCategory.Text = iBridUtil.GetStringValue(oGenreCategoryDr["PRODUCT_GENRE_CATEGORY_NM"]);

					using (DataSet oGenreDs = oProductGenre.GetList(pSiteCd, sGenreCategoryCd)) {
						foreach (DataRow oGenreDr in oGenreDs.Tables[0].Rows) {
							string sGenreCd = iBridUtil.GetStringValue(oGenreDr["PRODUCT_GENRE_CD"]);

							TreeNode tnGenre = new TreeNode();
							tnGenre.ShowCheckBox = true;
							tnGenre.Text = iBridUtil.GetStringValue(oGenreDr["PRODUCT_GENRE_NM"]);
							tnGenre.Value = sGenreCd;
							tnGenreCategory.ChildNodes.Add(tnGenre);

							bHasGenre = true;
							bShowNotExists = !bHasGenre;
						}
					}

					tnGenreCategory.Expanded = true;
					oTreeView.Nodes.Add(tnGenreCategory);
					oTreeView.ShowExpandCollapse = false;
					oTreeView.ShowLines = true;
					oTreeView.Visible = bHasGenre;

					cell.Controls.Add(oTreeView);

					if (row.Cells.Count > MAX_COUNT_OF_GENRE) {
						row = new TableRow();
						this.tblProductGenre.Rows.Add(row);
					}
				}
			}
		}
		this.lblGenreNothingMessage.Visible = bShowNotExists;

		for (int i = 0; i < this.ProductGenreList.Count; i++) {

			string sProductGenreCategoryCd = this.ProductGenreCategoryList[i];
			string sProductGenreCd = this.ProductGenreList[i];

			foreach (TreeView oTreeView in this.GetTreeViews()) {
				TreeNode oTreeNode = oTreeView.FindNode(string.Format("{0}/{1}", sProductGenreCategoryCd, sProductGenreCd));

				if (oTreeNode != null) {
					oTreeNode.Checked = true;
				}
			}
		}
	}

	private bool GetMultiCharFlag(string pSiteCd) {
		bool bMultiCharFlag = false;
		using (Site oSite = new Site()) {
			if (oSite.GetMultiCharFlag(pSiteCd)) {
				bMultiCharFlag = true;
			}
		}
		return bMultiCharFlag;
	}

	private IEnumerable<TreeView> GetTreeViews() {
		int iRowIndex = 1;
		int iColIndex = 1;
		foreach (TableRow oTableRow in this.tblProductGenre.Rows) {
			foreach (TableCell oTableCell in oTableRow.Cells) {
				TreeView oTreeView = oTableCell.FindControl(string.Format("tvGenre{0}{1}", iRowIndex, iColIndex)) as TreeView;
				if (oTreeView != null) {
					yield return oTreeView;
				}
				iColIndex++;
			}
			iColIndex = 1;
			iRowIndex++;
		}
	}

	protected void vdcAuctionStartDate_SeverValidate(object source, ServerValidateEventArgs args) {
		if (!this.IsValid) {
			return;
		}

		DateTime dtAuctionStart;
		if (!DateTime.TryParse(this.txtAuctionStartDate.Text, out dtAuctionStart)) {
			args.IsValid = false;
			this.vdcAuctionStartDate.ErrorMessage = "ｵｰｸｼｮﾝ開始日時を入力してください。";
			return;
		}
	}

	protected void vdcBlindEndDate_SeverValidate(object source, ServerValidateEventArgs args) {
		if (!this.IsValid) {
			return;
		}

		DateTime dtAuctionStart = DateTime.Parse(this.txtAuctionStartDate.Text);
		DateTime dtBlindEnd;
		if (!DateTime.TryParse(this.txtBlindEndDate.Text, out dtBlindEnd)) {
			args.IsValid = false;
			this.vdcBlindEndDate.ErrorMessage = "ﾌﾞﾗｲﾝﾄﾞ終了日時を入力してください。";
			return;
		}
		if (dtBlindEnd < dtAuctionStart) {
			args.IsValid = false;
			this.vdcBlindEndDate.ErrorMessage = "ｵｰｸｼｮﾝ開始日時以降を入力してください。";
			return;
		}
	}

	protected void vdcAuctionEndDate_SeverValidate(object source, ServerValidateEventArgs args) {
		if (!this.IsValid || !args.IsValid) {
			return;
		}

		DateTime dtBlindEnd = DateTime.Parse(this.txtBlindEndDate.Text);
		DateTime dtAuctionStart = DateTime.Parse(this.txtAuctionStartDate.Text);
		DateTime dtAuctionEnd;
		if (!DateTime.TryParse(this.txtAuctionEndDate.Text, out dtAuctionEnd)) {
			args.IsValid = false;
			this.vdcAuctionEndDate.ErrorMessage = "ｵｰｸｼｮﾝ終了日時を入力してください。";
			return;
		}
		if (dtAuctionEnd < dtAuctionStart) {
			args.IsValid = false;
			this.vdcAuctionEndDate.ErrorMessage = "ｵｰｸｼｮﾝ開催期間の大小関係関係が不正です。";
			return;
		}
		if (dtAuctionEnd < dtBlindEnd) {
			args.IsValid = false;
			this.vdcAuctionEndDate.ErrorMessage = "ﾌﾞﾗｲﾝﾄﾞ終了日時との大小関係関係が不正です。";
			return;
		}
	}

	protected void vdcPublishStartEnd_ServerValidate(object source, ServerValidateEventArgs args) {
		DateTime? dtPublishStart = SysPrograms.TryParseOrDafult(this.txtPublishStartDate.Text, DateTime.MinValue);
		DateTime? dtPublishEnd = SysPrograms.TryParseOrDafult(this.txtPublishEndDate.Text, DateTime.Parse("2050/12/31"));

		if (dtPublishEnd < dtPublishStart) {
			args.IsValid = false;
			this.vdcPublishStartEnd.ErrorMessage = "公開期間の大小関係が不正です。";
			return;
		}

		DateTime dtAuctionEnd;
		if (this.pnlAuctionInfo.Visible && DateTime.TryParse(this.txtAuctionEndDate.Text, out dtAuctionEnd)) {
			if (dtPublishEnd.Value < dtAuctionEnd) {
				args.IsValid = false;
				this.vdcPublishStartEnd.ErrorMessage = "ｵｰｸｼｮﾝ終了日時との大小関係が不正です。";
				return;
			}
		}
	}


}
