﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ProductMovieMainte.aspx.cs" Inherits="Product_ProductMovieMainte" Title="動画設定" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="動画設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlMainte">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <fieldset class="fieldset-inner">
                    <legend>
                        <%= DisplayWordUtil.Replace("[動画設定]") %>
                    </legend>
                    <table border="0" style="width: 640px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                ｻｲﾄ／商品ID.
                            </td>
                            <td class="tdDataStyle">
                                <asp:Label ID="lblSiteCd" runat="server" Text="<%# this.SiteCd %>"></asp:Label>-
                                <asp:Label ID="lblProductId" runat="server" Text="<%# this.ProductId %>"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                連携ID.</td>
                            <td class="tdDataStyle">
                                <asp:Label ID="lblLinkId" runat="server" Text="<%# this.LinkId %>"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                商品種別.
                            </td>
                            <td class="tdDataStyle">
                                <asp:Label ID="lblProductType" runat="server" Text="Label"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                動画種別.
                            </td>
                            <td class="tdDataStyle">
                                <asp:Label ID="lblProductMovieType" runat="server" Text="Label"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                商品名.
                            </td>
                            <td class="tdDataStyle">
                                <asp:Label ID="lblProductNm" runat="server" Text="Label"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                再生時間
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtPlayTime" runat="server" Text="<%# this.PlayTime %>" CausesValidation="true"></asp:TextBox>
                                <ajaxToolkit:MaskedEditExtender ID="mskPlayTime" runat="server" MaskType="Time" Mask="99:99:99"
                                    UserDateFormat="None" ClearMaskOnLostFocus="true" TargetControlID="txtPlayTime">
                                </ajaxToolkit:MaskedEditExtender>
                                <% // 必須チェック %>
                                <asp:RequiredFieldValidator ID="vdrPlayTime" runat="server" ErrorMessage="再生時間を入力してください。"
                                    ControlToValidate="txtPlayTime" ValidationGroup="Update" Display="Dynamic">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="vcePlayTime1" runat="Server" HighlightCssClass="validatorCallout"
                                    TargetControlID="vdrPlayTime">
                                </ajaxToolkit:ValidatorCalloutExtender>
                            </td>
                        </tr>
                    </table>
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click"
                        ValidationGroup="Update" />
                    <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click"
                        ValidationGroup="Update" />
                    <asp:Button runat="server" ID="btnCancel" Text="戻る" CssClass="seekbutton" OnClick="btnCancel_Click"
                        CausesValidation="False" />
                    <asp:Label runat="server" ID="lblUpdateErrer" Text="*更新先のデータがありません。"  ForeColor="red" Visible="false" />
                </fieldset>
            </fieldset>
        </asp:Panel>
    </div>
    <% // ====================================	%>
    <% //  Extender								%>
    <% // ====================================	%>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="動画情報を更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete"
        ConfirmText="動画情報を削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>
