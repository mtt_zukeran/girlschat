﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using iBridCommLib;
using ViComm;
using System.Collections.Generic;

public partial class Product_ProductMovieMainte : System.Web.UI.Page {
    protected string ProductId {
        get { return iBridUtil.GetStringValue(this.ViewState["ProductId"]); }
        private set { this.ViewState["ProductId"] = value; }
    }
    protected string SiteCd {
        get { return iBridUtil.GetStringValue(this.ViewState["SiteCd"]); }
        private set { this.ViewState["SiteCd"] = value; }
    }
    protected string ProductAgentCd {
        get { return iBridUtil.GetStringValue(this.ViewState["ProductAgentCd"]); }
        private set { this.ViewState["ProductAgentCd"] = value; }
    }
    protected string ProductSeq {
        get { return iBridUtil.GetStringValue(this.ViewState["ProductSeq"]); }
        private set { this.ViewState["ProductSeq"] = value; }
    }
    protected string ObjSeq {
        get { return iBridUtil.GetStringValue(this.ViewState["ObjSeq"]); }
        private set { this.ViewState["ObjSeq"] = value; }
    }
    protected string LinkId {
        get { return iBridUtil.GetStringValue(this.ViewState["LinkId"]); }
        private set { this.ViewState["LinkId"] = value; }
    }
    protected string LinkDate {
        get { return iBridUtil.GetStringValue(this.ViewState["LinkDate"]); }
        private set { this.ViewState["LinkDate"] = value; }
    }
    protected string ProductType {
        get { return iBridUtil.GetStringValue(this.ViewState["ProductType"]); }
        private set { this.ViewState["ProductType"] = value; }
    }
    protected string ProductMovieType {
        get { return iBridUtil.GetStringValue(this.ViewState["ProductMovieType"]); }
        private set { this.ViewState["ProductMovieType"] = value; }
    }
    protected string PlayTime {
        get { return iBridUtil.GetStringValue(this.ViewState["PlayTime"]); }
        private set { this.ViewState["PlayTime"] = value; }
    }
    protected string UneditableFlag {
        get { return iBridUtil.GetStringValue(this.ViewState["UneditableFlag"]); }
        private set { this.ViewState["UneditableFlag"] = value; }
    }
    protected string OuterMovieFlag {
        get { return iBridUtil.GetStringValue(this.ViewState["OuterMovieFlag"]); }
        private set { this.ViewState["OuterMovieFlag"] = value; }
    }
    protected string RevisionNo {
        get { return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]); }
        private set { this.ViewState["RevisionNo"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e) {
        if (!this.IsPostBack) {
            this.ProductId = iBridUtil.GetStringValue(this.Request["product_id"]);
            this.ObjSeq = iBridUtil.GetStringValue(this.Request["object_seq"]);
            this.lblUpdateErrer.Visible =false;
            this.GetData(this.ProductId, this.ObjSeq);
        }
    }
    
    protected void btnUpdate_Click(object sender, EventArgs e) {
        this.UpdateData(false);
    }

    protected void btnDelete_Click(object sender, EventArgs e) {
        this.UpdateData(true);
    }

    protected void btnCancel_Click(object sender, EventArgs e) {
        UrlBuilder oUrlBuilder = new UrlBuilder("ProductView.aspx");
        oUrlBuilder.Parameters.Add("product_id", this.ProductId);
        Response.Redirect(oUrlBuilder.ToString());
    }
    
    private void GetData(string pProductId, string pObjectSeq) {
        string sProductName = string.Empty;

        using (Product oProduct = new Product()) {
            using (DataSet ds = oProduct.GetOne(pProductId)) {
                DataRow dr = ds.Tables[0].Rows[0];
                this.SiteCd = iBridUtil.GetStringValue(dr["SITE_CD"]);
                this.ProductAgentCd = iBridUtil.GetStringValue(dr["PRODUCT_AGENT_CD"]);
                this.ProductSeq = iBridUtil.GetStringValue(dr["PRODUCT_SEQ"]);
                this.ProductType = iBridUtil.GetStringValue(dr["PRODUCT_TYPE"]);
                sProductName = iBridUtil.GetStringValue(dr["PRODUCT_NM"]);
            }
        }

        using (CodeDtl oCodeDtl = new CodeDtl())
        using (DbSession oDbSession = new DbSession()) {
            oDbSession.PrepareProcedure("PRODUCT_MOVIE_GET");
            oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.SiteCd);
            oDbSession.ProcedureInParm("pPRODUCT_AGENT_CD", DbSession.DbType.VARCHAR2, this.ProductAgentCd);
            oDbSession.ProcedureInParm("pPRODUCT_SEQ", DbSession.DbType.NUMBER, this.ProductSeq);
            oDbSession.ProcedureInParm("pOBJ_SEQ", DbSession.DbType.NUMBER, this.ObjSeq);
            oDbSession.ProcedureOutParm("pLINK_ID", DbSession.DbType.VARCHAR2);
            oDbSession.ProcedureOutParm("pLINK_DATE", DbSession.DbType.DATE);
            oDbSession.ProcedureOutParm("pUPLOAD_DATE", DbSession.DbType.DATE);
            oDbSession.ProcedureOutParm("pPRODUCT_MOVIE_TYPE", DbSession.DbType.VARCHAR2);
            oDbSession.ProcedureOutParm("pPLAY_TIME", DbSession.DbType.VARCHAR2);
            oDbSession.ProcedureOutParm("pUNEDITABLE_FLAG", DbSession.DbType.NUMBER);
            oDbSession.ProcedureOutParm("pOUTER_MOVIE_FLAG", DbSession.DbType.NUMBER);
            oDbSession.ProcedureOutParm("pREVISION_NO", DbSession.DbType.NUMBER);
            oDbSession.ProcedureOutParm("pRECORD_COUNT", DbSession.DbType.NUMBER);
            oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
            oDbSession.cmd.BindByName = true;
            oDbSession.ExecuteProcedure();

            this.LinkId = oDbSession.GetStringValue("pLINK_ID");
            this.LinkDate = oDbSession.GetStringValue("pLINK_DATE");
            this.ProductMovieType = oDbSession.GetStringValue("pPRODUCT_MOVIE_TYPE");
            this.PlayTime = oDbSession.GetStringValue("pPLAY_TIME");
            this.UneditableFlag = oDbSession.GetStringValue("pUNEDITABLE_FLAG");
            this.OuterMovieFlag = oDbSession.GetStringValue("pOUTER_MOVIE_FLAG");
            this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");

            this.pnlMainte.DataBind();

            this.lblProductNm.Text = sProductName;
            if (this.lblLinkId.Text.Equals(string.Empty)) {
                this.lblLinkId.Text = "(なし)";
            }
            if (oCodeDtl.GetOne(ViCommConst.CODE_TYPE_PRODUCT_MOVIE_TYPE, this.ProductMovieType)) {
                this.lblProductMovieType.Text = oCodeDtl.codeNm;
            }
            if (oCodeDtl.GetOne(ViCommConst.CODE_TYPE_PRODUCT_TYPE, this.ProductType)) {
                this.lblProductType.Text = oCodeDtl.codeNm;
            }
        }
    }

    private void UpdateData(bool pIsDelete) {

        List<string> oCpSizeTypeList = new List<string>();
        List<string> oCpFileFormatList = new List<string>();
        List<string> oCpTargetCarrierList = new List<string>();
        List<string> oProductMovieSubseqList = new List<string>();
        List<string> oFileNmList = new List<string>();
        List<string> oFileSizeList = new List<string>();
        List<string> oDownloadUrlList = new List<string>();
        List<string> oLinkIdList = new List<string>();

        int iDeleteFlag = pIsDelete ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF;

		// ========================
		// ★ 登録済チェック
		// ========================
		using (ProductMovie oProductMovie = new ProductMovie()) {
			if (oProductMovie.GetCount(this.SiteCd,this.ProductAgentCd,this.ProductSeq,this.ProductMovieType) <= 0) {
				this.lblUpdateErrer.Visible =true;
				return;
			}
		}

		

        using (DbSession oDbSession = new DbSession()) {
            oDbSession.PrepareProcedure("PRODUCT_MOVIE_MAINTE");
            oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.SiteCd);
            oDbSession.ProcedureInParm("pPRODUCT_AGENT_CD", DbSession.DbType.VARCHAR2, this.ProductAgentCd);
            oDbSession.ProcedureInParm("pPRODUCT_SEQ", DbSession.DbType.NUMBER, this.ProductSeq);
            oDbSession.ProcedureInParm("pOBJ_SEQ", DbSession.DbType.NUMBER, this.ObjSeq);
            oDbSession.ProcedureInParm("pPRODUCT_MOVIE_TYPE", DbSession.DbType.VARCHAR2, this.ProductMovieType);
            oDbSession.ProcedureInParm("pPLAY_TIME", DbSession.DbType.VARCHAR2, this.txtPlayTime.Text);
            oDbSession.ProcedureInParm("pUNEDITABLE_FLAG", DbSession.DbType.NUMBER, this.UneditableFlag);
            oDbSession.ProcedureInParm("pOUTER_MOVIE_FLAG", DbSession.DbType.NUMBER, this.OuterMovieFlag);
            oDbSession.ProcedureInParm("pLINK_ID", DbSession.DbType.VARCHAR2, this.LinkId);
            oDbSession.ProcedureInParm("pREVISION_NO", DbSession.DbType.NUMBER, this.RevisionNo);
            //oDbSession.ProcedureInParm("pWITH_DTL_MAINTE", DbSession.DbType.NUMBER, oWithDtlMainte);  default使用
            oDbSession.ProcedureInArrayParm("pCP_SIZE_TYPE_ARR", DbSession.DbType.VARCHAR2, oCpSizeTypeList.ToArray());
            oDbSession.ProcedureInArrayParm("pCP_FILE_FORMAT_ARR", DbSession.DbType.VARCHAR2, oCpFileFormatList.ToArray());
            oDbSession.ProcedureInArrayParm("pCP_TARGET_CARRIER_ARR", DbSession.DbType.VARCHAR2, oCpTargetCarrierList.ToArray());
            oDbSession.ProcedureInArrayParm("pPRODUCT_MOVIE_SUBSEQ_ARR", DbSession.DbType.NUMBER, oProductMovieSubseqList.ToArray());
            oDbSession.ProcedureInArrayParm("pFILE_NM_ARR", DbSession.DbType.VARCHAR2, oFileNmList.ToArray());
            oDbSession.ProcedureInArrayParm("pFILE_SIZE_ARR", DbSession.DbType.NUMBER, oFileSizeList.ToArray());
            oDbSession.ProcedureInArrayParm("pDOWNLOAD_URL_ARR", DbSession.DbType.VARCHAR2, oDownloadUrlList.ToArray());
            oDbSession.ProcedureInArrayParm("pLINK_ID_ARR", DbSession.DbType.VARCHAR2, oLinkIdList.ToArray());
            oDbSession.ProcedureInParm("pDELETE_FLAG", DbSession.DbType.NUMBER, iDeleteFlag);
            //oDbSession.ProcedureInParm("pWITHOUT_COMMIT", DbSession.DbType.NUMBER, pWithoutCommit);   default使用
            oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
            oDbSession.cmd.BindByName = true;
            oDbSession.ExecuteProcedure();
        }

        UrlBuilder oUrlBuilder = new UrlBuilder("ProductView.aspx");
        oUrlBuilder.Parameters.Add("product_id", this.ProductId);
        Response.Redirect(oUrlBuilder.ToString());
    }
}
