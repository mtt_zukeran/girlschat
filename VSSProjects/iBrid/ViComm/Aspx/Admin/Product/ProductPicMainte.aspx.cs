﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 動画アップロード設定
--	Progaram ID		: ProductPicMainte
--
--  Creation Date	: 2010.12.08
--  Creater			: iBrid
--
**************************************************************************/

using System;
using System.IO;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Product_ProductPicMainte : System.Web.UI.Page {
    protected string ProductId {
        get { return iBridUtil.GetStringValue(this.ViewState["ProductId"]); }
        private set { this.ViewState["ProductId"] = value; }
    }
	protected string ProductPicType {
		get { return iBridUtil.GetStringValue(this.ViewState["ProductPicType"]); }
		private set { this.ViewState["ProductPicType"] = value; }
	}

    protected void Page_Load(object sender, EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
        if (!IsPostBack) {
            InitPage();
        }
    }

    private void InitPage() {

        this.ProductId = iBridUtil.GetStringValue(this.Request["product_id"]);
        this.ProductPicType = iBridUtil.GetStringValue(this.Request["product_pic_type"]);

        this.GetData(this.ProductId, this.ProductPicType);

    }

    private void GetData(string pProductId, string sProductPicType) {

        using (Product pro = new Product()) {
            DataSet ds = pro.GetOne(pProductId);
            DataRow dr = ds.Tables[0].Rows[0];

            lblHeader.Text = "ｻｲﾄ／商品ID.";
            lblSiteCd.Text = dr["SITE_CD"].ToString();
            lblProductId.Text = dr["PRODUCT_ID"].ToString();
            lblLinkId.Text = dr["LINK_ID"].ToString();
            if (this.lblLinkId.Text.Equals(string.Empty)) {
                this.lblLinkId.Text = "(なし)";
            }
            lblProductNm.Text = dr["PRODUCT_NM"].ToString();

            ViewState["SITE_CD"] = dr["SITE_CD"].ToString();
            ViewState["PRODUCT_AGENT_CD"] = dr["PRODUCT_AGENT_CD"].ToString();
            ViewState["PRODUCT_SEQ"] = dr["PRODUCT_SEQ"].ToString();
            ViewState["PRODUCT_PIC_TYPE"] = sProductPicType;

            using (CodeDtl oCodeDtl = new CodeDtl()) {
                if (oCodeDtl.GetOne(ViCommConst.CODE_TYPE_PRODUCT_PIC_TYPE, iBridUtil.GetStringValue(ViewState["PRODUCT_PIC_TYPE"]))) {
                    lblProductPicType.Text = oCodeDtl.codeNm;
                }
                if (oCodeDtl.GetOne(ViCommConst.CODE_TYPE_PRODUCT_TYPE, iBridUtil.GetStringValue(iBridUtil.GetStringValue(dr["PRODUCT_TYPE"])))) {
                    lblProductType.Text = oCodeDtl.codeNm;
                }
            }
        }
    }

    protected void btnUpload_Click(object sender, EventArgs e) {
        string sSiteCd = iBridUtil.GetStringValue(ViewState["SITE_CD"]);

        string sWebPhisicalDir = string.Empty;
        string sDomain = string.Empty;

        using (Site oSite = new Site()) {
            oSite.GetValue(sSiteCd, "WEB_PHISICAL_DIR", ref sWebPhisicalDir);
            oSite.GetValue(sSiteCd, "HOST_NM", ref sDomain);
        }

        using (ProductPic proPic = new ProductPic()) {
            // 登録済チェック
			if (ViCommConst.ProductPicType.PACKAGE_A.Equals(this.ProductPicType)) {
                if (0 < proPic.GetCount(ViewState["SITE_CD"].ToString(), ViewState["PRODUCT_AGENT_CD"].ToString(), ViewState["PRODUCT_SEQ"].ToString(), ViewState["PRODUCT_PIC_TYPE"].ToString())) {
                    this.pnlAlreadyExists.Visible = true;
                    return;
                }
            }

            decimal dNo = proPic.GetPicNo();

            string sFileNm = string.Empty;
            string sPath = string.Empty;
            string sFullPath = string.Empty;

            if (uldProductPicture.HasFile) {

                sFileNm = ViCommConst.PIC_HEADER + iBridUtil.addZero(dNo.ToString(), ViCommConst.OBJECT_NM_LENGTH);
                sPath = ViCommPrograms.GetProductPhysicalDir(sWebPhisicalDir, sSiteCd, this.ProductId);

                sFullPath = sPath + "\\" + sFileNm + ViCommConst.PIC_FOODER;

                using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME, string.Empty, ViCommConst.FILE_UPLOAD_PASSWORD)) {
                    // オリジナル画像をMailToolsディレクトリに配置
                    string sInputDir = ConfigurationManager.AppSettings["MailParseDir"] + "\\";
                    uldProductPicture.SaveAs(Path.Combine(sInputDir, sFileNm + ViCommConst.PIC_FOODER));

                    ImageHelper.ConvertMobileImage(sSiteCd, sInputDir, sPath, sFileNm);
                }
            }
            ExecuteProductPicMainte(ViewState["SITE_CD"].ToString(), ViewState["PRODUCT_AGENT_CD"].ToString(),
                                    int.Parse(ViewState["PRODUCT_SEQ"].ToString()), int.Parse(dNo.ToString()),
                                    ViewState["PRODUCT_PIC_TYPE"].ToString(), 0, 0, 0, 0);
        }

        if (this.pnlList.Visible) {
            UrlBuilder oUrlBuilder = new UrlBuilder("ProductPicMainte.aspx");
            oUrlBuilder.Parameters.Add("product_id", lblProductId.Text);
            oUrlBuilder.Parameters.Add("product_pic_type", ViewState["PRODUCT_PIC_TYPE"].ToString());
            Response.Redirect(oUrlBuilder.ToString());
        } else {
            this.RedirectProductView();
        }

    }

    protected void btnCancel_Click(object sender, EventArgs e) {
        this.RedirectProductView();
    }

    protected void dsProductPic_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {

        chkProductView();
        e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["SITE_CD"]);
        e.InputParameters[1] = iBridUtil.GetStringValue(ViewState["PRODUCT_AGENT_CD"]);
        e.InputParameters[2] = iBridUtil.GetStringValue(ViewState["PRODUCT_SEQ"]);
        e.InputParameters[3] = iBridUtil.GetStringValue(ViewState["PRODUCT_PIC_TYPE"]);
    }

    protected void dsProductPic_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
        if (!this.pnlList.Visible) {
            return;
        }

        DataSet oDataSet = e.ReturnValue as DataSet;
        if (oDataSet != null) {
            if (oDataSet.Tables[0].Rows.Count == 0) {
                this.lblEmptyContentsMoviePic.Visible = true;
            }
        }
    }

    private void chkProductView() {
        //ここで一覧表示を行うかﾁｪｯｸを行う

        pnlList.Visible = false;
        switch (ViewState["PRODUCT_PIC_TYPE"].ToString()) {
            case ViCommConst.ProductPicType.CONTENTS:
            case ViCommConst.ProductPicType.SAMPLE:
                pnlList.Visible = true;
                break;
        }

    }

    protected void btnUp_Command(object sender, CommandEventArgs e) {
        SetPicPostion(decimal.Parse(e.CommandArgument.ToString()), -1);
    }

    protected void btnDown_Command(object sender, CommandEventArgs e) {
        SetPicPostion(decimal.Parse(e.CommandArgument.ToString()), 1);
    }

    protected void btnDelete_Command(object sender, CommandEventArgs e) {

        string[] sKeys = e.CommandArgument.ToString().Split(':');
        string sObjSeq = sKeys[0];
        string sRevisionNo = sKeys[1];
        DeletePicture(sObjSeq, sRevisionNo);
        Response.Redirect(string.Format("ProductPicMainte.aspx?product_id={0}&product_pic_type={1}", lblProductId.Text.ToString(), ViewState["PRODUCT_PIC_TYPE"].ToString()));
    }


    private void SetPicPostion(Decimal pPicSeq, int pMove) {

        decimal dNo = pPicSeq;
        using (DbSession db = new DbSession()) {
            db.PrepareProcedure("SET_PRODUCT_PIC_POSITION");
            db.ProcedureInParm("PSITE_CD", DbSession.DbType.VARCHAR2, ViewState["SITE_CD"]);
            db.ProcedureInParm("PPRODUCT_AGENT_CD", DbSession.DbType.VARCHAR2, ViewState["PRODUCT_AGENT_CD"].ToString());
            db.ProcedureInParm("PPRODUCT_SEQ", DbSession.DbType.NUMBER, int.Parse((string)ViewState["PRODUCT_SEQ"].ToString()));
            db.ProcedureInParm("POBJ_SEQ", DbSession.DbType.NUMBER, dNo);
            db.ProcedureInParm("PPRODUCT_PIC_TYPE", DbSession.DbType.VARCHAR2, ViewState["PRODUCT_PIC_TYPE"]);
            db.ProcedureInParm("pMOVE_POS", DbSession.DbType.NUMBER, pMove);
            db.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);
            db.ExecuteProcedure();
        }
        Response.Redirect(string.Format("ProductPicMainte.aspx?product_id={0}&product_pic_type={1}", lblProductId.Text.ToString(), ViewState["PRODUCT_PIC_TYPE"].ToString()));
    }
    private void DeletePicture(string pFileSeq, string pRevisionNo) {

        int dNo = int.Parse(pFileSeq);
        int dRevi = int.Parse(pRevisionNo);
        ExecuteProductPicMainte(ViewState["SITE_CD"].ToString(), ViewState["PRODUCT_AGENT_CD"].ToString(),
                                int.Parse(ViewState["PRODUCT_SEQ"].ToString()), dNo,
                                ViewState["PRODUCT_PIC_TYPE"].ToString(), 0, dRevi, 1, 0);

    }

    private string ExecuteProductPicMainte(string pSiteCode,
                                            string pProductAgentCd,
                                            int pPorductSeq,
                                            int pObjSeq,
                                            string pProductPicType,
                                            int pDisplayPosition,
                                            int pRevisionNo,
                                            int pDeleteFlag,
                                            int pWithoutCommit) {
        using (DbSession db = new DbSession()) {

            db.PrepareProcedure("PRODUCT_PIC_MAINTE");
            db.ProcedureInParm("PSITE_CD", DbSession.DbType.VARCHAR2, pSiteCode);
            db.ProcedureInParm("PPRODUCT_AGENT_CD", DbSession.DbType.VARCHAR2, pProductAgentCd);
            db.ProcedureInParm("PPRODUCT_SEQ", DbSession.DbType.NUMBER, pPorductSeq);
            db.ProcedureInParm("POBJ_SEQ", DbSession.DbType.NUMBER, pObjSeq);
            db.ProcedureInParm("PPRODUCT_PIC_TYPE", DbSession.DbType.VARCHAR2, pProductPicType);
            db.ProcedureInParm("PDISPLAY_POSITION", DbSession.DbType.NUMBER, pDisplayPosition);
            db.ProcedureInParm("PREVISION_NO", DbSession.DbType.NUMBER, pRevisionNo);
            db.ProcedureInParm("PDELETE_FLAG", DbSession.DbType.NUMBER, pDeleteFlag);
            db.ProcedureInParm("PWITHOUT_COMMIT", DbSession.DbType.NUMBER, pWithoutCommit);
            db.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);

            db.ExecuteProcedure();
            return db.GetStringValue("PSTATUS");
        }
    }

    private void RedirectProductView() {
        UrlBuilder oUrlBuilder = new UrlBuilder("ProductView.aspx");
        oUrlBuilder.Parameters.Add("product_id", lblProductId.Text.ToString());
        Response.Redirect(oUrlBuilder.ToString());
    }
}
