﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 商品機能設定
--	Progaram ID		: ProductFunctionSettingList
--
--  Creation Date	: 2011.06.17
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using iBridCommLib;
using System.Drawing;
using ViComm;

public partial class Product_ProductFunctionSettingList:System.Web.UI.Page {
	string recCount;

	protected string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		private set {
			this.ViewState["SiteCd"] = value;
		}
	}

	protected string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		private set {
			this.ViewState["RevisionNo"] = value;
		}
	}

	protected string Rowid {
		get {
			return iBridUtil.GetStringValue(this.ViewState["Rowid"]);
		}
		private set {
			this.ViewState["Rowid"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		if (!this.IsPostBack) {
			this.InitPage();
		}
	}

	private void InitPage() {
		this.pnlMainte.Visible = false;
		this.pnlDtl.Visible = false;
		this.ClearFields();
		this.GetList();
	}

	private void GetList() {
		this.grdProductFunctionSetting.DataBind();
		this.pnlCount.DataBind();
	}

	private void ClearFields() {
		this.pnlDtl.DataBind();
		
		this.txtAucAutoExtensionAcceptMin.Text = string.Empty;
		this.txtAucAutoExtensionMin.Text = string.Empty;

		this.lstTpAucWinningBid.SelectedIndex = 0;
		this.lstTpAucUpdateBid.SelectedIndex = 0;
	}

	protected void dsProductFunctionSetting_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}	
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		this.pnlMainte.Visible = false;
		this.pnlDtl.Visible = false;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.UpdateData(ViCommConst.FLAG_OFF);
		this.InitPage();
	}

	protected void lnkSiteCd_Command(object sender,CommandEventArgs e) {
		this.SiteCd = e.CommandArgument.ToString();
		this.pnlMainte.Visible = true;
		this.pnlDtl.Visible = true;
		this.SetMailTemplate();
		this.GetData();
	}
	
	protected string GetRecCount(){
		return this.recCount;
	}
	
	private void GetData() {
	
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("PRODUCT_FUNCTION_SETTING_GET");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			oDbSession.ProcedureOutParm("pTP_AUC_WINNING_BID",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pTP_AUC_UPDATE_BID",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pAUC_AUTO_EXTENSION_ACCEPT_MIN",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pAUC_AUTO_EXTENSION_MIN",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pREVISION_NO",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pROWID",DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pROW_COUNT",DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.RevisionNo = oDbSession.GetStringValue("pREVISION_NO");
			this.Rowid = oDbSession.GetStringValue("pROWID");

			if (int.Parse(oDbSession.GetStringValue("pROW_COUNT")) > 0) {
				this.pnlDtl.DataBind();
				this.lstTpAucWinningBid.SelectedIndex = lstTpAucWinningBid.Items.IndexOf(lstTpAucWinningBid.Items.FindByValue(oDbSession.GetStringValue("pTP_AUC_WINNING_BID")));
				this.lstTpAucUpdateBid.SelectedIndex = lstTpAucUpdateBid.Items.IndexOf(lstTpAucUpdateBid.Items.FindByValue(oDbSession.GetStringValue("pTP_AUC_UPDATE_BID")));
				this.txtAucAutoExtensionAcceptMin.Text = oDbSession.GetStringValue("pAUC_AUTO_EXTENSION_ACCEPT_MIN");
				this.txtAucAutoExtensionMin.Text = oDbSession.GetStringValue("pAUC_AUTO_EXTENSION_MIN");
			} else {
				this.ClearFields();
			}

			using (Site oSite = new Site()) {
				if (oSite.GetOne(this.SiteCd)) {
					this.lblSiteNm.Text = oSite.siteNm;
				}
			}
		}
	}

	private void SetMailTemplate() {
		lstTpAucWinningBid.Items.Clear();
		lstTpAucUpdateBid.Items.Clear();
		
		using (MailTemplate oMail = new MailTemplate()) {
			DataSet ds = oMail.GetNewVerList(this.SiteCd,ViCommConst.UsableSexCd.MAN);
			lstTpAucWinningBid.Items.Add(new ListItem("指定なし",string.Empty));
			lstTpAucUpdateBid.Items.Add(new ListItem("指定なし",string.Empty));

			for (int i = 0;i < ds.Tables[0].Rows.Count;i++) {
				if (ds.Tables[0].Rows[i]["MAIL_TEMPLATE_TYPE"].ToString().Equals(ViCommConst.MAIL_TP_INFO)) {
					lstTpAucWinningBid.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
					lstTpAucUpdateBid.Items.Add(new ListItem(ds.Tables[0].Rows[i]["TEMPLATE_NM"].ToString(),ds.Tables[0].Rows[i]["MAIL_TEMPLATE_NO"].ToString()));
				}
			}
		}
	}

	private void UpdateData(int pDelFlag) {
		if (this.pnlDtl.Visible) {
			string sTpAucWinningBid = this.lstTpAucWinningBid.SelectedValue;
			if(sTpAucWinningBid.Equals(string.Empty)){
				sTpAucWinningBid = "0";
			}
			string sTpAucUpdateBid = this.lstTpAucUpdateBid.SelectedValue;
			if(sTpAucUpdateBid.Equals(string.Empty)){
				sTpAucUpdateBid = "0";
			}
			using (DbSession oDbSession = new DbSession()) {
				oDbSession.PrepareProcedure("PRODUCT_FUNCTION_SETTING_MNT");
				oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
				oDbSession.ProcedureInParm("pTP_AUC_WINNING_BID",DbSession.DbType.VARCHAR2,sTpAucWinningBid);
				oDbSession.ProcedureInParm("pTP_AUC_UPDATE_BID",DbSession.DbType.VARCHAR2,sTpAucUpdateBid);
				oDbSession.ProcedureInParm("pAUC_AUTO_EXTENSION_ACCEPT_MIN",DbSession.DbType.NUMBER,this.txtAucAutoExtensionAcceptMin.Text);
				oDbSession.ProcedureInParm("pAUC_AUTO_EXTENSION_MIN",DbSession.DbType.NUMBER,this.txtAucAutoExtensionMin.Text);
				oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.RevisionNo);
				oDbSession.ProcedureInParm("pROWID",DbSession.DbType.VARCHAR2,this.Rowid);
				oDbSession.ProcedureInParm("pDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
				oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
				oDbSession.ExecuteProcedure();
			}
		}
	}
}
