﻿using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using iBridCommLib;
using ViComm;
using System.Drawing;

public partial class Product_ProductView : System.Web.UI.Page {

	protected void Page_Load(object sender, EventArgs e) {
		if (!this.IsPostBack) {
			this.txtProductId.Text = iBridUtil.GetStringValue(this.Request["product_id"]);
			GetData();
		}

	}

	protected void btnSeek_Click(object sender, EventArgs e) {
		this.txtProductId.Text = TrimEnd(this.txtProductId.Text);
		this.InitProductSituation();
		this.CloseExtInfo();
		this.dvwProduct.DataBind();
	}

	protected void btnClear_Click(object sender, EventArgs e) {
		this.txtProductId.Text = string.Empty;
		this.InitProductSituation();
		this.CloseExtInfo();
		this.dvwProduct.DataBind();
	}

	protected void btnBack_Click(object sender, EventArgs e) {

	}

	private void InitPage() {
		this.pnlProductInfo.Visible = false;
	}

	protected string GetProductTypeMark(object pProductType) {
		using (CodeDtl oCodeDtl = new CodeDtl()) {
			if (oCodeDtl.GetOne(ViCommConst.CODE_TYPE_PRODUCT_TYPE, iBridUtil.GetStringValue(pProductType))) {
				return oCodeDtl.codeNm;
			}
		}
		return string.Empty;
	}

	protected string GetPublishMark(object pPublishFlag) {
		switch (iBridUtil.GetStringValue(pPublishFlag)) {
			case ViCommConst.FLAG_ON_STR:
				return "公開";
			case ViCommConst.FLAG_OFF_STR:
				return "非公開";
			case ViCommConst.FLAG_WITHOUT_STR:
				return "未設定";
			default:
				return string.Empty;
		}
	}

	protected Color GetPublishMarkColor(object pPublishFlag) {
		switch (iBridUtil.GetStringValue(pPublishFlag)) {
			case ViCommConst.FLAG_ON_STR:
			case ViCommConst.FLAG_OFF_STR:
				return Color.Black;
			case ViCommConst.FLAG_WITHOUT_STR:
				return Color.Red;
			default:
				return Color.Black;
		}
	}

	protected string GetPickupMask(object pPickupMask) {
		int iMask = Convert.ToInt32(pPickupMask);
		if (iMask == 0) {
			return string.Empty;
		}

		string sLabel = string.Empty;
		int iValue = 1;
		for (int i = 1; i < 10; i++) {
			if ((iMask & iValue) > 0) {
				sLabel = sLabel + i.ToString("d2") + "&nbsp;";
			}
			iValue = iValue << 1;
		}
		if (!sLabel.Equals(string.Empty)) {
			sLabel = "Pickup " + sLabel;
		}
		return sLabel;
	}

	protected string GetDelMark(object pDelFlag) {
		if (pDelFlag.ToString().Equals(ViComm.ViCommConst.FLAG_ON_STR)) {
			return "削除済";
		} else {
			return string.Empty;
		}
	}

	protected string GetNotNewFlagMark(object pNotNewFlag) {
		if (ViCommConst.FLAG_ON_STR.Equals(iBridUtil.GetStringValue(pNotNewFlag))) {
			return "非表示";
		} else {
			return "表示";
		}
	}

	protected string GetHandleNm(object pSiteCd, object pProductSeq, object pProductAgentCd) {
		using (CastCharacter oCastCharacter = new CastCharacter()) {
			using (DataSet oDataSet = oCastCharacter.GetOneByProductSeq(iBridUtil.GetStringValue(pSiteCd), iBridUtil.GetStringValue(pProductSeq), iBridUtil.GetStringValue(pProductAgentCd))) {
				if (oDataSet != null && oDataSet.Tables[0].Rows.Count > 0) {
					DataRow oDataRow = oDataSet.Tables[0].Rows[0];
					return iBridUtil.GetStringValue(oDataRow["HANDLE_NM"]);
				}
			}
		}
		return string.Empty;
	}

	protected string GenerateCastViewUrl(object pSiteCd, object pProductSeq, object pProductAgentCd) {
		using (CastCharacter oCastCharacter = new CastCharacter()) {
			using (DataSet oDataSet = oCastCharacter.GetOneByProductSeq(iBridUtil.GetStringValue(pSiteCd), iBridUtil.GetStringValue(pProductSeq), iBridUtil.GetStringValue(pProductAgentCd))) {
				if (oDataSet != null && oDataSet.Tables[0].Rows.Count > 0) {
					DataRow oDataRow = oDataSet.Tables[0].Rows[0];
					return string.Format("~/Cast/CastView.aspx?loginid={0}", oDataRow["LOGIN_ID"]);
				}
			}
		}
		return string.Empty;
	}

	private void GetData() {
		this.pnlProductInfo.Visible = true;
		this.InitProductSituation();
		this.dvwProduct.DataBind();
	}

	protected string GenerateProductMainteUrl(object pSeed) {
		DataRowView oProductDataRowView = (DataRowView)pSeed;

		string sSiteCd = iBridUtil.GetStringValue(oProductDataRowView["SITE_CD"]);
		string sProductAgentCd = iBridUtil.GetStringValue(oProductDataRowView["PRODUCT_AGENT_CD"]);
		string sProductSeq = iBridUtil.GetStringValue(oProductDataRowView["PRODUCT_SEQ"]);

		UrlBuilder oUrlBuilder = new UrlBuilder("ProductMainte.aspx");
		oUrlBuilder.Parameters.Add("site_cd", sSiteCd);
		oUrlBuilder.Parameters.Add("product_agent_cd", sProductAgentCd);
		oUrlBuilder.Parameters.Add("product_seq", sProductSeq);
		return oUrlBuilder.ToString();
	}


	protected void dsProduct_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
		DataSet oProductDataSet = e.ReturnValue as DataSet;
		this.pnlProductInfo.Visible = false;
		if (oProductDataSet != null) {
			this.pnlProductInfo.Visible = oProductDataSet.Tables.Count > 0 && oProductDataSet.Tables[0].Rows.Count > 0;
		}

		if (this.pnlProductInfo.Visible) {
			DataRow oProductDataRow = oProductDataSet.Tables[0].Rows[0];
			string sSiteCd = iBridUtil.GetStringValue(oProductDataRow["SITE_CD"]);
			string sProductAgentCd = iBridUtil.GetStringValue(oProductDataRow["PRODUCT_AGENT_CD"]);
			string sProductSeq = iBridUtil.GetStringValue(oProductDataRow["PRODUCT_SEQ"]);
			string sProductType = iBridUtil.GetStringValue(oProductDataRow["PRODUCT_TYPE"]);
			string sLinkId = iBridUtil.GetStringValue(oProductDataRow["LINK_ID"]);
			bool bEditable = !iBridUtil.GetStringValue(oProductDataRow["UNEDITABLE_FLAG"]).Equals(ViCommConst.FLAG_ON_STR);

			this.DisplayKeyword(sSiteCd, sProductAgentCd, sProductSeq);
			this.DisplayGenre(sSiteCd, sProductAgentCd, sProductSeq);

			this.hdnSiteCd.Value = sSiteCd;
			this.hdnProductAgentCd.Value = sProductAgentCd;
			this.hdnProductSeq.Value = sProductSeq;
			this.hdnProductType.Value = sProductType;
			this.hdnLinkId.Value = sLinkId;
			this.hdnRevisionNo.Value = iBridUtil.GetStringValue(oProductDataRow["REVISION_NO"]);

			this.txtProductId.Text = TrimEnd(this.txtProductId.Text);
			this.txtRemarks.Text = iBridUtil.GetStringValue(oProductDataRow["REMARKS"]);	

			this.pnlProductMovie.Visible = false;
			this.pnlProductPic.Visible = false;
			this.pnlProductAuction.Visible = false;
			this.pnlProductTheme.Visible = false;
			this.InitProductSituation();

			if(ViCommConst.ProductType.IsMovie(sProductType)){
				this.pnlProductMovie.Visible = true;
				this.DisplayMovie(sSiteCd, sProductAgentCd, sProductSeq, bEditable);
			} else if (ViCommConst.ProductType.IsPic(sProductType)) {
				this.pnlProductPic.Visible = true;
				this.DisplayPic(sSiteCd, sProductAgentCd, sProductSeq, bEditable);
		
			} else if(ViCommConst.ProductType.IsAuction(sProductType)) {
				this.pnlProductAuction.Visible = true;
				this.DisplayAuction(sSiteCd,sProductAgentCd,sProductSeq,bEditable);

			} else if (ViCommConst.ProductType.IsTheme(sProductType)) {
				this.pnlProductTheme.Visible = true;
				this.DisplayProductTheme(sSiteCd, sProductAgentCd, sProductSeq, bEditable);
			}
		
		}

	}

	#region □■□ 表示処理(商品基本)  □■□ =========================================================================
	private void DisplayGenre(string pSiteCd, string pProductAgentCd, string pProductSeq) {
		using (ProductGenreRel oProductGenreRel = new ProductGenreRel()) {
			using (DataSet oGenreRelDataSet = oProductGenreRel.GetList(pSiteCd, pProductAgentCd, pProductSeq)) {
				this.grdGenre.DataSource = oGenreRelDataSet;
				this.grdGenre.DataBind();

				bool bExistsGenre = oGenreRelDataSet.Tables[0].Rows.Count > 0;
				lblEmptyGenreMsg.Visible = !bExistsGenre;
				grdGenre.Visible = bExistsGenre;
			}
		}
	}

	private void DisplayKeyword(string pSiteCd, string pProductAgentCd, string pProductSeq) {
		StringBuilder oKeywordTextBuilder = new StringBuilder();
		bool bExistsKeyword = false;

		using (ProductKeyword oProductKeyword = new ProductKeyword()) {
			using (DataSet oKeywordDataSet = oProductKeyword.GetList(pSiteCd, pProductAgentCd, pProductSeq)) {
				foreach (DataRow oKeywordDataRow in oKeywordDataSet.Tables[0].Rows) {
					string sKeywrodValue = iBridUtil.GetStringValue(oKeywordDataRow["PRODUCT_KEYWORD_VALUE"]);
					if (sKeywrodValue.Equals(string.Empty)) continue;

					if (oKeywordTextBuilder.Length != 0) {
						oKeywordTextBuilder.Append("/");
					}
					oKeywordTextBuilder.Append(sKeywrodValue);
					bExistsKeyword = bExistsKeyword || true;
				}
			}
		}
		this.lblKeyword.Text = oKeywordTextBuilder.ToString();
		this.lblKeyword.Visible = bExistsKeyword;
		this.lblEmptyKeywordMsg.Visible = !bExistsKeyword;
	}
	
	#endregion ========================================================================================================


	#region □■□ 表示処理(商品動画)  □■□ =========================================================================

	private void DisplayMovie(string pSiteCd, string pProductAgentCd, string pProductSeq, bool pEditable) {
		this.btnAppendContentsMovie.CommandArgument = ViCommConst.ProductMovieType.CONTENTS;
		this.btnAppendSampleMovie.CommandArgument = ViCommConst.ProductMovieType.SAMPLE;
		this.btnAppendMovieSamplePic.CommandArgument = ViCommConst.ProductPicType.SAMPLE;
		this.btnPackageAdd.CommandArgument = ViCommConst.ProductPicType.PACKAGE_A;
		this.btnPackage2Add.CommandArgument = ViCommConst.ProductPicType.PACKAGE_B;

		using (ProductPic oProductPic = new ProductPic())
		using (ProductMovie oProductMovie = new ProductMovie()) {

			// ================================
			// コンテンツ動画情報表示
			// ================================
			using (DataSet oDataSet = oProductMovie.GetListByMovieType(pSiteCd, pProductAgentCd, pProductSeq, ViCommConst.ProductMovieType.CONTENTS)) {
				string sEditCommandArgs = string.Empty;
				bool bHasContentsMovie = false;
				string sLinkId = null;
				if (oDataSet.Tables[0].Rows.Count > 0) {
					this.dvwContentsMovie.DataSource = oDataSet;
					this.dvwContentsMovie.DataBind();

					DataRow oProductMovieDataRow = oDataSet.Tables[0].Rows[0];
					string sProductId = iBridUtil.GetStringValue(oProductMovieDataRow["PRODUCT_ID"]);
					string sObjSeq = iBridUtil.GetStringValue(oProductMovieDataRow["OBJ_SEQ"]);
					sLinkId = iBridUtil.GetStringValue(oProductMovieDataRow["LINK_ID"]);

					sEditCommandArgs = string.Format("{0},{1}", sProductId, sObjSeq);
					bHasContentsMovie = true;

				}
				this.DisplayContentsMovieDtlSummary(pSiteCd, pProductAgentCd, pProductSeq, sLinkId);
				this.pnlContentsMovie.Visible = bHasContentsMovie;
				this.pnlEmptyContentsMovie.Visible = !this.pnlContentsMovie.Visible;
				this.btnEditContentsMovie.Enabled = pEditable;
				this.btnEditContentsMovie.CommandArgument = sEditCommandArgs;
			}
			// ================================
			// サンプル動画情報表示
			// ================================
			using (DataSet oDataSet = oProductMovie.GetListByMovieType(pSiteCd, pProductAgentCd, pProductSeq, ViCommConst.ProductMovieType.SAMPLE)) {
				string sEditCommandArgs = string.Empty;
				bool bHasSampleMovie = false;
				string sLinkId = null;
				if (oDataSet.Tables[0].Rows.Count > 0) {

					DataRow oProductMovieDataRow = oDataSet.Tables[0].Rows[0];
					string sProductId = iBridUtil.GetStringValue(oProductMovieDataRow["PRODUCT_ID"]);
					string sObjSeq = iBridUtil.GetStringValue(oProductMovieDataRow["OBJ_SEQ"]);
					sLinkId = iBridUtil.GetStringValue(oProductMovieDataRow["LINK_ID"]);

					sEditCommandArgs = string.Format("{0},{1}", sProductId, sObjSeq);
					bHasSampleMovie = true;

				}
				this.DisplaySampleMovieDtlSummary(pSiteCd, pProductAgentCd, pProductSeq, sLinkId);
				this.pnlSampleMovie.Visible = bHasSampleMovie;
				this.pnlEmptySampleMovie.Visible = !this.pnlSampleMovie.Visible;
				this.btnEditSampleMovie.Enabled = pEditable;
				this.btnEditSampleMovie.CommandArgument = sEditCommandArgs;
			}

			// ================================
			// サンプル画像情報表示
			// ================================
			using (DataSet oDataSet = oProductPic.GetList(pSiteCd, pProductAgentCd, pProductSeq, ViCommConst.ProductPicType.SAMPLE)) {
				bool bHasMovieSamplePic = false;
				if (oDataSet.Tables[0].Rows.Count > 0) {
					this.rptMovieSamplePic.DataSource = oDataSet;
					this.rptMovieSamplePic.DataBind();

					DataRow oProductPicDataRow = oDataSet.Tables[0].Rows[0];
					bHasMovieSamplePic = true;

				}
				this.pnlMovieSamplePic.Visible = bHasMovieSamplePic;
				this.pnlEmptyMovieSamplePic.Visible = !bHasMovieSamplePic;
			}
			// ================================
			// パッケージ(表)画像情報表示
			// ================================
			using (DataSet oDataSet = oProductPic.GetList(pSiteCd, pProductAgentCd, pProductSeq, ViCommConst.ProductPicType.PACKAGE_A)) {
				bool bHasPic = false;
				if (oDataSet.Tables[0].Rows.Count > 0) {
					this.imgMoviePackageA.ImageUrl = string.Format("~/{0}", oDataSet.Tables[0].Rows[0]["PHOTO_IMG_PATH"]);
					bHasPic = true;

					DataRow oPicDataRow = oDataSet.Tables[0].Rows[0];
					this.btnPackageDel.CommandArgument = string.Format("{0}:{1}", iBridUtil.GetStringValue(oPicDataRow["OBJ_SEQ"]), iBridUtil.GetStringValue(oPicDataRow["REVISION_NO"]));
				}
				this.pnlMoviePackageA.Visible = bHasPic;
				this.pnlEmptyMoviePackageA.Visible = !bHasPic;
				this.btnPackageDel.Visible = bHasPic;
				this.btnPackageAdd.Visible = !bHasPic;

			}
			// ================================
			// パッケージ(裏)画像情報表示
			// ================================
			using (DataSet oDataSet = oProductPic.GetList(pSiteCd, pProductAgentCd, pProductSeq, ViCommConst.ProductPicType.PACKAGE_B)) {
				bool bHasPic = false;
				if (oDataSet.Tables[0].Rows.Count > 0) {
					this.imgMoviePackageB.ImageUrl = string.Format("~/{0}", oDataSet.Tables[0].Rows[0]["PHOTO_IMG_PATH"]);
					bHasPic = true;

					DataRow oPicDataRow = oDataSet.Tables[0].Rows[0];
					this.btnPackage2Del.CommandArgument = string.Format("{0}:{1}", iBridUtil.GetStringValue(oPicDataRow["OBJ_SEQ"]), iBridUtil.GetStringValue(oPicDataRow["REVISION_NO"]));
				}
				this.pnlMoviePackageB.Visible = bHasPic;
				this.pnlEmptyMoviePackageB.Visible = !bHasPic;
				this.btnPackage2Del.Visible = bHasPic;
				this.btnPackage2Add.Visible = !bHasPic;

			}
		}
	}
	private void DisplayContentsMovieDtlSummary(string pSiteCd, string pProductAgentCd, string pProductSeq,string pLinkId) {
		this.lnkContentsMovie.Visible = false;
		using (ProductMovieDtl oProductMovieDtl = new ProductMovieDtl()) {
			DataSet oDataSet = oProductMovieDtl.GetSummary(pSiteCd, pProductAgentCd, pProductSeq, ViCommConst.ProductMovieType.CONTENTS);
			this.grdContentsMovieDtl.DataSource = oDataSet;
			this.grdContentsMovieDtl.DataBind();

			if (oDataSet.Tables[0].Rows.Count > 0 && string.IsNullOrEmpty(pLinkId)) {
				string sObjeSeq = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["OBJ_SEQ"]);

				string sImgPath = string.Format("~/productres/{0}/{1}/{2}.3gp", pSiteCd, this.txtProductId.Text, iBridUtil.addZero(sObjeSeq, ViCommConst.OBJECT_NM_LENGTH)); ;
				this.lnkContentsMovie.NavigateUrl = string.Format("javascript:openMovieViewer('{0}');", this.ResolveUrl(sImgPath));
				this.lnkContentsMovie.Visible = true;
			} else {
				this.lnkContentsMovie.Visible = false;
			}

		}
	}
	private void DisplaySampleMovieDtlSummary(string pSiteCd, string pProductAgentCd, string pProductSeq, string pLinkId) {
		this.lnkOpenSampleMovieViewer.Visible = false;
		using (ProductMovieDtl oProductMovieDtl = new ProductMovieDtl()) {
			DataSet oDataSet = oProductMovieDtl.GetSummary(pSiteCd, pProductAgentCd, pProductSeq, ViCommConst.ProductMovieType.SAMPLE);
			this.grdSampleMovieDtl.DataSource = oDataSet;
			this.grdSampleMovieDtl.DataBind();

			if (oDataSet.Tables[0].Rows.Count > 0 && string.IsNullOrEmpty(pLinkId)) {
				string sObjeSeq = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["OBJ_SEQ"]);

				string sImgPath = string.Format("~/productres/{0}/{1}/{2}.3gp", pSiteCd, this.txtProductId.Text, iBridUtil.addZero(sObjeSeq, ViCommConst.OBJECT_NM_LENGTH)); ;
				this.lnkOpenSampleMovieViewer.NavigateUrl = string.Format("javascript:openMovieViewer('{0}');", this.ResolveUrl(sImgPath));
				this.lnkOpenSampleMovieViewer.Visible = true;
			} else {
				this.lnkOpenSampleMovieViewer.Visible = false;
			}

		}
	}

	#endregion ========================================================================================================


	#region □■□ 表示処理(商品写真)  □■□ =========================================================================
	private void DisplayPic(string pSiteCd, string pProductAgentCd, string pProductSeq, bool pEditable) {
		this.btnAppendPicContentsPic.CommandArgument = ViCommConst.ProductPicType.CONTENTS;
		this.btnAppendPicSamplePic.CommandArgument = ViCommConst.ProductPicType.SAMPLE;
		this.btnAppendPicPackagePic.CommandArgument = ViCommConst.ProductPicType.PACKAGE_A;

		using (ProductPic oProductPic = new ProductPic()) {
			// ================================
			// 商品写真情報表示
			// ================================
			using (DataSet oDataSet = oProductPic.GetList(pSiteCd, pProductAgentCd, pProductSeq, ViCommConst.ProductPicType.PACKAGE_A)) {
				bool bHasPic = false;
				if (oDataSet.Tables[0].Rows.Count > 0) {
					this.imgPicPackagePic.ImageUrl = string.Format("~/{0}", oDataSet.Tables[0].Rows[0]["PHOTO_IMG_PATH"]);
					bHasPic = true;

					DataRow oPicDataRow = oDataSet.Tables[0].Rows[0];
					this.btnRemovePicPackagePic.CommandArgument = string.Format("{0}:{1}", iBridUtil.GetStringValue(oPicDataRow["OBJ_SEQ"]), iBridUtil.GetStringValue(oPicDataRow["REVISION_NO"]));
				}
				this.pnlPicPackagePic.Visible = bHasPic;
				this.pnlEmptyPicPackagePic.Visible = !bHasPic;
				this.btnRemovePicPackagePic.Visible = bHasPic;
				this.btnAppendPicPackagePic.Visible = !bHasPic;
			}

			// ================================
			// コンテンツ画像情報表示
			// ================================
			using (DataSet oDataSet = oProductPic.GetList(pSiteCd, pProductAgentCd, pProductSeq, ViCommConst.ProductPicType.CONTENTS)) {
				bool bHasPic = false;
				if (oDataSet.Tables[0].Rows.Count > 0) {
					this.rptPicContentsPic.DataSource = oDataSet;
					this.rptPicContentsPic.DataBind();
					bHasPic = true;
				}
				this.pnlPicContentsPic.Visible = bHasPic;
				this.pnlEmptyPicContentsPic.Visible = !this.pnlPicContentsPic.Visible;
			}
			// ================================
			// サンプル画像情報表示
			// ================================
			using (DataSet oDataSet = oProductPic.GetList(pSiteCd, pProductAgentCd, pProductSeq, ViCommConst.ProductPicType.SAMPLE)) {
				bool bHasPic = false;
				if (oDataSet.Tables[0].Rows.Count > 0) {
					this.rptPicSamplePic.DataSource = oDataSet;
					this.rptPicSamplePic.DataBind();
					bHasPic = true;
				}
				this.pnlPicSamplePic.Visible = bHasPic;
				this.pnlEmptyPicSamplePic.Visible = !this.pnlPicSamplePic.Visible;
			}
		}
	}
	#endregion ========================================================================================================

	#region □■□ 表示処理(商品オークション)  □■□ =========================================================================
	private void DisplayAuction(string pSiteCd,string pProductAgentCd,string pProductSeq,bool pEditable) {
		this.btnAppendSampleAuctionMovie.CommandArgument = ViCommConst.ProductMovieType.SAMPLE;
		this.btnAppendMovieSampleAuctionPic.CommandArgument = ViCommConst.ProductPicType.SAMPLE;
		this.btnAuctionPackageAdd.CommandArgument = ViCommConst.ProductPicType.PACKAGE_A;
		this.btnAuctionPackage2Add.CommandArgument = ViCommConst.ProductPicType.PACKAGE_B;

		using (ProductPic oProductPic = new ProductPic())
		using (ProductMovie oProductMovie = new ProductMovie()) {
			
			// ================================
			// サンプル動画情報表示
			// ================================
			using (DataSet oDataSet = oProductMovie.GetListByMovieType(pSiteCd,pProductAgentCd,pProductSeq,ViCommConst.ProductMovieType.SAMPLE)) {
				string sEditCommandArgs = string.Empty;
				bool bHasSampleMovie = false;
				string sLinkId = null;
				if (oDataSet.Tables[0].Rows.Count > 0) {

					DataRow oProductMovieDataRow = oDataSet.Tables[0].Rows[0];
					string sProductId = iBridUtil.GetStringValue(oProductMovieDataRow["PRODUCT_ID"]);
					string sObjSeq = iBridUtil.GetStringValue(oProductMovieDataRow["OBJ_SEQ"]);
					sLinkId = iBridUtil.GetStringValue(oProductMovieDataRow["LINK_ID"]);

					sEditCommandArgs = string.Format("{0},{1}",sProductId,sObjSeq);
					bHasSampleMovie = true;

				}
				this.DisplaySampleAuctionMovieDtlSummary(pSiteCd,pProductAgentCd,pProductSeq,sLinkId);
				this.pnlSampleAuctionMovie.Visible = bHasSampleMovie;
				this.pnlEmptySampleAuctionMovie.Visible = !this.pnlSampleAuctionMovie.Visible;
				this.btnEditSampleAuctionMovie.Enabled = pEditable;
				this.btnEditSampleAuctionMovie.CommandArgument = sEditCommandArgs;
			}

			// ================================
			// サンプル画像情報表示
			// ================================
			using (DataSet oDataSet = oProductPic.GetList(pSiteCd,pProductAgentCd,pProductSeq,ViCommConst.ProductPicType.SAMPLE)) {
				bool bHasMovieSamplePic = false;
				if (oDataSet.Tables[0].Rows.Count > 0) {
					this.rptMovieSampleAuctionPic.DataSource = oDataSet;
					this.rptMovieSampleAuctionPic.DataBind();

					DataRow oProductPicDataRow = oDataSet.Tables[0].Rows[0];
					bHasMovieSamplePic = true;

				}
				this.pnlMovieSampleAuctionPic.Visible = bHasMovieSamplePic;
				this.pnlEmptyMovieSampleAuctionPic.Visible = !bHasMovieSamplePic;
			}
			// ================================
			// パッケージ(表)画像情報表示
			// ================================
			using (DataSet oDataSet = oProductPic.GetList(pSiteCd,pProductAgentCd,pProductSeq,ViCommConst.ProductPicType.PACKAGE_A)) {
				bool bHasPic = false;
				if (oDataSet.Tables[0].Rows.Count > 0) {
					this.imgAuctionMoviePackageA.ImageUrl = string.Format("~/{0}",oDataSet.Tables[0].Rows[0]["PHOTO_IMG_PATH"]);
					bHasPic = true;

					DataRow oPicDataRow = oDataSet.Tables[0].Rows[0];
					this.btnAuctionPackageDel.CommandArgument = string.Format("{0}:{1}",iBridUtil.GetStringValue(oPicDataRow["OBJ_SEQ"]),iBridUtil.GetStringValue(oPicDataRow["REVISION_NO"]));
				}
				this.pnlAuctionMoviePackageA.Visible = bHasPic;
				this.pnlEmptyAuctionMoviePackageA.Visible = !bHasPic;
				this.btnAuctionPackageDel.Visible = bHasPic;
				this.btnAuctionPackageAdd.Visible = !bHasPic;

			}
			// ================================
			// パッケージ(裏)画像情報表示
			// ================================
			using (DataSet oDataSet = oProductPic.GetList(pSiteCd,pProductAgentCd,pProductSeq,ViCommConst.ProductPicType.PACKAGE_B)) {
				bool bHasPic = false;
				if (oDataSet.Tables[0].Rows.Count > 0) {
					this.imgAuctionMoviePackageB.ImageUrl = string.Format("~/{0}",oDataSet.Tables[0].Rows[0]["PHOTO_IMG_PATH"]);
					bHasPic = true;

					DataRow oPicDataRow = oDataSet.Tables[0].Rows[0];
					this.btnAuctionPackage2Del.CommandArgument = string.Format("{0}:{1}",iBridUtil.GetStringValue(oPicDataRow["OBJ_SEQ"]),iBridUtil.GetStringValue(oPicDataRow["REVISION_NO"]));
				}
				this.pnlAuctionMoviePackageB.Visible = bHasPic;
				this.pnlEmptyAuctionMoviePackageB.Visible = !bHasPic;
				this.btnAuctionPackage2Del.Visible = bHasPic;
				this.btnAuctionPackage2Add.Visible = !bHasPic;

			}
		}
	}
	private void DisplaySampleAuctionMovieDtlSummary(string pSiteCd,string pProductAgentCd,string pProductSeq,string pLinkId) {
		this.lnkOpenSampleAuctionMovieViewer.Visible = false;
		using (ProductMovieDtl oProductMovieDtl = new ProductMovieDtl()) {
			DataSet oDataSet = oProductMovieDtl.GetSummary(pSiteCd,pProductAgentCd,pProductSeq,ViCommConst.ProductMovieType.SAMPLE);
			this.grdSampleAuctionMovieDtl.DataSource = oDataSet;
			this.grdSampleAuctionMovieDtl.DataBind();

			if (oDataSet.Tables[0].Rows.Count > 0 && string.IsNullOrEmpty(pLinkId)) {
				string sObjeSeq = iBridUtil.GetStringValue(oDataSet.Tables[0].Rows[0]["OBJ_SEQ"]);

				string sImgPath = string.Format("~/productres/{0}/{1}/{2}.3gp",pSiteCd,this.txtProductId.Text,iBridUtil.addZero(sObjeSeq,ViCommConst.OBJECT_NM_LENGTH));
				;
				this.lnkOpenSampleAuctionMovieViewer.NavigateUrl = string.Format("javascript:openMovieViewer('{0}');",this.ResolveUrl(sImgPath));
				this.lnkOpenSampleAuctionMovieViewer.Visible = true;
			} else {
				this.lnkOpenSampleAuctionMovieViewer.Visible = false;
			}

			DisplayProductSituation();
		}
	}
	#endregion ========================================================================================================

	#region □■□ 表示処理(着せ替えﾂｰﾙ)  □■□ =========================================================================
	private void DisplayProductTheme(string pSiteCd, string pProductAgentCd, string pProductSeq, bool pEditable) {
		this.btnAddProductThemeSamplePic.CommandArgument = ViCommConst.ProductPicType.SAMPLE;
		this.btnAddProductThemePic.CommandArgument = ViCommConst.ProductPicType.PACKAGE_A;

		using (ProductPic oProductPic = new ProductPic()) {
			// ================================
			// 商品写真情報表示
			// ================================
			using (DataSet oDataSet = oProductPic.GetList(pSiteCd, pProductAgentCd, pProductSeq, ViCommConst.ProductPicType.PACKAGE_A)) {
				bool bHasPic = false;
				if (oDataSet.Tables[0].Rows.Count > 0) {
					this.imgProductThemePic.ImageUrl = string.Format("~/{0}", oDataSet.Tables[0].Rows[0]["PHOTO_IMG_PATH"]);
					bHasPic = true;

					DataRow oPicDataRow = oDataSet.Tables[0].Rows[0];
					this.btnDelProductThemePic.CommandArgument = string.Format("{0}:{1}", iBridUtil.GetStringValue(oPicDataRow["OBJ_SEQ"]), iBridUtil.GetStringValue(oPicDataRow["REVISION_NO"]));
				}
				this.pnlProductThemePic.Visible = bHasPic;
				this.pnlEmptyProductThemePic.Visible = !bHasPic;
				this.btnDelProductThemePic.Visible = bHasPic;
				this.btnAddProductThemePic.Visible = !bHasPic;
			}

			// ================================
			// サンプル画像情報表示
			// ================================
			using (DataSet oDataSet = oProductPic.GetList(pSiteCd, pProductAgentCd, pProductSeq, ViCommConst.ProductPicType.SAMPLE)) {
				bool bHasPic = false;
				if (oDataSet.Tables[0].Rows.Count > 0) {
					this.rptProductThemeSamplePic.DataSource = oDataSet;
					this.rptProductThemeSamplePic.DataBind();
					bHasPic = true;
				}
				this.pnlProductThemeSample.Visible = bHasPic;
				this.pnlEmptyProductThemeSamplePic.Visible = !this.pnlProductThemeSample.Visible;
			}
		}
	}
	#endregion ========================================================================================================

	protected void btnPackageDel_Command(object sender, CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		string sSiteCd = hdnSiteCd.Value.ToString();
		string sProductAgentCd = hdnProductAgentCd.Value.ToString();
		string sProductSeq = hdnProductSeq.Value.ToString();
		string sObjSeq = sKeys[0];
		string sRevisionNo = sKeys[1];

		using (ProductPic propic = new ProductPic()) {
			propic.DeleteProductPic(sSiteCd, sProductAgentCd, int.Parse(sProductSeq), int.Parse(sObjSeq), int.Parse(sRevisionNo));
		}

		Response.Redirect(string.Format("ProductView.aspx?product_id={0}", txtProductId.Text.ToString()));
	}
	protected void btnAppendMovie_Command(object sender, CommandEventArgs e) {
		string sProductId = this.txtProductId.Text;
		string sProductMovieType = (string)e.CommandArgument;

		UrlBuilder oUrlBuilder = new UrlBuilder("ProductMovieConvert.aspx");
		oUrlBuilder.Parameters.Add("product_id", sProductId);
		oUrlBuilder.Parameters.Add("product_movie_type", sProductMovieType);
		Response.Redirect(oUrlBuilder.ToString());
	}
	protected void btnEditMovie_Command(object sender, CommandEventArgs e) {
		string[] sArgs = ((string)e.CommandArgument).Split(',');
		string sProductId = sArgs[0];
		string sObjSeq = sArgs[1];


		UrlBuilder oUrlBuilder = new UrlBuilder("ProductMovieMainte.aspx");
		oUrlBuilder.Parameters.Add("product_id", sProductId);
		oUrlBuilder.Parameters.Add("object_seq", sObjSeq);
		Response.Redirect(oUrlBuilder.ToString());
	}
	protected void btnAppendPic_Command(object sender, CommandEventArgs e) {
		string sProductId = this.txtProductId.Text;
		string sPicType = (string)e.CommandArgument;

		UrlBuilder oUrlBuilder = new UrlBuilder("ProductPicMainte.aspx");
		oUrlBuilder.Parameters.Add("product_id", sProductId);
		oUrlBuilder.Parameters.Add("product_pic_type", sPicType);
		Response.Redirect(oUrlBuilder.ToString());
	}

	protected void dsProductAuctionBidLog_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		ProductAuctionBidLog.SearchCondition oCondition = new ProductAuctionBidLog.SearchCondition();
		oCondition.SiteCd = this.hdnSiteCd.Value;
		oCondition.ProductAgentCd = this.hdnProductAgentCd.Value;
		oCondition.ProductSeq = this.hdnProductSeq.Value;
		e.InputParameters[0] = oCondition;
	}

	protected void dsProductAuctionEntryLog_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		ProductAuctionEntryLog.SearchCondition oCondition = new ProductAuctionEntryLog.SearchCondition();
		oCondition.SiteCd = this.hdnSiteCd.Value;
		oCondition.ProductAgentCd = this.hdnProductAgentCd.Value;
		oCondition.ProductSeq = this.hdnProductSeq.Value;
		e.InputParameters[0] = oCondition;
	}

	protected void dvwProduct_DataBound(object sender,EventArgs e) {

		DetailsView oDtlVw = sender as DetailsView;
		if (oDtlVw.Rows.Count > 0) {
			if (ViCommConst.ProductType.IsAuction(hdnProductType.Value)) {
				oDtlVw.Rows[7].Visible = false;
				oDtlVw.Rows[10].Visible = false;
				oDtlVw.Rows[11].Visible = false;
				oDtlVw.Rows[12].Visible = false;
				oDtlVw.Rows[13].Visible = false;
				oDtlVw.Rows[14].Visible = false;
				pnlAuctionInfo.Visible = true;
				dvwProductAuction.DataBind();

				return;
			}
		}

		pnlAuctionInfo.Visible = false;
		return;
	}

	protected void lnkAuctionBidLog_Click(object sender,EventArgs e) {
		if (IsValid) {
			this.CloseExtInfo();
			this.GetData();

			if (ViCommConst.ProductType.IsAuction(this.hdnProductType.Value)) {
				if (this.IsExistProductData()) {
					this.pnlExtInfo.Visible = true;
					this.pnlAuctionBidLog.Visible = true;
					this.grdAuctionBidLog.DataSourceID = "dsAuctionBidLog";
					this.grdAuctionBidLog.DataBind();
				}
			}
		}
	}

	protected void lnkAuctionEntryLog_Click(object sender,EventArgs e) {
		if (IsValid) {
			this.CloseExtInfo();
			this.GetData();

			if (ViCommConst.ProductType.IsAuction(this.hdnProductType.Value)) {
				if (this.IsExistProductData()) {
					this.pnlExtInfo.Visible = true;
					this.pnlAuctionEntryLog.Visible = true;
					this.grdAuctionEntryLog.DataSourceID = "dsAuctionEntryLog";
					this.grdAuctionEntryLog.DataBind();
				}
			}
		}
	}
	
	protected void btnClose_Ext_Click(object sender,EventArgs e) {
		CloseExtInfo();
	}

	protected void btnUpdateAuctionStatus_Click(object sender, EventArgs e) {
		DropDownList oAuctionStatusList = this.dvwProductAuction.FindControl("lstAuctionStatus") as DropDownList;
		if (oAuctionStatusList == null) {
			return;
		}
		if (string.IsNullOrEmpty(oAuctionStatusList.SelectedValue)) {
			return;
		}
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("AUCTION_STATUS_UPDATE");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.hdnSiteCd.Value);
			oDbSession.ProcedureInParm("pPRODUCT_AGENT_CD", DbSession.DbType.VARCHAR2, this.hdnProductAgentCd.Value);
			oDbSession.ProcedureInParm("pPRODUCT_SEQ", DbSession.DbType.VARCHAR2, this.hdnProductSeq.Value);
			oDbSession.ProcedureInParm("pAUCTION_STATUS", DbSession.DbType.VARCHAR2, oAuctionStatusList.SelectedValue);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.GetData();
	}

	protected void btnRemarks_Click(object sender,EventArgs e){
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("PRODUCT_REMARKS_UPDATE");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.hdnSiteCd.Value);
			oDbSession.ProcedureInParm("pPRODUCT_AGENT_CD",DbSession.DbType.VARCHAR2,this.hdnProductAgentCd.Value);
			oDbSession.ProcedureInParm("pPRODUCT_SEQ",DbSession.DbType.VARCHAR2,this.hdnProductSeq.Value);
			oDbSession.ProcedureInParm("pREMARKS",DbSession.DbType.VARCHAR2,this.txtRemarks.Text);
			oDbSession.ProcedureInParm("pREVISION_NO",DbSession.DbType.NUMBER,this.hdnRevisionNo.Value);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}
		DataBind();
	}

	protected string ConvertCarrierCd2Nm(object pCarrierCd) {
		string sCarrierCd = iBridUtil.GetStringValue(pCarrierCd);
		switch (sCarrierCd) {
			case "d":
				return "docomo";
			case "a":
				return "AU";
			case "s":
				return "Softbank";
			default:
				return string.Empty;
		}
	}
	protected string TrimEnd(string pTargetStr) {
		char[] trims = { ' ' };
		string sRetStr = pTargetStr.TrimEnd(trims);

		return sRetStr;
	}

	protected string GetAuctionStatusMark(object pAuctionStatus){
		using (CodeDtl oCodeDtl = new CodeDtl()) {
			return oCodeDtl.GetOne("02", iBridUtil.GetStringValue(pAuctionStatus)) ? oCodeDtl.codeNm : "-";
		}
	}
	
	protected void InitProductSituation(){
		this.pnlProductSituation.Visible = false;
	}
	private void DisplayProductSituation() {
		this.pnlProductSituation.Visible = true;
	}

	protected void CloseExtInfo(){
	
		this.pnlAuctionEntryLog.Visible = false;
		this.pnlAuctionBidLog.Visible=false;
		this.pnlExtInfo.Visible = false;
		
	}
	private bool IsExistProductData() {
		bool ret = false;
		if (this.dvwProduct.DataItem != null) {
			ret = true;
		}
		return ret;
	}
	
	protected string GetBlindBidMark(object pBlindBidFlag){
		return iBridUtil.GetStringValue(pBlindBidFlag).Equals(ViCommConst.FLAG_ON_STR) ? "○":string.Empty;
	}

	protected string GetAutoExtensionFlagMark(object pAutoExtensionFlag) {
		return iBridUtil.GetStringValue(pAutoExtensionFlag).Equals(ViCommConst.FLAG_ON_STR) ? "有り" : "無し";
	}

	protected bool GetStatusUpdateEnabled(object pAuctionStatus) {
		return ViCommConst.AuctionStatus.WinBid.Equals(iBridUtil.GetStringValue(pAuctionStatus));
	}
}
