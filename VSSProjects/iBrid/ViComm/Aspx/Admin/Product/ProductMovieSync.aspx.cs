﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;
using System.Xml;

using ViComm;
using iBridCommLib;

public partial class Product_ProductMovieSync : System.Web.UI.Page {
	private const string CODE_SUCCESS = "0";
	private const string ASP_CATEGORY_MOVIE = "10";
	private DataTable oMovieFileTypeDataTable = null;
	
	private class ContentsInfo{
		public string ContentsId;
		public string FileFormat;
		public string FileName;
		public string FileSequence;
		public string FileSize;
		public string FileType;
		public string Link;
		public string SampleFlg;
		public string ValidAu;
		public string ValidDocomo;
		public string ValidSoftbank;		
	}
	
	protected void Page_Load(object sender, EventArgs e) {
		if(!this.IsPostBack){
			using(MovieFileType oMovieFileType = new MovieFileType()){
				this.oMovieFileTypeDataTable = oMovieFileType.GetList().Tables[0];
			}
		}

	}
	
	protected void btnExecute_Click(object sender, EventArgs e) {
		if(!this.IsValid)return;
		
		string sBoundaryDate = this.txtBoundaryDate.Text.Replace("/",string.Empty);

		string sFailReason = null;
		if(!this.SyncProductAction(sBoundaryDate,out sFailReason)){
			this.lblErrorMessage.Text = sFailReason;
		}
	}
	protected void btnDownloadImage_Click(object sender, EventArgs e) {
		if (!this.IsValid) return;
		
		
		string sProductAgentCd = this.lstProductAgent.SelectedValue;

		string sProductAgentDomain = string.Empty;

		using(ProductAgent oProductAgent = new ProductAgent()){
			using(DataSet oProductAgentDataSet = oProductAgent.GetOne(sProductAgentCd)){
				sProductAgentDomain = iBridUtil.GetStringValue(oProductAgentDataSet.Tables[0].Rows[0]["PRODUCT_AGENT_DOMAIN"]);			
			}
		}
		Response.Redirect(string.Format("http://{0}/img/arcimg/arcimg.tar",sProductAgentDomain));
		
	}
	
	
	private bool SyncProductAction(string pBoundaryDate,out string pFailReason){
		string sSiteCd = this.lstSiteCd.SelectedValue;
		string sProductAgentCd = this.lstProductAgent.SelectedValue;

		string sProductAgentDomain = string.Empty;
		string sProductAgentUid = string.Empty;
		string sProductAgentPwd = string.Empty;
		string sProductType = string.Empty;

		using(ProductAgent oProductAgent = new ProductAgent()){
			using(DataSet oProductAgentDataSet = oProductAgent.GetOne(sProductAgentCd)){
				sProductAgentDomain = iBridUtil.GetStringValue(oProductAgentDataSet.Tables[0].Rows[0]["PRODUCT_AGENT_DOMAIN"]);
				sProductAgentUid = iBridUtil.GetStringValue(oProductAgentDataSet.Tables[0].Rows[0]["PRODUCT_AGENT_UID"]);
				sProductAgentPwd = iBridUtil.GetStringValue(oProductAgentDataSet.Tables[0].Rows[0]["PRODUCT_AGENT_PWD"]);
				sProductType = iBridUtil.GetStringValue(oProductAgentDataSet.Tables[0].Rows[0]["PRODUCT_TYPE"]);
				
			}
		}

		bool bSuccessed = false;
		string sAgentIfUrl = string.Format("http://{0}/getMovie.cgi?mid={1}&mpw={2}&udate={3}",sProductAgentDomain,sProductAgentUid,sProductAgentPwd,pBoundaryDate);
			
		WebRequest oRequest = HttpWebRequest.Create(sAgentIfUrl);
		WebResponse oResponse = oRequest.GetResponse();
		try{
			using (Stream oResponseStream = oResponse.GetResponseStream()) {
				XmlReaderSettings oXmlReaderSettings = new XmlReaderSettings();
				oXmlReaderSettings.IgnoreWhitespace = true;
				
				XmlReader oXmlReader = XmlReader.Create(oResponseStream, oXmlReaderSettings);
				try{
					// =============================
					//  msg要素
					// =============================
					if(!oXmlReader.ReadToFollowing("msg")) throw new ApplicationException("not found msg element.");
					XmlReader oMsgXmlReader = oXmlReader.ReadSubtree();
					try{
						string sCode	= null;
						ParseMsgElement(oMsgXmlReader, out sCode,out pFailReason);
						
						bSuccessed = sCode.Equals(CODE_SUCCESS);	
					}finally{
						oMsgXmlReader.Close();
					}
					
					if(bSuccessed){

						// =============================
						//  item要素
						// =============================
						if (oXmlReader.ReadToFollowing("item")) {
							do {
								XmlReader oTitleXmlReader = oXmlReader.ReadSubtree();
								try{
									ParseItemElement(sSiteCd, sProductAgentCd, sProductType, oTitleXmlReader);
								}finally{
									oTitleXmlReader.Close();
								}
							} while (oXmlReader.ReadToNextSibling("item"));
						}
					}
				}finally{
					oXmlReader.Close();
				}
				oResponseStream.Flush();
				oResponseStream.Close();
			}
		}finally{
			oResponse.Close();
		}
		
		return bSuccessed;
	}
	#region □■□ XMLパース処理 □■□ ===============================================================================

	private void ParseMsgElement(XmlReader pXmlReader, out string pCode,out string pReason){
		XmlDocument oXmlDoc = new XmlDocument();
		oXmlDoc.Load(pXmlReader);

		pCode = oXmlDoc.SelectSingleNode("/msg/code").InnerText;
		pReason = oXmlDoc.SelectSingleNode("/msg/reason").InnerText;
	}

	private void ParseItemElement(string pSiteCd, string pProductAgentCd, string pProductType, XmlReader pXmlReader) {
		try{
			XmlDocument oXmlDoc = new XmlDocument();
			oXmlDoc.Load(pXmlReader);

			// 
			//  title/item
			// 
			string sTitleSeq = GetXmlNodeValue(oXmlDoc.SelectSingleNode("/item/title_seq"));
			string sTitleId = GetXmlNodeValue(oXmlDoc.SelectSingleNode("/item/title_id"));
			string sTitleNm = GetXmlNodeValue(oXmlDoc.SelectSingleNode("/item/title_nm"));
			string sTitleExplaination = GetXmlNodeValue(oXmlDoc.SelectSingleNode("/item/title_explaination"));
			string sMovieSeriesName = GetXmlNodeValue(oXmlDoc.SelectSingleNode("/item/movie_series_name"));
			string sIssueDate = GetXmlNodeValue(oXmlDoc.SelectSingleNode("/item/issued_date"));
			string sMovieMaker = GetXmlNodeValue(oXmlDoc.SelectSingleNode("/item/movie_maker"));
			string sMoviePlayTime = GetXmlNodeValue(oXmlDoc.SelectSingleNode("/item/movie_play_time"));
			string sPerformerNm = GetXmlNodeValue(oXmlDoc.SelectSingleNode("/item/performer_name"));
			string sReleaseDate = GetXmlNodeValue(oXmlDoc.SelectSingleNode("/item/release_date"));
			string sValidityDate = GetXmlNodeValue(oXmlDoc.SelectSingleNode("/item/validity_date"));
			string sTitleAffiliatePrice = GetXmlNodeValue(oXmlDoc.SelectSingleNode("/item/title_affiliate_price"));
			string sCategoryId = GetXmlNodeValue(oXmlDoc.SelectSingleNode("/item/category_id"));
			string sDelFlag = GetXmlNodeValue(oXmlDoc.SelectSingleNode("/item/del_flg"));

			// 
			//  genre(KEYWORD)
			// 
			List<string> oProductKeywordValueList = new List<string>();
			foreach (XmlNode oGenreItemNode in oXmlDoc.SelectNodes("/item/genre/item")) {
				string sProductGenreName = GetXmlNodeValue(oGenreItemNode.SelectSingleNode("genre_name"));
				string sProductParentGenreName = GetXmlNodeValue(oGenreItemNode.SelectSingleNode("parent_genre_name"));

				string sGenreName = string.Format("{0}-{1}", sProductParentGenreName,sProductGenreName);
				if (sGenreName.Equals(string.Empty)) continue;

				if (!oProductKeywordValueList.Contains(sGenreName)) {
					oProductKeywordValueList.Add(sGenreName);
				}
			}

			// 
			//  contents
			// 
			List<ContentsInfo> oContentsInfoList = new List<ContentsInfo>();
			foreach (XmlNode oContentsNode in oXmlDoc.SelectNodes("/item/contents/item")) {
				ContentsInfo oContentsInfo = new ContentsInfo();
				oContentsInfo.ContentsId = GetXmlNodeValue(oContentsNode.SelectSingleNode("contents_id"));
				oContentsInfo.FileFormat = GetXmlNodeValue(oContentsNode.SelectSingleNode("file_format"));
				oContentsInfo.FileName = GetXmlNodeValue(oContentsNode.SelectSingleNode("file_name"));
				oContentsInfo.FileSequence = GetXmlNodeValue(oContentsNode.SelectSingleNode("file_sequence"));
				oContentsInfo.FileSize = GetXmlNodeValue(oContentsNode.SelectSingleNode("file_size"));
				oContentsInfo.FileType = GetXmlNodeValue(oContentsNode.SelectSingleNode("file_type"));
				oContentsInfo.Link = GetXmlNodeValue(oContentsNode.SelectSingleNode("link"));
				oContentsInfo.SampleFlg = GetXmlNodeValue(oContentsNode.SelectSingleNode("sample_flg"));
				oContentsInfo.ValidAu = GetXmlNodeValue(oContentsNode.SelectSingleNode("valid_au"));
				oContentsInfo.ValidDocomo = GetXmlNodeValue(oContentsNode.SelectSingleNode("valid_docomo"));
				oContentsInfo.ValidSoftbank = GetXmlNodeValue(oContentsNode.SelectSingleNode("valid_softbank"));
				
				
				bool bIsValid = true;
				bIsValid = bIsValid && (oContentsInfo.FileSequence.Length <= 2);
				bIsValid = bIsValid && (!string.IsNullOrEmpty(oContentsInfo.FileType.Trim()));
				bIsValid = bIsValid && (!string.IsNullOrEmpty(oContentsInfo.FileFormat.Trim()));
				bIsValid = bIsValid && (!string.IsNullOrEmpty(oContentsInfo.FileSequence.Trim()));

				if (bIsValid) {
					oContentsInfoList.Add(oContentsInfo);
				}
			}
			
			if (sCategoryId.Equals(ASP_CATEGORY_MOVIE)){
				this.RegisterProductBasicInfo(
					pSiteCd,
					pProductAgentCd,
					pProductType,
					sTitleSeq,
					sTitleId,
					sTitleNm,
					sTitleExplaination,
					sMovieSeriesName,
					sIssueDate,
					sMovieMaker,
					sPerformerNm,
					sReleaseDate,
					sValidityDate,
					sTitleAffiliatePrice,
					sDelFlag,
					sMoviePlayTime,
					oProductKeywordValueList,
					oContentsInfoList);
			}
		}catch(Exception ex){
			EventLogWriter.Error(ex);
			// エラー分は読み飛ばし
		}
		

	}
	#endregion ========================================================================================================
	

	#region □■□ 登録処理 □■□ ====================================================================================
	/// <summary>
	///	商品基本情報を登録する
	/// </summary>
	private void RegisterProductBasicInfo(
										string pSiteCd,
										string pProductAgentCd,
										string pProductType,
										string pLinkId,
										string pPartNumber,
										string pProductNm,
										string pProductDiscription,
										string pProductSeries,
										string pReleaseDate,
										string pProductMaker,
										string pCast,
										string pPublishStartDate,
										string pPublishEndDate,
										string pGrossPrice,
										string pDelFlag,
										string pMoviePlayTime,
										List<string> pProductKeywordValueList,
										List<ContentsInfo> pContentsInfoList) {
							
		string sProductId = null;
		string sProductSeq = null;
		string sProductType = pProductType;
		string sRemarks = null;
		string sPublishFlag = ViCommConst.FLAG_WITHOUT_STR;
		string sNotNewFlag = null;
		string sRoyaltyRate = null;
		int iChargePoint = 0;
		int iSpecialChargePoint = 0;
		string sRevisionNo = null;
		DateTime oTmpDate;
		DateTime? oReleaseDate;
		DateTime oPublishStartDate;
		DateTime oPublishEndDate;
		int iGrossPrice;
		List<string> oProductGenreCategoryCdList = new List<string>();
		List<string> oProductGenreCdList = new List<string>();
		List<string> oUserSeqList = new List<string>();
		List<string> oUserCharNoList = new List<string>();
		List<string> oUnEditableFlagList = new List<string>();
		List<string> oPickupIdList = new List<string>();


		// ============================================
		//  データのサニタイズ
		// ============================================
		if (!pDelFlag.Equals(ViCommConst.FLAG_ON_STR)) {
			pDelFlag = ViCommConst.FLAG_OFF_STR;
		}
		if (!DateTime.TryParse(pReleaseDate, out oTmpDate)) {
			oReleaseDate = null;
		}else{
			oReleaseDate = oTmpDate.Date;
		}
		if (!DateTime.TryParse(pPublishStartDate, out oPublishStartDate)) {
			oPublishStartDate = DateTime.MinValue;
		}
		if (!DateTime.TryParse(pPublishEndDate, out oPublishEndDate)) {
			oPublishEndDate = DateTime.MaxValue;
		}
		if (!int.TryParse(pGrossPrice, out iGrossPrice)) {
			iGrossPrice = 0;
		}
　　　　
　　　　for(int i=0;i<pProductKeywordValueList.Count;i++){
　　　　	oUnEditableFlagList.Add(ViCommConst.FLAG_ON_STR);
　　　　}
　　　　
　　　　// ============================================
　　　　//  既登録分の引継ぎ
　　　　// ============================================
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("PRODUCT_GET_BASE");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, pSiteCd);
			oDbSession.ProcedureInParm("pPRODUCT_AGENT_CD", DbSession.DbType.VARCHAR2, pProductAgentCd);
			oDbSession.ProcedureBothParm("pPRODUCT_SEQ", DbSession.DbType.VARCHAR2, sProductSeq);
			oDbSession.ProcedureBothParm("pLINK_ID", DbSession.DbType.VARCHAR2, pLinkId);
			oDbSession.ProcedureBothParm("pPRODUCT_ID", DbSession.DbType.VARCHAR2, sProductId);
			oDbSession.ProcedureOutParm("pPRODUCT_TYPE", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPRODUCT_NM", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPRODUCT_DISCRIPTION", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPART_NUMBER", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPRODUCT_SERIES_SUMMARY", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pCAST_SUMMARY", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPRODUCT_MAKER_SUMMARY", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pPUBLISH_START_DATE", DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pPUBLISH_END_DATE", DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pRELEASE_DATE", DbSession.DbType.DATE);
			oDbSession.ProcedureOutParm("pGROSS_PRICE", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pCHARGE_POINT", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSPECIAL_CHARGE_POINT", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pPUBLISH_FLAG", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pDEL_FLAG", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pREMARKS", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pUNEDITABLE_FLAG", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pNOT_NEW_FLAG", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pROYALTY_RATE", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pREVISION_NO", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutArrayParm("pPRODUCT_GENRE_CATEGORY_CD_ARR", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutArrayParm("pPRODUCT_GENRE_CD_ARR", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutArrayParm("pPRODUCT_KEYWORD_VALUE_ARR", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutArrayParm("pUNEDITABLE_FLAG_ARR", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutArrayParm("pUSER_SEQ_ARR", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutArrayParm("pUSER_CHAR_NO_ARR", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutArrayParm("pPICKUP_ID_ARR", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pRECORD_COUNT", DbSession.DbType.NUMBER);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();

			if (oDbSession.GetIntValue("pRECORD_COUNT") > 0) {
				sProductSeq = oDbSession.GetStringValue("pPRODUCT_SEQ");
				sProductType = oDbSession.GetStringValue("pPRODUCT_TYPE");
				sPublishFlag = oDbSession.GetStringValue("pPUBLISH_FLAG");
				sRemarks = oDbSession.GetStringValue("pREMARKS");
				sRevisionNo = oDbSession.GetStringValue("pREVISION_NO");
				sNotNewFlag = oDbSession.GetStringValue("pNOT_NEW_FLAG");
				sRoyaltyRate = oDbSession.GetStringValue("pROYALTY_RATE");
				iChargePoint = oDbSession.GetIntValue("pCHARGE_POINT");
				iSpecialChargePoint = oDbSession.GetIntValue("pSPECIAL_CHARGE_POINT");

				//
				// ジャンル（引き継ぐだけ）
				//
				for (int i = 0; i < oDbSession.GetArrySize("pPRODUCT_GENRE_CD_ARR"); i++) {
					oProductGenreCategoryCdList.Add(oDbSession.GetArryStringValue("pPRODUCT_GENRE_CATEGORY_CD_ARR", i));
					oProductGenreCdList.Add(oDbSession.GetArryStringValue("pPRODUCT_GENRE_CD_ARR", i));
				}

				//
				// ｷｰﾜｰﾄﾞ（ﾏｰｼﾞ）
				//
				for (int i = 0; i < oDbSession.GetArrySize("pPRODUCT_KEYWORD_VALUE_ARR"); i++) {
					string sCurrProductKeywordValue = oDbSession.GetArryStringValue("pPRODUCT_KEYWORD_VALUE_ARR", i);
					string sCurrUnEditableFlag = oDbSession.GetArryIntValue("pUNEDITABLE_FLAG_ARR", i);
				
					if(sCurrUnEditableFlag.Equals(ViCommConst.FLAG_ON_STR))continue;
					
					if (!pProductKeywordValueList.Contains(sCurrProductKeywordValue)) {
						pProductKeywordValueList.Add(sCurrProductKeywordValue);
						oUnEditableFlagList.Add(sCurrUnEditableFlag);
					}
				}
				//
				// ﾋﾟｯｸｱｯﾌﾟID（引き継ぐだけ）
				//				
				for (int i = 0; i < oDbSession.GetArrySize("pPICKUP_ID_ARR"); i++) {
					string sPickupId = oDbSession.GetArryStringValue("pPICKUP_ID_ARR", i);
					oPickupIdList.Add(sPickupId);
				}

				//
				// ｷｬｽﾄｷｬﾗｸﾀｰ関連（引き継ぐだけ）
				//
				for (int i = 0; i < oDbSession.GetArrySize("pUSER_SEQ_ARR"); i++) {
					oUserSeqList.Add(oDbSession.GetArryStringValue("pUSER_SEQ_ARR", i));
					oUserCharNoList.Add(oDbSession.GetArryStringValue("pUSER_CHAR_NO_ARR", i));
				}

			}
		}

		// ============================================
		//  登録処理
		// ============================================
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("PRODUCT_MAINTE_BASE");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, pSiteCd);
			oDbSession.ProcedureInParm("pPRODUCT_AGENT_CD", DbSession.DbType.VARCHAR2, pProductAgentCd);
			oDbSession.ProcedureBothParm("pPRODUCT_SEQ", DbSession.DbType.NUMBER, sProductSeq);
			oDbSession.ProcedureInParm("pPRODUCT_TYPE", DbSession.DbType.VARCHAR2, sProductType);
			oDbSession.ProcedureInParm("pPRODUCT_NM", DbSession.DbType.VARCHAR2, pProductNm);
			oDbSession.ProcedureInParm("pPRODUCT_DISCRIPTION", DbSession.DbType.VARCHAR2, pProductDiscription);
			oDbSession.ProcedureInParm("pPART_NUMBER", DbSession.DbType.VARCHAR2, pPartNumber);
			oDbSession.ProcedureInParm("pPRODUCT_SERIES_SUMMARY", DbSession.DbType.VARCHAR2, pProductSeries);
			oDbSession.ProcedureInParm("pCAST_SUMMARY", DbSession.DbType.VARCHAR2, pCast);
			oDbSession.ProcedureInParm("pPRODUCT_MAKER_SUMMARY", DbSession.DbType.VARCHAR2, pProductMaker);
			oDbSession.ProcedureInParm("pPUBLISH_START_DATE", DbSession.DbType.DATE, oPublishStartDate);
			oDbSession.ProcedureInParm("pPUBLISH_END_DATE", DbSession.DbType.DATE, oPublishEndDate);
			oDbSession.ProcedureInParm("pRELEASE_DATE", DbSession.DbType.DATE, oReleaseDate);
			oDbSession.ProcedureInParm("pGROSS_PRICE", DbSession.DbType.NUMBER, iGrossPrice);
			oDbSession.ProcedureInParm("pCHARGE_POINT", DbSession.DbType.NUMBER, iChargePoint);
			oDbSession.ProcedureInParm("pSPECIAL_CHARGE_POINT", DbSession.DbType.NUMBER, iSpecialChargePoint);
			oDbSession.ProcedureInParm("pPUBLISH_FLAG", DbSession.DbType.VARCHAR2, sPublishFlag);
			oDbSession.ProcedureInParm("pDEL_FLAG", DbSession.DbType.VARCHAR2, pDelFlag);
			oDbSession.ProcedureInParm("pREMARKS", DbSession.DbType.VARCHAR2, sRemarks);
			oDbSession.ProcedureInParm("pLINK_ID", DbSession.DbType.VARCHAR2, pLinkId);
			oDbSession.ProcedureInParm("pLINK_DATE", DbSession.DbType.DATE, DateTime.Now);
			oDbSession.ProcedureInParm("pUNEDITABLE_FLAG", DbSession.DbType.VARCHAR2, ViCommConst.FLAG_ON_STR);		// 固定：ASP連携動画は編集不可
			oDbSession.ProcedureInParm("pNOT_NEW_FLAG", DbSession.DbType.NUMBER, SysPrograms.TryParseOrDafult(sNotNewFlag, 0));
			oDbSession.ProcedureInParm("pROYALTY_RATE", DbSession.DbType.NUMBER, SysPrograms.TryParseOrDafult(sRoyaltyRate,0));
			oDbSession.ProcedureInParm("pREVISION_NO", DbSession.DbType.VARCHAR2, sRevisionNo);
			oDbSession.ProcedureInArrayParm("pPRODUCT_GENRE_CATEGORY_CD_ARR", DbSession.DbType.VARCHAR2, oProductGenreCategoryCdList.ToArray());
			oDbSession.ProcedureInArrayParm("pPRODUCT_GENRE_CD_ARR", DbSession.DbType.VARCHAR2, oProductGenreCdList.ToArray());
			oDbSession.ProcedureInArrayParm("pPRODUCT_KEYWORD_VALUE_ARR", DbSession.DbType.VARCHAR2, pProductKeywordValueList.ToArray());
			oDbSession.ProcedureInArrayParm("pUNEDITABLE_FLAG_ARR", DbSession.DbType.NUMBER, oUnEditableFlagList.ToArray());
			oDbSession.ProcedureInArrayParm("pUSER_SEQ_ARR", DbSession.DbType.VARCHAR2, oUserSeqList.ToArray());
			oDbSession.ProcedureInArrayParm("pUSER_CHAR_NO_ARR", DbSession.DbType.VARCHAR2, oUserCharNoList.ToArray());
			oDbSession.ProcedureInArrayParm("pPICKUP_ID_ARR", DbSession.DbType.VARCHAR2, oPickupIdList.ToArray());
			oDbSession.ProcedureOutParm("pPRODUCT_ID", DbSession.DbType.VARCHAR2);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
			
			sProductSeq = oDbSession.GetStringValue("pPRODUCT_SEQ");
			
			this.SetupProductGenreRel(pSiteCd,pProductAgentCd,sProductSeq);
		}		
		
		if(!pDelFlag.Equals(ViCommConst.FLAG_ON_STR)){
			RegisterProductMovieInfo(pSiteCd,pProductAgentCd,sProductSeq,pLinkId,pMoviePlayTime,pContentsInfoList);
		}
	}
	
	private void SetupProductGenreRel(string pSiteCd,string pProductAgentCd,string pProductSeq){
		using(DbSession oDbSession = new DbSession()){
			oDbSession.PrepareProcedure("SETUP_PRODUCT_GENRE_REL");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, pSiteCd);
			oDbSession.ProcedureInParm("pPRODUCT_AGENT_CD", DbSession.DbType.VARCHAR2, pProductAgentCd);
			oDbSession.ProcedureInParm("pPRODUCT_SEQ", DbSession.DbType.NUMBER, pProductSeq);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.cmd.BindByName = true;
			oDbSession.ExecuteProcedure();
		}
	}

	private void RegisterProductMovieInfo(string pSiteCd, string pProductAgentCd, string pProductSeq, string pLinkId, string pMoviePlayTime, List<ContentsInfo> pContentsInfoList) {
		// ｻﾝﾌﾟﾙ動画とｺﾝﾃﾝﾂ動画を分離
		IDictionary<string,List<ContentsInfo>> oSplittedContentsInfoList = this.Split(pContentsInfoList);
		using(ProductMovie oProductMovie = new ProductMovie()){
			foreach(string sProductMovieType in oSplittedContentsInfoList.Keys){
				
				string sObjSeq = null;
				string sRevisionNo = null;	

				List<string> oSizeTypeList = new List<string>();
				List<string> oFileFormatList = new List<string>();
				List<string> oTargeCarrierList = new List<string>();
				List<string> oProductMovieSubSeqList = new List<string>();
				List<string> oFileNmList = new List<string>();
				List<string> oFileSizeList = new List<string>();
				List<string> oDownloadUrlList = new List<string>();
				List<string> oLinkIdList = new List<string>();
				
				
				sObjSeq = oProductMovie.GetObjSeqByLinkId(pSiteCd,pProductAgentCd,pProductSeq,pLinkId,sProductMovieType);
				
				foreach(ContentsInfo oContentsInfo in oSplittedContentsInfoList[sProductMovieType]){
					string sTargetCarrier = null;
					if(oContentsInfo.ValidAu.Equals(ViCommConst.FLAG_ON_STR)){
						sTargetCarrier = ViCommConst.FILE_SUFFIX_AU;
					}else if(oContentsInfo.ValidSoftbank.Equals(ViCommConst.FLAG_ON_STR)){
						sTargetCarrier = ViCommConst.FILE_SUFFIX_SOFTBANK;
					}else{
						sTargetCarrier = ViCommConst.FILE_SUFFIX_DOCOMO;
					}

					oSizeTypeList.Add(oContentsInfo.FileFormat);
					oFileFormatList.Add(oContentsInfo.FileType);
					oTargeCarrierList.Add(sTargetCarrier);
					oProductMovieSubSeqList.Add(oContentsInfo.FileSequence);
					oFileNmList.Add(oContentsInfo.FileName);
					oFileSizeList.Add(oContentsInfo.FileSize);
					oDownloadUrlList.Add(oContentsInfo.Link);
					oLinkIdList.Add(oContentsInfo.ContentsId);
				}
				using (DbSession oDbSession = new DbSession()) {			
					oDbSession.PrepareProcedure("PRODUCT_MOVIE_GET");
					oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, pSiteCd);
					oDbSession.ProcedureInParm("pPRODUCT_AGENT_CD", DbSession.DbType.VARCHAR2, pProductAgentCd);
					oDbSession.ProcedureInParm("pPRODUCT_SEQ", DbSession.DbType.NUMBER, pProductSeq);
					oDbSession.ProcedureInParm("pOBJ_SEQ", DbSession.DbType.NUMBER, sObjSeq);
					oDbSession.ProcedureOutParm("pLINK_ID", DbSession.DbType.VARCHAR2);
					oDbSession.ProcedureOutParm("pLINK_DATE", DbSession.DbType.DATE);
					oDbSession.ProcedureOutParm("pUPLOAD_DATE", DbSession.DbType.DATE);
					oDbSession.ProcedureOutParm("pPRODUCT_MOVIE_TYPE", DbSession.DbType.VARCHAR2);
					oDbSession.ProcedureOutParm("pPLAY_TIME", DbSession.DbType.VARCHAR2);
					oDbSession.ProcedureOutParm("pUNEDITABLE_FLAG", DbSession.DbType.NUMBER);
					oDbSession.ProcedureOutParm("pOUTER_MOVIE_FLAG", DbSession.DbType.NUMBER);
					oDbSession.ProcedureOutParm("pREVISION_NO", DbSession.DbType.NUMBER);
					oDbSession.ProcedureOutParm("pRECORD_COUNT", DbSession.DbType.VARCHAR2);
					oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
					oDbSession.cmd.BindByName = true;
					oDbSession.ExecuteProcedure();
					sRevisionNo = oDbSession.GetStringValue("pREVISION_NO");
				}
				using (DbSession oDbSession = new DbSession()){
					oDbSession.PrepareProcedure("PRODUCT_MOVIE_MAINTE");
					oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, pSiteCd);
					oDbSession.ProcedureInParm("pPRODUCT_AGENT_CD", DbSession.DbType.VARCHAR2, pProductAgentCd);
					oDbSession.ProcedureInParm("pPRODUCT_SEQ", DbSession.DbType.NUMBER, pProductSeq);
					oDbSession.ProcedureInParm("pOBJ_SEQ", DbSession.DbType.NUMBER, sObjSeq.Equals(string.Empty) ? null : sObjSeq);
					oDbSession.ProcedureInParm("pPRODUCT_MOVIE_TYPE", DbSession.DbType.VARCHAR2, sProductMovieType);
					oDbSession.ProcedureInParm("pPLAY_TIME", DbSession.DbType.VARCHAR2, pMoviePlayTime);
					oDbSession.ProcedureInParm("pUNEDITABLE_FLAG", DbSession.DbType.NUMBER, ViCommConst.FLAG_ON);
					oDbSession.ProcedureInParm("pOUTER_MOVIE_FLAG", DbSession.DbType.NUMBER, ViCommConst.FLAG_ON);
					oDbSession.ProcedureInParm("pLINK_ID", DbSession.DbType.VARCHAR2, pLinkId);
					oDbSession.ProcedureInParm("pREVISION_NO", DbSession.DbType.NUMBER, sRevisionNo);
					oDbSession.ProcedureInParm("pWITH_DTL_MAINTE", DbSession.DbType.NUMBER, ViCommConst.FLAG_ON);
					oDbSession.ProcedureInArrayParm("pCP_SIZE_TYPE_ARR", DbSession.DbType.VARCHAR2, oSizeTypeList.ToArray());
					oDbSession.ProcedureInArrayParm("pCP_FILE_FORMAT_ARR", DbSession.DbType.VARCHAR2, oFileFormatList.ToArray());
					oDbSession.ProcedureInArrayParm("pCP_TARGET_CARRIER_ARR", DbSession.DbType.VARCHAR2, oTargeCarrierList.ToArray());
					oDbSession.ProcedureInArrayParm("pPRODUCT_MOVIE_SUBSEQ_ARR", DbSession.DbType.NUMBER, oProductMovieSubSeqList.ToArray());
					oDbSession.ProcedureInArrayParm("pFILE_NM_ARR", DbSession.DbType.VARCHAR2, oFileNmList.ToArray());
					oDbSession.ProcedureInArrayParm("pFILE_SIZE_ARR", DbSession.DbType.NUMBER, oFileSizeList.ToArray());
					oDbSession.ProcedureInArrayParm("pDOWNLOAD_URL_ARR", DbSession.DbType.VARCHAR2, oDownloadUrlList.ToArray());
					oDbSession.ProcedureInArrayParm("pLINK_ID_ARR", DbSession.DbType.VARCHAR2, oLinkIdList.ToArray());
					oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
					oDbSession.cmd.BindByName = true;				
					oDbSession.ExecuteProcedure();
				}
			}
		}
	}
	
	
	private IDictionary<string,List<ContentsInfo>> Split(List<ContentsInfo> pContentsInfoList){
		IDictionary<string,List<ContentsInfo>> oSplittedContentsInfoList = new Dictionary<string,List<ContentsInfo>>();
		oSplittedContentsInfoList.Add(ViCommConst.ProductMovieType.CONTENTS,new List<ContentsInfo>());
		oSplittedContentsInfoList.Add(ViCommConst.ProductMovieType.SAMPLE, new List<ContentsInfo>());
	
		foreach(ContentsInfo oContentsInfo in pContentsInfoList){
			if(oContentsInfo.SampleFlg.Equals(ViCommConst.FLAG_ON_STR)){
				oSplittedContentsInfoList[ViCommConst.ProductMovieType.SAMPLE].Add(oContentsInfo);
			}else{
				oSplittedContentsInfoList[ViCommConst.ProductMovieType.CONTENTS].Add(oContentsInfo);
			}
		}

		return oSplittedContentsInfoList;		
	}
	#endregion ========================================================================================================
	
	private string GetXmlNodeValue(XmlNode pXmlNode){
		return (pXmlNode == null) ? string.Empty : pXmlNode.InnerText;
	}
	
}
