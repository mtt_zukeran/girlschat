﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ProductInquery.aspx.cs" Inherits="Product_ProductInquery" Title="商品検索"
    ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="商品検索"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[検索条件]</legend>
                <table border="0" class="tableStyle" style="width: 1100px">
                    <tr>
                        <td class="tdHeaderStyle">
                            サイト
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstSiteCd" runat="server" Width="180px" AutoPostBack="True"
                                DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" OnSelectedIndexChanged="lstSiteCd_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td class="tdHeaderStyle">
                            ｺﾝﾃﾝﾂ ﾌﾟﾛﾊﾞｲﾀﾞ
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstProductAgent" runat="server" Width="180px" AppendDataBoundItems="true"
                                DataSourceID="dsProductAgent" DataTextField="PRODUCT_AGENT_NM" DataValueField="PRODUCT_AGENT_CD">
                                <asp:ListItem Text="" Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="tdHeaderStyle">
                            商品種別
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstProductType" runat="server" Width="180px" AutoPostBack="True"
                                DataSourceID="dsProductType" DataTextField="CODE_NM" DataValueField="CODE" OnSelectedIndexChanged="lstProductType_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            商品名
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtProductNm" runat="server" Width="200px"></asp:TextBox>
                        </td>
                        <td class="tdHeaderStyle">
                            商品シリーズ
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtProductSeries" runat="server" Width="200px"></asp:TextBox>
                        </td>
                        <td class="tdHeaderStyle">
                            出演者
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtCast" runat="server" Width="200px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            商品ID
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtProductId" runat="server" Width="150"></asp:TextBox>
                        </td>
                        <td class="tdHeaderStyle2">
                            商品番号
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtPartNumber" runat="server" Width="150"></asp:TextBox>
                        </td>
                        <td class="tdHeaderStyle">
                            連携ID
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtLinkId" runat="server" Width="150"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            公開/非公開
                        </td>
                        <td class="tdDataStyle" colspan="3">
                            <asp:RadioButtonList ID="rdoPublishFlag" runat="server" RepeatDirection="horizontal">
                                <asp:ListItem Text="全て" Value=""></asp:ListItem>
                                <asp:ListItem Text="未設定のみ" Value="-1"></asp:ListItem>
                                <asp:ListItem Text="公開のみ" Value="1"></asp:ListItem>
                                <asp:ListItem Text="非公開のみ" Value="0"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td class="tdHeaderStyle">
                            ピックアップ種別
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstProductPickupManage" runat="server" DataSourceID="dsProductPickupManage"
                                DataTextField="PICKUP_TITLE" DataValueField="PICKUP_ID" Width="180px" OnDataBound="lstProductPickupManage_DataBound">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            公開開始日
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtPublishStartDateFrom" runat="server" Width="63"></asp:TextBox>
                            ～
                            <asp:TextBox ID="txtPublishStartDateTo" runat="server" Width="63"></asp:TextBox>
                            <asp:RangeValidator ID="vdrPublishStartDateFrom" runat="server" ErrorMessage="公開開始日Fromを正しく入力して下さい。"
                                ControlToValidate="txtPublishStartDateFrom" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
                                Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                            <asp:RangeValidator ID="vdrPublishStartDateTo" runat="server" ErrorMessage="公開開始日Toを正しく入力して下さい。"
                                ControlToValidate="txtPublishStartDateTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
                                Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                            <asp:CompareValidator ID="vdcPublishStartDateFrom" runat="server" ErrorMessage="大小関係が正しくありません。"
                                ControlToCompare="txtPublishStartDateFrom" ControlToValidate="txtPublishStartDateTo"
                                Operator="GreaterThanEqual" ValidationGroup="Key" Display="Dynamic">*</asp:CompareValidator>
                        </td>
                        <td class="tdHeaderStyle">
                            公開終了日
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtPublishEndDateFrom" runat="server" Width="63"></asp:TextBox>
                            ～
                            <asp:TextBox ID="txtPublishEndDateTo" runat="server" Width="63"></asp:TextBox>
                            <asp:RangeValidator ID="vdrPublishEndDateFrom" runat="server" ErrorMessage="公開終了日Fromを正しく入力して下さい。"
                                ControlToValidate="txtPublishEndDateFrom" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
                                Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                            <asp:RangeValidator ID="vdrPublishEndDateTo" runat="server" ErrorMessage="公開終了日Toを正しく入力して下さい。"
                                ControlToValidate="txtPublishEndDateTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
                                Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                            <asp:CompareValidator ID="vdcPublishEndDateFrom" runat="server" ErrorMessage="大小関係が正しくありません。"
                                ControlToCompare="txtPublishEndDateFrom" ControlToValidate="txtPublishEndDateTo"
                                Operator="GreaterThanEqual" ValidationGroup="Key" Display="Dynamic">*</asp:CompareValidator>
                        </td>
                        <td class="tdHeaderStyle">
                            連携削除
                        </td>
                        <td class="tdDataStyle">
                            <asp:CheckBox ID="chkDelete" runat="server" Text="連携削除データを対象とする"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            ログインID
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtLoginId" runat="server" Width="150"></asp:TextBox>
                        </td>
                        <td class="tdHeaderStyle">
                            ハンドル名
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtHandleName" runat="server" Width="150"></asp:TextBox>
                        </td>
                        <td class="tdHeaderStyle">
                            新着表示/非表示
                        </td>
                        <td class="tdDataStyle" colspan="3">
                            <asp:RadioButtonList ID="rdoNotNewFlag" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="表示" Value="0" Selected="true"></asp:ListItem>
                                <asp:ListItem Text="非表示" Value="1"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            商品ｷｰﾜｰﾄﾞ値
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtProductKeyWord" runat="server" Width="150"></asp:TextBox>
                        </td>
                        <td class="tdHeaderStyle">
                            お気に入られ数
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtFavoriteMeCountFrom" runat="server" MaxLength="6" Width="45px"></asp:TextBox>
                            &nbsp;～
                            <asp:TextBox ID="txtFavoriteMeCountTo" runat="server" MaxLength="6" Width="45px"></asp:TextBox>
                            <asp:RangeValidator ID="vdrFavoritMeCountFrom" runat="server" ErrorMessage="お気に入られ数Fromを正しく入力して下さい。"
                                ControlToValidate="txtFavoriteMeCountFrom" MaximumValue="999999" MinimumValue="0"
                                Type="Integer" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                            <asp:RangeValidator ID="vdrFavoritMeCountTo" runat="server" ErrorMessage="お気に入られ数Toを正しく入力して下さい。"
                                ControlToValidate="txtFavoriteMeCountTo" MaximumValue="999999" MinimumValue="0"
                                Type="Integer" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                            <asp:CompareValidator ID="vdcFavoritMeCountFrom" runat="server" ErrorMessage="大小関係が正しくありません。"
                                ControlToCompare="txtFavoriteMeCountFrom" ControlToValidate="txtFavoriteMeCountTo"
                                Operator="GreaterThanEqual" ValidationGroup="Key" Type="Integer" Display="Dynamic">*</asp:CompareValidator>
                        </td>
                        <td class="tdHeaderStyle">
                            購入数
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtBuyCountFrom" runat="server" MaxLength="6" Width="45px"></asp:TextBox>
                            &nbsp;～
                            <asp:TextBox ID="txtBuyCountTo" runat="server" MaxLength="6" Width="45px"></asp:TextBox>
                            <asp:RangeValidator ID="vdrBuyCountFrom" runat="server" ErrorMessage="購入数Fromを正しく入力して下さい。"
                                ControlToValidate="txtBuyCountFrom" MaximumValue="999999" MinimumValue="0" Type="Integer"
                                ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                            <asp:RangeValidator ID="vdrBuyCountTo" runat="server" ErrorMessage="購入数Toを正しく入力して下さい。"
                                ControlToValidate="txtBuyCountTo" MaximumValue="999999" MinimumValue="0" Type="Integer"
                                ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                            <asp:CompareValidator ID="vdcBuyCountFrom" runat="server" ErrorMessage="大小関係が正しくありません。"
                                ControlToCompare="txtBuyCountFrom" ControlToValidate="txtBuyCountTo" Operator="GreaterThanEqual"
                                ValidationGroup="Key" Type="Integer" Display="Dynamic">*</asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            ジャンル1
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstGenreCd1" runat="server" Width="180px" DataSourceID="dsProductGenreCategory"
                                OnDataBound="lstGenreCd_DataBound" DataTextField="GENRE_NM" DataValueField="GENRE_CATEGORY_CD">
                            </asp:DropDownList>
                        </td>
                        <td class="tdHeaderStyle2">
                            ジャンル2
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstGenreCd2" runat="server" Width="180px" DataSourceID="dsProductGenreCategory"
                                DataTextField="GENRE_NM" DataValueField="GENRE_CATEGORY_CD" OnDataBound="lstGenreCd_DataBound">
                            </asp:DropDownList>
                        </td>
                        <td class="tdHeaderStyle2">
                            ジャンル3
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstGenreCd3" runat="server" Width="180px" DataSourceID="dsProductGenreCategory"
                                DataTextField="GENRE_NM" DataValueField="GENRE_CATEGORY_CD" OnDataBound="lstGenreCd_DataBound">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            ジャンルなし
                        </td>
                        <td class="tdDataStyle" colspan="5">
                            <asp:CheckBox ID="chkNoGenre" runat="server" Text="ジャンル設定のない商品"></asp:CheckBox>
                        </td>
                    </tr>
                    <asp:PlaceHolder ID="plcProductBasicInfo" runat="server">
                        <tr>
                            <td class="tdHeaderStyle">
                                製造元
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtProductMaker" runat="server" Width="150"></asp:TextBox>
                            </td>
                            <td class="tdHeaderStyle">
                                発売日
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtReleaseDateFrom" runat="server" Width="63"></asp:TextBox>
                                ～
                                <asp:TextBox ID="txtReleaseDateTo" runat="server" Width="63"></asp:TextBox>
                                <asp:RangeValidator ID="vdrReleaseDateFrom" runat="server" ErrorMessage="発売日Fromを正しく入力して下さい。"
                                    ControlToValidate="txtReleaseDateFrom" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
                                    Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                                <asp:RangeValidator ID="vdrReleaseDateTo" runat="server" ErrorMessage="発売日Toを正しく入力して下さい。"
                                    ControlToValidate="txtReleaseDateTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
                                    Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                                <asp:CompareValidator ID="vdcReleaseDateFrom" runat="server" ErrorMessage="大小関係が正しくありません。"
                                    ControlToCompare="txtReleaseDateFrom" ControlToValidate="txtReleaseDateTo" Operator="GreaterThanEqual"
                                    ValidationGroup="Key" Display="Dynamic">*</asp:CompareValidator>
                            </td>
                            <td class="tdHeaderStyle">
                                ロイヤリティ率
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtRoyaltyRate" runat="server" Width="150" MaxLength="3"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle2">
                                課金ポイント
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtChargePointFrom" runat="server" Width="75"></asp:TextBox>
                                ～
                                <asp:TextBox ID="txtChargePointTo" runat="server" Width="75"></asp:TextBox>
                                <asp:CompareValidator ID="vdcChargePointFrom" runat="server" ErrorMessage="大小関係が正しくありません。"
                                    ControlToCompare="txtChargePointFrom" ControlToValidate="txtChargePointTo" Operator="GreaterThanEqual"
                                    ValidationGroup="Key" Type="Integer">*</asp:CompareValidator>
                            </td>
                            <td class="tdHeaderStyle2">
                                特別課金ポイント
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtSpecialChargePointFrom" runat="server" Width="75"></asp:TextBox>
                                ～
                                <asp:TextBox ID="txtSpecialChargePointTo" runat="server" Width="75"></asp:TextBox>
                                <asp:CompareValidator ID="vdcSpecialChargePointFrom" runat="server" ErrorMessage="大小関係が正しくありません。"
                                    ControlToCompare="txtSpecialChargePointFrom" ControlToValidate="txtSpecialChargePointTo"
                                    Operator="GreaterThanEqual" ValidationGroup="Key" Type="Integer">*</asp:CompareValidator>
                            </td>
                            <td class="tdHeaderStyle2">
                                グロス価格
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtGrossPriceFrom" runat="server" Width="75"></asp:TextBox>
                                ～
                                <asp:TextBox ID="txtGrossPriceTo" runat="server" Width="75"></asp:TextBox>
                                <asp:CompareValidator ID="vdcGrossPriceFrom" runat="server" ErrorMessage="大小関係が正しくありません。"
                                    ControlToCompare="txtGrossPriceFrom" ControlToValidate="txtGrossPriceTo" Operator="GreaterThanEqual"
                                    ValidationGroup="Key" Type="Integer">*</asp:CompareValidator>
                            </td>
                        </tr>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="plcProductAuctionInfo" runat="server">
                        <tr>
                            <td class="tdHeaderStyle">
                                オークション状態
                            </td>
                            <td class="tdDataStyle" colspan="3">
                                <asp:RadioButtonList ID="rdoAuctionStatus" runat="server" RepeatDirection="Horizontal"
                                    OnDataBound="rdoAuctionStatus_DataBound" DataSourceID="dsAuctionStatus" DataTextField="CODE_NM"
                                    DataValueField="CODE">
                                </asp:RadioButtonList>
                            </td>
                            <td class="tdHeaderStyle">
                                自動延長有無
                            </td>
                            <td class="tdDataStyle">
                                <asp:RadioButtonList ID="rdoAutoExtension" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="全て" Value="" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="有り" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="無し" Value="0"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                オークション開始日時
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtAuctionStartDateFrom" runat="server" Width="63"></asp:TextBox>
                                <asp:TextBox ID="txtAuctionStartHourFrom" runat="server" Width="15px" MaxLength="2"></asp:TextBox>
                                ～
                                <asp:TextBox ID="txtAuctionStartDateTo" runat="server" Width="63"></asp:TextBox>
                                <asp:TextBox ID="txtAuctionStartHourTo" runat="server" Width="15px" MaxLength="2"></asp:TextBox>
                                <asp:RangeValidator ID="vdrAuctionStartDateFrom" runat="server" ControlToValidate="txtAuctionStartDateFrom"
                                    Display="Dynamic" ErrorMessage="オークション開始日時Fromを正しく入力して下さい。" MaximumValue="2050/12/31"
                                    MinimumValue="0001/01/01" Type="Date" ValidationGroup="Key">*</asp:RangeValidator>
                                <asp:RangeValidator ID="vdrAuctionStartDateTo" runat="server" ControlToValidate="txtAuctionStartDateTo"
                                    Display="Dynamic" ErrorMessage="オークション開始日時Toを正しく入力して下さい。" MaximumValue="2050/12/31"
                                    MinimumValue="0001/01/01" Type="Date" ValidationGroup="Key">*</asp:RangeValidator>
                                <asp:RangeValidator ID="vdrAuctionStartHourFrom" runat="server" ControlToValidate="txtAuctionStartHourFrom"
                                    Display="Dynamic" ErrorMessage="オークション開始日時Fromを正しく入力して下さい。" MaximumValue="23"
                                    MinimumValue="00" Type="Integer" ValidationGroup="Key">*</asp:RangeValidator>
                                <asp:RangeValidator ID="vdrAuctionStartHourTo" runat="server" ControlToValidate="txtAuctionStartHourTo"
                                    Display="Dynamic" ErrorMessage="オークション開始日時Toを正しく入力して下さい。" MaximumValue="23" MinimumValue="00"
                                    Type="Integer" ValidationGroup="Key">*</asp:RangeValidator>
                                <asp:CompareValidator ID="vdcAuctionStartDateFromTo" runat="server" ControlToCompare="txtAuctionStartDateFrom"
                                    ControlToValidate="txtAuctionStartDateTo" Display="Dynamic" ErrorMessage="大小関係が正しくありません。"
                                    Operator="GreaterThanEqual" ValidationGroup="Key">*</asp:CompareValidator>
                            </td>
                            <td class="tdHeaderStyle">
                                ブラインド終了日時
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtBlindEndDateFrom" runat="server" Width="63"></asp:TextBox>
                                <asp:TextBox ID="txtBlindEndHourFrom" runat="server" Width="15px" MaxLength="2"></asp:TextBox>
                                ～
                                <asp:TextBox ID="txtBlindEndDateTo" runat="server" Width="63"></asp:TextBox>
                                <asp:TextBox ID="txtBlindEndHourTo" runat="server" Width="15px" MaxLength="2"></asp:TextBox>
                                <asp:RangeValidator ID="vdrBlindEndDateFrom" runat="server" ControlToValidate="txtBlindEndDateFrom"
                                    Display="Dynamic" ErrorMessage="ブラインド終了日時Fromを正しく入力して下さい。" MaximumValue="2050/12/31"
                                    MinimumValue="0001/01/01" Type="Date" ValidationGroup="Key">*</asp:RangeValidator>
                                <asp:RangeValidator ID="vdrBlindEndDateTo" runat="server" ControlToValidate="txtBlindEndDateTo"
                                    Display="Dynamic" ErrorMessage="ブラインド終了日時Toを正しく入力して下さい。" MaximumValue="2050/12/31"
                                    MinimumValue="0001/01/01" Type="Date" ValidationGroup="Key">*</asp:RangeValidator>
                                <asp:RangeValidator ID="vdrBlindEndHourFrom" runat="server" ControlToValidate="txtBlindEndHourFrom"
                                    Display="Dynamic" ErrorMessage="ブラインド終了日時Fromを正しく入力して下さい。" MaximumValue="23"
                                    MinimumValue="00" Type="Integer" ValidationGroup="Key">*</asp:RangeValidator>
                                <asp:RangeValidator ID="vdrBlindEndHourTo" runat="server" ControlToValidate="txtBlindEndHourTo"
                                    Display="Dynamic" ErrorMessage="ブラインド終了日時Toを正しく入力して下さい。" MaximumValue="23" MinimumValue="00"
                                    Type="Integer" ValidationGroup="Key">*</asp:RangeValidator>
                                <asp:CompareValidator ID="vdcBlindEndDateFromTo" runat="server" ControlToCompare="txtBlindEndDateFrom"
                                    ControlToValidate="txtBlindEndDateTo" Display="Dynamic" ErrorMessage="大小関係が正しくありません。"
                                    Operator="GreaterThanEqual" ValidationGroup="Key">*</asp:CompareValidator>
                            </td>
                            <td class="tdHeaderStyle">
                                オークション終了日時
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtAuctionEndDateFrom" runat="server" Width="63"></asp:TextBox>
                                <asp:TextBox ID="txtAuctionEndHourFrom" runat="server" Width="15px" MaxLength="2"></asp:TextBox>
                                ～
                                <asp:TextBox ID="txtAuctionEndDateTo" runat="server" Width="63"></asp:TextBox>
                                <asp:TextBox ID="txtAuctionEndHourTo" runat="server" Width="15px" MaxLength="2"></asp:TextBox>
                                <asp:RangeValidator ID="vdrAuctionEndDateFrom" runat="server" ControlToValidate="txtAuctionEndDateFrom"
                                    Display="Dynamic" ErrorMessage="オークション終了日時Fromを正しく入力して下さい。" MaximumValue="2050/12/31"
                                    MinimumValue="0001/01/01" Type="Date" ValidationGroup="Key">*</asp:RangeValidator>
                                <asp:RangeValidator ID="vdrAuctionEndDateTo" runat="server" ControlToValidate="txtAuctionEndDateTo"
                                    Display="Dynamic" ErrorMessage="オークション終了日時Toを正しく入力して下さい。" MaximumValue="2050/12/31"
                                    MinimumValue="0001/01/01" Type="Date" ValidationGroup="Key">*</asp:RangeValidator>
                                <asp:RangeValidator ID="vdrAuctionEndHourFrom" runat="server" ControlToValidate="txtAuctionEndHourFrom"
                                    Display="Dynamic" ErrorMessage="オークション終了日時Fromを正しく入力して下さい。" MaximumValue="23"
                                    MinimumValue="00" Type="Integer" ValidationGroup="Key">*</asp:RangeValidator>
                                <asp:RangeValidator ID="vdrAuctionEndHourTo" runat="server" ControlToValidate="txtAuctionEndHourTo"
                                    Display="Dynamic" ErrorMessage="オークション終了日時Toを正しく入力して下さい。" MaximumValue="23" MinimumValue="00"
                                    Type="Integer" ValidationGroup="Key">*</asp:RangeValidator>
                                <asp:CompareValidator ID="vdcAuctionEndDateFromTo" runat="server" ControlToCompare="txtAuctionEndDateFrom"
                                    ControlToValidate="txtAuctionEndDateTo" Display="Dynamic" ErrorMessage="大小関係が正しくありません。"
                                    Operator="GreaterThanEqual" ValidationGroup="Key">*</asp:CompareValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                開始金額
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtReserveAmtFrom" runat="server" Width="55" MaxLength="8"></asp:TextBox>
                                ～
                                <asp:TextBox ID="txtReserveAmtTo" runat="server" Width="55" MaxLength="8"></asp:TextBox>
                                <asp:CompareValidator ID="vdcReserveAmtFromTo" runat="server" ControlToCompare="txtReserveAmtFrom"
                                    ControlToValidate="txtReserveAmtTo" ErrorMessage="大小関係が正しくありません。" Operator="GreaterThanEqual"
                                    Type="Integer" ValidationGroup="Key">*</asp:CompareValidator>
                            </td>
                            <td class="tdHeaderStyle">
                                参加ポイント
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtEntryPointFrom" runat="server" Width="45" MaxLength="6"></asp:TextBox>
                                ～
                                <asp:TextBox ID="txtEntryPointTo" runat="server" Width="45" MaxLength="6"></asp:TextBox>
                                <asp:CompareValidator ID="vdcEntryPointFromTo" runat="server" ControlToCompare="txtEntryPointFrom"
                                    ControlToValidate="txtEntryPointTo" ErrorMessage="大小関係が正しくありません。" Operator="GreaterThanEqual"
                                    Type="Integer" ValidationGroup="Key">*</asp:CompareValidator>
                            </td>
                            <td class="tdHeaderStyle">
                                参加ﾎﾟｲﾝﾄ ﾌﾞﾗｲﾝﾄﾞ時
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtBlindEntryPointFrom" runat="server" Width="45" MaxLength="6"></asp:TextBox>
                                ～
                                <asp:TextBox ID="txtBlindEntryPointTo" runat="server" Width="45" MaxLength="6"></asp:TextBox>
                                <asp:CompareValidator ID="vdcBlindEntryPointFromTo" runat="server" ControlToCompare="txtBlindEntryPointFrom"
                                    ControlToValidate="txtBlindEntryPointTo" ErrorMessage="大小関係が正しくありません。" Operator="GreaterThanEqual"
                                    Type="Integer" ValidationGroup="Key">*</asp:CompareValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle2">
                                入札単位
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtMinimumBidAmtFrom" runat="server" Width="55" MaxLength="8"></asp:TextBox>
                                ～
                                <asp:TextBox ID="txtMinimumBidAmtTo" runat="server" Width="55" MaxLength="8"></asp:TextBox>
                                <asp:CompareValidator ID="vdcMinimumBidAmtFromTo" runat="server" ControlToCompare="txtMinimumBidAmtFrom"
                                    ControlToValidate="txtMinimumBidAmtTo" ErrorMessage="大小関係が正しくありません。" Operator="GreaterThanEqual"
                                    Type="Integer" ValidationGroup="Key">*</asp:CompareValidator>
                            </td>
                            <td class="tdHeaderStyle2">
                                入札ﾎﾟｲﾝﾄ
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtBidPointFrom" runat="server" Width="45" MaxLength="6"></asp:TextBox>
                                ～
                                <asp:TextBox ID="txtBidPointTo" runat="server" Width="45" MaxLength="6"></asp:TextBox>
                                <asp:CompareValidator ID="vdcBidPointFromTo" runat="server" ControlToCompare="txtBidPointFrom"
                                    ControlToValidate="txtBidPointTo" ErrorMessage="大小関係が正しくありません。" Operator="GreaterThanEqual"
                                    Type="Integer" ValidationGroup="Key">*</asp:CompareValidator>
                            </td>
                            <td class="tdHeaderStyle2">
                                入札ﾎﾟｲﾝﾄ ﾌﾞﾗｲﾝﾄﾞ時
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtBlindBidPointFrom" runat="server" Width="45" MaxLength="6"></asp:TextBox>
                                ～
                                <asp:TextBox ID="txtBlindBidPointTo" runat="server" Width="45" MaxLength="6"></asp:TextBox>
                                <asp:CompareValidator ID="vdcBlindBidPointFromTo" runat="server" ControlToCompare="txtBlindBidPointFrom"
                                    ControlToValidate="txtBlindBidPointTo" ErrorMessage="大小関係が正しくありません。" Operator="GreaterThanEqual"
                                    Type="Integer" ValidationGroup="Key">*</asp:CompareValidator>
                            </td>
                        </tr>
                    </asp:PlaceHolder>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" Width="100px"
                    OnClick="btnListSeek_Click" CausesValidation="True" ValidationGroup="Key" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" Width="100px"
                    OnClick="btnClear_Click" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlList">
            <fieldset>
                <legend>
                    <%= DisplayWordUtil.Replace("[商品一覧]") %>
                </legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%#GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%=grdProduct.PageIndex + 1%>
                        of
                        <%=grdProduct.PageCount%>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:LinkButton ID="lnkCondition" runat="server" OnClick="lnkCondition_Click" Text="[条件非表示]" />
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="290px">
                    <asp:GridView ID="grdProduct" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        DataSourceID="dsProduct" AllowSorting="True" SkinID="GridView" OnRowDataBound="grdProduct_RowDataBound"
                        OnDataBound="grdProduct_DataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="ID<br />/商品画像" SortExpression="PRODUCT_ID">
                                <ItemStyle HorizontalAlign="Center" Width="85px" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkProductView" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"PRODUCT_ID") %>'
                                        NavigateUrl='<%# GenerateProductViewUrl(DataBinder.Eval(Container.DataItem,"PRODUCT_ID")) %>'>
                                    </asp:HyperLink>
                                    <br />
                                    <asp:Image ID="imgProductPic" AlternateText="未登録" ForeColor="#FF0000" runat="server"
                                        ImageUrl='<%# string.Format("~/{0}",Eval("PRODUCT_IMG_PATH_S")) %>' Visible='<%# GetProductPicVisible(Eval("OBJ_SEQ")) %>' />
                                    <asp:Panel ID="pnlEmptyPic" runat="server">
                                        <asp:Label ID="lblEmptyPicMsg" runat="server" Text="画像未登録" ForeColor="#FF0000" Visible='<%# !GetProductPicVisible(Eval("OBJ_SEQ")) %>'></asp:Label>
                                    </asp:Panel>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="商品番号" SortExpression="PRODUCT_ID">
                                <ItemStyle HorizontalAlign="Left" Width="110px" />
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem,"PART_NUMBER") %>
                                    <br />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="商品名" SortExpression="PRODUCT_NM">
                                <ItemStyle HorizontalAlign="Left" Width="300px" />
                                <ItemTemplate>
                                    <asp:Label ID="lnkCastView" runat="server" Text='<%# Bind("PRODUCT_NM") %>'></asp:Label>
                                    <span style="color: Teal">
                                        <asp:Label ID="lblPickup" runat="server" Text='<%# GetPickupMask(Eval("PICKUP_MASK")) %>'></asp:Label>
                                    </span>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="関連出演者">
                                <ItemStyle HorizontalAlign="Left" Width="100px" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblNandleNm" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "HANDLE_NM") %>'
                                        NavigateUrl='<%# GenerateCastViewUrl(DataBinder.Eval(Container.DataItem, "LOGIN_ID")) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="公開開始日<br />/公開終了日">
                                <HeaderStyle HorizontalAlign="center" />
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "PUBLISH_START_DATE", "{0:yyyy/MM/dd}")%>
                                    <br />
                                    <%# DataBinder.Eval(Container.DataItem, "PUBLISH_END_DATE", "{0:yyyy/MM/dd}")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ｵｰｸｼｮﾝ開始日時<br />/ﾌﾞﾗｲﾝﾄﾞ終了日時<br />/ｵｰｸｼｮﾝ終了日時">
                                <HeaderStyle HorizontalAlign="center" />
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "AUCTION_START_DATE", "{0:yyyy/MM/dd HH:mm:ss}")%>
                                    <br />
                                    <%# DataBinder.Eval(Container.DataItem, "BLIND_END_DATE", "{0:yyyy/MM/dd HH:mm:ss}")%>
                                    <br />
                                    <%# DataBinder.Eval(Container.DataItem, "AUCTION_END_DATE", "{0:yyyy/MM/dd HH:mm:ss}")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ｸﾞﾛｽ価格" SortExpression="GROSS_PRICE">
                                <ItemStyle HorizontalAlign="Right" />
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "GROSS_PRICE", "&yen;{0:N0}")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="購入ﾎﾟｲﾝﾄ<br />/特別ﾎﾟｲﾝﾄ">
                                <HeaderStyle HorizontalAlign="center" />
                                <ItemStyle HorizontalAlign="Right" />
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "CHARGE_POINT", "{0:N0}pt")%>
                                    <br />
                                    <%# DataBinder.Eval(Container.DataItem, "SPECIAL_CHARGE_POINT", "{0:N0}pt")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="最高入札額<br />/入札回数">
                                <HeaderStyle HorizontalAlign="center" />
                                <ItemStyle HorizontalAlign="Right" />
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "MAX_BID_AMT", "&yen;{0:N0}")%>
                                    <br />
                                    <%# DataBinder.Eval(Container.DataItem, "BID_COUNT", "{0:N0}")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="公開状態" SortExpression="PUBLISH_FLAG">
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblPublishFlag" runat="server" Text='<%# GetPublishFlagNm(Eval("PUBLISH_FLAG")) %>'
                                        ForeColor='<%# GetPublishFlagNmColor(Eval("PUBLISH_FLAG")) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ｵｰｸｼｮﾝ状態<br />/自動延長" SortExpression="PUBLISH_FLAG">
                                <ItemStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lblLastBidDate" runat="server" Text='<%# Eval("AUCTION_STATUS_NM") %>'></asp:Label>
                                    <br />
                                    <asp:Label ID="lblAutoExtension" runat="server" Text='<%# ViComm.ViCommConst.FLAG_ON_STR.Equals(Eval("AUTO_EXTENSION_FLAG","{0}")) ? "延長有り" : "延長無し" %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="お気に入られ数" SortExpression="FAVORITE_ME_COUNT">
                                <ItemTemplate>
                                    <asp:Label ID="lblFavoriteMeCount" runat="server" Text='<%# Eval("FAVORITE_ME_COUNT", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="購入数" SortExpression="BUY_COUNT">
                                <ItemTemplate>
                                    <asp:Label ID="lblBuyCount" runat="server" Text='<%# Eval("BUY_COUNT", "{0:N0}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="50px" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <% // ************************************* %>
    <% //  DataSource                           %>
    <% // ************************************* %>
    <asp:ObjectDataSource ID="dsProduct" runat="server" TypeName="Product" SelectCountMethod="GetPageCount"
        SelectMethod="GetPageCollection" EnablePaging="True" OnSelected="dsProduct_Selected"
        OnSelecting="dsProduct_Selecting">
        <SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="Object" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site">
        <SelectParameters>
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsProductAgent" runat="server" SelectMethod="GetList" TypeName="ProductAgent">
        <SelectParameters>
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsProductType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="25" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsProductGenreCategory" runat="server" SelectMethod="GetList2"
        OnSelecting="dsProductGenreCategory_Selecting" TypeName="ProductGenreCategory">
        <SelectParameters>
            <asp:Parameter Name="pSiteCd" Type="String" />
            <asp:Parameter Name="pProductType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsProductPickupManage" runat="server" SelectMethod="GetList"
        TypeName="ProductPickupManage">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:ControlParameter ControlID="lstProductType" Name="pProductType" PropertyName="SelectedValue"
                Type="String" />
            <%--<asp:Parameter Name="pPickupType" Type="String" DefaultValue="01" />--%>
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsAuctionStatus" runat="server" SelectMethod="GetList"
        TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="02" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilterNum1" runat="server" Enabled="true"
        FilterType="Numbers" TargetControlID="txtSpecialChargePointFrom" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilterNum2" runat="server" Enabled="true"
        FilterType="Numbers" TargetControlID="txtSpecialChargePointTo" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilterNum3" runat="server" Enabled="true"
        FilterType="Numbers" TargetControlID="txtGrossPriceFrom" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilterNum4" runat="server" Enabled="true"
        FilterType="Numbers" TargetControlID="txtGrossPriceTo" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilterNum5" runat="server" Enabled="true"
        FilterType="Numbers" TargetControlID="txtChargePointFrom" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilterNum6" runat="server" Enabled="true"
        FilterType="Numbers" TargetControlID="txtChargePointTo" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilterNum7" runat="server" Enabled="true"
        FilterType="Numbers" TargetControlID="txtRoyaltyRate" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtReserveAmtFrom" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtReserveAmtTo" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtEntryPointFrom" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtEntryPointTo" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtBlindEntryPointFrom" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtBlindEntryPointTo" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtMinimumBidAmtFrom" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtMinimumBidAmtTo" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtBidPointFrom" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtBidPointTo" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtBlindBidPointFrom" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtBlindBidPointTo" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtAuctionStartHourFrom" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtAuctionStartHourTo" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtAuctionEndHourFrom" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtAuctionEndHourTo" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtBlindEndHourFrom" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtBlindEndHourTo" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
        TargetControlID="vdrPublishStartDateFrom" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2"
        TargetControlID="vdrPublishStartDateTo" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3"
        TargetControlID="vdcPublishStartDateFrom" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4"
        TargetControlID="vdrPublishEndDateFrom" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5"
        TargetControlID="vdrPublishEndDateTo" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6"
        TargetControlID="vdcPublishEndDateFrom" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7"
        TargetControlID="vdcSpecialChargePointFrom" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8"
        TargetControlID="vdcGrossPriceFrom" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9"
        TargetControlID="vdcChargePointFrom" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10"
        TargetControlID="vdrReleaseDateFrom" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11"
        TargetControlID="vdrReleaseDateTo" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender12"
        TargetControlID="vdcReleaseDateFrom" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender13"
        TargetControlID="vdrAuctionStartDateFrom" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender14"
        TargetControlID="vdrAuctionStartDateTo" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender15"
        TargetControlID="vdcAuctionStartDateFromTo" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender16"
        TargetControlID="vdrBlindEndDateFrom" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender17"
        TargetControlID="vdrBlindEndDateTo" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender18"
        TargetControlID="vdcBlindEndDateFromTo" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender19"
        TargetControlID="vdrAuctionEndDateFrom" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender20"
        TargetControlID="vdrAuctionEndDateTo" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender21"
        TargetControlID="vdcAuctionEndDateFromTo" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender22"
        TargetControlID="vdrAuctionStartHourFrom" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender23"
        TargetControlID="vdrAuctionStartHourTo" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender24"
        TargetControlID="vdrBlindEndHourFrom" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender25"
        TargetControlID="vdrBlindEndHourTo" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender26"
        TargetControlID="vdrAuctionEndHourFrom" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender27"
        TargetControlID="vdrAuctionEndHourTo" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:MaskedEditExtender ID="mskReleaseDateFrom" runat="server" MaskType="Date"
        Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true" TargetControlID="txtReleaseDateFrom">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:MaskedEditExtender ID="mskReleaseDateTo" runat="server" MaskType="Date"
        Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true" TargetControlID="txtReleaseDateTo">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:MaskedEditExtender ID="mskPublishStartDateFrom" runat="server" MaskType="Date"
        Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true" TargetControlID="txtPublishStartDateFrom">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:MaskedEditExtender ID="mskPublishStartDateTo" runat="server" MaskType="Date"
        Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true" TargetControlID="txtPublishStartDateTo">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:MaskedEditExtender ID="mskPublishEndDateFrom" runat="server" MaskType="Date"
        Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true" TargetControlID="txtPublishEndDateFrom">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:MaskedEditExtender ID="mskPublishEndDateTo" runat="server" MaskType="Date"
        Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true" TargetControlID="txtPublishEndDateTo">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:MaskedEditExtender ID="mskAuctionStartDateFrom" runat="server" MaskType="Date"
        Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true" TargetControlID="txtAuctionStartDateFrom">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:MaskedEditExtender ID="mskAuctionStartDateTo" runat="server" MaskType="Date"
        Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true" TargetControlID="txtAuctionStartDateTo">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:MaskedEditExtender ID="mskAuctionEndDateFrom" runat="server" MaskType="Date"
        Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true" TargetControlID="txtAuctionEndDateFrom">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:MaskedEditExtender ID="mskAuctionEndDateTo" runat="server" MaskType="Date"
        Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true" TargetControlID="txtAuctionEndDateTo">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:MaskedEditExtender ID="mskBlindEndDateFrom" runat="server" MaskType="Date"
        Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true" TargetControlID="txtBlindEndDateFrom">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:MaskedEditExtender ID="mskBlindEndDateTo" runat="server" MaskType="Date"
        Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true" TargetControlID="txtBlindEndDateTo">
    </ajaxToolkit:MaskedEditExtender>
</asp:Content>
