﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ProductPicMainte.aspx.cs" Inherits="Product_ProductPicMainte" Title="画像アップロード" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="画像アップロード"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlMainte">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel runat="server" ID="pnlDtl">
                    <fieldset class="fieldset-inner">
                        <legend>
                            <%= DisplayWordUtil.Replace("[商品画像アップロード]") %>
                        </legend>
                        <table border="0" style="width: 640px" class="tableStyle">
                            <tr>
                                <td class="tdHeaderStyle">
                                    <asp:Label ID="lblHeader" runat="server" Text="Label"></asp:Label>
                                </td>
                                <td class="tdDataStyle">
                                    <asp:Label ID="lblSiteCd" runat="server" Text="Label"></asp:Label>-
                                    <asp:Label ID="lblProductId" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    <%= DisplayWordUtil.Replace("連携ID.") %>
                                </td>
                                <td class="tdDataStyle">
                                    <asp:Label ID="lblLinkId" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    <%= DisplayWordUtil.Replace("商品種別.") %>
                                </td>
                                <td class="tdDataStyle">
                                    <asp:Label ID="lblProductType" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    <%= DisplayWordUtil.Replace("写真種別.") %>
                                </td>
                                <td class="tdDataStyle">
                                    <asp:Label ID="lblProductPicType" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    <%= DisplayWordUtil.Replace("商品名.") %>
                                </td>
                                <td class="tdDataStyle">
                                    <asp:Label ID="lblProductNm" runat="server" Text="Label"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle2">
                                    ファイルアップロード
                                </td>
                                <td class="tdDataStyle">
                                    <asp:FileUpload ID="uldProductPicture" runat="server" Width="400px" />
                                    <asp:RequiredFieldValidator ID="rfvUplod" runat="server" ErrorMessage="アップロードファイルが入力されていません。"
                                        SetFocusOnError="True" ValidationGroup="Upload" ControlToValidate="uldProductPicture">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                        <asp:Button ID="btnUpload" runat="server" ValidationGroup="Upload" Text="実行" CssClass="seekbutton"
                            OnClick="btnUpload_Click" />
                        <asp:Button runat="server" ID="btnCancel" Text="戻る" CssClass="seekbutton" OnClick="btnCancel_Click"
                            CausesValidation="False" />
                        <asp:Panel ID="pnlAlreadyExists" runat="server" Visible="false">
                        <br /><br />
                            <asp:Label ID="lblAlreadyExists" runat="server" Text="すでに登録済みです。" ForeColor="Red"
                                Font-Bold="true"></asp:Label>
                        </asp:Panel>
                    </fieldset>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel ID="pnlList" runat="server">
            <fieldset>
                <legend>[商品画像一覧]</legend>
                <asp:Label ID="lblEmptyContentsMoviePic" runat="server" Text="画像は登録されていません。" ForeColor="#FF0000"
                    Font-Bold="True" Visible="false"></asp:Label>
                <asp:DataList ID="lstPic" runat="server" DataSourceID="dsProductPic" Width="300px">
                    <ItemTemplate>
                        <asp:Image ID="Image1" runat="server" ImageUrl='<%# string.Format("~/{0}",Eval("PHOTO_IMG_PATH")) %>' />
                        <br />
                        <asp:ImageButton ID="btnUp" runat="server" ImageUrl="../image/up.gif" OnClientClick="return confirm('表示を上にしますか？');"
                            OnCommand="btnUp_Command" CommandArgument='<%# Eval("OBJ_SEQ") %>' Visible='<%# Eval("UP_BTN_FLAG") %>' />&nbsp;
                        <asp:ImageButton ID="btnDown" runat="server" ImageUrl="../image/down.gif" OnClientClick="return confirm('表示を下にしますか？');"
                            OnCommand="btnDown_Command" CommandArgument='<%# Eval("OBJ_SEQ") %>' Visible='<%# Eval("DOWN_BTN_FLAG") %>' />&nbsp;
                        <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="../image/del.gif" CommandArgument='<%# string.Format("{0}:{1}",Eval("OBJ_SEQ"),Eval("REVISION_NO")) %>'
                            OnClientClick="return confirm('削除を行いますか？');" OnCommand="btnDelete_Command" /><br />
                        <br />
                    </ItemTemplate>
                </asp:DataList>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsProductPic" runat="server" SelectMethod="GetList" TypeName="ProductPic"
        OnSelecting="dsProductPic_Selecting" OnSelected="dsProductPic_Selected">
        <SelectParameters>
            <asp:Parameter Name="pSiteCd" Type="String" />
            <asp:Parameter Name="pProductAgentCd" Type="String" />
            <asp:Parameter Name="pProductSeq" Type="String" />
            <asp:Parameter Name="pProductPicType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
        TargetControlID="rfvUplod" HighlightCssClass="validatorCallout" />
</asp:Content>
