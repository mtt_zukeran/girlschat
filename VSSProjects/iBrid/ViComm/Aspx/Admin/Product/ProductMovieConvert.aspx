﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ProductMovieConvert.aspx.cs" Inherits="Product_ProductMovieConvert"
    Title="動画アップロード" %>
<%@ Import Namespace="ViComm" %>    

<script runat="server">
	public long GetLength(System.IO.FileInfo pFile){
		using (Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME, ViCommConst.FILE_UPLOAD_PASSWORD)) {
			return pFile.Length;	
		}		
	}
	
</script>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="動画アップロード"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlMainte">
            <fieldset class="fieldset-inner">
                <legend>[動画アップロード]</legend>
                <table border="0" style="width: 640px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            ｻｲﾄ／商品ID.
                        </td>
                        <td class="tdDataStyle">
                            <asp:Label ID="lblSiteCd" runat="server" Text="<%# this.SiteCd %>"></asp:Label>-
                            <asp:Label ID="lblProductId" runat="server" Text="<%# this.ProductId %>"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            連携ID.</td>
                        <td class="tdDataStyle">
                            <asp:Label ID="lblLinkId" runat="server" Text="Label"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            商品種別.
                        </td>
                        <td class="tdDataStyle">
                            <asp:Label ID="lblProductType" runat="server" Text="Label"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            動画種別.
                        </td>
                        <td class="tdDataStyle">
                            <asp:Label ID="lblProductMovieType" runat="server" Text="Label"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            商品名.
                        </td>
                        <td class="tdDataStyle">
                            <asp:Label ID="lblProductNm" runat="server" Text="Label"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            アスペクト比
                        </td>
                        <td class="tdDataStyle">
                            <asp:RadioButton ID="rdoAspectNone" runat="server" GroupName="aspect" Checked="true"
                                Text="指定しない"></asp:RadioButton>
                            <asp:RadioButton ID="rdoAspectNormal" runat="server" GroupName="aspect" Checked="false"
                                Text="1.33:1（4:3）"></asp:RadioButton>
                            <asp:RadioButton ID="rdoAspectWide" runat="server" GroupName="aspect" Checked="false"
                                Text="1.78:1（16:9）"></asp:RadioButton>
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnRefresh" Text="最新の情報に更新" CssClass="seekbutton"
                    OnClick="btnRefresh_Click" CausesValidation="False" />
                <asp:Button runat="server" ID="btnCancel" Text="戻る" CssClass="seekbutton" OnClick="btnCancel_Click"
                    CausesValidation="False" />
            </fieldset>
            <fieldset>
                <legend>登録対象動画ファイル</legend>
                <br />
                <asp:Label ID="lblAlreadyExists" runat="server" Text="すでに登録済みです。" ForeColor="Red"
                    Font-Bold="true" Visible="false"></asp:Label>
                <asp:GridView ID="grdTargetFile" runat="server" AllowPaging="true" AutoGenerateColumns="False"
                    SkinID="GridView" Width="740px" DataSourceID="dsTargetFile">
                    <EmptyDataTemplate>
                        <asp:Label ID="lblNotFound" runat="server" Text="登録対象のファイルが存在しません。" ForeColor="Red"
                            Font-Bold="true"></asp:Label>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:BoundField HeaderText="ファイル名" DataField="Name" />
                        <asp:TemplateField HeaderText="サイズ(KB)" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right">
                            <ItemTemplate>
                                <%# string.Format("{0:#,##0}",Convert2KB(GetLength((System.IO.FileInfo)Container.DataItem)))%>
                            </ItemTemplate>
                        </asp:TemplateField>
						<asp:BoundField HeaderText="更新日時" DataField="LastWriteTime" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}"
                            ItemStyle-Width="150px" ItemStyle-HorizontalAlign="Right" />
                        <asp:TemplateField HeaderText="登　録" ItemStyle-Width="64px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnConvert" runat="server" Text="登　録" OnClientClick="return confirm('登録を行いますか？');"
                                    OnCommand="btnConvert_Command" CommandArgument="<%# ((System.IO.FileInfo)Container.DataItem).FullName %>" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerSettings Mode="NumericFirstLast" />
                </asp:GridView>
            </fieldset>
        </asp:Panel>
    </div>
    <% // ====================================	%>
    <% // ObjectDataSource						%>
    <% // ====================================	%>
    <asp:ObjectDataSource ID="dsTargetFile" runat="server" TypeName="MovieConvertHelper"
        SelectMethod="GetConvertTargetFileList"></asp:ObjectDataSource>
</asp:Content>
