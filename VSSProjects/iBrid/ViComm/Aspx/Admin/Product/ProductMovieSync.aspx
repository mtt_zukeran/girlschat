﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ProductMovieSync.aspx.cs" Inherits="Product_ProductMovieSync" Title="商品動画情報取得" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="商品動画情報取得"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<asp:Panel runat="server" ID="pnlKey">
		<fieldset class="fieldset">
			<legend>[商品動画情報取得]</legend>
			<asp:Panel runat="server" ID="pnlProductHeader">
				<table border="0" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイトコード						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" Width="240px" 
								DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" AutoPostBack="True" >
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle">
							ｺﾝﾃﾝﾂ ﾌﾟﾛﾊﾞｲﾀﾞ						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstProductAgent" runat="server" Width="240px" DataSourceID="dsProductAgent" DataTextField="PRODUCT_AGENT_NM" DataValueField="PRODUCT_AGENT_CD" AppendDataBoundItems="true">
								<asp:ListItem Text="" Value="" />
							</asp:DropDownList>
							<% // 必須チェック %>
							<asp:RequiredFieldValidator ID="vdrProductAgent" runat="server" ErrorMessage="ｺﾝﾃﾝﾂ ﾌﾟﾛﾊﾞｲﾀﾞを選択してください。" ControlToValidate="lstProductAgent" ValidationGroup="Execute" Display="Dynamic" >*</asp:RequiredFieldValidator>							
							<ajaxToolkit:ValidatorCalloutExtender ID="vceProductAgent1" runat="Server" TargetControlID="vdrProductAgent" HighlightCssClass="validatorCallout" />
							<asp:RequiredFieldValidator ID="vdrProductAgent2" runat="server" ErrorMessage="ｺﾝﾃﾝﾂ ﾌﾟﾛﾊﾞｲﾀﾞを選択してください。" ControlToValidate="lstProductAgent" ValidationGroup="DownloadImage" Display="Dynamic" >*</asp:RequiredFieldValidator>							
							<ajaxToolkit:ValidatorCalloutExtender ID="vceProductAgent2" runat="Server" TargetControlID="vdrProductAgent2" HighlightCssClass="validatorCallout" />
						</td>
					</tr>
				</table>						
			</asp:Panel>
			<br />
			<table border="0" style="width: 580px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle2">
						取得対象日
					</td>
					<td class="tdDataStyle">
						<asp:TextBox ID="txtBoundaryDate" runat="server">
						</asp:TextBox>
						以降のデータを取得する
						<asp:RequiredFieldValidator ID="vdrBoundaryDate" runat="server" ErrorMessage="取得対象日を入力して下さい。" ControlToValidate="txtBoundaryDate" ValidationGroup="Execute">*</asp:RequiredFieldValidator>
						<asp:RangeValidator ID="vddBoundaryDate" runat="server" ErrorMessage="取得対象日を正しく入力して下さい。" ControlToValidate="txtBoundaryDate" MaximumValue="2099/12/31" MinimumValue="1900/01/01" Type="Date" ValidationGroup="Execute">*</asp:RangeValidator>
					</td>
				</tr>
			</table>
			<br />
			<asp:Button ID="btnExecute" runat="server" Text="実行" OnClick="btnExecute_Click" Width="150px" ValidationGroup="Execute"/>
			<asp:Button ID="btnDownloadImage" runat="server" Text="画像取得" OnClick="btnDownloadImage_Click" Width="150px" ValidationGroup="DownloadImage"/>
			<asp:Label ID="lblErrorMessage" runat="server" ForeColor="red"></asp:Label>			
		</fieldset>
	</asp:Panel>
	<% // *****************************************		%>
	<% //  DataSource									%>
	<% // *****************************************		%>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site">
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsProductAgent" runat="server" SelectMethod="GetList" TypeName="ProductAgent" >
		<SelectParameters>
			<asp:Parameter Type="int32" Name="pOuterAgentFlag" DefaultValue="1"/>
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsProductType"	runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="25" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>			

	<ajaxToolkit:MaskedEditExtender ID="meBoundaryDate" runat="server" TargetControlID="txtBoundaryDate" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay"></ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrBoundaryDate" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vddBoundaryDate" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnExecute" ConfirmText="商品動画情報取得を行います。よろしいですか？" ConfirmOnFormSubmit="true" />

</asp:Content>

