﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ProductGenreCategoryList.aspx.cs" Inherits="Product_ProductGenreCategoryList"
    Title="商品ジャンルカテゴリ設定" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="商品ジャンルカテゴリ設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlMainte">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel runat="server" ID="pnlKey">
                    <fieldset class="fieldset-inner">
                        <table border="0" style="width: 640px" class="tableStyle">
                            <tr>
                                <td class="tdHeaderStyle2">
                                    サイトコード
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                        DataValueField="SITE_CD" Width="240px" Enabled="false">
                                    </asp:DropDownList>
                                </td>
                                <td class="tdHeaderStyle2">
                                    商品種別
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstProductType" runat="server" Width="180px" DataSourceID="dsProductType"
                                        DataTextField="CODE_NM" DataValueField="CODE" Enabled="false">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlDtl">
                    <fieldset class="fieldset-inner">
                        <legend>[商品ジャンル内容]</legend>
                        <table border="0" style="width: 640px" class="tableStyle">
                            <tr>
                                <td class="tdHeaderStyle">
                                    商品ｼﾞｬﾝﾙｶﾃｺﾞﾘｺｰﾄﾞ
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtProductGenreCategoryCd" runat="server" MaxLength="5" Width="200px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vdrProductGenreCategoryCd" runat="server" ErrorMessage="商品ｼﾞｬﾝﾙｶﾃｺﾞﾘｺｰﾄﾞを入力して下さい。"
                                        ControlToValidate="txtProductGenreCategoryCd" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                        TargetControlID="vdrProductGenreCategoryCd" HighlightCssClass="validatorCallout" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle2">
                                    商品ｼﾞｬﾝﾙｶﾃｺﾞﾘ名
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtProductGenreCategoryNm" runat="server" MaxLength="128" Width="200px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vdrProductGenreCategoryNm" runat="server" ErrorMessage="商品ｼﾞｬﾝﾙｶﾃｺﾞﾘ名を入力して下さい。"
                                        ControlToValidate="txtProductGenreCategoryNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                        TargetControlID="vdrProductGenreCategoryNm" HighlightCssClass="validatorCallout" />
                                </td>
                            </tr>
                        </table>
                        <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click"
                            ValidationGroup="Detail" />
                        <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click"
                            ValidationGroup="Key" />
                        <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click"
                            CausesValidation="False" />
                    </fieldset>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <fieldset>
            <legend>[商品ジャンルカテゴリ一覧]</legend>
            <table border="0" style="width: 640px" class="tableStyle">
                <tr>
                    <td class="tdHeaderStyle2">
                        サイトコード
                    </td>
                    <td class="tdDataStyle">
                        <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                            DataValueField="SITE_CD" Width="240px">
                        </asp:DropDownList>
                    </td>
                    <td class="tdHeaderStyle2">
                        商品種別
                    </td>
                    <td class="tdDataStyle">
                        <asp:DropDownList ID="lstSeekProductType" runat="server" Width="180px" DataSourceID="dsProductType"
                            DataTextField="CODE_NM" DataValueField="CODE">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
            <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                CausesValidation="False" />
            <asp:Button runat="server" ID="btnRegist" Text="商品ｼﾞｬﾝﾙｶﾃｺﾞﾘ追加" CssClass="seekbutton"
                CausesValidation="False" OnClick="btnRegist_Click" />
            <br />
            <br />
            <asp:GridView ID="grdProductGenreCategory" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                DataSourceID="dsProductGenreCategory" AllowSorting="True" SkinID="GridView">
                <Columns>
                    <asp:BoundField DataField="PRODUCT_GENRE_CATEGORY_CD" HeaderText="商品ｼﾞｬﾝﾙｶﾃｺﾞﾘｺｰﾄﾞ">
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkGenreCategory" runat="server" Text='<%# Eval("PRODUCT_GENRE_CATEGORY_NM") %>'
                                CommandArgument='<%# Eval("PRODUCT_GENRE_CATEGORY_CD") %>' OnCommand="lnkGenreCategory_Command"
                                CausesValidation="False">
                            </asp:LinkButton>
                        </ItemTemplate>
                        <HeaderTemplate>
                            商品ｼﾞｬﾝﾙｶﾃｺﾞﾘ名
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                </Columns>
                <PagerSettings Mode="NumericFirstLast" />
            </asp:GridView>
            <asp:Panel runat="server" ID="pnlCount">
                <a class="reccount">Record Count
                    <%#GetRecCount() %>
                </a>
                <br />
                <a class="reccount">Current viewing page
                    <%=grdProductGenreCategory.PageIndex + 1%>
                    of
                    <%=grdProductGenreCategory.PageCount%>
                </a>
            </asp:Panel>
        </fieldset>
        <br />
    </div>
    <asp:ObjectDataSource ID="dsProductGenreCategory" runat="server" SelectMethod="GetPageCollection"
        TypeName="ProductGenreCategory" SelectCountMethod="GetPageCount" EnablePaging="True"
        OnSelected="dsProductGenreCategory_Selected" OnSelecting="dsProductGenreCategory_Selecting">
        <SelectParameters>
            <asp:Parameter Name="pSiteCd" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsProductType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="25" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete"
        ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
