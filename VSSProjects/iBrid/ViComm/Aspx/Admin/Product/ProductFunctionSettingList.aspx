﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ProductFunctionSettingList.aspx.cs" Inherits="Product_ProductFunctionSettingList" 
	Title="商品機能設定" ValidateRequest="false"%>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="商品機能設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[サイト内容]</legend>
						<table border="0" style="width: 760px; margin-bottom: 10px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									サイト名
								</td>
								<td class="tdDataStyle">
									<asp:Label ID="lblSiteNm" runat="server"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ｵｰｸｼｮﾝ落札通知<br />
									ﾃﾝﾌﾟﾚｰﾄ番号
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstTpAucWinningBid" runat="server" Width="500px"></asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									ｵｰｸｼｮﾝ高値更新通知<br />
									ﾃﾝﾌﾟﾚｰﾄ番号
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstTpAucUpdateBid" runat="server" Width="500px"></asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									自動延長受付期間（分）
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtAucAutoExtensionAcceptMin" runat="server" ></asp:TextBox>
									<asp:RangeValidator ID="vdrAucAutoExtensionAcceptMin" runat="server" ErrorMessage="自動延長受付期間を正しく入力してください" ControlToValidate="txtAucAutoExtensionAcceptMin" MaximumValue="999" MinimumValue="0"
										Type="Integer" ValidationGroup="Update" Display="Dynamic">*</asp:RangeValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrAucAutoExtensionAcceptMin" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtAucAutoExtensionAcceptMin" />
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									自動延長時間（分）
								</td>
								<td class="tdDataStyle">
								<asp:TextBox ID="txtAucAutoExtensionMin" runat="server" ></asp:TextBox>
									<asp:RangeValidator ID="vdrAucAutoExtensionMin" runat="server" ErrorMessage="自動延長時間を正しく入力してください" ControlToValidate="txtAucAutoExtensionMin" MaximumValue="999" MinimumValue="0"
										Type="Integer" ValidationGroup="Update" Display="Dynamic">*</asp:RangeValidator>
									<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrAucAutoExtensionMin" HighlightCssClass="validatorCallout" />
									<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtAucAutoExtensionMin" />
								</td>
							</tr>
						</table>
						<asp:Button ID="btnUpdate" runat="server" CausesValidation="True" ValidationGroup="Update" CssClass="seekbutton" OnClick="btnUpdate_Click" Text="更新" OnClientClick="return confirm('更新を行いますか？');" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<asp:Panel ID="pnlList" runat="server" ScrollBars="auto">
			<fieldset>
				<legend>[サイト一覧]</legend>
				<asp:Panel ID="pnlGrid" runat="server">
					<asp:GridView ID="grdProductFunctionSetting" runat="server" DataSourceID="dsProductFunctionSetting" AutoGenerateColumns="False" AllowPaging="true" AllowSorting="true"
						SkinID="GridViewColor">
						<Columns>
							<asp:TemplateField HeaderText="サイトコード">
								<ItemTemplate>
									<asp:LinkButton ID="lnkSiteCd" runat="server" Text='<%# Eval("SITE_CD") %>' CommandArgument='<%# Eval("SITE_CD") %>' OnCommand="lnkSiteCd_Command" CausesValidation="False">
									</asp:LinkButton>
									<asp:Label ID="lblConnect" runat="server" Text='<%# Bind("SITE_NM") %>'></asp:Label>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Left" />
							</asp:TemplateField>
							<asp:BoundField DataField="UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
			</asp:Panel>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br />
				<a class="reccount">Current viewing page
					<%=grdProductFunctionSetting.PageIndex + 1%>
					of
					<%=grdProductFunctionSetting.PageCount%>
				</a>
			</asp:Panel>
			</fieldset>
			<br />
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsProductFunctionSetting" runat="server" SelectMethod="GetPageCollection" SelectCountMethod="GetPageCount" EnablePaging="true" OnSelected="dsProductFunctionSetting_Selected"
		TypeName="ProductFunctionSetting"></asp:ObjectDataSource>
</asp:Content>