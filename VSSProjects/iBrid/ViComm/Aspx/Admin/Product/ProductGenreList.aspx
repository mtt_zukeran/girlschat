﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ProductGenreList.aspx.cs" Inherits="Product_ProductGenreList" Title="商品ジャンル設定"
    ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="商品ジャンル設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlMainte">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel runat="server" ID="pnlKey">
                    <fieldset class="fieldset-inner">
                        <table border="0" style="width: 640px" class="tableStyle">
                            <tr>
                                <td class="tdHeaderStyle2">
                                    サイトコード
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                        DataValueField="SITE_CD" Width="240px" Enabled="false">
                                    </asp:DropDownList>
                                </td>
                                <td class="tdHeaderStyle2">
                                    商品種別
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstProductType" runat="server" Width="180px" DataSourceID="dsProductType"
                                        DataTextField="CODE_NM" DataValueField="CODE" Enabled="false">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlDtl">
                    <fieldset class="fieldset-inner">
                        <legend>[商品ジャンル内容]</legend>
                        <table border="0" style="width: 640px" class="tableStyle">
                            <tr>
                                <td class="tdHeaderStyle">
                                    商品ｼﾞｬﾝﾙｶﾃｺﾞﾘ
                                </td>
                                <td class="tdDataStyle">
                                    <asp:DropDownList ID="lstProductGenreCagetory" runat="server" DataSourceID="dsProductGenreCategory"
                                        DataTextField="PRODUCT_GENRE_CATEGORY_NM" DataValueField="PRODUCT_GENRE_CATEGORY_CD"
                                        Width="240px" OnDataBound="lstProductGenreCategory_DataBound">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="vdrProductGenreCagetory" runat="server" ErrorMessage="商品ｼﾞｬﾝﾙを選択してください。"
                                        ControlToValidate="lstProductGenreCagetory" ValidationGroup="Detail" Display="Dynamic">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server"
                                        TargetControlID="vdrProductGenreCagetory" HighlightCssClass="validatorCallout" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    商品ｼﾞｬﾝﾙｺｰﾄﾞ
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtProductGenreCd" runat="server" MaxLength="5" Width="200px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vdrProductGenreCd" runat="server" ErrorMessage="商品ｼﾞｬﾝﾙｺｰﾄﾞを入力して下さい。"
                                        ControlToValidate="txtProductGenreCd" ValidationGroup="Detail" Display="Dynamic">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                        TargetControlID="vdrProductGenreCd" HighlightCssClass="validatorCallout" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    商品ｼﾞｬﾝﾙ名
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtProductGenreNm" runat="server" MaxLength="256" Width="200px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vdrProductGenreNm" runat="server" ErrorMessage="商品ｼﾞｬﾝﾙ名を入力して下さい。"
                                        ControlToValidate="txtProductGenreNm" ValidationGroup="Detail" Display="Dynamic">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                        TargetControlID="vdrProductGenreNm" HighlightCssClass="validatorCallout" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    優先順位
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtPriority" runat="server" MaxLength="3" Width="60px"></asp:TextBox>&nbsp;
                                    <asp:RequiredFieldValidator ID="vdrPriority" runat="server" ErrorMessage="優先順位を入力して下さい。"
                                        ControlToValidate="txtPriority" ValidationGroup="Detail" Display="dynamic">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
                                        TargetControlID="vdrPriority" HighlightCssClass="validatorCallout" />
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                        Enabled="true" FilterType="Numbers" TargetControlID="txtPriority" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle2">
                                    公開フラグ
                                </td>
                                <td class="tdDataStyle">
                                    <asp:RadioButtonList ID="rdoPublishFlag" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="公開" Value="1" Selected="true" />
                                        <asp:ListItem Text="非公開" Value="0" />
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>
                        <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click"
                            ValidationGroup="Detail" />
                        <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click"
                            ValidationGroup="Key" />
                        <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click"
                            CausesValidation="False" />
                    </fieldset>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <fieldset>
            <legend>[商品ジャンル一覧]</legend>
            <table border="0" style="width: 640px" class="tableStyle">
                <tr>
                    <td class="tdHeaderStyle2">
                        サイトコード
                    </td>
                    <td class="tdDataStyle">
                        <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                            DataValueField="SITE_CD" Width="240px">
                        </asp:DropDownList>
                    </td>
                    <td class="tdHeaderStyle2">
                        商品種別
                    </td>
                    <td class="tdDataStyle">
                        <asp:DropDownList ID="lstSeekProductType" runat="server" Width="180px" DataSourceID="dsProductType"
                            DataTextField="CODE_NM" DataValueField="CODE">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
            <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                CausesValidation="False" />
            <asp:Button runat="server" ID="btnRegist" Text="商品ｼﾞｬﾝﾙ追加" CssClass="seekbutton"
                CausesValidation="False" OnClick="btnRegist_Click" />
            <br />
            <br />
            <asp:GridView ID="grdProductGenre" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                DataSourceID="dsProductGenre" AllowSorting="True" SkinID="GridView" OnDataBound="grdProductGenre_DataBound">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            商品ｼﾞｬﾝﾙｶﾃｺﾞﾘｺｰﾄﾞ
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblProductGenreCategoryCd" runat="server" Text='<%# Eval("PRODUCT_GENRE_CATEGORY_CD")%>'></asp:Label>
                            <br />
                            <asp:Label ID="lblProductGenreCategoryNm" runat="server" Text='<%# string.Format("({0})",Eval("PRODUCT_GENRE_CATEGORY_NM"))%>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="PRODUCT_GENRE_CD" HeaderText="商品ｼﾞｬﾝﾙｺｰﾄﾞ">
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            商品ｼﾞｬﾝﾙ名
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkProductGenre" runat="server" NavigateUrl='<%# string.Format("~/Product/ProductGenreList.aspx?site_cd={0}&product_type={1}&product_genre_category_cd={2}&product_genre_cd={3}",Eval("SITE_CD"),Eval("PRODUCT_TYPE"),Eval("PRODUCT_GENRE_CATEGORY_CD"),Eval("PRODUCT_GENRE_CD")) %>'><%#Eval("PRODUCT_GENRE_NM")%></asp:HyperLink>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="PRIORITY" HeaderText="優先順位">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            公開フラグ
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPublishFlag" runat="server" Text='<%# GetPublishFlagMark(Eval("PUBLISH_FLAG")) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerSettings Mode="NumericFirstLast" />
            </asp:GridView>
            <asp:Panel runat="server" ID="pnlCount">
                <a class="reccount">Record Count
                    <%#GetRecCount() %>
                </a>
                <br />
                <a class="reccount">Current viewing page
                    <%=grdProductGenre.PageIndex + 1%>
                    of
                    <%=grdProductGenre.PageCount%>
                </a>
            </asp:Panel>
        </fieldset>
        <br />
    </div>
    <asp:ObjectDataSource ID="dsProductGenre" runat="server" SelectMethod="GetPageCollection"
        TypeName="ProductGenre" SelectCountMethod="GetPageCount" EnablePaging="True"
        OnSelected="dsProductGenre_Selected" OnSelecting="dsProductGenre_Selecting">
        <SelectParameters>
            <asp:Parameter Name="pSiteCd" Type="String" />
            <asp:Parameter Name="pProductType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsProductType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="25" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsProductGenreCategory" runat="server" SelectMethod="GetList"
        TypeName="ProductGenreCategory">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSeekSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:ControlParameter ControlID="lstSeekProductType" Name="pProductType" PropertyName="SelectedValue"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="更新を行いますか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete"
        ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
