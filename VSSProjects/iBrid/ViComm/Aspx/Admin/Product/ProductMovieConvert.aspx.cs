﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

using ViComm;
using iBridCommLib;

public partial class Product_ProductMovieConvert : System.Web.UI.Page {

	#region □■□ プロパティ □■□ ==================================================================================
	protected string SiteCd{
		get { return iBridUtil.GetStringValue(this.ViewState["SiteCd"]); }
		private set { this.ViewState["SiteCd"] = value; }
	}
	protected string ProductAgenetCd{
		get { return iBridUtil.GetStringValue(this.ViewState["ProductAgenetCd"]); }
		private set { this.ViewState["ProductAgenetCd"] = value; }
	}
	protected string ProductId {
		get { return iBridUtil.GetStringValue(this.ViewState["ProductId"]); }
		private set { this.ViewState["ProductId"] = value; }
	}
	protected string ProductSeq {
		get { return iBridUtil.GetStringValue(this.ViewState["ProductSeq"]); }
		private set { this.ViewState["ProductSeq"] = value; }
	}
	protected string ProductMovieType {
		get { return iBridUtil.GetStringValue(this.ViewState["ProductMovieType"]); }
		private set { this.ViewState["ProductMovieType"] = value; }
	}	
	#endregion ========================================================================================================
	
	protected void Page_Load(object sender, EventArgs e) {
		if(!this.IsPostBack){

			this.ProductId = iBridUtil.GetStringValue(this.Request.QueryString["product_id"]);
			this.ProductMovieType = iBridUtil.GetStringValue(this.Request.QueryString["product_movie_type"]);

			using (CodeDtl oCodeDtl = new CodeDtl()) 
			using (Product oProduct = new Product()) {
				using (DataSet oProductDataSet = oProduct.GetOne(this.ProductId)) {
					DataRow oProductDataRow = oProductDataSet.Tables[0].Rows[0];
					this.SiteCd = iBridUtil.GetStringValue(oProductDataRow["SITE_CD"]);
					this.ProductAgenetCd = iBridUtil.GetStringValue(oProductDataRow["PRODUCT_AGENT_CD"]);
					this.ProductSeq = iBridUtil.GetStringValue(oProductDataRow["PRODUCT_SEQ"]);
					
					this.lblSiteCd.Text = this.SiteCd;
					this.lblProductId.Text = this.ProductId;
					this.lblLinkId.Text = iBridUtil.GetStringValue(oProductDataRow["LINK_ID"]);
					if(this.lblLinkId.Text.Equals(string.Empty)){
						this.lblLinkId.Text = "(なし)";
					}
					this.lblProductNm.Text = iBridUtil.GetStringValue(oProductDataRow["PRODUCT_NM"]);
					
					if(oCodeDtl.GetOne(ViCommConst.CODE_TYPE_PRODUCT_MOVIE_TYPE, this.ProductMovieType)){
						this.lblProductMovieType.Text = oCodeDtl.codeNm;
					}
                    if (oCodeDtl.GetOne(ViCommConst.CODE_TYPE_PRODUCT_TYPE, iBridUtil.GetStringValue(oProductDataRow["PRODUCT_TYPE"]))) {
                        this.lblProductType.Text = oCodeDtl.codeNm;
                    }
					this.grdTargetFile.DataBind();
				}
			}			
		}

	}	
	
	protected void btnRefresh_Click(object sender, EventArgs e) {
		string sTmpDataSourceId = this.grdTargetFile.DataSourceID;
		this.grdTargetFile.DataSourceID = null;
		this.grdTargetFile.DataBind();
		this.grdTargetFile.DataSourceID = sTmpDataSourceId;
		this.grdTargetFile.DataBind();
	}

	protected void btnCancel_Click(object sender, EventArgs e) {
		UrlBuilder oUrlBuilder = new UrlBuilder("ProductView.aspx");
		oUrlBuilder.Parameters.Add("product_id",this.ProductId);
		Response.Redirect(oUrlBuilder.ToString());
	}
	
	protected void btnConvert_Command(object sender, CommandEventArgs e) {
		string sFilePath = e.CommandArgument as string;

		string sAspectType = ViCommConst.MovieAspectType.NONE;
		if (this.rdoAspectNormal.Checked) {
			sAspectType = ViCommConst.MovieAspectType.NORMAL;
		} else if (this.rdoAspectWide.Checked) {
			sAspectType = ViCommConst.MovieAspectType.WIDE;
		}

		using (Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME, ViCommConst.FILE_UPLOAD_PASSWORD)) {
		
			if (!string.IsNullOrEmpty(sFilePath) && File.Exists(sFilePath)) {
				FileInfo oOriginalFile = new FileInfo(sFilePath);

				decimal dMovieSeq = this.CreateMovieSeq();
				string sUploadFilePath = MovieConvertHelper.GetUploadFilePath(dMovieSeq);
				using (IDisposable oImpersonater = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME, string.Empty, ViCommConst.FILE_UPLOAD_PASSWORD)) {
					oOriginalFile.CopyTo(sUploadFilePath);
				}

                // ========================
                // ★ 登録済チェック
                // ========================
                using (ProductMovie oProductMovie = new ProductMovie()) {
                    if (oProductMovie.GetCount(this.SiteCd, this.ProductAgenetCd, this.ProductSeq, this.ProductMovieType) > 0) {
                        this.lblAlreadyExists.Visible = true;
                        return;
                    }
                }

				// ========================
				// ★ 変換処理
				// ========================
				localhost.ConvertMobileMovieResult[] oConvertResultArr 
					= MovieConvertHelper.ConvertProductMovie(this.SiteCd, this.ProductId, dMovieSeq, sAspectType);


				// ========================
				// ★ 登録済チェック
				// ========================
				using (ProductMovie oProductMovie = new ProductMovie()) {
					if (oProductMovie.GetCount(this.SiteCd,this.ProductAgenetCd,this.ProductSeq,this.ProductMovieType) > 0) {
						this.lblAlreadyExists.Visible = true;
						return;
					}
				}

				// ========================
				// ★ 変換結果⇒商品動画詳細　変換
				// ========================
				List<string> oSizeTypeList = new List<string>();
				List<string> oFileFormatList = new List<string>();
				List<string> oTargeCarrierList = new List<string>();
				List<string> oProductMovieSubSeqList = new List<string>();
				List<string> oFileNmList = new List<string>();
				List<string> oFileSizeList = new List<string>();
				List<string> oDownloadUrlList = new List<string>();
				List<string> oLinkIdList = new List<string>();
				
				DataTable oMovieFileTypeDataTable = null;
				using(MovieFileType oMovieFileType = new MovieFileType()){
					oMovieFileTypeDataTable = oMovieFileType.GetList().Tables[0];
				}
				
				foreach(localhost.ConvertMobileMovieResult oConvertResult in oConvertResultArr){
					DataRow[] oMovieFileTypeRowArr	= oMovieFileTypeDataTable.Select(string.Format("MOVIE_FILE_TYPE='{0}'",oConvertResult.MobileFileType));

					if (oMovieFileTypeRowArr == null || oMovieFileTypeRowArr.Length < 1) {
						continue;
					}
					
					int iFileCount = oConvertResult.FileNum;
					string sSizeType = iBridUtil.GetStringValue(oMovieFileTypeRowArr[0]["SIZE_TYPE"]);
					string sFileType = iBridUtil.GetStringValue(oMovieFileTypeRowArr[0]["FILE_FORMAT"]);
					string sTargetCarrier = iBridUtil.GetStringValue(oMovieFileTypeRowArr[0]["TARGET_CARRIER"]);
					string sTargetUseType = iBridUtil.GetStringValue(oMovieFileTypeRowArr[0]["TARGET_USE_TYPE"]);

					for(int i=0;i<iFileCount;i++){
						string sFileNm = string.Format("{0}_{1}_{2}_{3}_{4}.{5}",
							iBridUtil.addZero(dMovieSeq.ToString(), ViCommConst.OBJECT_NM_LENGTH),
							sSizeType.Substring(0,4),
							sTargetCarrier,
							sTargetUseType,
							((int)(i+1)).ToString().PadLeft(3,'0'),
							sFileType);
						
					
						oSizeTypeList.Add(sSizeType + sTargetUseType);
						oFileFormatList.Add(sFileType);
						oTargeCarrierList.Add(sTargetCarrier);
						oProductMovieSubSeqList.Add(i.ToString());
						oFileNmList.Add(sFileNm);
						oFileSizeList.Add(null);
						oDownloadUrlList.Add(null);
						oLinkIdList.Add(null);
					}
				}
				

				// ========================
				// ★ 商品動画情報登録
				// ========================
				using (DbSession oDbSession = new DbSession()) {
					oDbSession.PrepareProcedure("PRODUCT_MOVIE_MAINTE");
					oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.SiteCd);
					oDbSession.ProcedureInParm("pPRODUCT_AGENT_CD", DbSession.DbType.VARCHAR2, this.ProductAgenetCd);
					oDbSession.ProcedureInParm("pPRODUCT_SEQ", DbSession.DbType.NUMBER, this.ProductSeq);
					oDbSession.ProcedureInParm("pOBJ_SEQ", DbSession.DbType.NUMBER, dMovieSeq);
					oDbSession.ProcedureInParm("pPRODUCT_MOVIE_TYPE", DbSession.DbType.VARCHAR2, this.ProductMovieType);
					oDbSession.ProcedureInParm("pPLAY_TIME", DbSession.DbType.VARCHAR2, null);
					oDbSession.ProcedureInParm("pUNEDITABLE_FLAG", DbSession.DbType.NUMBER, ViCommConst.FLAG_OFF);
					oDbSession.ProcedureInParm("pOUTER_MOVIE_FLAG", DbSession.DbType.NUMBER, ViCommConst.FLAG_OFF);
					oDbSession.ProcedureInParm("pLINK_ID", DbSession.DbType.VARCHAR2, null);
					oDbSession.ProcedureInParm("pREVISION_NO", DbSession.DbType.NUMBER, null);
					oDbSession.ProcedureInParm("pWITH_DTL_MAINTE", DbSession.DbType.NUMBER, ViCommConst.FLAG_ON);
					oDbSession.ProcedureInArrayParm("pCP_SIZE_TYPE_ARR", DbSession.DbType.VARCHAR2, oSizeTypeList.ToArray());
					oDbSession.ProcedureInArrayParm("pCP_FILE_FORMAT_ARR", DbSession.DbType.VARCHAR2, oFileFormatList.ToArray());
					oDbSession.ProcedureInArrayParm("pCP_TARGET_CARRIER_ARR", DbSession.DbType.VARCHAR2, oTargeCarrierList.ToArray());
					oDbSession.ProcedureInArrayParm("pPRODUCT_MOVIE_SUBSEQ_ARR", DbSession.DbType.NUMBER, oProductMovieSubSeqList.ToArray());
					oDbSession.ProcedureInArrayParm("pFILE_NM_ARR", DbSession.DbType.VARCHAR2, oFileNmList.ToArray());
					oDbSession.ProcedureInArrayParm("pFILE_SIZE_ARR", DbSession.DbType.NUMBER, oFileSizeList.ToArray());
					oDbSession.ProcedureInArrayParm("pDOWNLOAD_URL_ARR", DbSession.DbType.VARCHAR2, oDownloadUrlList.ToArray());
					oDbSession.ProcedureInArrayParm("pLINK_ID_ARR", DbSession.DbType.VARCHAR2, oLinkIdList.ToArray());
					oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
					oDbSession.cmd.BindByName = true;
					oDbSession.ExecuteProcedure();
				}

				// 成功した場合はオリジナルを削除する
				using(Impersonator.Impersonate(ViCommConst.FILE_UPLOAD_USERNAME,ViCommConst.FILE_UPLOAD_PASSWORD)){
					oOriginalFile.Delete();
				}
			}
		}

		UrlBuilder oUrlBuilder = new UrlBuilder("ProductView.aspx");
		oUrlBuilder.Parameters.Add("product_id", this.ProductId);
		Response.Redirect(oUrlBuilder.ToString());
	}
	
	protected decimal CreateMovieSeq() {
		using (CastMovie objMovie = new CastMovie()) {
			// MOVIE_SEQを採番
			return objMovie.GetMovieNo();
		}
	}
	/// <summary>
	/// BをKBに変換する
	/// </summary>
	/// <param name="pByteLength"></param>
	/// <returns></returns>
	protected long Convert2KB(long pByteLength) {
		long lResult = 0;
		long lOvermeasure;
		lResult = Math.DivRem(pByteLength, 1024, out lOvermeasure);
		if (lOvermeasure > 0) lResult += 1;
		return lResult;
	}
}
