﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ProductMainte.aspx.cs" Inherits="Product_ProductMainte" Title="商品設定" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="商品設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlMainte">
            <fieldset class="fieldset">
                <asp:Panel runat="server" ID="pnlProductHeader">
                    <table border="0" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                サイトコード
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" Width="240px" DataSourceID="dsSite"
                                    DataTextField="SITE_NM" DataValueField="SITE_CD">
                                </asp:DropDownList>
                            </td>
                            <td class="tdHeaderStyle2">
                                ｺﾝﾃﾝﾂ ﾌﾟﾛﾊﾞｲﾀﾞ
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstProductAgent" runat="server" Width="240px" DataSourceID="dsProductAgent"
                                    DataTextField="PRODUCT_AGENT_NM" DataValueField="PRODUCT_AGENT_CD">
                                </asp:DropDownList>
                                <% // 必須チェック %>
                                <asp:RequiredFieldValidator ID="vdrProductAgent" runat="server" ErrorMessage="ｺﾝﾃﾝﾂ ﾌﾟﾛﾊﾞｲﾀﾞを選択してください。"
                                    ControlToValidate="lstProductAgent" ValidationGroup="Create" Display="Dynamic">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="vceProductAgent1" runat="Server" TargetControlID="vdrProductAgent"
                                    HighlightCssClass="validatorCallout" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle2">
                                商品種別
                            </td>
                            <td class="tdDataStyle" colspan="3">
                                <asp:DropDownList ID="lstProductType" runat="server" Width="180px" DataSourceID="dsProductType"
                                    DataTextField="CODE_NM" DataValueField="CODE">
                                    <asp:ListItem Text="" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <% // 必須チェック %>
                                <asp:RequiredFieldValidator ID="vdrProductType" runat="server" ErrorMessage="商品種別を選択してください。"
                                    ControlToValidate="lstProductType" ValidationGroup="Create" Display="Dynamic">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="vceProductType1" runat="Server" TargetControlID="vdrProductType"
                                    HighlightCssClass="validatorCallout" />
                            </td>
                        </tr>
                    </table>
                    <asp:Panel ID="pnlCreate" runat="server" Visible="false">
                        <asp:Button runat="server" ID="btnCreate" Text="作成" CssClass="seekbutton" ValidationGroup="Create"
                            OnClick="btnCreate_Click" />
                        <asp:Button runat="server" ID="btnCreateCancel" Text="キャンセル" CssClass="seekbutton"
                            CausesValidation="False" OnClick="btnCreateCancel_Click" />
                        <br />
                        <br />
                    </asp:Panel>
                </asp:Panel>
                <% // *****************************************		%>
                <% //  商品情報										%>
                <% // *****************************************		%>
                <asp:Panel runat="server" ID="pnlProductBasicInfo">
                    <fieldset class="fieldset">
                        <legend>[商品情報]</legend>
                        <table border="0" style="width: 640px" class="tableStyle">
                            <tr>
                                <td class="tdHeaderStyle">
                                    商品ID(商品SEQ)
                                </td>
                                <td class="tdDataStyle">
                                    <asp:Label ID="lblProductId" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    商品名
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtProductNm" runat="server" MaxLength="64" Width="200px"></asp:TextBox>
                                    <% // 必須チェック %>
                                    <asp:RequiredFieldValidator ID="vdrProductNm" runat="server" ErrorMessage="商品名を入力してください。"
                                        ControlToValidate="txtProductNm" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="vdeProductNm" runat="Server" TargetControlID="vdrProductNm"
                                        HighlightCssClass="validatorCallout" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    商品番号
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtPartNumber" runat="server" MaxLength="64" Width="200px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    商品シリーズ
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtProductSeriesSummary" runat="server" MaxLength="64" Width="200px"></asp:TextBox>
                                </td>
                            </tr>
                            <asp:Panel ID="pnlProductInfoDetail" runat="server" Visible="true">
                                <tr>
                                    <td class="tdHeaderStyle">
                                        製造元
                                    </td>
                                    <td class="tdDataStyle">
                                        <asp:TextBox ID="txtProductMakerSummary" runat="server" MaxLength="64" Width="200px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdHeaderStyle">
                                        発売日
                                    </td>
                                    <td class="tdDataStyle">
                                        <asp:TextBox ID="txtReleaseDate" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
                                        <ajaxToolkit:MaskedEditExtender ID="mskReleaseDate" runat="server" MaskType="Date"
                                            Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true" TargetControlID="txtReleaseDate">
                                        </ajaxToolkit:MaskedEditExtender>
                                        <% //範囲チェック %>
                                        <asp:CustomValidator ID="vdcReleaseDate" runat="server" ErrorMessage="入力書式が正しくありません。"
                                            ControlToValidate="txtReleaseDate" OnServerValidate="ValidateDate" ValidationGroup="Update"
                                            Display="Dynamic">*</asp:CustomValidator>
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server"
                                            HighlightCssClass="validatorCallout" TargetControlID="vdcReleaseDate">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdHeaderStyle">
                                        グロス価格
                                    </td>
                                    <td class="tdDataStyle">
                                        <asp:TextBox ID="txtGrossPrice" runat="server" MaxLength="5" Width="50px" Style="text-align: right;"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdHeaderStyle">
                                        ロイヤリティ率
                                    </td>
                                    <td class="tdDataStyle">
                                        <asp:TextBox ID="txtRoyaltyRate" runat="server" MaxLength="3" Width="50px" Style="text-align: right;"></asp:TextBox>
                                        <% //範囲チェック %>
                                        <asp:RangeValidator ID="vdrRoyaltyRate" runat="server" ErrorMessage="0～100の範囲で入力して下さい。"
                                            MinimumValue="0" MaximumValue="100" Type="integer" ControlToValidate="txtRoyaltyRate"
                                            ValidationGroup="Update" Display="Dynamic">*</asp:RangeValidator>
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server"
                                            HighlightCssClass="validatorCallout" TargetControlID="vdrRoyaltyRate">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdHeaderStyle">
                                        課金ポイント
                                    </td>
                                    <td class="tdDataStyle">
                                        <asp:TextBox ID="txtChargePoint" runat="server" MaxLength="5" Width="50px" Style="text-align: right;"></asp:TextBox>
                                        <% // 必須チェック %>
                                        <asp:RequiredFieldValidator ID="vdrChargePoint" runat="server" ErrorMessage="課金ポイントを入力して下さい。"
                                            ControlToValidate="txtChargePoint" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                        <ajaxToolkit:ValidatorCalloutExtender ID="vcerChargePoint1" runat="Server" TargetControlID="vdrChargePoint"
                                            HighlightCssClass="validatorCallout" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdHeaderStyle">
                                        特別課金ポイント
                                    </td>
                                    <td class="tdDataStyle">
                                        <asp:TextBox ID="txtSpecialChargePoint" runat="server" MaxLength="5" Width="50px"
                                            Style="text-align: right;"></asp:TextBox>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <tr>
                                <td class="tdHeaderStyle">
                                    公開開始日時
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtPublishStartDate" runat="server" MaxLength="16" Width="120px"></asp:TextBox>
                                    <ajaxToolkit:MaskedEditExtender ID="mskPublishStartDate" runat="server" MaskType="DateTime"
                                        Mask="9999/99/99 99:99:99" UserDateFormat="YearMonthDay" UserTimeFormat="TwentyFourHour"
                                        ClearMaskOnLostFocus="true" TargetControlID="txtPublishStartDate">
                                    </ajaxToolkit:MaskedEditExtender>
                                    <% //範囲チェック %>
                                    <asp:CustomValidator ID="vdcPublishStartDate" runat="server" ControlToValidate="txtPublishStartDate"
                                        Display="Dynamic" ErrorMessage="入力書式が正しくありません。" OnServerValidate="ValidateDate"
                                        ValidationGroup="Update">*</asp:CustomValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdcPublishStartDate">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    公開終了日時
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtPublishEndDate" runat="server" MaxLength="16" Width="120px"></asp:TextBox>
                                    <ajaxToolkit:MaskedEditExtender ID="mskPublishEndDate" runat="server" MaskType="DateTime"
                                        Mask="9999/99/99 99:99:99" UserDateFormat="YearMonthDay" UserTimeFormat="TwentyFourHour"
                                        ClearMaskOnLostFocus="true" TargetControlID="txtPublishEndDate">
                                    </ajaxToolkit:MaskedEditExtender>
                                    <% //範囲チェック %>
                                    <asp:CustomValidator ID="vdcPublishEndDate" runat="server" ControlToValidate="txtPublishEndDate"
                                        Display="Dynamic" ErrorMessage="入力書式が正しくありません。" OnServerValidate="ValidateDate"
                                        ValidationGroup="Update">*</asp:CustomValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdcPublishEndDate">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:CustomValidator ID="vdcPublishStartEnd" runat="server" ControlToValidate="txtPublishEndDate"
                                        Display="Dynamic" ErrorMessage="公開期間の大小関係が正しくありません。" OnServerValidate="vdcPublishStartEnd_ServerValidate"
                                        ValidationGroup="Update">*</asp:CustomValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdcPublishStartEnd">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    新着表示/非表示
                                </td>
                                <td class="tdDataStyle">
                                    <asp:RadioButtonList ID="rdoNotNewFlag" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="表示" Value="0" Selected="true"></asp:ListItem>
                                        <asp:ListItem Text="非表示" Value="1"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    出演者
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtCastSummary" runat="server" MaxLength="64" Width="200px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    関連出演者
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtLoginId" runat="server" MaxLength="8" Width="80px"></asp:TextBox>
                                    <asp:TextBox ID="txtUserCharNo" runat="server" MaxLength="2" Width="20px" Visible="false"></asp:TextBox>
                                    <asp:Button ID="btnConfirmLoginId" runat="server" Text="確認する" OnClick="btnConfirmLoginId_Click"
                                        ValidationGroup="ConfirmLoginId" />
                                    <asp:Label ID="lblHandlNm" runat="server"></asp:Label>
                                    <% //範囲チェック %>
                                    <asp:CustomValidator ID="vdcCast1" runat="server" ControlToValidate="txtLoginId"
                                        ErrorMessage="該当するｷｬｽﾄｷｬﾗｸﾀｰが存在しません。" OnServerValidate="vdcCast_ServerValidate"
                                        ValidationGroup="Update" Display="Dynamic">*</asp:CustomValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdcCast1">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:CustomValidator ID="vdcCast2" runat="server" ControlToValidate="txtLoginId"
                                        ErrorMessage="該当するｷｬｽﾄｷｬﾗｸﾀｰが存在しません。" OnServerValidate="vdcCast_ServerValidate"
                                        ValidationGroup="ConfirmLoginId" Display="Dynamic">*</asp:CustomValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdcCast2">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    商品説明
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtProcuctDiscription" runat="server" MaxLength="256" Width="300px"
                                        Height="60px" TextMode="MultiLine"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    備考
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtRemarks" runat="server" MaxLength="256" Width="300px" Height="60px"
                                        TextMode="MultiLine"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle2">
                                    公開/非公開
                                </td>
                                <td class="tdDataStyle">
                                    <asp:RadioButtonList ID="rdoPublish" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="未設定" Value="-1" Selected="true"></asp:ListItem>
                                        <asp:ListItem Text="公開" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="非公開" Value="0"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>
                        <asp:HiddenField ID="hdnUneditableFlag" runat="server"></asp:HiddenField>
                        <asp:HiddenField ID="hdnProductSeq" runat="server"></asp:HiddenField>
                        <asp:HiddenField ID="hdnLinkId" runat="server"></asp:HiddenField>
                        <asp:HiddenField ID="hdnLinkDate" runat="server"></asp:HiddenField>
                        <asp:HiddenField ID="hdnRevisionNo" runat="server"></asp:HiddenField>
                        <asp:HiddenField ID="hdnIsInitial" runat="server"></asp:HiddenField>
                    </fieldset>
                </asp:Panel>
                <% // *****************************************		%>
                <% //  オークション情報								%>
                <% // *****************************************		%>
                <asp:Panel runat="server" ID="pnlAuctionInfo">
                    <fieldset class="fieldset">
                        <legend>[オークション情報]</legend>
                        <table border="0" style="width: 640px" class="tableStyle">
                            <tr>
                                <td class="tdHeaderStyle">
                                    オークション状態
                                </td>
                                <td class="tdDataStyle">
                                    <asp:Label ID="lblAuctionStatus" runat="server" Text="-"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    ｵｰｸｼｮﾝ開始日時
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtAuctionStartDate" runat="server" MaxLength="16" Width="120px"></asp:TextBox>
                                    <ajaxToolkit:MaskedEditExtender ID="mskAuctionStatus" runat="server" MaskType="DateTime"
                                        Mask="9999/99/99 99:99:99" UserDateFormat="YearMonthDay" UserTimeFormat="TwentyFourHour"
                                        ClearMaskOnLostFocus="true" TargetControlID="txtAuctionStartDate">
                                    </ajaxToolkit:MaskedEditExtender>
                                    <% // 必須チェック %>
                                    <asp:RequiredFieldValidator ID="vdrAuctionStartDate" runat="server" ErrorMessage="ｵｰｸｼｮﾝ開始日時を入力してください。"
                                        ControlToValidate="txtAuctionStartDate" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="vceAuctionStartDate" runat="Server" TargetControlID="vdrAuctionStartDate"
                                        HighlightCssClass="validatorCallout" />
                                    <% // 範囲チェック %>
                                    <asp:CustomValidator ID="vdcAuctionStartDate" runat="server" ErrorMessage="" ControlToValidate="txtAuctionStartDate"
                                        ValidationGroup="Update" OnServerValidate="vdcAuctionStartDate_SeverValidate">*</asp:CustomValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdcAuctionStartDate">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    ﾌﾞﾗｲﾝﾄﾞ終了日時
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtBlindEndDate" runat="server" MaxLength="16" Width="120px"></asp:TextBox>
                                    <ajaxToolkit:MaskedEditExtender ID="mskBlindEndDate" runat="server" MaskType="DateTime"
                                        Mask="9999/99/99 99:99:99" UserDateFormat="YearMonthDay" UserTimeFormat="TwentyFourHour"
                                        ClearMaskOnLostFocus="true" TargetControlID="txtBlindEndDate">
                                    </ajaxToolkit:MaskedEditExtender>
                                    <% // 必須チェック %>
                                    <asp:RequiredFieldValidator ID="vdrBlindEndDate" runat="server" ControlToValidate="txtBlindEndDate"
                                        ErrorMessage="ﾌﾞﾗｲﾝﾄﾞ終了日時を入力してください。" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdrBlindEndDate">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                    <% // 範囲チェック %>
                                    <asp:CustomValidator ID="vdcBlindEndDate" runat="server" ControlToValidate="txtBlindEndDate"
                                        ErrorMessage="" OnServerValidate="vdcBlindEndDate_SeverValidate" ValidationGroup="Update">*</asp:CustomValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdcBlindEndDate">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    ｵｰｸｼｮﾝ終了日時
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtAuctionEndDate" runat="server" MaxLength="16" Width="120px"></asp:TextBox>
                                    <ajaxToolkit:MaskedEditExtender ID="mskAuctionEndDate" runat="server" MaskType="DateTime"
                                        Mask="9999/99/99 99:99:99" UserDateFormat="YearMonthDay" UserTimeFormat="TwentyFourHour"
                                        ClearMaskOnLostFocus="true" TargetControlID="txtAuctionEndDate">
                                    </ajaxToolkit:MaskedEditExtender>
                                    <% // 必須チェック %>
                                    <asp:RequiredFieldValidator ID="vdrAuctionEndDate" runat="server" ControlToValidate="txtAuctionEndDate"
                                        ErrorMessage="ｵｰｸｼｮﾝ終了日時を入力してください。" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdrAuctionEndDate">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                    <% // 範囲チェック %>
                                    <asp:CustomValidator ID="vdcAuctionEndDate" runat="server" ControlToValidate="txtAuctionEndDate"
                                        ErrorMessage="" OnServerValidate="vdcAuctionEndDate_SeverValidate" ValidationGroup="Update">*</asp:CustomValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdcAuctionEndDate">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    開始金額
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtReserveAmt" runat="server" MaxLength="7" Width="70px"></asp:TextBox>円
                                    <ajaxToolkit:FilteredTextBoxExtender ID="extReserveAmt" runat="server" Enabled="true"
                                        FilterType="Numbers" TargetControlID="txtReserveAmt" />
                                    <% // 必須チェック %>
                                    <asp:RequiredFieldValidator ID="vdrReserveAmt" runat="server" ControlToValidate="txtReserveAmt"
                                        ErrorMessage="開始金額を入力してください。" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdrReserveAmt">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    入札単位
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtMinimumBidAmt" runat="server" MaxLength="7" Width="70px"></asp:TextBox>円
                                    <ajaxToolkit:FilteredTextBoxExtender ID="extMinimumBidAmt" runat="server" Enabled="true"
                                        FilterType="Numbers" TargetControlID="txtMinimumBidAmt" />
                                    <% // 必須チェック %>
                                    <asp:RequiredFieldValidator ID="vdrMininumBidAmt" runat="server" ControlToValidate="txtMinimumBidAmt"
                                        ErrorMessage="入札単位を入力してください。" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdrMininumBidAmt">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    参加ﾎﾟｲﾝﾄ
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtEntryPoint" runat="server" MaxLength="5" Width="50px"></asp:TextBox>Pt
                                    <ajaxToolkit:FilteredTextBoxExtender ID="extEntryPoint" runat="server" Enabled="true"
                                        FilterType="Numbers" TargetControlID="txtEntryPoint" />
                                     <% // 必須チェック %>
                                    <asp:RequiredFieldValidator ID="vdrEntryPoint" runat="server" ControlToValidate="txtEntryPoint"
                                        ErrorMessage="参加ﾎﾟｲﾝﾄを入力してください。" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdrEntryPoint">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                               </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    参加ﾎﾟｲﾝﾄ ﾌﾞﾗｲﾝﾄﾞ時
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtBlindEntryPoint" runat="server" MaxLength="5" Width="50px"></asp:TextBox>Pt
                                    <ajaxToolkit:FilteredTextBoxExtender ID="extBlindEntryPoint" runat="server" Enabled="true"
                                        FilterType="Numbers" TargetControlID="txtBlindEntryPoint" />
                                    <% // 必須チェック %>
                                    <asp:RequiredFieldValidator ID="vdrBlindEntryPoint" runat="server" ControlToValidate="txtBlindEntryPoint"
                                        ErrorMessage="ﾌﾞﾗｲﾝﾄﾞ時の参加ﾎﾟｲﾝﾄを入力してください。" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdrBlindEntryPoint">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    入札ﾎﾟｲﾝﾄ
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtBidPoint" runat="server" MaxLength="5" Width="50px"></asp:TextBox>Pt
                                    <ajaxToolkit:FilteredTextBoxExtender ID="extBidPoint" runat="server" Enabled="true"
                                        FilterType="Numbers" TargetControlID="txtBidPoint" />
                                    <% // 必須チェック %>
                                    <asp:RequiredFieldValidator ID="vdrBidPoint" runat="server" ControlToValidate="txtBidPoint"
                                        ErrorMessage="入札ﾎﾟｲﾝﾄを入力してください。" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender17" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdrBidPoint">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle">
                                    入札ﾎﾟｲﾝﾄ ﾌﾞﾗｲﾝﾄﾞ時
                                </td>
                                <td class="tdDataStyle">
                                    <asp:TextBox ID="txtBlindBidPoint" runat="server" MaxLength="5" Width="50px"></asp:TextBox>Pt
                                    <ajaxToolkit:FilteredTextBoxExtender ID="extBlindBidPoint" runat="server" Enabled="true"
                                        FilterType="Numbers" TargetControlID="txtBlindBidPoint" />
                                    <% // 必須チェック %>
                                    <asp:RequiredFieldValidator ID="vdrBlindBidPoint" runat="server" ControlToValidate="txtBlindBidPoint"
                                        ErrorMessage="ﾌﾞﾗｲﾝﾄﾞ時の入札ﾎﾟｲﾝﾄを入力してください。" ValidationGroup="Update">*</asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18" runat="Server"
                                        HighlightCssClass="validatorCallout" TargetControlID="vdrBlindBidPoint">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdHeaderStyle2">
                                    自動延長有
                                </td>
                                <td class="tdDataStyle">
                                    <asp:CheckBox ID="chkAutoExtension" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                    <asp:HiddenField ID="hdnAuctionRevisionNo" runat="server"></asp:HiddenField>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlPickup">
                    <fieldset class="fieldset" style="white-space: nowrap;">
                        <legend>[ﾋﾟｯｸｱｯﾌﾟ]</legend>
                        <asp:Repeater ID="rptPickup" runat="server" DataSource="<%# DummyPickupArray %>">
                            <ItemTemplate>
                                <asp:DropDownList ID="lstPickup" runat="server" DataSourceID="dsProductPickupManage"
                                    DataTextField="PICKUP_TITLE" DataValueField="PICKUP_ID" AppendDataBoundItems="true">
                                    <asp:ListItem Text="" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <%# ( (Container.ItemIndex + 1) % 5 == 0)?"<br />":"" %>
                            </ItemTemplate>
                        </asp:Repeater>
                    </fieldset>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlProductGenre">
                    <fieldset class="fieldset" style="white-space: nowrap;">
                        <legend>[ジャンル]</legend>
                        <asp:Table ID="tblProductGenre" runat="server" EnableViewState="true">
                        </asp:Table>
                        <asp:Label ID="lblGenreNothingMessage" runat="server" Text="ジャンルが登録されていません。" ForeColor="red"></asp:Label>
                    </fieldset>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlKeyword">
                    <fieldset class="fieldset" style="white-space: nowrap;">
                        <legend>[キーワード]</legend>
                        <asp:Repeater ID="rptKeyword" runat="server" DataSource="<%# DummyKeywordArray %>">
                            <ItemTemplate>
                                <asp:TextBox ID="txtKeywordValue" runat="server" MaxLength="64" Width="200px"></asp:TextBox>
                                <asp:HiddenField ID="hdnKeywordUnEditableFlag" runat="server"></asp:HiddenField>
                                <%# ( (Container.ItemIndex + 1) % 4 == 0)?"<br />":"" %>
                            </ItemTemplate>
                        </asp:Repeater>
                    </fieldset>
                </asp:Panel>
                <asp:Panel ID="pnlCommand" runat="server">
                    <asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Update"
                        OnClick="btnUpdate_Click" />
                    <asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" ValidationGroup="Delete"
                        OnClick="btnDelete_Click" Visible="false" />
                    <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" CausesValidation="False"
                        Visible="false" OnClick="btnCancel_Click" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
    </div>
    <% // *****************************************		%>
    <% //  DataSource									%>
    <% // *****************************************		%>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsProductAgent" runat="server" SelectMethod="GetList" TypeName="ProductAgent"
        OnSelecting="dsProductAgent_Selecting">
        <SelectParameters>
            <asp:Parameter Name="pOuterAgentFlag" Type="object" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsProductType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="25" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsProductPickupManage" runat="server" SelectMethod="GetList"
        TypeName="ProductPickupManage" OnSelecting="dsProductPickupManage_Selecting">
        <SelectParameters>
            <asp:Parameter Name="pSiteCd" Type="String" />
            <asp:Parameter Name="pProductType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <% // *****************************************		%>
    <% //  Extender										%>
    <% // *****************************************		%>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtGrossPrice" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtChargePoint" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtSpecialChargePoint" />
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtRoyaltyRate" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate"
        ConfirmText="商品情報を更新します。よろしいですか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete"
        ConfirmText="商品情報を削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
</asp:Content>
