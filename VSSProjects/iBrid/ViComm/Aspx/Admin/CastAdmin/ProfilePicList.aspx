<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ProfilePicList.aspx.cs" Inherits="Cast_ProfilePicList" Title="プロフィール画像"
	ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="プロフィール画像"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 740px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD"
							OnSelectedIndexChanged="lstSiteCd_SelectedIndexChanged" AutoPostBack="True" Width="240px">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle">
							アップロード日付
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtUploadDayFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RangeValidator ID="vdeUploadDayFrom" runat="server" ControlToValidate="txtUploadDayFrom" ErrorMessage="アップロード日付Fromを正しく入力して下さい。" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							〜&nbsp;
							<asp:TextBox ID="txtUploadDayTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RangeValidator ID="vdeUploadDayTo" runat="server" ControlToValidate="txtUploadDayTo" ErrorMessage="アップロード日付Toを正しく入力して下さい。" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							未認証画像のみ出力
						</td>
						<td class="tdDataStyle">
							<asp:CheckBox ID="chkNotApproveFlag" runat="server" Text="" />
						</td>
						<td class="tdHeaderStyle2">
							プロフ画像のみ出力
						</td>
						<td class="tdDataStyle">
							<asp:CheckBox ID="chkProfilePicFlag" runat="server" Text="" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							ログインＩＤ
						</td>
						<td class="tdDataStyle" colspan="3">
							<asp:TextBox ID="txtLoginId" runat="server"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							画像属性区分
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstCastPicAttrType" runat="server" Width="240px" AutoPostBack="true"
							    DataSourceID="" DataTextField="CAST_PIC_ATTR_TYPE_NM" DataValueField="CAST_PIC_ATTR_TYPE_SEQ" 
							    OnSelectedIndexChanged="lstCastPicAttrType_SelectedIndexChanged">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle2">
							画像属性値
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstCastPicAttrTypeValue" runat="server" 
							    DataSourceID="" DataTextField="CAST_PIC_ATTR_NM" DataValueField="CAST_PIC_ATTR_SEQ" Width="240px">
							</asp:DropDownList>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="True" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
			</asp:Panel>
		</fieldset>
		<fieldset>
			<legend>[プロフィール画像一覧]</legend>
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="550px">
				<asp:GridView ID="grdPic" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCastPic" SkinID="GridViewColor" DataKeyNames="SITE_CD,USER_SEQ,USER_CHAR_NO,PIC_SEQ,LOGIN_ID"
					OnDataBound="grdPic_DataBound">
					<Columns>
						<asp:TemplateField HeaderText="写真" SortExpression="PIC_SEQ">
							<ItemTemplate>
								<a href="<%# PickupHelper.GeneratePickupObjsViewerUrl(this, Eval("SITE_CD").ToString(),Eval("PIC_SEQ").ToString(), ViComm.ViCommConst.PicupTypes.PROFILE_PIC) %>">
									<img src='<%# string.Format("{0}?t={1}",Eval("OBJ_SMALL_PHOTO_IMG_PATH", "../{0}"),DateTime.Now.ToString("HHmmssfffff")) %>' alt='プロフィール画像' />
								</a>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField DataField="LOGIN_ID" HeaderText="ﾛｸﾞｲﾝID">
							<ItemStyle Wrap="false" />
						</asp:BoundField>
						<asp:TemplateField HeaderText="ハンドル名">
							<ItemStyle Wrap="false" />
							<ItemTemplate>
								<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# string.Format("../Cast/CastView.aspx?loginid={0}&return={1}",Eval("LOGIN_ID"),"ProfilePicList.aspx") %>'
									Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:HyperLink>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField DataField="UPLOAD_DATE" HeaderText="アップロード日時" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" Wrap="false" />
						</asp:BoundField>
						<asp:TemplateField HeaderText="プロフ用">
							<ItemStyle HorizontalAlign="Center" />
							<ItemTemplate>
								<asp:Label ID="lblProfilePicType" runat="server" Text='<%# GetProfilePicMark(Eval("PROFILE_PIC_FLAG")) %>'></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<%--<asp:TemplateField HeaderText="削除">
							<ItemStyle HorizontalAlign="Center" />
							<ItemTemplate>
								<asp:Button ID="btnDelete" runat="server" CommandArgument='<%# Container.DataItemIndex %>' OnClick="btnDelete_OnClick" Text="削除" OnClientClick="return confirm('削除を行いますか？');">
								</asp:Button>
							</ItemTemplate>
						</asp:TemplateField>--%>
						<asp:TemplateField HeaderText="公開">
							<ItemStyle HorizontalAlign="Center" />
							<ItemTemplate>
								<asp:Button ID="btnUnClose" runat="server" CommandArgument='<%# Container.DataItemIndex %>' OnClick="btnUnClose_OnClick" Text="公開" OnClientClick="return confirm('画像を公開しますか？');">
								</asp:Button>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="非公開">
							<ItemStyle HorizontalAlign="Center" />
							<ItemTemplate>
								<asp:Button ID="btnClose" runat="server" CommandArgument='<%# Container.DataItemIndex %>' OnClick="btnClose_OnClick" Text="非公開" OnClientClick="return confirm('画像を非公開にしますか？');">
								</asp:Button>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="" Visible="false">
							<ItemStyle HorizontalAlign="Center" />
							<ItemTemplate>
								<asp:Label ID="lblClosed" runat="server" Text='<%# Eval("OBJ_NOT_PUBLISH_FLAG") %>'></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="未認証">
							<ItemStyle HorizontalAlign="Center" />
							<ItemTemplate>
								<asp:Label ID="lblNotApproveFlag" runat="server" Text='<%# GetProfilePicMark(Eval("OBJ_NOT_APPROVE_FLAG")) %>'></asp:Label>
							</ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="写真設定">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkMainte" runat="server" CommandArgument='<%# string.Format("{0}:{1}:{2}:{3}",Eval("SITE_CD"),Eval("USER_SEQ"),Eval("USER_CHAR_NO"),Eval("PIC_SEQ")) %>'
                                    OnCommand="lnkMainte_Command" Text="設定"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:HyperLinkField HeaderText="写真設定" Text="設定" DataNavigateUrlFields="SITE_CD,USER_SEQ,USER_CHAR_NO,PIC_SEQ"
                            DataNavigateUrlFormatString="../CastAdmin/ProfilePicMainte.aspx?sitecd={0}&userseq={1}&usercharno={2}&picseq={3}&return=ProfilePicList">
                            <HeaderStyle Wrap="false" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:HyperLinkField>--%>
                    </Columns>
                    <PagerSettings Mode="NumericFirstLast" />
				</asp:GridView>
			</asp:Panel>
			&nbsp;
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br />
				<a class="reccount">Current viewing page
					<%=grdPic.PageIndex + 1%>
					of
					<%=grdPic.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsCastPic" runat="server" SelectMethod="GetProfilePicPageCollection" TypeName="CastPic" SelectCountMethod="GetProfilePicPageCount"
		EnablePaging="True" OnSelected="dsCastPic_Selected" OnSelecting="dsCastPic_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUploadDateFrom" Type="String" />
			<asp:Parameter Name="pUploadDateTo" Type="String" />
			<asp:Parameter Name="pNotApproveFlag" Type="String" />
			<asp:Parameter Name="pProfilePicFlag" Type="String" />
			<asp:Parameter Name="pCastPicAttrType" Type="String" />
			<asp:Parameter Name="pCastPicAttrTypeValue" Type="String" />
			<asp:Parameter Name="pLoginId" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastPicAttrType" runat="server" SelectMethod="GetListIncDefault" TypeName="CastPicAttrType" OnSelecting="dsCastPicAttrType_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastPicAttrTypeValue" runat="server" SelectMethod="GetListIncDefault" TypeName="CastPicAttrTypeValue" OnSelecting="dsCastPicAttrTypeValue_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pCastPicAttrTypeSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:MaskedEditExtender ID="mskUploadDayFrom" runat="server" TargetControlID="txtUploadDayFrom" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay"
		ClearMaskOnLostFocus="true">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:ValidatorCalloutExtender ID="vceUploadDayFrom" runat="Server" TargetControlID="vdeUploadDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskUploadDayTo" runat="server" TargetControlID="txtUploadDayTo" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay"
		ClearMaskOnLostFocus="true">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:ValidatorCalloutExtender ID="vceUploadDayTo" runat="Server" TargetControlID="vdeUploadDayTo" HighlightCssClass="validatorCallout" />
</asp:Content>
