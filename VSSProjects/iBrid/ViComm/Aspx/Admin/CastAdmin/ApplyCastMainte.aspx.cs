﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 申し込み中出演者認証
--	Progaram ID		: ApplyCastMainte
--
--  Creation Date	: 2009.12.14
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.IO;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class CastAdmin_ApplyCastMainte:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			InitPage();
			DataBind();
		}
	}

	private void InitPage() {
		lblExistUser.Visible = false;
		ViewState["TEMP_REGIST_ID"] = iBridUtil.GetStringValue(Request.QueryString["tempid"]);
		GetData();
	}

	private void GetData() {
		DataSet ds;
		DataRow dr;
		using (TempRegist oTempRegist = new TempRegist()) {
			ds = oTempRegist.GetOne(ViewState["TEMP_REGIST_ID"].ToString());
			dr = ds.Tables["VW_TEMP_REGIST02"].Rows[0];
			lblCastNm.Text = dr["CAST_NM"].ToString();
			lblEmailAddr.Text = dr["EMAIL_ADDR"].ToString();
			lblTel.Text = dr["TEL"].ToString();
			lblsignUpFlag.Text = dr["SIGNUP_FLAG"].ToString();
			lblImodeId.Text = dr["IMODE_ID"].ToString();
			lblGuardianNm.Text = dr["GUARDIAN_NM"].ToString();
			lblGuardianTel.Text = dr["GUARDIAN_TEL"].ToString();
			txtRemarks.Text = dr["REMARKS"].ToString();
			string sBirthDay = dr["BIRTHDAY"].ToString();

			ViewState["TRACKING_URL"] = dr["TRACKING_URL"].ToString();
			ViewState["TRACKING_ADDITION_INFO"] = dr["TRACKING_ADDITION_INFO"].ToString();
			ViewState["REGIST_AFFILIATE_CD"] = dr["REGIST_AFFILIATE_CD"].ToString();
			ViewState["REGIST_IP_ADDR"] = dr["REGIST_IP_ADDR"].ToString();
			ViewState["MOBILE_CARRIER_CD"] = dr["MOBILE_CARRIER_CD"].ToString();
			ViewState["TERMINAL_UNIQUE_ID"] = dr["TERMINAL_UNIQUE_ID"].ToString();
			ViewState["IMODE_ID"] = dr["IMODE_ID"].ToString();
			ViewState["GAME_REGIST_FLAG"] = dr["GAME_REGIST_FLAG"].ToString();

			ViewState["SITE_CD"] = dr["SITE_CD"].ToString();
			ViewState["USER_SEQ"] = dr["USER_SEQ"].ToString();

			string sTmpBirthday;
			if (sBirthDay.Length == 8) {
				sTmpBirthday = sBirthDay.Substring(0,4) + "/" + sBirthDay.Substring(4,2) + "/" + sBirthDay.Substring(6,2);
			} else {
				sTmpBirthday = sBirthDay;
			}
			lblBirthDay.Text = sTmpBirthday;

			// 和暦年を生年月日の後ろに連結する
			DateTime dt = Convert.ToDateTime(sTmpBirthday);
			System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("ja-JP",false);
			ci.DateTimeFormat.Calendar = new System.Globalization.JapaneseCalendar();
			lblBirthdayJp.Text = dt.ToString(" (gy年)",ci);

			if (!string.IsNullOrEmpty(sTmpBirthday)) {
				lblAge.Text = string.Format("({0}才)",ViCommPrograms.Age(sTmpBirthday));
			}
			lblApplyDate.Text = dr["REGIST_APPLY_DATE"].ToString();
		}

		this.GetPictureData();

		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);

		// Available Serive?
		using (ManageCompany oCompany = new ManageCompany()) {
			if (oCompany.IsAvailableService(ViCommConst.RELEASE_CAST_IP_PIC_AUTH)) {
				btnUpdate.Enabled = ((dr["ID_PIC_SEQ1"].ToString().Equals("") == false) && (dr["ID_PIC_SEQ2"].ToString().Equals("") == false));
			}
		}

		// 生年月日の重複一覧表示用
		grdDupCast.PageIndex = 0;
		grdDupCast.PageSize = 50;
		grdDupCast.DataSourceID = "dsDupCast";
		grdDupCast.DataBind();
	}

	private void GetPictureData() {
		using (TempRegist oTempRegist = new TempRegist()) {
			DataSet oDataSet = oTempRegist.GetOneIdPicSeq(ViewState["TEMP_REGIST_ID"].ToString());
			if (oDataSet.Tables[0].Rows.Count > 0) {
				this.rptUpload.DataSource = oDataSet.Tables[0].Rows[0].ItemArray;
				this.rptUpload.DataBind();
			}
		}
	}

	protected void dsTempRegist_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["TEMP_REGIST_ID"]);
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		string sSiteCd = "";
		string sLoginId = "";
		string sUserSeq = "";
		string sResult = "";
		string sRegistStatus = "";
		lblExistUser.Visible = false;

		if (ViCommConst.FLAG_ON_STR.Equals(iBridUtil.GetStringValue(ViewState["GAME_REGIST_FLAG"]))) {
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("APPLY_CAST_FROM_GAME");
				db.ProcedureInParm("pTEMP_REGIST_ID",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["TEMP_REGIST_ID"]));
				db.ProcedureOutParm("pSITE_CD",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("pLOGIN_ID",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("pUSER_SEQ",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("pRESULT",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("pREGIST_STATUS",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();

				sSiteCd = db.GetStringValue("PSITE_CD");
				sLoginId = db.GetStringValue("PLOGIN_ID");
				sUserSeq = db.GetStringValue("PUSER_SEQ");
				sResult = db.GetStringValue("PRESULT");
				sRegistStatus = db.GetStringValue("PREGIST_STATUS");
			}
		} else {
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("APPLY_CAST");
				db.ProcedureInParm("PTEMP_REGIST_ID",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["TEMP_REGIST_ID"]));
				db.ProcedureInParm("PMANAGER_SEQ",DbSession.DbType.VARCHAR2,lstManager.SelectedValue);
				db.ProcedureInParm("PREMARKS",DbSession.DbType.VARCHAR2,txtRemarks.Text);
				db.ProcedureOutParm("PSITE_CD",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("PLOGIN_ID",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("PUSER_SEQ",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("PREGIST_STATUS",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();

				sSiteCd = db.GetStringValue("PSITE_CD");
				sLoginId = db.GetStringValue("PLOGIN_ID");
				sUserSeq = db.GetStringValue("PUSER_SEQ");
				sResult = db.GetStringValue("PRESULT");
				sRegistStatus = db.GetStringValue("PREGIST_STATUS");
			}
		}

		string sWebPhisicalDir = "";

		using (Site oSite = new Site()) {
			oSite.GetValue(sSiteCd,"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
		}

		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
			if (sResult.Equals("0")) {
				string sDir = "";

				sDir = sWebPhisicalDir + ViCommConst.PIC_DIRECTRY + "\\" + sSiteCd + string.Format("\\Operator\\{0}",sLoginId);
				Directory.CreateDirectory(sDir);

				sDir = sWebPhisicalDir + ViCommConst.MOVIE_DIRECTRY + "\\" + sSiteCd + string.Format("\\Operator\\{0}",sLoginId);
				Directory.CreateDirectory(sDir);
				if (sRegistStatus.Equals(ViCommConst.REGIST_COMPLITE)) {
					if (ViewState["REGIST_AFFILIATE_CD"].ToString().Equals("") == false) {

						string sUID;
						if (ViewState["MOBILE_CARRIER_CD"].ToString().Equals(ViCommConst.KDDI)) {
							sUID = ViewState["TERMINAL_UNIQUE_ID"].ToString();
						} else {
							sUID = ViewState["IMODE_ID"].ToString();
						}
						string sTrackingUrl = string.Format(ViewState["TRACKING_URL"].ToString() + ViewState["TRACKING_ADDITION_INFO"].ToString(),ViewState["REGIST_AFFILIATE_CD"].ToString(),sLoginId,ViewState["REGIST_IP_ADDR"].ToString(),sUID);
						if (!sTrackingUrl.Equals(string.Empty)) {
							ViCommInterface.TransToParent(sTrackingUrl,true);
						}
					}
				}
				Server.Transfer("ApplyCastList.aspx");
			} else if (sResult.Equals("1")) {
				Server.Transfer("ApplyCastList.aspx");
			} else if (sResult.Equals("2")) {
				lblExistUser.Visible = true;
			}
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("TEMP_REGIST_DELETE");
			db.ProcedureInParm("PTEMP_REGIST_ID",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["TEMP_REGIST_ID"]));
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		Server.Transfer("ApplyCastList.aspx");
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		Server.Transfer("ApplyCastList.aspx");
	}

	protected void dsTempRegist_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		DataSet ds = (DataSet)e.ReturnValue;
		if (ds == null) {
			return;
		}
	}

	protected void btnUpload_Command(object sender,CommandEventArgs e) {
		if (!e.CommandName.Equals("UPLOAD")) {
			return;
		}

		int iItemIndex;
		if (int.TryParse(e.CommandArgument.ToString(),out iItemIndex)) {
			FileUpload oFileUpload = this.rptUpload.Items[iItemIndex].FindControl("uldIdPic") as FileUpload;
			if (oFileUpload != null) {
				this.UploadPicture(oFileUpload,iItemIndex + 1);
				this.GetPictureData();
			}
		}
	}

	private void UploadPicture(System.Web.UI.WebControls.FileUpload pUpload,int pPicNo) {
		string sWebPhisicalDir = "";
		string sDomain = "";
		decimal dNo;

		using (Site oSite = new Site()) {
			oSite.GetValue(ViCommConst.CAST_SITE_CD,"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
			oSite.GetValue(ViCommConst.CAST_SITE_CD,"HOST_NM",ref sDomain);
		}

		using (CastPic objPic = new CastPic()) {
			dNo = objPic.GetPicNo();
		}

		string sFileNm = "",sPath = "",sFullPath = "";

		if (pUpload.HasFile) {

			sFileNm = ViCommConst.PIC_HEADER + iBridUtil.addZero(dNo.ToString(),ViCommConst.OBJECT_NM_LENGTH);
			sPath = sWebPhisicalDir +
							ViCommConst.PIC_DIRECTRY + "\\" + ViCommConst.CAST_SITE_CD + "\\Operator\\ID-PIC";

			sFullPath = sPath + "\\" + sFileNm + ViCommConst.PIC_FOODER;

			using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {
				if (!System.IO.File.Exists(sFullPath)) {
					pUpload.SaveAs(sFullPath);
				}

				using (DbSession db = new DbSession()) {
					db.PrepareProcedure("TEMP_REGIST_ID_PIC_UPLOAD");
					db.ProcedureInParm("PTEMP_REGIST_ID",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["TEMP_REGIST_ID"]));
					db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.VARCHAR2,dNo);
					db.ProcedureInParm("PID_PIC_NO",DbSession.DbType.NUMBER,pPicNo);
					db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
					db.ExecuteProcedure();
				}
				GetData();
			}
		}
	}

	protected void btnUpdateRmarks_Click(object sender,EventArgs e) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_TEMP_REGIST_REMARKS");
			db.ProcedureInParm("PTEMP_REGIST_ID",DbSession.DbType.VARCHAR2,iBridUtil.GetStringValue(ViewState["TEMP_REGIST_ID"]));
			db.ProcedureInParm("PREMARKS",DbSession.DbType.VARCHAR2,txtRemarks.Text);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		GetData();
	}

	protected bool GetImgVisible(object pIdPicSeq) {
		string sIdPicSeq = iBridUtil.GetStringValue(pIdPicSeq);
		return !string.IsNullOrEmpty(sIdPicSeq);
	}

	protected string GetImgUrl(object pIdPicSeq) {
		return this.GetImgUrl(pIdPicSeq, false);
	}

	protected string GetImgUrl(object pIdPicSeq,bool pIsPicViewer) {
		if (!this.GetImgVisible(pIdPicSeq)) {
			return string.Empty;
		}

		// 身分証の画像URL
		string sImgUrl = "/data/" + ViCommConst.CAST_SITE_CD + "/operator/ID-PIC/" + iBridUtil.addZero(iBridUtil.GetStringValue(pIdPicSeq),ViCommConst.OBJECT_NM_LENGTH) + ".jpg";

		// ポップアップで画像を表示する場合のURL
		if (pIsPicViewer) {
			return sImgUrl;
		}
		return ConfigurationManager.AppSettings["Root"] + sImgUrl;
	}

	protected string GeneratePicViewerUrl(object pIdPicSeq) {
		if (!this.GetImgVisible(pIdPicSeq)) {
			return string.Empty;
		}
		return string.Format("javascript:openPicViewer('{0}');",this.GetImgUrl(pIdPicSeq, true));
	}

	protected void dsDupCast_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = iBridUtil.GetStringValue(ViewState["USER_SEQ"]);
		e.InputParameters[1] = lblBirthDay.Text;
	}

	protected void grdDupCast_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			lblMessage.Visible = true;
		}
	}
}
