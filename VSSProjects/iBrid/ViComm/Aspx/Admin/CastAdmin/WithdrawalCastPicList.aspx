<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" ValidateRequest="false"
	CodeFile="WithdrawalCastPicList.aspx.cs" Inherits="CastAdmin_WithdrawalCastPicList" Title="退会出演者画像一覧"
%>
<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="退会出演者画像一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<%-- ============================== --%>
		<%--  Search Condition              --%>
		<%-- ============================== --%>
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 740px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							ログインＩＤ
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtLoginId" runat="server"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							退会日
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtWithdrawalDayFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RangeValidator ID="vdeWithdrawalDayFrom" runat="server" ControlToValidate="txtWithdrawalDayFrom" ErrorMessage="アップロード日付Fromを正しく入力して下さい。" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							〜&nbsp;
							<asp:TextBox ID="txtWithdrawalDayTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RangeValidator ID="vdeWithdrawalDayTo" runat="server" ControlToValidate="txtWithdrawalDayTo" ErrorMessage="アップロード日付Toを正しく入力して下さい。" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							表示画像
						</td>
						<td class="tdDataStyle">
							<asp:CheckBox ID="chkProfilePicFlag" runat="server" Text="プロフ画像" />
							<asp:CheckBox ID="chkBbsPicFlag" runat="server" Text="掲示板画像" />
							<asp:CheckBox ID="chkStockPicFlag" runat="server" Text="ストック画像" />
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="True" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
			</asp:Panel>
		</fieldset>

		<%-- ============================== --%>
		<%--  Data                          --%>
		<%-- ============================== --%>
		<fieldset>
			<legend>[画像一覧]</legend>
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="550px">
				<asp:GridView ID="grdPic" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCastPic" SkinID="GridViewColor">
					<Columns>
						<asp:TemplateField HeaderText="写真" SortExpression="PIC_SEQ">
							<ItemTemplate>
								<img src='<%# string.Format("{0}?t={1}",Eval("SMALL_PHOTO_IMG_PATH", "../{0}"),DateTime.Now.ToString("HHmmssfffff")) %>' alt='画像' />
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ログインID">
							<ItemStyle Wrap="false" />
							<ItemTemplate>
								<asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# string.Format("../Cast/CastView.aspx?loginid={0}&return={1}",Eval("LOGIN_ID"),"WithdrawalCastPicList.aspx") %>'
									Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField DataField="HANDLE_NM" HeaderText="ハンドル名">
							<ItemStyle Wrap="false" />
						</asp:BoundField>
						<asp:BoundField DataField="UPLOAD_DATE" HeaderText="アップロード日時" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" Wrap="false" />
						</asp:BoundField>
						<asp:BoundField DataField="WITHDRAWAL_DATE" HeaderText="退会日時" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" Wrap="false" />
						</asp:BoundField>
						<asp:TemplateField HeaderText="詳細">
							<ItemStyle HorizontalAlign="Center" />
							<ItemTemplate>
								<asp:HyperLink ID="lnkDetail" runat="server" NavigateUrl='<%# string.Format("WithdrawalCastPicDetail.aspx?loginid={0}&picseq={1}",Eval("LOGIN_ID"),Eval("PIC_SEQ")) %>'
									Text="確認"></asp:HyperLink>
							</ItemTemplate>
						</asp:TemplateField>
                    </Columns>
                    <PagerSettings Mode="NumericFirstLast" />
				</asp:GridView>
			</asp:Panel>
			&nbsp;
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br />
				<a class="reccount">Current viewing page
					<%=grdPic.PageIndex + 1%>
					of
					<%=grdPic.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>

	<%-- ============================== --%>
	<%--  Data Source                   --%>
	<%-- ============================== --%>
	<asp:ObjectDataSource ID="dsCastPic" runat="server" SelectMethod="GetWithdrawalPicPageCollection" TypeName="CastPic" SelectCountMethod="GetWithdrawalPicPageCount"
		EnablePaging="True" OnSelected="dsCastPic_Selected" OnSelecting="dsCastPic_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pLoginId" Type="String" />
			<asp:Parameter Name="pWithdrawalDateFrom" Type="String" />
			<asp:Parameter Name="pWithdrawalDateTo" Type="String" />
			<asp:Parameter Name="pPicType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:MaskedEditExtender ID="mskWithdrawalDayFrom" runat="server" TargetControlID="txtWithdrawalDayFrom" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay"
		ClearMaskOnLostFocus="true">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:ValidatorCalloutExtender ID="vceWithdrawalDayFrom" runat="Server" TargetControlID="vdeWithdrawalDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskWithdrawalDayTo" runat="server" TargetControlID="txtWithdrawalDayTo" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay"
		ClearMaskOnLostFocus="true">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:ValidatorCalloutExtender ID="vceWithdrawalDayTo" runat="Server" TargetControlID="vdeWithdrawalDayTo" HighlightCssClass="validatorCallout" />
</asp:Content>
