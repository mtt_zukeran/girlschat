﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 認証待ちストック動画一覧
--	Progaram ID		: StockMovieCheckList
--
--  Creation Date	: 2009.08.26
--  Creater			: i-Brid(Y.Inoue)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater				Update Explain

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class CastAdmin_StockMovieCheckList:System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SiteCd {
		get { return iBridUtil.GetStringValue(this.ViewState["SiteCd"]); }
		set { this.ViewState["SiteCd"] = value; }
	}

	private string NotPublishFlag {
		get { return iBridUtil.GetStringValue(this.ViewState["NotPublishFlag"]); }
		set { this.ViewState["NotPublishFlag"] = value; }
	}

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			this.SiteCd = Request.QueryString["sitecd"];
			this.NotPublishFlag = Request.QueryString["closed"];
			FirstLoad();
			InitPage();
			if (iBridUtil.GetStringValue(this.SiteCd).Equals(string.Empty) == false) {
				lstSiteCd.SelectedValue = this.SiteCd;
			}
			//} else if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals(string.Empty)) {
			//    lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			//}
			GetList();
		}
	}

	protected void dsCastMovie_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void FirstLoad() {
		grdMovie.PageSize = 100;
		grdMovie.DataSourceID = string.Empty;
		DataBind();
		if (Session["SiteCd"].ToString().Equals(string.Empty)) {
			lstSiteCd.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		}
		lstSiteCd.DataSourceID = string.Empty;
	}

	private void InitPage() {
		grdMovie.DataSourceID = string.Empty;
		DataBind();
		recCount = "0";
		chkClosed.Checked = false;
		if (this.NotPublishFlag == "1") {
			chkClosed.Checked = true;
		}
	}

	private void ClearField() {
		lstSiteCd.SelectedIndex = 0;
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		if(!this.IsValid) return;
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.Response.Redirect(this.Request.Url.PathAndQuery);
	}

	protected void lnkQuickTime_Command(object sender,CommandEventArgs e) {
		using (CastMovie oMovie = new CastMovie()) {
			if (oMovie.GetOne(decimal.Parse(e.CommandArgument.ToString()))) {
				string sRoot = ConfigurationManager.AppSettings["Root"];
				string sURL = "http://" + Request.Url.Authority + sRoot + ViCommConst.WEB_MOVIE_PATH + "/" + oMovie.siteCd + string.Format("/operator/{0}/{1}{2}",oMovie.loginId,iBridUtil.addZero(oMovie.movieSeq.ToString(),ViCommConst.OBJECT_NM_LENGTH),ViCommConst.MOVIE_FOODER);
				string sScripts = string.Format("<script language=\"javascript\">window.open('{0}','','width=300,height=300,resizable=no,directories=no,scrollbars=no' , false);</script>",sURL);
				ClientScript.RegisterStartupScript(Page.GetType(),"OpenNewWindow",sScripts);
			}
		}
	}

	protected void lnkMainte_Command(object sender,CommandEventArgs e) {
		string[] sKey = e.CommandArgument.ToString().Split(':');
		string sNotPublishFlag = string.Empty;
		if (chkClosed.Checked) {
			sNotPublishFlag = "1";
		}
		Response.Redirect(string.Format("../CastAdmin/StockMovieMainte.aspx?sitecd={0}&userseq={1}&usercharno={2}&movieseq={3}&return={4}&closed={5}",sKey[0],sKey[1],sKey[2],sKey[3],sKey[4],sNotPublishFlag));
	}

	private void GetList() {
		grdMovie.DataSourceID = "dsCastMovie";
		grdMovie.PageIndex = 0;
		grdMovie.DataBind();
		pnlCount.DataBind();
	}

	protected void dsCastMovie_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		string sNotPublishFlag = string.Empty;
		if (chkClosed.Checked) {
			sNotPublishFlag = "1";
		}
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = "1";
		e.InputParameters[2] = sNotPublishFlag;
		e.InputParameters[3] = ViCommConst.ATTACHED_MAIL.ToString();
		e.InputParameters[4] = string.IsNullOrEmpty(this.txtUploadDayFrom.Text.Replace("/", string.Empty)) ? null : this.txtUploadDayFrom.Text;
		e.InputParameters[5] = string.IsNullOrEmpty(this.txtUploadDayTo.Text.Replace("/", string.Empty)) ? null : this.txtUploadDayTo.Text;		
	}

	protected void grdMovie_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			if (DataBinder.Eval(e.Row.DataItem,"OBJ_NOT_PUBLISH_FLAG").ToString() == "1") {
				e.Row.BackColor = System.Drawing.Color.LightYellow;
			}
			if (DataBinder.Eval(e.Row.DataItem, "CAUTION_FLAG").ToString() == "1") {
				e.Row.BackColor = System.Drawing.Color.Gainsboro;
			}
		}
	}
}
