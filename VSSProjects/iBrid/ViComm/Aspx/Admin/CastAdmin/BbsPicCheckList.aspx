<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="BbsPicCheckList.aspx.cs" Inherits="Cast_BbsPicCheckList" Title="認証待ち掲示板画像"
    ValidateRequest="false" %>
<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="認証待ち掲示板画像"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <fieldset class="fieldset">
            <legend>[検索条件]</legend>
            <asp:Panel runat="server" ID="pnlKey">
                <table border="0" style="width: 740px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            サイト
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" OnSelectedIndexChanged="lstSiteCd_SelectedIndexChanged" AutoPostBack="True" Width="240px">
                            </asp:DropDownList>
                        </td>
                        <td class="tdHeaderStyle">
                            アップロード日付
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtUploadDayFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
                            <asp:RangeValidator ID="vdeUploadDayFrom" runat="server" ControlToValidate="txtUploadDayFrom"
                                ErrorMessage="アップロード日付Fromを正しく入力して下さい。" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
                                Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                            〜&nbsp;
                            <asp:TextBox ID="txtUploadDayTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
                            <asp:RangeValidator ID="vdeUploadDayTo" runat="server" ControlToValidate="txtUploadDayTo"
                                ErrorMessage="アップロード日付Toを正しく入力して下さい。" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
                                Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                        </td>
                    </tr>
					<tr>
						<td class="tdHeaderStyle">
							画像属性区分
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstCastPicAttrType" runat="server" Width="240px" AutoPostBack="true"
							    DataSourceID="" DataTextField="CAST_PIC_ATTR_TYPE_NM" DataValueField="CAST_PIC_ATTR_TYPE_SEQ" 
							    OnSelectedIndexChanged="lstCastPicAttrType_SelectedIndexChanged">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle2">
							画像属性値
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstCastPicAttrTypeValue" runat="server" 
							    DataSourceID="" DataTextField="CAST_PIC_ATTR_NM" DataValueField="CAST_PIC_ATTR_SEQ" Width="240px">
							</asp:DropDownList>
						</td>
					</tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            非公開画像も出力
                        </td>
                        <td class="tdDataStyle" colspan="3">
                            <asp:CheckBox ID="chkClosed" runat="server" />
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    CausesValidation="True" ValidationGroup="Key" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
                <asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" CausesValidation="True" OnClick="btnCSV_Click" />
            </asp:Panel>
        </fieldset>
        <fieldset>
            <legend>[認証待ち掲示板画像一覧]</legend>
            <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="540px">
                <asp:GridView ID="grdPic" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"
                    DataSourceID="dsCastPic" SkinID="GridViewColor" DataKeyNames="SITE_CD,USER_SEQ,USER_CHAR_NO,PIC_SEQ,LOGIN_ID,PIC_TITLE,PIC_DOC,CAST_PIC_ATTR_TYPE_SEQ,CAST_PIC_ATTR_SEQ"
                    OnRowDataBound="grdPic_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="写真" SortExpression="PIC_SEQ">
							<ItemTemplate>
								<a href="<%# PickupHelper.GeneratePickupObjsViewerUrl(this, Eval("SITE_CD").ToString(),Eval("PIC_SEQ").ToString(), ViComm.ViCommConst.PicupTypes.BBS_PIC, ViCommConst.FLAG_OFF) %>">
									<img src="<%# string.Format("{0}?t={1}",Eval("OBJ_SMALL_PHOTO_IMG_PATH", "../{0}"),DateTime.Now.ToString("HHmmssfffff")) %>" alt="掲示板画像" />
								</a>
							</ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="SITE_NM" HeaderText="サイト">
                            <ItemStyle Wrap="false" />
                        </asp:BoundField>                        
                        <asp:TemplateField HeaderText="氏名/ﾊﾝﾄﾞﾙ名">
                            <ItemStyle Wrap="false" />
                            <ItemTemplate>
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# string.Format("../Cast/CastView.aspx?loginid={0}&return={1}",Eval("LOGIN_ID"),"BbsPicCheckList.aspx") %>'
                                    Text='<%# Eval("CAST_NM") %>'></asp:HyperLink><br />
								<asp:Label runat="server" ID="lblHandleNm" Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="UPLOAD_DATE" HeaderText="アップロード日時" DataFormatString="{0:yy/MM/dd HH:mm}"
                            HtmlEncode="False" SortExpression="UPLOAD_DATE">
                            <ItemStyle HorizontalAlign="Center" Wrap="false" />
                        </asp:BoundField>
                        <asp:BoundField DataField="PIC_TITLE" HeaderText="タイトル" HtmlEncode="False">
                            <HeaderStyle Wrap="false" />
                            <ItemStyle Width="150px" HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="PIC_DOC" HeaderText="内容" HtmlEncode="False">
                            <HeaderStyle Wrap="false" />
                            <ItemStyle Width="150px" HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="CAST_PIC_ATTR_TYPE_NM" HeaderText="写真属性" HtmlEncode="False">
                            <HeaderStyle Wrap="false" />
                        </asp:BoundField>
                        <asp:BoundField DataField="CAST_PIC_ATTR_NM" HeaderText="写真属性値" HtmlEncode="False">
                            <HeaderStyle Wrap="false" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="認証">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Button ID="btnUpdate" runat="server" CommandArgument='<%# Container.DataItemIndex %>'
                                    OnClick="btnUpdate_OnClick" Text="認証" OnClientClick="return confirm('設定を行いますか？');">
                                </asp:Button>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="写真設定">
                            <HeaderStyle Wrap="false" />
                            <ItemStyle HorizontalAlign="Center" Wrap="false" />
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkMainte" runat="server" CommandArgument='<%# string.Format("{0}:{1}:{2}:{3}:{4}",Eval("SITE_CD"),Eval("USER_SEQ"),Eval("USER_CHAR_NO"),Eval("PIC_SEQ"),"BbsPicCheckList") %>'
                                    OnCommand="lnkMainte_Command" Text='<%# Eval("NOT_APPROVE_MARK_ADMIN") %>'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="メールアドレス" Visible="false">
                            <ItemStyle HorizontalAlign="left" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblEmail" Text='<%# Eval("EMAIL_ADDR") %>' CssClass="Warp" Width="100px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerSettings Mode="NumericFirstLast" />
                </asp:GridView>
            </asp:Panel>
            &nbsp;
            <asp:Panel runat="server" ID="pnlCount">
                <a class="reccount">Record Count
                    <%#GetRecCount() %>
                </a>
                <br />
                <a class="reccount">Current viewing page
                    <%=grdPic.PageIndex + 1%>
                    of
                    <%=grdPic.PageCount%>
                </a>
            </asp:Panel>
        </fieldset>
        <br />
    </div>
    <asp:ObjectDataSource ID="dsCastPic" runat="server" SelectMethod="GetApproveManagePageCollection"
        TypeName="CastPic" SelectCountMethod="GetApproveManagePageCount" EnablePaging="True"
        OnSelected="dsCastPic_Selected" OnSelecting="dsCastPic_Selecting">
        <SelectParameters>
            <asp:Parameter Name="pSiteCd" Type="String" />
            <asp:Parameter Name="pNotApproveFlag" Type="String" />
            <asp:Parameter Name="pNotPublishFlag" Type="String" />
            <asp:Parameter Name="pPicType" Type="String" />
            <asp:Parameter Name="pUploadDateFrom" Type="String" />
            <asp:Parameter Name="pUploadDateTo" Type="String" />
			<asp:Parameter Name="pCastPicAttrType" Type="String" />
			<asp:Parameter Name="pCastPicAttrTypeValue" Type="String" />
            <asp:Parameter Name="pLoginId" Type="string" DefaultValue="" />
			<asp:QueryStringParameter Name="pItemNo" QueryStringField="pictype_itemno" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastPicAttrType" runat="server" SelectMethod="GetListByItemNo" TypeName="CastPicAttrType" OnSelecting="dsCastPicAttrType_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:QueryStringParameter Name="pItemNo" QueryStringField="pictype_itemno" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastPicAttrTypeValue" runat="server" SelectMethod="GetListIncDefault" TypeName="CastPicAttrTypeValue" OnSelecting="dsCastPicAttrTypeValue_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pCastPicAttrTypeSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site">
    </asp:ObjectDataSource>
    <ajaxToolkit:MaskedEditExtender ID="mskUploadDayFrom" runat="server" TargetControlID="txtUploadDayFrom"
        MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:ValidatorCalloutExtender ID="vceUploadDayFrom" runat="Server" TargetControlID="vdeUploadDayFrom"
        HighlightCssClass="validatorCallout" />
    <ajaxToolkit:MaskedEditExtender ID="mskUploadDayTo" runat="server" TargetControlID="txtUploadDayTo"
        MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:ValidatorCalloutExtender ID="vceUploadDayTo" runat="Server" TargetControlID="vdeUploadDayTo"
        HighlightCssClass="validatorCallout" />
</asp:Content>
