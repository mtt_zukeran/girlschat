﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 同一電話番号検索
--	Progaram ID		: SameTelInquiry
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Cast_SameTelInquiry:System.Web.UI.Page {
	private int siteCount = 0;
	private const int SITE_START_COL = 6;
	private const int SITE_END_COL = 11;

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		grdCast.DataSourceID = "";
		pnlInfo.Visible = false;
		ClearField();
		DataBind();
		if (!IsPostBack) {
			lstOnlineStatus.Items.Insert(0,new ListItem("",""));
			lstOnlineStatus.DataSourceID = "";
			lstOnlineStatus.SelectedIndex = 0;

			if (Session["SiteCd"].ToString().Equals("")) {
				lstSiteCd.Items.Insert(0,new ListItem("",""));
			}
			lstSiteCd.DataSourceID = "";
			if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
				lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			} else {
				lstSiteCd.SelectedIndex = 0;
			}
		}
	}

	private void ClearField() {
		lstOnlineStatus.SelectedIndex = 0;
		lstSiteCd.SelectedIndex = 0;
		txtTerminalId.Text = "";
		rdoTerminalMobile.Checked = true;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}


	protected void grdCast_DataBound(object sender,EventArgs e) {
		int i = SITE_START_COL;

		if (grdCast.HeaderRow == null) {
			return;
		}
		using (Site oSite = new Site()) {
			DataSet ds = oSite.GetList();
			foreach (DataRow dr in ds.Tables[0].Rows) {
				grdCast.HeaderRow.Cells[i++].Text = dr["SITE_MARK"].ToString();
				if (i > SITE_END_COL) {
					break;
				}
			}
		}
		for (int j = i + 1;j <= 10;j++) {
			grdCast.HeaderRow.Cells[j].Text = "";
		}

		joinCells(grdCast,0);
	}

	protected void grdCast_RowCreated(object sender,GridViewRowEventArgs e) {
		if ((e.Row.RowType == DataControlRowType.Header)) {
			using (Site oSite = new Site()) {
				DataSet ds = oSite.GetList();
				siteCount = ds.Tables[0].Rows.Count;
			}
		}
		if ((e.Row.RowType == DataControlRowType.DataRow) || (e.Row.RowType == DataControlRowType.Header)) {
			for (int i = SITE_START_COL + siteCount;i <= SITE_END_COL;i++) {
				e.Row.Cells[i].Visible = false;
			}
		}
	}

	private void GetList() {
		grdCast.DataSourceID = "dsCast";
		grdCast.DataBind();
		pnlInfo.Visible = true;
	}

	protected void joinCells(GridView pGrd,int pCol) {
		int iRow = pGrd.Rows.Count;
		int iBaseidx = 0;

		while (iBaseidx < iRow) {
			int iNextIdx = iBaseidx + 1;
			TableCell celBase = pGrd.Rows[iBaseidx].Cells[pCol];

			while (iNextIdx < iRow) {

				TableCell celNext = pGrd.Rows[iNextIdx].Cells[pCol];

				if (getText(celBase).Equals(getText(celNext))) {
					if (celBase.RowSpan == 0) {
						celBase.RowSpan = 2;
					} else {
						celBase.RowSpan++;
					}
					pGrd.Rows[iNextIdx].Cells.Remove(celNext);
					iNextIdx++;
				} else {
					break;
				}
			}
			iBaseidx = iNextIdx;
		}
	}

	private string getText(TableCell tc) {
		HyperLink dblc =
		  (HyperLink)tc.Controls[1];
		return dblc.Text;
	}

	protected string GetFromDay() {
		return DateTime.Now.ToString("yyyy/MM") + "/01";
	}

	protected string GetToDay() {
		int lastday = DateTime.DaysInMonth(DateTime.Now.Year,DateTime.Now.Month);
		return DateTime.Now.ToString("yyyy/MM") + "/" + lastday.ToString();
	}
}
