﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ピックアップオブジェ コメント

--	Progaram ID		: PickupObjsCommentList
--
--  Creation Date	: 2013.05.28
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;
using System.Collections.Generic;

public partial class CastAdmin_PickupObjsCommentList:System.Web.UI.Page {
	private string recCount = string.Empty;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string LoginId {
		get {
			return iBridUtil.GetStringValue(this.ViewState["LoginId"]);
		}
		set {
			this.ViewState["LoginId"] = value;
		}
	}

	private string PickupId {
		get {
			return iBridUtil.GetStringValue(this.ViewState["PickupId"]);
		}
		set {
			this.ViewState["PickupId"] = value;
		}
	}

	private string ObjSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["ObjSeq"]);
		}
		set {
			this.ViewState["ObjSeq"] = value;
		}
	}

	private DataSet PickupObjsCommentData {
		get {
			return this.ViewState["PickupObjsCommentData"] as DataSet;
		}
		set {
			this.ViewState["PickupObjsCommentData"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();

			this.pnlInfo.Visible = true;
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}

		this.pnlInfo.Visible = true;
		this.GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void dsPickupObjsComment_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		DataSet oPickupObjsCommentDataSet = e.ReturnValue as DataSet;
		if (oPickupObjsCommentDataSet != null && (oPickupObjsCommentDataSet).Tables[0].Rows.Count > 0) {
			this.PickupObjsCommentData = oPickupObjsCommentDataSet;
		}
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void grdPickupObjsComment_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			if (DataBinder.Eval(e.Row.DataItem,"ADMIN_DEL_FLAG").ToString().Equals(ViCommConst.FLAG_ON_STR)) {
				e.Row.ForeColor = Color.Gray;
			} else if (DataBinder.Eval(e.Row.DataItem,"DEL_FLAG").ToString().Equals(ViCommConst.FLAG_ON_STR)) {
				e.Row.ForeColor = Color.Gray;
			}
		}
	}

	protected void dsPickupObjsComment_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		PickupObjsComment.SearchCondition oSearchCondition = new PickupObjsComment.SearchCondition();
		oSearchCondition.Keyword = this.txtKeyword.Text.Trim();
		oSearchCondition.LoginId = this.txtLoginId.Text.Trim();
		oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
		oSearchCondition.PickupId = this.PickupId;
		oSearchCondition.ObjSeq = this.ObjSeq;
		e.InputParameters[0] = oSearchCondition;
	}

	private void InitPage() {

		this.recCount = "0";
		this.txtLoginId.Text = string.Empty;
		this.txtKeyword.Text = string.Empty;
		this.grdPickupObjsComment.DataSourceID = string.Empty;

		this.pnlInfo.Visible = false;
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		if (!this.IsPostBack) {
			this.SiteCd = this.Request.QueryString["sitecd"];
			this.PickupId = this.Request.QueryString["pickupid"];
			this.ObjSeq = this.Request.QueryString["objseq"];


			if (!string.IsNullOrEmpty(this.SiteCd) && !string.IsNullOrEmpty(this.PickupId) && !string.IsNullOrEmpty(this.ObjSeq)) {
				this.lstSiteCd.SelectedValue = this.SiteCd;

				this.pnlInfo.Visible = true;
				this.GetList();
			}
		}
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	private void GetList() {
		this.grdPickupObjsComment.PageIndex = 0;
		this.grdPickupObjsComment.PageSize = 10;
		this.grdPickupObjsComment.DataSourceID = "dsPickupObjsComment";
		this.grdPickupObjsComment.DataBind();
		this.pnlCount.DataBind();
	}

	protected void lnkDelPickupObjsComment_Command(object sender,CommandEventArgs e) {
		string[] arguments = e.CommandArgument.ToString().Split(',');

		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("DELETE_PICKUP_OBJS_COMMENT_ADM");
			oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,arguments[0]);
			oDbSession.ProcedureInParm("pPICKUP_OBJS_COMMENT_SEQ",DbSession.DbType.VARCHAR2,arguments[1]);
			oDbSession.ProcedureInParm("pDEL_FLAG",DbSession.DbType.VARCHAR2,ViCommConst.FLAG_ON_STR.Equals(arguments[2]) ? ViCommConst.FLAG_OFF_STR : ViCommConst.FLAG_ON_STR);
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}

		this.GetList();
	}
}
