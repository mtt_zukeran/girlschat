﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 公開動画一覧
--	Progaram ID		: ProfileMovieOpenList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater				Update Explain
  2009/09/03	i-Brid(Y.Inoue)		担当サイト対応  2010/06/29	伊藤和明			検索項目に日付を追加
  2010/07/16	Koyanagi			検索項目に非公開ﾌﾗｸﾞを追加

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using iBridCommLib;
using ViComm;

public partial class Cast_ProfileMovieOpenList : System.Web.UI.Page {
	private string recCount = "";
	private Stream filter;

	protected void Page_Load(object sender, EventArgs e) {
		filter = Response.Filter;
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
			ViewState["SiteCd"] = iBridUtil.GetStringValue(Request.QueryString["moviesite"]);
			if (iBridUtil.GetStringValue(ViewState["SiteCd"]).Equals("") == false) {
				lstSiteCd.SelectedValue = ViewState["SiteCd"].ToString();
			} else if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
				lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}
			GetList();
		}
	}

	protected void dsCastMovie_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void FirstLoad() {
		grdMovie.PageSize = 100;
		grdMovie.DataSourceID = "";
		DataBind();
		if (Session["SiteCd"].ToString().Equals("")) {
			lstSiteCd.Items.Insert(0, new ListItem("", ""));
		}
		lstSiteCd.DataSourceID = "";
	}

	private void InitPage() {
		grdMovie.DataSourceID = "";
		DataBind();
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}


	protected void btnListSeek_Click(object sender, EventArgs e) {
		if (!this.IsValid) return;
		GetList();
	}

	protected void btnClear_Click(object sender, EventArgs e) {
		this.Response.Redirect(this.Request.Url.PathAndQuery);
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		DataSet ds;

		using (CastMovie oCastMovie = new CastMovie()) {
			ds = oCastMovie.GetApproveManagePageCollection(
				lstSiteCd.SelectedValue,
				"0",
				"0",
				ViCommConst.ATTACHED_PROFILE.ToString(),
				string.IsNullOrEmpty(this.txtUploadDayFrom.Text.Replace("/",string.Empty)) ? null : this.txtUploadDayFrom.Text,
				string.IsNullOrEmpty(this.txtUploadDayTo.Text.Replace("/",string.Empty)) ? null : this.txtUploadDayTo.Text,
				this.txtLoginId.Text.TrimEnd(),
				0,
				Int32.MaxValue
			);
		}

		Response.Filter = filter;
		Response.ContentType = "application/download";
		Response.AddHeader("Content-Disposition","attachment;filename=profileMovieOpenList.csv");
		Response.ContentEncoding = Encoding.GetEncoding("Shift_JIS");

		Response.Write("\"サイト\",\"ログインID\",\"ハンドル名\",\"タイトル\",\"アップロード日時\"\r\n");

		foreach (DataRow dr in ds.Tables[0].Rows) {
			string sData = string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\"\r\n",
				iBridUtil.GetStringValue(dr["SITE_NM"]),
				iBridUtil.GetStringValue(dr["LOGIN_ID"]),
				iBridUtil.GetStringValue(dr["HANDLE_NM"]),
				iBridUtil.GetStringValue(dr["MOVIE_TITLE"]).Replace(Environment.NewLine,""),
				iBridUtil.GetStringValue(dr["UPLOAD_DATE"])
			);

			Response.Write(sData);
		}

		Response.End();
	}

	protected void lnkQuickTime_Command(object sender, CommandEventArgs e) {
		using (CastMovie oMovie = new CastMovie()) {
			if (oMovie.GetOne(decimal.Parse(e.CommandArgument.ToString()))) {
				string sRoot = ConfigurationManager.AppSettings["Root"];
				string sURL = "http://" + Request.Url.Authority + sRoot + ViCommConst.WEB_MOVIE_PATH + "/" + oMovie.siteCd + string.Format("/operator/{0}/{1}{2}", oMovie.loginId, iBridUtil.addZero(oMovie.movieSeq.ToString(), ViCommConst.OBJECT_NM_LENGTH), ViCommConst.MOVIE_FOODER);
				string sScripts = string.Format("<script language=\"javascript\">window.open('{0}','','width=300,height=300,resizable=no,directories=no,scrollbars=no' , false);</script>", sURL);
				ClientScript.RegisterStartupScript(Page.GetType(), "OpenNewWindow", sScripts);
			}
		}
	}

	private void GetList() {
		grdMovie.DataSourceID = "dsCastMovie";
		grdMovie.PageIndex = 0;
		grdMovie.DataBind();
		pnlCount.DataBind();
	}

	protected void dsCastMovie_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = "0";
		e.InputParameters[2] = "0";
		e.InputParameters[3] = ViCommConst.ATTACHED_PROFILE.ToString();
		e.InputParameters[4] = string.IsNullOrEmpty(this.txtUploadDayFrom.Text.Replace("/", string.Empty)) ? null : this.txtUploadDayFrom.Text;
		e.InputParameters[5] = string.IsNullOrEmpty(this.txtUploadDayTo.Text.Replace("/", string.Empty)) ? null : this.txtUploadDayTo.Text;
		e.InputParameters[6] = this.txtLoginId.Text.TrimEnd();
	}

	protected void btnBbsMovieUpdate_OnCommand(object sender,CommandEventArgs e) {
		string[] sArgs = e.CommandArgument.ToString().Split(',');
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_CAST_MOVIE_STATUS");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,sArgs[0]);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,sArgs[1]);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,sArgs[2]);
			db.ProcedureInParm("pMOVIE_SEQ",DbSession.DbType.VARCHAR2,sArgs[3]);
			db.ProcedureInParm("pMOVIE_TYPE",DbSession.DbType.NUMBER,ViCommConst.ATTACHED_PROFILE);
			db.ProcedureInParm("pOBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,0);
			db.ProcedureInParm("pOBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER,1);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		bool bTxNotPublicNotice;
		using (ManageCompany oManageCompany = new ManageCompany()) {
			bTxNotPublicNotice = oManageCompany.IsAvailableService(ViCommConst.RELEASE_CAST_OBJ_NON_PUBLIC_INFO,2);
		}

		if (bTxNotPublicNotice) {
			Server.Transfer(string.Format("./TxCastObjNonPublicMail.aspx?site={0}&userseq={1}&usercharNo={2}&mailtype={3}&return={4}",sArgs[0],sArgs[1],sArgs[2],ViCommConst.MAIL_TP_CAST_PROF_MOVIE_NP,"ProfileMovieOpenList"));
		} else {
			this.grdMovie.DataBind();
		}
	}
}
