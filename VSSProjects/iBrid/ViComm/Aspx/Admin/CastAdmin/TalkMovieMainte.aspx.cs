﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 会話動画認証
--	Progaram ID		: TalkMovieMainte
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater		Update Explain
  2010/07/16	Koyanagi	動画ｽﾃｰﾀｽ「非公開」に対応

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Cast_TalkMovieMainte:System.Web.UI.Page {
	private const int MAX_PART = 5;

	protected string SeekSiteCd {
		get {
			return this.ViewState["SeekSiteCd"] as string;
		}
		set {
			this.ViewState["SeekSiteCd"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		lblSiteCd.Text = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		lblUserSeq.Text = iBridUtil.GetStringValue(Request.QueryString["userseq"]);
		lblUserCharNo.Text = iBridUtil.GetStringValue(Request.QueryString["usercharno"]);
		ViewState["SITE_CD"] = lblSiteCd.Text;
		ViewState["USER_SEQ"] = lblUserSeq.Text;
		ViewState["MOVIE_SEQ"] = iBridUtil.GetStringValue(Request.QueryString["movieseq"]);
		ViewState["RETURN"] = iBridUtil.GetStringValue(Request.QueryString["return"]);
		ViewState["OBJ_NOT_PUBLISH_FLAG"] = iBridUtil.GetStringValue(Request.QueryString["closed"]);
		SeekSiteCd = iBridUtil.GetStringValue(Request.QueryString["seeksitecd"]);

		if (GetMultiCharFlag(lblSiteCd.Text)) {
			lblHeader.Text = "ｻｲﾄ／SEQ／ｷｬﾗｸﾀｰNo.";
			lblUserCharNo.Text = " - " + lblUserCharNo.Text;
			lblUserCharNo.Visible = true;
		} else {
			lblHeader.Text = "ｻｲﾄ／SEQ.";
			lblUserCharNo.Visible = false;
		}

		using (Cast oCast = new Cast()) {
			string sName = "",sId = "";
			oCast.GetValue(lblUserSeq.Text,"CAST_NM",ref sName);
			oCast.GetValue(lblUserSeq.Text,"LOGIN_ID",ref sId);
			lblCastNm.Text = sName;
			lblLoginId.Text = sId;
		}
		ClearField();
		DataBind();
		GetData();
	}

	private void ClearField() {
		txtChargePoint.Text = "";
		txtHtmlDoc.Text = "";
		txtMovieTitle.Text = "";
		lstAuthType.SelectedIndex = 0;
		chkPickupFlag.Checked = false;
		lblRecordingSec.Text = "";
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		ReturnToCall();
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("TALK_MOVIE_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,ViewState["SITE_CD"]);
			db.ProcedureInParm("PMOVIE_SEQ",DbSession.DbType.NUMBER,decimal.Parse(ViewState["MOVIE_SEQ"].ToString()));
			db.ProcedureOutParm("PIVP_FILE_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMOVIE_TITLE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHTML_DOC",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCHARGE_POINT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PUPLOAD_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPICKUP_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("RECORDING_SEC",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);

			db.ProcedureOutArrayParm("PPART_START_SEC",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PPART_PLAY_SEC",DbSession.DbType.VARCHAR2,100);
			db.ProcedureOutArrayParm("PPART_HTML_DOC",DbSession.DbType.VARCHAR2,100);

			db.ProcedureOutParm("PPART_RECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtMovieTitle.Text = db.GetStringValue("PMOVIE_TITLE");
				txtHtmlDoc.Text = db.GetStringValue("PHTML_DOC");
				txtChargePoint.Text = db.GetStringValue("PCHARGE_POINT");
				lblRecordingSec.Text = db.GetStringValue("RECORDING_SEC") +"秒";
				string sUnAtuthFlag = db.GetStringValue("POBJ_NOT_APPROVE_FLAG");
				string sNotPublishFlag = db.GetStringValue("POBJ_NOT_PUBLISH_FLAG");
				if (sUnAtuthFlag.Equals("0")) {
					if (sNotPublishFlag.Equals("0")) {
						lstAuthType.SelectedValue = ViCommConst.MOVIE_APPLY_OK;
					} else if (sNotPublishFlag.Equals("1")) {
						lstAuthType.SelectedValue = ViCommConst.MOVIE_APPLY_NG;
					}
				} else if (sUnAtuthFlag.Equals("1")) {
					lstAuthType.SelectedValue = ViCommConst.MOVIE_APPLY_WAIT;
				}
				chkPickupFlag.Checked = (db.GetIntValue("PPICKUP_FLAG") == 1);
			} else {
				ClearField();
			}


			for (int i = 0;i < MAX_PART;i++) {
				TextBox txtStartPosSec = (TextBox)plcHolder.FindControl(string.Format("txtStartPosSec{0}",i)) as TextBox;
				TextBox txtPlaySec = (TextBox)plcHolder.FindControl(string.Format("txtPlaySec{0}",i)) as TextBox;
				TextBox txtPartHtml = (TextBox)plcHolder.FindControl(string.Format("txtPartHtml{0}",i)) as TextBox;

				if (i < db.GetIntValue("PPART_RECORD_COUNT")) {
					txtStartPosSec.Text = db.GetArryStringValue("PPART_START_SEC",i);
					txtPlaySec.Text = db.GetArryStringValue("PPART_PLAY_SEC",i);
					txtPartHtml.Text = db.GetArryStringValue("PPART_HTML_DOC",i);
				} else {
					txtStartPosSec.Text = "";
					txtPlaySec.Text = "";
					txtPartHtml.Text = "";
				}
			}
		}
		pnlDtl.Visible = true;
	}

	private void UpdateData(int pDelFlag) {
		int iNotApproveFlag = 0;
		int iPickupFlag = 0;
		int iDelFlag = 0;
		int iNotPublishFlag = 0;

		if (pDelFlag == 0) {
			switch (lstAuthType.SelectedValue) {
				case ViCommConst.MOVIE_APPLY_WAIT:	// 認証待ち
					iDelFlag = 0;
					iNotApproveFlag = 1;
					iNotPublishFlag = 0;
					break;

				case ViCommConst.MOVIE_APPLY_OK:	// 公開
					iDelFlag = 0;
					iNotApproveFlag = 0;
					iNotPublishFlag = 0;
					break;

				case ViCommConst.MOVIE_APPLY_REMOVE: // 削除
					iNotApproveFlag = 0;
					iDelFlag = 1;
					iNotPublishFlag = 0;
					break;

				case ViCommConst.MOVIE_APPLY_NG:	// 非公開
					iNotApproveFlag = 0;
					iDelFlag = 0;
					iNotPublishFlag = 1;
					break;
			}
		} else {
			iDelFlag = 1;
		}

		if (chkPickupFlag.Checked) {
			iPickupFlag = 1;
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("TALK_MOVIE_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,ViewState["SITE_CD"]);
			db.ProcedureInParm("PMOVIE_SEQ",DbSession.DbType.NUMBER,decimal.Parse(ViewState["MOVIE_SEQ"].ToString()));
			db.ProcedureInParm("PMOVIE_TITLE",DbSession.DbType.VARCHAR2,txtMovieTitle.Text);
			db.ProcedureInParm("PHTML_DOC",DbSession.DbType.VARCHAR2,txtHtmlDoc.Text);
			db.ProcedureInParm("PCHARGE_POINT",DbSession.DbType.NUMBER,decimal.Parse(txtChargePoint.Text));
			db.ProcedureInParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,iNotApproveFlag);
			db.ProcedureInParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER,iNotPublishFlag);
			db.ProcedureInParm("PPICKUP_FLAG",DbSession.DbType.NUMBER,iPickupFlag);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,iDelFlag);

			string[] sStartPosSec = new string[MAX_PART];
			string[] sPlaySec = new string[MAX_PART];
			string[] sPartHtml = new string[MAX_PART];

			for (int i = 0;i < MAX_PART;i++) {
				TextBox txtStartPosSec = (TextBox)plcHolder.FindControl(string.Format("txtStartPosSec{0}",i)) as TextBox;
				TextBox txtPlaySec = (TextBox)plcHolder.FindControl(string.Format("txtPlaySec{0}",i)) as TextBox;
				TextBox txtPartHtml = (TextBox)plcHolder.FindControl(string.Format("txtPartHtml{0}",i)) as TextBox;
				sStartPosSec[i] = txtStartPosSec.Text;
				sPlaySec[i] = txtPlaySec.Text;
				sPartHtml[i] = txtPartHtml.Text;
			}
			db.ProcedureInArrayParm("PPART_START_SEC",DbSession.DbType.VARCHAR2,MAX_PART,sStartPosSec);
			db.ProcedureInArrayParm("PPART_PLAY_SEC",DbSession.DbType.VARCHAR2,MAX_PART,sPlaySec);
			db.ProcedureInArrayParm("PPART_HTML_DOC",DbSession.DbType.VARCHAR2,MAX_PART,sPartHtml);

			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		ReturnToCall();
	}

	private void ReturnToCall() {
		if (ViewState["RETURN"].ToString().Equals("TalkMovieCheckList")) {
			Server.Transfer(string.Format("TalkMovieCheckList.aspx?moviesite={0}&closed={1}&seeksitecd={2}",ViewState["SITE_CD"].ToString(),ViewState["OBJ_NOT_PUBLISH_FLAG"].ToString(),SeekSiteCd));

		} else if (ViewState["RETURN"].ToString().Equals("TalkMovieOpenList")) {
			Server.Transfer(string.Format("TalkMovieOpenList.aspx?moviesite={0}",ViewState["SITE_CD"].ToString()));

		//} else if (ViewState["RETURN"].ToString().Equals("TalkMovieClosedList")) {
		//    Server.Transfer(string.Format("TalkMovieClosedList.aspx?moviesite={0}",ViewState["SITE_CD"].ToString()));

		} else {
			Server.Transfer(string.Format("../Cast/CastView.aspx?loginid={0}&talkmoviesite={1}",lblLoginId.Text,ViewState["SITE_CD"].ToString()));
		}
	}

	private bool GetMultiCharFlag(string pSiteCd) {
		bool bMultiCharFlag = false;
		using (Site oSite = new Site()) {
			if (oSite.GetMultiCharFlag(pSiteCd)) {
				bMultiCharFlag = true;
			}
		}
		return bMultiCharFlag;
	}
}
