<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TalkMovieCheckList.aspx.cs" Inherits="Cast_TalkMovieCheckList"
	Title="認証待ち会話ムービー" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="認証待ち会話ムービー"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 740px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle2">
							アップロード日付
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtUploadDayFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RangeValidator ID="vdeUploadDayFrom" runat="server" ControlToValidate="txtUploadDayFrom" ErrorMessage="アップロード日付Fromを正しく入力して下さい。" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							〜&nbsp;
							<asp:TextBox ID="txtUploadDayTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RangeValidator ID="vdeUploadDayTo" runat="server" ControlToValidate="txtUploadDayTo" ErrorMessage="アップロード日付Toを正しく入力して下さい。" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							非公開動画も出力
						</td>
						<td class="tdDataStyle" colspan="3">
							<asp:CheckBox ID="chkClosed" runat="server" />
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="True" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
				<br clear="left" />
				<asp:Label ID="Label1" runat="server" Text="※ 動画の確認を行う場合はＩＶＰサーバーのMovieフォルダーをＭドライブに接続してください。" Font-Size="Small" ForeColor="Gray" Font-Bold="True"></asp:Label>
			</asp:Panel>
		</fieldset>
		<fieldset>
			<legend>[認証待ち会話ムービー一覧]</legend>
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="520px">
				<asp:GridView ID="grdMovie" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False" DataSourceID="dsTalkMovie" SkinID="GridViewColor" OnRowDataBound="grdMovie_RowDataBound">
					<Columns>
						<asp:BoundField DataField="SITE_NM" HeaderText="サイト"></asp:BoundField>
						<asp:TemplateField HeaderText="氏名">
							<ItemTemplate>
								<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# string.Format("../Cast/CastView.aspx?loginid={0}",Eval("LOGIN_ID")) %>' Text='<%# Eval("CAST_NM") %>'></asp:HyperLink>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField DataField="HANDLE_NM" HeaderText="ハンドル名"></asp:BoundField>
						<asp:BoundField DataField="IVP_FILE_NM" HeaderText="ファイル名"></asp:BoundField>
						<asp:BoundField DataField="RECORDING_SEC" HeaderText="秒数">
							<ItemStyle HorizontalAlign="Right" />
						</asp:BoundField>
						<asp:BoundField DataField="UPLOAD_DATE" HeaderText="アップロード日時" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False" SortExpression="UPLOAD_DATE">
							<ItemStyle HorizontalAlign="Center" />
						</asp:BoundField>
						<asp:TemplateField HeaderText="確認">
							<ItemStyle HorizontalAlign="Center" />
							<ItemTemplate>
								<asp:LinkButton ID="lnkQuickTime" runat="server" CommandArgument='<%# Eval("PLAY_FILE_NM") %>' OnCommand="lnkQuickTime_Command" Text="確認"></asp:LinkButton>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="設定">
							<ItemStyle HorizontalAlign="Center" />
							<ItemTemplate>
								<asp:LinkButton ID="lnkMainte" runat="server" CommandArgument='<%# string.Format("{0}:{1}:{2}:{3}",Eval("SITE_CD"),Eval("USER_SEQ"),Eval("MOVIE_SEQ"),"TalkMovieCheckList") %>'
									OnCommand="lnkMainte_Command" Text='<%# Eval("NOT_APPROVE_MARK_ADMIN") %>'></asp:LinkButton>
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
					<PagerSettings Mode="NumericFirstLast" />
				</asp:GridView>
			</asp:Panel>
			&nbsp;
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br />
				<a class="reccount">Current viewing page
					<%=grdMovie.PageIndex + 1%>
					of
					<%=grdMovie.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsTalkMovie" runat="server" SelectMethod="GetApproveManagePageCollection" TypeName="TalkMovie" SelectCountMethod="GetApproveManagePageCount"
		EnablePaging="True" OnSelected="dsTalkMovie_Selected" OnSelecting="dsTalkMovie_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pNotApproveFlag" Type="String" />
			<asp:Parameter Name="pNotPublishFlag" Type="String" />
			<asp:Parameter Name="pUploadDateFrom" Type="String" />
			<asp:Parameter Name="pUploadDateTo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:MaskedEditExtender ID="mskUploadDayFrom" runat="server" TargetControlID="txtUploadDayFrom" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay"
		ClearMaskOnLostFocus="true">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:ValidatorCalloutExtender ID="vceUploadDayFrom" runat="Server" TargetControlID="vdeUploadDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskUploadDayTo" runat="server" TargetControlID="txtUploadDayTo" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay"
		ClearMaskOnLostFocus="true">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:ValidatorCalloutExtender ID="vceUploadDayTo" runat="Server" TargetControlID="vdeUploadDayTo" HighlightCssClass="validatorCallout" />
</asp:Content>
