﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="StockPicMainte.aspx.cs" Inherits="CastAdmin_StockPicMainte" Title="ストック写真認証" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="ストック写真認証"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[写真認証]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					$NO_TRANS_START;
					<table border="0" style="width: 640px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle">
								ｻｲﾄ／SEQ／ｷｬﾗｸﾀｰNo.
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblSiteCd" runat="server" Text="Label"></asp:Label>-
								<asp:Label ID="lblLoginId" runat="server" Text="Label"></asp:Label>-
								<asp:Label ID="lblUserSeq" runat="server" Text="Label" Visible="false"></asp:Label>
								<asp:Label ID="lblUserCharNo" runat="server" Text="Label"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								<%= DisplayWordUtil.Replace("出演者名.") %>
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblUserNm" runat="server" Text="Label"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle2">
								写真ステータス
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstAuthType" runat="server" DataSourceID="dsAuthType" DataTextField="CODE_NM" DataValueField="CODE" Width="118px" OnDataBound="lstAuthType_DataBound">
								</asp:DropDownList>
							</td>
						</tr>
					</table>
					$NO_TRANS_END;
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
					<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsAuthType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="54" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
