<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SameTelInquiry.aspx.cs" Inherits="Cast_SameTelInquiry" Title="同一電話番号検索"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="同一電話番号検索"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							端末種別
						</td>
						<td class="tdDataStyle">
							<asp:RadioButton ID="rdoTerminalMobile" runat="server" Text="携帯電話" GroupName="TerminalType" />
							<asp:RadioButton ID="rdoTerminalPC" runat="server" Text="PC" GroupName="TerminalType" />
						</td>
						<td class="tdHeaderStyle">
							端末識別
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtTerminalId" runat="server"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							オンライン状態
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstOnlineStatus" runat="server" DataSourceID="dsOnline" DataTextField="CODE_NM" DataValueField="CODE" Width="180px">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle2">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
							</asp:DropDownList>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[同一端末識別一覧]</legend>
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="580px">
					<asp:GridView ID="grdCast" runat="server" AutoGenerateColumns="False" DataSourceID="dsCastCharacter" SkinID="GridView" OnDataBound="grdCast_DataBound"
						OnRowCreated="grdCast_RowCreated">
						<Columns>
							<asp:TemplateField HeaderText="端末識別">
								<ItemTemplate>
									<asp:HyperLink ID="HyperLink3" runat="server" Text='<%# Eval("TERM_ID") %>' NavigateUrl='<%# string.Format("~/Status/PointLogByDetail.aspx?sitecd={0}&dayfrom={1}&dayto={2}&tel={3}","",GetFromDay(),GetToDay(),Eval("TERM_ID")) %>'></asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="LOGIN_ID" HeaderText="ID"></asp:BoundField>
							<asp:BoundField DataField="PRODUCTION_NM" HeaderText="プロダクション名">
								<ItemStyle Width="110px" />
							</asp:BoundField>
							<asp:BoundField DataField="MANAGER_NM" HeaderText="担当名">
								<ItemStyle Width="90px" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="氏名">
								<ItemTemplate>
									<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# string.Format("../Cast/CastView.aspx?loginid={0}&return={1}",Eval("LOGIN_ID"),"SameTelInquiry.aspx") %>'
										Text='<%# Eval("CAST_NM") %>'></asp:HyperLink>
									<br />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="USER_STATUS_NM" HeaderText="認 証">
								<ItemStyle HorizontalAlign="Center" Width="48px" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="M1">
								<ItemTemplate>
									<asp:Image ID="imgSysOnlineImg1" runat="server" ImageUrl='<%# Eval("SYS_ONLINE_IMG_PATH1","../{0}") %>' Visible='<%#Eval("SYS_ONLINE_IMG_PATH1").ToString().Equals("")==false%>' />
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="12px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="M2">
								<ItemTemplate>
									<asp:Image ID="imgSysOnlineImg2" runat="server" ImageUrl='<%# Eval("SYS_ONLINE_IMG_PATH2","../{0}") %>' Visible='<%#Eval("SYS_ONLINE_IMG_PATH2").ToString().Equals("")==false%>' />
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="12px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="M3">
								<ItemTemplate>
									<asp:Image ID="imgSysOnlineImg3" runat="server" ImageUrl='<%# Eval("SYS_ONLINE_IMG_PATH3","../{0}") %>' Visible='<%#Eval("SYS_ONLINE_IMG_PATH3").ToString().Equals("")==false%>' />
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="12px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="M4">
								<ItemTemplate>
									<asp:Image ID="imgSysOnlineImg4" runat="server" ImageUrl='<%# Eval("SYS_ONLINE_IMG_PATH4","../{0}") %>' Visible='<%#Eval("SYS_ONLINE_IMG_PATH4").ToString().Equals("")==false%>' />
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="12px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="M5">
								<ItemTemplate>
									<asp:Image ID="imgSysOnlineImg5" runat="server" ImageUrl='<%# Eval("SYS_ONLINE_IMG_PATH5","../{0}") %>' Visible='<%#Eval("SYS_ONLINE_IMG_PATH5").ToString().Equals("")==false%>' />
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="12px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="M6">
								<ItemTemplate>
									<asp:Image ID="imgSysOnlineImg6" runat="server" ImageUrl='<%# Eval("SYS_ONLINE_IMG_PATH6","../{0}") %>' Visible='<%#Eval("SYS_ONLINE_IMG_PATH6").ToString().Equals("")==false%>' />
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="12px" />
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsCast" runat="server" SelectMethod="GetSamePhoneUser" TypeName="Cast">
		<SelectParameters>
			<asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstOnlineStatus" Name="pOnLine" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="txtTerminalId" Name="pTerminalId" PropertyName="Text" Type="String" />
			<asp:ControlParameter ControlID="rdoTerminalPC" Name="pPC" PropertyName="Checked" Type="Boolean" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsOnLine" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="52" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
