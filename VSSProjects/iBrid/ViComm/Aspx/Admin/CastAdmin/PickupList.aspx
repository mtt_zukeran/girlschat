<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PickupList.aspx.cs" Inherits="Cast_PickupList" Title="ピックアップ一覧"
	ValidateRequest="false" %>
<%@ Import Namespace="ViComm" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="ピックアップ一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle2" style="height: 24px">
							サイト
						</td>
						<td class="tdDataStyle" style="height: 24px">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px" AutoPostBack="True"
								OnSelectedIndexChanged="lstSiteCd_SelectedIndexChanged">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle2" style="height: 24px">
							カテゴリ
						</td>
						<td class="tdDataStyle" style="height: 24px">
							<asp:DropDownList ID="lstActCategorySeq" runat="server" DataSourceID="dsActCategory" DataTextField="ACT_CATEGORY_NM" DataValueField="ACT_CATEGORY_SEQ"
								Width="180px">
							</asp:DropDownList>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlList">
			<fieldset>
				<legend>
					<%= DisplayWordUtil.Replace("[出演者一覧]") %>
				</legend>
				<div align="center">
					<asp:Label ID="lblPickupTitle" runat="server" Text="ピックアップタイトル：" Font-Bold="True" Font-Size="Medium" ForeColor="#FF8000"></asp:Label>
				</div>
				<br />
				<div align="center">
					<asp:Label ID="lblNoCheckError" runat="server" Text="ピックアップ出演者が一人も選択されていません。" Font-Bold="True" Font-Size="Small" ForeColor="Red" Visible="False"></asp:Label>
				</div>
				<asp:Button ID="btnSubmitCreate1" runat="server" Text="ピックアップ作成" OnClick="btnSubmitCreate_Click" CssClass="seekbutton" />
				<asp:Button ID="btnSubmitClear" runat="server" Text="現在のピックアップをクリア" OnClick="btnSubmitClear_Click" CssClass="seekbutton" />
				<br />
				&nbsp;
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="500px">
					<asp:GridView ID="grdCast" runat="server" AutoGenerateColumns="False" DataSourceID="dsCastCharacter" AllowSorting="True" SkinID="GridViewColor">
						<Columns>
							<asp:TemplateField HeaderText="選択">
								<ItemTemplate>
									<asp:CheckBox ID="chkPickup" runat="server" />
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" />
							</asp:TemplateField>
							<asp:BoundField DataField="LOGIN_ID" HeaderText="ID" SortExpression="LOGIN_ID">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
							<asp:BoundField DataField="USER_SEQ" HeaderText="SEQ" SortExpression="USER_SEQ" DataFormatString="{0:D5}">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="ハンドル名" SortExpression="HANDLE_NM">
								<ItemTemplate>
									<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# string.Format("~/Cast/CastView.aspx?loginid={0}",Eval("LOGIN_ID")) %>' Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="カテゴリ" SortExpression="ACT_CATEGORY_SEQ">
								<ItemTemplate>
									<asp:Label ID="lblActCategory" runat="server" Text='<%# Eval("ACT_CATEGORY_NM") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="COMMENT_PICKUP" HeaderText="コメント" SortExpression="COMMENT_PICKUP"></asp:BoundField>
							<asp:BoundField DataField="PICKUP_MARK" HeaderText="選択" SortExpression="PICKUP_FLAG">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
						</Columns>
					</asp:GridView>
				</asp:Panel>
				<br />
				<asp:Button ID="btnSubmitCreate2" runat="server" Text="ピックアップ作成" OnClick="btnSubmitCreate_Click" />
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastCharacter" runat="server" SelectMethod="GetPickupList" TypeName="CastCharacter">
		<SelectParameters>
			<asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstActcategorySeq" Name="pActCategorySeq" PropertyName="SelectedValue" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsActCategory" runat="server" SelectMethod="GetList" TypeName="ActCategory" OnSelecting="dsActCategory_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnSubmitClear" ConfirmText="現在のピックアップをクリアしますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
