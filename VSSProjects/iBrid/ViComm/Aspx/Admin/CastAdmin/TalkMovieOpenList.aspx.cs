﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 公開中会話動画一覧
--	Progaram ID		: TalkMovieOpenList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater				Update Explain
  2009/09/03	i-Brid(Y.Inoue)		担当サイト対応  2010/06/29	伊藤和明			検索項目に日付を追加
  2010/07/16	Koyanagi			検索項目に非公開ﾌﾗｸﾞを追加

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Cast_TalkMovieOpenList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
			ViewState["SiteCd"] = iBridUtil.GetStringValue(Request.QueryString["moviesite"]);
			if (iBridUtil.GetStringValue(ViewState["SiteCd"]).Equals("") == false) {
				lstSiteCd.SelectedValue = ViewState["SiteCd"].ToString();
			} else if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
				lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}
			GetList();
		}
	}

	protected void dsTalkMovie_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void FirstLoad() {
		grdMovie.PageSize = 100;
		grdMovie.DataSourceID = "";
		DataBind();
		if (Session["SiteCd"].ToString().Equals("")) {
			lstSiteCd.Items.Insert(0,new ListItem("",""));
		}
		lstSiteCd.DataSourceID = "";
	}

	private void InitPage() {
		grdMovie.DataSourceID = "";
		DataBind();
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		if(!this.IsValid)return;
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.Response.Redirect(this.Request.Url.PathAndQuery);
	}

	protected void lnkQuickTime_Command(object sender,CommandEventArgs e) {
		string sURL = string.Format("file:///M:/{0}",e.CommandArgument.ToString());
		string sScripts = string.Format("<script language=\"javascript\">window.open('{0}','','width=300,height=300,resizable=no,directories=no,scrollbars=no' , false);</script>",sURL);
		ClientScript.RegisterStartupScript(Page.GetType(),"OpenNewWindow",sScripts);
	}

	private void GetList() {
		grdMovie.DataSourceID = "dsTalkMovie";
		grdMovie.PageIndex = 0;
		grdMovie.DataBind();
		pnlCount.DataBind();
	}

	protected void dsTalkMovie_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = "0";
		e.InputParameters[2] = "0";
		e.InputParameters[3] = string.IsNullOrEmpty(this.txtUploadDayFrom.Text.Replace("/",string.Empty)) ? null : this.txtUploadDayFrom.Text;
		e.InputParameters[4] = string.IsNullOrEmpty(this.txtUploadDayTo.Text.Replace("/", string.Empty)) ? null : this.txtUploadDayTo.Text;				
	}
}
