﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 公開掲示板動画一覧
--	Progaram ID		: BbsMovieOpenList
--
--  Creation Date	: 2009.08.26
--  Creater			: i-Brid(Y.Inoue)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater				Update Explain
  2009/09/03	i-Brid(Y.Inoue)		担当サイト対応  2010/06/29	伊藤和明			検索項目に日付を追加
  2010/07/16	Koyanagi			検索項目に非公開ﾌﾗｸﾞを追加

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using iBridCommLib;
using ViComm;

public partial class Cast_BbsMovieOpenList : System.Web.UI.Page {
	private string recCount = "";
	private Stream filter;

	protected string MovieTypeItemNo {
		get {
			return this.ViewState["MovieTypeItemNo"] as string;
		}
		set {
			this.ViewState["MovieTypeItemNo"] = value;
		}
	}
	protected void Page_Load(object sender, EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			FirstLoad();
			InitPage();

			ViewState["SiteCd"] = iBridUtil.GetStringValue(Request.QueryString["bbsmoviesite"]);
		
			if (iBridUtil.GetStringValue(ViewState["SiteCd"]).Equals("") == false) {
				lstSiteCd.SelectedValue = ViewState["SiteCd"].ToString();
			} else if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
				lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}
			
			string sMovieTypeSeq = string.Empty;
			string sMovieTypeItemNo = string.Empty;
			if (!iBridUtil.GetStringValue(Request.QueryString["movietype_itemno"]).Equals(string.Empty)) {
				sMovieTypeItemNo = iBridUtil.GetStringValue(Request.QueryString["movietype_itemno"]);
				this.MovieTypeItemNo = sMovieTypeItemNo;
				
				if (sMovieTypeItemNo.Equals(ViCommConst.CastMovieAttrTypeDef.PLAN_MOVIE_ITEM_NO)) {
					this.lblPgmTitle.Text = "公開中企画動画";
				}
				
				using (CastMovieAttrType oCastMovieAttrType = new CastMovieAttrType()) {
					using (DataSet ds = oCastMovieAttrType.GetOneByItemNo(lstSiteCd.SelectedValue,sMovieTypeItemNo)) {
						if (ds.Tables[0].Rows.Count > 0) {
							sMovieTypeSeq = iBridUtil.GetStringValue(ds.Tables[0].Rows[0]["CAST_MOVIE_ATTR_TYPE_SEQ"]);
						}
					}
				}
			}
			this.InitCastMovieAttrType(sMovieTypeSeq);
			this.InitCastMovieAttrTypeValue(string.Empty);

			
			GetList();
		}
	}

	protected void dsCastMovie_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void FirstLoad() {
		grdMovie.PageSize = 100;
		grdMovie.DataSourceID = "";
		DataBind();
		if (Session["SiteCd"].ToString().Equals("")) {
			lstSiteCd.Items.Insert(0, new ListItem("", ""));
		}
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		lstSiteCd.DataSourceID = "";
	}

	private void InitPage() {
		grdMovie.DataSourceID = "";
		DataBind();
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}


	protected void btnListSeek_Click(object sender, EventArgs e) {
		if (!this.IsValid) return;
		GetList();
	}

	protected void btnClear_Click(object sender, EventArgs e) {
		this.Response.Redirect(this.Request.Url.PathAndQuery);
	}

	protected void lnkQuickTime_Command(object sender, CommandEventArgs e) {
		using (CastMovie oMovie = new CastMovie()) {
			if (oMovie.GetOne(decimal.Parse(e.CommandArgument.ToString()))) {
				string sRoot = ConfigurationManager.AppSettings["Root"];
				string sURL = "http://" + Request.Url.Authority + sRoot + ViCommConst.WEB_MOVIE_PATH + "/" + oMovie.siteCd + string.Format("/operator/{0}/{1}{2}", oMovie.loginId, iBridUtil.addZero(oMovie.movieSeq.ToString(), ViCommConst.OBJECT_NM_LENGTH), ViCommConst.MOVIE_FOODER);
				string sScripts = string.Format("<script language=\"javascript\">window.open('{0}','','width=300,height=300,resizable=no,directories=no,scrollbars=no' , false);</script>", sURL);
				ClientScript.RegisterStartupScript(Page.GetType(), "OpenNewWindow", sScripts);
			}
		}
	}

	private void GetList() {
		grdMovie.DataSourceID = "dsCastMovie";
		grdMovie.PageIndex = 0;
		grdMovie.DataBind();
		pnlCount.DataBind();
	}

	protected void dsCastMovie_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = "0";
		e.InputParameters[2] = "0";
		e.InputParameters[3] = ViCommConst.ATTACHED_BBS.ToString();
		e.InputParameters[4] = string.IsNullOrEmpty(this.txtUploadDayFrom.Text.Replace("/", string.Empty)) ? null : this.txtUploadDayFrom.Text;
		e.InputParameters[5] = string.IsNullOrEmpty(this.txtUploadDayTo.Text.Replace("/", string.Empty)) ? null : this.txtUploadDayTo.Text;
		e.InputParameters[6] = lstCastMovieAttrType.SelectedValue;
		e.InputParameters[7] = lstCastMovieAttrTypeValue.SelectedValue;
		e.InputParameters[8] = this.txtLoginId.Text.TrimEnd();
	}


	protected void lstCastMovieAttrType_SelectedIndexChanged(object sender, EventArgs e) {
		this.InitCastMovieAttrTypeValue(string.Empty);
	}

	protected void dsCastMovieAttrType_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
	}

	protected void dsCastMovieAttrTypeValue_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = lstCastMovieAttrType.SelectedValue;
	}

	protected void InitCastMovieAttrType(string pCastMovieAttrType) {
		lstCastMovieAttrType.DataSourceID = "dsCastMovieAttrType";
		lstCastMovieAttrType.DataBind();
		lstCastMovieAttrType.Items.Insert(0, new ListItem(string.Empty, string.Empty));
		lstCastMovieAttrType.DataSourceID = string.Empty;
		if (pCastMovieAttrType.Equals(string.Empty) ||
			lstCastMovieAttrType.Items.FindByValue(pCastMovieAttrType) == null) {
			lstCastMovieAttrType.SelectedIndex = 0;
		} else {
			lstCastMovieAttrType.SelectedValue = pCastMovieAttrType;
		}
	}

	protected void InitCastMovieAttrTypeValue(string pCastMovieAttrTypeValue) {
		lstCastMovieAttrTypeValue.DataSourceID = "dsCastMovieAttrTypeValue";
		lstCastMovieAttrTypeValue.DataBind();
		lstCastMovieAttrTypeValue.Items.Insert(0, new ListItem(string.Empty, string.Empty));
		lstCastMovieAttrTypeValue.DataSourceID = string.Empty;

		if (pCastMovieAttrTypeValue.Equals(string.Empty) ||
			lstCastMovieAttrTypeValue.Items.FindByValue(pCastMovieAttrTypeValue) == null) {
			lstCastMovieAttrTypeValue.SelectedIndex = 0;
		} else {
			lstCastMovieAttrTypeValue.SelectedValue = pCastMovieAttrTypeValue;
		}
	}

	protected void lstSiteCd_SelectedIndexChanged(object sender, EventArgs e) {
		this.InitCastMovieAttrType(string.Empty);
		this.InitCastMovieAttrTypeValue(string.Empty);
	}

	protected void lnkMainte_Command(object sender,CommandEventArgs e) {
		string[] sKey = e.CommandArgument.ToString().Split(':');
		Response.Redirect(string.Format("../CastAdmin/BbsMovieMainte.aspx?sitecd={0}&userseq={1}&usercharno={2}&movieseq={3}&return={4}&movietype_itemno={5}",sKey[0],sKey[1],sKey[2],sKey[3],sKey[4],this.MovieTypeItemNo));
	}

	protected void btnBbsMovieUpdate_OnCommand(object sender,CommandEventArgs e) {
		string[] sArgs = e.CommandArgument.ToString().Split(',');
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("UPDATE_CAST_MOVIE_STATUS");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,sArgs[0]);
			db.ProcedureInParm("pUSER_SEQ",DbSession.DbType.VARCHAR2,sArgs[1]);
			db.ProcedureInParm("pUSER_CHAR_NO",DbSession.DbType.VARCHAR2,sArgs[2]);
			db.ProcedureInParm("pMOVIE_SEQ",DbSession.DbType.VARCHAR2,sArgs[3]);
			db.ProcedureInParm("pMOVIE_TYPE",DbSession.DbType.NUMBER,ViCommConst.ATTACHED_BBS);
			db.ProcedureInParm("pOBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,0);
			db.ProcedureInParm("pOBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER,1);
			db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		bool bTxNotPublicNotice;
		using (ManageCompany oManageCompany = new ManageCompany()) {
			bTxNotPublicNotice = oManageCompany.IsAvailableService(ViCommConst.RELEASE_CAST_OBJ_NON_PUBLIC_INFO,2);
		}

		if (bTxNotPublicNotice) {
			Server.Transfer(string.Format("../CastAdmin/TxCastObjNonPublicMail.aspx?site={0}&userseq={1}&usercharNo={2}&mailtype={3}&return={4}",sArgs[0],sArgs[1],sArgs[2],ViCommConst.MAIL_TP_CAST_BBS_MOVIE_NP,"BbsMovieOpenList"));
		} else {
			this.grdMovie.DataBind();
		}
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		Response.Filter = filter;
		Response.AddHeader("Content-Disposition",string.Format("attachment;filename=BBS_MOVIE_OPEN_{0}.CSV",lstSiteCd.SelectedValue));
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		DataSet ds;
		using (CastMovie oCastMovie = new CastMovie()) {
			ds = oCastMovie.GetApproveManagePageCollection(
					lstSiteCd.SelectedValue,
					ViCommConst.FLAG_OFF_STR,
					ViCommConst.FLAG_OFF_STR,
					ViCommConst.ATTACHED_BBS.ToString(),
					string.IsNullOrEmpty(this.txtUploadDayFrom.Text.Replace("/",string.Empty)) ? null : this.txtUploadDayFrom.Text,
					string.IsNullOrEmpty(this.txtUploadDayTo.Text.Replace("/",string.Empty)) ? null : this.txtUploadDayTo.Text,
					lstCastMovieAttrType.SelectedValue,
					lstCastMovieAttrTypeValue.SelectedValue,
					this.txtLoginId.Text.TrimEnd(),
					string.Empty,
					0,
					SysConst.DB_MAX_ROWS
				);
			SetCsvData(ds);
			Response.End();
		}
	}

	public void SetCsvData(DataSet pDataSet) {

		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		string sHeaderString = "ﾛｸﾞｲﾝID,ﾊﾝﾄﾞﾙ名,ｱｯﾌﾟﾛｰﾄﾞ日時,動画属性,動画属性値\r\n";
		Response.BinaryWrite(encoding.GetBytes(sHeaderString));

		string sDtl = "";

		DataRow dr = null;
		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];

			sDtl = string.Format("{0},{1},{2},{3},{4}",
							dr["LOGIN_ID"].ToString(),
							dr["HANDLE_NM"].ToString(),
							dr["UPLOAD_DATE"].ToString(),
							dr["CAST_MOVIE_ATTR_TYPE_NM"].ToString(),
							dr["CAST_MOVIE_ATTR_NM"].ToString()
							);
			sDtl += "\r\n";
			Response.BinaryWrite(encoding.GetBytes(sDtl));
		}
	}
}
