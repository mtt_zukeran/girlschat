﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ストック写真認証
--	Progaram ID		: StockPicMainte
--
--  Creation Date	: 2011.08.04
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater		Update Explain


-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class CastAdmin_StockPicMainte : System.Web.UI.Page {

	private string SiteCd {
		get { return iBridUtil.GetStringValue(this.ViewState["SiteCd"]); }
		set { this.ViewState["SiteCd"] = value; }
	}
	private string UserSeq {
		get { return iBridUtil.GetStringValue(this.ViewState["UserSeq"]); }
		set { this.ViewState["UserSeq"] = value; }
	}
	private string UserCharNo {
		get { return iBridUtil.GetStringValue(this.ViewState["UserCharNo"]); }
		set { this.ViewState["UserCharNo"] = value; }
	}
	private string PicSeq {
		get { return iBridUtil.GetStringValue(this.ViewState["PicSeq"]); }
		set { this.ViewState["PicSeq"] = value; }
	}
	private string PicTitle {
		get { return iBridUtil.GetStringValue(this.ViewState["PicTitle"]); }
		set { this.ViewState["PicTitle"] = value; }
	}
	private string PicDoc {
		get { return iBridUtil.GetStringValue(this.ViewState["PicDoc"]); }
		set { this.ViewState["PicDoc"] = value; }
	}
	private string ObjNotPublishFlag {
		get { return iBridUtil.GetStringValue(this.ViewState["ObjNotPublishFlag"]); }
		set { this.ViewState["ObjNotPublishFlag"] = value; }
	}
	private string Rowid {
		get { return iBridUtil.GetStringValue(this.ViewState["Rowid"]); }
		set { this.ViewState["Rowid"] = value; }
	}
	private string RevisionNo {
		get { return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]); }
		set { this.ViewState["RevisionNo"] = value; }
	}
	private string ReturnPage {
		get { return iBridUtil.GetStringValue(this.ViewState["ReturnPage"]); }
		set { this.ViewState["ReturnPage"] = value; }
	}
	protected string PrevNonPublicFlag {
		get { return this.ViewState["PrevNonPublicFlag"] as string; }
		set { this.ViewState["PrevNonPublicFlag"] = value; }
	}

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		this.SiteCd = Request.QueryString["sitecd"];
		this.UserSeq = Request.QueryString["userseq"];
		this.UserCharNo = Request.QueryString["usercharno"];
		this.PicSeq = Request.QueryString["picseq"];
		this.ReturnPage = Request.QueryString["return"];
		this.ObjNotPublishFlag = Request.QueryString["closed"];

		lblSiteCd.Text = this.SiteCd;
		lblUserSeq.Text = this.UserSeq;
		lblUserCharNo.Text = this.UserCharNo;

		using (Cast oCast = new Cast()) {
			string sName = string.Empty,sId = string.Empty;
			oCast.GetValue(lblUserSeq.Text,"CAST_NM",ref sName);
			oCast.GetValue(lblUserSeq.Text,"LOGIN_ID",ref sId);
			lblUserNm.Text = sName;
			lblLoginId.Text = sId;
		}
		ClearField();
		DataBind();
		GetData();
	}

	private void ClearField() {
		lstAuthType.SelectedIndex = 0;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData();
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		ReturnToCall();
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {

			db.PrepareProcedure("CAST_PIC_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,this.UserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,this.UserCharNo);
			db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.VARCHAR2,this.PicSeq);
			db.ProcedureOutParm("PPIC_TITLE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPIC_DOC",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PUPLOAD_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPIC_TYPE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_PIC_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_PIC_ATTR_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			this.RevisionNo = db.GetStringValue("PREVISION_NO");
			this.Rowid = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				this.PicTitle = db.GetStringValue("PPIC_TITLE");
				this.PicDoc = db.GetStringValue("PPIC_DOC");
				string sUnAtuthFlag = db.GetStringValue("POBJ_NOT_APPROVE_FLAG");
				string sNotPublishFlag = db.GetStringValue("POBJ_NOT_PUBLISH_FLAG");
				if (sUnAtuthFlag.Equals("0")) {
					if (sNotPublishFlag.Equals("0")) {
						lstAuthType.SelectedValue = ViCommConst.MOVIE_APPLY_OK;
					} else if (sNotPublishFlag.Equals("1")) {
						lstAuthType.SelectedValue = ViCommConst.MOVIE_APPLY_NG;
					}
				} else if (sUnAtuthFlag.Equals("1")) {
					lstAuthType.SelectedValue = ViCommConst.MOVIE_APPLY_WAIT;
				}
			} else {
				ClearField();
			}
		}
		pnlDtl.Visible = true;
		this.PrevNonPublicFlag = lstAuthType.SelectedValue;
	}

	private void UpdateData() {
		int iNotApproveFlag = 0;
		int iNotPublishFlag = 0;

		switch (lstAuthType.SelectedValue) {
			case ViCommConst.MOVIE_APPLY_WAIT:	// 認証待ち
				iNotApproveFlag = 1;
				iNotPublishFlag = 0;
				break;

			case ViCommConst.MOVIE_APPLY_OK:	// 公開
				iNotApproveFlag = 0;
				iNotPublishFlag = 0;
				break;

			case ViCommConst.MOVIE_APPLY_REMOVE: // 削除
				throw new ArgumentException("ストック画像の削除はできません。");

			case ViCommConst.MOVIE_APPLY_NG:	// 非公開
				iNotApproveFlag = 0;
				iNotPublishFlag = 1;
				break;
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_PIC_MAINTE");
			db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.VARCHAR2,this.PicSeq);
			db.ProcedureInParm("PPIC_TITLE",DbSession.DbType.VARCHAR2,this.PicTitle);
			db.ProcedureInParm("PPIC_DOC",DbSession.DbType.VARCHAR2,this.PicDoc);
			db.ProcedureInParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,iNotApproveFlag);
			db.ProcedureInParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER,iNotPublishFlag);
			db.ProcedureInParm("PPIC_TYPE",DbSession.DbType.NUMBER,ViCommConst.ATTACHED_MAIL);
			db.ProcedureInParm("PCAST_PIC_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,ViCommConst.DEFAULT_CAST_PIC_ATTR_TYPE_SEQ);
			db.ProcedureInParm("PCAST_PIC_ATTR_SEQ",DbSession.DbType.VARCHAR2,ViCommConst.DEFAULT_CAST_PIC_ATTR_SEQ);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,this.Rowid);
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(this.RevisionNo));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF_STR);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		bool bTxNotPublicNotice;
		using (ManageCompany oManageCompany = new ManageCompany()) {
			bTxNotPublicNotice = oManageCompany.IsAvailableService(ViCommConst.RELEASE_CAST_OBJ_NON_PUBLIC_INFO,2);
		}

		if (bTxNotPublicNotice && !this.PrevNonPublicFlag.Equals(ViCommConst.MOVIE_APPLY_NG) && this.lstAuthType.SelectedValue.Equals(ViCommConst.MOVIE_APPLY_NG)) {
			Server.Transfer(string.Format("./TxCastObjNonPublicMail.aspx?site={0}&userseq={1}&usercharNo={2}&mailtype={3}&return={4}",this.SiteCd,this.UserSeq,this.UserCharNo,ViCommConst.MAIL_TP_CAST_STOCK_PIC_NP,this.ReturnPage));
		} else {
			ReturnToCall();
		}
	}

	private void ReturnToCall() {
		if (this.ReturnPage.Equals("StockPicCheckList")) {
			Server.Transfer(string.Format("../CastAdmin/StockPicCheckList.aspx?sitecd={0}&closed={1}",this.SiteCd,this.ObjNotPublishFlag));

		} else if (this.ReturnPage.Equals("StockPicOpenList")) {
			Server.Transfer(string.Format("../CastAdmin/StockPicOpenList.aspx?sitecd={0}",this.SiteCd));

		} else {
			Server.Transfer(string.Format("../Cast/CastView.aspx?loginid={0}&sitecd={1}",lblLoginId.Text,this.SiteCd));
		}
	}

	protected void lstAuthType_DataBound(object sender,EventArgs e) {
		ListItem oItemDel = this.lstAuthType.Items.FindByValue(ViCommConst.MOVIE_APPLY_REMOVE);
		if (oItemDel != null) {
			this.lstAuthType.Items.Remove(oItemDel);
		}
	}
}
