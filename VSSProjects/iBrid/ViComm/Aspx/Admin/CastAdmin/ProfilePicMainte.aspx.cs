﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: プロフィール写真認証
--	Progaram ID		: ProfilePicMainte
--
--  Creation Date	: 2010.09.25
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using iBridCommLib;
using ViComm;

public partial class Cast_ProfilePicMainte:System.Web.UI.Page {
	private Stream filter;

	protected string SeekSiteCd {
		get {
			return this.ViewState["SeekSiteCd"] as string;
		}
		set {
			this.ViewState["SeekSiteCd"] = value;
		}
	}
	
	protected string PrevNonPublicFlag {
		get {
			return this.ViewState["PrevNonPublicFlag"] as string;
		}
		set {
			this.ViewState["PrevNonPublicFlag"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		filter = Response.Filter;
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		lblSiteCd.Text = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		lblUserSeq.Text = iBridUtil.GetStringValue(Request.QueryString["userseq"]);
		lblUserCharNo.Text = iBridUtil.GetStringValue(Request.QueryString["usercharno"]);
		ViewState["SITE_CD"] = lblSiteCd.Text;
		ViewState["USER_SEQ"] = lblUserSeq.Text;
		ViewState["USER_CHAR_NO"] = lblUserCharNo.Text;
		ViewState["PIC_SEQ"] = iBridUtil.GetStringValue(Request.QueryString["picseq"]);
		ViewState["RETURN"] = iBridUtil.GetStringValue(Request.QueryString["return"]);
		ViewState["OBJ_NOT_PUBLISH_FLAG"] = iBridUtil.GetStringValue(Request.QueryString["closed"]);
		SeekSiteCd = iBridUtil.GetStringValue(Request.QueryString["seeksitecd"]);
		
		using (Cast oCast = new Cast()) {
			string sName = string.Empty,sId = string.Empty;
			oCast.GetValue(lblUserSeq.Text,"CAST_NM",ref sName);
			oCast.GetValue(lblUserSeq.Text,"LOGIN_ID",ref sId);
			lblUserNm.Text = sName;
			lblLoginId.Text = sId;
		}
		ClearField();
		DataBind();
		GetData();

		if (ViCommConst.FLAG_ON_STR.Equals(iBridUtil.GetStringValue(ConfigurationManager.AppSettings["enabledNoTitleObj"]))) {
			this.vdrPicTitle.Enabled = false;
		}
	}

	private void ClearField() {
		txtPicTitle.Text = string.Empty;
		lstAuthType.SelectedIndex = 0;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		ReturnToCall();
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {

			db.PrepareProcedure("CAST_PIC_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,ViewState["SITE_CD"].ToString());
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,ViewState["USER_SEQ"].ToString());
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViewState["USER_CHAR_NO"].ToString());
			db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.VARCHAR2,ViewState["PIC_SEQ"].ToString());
			db.ProcedureOutParm("PPIC_TITLE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPIC_DOC",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PUPLOAD_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPIC_TYPE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_PIC_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_PIC_ATTR_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtPicTitle.Text = db.GetStringValue("PPIC_TITLE");
				txtPicDoc.Text = db.GetStringValue("PPIC_DOC");
				string sUnAtuthFlag = db.GetStringValue("POBJ_NOT_APPROVE_FLAG");
				string sNotPublishFlag = db.GetStringValue("POBJ_NOT_PUBLISH_FLAG");
				if (sUnAtuthFlag.Equals("0")) {
					if (sNotPublishFlag.Equals("0")) {
						lstAuthType.SelectedValue = ViCommConst.MOVIE_APPLY_OK;
					} else if (sNotPublishFlag.Equals("1")) {
						lstAuthType.SelectedValue = ViCommConst.MOVIE_APPLY_NG;
					}
				} else if (sUnAtuthFlag.Equals("1")) {
					lstAuthType.SelectedValue = ViCommConst.MOVIE_APPLY_WAIT;
				}
				ViewState["PIC_TYPE"] = db.GetStringValue("PPIC_TYPE");
				lstCastPicAttrType.DataBind();
				lstCastPicAttrType.DataSourceID = string.Empty;
				lstCastPicAttrType.SelectedValue = db.GetStringValue("PCAST_PIC_ATTR_TYPE_SEQ");
				lstCastPicAttrTypeValue.DataSourceID = "dsCastPicAttrTypeValue";
				lstCastPicAttrTypeValue.DataBind();
				lstCastPicAttrTypeValue.DataSourceID = string.Empty;
				lstCastPicAttrTypeValue.SelectedValue = db.GetStringValue("PCAST_PIC_ATTR_SEQ");
				using (CastPic oCastPic = new CastPic()) {
					if (oCastPic.GetOne(ViewState["SITE_CD"].ToString(),ViewState["USER_SEQ"].ToString(),ViewState["USER_CHAR_NO"].ToString(),ViewState["PIC_SEQ"].ToString())) {
						imgPic.ImageUrl = string.Format("../{0}?t={1}",oCastPic.objPhotImgPath,DateTime.Now.ToString("HHmmssfffff"));
						ViewState["IMG_PATH"] = oCastPic.objPhotImgPath;
						lblObjModifyRequestSpec.Text = oCastPic.objModifyRequestSpec;
					}
				}
			} else {
				ClearField();
			}
		}
		pnlDtl.Visible = true;
		this.PrevNonPublicFlag = lstAuthType.SelectedValue;
	}

	private void UpdateData(int pDelFlag) {
		int iNotApproveFlag = 0;
		int iDelFlag = 0;
		int iNotPublishFlag = 0;

		if (pDelFlag == 0) {
			switch (lstAuthType.SelectedValue) {
				case ViCommConst.MOVIE_APPLY_WAIT:	// 認証待ち
					iDelFlag = 0;
					iNotApproveFlag = 1;
					iNotPublishFlag = 0;
					break;

				case ViCommConst.MOVIE_APPLY_OK:	// 公開


					iDelFlag = 0;
					iNotApproveFlag = 0;
					iNotPublishFlag = 0;
					break;

				case ViCommConst.MOVIE_APPLY_REMOVE: // 削除
					iNotApproveFlag = 0;
					iDelFlag = 1;
					iNotPublishFlag = 0;
					break;

				case ViCommConst.MOVIE_APPLY_NG:	// 非公開


					iNotApproveFlag = 0;
					iDelFlag = 0;
					iNotPublishFlag = 1;
					break;
			}
		} else {
			iDelFlag = 1;
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_PIC_MAINTE");
			db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.VARCHAR2,ViewState["PIC_SEQ"].ToString());
			db.ProcedureInParm("PPIC_TITLE",DbSession.DbType.VARCHAR2,GetPicTitle(txtPicTitle.Text));
			db.ProcedureInParm("PPIC_DOC",DbSession.DbType.VARCHAR2,GetPicDoc(txtPicDoc.Text));
			db.ProcedureInParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,iNotApproveFlag);
			db.ProcedureInParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER,iNotPublishFlag);
			db.ProcedureInParm("PPIC_TYPE",DbSession.DbType.NUMBER,ViewState["PIC_TYPE"].ToString());
			db.ProcedureInParm("PCAST_PIC_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,lstCastPicAttrType.SelectedValue);
			db.ProcedureInParm("PCAST_PIC_ATTR_SEQ",DbSession.DbType.VARCHAR2,lstCastPicAttrTypeValue.SelectedValue);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,iDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		if (iDelFlag == 1) {
			string sFileNm = string.Empty;
			string sWebPhisicalDir = string.Empty;

			string sSiteCd = ViewState["SITE_CD"].ToString();
			string sUserSeq = ViewState["USER_SEQ"].ToString();
			string sUserCharNo = ViewState["USER_CHAR_NO"].ToString();
			string sPicSeq = ViewState["PIC_SEQ"].ToString();
			using (Site oSite = new Site()) {
				oSite.GetValue(sSiteCd,"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
			}

			sFileNm = iBridUtil.addZero(sPicSeq,ViCommConst.OBJECT_NM_LENGTH);
			ViCommPrograms.DeleteFiles(ViCommPrograms.GetCastPicDir(sWebPhisicalDir,sSiteCd,lblLoginId.Text),string.Format("{0}*",sFileNm));

		} else if (uldCastPic.HasFile) {
			string sWebPhisicalDir = string.Empty;
			using (Site oSite = new Site()) {
				oSite.GetValue(ViewState["SITE_CD"].ToString(),"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
			}

			string sFileNm = ViCommConst.PIC_HEADER + iBridUtil.addZero(ViewState["PIC_SEQ"].ToString(),ViCommConst.OBJECT_NM_LENGTH);
			string sPath = sWebPhisicalDir +
							ViCommConst.PIC_DIRECTRY + "\\" + ViewState["SITE_CD"].ToString() + "\\Operator\\" + lblLoginId.Text;

			string sFullPath = sPath + "\\" + sFileNm + ViCommConst.PIC_FOODER;

			using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,string.Empty,ViCommConst.FILE_UPLOAD_PASSWORD)) {

				if (System.IO.File.Exists(sFullPath)) {
					uldCastPic.SaveAs(sFullPath);

					string sInputDir = ConfigurationManager.AppSettings["MailParseDir"] + "\\";
					uldCastPic.SaveAs(Path.Combine(sInputDir,sFileNm + ViCommConst.PIC_FOODER));

					ImageHelper.ConvertMobileImage(ViewState["SITE_CD"].ToString(),sInputDir,sPath,sFileNm);
				}
			}
		}

		bool bTxNotPublicNotice;
		using (ManageCompany oManageCompany = new ManageCompany()) {
			bTxNotPublicNotice = oManageCompany.IsAvailableService(ViCommConst.RELEASE_CAST_OBJ_NON_PUBLIC_INFO,2);
		}

		if (bTxNotPublicNotice && !this.PrevNonPublicFlag.Equals(ViCommConst.MOVIE_APPLY_NG) && this.lstAuthType.SelectedValue.Equals(ViCommConst.MOVIE_APPLY_NG)) {
			Server.Transfer(string.Format("./TxCastObjNonPublicMail.aspx?site={0}&seeksitecd={1}&userseq={2}&usercharNo={3}&mailtype={4}&return={5}",ViewState["SITE_CD"].ToString(),SeekSiteCd,ViewState["USER_SEQ"].ToString(),ViewState["USER_CHAR_NO"].ToString(),ViCommConst.MAIL_TP_CAST_PROF_PIC_NP,ViewState["RETURN"].ToString()));
		} else {
			ReturnToCall();
		}
	}

	private string GetPicTitle(string name) {
		byte[] byteStr = System.Text.Encoding.UTF8.GetBytes(name);
		string ret = name;
		if (byteStr.Length > 90) {
			ret = SysPrograms.Substring(name, 30);
		}
		return ret;
	}

	private string GetPicDoc(string name) {
		byte[] byteStr = System.Text.Encoding.UTF8.GetBytes(name);
		string ret = name;
		if (byteStr.Length > 3000) {
			ret = SysPrograms.Substring(name, 1000);
		}
		return ret;
	}
	private void ReturnToCall() {
		if (ViewState["RETURN"].ToString().Equals("ProfilePicList")) {
			Server.Transfer(string.Format("../CastAdmin/ProfilePicList.aspx?site={0}&seeksitecd={1}",ViewState["SITE_CD"].ToString(),SeekSiteCd));
		} else {
			Server.Transfer(string.Format("../Cast/CastView.aspx?loginid={0}",lblLoginId.Text));
		}
	}

	protected void lstCastPicAttrType_SelectedIndexChanged(object sender,EventArgs e) {
		lstCastPicAttrTypeValue.DataSourceID = "dsCastPicAttrTypeValue";
		lstCastPicAttrTypeValue.DataBind();
		lstCastPicAttrTypeValue.DataSourceID = string.Empty;
	}

	protected void dsCastPicAttrType_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = ViewState["SITE_CD"].ToString();
	}

	protected void dsCastPicAttrTypeValue_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = ViewState["SITE_CD"].ToString();
		e.InputParameters[1] = lstCastPicAttrType.SelectedValue;
	}

	/// <summary>
	/// 画像ダウンロードリンク押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void lnkImgDownload_Click(object sender,EventArgs e) {
		// 変換前の画像をダウンロード
		ImageHelper.DownloadCastPicOriginalImage(
			filter
			,ViewState["SITE_CD"].ToString()
			,lblLoginId.Text
			,ViewState["PIC_SEQ"].ToString()
		);
	}
}
