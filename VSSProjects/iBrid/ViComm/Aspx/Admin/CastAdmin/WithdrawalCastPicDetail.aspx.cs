﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 退会出演者画像一覧
--	Progaram ID		: WithdrawalCastPicList
--
--  Creation Date	: 2017.05.16
--  Creater			: M&TT Zukeran
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater				Update Explain

-------------------------------------------------------------------------*/
using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class CastAdmin_WithdrawalCastPicDetail:System.Web.UI.Page {
	private Stream filter;

	protected void Page_Load(object sender,EventArgs e) {
		filter = Response.Filter;
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!IsPostBack) {
			// 戻るボタンの遷移先を前の画面に設定
			btnBack.PostBackUrl = Request.UrlReferrer.AbsoluteUri;

			string sLoginId = iBridUtil.GetStringValue(Request.QueryString["loginid"]);
			string sPicSeq = iBridUtil.GetStringValue(Request.QueryString["picseq"]);
			CastPic oCastPic = new CastPic();
			DataSet oDS = oCastPic.GetOneWithdrawalPic(PwViCommConst.MAIN_SITE_CD,sLoginId,sPicSeq);
			if (oDS.Tables[0].Rows.Count > 0) {
				DataRow oDR = oDS.Tables[0].Rows[0];
				ViewState["PIC_SEQ"] = sPicSeq;
				ViewState["SITE_CD"] = PwViCommConst.MAIN_SITE_CD;

				lblLoginId.Text = oDR["LOGIN_ID"].ToString();
				lblHandleNm.Text = oDR["HANDLE_NM"].ToString();
				lblWithdrawalDate.Text = string.Format("{0:yyyy/MM/dd HH:mm:ss}",oDR["WITHDRAWAL_DATE"]);
				lblUploadDate.Text = string.Format("{0:yyyy/MM/dd HH:mm:ss}",oDR["UPLOAD_DATE"]);
				imgPic.ImageUrl = string.Format("../{0}?t={1}",oDR["PHOTO_IMG_PATH"].ToString(),DateTime.Now.ToString("HHmmssfffff"));
				// 画像サイズを取得＆設定
				ImageHelper.ImageSizeClass info = ImageHelper.GetImageSize(
					Request.Url.Host
					,string.Format("http://{0}/Admin/data/{1}/operator/{2}/{3}{4}"
						,Request.Url.Host
						,PwViCommConst.MAIN_SITE_CD
						,sLoginId
						,sPicSeq.PadLeft(15,'0')
						,ViCommConst.PIC_FOODER_ORIGINAL
					)
				);
				lblImageSize.Text = string.Format("{0} x {1} pixel",info.width,info.height);
			}
		}
	}

	/// <summary>
	/// 画像ダウンロードリンク押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void lnkImgDownload_Click(object sender,EventArgs e) {
		// 変換前の画像をダウンロード
		ImageHelper.DownloadCastPicOriginalImage(
			filter
			,PwViCommConst.MAIN_SITE_CD
			,iBridUtil.GetStringValue(Request.QueryString["loginid"])
			,iBridUtil.GetStringValue(Request.QueryString["picseq"])
		);
	}
}
