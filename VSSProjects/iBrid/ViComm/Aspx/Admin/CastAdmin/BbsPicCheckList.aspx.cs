﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 認証待ち掲示板画像一覧
--	Progaram ID		: BbsPicCheckList
--
--  Creation Date	: 2009.08.24
--  Creater			: i-Brid(Y.Inoue)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater				Update Explain
  2009/09/03	i-Brid(Y.Inoue)		担当サイト対応  2010/06/29	伊藤和明			検索項目に日付を追加
  2010/07/16	Koyanagi			検索項目に非公開ﾌﾗｸﾞを追加

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using iBridCommLib;
using ViComm;

public partial class Cast_BbsPicCheckList:System.Web.UI.Page {
	private string recCount = "";
	private Stream filter;

	protected string PicAttrTypeItemNo {
		get {
			return this.ViewState["PicAttrTypeItemNo"] as string;
		}
		set {
			this.ViewState["PicAttrTypeItemNo"] = value;
		}
	}
	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
			if (iBridUtil.GetStringValue(Request.QueryString["closed"]).ToString() == "1") {
				chkClosed.Checked = true;
			}

			if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
				lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}

			string sPicTypeSeq = string.Empty;
			string sPicTypeItemNo = string.Empty;
			if (!iBridUtil.GetStringValue(Request.QueryString["pictype_itemno"]).Equals(string.Empty)) {
				sPicTypeItemNo = iBridUtil.GetStringValue(Request.QueryString["pictype_itemno"]);
				this.PicAttrTypeItemNo = sPicTypeItemNo;
				
				if (sPicTypeItemNo.Equals(ViCommConst.CastPicAttrTypeDef.PLAN_PIC_ITEM_NO)) {
					this.lblPgmTitle.Text = "認証待ち企画画像";
				}
				
				using (CastPicAttrType oCastPicAttrType = new CastPicAttrType()) {
					using (DataSet ds = oCastPicAttrType.GetOneByItemNo(lstSiteCd.SelectedValue,sPicTypeItemNo)) {
						if (ds.Tables[0].Rows.Count > 0) {
							sPicTypeSeq = iBridUtil.GetStringValue(ds.Tables[0].Rows[0]["CAST_PIC_ATTR_TYPE_SEQ"]);
						}
					}
				}
			}
			this.InitCastPicAttrType(sPicTypeSeq);
			this.InitCastPicAttrTypeValue(string.Empty);
			GetList();
		}
	}

	protected void dsCastPic_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void FirstLoad() {
		grdPic.PageSize = 100;
		grdPic.DataSourceID = "";
		DataBind();
		if (Session["SiteCd"].ToString().Equals("")) {
			lstSiteCd.Items.Insert(0,new ListItem("",""));
		}
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
		    lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		lstSiteCd.DataSourceID = "";
	}

	private void InitPage() {
		grdPic.DataSourceID = "";
		DataBind();
		recCount = "0";
		chkClosed.Checked = false;
	}

	private void ClearField() {
		lstSiteCd.SelectedIndex = 0;
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid)
			return;
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.Response.Redirect(this.Request.Url.PathAndQuery);
	}

	protected void lnkMainte_Command(object sender,CommandEventArgs e) {
		string[] sKey = e.CommandArgument.ToString().Split(':');
		string sNotPublishFlag = "";
		if (chkClosed.Checked) {
			sNotPublishFlag = "1";
		}
		Response.Redirect(string.Format("../CastAdmin/BbsPicMainte.aspx?sitecd={0}&userseq={1}&usercharno={2}&picseq={3}&return={4}&closed={5}&pictype_itemno={6}&seeksitecd={7}",sKey[0],sKey[1],sKey[2],sKey[3],sKey[4],sNotPublishFlag,this.PicAttrTypeItemNo,this.lstSiteCd.SelectedValue));
	}

	protected string GetClientScript(object pPicUrl) {
		string sRoot = Request.Url.Authority + ConfigurationManager.AppSettings["Root"];
		return string.Format("javascript:win=window.open('http://{1}/ViewPic.html?url={0}','PictureZoom','width=300,height=320,resizable=yes,directories=no,scrollbars=no' , false);win.focus();return false;",pPicUrl,sRoot);
	}


	protected void btnUpdate_OnClick(object sender,EventArgs e) {
		string sSiteCd = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][0]).ToString();
		string sUserSeq = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][1]).ToString();
		string sUserCharNo = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][2]).ToString();
		string sPicSeq = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][3]).ToString();
		string sTxtTitle = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][5]).ToString();
		string sTxtDoc = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][6]).ToString();
		string sCastPicAttrTypeSeq = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][7]).ToString();
		string sCastPicAttrSeq = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][8]).ToString();
		string sNotPublishFlag = "";
		if (chkClosed.Checked) {
			sNotPublishFlag = "1";
		}
		UpdateProfilePicture(sSiteCd,sUserSeq,sUserCharNo,sPicSeq,"0","0",sTxtTitle,sTxtDoc,sCastPicAttrTypeSeq,sCastPicAttrSeq);
		Server.Transfer(string.Format("BbsPicCheckList.aspx?sitecd={0}&closed={1}&pictype_itemno={2}&eeeksitecd={3}",sSiteCd,sNotPublishFlag,this.PicAttrTypeItemNo,this.lstSiteCd.SelectedValue));
	}

	private void UpdateProfilePicture(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pPicSeq,
		string pDelFlag,
		string pProfilePicFlag,
		string pTitle,
		string pDoc,
		string pCastPicAttrTypeSeq,
		string pCastPicAttrSeq
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_PIC_PROFILE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.VARCHAR2,pPicSeq);
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureInParm("PPROFILE_PIC_FLAG",DbSession.DbType.NUMBER,pProfilePicFlag);
			db.ProcedureInParm("PPIC_TYPE",DbSession.DbType.NUMBER,ViCommConst.ATTACHED_BBS);
			db.ProcedureInParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			db.ProcedureInParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			db.ProcedureInParm("PPIC_TITLE",DbSession.DbType.VARCHAR2,pTitle);
			db.ProcedureInParm("PPIC_DOC",DbSession.DbType.VARCHAR2,pDoc);
			db.ProcedureInParm("PCAST_PIC_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,pCastPicAttrTypeSeq);
			db.ProcedureInParm("PCAST_PIC_ATTR_SEQ",DbSession.DbType.VARCHAR2,pCastPicAttrSeq);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
		}
	}

	private void GetList() {
		grdPic.DataSourceID = "dsCastPic";
		grdPic.PageIndex = 0;
		grdPic.DataBind();
		pnlCount.DataBind();
	}

	protected void dsCastPic_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		string sNotPublishFlag = "";
		if (chkClosed.Checked) {
			sNotPublishFlag = "1";
		}
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = "1";
		e.InputParameters[2] = sNotPublishFlag;
		e.InputParameters[3] = ViCommConst.ATTACHED_BBS.ToString();
		e.InputParameters[4] = string.IsNullOrEmpty(this.txtUploadDayFrom.Text.Replace("/",string.Empty)) ? null : this.txtUploadDayFrom.Text;
		e.InputParameters[5] = string.IsNullOrEmpty(this.txtUploadDayTo.Text.Replace("/", string.Empty)) ? null : this.txtUploadDayTo.Text;
		e.InputParameters[6] = lstCastPicAttrType.SelectedValue;
		e.InputParameters[7] = lstCastPicAttrTypeValue.SelectedValue;
	}

	protected void grdPic_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			if (DataBinder.Eval(e.Row.DataItem,"OBJ_NOT_PUBLISH_FLAG").ToString() == "1") {
				e.Row.BackColor = System.Drawing.Color.LightYellow;
			}
			if (DataBinder.Eval(e.Row.DataItem,"CAUTION_FLAG").ToString() == "1") {
				e.Row.BackColor = System.Drawing.Color.Gainsboro;
			}
		}
	}

	protected void lstCastPicAttrType_SelectedIndexChanged(object sender, EventArgs e) {
		this.InitCastPicAttrTypeValue(string.Empty);
	}

	protected void dsCastPicAttrType_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
	}

	protected void dsCastPicAttrTypeValue_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = lstCastPicAttrType.SelectedValue;
	}

	protected void InitCastPicAttrType(string pCastPicAttrType) {
		lstCastPicAttrType.DataSourceID = "dsCastPicAttrType";
		lstCastPicAttrType.DataBind();
		lstCastPicAttrType.Items.Insert(0, new ListItem(string.Empty, string.Empty));
		lstCastPicAttrType.DataSourceID = string.Empty;
		if (pCastPicAttrType.Equals(string.Empty) || 
			lstCastPicAttrType.Items.FindByValue(pCastPicAttrType) == null) {
			lstCastPicAttrType.SelectedIndex = 0;
		} else {
			lstCastPicAttrType.SelectedValue = pCastPicAttrType;
		}
	}

	protected void InitCastPicAttrTypeValue(string pCastPicAttrTypeValue) {
		lstCastPicAttrTypeValue.DataSourceID = "dsCastPicAttrTypeValue";
		lstCastPicAttrTypeValue.DataBind();
		lstCastPicAttrTypeValue.Items.Insert(0, new ListItem(string.Empty, string.Empty));
		lstCastPicAttrTypeValue.DataSourceID = string.Empty;

		if (pCastPicAttrTypeValue.Equals(string.Empty) || 
			lstCastPicAttrTypeValue.Items.FindByValue(pCastPicAttrTypeValue) == null) {
			lstCastPicAttrTypeValue.SelectedIndex = 0;
		} else {
			lstCastPicAttrTypeValue.SelectedValue = pCastPicAttrTypeValue;
		}
	}

	protected void lstSiteCd_SelectedIndexChanged(object sender, EventArgs e) {
		this.InitCastPicAttrType(string.Empty);
		this.InitCastPicAttrTypeValue(string.Empty);
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		Response.Filter = filter;
		Response.AddHeader("Content-Disposition",string.Format("attachment;filename=BBS_PIC_CHECK_LIST_{0}.CSV",lstSiteCd.SelectedValue));
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		DataSet ds;
		using (CastPic oCastPic = new CastPic()) {
			string sNotPublishFlag = "";
			if (chkClosed.Checked) {
				sNotPublishFlag = "1";
			}
			
			ds = oCastPic.GetApproveManagePageCollection(
					lstSiteCd.SelectedValue,
					ViCommConst.FLAG_ON_STR,
					sNotPublishFlag,
					ViCommConst.ATTACHED_BBS.ToString(),
					string.IsNullOrEmpty(this.txtUploadDayFrom.Text.Replace("/",string.Empty)) ? null : this.txtUploadDayFrom.Text,
					string.IsNullOrEmpty(this.txtUploadDayTo.Text.Replace("/", string.Empty)) ? null : this.txtUploadDayTo.Text,
					lstCastPicAttrType.SelectedValue,
					lstCastPicAttrTypeValue.SelectedValue,
					string.Empty,
					string.Empty,
					0,
					SysConst.DB_MAX_ROWS
				);
			SetCsvData(ds);
			Response.End();
		}
	}

	public void SetCsvData(DataSet pDataSet) {

		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		string sHeaderString = "ﾛｸﾞｲﾝID,ﾊﾝﾄﾞﾙ名,ｱｯﾌﾟﾛｰﾄﾞ日時,写真属性,写真属性値\r\n";
		Response.BinaryWrite(encoding.GetBytes(sHeaderString));

		string sDtl = "";

		DataRow dr = null;
		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];

			sDtl = string.Format("{0},{1},{2},{3},{4}",
							dr["LOGIN_ID"].ToString(),
							dr["HANDLE_NM"].ToString(),
							dr["UPLOAD_DATE"].ToString(),
							dr["CAST_PIC_ATTR_TYPE_NM"].ToString(),
							dr["CAST_PIC_ATTR_NM"].ToString()
							);
			sDtl += "\r\n";
			Response.BinaryWrite(encoding.GetBytes(sDtl));
		}
	}
}
