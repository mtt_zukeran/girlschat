﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 未認証キャストキャラクター一覧
--	Progaram ID		: NotCertifiedCastCharacterList
--
--  Creation Date	: 2010.04.05
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class CastAdmin_NotCertifiedCastCharacterList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			if (Session["SiteCd"].ToString().Equals("")) {
				lstSiteCd.DataBind();
				lstSiteCd.Items.Insert(0,new ListItem("",""));
				lstSiteCd.DataSourceID = "";
			}
			ViewState["RETURN"] = "CastCharacterInquery.aspx";
			InitPage();
		}
	}

	private void InitPage() {
		pnlKey.Enabled = true;
		grdCast.PageSize = 100;
		ClearField();
		DataBind();

		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);

	}

	private void ClearField() {
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		} else {
			lstSiteCd.SelectedIndex = 0;
		}
		txtLoginId.Text = "";
		txtCastNm.Text = "";

		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (IsValid) {
			GetList();
		}
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	private void GetList() {
		grdCast.DataSourceID = "dsCastCharacter";
		grdCast.PageIndex = 0;
		grdCast.DataBind();
		pnlCount.DataBind();
	}

	protected void dsCastCharacter_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsCastCharacter_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = "";
		e.InputParameters[1] = txtLoginId.Text;
		e.InputParameters[2] = "";
		e.InputParameters[3] = txtCastNm.Text;
		e.InputParameters[4] = lstSiteCd.SelectedValue;
		e.InputParameters[5] = "";
		e.InputParameters[6] = "";
		e.InputParameters[7] = "";
		e.InputParameters[8] = "";
		e.InputParameters[9] = "";
		e.InputParameters[10] = "";
		e.InputParameters[11] = "";
		e.InputParameters[12] = "";
		e.InputParameters[13] = "";
		e.InputParameters[14] = "";
		e.InputParameters[15] = "";
		e.InputParameters[16] = "";
		e.InputParameters[17] = "";
		e.InputParameters[18] = "";
		e.InputParameters[19] = "";
		e.InputParameters[20] = ViCommConst.NaFlag.NO_CERTIFIED.ToString();
		e.InputParameters[21] = "";
		e.InputParameters[22] = "";
		e.InputParameters[23] = "";
		e.InputParameters[24] = "";
		e.InputParameters[25] = "";
		e.InputParameters[26] = "";
		e.InputParameters[27] = "";
	}
}
