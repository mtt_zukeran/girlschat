﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者投稿非公開設定通知メール送信
--	Progaram ID		: TxCastObjNonPublicMail
--
--  Creation Date	: 2016.02.05
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater		Update Explain


-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class CastAdmin_TxCastObjNonPublicMail:System.Web.UI.Page {

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}
	private string UserSeq {
		get {
			return iBridUtil.GetStringValue(this.ViewState["UserSeq"]);
		}
		set {
			this.ViewState["UserSeq"] = value;
		}
	}
	private string UserCharNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["UserCharNo"]);
		}
		set {
			this.ViewState["UserCharNo"] = value;
		}
	}
	private string MailType {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MailType"]);
		}
		set {
			this.ViewState["MailType"] = value;
		}
	}
	private string ObjNotPublishFlag {
		get {
			return iBridUtil.GetStringValue(this.ViewState["ObjNotPublishFlag"]);
		}
		set {
			this.ViewState["ObjNotPublishFlag"] = value;
		}
	}
	private string Rowid {
		get {
			return iBridUtil.GetStringValue(this.ViewState["Rowid"]);
		}
		set {
			this.ViewState["Rowid"] = value;
		}
	}
	private string RevisionNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["RevisionNo"]);
		}
		set {
			this.ViewState["RevisionNo"] = value;
		}
	}
	private string ReturnPage {
		get {
			return iBridUtil.GetStringValue(this.ViewState["ReturnPage"]);
		}
		set {
			this.ViewState["ReturnPage"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			InitPage();
			
			switch(this.MailType) {
				case ViCommConst.MAIL_TP_CAST_PROF_PIC_NP:
					lblPgmTitle.Text = "プロフ画像非公開通知メール送信";
					break;
				case ViCommConst.MAIL_TP_CAST_PROF_MOVIE_NP:
					lblPgmTitle.Text = "プロフ動画非公開通知メール送信";
					break;
				case ViCommConst.MAIL_TP_CAST_BBS_PIC_NP:
					lblPgmTitle.Text = "掲示板画像非公開通知メール送信";
					break;
				case ViCommConst.MAIL_TP_CAST_BBS_MOVIE_NP:
					lblPgmTitle.Text = "掲示板動画非公開通知メール送信";
					break;
				case ViCommConst.MAIL_TP_CAST_STOCK_PIC_NP:
					lblPgmTitle.Text = "ストック画像非公開通知メール送信";
					break;
				case ViCommConst.MAIL_TP_CAST_STOCK_MOVIE_NP:
					lblPgmTitle.Text = "ストック動画非公開通知メール送信";
					break;
				case ViCommConst.MAIL_TP_CAST_GAME_PIC_NP:
					lblPgmTitle.Text = "ゲーム男性用お宝画像非公開通知メール送信";
					break;
				case ViCommConst.MAIL_TP_CAST_GAME_MOVIE_NP:
					lblPgmTitle.Text = "ゲーム限定動画非公開通知メール送信";
					break;
			}
		}
	}

	private void InitPage() {
		this.SiteCd = Request.QueryString["site"];
		this.UserSeq = Request.QueryString["userseq"];
		this.UserCharNo = Request.QueryString["usercharno"];
		this.MailType = Request.QueryString["mailtype"];
		this.ReturnPage = Request.QueryString["return"];
		
		CreateList();
		
		ClearField();
	}
	
	private void CreateList() {
		lstMailTemplate.Items.Clear();
		DataSet oDataSet;
		using (MailTemplate oMailTemplate = new MailTemplate()) {
			oDataSet = oMailTemplate.GetListByTemplateType(this.SiteCd,this.MailType,ViCommConst.WOMAN);
		}
		
		if (oDataSet.Tables[0].Rows.Count > 0) {
			foreach(DataRow dr in oDataSet.Tables[0].Rows) {
				lstMailTemplate.Items.Add(new ListItem(iBridUtil.GetStringValue(dr["TEMPLATE_NM"]),iBridUtil.GetStringValue(dr["MAIL_TEMPLATE_NO"])));
			}
		} else {
			lstMailTemplate.Visible = false;
			btnTxMail.Visible = false;
			lblMessage.Text = "削除理由が登録されていません。";
			lblMessage.Visible = true;
			lnkReturnPage.Visible = true;
		}
	}

	private void ClearField() {
		lstMailTemplate.SelectedIndex = 0;
	}

	protected void btnTxMail_Click(object sender,EventArgs e) {
		TxMail();
		
		lstMailTemplate.Visible = false;
		btnTxMail.Visible = false;
		lblMessage.Visible = true;
		lnkReturnPage.Visible = true;
	}

	private void TxMail() {
		string[] sUserSeq = new string[1];
		sUserSeq[0] = this.UserSeq;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("TX_ADMIN_TO_CAST_MAIL");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
			db.ProcedureInParm("PMAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,lstMailTemplate.SelectedValue);
			db.ProcedureInArrayParm("PCAST_USER_SEQ",DbSession.DbType.VARCHAR2,1,sUserSeq);
			db.ProcedureInParm("PMAN_USER_COUNT",DbSession.DbType.NUMBER,1);
			db.ProcedureInParm("PORIGINAL_TITLE",DbSession.DbType.VARCHAR2,string.Empty);
			db.ProcedureInArrayParm("PORIGINAL_DOC",DbSession.DbType.VARCHAR2,0,new string[]{string.Empty});
			db.ProcedureInParm("PORIGINAL_DOC_COUNT",DbSession.DbType.NUMBER,0);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	protected void lnkReturnPage_Command(object sender,EventArgs e) {
		if (this.MailType.Equals(ViCommConst.MAIL_TP_CAST_GAME_PIC_NP) || this.MailType.Equals(ViCommConst.MAIL_TP_CAST_GAME_MOVIE_NP)) {
			Server.Transfer(string.Format("../Extension/{0}.aspx?site={1}",this.ReturnPage,this.SiteCd));
		} else {
			Server.Transfer(string.Format("./{0}.aspx?site={1}",this.ReturnPage,this.SiteCd));
		}
	}
}
