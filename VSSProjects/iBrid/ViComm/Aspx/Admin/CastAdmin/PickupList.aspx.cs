﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ピックアップメンテナンス
--	Progaram ID		: PickupList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Cast_PickupList:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		ClearField();
		DataBind();
		if (!IsPostBack) {
			lstSiteCd.DataSourceID = "";
			string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			string sActCategorySeq = iBridUtil.GetStringValue(Request.QueryString["categoryseq"]);
			if (!sSiteCd.Equals("")) {
				lstSiteCd.SelectedValue = sSiteCd;
			} else if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
				lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}
			lstActCategorySeq.DataBind();
			if (!sActCategorySeq.Equals("")) {
				lstActCategorySeq.SelectedValue = sActCategorySeq;
			}
			if ((!sSiteCd.Equals("")) && (!sActCategorySeq.Equals(""))) {
				btnListSeek_Click(null,null);
			}
		}
		
		this.lblNoCheckError.Text = DisplayWordUtil.Replace(this.lblNoCheckError.Text);
	}

	private void ClearField() {
		lblNoCheckError.Visible = false;
		pnlList.Visible = false;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		pnlList.Visible = true;
		GetData();
		grdCast.DataSourceID = "dsCastCharacter";
		grdCast.DataBind();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnSubmitCreate_Click(object sender,EventArgs e) {
		String sCheckedId = "";
		for (int i = 0;i < this.grdCast.Rows.Count;i++) {
			GridViewRow row = this.grdCast.Rows[i];
			CheckBox oChk = (CheckBox)row.FindControl("chkPickup");
			if (oChk.Checked) {
				TableCell objTableCell = row.Cells[2];
				String sCastId = objTableCell.Text;
				sCheckedId = sCheckedId + "," + sCastId;
			}
		}
		if (sCheckedId.Length > 0) {
			sCheckedId = sCheckedId.Substring(1);
			lblNoCheckError.Visible = false;
			Server.Transfer(
				string.Format("PickupMainte.aspx?sitecd={0}&sitenm={1}&id={2}&categoryseq={3}",lstSiteCd.SelectedValue,lstSiteCd.Items[lstSiteCd.SelectedIndex].Text,sCheckedId,lstActCategorySeq.SelectedValue)
			);
		} else {
			lblNoCheckError.Visible = true;
		}
	}


	protected void btnSubmitClear_Click(object sender,EventArgs e) {

		string[] sPickUpCast = null;

		using (CastCharacter oCharacter = new CastCharacter()) {
			sPickUpCast = oCharacter.GetPickUpCast(lstSiteCd.SelectedValue,lstActCategorySeq.SelectedValue);
		}

		//PickUpはメインキャラのみ対応

		if (sPickUpCast != null) {
			string[] sPickUpUserCharNo = new string[sPickUpCast.Length];
			for (int i = 0;i < sPickUpCast.Length;i++) {
				sPickUpUserCharNo.SetValue(ViCommConst.MAIN_CHAR_NO,i);
			}
			string[] sUpdateUserSeq = new string[1];
			sUpdateUserSeq[0] = "";

			string[] sUpdateUserCharNo = new string[1];
			sUpdateUserCharNo[0] = "";

			string[] sUpdateComent = new string[1];
			sUpdateComent[0] = "";

			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("PICKUP_CHARACTER_MAINTE");
				db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
				db.ProcedureInArrayParm("pDELETE_USER_SEQ",DbSession.DbType.VARCHAR2,sPickUpCast.Length,sPickUpCast);
				db.ProcedureInArrayParm("pDELETE_USER_CHAR_NO",DbSession.DbType.VARCHAR2,sPickUpCast.Length,sPickUpUserCharNo); //メインキャラのみ
				db.ProcedureInParm("pDELETE_RECORD_COUNT",DbSession.DbType.NUMBER,sPickUpCast.Length);
				db.ProcedureInArrayParm("pUPDATE_USER_SEQ",DbSession.DbType.VARCHAR2,1,sUpdateUserSeq);
				db.ProcedureInArrayParm("pUPDATE_USER_CHAR_NO",DbSession.DbType.VARCHAR2,1,sUpdateUserCharNo); //メインキャラのみ
				db.ProcedureInArrayParm("pUPDATE_COMMENT_PICKUP",DbSession.DbType.VARCHAR2,1,sUpdateComent);
				db.ProcedureInParm("pUPDATE_RECORD_COUNT",DbSession.DbType.NUMBER,0);
				db.ProcedureInParm("pMODE",DbSession.DbType.VARCHAR2,"CLEAR");
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
			}
		}
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("PICKUP_TITLE_MAINTE");
			db.ProcedureInParm("PPICKUP_TITLE",DbSession.DbType.VARCHAR2,"");
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		Server.Transfer(string.Format("PickupList.aspx?sitecd={0}",lstSiteCd.SelectedValue));
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("PICKUP_TITLE_GET");
			db.ProcedureInParm("PACT_CATEGORY_SEQ",DbSession.DbType.VARCHAR2,lstActCategorySeq.SelectedValue);
			db.ProcedureOutParm("PPICKUP_TITLE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			lblPickupTitle.Text = "ピックアップタイトル：" + db.GetStringValue("PPICKUP_TITLE");
			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");
		}
	}

	protected void dsActCategory_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
	}

	protected void lstSiteCd_SelectedIndexChanged(object sender,EventArgs e) {
		lstActCategorySeq.DataBind();
	}
}
