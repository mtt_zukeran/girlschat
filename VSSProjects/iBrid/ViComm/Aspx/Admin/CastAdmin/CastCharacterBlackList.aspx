<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastCharacterBlackList.aspx.cs" Inherits="CastAdmin_CastCharacterBlackList"
	Title="出演者ブラックリスト設定" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="出演者ブラックリスト設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 850px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									サイトコード
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="140px">
									</asp:DropDownList>
								</td>
								<td class="tdHeaderStyle2">
									ブラック種別
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstBlackType" runat="server" DataSourceID="dsBlackType" DataTextField="CODE_NM" DataValueField="CODE" Width="150px">
									</asp:DropDownList>
								</td>
								<td class="tdHeaderStyle2">
									ログインID
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLoginId" runat="server" MaxLength="15" Width="60px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrLoginId" runat="server" ErrorMessage="ログインIDを入力して下さい。" ControlToValidate="txtLoginId" ValidationGroup="Key">*</asp:RequiredFieldValidator>
									<asp:CustomValidator ID="vdcLoginId" runat="server" ControlToValidate="txtLoginId" ErrorMessage="ログインIDが未登録です" OnServerValidate="vdcLoginId_ServerValidate"
										ValidationGroup="Key">*</asp:CustomValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnSeekCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[ブラックリスト設定]</legend>
						<table border="0" style="width: 800px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle2">
									備考
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtRemarks" runat="server" MaxLength="1000" Width="650px" Rows="3" TextMode="MultiLine"></asp:TextBox>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[設定一覧]</legend>
			<table border="0" style="width: 600px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle2">
						サイトコード
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="120px">
						</asp:DropDownList>
					</td>
					<td class="tdHeaderStyle2">
						ブラック種別
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekBlackType" runat="server" AutoPostBack="true" DataSourceID="dsBlackType" DataTextField="CODE_NM" DataValueField="CODE" Width="150px" OnSelectedIndexChanged="lstSeekBlackType_SelectedIndexChanged">
						</asp:DropDownList>
					</td>
					<asp:PlaceHolder ID="plcHolder" runat="server" Visible="False">
						<td class="tdHeaderStyle2">
							ログインID
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtSeekLoginId" runat="server" MaxLength="15" Width="60px"></asp:TextBox>
						</td>
					</asp:PlaceHolder>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnRegist" Text="追加" CssClass="seekbutton" CausesValidation="False" OnClick="btnRegist_Click" />
			<br />
			<br />
			<asp:Label ID="lblNotification" runat="server" Text="※ブラックリストに追加した会員は強制退室します" ForeColor="red" Visible="false"></asp:Label>
			<asp:GridView ID="grdCastCharacterBlack" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCastCharacterBlack" AllowSorting="True"
				SkinID="GridViewColor">
				<Columns>
					<asp:TemplateField HeaderText="ログインID">
						<ItemTemplate>
							<asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# string.Format("~/CastAdmin/CastCharacterBlackList.aspx?site={0}&loginid={1}&blacktype={2}",Eval("SITE_CD"),Eval("LOGIN_ID"),Eval("BLACK_TYPE")) %>'
								Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
							<itemstyle horizontalalign="Left" wrap="false" />
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="ﾊﾝﾄﾞﾙ名">
						<ItemStyle HorizontalAlign="Left" Wrap="false" />
						<ItemTemplate>
							<asp:Label runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>' ID="lblHandleNm" />
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField DataField="REMARKS" HeaderText="備考">
						<ItemStyle HorizontalAlign="Left" Wrap="true" />
					</asp:BoundField>
					<asp:BoundField DataField="REGIST_DATE" HeaderText="登録日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
						<ItemStyle HorizontalAlign="Center" Wrap="false" />
					</asp:BoundField>
					<asp:BoundField DataField="UPDATE_DATE" HeaderText="更新日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HtmlEncode="False">
						<ItemStyle HorizontalAlign="Center" Wrap="false" />
					</asp:BoundField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br />
				<a class="reccount">Current viewing page
					<%=grdCastCharacterBlack.PageIndex + 1%>
					of
					<%=grdCastCharacterBlack.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsCastCharacterBlack" runat="server" SelectMethod="GetPageCollection" TypeName="CastCharacterBlack" SelectCountMethod="GetPageCount"
		EnablePaging="True" OnSelected="dsCastCharacterBlack_Selected" OnSelecting="dsCastCharacterBlack_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pBlackType" Type="String" />
			<asp:Parameter Name="pLoginId" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsBlackType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="13" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrLoginId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdcLoginId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
