﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: プロフィール画像一覧
--	Progaram ID		: ProfilePicList
--
--  Creation Date	: 2010.05.17
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater				Update Explain
  2010/06/29	伊藤和明			検索項目に日付を追加

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Cast_ProfilePicList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			FirstLoad();
			InitPage();

			string sPicTypeSeq = string.Empty;
			string sPicTypeValueSeq = string.Empty;
			if (!iBridUtil.GetStringValue(Request.QueryString["pictypeseq"]).Equals(string.Empty)) {
				sPicTypeSeq = iBridUtil.GetStringValue(Request.QueryString["pictypeseq"]);
				sPicTypeValueSeq = iBridUtil.GetStringValue(Request.QueryString["pictypevalueseq"]);
				if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
					lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
				}
			}
			this.InitCastPicAttrType(sPicTypeSeq);
			this.InitCastPicAttrTypeValue(sPicTypeValueSeq);

			GetList();
		}
	}

	protected void dsCastPic_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void FirstLoad() {
		grdPic.PageSize = 100;
		grdPic.DataSourceID = "";
		DataBind();
		if (Session["SiteCd"].ToString().Equals("")) {
			lstSiteCd.Items.Insert(0,new ListItem("",""));
		}
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["site"]);
		if (!sSiteCd.Equals(string.Empty)) {
			lstSiteCd.SelectedValue = sSiteCd;
		}
		lstSiteCd.DataSourceID = "";
	}

	private void InitPage() {
		grdPic.DataSourceID = "";
		DataBind();
		recCount = "0";
	}

	private void ClearField() {
		lstSiteCd.SelectedIndex = 0;
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid)
			return;
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.Response.Redirect(this.Request.Url.PathAndQuery);
	}

	protected void btnDelete_OnClick(object sender,EventArgs e) {
		string sSiteCd = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][0]).ToString();
		string sUserSeq = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][1]).ToString();
		string sUserCharNo = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][2]).ToString();
		string sPicSeq = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][3]).ToString();
		string sLoginId = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][4]).ToString();
		DeletePicture(sPicSeq,sUserSeq,sUserCharNo,sSiteCd,sLoginId);
		UpdateProfilePicture(sSiteCd,sUserSeq,sUserCharNo,sPicSeq,"1","0",null,null);
		Server.Transfer(string.Format("ProfilePicList.aspx?sitecd={0}",sSiteCd));
	}

	private void UpdateProfilePicture(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pPicSeq,
		string pDelFlag,
		string pProfilePicFlag,
		string pTitle,
		string pDoc
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_PIC_PROFILE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.VARCHAR2,pPicSeq);
			db.ProcedureInParm("pDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureInParm("pPROFILE_PIC_FLAG",DbSession.DbType.NUMBER,pProfilePicFlag);
			db.ProcedureInParm("pPIC_TYPE",DbSession.DbType.NUMBER,ViCommConst.ATTACHED_PROFILE);
			db.ProcedureInParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			db.ProcedureInParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			db.ProcedureInParm("PPIC_TITLE",DbSession.DbType.VARCHAR2,pTitle);
			db.ProcedureInParm("PPIC_DOC",DbSession.DbType.VARCHAR2,pDoc);
			db.ProcedureInParm("PCAST_PIC_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,ViCommConst.DEFAULT_CAST_PIC_ATTR_TYPE_SEQ.ToString());
			db.ProcedureInParm("PCAST_PIC_ATTR_SEQ",DbSession.DbType.VARCHAR2,ViCommConst.DEFAULT_CAST_PIC_ATTR_SEQ.ToString());
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
		}
	}

	private void DeletePicture(string pFileSeq,string pUserSeq,string pUserCharNo,string siteCd,string pLoginId) {
		string sFileNm = "";
		string sWebPhisicalDir = "";
		using (Site oSite = new Site()) {
			oSite.GetValue(siteCd,"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
		}


		using (Impersonator oImpersonator = new Impersonator(ViCommConst.FILE_UPLOAD_USERNAME,"",ViCommConst.FILE_UPLOAD_PASSWORD)) {

			sFileNm = iBridUtil.addZero(pFileSeq,ViCommConst.OBJECT_NM_LENGTH);

			string sFullPath = sWebPhisicalDir +
								ViCommConst.PIC_DIRECTRY + "\\" + siteCd + "\\Operator\\" + pLoginId + "\\" +
								sFileNm + ViCommConst.PIC_FOODER;

			if (System.IO.File.Exists(sFullPath)) {
				System.IO.File.Delete(sFullPath);
			}


			sFullPath = sWebPhisicalDir +
								ViCommConst.PIC_DIRECTRY + "\\" + siteCd + "\\Operator\\" + pLoginId + "\\" +
								sFileNm + ViCommConst.PIC_FOODER_SMALL;

			if (System.IO.File.Exists(sFullPath)) {
				System.IO.File.Delete(sFullPath);
			}
		}
	}

	protected void btnClose_OnClick(object sender,EventArgs e) {
		string sSiteCd = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][0]).ToString();
		string sUserSeq = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][1]).ToString();
		string sUserCharNo = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][2]).ToString();
		string sPicSeq = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][3]).ToString();
		UpdateProfilePictureNotPublishFlag(sSiteCd,sUserSeq,sUserCharNo,sPicSeq,ViCommConst.FLAG_ON);

		bool bTxNotPublicNotice;
		using (ManageCompany oManageCompany = new ManageCompany()) {
			bTxNotPublicNotice = oManageCompany.IsAvailableService(ViCommConst.RELEASE_CAST_OBJ_NON_PUBLIC_INFO,2);
		}

		if (bTxNotPublicNotice) {
			Server.Transfer(string.Format("./TxCastObjNonPublicMail.aspx?site={0}&seeksitecd={1}&userseq={2}&usercharNo={3}&mailtype={4}&return={5}",sSiteCd,lstSiteCd.SelectedValue,sUserSeq,sUserCharNo,ViCommConst.MAIL_TP_CAST_PROF_PIC_NP,"ProfilePicList"));
		} else {
			Server.Transfer(string.Format("ProfilePicList.aspx?sitecd={0}&seeksitecd={1}",sSiteCd,lstSiteCd.SelectedValue));
		}
	}

	protected void btnUnClose_OnClick(object sender,EventArgs e) {
		string sSiteCd = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][0]).ToString();
		string sUserSeq = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][1]).ToString();
		string sUserCharNo = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][2]).ToString();
		string sPicSeq = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][3]).ToString();
		UpdateProfilePictureNotPublishFlag(sSiteCd,sUserSeq,sUserCharNo,sPicSeq,ViCommConst.FLAG_OFF);
		Server.Transfer(string.Format("ProfilePicList.aspx?sitecd={0}&seeksitecd={1}",sSiteCd,lstSiteCd.SelectedValue));
	}

	private void UpdateProfilePictureNotPublishFlag(string pSiteCd,string pUserSeq,string pUserCharNo,string pPicSeq,int pNotPublishFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_PIC_PROFILE_CLOSE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.VARCHAR2,pPicSeq);
			db.ProcedureInParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER,pNotPublishFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	protected string GetClientScript(object pPicUrl) {
		string sRoot = Request.Url.Authority + ConfigurationManager.AppSettings["Root"];
		return string.Format("javascript:win=window.open('http://{1}/ViewPic.html?url={0}','PictureZoom','width=300,height=320,resizable=yes,directories=no,scrollbars=no' , false);win.focus();return false;",pPicUrl,sRoot);
	}

	private void GetList() {
		grdPic.DataSourceID = "dsCastPic";
		grdPic.PageIndex = 0;
		grdPic.DataBind();
		pnlCount.DataBind();
	}

	protected void dsCastPic_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = string.IsNullOrEmpty(this.txtUploadDayFrom.Text.Replace("/",string.Empty)) ? null : this.txtUploadDayFrom.Text;
		e.InputParameters[2] = string.IsNullOrEmpty(this.txtUploadDayTo.Text.Replace("/",string.Empty)) ? null : this.txtUploadDayTo.Text;
		e.InputParameters[3] = chkNotApproveFlag.Checked ? ViCommConst.FLAG_ON_STR : null;
		e.InputParameters[4] = chkProfilePicFlag.Checked ? ViCommConst.FLAG_ON_STR : null;
		e.InputParameters[5] = lstCastPicAttrType.SelectedValue;
		e.InputParameters[6] = lstCastPicAttrTypeValue.SelectedValue;
		e.InputParameters[7] = this.txtLoginId.Text.TrimEnd();
	}

	protected void grdPic_DataBound(object sender,EventArgs e) {
		// 公開・非公開の表示切替
		foreach (GridViewRow gvr in grdPic.Rows) {
			Button btnUnClose = (Button)gvr.FindControl("btnUnClose") as Button;
			Button btnClose = (Button)gvr.FindControl("btnClose") as Button;
			Label lbl = (Label)gvr.FindControl("lblClosed") as Label;
			if (lbl.Text.Equals(ViCommConst.FLAG_ON_STR)) {
				btnUnClose.Visible = true;
				btnClose.Visible = false;
			} else {
				btnUnClose.Visible = false;
				btnClose.Visible = true;
			}
		}
	}

	protected string GetProfilePicMark(object pProfilePicFlag) {
		if (pProfilePicFlag.ToString().Equals(ViCommConst.FLAG_ON_STR)) {
			return "★";
		} else {
			return string.Empty;
		}
	}

	protected void lstCastPicAttrType_SelectedIndexChanged(object sender, EventArgs e) {
		this.InitCastPicAttrTypeValue(string.Empty);
	}

	protected void dsCastPicAttrType_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
	}

	protected void dsCastPicAttrTypeValue_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = lstCastPicAttrType.SelectedValue;
	}

	protected void InitCastPicAttrType(string pCastPicAttrType) {
		lstCastPicAttrType.DataSourceID = "dsCastPicAttrType";
		lstCastPicAttrType.DataBind();
		lstCastPicAttrType.Items.Insert(0, new ListItem(string.Empty, string.Empty));
		lstCastPicAttrType.DataSourceID = string.Empty;
		if (pCastPicAttrType.Equals(string.Empty) ||
			lstCastPicAttrType.Items.FindByValue(pCastPicAttrType) == null) {
			lstCastPicAttrType.SelectedIndex = 0;
		} else {
			lstCastPicAttrType.SelectedValue = pCastPicAttrType;
		}
	}

	protected void InitCastPicAttrTypeValue(string pCastPicAttrTypeValue) {
		lstCastPicAttrTypeValue.DataSourceID = "dsCastPicAttrTypeValue";
		lstCastPicAttrTypeValue.DataBind();
		lstCastPicAttrTypeValue.Items.Insert(0, new ListItem(string.Empty, string.Empty));
		lstCastPicAttrTypeValue.DataSourceID = string.Empty;

		if (pCastPicAttrTypeValue.Equals(string.Empty) ||
			lstCastPicAttrTypeValue.Items.FindByValue(pCastPicAttrTypeValue) == null) {
			lstCastPicAttrTypeValue.SelectedIndex = 0;
		} else {
			lstCastPicAttrTypeValue.SelectedValue = pCastPicAttrTypeValue;
		}
	}

	protected void lstSiteCd_SelectedIndexChanged(object sender, EventArgs e) {
		this.InitCastPicAttrType(string.Empty);
		this.InitCastPicAttrTypeValue(string.Empty);
	}

	protected void lnkMainte_Command(object sender,CommandEventArgs e) {
		string[] sKey = e.CommandArgument.ToString().Split(':');
		Response.Redirect(string.Format("../CastAdmin/ProfilePicMainte.aspx?sitecd={0}&userseq={1}&usercharno={2}&picseq={3}&return=ProfilePicList&seeksitecd={4}",sKey[0],sKey[1],sKey[2],sKey[3],lstSiteCd.SelectedValue));
	}

}
