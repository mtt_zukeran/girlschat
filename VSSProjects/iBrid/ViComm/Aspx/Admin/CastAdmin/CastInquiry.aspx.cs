﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者検索
--	Progaram ID		: CastInquiry
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;
using System.Text;

public partial class Cast_CastInquiry:System.Web.UI.Page {
	private string recCount = "";
	private int siteCount;
	private const int SITE_START_COL = 5;
	private const int SITE_END_COL = 10;

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			InitPage();
			lstOnlineStatus.Items.Insert(0,new ListItem("",""));
			lstOnlineStatus.Items.Add(new ListItem("稼動中","*"));
			lstOnlineStatus.DataSourceID = "";
			lstOnlineStatus.SelectedIndex = 0;

			lstProductionSeq.Items.Insert(0,new ListItem("",""));
			lstProductionSeq.DataSourceID = "";
			lstProductionSeq.SelectedIndex = 0;

			if (Session["SiteCd"].ToString().Equals("")) {
				lstSiteCd.Items.Insert(0,new ListItem("",""));
			}
			lstSiteCd.DataSourceID = "";
			if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
				lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			} else {
				lstSiteCd.SelectedIndex = 0;
			}

			lstUserStatus.Items.Insert(0,new ListItem("",""));
			lstUserStatus.DataSourceID = "";
			lstUserStatus.SelectedIndex = 0;
		}
	}

	protected void dsCast_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		grdCast.PageSize = 100;
		grdCast.DataSourceID = "";

		// Available Serive?
		using (ManageCompany oCompany = new ManageCompany()) {
			plcInvalidBankAccount.Visible = oCompany.IsAvailableService(ViCommConst.RELEASE_VALIDATE_ACCOUNT_INFO);
		}

		pnlInfo.Visible = false;
		ClearField();
		DataBind();

		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);
		this.btnCSV.Visible = false;
	}

	private void ClearField() {
		lstOnlineStatus.SelectedIndex = 0;
		lstProductionSeq.SelectedIndex = 0;
		lstSiteCd.SelectedIndex = 0;
		txtLoginId.Text = "";
		txtCastNm.Text = "";
		txtEmailAddr.Text = "";
		txtRegistDayFrom.Text = "";
		txtRegistDayTo.Text = "";
		txtLastLoginDayFrom.Text = "";
		txtLastLoginDayTo.Text = "";
		txtTotalPaymentAmtFrom.Text = "";
		txtTotalPaymentAmtTo.Text = "";
		rdoNonExistMailAddrFlag.SelectedValue = "";
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (IsValid) {
			GetList();
		}
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnCSV_Click(object sender, EventArgs e) {
		if (grdCast.Rows.Count == 0) {
			return;
		}

		StringBuilder oCSVData = new StringBuilder();
		using (Cast oCast = new Cast()) {
			bool bUseNotPaymentAmt = false;
			using (DataSet oDataSet = oCast.GetCSVList(
				this.lstOnlineStatus.SelectedValue, 
				this.txtLoginId.Text, 
				this.lstProductionSeq.SelectedValue, 
				this.txtCastNm.Text, 
				this.lstSiteCd.SelectedValue, 
				this.lstUserStatus.SelectedValue, 
				this.txtEmailAddr.Text, 
				this.txtRegistDayFrom.Text, 
				this.txtRegistDayTo.Text, 
				this.txtLastLoginDayFrom.Text, 
				this.txtLastLoginDayTo.Text, 
				this.txtTotalPaymentAmtFrom.Text, 
				this.txtTotalPaymentAmtTo.Text, 
				this.chkInvalidBankAccountFlag.Checked, 
				this.rdoNonExistMailAddrFlag.SelectedValue,
				bUseNotPaymentAmt)) {

				if (oDataSet.Tables[0].Rows.Count == 0) {
					return;
				}

				string sNotPaymentAmtHeader = string.Empty;
				string sNotPaymentAmt = string.Empty;
				if (bUseNotPaymentAmt) {
					sNotPaymentAmtHeader = ",未清算報酬累計";
				}

				// header
				oCSVData.AppendLine(string.Format("ログインID,ログインPASS,メールアドレス{0}", sNotPaymentAmtHeader));

				foreach (DataRow oDataRow in oDataSet.Tables[0].Rows) {
					if (bUseNotPaymentAmt) {
						sNotPaymentAmt = string.Format(",{0}",oDataRow["NOT_PAYMENT_AMT"]);
					}
					oCSVData.AppendFormat("{0},{1},{2}{3}", oDataRow["LOGIN_ID"], oDataRow["LOGIN_PASSWORD"], oDataRow["EMAIL_ADDR"], sNotPaymentAmt);
					oCSVData.AppendLine();
				}

				Response.ContentType = "application/download";
				Response.AppendHeader("Content-Disposition", "attachment;filename=CastListBasic.csv");
				Response.ContentEncoding = System.Text.Encoding.GetEncoding("Shift_JIS");
				Response.Write(oCSVData);
				Response.End();
			}
		}
	}

	protected void grdCast_DataBound(object sender, EventArgs e) {
		int i = SITE_START_COL;

		if (grdCast.HeaderRow == null) {
			return;
		}
		using (Site oSite = new Site()) {
			DataSet ds = oSite.GetList();
			foreach (DataRow dr in ds.Tables[0].Rows) {
				grdCast.HeaderRow.Cells[i++].Text = dr["SITE_MARK"].ToString();
				if (i > SITE_END_COL) {
					break;
				}
			}
		}
		for (int j = i + 1;j <= 10;j++) {
			grdCast.HeaderRow.Cells[j].Text = "";
		}
	}

	protected void grdCast_RowCreated(object sender,GridViewRowEventArgs e) {
		if ((e.Row.RowType == DataControlRowType.Header)) {
			using (Site oSite = new Site()) {
				DataSet ds = oSite.GetList();
				siteCount = ds.Tables[0].Rows.Count;
			}
		}
		if ((e.Row.RowType == DataControlRowType.DataRow) || (e.Row.RowType == DataControlRowType.Header)) {
			for (int i = SITE_START_COL + siteCount;i <= SITE_END_COL;i++) {
				e.Row.Cells[i].Visible = false;
			}
		}
	}

	private void GetList() {
		grdCast.DataSourceID = "dsCast";
		grdCast.PageIndex = 0;
		grdCast.DataBind();
		pnlCount.DataBind();
		pnlInfo.Visible = true;

		int iCompare = string.Compare(Session["AdminType"].ToString(), ViCommConst.RIGHT_SITE_MANAGER);
		this.btnCSV.Visible = (iCompare >= 0);
	}


	protected void dsCast_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		SetupCondition();

		e.InputParameters[0] = lstOnlineStatus.SelectedValue;
		e.InputParameters[1] = txtLoginId.Text;
		e.InputParameters[2] = lstProductionSeq.SelectedValue;
		e.InputParameters[3] = txtCastNm.Text;
		e.InputParameters[4] = lstSiteCd.SelectedValue;
		e.InputParameters[5] = lstUserStatus.SelectedValue;
		e.InputParameters[6] = txtEmailAddr.Text;
		e.InputParameters[7] = txtRegistDayFrom.Text;
		e.InputParameters[8] = txtRegistDayTo.Text;
		e.InputParameters[9] = txtLastLoginDayFrom.Text;
		e.InputParameters[10] = txtLastLoginDayTo.Text;
		e.InputParameters[11] = txtTotalPaymentAmtFrom.Text;
		e.InputParameters[12] = txtTotalPaymentAmtTo.Text;
		e.InputParameters[13] = chkInvalidBankAccountFlag.Checked;
		e.InputParameters[14] = rdoNonExistMailAddrFlag.SelectedValue;
	}

	private void SetupCondition() {
		if ((!txtRegistDayFrom.Text.Equals("")) || (!txtRegistDayTo.Text.Equals(""))) {
			if (txtRegistDayFrom.Text.Equals("")) {
				txtRegistDayFrom.Text = txtRegistDayTo.Text;
			} else if (txtRegistDayTo.Text.Equals("")) {
				txtRegistDayTo.Text = txtRegistDayFrom.Text;
			}
		}

		if ((!txtLastLoginDayFrom.Text.Equals("")) || (!txtLastLoginDayTo.Text.Equals(""))) {
			if (txtLastLoginDayFrom.Text.Equals("")) {
				txtLastLoginDayFrom.Text = txtLastLoginDayTo.Text;
			} else if (txtLastLoginDayTo.Text.Equals("")) {
				txtLastLoginDayTo.Text = txtLastLoginDayFrom.Text;
			}
		}

		if ((!txtTotalPaymentAmtFrom.Text.Equals("")) || (!txtTotalPaymentAmtTo.Text.Equals(""))) {
			if (txtTotalPaymentAmtFrom.Text.Equals("")) {
				txtTotalPaymentAmtFrom.Text = txtTotalPaymentAmtTo.Text;
			} else if (txtTotalPaymentAmtTo.Text.Equals("")) {
				txtTotalPaymentAmtTo.Text = txtTotalPaymentAmtFrom.Text;
			}
		}
	}
}
