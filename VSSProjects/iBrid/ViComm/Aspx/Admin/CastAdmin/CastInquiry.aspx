<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CastInquiry.aspx.cs" Inherits="Cast_CastInquiry" Title="出演者検索"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="出演者検索"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 750px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle">
							出演者状態
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstUserStatus" runat="server" DataSourceID="dsUserStatus" DataTextField="CODE_NM" DataValueField="CODE" Width="180px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							オンライン状態
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstOnlineStatus" runat="server" DataSourceID="dsOnline" DataTextField="CODE_NM" DataValueField="CODE" Width="180px">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle">
							プロダクション
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstProductionSeq" runat="server" DataSourceID="dsProduction" DataTextField="PRODUCTION_NM" DataValueField="PRODUCTION_SEQ" Width="180px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							ログインＩＤ
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtLoginId" runat="server"></asp:TextBox>
						</td>
						<td class="tdHeaderStyle">
							<%= DisplayWordUtil.Replace("出演者名") %>
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtCastNm" runat="server"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							メールアドレス
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtEmailAddr" runat="server"></asp:TextBox>
						</td>
						<td class="tdHeaderStyle">
							登録日
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtRegistDayFrom" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
							&nbsp;〜
							<asp:TextBox ID="txtRegistDayTo" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
							<asp:RangeValidator ID="vdrRegistDayFrom" runat="server" ErrorMessage="登録日Fromを正しく入力して下さい。" ControlToValidate="txtRegistDayFrom" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:RangeValidator ID="vdrRegistDayTo" runat="server" ErrorMessage="登録日Toを正しく入力して下さい。" ControlToValidate="txtRegistDayTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
								Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:CompareValidator ID="vdcRegistDayFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtRegistDayFrom" ControlToValidate="txtRegistDayTo"
								Operator="GreaterThanEqual" ValidationGroup="Key" Display="Dynamic">*</asp:CompareValidator>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							報酬累計額
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtTotalPaymentAmtFrom" runat="server" MaxLength="9" Width="70px"></asp:TextBox>&nbsp;〜
							<asp:TextBox ID="txtTotalPaymentAmtTo" runat="server" MaxLength="9" Width="70px"></asp:TextBox>
							<asp:CompareValidator ID="vdcTotalPaymentAmtFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtTotalPaymentAmtFrom" ControlToValidate="txtTotalPaymentAmtTo"
								Operator="GreaterThanEqual" ValidationGroup="Key" Type="Integer">*</asp:CompareValidator>
						</td>
						<td class="tdHeaderStyle2">
							最終ログイン日
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtLastLoginDayFrom" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
							&nbsp;〜
							<asp:TextBox ID="txtLastLoginDayTo" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
							<asp:RangeValidator ID="vdrLastLoginDayFrom" runat="server" ErrorMessage="最終ﾛｸﾞｲﾝ日Fromを正しく入力して下さい。" ControlToValidate="txtLastLoginDayFrom" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:RangeValidator ID="vdrLastLoginDayTo" runat="server" ErrorMessage="最終ﾛｸﾞｲﾝ日Toを正しく入力して下さい。" ControlToValidate="txtLastLoginDayTo" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:CompareValidator ID="vdcLastLoginDayFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtLastLoginDayFrom" ControlToValidate="txtLastLoginDayTo"
								Operator="GreaterThanEqual" ValidationGroup="Key" Display="Dynamic">*</asp:CompareValidator>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							メールアドレス状態
						</td>
						<td class="tdDataStyle">
							<asp:RadioButtonList ID="rdoNonExistMailAddrFlag" runat="server" RepeatDirection="Horizontal">
								<asp:ListItem Text="全て" Value=""></asp:ListItem>
								<asp:ListItem Text="正常" Value="0"></asp:ListItem>
								<asp:ListItem Text="ﾌｨﾙﾀﾘﾝｸﾞｴﾗｰ" Value="1"></asp:ListItem>
								<asp:ListItem Text="不通ｴﾗｰ" Value="2"></asp:ListItem>
							</asp:RadioButtonList>
						</td>
						<asp:PlaceHolder ID="plcInvalidBankAccount" runat="server" Visible="true">
							<td class="tdHeaderStyle2">
								口座情報無効者
							</td>
							<td class="tdDataStyle">
								<asp:CheckBox ID="chkInvalidBankAccountFlag" runat="server" Text="支払口座が無効の出演者を選択する" />
							</td>
						</asp:PlaceHolder>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="True" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
				<asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" CausesValidation="False" OnClick="btnCSV_Click" />
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>
					<%= DisplayWordUtil.Replace("[出演者一覧]") %>
				</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count
						<%#GetRecCount() %>
					</a>
					<br />
					<a class="reccount">Current viewing page
						<%=grdCast.PageIndex + 1%>
						of
						<%=grdCast.PageCount%>
					</a>
				</asp:Panel>
				&nbsp;
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="500px">
					<asp:GridView ID="grdCast" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCast" SkinID="GridView" OnDataBound="grdCast_DataBound"
						OnRowCreated="grdCast_RowCreated">
						<Columns>
							<asp:BoundField DataField="LOGIN_ID" HeaderText="ID"></asp:BoundField>
							<asp:BoundField DataField="PRODUCTION_NM" HeaderText="プロダクション名"></asp:BoundField>
							<asp:BoundField DataField="MANAGER_NM" HeaderText="担当名"></asp:BoundField>
							<asp:TemplateField HeaderText="氏名">
								<ItemTemplate>
									<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# string.Format("../Cast/CastView.aspx?loginid={0}&return={1}",Eval("LOGIN_ID"),"CastInquiry.aspx") %>'
										Text='<%# Eval("CAST_NM") %>'></asp:HyperLink>
									<br />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="USER_STATUS_NM" HeaderText="出演者状態">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
							<asp:TemplateField HeaderText="M1">
								<ItemTemplate>
									<asp:Image ID="imgSysOnlineImg1" runat="server" ImageUrl='<%# Eval("SYS_ONLINE_IMG_PATH1","../{0}") %>' Visible='<%#Eval("SYS_ONLINE_IMG_PATH1").ToString().Equals("")==false%>' />
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="12px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="M2">
								<ItemTemplate>
									<asp:Image ID="imgSysOnlineImg2" runat="server" ImageUrl='<%# Eval("SYS_ONLINE_IMG_PATH2","../{0}") %>' Visible='<%#Eval("SYS_ONLINE_IMG_PATH2").ToString().Equals("")==false%>' />
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="12px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="M3">
								<ItemTemplate>
									<asp:Image ID="imgSysOnlineImg3" runat="server" ImageUrl='<%# Eval("SYS_ONLINE_IMG_PATH3","../{0}") %>' Visible='<%#Eval("SYS_ONLINE_IMG_PATH3").ToString().Equals("")==false%>' />
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="12px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="M4">
								<ItemTemplate>
									<asp:Image ID="imgSysOnlineImg4" runat="server" ImageUrl='<%# Eval("SYS_ONLINE_IMG_PATH4","../{0}") %>' Visible='<%#Eval("SYS_ONLINE_IMG_PATH4").ToString().Equals("")==false%>' />
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="12px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="M5">
								<ItemTemplate>
									<asp:Image ID="imgSysOnlineImg5" runat="server" ImageUrl='<%# Eval("SYS_ONLINE_IMG_PATH5","../{0}") %>' Visible='<%#Eval("SYS_ONLINE_IMG_PATH5").ToString().Equals("")==false%>' />
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="12px" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="M6">
								<ItemTemplate>
									<asp:Image ID="imgSysOnlineImg6" runat="server" ImageUrl='<%# Eval("SYS_ONLINE_IMG_PATH6","../{0}") %>' Visible='<%#Eval("SYS_ONLINE_IMG_PATH6").ToString().Equals("")==false%>' />
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Center" Width="12px" />
							</asp:TemplateField>
							<asp:BoundField DataField="UPDATE_DATE" HeaderText="更新日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
								<ItemStyle HorizontalAlign="Center" />
							</asp:BoundField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsCast" runat="server" SelectMethod="GetPageCollection" TypeName="Cast" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsCast_Selected" OnSelecting="dsCast_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pOnline" Type="String" />
			<asp:Parameter Name="pLoginId" Type="String" />
			<asp:Parameter Name="pProductionSeq" Type="String" />
			<asp:Parameter Name="pCastNm" Type="String" />
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pUserStatus" Type="String" />
			<asp:Parameter Name="pEmailAddr" Type="String" />
			<asp:Parameter Name="pRegistDayFrom" Type="String" />
			<asp:Parameter Name="pRegistDayTo" Type="String" />
			<asp:Parameter Name="pLastLoginDayFrom" Type="String" />
			<asp:Parameter Name="pLastLoginDayTo" Type="String" />
			<asp:Parameter Name="pTotalPaymentAmtFrom" Type="String" />
			<asp:Parameter Name="pTotalPaymentAmtTo" Type="String" />
			<asp:Parameter Name="pBankAccountInvalidFlag" Type="Boolean" />
			<asp:Parameter Name="pNonExistMailAddrFlag" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsProduction" runat="server" SelectMethod="GetList" TypeName="Production"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUserStatus" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="51" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsOnLine" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="52" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtTotalPaymentAmtFrom" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum2" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtTotalPaymentAmtTo" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrRegistDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrRegistDayTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdcRegistDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdrLastLoginDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdrLastLoginDayTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdcLastLoginDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vdcTotalPaymentAmtFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskRegistDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtRegistDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskRegistDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtRegistDayTo">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskLastLoginDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtLastLoginDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskLastLoginDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtLastLoginDayTo">
	</ajaxToolkit:MaskedEditExtender>
</asp:Content>
