﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 申し込み中キャスト一覧
--	Progaram ID		: ApplyCastList
--
--  Creation Date	: 2009.12.14
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;
using System.Text;

public partial class CastAdmin_ApplyCastList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			SysPrograms.SetupTime(lstApplyTimeFrom,lstApplyTimeTo);
			InitPage();
		}
	}

	private void InitPage() {
		pnlKey.Enabled = true;
		grdCast.PageSize = 100;
		ClearField();
		lstSiteCd.DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		DataBind();

		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);

        int nRecCount = 0;
        int.TryParse(recCount, out nRecCount);
        btnCsv.Enabled = (nRecCount > 0)?true:false;

	}

	private void ClearField() {
		lstSiteCd.SelectedIndex = 0;
		lstApplyTimeFrom.SelectedIndex = 0;
		lstApplyTimeTo.SelectedIndex = 0;
		txtTel.Text = "";
		txtEMailAddr.Text = "";
		txtCastNm.Text = "";
		txtApplyDayFrom.Text = "";
		txtApplyDayTo.Text = "";
        lblErrorMessage.Text = "";
        lblErrorMessage.Visible = false;
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (IsValid) {
			GetList();
		}
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	private void GetList() {
		grdCast.DataSourceID = "dsTempRegist";
		grdCast.PageIndex = 0;
		grdCast.DataBind();
		pnlCount.DataBind();

        int nRecCount = 0;
        int.TryParse(recCount, out nRecCount);
        btnCsv.Enabled = (nRecCount > 0) ? true : false;
        lblErrorMessage.Text = "";
        lblErrorMessage.Visible = false;
    }

	protected void dsTempRegist_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsTempRegist_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = txtTel.Text.TrimEnd();
		e.InputParameters[2] = txtEMailAddr.Text.TrimEnd();
		e.InputParameters[3] = txtCastNm.Text.TrimEnd();
		e.InputParameters[4] = txtApplyDayFrom.Text;
		e.InputParameters[5] = txtApplyDayTo.Text;
		e.InputParameters[6] = lstApplyTimeFrom.SelectedValue;
		e.InputParameters[7] = lstApplyTimeTo.SelectedValue;
	}

	protected string GetAgeByBirthDay(object pBirthday) {
		if(pBirthday.ToString().Length==8){
			return ViCommPrograms.Age(string.Format("{0}/{1}/{2}",pBirthday.ToString().Substring(0,4),pBirthday.ToString().Substring(4,2),pBirthday.ToString().Substring(6,2))).ToString();
		}else{
			return string.Empty;
		}
	}

    //2017/04/19 yogi CSVファイルへ出力する機能を追加
    protected void btnCSV_Click(object sender, EventArgs e)
    {
        string sCsvPassword = string.Empty;
        using (SysEx oSysEx = new SysEx())
        {
            oSysEx.GetValue("CSV_PASSWORD", out sCsvPassword);
        }
        if (!sCsvPassword.Equals(txtCsvPassword.Text))
        {
            lblErrorMessage.Text = "パスワードが一致しません。";
            lblErrorMessage.Visible = true;
            return;
        }

        string fileName = "ApplyCastList.csv";

        using (TempRegist obTempRegist = new TempRegist())
        {
            using (DataSet ds = obTempRegist.GetCSVList(
                                                lstSiteCd.SelectedValue,
                                                txtTel.Text.TrimEnd(),
                                                txtEMailAddr.Text.TrimEnd(),
                                                txtCastNm.Text.TrimEnd(),
                                                txtApplyDayFrom.Text,
                                                txtApplyDayTo.Text,
                                                lstApplyTimeFrom.SelectedValue,
                                                lstApplyTimeTo.SelectedValue))
            {
                if (ds.Tables[0].Rows.Count == 0)
                {
                    return;
                }

                StringBuilder sbCSVData = new StringBuilder();

                // header部
                sbCSVData.Append("電話番号,");
                sbCSVData.Append("ﾒｰﾙｱﾄﾞﾚｽ");

                sbCSVData.AppendLine("");

                //Data部
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    sbCSVData.Append(dr["TEL"]).Append(",");
                    sbCSVData.Append(dr["EMAIL_ADDR"]);

                    sbCSVData.AppendLine();
                }

                Response.ContentType = "application/download";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + fileName);
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("Shift_JIS");

                Response.Write(sbCSVData.ToString());
                Response.End();

            }
        }
    }
}
