<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="MultiPickupMainte.aspx.cs" Inherits="Cast_MultiPickupMainte" Title="ピックアップ設定"
    ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="ピックアップ設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel ID="pnlHeader" runat="server">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlRegister" runat="server">
                    <table border="0" style="width: 640px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                サイト
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    Enabled="false" DataValueField="SITE_CD" Width="240px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                ピックアップ区分
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstPickup" runat="server" AutoPostBack="true" DataSourceID="dsPickupType"
                                    DataTextField="CODE_NM" DataValueField="CODE">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle2">
                                ピックアップタイトル
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtNewPickupTitle" runat="server" Width="300px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="vdrNewPickupTitle" runat="server" ControlToValidate="txtNewPickupTitle"
                                    ErrorMessage="ピックアップタイトルを正しく入力して下さい" ValidationGroup="Key">*</asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server"
                                    HighlightCssClass="validatorCallout" TargetControlID="vdrNewPickupTitle">
                                </ajaxToolkit:ValidatorCalloutExtender>
                            </td>
                        </tr>
                    </table>
                    <asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seektopbutton" OnClick="btnSeek_Click"
                        ValidationGroup="Key" />
                    <asp:Button ID="btnCancelRegister" runat="server" CssClass="seektopbutton" OnClick="btnCancel_Click"
                        Text="キャンセル" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[検索]</legend>
                <table border="0" style="width: 800px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            サイト
                        </td>
                        <td class="tdDataStyle" colspan="3">
                            <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="240px" OnSelectedIndexChanged="lstSeekSiteCd_SelectedIndexChanged"
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            ピックアップ種別
                        </td>
                        <td class="tdDataStyle" colspan="3">
                            <asp:DropDownList ID="lstSeekPickupId" runat="server" DataSourceID="dsPickup" DataTextField="PICKUP_TITLE"
                                DataValueField="PICKUP_ID" Width="240px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            ログインID
							<asp:LinkButton ID="lnkDisplayLoginIdList" runat="server" Text="複数入力" OnClick="lnkDisplayLoginIdList_Click"></asp:LinkButton>
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtLoginId" runat="server"></asp:TextBox>
                        </td>
                        <td class="tdHeaderStyle2">
                            ハンドル名
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtHandelNm" runat="server" MaxLength="120"></asp:TextBox>
                        </td>
                    </tr>
					<asp:Panel ID="pnlLoginIdList" runat="server" Visible="false">
						<tr>
							<td class="tdHeaderStyle">
								ログインID（複数入力）<br />
								<asp:LinkButton ID="lnkHideLoginIdList" runat="server" Text="閉じる" OnClick="lnkHideLoginIdList_Click"></asp:LinkButton>
								<asp:CheckBox ID="chkLoginIdListVisible" runat="server" Visible="false" />
							</td>
							<td class="tdDataStyle" colspan="7">
								<asp:TextBox ID="txtLoginIdList" runat="server" Width="450px" TextMode="multiline" Rows="3"></asp:TextBox>
							</td>
						</tr>
					</asp:Panel>						
                    <tr>
                        <td class="tdHeaderStyle2" id="tdHeaderPickupFlag" runat="server">
                            ピックアップ公開/非公開
                        </td>
                        <td class="tdDataStyle" colspan="3">
                            <asp:RadioButtonList ID="rdoPickupFlag" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="全て" Value="" Selected="true"></asp:ListItem>
                            <asp:ListItem Text="公開のみ" Value="1"></asp:ListItem>
                            <asp:ListItem Text="非公開のみ" Value="0"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <asp:PlaceHolder ID="plcExt" runat="server">
                        <tr>
                            <td class="tdHeaderStyle2">
                                並び替え
                            </td>
                            <td class="tdDataStyle">
                                <asp:RadioButtonList ID="rdoSort" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="true" Text="投稿順" Value=""></asp:ListItem>
                                    <asp:ListItem Text="ピックアップ追加順" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="閲覧回数" Value="2"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td class="tdHeaderStyle2">
                                出演者状態
                            </td>
                            <td class="tdDataStyle">
								<asp:CheckBox ID="chkNormal" runat="server" Text="通常" />
								<asp:CheckBox ID="chkAuthWait" runat="server" Text="認証待ち" ForeColor="SeaGreen" Visible="false" />
								<asp:CheckBox ID="chkHold" runat="server" Text="保留" ForeColor="Blue" />
								<asp:CheckBox ID="chkResigned" runat="server" Text="退会" ForeColor="Maroon" />
								<asp:CheckBox ID="chkStop" runat="server" Text="禁止" ForeColor="Red" />
								<asp:CheckBox ID="chkBan" runat="server" Text="停止" ForeColor="Red" />
                            </td>
                        </tr>
                    </asp:PlaceHolder>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seektopbutton" OnClick="btnListSeek_Click" />
                <asp:Button ID="btnRegister" runat="server" CssClass="seektopbutton" OnClick="btnRegister_Click"
                    Text="追加" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seektopbutton" OnClick="btnClear_Click" />
            </fieldset>
        </asp:Panel>
        <% //*********************************	%>
        <% // ピックアップ一覧					%>
        <% //*********************************	%>
        <asp:Panel ID="pnlGrid" runat="server">
            <fieldset class="fieldset">
                <legend>[ピックアップ一覧]</legend>
                <asp:GridView ID="grdPickup" runat="server" AutoGenerateColumns="False" DataSourceID="dsPickup"
                    AllowSorting="True" SkinID="GridViewColor">
                    <Columns>
                        <asp:BoundField DataField="PICKUP_ID" HeaderText="ID" />
                        <asp:HyperLinkField DataTextField="PICKUP_TITLE" HeaderText="ﾋﾟｯｸｱｯﾌﾟﾀｲﾄﾙ" DataNavigateUrlFields="SITE_CD,PICKUP_ID"
                            DataNavigateUrlFormatString="MultiPickupMainte.aspx?sitecd={0}&amp;pickupid={1}&amp;append=1" />
                        <asp:BoundField DataField="PICKUP_TYPE_NM" HeaderText="ﾋﾟｯｸｱｯﾌﾟ区分" />
                        <asp:TemplateField HeaderText="公開件数">
                            <ItemTemplate>
                                <%# GetPickupCount(Eval("PICKUP_TYPE"),Eval("PICKUP_COUNT"),Eval("PICKUP_PIC_COUNT"),Eval("PICKUP_MOVIE_COUNT"))%>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="登録件数">
                            <ItemTemplate>
                                <%# GetPickupCount(Eval("PICKUP_TYPE"), Eval("REGIST_COUNT"), Eval("REGIST_PIC_COUNT"),Eval("REGIST_MOVIE_COUNT"))%>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ﾄｯﾌﾟﾍﾟｰｼﾞで利用する">
                            <ItemTemplate>
                                <asp:Label ID="lblUseTopPageMark" runat="server" Text='<%# GetFlagMark(Eval("USE_TOP_PAGE_FLAG")) %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="出演者様へ公開する">
                            <ItemTemplate>
                                <asp:Label ID="lblCastPublish" runat="server" Text='<%# ViCommConst.FLAG_ON_STR.Equals(Eval("CAST_PUBLISH_FLAG", "{0}")) ? "公開" : "非公開" %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="公開/非公開">
                            <ItemTemplate>
                                <asp:Label ID="lblPickupFlag" runat="server" Text='<%# ViCommConst.FLAG_ON_STR.Equals(Eval("PICKUP_FLAG", "{0}")) ? "公開" : "非公開" %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="UPDATE_DATE" HeaderText="修正日" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}"
                            HtmlEncode="False">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
            </fieldset>
        </asp:Panel>
        <asp:Panel ID="pnlDtl" runat="server">
            <fieldset class="fieldset">
                <legend>[キャラクター設定]</legend>
                <table border="0">
                    <tr>
                        <td>
                            <table border="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:Button ID="btnSubmitUpdate" runat="server" Text="ピックアップ更新" OnClick="btnSubmitUpdate_Click"
                                            CssClass="seektopbutton" ValidationGroup="Detail" />
                                    </td>
                                    <td>
                                        &nbsp;&nbsp;
                                    </td>
                                    <td>
                                        <asp:Button ID="btnDelete" runat="server" Text="削除" OnClick="btnDelete_Click" CssClass="seektopbutton" Visible="false" />
                                    </td>
                                    <td>
                                        <asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seektopbutton" OnClick="btnCancel_Click"
                                            CausesValidation="False" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <fieldset class="fieldset-inner">
                    <legend>[タイトル設定]</legend>
                    <table border="0">
                        <tr>
                            <td valign="top">
                                <asp:Panel ID="pnlInfo" runat="server">
                                    <% //*********************************	%>
                                    <% // ピックアップ編集					%>
                                    <% //*********************************	%>
                                    <table border="0" cellspacing="0" class="tableStyle">
                                        <tr>
                                            <td class="tdHeaderStyle">
                                                タイトル
                                            </td>
                                            <td class="tdDataStyle">
                                                <asp:TextBox ID="txtPickupTitle" runat="server" MaxLength="40" Width="300px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="vdrPickupTitle" runat="server" ErrorMessage="ピックアップタイトルを入力して下さい。"
                                                    ControlToValidate="txtPickupTitle" ValidationGroup="Detail" Display="None">*</asp:RequiredFieldValidator>
                                            </td>
                                            <td class="tdHeaderStyle">
                                                公開
                                            </td>
                                            <td class="tdDataStyle">
                                                <asp:CheckBox ID="chkPickupFlag" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <% if (this.lstPickupType.SelectedValue.Equals(ViCommConst.PicupTypes.CAST_CHARACTER) || this.lstPickupType.SelectedValue.Equals(ViCommConst.PicupTypes.PROFILE_PIC)) { %>
                                            <td class="tdHeaderStyle">
                                                ピックアップ区分
                                            </td>
                                            <% } else { %>
                                            <td class="tdHeaderStyle2">
                                                ピックアップ区分
                                            </td>
                                            <% } %>
                                            <td class="tdDataStyle">
                                                <asp:DropDownList ID="lstPickupType" runat="server" AutoPostBack="true" DataSourceID="dsPickupType"
                                                    DataTextField="CODE_NM" DataValueField="CODE">
                                                </asp:DropDownList>
                                                <asp:Label ID="lblPickupTypeMsg" runat="server" ForeColor="red" Text="※データが登録されているため変更できません。"></asp:Label>
                                                <% if (this.lstPickupType.SelectedValue.Equals(ViCommConst.PicupTypes.CAST_CHARACTER)) { %>
                                                <asp:Panel ID="pnlLink" runat="server">
                                                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Cast/CastCharacterInquiry.aspx">出演者詳細検索のピックアップ作成</asp:HyperLink>
                                                </asp:Panel>
                                                <%} %>
                                            </td>
                                            <td class="tdHeaderStyle2">
                                                公開期間
                                            </td>
                                            <td class="tdDataStyle">
                                                <asp:TextBox ID="txtPublishStartDate" runat="server" MaxLength="10" Width="100px"></asp:TextBox>&nbsp;〜&nbsp;
                                                <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender1" runat="server" MaskType="DateTime"
                                                    Mask="9999/99/99 99:99" UserDateFormat="YearMonthDay" UserTimeFormat="TwentyFourHour"
                                                    ClearMaskOnLostFocus="true" TargetControlID="txtPublishStartDate">
                                                </ajaxToolkit:MaskedEditExtender>
                                                <asp:TextBox ID="txtPublishEndDate" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
                                                <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender2" runat="server" MaskType="DateTime"
                                                    Mask="9999/99/99 99:99" UserDateFormat="YearMonthDay" UserTimeFormat="TwentyFourHour"
                                                    ClearMaskOnLostFocus="true" TargetControlID="txtPublishEndDate">
                                                </ajaxToolkit:MaskedEditExtender>
                                                <br />
                                                <asp:CustomValidator ID="vdcFromTo" runat="server" ValidationGroup="Detail" OnServerValidate="vdcFromTo_ServerValidate"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                        <% if (this.lstPickupType.SelectedValue.Equals(ViCommConst.PicupTypes.CAST_CHARACTER)) { %>
                                        <tr>
                                            <td class="tdHeaderStyle2">
                                                トップページにて利用する
                                            </td>
                                            <td class="tdDataStyle" colspan="3">
                                                <asp:CheckBox ID="chkUseTopPageFlag" runat="server" />
                                                <asp:CustomValidator ID="vdcUseTopPageFlag" runat="server" ErrorMessage="他のピックアップで利用フラグがONになっています。"
                                                    OnServerValidate="vdcUseTopPageFlag_ServerValidate" ValidationGroup="Detail"
                                                    Display="Dynamic">* 他のピックアップで利用フラグがONになっています。</asp:CustomValidator>
                                            </td>
                                        </tr>
                                        <% } %>
                                        <% if (this.lstPickupType.SelectedValue.Equals(ViCommConst.PicupTypes.PROFILE_PIC)) { %>
                                        <tr>
                                            <td class="tdHeaderStyle<% if (!CheckCastPublishType()) { %>2<% } %>">
                                                プロフィール画像差し替え
                                            </td>
                                            <td class="tdDataStyle" colspan="3">
                                                <asp:CheckBox ID="chkApplayProfilePicFlag" runat="server" />
                                            </td>
                                        </tr>
                                        <% } %>
                                        <% if (CheckCastPublishType()) { %>
                                        <tr>
                                            <td class="tdHeaderStyle2">
                                                出演者様へ公開する
                                            </td>
                                            <td class="tdDataStyle" colspan="3">
                                                <asp:CheckBox ID="chkCastPublishFlag" runat="server" />
                                                ※出演者様の画像・動画管理ページでピックアップタイトルを表示
                                            </td>
                                        </tr>
                                        <% } %>
                                        <asp:PlaceHolder ID="plcHide" runat="server" Visible="false">
                                            <tr>
                                                <td class="tdHeaderStyle2">
                                                    ピックアップヘッダー
                                                </td>
                                                <td colspan="3">
                                                    $NO_TRANS_START;
                                                    <pin:pinEdit ID="txtPickupHeaderDoc" runat="server" UserMode="HTML" DisplayStyle="Style5"
                                                        DocumentView="True" HtmlMode="Body" Ruler="False" ReturnMode="BR" SpellMode="Inline"
                                                        ToolbarConfiguration="ToolbarDesigner" Toolbar="T09SE10116012136267;T22232427282930313233343536;B53SE54SE55"
                                                        FormatMode="Classic" BorderWidth="1px" Height="200px" Width="310px" DocumentWidth="230"
                                                        Font-Bold="False" FontSizeType="px" RelativeBaseImageUrl="/" RelativeBaseLinkUrl="/"
                                                        RelativeImageRoot="/" DebugMode="False"></pin:pinEdit>
                                                    $NO_TRANS_END;
                                                </td>
                                            </tr>
                                        </asp:PlaceHolder>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <fieldset class="fieldset-inner">
                    <legend>[ピックアップ設定]</legend>
                    <table border="0" cellspacing="0">
                        <tr>
                        <asp:Panel ID="pnlAllClear" runat="server" Visible="false">
                            <td valign="top">
                                <asp:Button ID="btnAllClear" runat="server" Text="全件削除" OnClick="btnAllClear_Click"
                                    CssClass="seekbutton" />
                            </td>
                            <td>
                                &nbsp;&nbsp;
                            </td>
                        </asp:Panel>
                            <td valign="top">
                                <asp:Panel ID="pnlCount" runat="server">
                                    <a class="reccount" style="float: none">Record Count
                                        <%= this.DataCount %>
                                    </a>
                                    <br />
                                    <a class="reccount" style="float: none">Current viewing page
                                        <%= this.PageIndex %>
                                        of
                                        <%= this.LastPageIndex %>
                                    </a>
                                </asp:Panel>
                            </td>
                            <td>
                                &nbsp;&nbsp;
                            </td>
                            <td valign="top">
                                <asp:HyperLink ID="lnkPickupObjsMainte" runat="server" Text="ピックアップの一括サイト公開" NavigateUrl='<%# GetNavigateUrl() %>'
                                    Visible='<%# IsLinkVisible() %>'></asp:HyperLink><br />
                                <asp:LinkButton ID="lnkCondition" runat="server" OnClick="lnkCondition_Click" Text="[検索条件表示]" />
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;
                            </td>
                            <td align="left">
                                <asp:Label ID="lblErrorMessage1" runat="server" Text="※背景色が灰色の場合、サイト上に公開されていません。"></asp:Label><br />
                                <asp:Label ID="lblErrorMessage2" runat="server" Text="※ページ移動は、ピックアップ設定が更新可能でないと出来ません。"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <asp:Panel ID="pnlObject" runat="server" Height="530px" ScrollBars="auto">
                        <% //*********************************	%>
                        <% // オブジェクト一覧					%>
                        <% //*********************************	%>
                        <asp:DataList ID="lstObject" runat="server" OnItemDataBound="lstDataList_ItemDataBound">
                            <ItemTemplate>
                                <table border="0" class="tableStyleAutoSize">
                                    <thead>
                                        <tr style="background-color: #E0E0E0">
                                            <td class="Pad">
                                                No
                                            </td>
                                            <td class="Pad">
                                                削除
                                            </td>
                                            <td class="Pad">
                                                公開
                                            </td>
                                            <td class="Pad">
                                                優先度
                                            </td>
                                            <td class="Pad">
                                                <%# IsPic(Eval("PICKUP_TYPE").ToString()) ? "写真" : "動画"%>
                                            </td>
                                            <td class="Pad">
                                                <%= DisplayWordUtil.Replace("出演者ID") %>
                                                /ハンドル名/タイトル/出演者状態/公開期間
                                            </td>
                                            <td class="Pad">
                                                閲覧回数
                                            </td>
                                            <td class="Pad">
                                                Pickup
                                            </td>
                                            <td class="pad">
												投票数
                                            </td>
                                            <td class="pad">
												ｺﾒﾝﾄ数
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr valign="top">
                                            <td valign="middle">
                                                <%# Container.ItemIndex + 1 + pageSize * (this.PageIndex - 1)%>
                                                <asp:HiddenField ID="hdnObjSeq" runat="server" Value='<%# Eval("OBJ_SEQ") %>'></asp:HiddenField>
                                            </td>
                                            <td valign="middle">
                                                <asp:CheckBox ID="chkObjDeleteFlag" runat="server" Checked="false"></asp:CheckBox>
                                            </td>
                                            <td valign="middle">
                                                <asp:CheckBox ID="chkObjPickupFlag" runat="server" Checked='<%# Eval("PICKUP_FLAG").ToString().Equals(ViCommConst.FLAG_ON_STR) %>'>
                                                </asp:CheckBox>
                                            </td>
                                            <td valign="middle">
                                                <asp:TextBox ID="txtPriority" runat="server" MaxLength="3" Width="30px" Text='<%# Eval("PRIORITY")%>'
                                                    Style="text-align: right;">
                                                </asp:TextBox>
                                            </td>
                                            <td class="Pad" style="white-space: nowrap;" valign="middle">
                                                <asp:Panel ID="pnlObjPic" runat="server" Visible='<%# IsPic(Eval("PICKUP_TYPE").ToString()) %>'>
                                                    <a href="<%# PickupHelper.GeneratePickupObjsViewerUrl(this, Eval("SITE_CD").ToString(),Eval("OBJ_SEQ").ToString(), Eval("PICKUP_TYPE").ToString(), ViCommConst.FLAG_OFF) %>">
                                                        <asp:Image ID="imgObjPic" runat="server" Width="70px" ImageUrl='<%# string.Format("~/{0}",Eval("SMALL_PHOTO_IMG_PATH")) %>'>
                                                        </asp:Image>
                                                    </a>
                                                </asp:Panel>
                                                <asp:Panel ID="pnlObjMoive" runat="server" Visible='<%# IsMovie(Eval("PICKUP_TYPE").ToString()) %>'>
                                                    <a href="<%# PickupHelper.GeneratePickupObjsViewerUrl(this, Eval("SITE_CD").ToString(),Eval("OBJ_SEQ").ToString(), Eval("PICKUP_TYPE").ToString(), ViCommConst.FLAG_OFF) %>">
                                                        確認 </a>
                                                </asp:Panel>
                                                <br />
                                                <asp:Label ID="lblClosedMark" runat="server" Text='<%# GetClosedMark(Eval("OBJ_NOT_APPROVE_FLAG"), Eval("OBJ_NOT_PUBLISH_FLAG"), Eval("PIC_TYPE")) %>' />
                                            </td>
                                            <td class="Pad" align="left">
                                                <asp:HyperLink ID="lnlCastView" runat="server" NavigateUrl='<%# string.Format("../Cast/CastView.aspx?loginid={0}&return={1}",Eval("LOGIN_ID"),"MultiPickupMainte.aspx") %>'
                                                    Text='<%# Eval("LOGIN_ID") %>'>
                                                </asp:HyperLink>/
                                                <asp:Label ID="lblObjHandleNm" runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'>
                                                </asp:Label>/
                                                <asp:Label ID="lblObjTitle" runat="server" Text='<%# Eval("OBJ_TITLE").ToString().Equals(string.Empty)?"(未設定)":Eval("OBJ_TITLE") %>'>
                                                </asp:Label>/
                                                <asp:Label ID="lblUserStatus" runat="server" Text='<%# GetUserStatusMark(Eval("USER_STATUS")) %>'></asp:Label>/
                                                <br />
                                                <asp:TextBox ID="txtStartPubDay" runat="server" MaxLength="10" Width="80px" Text='<%# Eval("PICKUP_START_PUB_DAY", "{0:yyyy/MM/dd}") %>'></asp:TextBox>&nbsp;〜&nbsp;
                                                <asp:TextBox ID="txtEndPubDay" runat="server" MaxLength="10" Width="80px" Text='<%# Eval("PICKUP_END_PUB_DAY", "{0:yyyy/MM/dd}") %>'></asp:TextBox>
                                                <br />
                                                <asp:TextBox ID="txtObjComment" runat="server" Columns="60" Rows="3" TextMode="MultiLine"
                                                    Text='<%# Eval("COMMENT_PICKUP") %>'></asp:TextBox>
                                                <asp:Panel ID="pnlError" runat="server" Visible="false">
                                                    <asp:Label ID="lblErrorMessage" runat="server" Text="" ForeColor="red"></asp:Label>
                                                </asp:Panel>
                                            </td>
                                            <td valign="middle" align="right">
                                                <asp:Label ID="lblReadingCount" runat="server" Text='<%# Eval("READING_COUNT","{0:N0}") %>'></asp:Label>
                                            </td>
                                            <td class="Pad">
                                                <span style="color: Red">
                                                    <asp:Literal ID="lblOthers" runat="server" Text='<%# Eval("OTHER_PICKUP").ToString().Replace(",","<br/>") %>'></asp:Literal>
                                                </span>
                                            </td>
                                            <td valign="middle" align="right">
                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("VOTE_COUNT","{0:N0}") %>'></asp:Label>
                                            </td>
                                            <td valign="middle" align="right">
                                                <asp:HyperLink ID="lnkPickupObjsCommentList" runat="server" NavigateUrl='<%# string.Format("PickupObjsCommentList.aspx?sitecd={0}&pickupid={1}&objseq={2}",Eval("SITE_CD"),Eval("PICKUP_ID"),Eval("OBJ_SEQ")) %>'
												Text='<%# Eval("COMMENT_COUNT") %>' Width="50px"
												CssClass="Warp"></asp:HyperLink>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                            <FooterTemplate>
                                <asp:Panel ID="pnlPaging" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="lnkPageFirst" runat="server" CommandName="PAGING" CommandArgument="1"
                                                    OnCommand="lnkPaging_Command" Text="<<" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkPagePrevious" runat="server" CommandName="PAGING" CommandArgument="1"
                                                    OnCommand="lnkPaging_Command" Text="..." />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkPage1" runat="server" CommandName="PAGING" CommandArgument="1"
                                                    OnCommand="lnkPaging_Command" Text="1" />
                                                <asp:Label ID="lblPage1" runat="server" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkPage2" runat="server" CommandName="PAGING" CommandArgument="1"
                                                    OnCommand="lnkPaging_Command" Text="2" />
                                                <asp:Label ID="lblPage2" runat="server" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkPage3" runat="server" CommandName="PAGING" CommandArgument="1"
                                                    OnCommand="lnkPaging_Command" Text="3" />
                                                <asp:Label ID="lblPage3" runat="server" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkPage4" runat="server" CommandName="PAGING" CommandArgument="1"
                                                    OnCommand="lnkPaging_Command" Text="4" />
                                                <asp:Label ID="lblPage4" runat="server" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkPage5" runat="server" CommandName="PAGING" CommandArgument="1"
                                                    OnCommand="lnkPaging_Command" Text="5" />
                                                <asp:Label ID="lblPage5" runat="server" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkPage6" runat="server" CommandName="PAGING" CommandArgument="1"
                                                    OnCommand="lnkPaging_Command" Text="6" />
                                                <asp:Label ID="lblPage6" runat="server" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkPage7" runat="server" CommandName="PAGING" CommandArgument="1"
                                                    OnCommand="lnkPaging_Command" Text="7" />
                                                <asp:Label ID="lblPage7" runat="server" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkPage8" runat="server" CommandName="PAGING" CommandArgument="1"
                                                    OnCommand="lnkPaging_Command" Text="8" />
                                                <asp:Label ID="lblPage8" runat="server" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkPage9" runat="server" CommandName="PAGING" CommandArgument="1"
                                                    OnCommand="lnkPaging_Command" Text="9" />
                                                <asp:Label ID="lblPage9" runat="server" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkPage10" runat="server" CommandName="PAGING" CommandArgument="1"
                                                    OnCommand="lnkPaging_Command" Text="10" />
                                                <asp:Label ID="lblPage10" runat="server" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkPageNext" runat="server" CommandName="PAGING" CommandArgument="1"
                                                    OnCommand="lnkPaging_Command" Text="..." />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkPageLast" runat="server" CommandName="PAGING" CommandArgument="1"
                                                    OnCommand="lnkPaging_Command" Text=">>" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </FooterTemplate>
                        </asp:DataList>
                    </asp:Panel>
                    <asp:Panel ID="pnlCharacter" runat="server" Height="530px" ScrollBars="auto">
                        <% //*********************************	%>
                        <% // ｷｬﾗｸﾀｰ一覧						%>
                        <% //*********************************	%>
                        <asp:DataList ID="lstCharacter" runat="server" OnItemDataBound="lstDataList_ItemDataBound">
                            <ItemTemplate>
                                <table border="0" class="tableStyleAutoSize">
                                    <tr style="background-color: #E0E0E0">
                                        <td class="Pad">
                                            No
                                        </td>
                                        <td class="Pad">
                                            削除
                                        </td>
                                        <td class="Pad">
                                            公開
                                        </td>
                                        <td class="Pad">
                                            優先度
                                        </td>
                                        <td class="Pad">
                                            写真
                                        </td>
                                        <td class="Pad">
                                            <%= DisplayWordUtil.Replace("出演者ID") %>
                                            /ハンドル名/カテゴリ/出演者状態/公開期間/コメント
                                        </td>
                                        <td class="Pad">
                                            Pickup
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <td valign="middle">
                                            <%# Container.ItemIndex + 1 + pageSize * (this.PageIndex - 1)%>
                                            <asp:HiddenField ID="hdnUserSeq" runat="server" Value='<%# Eval("USER_SEQ") %>'></asp:HiddenField>
                                            <asp:HiddenField ID="hdnUserCharNo" runat="server" Value='<%# Eval("USER_CHAR_NO") %>'>
                                            </asp:HiddenField>
                                        </td>
                                        <td valign="middle">
                                            <asp:CheckBox ID="chkDeleteFlag" runat="server" Checked="false"></asp:CheckBox>
                                        </td>
                                        <td valign="middle">
                                            <asp:CheckBox ID="chkPickup" runat="server" Checked='<%# Eval("PICKUP_FLAG").ToString().Equals(ViCommConst.FLAG_ON_STR) %>'>
                                            </asp:CheckBox>
                                        </td>
                                        <td valign="middle">
                                            <asp:TextBox ID="txtPriority" runat="server" MaxLength="3" Width="30px" Text='<%# Eval("PRIORITY")%>'
                                                Style="text-align: right;">
                                            </asp:TextBox>
                                        </td>
                                        <td class="Pad">
                                            <asp:Image ID="imgPic" runat="server" ImageUrl='<%# string.Format("../{0}",Eval("SMALL_PHOTO_IMG_PATH")) %>'
                                                Width="70px"></asp:Image><br />
                                            <asp:Label ID="lblClosedMark" runat="server" Text='<%# GetClosedMark(Eval("OBJ_NOT_APPROVE_FLAG"), Eval("OBJ_NOT_PUBLISH_FLAG"), string.Empty) %>' />
                                        </td>
                                        <td class="Pad" align="left">
                                            <asp:HyperLink ID="lnlCastView" runat="server" NavigateUrl='<%# string.Format("../Cast/CastView.aspx?loginid={0}&return={1}",Eval("LOGIN_ID"),"MultiPickupMainte.aspx") %>'
                                                Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>/
                                            <asp:Label ID="lblHandleNm" runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:Label>/
                                            <asp:Label ID="lblCategoryNm" runat="server" Text='<%#string.Format("[{0}]", Eval("ACT_CATEGORY_NM")) %>'></asp:Label>/
                                            <asp:Label ID="lblUserStatus" runat="server" Text='<%# GetUserStatusMark(Eval("USER_STATUS")) %>'></asp:Label>/<br />
                                            <asp:TextBox ID="txtStartPubDay" runat="server" MaxLength="10" Width="80px" Text='<%# Eval("PICKUP_START_PUB_DAY", "{0:yyyy/MM/dd}") %>'></asp:TextBox>&nbsp;〜&nbsp;
                                            <asp:TextBox ID="txtEndPubDay" runat="server" MaxLength="10" Width="80px" Text='<%# Eval("PICKUP_END_PUB_DAY", "{0:yyyy/MM/dd}") %>'></asp:TextBox>
                                            <br />
                                            <asp:TextBox ID="txtComment" runat="server" Columns="60" Rows="3" TextMode="MultiLine"
                                                Text='<%# Eval("COMMENT_PICKUP") %>'></asp:TextBox>
                                            <asp:Panel ID="pnlError" runat="server" Visible="false">
                                                <asp:Label ID="lblErrorMessage" runat="server" Text="" ForeColor="red"></asp:Label>
                                            </asp:Panel>
                                        </td>
                                        <td class="Pad">
                                            <span style="color: Red">
                                                <asp:Literal ID="lblOthers" runat="server" Text='<%# Eval("OTHER_PICKUP").ToString().Replace(",","<br/>") %>'></asp:Literal>
                                            </span>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                            <FooterTemplate>
                                <asp:Panel ID="pnlPaging" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="lnkPageFirst" runat="server" CommandName="PAGING" CommandArgument="1"
                                                    OnCommand="lnkPaging_Command" Text="<<" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkPagePrevious" runat="server" CommandName="PAGING" CommandArgument="1"
                                                    OnCommand="lnkPaging_Command" Text="..." />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkPage1" runat="server" CommandName="PAGING" CommandArgument="1"
                                                    OnCommand="lnkPaging_Command" Text="1" />
                                                <asp:Label ID="lblPage1" runat="server" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkPage2" runat="server" CommandName="PAGING" CommandArgument="1"
                                                    OnCommand="lnkPaging_Command" Text="2" />
                                                <asp:Label ID="lblPage2" runat="server" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkPage3" runat="server" CommandName="PAGING" CommandArgument="1"
                                                    OnCommand="lnkPaging_Command" Text="3" />
                                                <asp:Label ID="lblPage3" runat="server" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkPage4" runat="server" CommandName="PAGING" CommandArgument="1"
                                                    OnCommand="lnkPaging_Command" Text="4" />
                                                <asp:Label ID="lblPage4" runat="server" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkPage5" runat="server" CommandName="PAGING" CommandArgument="1"
                                                    OnCommand="lnkPaging_Command" Text="5" />
                                                <asp:Label ID="lblPage5" runat="server" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkPage6" runat="server" CommandName="PAGING" CommandArgument="1"
                                                    OnCommand="lnkPaging_Command" Text="6" />
                                                <asp:Label ID="lblPage6" runat="server" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkPage7" runat="server" CommandName="PAGING" CommandArgument="1"
                                                    OnCommand="lnkPaging_Command" Text="7" />
                                                <asp:Label ID="lblPage7" runat="server" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkPage8" runat="server" CommandName="PAGING" CommandArgument="1"
                                                    OnCommand="lnkPaging_Command" Text="8" />
                                                <asp:Label ID="lblPage8" runat="server" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkPage9" runat="server" CommandName="PAGING" CommandArgument="1"
                                                    OnCommand="lnkPaging_Command" Text="9" />
                                                <asp:Label ID="lblPage9" runat="server" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkPage10" runat="server" CommandName="PAGING" CommandArgument="1"
                                                    OnCommand="lnkPaging_Command" Text="10" />
                                                <asp:Label ID="lblPage10" runat="server" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkPageNext" runat="server" CommandName="PAGING" CommandArgument="1"
                                                    OnCommand="lnkPaging_Command" Text="..." />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkPageLast" runat="server" CommandName="PAGING" CommandArgument="1"
                                                    OnCommand="lnkPaging_Command" Text=">>" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </FooterTemplate>
                        </asp:DataList>
                    </asp:Panel>
                </fieldset>
            </fieldset>
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsPickupType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="27" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsPickup" runat="server" SelectMethod="GetList" TypeName="Pickup">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSeekSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnSubmitUpdate"
        ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" ConfirmOnFormSubmit="true"
        ConfirmText="削除を行ないますか？" TargetControlID="btnDelete">
    </ajaxToolkit:ConfirmButtonExtender>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender3" runat="server" ConfirmOnFormSubmit="true"
        ConfirmText="ピックアップの全件削除を行ないますか？" TargetControlID="btnAllClear">
    </ajaxToolkit:ConfirmButtonExtender>
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
        TargetControlID="vdrPickupTitle" HighlightCssClass="validatorCallout" />
</asp:Content>
