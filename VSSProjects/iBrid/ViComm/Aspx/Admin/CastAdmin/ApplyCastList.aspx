<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ApplyCastList.aspx.cs" Inherits="CastAdmin_ApplyCastList"
	Title="新規登録キャスト認証" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="新規登録キャスト認証"></asp:Label>
	
	<script type="text/javascript">
        function ClearErrMsg() 
        {
            document.getElementById("<%= lblErrorMessage.ClientID %>").innerText="";
            document.getElementById("<%= lblErrorMessage.ClientID %>").style.visibility="hidden";
            return true;
        } 	
    </script>
    
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 800px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle">
							電話番号
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtTel" runat="server" MaxLength="11" Width="88px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							ﾒｰﾙｱﾄﾞﾚｽ
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtEMailAddr" runat="server" Width="170px"></asp:TextBox>
						</td>
						<td class="tdHeaderStyle2">
							氏名
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtCastNm" runat="server" MaxLength="20" Width="120px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							申請日
						</td>
						<td class="tdDataStyle" colspan="3">
							<asp:TextBox ID="txtApplyDayFrom" runat="server" MaxLength="10" Width="66px" Style="padding: 1px 0px 1px 0px"></asp:TextBox>
							<asp:DropDownList ID="lstApplyTimeFrom" runat="server" Width="44px">
							</asp:DropDownList>&nbsp;〜
							<asp:TextBox ID="txtApplyDayTo" runat="server" MaxLength="10" Width="66px" Style="padding: 1px 0px 1px 0px"></asp:TextBox>
							<asp:DropDownList ID="lstApplyTimeTo" runat="server" Width="44px">
							</asp:DropDownList>
							<asp:RangeValidator ID="vdrApplyDayFrom" runat="server" ErrorMessage="申請日Fromを正しく入力して下さい。" ControlToValidate="txtApplyDayFrom" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:RangeValidator ID="vdrApplyDayTo" runat="server" ErrorMessage="申請日Toを正しく入力して下さい。" ControlToValidate="txtApplyDayTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
								Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:CompareValidator ID="vdcApplyDayFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="txtApplyDayFrom" ControlToValidate="txtApplyDayTo"
								Operator="GreaterThanEqual" ValidationGroup="Key" Display="Dynamic">*</asp:CompareValidator>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
				<table border="0" cellpadding="0" cellspacing="0" style="clear:left;">
				<tr>
				    <td>		
				        <asp:Label ID="lblCsvPassword" runat="server" Text="ﾊﾟｽﾜｰﾄﾞ:" ></asp:Label>
				        <asp:TextBox ID="txtCsvPassword" runat="server" MaxLength="20" Width="140px" TextMode="Password"></asp:TextBox>
				        <asp:Button runat="server" ID="btnCsv" Text="ＣＳＶ出力" OnClientClick="ClearErrMsg();" OnClick="btnCSV_Click" />
				        <asp:Label ID="lblErrorMessage" runat="server" Text="" Visible="false" ForeColor="red"></asp:Label>&nbsp;
				    </td>
				</tr>
				</table>
			</asp:Panel>
		</fieldset>
		<fieldset>
			<legend>
				<%= DisplayWordUtil.Replace("[申請中出演者一覧]") %>
			</legend>
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="430px">
				<asp:GridView ID="grdCast" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsTempRegist" SkinID="GridViewColor" AllowSorting="True">
					<Columns>
						<asp:BoundField DataField="CAST_NM" HeaderText="氏名" SortExpression="CAST_NM" />
						<asp:BoundField DataField="CAST_KANA_NM" HeaderText="氏名(フリガナ)" SortExpression="CAST_KANA_NM" />
						<asp:TemplateField HeaderText="年齢">
							<ItemTemplate>
								<asp:Label ID="lblAge" runat="server" Text='<%# GetAgeByBirthDay(Eval("BIRTHDAY")) %>'></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField DataField="SIGNUP_FLAG" HeaderText="身分証" SortExpression="SIGNUP_FLAG" />
						<asp:BoundField DataField="AD_CD" HeaderText="広告ｺｰﾄﾞ" SortExpression="AD_CD" />
						<asp:TemplateField HeaderText="紹介者ID" SortExpression="FRIEND_LOGIN_ID">
							<ItemTemplate>
								<asp:HyperLink ID="lnkFriendLoginId" runat="server" NavigateUrl='<%# string.Format("../Cast/CastView.aspx?loginid={0}",Eval("FRIEND_LOGIN_ID")) %>' Text='<%# Eval("FRIEND_LOGIN_ID") %>'></asp:HyperLink>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField DataField="REGIST_APPLY_DATE" HeaderText="登録申請日時" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False" SortExpression="REGIST_APPLY_DATE">
							<ItemStyle HorizontalAlign="Center" />
						</asp:BoundField>
						<asp:HyperLinkField HeaderText="詳細" Text="詳細" DataNavigateUrlFields="TEMP_REGIST_ID" DataNavigateUrlFormatString="../CastAdmin/ApplyCastMainte.aspx?tempid={0}&amp;return=ApplyCastList.aspx">
							<ItemStyle Width="60px" HorizontalAlign="Center" />
						</asp:HyperLinkField>
					</Columns>
					<PagerSettings Mode="NumericFirstLast" />
				</asp:GridView>
			</asp:Panel>
			&nbsp;
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br>
				<a class="reccount">Current viewing page
					<%=grdCast.PageIndex + 1%>
					of
					<%=grdCast.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsTempRegist" runat="server" SelectMethod="GetPageCollection" TypeName="TempRegist" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsTempRegist_Selected" OnSelecting="dsTempRegist_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pTel" Type="String" />
			<asp:Parameter Name="pEmailAddr" Type="String" />
			<asp:Parameter Name="pCastNm" Type="String" />
			<asp:Parameter Name="pApplyDayFrom" Type="String" />
			<asp:Parameter Name="pApplyDayTo" Type="String" />
			<asp:Parameter Name="pApplyTimeFrom" Type="String" />
			<asp:Parameter Name="pApplyTimeTo" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrApplyDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrApplyDayTo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdcApplyDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskApplyDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtApplyDayFrom">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:MaskedEditExtender ID="mskApplyDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
		TargetControlID="txtApplyDayTo">
	</ajaxToolkit:MaskedEditExtender>
</asp:Content>
