﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using iBridCommLib;
using ViComm;

public partial class CastAdmin_PickupObjViewer : System.Web.UI.Page {
	private const int COLINDEX_OBJ_TITLE = 1;

	public IDictionary<string,string> SelectedPickupDict{
		get{
			if(this.Session["CastAdmin_PickupObjViewer.SelectedPickupDict"] == null){
				this.Session["CastAdmin_PickupObjViewer.SelectedPickupDict"] = new Dictionary<string,string>();
			}
			return (IDictionary<string,string>)this.Session["CastAdmin_PickupObjViewer.SelectedPickupDict"];
		}			
	}

	/// <summary>ｻｲﾄｺｰﾄﾞを取得または設定する</summary>
	protected string SiteCd {
		get { return (string)this.ViewState["SiteCd"]; }
		private set { this.ViewState["SiteCd"] = value; }
	}
	/// <summary>オブジェクトSEQを取得または設定する</summary>
	protected string ObjSeq {
		get { return (string)this.ViewState["ObjSeq"]; }
		private set { this.ViewState["ObjSeq"] = value; }
	}
	/// <summary>オブジェクト区分(ピックアップ区分)を取得または設定する</summary>
	protected string PickupType {
		get { return (string)this.ViewState["ObjType"]; }
		private set { this.ViewState["ObjType"] = value; }
	}
	/// <summary>動画のパス</summary>
	protected string MoviePath {
		get { return (string)this.ViewState["MoviePath"]; }
		private set { this.ViewState["MoviePath"] = value; }
	}
	/// <summary>ピックアップ有りか否か</summary>
	protected string WithPickupFlag {
		get { return iBridUtil.GetStringValue(this.ViewState["WithPickupFlag"]); }
		private set { this.ViewState["WithPickupFlag"] = value; }
	}
	/// <summary>出演者SEQを取得または設定する</summary>
	private string CastSeq {
		get { return (string)this.ViewState["CastSeq"]; }
		set { this.ViewState["CastSeq"] = value; }
	}

	private int CompanyMask {
		get { return int.Parse(Session["CompanyMask"].ToString()); }
	}

	#region □■□ イベントハンドラ □■□ ============================================================================

	protected void Page_Load(object sender, EventArgs e) {

		//Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.SiteCd = iBridUtil.GetStringValue(this.Request.QueryString["sitecd"]);
			this.PickupType = iBridUtil.GetStringValue(this.Request.QueryString["objtype"]);
			this.ObjSeq = iBridUtil.GetStringValue(this.Request.QueryString["objseq"]);
			this.WithPickupFlag = this.Request.QueryString["withpickup"];

			this.InitPage();

			if (SelectedPickupDict.Keys.Contains(this.PickupType)) {
				int iIndex = 0;
				foreach (ListItem oListItem in this.lstPickup.Items) {
					if (oListItem.Value.Equals(SelectedPickupDict[this.PickupType])) {
						break;
					}
					iIndex += 1;
				}
				this.lstPickup.SelectedIndex = iIndex;
				this.DisplayPickupObjList();
			}

			this.CastSeq = string.Empty;

			switch (this.PickupType) {
				case ViCommConst.PicupTypes.CAST_CHARACTER:
					this.CastSeq = this.ObjSeq;
					break;
				case ViCommConst.PicupTypes.BBS_PIC:
				case ViCommConst.PicupTypes.PROFILE_PIC:
					using (CastPic oCastPic = new CastPic()) {
						if (oCastPic.GetOne(this.ObjSeq)) {
							this.CastSeq = oCastPic.userSeq;
						}
					}
					break;
				case ViCommConst.PicupTypes.BBS_MOVIE:
				case ViCommConst.PicupTypes.PROFILE_MOVIE:
				case ViCommConst.PicupTypes.SOCIAL_GAME_MOVIE:
					using (CastMovie oCastMovie = new CastMovie()) {
						if (oCastMovie.GetOne(decimal.Parse(this.ObjSeq))) {
							this.CastSeq = oCastMovie.userSeq.ToString();
						}
					}
					break;
				case ViCommConst.PicupTypes.YAKYUKEN_PIC:
					using (YakyukenPic oYakyukenPic = new YakyukenPic()) {
						if (oYakyukenPic.GetOne(this.ObjSeq)) {
							this.CastSeq = oYakyukenPic.castUserSeq;
						}
					}
					break;
				case ViCommConst.PicupTypes.SOCIAL_GAME_PIC:
					using (ManTreasure oManTreasure = new ManTreasure()) {
						if (oManTreasure.GetOne(this.ObjSeq)) {
							this.CastSeq = oManTreasure.userSeq;
						}
					}
					break;
			}

			this.pnlPickupObjList.Visible = this.IsWithPickup();
			if (this.pnlPickupObjList.Visible) {
				using (CastCharacter oCastCharacter = new CastCharacter()) {
					pnlPickupObjList.Visible = oCastCharacter.IsUsePickup(this.SiteCd,this.CastSeq,ViCommConst.MAIN_CHAR_NO);
				}
			} else {
				this.tabPersonalObjs.Height = 530;
			}
		}
	}
	
	protected void btnAppendPickup_Click(object sender, EventArgs e) {
		this.AppednPickup(false);
		this.DisplayObjInfo(this.ObjSeq);
	}
	protected void btnAppendPickupWithPickup_Click(object sender,EventArgs e) {
		this.AppednPickup(true);
		this.DisplayObjInfo(this.ObjSeq);
	}
	protected void btnRemovePickup_Click(object sender, EventArgs e) {
		this.RemovePickup(this.SiteCd,this.lstPickup.SelectedValue,this.ObjSeq);
		this.DisplayObjInfo(this.ObjSeq);
	}
	protected void lstPickup_SelectedIndexChanged(object sender, EventArgs e) {
		this.DisplayPickupObjList();
		if(!SelectedPickupDict.Keys.Contains(this.PickupType)){
			SelectedPickupDict.Add(this.PickupType, string.Empty);
		}
		SelectedPickupDict[this.PickupType] = this.lstPickup.SelectedValue;
	}

	protected void vdcObjSeq_ServerValidate(object source, ServerValidateEventArgs args) {
		string sPickupId = this.lstPickup.SelectedValue;
		if (sPickupId.Equals(string.Empty)) return;
		if (this.PickupType.Equals(ViCommConst.PicupTypes.CAST_CHARACTER)) {
			using(PickupCharacter oPickupCharacter = new PickupCharacter()){
				args.IsValid = !oPickupCharacter.Excists(this.SiteCd, sPickupId, this.ObjSeq, ViCommConst.MAIN_CHAR_NO);
			}
			
		}else{
			using (PickupObjs oPickupObj = new PickupObjs()) {
				args.IsValid = !oPickupObj.Excists(this.SiteCd, sPickupId, this.ObjSeq);
			}
		}
	}
	protected void vdcObjSeq4Remove_ServerValidate(object source, ServerValidateEventArgs args) {
		string sPickupId = this.lstPickup.SelectedValue;
		if(sPickupId.Equals(string.Empty))return;
		if (this.PickupType.Equals(ViCommConst.PicupTypes.CAST_CHARACTER)) {
			using (PickupCharacter oPickupCharacter = new PickupCharacter()) {
				args.IsValid = oPickupCharacter.Excists(this.SiteCd, sPickupId, this.ObjSeq, ViCommConst.MAIN_CHAR_NO);
			}

		} else {
			using (PickupObjs oPickupObj = new PickupObjs()) {
				args.IsValid = oPickupObj.Excists(this.SiteCd, sPickupId, this.ObjSeq);
			}
		}
	}

	protected void vdcHidePic_ServerValidate(object source,ServerValidateEventArgs args) {
		args.IsValid = true;
	}

	protected void grdPickupObjList_RowCommand(object sender, GridViewCommandEventArgs e) {
		if(e.CommandName.Equals("RemovePickup")){
			string[] sArgs = ((string)e.CommandArgument).Split(',');
			
			// ピックアップから削除
			this.RemovePickup(sArgs[0],sArgs[1],sArgs[2]);
			// 再表示
			this.DisplayPickupObjList();
		} else if (e.CommandName.Equals("DisplayObjInfo")) {
			string sObjSeq = (string)e.CommandArgument;
			this.DisplayObjInfo(sObjSeq);
		}
	}
	
	protected void dsPickupList_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = this.SiteCd;
		e.InputParameters[1] = this.PickupType;
	}
	protected void dsPickupObjList_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		if(string.IsNullOrEmpty(this.lstPickup.SelectedValue)){
			e.Cancel = true;
			return;
		}
		e.InputParameters[0] = this.SiteCd;
		e.InputParameters[1] = this.lstPickup.SelectedValue;
		e.InputParameters[2] = this.PickupType;
		e.InputParameters[3] = string.Empty;
		e.InputParameters[4] = string.Empty;

		if(this.IsCastCharacter()){
			this.grdPickupObjList.Columns[COLINDEX_OBJ_TITLE].Visible = false;
		}

	}
	
	#endregion ========================================================================================================

	private void InitPage() {
		this.ClearField();
		this.lstPickup.DataBind();
		this.DisplayObjInfo(this.ObjSeq);
		
		switch(this.PickupType){
			case ViCommConst.PicupTypes.CAST_CHARACTER:
				this.lblObjInfoTitle.Text = "ｷｬｽﾄｷｬﾗｸﾀｰ";
				break;
			case ViCommConst.PicupTypes.PROFILE_MOVIE:
				this.lblObjInfoTitle.Text = string.Format("{0}プロフ動画",this.IsWithPickup() ? "公開中" : "認証待ち");
				break;
			case ViCommConst.PicupTypes.PROFILE_PIC:
				this.lblObjInfoTitle.Text = "プロフ画像";
				break;
			case ViCommConst.PicupTypes.BBS_MOVIE:
				this.lblObjInfoTitle.Text = string.Format("{0}掲示板動画",this.IsWithPickup() ? "公開中" : "認証待ち");
				break;
			case ViCommConst.PicupTypes.BBS_PIC:
				this.lblObjInfoTitle.Text = string.Format("{0}掲示板画像",this.IsWithPickup() ? "公開中" : "認証待ち");
				break;
			case ViCommConst.PicupTypes.YAKYUKEN_PIC:
				this.lblObjInfoTitle.Text = string.Format("{0}野球拳画像",this.IsWithPickup() ? "公開中" : "認証待ち");
				break;
			case ViCommConst.PicupTypes.SOCIAL_GAME_PIC:
				this.lblObjInfoTitle.Text = "ゲームお宝画像";
				break;
			case ViCommConst.PicupTypes.SOCIAL_GAME_MOVIE:
				this.lblObjInfoTitle.Text = "ゲームお宝動画";
				break;
			case ViCommConst.PicupTypes.MAIL_STOCK_PIC:
				this.lblObjInfoTitle.Text = "メールストック画像";
				break;
			case ViCommConst.PicupTypes.MAIL_STOCK_MOVIE:
				this.lblObjInfoTitle.Text = "メールストック動画";
				break;
			
			default:
				this.lblObjInfoTitle.Text = "unknown";
				break;
		}

	}

	private void ClearField() {
		this.lblObjTitle.Text = string.Empty;
		this.lblCastNm.Text = string.Empty;
		//this.lblHandleNm.Text = string.Empty;
		this.lnkHandleNm.Text = string.Empty;
	}

	/// <summary>
	/// オブジェクトの情報が画面に出力する

	/// </summary>
	private void DisplayObjInfo(string pObjSeq){
		string sObjTitle = string.Empty;
		string sCastNm = string.Empty;
		string sHandleNm = string.Empty;
		string sImgPath = string.Empty;
		string sMoviePath = string.Empty;
		string sLoginId = string.Empty;
		string sCommentText = string.Empty;
		string sYakyukenType = string.Empty;
		string sYakyukenTypeNm = string.Empty;
		string sAttrTypeNm = string.Empty;
		bool bIsImage = false;
		bool bIsMovie = false;
		List<string> oPickupList = new List<string>();
		
		string sUserSeq = string.Empty;
		string sUserCharNo = string.Empty;
		switch(this.PickupType){
			case ViCommConst.PicupTypes.CAST_CHARACTER:

				using (CastCharacter oCastCharacter = new CastCharacter()) {
					using (DataSet oCastCharacterDataSet = oCastCharacter.GetOneByUserSeq(this.SiteCd, pObjSeq, ViCommConst.MAIN_CHAR_NO)) {
						if (oCastCharacterDataSet.Tables.Count > 0 && oCastCharacterDataSet.Tables[0].Rows.Count > 0) {
							DataRow oCastCharacterDataRow = oCastCharacterDataSet.Tables[0].Rows[0];

							sUserSeq = iBridUtil.GetStringValue(oCastCharacterDataRow["USER_SEQ"]);
							sUserCharNo = iBridUtil.GetStringValue(oCastCharacterDataRow["USER_CHAR_NO"]);
							sCastNm = iBridUtil.GetStringValue(oCastCharacterDataRow["CAST_NM"]);
							sHandleNm = iBridUtil.GetStringValue(oCastCharacterDataRow["HANDLE_NM"]);
							sImgPath = iBridUtil.GetStringValue(oCastCharacterDataRow["PHOTO_IMG_PATH"]);
							sLoginId = iBridUtil.GetStringValue(oCastCharacterDataRow["LOGIN_ID"]);
							bIsImage = true;// プロフ画像表示のため							
						}
					}					
				}
				using(PickupCharacter oPickupCharacter = new PickupCharacter()){
					oPickupList = oPickupCharacter.GetPickupIdList(this.SiteCd, sUserSeq, sUserCharNo);
				}
				
				this.ObjSeq = sUserSeq;
				break;
			case ViCommConst.PicupTypes.BBS_PIC:
			case ViCommConst.PicupTypes.PROFILE_PIC:
			case ViCommConst.PicupTypes.MAIL_STOCK_PIC:
				using(CastPic oCastPic = new CastPic()){
					if (oCastPic.GetOne(pObjSeq)) {
						//memo:ここで厳密にPIC_TYPEのチェックをしたほうが良いかも

						
						this.ObjSeq = oCastPic.picSeq;
						
						sUserSeq = oCastPic.userSeq;
						sUserCharNo = oCastPic.userCharNo;
																
						sObjTitle = oCastPic.picTitle;
						sCastNm = oCastPic.castNm;
						sHandleNm = oCastPic.handleNm;						
						sImgPath = oCastPic.objPhotImgPath;
						sLoginId = oCastPic.loginId;
					}
					using (PickupObjs oPickupObjs = new PickupObjs()) {
						oPickupList = oPickupObjs.GetPickupIdList(this.SiteCd, this.ObjSeq);
					}
					bIsImage = true;
				}
				break;
			case ViCommConst.PicupTypes.BBS_MOVIE:
			case ViCommConst.PicupTypes.PROFILE_MOVIE:
			case ViCommConst.PicupTypes.SOCIAL_GAME_MOVIE:
			case ViCommConst.PicupTypes.MAIL_STOCK_MOVIE:
				using (CastMovie oCastMovie = new CastMovie()) {
					if (oCastMovie.GetOne(decimal.Parse(pObjSeq))) {
						//memo:ここで厳密にMOVIE_TYPEのチェックをしたほうが良いかも

				
						this.ObjSeq = oCastMovie.movieSeq.ToString();
						sUserSeq = oCastMovie.userSeq.ToString();
						sUserCharNo = oCastMovie.userCharNo.ToString();

						string sFileNm = iBridUtil.addZero(oCastMovie.movieSeq.ToString(), ViCommConst.OBJECT_NM_LENGTH) + ViCommConst.MOVIE_FOODER;
						sMoviePath = ResolveUrl(string.Format("~/{0}/{1}/operator/{2}/{3}", ViCommConst.WEB_MOVIE_PATH, this.SiteCd,oCastMovie.loginId,sFileNm));
						sObjTitle = oCastMovie.movieTitle;
						sCastNm = oCastMovie.castNm;
						sHandleNm = oCastMovie.handleNm;
						sLoginId = oCastMovie.loginId;
					}
					using (PickupObjs oPickupObjs = new PickupObjs()) {
						oPickupList = oPickupObjs.GetPickupIdList(this.SiteCd, this.ObjSeq);
					}
					bIsMovie = true;
				}
				break;
			case ViCommConst.PicupTypes.YAKYUKEN_PIC:
				using (YakyukenPic oYakyukenPic = new YakyukenPic()) {
					if (oYakyukenPic.GetOne(pObjSeq)) {

						this.ObjSeq = oYakyukenPic.picSeq;

						sUserSeq = oYakyukenPic.castUserSeq;
						sUserCharNo = oYakyukenPic.castCharNo;

						sCommentText = oYakyukenPic.commentText;
						sYakyukenType = oYakyukenPic.yakyukenType;
						sYakyukenTypeNm = this.GetYakyukenTypeNm(sYakyukenType);
						
						sCastNm = oYakyukenPic.castNm;
						sHandleNm = oYakyukenPic.handleNm;
						sImgPath = oYakyukenPic.objPhotoImgPath;
						
						sLoginId = oYakyukenPic.loginId;
					}
					using (PickupObjs oPickupObjs = new PickupObjs()) {
						oPickupList = oPickupObjs.GetPickupIdList(this.SiteCd,this.ObjSeq);
					}
					bIsImage = true;
				}
				break;
			case ViCommConst.PicupTypes.SOCIAL_GAME_PIC:
				using (ManTreasure oManTreasure = new ManTreasure()) {
					if (oManTreasure.GetOne(pObjSeq)) {
						this.ObjSeq = oManTreasure.castGamePicSeq;
						sUserSeq = oManTreasure.userSeq;
						sUserCharNo = oManTreasure.userCharNo;
						sAttrTypeNm = oManTreasure.castGameAttrTypeNm;
						sObjTitle = oManTreasure.picTitle;
						sCastNm = oManTreasure.castNm;
						sHandleNm = oManTreasure.handleNm;
						sImgPath = oManTreasure.objPhotoImgPath;
						sLoginId = oManTreasure.loginId;
						bIsImage = true;
					}
				}
				break;
		}

		this.rptIncludedPickup.DataSource=oPickupList;
		this.rptIncludedPickup.DataBind();		
		this.lnkLoginId.Text = sLoginId;
		this.lnkLoginId.OnClientClick = string.Format("return redirectParent('{0}?loginid={1}');", ResolveUrl("~/Cast/CastView.aspx"),sLoginId);
		this.lblObjTitle.Text = FormatObjTitle(sObjTitle);
		this.lblCastNm.Text = sCastNm;
		this.lblCommentText.Text = sCommentText;
		this.lblYakyukenTypeNm.Text = sYakyukenTypeNm;
		this.lblAttrTypeNm.Text = sAttrTypeNm;
		//this.lblHandleNm.Text = sHandleNm;
		this.lnkHandleNm.Text = sHandleNm;
		this.imgObject.ImageUrl = string.Format("../{0}",sImgPath);
		this.MoviePath = sMoviePath;
		this.imgObject.Visible = bIsImage;
		this.pnlMovie.Visible = bIsMovie;
		this.pnlObjView.DataBind();

		this.DisplayPickupObjList();

		// Object Viewer
		this.ctrlProfilePicList.SiteCd = this.SiteCd;
		this.ctrlProfilePicList.UserSeq = sUserSeq;
		this.ctrlProfilePicList.UserCharNo = sUserCharNo;
		this.ctrlProfilePicList.PicType = ViCommConst.ATTACHED_PROFILE.ToString();
		this.ctrlProfilePicList.Display();

		this.ctrlBbsPicList.SiteCd = this.SiteCd;
		this.ctrlBbsPicList.UserSeq = sUserSeq;
		this.ctrlBbsPicList.UserCharNo = sUserCharNo;
		this.ctrlBbsPicList.PicType = ViCommConst.ATTACHED_BBS.ToString();
		this.ctrlBbsPicList.Display();

		this.ctrlProfileMovieList.SiteCd = this.SiteCd;
		this.ctrlProfileMovieList.UserSeq = sUserSeq;
		this.ctrlProfileMovieList.UserCharNo = sUserCharNo;
		this.ctrlProfileMovieList.MovieType = ViCommConst.ATTACHED_PROFILE.ToString();
		this.ctrlProfileMovieList.Display();

		this.ctrlBbsMovieList.SiteCd = this.SiteCd;
		this.ctrlBbsMovieList.UserSeq = sUserSeq;
		this.ctrlBbsMovieList.UserCharNo = sUserCharNo;
		this.ctrlBbsMovieList.MovieType = ViCommConst.ATTACHED_BBS.ToString();
		this.ctrlBbsMovieList.Display();

		this.ctrlYakyukenPicList.SiteCd = this.SiteCd;
		this.ctrlYakyukenPicList.UserSeq = sUserSeq;
		this.ctrlYakyukenPicList.UserCharNo = sUserCharNo;
		this.ctrlYakyukenPicList.Display();

		this.ctrlSocialGamePicList.SiteCd = this.SiteCd;
		this.ctrlSocialGamePicList.UserSeq = sUserSeq;
		this.ctrlSocialGamePicList.UserCharNo = sUserCharNo;
		this.ctrlSocialGamePicList.Display();

		this.ctrlSocialGameMovieList.SiteCd = this.SiteCd;
		this.ctrlSocialGameMovieList.UserSeq = sUserSeq;
		this.ctrlSocialGameMovieList.UserCharNo = sUserCharNo;
		this.ctrlSocialGameMovieList.MovieType = ViCommConst.ATTACHED_SOCIAL_GAME.ToString();
		this.ctrlSocialGameMovieList.Display();

		this.ctrlMailStockPicList.SiteCd = this.SiteCd;
		this.ctrlMailStockPicList.UserSeq = sUserSeq;
		this.ctrlMailStockPicList.UserCharNo = sUserCharNo;
		this.ctrlMailStockPicList.PicType = ViCommConst.ATTACHED_MAIL.ToString();
		this.ctrlMailStockPicList.Display();

		this.ctrlMailStockMovieList.SiteCd = this.SiteCd;
		this.ctrlMailStockMovieList.UserSeq = sUserSeq;
		this.ctrlMailStockMovieList.UserCharNo = sUserCharNo;
		this.ctrlMailStockMovieList.MovieType = ViCommConst.ATTACHED_MAIL.ToString();
		this.ctrlMailStockMovieList.Display();

		if (iBridUtil.GetStringValue(Request.QueryString["seek_resembled"]).Equals(ViCommConst.FLAG_ON_STR)) {
			this.ctrlResembledPicList.SiteCd = this.SiteCd;
			this.ctrlResembledPicList.UserSeq = sUserSeq;
			this.ctrlResembledPicList.UserCharNo = sUserCharNo;
			this.ctrlResembledPicList.LoginId = sLoginId;
			this.ctrlResembledPicList.OriginalPicSeq = this.ObjSeq;
			this.ctrlResembledPicList.Display();
		}

		switch (this.PickupType) {
			case ViCommConst.PicupTypes.CAST_CHARACTER:
				this.tabPersonalObjs.ActiveTab = this.tpProfPic;
				break;
			case ViCommConst.PicupTypes.BBS_PIC:
				this.tabPersonalObjs.ActiveTab = this.tpBbsPic;
				break;
			case ViCommConst.PicupTypes.PROFILE_PIC:
				this.tabPersonalObjs.ActiveTab = this.tpProfPic;
				break;
			case ViCommConst.PicupTypes.BBS_MOVIE:
				this.tabPersonalObjs.ActiveTab = this.tpBbsMovie;
				break;
			case ViCommConst.PicupTypes.PROFILE_MOVIE:
				this.tabPersonalObjs.ActiveTab = this.tpProfMovie;
				break;
			case ViCommConst.PicupTypes.YAKYUKEN_PIC:
				this.tabPersonalObjs.ActiveTab = this.tpYakyukenPic;
				break;
			case ViCommConst.PicupTypes.SOCIAL_GAME_PIC:
				this.tabPersonalObjs.ActiveTab = this.tpSocialGamePic;
				break;
			case ViCommConst.PicupTypes.SOCIAL_GAME_MOVIE:
				this.tabPersonalObjs.ActiveTab = this.tpSocialGameMovie;
				break;
			case ViCommConst.PicupTypes.MAIL_STOCK_PIC:
				this.tabPersonalObjs.ActiveTab = this.tpMailStockPic;
				break;
			case ViCommConst.PicupTypes.MAIL_STOCK_MOVIE:
				this.tabPersonalObjs.ActiveTab = this.tpMailStockMovie;
				break;
		}
		
		if (iBridUtil.GetStringValue(Request.QueryString["seek_resembled"]).Equals(ViCommConst.FLAG_ON_STR)) {
			this.tabPersonalObjs.ActiveTab = this.tpResembledPic;
		}
		
	}
	/// <summary>
	/// ピックアップに登録されているオブジェクトのリストを表示する
	/// </summary>
	private void DisplayPickupObjList(){
		this.grdPickupObjList.DataBind();
		this.grdPickupObjList.PageIndex = 0;
		string sPickupId = this.lstPickup.SelectedValue;
		
		string sPickupQuery = string.Empty;
		if(!sPickupId.Equals(string.Empty)){
			sPickupQuery = string.Format("sitecd={0}&pickupid={1}",this.SiteCd,sPickupId);
		}
		this.lnkOpenMainte.OnClientClick = string.Format("return redirectParent('{0}?{1}');", this.ResolveUrl("~/CastAdmin/MultiPickupMainte.aspx"), sPickupQuery);		
	}
	
	private void AppednPickup(bool pIsPickup){
		if(this.IsValid){
			string sPickupId = this.lstPickup.SelectedValue;
			int? iPriority = null;

			if (this.PickupType.Equals(ViCommConst.PicupTypes.CAST_CHARACTER)) {
				using (DbSession oDbSession = new DbSession()) {
					oDbSession.PrepareProcedure("MULTI_PICKUP_CHAR_MAINTE");
					oDbSession.cmd.BindByName = true;
					oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
					oDbSession.ProcedureInParm("pPICKUP_ID",DbSession.DbType.VARCHAR2,sPickupId);
					oDbSession.ProcedureInParm("pUSER_SEQ",DbSession.DbType.NUMBER,this.ObjSeq);
					oDbSession.ProcedureInParm("pUSER_CHAR_NO", DbSession.DbType.VARCHAR2, ViCommConst.MAIN_CHAR_NO);
					if (iPriority != null) {
						oDbSession.ProcedureInParm("pPRIORITY", DbSession.DbType.NUMBER, iPriority);
					}
					oDbSession.ProcedureInParm("pPICKUP_FLAG",DbSession.DbType.NUMBER,pIsPickup ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF);
					oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
					oDbSession.ExecuteProcedure();
				}
			} else {
				using (DbSession oDbSession = new DbSession()) {
					oDbSession.PrepareProcedure("MULTI_PICKUP_OBJS_MAINTE");
					oDbSession.cmd.BindByName = true;
					oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.SiteCd);
					oDbSession.ProcedureInParm("pPICKUP_ID",DbSession.DbType.VARCHAR2,sPickupId);
					oDbSession.ProcedureInParm("pOBJ_SEQ",DbSession.DbType.NUMBER,this.ObjSeq);
					if (iPriority != null) {
						oDbSession.ProcedureInParm("pPRIORITY", DbSession.DbType.NUMBER, iPriority);
					}
					oDbSession.ProcedureInParm("pPICKUP_FLAG",DbSession.DbType.NUMBER,pIsPickup ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF);
					oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
					oDbSession.ExecuteProcedure();
				}
			}
		}
	}
	
	private void RemovePickup(string pSiteCd,string pPickupId,string pObjSeq){
		string sPickupId = this.lstPickup.SelectedValue;

		if(this.PickupType.Equals(ViCommConst.PicupTypes.CAST_CHARACTER)){
			using (DbSession oDbSession = new DbSession()) {
				oDbSession.PrepareProcedure("MULTI_PICKUP_CHAR_MAINTE");
				oDbSession.cmd.BindByName = true;
				oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, this.SiteCd);
				oDbSession.ProcedureInParm("pPICKUP_ID", DbSession.DbType.VARCHAR2, sPickupId);
				oDbSession.ProcedureInParm("pUSER_SEQ", DbSession.DbType.NUMBER, pObjSeq);
				oDbSession.ProcedureInParm("pUSER_CHAR_NO", DbSession.DbType.VARCHAR2, ViCommConst.MAIN_CHAR_NO);
				oDbSession.ProcedureInParm("pDEL_FLAG", DbSession.DbType.NUMBER, ViCommConst.FLAG_ON);		
				oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
				oDbSession.ExecuteProcedure();
			}				
		}else{
			using (DbSession oDbSession = new DbSession()) {
				oDbSession.PrepareProcedure("MULTI_PICKUP_OBJS_MAINTE");
				oDbSession.cmd.BindByName = true;
				oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, pSiteCd);
				oDbSession.ProcedureInParm("pPICKUP_ID", DbSession.DbType.VARCHAR2, pPickupId);
				oDbSession.ProcedureInParm("pOBJ_SEQ", DbSession.DbType.NUMBER, pObjSeq);
				oDbSession.ProcedureInParm("pDEL_FLAG", DbSession.DbType.NUMBER, ViCommConst.FLAG_ON);
				oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
				oDbSession.ExecuteProcedure();
			}
		}
	}


	protected string FormatObjTitle(string sTitlle){
		return string.IsNullOrEmpty(sTitlle) ? "(未設定)" : sTitlle;
	}
	
	protected bool IsCastCharacter(){
		return this.PickupType.Equals(ViCommConst.PicupTypes.CAST_CHARACTER);
	}

	protected void grdPickupObjList_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.DataItem == null) return;

		string sObjSeq = iBridUtil.GetStringValue(DataBinder.Eval(e.Row.DataItem,"OBJ_SEQ"));
		if (this.ObjSeq.Equals(sObjSeq)) {
			e.Row.BackColor = System.Drawing.Color.LightYellow;
		} else {
			switch (this.PickupType) {
				case ViCommConst.PicupTypes.PROFILE_PIC:
					string sUnAuthFlag = iBridUtil.GetStringValue(DataBinder.Eval(e.Row.DataItem,"OBJ_NOT_APPROVE_FLAG"));
					string sNotPublishFlag = iBridUtil.GetStringValue(DataBinder.Eval(e.Row.DataItem,"OBJ_NOT_PUBLISH_FLAG"));
					string sPicType = iBridUtil.GetStringValue(DataBinder.Eval(e.Row.DataItem,"PIC_TYPE"));
					if ((ViCommConst.FLAG_ON_STR.Equals(sUnAuthFlag)) ||
						(ViCommConst.FLAG_ON_STR.Equals(sNotPublishFlag)) ||
						(ViCommConst.ATTACHED_HIDE.ToString().Equals(sPicType))) {
						e.Row.BackColor = System.Drawing.Color.LightGray;
					}
					break;
				default:
					break;
			}
		}
	}

	protected void lnkChangePickup_Command(object sender, CommandEventArgs e) {
		this.lstPickup.SelectedValue = (string)e.CommandArgument;
	}

	protected bool IsWithPickup() {
		return ViCommConst.FLAG_ON_STR.Equals(this.WithPickupFlag) ? true : false;
	}

	protected void lnkHandleNm_Click(object sender,EventArgs e) {
		this.Redirect(this.CastSeq,ViCommConst.PicupTypes.CAST_CHARACTER,ViCommConst.FLAG_ON_STR);
	}

	protected void ctrlPicList_ObjClick(object sender,ObjClickEventArgs e) {
		string sObjType;
		if (e.ObjType.Equals(ViCommConst.ATTACHED_PROFILE.ToString())) {
			sObjType = ViCommConst.PicupTypes.PROFILE_PIC;
		} else if (e.ObjType.Equals(ViCommConst.ATTACHED_BBS.ToString())) {
			sObjType = ViCommConst.PicupTypes.BBS_PIC;
		} else if (e.ObjType.Equals(ViCommConst.ATTACHED_YAKYUKEN.ToString())) {
			sObjType = ViCommConst.PicupTypes.YAKYUKEN_PIC;
		} else if (e.ObjType.Equals(ViCommConst.ATTACHED_SOCIAL_GAME.ToString())) {
			sObjType = ViCommConst.PicupTypes.SOCIAL_GAME_PIC;
		} else if (e.ObjType.Equals(ViCommConst.ATTACHED_MAIL.ToString())) {
			sObjType = ViCommConst.PicupTypes.MAIL_STOCK_PIC;
		} else {
			return;
		}
		this.Redirect(e.ObjSeq,sObjType,e.AddableFlag);
	}

	protected void ctrlMovieList_ObjClick(object sender,ObjClickEventArgs e) {
		string sObjType;
		if (e.ObjType.Equals(ViCommConst.ATTACHED_PROFILE.ToString())) {
			sObjType = ViCommConst.PicupTypes.PROFILE_MOVIE;
		} else if (e.ObjType.Equals(ViCommConst.ATTACHED_BBS.ToString())) {
			sObjType = ViCommConst.PicupTypes.BBS_MOVIE;
		} else if (e.ObjType.Equals(ViCommConst.ATTACHED_SOCIAL_GAME.ToString())) {
			sObjType = ViCommConst.PicupTypes.SOCIAL_GAME_MOVIE;
		} else if (e.ObjType.Equals(ViCommConst.ATTACHED_MAIL.ToString())) {
			sObjType = ViCommConst.PicupTypes.MAIL_STOCK_MOVIE;
		} else {
			return;
		}
		this.Redirect(e.ObjSeq,sObjType,e.AddableFlag);
	}

	protected void ctrlResembledPicList_ObjClick(object sender,ObjClickEventArgs e) {
		string sObjType;
		if (e.ObjType.Equals(ViCommConst.ATTACHED_PROFILE.ToString())) {
			sObjType = ViCommConst.PicupTypes.PROFILE_PIC;
		} else if (e.ObjType.Equals(ViCommConst.ATTACHED_BBS.ToString())) {
			sObjType = ViCommConst.PicupTypes.BBS_PIC;
		} else if (e.ObjType.Equals(ViCommConst.ATTACHED_YAKYUKEN.ToString())) {
			sObjType = ViCommConst.PicupTypes.YAKYUKEN_PIC;
		} else if (e.ObjType.Equals(ViCommConst.ATTACHED_SOCIAL_GAME.ToString())) {
			sObjType = ViCommConst.PicupTypes.SOCIAL_GAME_PIC;
		} else if (e.ObjType.Equals(ViCommConst.ATTACHED_MAIL.ToString())) {
			sObjType = ViCommConst.PicupTypes.MAIL_STOCK_PIC;
		} else {
			return;
		}
		this.Redirect(e.ObjSeq,sObjType,e.AddableFlag);
	}

	private void Redirect(string pObjSeq,string pObjType,string pAddableFlag) {
		this.Response.Redirect(
			string.Format("~/CastAdmin/PickupObjViewer.aspx?sitecd={0}&objtype={1}&objseq={2}&withpickup={3}",
				this.SiteCd,
				pObjType,
				pObjSeq,
				pAddableFlag));
	}

	protected string GetYakyukenTypeNm(string pYakyukenType) {
		string sValue = string.Empty;

		switch (pYakyukenType) {
			case "1":
				sValue = "洋服着用";
				break;
			case "2":
				sValue = "上半身下着";
				break;
			case "3":
				sValue = "全身下着";
				break;
			case "4":
				sValue = "ﾊﾟﾝﾃｨ姿/手ぶら";
				break;
			case "5":
				sValue = "ﾊﾟﾝﾃｨ姿/おっぱい";
				break;
			case "6":
				sValue = "裸/下陰部隠し";
				break;
		}

		return sValue;
	}
	
	protected bool IsYakyuken() {
		if (this.PickupType.Equals(ViCommConst.PicupTypes.YAKYUKEN_PIC)) {
			return true;
		} else {
			return false;
		}
	}

	protected void btnSeekResembled_Click(object sender,EventArgs e) {
		this.Response.Redirect(
			string.Format("~/CastAdmin/PickupObjViewer.aspx?sitecd={0}&objtype={1}&objseq={2}&withpickup={3}&seek_resembled=1",
				this.SiteCd,
				this.PickupType,
				this.ObjSeq,
				iBridUtil.GetStringValue(Request.QueryString["withpickup"])));
	}
}
