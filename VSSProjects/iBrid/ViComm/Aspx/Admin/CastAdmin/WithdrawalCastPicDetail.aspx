﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
	CodeFile="WithdrawalCastPicDetail.aspx.cs" Inherits="CastAdmin_WithdrawalCastPicDetail" Title="退会出演者画像詳細"
%>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="退会出演者画像詳細"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<%-- ============================== --%>
		<%--  Data                          --%>
		<%-- ============================== --%>
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[画像詳細]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					<table border="0" style="width: 640px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle">
								ログインID
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblLoginId" runat="server" Text="Label"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								ハンドル名
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblHandleNm" runat="server" Text="Label"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								退会日
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblWithdrawalDate" runat="server" Text="Label"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								アップロード日時
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblUploadDate" runat="server" Text="Label"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								画像の大きさ
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblImageSize" runat="server" Text="Label"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								写真
							</td>
							<td class="tdDataStyle">
								<asp:Image ID="imgPic" runat="server" BorderStyle="none" BorderWidth="0px">
								</asp:Image>
								<br />
								<asp:LinkButton ID="lnkImgDownload" runat="server" OnClick="lnkImgDownload_Click" Text="画像ダウンロード">
								</asp:LinkButton>
							</td>
						</tr>
					</table>
					<asp:Button runat="server" ID="btnBack" Text="戻る" CssClass="seekbutton" CausesValidation="False" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>
	</div>
</asp:Content>
