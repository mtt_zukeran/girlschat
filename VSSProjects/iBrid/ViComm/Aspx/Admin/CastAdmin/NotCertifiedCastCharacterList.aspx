<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="NotCertifiedCastCharacterList.aspx.cs" Inherits="CastAdmin_NotCertifiedCastCharacterList"
	Title="未認証キャストキャラクター一覧" ValidateRequest="false" %>
<%@ Import Namespace="ViComm" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="未認証キャストキャラクター一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 700px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="200px">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle2">
							ログインID
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtLoginId" runat="server" MaxLength="15" Width="88px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							氏名
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtCastNm" runat="server" MaxLength="15" Width="100px"></asp:TextBox>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
			</asp:Panel>
		</fieldset>
		<fieldset>
			<legend><%= DisplayWordUtil.Replace("[未認証ｷｬｽﾄｷｬﾗｸﾀｰ一覧]")%></legend>
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="450px">
				<asp:GridView ID="grdCast" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCastCharacter" SkinID="GridViewColor" Width="500px">
				<Columns>
					<asp:BoundField DataField="SITE_NM" HeaderText="ｻｲﾄ" />
					<asp:BoundField DataField="LOGIN_ID" HeaderText="ID" />
					<asp:BoundField DataField="CAST_NM" HeaderText="氏名" />
					<asp:TemplateField HeaderText="ハンドル名">
						<ItemTemplate>
							<asp:HyperLink ID="lnkCharacterMainte" runat="server" NavigateUrl='<%# string.Format("../Cast/CastCharacterMainte.aspx?userseq={0}&sitecd={1}&usercharno={2}&return={3}",Eval("USER_SEQ"),Eval("SITE_CD"),Eval("USER_CHAR_NO"),ViewState["RETURN"].ToString()) %>'
								Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM"))%>'></asp:HyperLink>
						</ItemTemplate>
						<ItemStyle Width="120px" />
					</asp:TemplateField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			</asp:Panel>
			&nbsp;
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br>
				<a class="reccount">Current viewing page
					<%=grdCast.PageIndex + 1%>
					of
					<%=grdCast.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsCastCharacter" runat="server" SelectMethod="GetPageCollection" TypeName="CastCharacter" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsCastCharacter_Selected" OnSelecting="dsCastCharacter_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pOnline" Type="String" />
			<asp:Parameter Name="pLoginId" Type="String" />
			<asp:Parameter Name="pProductionSeq" Type="String" />
			<asp:Parameter Name="pCastNm" Type="String" />
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pTalkType" Type="String" />
			<asp:Parameter Name="pTerminalType" Type="String" />
			<asp:Parameter Name="pHandleNm" Type="String" />
			<asp:Parameter Name="pActCategorySeq" Type="String" />
			<asp:Parameter Name="pEmailAddr" Type="String" />
			<asp:Parameter Name="pPicType" Type="String" />
			<asp:Parameter Name="pNonExistMailAddrFlag" Type="String" />
			<asp:Parameter Name="pMainCharOnlyFlag" Type="String" />
			<asp:Parameter Name="pManagerSeq" Type="String" />
			<asp:Parameter Name="pRegistDayFrom" Type="String" />
			<asp:Parameter Name="pRegistDayTo" Type="String" />
			<asp:Parameter Name="pLastLoginDayFrom" Type="String" />
			<asp:Parameter Name="pLastLoginDayTo" Type="String" />
			<asp:Parameter Name="pTotalPaymentAmtFrom" Type="String" />
			<asp:Parameter Name="pTotalPaymentAmtTo" Type="String" />
			<asp:Parameter Name="pNaFlag" Type="String" />
			<asp:Parameter Name="pUserRank" Type="String" />
			<asp:Parameter Name="pAdCd" Type="String" />
			<asp:Parameter Name="pTel" Type="String" />
			<asp:Parameter Name="pCastAttrValue1" Type="String" />
			<asp:Parameter Name="pCastAttrValue2" Type="String" />
			<asp:Parameter Name="pCastAttrValue3" Type="String" />
			<asp:Parameter Name="pCastAttrValue4" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
</asp:Content>
