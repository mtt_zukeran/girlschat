<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ApplyCastMainte.aspx.cs" Inherits="CastAdmin_ApplyCastMainte"
	Title="出演者認証" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="出演者認証"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>
					<%= DisplayWordUtil.Replace("[出演者認証]") %>
				</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					<table border="0">
						<tr style="margin-bottom: 0px">
							<td valign="top">
								<table border="0" style="width: 600px" class="tableStyle">
									<tr>
										<td class="tdHeaderStyle">
											氏名
										</td>
										<td class="tdDataStyle">
											<asp:Label ID="lblCastNm" runat="server" Text="Label"></asp:Label>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											メールアドレス
										</td>
										<td class="tdDataStyle">
											<asp:Label ID="lblEmailAddr" runat="server" Text="Label"></asp:Label>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											電話番号
										</td>
										<td class="tdDataStyle">
											<asp:Label ID="lblTel" runat="server" Text="Label"></asp:Label>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											生年月日
										</td>
										<td class="tdDataStyle">
											<asp:Label ID="lblBirthDay" runat="server" Text="Label"></asp:Label>
											<asp:Label ID="lblBirthdayJp" runat="server" Text="Label"></asp:Label>
											<asp:Label ID="lblAge" runat="server" Text="Label"></asp:Label>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											申請日
										</td>
										<td class="tdDataStyle">
											<asp:Label ID="lblApplyDate" runat="server" Text="Label"></asp:Label>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											身分証
										</td>
										<td class="tdDataStyle">
											<asp:Label ID="lblsignUpFlag" runat="server" Text="Label"></asp:Label>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											担当
										</td>
										<td class="tdDataStyle">
											<asp:DropDownList ID="lstManager" runat="server" DataSourceID="dsManager" DataTextField="MANAGER_NM" DataValueField="MANAGER_SEQ" Width="250px">
											</asp:DropDownList>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											契約者番号
										</td>
										<td class="tdDataStyle">
											<asp:Label ID="lblImodeId" runat="server" Text="Label"></asp:Label>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											保護者の氏名
										</td>
										<td class="tdDataStyle">
											<asp:Label ID="lblGuardianNm" runat="server" Text="Label"></asp:Label>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle">
											保護者の電話番号
										</td>
										<td class="tdDataStyle">
											<asp:Label ID="lblGuardianTel" runat="server" Text="Label"></asp:Label>
										</td>
									</tr>
									<tr>
										<td class="tdHeaderStyle2">
											備考
										</td>
										<td class="tdDataStyle">
											<asp:TextBox ID="txtRemarks" runat="server" MaxLength="1000" Width="380px" Height="60px" TextMode="MultiLine"></asp:TextBox><br />
											<asp:Button ID="btnUpdateRmarks" runat="server" Text="更新" CssClass="seekbutton" OnClick="btnUpdateRmarks_Click" />
										</td>
									</tr>
									<tr>
										<td colspan="2" align="center">
											<asp:Label ID="lblExistUser" runat="server" Text="*メールアドレスまたは契約者番号に該当する会員が既に登録されています。認証することはできません。" ForeColor="red" Visible="false"></asp:Label>
										</td>
									</tr>
								</table>
								<asp:Button runat="server" ID="btnUpdate" Text="認証" CssClass="seekbutton" OnClick="btnUpdate_Click" />
								<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekrightbutton" OnClick="btnDelete_Click" />
								<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekrightbutton" OnClick="btnCancel_Click" CausesValidation="False" /><br />

								<asp:Panel ID="pnlGridDupCast" runat="server" ScrollBars="auto" Height="290px">
									<asp:Label ID="lblMessage" runat="server" Text="生年月日で重複があります。" ForeColor="red" Visible="false" />
									<asp:GridView ID="grdDupCast" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsDupCast" AllowSorting="False" SkinID="GridView" OnRowDataBound="grdDupCast_RowDataBound"
										HeaderStyle-Wrap="false" RowStyle-Wrap="false">
										<Columns>
											<asp:TemplateField HeaderText="ID">
												<ItemTemplate>
													<asp:HyperLink ID="lnkCastView" runat="server"
															NavigateUrl='<%# string.Format("../Cast/CastView.aspx?loginid={0}&return={1}",Eval("LOGIN_ID"),"ApplyCastMainte.aspx") %>'
															Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
												</ItemTemplate>
												<ItemStyle HorizontalAlign="Center" />
											</asp:TemplateField>

											<asp:TemplateField HeaderText="名前">
												<ItemTemplate>
													<asp:Label ID="lblCastNm2" runat="server" Text='<%# Eval("CAST_NM") %>'></asp:Label>
												</ItemTemplate>
												<ItemStyle HorizontalAlign="Center" />
											</asp:TemplateField>

											<asp:TemplateField HeaderText="身分証">
												<ItemTemplate>
													<asp:HyperLink ID="lnkCastPicViewer1" runat="server" Text="身分証1"
															NavigateUrl='<%# GeneratePicViewerUrl(Eval("ID_PIC_SEQ1")) %>'
															Visible='<%# GetImgVisible(Eval("ID_PIC_SEQ1")) %>'></asp:HyperLink>
													<asp:HyperLink ID="lnkCastPicViewer2" runat="server" Text="身分証2"
															NavigateUrl='<%# GeneratePicViewerUrl(Eval("ID_PIC_SEQ2")) %>'
															Visible='<%# GetImgVisible(Eval("ID_PIC_SEQ2")) %>'></asp:HyperLink>
													<asp:HyperLink ID="lnkCastPicViewer3" runat="server" Text="身分証3"
															NavigateUrl='<%# GeneratePicViewerUrl(Eval("ID_PIC_SEQ3")) %>'
															Visible='<%# GetImgVisible(Eval("ID_PIC_SEQ3")) %>'></asp:HyperLink>
													<asp:HyperLink ID="lnkCastPicViewer4" runat="server" Text="身分証4"
															NavigateUrl='<%# GeneratePicViewerUrl(Eval("ID_PIC_SEQ4")) %>'
															Visible='<%# GetImgVisible(Eval("ID_PIC_SEQ4")) %>'></asp:HyperLink>
													<asp:HyperLink ID="lnkCastPicViewer5" runat="server" Text="身分証5"
															NavigateUrl='<%# GeneratePicViewerUrl(Eval("ID_PIC_SEQ5")) %>'
															Visible='<%# GetImgVisible(Eval("ID_PIC_SEQ5")) %>'></asp:HyperLink>
													<asp:HyperLink ID="lnkCastPicViewer6" runat="server" Text="身分証6"
															NavigateUrl='<%# GeneratePicViewerUrl(Eval("ID_PIC_SEQ6")) %>'
															Visible='<%# GetImgVisible(Eval("ID_PIC_SEQ6")) %>'></asp:HyperLink>
													<asp:HyperLink ID="lnkCastPicViewer7" runat="server" Text="身分証7"
															NavigateUrl='<%# GeneratePicViewerUrl(Eval("ID_PIC_SEQ7")) %>'
															Visible='<%# GetImgVisible(Eval("ID_PIC_SEQ7")) %>'></asp:HyperLink>
													<asp:HyperLink ID="lnkCastPicViewer8" runat="server" Text="身分証8"
															NavigateUrl='<%# GeneratePicViewerUrl(Eval("ID_PIC_SEQ8")) %>'
															Visible='<%# GetImgVisible(Eval("ID_PIC_SEQ8")) %>'></asp:HyperLink>
													<asp:HyperLink ID="lnkCastPicViewer9" runat="server" Text="身分証9"
															NavigateUrl='<%# GeneratePicViewerUrl(Eval("ID_PIC_SEQ9")) %>'
															Visible='<%# GetImgVisible(Eval("ID_PIC_SEQ9")) %>'></asp:HyperLink>
													<asp:HyperLink ID="lnkCastPicViewer10" runat="server" Text="身分証10"
															NavigateUrl='<%# GeneratePicViewerUrl(Eval("ID_PIC_SEQ10")) %>'
															Visible='<%# GetImgVisible(Eval("ID_PIC_SEQ10")) %>'></asp:HyperLink>
												</ItemTemplate>
												<ItemStyle HorizontalAlign="Center" />
											</asp:TemplateField>
										</Columns>
										<PagerSettings Mode="NumericFirstLast" />
									</asp:GridView>
								</asp:Panel>
							</td>
							<td valign="top">
								<asp:Panel runat="server" ID="pnlPic" ScrollBars="auto" Height="600px">
									<table border="0" style="width: 500px" class="tableStyle">
										<asp:Repeater ID="rptUpload" runat="server">
											<ItemTemplate>
												<tr>
													<td class="tdHeaderSmallStyle2">
														<%# string.Concat("身分証画像<br />", Container.ItemIndex + 1) %>
													</td>
													<td class="tdDataStyle">
														<asp:FileUpload ID="uldIdPic" runat="server" Width="400px" />
														<asp:Button ID="btnUpload" runat="server" ValidationGroup='<%# string.Concat("Upload", Container.ItemIndex + 1) %>' Text='<%# string.Concat("ｱﾌﾟﾛｰﾄﾞ", Container.ItemIndex + 1) %>'
															CssClass="seekbutton" OnCommand="btnUpload_Command" CommandArgument='<%# Container.ItemIndex %>' CommandName="UPLOAD" /><br />
														<asp:RequiredFieldValidator ID="rfvUplod" runat="server" ErrorMessage='<%# string.Format("身分証ファイル{0}が入力されていません。", Container.ItemIndex + 1) %>' SetFocusOnError="True"
															ValidationGroup='<%# string.Concat("Upload", Container.ItemIndex+1) %>' ControlToValidate="uldIdPic"></asp:RequiredFieldValidator>
													</td>
												</tr>
												<tr>
													<td class="tdDataStyle" colspan="3">
														<asp:Image ID="imgIdPic" runat="server" Height="300px" Width="400px" ImageUrl='<%# GetImgUrl(Container.DataItem) %>' Visible='<%# GetImgVisible(Container.DataItem) %>' />
														<asp:Label ID="lblNoImage" runat="server" Text="未登録" ForeColor="Red" Visible="<%# !GetImgVisible(Container.DataItem) %>"></asp:Label>
													</td>
												</tr>
											</ItemTemplate>
										</asp:Repeater>
									</table>
								</asp:Panel>
							</td>
						</tr>
					</table>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsDupCast" runat="server" SelectMethod="GetDupCastPageCollection" TypeName="Cast" SelectCountMethod="GetDupCastPageCount" EnablePaging="True"
		OnSelecting="dsDupCast_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pUserSeq" Type="String" />
			<asp:Parameter Name="pBirthday" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsManager" runat="server" SelectMethod="GetList" TypeName="Manager"></asp:ObjectDataSource>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="認証を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を行ないますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
