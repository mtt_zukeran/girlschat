<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TalkMovieMainte.aspx.cs" Inherits="Cast_TalkMovieMainte" Title="会話動画認証" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="会話動画認証"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[動画認証]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					$NO_TRANS_START;				
					<table border="0" style="width: 640px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle">
								<asp:Label ID="lblHeader" runat="server" Text="Label"></asp:Label>
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblSiteCd" runat="server" Text="Label"></asp:Label>-
								<asp:Label ID="lblLoginId" runat="server" Text="Label"></asp:Label>
								<asp:Label ID="lblUserSeq" runat="server" Text="Label" Visible="false"></asp:Label>
								<asp:Label ID="lblUserCharNo" runat="server" Text="Label"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								<%# DisplayWordUtil.Replace("出演者名.")%>
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblCastNm" runat="server" Text="Label"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								動画タイトル
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtMovieTitle" runat="server" MaxLength="30" Width="313px"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								動画紹介文章
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtHtmlDoc" runat="server" MaxLength="1500" Width="500px" TextMode="MultiLine" Rows="5"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								消費ポイント
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtChargePoint" runat="server" MaxLength="3" Width="40px"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								動画ステータス
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstAuthType" runat="server" DataSourceID="dsAuthType" DataTextField="CODE_NM" DataValueField="CODE" Width="118px">
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								オススメ対象動画
							</td>
							<td class="tdDataStyle">
								<asp:CheckBox ID="chkPickupFlag" runat="server" />
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								再生秒数
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblRecordingSec" runat="server"></asp:Label>
							</td>
						</tr>
					</table>
					「分割設定」
					<asp:PlaceHolder ID="plcHolder" runat="server">
						<table border="0" style="width: 740px" class="tableStyle">
							<tr>
								<td class="tdHeaderSmallStyle">
									�@
								</td>
								<td class="tdDataStyle">
									開始位置秒数<br />
									<asp:TextBox ID="txtStartPosSec0" runat="server" MaxLength="4" Width="48px"></asp:TextBox>
								</td>
								<td class="tdDataStyle">
									再生秒数<br />
									<asp:TextBox ID="txtPlaySec0" runat="server" MaxLength="4" Width="48px"></asp:TextBox>
								</td>
								<td class="tdDataStyle">
									分割紹介文章<br />
									<asp:TextBox ID="txtPartHtml0" runat="server" MaxLength="300" Width="500px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderSmallStyle">
									�A
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtStartPosSec1" runat="server" MaxLength="4" Width="48px"></asp:TextBox>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtPlaySec1" runat="server" MaxLength="4" Width="48px"></asp:TextBox>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtPartHtml1" runat="server" MaxLength="300" Width="500px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderSmallStyle">
									�B
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtStartPosSec2" runat="server" MaxLength="4" Width="48px"></asp:TextBox>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtPlaySec2" runat="server" MaxLength="4" Width="48px"></asp:TextBox>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtPartHtml2" runat="server" MaxLength="300" Width="500px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderSmallStyle">
									�C
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtStartPosSec3" runat="server" MaxLength="4" Width="48px"></asp:TextBox>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtPlaySec3" runat="server" MaxLength="4" Width="48px"></asp:TextBox>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtPartHtml3" runat="server" MaxLength="300" Width="500px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderSmallStyle">
									�D
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtStartPosSec4" runat="server" MaxLength="4" Width="48px"></asp:TextBox>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtPlaySec4" runat="server" MaxLength="4" Width="48px"></asp:TextBox>
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtPartHtml4" runat="server" MaxLength="300" Width="500px"></asp:TextBox>
								</td>
							</tr>
						</table>
					</asp:PlaceHolder>
					$NO_TRANS_END;								
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
					<!--<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />-->
					<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsAuthType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="54" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtStartPosSec0" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtStartPosSec1" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtStartPosSec2" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtStartPosSec3" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtStartPosSec4" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPlaySec0" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPlaySec1" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPlaySec2" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPlaySec3" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtPlaySec4" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
