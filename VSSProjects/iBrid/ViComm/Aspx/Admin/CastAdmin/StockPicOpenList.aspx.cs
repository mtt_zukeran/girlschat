﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 公開中ストック画像一覧
--	Progaram ID		: StockPicOpenList
--
--  Creation Date	: 2011.08.04
--  Creater			: i-Brid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater				Update Explain

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class CastAdmin_StockPicOpenList : System.Web.UI.Page {
	private string recCount = string.Empty;

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
			GetList();
		}
	}

	protected void dsCastPic_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void FirstLoad() {
		grdPic.PageSize = 100;
		grdPic.DataSourceID = string.Empty;
		DataBind();
		if (Session["SiteCd"].ToString().Equals(string.Empty)) {
			lstSiteCd.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		}
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals(string.Empty)) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		lstSiteCd.DataSourceID = string.Empty;
	}

	private void InitPage() {
		grdPic.DataSourceID = string.Empty;
		DataBind();
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid)
			return;
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.Response.Redirect(this.Request.Url.PathAndQuery);
	}


	protected string GetClientScript(object pPicUrl) {
		string sRoot = Request.Url.Authority  + ConfigurationManager.AppSettings["Root"];
		return string.Format("javascript:win=window.open('http://{1}/ViewPic.html?url={0}','PictureZoom','width=300,height=320,resizable=yes,directories=no,scrollbars=no' , false);win.focus();return false;",pPicUrl,sRoot);
	}

	protected void btnUpdate_OnClick(object sender,EventArgs e) {
		string sSiteCd = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][0]).ToString();
		string sUserSeq = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][1]).ToString();
		string sUserCharNo = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][2]).ToString();
		string sPicSeq = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][3]).ToString();
		string sTxtTitle = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][5]).ToString();
		string sTxtDoc = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][6]).ToString();
		string sCastPicAttrTypeSeq = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][7]).ToString();
		string sCastPicAttrSeq = (grdPic.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][8]).ToString();
		UpdateProfilePicture(sSiteCd,sUserSeq,sUserCharNo,sPicSeq,"0","0",sTxtTitle,sTxtDoc,sCastPicAttrTypeSeq,sCastPicAttrSeq);

		bool bTxNotPublicNotice;
		using (ManageCompany oManageCompany = new ManageCompany()) {
			bTxNotPublicNotice = oManageCompany.IsAvailableService(ViCommConst.RELEASE_CAST_OBJ_NON_PUBLIC_INFO,2);
		}

		if (bTxNotPublicNotice) {
			Server.Transfer(string.Format("../CastAdmin/TxCastObjNonPublicMail.aspx?site={0}&userseq={1}&usercharNo={2}&mailtype={3}&return={4}",sSiteCd,sUserSeq,sUserCharNo,ViCommConst.MAIL_TP_CAST_STOCK_PIC_NP,"StockPicOpenList"));
		} else {
			Server.Transfer(string.Format("StockPicOpenList.aspx?sitecd={0}",sSiteCd));
		}
	}

	private void UpdateProfilePicture(
		string pSiteCd,
		string pUserSeq,
		string pUserCharNo,
		string pPicSeq,
		string pDelFlag,
		string pProfilePicFlag,
		string pTitle,
		string pDoc,
		string pCastPicAttrTypeSeq,
		string pCastPicAttrSeq
	) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_PIC_PROFILE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.NUMBER,pUserSeq);
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,pUserCharNo);
			db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.NUMBER,pPicSeq);
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureInParm("PPROFILE_PIC_FLAG",DbSession.DbType.NUMBER,pProfilePicFlag);
			db.ProcedureInParm("PPIC_TYPE",DbSession.DbType.NUMBER,ViCommConst.ATTACHED_MAIL);
			db.ProcedureInParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_OFF);
			db.ProcedureInParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER,ViCommConst.FLAG_ON);
			db.ProcedureInParm("PPIC_TITLE",DbSession.DbType.VARCHAR2,pTitle);
			db.ProcedureInParm("PPIC_DOC",DbSession.DbType.VARCHAR2,pDoc);
			db.ProcedureInParm("PCAST_PIC_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,pCastPicAttrTypeSeq);
			db.ProcedureInParm("PCAST_PIC_ATTR_SEQ",DbSession.DbType.VARCHAR2,pCastPicAttrSeq);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.cmd.BindByName = true;
			db.ExecuteProcedure();
		}
	}

	private void GetList() {
		grdPic.DataSourceID = "dsCastPic";
		grdPic.PageIndex = 0;
		grdPic.DataBind();
		pnlCount.DataBind();
	}

	protected void dsCastPic_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = "0";
		e.InputParameters[2] = "0";
		e.InputParameters[3] = ViCommConst.ATTACHED_MAIL.ToString();
		e.InputParameters[4] = string.IsNullOrEmpty(this.txtUploadDayFrom.Text.Replace("/",string.Empty)) ? null : this.txtUploadDayFrom.Text;
		e.InputParameters[5] = string.IsNullOrEmpty(this.txtUploadDayTo.Text.Replace("/",string.Empty)) ? null : this.txtUploadDayTo.Text;
		e.InputParameters[6] = this.txtLoginId.Text.TrimEnd();
	}
}
