﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 掲示板写真認証
--	Progaram ID		: BbsPicMainte
--
--  Creation Date	: 2010.05.18
--  Creater			: i-Brid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater		Update Explain
  2010/07/16	Koyanagi	写真ｽﾃｰﾀｽ「非公開」に対応

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Cast_BbsPicMainte:System.Web.UI.Page {

	protected string PicAttrTypeItemNo {
		get {
			return this.ViewState["PicAttrTypeItemNo"] as string;
		}
		set {
			this.ViewState["PicAttrTypeItemNo"] = value;
		}
	}

	protected string SeekSiteCd {
		get {
			return this.ViewState["SeekSiteCd"] as string;
		}
		set {
			this.ViewState["SeekSiteCd"] = value;
		}
	}

	protected string PrevNonPublicFlag {
		get {
			return this.ViewState["PrevNonPublicFlag"] as string;
		}
		set {
			this.ViewState["PrevNonPublicFlag"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		lblSiteCd.Text = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		lblUserSeq.Text = iBridUtil.GetStringValue(Request.QueryString["userseq"]);
		lblUserCharNo.Text = iBridUtil.GetStringValue(Request.QueryString["usercharno"]);
		ViewState["SITE_CD"] = lblSiteCd.Text;
		ViewState["USER_SEQ"] = lblUserSeq.Text;
		ViewState["USER_CHAR_NO"] = lblUserCharNo.Text;
		ViewState["PIC_SEQ"] = iBridUtil.GetStringValue(Request.QueryString["picseq"]);
		ViewState["RETURN"] = iBridUtil.GetStringValue(Request.QueryString["return"]);
		ViewState["OBJ_NOT_PUBLISH_FLAG"] = iBridUtil.GetStringValue(Request.QueryString["closed"]);
		PicAttrTypeItemNo = iBridUtil.GetStringValue(Request.QueryString["pictype_itemno"]);
		SeekSiteCd = iBridUtil.GetStringValue(Request.QueryString["seeksitecd"]);

		using (Cast oCast = new Cast()) {
			string sName = string.Empty,sId = string.Empty;
			oCast.GetValue(lblUserSeq.Text,"CAST_NM",ref sName);
			oCast.GetValue(lblUserSeq.Text,"LOGIN_ID",ref sId);
			lblUserNm.Text = sName;
			lblLoginId.Text = sId;
		}
		ClearField();
		DataBind();
		GetData();

		if (ViCommConst.FLAG_ON_STR.Equals(iBridUtil.GetStringValue(ConfigurationManager.AppSettings["enabledNoTitleObj"]))) {
			this.vdrPicTitle.Enabled = false;
		}
	}

	private void ClearField() {
		txtPicTitle.Text = string.Empty;
		lstAuthType.SelectedIndex = 0;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		ReturnToCall();
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {

			db.PrepareProcedure("CAST_PIC_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,ViewState["SITE_CD"].ToString());
			db.ProcedureInParm("PUSER_SEQ",DbSession.DbType.VARCHAR2,ViewState["USER_SEQ"].ToString());
			db.ProcedureInParm("PUSER_CHAR_NO",DbSession.DbType.VARCHAR2,ViewState["USER_CHAR_NO"].ToString());
			db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.VARCHAR2,ViewState["PIC_SEQ"].ToString());
			db.ProcedureOutParm("PPIC_TITLE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPIC_DOC",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PUPLOAD_DATE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPIC_TYPE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PCAST_PIC_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_PIC_ATTR_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				txtPicTitle.Text = db.GetStringValue("PPIC_TITLE");
				txtPicDoc.Text = db.GetStringValue("PPIC_DOC");
				string sUnAtuthFlag = db.GetStringValue("POBJ_NOT_APPROVE_FLAG");
				string sNotPublishFlag = db.GetStringValue("POBJ_NOT_PUBLISH_FLAG");
				if (sUnAtuthFlag.Equals("0")) {
					if (sNotPublishFlag.Equals("0")) {
						lstAuthType.SelectedValue = ViCommConst.MOVIE_APPLY_OK;
					} else if (sNotPublishFlag.Equals("1")) {
						lstAuthType.SelectedValue = ViCommConst.MOVIE_APPLY_NG;
					}
				} else if (sUnAtuthFlag.Equals("1")) {
					lstAuthType.SelectedValue = ViCommConst.MOVIE_APPLY_WAIT;
				}

				lstCastPicAttrType.DataBind();
				lstCastPicAttrType.DataSourceID = string.Empty;
				lstCastPicAttrType.SelectedValue = db.GetStringValue("PCAST_PIC_ATTR_TYPE_SEQ");
				lstCastPicAttrTypeValue.DataSourceID = "dsCastPicAttrTypeValue";
				lstCastPicAttrTypeValue.DataBind();
				lstCastPicAttrTypeValue.DataSourceID = string.Empty;
				lstCastPicAttrTypeValue.SelectedValue = db.GetStringValue("PCAST_PIC_ATTR_SEQ");
			} else {
				ClearField();
			}
		}
		pnlDtl.Visible = true;
		this.PrevNonPublicFlag = lstAuthType.SelectedValue;
	}

	private void UpdateData(int pDelFlag) {
		int iNotApproveFlag = 0;
		int iDelFlag = 0;
		int iNotPublishFlag = 0;

		if (pDelFlag == 0) {
			switch (lstAuthType.SelectedValue) {
				case ViCommConst.MOVIE_APPLY_WAIT:	// 認証待ち
					iDelFlag = 0;
					iNotApproveFlag = 1;
					iNotPublishFlag = 0;
					break;

				case ViCommConst.MOVIE_APPLY_OK:	// 公開
					iDelFlag = 0;
					iNotApproveFlag = 0;
					iNotPublishFlag = 0;
					break;

				case ViCommConst.MOVIE_APPLY_REMOVE: // 削除
					iNotApproveFlag = 0;
					iDelFlag = 1;
					iNotPublishFlag = 0;
					break;

				case ViCommConst.MOVIE_APPLY_NG:	// 非公開
					iNotApproveFlag = 0;
					iDelFlag = 0;
					iNotPublishFlag = 1;
					break;
			}
		} else {
			iDelFlag = 1;
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("CAST_PIC_MAINTE");
			db.ProcedureInParm("PPIC_SEQ",DbSession.DbType.VARCHAR2,ViewState["PIC_SEQ"].ToString());
			db.ProcedureInParm("PPIC_TITLE",DbSession.DbType.VARCHAR2,GetPicTitle(txtPicTitle.Text));
			db.ProcedureInParm("PPIC_DOC",DbSession.DbType.VARCHAR2,GetPicDoc(txtPicDoc.Text));
			db.ProcedureInParm("POBJ_NOT_APPROVE_FLAG",DbSession.DbType.NUMBER,iNotApproveFlag);
			db.ProcedureInParm("POBJ_NOT_PUBLISH_FLAG",DbSession.DbType.NUMBER,iNotPublishFlag);
			db.ProcedureInParm("PPIC_TYPE",DbSession.DbType.NUMBER,ViCommConst.ATTACHED_BBS);
			db.ProcedureInParm("PCAST_PIC_ATTR_TYPE_SEQ",DbSession.DbType.VARCHAR2,lstCastPicAttrType.SelectedValue);
			db.ProcedureInParm("PCAST_PIC_ATTR_SEQ",DbSession.DbType.VARCHAR2,lstCastPicAttrTypeValue.SelectedValue);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,iDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}

		if (iDelFlag == 1) {
			string sFileNm = string.Empty;
			string sWebPhisicalDir = string.Empty;

			string sSiteCd = ViewState["SITE_CD"].ToString();
			string sUserSeq = ViewState["USER_SEQ"].ToString();
			string sUserCharNo = ViewState["USER_CHAR_NO"].ToString();
			string sPicSeq = ViewState["PIC_SEQ"].ToString();
			using (Site oSite = new Site()) {
				oSite.GetValue(sSiteCd,"WEB_PHISICAL_DIR",ref sWebPhisicalDir);
			}

			sFileNm = iBridUtil.addZero(sPicSeq, ViCommConst.OBJECT_NM_LENGTH);
			ViCommPrograms.DeleteFiles(ViCommPrograms.GetCastPicDir(sWebPhisicalDir, sSiteCd, lblLoginId.Text), string.Format("{0}*", sFileNm));

		}

		bool bTxNotPublicNotice;
		using (ManageCompany oManageCompany = new ManageCompany()) {
			bTxNotPublicNotice = oManageCompany.IsAvailableService(ViCommConst.RELEASE_CAST_OBJ_NON_PUBLIC_INFO,2);
		}

		if (bTxNotPublicNotice && !this.PrevNonPublicFlag.Equals(ViCommConst.MOVIE_APPLY_NG) && this.lstAuthType.SelectedValue.Equals(ViCommConst.MOVIE_APPLY_NG)) {
			Server.Transfer(string.Format("./TxCastObjNonPublicMail.aspx?site={0}&seeksitecd={1}&userseq={2}&usercharNo={3}&mailtype={4}&return={5}",ViewState["SITE_CD"].ToString(),this.SeekSiteCd,ViewState["USER_SEQ"].ToString(),ViewState["USER_CHAR_NO"].ToString(),ViCommConst.MAIL_TP_CAST_BBS_PIC_NP,ViewState["RETURN"].ToString()));
		} else {
			ReturnToCall();
		}
	}

	private string GetPicTitle(string name) {
		byte[] byteStr = System.Text.Encoding.UTF8.GetBytes(name);
		string ret = name;
		if (byteStr.Length > 90) {
			ret = SysPrograms.Substring(name, 30);
		}
		return ret;
	}

	private string GetPicDoc(string name) {
		byte[] byteStr = System.Text.Encoding.UTF8.GetBytes(name);
		string ret = name;
		if (byteStr.Length > 3000) {
			ret = SysPrograms.Substring(name, 1000);
		}
		return ret;
	}
	private void ReturnToCall() {
		if (ViewState["RETURN"].ToString().Equals("BbsPicCheckList")) {
			Server.Transfer(string.Format("../CastAdmin/BbsPicCheckList.aspx?bbspicsite={0}&closed={1}&pictype_itemno={2}&seeksitecd={3}",ViewState["SITE_CD"].ToString(),ViewState["OBJ_NOT_PUBLISH_FLAG"].ToString(),this.PicAttrTypeItemNo,this.SeekSiteCd));

		} else if (ViewState["RETURN"].ToString().Equals("BbsPicOpenList")) {
			Server.Transfer(string.Format("../CastAdmin/BbsPicOpenList.aspx?bbspicsite={0}&pictype_itemno={1}",ViewState["SITE_CD"].ToString(),this.PicAttrTypeItemNo));

		//} else if (ViewState["RETURN"].ToString().Equals("BbsPicClosedList")) {
		//    Server.Transfer(string.Format("../CastAdmin/BbsPicClosedList.aspx?bbspicsite={0}",ViewState["SITE_CD"].ToString()));
		
		} else {
			Server.Transfer(string.Format("../Cast/CastView.aspx?loginid={0}&bbspicsite={1}",lblLoginId.Text,ViewState["SITE_CD"].ToString()));
		}
	}

	protected void lstCastPicAttrType_SelectedIndexChanged(object sender,EventArgs e) {
		lstCastPicAttrTypeValue.DataSourceID = "dsCastPicAttrTypeValue";
		lstCastPicAttrTypeValue.DataBind();
		lstCastPicAttrTypeValue.DataSourceID = string.Empty;
	}

	protected void dsCastPicAttrType_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = ViewState["SITE_CD"].ToString();
	}

	protected void dsCastPicAttrTypeValue_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = ViewState["SITE_CD"].ToString();
		e.InputParameters[1] = lstCastPicAttrType.SelectedValue;
	}
}
