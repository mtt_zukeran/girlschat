<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ProfileMovieOpenList.aspx.cs" Inherits="Cast_ProfileMovieOpenList"
	Title="公開中プロフ動画" ValidateRequest="false" %>
<%@ Import Namespace="ViComm" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="公開中プロフ動画"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 740px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle2">
							アップロード日付
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtUploadDayFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RangeValidator ID="vdeUploadDayFrom" runat="server" ControlToValidate="txtUploadDayFrom" ErrorMessage="アップロード日付Fromを正しく入力して下さい。" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							〜&nbsp;
							<asp:TextBox ID="txtUploadDayTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RangeValidator ID="vdeUploadDayTo" runat="server" ControlToValidate="txtUploadDayTo" ErrorMessage="アップロード日付Toを正しく入力して下さい。" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							ログインＩＤ
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtLoginId" runat="server"></asp:TextBox>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="true" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
				<asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" OnClick="btnCSV_Click" CausesValidation="true" ValidationGroup="Key" />
			</asp:Panel>
		</fieldset>
		<fieldset>
			<legend>[公開中プロフ動画一覧]</legend>
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="540px">
				<asp:GridView ID="grdMovie" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCastMovie" AllowSorting="True" SkinID="GridViewColor">
					<Columns>
						<asp:BoundField DataField="SITE_NM" HeaderText="サイト"></asp:BoundField>
						<asp:BoundField DataField="LOGIN_ID" HeaderText="ﾛｸﾞｲﾝID" HeaderStyle-CssClass="Pad"></asp:BoundField>
						<asp:TemplateField HeaderText="ハンドル名" SortExpression="HANDLE_NM">
							<ItemTemplate>
								<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# string.Format("../Cast/CastView.aspx?loginid={0}&return={1}",Eval("LOGIN_ID"),"ProfileMovieOpenList.aspx") %>'
									Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:HyperLink>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="動画確認">
							<ItemStyle HorizontalAlign="Center" />
							<ItemTemplate>
								<a href="<%# PickupHelper.GeneratePickupObjsViewerUrl(this, Eval("SITE_CD").ToString(),Eval("MOVIE_SEQ").ToString(), ViComm.ViCommConst.PicupTypes.PROFILE_MOVIE) %>">
									確認 </a>
							</ItemTemplate>
							<HeaderStyle CssClass="Pad" />
						</asp:TemplateField>
						<asp:HyperLinkField HeaderText="動画設定" Text="設定" DataNavigateUrlFields="SITE_CD,USER_SEQ,USER_CHAR_NO,MOVIE_SEQ" DataNavigateUrlFormatString="../cast/ProfileMovieMainte.aspx?sitecd={0}&userseq={1}&usercharno={2}&movieseq={3}&return=ProfileMovieOpenList">
							<ItemStyle HorizontalAlign="Center" />
						</asp:HyperLinkField>
                        <asp:TemplateField HeaderText="認証">
                            <ItemTemplate>
                                <asp:Button ID="btnBbsMovieUpdate" runat="server" CommandArgument='<%# string.Format("{0},{1},{2},{3}",Eval("SITE_CD"),Eval("USER_SEQ"),Eval("USER_CHAR_NO"),Eval("MOVIE_SEQ")) %>'
                                    OnCommand="btnBbsMovieUpdate_OnCommand" Text="非公開" OnClientClick="return confirm('設定を行いますか？');">
                                </asp:Button>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
						<asp:BoundField DataField="MOVIE_TITLE" HeaderText="タイトル"></asp:BoundField>
						<asp:BoundField DataField="UPLOAD_DATE" HeaderText="アップロード日時" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" />
						</asp:BoundField>
					</Columns>
					<PagerSettings Mode="NumericFirstLast" />
				</asp:GridView>
			</asp:Panel>
			&nbsp;
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br />
				<a class="reccount">Current viewing page
					<%=grdMovie.PageIndex + 1%>
					of
					<%=grdMovie.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsCastMovie" runat="server" SelectMethod="GetApproveManagePageCollection" TypeName="CastMovie" SelectCountMethod="GetApproveManagePageCount"
		EnablePaging="True" OnSelected="dsCastMovie_Selected" OnSelecting="dsCastMovie_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pNotApproveFlag" Type="String" />
			<asp:Parameter Name="pNotPublishFlag" Type="String" />
			<asp:Parameter Name="pMovieType" Type="String" />
			<asp:Parameter Name="pUploadDateFrom" Type="String" />
			<asp:Parameter Name="pUploadDateTo" Type="String" />
			<asp:Parameter Name="pLoginId" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:MaskedEditExtender ID="mskUploadDayFrom" runat="server" TargetControlID="txtUploadDayFrom" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay"
		ClearMaskOnLostFocus="true">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:ValidatorCalloutExtender ID="vceUploadDayFrom" runat="Server" TargetControlID="vdeUploadDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskUploadDayTo" runat="server" TargetControlID="txtUploadDayTo" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay"
		ClearMaskOnLostFocus="true">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:ValidatorCalloutExtender ID="vceUploadDayTo" runat="Server" TargetControlID="vdeUploadDayTo" HighlightCssClass="validatorCallout" />
</asp:Content>
