<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PickupMainte.aspx.cs" Inherits="Cast_PickupMainte" Title="ピックアップ設定" ValidateRequest="false" %>
<%@ Import Namespace="ViComm" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="ピックアップ設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[タイトル設定]</legend>
			<fieldset class="fieldset-inner">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:Label ID="lblSiteNm" runat="server" Text=""></asp:Label>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							ピックアップタイトル
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtPickupTitle" runat="server" MaxLength="40"></asp:TextBox>
							<asp:RequiredFieldValidator ID="vdrPickupTitle" runat="server" ErrorMessage="ピックアップタイトルを入力して下さい。" ControlToValidate="txtPickupTitle">*</asp:RequiredFieldValidator>
						</td>
					</tr>
				</table>
				<asp:Button ID="btnSubmitUpdate" runat="server" Text="ピックアップ更新" OnClick="btnSubmitUpdate_Click" CssClass="seekbutton" />
				<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
			</fieldset>
		</fieldset>
		<fieldset class="fieldset">
			<legend>[キャラクター設定]</legend>
			<fieldset class="fieldset-inner">
				<asp:DataList ID="lstCharacter" runat="server">
					<ItemTemplate>
						<table border="0" style="width: 640px" class="tableStyle">
							<colgroup>
								<col width="20%">
								<col width="35%">
								<col width="45%">
							</colgroup>
							<tr style="background-color: #E0E0E0">
								<td>
									<asp:Label ID="lblCastId" runat="server" Text="出演者ID"></asp:Label>
								</td>
								<td>
									<asp:Label ID="lblCastNm" runat="server" Text="出演者名"></asp:Label></td>
								<td>
									<asp:Label ID="Label4" runat="server" Text="カテゴリ"></asp:Label></td>
							</tr>
							<tr>
								<td>
									<asp:Label ID="lblloginId" runat="server" Text='<%# Eval("LOGIN_ID") %>'></asp:Label>
									<asp:Label ID="lblUserSeq" runat="server" Text='<%# Eval("USER_SEQ") %>' Visible="false"></asp:Label>
								</td>
								<td>
									<asp:Label ID="lblHandleNm" runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:Label></td>
								<td>
									<asp:Label ID="lblCategoryNm" runat="server" Text='<%# Eval("ACT_CATEGORY_NM") %>'></asp:Label></td>
							</tr>
							<tr>
								<td>
									<asp:Label ID="Label5" runat="server" Text="コメント"></asp:Label>
								</td>
								<td colspan="2">
									<asp:TextBox ID="txtComment" runat="server" Columns="60" Rows="3" TextMode="MultiLine" Text='<%# Eval("COMMENT_PICKUP") %>'></asp:TextBox></td>
							</tr>
						</table>
					</ItemTemplate>
					<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
				</asp:DataList>
			</fieldset>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrPickupTitle" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnSubmitUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
