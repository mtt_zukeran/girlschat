﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TxCastObjNonPublicMail.aspx.cs" Inherits="CastAdmin_TxCastObjNonPublicMail" Title="出演者投稿非公開通知メール送信" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text=""></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[メール送信]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					<table border="0" style="width: 640px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle2">
								ﾒｰﾙﾃﾝﾌﾟﾚｰﾄ
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstMailTemplate" runat="server">
								</asp:DropDownList>
								<asp:Label ID="lblMessage" runat="server" Text="メールを送信しました。" Visible="false" />
							</td>
						</tr>
					</table>
					<asp:Button runat="server" ID="btnTxMail" Text="送信" CssClass="seekbutton" OnClick="btnTxMail_Click" ValidationGroup="Detail" /><br /><br />
					<asp:LinkButton ID="lnkReturnPage" runat="server" CausesValidation="false" CommandName="" OnCommand="lnkReturnPage_Command" Text="戻る"></asp:LinkButton>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
	</div>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnTxMail" ConfirmText="メールを送信しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
