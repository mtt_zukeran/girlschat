<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BbsPicMainte.aspx.cs" Inherits="Cast_BbsPicMainte" Title="掲示板写真認証" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="掲示板写真認証"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[写真認証]</legend>
				<asp:Panel runat="server" ID="pnlDtl">
					$NO_TRANS_START;
					<table border="0" style="width: 640px" class="tableStyle">
						<tr>
							<td class="tdHeaderStyle">
								ｻｲﾄ／SEQ／ｷｬﾗｸﾀｰNo.
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblSiteCd" runat="server" Text="Label"></asp:Label>-
								<asp:Label ID="lblLoginId" runat="server" Text="Label"></asp:Label>-
								<asp:Label ID="lblUserSeq" runat="server" Text="Label" Visible="false"></asp:Label>
								<asp:Label ID="lblUserCharNo" runat="server" Text="Label"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								<%= DisplayWordUtil.Replace("出演者名.") %>
							</td>
							<td class="tdDataStyle">
								<asp:Label ID="lblUserNm" runat="server" Text="Label"></asp:Label>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								写真属性
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstCastPicAttrType" runat="server" DataSourceID="dsCastPicAttrType" DataTextField="CAST_PIC_ATTR_TYPE_NM" DataValueField="CAST_PIC_ATTR_TYPE_SEQ"
									AutoPostBack="True" OnSelectedIndexChanged="lstCastPicAttrType_SelectedIndexChanged">
								</asp:DropDownList>
								<asp:RequiredFieldValidator ID="vdrCastPicAttrType" runat="server" ErrorMessage="写真属性を選択して下さい。" ControlToValidate="lstCastPicAttrType" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								写真属性値
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstCastPicAttrTypeValue" runat="server" DataSourceID="dsCastPicAttrTypeValue" DataTextField="CAST_PIC_ATTR_NM" DataValueField="CAST_PIC_ATTR_SEQ">
								</asp:DropDownList>
								<asp:RequiredFieldValidator ID="vdrCastPicAttrTypeValue" runat="server" ErrorMessage="写真属性値を選択して下さい。" ControlToValidate="lstCastPicAttrTypeValue" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle">
								写真タイトル
							</td>
							<td class="tdDataStyle">
								<asp:TextBox ID="txtPicTitle" runat="server" MaxLength="36" Width="313px"></asp:TextBox>
								<asp:RequiredFieldValidator ID="vdrPicTitle" runat="server" ErrorMessage="写真タイトルを入力して下さい。" ControlToValidate="txtPicTitle" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle" style="height: 71px">
								写真本文
							</td>
							<td class="tdDataStyle" style="height: 71px">
								<asp:TextBox ID="txtPicDoc" runat="server" MaxLength="1000" Width="313px" Height="74px" TextMode="MultiLine"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td class="tdHeaderStyle2">
								写真ステータス
							</td>
							<td class="tdDataStyle">
								<asp:DropDownList ID="lstAuthType" runat="server" DataSourceID="dsAuthType" DataTextField="CODE_NM" DataValueField="CODE" Width="118px">
								</asp:DropDownList>
							</td>
						</tr>
					</table>
					$NO_TRANS_END;
					<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
					<!--<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />-->
					<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
				</asp:Panel>
			</fieldset>
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsAuthType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="54" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastPicAttrType" runat="server" SelectMethod="GetListIncDefault" TypeName="CastPicAttrType" OnSelecting="dsCastPicAttrType_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastPicAttrTypeValue" runat="server" SelectMethod="GetListIncDefault" TypeName="CastPicAttrTypeValue" OnSelecting="dsCastPicAttrTypeValue_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pCastPicAttrTypeSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrPicTitle" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrCastPicAttrType" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrCastPicAttrTypeValue" HighlightCssClass="validatorCallout" />
</asp:Content>
