<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BbsMovieOpenList.aspx.cs" Inherits="Cast_BbsMovieOpenList"
	Title="公開中掲示板動画" ValidateRequest="false" %>
<%@ Import Namespace="ViComm" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="公開中掲示板動画"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 740px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" 
							    OnSelectedIndexChanged="lstSiteCd_SelectedIndexChanged" AutoPostBack="True" Width="240px">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle">
							アップロード日付
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtUploadDayFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RangeValidator ID="vdeUploadDayFrom" runat="server" ControlToValidate="txtUploadDayFrom" ErrorMessage="アップロード日付Fromを正しく入力して下さい。" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							〜&nbsp;
							<asp:TextBox ID="txtUploadDayTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RangeValidator ID="vdeUploadDayTo" runat="server" ControlToValidate="txtUploadDayTo" ErrorMessage="アップロード日付Toを正しく入力して下さい。" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							動画属性区分
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstCastMovieAttrType" runat="server" Width="240px" AutoPostBack="true"
							    DataSourceID="" DataTextField="CAST_MOVIE_ATTR_TYPE_NM" DataValueField="CAST_MOVIE_ATTR_TYPE_SEQ" 
							    OnSelectedIndexChanged="lstCastMovieAttrType_SelectedIndexChanged">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle2">
							動画属性値
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstCastMovieAttrTypeValue" runat="server" 
							    DataSourceID="" DataTextField="CAST_MOVIE_ATTR_NM" DataValueField="CAST_MOVIE_ATTR_SEQ" Width="240px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							ログインＩＤ
						</td>
						<td class="tdDataStyle" colspan="3">
							<asp:TextBox ID="txtLoginId" runat="server"></asp:TextBox>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="True" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
				<asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" CausesValidation="True" OnClick="btnCSV_Click" />
			</asp:Panel>
		</fieldset>
		<fieldset>
			<legend>[公開中掲示板動画一覧]</legend>
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="540px">
				<asp:GridView ID="grdMovie" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCastMovie" AllowSorting="True" SkinID="GridViewColor">
					<Columns>
						<asp:BoundField DataField="SITE_NM" HeaderText="サイト">
							<ItemStyle Wrap="false" />
						</asp:BoundField>
						<asp:BoundField DataField="LOGIN_ID" HeaderText="ﾛｸﾞｲﾝID">
							<HeaderStyle Wrap="false" />
							<ItemStyle Wrap="false" />
						</asp:BoundField>
						<asp:TemplateField HeaderText="ハンドル名" SortExpression="HANDLE_NM">
							<ItemStyle Wrap="false" />
							<ItemTemplate>
								<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# string.Format("../Cast/CastView.aspx?loginid={0}&return={1}",Eval("LOGIN_ID"),"BbsMovieOpenList.aspx") %>'
									Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:HyperLink>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="動画確認">
							<HeaderStyle Wrap="false" />
							<ItemStyle HorizontalAlign="Center" Wrap="false" />
							<ItemTemplate>
								<a href="<%# PickupHelper.GeneratePickupObjsViewerUrl(this, Eval("SITE_CD").ToString(),Eval("MOVIE_SEQ").ToString(), ViComm.ViCommConst.PicupTypes.BBS_MOVIE) %>">
									確認 </a>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="動画設定">
							<HeaderStyle Wrap="false" />
							<ItemStyle HorizontalAlign="Center" />
							<ItemTemplate>
								<asp:LinkButton ID="lnkMainte" runat="server" CommandArgument='<%# string.Format("{0}:{1}:{2}:{3}:{4}",Eval("SITE_CD"),Eval("USER_SEQ"),Eval("USER_CHAR_NO"),Eval("MOVIE_SEQ"),"BbsMovieOpenList") %>'
									OnCommand="lnkMainte_Command" Text='<%# Eval("NOT_APPROVE_MARK_ADMIN") %>'></asp:LinkButton>
							</ItemTemplate>
						</asp:TemplateField>						
						<asp:BoundField DataField="MOVIE_TITLE" HeaderText="タイトル">
							<ItemStyle Width="150px" HorizontalAlign="Left" />
						</asp:BoundField>
						<asp:BoundField DataField="MOVIE_DOC" HeaderText="内容" HtmlEncode="False">
							<ItemStyle Width="150px" HorizontalAlign="Left" />
						</asp:BoundField>
						<asp:BoundField DataField="CAST_MOVIE_ATTR_TYPE_NM" HeaderText="動画属性" HtmlEncode="False">
							<HeaderStyle Wrap="false" />
						</asp:BoundField>
						<asp:BoundField DataField="CAST_MOVIE_ATTR_NM" HeaderText="動画属性値" HtmlEncode="False">
							<HeaderStyle Wrap="false" />
						</asp:BoundField>
                        <asp:TemplateField HeaderText="認証">
                            <ItemTemplate>
                                <asp:Button ID="btnBbsMovieUpdate" runat="server" CommandArgument='<%# string.Format("{0},{1},{2},{3}",Eval("SITE_CD"),Eval("USER_SEQ"),Eval("USER_CHAR_NO"),Eval("MOVIE_SEQ")) %>'
                                    OnCommand="btnBbsMovieUpdate_OnCommand" Text="非公開" OnClientClick="return confirm('設定を行いますか？');">
                                </asp:Button>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
						<asp:BoundField DataField="UPLOAD_DATE" HeaderText="アップロード日時" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" Wrap="false" />
						</asp:BoundField>
					</Columns>
					<PagerSettings Mode="NumericFirstLast" />
				</asp:GridView>
			</asp:Panel>
			&nbsp;
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br />
				<a class="reccount">Current viewing page
					<%=grdMovie.PageIndex + 1%>
					of
					<%=grdMovie.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsCastMovie" runat="server" SelectMethod="GetApproveManagePageCollection" TypeName="CastMovie" SelectCountMethod="GetApproveManagePageCount"
		EnablePaging="True" OnSelected="dsCastMovie_Selected" OnSelecting="dsCastMovie_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pNotApproveFlag" Type="String" />
			<asp:Parameter Name="pNotPublishFlag" Type="String" />
			<asp:Parameter Name="pMovieType" Type="String" />
			<asp:Parameter Name="pUploadDateFrom" Type="String" />
			<asp:Parameter Name="pUploadDateTo" Type="String" />
			<asp:Parameter Name="pCastMovieAttrType" Type="String" />
			<asp:Parameter Name="pCastMovieAttrTypeValue" Type="String" />
			<asp:Parameter Name="pLoginId" Type="String" />
			<asp:QueryStringParameter Name="pItemNo" QueryStringField="movietype_itemno" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastMovieAttrType" runat="server" SelectMethod="GetListByItemNo" TypeName="CastMovieAttrType" OnSelecting="dsCastMovieAttrType_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:QueryStringParameter Name="pItemNo" QueryStringField="movietype_itemno" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsCastMovieAttrTypeValue" runat="server" SelectMethod="GetListIncDefault" TypeName="CastMovieAttrTypeValue" OnSelecting="dsCastMovieAttrTypeValue_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pCastMovieAttrTypeSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:MaskedEditExtender ID="mskUploadDayFrom" runat="server" TargetControlID="txtUploadDayFrom" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay"
		ClearMaskOnLostFocus="true">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:ValidatorCalloutExtender ID="vceUploadDayFrom" runat="Server" TargetControlID="vdeUploadDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskUploadDayTo" runat="server" TargetControlID="txtUploadDayTo" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay"
		ClearMaskOnLostFocus="true">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:ValidatorCalloutExtender ID="vceUploadDayTo" runat="Server" TargetControlID="vdeUploadDayTo" HighlightCssClass="validatorCallout" />
</asp:Content>
