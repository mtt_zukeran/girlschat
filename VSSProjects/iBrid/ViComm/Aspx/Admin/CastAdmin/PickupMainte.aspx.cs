﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ピックアップメンテナンス
--	Progaram ID		: PickupMainte
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Cast_PickupMainte:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			string sCheckedId = Request.QueryString["id"].ToString();
			lblSiteNm.Text = iBridUtil.GetStringValue(Request.QueryString["sitenm"].ToString());

			ViewState["SITE_CD"] = Request.QueryString["sitecd"].ToString();
			ViewState["ACT_CATEGORY_SEQ"] = Request.QueryString["categoryseq"].ToString();

			GetData();
			char c = ',';
			string[] sCastId = sCheckedId.Split(new char[] { c });

			using (CastCharacter oCharacter = new CastCharacter()) {
				lstCharacter.DataSource = oCharacter.GetPickUpCast(ViewState["SITE_CD"].ToString(),ViewState["ACT_CATEGORY_SEQ"].ToString(),sCastId);
				lstCharacter.DataBind();
			}

			for (int i = 0;i < lstCharacter.Items.Count;i++) {
				DataListItem di = (DataListItem)lstCharacter.Items[i];
				Label lblCastId = (Label)di.FindControl("lblCastId");
				Label lblCastNm = (Label)di.FindControl("lblCastNm");
				lblCastId.Text = DisplayWordUtil.Replace(lblCastId.Text);
				lblCastNm.Text = DisplayWordUtil.Replace(lblCastNm.Text);
			}
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		Server.Transfer(string.Format("PickupList.aspx?sitecd={0}&categoryseq={0}",ViewState["SITE_CD"].ToString(),ViewState["ACT_CATEGORY_SEQ"].ToString()));
	}

	protected void btnSubmitUpdate_Click(object sender,EventArgs e) {
		UpdateData();
		string sUserSeq = "";
		string sComment = "";
		for (int i = 0;i < lstCharacter.Items.Count;i++) {
			DataListItem di = (DataListItem)lstCharacter.Items[i];
			Label lblUserSeq = (Label)di.FindControl("lblUserSeq");
			TextBox txtComment = (TextBox)di.FindControl("txtComment");
			sUserSeq = sUserSeq + "," + lblUserSeq.Text;
			sComment = sComment + "," + txtComment.Text;
		}
		if (sUserSeq.Length > 0) {
			sUserSeq = sUserSeq.Substring(1);
		}
		if (sComment.Length > 0) {
			sComment = sComment.Substring(1);
		}

		char c = ',';
		string[] slUserSeq = sUserSeq.Split(new char[] { c });
		string[] slComment = sComment.Split(new char[] { c });

		//PickUpはメインキャラのみ対応
		string[] sUpdateUserCharNo = new string[slUserSeq.Length];
		for (int i = 0;i < slUserSeq.Length;i++) {
			sUpdateUserCharNo.SetValue(ViCommConst.MAIN_CHAR_NO,i);
		}

		// Get Old Pickup
		string[] sDelUserSeq = null;
		using (CastCharacter oCharacter = new CastCharacter()) {
			sDelUserSeq = oCharacter.GetPickUpCast(ViewState["SITE_CD"].ToString(),ViewState["ACT_CATEGORY_SEQ"].ToString());
		}

		//PickUpはメインキャラのみ対応
		string[] sDeleteUserCharNo = null;
		int iDelCount = 0;

		if (sDelUserSeq == null) {
			sDelUserSeq = new string[1];
			sDelUserSeq[0] = "";
			sDeleteUserCharNo = new string[1];
			sDeleteUserCharNo[0] = "";
		} else {
			sDeleteUserCharNo = new string[sDelUserSeq.Length];
			for (int i = 0;i < sDelUserSeq.Length;i++) {
				sDeleteUserCharNo.SetValue(ViCommConst.MAIN_CHAR_NO,i);
			}
		}
		iDelCount = sDelUserSeq.Length;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("PICKUP_CHARACTER_MAINTE");
			db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,ViewState["SITE_CD"].ToString());
			db.ProcedureInArrayParm("pDELETE_USER_SEQ",DbSession.DbType.VARCHAR2,iDelCount,sDelUserSeq);
			db.ProcedureInArrayParm("pDELETE_USER_CHAR_NO",DbSession.DbType.VARCHAR2,iDelCount,sDeleteUserCharNo);
			db.ProcedureInParm("pDELETE_RECORD_COUNT",DbSession.DbType.NUMBER,iDelCount);
			db.ProcedureInArrayParm("pUPDATE_USER_SEQ",DbSession.DbType.VARCHAR2,slUserSeq.Length,slUserSeq);
			db.ProcedureInArrayParm("pUPDATE_USER_CHAR_NO",DbSession.DbType.VARCHAR2,slUserSeq.Length,sUpdateUserCharNo);
			db.ProcedureInArrayParm("pUPDATE_COMMENT_PICKUP",DbSession.DbType.VARCHAR2,slUserSeq.Length,slComment);
			db.ProcedureInParm("pUPDATE_RECORD_COUNT",DbSession.DbType.NUMBER,slUserSeq.Length);
			db.ProcedureInParm("pMODE",DbSession.DbType.VARCHAR2,"PICKUP");
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		Server.Transfer(string.Format("PickupList.aspx?sitecd={0}",ViewState["SITE_CD"].ToString()));
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("PICKUP_TITLE_GET");
			db.ProcedureInParm("PACT_CATEGORY_SEQ",DbSession.DbType.VARCHAR2,ViewState["ACT_CATEGORY_SEQ"].ToString());
			db.ProcedureOutParm("PPICKUP_TITLE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			txtPickupTitle.Text = db.GetStringValue("PPICKUP_TITLE");
		}
	}

	private void UpdateData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("PICKUP_TITLE_MAINTE");
			db.ProcedureInParm("PPICKUP_TITLE",DbSession.DbType.VARCHAR2,txtPickupTitle.Text);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

}
