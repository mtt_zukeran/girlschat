﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="StockPicOpenList.aspx.cs" Inherits="CastAdmin_StockPicOpenList" Title="公開中ストック画像"
	ValidateRequest="false" %>
<%@ Import Namespace="ViComm" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="公開中ストック画像"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 740px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト

						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle2">
							アップロード日付

						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtUploadDayFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RangeValidator ID="vdeUploadDayFrom" runat="server" ControlToValidate="txtUploadDayFrom" ErrorMessage="アップロード日付Fromを正しく入力して下さい。" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							～&nbsp;
							<asp:TextBox ID="txtUploadDayTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
							<asp:RangeValidator ID="vdeUploadDayTo" runat="server" ControlToValidate="txtUploadDayTo" ErrorMessage="アップロード日付Toを正しく入力して下さい。" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							ログインＩＤ
						</td>
						<td class="tdDataStyle" colspan="3">
							<asp:TextBox ID="txtLoginId" runat="server"></asp:TextBox>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="True" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
			</asp:Panel>
		</fieldset>
		<fieldset>
			<legend>[公開中ストック画像一覧]</legend>
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="540px">
				<asp:GridView ID="grdPic" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsCastPic" SkinID="GridViewColor" DataKeyNames="SITE_CD,USER_SEQ,USER_CHAR_NO,PIC_SEQ,LOGIN_ID,PIC_TITLE,PIC_DOC,CAST_PIC_ATTR_TYPE_SEQ,CAST_PIC_ATTR_SEQ">
					<Columns>
						<asp:TemplateField HeaderText="写真" SortExpression="PIC_SEQ">
							<ItemTemplate>
								<a href="<%# PickupHelper.GeneratePickupObjsViewerUrl(this, Eval("SITE_CD").ToString(),Eval("PIC_SEQ").ToString(), ViComm.ViCommConst.PicupTypes.MAIL_STOCK_PIC, ViCommConst.FLAG_OFF) %>">
									<img src="<%# string.Format("{0}?t={1}",Eval("OBJ_SMALL_PHOTO_IMG_PATH", "../{0}"),DateTime.Now.ToString("HHmmssfffff")) %>" alt="ﾒｰﾙｽﾄｯｸ画像" />
								</a>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField DataField="LOGIN_ID" HeaderText="ﾛｸﾞｲﾝID">
							<ItemStyle Wrap="False" />
						</asp:BoundField>
						<asp:TemplateField HeaderText="ハンドル名">
							<ItemStyle Wrap="False" />
							<ItemTemplate>
								<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# string.Format("../Cast/CastView.aspx?loginid={0}&return={1}",Eval("LOGIN_ID"),"StockPicCheckList.aspx") %>'
									Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:HyperLink>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField DataField="UPLOAD_DATE" HeaderText="アップロード日時" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" Wrap="False" />
						</asp:BoundField>
						<asp:TemplateField HeaderText="認証">
							<ItemStyle HorizontalAlign="Center" />
							<ItemTemplate>
								<asp:Button ID="btnUpdate" runat="server" CommandArgument='<%# Container.DataItemIndex %>' OnClick="btnUpdate_OnClick" Text="非公開" OnClientClick="return confirm('設定を行いますか？');">
								</asp:Button>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:HyperLinkField HeaderText="写真設定" Text="設定" DataNavigateUrlFields="SITE_CD,USER_SEQ,USER_CHAR_NO,PIC_SEQ" DataNavigateUrlFormatString="../CastAdmin/StockPicMainte.aspx?sitecd={0}&amp;userseq={1}&amp;usercharno={2}&amp;picseq={3}&amp;return=StockPicOpenList">
							<HeaderStyle Wrap="False" />
							<ItemStyle HorizontalAlign="Center" />
						</asp:HyperLinkField>
					</Columns>
					<PagerSettings Mode="NumericFirstLast" />
				</asp:GridView>
			</asp:Panel>
			&nbsp;
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br />
				<a class="reccount">Current viewing page
					<%=grdPic.PageIndex + 1%>
					of
					<%=grdPic.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsCastPic" runat="server" SelectMethod="GetApproveManagePageCollection" TypeName="CastPic" SelectCountMethod="GetApproveManagePageCount"
		EnablePaging="True" OnSelected="dsCastPic_Selected" OnSelecting="dsCastPic_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pNotApproveFlag" Type="String" />
			<asp:Parameter Name="pNotPublishFlag" Type="String" />
			<asp:Parameter Name="pPicType" Type="String" />
			<asp:Parameter Name="pUploadDateFrom" Type="String" />
			<asp:Parameter Name="pUploadDateTo" Type="String" />
			<asp:Parameter Name="pLoginId" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:MaskedEditExtender ID="mskUploadDayFrom" runat="server" TargetControlID="txtUploadDayFrom" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay"
		ClearMaskOnLostFocus="true">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:ValidatorCalloutExtender ID="vceUploadDayFrom" runat="Server" TargetControlID="vdeUploadDayFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:MaskedEditExtender ID="mskUploadDayTo" runat="server" TargetControlID="txtUploadDayTo" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay"
		ClearMaskOnLostFocus="true">
	</ajaxToolkit:MaskedEditExtender>
	<ajaxToolkit:ValidatorCalloutExtender ID="vceUploadDayTo" runat="Server" TargetControlID="vdeUploadDayTo" HighlightCssClass="validatorCallout" />
</asp:Content>
