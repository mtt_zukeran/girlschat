﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PickupObjViewer.aspx.cs" Inherits="CastAdmin_PickupObjViewer"%>
<%@ Register TagPrefix="ibrid" TagName="ListPicView" Src="../Controls/ListPicView.ascx"  %>
<%@ Register TagPrefix="ibrid" TagName="ListMovieView" Src="../Controls/ListMovieView.ascx"  %>
<%@ Register TagPrefix="ibrid" TagName="ListYakyukenPicView" Src="../Controls/ListYakyukenPicView.ascx"  %>
<%@ Register TagPrefix="ibrid" TagName="ListSocialGamePicView" Src="../Controls/ListSocialGamePicView.ascx"  %>
<%@ Register TagPrefix="ibrid" TagName="ListResembledPicView" Src="../Controls/ListResembledPicView.ascx"  %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Vicomm Site Management</title>
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Cache-Control" content="no-cache">
	<script type="text/javascript">
	//<![CDATA[
		function redirectParent(url){
			window.opener.location=url;
			return false;
		}
	//]]
	</script>
	<style type="text/css">
		.test
		{
			display:-moz-inline-box;
			display:inline-block;
			padding-right:100px;
		}
	</style>	
</head>
<body>
	<form id="form1" runat="server">
		<div id="container">
			<div id="header">
				<ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" 
					EnableScriptGlobalization="True" EnableScriptLocalization="True">
				</ajaxToolkit:ToolkitScriptManager>
			</div>
			<div id="content">
				<div id="contentright">
					<div id="contentborder" style="width:800px">
						[<a href="javascript:close()">閉じる</a>]
						<asp:Panel ID="pnlObjView" runat="server">
							<fieldset class="fieldset">
								<legend>[<asp:Label ID="lblObjInfoTitle" runat="server"></asp:Label>]</legend>
								<table border="0" cellpadding="0" cellspacing="0" style="width:99%;">
									<tr>
										<td  valign="top" class="tdDataStyle" width="200px">
											<asp:Image ID="imgObject" runat="server" width="200px" Visible="false"></asp:Image>
											<asp:Panel ID="pnlMovie" runat="server" Visible="false">
												<object classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" codebase="http://www.apple.com/qtactivex/qtplugin.cab"
　　												height="216" width="200">
　　												<param name="src" value='<%# MoviePath %>'/>
													<param name="autoplay" value="false"/>
　　　												<param name="controller" value="true"/>
													<param name="scale" value="tofit" />
　　												<embed
　　　												  height="216"
　　　												  width="200"
　　　												  src='<%# MoviePath %>'
　　　												  autoplay="false"
　　　												  controller="true"
　　　												  scale="tofit"
　　　												  pluginspage="http://www.apple.com/quicktime/download/"
　　　												  type="video/quicktime"></embed>
												</object>														
											</asp:Panel>
										</td>
										<td valign="top"class="tdDataStyle" >
											<table border="0" class="tableStyle" style="width:99%;font-size:smaller;" >
<%	if (this.PickupType.Equals(ViComm.ViCommConst.PicupTypes.YAKYUKEN_PIC)) { %>
												<tr>
													<td class="tdHeaderStyle" colspan="2">
														コメント
													</td>
												</tr>
												<tr>
													<td class="tdDataStyle" colspan="2">
														<asp:Label ID="lblCommentText" runat="server"></asp:Label>
													</td>
												</tr>
												<tr>
													<td class="tdHeaderStyle" colspan="2">
														種別
													</td>
												</tr>
												<tr>
													<td class="tdDataStyle" colspan="2">
														<asp:Label ID="lblYakyukenTypeNm" runat="server"></asp:Label>
													</td>
												</tr>
<%	} else if (this.PickupType.Equals(ViComm.ViCommConst.PicupTypes.SOCIAL_GAME_PIC)) { %>
												<tr>
													<td class="tdHeaderStyle" style="width:100px;">
														属性
													</td>
													<td class="tdDataStyle">
														<asp:Label ID="lblAttrTypeNm" runat="server"></asp:Label>
													</td>
												</tr>
<%	} else if (this.PickupType.Equals(ViComm.ViCommConst.PicupTypes.SOCIAL_GAME_MOVIE)) { %>
<%	} else { %>
<%		if(!IsCastCharacter()){ %>
												<tr>
													<td class="tdHeaderStyle" colspan="2">
														タイトル
													</td>
												</tr>
												<tr>
													<td class="tdDataStyle" colspan="2">
														<asp:Label ID="lblObjTitle" runat="server"></asp:Label>
													</td>
												</tr>
<%		} %>
<%	} %>
												<tr>
													<td class="tdHeaderStyle" style="width:100px;">
														ログインID
													</td>
													<td class="tdDataStyle">
														<asp:LinkButton ID="lnkLoginId" runat="server"></asp:LinkButton>
													</td>
												</tr>							
												<tr>
													<td class="tdHeaderStyle" style="width:100px;">
														氏名
													</td>
													<td class="tdDataStyle">
														<asp:Label ID="lblCastNm" runat="server"></asp:Label>
													</td>
												</tr>							
												<tr>
													<td class="tdHeaderStyle" style="width:100px;">
														ハンドル名

													</td>
													<td class="tdDataStyle">
														<%--<asp:Label ID="lblHandleNm" runat="server"></asp:Label>--%>
														<asp:LinkButton ID="lnkHandleNm" runat="server" OnClick="lnkHandleNm_Click"></asp:LinkButton>
													</td>
												</tr>
												<% if (IsWithPickup()) {%>
                                                <tr>
													<td class="tdHeaderStyle" style="width:100px;">
														Pickup
													</td>
													<td class="tdDataStyle">
														<asp:UpdatePanel ID="updIncluededPickup" runat="server">
														<Triggers>
															<asp:AsyncPostBackTrigger ControlID="btnAppendPickup" />
														</Triggers>
														<ContentTemplate>
															<% if(this.rptIncludedPickup.Items.Count == 0){ %>
																-- 未登録 --
															<% } %>
															<asp:Repeater ID="rptIncludedPickup" runat="server">
																<SeparatorTemplate>,</SeparatorTemplate>
																<ItemTemplate>
																	<asp:LinkButton ID="lnkChangePickup" runat="server" Text="<%# Container.DataItem %>" 
																		CommandArgument="<%# Container.DataItem %>" 
																		OnCommand="lnkChangePickup_Command">
																	</asp:LinkButton>																
																</ItemTemplate>
															</asp:Repeater>
														</ContentTemplate>
														</asp:UpdatePanel>
													</td>
												</tr>
												<%} %>												
											</table>
											<asp:Button runat="server" ID="btnSeekResembled" Text="類似検索" CssClass="seekbutton" OnClick="btnSeekResembled_Click" />
										</td>
									</tr>			
								</table>
							</fieldset>
						
							<div>
								<ajaxToolkit:TabContainer ID="tabPersonalObjs" runat="server" Height="330px" ScrollBars="Vertical" CssClass="ajax__tab_yuitabview-theme">
									<ajaxToolkit:TabPanel ID="tpProfPic" runat="server">
										<HeaderTemplate>
											ﾌﾟﾛﾌ画像
										</HeaderTemplate>
										<ContentTemplate>
											<ibrid:ListPicView ID="ctrlProfilePicList" runat="server" ColumnSize="6" OnObjClick="ctrlPicList_ObjClick">
											</ibrid:ListPicView>
										</ContentTemplate>
									</ajaxToolkit:TabPanel>
									<ajaxToolkit:TabPanel ID="tpProfMovie" runat="server">
										<HeaderTemplate>
											ﾌﾟﾛﾌ動画
										</HeaderTemplate>
										<ContentTemplate>
											<ibrid:ListMovieView ID="ctrlProfileMovieList" runat="server" ColumnSize="6" OnObjClick="ctrlMovieList_ObjClick">
											</ibrid:ListMovieView>
										</ContentTemplate>
									</ajaxToolkit:TabPanel>
									<ajaxToolkit:TabPanel ID="tpBbsPic" runat="server">
										<HeaderTemplate>
											掲示板画像
										</HeaderTemplate>
										<ContentTemplate>
											<ibrid:ListPicView ID="ctrlBbsPicList" runat="server" ColumnSize="6" OnObjClick="ctrlPicList_ObjClick">
											</ibrid:ListPicView>
										</ContentTemplate>
									</ajaxToolkit:TabPanel>
									<ajaxToolkit:TabPanel ID="tpBbsMovie" runat="server">
										<HeaderTemplate>
											掲示板動画
										</HeaderTemplate>
										<ContentTemplate>
											<ibrid:ListMovieView ID="ctrlBbsMovieList" runat="server" ColumnSize="6" OnObjClick="ctrlMovieList_ObjClick">
											</ibrid:ListMovieView>
										</ContentTemplate>
									</ajaxToolkit:TabPanel>
									<ajaxToolkit:TabPanel ID="tpYakyukenPic" runat="server">
										<HeaderTemplate>
											野球拳画像
										</HeaderTemplate>
										<ContentTemplate>
											<ibrid:ListYakyukenPicView ID="ctrlYakyukenPicList" runat="server" ColumnSize="6" OnObjClick="ctrlPicList_ObjClick">
											</ibrid:ListYakyukenPicView>
										</ContentTemplate>
									</ajaxToolkit:TabPanel>
									<ajaxToolkit:TabPanel ID="tpSocialGamePic" runat="server">
										<HeaderTemplate>
											ｹﾞｰﾑ画像
										</HeaderTemplate>
										<ContentTemplate>
											<ibrid:ListSocialGamePicView ID="ctrlSocialGamePicList" runat="server" ColumnSize="6" OnObjClick="ctrlPicList_ObjClick">
											</ibrid:ListSocialGamePicView>
										</ContentTemplate>
									</ajaxToolkit:TabPanel>
									<ajaxToolkit:TabPanel ID="tpSocialGameMovie" runat="server">
										<HeaderTemplate>
											ｹﾞｰﾑ動画
										</HeaderTemplate>
										<ContentTemplate>
											<ibrid:ListMovieView ID="ctrlSocialGameMovieList" runat="server" ColumnSize="6" OnObjClick="ctrlMovieList_ObjClick">
											</ibrid:ListMovieView>
										</ContentTemplate>
									</ajaxToolkit:TabPanel>
									<ajaxToolkit:TabPanel ID="tpMailStockPic" runat="server">
										<HeaderTemplate>
											ﾒｰﾙｽﾄｯｸ画像
										</HeaderTemplate>
										<ContentTemplate>
											<ibrid:ListPicView ID="ctrlMailStockPicList" runat="server" ColumnSize="6" OnObjClick="ctrlPicList_ObjClick">
											</ibrid:ListPicView>
										</ContentTemplate>
									</ajaxToolkit:TabPanel>
									<ajaxToolkit:TabPanel ID="tpMailStockMovie" runat="server">
										<HeaderTemplate>
											ﾒｰﾙｽﾄｯｸ動画
										</HeaderTemplate>
										<ContentTemplate>
											<ibrid:ListMovieView ID="ctrlMailStockMovieList" runat="server" ColumnSize="6" OnObjClick="ctrlMovieList_ObjClick">
											</ibrid:ListMovieView>
										</ContentTemplate>
									</ajaxToolkit:TabPanel>
									<ajaxToolkit:TabPanel ID="tpResembledPic" runat="server">
										<HeaderTemplate>
											類似検索
										</HeaderTemplate>
										<ContentTemplate>
											<ibrid:ListResembledPicView ID="ctrlResembledPicList" runat="server" ColumnSize="6" OnObjClick="ctrlResembledPicList_ObjClick">
											</ibrid:ListResembledPicView>
										</ContentTemplate>
									</ajaxToolkit:TabPanel>
								</ajaxToolkit:TabContainer>
							</div>
						</asp:Panel>
						
						<asp:UpdatePanel ID="updPickupObjList" runat="server" RenderMode="block">
							<Triggers>
								<asp:PostBackTrigger ControlID="grdPickupObjList"></asp:PostBackTrigger>
							</Triggers>
							<ContentTemplate>
								<asp:Panel ID="pnlPickupObjList" runat="server" Height="300px" ScrollBars="Horizontal">
									<fieldset>
										<legend>[ﾋﾟｯｸｱｯﾌﾟ一覧]</legend>	
										ﾋﾟｯｸｱｯﾌﾟ：

										<asp:DropDownList ID="lstPickup" runat="server" AppendDataBoundItems="true"
											CausesValidation="false" 
											DataSourceID="dsPickupList" DataTextField="PICKUP_TITLE" DataValueField="PICKUP_ID"
											Width="200px"
											AutoPostBack="true" OnSelectedIndexChanged="lstPickup_SelectedIndexChanged">
											<asp:ListItem Text=""></asp:ListItem>
										</asp:DropDownList>
										<asp:Button ID="btnAppendPickup" runat="server" Text="追加"
											ValidationGroup="AppendPickup"
											OnClick="btnAppendPickup_Click" />
                                        <asp:Button ID="btnAppendPickupWithPickup" runat="server" OnClick="btnAppendPickupWithPickup_Click" Text="公開追加"
                                            ValidationGroup="AppendPickup" />
                                        <asp:Button ID="btnRemovePickup" runat="server" Text="除外"
											OnClientClick="return window.confirm('ﾋﾟｯｸｱｯﾌﾟから除外します。よろしいですか？');"
											ValidationGroup="RemovePickup"										
											OnClick="btnRemovePickup_Click"  /><br />
										<asp:LinkButton ID="lnkOpenMainte" runat="server" Text="ﾋﾟｯｸｱｯﾌﾟ設定" >
										</asp:LinkButton>
										<div>
											<asp:TextBox ID="txtDummy01" runat="server" Visible="false"></asp:TextBox>
											<asp:CustomValidator ID="vdcObjSeq" runat="server"
												ControlToValidate="txtDummy01" 
												ErrorMessage="既にﾋﾟｯｸｱｯﾌﾟに追加されています。" 
												ValidationGroup="AppendPickup" 
												Display="dynamic"
												SetFocusOnError="false"
												ValidateEmptyText="true"
												OnServerValidate="vdcObjSeq_ServerValidate" ></asp:CustomValidator>
											<asp:RequiredFieldValidator ID="rfvPickup" runat="server" 
												ControlToValidate="lstPickup" 
												ErrorMessage="ﾋﾟｯｸｱｯﾌﾟが選択されていません。"
												EnableClientScript="false"
												Display="dynamic" 
												ValidationGroup="AppendPickup"  ></asp:RequiredFieldValidator>			
											<asp:CustomValidator ID="vdcHidePic" runat="server"
												ControlToValidate="txtDummy01" 
												ErrorMessage="ﾌﾟﾛﾌ画像ﾋﾟｯｸｱｯﾌﾟに非公開の画像は追加できません。" 
												ValidationGroup="AppendPickup" 
												Display="dynamic"
												SetFocusOnError="false"
												ValidateEmptyText="true"
												OnServerValidate="vdcHidePic_ServerValidate" ></asp:CustomValidator>
											<asp:CustomValidator ID="vdcObjSeq4Remove" runat="server"
												ControlToValidate="txtDummy01" 
												ErrorMessage="ﾋﾟｯｸｱｯﾌﾟに登録されていません。" 
												ValidationGroup="RemovePickup" 
												Display="dynamic"
												SetFocusOnError="false"
												ValidateEmptyText="true"
												OnServerValidate="vdcObjSeq4Remove_ServerValidate" ></asp:CustomValidator>
											<asp:RequiredFieldValidator ID="rfvPickup4Remove" runat="server" 
												ControlToValidate="lstPickup" 
												ErrorMessage="ﾋﾟｯｸｱｯﾌﾟが選択されていません。"
												EnableClientScript="false"
												Display="dynamic" 
												ValidationGroup="RemovePickup"  ></asp:RequiredFieldValidator>			
										</div>
										<asp:GridView ID="grdPickupObjList" runat="server" DataSourceID="dsPickupObjList"
											AutoGenerateColumns="false" AllowSorting="false" AllowPaging="true" PageSize="10" EnableViewState="false" SkinID="GridViewColor" OnRowCommand="grdPickupObjList_RowCommand" OnRowDataBound="grdPickupObjList_RowDataBound">
											<Columns>
												<asp:TemplateField HeaderText="公開中">
													<ItemTemplate>
														<asp:Literal ID="lblPickupFlag" runat="server" Text='<%# Eval("PICKUP_FLAG").ToString().Equals("1")?"○":""  %>'></asp:Literal>					
													</ItemTemplate>
													<ItemStyle HorizontalAlign="Center" />
												</asp:TemplateField>	
												<asp:TemplateField HeaderText="タイトル">
													<ItemTemplate>
														<%# FormatObjTitle(Eval("OBJ_TITLE") as string)%>					
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField HeaderText="氏名">
													<ItemTemplate>
														<asp:LinkButton ID="lnkDisplayObjInfo" runat="server" 
															CommandName="DisplayObjInfo" CommandArgument='<%# Eval("OBJ_SEQ").ToString()%>'
															Text='<%# FormatObjTitle(Eval("CAST_NM") as string)%>'>
														</asp:LinkButton>						
													</ItemTemplate>
												</asp:TemplateField>
												<asp:BoundField DataField="HANDLE_NM" HeaderText="ハンドル名" />
											</Columns>
											<PagerSettings Mode="NumericFirstLast" />
										</asp:GridView>
									</fieldset>
								</asp:Panel>						
							</ContentTemplate>	
						</asp:UpdatePanel>
						
					</div>
				</div>
			</div>
		</div>
		<% /**** ObjectDataSource ***/ %>
		<asp:ObjectDataSource ID="dsPickupList" runat="server" SelectMethod="GetListLite" TypeName="Pickup" OnSelecting="dsPickupList_Selecting">
			<SelectParameters>
				<asp:Parameter Name="pSiteCd" Type="String" />
				<asp:Parameter Name="pPickupType" Type="String" />
			</SelectParameters>
		</asp:ObjectDataSource>
		<asp:ObjectDataSource ID="dsPickupObjList" runat="server" 
			SelectCountMethod="GetPageCount" SelectMethod="GetPageCollection" TypeName="PickupObjs" OnSelecting="dsPickupObjList_Selecting"
			EnablePaging="true">
			<SelectParameters>
				<asp:Parameter Name="pSiteCd" Type="String" />
				<asp:Parameter Name="pPickupId" Type="String" />
				<asp:Parameter Name="pPickupType" Type="String" />
				<asp:Parameter Name="pLoginId" Type="String" />
				<asp:Parameter Name="pHandleNm" Type="String" />
                <asp:Parameter Name="pPickupFlag" Type="String" DefaultValue="" />
                <asp:Parameter Name="pSortType" Type="String" DefaultValue="" />
            </SelectParameters>
		</asp:ObjectDataSource>			
	</form>
</body>
</html>


