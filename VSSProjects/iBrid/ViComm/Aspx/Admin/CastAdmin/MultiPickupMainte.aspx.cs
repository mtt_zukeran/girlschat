﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ピックアップメンテナンス
--	Progaram ID		: MultiPickupMainte
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using iBridCommLib;
using ViComm;

public partial class Cast_MultiPickupMainte : System.Web.UI.Page {
	protected const int pageSize = 20;
	private const int availablePageCount = 10;

	private string SiteCd {
		get { return iBridUtil.GetStringValue(this.ViewState["SiteCd"]); }
		set { this.ViewState["SiteCd"] = value; }
	}
	private string PickupId {
		get { return iBridUtil.GetStringValue(this.ViewState["PickupId"]); }
		set { this.ViewState["PickupId"] = value; }
	}
	protected int DataCount {
		get { return int.Parse(iBridUtil.GetStringValue(this.ViewState["DataCount"])); }
		private set { this.ViewState["DataCount"] = value; }
	}
	protected int PageIndex {
		get { return int.Parse(iBridUtil.GetStringValue(this.ViewState["PageIndex"])); }
		private set { this.ViewState["PageIndex"] = value; }
	}
	protected int LastPageIndex {
		get { return int.Parse(iBridUtil.GetStringValue(this.ViewState["LastPageIndex"])); }
		private set { this.ViewState["LastPageIndex"] = value; }
	}
	private int CompanyMask {
		get { return int.Parse(Session["CompanyMask"].ToString()); }
	}
	#region Sorting
	private string SortExpression {
		get { return iBridUtil.GetStringValue(this.ViewState["SortExpression"]); }
		set { this.ViewState["SortExpression"] = value; }
	}

	private string SortDirect {
		get { return iBridUtil.GetStringValue(this.ViewState["SortDirect"]); }
		set { this.ViewState["SortDirect"] = value; }
	}
	#endregion

	protected void Page_Load(object sender, EventArgs e) {

		txtPickupHeaderDoc.EditorDirectory = string.Format("http://{0}:{1}/pinEdit/editor/", Request.UrlReferrer.Host, Request.UrlReferrer.Port);
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);

		if (!IsPostBack) {
			this.PageIndex = 1;
			this.LastPageIndex = 0;
			this.DataCount = 0;
			string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			string sPickupId = iBridUtil.GetStringValue(Request.QueryString["pickupid"]);
			this.SiteCd = sSiteCd;
			this.PickupId = sPickupId;
			string sAppendUserSeq = iBridUtil.GetStringValue(Request.QueryString["userseq"]);
			bool bAppend = iBridUtil.GetStringValue(Request.QueryString["append"]).Equals("1");

			pnlHeader.Visible = false;
			pnlKey.Visible = true;
			pnlGrid.Visible = true;
			pnlDtl.Visible = false;
			pnlRegister.Visible = false;

			lstSeekSiteCd.DataBind();
			lstSeekPickupId.DataBind();
			lstPickupType.DataBind();

			this.tdHeaderPickupFlag.Attributes["class"] = "tdHeaderStyle2";
			this.chkNormal.Checked = true;
			this.plcExt.Visible = false;
			this.chkAuthWait.Checked = false;
			this.chkStop.Checked = false;
			this.chkHold.Checked = false;
			this.chkResigned.Checked = false;
			this.chkBan.Checked = false;
			this.chkCastPublishFlag.Checked = false;

			if (!string.IsNullOrEmpty(sSiteCd) && !string.IsNullOrEmpty(sPickupId)) {
				this.grdPickup.DataSourceID = string.Empty;

				lstSeekSiteCd.SelectedValue = sSiteCd;
				lstSeekPickupId.SelectedValue = sPickupId;

				if (bAppend) {
					if (!sAppendUserSeq.Equals(string.Empty)) {
						List<string> oUserSeqList = new List<string>();
						List<string> oUserCharNoList = new List<string>();
						List<string> oCommentPickupList = new List<string>();
						List<string> oPickupFlagList = new List<string>();

						foreach (string sUserSeq in sAppendUserSeq.Split(',')) {
							oUserSeqList.Add(sUserSeq);
							oUserCharNoList.Add(ViCommConst.MAIN_CHAR_NO);
							oCommentPickupList.Add(string.Empty);
							oPickupFlagList.Add(ViCommConst.FLAG_OFF_STR);
						}
						using (DbSession db = new DbSession()) {
							db.PrepareProcedure("MULTI_PICKUP_CHAR_BULK_MAINTE");
							db.cmd.BindByName = true;
							db.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, sSiteCd);
							db.ProcedureInParm("pPICKUP_ID", DbSession.DbType.VARCHAR2, sPickupId);
							db.ProcedureInArrayParm("pDEL_USER_SEQ_LIST", DbSession.DbType.NUMBER, 0, new string[] { });
							db.ProcedureInArrayParm("pDEL_USER_CHAR_NO_LIST", DbSession.DbType.VARCHAR2, 0, new string[] { });
							db.ProcedureInArrayParm("pUSER_SEQ_LIST", DbSession.DbType.NUMBER, oUserSeqList.Count, oUserSeqList.ToArray());
							db.ProcedureInArrayParm("pUSER_CHAR_NO_LIST", DbSession.DbType.VARCHAR2, oUserCharNoList.Count, oUserCharNoList.ToArray());
							db.ProcedureInArrayParm("pCOMMENT_PICKUP_LIST", DbSession.DbType.VARCHAR2, oCommentPickupList.Count, oCommentPickupList.ToArray());
							db.ProcedureInArrayParm("pPICKUP_FLAG_LIST", DbSession.DbType.NUMBER, oPickupFlagList.Count, oPickupFlagList.ToArray());
							db.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);
							db.ExecuteProcedure();
						}
					}
				}

				GetData();
			} else if (!string.IsNullOrEmpty(sSiteCd)) {
				lstSeekSiteCd.SelectedValue = sSiteCd;
			}
		}
	}

	protected void btnSeek_Click(object sender, EventArgs e) {
		if (IsValid) {
			this.SiteCd = this.lstSiteCd.SelectedValue;
			this.PickupId = string.Empty;

			GetData();
		}
	}

	protected void btnRegister_Click(object sender, EventArgs e) {
		this.lstSiteCd.SelectedValue = this.lstSeekSiteCd.SelectedValue;
		this.SiteCd = this.lstSeekSiteCd.SelectedValue;
		this.PickupId = string.Empty;

		this.pnlRegister.Visible = true;
		this.pnlHeader.Visible = true;
		this.pnlKey.Visible = false;
	}

	protected void btnListSeek_Click(object sender, EventArgs e) {
		this.SiteCd = this.lstSeekSiteCd.SelectedValue;
		this.PickupId = this.lstSeekPickupId.SelectedValue;
		this.PageIndex = 1;
		this.grdPickup.DataSourceID = string.Empty;

		GetData();
	}

	protected void btnAllClear_Click(object sender, EventArgs e) {
		if (this.lstPickupType.SelectedValue.Equals(ViCommConst.PicupTypes.CAST_CHARACTER)) {
			this.UpdatePickupCharacter(ViCommConst.FLAG_ON);
		} else {
			this.UpdatePickupObject(ViCommConst.FLAG_ON);
		}

		GetData();
	}

	protected void btnCancel_Click(object sender, EventArgs e) {
		pnlHeader.Visible = false;
		pnlKey.Visible = true;
		pnlGrid.Visible = true;
		pnlDtl.Visible = false;
		pnlRegister.Visible = false;
		this.lstSeekPickupId.SelectedIndex = 0;
		this.txtLoginId.Text = string.Empty;
		this.txtLoginIdList.Text = string.Empty;
		this.txtHandelNm.Text = string.Empty;
		this.grdPickup.DataSourceID = "dsPickup";
	}

	protected void btnDelete_Click(object sender, EventArgs e) {
		if (IsValid) {
			UpdateData(ViCommConst.FLAG_ON);

			this.lstSeekPickupId.DataBind();
			this.lstSeekPickupId.SelectedIndex = 0;
			this.txtLoginId.Text = string.Empty;
			this.txtLoginIdList.Text = string.Empty;
			this.txtHandelNm.Text = string.Empty;

			pnlHeader.Visible = false;
			pnlKey.Visible = true;
			grdPickup.DataSourceID = "dsPickup";
			grdPickup.DataBind();
			pnlGrid.Visible = true;
			pnlDtl.Visible = false;
			pnlRegister.Visible = false;
		}
	}

	protected void btnClear_Click(object sender, EventArgs e) {
		this.lstSeekSiteCd.SelectedIndex = 0;
		this.lstSeekPickupId.SelectedIndex = 0;
		this.txtLoginId.Text = string.Empty;
		this.txtLoginIdList.Text = string.Empty;
		this.txtHandelNm.Text = string.Empty;
		this.rdoPickupFlag.SelectedIndex = 0;
		this.chkNormal.Checked = true;
		this.chkAuthWait.Checked = false;
		this.chkStop.Checked = false;
		this.chkHold.Checked = false;
		this.chkResigned.Checked = false;
		this.chkBan.Checked = false;
		this.chkCastPublishFlag.Checked = false;
	}

	protected void btnSubmitUpdate_Click(object sender, EventArgs e) {
		if (IsValid && this.IsCorrect()) {
			UpdateData(ViCommConst.FLAG_OFF);

			if (this.lstPickupType.SelectedValue.Equals(ViCommConst.PicupTypes.CAST_CHARACTER)) {
				this.UpdatePickupCharacter(ViCommConst.FLAG_OFF);
			} else {
				this.UpdatePickupObject(ViCommConst.FLAG_OFF);
			}

			this.lstSeekPickupId.DataBind();
			this.lstSeekPickupId.SelectedValue = this.PickupId;
			this.txtLoginId.Text = string.Empty;
			this.txtLoginIdList.Text = string.Empty;
			this.txtHandelNm.Text = string.Empty;

			pnlHeader.Visible = false;
			pnlKey.Visible = true;
			grdPickup.DataSourceID = "dsPickup";
			grdPickup.DataBind();
			pnlGrid.Visible = true;
			pnlDtl.Visible = false;
			pnlRegister.Visible = false;
		}
	}

	protected void lnkCondition_Click(object sender, EventArgs e) {
		this.SetupCondition(this.pnlKey.Visible);
	}

	private void SetupCondition(bool pIsPanelVisible) {
		if (pIsPanelVisible) {
			this.pnlKey.Visible = false;
			this.lnkCondition.Text = "[検索条件表示]";
		} else {
			this.pnlKey.Visible = true;
			this.lnkCondition.Text = "[条件非表示]";
		}
	}

	protected void vdcUseTopPageFlag_ServerValidate(object source, ServerValidateEventArgs e) {
		if (chkUseTopPageFlag.Checked) {
			using (Pickup oPickup = new Pickup()) {
				e.IsValid = !oPickup.IsExistOtherUseTopPageFlag(lstSeekSiteCd.SelectedValue, lstSeekPickupId.SelectedValue);
			}
		} else {
			e.IsValid = true;
		}
	}

	private void GetData() {
		this.DataCount = 0;
		this.LastPageIndex = 0;

		string sPickupType = ViCommConst.PicupTypes.UNCLASSIFIED;

		// PICKUP情報取得
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MULTI_PICKUP_TITLE_GET");
			db.ProcedureInParm("PSITE_CD", DbSession.DbType.VARCHAR2, this.SiteCd);
			db.ProcedureInParm("PPICKUP_ID", DbSession.DbType.VARCHAR2, this.PickupId);
			db.ProcedureOutParm("PPICKUP_TITLE", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPICKUP_HEADER_DOC", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PUSE_TOP_PAGE_FLAG", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PPICKUP_TYPE", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PPICKUP_FLAG", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pAPPLY_PROFILE_PIC_FLAG", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pCAST_PUBLISH_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("pPUBLISH_START_DATE", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("pPUBLISH_END_DATE", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHAS_LIST", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID", DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT", DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0) {
				sPickupType = db.GetStringValue("PPICKUP_TYPE");
				txtPickupTitle.Text = db.GetStringValue("PPICKUP_TITLE");
				txtPickupHeaderDoc.Text = db.GetStringValue("PPICKUP_HEADER_DOC");
				chkUseTopPageFlag.Checked = db.GetStringValue("PUSE_TOP_PAGE_FLAG").Equals(ViCommConst.FLAG_ON_STR);
				chkPickupFlag.Checked = ViCommConst.FLAG_ON_STR.Equals(db.GetStringValue("pPICKUP_FLAG"));
				int iHasList = db.GetIntValue("PHAS_LIST");
				this.lstPickupType.Enabled = !(iHasList > 0);
				this.lblPickupTypeMsg.Visible = (iHasList > 0);
				this.lstPickupType.SelectedValue = sPickupType;
				this.chkApplayProfilePicFlag.Checked = ViCommConst.FLAG_ON_STR.Equals(db.GetStringValue("pAPPLY_PROFILE_PIC_FLAG"));
				this.txtPublishStartDate.Text = db.GetStringValue("pPUBLISH_START_DATE");
				this.txtPublishEndDate.Text = db.GetStringValue("pPUBLISH_END_DATE");
				chkCastPublishFlag.Checked = ViCommConst.FLAG_ON_STR.Equals(db.GetStringValue("pCAST_PUBLISH_FLAG"));

				string sLoginId = this.chkLoginIdListVisible.Checked ? this.txtLoginIdList.Text.Trim() : this.txtLoginId.Text.Trim();
				int iStatus = 0;
				if (chkAuthWait.Checked) {
					iStatus += ViCommConst.MASK_WOMAN_AUTH_WAIT;
				}
				if (chkNormal.Checked) {
					iStatus += ViCommConst.MASK_WOMAN_NORMAL;
				}
				if (chkStop.Checked) {
					iStatus += ViCommConst.MASK_WOMAN_STOP;
				}
				if (chkHold.Checked) {
					iStatus += ViCommConst.MASK_WOMAN_HOLD;
				}
				if (chkResigned.Checked) {
					iStatus += ViCommConst.MASK_WOMAN_RESIGNED;
				}
				if (chkBan.Checked) {
					iStatus += ViCommConst.MASK_WOMAN_BAN;
				}
				string sNaFlag = ViCommConst.FLAG_OFF_STR;

				// ==========================
				//  ピックアップに登録されているデータの一覧を表示
				// ==========================
				if (sPickupType.Equals(ViCommConst.PicupTypes.UNCLASSIFIED)) {
					// なにもしない
					this.pnlCharacter.Visible = false;
					this.pnlObject.Visible = false;

				} else if (sPickupType.Equals(ViCommConst.PicupTypes.CAST_CHARACTER)) {
					//
					// ｷｬｽﾄｷｬﾗｸﾀｰ
					//
					using (PickupCharacter oPickupCharacter = new PickupCharacter()) {
						this.DataCount = oPickupCharacter.GetPageCount(this.SiteCd,this.PickupId,sLoginId,this.txtHandelNm.Text.TrimEnd(),this.rdoPickupFlag.SelectedValue,iStatus,sNaFlag);
						this.lstCharacter.DataSource = oPickupCharacter.GetPageCollection(this.SiteCd,this.PickupId,sLoginId,this.txtHandelNm.Text.TrimEnd(),this.rdoPickupFlag.SelectedValue,iStatus,sNaFlag,this.rdoSort.SelectedValue,pageSize * (this.PageIndex - 1),pageSize);
						this.lstCharacter.DataBind();
					}
					this.pnlCharacter.Visible = true;
					this.pnlObject.Visible = false;
				} else {
					//
					// ｵﾌﾞｼﾞｪｸﾄ(写真/動画)
					//
					using (PickupObjs oPickupObjs = new PickupObjs()) {
						this.DataCount = oPickupObjs.GetPageCount(this.SiteCd,this.PickupId,sPickupType,sLoginId,this.txtHandelNm.Text.TrimEnd(),this.rdoPickupFlag.SelectedValue,iStatus,sNaFlag,string.Empty);
						this.lstObject.DataSource = oPickupObjs.GetPageCollection(this.SiteCd,this.PickupId,sPickupType,sLoginId,this.txtHandelNm.Text.TrimEnd(),this.rdoPickupFlag.SelectedValue,iStatus,sNaFlag,this.rdoSort.SelectedValue,pageSize * (this.PageIndex - 1),pageSize);
						this.lstObject.DataBind();
					}
					this.pnlCharacter.Visible = false;
					this.pnlObject.Visible = true;
				}
			} else {
				this.txtPickupTitle.Text = this.txtNewPickupTitle.Text;
				this.lstPickupType.SelectedValue = this.lstPickup.SelectedValue;
				this.lstPickupType.Enabled = true;
				this.chkPickupFlag.Checked = false;
				this.chkUseTopPageFlag.Checked = false;
				this.chkCastPublishFlag.Checked = false;
				this.lstCharacter.DataSource = string.Empty;
				this.lstCharacter.DataBind();
				this.lstObject.DataSource = string.Empty;
				this.lstObject.DataBind();
				this.lblPickupTypeMsg.Visible = false;

				this.pnlCount.DataBind();
			}
		}

		pnlDtl.Visible = true;
		pnlHeader.Visible = false;
		pnlGrid.Visible = false;
		lnkPickupObjsMainte.DataBind();
		this.SetupCondition(true);
	}

	#region □■□ DB更新系処理 □■□ ================================================================================
	private void UpdateData(int pDelFlag) {
		string[] sDoc;
		int iDocCount;

		SysPrograms.SeparateHtml(HttpUtility.HtmlDecode(txtPickupHeaderDoc.Text), 1, out sDoc, out iDocCount);
		sDoc[0] = sDoc[0] ?? string.Empty;

		bool bUseTopPageFlag = false;
		if (this.lstPickupType.SelectedValue.Equals(ViCommConst.PicupTypes.CAST_CHARACTER)) {
			bUseTopPageFlag = chkUseTopPageFlag.Checked;
		}
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("MULTI_PICKUP_TITLE_MAINTE");
			oDbSession.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, lstSiteCd.SelectedValue);
			oDbSession.ProcedureBothParm("pPICKUP_ID", DbSession.DbType.VARCHAR2, this.PickupId);
			oDbSession.ProcedureInParm("pPICKUP_TITLE", DbSession.DbType.VARCHAR2, txtPickupTitle.Text);
			oDbSession.ProcedureInParm("pPICKUP_HEADER_DOC", DbSession.DbType.VARCHAR2, sDoc[0]);
			oDbSession.ProcedureInParm("pUSE_TOP_PAGE_FLAG", DbSession.DbType.NUMBER, bUseTopPageFlag);
			oDbSession.ProcedureInParm("pPICKUP_TYPE", DbSession.DbType.VARCHAR2, this.lstPickupType.SelectedValue);
			oDbSession.ProcedureInParm("pPICKUP_FLAG", DbSession.DbType.VARCHAR2, this.chkPickupFlag.Checked ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF);
			oDbSession.ProcedureInParm("pAPPLY_PROFILE_PIC_FLAG", DbSession.DbType.NUMBER, this.chkApplayProfilePicFlag.Checked ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF);
			oDbSession.ProcedureInParm("pCAST_PUBLISH_FLAG",DbSession.DbType.NUMBER,this.chkCastPublishFlag.Checked ? ViCommConst.FLAG_ON : ViCommConst.FLAG_OFF);
			oDbSession.ProcedureInParm("pPUBLISH_START_DATE", DbSession.DbType.VARCHAR2, this.txtPublishStartDate.Text);
			oDbSession.ProcedureInParm("pPUBLISH_END_DATE", DbSession.DbType.VARCHAR2, this.txtPublishEndDate.Text);
			oDbSession.ProcedureInParm("pROWID", DbSession.DbType.VARCHAR2, iBridUtil.GetStringValue(ViewState["ROWID"]));
			oDbSession.ProcedureInParm("pREVISION_NO", DbSession.DbType.NUMBER, iBridUtil.GetStringValue(ViewState["REVISION_NO"]));
			oDbSession.ProcedureInParm("pDEL_FLAG", DbSession.DbType.NUMBER, pDelFlag);
			oDbSession.ProcedureOutParm("pSTATUS", DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();

			this.PickupId = oDbSession.GetStringValue("pPICKUP_ID");
		}
	}

	private void UpdatePickupCharacter(int pAllClear) {

		string sSiteCd = this.lstSeekSiteCd.SelectedValue;
		string sPickupId = this.lstSeekPickupId.SelectedValue;

		List<string> oDelUserSeqList = new List<string>();
		List<string> oDelUserCharNoList = new List<string>();
		List<string> oUserSeqList = new List<string>();
		List<string> oUserCharNoList = new List<string>();
		List<string> oCommentPickupList = new List<string>();
		List<string> oPriorityList = new List<string>();
		List<string> oPickupFlagList = new List<string>();
		List<string> oStartPubDayList = new List<string>();
		List<string> oEndPubDayList = new List<string>();

		if (ViCommConst.FLAG_ON == pAllClear) {
			using (PickupCharacter oPickupCharacter = new PickupCharacter()) {
				string sLoginId = this.chkLoginIdListVisible.Checked ? this.txtLoginIdList.Text.Trim() : this.txtLoginId.Text.Trim();
				DataSet oDataSet = oPickupCharacter.GetPageCollection(this.SiteCd,this.PickupId,sLoginId,this.txtHandelNm.Text.TrimEnd(),this.rdoPickupFlag.SelectedValue,false,false,this.rdoSort.SelectedValue,pageSize * (this.PageIndex - 1),pageSize);
				if (oDataSet.Tables[0].Rows.Count == 0) return;
				foreach (DataRow oDataRow in oDataSet.Tables[0].Rows) {
					oDelUserSeqList.Add(oDataRow["USER_SEQ"].ToString());
					oDelUserCharNoList.Add(ViCommConst.MAIN_CHAR_NO);
				}
			}
		} else {
			if (this.lstCharacter.Items.Count == 0) return;
			foreach (DataListItem oListItem in this.lstCharacter.Items) {
				HiddenField hdnUserSeq = (HiddenField)oListItem.FindControl("hdnUserSeq");
				HiddenField hdnUserCharNo = (HiddenField)oListItem.FindControl("hdnUserCharNo");
				TextBox txtComment = (TextBox)oListItem.FindControl("txtComment");
				CheckBox chkPickup = (CheckBox)oListItem.FindControl("chkPickup");
				CheckBox chkDeleteFlag = (CheckBox)oListItem.FindControl("chkDeleteFlag");
				TextBox txtPriority = (TextBox)oListItem.FindControl("txtPriority");
				TextBox txtStartPubDay = (TextBox)oListItem.FindControl("txtStartPubDay");
				TextBox txtEndPubDay = (TextBox)oListItem.FindControl("txtEndPubDay");

				string sUserSeq = hdnUserSeq.Value;
				string sUserCharNo = hdnUserCharNo.Value;
				string sCommentPickup = txtComment.Text;
				string sPriority = txtPriority.Text;
				string sPickupFlag = chkPickup.Checked ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;

				if (ViCommConst.FLAG_ON == pAllClear || chkDeleteFlag.Checked) {
					oDelUserSeqList.Add(sUserSeq);
					oDelUserCharNoList.Add(ViCommConst.MAIN_CHAR_NO);
				} else {
					oUserSeqList.Add(sUserSeq);
					oUserCharNoList.Add(ViCommConst.MAIN_CHAR_NO);
					oCommentPickupList.Add(sCommentPickup);
					oStartPubDayList.Add(txtStartPubDay.Text);
					oEndPubDayList.Add(txtEndPubDay.Text);
					oPriorityList.Add(sPriority);
					oPickupFlagList.Add(sPickupFlag);
				}
			}
		}
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MULTI_PICKUP_CHAR_BULK_MAINTE");
			db.cmd.BindByName = true;
			db.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, sSiteCd);
			db.ProcedureInParm("pPICKUP_ID", DbSession.DbType.VARCHAR2, sPickupId);
			db.ProcedureInArrayParm("pDEL_USER_SEQ_LIST", DbSession.DbType.NUMBER, oDelUserSeqList.Count, oDelUserSeqList.ToArray());
			db.ProcedureInArrayParm("pDEL_USER_CHAR_NO_LIST", DbSession.DbType.VARCHAR2, oDelUserCharNoList.Count, oDelUserCharNoList.ToArray());
			db.ProcedureInArrayParm("pUSER_SEQ_LIST", DbSession.DbType.NUMBER, oUserSeqList.Count, oUserSeqList.ToArray());
			db.ProcedureInArrayParm("pUSER_CHAR_NO_LIST", DbSession.DbType.VARCHAR2, oUserCharNoList.Count, oUserCharNoList.ToArray());
			db.ProcedureInArrayParm("pCOMMENT_PICKUP_LIST", DbSession.DbType.VARCHAR2, oCommentPickupList.Count, oCommentPickupList.ToArray());
			db.ProcedureInArrayParm("pPICKUP_START_PUB_DAY_LIST", DbSession.DbType.VARCHAR2, oStartPubDayList.Count, oStartPubDayList.ToArray());
			db.ProcedureInArrayParm("pPICKUP_END_PUB_DAY_LIST", DbSession.DbType.VARCHAR2, oEndPubDayList.Count, oEndPubDayList.ToArray());
			db.ProcedureInArrayParm("pPRIORITY_LIST", DbSession.DbType.NUMBER, oPriorityList.Count, oPriorityList.ToArray());
			db.ProcedureInArrayParm("pPICKUP_FLAG_LIST", DbSession.DbType.NUMBER, oPickupFlagList.Count, oPickupFlagList.ToArray());
			db.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	private void UpdatePickupObject(int pAllClear) {

		string sSiteCd = this.lstSeekSiteCd.SelectedValue;
		string sPickupId = this.lstSeekPickupId.SelectedValue;
		List<string> oDelObjSeqList = new List<string>();
		List<string> oObjSeqList = new List<string>();
		List<string> oCommentPickupList = new List<string>();
		List<string> oPriorityList = new List<string>();
		List<string> oPickupFlagList = new List<string>();
		List<string> oStartPubDayList = new List<string>();
		List<string> oEndPubDayList = new List<string>();

		if (ViCommConst.FLAG_ON == pAllClear) {
			if (this.lstPickupType.SelectedValue == ViCommConst.PicupTypes.UNCLASSIFIED) return;
			using (PickupObjs oPickupObjs = new PickupObjs()) {
				string sLoginId = this.chkLoginIdListVisible.Checked ? this.txtLoginIdList.Text.Trim() : this.txtLoginId.Text.Trim();
				DataSet oDataSet = oPickupObjs.GetPageCollection(this.SiteCd,this.PickupId,this.lstPickupType.SelectedValue,sLoginId,this.txtHandelNm.Text.TrimEnd(),this.rdoPickupFlag.SelectedValue,false,false,this.rdoSort.SelectedValue,pageSize * (this.PageIndex - 1),pageSize);
				
				if (oDataSet.Tables[0].Rows.Count == 0) return;
				foreach (DataRow oDataRow in oDataSet.Tables[0].Rows) {
					oDelObjSeqList.Add(oDataRow["OBJ_SEQ"].ToString());
				}
			}
		} else {
			if (this.lstObject.Items.Count == 0) return;
			foreach (DataListItem oListItem in this.lstObject.Items) {
				HiddenField hdnObjSeq = (HiddenField)oListItem.FindControl("hdnObjSeq");
				TextBox txtObjComment = (TextBox)oListItem.FindControl("txtObjComment");
				CheckBox chkObjPickupFlag = (CheckBox)oListItem.FindControl("chkObjPickupFlag");
				CheckBox chkObjDeleteFlag = (CheckBox)oListItem.FindControl("chkObjDeleteFlag");
				TextBox txtPriority = (TextBox)oListItem.FindControl("txtPriority");
				TextBox txtStartPubDay = (TextBox)oListItem.FindControl("txtStartPubDay");
				TextBox txtEndPubDay = (TextBox)oListItem.FindControl("txtEndPubDay");

				string sPriority = txtPriority.Text;
				string sObjSeq = hdnObjSeq.Value;
				string sCommentPickup = txtObjComment.Text;
				string sPickupFlag = chkObjPickupFlag.Checked ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;

				if (ViCommConst.FLAG_ON == pAllClear || chkObjDeleteFlag.Checked) {
					oDelObjSeqList.Add(sObjSeq);
				} else {
					oObjSeqList.Add(sObjSeq);
					oCommentPickupList.Add(sCommentPickup);
					oStartPubDayList.Add(txtStartPubDay.Text);
					oEndPubDayList.Add(txtEndPubDay.Text);
					oPickupFlagList.Add(sPickupFlag);
					oPriorityList.Add(sPriority);
				}
			}
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MULTI_PICKUP_OBJS_BULK_MAINTE");
			db.cmd.BindByName = true;
			db.ProcedureInParm("pSITE_CD", DbSession.DbType.VARCHAR2, sSiteCd);
			db.ProcedureInParm("pPICKUP_ID", DbSession.DbType.VARCHAR2, sPickupId);
			db.ProcedureInArrayParm("pDEL_OBJ_SEQ_LIST", DbSession.DbType.NUMBER, oDelObjSeqList.Count, oDelObjSeqList.ToArray());
			db.ProcedureInArrayParm("pOBJ_SEQ_LIST", DbSession.DbType.NUMBER, oObjSeqList.Count, oObjSeqList.ToArray());
			db.ProcedureInArrayParm("pCOMMENT_PICKUP_LIST", DbSession.DbType.VARCHAR2, oCommentPickupList.Count, oCommentPickupList.ToArray());
			db.ProcedureInArrayParm("pPICKUP_START_PUB_DAY_LIST", DbSession.DbType.VARCHAR2, oStartPubDayList.Count, oStartPubDayList.ToArray());
			db.ProcedureInArrayParm("pPICKUP_END_PUB_DAY_LIST", DbSession.DbType.VARCHAR2, oEndPubDayList.Count, oEndPubDayList.ToArray());
			db.ProcedureInArrayParm("pPRIORITY_LIST", DbSession.DbType.NUMBER, oPriorityList.Count, oPriorityList.ToArray());
			db.ProcedureInArrayParm("pPICKUP_FLAG_LIST", DbSession.DbType.NUMBER, oPriorityList.Count, oPickupFlagList.ToArray());
			db.ProcedureOutParm("PSTATUS", DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
	}

	protected string GetRecCount() {
		return lstCharacter.Items.Count.ToString();
	}

	protected void lstSeekSiteCd_SelectedIndexChanged(object sender, EventArgs e) {
		lstSeekPickupId.DataBind();
		grdPickup.DataBind();
	}
	#endregion ========================================================================================================

	protected string GetFlagMark(object pValue) {
		if (pValue.ToString().Equals(ViCommConst.FLAG_ON_STR)) {
			return "○";
		}
		return string.Empty;
	}

	protected string GetClosedMark(object pNotApproveFlag, object pNotPublishFlag, object pObjType) {
		if (ViCommConst.PicupTypes.PROFILE_PIC.Equals(this.lstPickupType.SelectedValue) &&
			iBridUtil.GetStringValue(ViCommConst.ATTACHED_HIDE).Equals(iBridUtil.GetStringValue(pObjType))) {
			return "非公開";
		}

		return ViCommConst.FLAG_OFF_STR.Equals(iBridUtil.GetStringValue(pNotApproveFlag)) &&
			ViCommConst.FLAG_OFF_STR.Equals(iBridUtil.GetStringValue(pNotPublishFlag)) ? "公開中" : "非公開";
	}

	protected bool IsMovie(string pPikupType) {
		bool bIsMovie = false;

		bIsMovie = bIsMovie || pPikupType.Equals(ViCommConst.PicupTypes.PROFILE_MOVIE);
		bIsMovie = bIsMovie || pPikupType.Equals(ViCommConst.PicupTypes.BBS_MOVIE);

		return bIsMovie;
	}
	protected bool IsPic(string pPikupType) {
		bool bIsPic = false;

		bIsPic = bIsPic || pPikupType.Equals(ViCommConst.PicupTypes.PROFILE_PIC);
		bIsPic = bIsPic || pPikupType.Equals(ViCommConst.PicupTypes.BBS_PIC);

		return bIsPic;
	}

	protected bool IsCharacter(object pPikupType) {
		return pPikupType.ToString().Equals(ViCommConst.PicupTypes.CAST_CHARACTER);
	}

	protected string GetPickupCount(object pPickupType, object pCharacterCount, object pPicCount, object pMovieCount) {

		if (IsCharacter(pPickupType)) {
			return pCharacterCount.ToString();
		} else if (IsPic(pPickupType.ToString())) {
			return pPicCount.ToString();
		} else {
			return pMovieCount.ToString();
		}

	}

	protected void lstDataList_ItemDataBound(object sender, DataListItemEventArgs e) {
		if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem) {
			DataRow oDataRow = (e.Item.DataItem as DataRowView).Row as DataRow;
			if (oDataRow == null) {
				return;
			}

			if (!ViCommConst.USER_WOMAN_NORMAL.Equals(iBridUtil.GetStringValue(oDataRow["USER_STATUS"])) || 
				ViCommConst.FLAG_ON_STR.Equals(iBridUtil.GetStringValue(oDataRow["NA_FLAG"]))) {
				e.Item.BackColor = Color.DarkGray;
				return;
			}

			if (oDataRow.Table.Columns.Contains("PIC_TYPE") &&
				ViCommConst.PicupTypes.PROFILE_PIC.Equals(this.lstPickupType.SelectedValue) &&
				iBridUtil.GetStringValue(ViCommConst.ATTACHED_HIDE).Equals(iBridUtil.GetStringValue(oDataRow["PIC_TYPE"]))) {
				e.Item.BackColor = Color.DarkGray;
				return;
			}

			e.Item.BackColor =
				ViCommConst.FLAG_OFF_STR.Equals(iBridUtil.GetStringValue(oDataRow["OBJ_NOT_APPROVE_FLAG"])) &&
				ViCommConst.FLAG_OFF_STR.Equals(iBridUtil.GetStringValue(oDataRow["OBJ_NOT_PUBLISH_FLAG"])) ? Color.Empty : Color.DarkGray;

		} else if (e.Item.ItemType == ListItemType.Footer) {
			if (this.DataCount <= pageSize) {
				this.LastPageIndex = this.DataCount > 0 ? 1 : 0;
				(e.Item.FindControl("pnlPaging") as Panel).Visible = false;
			} else {
				int iLastPageIndex = (int)System.Math.Ceiling((double)this.DataCount / pageSize);
				this.LastPageIndex = iLastPageIndex;
				
				int iStartLinkNumber;
				if (iLastPageIndex - this.PageIndex < availablePageCount) {
					iStartLinkNumber = iLastPageIndex - availablePageCount + 1 < 0 || (this.PageIndex - 1) < availablePageCount ? 1 : iLastPageIndex - availablePageCount + 1;
				} else {
					iStartLinkNumber = ((this.PageIndex - 1) / availablePageCount * availablePageCount) + 1;
				}

				int iPageLinkNumber = iStartLinkNumber;
				for (int i = 0; i < availablePageCount; i++) {
					LinkButton oPageNumberLinkButton = e.Item.FindControl(string.Concat("lnkPage", i + 1)) as LinkButton;
					Label oPageNumberLabel = e.Item.FindControl(string.Concat("lblPage", i + 1)) as Label;

					if (iPageLinkNumber > iLastPageIndex) {
						oPageNumberLinkButton.Visible = false;
						oPageNumberLabel.Visible = false;
					} else {
						if (this.PageIndex == iPageLinkNumber) {
							oPageNumberLabel.Text = iPageLinkNumber.ToString();
							oPageNumberLabel.Visible = true;
							oPageNumberLinkButton.Visible = false;
						} else {
							oPageNumberLabel.Visible = false;
							oPageNumberLinkButton.Text = iPageLinkNumber.ToString();
							oPageNumberLinkButton.CommandArgument = iPageLinkNumber.ToString();
						}
					}
					iPageLinkNumber++;
				}

				LinkButton oPageFirstLinkButton = e.Item.FindControl("lnkPageFirst") as LinkButton;
				LinkButton oPagePreviousLinkButton = e.Item.FindControl("lnkPagePrevious") as LinkButton;
				LinkButton oPageNextLinkButton = e.Item.FindControl("lnkPageNext") as LinkButton;
				LinkButton oPageLastLinkButton = e.Item.FindControl("lnkPageLast") as LinkButton;

				if (this.DataCount > pageSize) {
					if (availablePageCount < this.PageIndex) {
						oPageFirstLinkButton.Visible = true;
						oPagePreviousLinkButton.Visible = true;
						oPagePreviousLinkButton.CommandArgument = (iStartLinkNumber - 1).ToString();
					} else {
						oPageFirstLinkButton.Visible = false;
						oPagePreviousLinkButton.Visible = false;
					}

					if (iLastPageIndex - iStartLinkNumber < availablePageCount) {
						oPageNextLinkButton.Visible = false;
						oPageLastLinkButton.Visible = false;
					} else {
						oPageNextLinkButton.Visible = true;
						oPageLastLinkButton.Visible = true;
						oPageNextLinkButton.CommandArgument = (iPageLinkNumber).ToString();
						oPageLastLinkButton.CommandArgument = iLastPageIndex.ToString();
					}
				}
			}
			this.pnlCount.DataBind();
		}
	}

	private bool IsCorrect() {
		DataList oDataList;
		switch (this.lstPickupType.SelectedValue) {
			case ViCommConst.PicupTypes.UNCLASSIFIED:
				return true;
			case ViCommConst.PicupTypes.CAST_CHARACTER:
				oDataList = this.lstCharacter;
				break;
			default:
				oDataList = this.lstObject;
				break;
		}

		bool bIsCorrect = true;

		foreach (DataListItem oListItem in oDataList.Items) {
			StringBuilder oErrorBuilder = new StringBuilder();
			Panel pnlError = oListItem.FindControl("pnlError") as Panel;
			Label lblErrorMessage = oListItem.FindControl("lblErrorMessage") as Label;

			string sPriority = (oListItem.FindControl("txtPriority") as TextBox).Text;
			int iPriority;

			if (int.TryParse(sPriority, out iPriority)) {
				if (!(0 <= iPriority && iPriority < 1000)) {
					oErrorBuilder.Append("優先度は3桁以下の数字で入力して下さい。");
				}
			} else {
				oErrorBuilder.Append("優先度は3桁以下の数字で入力して下さい。");
			}

			string sStartDate = (oListItem.FindControl("txtStartPubDay") as TextBox).Text;
			string sEndDate = (oListItem.FindControl("txtEndPubDay") as TextBox).Text;
			DateTime oStartDate;
			DateTime oEndDate;
			bool bIsStartDateValid = DateTime.TryParseExact(sStartDate, "yyyy/MM/dd", null, System.Globalization.DateTimeStyles.None, out oStartDate);
			bool bIsEndDateValid = DateTime.TryParseExact(sEndDate, "yyyy/MM/dd", null, System.Globalization.DateTimeStyles.None, out oEndDate);

			if (!string.IsNullOrEmpty(sStartDate) && !bIsStartDateValid) {
				oErrorBuilder.AppendFormat("{0}公開開始日は yyyy/mm/dd 形式で入力して下さい。", oErrorBuilder.Length > 0 ? "<br />" : string.Empty);
			}
			if (!string.IsNullOrEmpty(sEndDate) && !bIsEndDateValid) {
				oErrorBuilder.AppendFormat("{0}公開終了日は yyyy/mm/dd 形式で入力して下さい。", oErrorBuilder.Length > 0 ? "<br />" : string.Empty);
			}
			if (bIsStartDateValid && bIsEndDateValid) {
				if (oStartDate > oEndDate) {
					oErrorBuilder.AppendFormat("{0}公開期間の大小関係が不正です。", oErrorBuilder.Length > 0 ? "<br />" : string.Empty);
				}
			}
			if (oErrorBuilder.Length > 0) {
				pnlError.Visible = true;
				lblErrorMessage.Text = oErrorBuilder.ToString();
				bIsCorrect = false;
			}

			if ((oListItem.FindControl("lblClosedMark") as Label).Text.StartsWith("非")) {
				oListItem.BackColor = Color.DarkGray;
			}
		}

		return bIsCorrect;
	}

	protected bool IsLinkVisible() {
		if (!ViCommConst.PicupTypes.CAST_CHARACTER.Equals(this.lstPickupType.SelectedValue) && !string.IsNullOrEmpty(this.PickupId)) {
			return true;
		}
		return false;
	}

	protected string GetNavigateUrl() {
		return string.Format("~/Extension/PickupObjsMainte.aspx?sitecd={0}&pickupid={1}", this.SiteCd, this.PickupId);
	}

	protected void lnkSort_Command(object sender, CommandEventArgs e) {
		if (!e.CommandName.Equals("SORT")) {
			return;
		}

		if (this.SortExpression.Equals(e.CommandArgument.ToString())) {
			if (this.SortDirect.Equals("ASC")) {
				this.SortDirect = "DESC";
			} else if (this.SortDirect.Equals("DESC")) {
				this.SortDirect = "ASC";
			}
		} else {
			this.SortDirect = "ASC";
		}

		this.SortExpression = e.CommandArgument.ToString();

		this.GetData();
	}

	protected void lnkPaging_Command(object sender, CommandEventArgs e) {
		if (!e.CommandName.Equals("PAGING")) {
			return;
		}

		if (this.IsCorrect()) {
			this.lblErrorMessage2.ForeColor = Color.Empty;
			if (this.lstPickupType.SelectedValue.Equals(ViCommConst.PicupTypes.CAST_CHARACTER)) {
				this.UpdatePickupCharacter(ViCommConst.FLAG_OFF);
			} else {
				this.UpdatePickupObject(ViCommConst.FLAG_OFF);
			}
		} else {
			this.lblErrorMessage2.ForeColor = Color.Red;
			return;
		}

		this.PageIndex = int.Parse(e.CommandArgument.ToString());

		this.GetData();
	}

	protected void vdcFromTo_ServerValidate(object senver, ServerValidateEventArgs e) {
		DateTime oStartDate;
		DateTime oEndDate;
		bool bIsStartDateValid = DateTime.TryParse(this.txtPublishStartDate.Text, out oStartDate);
		bool bIsEndDateValid = DateTime.TryParse(this.txtPublishEndDate.Text, out oEndDate);

		if (!string.IsNullOrEmpty(this.txtPublishStartDate.Text) && !bIsStartDateValid) {
			this.vdcFromTo.ErrorMessage = "公開開始日が不正です。";
			e.IsValid = false;
			return;
		}
		if (!string.IsNullOrEmpty(this.txtPublishEndDate.Text) && !bIsEndDateValid) {
			this.vdcFromTo.ErrorMessage = "公開終了日が不正です。";
			e.IsValid = false;
			return;
		}
		if (bIsStartDateValid && bIsEndDateValid) {
			if (oStartDate > oEndDate) {
				this.vdcFromTo.ErrorMessage = "公開期間の大小関係が不正です。";
				e.IsValid = false;
				return;
			}
		}
	}

	protected void lnkDisplayLoginIdList_Click(object sender,EventArgs e) {
		this.SetupConditionLoginIdList(true);
	}

	protected void lnkHideLoginIdList_Click(object sender,EventArgs e) {
		this.SetupConditionLoginIdList(false);
	}

	private void SetupConditionLoginIdList(bool pIsVisible) {
		this.lnkDisplayLoginIdList.Visible = !pIsVisible;
		this.txtLoginId.Enabled = !pIsVisible;
		this.txtLoginId.BackColor = pIsVisible ? System.Drawing.SystemColors.Control : System.Drawing.Color.White;
		this.pnlLoginIdList.Visible = pIsVisible;
		this.chkLoginIdListVisible.Checked = pIsVisible;
	}

	protected string GetUserStatusMark(object pUserStatus) {
		switch (iBridUtil.GetStringValue(pUserStatus)) {
			case ViCommConst.USER_WOMAN_AUTH_WAIT:
				return "認証待ち";
			case ViCommConst.USER_WOMAN_NORMAL:
				return "利用可能";
			case ViCommConst.USER_WOMAN_STOP:
				return "利用禁止";
			case ViCommConst.USER_WOMAN_HOLD:
				return "保留";
			case ViCommConst.USER_WOMAN_RESIGNED:
				return "退会";
			case ViCommConst.USER_WOMAN_BAN:
				return "利用停止";
		}
		return string.Empty;
	}

	/// <summary>
	/// 出演者様へ公開するピックアップ区分の選択中かどうかを判定
	/// </summary>
	/// <returns></returns>
	protected bool CheckCastPublishType() {
		bool bFlag = false;
		switch(lstPickupType.SelectedValue) {
			case ViCommConst.PicupTypes.PROFILE_PIC:
			case ViCommConst.PicupTypes.PROFILE_MOVIE:
			case ViCommConst.PicupTypes.BBS_PIC:
			case ViCommConst.PicupTypes.BBS_MOVIE:
				bFlag = true;
				break;
			default:
				bFlag = false;
				break;
		}
		return bFlag;
	}
}
