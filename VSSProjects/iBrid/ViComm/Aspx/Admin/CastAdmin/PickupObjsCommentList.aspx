﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="PickupObjsCommentList.aspx.cs" Inherits="CastAdmin_PickupObjsCommentList" Title="ピックアップコメント検索"
    ValidateRequest="false" %>
<%@ Import Namespace="ViComm" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="ピックアップコメント検索"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 800px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                サイトコード

                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                ﾛｸﾞｲﾝID
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtLoginId" runat="server" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle2">
                                キーワード<br />
                                (空白区切り/全てを含む)
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtKeyword" runat="server" Width="300px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" />
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[記事一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdPickupObjsComment.PageIndex + 1 %>
                        of
                        <%= grdPickupObjsComment.PageCount%>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="443px">
                    <asp:GridView ID="grdPickupObjsComment" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        DataSourceID="dsPickupObjsComment" SkinID="GridViewColor" AllowSorting="true" OnRowDataBound="grdPickupObjsComment_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="書込日時">
                                <ItemTemplate>
                                    <asp:Label ID="lblCommentDate" runat="server" Text='<%# Eval("COMMENT_DATE", "{0:yyyy/MM/dd HH:mm:ss}") %>'></asp:Label><br />
                                    <asp:LinkButton ID="lnkDel" runat="server" CommandArgument='<%#string.Format("{0},{1},{2}",Eval("SITE_CD"),Eval("PICKUP_OBJS_COMMENT_SEQ"),Eval("ADMIN_DEL_FLAG")) %>'
                                        OnClientClick="return confirm('削除を実行しますか？');" OnCommand="lnkDelPickupObjsComment_Command"
                                        Text='<%# ViCommConst.FLAG_ON_STR.Equals(Eval("ADMIN_DEL_FLAG","{0}")) ? "復活" : "削除" %>'></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ﾛｸﾞｲﾝID">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblLoginId" runat="server" NavigateUrl='<%# string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>'
                                        Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle Width="120px" HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ﾊﾝﾄﾞﾙ名">
                                <ItemTemplate>
                                    <asp:Label ID="lblHandleNm" runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="120px" HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="内容">
                                <ItemTemplate>
                                    <asp:Label ID="lblCommentText" runat="server" Text='<%# Eval("COMMENT_TEXT") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
 							<asp:TemplateField HeaderText="削除">
								<ItemTemplate>
									<%# Eval("ADMIN_DEL_STATUS") %><br />
									<%# Eval("DEL_STATUS") %>
								</ItemTemplate>
								<ItemStyle VerticalAlign="Top" HorizontalAlign="Center" />
							</asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
            <br />
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsPickupObjsComment" runat="server" SelectMethod="GetPageCollection" ConvertNullToDBNull="true" SortParameterName=""
        TypeName="PickupObjsComment" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelected="dsPickupObjsComment_Selected"
        OnSelecting="dsPickupObjsComment_Selecting">
        <SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
</asp:Content>
