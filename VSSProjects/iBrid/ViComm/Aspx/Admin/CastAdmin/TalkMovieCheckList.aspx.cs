﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 認証待ち会話ムービー一覧
--	Progaram ID		: TalkMovieCheckList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater			Update Explain
  2009/09/03	i-Brid(Y.Inoue) 担当サイト対応  2010/06/29	伊藤和明		検索項目に日付を追加
  2010/07/16	Koyanagi		検索項目に非公開ﾌﾗｸﾞを追加

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Cast_TalkMovieCheckList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			FirstLoad();
			InitPage();
			ViewState["SiteCd"] = iBridUtil.GetStringValue(Request.QueryString["moviesite"]);
			ViewState["NotPublishFlag"] = iBridUtil.GetStringValue(Request.QueryString["closed"]);
			if (iBridUtil.GetStringValue(ViewState["SiteCd"]).Equals("") == false) {
				lstSiteCd.SelectedValue = ViewState["SiteCd"].ToString();
			} else if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
				lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(this.Request.QueryString["seeksitecd"]);
			if (ViewState["NotPublishFlag"].ToString() == "1") {
				chkClosed.Checked = true;
			}
			GetList();
		}
	}

	protected void dsTalkMovie_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	private void FirstLoad() {
		grdMovie.PageSize = 100;
		grdMovie.DataSourceID = "";
		DataBind();
		if (Session["SiteCd"].ToString().Equals("")) {
			lstSiteCd.Items.Insert(0,new ListItem("",""));
		}
		lstSiteCd.DataSourceID = "";
	}

	private void InitPage() {
		grdMovie.DataSourceID = "";
		DataBind();
		recCount = "0";
		chkClosed.Checked = false;
	}

	protected string GetRecCount() {
		return recCount;
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		if(!this.IsValid)return;
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.Response.Redirect(this.Request.Url.PathAndQuery);
	}

	protected void lnkMainte_Command(object sender,CommandEventArgs e) {
		string[] sKey = e.CommandArgument.ToString().Split(':');
		string sNotPublishFlag = "";
		if (chkClosed.Checked) {
			sNotPublishFlag = "1";
		}
		Response.Redirect(string.Format("../CastAdmin/TalkMovieMainte.aspx?sitecd={0}&userseq={1}&movieseq={2}&return={3}&closed={4}&seeksitecd={5}",sKey[0],sKey[1],sKey[2],sKey[3],sNotPublishFlag,lstSiteCd.SelectedValue));
	}

	protected void lnkQuickTime_Command(object sender,CommandEventArgs e) {
		string sURL = string.Format("file:///M:/{0}",e.CommandArgument.ToString());
		string sScripts = string.Format("<script language=\"javascript\">window.open('{0}','','width=300,height=300,resizable=no,directories=no,scrollbars=no' , false);</script>",sURL);
		ClientScript.RegisterStartupScript(Page.GetType(),"OpenNewWindow",sScripts);
	}

	private void GetList() {
		grdMovie.DataSourceID = "dsTalkMovie";
		grdMovie.PageIndex = 0;
		grdMovie.DataBind();
		pnlCount.DataBind();
	}

	protected void dsTalkMovie_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		string sNotPublishFlag = "";
		if (chkClosed.Checked) {
			sNotPublishFlag = "1";
		}
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = "1";
		e.InputParameters[2] = sNotPublishFlag;
		e.InputParameters[3] = string.IsNullOrEmpty(this.txtUploadDayFrom.Text.Replace("/",string.Empty)) ? null : this.txtUploadDayFrom.Text;
		e.InputParameters[4] = string.IsNullOrEmpty(this.txtUploadDayTo.Text.Replace("/", string.Empty)) ? null : this.txtUploadDayTo.Text;
		
	}

	protected void grdMovie_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			if (DataBinder.Eval(e.Row.DataItem,"OBJ_NOT_PUBLISH_FLAG").ToString() == "1") {
				e.Row.BackColor = System.Drawing.Color.LightYellow;
			}
		}
	}
}
