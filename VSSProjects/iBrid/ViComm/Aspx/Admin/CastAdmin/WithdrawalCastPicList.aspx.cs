﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 退会出演者画像一覧
--	Progaram ID		: WithdrawalCastPicList
--
--  Creation Date	: 2017.05.16
--  Creater			: M&TT Zukeran
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date			Updater				Update Explain

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class CastAdmin_WithdrawalCastPicList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			this.InitPage();
		}
	}

	private void InitPage() {
		// 初期化
		txtLoginId.Text = string.Empty;
		txtWithdrawalDayFrom.Text = string.Empty;
		txtWithdrawalDayTo.Text = string.Empty;
		chkProfilePicFlag.Checked = false;
		chkBbsPicFlag.Checked = false;
		chkStockPicFlag.Checked = false;

		recCount = "0";
		grdPic.PageSize = 100;
		grdPic.PageIndex = 0;
		grdPic.DataSourceID = "";
		grdPic.DataBind();
		pnlCount.DataBind();
	}

	/// <summary>
	/// 一覧データ取得
	/// </summary>
	private void GetList() {
		grdPic.DataSourceID = "dsCastPic";
		grdPic.PageIndex = 0;
		grdPic.DataBind();
		pnlCount.DataBind();
	}

	/// <summary>
	/// 検索ボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (!this.IsValid) {
			return;
		}
		this.GetList();
	}

	/// <summary>
	/// クリアボタン押下
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	/// <summary>
	/// 検索結果件数取得
	/// </summary>
	/// <returns></returns>
	protected string GetRecCount() {
		return recCount;
	}

	protected void dsCastPic_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void dsCastPic_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = PwViCommConst.MAIN_SITE_CD;
		e.InputParameters[1] = txtLoginId.Text;
		e.InputParameters[2] = string.IsNullOrEmpty(this.txtWithdrawalDayFrom.Text.Replace("/",string.Empty)) ? null : this.txtWithdrawalDayFrom.Text;
		e.InputParameters[3] = string.IsNullOrEmpty(this.txtWithdrawalDayTo.Text.Replace("/",string.Empty)) ? null : this.txtWithdrawalDayTo.Text;
		ArrayList list = new ArrayList();
		// プロフ画像
		if (chkProfilePicFlag.Checked) {
			list.Add(ViCommConst.ATTACHED_PROFILE.ToString());
			list.Add(ViCommConst.ATTACHED_HIDE.ToString());
		}
		// 掲示板画像
		if (chkBbsPicFlag.Checked) {
			list.Add(ViCommConst.ATTACHED_BBS.ToString());
		}
		// ストック画像
		if (chkStockPicFlag.Checked) {
			list.Add(ViCommConst.ATTACHED_MAIL.ToString());
		}
		if (list.Count > 0) {
			e.InputParameters[4] = string.Join(",",(string[])list.ToArray(typeof(string)));
		} else {
			e.InputParameters[4] = string.Empty;
		}
	}

}
