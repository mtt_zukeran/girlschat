﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ヘッダ画面
--	Progaram ID		: Header
--
--  Creation Date	: 2010.04.06
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Configuration;
using System.Web.UI.WebControls;
using iBridCommLib;
using ViComm;

public partial class Header:System.Web.UI.Page {

	protected void Page_PreInit(object sender,EventArgs e) {
		string sTheme = iBridUtil.GetStringValue(ConfigurationManager.AppSettings["theme"]);
		if (!sTheme.Equals(string.Empty)) {
			Page.Theme = sTheme;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			lblLocNm.Text = iBridUtil.GetStringValue(Session["SysLocNm"]);
		}
	}

	protected void lnkLogout_Command(object sender,CommandEventArgs e) {
		string sRoot = ConfigurationManager.AppSettings["Root"];
		string sUrl = "http://" + Request.Url.Authority + sRoot + "/LoginAdmin.aspx";
		Session.RemoveAll();
		Response.Redirect(sUrl);
	}
}
