﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: エラー通知
--	Progaram ID		: WebError
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Configuration;
using System.Web.UI.WebControls;
using iBridCommLib;
using ViComm;

public partial class error_WebError : System.Web.UI.Page {
	protected void Page_Load(object sender, EventArgs e) {
		string sError = iBridUtil.GetStringValue(Request.QueryString["errtype"]);
		if (sError.Equals(ViCommConst.USER_INFO_INTERNAL_ERROR.ToString())) {
			lblText.Text = "内部エラーが発生しました。";
		}
	}

	protected void Button1_Click(object sender, EventArgs e) {
		Server.Transfer(ConfigurationManager.AppSettings["Root"] + "/LoginAdmin.aspx");
	}
}
