﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: エラー通知
--	Progaram ID		: DbAttention
--
--  Creation Date	: 2009.07.28
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Configuration;
using System.Web.UI.WebControls;
using iBridCommLib;
using ViComm;

public partial class error_DbAttention:System.Web.UI.Page {
	protected void Page_Load(object sender,EventArgs e) {
		string sError = iBridUtil.GetStringValue(Request.QueryString["errtype"]);
		if (sError.Equals(ViCommConst.SP_ERR_STATUS_ALREADY_UPDATED.ToString())) {
			lblTitle.Text = "重複操作";
			lblText.Text = "他の方がデータの編集をしたため<br />データの取得をやり直してください。";
		}
		else if(sError.Equals(ViCommConst.SP_ERR_STATUS_HAVE_CHILD_RECORD.ToString())) {
			lblTitle.Text = "削除エラー";
			lblText.Text = "子レコードが存在するため<br />このデータは消去できませんでした。";
		}
	}
}
