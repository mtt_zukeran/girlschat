﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WebErrorForMenu.aspx.cs" Inherits="error_WebErrorForMenu" Title="Attention" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>ViComm Site Management</title>
</head>
<body class="menu">
	<form id="form1" runat="server" target="_parent">
		<div align="center">
			<asp:Label ID="lblText" runat="server" Text="ログインが無効になりました。" ForeColor="Red" Font-Size="Small"></asp:Label>
			<br />
			<asp:Label ID="Label4" runat="server" ForeColor="Red" Text="再度ログインしてください。" Font-Size="Small"></asp:Label>
			<br />
			<br />
			<asp:Button ID="Button3" runat="server" Text="再ログイン" OnClick="Button1_Click" Width="150px" />
		</div>
	</form>
</body>
</html>
