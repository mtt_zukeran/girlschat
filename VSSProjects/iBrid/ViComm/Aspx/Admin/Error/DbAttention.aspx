<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DbAttention.aspx.cs" Inherits="error_DbAttention" Title="Attention" %>

<html xmlns="http://www.w3.org/1999/xhtml">

<head id="Head1" runat="server">
	<title>ViComm Site Management</title>
</head>
<body>
	<form id="form1" runat="server" target="_parent">
		<div id="container">
<%--
			<div id="header">
				<asp:Label ID="Label2" runat="server" Text="ViComm Site Management" CssClass="sysname"></asp:Label>
				<asp:LoginName ID="LoginName1" runat="server" CssClass="username" />
			</div>
--%>
			<div id="content">
				<div id="contentright">
					<div id="title">
						<asp:Label ID="lblTitle" runat="server"></asp:Label>
					</div>
					<div id="contentborder">
						<br />
						<br />
						<table align="center" cellpadding="0" cellspacing="0">
							<tr>
								<td style="height: 81px;">
									<div>
										<asp:Label ID="lblText" runat="server" ForeColor="Red"></asp:Label>
									</div>
									<div>
										<asp:Label ID="Label4" runat="server" ForeColor="Red" Text="前のページに戻ってください。"></asp:Label>
									</div>
									<div align="center">
										<br />
										<asp:Button ID="Button3" runat="server" Text="戻る" Width="174px" OnClientClick="history.back();return false;" />
									</div>
								</td>
							</tr>
						</table>
						<br />
						<br />
						<br />
					</div>
				</div>
			</div>
			<div id="footer">
				<p>
					&copy;
					<asp:Label ID="lblCopyrightYear" runat="server" Text="2008" />
					<asp:Label ID="lblCopyrightName" runat="server" Text="Powerd by" EnableViewState="false" />
					<a href="http://www.ibrid.co.jp">iBrid Co. Ltd.</a>
				</p>
			</div>
		</div>
	</form>
</body>
</html>
