﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MailScheduleMainteWoman.aspx.cs" Inherits="Mail_MailScheduleMainteWoman"
	Title="出演者メール配信スケジュール" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="出演者メール配信スケジュール"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<!-- 検索条件パネル -->
		<asp:Panel runat="server" ID="pnlPattern">
			<%--<a>『tag』+DBのColumn名としてIDを設定する。（Column名がDBに更新される）対象外はControl識別3文字をつける</a>--%>
			<fieldset class="fieldset">
				<legend>[パターン設定]</legend>
				<asp:HiddenField ID="hdfPatternSeq" runat="server" Visible="False" Value="">
				</asp:HiddenField>
				<table border="0" style="width: 900px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							パターン名
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtPatternName" runat="server" MaxLength="60" Width="150px"></asp:TextBox>
							<asp:RequiredFieldValidator ID="vdrPatternName" runat="server" ErrorMessage="パターン名を入力して下さい。" ControlToValidate="txtPatternName" ValidationGroup="Key">*</asp:RequiredFieldValidator>
						</td>
						<td class="tdHeaderStyle">
							設定
						</td>
						<td class="tdDataStyle">
							<asp:RadioButton ID="rdoMailEnable" runat="server" Checked="True" GroupName="MailEnable" Text="有効">
							</asp:RadioButton>
							<asp:RadioButton ID="rdoMailNoEnable" runat="server" GroupName="MailEnable" Text="無効">
							</asp:RadioButton>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="tagSITE_CD" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px" AutoPostBack="True"
								OnSelectedIndexChanged="tagSITE_CD_SelectedIndexChanged">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle">
							オンライン状態
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="tagCHARACTER_ONLINE_STATUS" runat="server" DataSourceID="dsOnline" DataTextField="CODE_NM" DataValueField="CODE" Width="180px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							プロダクション
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="tagPRODUCTION_SEQ" runat="server" DataSourceID="dsProduction" DataTextField="PRODUCTION_NM" DataValueField="PRODUCTION_SEQ" Width="180px"
								OnSelectedIndexChanged="lstProductionSeq_SelectedIndexChanged" AutoPostBack="True">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle">
							担当
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="tagMANAGER_SEQ" runat="server" DataSourceID="dsManager" DataTextField="MANAGER_NM" DataValueField="MANAGER_SEQ" Width="180px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							ログインＩＤ
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="tagLOGIN_ID" runat="server"></asp:TextBox>
						</td>
						<td class="tdHeaderStyle">
							<%= DisplayWordUtil.Replace("出演者名") %>
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="tagCAST_NM" runat="server"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							ハンドル名
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="tagHANDLE_NM" runat="server"></asp:TextBox>
						</td>
						<td class="tdHeaderStyle">
							接続種別
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="tagCONNECT_TYPE" runat="server" DataSourceID="dsConnectType" DataTextField="CODE_NM" DataValueField="CODE" Width="180px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							端末種別
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="tagUSE_TERMINAL_TYPE" runat="server" DataSourceID="dsUseTerminalType" DataTextField="CODE_NM" DataValueField="CODE" Width="180px">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle">
							カテゴリ
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="tagACT_CATEGORY_SEQ" runat="server" DataSourceID="dsActCategory" DataTextField="ACT_CATEGORY_NM" DataValueField="ACT_CATEGORY_SEQ"
								Width="180px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							メールアドレス
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="tagEMAIL_ADDR" runat="server"></asp:TextBox>
							<asp:CheckBox ID="tagEXCLUDE_EMAIL_ADDR_FLAG" runat="server" Text="除外" />
						</td>
						<td class="tdHeaderStyle">
							登録日
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="tagREGIST_DATE" runat="server" MaxLength="3" Width="44px"></asp:TextBox>日経過
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							報酬累計額
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="tagTOTAL_PAYMENT_AMT_FROM" runat="server" MaxLength="9" Width="70px"></asp:TextBox>&nbsp;～
							<asp:TextBox ID="tagTOTAL_PAYMENT_AMT_TO" runat="server" MaxLength="9" Width="70px"></asp:TextBox>
							<asp:CompareValidator ID="vdcTotalPaymentAmtFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="tagTOTAL_PAYMENT_AMT_FROM" ControlToValidate="tagTOTAL_PAYMENT_AMT_TO"
								Operator="GreaterThanEqual" ValidationGroup="Key" Type="Integer">*</asp:CompareValidator>
						</td>
						<td class="tdHeaderStyle">
							最終ログイン日
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="tagLAST_LOGIN_DATE" runat="server" MaxLength="3" Width="44px"></asp:TextBox>日経過
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							メールアドレス状態
						</td>
						<td class="tdDataStyle">
							<asp:PlaceHolder ID="tagNON_EXIST_MAIL_ADDR_FLAG" runat="server">
								<asp:RadioButton ID="rdoMailOK" runat="server" GroupName="MailType" Text="正常">
								</asp:RadioButton>
								<asp:RadioButton ID="rdoMailNG" runat="server" GroupName="MailType" Text="ﾌｨﾙﾀﾘﾝｸﾞｴﾗｰ">
								</asp:RadioButton>
							</asp:PlaceHolder>
						</td>
						<td class="tdHeaderStyle">
							ユーザーランク
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="tagUSER_RANK" runat="server" Width="180px" DataTextField="USER_RANK_NM" DataValueField="USER_RANK">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							ﾒｰﾙ種別
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="tagMAIL_TEMPLATE_NO" runat="server" Width="200px" DataValueField="MAIL_TEMPLATE_NO" DataTextField="TEMPLATE_NM">
							</asp:DropDownList>
							<asp:HiddenField ID="hdfMailInfoCnt" runat="server" Visible="False" Value="0">
							</asp:HiddenField>
						</td>
						<td class="tdHeaderStyle">
							広告コード
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="tagAD_CD" runat="server" MaxLength="32" Width="200px"></asp:TextBox>
							<asp:RegularExpressionValidator ID="vdeAdCd" runat="server" ErrorMessage="広告コードは32桁以下の英数字で入力して下さい。" ValidationExpression="[a-zA-Z0-9-\/_]{1,32}" ControlToValidate="tagAD_CD"
								ValidationGroup="Key" Display="Dynamic">*</asp:RegularExpressionValidator>
							<asp:CustomValidator ID="vdcAdCd" runat="server" ControlToValidate="tagAD_CD" ErrorMessage="広告コードが未登録です。" OnServerValidate="vdcAdCd_ServerValidate" ValidationGroup="Key"
								Display="Dynamic">*</asp:CustomValidator>
							<asp:Label ID="lblAdNm" runat="server" Text=""></asp:Label>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							ｷｬﾗｸﾀ状態
						</td>
						<td class="tdDataStyle">
							<asp:CheckBoxList ID="tagNA_FLAG" runat="server" DataSourceID="dsNaFlag" DataTextField="CODE_NM" DataValueField="CODE" RepeatDirection="Horizontal">
							</asp:CheckBoxList>
						</td>
						<td class="tdHeaderStyle">
							ﾒｰﾙｻｰﾊﾞ
						</td>
						<td class="tdDataStyle">
							<asp:PlaceHolder ID="tagMAIL_SERVER" runat="server">
								<asp:RadioButton ID="rdoMailServerBeam" runat="server" GroupName="MailServer" Text="BEAM">
								</asp:RadioButton>
								<asp:RadioButton ID="rdoMailServerMuller3" runat="server" GroupName="MailServer" Text="MULLER3">
								</asp:RadioButton>
							</asp:PlaceHolder>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							配信時間
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstTaskHour" runat="server" Width="40px">
							</asp:DropDownList>
							<asp:DropDownList ID="lstTaskMin" runat="server" Width="40px">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle2">
							配信曜日
						</td>
						<td class="tdDataStyle">
							<asp:PlaceHolder ID="plcWeek" runat="server">
								<asp:CheckBox ID="chkWeek1" runat="server" Text="月曜 " />
								<asp:CheckBox ID="chkWeek2" runat="server" Text="火曜 " />
								<asp:CheckBox ID="chkWeek3" runat="server" Text="水曜 " />
								<asp:CheckBox ID="chkWeek4" runat="server" Text="木曜 " />
								<asp:CheckBox ID="chkWeek5" runat="server" Text="金曜 " />
								<asp:CheckBox ID="chkWeek6" runat="server" Text="土曜 " />
								<asp:CheckBox ID="chkWeek7" runat="server" Text="日曜 " />
							</asp:PlaceHolder>
							<asp:Label ID="lblTaskWeek" runat="server" Text="曜日指定して下さい。" Font-Size="Small" ForeColor="Red"></asp:Label>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Key" OnClick="btnUpdate_Click" />
				<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" CausesValidation="False" OnClick="btnUpdate_Click" />
				<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" CausesValidation="False" OnClick="btnCancel_Click" />
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlList">
			<fieldset>
				<legend>[パターン一覧]</legend>
				<asp:GridView ID="grdPattern" runat="server" AllowPaging="True" DataSourceID="dsSchedulePattern" AllowSorting="True" SkinID="GridView" AutoGenerateColumns="False"
					Width="900px" OnRowDataBound="grdPattern_RowDataBound">
					<Columns>
						<asp:TemplateField HeaderText="パターン名">
							<ItemTemplate>
								<asp:LinkButton ID="lnkPatternNm" runat="server" Text='<%# Eval("PATTERN_NAME") %>' CommandArgument='<%# Eval("PATTERN_SEQ") %>' OnCommand="lnkPatternNm_Command"
									CausesValidation="False"></asp:LinkButton>
							</ItemTemplate>
							<HeaderTemplate>
								パターン名
							</HeaderTemplate>
							<HeaderStyle Width="250px" />
						</asp:TemplateField>
						<asp:BoundField DataField="TX_TIME" HeaderText="配信時刻" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" />
							<HeaderStyle Width="100px" />
						</asp:BoundField>
						<asp:BoundField DataField="PATTERN_FLG" HeaderText="設定" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" />
							<HeaderStyle Width="50px" />
						</asp:BoundField>
						<asp:BoundField DataField="TX_WEEK1" HeaderText="月" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" />
							<HeaderStyle Width="50px" />
						</asp:BoundField>
						<asp:BoundField DataField="TX_WEEK2" HeaderText="火" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" />
							<HeaderStyle Width="50px" />
						</asp:BoundField>
						<asp:BoundField DataField="TX_WEEK3" HeaderText="水" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" />
							<HeaderStyle Width="50px" />
						</asp:BoundField>
						<asp:BoundField DataField="TX_WEEK4" HeaderText="木" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" />
							<HeaderStyle Width="50px" />
						</asp:BoundField>
						<asp:BoundField DataField="TX_WEEK5" HeaderText="金" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" />
							<HeaderStyle Width="50px" />
						</asp:BoundField>
						<asp:BoundField DataField="TX_WEEK6" HeaderText="土" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" />
							<HeaderStyle Width="50px" />
						</asp:BoundField>
						<asp:BoundField DataField="TX_WEEK7" HeaderText="日" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" />
							<HeaderStyle Width="50px" />
						</asp:BoundField>
						<asp:BoundField DataField="TX_MAIL_LAST_DATE" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HeaderText="最新配信日" HtmlEncode="False">
							<HeaderStyle Width="150px" />
						</asp:BoundField>
					</Columns>
				</asp:GridView>
				<a class="reccount">Record Count
					<%# GetRecCount() %>
				</a>
				<br>
				<a class="reccount">Current viewing page
					<%=grdPattern.PageIndex + 1%>
					of
					<%=grdPattern.PageCount%>
				</a>
				<asp:Button ID="btnRegist" runat="server" Text="パターン追加" OnClick="btnRegist_Click" />
			</fieldset>
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsProduction" runat="server" SelectMethod="GetList" TypeName="Production"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsManager" runat="server" SelectMethod="GetProductionList" TypeName="Manager" OnSelecting="dsManager_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pProductionSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUserStatus" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="51" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsOnLine" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="52" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSchedulePattern" runat="server" EnablePaging="True" SelectCountMethod="GetPageCount" SelectMethod="GetPageCollection" TypeName="SchedulePattern"
		OnSelecting="dsSchedulePattern_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSexCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsConnectType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="61" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUseTerminalType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="55" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsActCategory" runat="server" SelectMethod="GetList" TypeName="ActCategory" OnSelecting="dsActCategory_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsNaFlag" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="98" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum3" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="tagTOTAL_PAYMENT_AMT_FROM" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum4" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="tagTOTAL_PAYMENT_AMT_TO" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrPatternName" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdcTotalPaymentAmtFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8" TargetControlID="vdeAdCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9" TargetControlID="vdcAdCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
