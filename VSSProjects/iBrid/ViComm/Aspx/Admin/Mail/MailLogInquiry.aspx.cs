﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メール配信記録
--	Progaram ID		: MailLogInquiry
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater		Update Explain
  2010/06/15  KazuakiItoh	一括送信メールのデフォルト非表示対応

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Mail_MailLogInquiry:System.Web.UI.Page {
	#region Sorting

	private string SortExpression {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortExpression"]);
		}
		set {
			this.ViewState["SortExpression"] = value;
		}
	}

	private string SortDirection {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortDirection"]);
		}
		set {
			this.ViewState["SortDirection"] = value;
		}
	}
	#endregion

	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			FisrtLoad();
			InitPage();
		}
	}

	private void FisrtLoad() {
		grdMail.PageSize = int.Parse(Session["PageSize"].ToString());
		grdMail.DataSourceID = "";
		DataBind();
		if (Session["SiteCd"].ToString().Equals("")) {
			lstSiteCd.Items.Insert(0,new ListItem("",""));
		}
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		lstSiteCd.DataSourceID = "";
		lstMailType.Items.Insert(0,new ListItem("",""));
		lstMailType.DataSourceID = "";
	}

	private void InitPage() {
		pnlInfo.Visible = false;
		ClearField();
	}

	private void ClearField() {
		txtTxLoginId.Text = "";
		txtRxLoginId.Text = "";
		txtEmailAddr.Text = "";
		txtReportDayFrom.Text = DateTime.Now.ToString("yyyy/MM/dd");
		txtReportTimeFrom.Text = "";
		txtReportDayTo.Text = "";
		txtReportTimeTo.Text = "";
		lstMailType.SelectedIndex = 0;
		SortExpression = string.Empty;
		SortDirection = string.Empty;
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	private void GetList() {
		if (txtReportDayTo.Text.Equals("")) {
			txtReportDayTo.Text = txtReportDayFrom.Text;
		}
		DateTime dtTo = System.DateTime.ParseExact(txtReportDayTo.Text,"yyyy/MM/dd",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);

		grdMail.PageSize = 50;
		grdMail.DataSourceID = "dsReqTxMailDtl";
		DataBind();
		pnlInfo.Visible = true;
	}

	protected string CheckAdminLevel(object pText) {
		if (Session["AdminType"].ToString().Equals(ViCommConst.RIGHT_SITE_OWNER)) {
			return pText.ToString();
		} else {
			return "***********";
		}
	}

	protected string GetTransferNm(object pTransfer) {
		if (pTransfer.ToString().Equals("1")) {
			return "振替済み";
		} else {
			return "";
		}
	}

	protected void dsReqTxMailDtl_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = lstMailType.SelectedValue;
		e.InputParameters[2] = txtReportDayFrom.Text;
		e.InputParameters[3] = txtReportTimeFrom.Text;
		if (txtReportDayTo.Text.Equals("")) {
			txtReportDayTo.Text = txtReportDayFrom.Text;
		}

		e.InputParameters[4] = txtReportDayTo.Text;
		e.InputParameters[5] = txtReportTimeTo.Text;
		e.InputParameters[6] = txtTxLoginId.Text;
		e.InputParameters[7] = txtTxUserCharNo.Text;
		e.InputParameters[8] = txtRxLoginId.Text;
		e.InputParameters[9] = txtRxUserCharNo.Text;
		e.InputParameters[10] = txtEmailAddr.Text;
		e.InputParameters[11] = rdoWithBatchMail.SelectedValue;
		e.InputParameters[12] = SortExpression;
		e.InputParameters[13] = SortDirection;
	}

	protected void dsReqTxMailDtl_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void grdMail_Sorting(object sender,GridViewSortEventArgs e) {
		if (this.SortExpression.Equals(e.SortExpression)) {
			if (this.SortDirection.Equals("ASC")) {
				this.SortDirection = "DESC";
			} else if (this.SortDirection.Equals("DESC")) {
				this.SortDirection = "ASC";
			}
		} else {
			this.SortDirection = "ASC";
		}

		this.SortExpression = e.SortExpression;

		e.Cancel = true;
		this.GetList();
	}

	protected string GetViewUrl(object pSexCd,object pSiteCd,object pLoginId,object pMailTemplateSiteCd,object pUserLoginId,object pCastLoginId,object pTXRX) {
		if (pSexCd.Equals(ViCommConst.MAN)) {
			return string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",pSiteCd.ToString(),pLoginId.ToString());
		} else if (pSexCd.Equals(ViCommConst.OPERATOR)) {
			return string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}",pLoginId.ToString(),"MailLogInquiry.aspx");
		} else if (pTXRX.Equals("TX")) {
			return string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}",pCastLoginId.ToString(),"MailLogInquiry.aspx");
		} else {
			return string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",pMailTemplateSiteCd.ToString(),pUserLoginId.ToString());
		}
	}
	
	protected decimal Convert2KB(object pValue){
		decimal dKbValue;
		if(!decimal.TryParse(iBridUtil.GetStringValue(pValue),out dKbValue)){
			dKbValue = decimal.Zero;
		}
		return Math.Ceiling(dKbValue / (decimal)1024);
	}
	
	protected bool IsDocomoMailAddr(object pValue){
		return iBridUtil.GetStringValue(pValue).ToLower().EndsWith("docomo.ne.jp");
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		if (grdMail.Rows.Count == 0) {
			return;
		}

		DataSet ds = this.GetListDs();

		//ヘッダ作成
		string sHeader = "送信日時,送信者ID,受信者ID,件名\r\n";

		//CSV出力
		Response.ContentType = "application/download";
		Response.AppendHeader("Content-Disposition","attachment;filename=MailLogList.csv");
		Response.ContentEncoding = System.Text.Encoding.GetEncoding("Shift_JIS");

		Response.Write(sHeader);

		if (ds.Tables[0].Rows.Count > 0) {
			foreach (DataRow dr in ds.Tables[0].Rows) {
				Response.Write(string.Format("{0},{1},{2},{3}\r\n",dr["REQUEST_TX_DATE"].ToString(),dr["TX_LOGIN_ID"].ToString(),
							   dr["RX_LOGIN_ID"].ToString(),dr["MAIL_TITLE"].ToString()));
			}
		}

		Response.End();
	}
	
	private DataSet GetListDs() {
		DataSet oDataSet;
		using (ReqTxMailDtl oReqTxMailDtl = new ReqTxMailDtl()) {
			oDataSet = oReqTxMailDtl.GetPageCollection(
				lstSiteCd.SelectedValue,
				lstMailType.SelectedValue,
				txtReportDayFrom.Text,
				txtReportTimeFrom.Text,
				txtReportDayTo.Text.Equals("") ? txtReportDayFrom.Text : txtReportDayTo.Text,
				txtReportTimeTo.Text,
				txtTxLoginId.Text,
				txtTxUserCharNo.Text,
				txtRxLoginId.Text,
				txtRxUserCharNo.Text,
				txtEmailAddr.Text,
				0,
				SysConst.DB_MAX_ROWS,
				rdoWithBatchMail.SelectedValue,
				SortExpression,
				SortDirection
			);
		}
		return oDataSet;
	}
}
