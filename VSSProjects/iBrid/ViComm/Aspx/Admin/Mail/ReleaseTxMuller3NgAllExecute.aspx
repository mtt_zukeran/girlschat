﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReleaseTxMuller3NgAllExecute.aspx.cs" Inherits="Mail_ReleaseTxMuller3NgAllExecute"
	Title="MULLER3フィルタリングエラー一括解除" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="MULLER3フィルタリングエラー一括解除"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<asp:Panel runat="server" ID="pnlKey">
                <asp:Button runat="server" ID="btnExecute" Text="一括解除" CssClass="seekbutton" CausesValidation="False" OnClick="btnExecute_Click" />
                <asp:Label runat="server" ID="lblDone" Text="一括解除が完了しました。" ForeColor="red" Visible="false" />
			</asp:Panel>
		</fieldset>
	</div>
</asp:Content>
