﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 管理者ﾒｰﾙ送信履歴
--	Progaram ID		: AdminMailTxHistoryList.aspx
--
--  Creation Date	: 2011.02.15
--  Creater			: K.Itoh
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;

using iBridCommLib;
using ViComm;

public partial class Mail_AdminMailTxHistoryList : System.Web.UI.Page {
	private int recCount = 0;

	protected void Page_Load(object sender, EventArgs e) {
		if(!this.IsPostBack){
			this.InitPage();
		}
	}
	protected void btnListSeek_Click(object sender, EventArgs e) {
		if (IsValid) {
			GetList();
		}
	}
	protected void btnClear_Click(object sender, EventArgs e) {
		this.ClearField();
	}
	protected void dsData_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null && !(e.ReturnValue is DataSet)) {
			recCount = int.Parse(iBridUtil.GetStringValue(e.ReturnValue));
		}
	}
	protected void dsData_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = this.lstSiteCd.SelectedValue;
		e.InputParameters["pProcStatus"] = this.lstProcStatus.SelectedValue;
	}



	private void InitPage(){
		this.lstSiteCd.DataBind();
		this.lstProcStatus.DataBind();
		this.ClearField();
	}
	private void ClearField(){
		if (!SessionObjs.MenuSite.Equals(string.Empty)) {
			lstSiteCd.SelectedValue = SessionObjs.MenuSite;
		}
		lstProcStatus.SelectedIndex = 0;
		recCount = 0;
		this.pnlInfo.Visible = false;
	}
	private void GetList() {
		pnlInfo.Visible = true;
		this.grdData.DataSourceID = this.dsData.ID;
		this.grdData.DataBind();
		this.pnlCount.DataBind();
		this.pnlInfo.Visible = true;
	}
	protected int GetRecCount() {
		return recCount;
	}

	
	protected Color GetProcStatusColor(string pProcStatus){
		Color oColor = Color.Black;
		
		switch(pProcStatus){
			case ViCommConst.BulkProcStatus.PROCESSING:
				oColor = Color.Blue;
				break;
			case ViCommConst.BulkProcStatus.ERROR:
				oColor = Color.Red;
				break;
		}

		return oColor;
	}
	
}
