﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AdminMailTxHistoryList.aspx.cs" Inherits="Mail_AdminMailTxHistoryList" Title="Untitled Page" %>
<%@ Import namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="管理者ﾒｰﾙ送信履歴"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" Runat="Server">
	<div class="admincontent">
		<%-- ============================== --%>
		<%--  Search Condition              --%>
		<%-- ============================== --%>
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle2">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderStyle2">
							ｽﾃｰﾀｽ
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstProcStatus" runat="server" DataSourceID="dsBulkProcStatus" DataTextField="CODE_NM" DataValueField="CODE" Width="240px" AppendDataBoundItems="true">
								<asp:ListItem Text="" Value=""></asp:ListItem>
							</asp:DropDownList>
						</td>
					</tr>					
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索"		CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="True" />
				<asp:Button runat="server" ID="btnClear"	Text="クリア"	CssClass="seekbutton" OnClick="btnClear_Click" />
			</asp:Panel>
		</fieldset>

		<%-- ============================== --%>
		<%--  Data		                    --%>
		<%-- ============================== --%>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset class="fieldset">
				<legend>[送信履歴]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count
						<%#GetRecCount() %>
					</a>
					<br />
					<a class="reccount">Current viewing page
						<%=grdData.PageIndex + 1%>
						of
						<%=grdData.PageCount%>
					</a>
				</asp:Panel>
				&nbsp;				
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto">
					<asp:GridView ID="grdData" runat="server" AllowSorting="false" AllowPaging="True" AutoGenerateColumns="False" SkinID="GridView" PageSize="20">
					    <Columns>
						    <asp:TemplateField HeaderText="ｽﾃｰﾀｽ" SortExpression="BULK_PROC_STATUS">
							    <ItemStyle HorizontalAlign="center" Font-Bold="true"/>
								<ItemTemplate>
									<asp:Label ID="lblStatus" runat="server" ForeColor='<%# GetProcStatusColor(Eval("BULK_PROC_STATUS").ToString()) %>' ToolTip='<%# Eval("ADMIN_MAIL_TX_HISTORY_SEQ") %>' >
										<%# DataBinder.Eval(Container.DataItem, "BULK_PROC_STATUS_NM") %>
									</asp:Label> 
							    </ItemTemplate>
						    </asp:TemplateField>							    
						    <asp:TemplateField HeaderText="開始日時" SortExpression="START_DATE">
							    <ItemStyle HorizontalAlign="left"/>
								<ItemTemplate>
									<%# DataBinder.Eval(Container.DataItem, "START_DATE", "{0:yyyy/MM/dd HH:mm:ss}")%>
							    </ItemTemplate>
						    </asp:TemplateField>
						    <asp:TemplateField HeaderText="終了日時" SortExpression="END_DATE">
							    <ItemStyle HorizontalAlign="left"/>
								<ItemTemplate>
									<%# DataBinder.Eval(Container.DataItem, "END_DATE", "{0:yyyy/MM/dd HH:mm:ss}")%>
							    </ItemTemplate>
						    </asp:TemplateField>
						    <asp:TemplateField HeaderText="実行者" SortExpression="REGIST_ADMIN_ID">
							    <ItemStyle HorizontalAlign="left"/>
								<ItemTemplate>
									<%# DataBinder.Eval(Container.DataItem, "REGIST_ADMIN_ID")%>
							    </ItemTemplate>
						    </asp:TemplateField>							    
						    
						    <asp:TemplateField HeaderText="ﾒｰﾙﾃﾝﾌﾟﾚｰﾄ" SortExpression="MAIL_TEMPLATE_NO">
							    <ItemStyle HorizontalAlign="left"/>
								<ItemTemplate>
									<%# DataBinder.Eval(Container.DataItem, "MAIL_TEMPLATE_NO")%>
									:
									<%# DataBinder.Eval(Container.DataItem, "TEMPLATE_NM")%>
							    </ItemTemplate>
						    </asp:TemplateField>		
						    <asp:TemplateField HeaderText="送信種別" SortExpression="MAIL_SEND_TYPE">
							    <ItemStyle HorizontalAlign="left"/>
								<ItemTemplate>
									<%# DataBinder.Eval(Container.DataItem, "MAIL_SEND_TYPE_NM")%>
							    </ItemTemplate>
						    </asp:TemplateField>	
						    <asp:TemplateField HeaderText="送信対象" SortExpression="RX_SEX_CD">
							    <ItemStyle HorizontalAlign="center"/>
								<ItemTemplate>
									<%# DataBinder.Eval(Container.DataItem, "RX_SEX_CD").Equals(ViCommConst.MAN)? "会員" : "出演者"%>
							    </ItemTemplate>
						    </asp:TemplateField>							    							    						    				    						    							    
					    </Columns>
					    <PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>				
			</fieldset>
		</asp:Panel>
	</div>
	
	<%-- ============================== --%>
	<%--  Data Source                   --%>
	<%-- ============================== --%>
	<asp:ObjectDataSource ID="dsSite"			runat="server" TypeName="Site"					SelectMethod="GetNewVerList" ></asp:ObjectDataSource>	
	<asp:ObjectDataSource ID="dsBulkProcStatus" runat="server" TypeName="CodeDtl"				SelectMethod="GetList" >
		<SelectParameters>
			<asp:Parameter Name="pCodeType" Type="String" DefaultValue="30" />
		</SelectParameters>
	</asp:ObjectDataSource>	
	<asp:ObjectDataSource ID="dsData"			runat="server" TypeName="AdminMailTxHistory"	SelectMethod="GetPageCollection"  SelectCountMethod="GetPageCount" 
		EnablePaging="True"	OnSelecting="dsData_Selecting" OnSelected="dsData_Selected">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd"		Type="String" />
			<asp:Parameter Name="pProcStatus"	Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	
</asp:Content>

