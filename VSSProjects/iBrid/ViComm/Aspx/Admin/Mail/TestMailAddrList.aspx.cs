﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: テスト送信先設定--	Progaram ID		: TestMailAddrList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;

public partial class Mail_TestMailAddrList:System.Web.UI.Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		grdTestMailAddr.PageSize = 999;
		lstSiteCd.SelectedIndex = 0;
		txtLoginId.Text = "";
		lblHandleNm.Text = "";
		txtCastLoginId.Text = "";
		txtManLoginId.Text = "";
		ClearField();
		pnlMainte.Visible = false;
		lstSeekSiteCd.DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		DataBind();
	}

	private void ClearField() {
		txtTestEmailAddr.Text = "";
		recCount = "0";
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		if (IsValid) {
			GetData();
		}
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
		}
	}

	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(1);
		}
	}

	protected void btnUpdateFrom_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateFromData();
		}
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		pnlMainte.Visible = true;
		pnlKey.Enabled = true;
		pnlDtl.Visible = false;
		lstSiteCd.SelectedIndex = lstSeekSiteCd.SelectedIndex;
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void lnkTestMailAddr_Command(object sender,CommandEventArgs e) {
		string sKey = e.CommandArgument.ToString();
		int idx = sKey.IndexOf(":");
		if (idx >= 0) {
			lstSiteCd.SelectedValue = sKey.Substring(0,idx);
			txtLoginId.Text = sKey.Substring(idx + 1,sKey.Length - idx - 1);
			GetData();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}


	private void GetList() {
		grdTestMailAddr.DataBind();
		pnlCount.DataBind();
	}

	private void GetData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("TEST_MAIL_ADDR_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PLOGIN_ID",DbSession.DbType.VARCHAR2,txtLoginId.Text);
			db.ProcedureOutParm("PTEST_EMAIL_ADDR",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();

			ViewState["REVISION_NO"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID"] = db.GetStringValue("PROWID");

			txtTestEmailAddr.Text = db.GetStringValue("PTEST_EMAIL_ADDR");
		}
		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		pnlKey.Enabled = false;

	}

	private void UpdateData(int pDelFlag) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("TEST_MAIL_ADDR_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PLOGIN_ID",DbSession.DbType.VARCHAR2,txtLoginId.Text);
			db.ProcedureInParm("PTEST_EMAIL_ADDR",DbSession.DbType.VARCHAR2,txtTestEmailAddr.Text);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO"].ToString()));
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		int iIdx = lstSiteCd.SelectedIndex;
		InitPage();
		lstSeekSiteCd.SelectedIndex = iIdx;
		GetList();
	}

	private void UpdateFromData() {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("TEST_MAIL_FROM_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PMAN_LOGIN_ID",DbSession.DbType.VARCHAR2,txtManLoginId.Text);
			db.ProcedureInParm("PCAST_LOGIN_ID",DbSession.DbType.VARCHAR2,txtCastLoginId.Text);
			db.ProcedureInParm("PROWID",DbSession.DbType.VARCHAR2,ViewState["ROWID_FROM"].ToString());
			db.ProcedureInParm("PREVISION_NO",DbSession.DbType.NUMBER,decimal.Parse(ViewState["REVISION_NO_FROM"].ToString()));
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		int iIdx = lstSiteCd.SelectedIndex;
		InitPage();
		lstSeekSiteCd.SelectedIndex = iIdx;
		GetList();
	}

	protected void dsTestMailAddr_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("TEST_MAIL_FROM_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureOutParm("PMAN_LOGIN_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMAN_HANDLE_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_LOGIN_ID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_HANDLE_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PROWID",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			ViewState["REVISION_NO_FROM"] = db.GetStringValue("PREVISION_NO");
			ViewState["ROWID_FROM"] = db.GetStringValue("PROWID");
			txtCastLoginId.Text = db.GetStringValue("PCAST_LOGIN_ID");
			lblCastHandleNm.Text = db.GetStringValue("PCAST_HANDLE_NM");
			txtManLoginId.Text = db.GetStringValue("PMAN_LOGIN_ID");
			lblManHandleNm.Text = db.GetStringValue("PMAN_HANDLE_NM");
		}

		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
	}

	protected void dsTestMailAddr_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void vdcLoginId_ServerValidate(object source,ServerValidateEventArgs args) {
		lblHandleNm.Text = "";
		using (CastCharacter oCastCharacter = new CastCharacter()) {
			args.IsValid = oCastCharacter.IsExistLoginId(lstSiteCd.SelectedValue,txtLoginId.Text,"");
			if (args.IsValid) {
				lblHandleNm.Text = oCastCharacter.handleNm;
			} else {
				using (UserManCharacter oManCharacter = new UserManCharacter()) {
					args.IsValid = oManCharacter.IsExistLoginId(lstSiteCd.SelectedValue,txtLoginId.Text,"");
					if (args.IsValid) {
						lblHandleNm.Text = oManCharacter.handleNm;
					}
				}
			}
		}
	}

	protected void vdcManLoginId_ServerValidate(object source,ServerValidateEventArgs args) {
		lblManHandleNm.Text = "";
		using (UserManCharacter oManCharacter = new UserManCharacter()) {
			args.IsValid = oManCharacter.IsExistLoginId(lstSiteCd.SelectedValue,txtManLoginId.Text,"");
			if (args.IsValid) {
				lblManHandleNm.Text = oManCharacter.handleNm;
			}
		}
	}

	protected void vdcCastLoginId_ServerValidate(object source,ServerValidateEventArgs args) {
		lblCastHandleNm.Text = "";
		using (CastCharacter oCastCharacter = new CastCharacter()) {
			args.IsValid = oCastCharacter.IsExistLoginId(lstSiteCd.SelectedValue,txtCastLoginId.Text,"");
			if (args.IsValid) {
				lblCastHandleNm.Text = oCastCharacter.handleNm;
			}
		}
	}
}
