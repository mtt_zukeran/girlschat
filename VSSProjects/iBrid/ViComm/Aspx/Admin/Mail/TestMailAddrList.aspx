<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TestMailAddrList.aspx.cs" Inherits="Mail_TestMailAddrList"
	Title="テストメール送信先・送信元設定" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="テストメール送信先・送信元設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									サイトコード
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									ログインＩＤ
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtLoginId" runat="server" Width="70px" MaxLength="8"></asp:TextBox>
									<asp:Label ID="lblHandleNm" runat="server" Text=""></asp:Label>
									<asp:RequiredFieldValidator ID="vdrLoginId" runat="server" ErrorMessage="ログインＩＤを入力して下さい。" ControlToValidate="txtLoginId" ValidationGroup="Key">*</asp:RequiredFieldValidator>
									<asp:CustomValidator ID="vdcLoginId" runat="server" ControlToValidate="txtLoginId" ErrorMessage="このログインＩＤは存在しません。" OnServerValidate="vdcLoginId_ServerValidate"
										ValidationGroup="Key">*</asp:CustomValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
					</fieldset>
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlDtl">
					<fieldset class="fieldset-inner">
						<legend>[送信先会員]</legend>
						<table border="0" style="width: 640px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									送信先メールアドレス
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtTestEmailAddr" runat="server" MaxLength="60" Width="200px"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrTestEmailAddr" runat="server" ErrorMessage="送信先メールアドレスを入力して下さい。" ControlToValidate="txtTestEmailAddr" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeTestEmailAddr" runat="server" ControlToValidate="txtTestEmailAddr" Display="Dynamic" ErrorMessage="送信先メールアドレスを正しく入力して下さい。"
										ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
						<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" OnClick="btnDelete_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnCancel_Click" CausesValidation="False" />
					</fieldset>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<fieldset>
			<legend>[送信先一覧]</legend>
			<table border="0" style="width: 640px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle2">
						サイトコード
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
						</asp:DropDownList>
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnRegist" Text="送信先追加" CssClass="seekbutton" CausesValidation="False" OnClick="btnRegist_Click" />
			<br />
			<br />
			<asp:GridView ID="grdTestMailAddr" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsTestMailAddr" AllowSorting="True" SkinID="GridViewColor">
				<Columns>
					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="lnkTestMailAddr" runat="server" Text='<%# Eval("LOGIN_ID") %>' CommandArgument='<%# string.Format("{0}:{1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>'
								OnCommand="lnkTestMailAddr_Command" CausesValidation="False">
							</asp:LinkButton>
						</ItemTemplate>
						<HeaderTemplate>
							ログインＩＤ
						</HeaderTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:BoundField DataField="TEST_EMAIL_ADDR" HeaderText="送信先ｱﾄﾞﾚｽ"></asp:BoundField>
					<asp:BoundField DataField="SEX_NM" HeaderText="性別"></asp:BoundField>
					<asp:TemplateField HeaderText="ﾊﾝﾄﾞﾙ名">
						<ItemTemplate>
							<asp:Label runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>' ID="lblHandleNm" />
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br>
				<a class="reccount">Current viewing page
					<%=grdTestMailAddr.PageIndex + 1%>
					of
					<%=grdTestMailAddr.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<fieldset class="fieldset">
			<legend>[送信元会員]</legend>
			<table border="0" style="width: 640px" class="tableStyle">
				<asp:PlaceHolder ID="plcNoUse" runat="server" Visible="false">
					<tr>
						<td class="tdHeaderStyle2">
							送信元男性会員ﾛｸﾞｲﾝID
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtManLoginId" runat="server" Width="70px" MaxLength="8"></asp:TextBox>
							<asp:Label ID="lblManHandleNm" runat="server" Text=""></asp:Label>
							<asp:CustomValidator ID="vdcManLoginId" runat="server" ControlToValidate="txtManLoginId" OnServerValidate="vdcManLoginId_ServerValidate" ValidationGroup="DetailFrom">このログインＩＤは存在しません。</asp:CustomValidator>
						</td>
					</tr>
				</asp:PlaceHolder>
				<tr>
					<td class="tdHeaderStyle2">
						送信元出演者ﾛｸﾞｲﾝID
					</td>
					<td class="tdDataStyle">
						<asp:TextBox ID="txtCastLoginId" runat="server" Width="70px" MaxLength="8"></asp:TextBox>
						<asp:Label ID="lblCastHandleNm" runat="server" Text=""></asp:Label>
						<asp:CustomValidator ID="vdcCastLoginId" runat="server" ControlToValidate="txtCastLoginId" OnServerValidate="vdcCastLoginId_ServerValidate" ValidationGroup="DetailFrom">このログインＩＤは存在しません。</asp:CustomValidator>
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnUpdateFrom" Text="更新" CssClass="seekbutton" OnClick="btnUpdateFrom_Click" ValidationGroup="DetailFrom" />
		</fieldset>
	</div>
	<asp:ObjectDataSource ID="dsTestMailAddr" runat="server" SelectMethod="GetPageCollection" TypeName="TestMailAddr" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsTestMailAddr_Selected" OnSelecting="dsTestMailAddr_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender3" runat="server" TargetControlID="btnUpdateFrom" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrLoginId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdcLoginId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdrTestEmailAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdeTestEmailAddr" HighlightCssClass="validatorCallout" />
</asp:Content>
