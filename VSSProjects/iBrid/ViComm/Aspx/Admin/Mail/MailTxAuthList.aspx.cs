﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メール未認証一覧
--	Progaram ID		: MailTxAuthList
--
--  Creation Date	: 2011.08.05
--  Creater			: iBrid(R.Suzuki)
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater		Update Explain
-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Mail_MailTxAuthList:System.Web.UI.Page {

	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			FisrtLoad();
			InitPage();
			GetList();
		}
	}

	private void FisrtLoad() {
		grdMail.PageSize = int.Parse(Session["PageSize"].ToString());
		grdMail.DataSourceID = "";
		DataBind();
		if (Session["SiteCd"].ToString().Equals("")) {
			lstSiteCd.Items.Insert(0,new ListItem("",""));
		}
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		lstSiteCd.DataSourceID = "";
	}

	private void InitPage() {
		pnlInfo.Visible = false;
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	private void GetList() {
		grdMail.PageSize = 100;
		grdMail.AllowSorting = false;

		grdMail.DataSourceID = "dsReqTxMail";
		DataBind();
		pnlInfo.Visible = true;
	}

	protected string GetMailDoc(object pCastOriginalDoc1,object pCastOriginalDoc2,object pCastOriginalDoc3,object pCastOriginalDoc4,object pCastOriginalDoc5) {
		return pCastOriginalDoc1.ToString() + pCastOriginalDoc2.ToString() + pCastOriginalDoc3.ToString() + pCastOriginalDoc4.ToString() + pCastOriginalDoc5.ToString();
	}

	protected string GetViewUrl(object pSexCd,object pSiteCd,object pLoignId) {
		if (pSexCd.Equals(ViCommConst.MAN)) {
			return string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",pSiteCd.ToString(),pLoignId.ToString());
		} else {
			return string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}",pLoignId.ToString(),"MailInquiry.aspx");
		}
	}

	protected void dsReqTxMail_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
	}

	protected void dsReqTxMail_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected string GenerateOpenPicScript(object pDataItem) {
		DataRowView oDataRowView = (DataRowView)pDataItem;

		string sSiteCd = this.lstSiteCd.SelectedValue;
		string sTxLoginId = iBridUtil.GetStringValue(oDataRowView["TX_LOGIN_ID"]);
		string sPicSeq = iBridUtil.GetStringValue(oDataRowView["PIC_SEQ"]);
		string sTxSexCd = iBridUtil.GetStringValue(oDataRowView["TX_SEX_CD"]);

		string sImgPath = string.Empty;

		if (sTxSexCd.Equals(ViCommConst.MAN)) {
			sImgPath = string.Format("data/{0}/Man/{1}.jpg",sSiteCd,iBridUtil.addZero(sPicSeq,ViCommConst.OBJECT_NM_LENGTH));
		} else {
			sImgPath = string.Format("data/{0}/Operator/{1}/{2}.jpg",sSiteCd,sTxLoginId,iBridUtil.addZero(sPicSeq,ViCommConst.OBJECT_NM_LENGTH));
		}

		return string.Format("javascript:openPicViewer('{0}');",sImgPath);
	}
	protected string GenerateOpenMovieScript(object pDataItem) {
		DataRowView oDataRowView = (DataRowView)pDataItem;

		string sSiteCd = this.lstSiteCd.SelectedValue;
		string sTxLoginId = iBridUtil.GetStringValue(oDataRowView["TX_LOGIN_ID"]);
		string sMovieSeq = iBridUtil.GetStringValue(oDataRowView["MOVIE_SEQ"]);
		string sTxSexCd = iBridUtil.GetStringValue(oDataRowView["TX_SEX_CD"]);

		string sImgPath = string.Empty;

		if (sTxSexCd.Equals(ViCommConst.MAN)) {
			sImgPath = string.Format("~/movie/{0}/Man/{1}.3gp",sSiteCd,iBridUtil.addZero(sMovieSeq,ViCommConst.OBJECT_NM_LENGTH));
		} else {
			sImgPath = string.Format("~/movie/{0}/Operator/{1}/{2}.3gp",sSiteCd,sTxLoginId,iBridUtil.addZero(sMovieSeq,ViCommConst.OBJECT_NM_LENGTH));
		}

		return string.Format("javascript:openMovieViewer('{0}');",this.ResolveUrl(sImgPath));
	}

	protected string GenerateSmallPicPath(object pDataItem) {
		DataRowView oDataRowView = (DataRowView)pDataItem;

		string sSiteCd = this.lstSiteCd.SelectedValue;
		string sTxLoginId = iBridUtil.GetStringValue(oDataRowView["TX_LOGIN_ID"]);
		string sPicSeq = iBridUtil.GetStringValue(oDataRowView["PIC_SEQ"]);
		string sTxSexCd = iBridUtil.GetStringValue(oDataRowView["TX_SEX_CD"]);

		string sImgPath = string.Empty;

		if (sTxSexCd.Equals(ViCommConst.MAN)) {
			sImgPath = string.Format("../data/{0}/Man/{1}_s.jpg",sSiteCd,iBridUtil.addZero(sPicSeq,ViCommConst.OBJECT_NM_LENGTH));
		} else {
			sImgPath = string.Format("../data/{0}/Operator/{1}/{2}_s.jpg",sSiteCd,sTxLoginId,iBridUtil.addZero(sPicSeq,ViCommConst.OBJECT_NM_LENGTH));
		}

		return sImgPath;
	}

	protected string GenerateSmallPicPathMovie(object pDataItem) {
		DataRowView oDataRowView = (DataRowView)pDataItem;

		string sSiteCd = this.lstSiteCd.SelectedValue;
		string sTxLoginId = iBridUtil.GetStringValue(oDataRowView["TX_LOGIN_ID"]);
		string sPicSeq = iBridUtil.GetStringValue(oDataRowView["MOVIE_SEQ"]);
		string sTxSexCd = iBridUtil.GetStringValue(oDataRowView["TX_SEX_CD"]);

		string sImgPath = string.Empty;

		if (sTxSexCd.Equals(ViCommConst.MAN)) {
			sImgPath = string.Format("../data/{0}/Man/{1}.jpg",sSiteCd,iBridUtil.addZero(sPicSeq,ViCommConst.OBJECT_NM_LENGTH));
		} else {
			sImgPath = string.Format("../data/{0}/Operator/{1}/{2}.jpg",sSiteCd,sTxLoginId,iBridUtil.addZero(sPicSeq,ViCommConst.OBJECT_NM_LENGTH));
		}

		return sImgPath;
	}

	protected void btnUpdate_OnClick(object sender,EventArgs e) {
		string sSiteCd = (grdMail.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][0]).ToString();
		string sRequestTxMailSeq = (grdMail.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][1]).ToString();
		if (AuthAttachedObjMail(sRequestTxMailSeq,0)) {
			Server.Transfer(string.Format("MailTxAuthList.aspx?sitecd={0}",sSiteCd));
		} else {
			((Button)sender).Text = "PTが足りません";
			((Button)sender).Enabled = false;
		}
	}

	protected void btnDelete_OnClick(object sender,EventArgs e) {
		string sSiteCd = (grdMail.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][0]).ToString();
		string sRequestTxMailSeq = (grdMail.DataKeys[int.Parse(((Button)sender).CommandArgument.ToString())][1]).ToString();
		AuthAttachedObjMail(sRequestTxMailSeq,1);

		Server.Transfer(string.Format("MailTxAuthList.aspx?sitecd={0}",sSiteCd));
	}

	private bool AuthAttachedObjMail(string pRequestTxMailSeq,int pDeleteFlag) {
		bool bHavePoint = true;

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("AUTH_ATTACHED_OBJ_MAIL");
			db.ProcedureInParm("pREQUEST_TX_MAIL_SEQ",DbSession.DbType.VARCHAR2,pRequestTxMailSeq);
			db.ProcedureInParm("pDEL_FLAG",DbSession.DbType.NUMBER,pDeleteFlag);
			db.ProcedureOutParm("pNOT_HAVE_POINT_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();

			if (db.GetStringValue("pNOT_HAVE_POINT_FLAG").Equals(ViCommConst.FLAG_ON_STR)) {
				bHavePoint = false;
			}
		}
		return bHavePoint;
	}
}
