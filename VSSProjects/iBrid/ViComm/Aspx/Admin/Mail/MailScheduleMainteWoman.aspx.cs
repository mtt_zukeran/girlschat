﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者メール配信スケジュール
--	Progaram ID		: MailScheduleMainteWoman
--
--  Creation Date	: 2010.05.21
--  Creater			: Nakano
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using iBridCommLib;
using ViComm;

public partial class Mail_MailScheduleMainteWoman:Page {
	private string RecCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();

			SetupOnlineStatus();
			SetupProductionSeq();
			SetupManagerSeq();
			SetupConnectType();
			SetupUseTerminalType();
			SetupCategory();
			tagNA_FLAG.DataBind();
		}
	}

	private void InitPage() {
		ClearField();
		pnlPattern.Visible = false;
		btnDelete.Visible = false;
		grdPattern.PageSize = 100;
		grdPattern.DataBind();

		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);
	}

	private void ClearField() {
		for (int i = 0;i < pnlPattern.Controls.Count;i++) {
			if (pnlPattern.Controls[i] is TextBox) {
				((TextBox)pnlPattern.Controls[i]).Text = "";
			} else if (pnlPattern.Controls[i] is CheckBox) {
				((CheckBox)pnlPattern.Controls[i]).Checked = false;
			} else if (pnlPattern.Controls[i] is HiddenField) {
				((HiddenField)pnlPattern.Controls[i]).Value = "";
			} else if (pnlPattern.Controls[i] is Label) {
				if (((Label)pnlPattern.Controls[i]).ForeColor != Color.Red) {
					((Label)pnlPattern.Controls[i]).Text = "";
				}
			} else if (pnlPattern.Controls[i] is PlaceHolder) {
				for (int iPlh = 0;iPlh < pnlPattern.Controls[i].Controls.Count;iPlh++) {
					if (pnlPattern.Controls[i].Controls[iPlh] is CheckBox) {
						((CheckBox)pnlPattern.Controls[i].Controls[iPlh]).Checked = false;
					} else if (pnlPattern.Controls[i].Controls[iPlh] is TextBox) {
						((TextBox)pnlPattern.Controls[i].Controls[iPlh]).Text = "";
					} else if (pnlPattern.Controls[i].Controls[iPlh] is Label) {
						((Label)pnlPattern.Controls[i].Controls[iPlh]).Text = "";
					}
				}
			}
		}

		rdoMailOK.Checked = true;
		rdoMailNG.Checked = false;
		
		rdoMailServerBeam.Checked = true;
		rdoMailServerMuller3.Checked = false;

		lstTaskHour.Items.Clear();
		tagNA_FLAG.SelectedIndex = -1;
		for (int i = 0;i < 24;i++) {
			lstTaskHour.Items.Add(new ListItem(i.ToString("00"),i.ToString("00")));
		}
		lstTaskMin.Items.Clear();
		lstTaskMin.Items.Add(new ListItem("00","00"));
		lstTaskMin.Items.Add(new ListItem("15","15"));
		lstTaskMin.Items.Add(new ListItem("30","30"));
		lstTaskMin.Items.Add(new ListItem("45","45"));

		rdoMailEnable.Checked = true;
		rdoMailNoEnable.Checked = false;
		lblTaskWeek.Visible = false;
		RecCount = "0";
	}

	protected void grdPattern_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowIndex >= 0) {
			if (e.Row.Cells[2].Text.Equals("1")) {
				e.Row.Cells[2].Text = "有効";
				e.Row.BackColor = Color.FromArgb(0xFF,0xFF,0xFF);

				e.Row.Cells[8].BackColor = Color.FromArgb(0xCC,0xCC,0xFF);
				e.Row.Cells[9].BackColor = Color.FromArgb(0xFF,0xCC,0xCC);
			} else {
				e.Row.Cells[2].Text = "";
				e.Row.BackColor = Color.FromArgb(0xE0,0xE0,0xE0);
			}
			for (int iCol = grdPattern.Columns.Count - 8;iCol < grdPattern.Columns.Count - 1;iCol++) {
				if (e.Row.Cells[iCol].Text.Equals("1")) {
					e.Row.Cells[iCol].Text = "配信";
				} else {
					e.Row.Cells[iCol].Text = "";
				}
			}
		}
	}

	protected void lnkPatternNm_Command(object sender,CommandEventArgs e) {
		InitPage();
		hdfPatternSeq.Value = e.CommandArgument.ToString();
		btnDelete.Visible = true;

		SetupOnlineStatus();
		SetupProductionSeq();
		SetupManagerSeq();
		SetupConnectType();
		SetupUseTerminalType();
		SetupCategory();

		GetData();
	}

	protected string GetRecCount() {
		return RecCount;
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		hdfPatternSeq.Value = "";
		tagSITE_CD.DataBind();
		if (Session["MENU_SITE"] != null) {
			tagSITE_CD.SelectedValue = Session["MENU_SITE"].ToString();
		} else {
			tagSITE_CD.SelectedIndex = 0;
		}

		SetupOnlineStatus();
		SetupProductionSeq();
		SetupManagerSeq();
		SetupConnectType();
		SetupUseTerminalType();
		SetupCategory();

		SetupUserRank();
		SetupMailTemplate();
		pnlPattern.Visible = true;
		btnDelete.Visible = false;
	}

	protected void dsSchedulePattern_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = ViCommConst.OPERATOR;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (sender == btnUpdate) {
			if (IsValid == false) {
				return;
			}
			if (!TaskWeekValidate()) {
				lblTaskWeek.Visible = true;
				return;
			}

			SetFromToData();
			UpdateData(0);
		} else {
			UpdateData(1);
		}
	}

	private void UpdateData(int pDelFlag) {

		ArrayList alSearchName = new ArrayList();
		ArrayList alSearchValue = new ArrayList();

		if (pDelFlag == 0) {
			for (int i = 0;i < pnlPattern.Controls.Count;i++) {
				if ((pnlPattern.Controls[i].ID == null) || (pnlPattern.Controls[i].ID.IndexOf("tag") != 0)) {
					continue;
				}

				if ((pnlPattern.Controls[i] is TextBox) && (((TextBox)pnlPattern.Controls[i]).Text.Length > 0)) {
					alSearchName.Add(pnlPattern.Controls[i].ID.Substring(3));
					alSearchValue.Add(((TextBox)pnlPattern.Controls[i]).Text);
				} else if ((pnlPattern.Controls[i] is DropDownList) && (((DropDownList)pnlPattern.Controls[i]).SelectedValue.Length > 0)) {
					alSearchName.Add(pnlPattern.Controls[i].ID.Substring(3));
					alSearchValue.Add(((DropDownList)pnlPattern.Controls[i]).SelectedValue);
				} else if ((pnlPattern.Controls[i] is CheckBox) && (((CheckBox)pnlPattern.Controls[i]).Checked)) {
					alSearchName.Add(pnlPattern.Controls[i].ID.Substring(3));
					alSearchValue.Add("1");
				} else if (pnlPattern.Controls[i] is PlaceHolder) {
					int iData = 0;
					for (int iPlh = 0;iPlh < pnlPattern.Controls[i].Controls.Count;iPlh++) {
						if (pnlPattern.Controls[i].Controls[iPlh] is RadioButton) {
							if ((((RadioButton)pnlPattern.Controls[i].Controls[iPlh]).Checked) &&
								 (CnvStrConditon(pnlPattern.Controls[i].Controls[iPlh].ID).Length > 0)) {
								alSearchName.Add(pnlPattern.Controls[i].ID.Substring(3));
								alSearchValue.Add(CnvStrConditon(pnlPattern.Controls[i].Controls[iPlh].ID));
							}
						}
					}
					if (iData > 0) {
						alSearchName.Add(pnlPattern.Controls[i].ID.Substring(3));
						alSearchValue.Add(iData.ToString());
					}
				}
			}

			string sNaFlags = string.Empty;
			foreach (ListItem oListItem in tagNA_FLAG.Items) {
				if (!oListItem.Selected)
					continue;

				if (!sNaFlags.Equals(string.Empty)) {
					sNaFlags += ",";
				}
				sNaFlags += oListItem.Value;
			}
			if (!sNaFlags.Equals(string.Empty)) {
				alSearchName.Add("NA_FLAG");
				alSearchValue.Add(sNaFlags);
			}
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SCHEDULE_PATTERN_MAINTE");
			db.ProcedureInParm("PPATTERN_SEQ",DbSession.DbType.NUMBER,hdfPatternSeq.Value);
			db.ProcedureInParm("PPATTERN_NAME",DbSession.DbType.VARCHAR2,txtPatternName.Text);
			db.ProcedureInParm("PPATTERN_FLG",DbSession.DbType.NUMBER,rdoMailEnable.Checked);
			db.ProcedureInParm("PTX_TIME",DbSession.DbType.VARCHAR2,string.Format("{0}:{1}:00",lstTaskHour.SelectedValue,lstTaskMin.SelectedValue));
			db.ProcedureInParm("PTX_WEEK1",DbSession.DbType.NUMBER,chkWeek1.Checked);
			db.ProcedureInParm("PTX_WEEK2",DbSession.DbType.NUMBER,chkWeek2.Checked);
			db.ProcedureInParm("PTX_WEEK3",DbSession.DbType.NUMBER,chkWeek3.Checked);
			db.ProcedureInParm("PTX_WEEK4",DbSession.DbType.NUMBER,chkWeek4.Checked);
			db.ProcedureInParm("PTX_WEEK5",DbSession.DbType.NUMBER,chkWeek5.Checked);
			db.ProcedureInParm("PTX_WEEK6",DbSession.DbType.NUMBER,chkWeek6.Checked);
			db.ProcedureInParm("PTX_WEEK7",DbSession.DbType.NUMBER,chkWeek7.Checked);
			db.ProcedureInArrayParm("PPATTERN_SEARCH_NAME",DbSession.DbType.VARCHAR2,alSearchName.Count,(string[])alSearchName.ToArray(typeof(string)));
			db.ProcedureInArrayParm("PPATTERN_SEARCH_VALUE",DbSession.DbType.VARCHAR2,alSearchValue.Count,(string[])alSearchValue.ToArray(typeof(string)));
			db.ProcedureInParm("PSEX_CD",DbSession.DbType.VARCHAR2,ViCommConst.OPERATOR);
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		InitPage();
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void tagSITE_CD_SelectedIndexChanged(object sender,EventArgs e) {
		SetupUserRank();
		SetupMailTemplate();
		tagMAIL_TEMPLATE_NO_SelectedIndexChanged(sender,e);
	}

	private void GetData() {
		tagSITE_CD.DataBind();
		if (Session["MENU_SITE"] != null) {
			tagSITE_CD.SelectedValue = Session["MENU_SITE"].ToString();
		} else {
			tagSITE_CD.SelectedIndex = 0;
		}

		ArrayList alID = new ArrayList();
		ArrayList alValue = new ArrayList();

		using (SchedulePattern oPattern = new SchedulePattern()) {
			using (DataSet ds = oPattern.GetOne(hdfPatternSeq.Value)) {
				foreach (DataRow dr in ds.Tables[0].Rows) {
					txtPatternName.Text = iBridUtil.GetStringValue(dr["PATTERN_NAME"]);
					if (iBridUtil.GetStringValue(dr["PATTERN_FLG"]).Equals("1")) {
						rdoMailEnable.Checked = true;
					} else {
						rdoMailNoEnable.Checked = true;
					}
					if (iBridUtil.GetStringValue(dr["TX_TIME"]).Length >= 8) {
						if (lstTaskHour.Items.IndexOf(new ListItem(iBridUtil.GetStringValue(dr["TX_TIME"]).Substring(0,2))) >= 0) {
							lstTaskHour.Text = iBridUtil.GetStringValue(dr["TX_TIME"]).Substring(0,2);
						}
						if (lstTaskMin.Items.IndexOf(new ListItem(iBridUtil.GetStringValue(dr["TX_TIME"]).Substring(3,2))) >= 0) {
							lstTaskMin.SelectedValue = iBridUtil.GetStringValue(dr["TX_TIME"]).Substring(3,2);
						}
					}
					for (int iCnt = 1;iCnt <= 7;iCnt++) {
						if (iBridUtil.GetStringValue(dr["TX_WEEK" + iCnt.ToString()]).Equals("1")) {
							CheckBox chkOk = (CheckBox)plcWeek.FindControl(string.Format("chkWeek{0}",iCnt)) as CheckBox;
							chkOk.Checked = true;
						}
					}
				}
			}
			using (DataSet ds = oPattern.GetListCnd(hdfPatternSeq.Value)) {
				foreach (DataRow dr in ds.Tables[0].Rows) {
					object ctlData = pnlPattern.FindControl("tag" + iBridUtil.GetStringValue(dr["PATTERN_SEARCH_NAME"]));
					if ((ctlData != null)) {
						if (ctlData is TextBox) {
							((TextBox)ctlData).Text = iBridUtil.GetStringValue(dr["PATTERN_SEARCH_VALUE"]);
						} else if (ctlData is DropDownList) {
							if (((DropDownList)ctlData).ID == "tagSITE_CD") {
								((DropDownList)ctlData).SelectedValue = iBridUtil.GetStringValue(dr["PATTERN_SEARCH_VALUE"]);
								SetupUserRank();
								SetupMailTemplate();
							} else {
								alID.Add(((DropDownList)ctlData).ID);
								alValue.Add(iBridUtil.GetStringValue(dr["PATTERN_SEARCH_VALUE"]));
							}
						} else if (ctlData is CheckBox) {
							if (iBridUtil.GetStringValue(dr["PATTERN_SEARCH_VALUE"]).Equals("1")) {
								((CheckBox)ctlData).Checked = true;
							}
						} else if (ctlData is PlaceHolder) {
							int iCondData = 0;
							if (SysPrograms.IsNumber(iBridUtil.GetStringValue(dr["PATTERN_SEARCH_VALUE"]))) {
								iCondData = int.Parse(iBridUtil.GetStringValue(dr["PATTERN_SEARCH_VALUE"]));
							}
							for (int iPlh = 0;iPlh < ((PlaceHolder)ctlData).Controls.Count;iPlh++) {
								if (((PlaceHolder)ctlData).Controls[iPlh] is RadioButton) {
									if (CnvStrConditon(((PlaceHolder)ctlData).Controls[iPlh].ID) == iBridUtil.GetStringValue(dr["PATTERN_SEARCH_VALUE"])) {
										((RadioButton)((PlaceHolder)ctlData).Controls[iPlh]).Checked = true;
									}
								}
							}
						} else if (ctlData is CheckBoxList) {
							if (((CheckBoxList)ctlData).ID == "tagNA_FLAG") {
								foreach (string sValue in iBridUtil.GetStringValue(dr["PATTERN_SEARCH_VALUE"]).Split(',')) {
									foreach (ListItem oListItem in ((CheckBoxList)ctlData).Items) {
										if (oListItem.Value.Equals(sValue)) {
											oListItem.Selected = true;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		for (int i = 0;i < alID.Count;i++) {
			object ctlData = pnlPattern.FindControl(alID[i].ToString());

			if ((ctlData != null) && (ctlData is DropDownList)) {
				if (((DropDownList)ctlData).Items.FindByValue(alValue[i].ToString()) != null) {
					((DropDownList)ctlData).SelectedIndex = ((DropDownList)ctlData).Items.IndexOf(((DropDownList)ctlData).Items.FindByValue(alValue[i].ToString()));
				}
			}
		}

		pnlPattern.Visible = true;
	}

	private void SetupUserRank() {
		DataSet ds;
		tagUSER_RANK.Items.Clear();
		tagUSER_RANK.Items.Add(new ListItem("",""));
		using (CastRank oCastRank = new CastRank()) {
			ds = oCastRank.GetList();
			foreach (DataRow dr in ds.Tables[0].Rows) {
				tagUSER_RANK.Items.Add(new ListItem(iBridUtil.GetStringValue(dr["USER_RANK_NM"]),iBridUtil.GetStringValue(dr["USER_RANK"])));
			}
		}
	}

	private void SetupMailTemplate() {
		DataSet ds;
		tagMAIL_TEMPLATE_NO.Items.Clear();
		using (MailTemplate oMail = new MailTemplate()) {
			ds = oMail.GetListByTemplateType(tagSITE_CD.SelectedValue,ViCommConst.MAIL_TP_CAST_MAIL_MAGAZINE,string.Empty);
			foreach (DataRow dr in ds.Tables[0].Rows) {
				tagMAIL_TEMPLATE_NO.Items.Add(new ListItem(iBridUtil.GetStringValue(dr["TEMPLATE_NM"]),iBridUtil.GetStringValue(dr["MAIL_TEMPLATE_NO"])));
			}
			hdfMailInfoCnt.Value = tagMAIL_TEMPLATE_NO.Items.Count.ToString();

			ds = oMail.GetListByTemplateType(tagSITE_CD.SelectedValue,ViCommConst.MAIL_TP_APPROACH_ADMIN_TO_USER,string.Empty);
			foreach (DataRow dr in ds.Tables[0].Rows) {
				tagMAIL_TEMPLATE_NO.Items.Add(new ListItem(iBridUtil.GetStringValue(dr["TEMPLATE_NM"]),iBridUtil.GetStringValue(dr["MAIL_TEMPLATE_NO"])));
			}
		}
	}

	protected void tagMAIL_TEMPLATE_NO_SelectedIndexChanged(object sender,EventArgs e) {
	}

	private string CnvStrConditon(string pID) {
		string sRet = "0";
		switch (pID) {
			case "rdoMailOK":
				sRet = "0";
				break;
			case "rdoMailNG":
				sRet = "1";
				break;
			case "rdoMailServerBeam":
				sRet = "1";
				break;
			case "rdoMailServerMuller3":
				sRet = "2";
				break;
		}
		return sRet;
	}

	private bool TaskWeekValidate() {
		return chkWeek1.Checked || chkWeek2.Checked || chkWeek3.Checked || chkWeek4.Checked || chkWeek5.Checked || chkWeek6.Checked || chkWeek7.Checked;
	}

	private void SetFromToData() {

		for (int i = 0;i < pnlPattern.Controls.Count;i++) {
			if ((pnlPattern.Controls[i].ID == null) || (pnlPattern.Controls[i].ID.IndexOf("tag") != 0)) {
				continue;
			}
			if ((pnlPattern.Controls[i] is TextBox) && pnlPattern.Controls[i].ID.IndexOf("_FROM") > 0) {
				object ctlData = pnlPattern.FindControl(pnlPattern.Controls[i].ID.Replace("_FROM","_TO"));
				if ((ctlData != null) && (ctlData is TextBox)) {

					if ((!((TextBox)pnlPattern.Controls[i]).Text.Equals("")) || (!((TextBox)ctlData).Text.Equals(""))) {
						if (((TextBox)pnlPattern.Controls[i]).Text.Equals("")) {
							((TextBox)pnlPattern.Controls[i]).Text = ((TextBox)ctlData).Text;
						} else if (((TextBox)ctlData).Text.Equals("")) {
							((TextBox)ctlData).Text = ((TextBox)pnlPattern.Controls[i]).Text;
						}
					}
				}
			}
		}

		if ((tagTOTAL_PAYMENT_AMT_FROM.Text.Length > 0) || (tagTOTAL_PAYMENT_AMT_TO.Text.Length > 0)) {
			if (tagTOTAL_PAYMENT_AMT_FROM.Text.Length == 0) {
				tagTOTAL_PAYMENT_AMT_FROM.Text = tagTOTAL_PAYMENT_AMT_TO.Text;
			} else if (tagTOTAL_PAYMENT_AMT_TO.Text.Length == 0) {
				tagTOTAL_PAYMENT_AMT_TO.Text = tagTOTAL_PAYMENT_AMT_FROM.Text;
			}
		}
	}

	private void SetupOnlineStatus() {
		tagCHARACTER_ONLINE_STATUS.DataBind();
		tagCHARACTER_ONLINE_STATUS.Items.Insert(0,new ListItem("",""));
	}

	private void SetupProductionSeq() {
		tagPRODUCTION_SEQ.DataBind();
		tagPRODUCTION_SEQ.Items.Insert(0,new ListItem("",""));
	}

	private void SetupManagerSeq() {
		tagMANAGER_SEQ.DataBind();
		if (tagMANAGER_SEQ.Items.Count > 0 && tagMANAGER_SEQ.Items[0].Value != "") {
			tagMANAGER_SEQ.Items.Insert(0,new ListItem("",""));
		}
	}

	private void SetupConnectType() {
		tagCONNECT_TYPE.DataBind();
		tagCONNECT_TYPE.Items.Insert(0,new ListItem("",""));
	}

	private void SetupUseTerminalType() {
		tagUSE_TERMINAL_TYPE.DataBind();
		tagUSE_TERMINAL_TYPE.Items.Insert(0,new ListItem("",""));
	}

	private void SetupCategory() {
		tagACT_CATEGORY_SEQ.DataBind();
		tagACT_CATEGORY_SEQ.Items.Insert(0,new ListItem("",""));
	}

	protected void dsActCategory_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = tagSITE_CD.SelectedValue;
	}

	protected void vdcAdCd_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid) {
			using (Ad oAd = new Ad()) {
				string sAdNm = "";
				args.IsValid = oAd.IsExist(tagAD_CD.Text,ref sAdNm);
				lblAdNm.Text = sAdNm;
			}
		}
	}

	protected void dsManager_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {

		if (tagPRODUCTION_SEQ.Items.Count == 0) {
			e.InputParameters[0] = "";
		} else if (tagPRODUCTION_SEQ.Items.Count > 0 && tagPRODUCTION_SEQ.SelectedIndex == 0 && !string.IsNullOrEmpty(tagPRODUCTION_SEQ.Items[0].Value)) {
			e.InputParameters[0] = "";
		} else {
			e.InputParameters[0] = tagPRODUCTION_SEQ.SelectedValue;
		}
	}

	protected void lstProductionSeq_SelectedIndexChanged(object sender,EventArgs e) {
		tagMANAGER_SEQ.DataSourceID = "dsManager";
		tagMANAGER_SEQ.DataBind();
		tagMANAGER_SEQ.Items.Insert(0,new ListItem("",""));
		tagMANAGER_SEQ.DataSourceID = "";
	}

}
