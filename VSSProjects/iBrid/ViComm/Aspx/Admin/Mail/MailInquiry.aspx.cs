﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メール記録
--	Progaram ID		: MailInquiry
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater		Update Explain
  2010/06/15  KazuakiItoh	一括送信メールのデフォルト非表示対応

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Mail_MailInquiry:System.Web.UI.Page {
	#region Sorting

	private string SortExpression {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortExpression"]);
		}
		set {
			this.ViewState["SortExpression"] = value;
		}
	}

	private string SortDirection {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortDirection"]);
		}
		set {
			this.ViewState["SortDirection"] = value;
		}
	}
	#endregion

	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			FisrtLoad();
			InitPage();
		}
	}

	private void FisrtLoad() {
		grdMail.PageSize = int.Parse(Session["PageSize"].ToString());
		grdMail.DataSourceID = "";
		DataBind();
		if (Session["SiteCd"].ToString().Equals("")) {
			lstSiteCd.Items.Insert(0,new ListItem("",""));
		}
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		lstSiteCd.DataSourceID = "";
		lstMailType.Items.Insert(0,new ListItem("",""));
		lstMailType.DataSourceID = "";
	}

	private void InitPage() {
		pnlInfo.Visible = false;
		ClearField();
	}

	private void ClearField() {
		txtTxLoginId.Text = "";
		txtRxLoginId.Text = "";
		txtReportDayFrom.Text = DateTime.Now.ToString("yyyy/MM/dd");
		txtReportTimeFrom.Text = "";
		txtReportDayTo.Text = "";
		txtReportTimeTo.Text = "";
		chkPagingOff.Checked = false;
		lstMailType.SelectedIndex = 0;
		SortExpression = string.Empty;
		SortDirection = string.Empty;
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	private void GetList() {
		if (txtReportDayTo.Text.Equals("")) {
			txtReportDayTo.Text = txtReportDayFrom.Text;
		}
		DateTime dtTo = System.DateTime.ParseExact(txtReportDayTo.Text,"yyyy/MM/dd",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);

		if (chkPagingOff.Checked) {
			grdMail.PageSize = 999999;
		} else {
			grdMail.PageSize = 50;
		}
		grdMail.DataSourceID = "dsMailLog";
		DataBind();
		pnlInfo.Visible = true;
	}

	protected string GetReadNm(object pReadFlag) {
		if (pReadFlag.ToString().Equals("1")) {
			return "既読";
		} else {
			return "";
		}
	}

	protected string GetMailTitle(object pMailTitle,object pCastOriginalTitle) {
		if (!pCastOriginalTitle.ToString().Equals("")) {
			return pCastOriginalTitle.ToString();
		} else {
			return pMailTitle.ToString();
		}
	}

	protected string GetMailDoc(
		object pMailDoc1,
		object pMailDoc2,
		object pMailDoc3,
		object pMailDoc4,
		object pMailDoc5,
		object pCastOriginalDoc1,
		object pCastOriginalDoc2,
		object pCastOriginalDoc3,
		object pCastOriginalDoc4,
		object pCastOriginalDoc5) {
		if (!pCastOriginalDoc1.ToString().Equals("")) {
			return pCastOriginalDoc1.ToString() + pCastOriginalDoc2.ToString() + pCastOriginalDoc3.ToString() + pCastOriginalDoc4.ToString() + pCastOriginalDoc5.ToString();
		} else {
			return pMailDoc1.ToString() + pMailDoc2.ToString() + pMailDoc3.ToString() + pMailDoc4.ToString() + pMailDoc5.ToString();
		}
	}

	protected string GetDelMark(object pTxDelFlag,object pRxDelFlag) {
		string sTx = pTxDelFlag.ToString();
		string sRx = pRxDelFlag.ToString();

		if ((sTx.Equals("0")) && (sRx.Equals("0"))) {
			return "";
		} else if ((sTx.Equals("1")) && (sRx.Equals("1"))) {
			return "削除済";
		} else if ((sTx.Equals("0")) && (sRx.Equals("1"))) {
			return "受信側削除";
		} else {
			return "送信側削除";
		}
	}

	protected string GetViewUrl(object pSexCd,object pSiteCd,object pLoignId) {
		if (pSexCd.Equals(ViCommConst.MAN)) {
			return string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",pSiteCd.ToString(),pLoignId.ToString());
		} else {
			return string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}",pLoignId.ToString(),"MailInquiry.aspx");
		}
	}

	protected bool IsAreaManager(object pSexCd,object pManagerSeq) {
		if (Session["AdminType"].ToString().CompareTo(ViCommConst.RIGHT_LOCAL_STAFF) > 0) {
			return true;
		} else {
			if (pSexCd.Equals(ViCommConst.MAN)) {
				return false;
			} else {
				return iBridUtil.GetStringValue(Session["MANAGER_SEQ"]).Equals(pManagerSeq.ToString());
			}
		}
	}

	protected void lnkDelMail_Command(object sender,CommandEventArgs e) {
		string sMailSeq = e.CommandArgument.ToString();
		using (MailBox oMailBox = new MailBox()) {
			oMailBox.UpdateMailDel(sMailSeq,ViCommConst.RX);
			oMailBox.UpdateMailDel(sMailSeq,ViCommConst.TX);
		}
		DataBind();
	}

	protected void dsMail_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		string sAttachedType = string.Empty;
		foreach (ListItem oItem in this.chkAttachedType.Items){
			if(!oItem.Selected) continue;

			if (!sAttachedType.Equals(string.Empty)) sAttachedType += ",";
			sAttachedType += oItem.Value;
		}
		
	
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = lstMailType.SelectedValue;
		e.InputParameters[2] = txtReportDayFrom.Text;
		e.InputParameters[3] = txtReportTimeFrom.Text;
		if (txtReportDayTo.Text.Equals("")) {
			txtReportDayTo.Text = txtReportDayFrom.Text;
		}
		e.InputParameters[4] = txtReportDayTo.Text;
		e.InputParameters[5] = txtReportTimeTo.Text;
		e.InputParameters[6] = txtTxLoginId.Text;
		e.InputParameters[7] = txtTxUserCharNo.Text;
		e.InputParameters[8] = txtRxLoginId.Text;
		e.InputParameters[9] = txtRxUserCharNo.Text;

		if (Session["AdminType"].ToString().CompareTo(ViCommConst.RIGHT_LOCAL_STAFF) <= 0) {
			e.InputParameters[10] = iBridUtil.GetStringValue(Session["MANAGER_SEQ"]);
		} else {
			e.InputParameters[10] = "";
		}
		e.InputParameters[11] = rdoWithBatchMail.SelectedValue;
		e.InputParameters[12] = sAttachedType;
		e.InputParameters[13] = txtKeyword.Text.Trim();
		e.InputParameters[14] = SortExpression;
		e.InputParameters[15] = SortDirection;
	}

	protected void dsMail_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected void grdMail_Sorting(object sender,GridViewSortEventArgs e) {
		if (this.SortExpression.Equals(e.SortExpression)) {
			if (this.SortDirection.Equals("ASC")) {
				this.SortDirection = "DESC";
			} else if (this.SortDirection.Equals("DESC")) {
				this.SortDirection = "ASC";
			}
		} else {
			this.SortDirection = "ASC";
		}

		this.SortExpression = e.SortExpression;

		e.Cancel = true;
		this.GetList();
	}

	protected string GenerateOpenPicScript(object pDataItem) {
		DataRowView oDataRowView = (DataRowView)pDataItem;
		
		string sSiteCd = this.lstSiteCd.SelectedValue;
		string sTxLoginId = iBridUtil.GetStringValue(oDataRowView["TX_LOGIN_ID"]);
		string sPicSeq = iBridUtil.GetStringValue(oDataRowView["PIC_SEQ"]);
		string sTxSexCd = iBridUtil.GetStringValue(oDataRowView["TX_SEX_CD"]);
		
		string sImgPath = string.Empty;
		
		if(sTxSexCd.Equals(ViCommConst.MAN)){
			sImgPath = string.Format("data/{0}/Man/{1}.jpg", sSiteCd, iBridUtil.addZero(sPicSeq, ViCommConst.OBJECT_NM_LENGTH));
		}else{
			sImgPath = string.Format("data/{0}/Operator/{1}/{2}.jpg", sSiteCd, sTxLoginId, iBridUtil.addZero(sPicSeq, ViCommConst.OBJECT_NM_LENGTH));
		}

		return string.Format("javascript:openPicViewer('{0}');", sImgPath);
	}
	protected string GenerateOpenMovieScript(object pDataItem) {
		DataRowView oDataRowView = (DataRowView)pDataItem;

		string sSiteCd = this.lstSiteCd.SelectedValue;
		string sTxLoginId = iBridUtil.GetStringValue(oDataRowView["TX_LOGIN_ID"]);
		string sMovieSeq = iBridUtil.GetStringValue(oDataRowView["MOVIE_SEQ"]);
		string sTxSexCd = iBridUtil.GetStringValue(oDataRowView["TX_SEX_CD"]);

		string sImgPath = string.Empty;

		if (sTxSexCd.Equals(ViCommConst.MAN)) {
			sImgPath = string.Format("~/movie/{0}/Man/{1}.3gp", sSiteCd, iBridUtil.addZero(sMovieSeq, ViCommConst.OBJECT_NM_LENGTH));
		} else {
			sImgPath = string.Format("~/movie/{0}/Operator/{1}/{2}.3gp", sSiteCd, sTxLoginId, iBridUtil.addZero(sMovieSeq, ViCommConst.OBJECT_NM_LENGTH));
		}

		return string.Format("javascript:openMovieViewer('{0}');", this.ResolveUrl(sImgPath));
	}

	protected string GenerateSmallPicPath(object pDataItem) {
		DataRowView oDataRowView = (DataRowView)pDataItem;

		string sSiteCd = this.lstSiteCd.SelectedValue;
		string sTxLoginId = iBridUtil.GetStringValue(oDataRowView["TX_LOGIN_ID"]);
		string sPicSeq = iBridUtil.GetStringValue(oDataRowView["PIC_SEQ"]);
		string sTxSexCd = iBridUtil.GetStringValue(oDataRowView["TX_SEX_CD"]);

		string sImgPath = string.Empty;

		if (sTxSexCd.Equals(ViCommConst.MAN)) {
			sImgPath = string.Format("../data/{0}/Man/{1}_s.jpg", sSiteCd, iBridUtil.addZero(sPicSeq, ViCommConst.OBJECT_NM_LENGTH));
		} else {
			sImgPath = string.Format("../data/{0}/Operator/{1}/{2}_s.jpg", sSiteCd, sTxLoginId, iBridUtil.addZero(sPicSeq, ViCommConst.OBJECT_NM_LENGTH));
		}

		return sImgPath;
	}

	protected string GenerateSmallPicPathMovie(object pDataItem) {
		DataRowView oDataRowView = (DataRowView)pDataItem;

		string sSiteCd = this.lstSiteCd.SelectedValue;
		string sTxLoginId = iBridUtil.GetStringValue(oDataRowView["TX_LOGIN_ID"]);
		string sPicSeq = iBridUtil.GetStringValue(oDataRowView["MOVIE_SEQ"]);
		string sTxSexCd = iBridUtil.GetStringValue(oDataRowView["TX_SEX_CD"]);

		string sImgPath = string.Empty;

		if (sTxSexCd.Equals(ViCommConst.MAN)) {
			sImgPath = string.Format("../data/{0}/Man/{1}.jpg", sSiteCd, iBridUtil.addZero(sPicSeq, ViCommConst.OBJECT_NM_LENGTH));
		} else {
			sImgPath = string.Format("../data/{0}/Operator/{1}/{2}.jpg", sSiteCd, sTxLoginId, iBridUtil.addZero(sPicSeq, ViCommConst.OBJECT_NM_LENGTH));
		}

		return sImgPath;
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		if (grdMail.Rows.Count == 0) {
			return;
		}

		DataSet ds = this.GetListDs();
		
		//ヘッダ作成 追加
		string sHeader = "作成日時,送信者ID,受信者ID,件名\r\n";
		
		//CSV出力
		Response.ContentType = "application/download";
		Response.AppendHeader("Content-Disposition","attachment;filename=MailList.csv");
		Response.ContentEncoding = System.Text.Encoding.GetEncoding("Shift_JIS");
		
		Response.Write(sHeader);
		
		if (ds.Tables[0].Rows.Count > 0) {
			foreach(DataRow dr in ds.Tables[0].Rows) {
				Response.Write(string.Format("{0},{1},{2},{3}\r\n",dr["CREATE_DATE"].ToString(),dr["TX_LOGIN_ID"].ToString(),
							   dr["RX_LOGIN_ID"].ToString(),dr["MAIL_TITLE"].ToString()));
			}
		}
		
		Response.End();
	}

	private DataSet GetListDs() {
		DataSet oDataSet;
		using (MailLog oMailLog = new MailLog()) {
			string sAttachedType = string.Empty;
			foreach (ListItem oItem in this.chkAttachedType.Items) {
				if (!oItem.Selected)
					continue;

				if (!sAttachedType.Equals(string.Empty))
					sAttachedType += ",";
				sAttachedType += oItem.Value;
			}
			
			oDataSet = oMailLog.GetPageCollection(
				lstSiteCd.SelectedValue,
				lstMailType.SelectedValue,
				txtReportDayFrom.Text,
				txtReportTimeFrom.Text,
				txtReportDayTo.Text.Equals("") ? txtReportDayFrom.Text : txtReportDayTo.Text,
				txtReportTimeTo.Text,
				txtTxLoginId.Text,
				txtTxUserCharNo.Text,
				txtRxLoginId.Text,
				txtRxUserCharNo.Text,
				Session["AdminType"].ToString().CompareTo(ViCommConst.RIGHT_LOCAL_STAFF) <= 0 ? iBridUtil.GetStringValue(Session["MANAGER_SEQ"]) : "",
				0,
				SysConst.DB_MAX_ROWS,
				rdoWithBatchMail.SelectedValue,
				sAttachedType,
				txtKeyword.Text.Trim(),
				SortExpression,
				SortDirection
			);	
		}
		return oDataSet;
	}
}
