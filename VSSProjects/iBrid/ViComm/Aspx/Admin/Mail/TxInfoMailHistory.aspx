﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TxInfoMailHistory.aspx.cs" Inherits="Mail_TxInfoMailHistory" Title="お知らせメール送信履歴" %>
<%@ Import namespace="ViComm" %>
<%@ Import Namespace="iBridCommLib" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="お知らせメール送信履歴"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" Runat="Server">
	<div class="admincontent">
		<%-- ============================== --%>
		<%--  Search Condition              --%>
		<%-- ============================== --%>
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							送信日時
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtSendDayFrom" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
							<asp:TextBox ID="txtSendTimeFrom" runat="server" Width="15px" MaxLength="2"></asp:TextBox>
							～
							<asp:TextBox ID="txtSendDayTo" runat="server" MaxLength="10" Width="62px"></asp:TextBox>
							<asp:TextBox ID="txtSendTimeTo" runat="server" Width="15px" MaxLength="2"></asp:TextBox>
							<asp:RangeValidator ID="vdrSendTimeFrom" runat="server" ErrorMessage="送信時間Fromを正しく入力して下さい。" ControlToValidate="txtSendTimeFrom" MaximumValue="23" MinimumValue="00"
								Type="Integer" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:RangeValidator ID="vdrSendTimeTo" runat="server" ErrorMessage="送信時間Toを正しく入力して下さい。" ControlToValidate="txtSendTimeTo" MaximumValue="23" MinimumValue="00"
								Type="Integer" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:RangeValidator ID="vdrSendDayFrom" runat="server" ErrorMessage="送信日Fromを正しく入力して下さい。" ControlToValidate="txtSendDayFrom" MaximumValue="2099/12/31"
								MinimumValue="1990/01/01" Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<asp:RangeValidator ID="vdrSendDayTo" runat="server" ErrorMessage="送信日Toを正しく入力して下さい。" ControlToValidate="txtSendDayTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
								Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
							<ajaxToolkit:MaskedEditExtender ID="mskSendDayFrom" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
								TargetControlID="txtSendDayFrom"></ajaxToolkit:MaskedEditExtender>
							<ajaxToolkit:MaskedEditExtender ID="mskSendDayTo" runat="server" MaskType="Date" Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="true"
								TargetControlID="txtSendDayTo"></ajaxToolkit:MaskedEditExtender>
							<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtSendTimeFrom" />
							<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtSendTimeTo" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							送信状態
						</td>
						<td class="tdDataStyle">
							<asp:RadioButtonList ID="rdoSendFlag" runat="server" RepeatDirection="horizontal">
								<asp:ListItem Selected="true" Text="全て" Value=""></asp:ListItem>
								<asp:ListItem Text="未送信" Value="0"></asp:ListItem>
								<asp:ListItem Text="送信済み" Value="1"></asp:ListItem>
							</asp:RadioButtonList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							性別
						</td>
						<td class="tdDataStyle">
							<asp:RadioButtonList ID="rdoSexCd" runat="server" RepeatDirection="horizontal">
								<asp:ListItem Selected="true" Text="両方" Value=""></asp:ListItem>
								<asp:ListItem Text="男性" Value="1"></asp:ListItem>
								<asp:ListItem Text="女性" Value="3"></asp:ListItem>
							</asp:RadioButtonList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							送信方法
						</td>
						<td class="tdDataStyle">
							<asp:RadioButtonList ID="rdoReservationFlag" runat="server" RepeatDirection="horizontal">
								<asp:ListItem Selected="true" Text="全て" Value=""></asp:ListItem>
								<asp:ListItem Text="手動送信" Value="0"></asp:ListItem>
								<asp:ListItem Text="予約送信" Value="1"></asp:ListItem>
							</asp:RadioButtonList>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="True" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
			</asp:Panel>
		</fieldset>

		<%-- ============================== --%>
		<%--  Data                          --%>
		<%-- ============================== --%>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset class="fieldset">
				<legend>[送信履歴一覧]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count
						<%#GetRecCount() %>
					</a>
					<br />
					<a class="reccount">Current viewing page
						<%=grdData.PageIndex + 1%>
						of
						<%=grdData.PageCount%>
					</a>
				</asp:Panel>
				&nbsp;
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto">
					<asp:GridView ID="grdData" runat="server" AllowSorting="false" AllowPaging="True" AutoGenerateColumns="False" SkinID="GridViewColor" PageSize="20" OnRowDataBound="grdData_RowDataBound">
						<Columns>
							<asp:TemplateField HeaderText="送信予定日時" SortExpression="RESERVATION_SEND_DATE">
								<ItemStyle HorizontalAlign="left"/>
								<ItemTemplate>
									<%# DataBinder.Eval(Container.DataItem,"RESERVATION_SEND_DATE","{0:yyyy/M/d HH:mm}")%>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="送信完了日時" SortExpression="END_DATE">
								<ItemStyle HorizontalAlign="left"/>
								<ItemTemplate>
									<%# DataBinder.Eval(Container.DataItem, "END_DATE", "{0:yyyy/M/d HH:mm}")%>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="性別" SortExpression="RX_SEX_CD">
								<ItemStyle HorizontalAlign="center"/>
								<ItemTemplate>
									<%# DataBinder.Eval(Container.DataItem, "RX_SEX_CD").Equals(ViCommConst.MAN)? "男性" : "女性"%>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾃﾝﾌﾟﾚｰﾄ名称" SortExpression="TEMPLATE_NM">
								<ItemStyle HorizontalAlign="right"/>
								<ItemTemplate>
									<asp:HyperLink ID="lnkMailTemplate" runat="server" NavigateUrl='<%# GetUrlMailTemplate(Eval("SITE_CD"),Eval("MAIL_TEMPLATE_NO")) %>'
										Text='<%# Eval("TEMPLATE_NM") %>'></asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="送信予定人数" SortExpression="RESERVATION_SEND_NUM">
								<ItemStyle HorizontalAlign="right"/>
								<ItemTemplate>
									<%# Eval("RESERVATION_SEND_NUM")%>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="送信人数" SortExpression="SEND_NUM">
								<ItemStyle HorizontalAlign="right"/>
								<ItemTemplate>
									<%# Eval("SEND_NUM")%>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="送信サーバ" SortExpression="MAIL_SERVER">
								<ItemStyle HorizontalAlign="center"/>
								<ItemTemplate>
									<%# GetMailServerNm(Eval("MAIL_SERVER"))%>
								</ItemTemplate>
							</asp:TemplateField>

							<asp:TemplateField HeaderText="送信数" SortExpression="TX_MAIL_COUNT">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<asp:HyperLink ID="lnkTxMailCnt" runat="server" NavigateUrl='<%# string.Format("~/Status/AccessMailCountDailyList.aspx?site={0}&mailtemplateno={1}&sexcd={2}&txmailday={3}",Eval("SITE_CD"),Eval("MAIL_TEMPLATE_NO"),Eval("RX_SEX_CD"),DataBinder.Eval(Container.DataItem, "RESERVATION_SEND_DATE", "{0:yyyy/MM/dd}")) %>'
											Text='<%# DataBinder.Eval(Container.DataItem, "TX_MAIL_COUNT") %>'></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ｱｸｾｽ総数">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<%# Eval("ACCESS_COUNT") %>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾕﾆｰｸ数">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<%# Eval("UNIQUE_ACCESS_COUNT") %>
								</ItemTemplate>
								<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ｱｸｾｽ率">
								<HeaderStyle Wrap="false" />
								<ItemTemplate>
									<%# GetUniqueAccessRate(Eval("TX_MAIL_COUNT").ToString(),Eval("UNIQUE_ACCESS_COUNT").ToString()) %>
								</ItemTemplate>
							</asp:TemplateField>

							<asp:TemplateField HeaderText="送信方法" SortExpression="RESERVATION_FLAG">
								<ItemStyle HorizontalAlign="center"/>
								<ItemTemplate>
									<%# Eval("RESERVATION_FLAG").ToString().Equals(ViCommConst.FLAG_OFF_STR) ? "手動送信" : "予約送信" %>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="詳細">
								<ItemStyle HorizontalAlign="center" Wrap="false" />
								<ItemTemplate>
									<asp:PlaceHolder ID="phDtl" runat="server"></asp:PlaceHolder>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="削除">
								<ItemStyle HorizontalAlign="center" Wrap="false" />
								<ItemTemplate>
									<asp:LinkButton ID="lnkDelHistory" runat="server" Text="削除"
										CommandArgument='<%# string.Format("{0}:{1}",Eval("RESERVATION_MAIL_HISTORY_SEQ"),Eval("REVISION_NO")) %>'
										OnCommand="lnkDelHistory_Command"
										OnClientClick="return confirm('予約送信設定の削除を行ないますか？');">
									</asp:LinkButton>
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
	</div>

	<%-- ============================== --%>
	<%--  Data Source                   --%>
	<%-- ============================== --%>
	<asp:ObjectDataSource ID="dsData" runat="server" TypeName="TxInfoMailHistory" SelectMethod="GetPageCollection" SelectCountMethod="GetPageCount"
		EnablePaging="True" OnSelecting="dsData_Selecting" OnSelected="dsData_Selected">
		<SelectParameters>
			<asp:Parameter Name="pSearchCondition" Type="object" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
