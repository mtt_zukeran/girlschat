﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 一括メール記録
--	Progaram ID		: BatchMailInquiry
--
--  Creation Date	: 2011.10.11
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater		Update Explain
  YYYY/MM/DD  XXXXXXXXXX	XXXXXXXXXXXXXXXXXXXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.IO;
using System.Text;
using iBridCommLib;
using ViComm;

public partial class Mail_BatchMailInquiry : System.Web.UI.Page {

	private string recCount = string.Empty;
	private Stream filter;

	#region Sorting

	private string SortExpression {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortExpression"]);
		}
		set {
			this.ViewState["SortExpression"] = value;
		}
	}

	private string SortDirect {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortDirect"]);
		}
		set {
			this.ViewState["SortDirect"] = value;
		}
	}
	#endregion

	protected void Page_Load(object sender,EventArgs e) {
		filter = Response.Filter;
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			FisrtLoad();
			this.InitPage();
		}
	}

	protected void grdMail_Sorting(object sender,GridViewSortEventArgs e) {
		if (this.SortExpression.Equals(e.SortExpression)) {
			if (this.SortDirect.Equals("ASC")) {
				this.SortDirect = "DESC";
			} else if (this.SortDirect.Equals("DESC")) {
				this.SortDirect = "ASC";
			}
		} else {
			this.SortDirect = "ASC";
		}

		this.SortExpression = e.SortExpression;

		e.Cancel = true;

		this.GetList();
	}

	private void FisrtLoad() {
		this.grdMail.PageSize = int.Parse(this.Session["PageSize"].ToString());
		this.grdMail.DataSourceID = string.Empty;
		this.DataBind();
		if (this.Session["SiteCd"].ToString().Equals(string.Empty)) {
			this.lstSiteCd.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		}
		if (!iBridUtil.GetStringValue(this.Session["MENU_SITE"]).Equals(string.Empty)) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(this.Session["MENU_SITE"]);
		}
		this.lstSiteCd.DataSourceID = string.Empty;
	}

	private void InitPage() {
		this.pnlInfo.Visible = false;
		ClearField();
	}

	private void ClearField() {
		this.txtTxLoginId.Text = string.Empty;
		this.txtReportDayFrom.Text = DateTime.Now.ToString("yyyy/MM/dd");
		this.txtReportTimeFrom.Text = string.Empty;
		this.txtReportDayTo.Text = string.Empty;
		this.txtReportTimeTo.Text = string.Empty;
		this.chkPagingOff.Checked = false;
		this.SortExpression = string.Empty;
		this.SortDirect = string.Empty;
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		this.GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		DataSet ds;
		string sAttachedType = string.Empty;

		foreach (ListItem oItem in this.chkAttachedType.Items) {
			if (!oItem.Selected)
				continue;

			if (!sAttachedType.Equals(string.Empty))
				sAttachedType += ",";
			sAttachedType += oItem.Value;
		}

		if (this.txtReportDayTo.Text.Equals(string.Empty)) {
			this.txtReportDayTo.Text = this.txtReportDayFrom.Text;
		}

		using (BatchMail oBatchMail = new BatchMail()) {
			ds = oBatchMail.GetPageCollection(
				this.lstSiteCd.SelectedValue,
				this.txtReportDayFrom.Text,
				this.txtReportTimeFrom.Text,
				this.txtReportDayTo.Text,
				this.txtReportTimeTo.Text,
				this.txtTxLoginId.Text,
				this.txtTxUserCharNo.Text,
				sAttachedType,
				this.SortExpression,
				this.SortDirect,
				0,
				Int32.MaxValue
			);
		}

		Response.Filter = filter;
		Response.ContentType = "application/download";
		Response.AddHeader("Content-Disposition","attachment;filename=batchMailInquiry.csv");
		Response.ContentEncoding = Encoding.GetEncoding("Shift_JIS");

		Response.Write("\"送信日時\",\"送信者ID\",\"送信者ハンドル名\",\"件名\",\"本文\",\"検索結果数\",\"実送信数\"\r\n");

		foreach (DataRow dr in ds.Tables[0].Rows) {
			string sData = string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\"\r\n",
				iBridUtil.GetStringValue(dr["REQUEST_TX_DATE"]),
				iBridUtil.GetStringValue(dr["TX_LOGIN_ID"]),
				iBridUtil.GetStringValue(dr["TX_HANDLE_NM"]),
				iBridUtil.GetStringValue(dr["ORIGINAL_TITLE"]).Replace(Environment.NewLine,""),
				string.Format("{0}{1}{2}{3}{4}",iBridUtil.GetStringValue(dr["ORIGINAL_DOC1"]),iBridUtil.GetStringValue(dr["ORIGINAL_DOC2"]),iBridUtil.GetStringValue(dr["ORIGINAL_DOC3"]),iBridUtil.GetStringValue(dr["ORIGINAL_DOC4"]),iBridUtil.GetStringValue(dr["ORIGINAL_DOC5"])).Replace(Environment.NewLine,""),
				iBridUtil.GetStringValue(dr["BATCH_MAIL_FIND_COUNT"]),
				iBridUtil.GetStringValue(dr["BATCH_MAIL_SEND_REAL_COUNT"])
			);

			Response.Write(sData);
		}

		Response.End();
	}

	private void GetList() {
		if (this.txtReportDayTo.Text.Equals(string.Empty)) {
			this.txtReportDayTo.Text = this.txtReportDayFrom.Text;
		}
		DateTime dtTo = System.DateTime.ParseExact(this.txtReportDayTo.Text,"yyyy/MM/dd",System.Globalization.DateTimeFormatInfo.InvariantInfo,System.Globalization.DateTimeStyles.None);

		if (this.chkPagingOff.Checked) {
			this.grdMail.PageSize = 999999;
		} else {
			this.grdMail.PageSize = 50;
		}
		this.grdMail.DataSourceID = "dsBatchMail";
		this.DataBind();
		this.pnlInfo.Visible = true;
	}

	protected string GetMailTitle(object pMailTitle,object pCastOriginalTitle) {
		if (!pCastOriginalTitle.ToString().Equals(string.Empty)) {
			return pCastOriginalTitle.ToString();
		} else {
			return pMailTitle.ToString();
		}
	}

	protected string GetViewUrl(object pSiteCd,object pLoignId) {
		return string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}",pLoignId.ToString(),"MailInquiry.aspx");
	}

	protected void lnkDelMail_Command(object sender,CommandEventArgs e) {
		string sMailSeq = e.CommandArgument.ToString();
		using (MailBox oMailBox = new MailBox()) {
			oMailBox.UpdateMailDel(sMailSeq,ViCommConst.RX);
			oMailBox.UpdateMailDel(sMailSeq,ViCommConst.TX);
		}
		this.DataBind();
	}

	protected void dsBatchMail_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		string sAttachedType = string.Empty;
		foreach (ListItem oItem in this.chkAttachedType.Items) {
			if (!oItem.Selected) continue;

			if (!sAttachedType.Equals(string.Empty)) sAttachedType += ",";
			sAttachedType += oItem.Value;
		}

		if (this.txtReportDayTo.Text.Equals(string.Empty)) {
			this.txtReportDayTo.Text = this.txtReportDayFrom.Text;
		}
		e.InputParameters["pSiteCd"] = this.lstSiteCd.SelectedValue;
		e.InputParameters["pReportDayFrom"] = this.txtReportDayFrom.Text;
		e.InputParameters["pReportTimeFrom"] = this.txtReportTimeFrom.Text;
		e.InputParameters["pReportDayTo"] = this.txtReportDayTo.Text;
		e.InputParameters["pReportTimeTo"] = this.txtReportTimeTo.Text;
		e.InputParameters["pTxLoginId"] = this.txtTxLoginId.Text;
		e.InputParameters["pTxUserCharNo"] = this.txtTxUserCharNo.Text;
		e.InputParameters["pAttachedType"] = sAttachedType;
		e.InputParameters["pSortExpression"] = this.SortExpression;
		e.InputParameters["pSortDirection"] = this.SortDirect;
	}

	protected void dsBatchMail_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected string GenerateOpenPicScript(object pDataItem) {
		DataRowView oDataRowView = (DataRowView)pDataItem;

		string sSiteCd = this.lstSiteCd.SelectedValue;
		string sTxLoginId = iBridUtil.GetStringValue(oDataRowView["TX_LOGIN_ID"]);
		string sPicSeq = iBridUtil.GetStringValue(oDataRowView["PIC_SEQ"]);

		string sImgPath = string.Empty;

		sImgPath = string.Format("data/{0}/Operator/{1}/{2}.jpg",sSiteCd,sTxLoginId,iBridUtil.addZero(sPicSeq,ViCommConst.OBJECT_NM_LENGTH));


		return string.Format("javascript:openPicViewer('{0}');",sImgPath);
	}
	protected string GenerateOpenMovieScript(object pDataItem) {
		DataRowView oDataRowView = (DataRowView)pDataItem;

		string sSiteCd = this.lstSiteCd.SelectedValue;
		string sTxLoginId = iBridUtil.GetStringValue(oDataRowView["TX_LOGIN_ID"]);
		string sMovieSeq = iBridUtil.GetStringValue(oDataRowView["MOVIE_SEQ"]);

		string sImgPath = string.Empty;

		sImgPath = string.Format("~/movie/{0}/Operator/{1}/{2}.3gp",sSiteCd,sTxLoginId,iBridUtil.addZero(sMovieSeq,ViCommConst.OBJECT_NM_LENGTH));

		return string.Format("javascript:openMovieViewer('{0}');",this.ResolveUrl(sImgPath));
	}

	protected string GenerateSmallPicPath(object pDataItem) {
		DataRowView oDataRowView = (DataRowView)pDataItem;

		string sSiteCd = this.lstSiteCd.SelectedValue;
		string sTxLoginId = iBridUtil.GetStringValue(oDataRowView["TX_LOGIN_ID"]);
		string sPicSeq = iBridUtil.GetStringValue(oDataRowView["PIC_SEQ"]);

		string sImgPath = string.Empty;

		sImgPath = string.Format("../data/{0}/Operator/{1}/{2}_s.jpg",sSiteCd,sTxLoginId,iBridUtil.addZero(sPicSeq,ViCommConst.OBJECT_NM_LENGTH));

		return sImgPath;
	}

	protected string GenerateSmallPicPathMovie(object pDataItem) {
		DataRowView oDataRowView = (DataRowView)pDataItem;

		string sSiteCd = this.lstSiteCd.SelectedValue;
		string sTxLoginId = iBridUtil.GetStringValue(oDataRowView["TX_LOGIN_ID"]);
		string sPicSeq = iBridUtil.GetStringValue(oDataRowView["MOVIE_SEQ"]);

		string sImgPath = string.Empty;

		sImgPath = string.Format("../data/{0}/Operator/{1}/{2}.jpg",sSiteCd,sTxLoginId,iBridUtil.addZero(sPicSeq,ViCommConst.OBJECT_NM_LENGTH));

		return sImgPath;
	}

}
