﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: MULLER3フィルタリングエラー一括解除
--	Progaram ID		: ReleaseTxMuller3NgAllExecute
--
--  Creation Date	: 2016.07.06
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Mail_ReleaseTxMuller3NgAllExecute:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			lblDone.Visible = false;
		}
	}

	protected void btnExecute_Click(object sender,EventArgs e) {
		using (DbSession oDbSession = new DbSession()) {
			oDbSession.PrepareProcedure("RELEASE_TX_MULLER3_NG_ALL");
			oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
			oDbSession.ExecuteProcedure();
		}
		
		lblDone.Visible = true;
	}
}