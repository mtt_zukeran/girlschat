﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="IncorrectEmailAddrList.aspx.cs" Inherits="Mail_IncorrectEmailAddrList" Title="不正メールアドレス一覧" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="不正メールアドレス一覧"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" Runat="Server">
	<div class="admincontent">
		<fieldset>
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle2">
							サイトコード
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
							</asp:DropDownList>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset class="fieldset">
				<legend>[不正メールアドレス一覧]</legend>
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">
						Record Count <%# GetRecCount() %>
					</a>
					<br />
					<a class="reccount">
						Current viewing page <%=grdData.PageIndex + 1%> of <%=grdData.PageCount%>
					</a>
				</asp:Panel>
				&nbsp;
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto">
					<asp:GridView ID="grdData" runat="server" DataSourceID="" AllowSorting="false" AllowPaging="True" AutoGenerateColumns="False" SkinID="GridView" PageSize="100">
					    <Columns>
							<asp:TemplateField HeaderText="メールアドレス">
								<ItemTemplate>
									<asp:HyperLink ID="lnkEmailAddr" runat="server" NavigateUrl='<%# GetEmailAddrUrl(Eval("SITE_CD"),Eval("SEX_CD"),Eval("LOGIN_ID")) %>' Text='<%# Eval("EMAIL_ADDR") %>'></asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="性別">
								<ItemTemplate>
									<asp:Label ID="lblSexCd" runat="server" Text='<%# GetSexCdText(Eval("SEX_CD")) %>' />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="REGIST_DATE" HeaderText="登録日" DataFormatString="{0:yy/MM/dd HH:mm}" HtmlEncode="False">
							</asp:BoundField>
                            <asp:TemplateField HeaderText="操作">
                                <ItemTemplate>
									<asp:LinkButton ID="lnkDelete" runat="server" Text="削除" OnCommand="lnkDelete_Command" CommandArgument='<%# Container.DataItemIndex %>' ></asp:LinkButton>
									<ajaxToolkit:ConfirmButtonExtender ID="cfmLnkDelete" runat="server" TargetControlID="lnkDelete" ConfirmText="削除します。よろしいですか？" ConfirmOnFormSubmit="true" />
									<asp:HiddenField ID="hdnSiteCd" runat="server" Value='<%# Eval("SITE_CD") %>' />
									<asp:HiddenField ID="hdnEmailAddr" runat="server" Value='<%# Eval("EMAIL_ADDR") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
					    </Columns>
					    <PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsData" runat="server" TypeName="IncorrectEmailAddr" SelectMethod="GetPageCollection" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelecting="dsData_Selecting" OnSelected="dsData_Selected">
		<SelectParameters>
			<asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>

