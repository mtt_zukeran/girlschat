﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 不正メールアドレス一覧
--	Progaram ID		: IncorrectEmailAddrList
--  Creation Date	: 2015.03.13
--  Creater			: K.Miyazato
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ViComm;
using iBridCommLib;

public partial class Mail_IncorrectEmailAddrList:System.Web.UI.Page {
	private int recCount = 0;

	protected void Page_Load(object sender,EventArgs e) {
		if (!this.IsPostBack) {
			grdData.DataSourceID = "dsData";
			pnlGrid.DataBind();
			pnlCount.DataBind();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		grdData.PageIndex = 0;
		pnlGrid.DataBind();
		pnlCount.DataBind();
	}

	protected void lnkDelete_Command(object sender,CommandEventArgs e) {
		int iIndex = int.Parse(iBridUtil.GetStringValue(e.CommandArgument));
		GridViewRow oRow = this.grdData.Rows[iIndex];
		HiddenField oSiteCd = oRow.FindControl("hdnSiteCd") as HiddenField;
		HiddenField oEmailAddr = oRow.FindControl("hdnEmailAddr") as HiddenField;

		if (!string.IsNullOrEmpty(oSiteCd.Value) && !string.IsNullOrEmpty(oEmailAddr.Value)) {
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("DELETE_INCORRECT_EMAIL_ADDR");
				db.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,oSiteCd.Value);
				db.ProcedureInParm("pEMAIL_ADDR",DbSession.DbType.VARCHAR2,oEmailAddr.Value);
				db.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
				db.cmd.BindByName = true;
				db.ExecuteProcedure();
			}
		}

		grdData.PageIndex = 0;
		pnlGrid.DataBind();
		pnlCount.DataBind();
	}

	protected int GetRecCount() {
		return this.recCount;
	}

	protected void dsData_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		IncorrectEmailAddr.SearchCondition oSearchCondition = new IncorrectEmailAddr.SearchCondition();
		oSearchCondition.SiteCd = lstSiteCd.SelectedValue;
		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsData_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null && !(e.ReturnValue is DataSet)) {
			recCount = int.Parse(iBridUtil.GetStringValue(e.ReturnValue));
		}
	}

	protected string GetEmailAddrUrl(object pSiteCd,object pSexCd,object pLoginId) {
		string sUrl = string.Empty;

		if (!string.IsNullOrEmpty(pLoginId.ToString())) {
			if (pSexCd.ToString().Equals(ViCommConst.MAN)) {
				sUrl = string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",pSiteCd.ToString(),pLoginId.ToString());
			} else if (pSexCd.ToString().Equals(ViCommConst.OPERATOR)) {
				sUrl = string.Format("~/Cast/CastView.aspx?loginid={0}",pLoginId.ToString());
			}
		}

		return sUrl;
	}

	protected string GetSexCdText(object pSexCd) {
		string sText = string.Empty;

		switch (pSexCd.ToString()) {
			case ViCommConst.MAN:
				sText = "会員";
				break;
			case ViCommConst.OPERATOR:
				sText = "出演者";
				break;
		}

		return sText;
	}
}
