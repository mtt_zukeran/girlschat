<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MailTemplateList.aspx.cs" Inherits="Mail_MailTemplateList"
	Title="メールテンプレート設定" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="メールテンプレート設定"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<asp:Literal ID="ltlUserColor" runat="server"></asp:Literal>
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlMainte">
			<asp:Label ID="lblHtmlDocSeq" runat="server" Text="" Visible="false"></asp:Label>
			<fieldset class="fieldset">
				<legend>[設定]</legend>
				<asp:Panel runat="server" ID="pnlKey">
					<fieldset class="fieldset-inner">
						<table border="0" style="width: 600px" class="tableStyle">
							<tr>
								<td class="tdHeaderStyle">
									サイトコード
								</td>
								<td class="tdDataStyle">
									<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle">
									テンプレート番号
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtMailTemplateNo" runat="server" Width="40px" MaxLength="4"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrMailTemplateNo" runat="server" ErrorMessage="テンプレート番号を入力して下さい。" ControlToValidate="txtMailTemplateNo" ValidationGroup="Key">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeMailTemplateNo" runat="server" ErrorMessage="テンプレート番号は4桁の数字で入力して下さい。" ValidationExpression="\d{4}" ControlToValidate="txtMailTemplateNo"
										ValidationGroup="Key">*</asp:RegularExpressionValidator>
								</td>
								<td class="tdHeaderStyle2">
									順序番号
								</td>
								<td class="tdDataStyle">
									<asp:TextBox ID="txtHtmlDocSubSeq" runat="server" Width="24px" MaxLength="2"></asp:TextBox>
									<asp:RequiredFieldValidator ID="vdrHtmlDocSubSeq" runat="server" ErrorMessage="順序番号を入力して下さい。" ControlToValidate="txtHtmlDocSubSeq" ValidationGroup="Key">*</asp:RequiredFieldValidator>
									<asp:RangeValidator ID="vdaHtmlDocSubSeq" runat="server" ErrorMessage="順序番号は1から10の数字で入力してください。" ControlToValidate="txtHtmlDocSubSeq" MaximumValue="10"
										MinimumValue="1" Type="Integer" ValidationGroup="Key">*</asp:RangeValidator>
								</td>
							</tr>
							<tr>
								<td class="tdHeaderStyle2">
									テンプレートコピー
								</td>
								<td class="tdDataStyle" colspan="3">
									<asp:TextBox ID="txtCopyMailTemplateNo" runat="server" Width="40px" MaxLength="4"></asp:TextBox>
									<asp:Button runat="server" ID="btnCopy" Text="コピー" CssClass="seekbottombutton" OnClick="btnCopy_Click" OnClientClick='return confirm("コピーしてよろしいですか？");'
										ValidationGroup="Copy" />
									<asp:Label ID="lblMailTemplateNoMessage" runat="server" Text="" Visible="false" ForeColor="red"></asp:Label>
									<asp:RequiredFieldValidator ID="vdrCopyMailTemplateNo" runat="server" ErrorMessage="テンプレート番号を入力して下さい。" ControlToValidate="txtCopyMailTemplateNo" ValidationGroup="Copy">*</asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator ID="vdeCopyMailTemplateNo" runat="server" ErrorMessage="テンプレート番号は4桁の数字で入力して下さい。" ValidationExpression="\d{4}" ControlToValidate="txtCopyMailTemplateNo"
										ValidationGroup="Copy">*</asp:RegularExpressionValidator>
								</td>
							</tr>
						</table>
						<asp:Button runat="server" ID="btnSeek" Text="検索" CssClass="seekbutton" OnClick="btnSeek_Click" ValidationGroup="Key" />
						<asp:Button runat="server" ID="btnSeekCancel" Text="キャンセル" CssClass="seekbutton" OnClick="btnSeekCancel_Click" CausesValidation="False" />
						<asp:Button runat="server" ID="btnMailTemplateDelete" Text="メールテンプレート削除" OnClientClick='return confirm("削除してもよろしいですか？");' CssClass="seekbutton" OnClick="btnMailTemplateDelete_Click" CausesValidation="False" />
					</fieldset>
					<fieldset class="fieldset-inner">
						<asp:Panel ID="Panel1" runat="server">
							<table border="0" style="width: 680px">
								<tr>
									<td>
										�@<%= DisplayWordUtil.Replace("受信者ﾒｰﾙｱﾄﾞﾚｽ")%>
									</td>
									<td>
										�A<%= DisplayWordUtil.Replace("受信者ﾛｸﾞｲﾝID")%>
									</td>
									<td>
										�B<%= DisplayWordUtil.Replace("受信者ﾊﾟｽﾜｰﾄﾞ")%>
									</td>
									<td>
										�C<%= DisplayWordUtil.Replace("受信者ﾊﾝﾄﾞﾙ名")%>
									</td>
								</tr>
								<tr>
									<td>
										�D<%= DisplayWordUtil.Replace("送信者ﾊﾝﾄﾞﾙ名")%>
									</td>
									<td>
										�E<%= DisplayWordUtil.Replace("送信者ﾛｸﾞｲﾝID")%>
									</td>
									<td>
										�F<%= DisplayWordUtil.Replace("送信者画像ﾊﾟｽ")%>
									</td>
									<td>
										�G<asp:Label ID="lblAttrNm1" runat="server" Text="Label"></asp:Label>
									</td>
									<td>
										�H<asp:Label ID="lblAttrNm2" runat="server" Text="Label"></asp:Label>
									</td>
									<td>
										�I NONE
									</td>
									<td>
										�J NONE
									</td>
									<td>
										�KサイトURL
									</td>
								</tr>
							</table>
						</asp:Panel>
					</fieldset>
				</asp:Panel>
				<table border="0">
					<tr>
						<td valign="top">
							<fieldset class="fieldset-inner">
								<legend>[アウトルック]</legend>
								<asp:Button runat="server" ID="Button1" Text="テスト送信" CssClass="seekbottombutton" OnClientClick='return confirm("テストメールを送信しますか？");' OnClick="btnTestSend_Click"
									CausesValidation="False" />
								<asp:Label ID="lblMimeSize" runat="server" Text="" Visible="true"></asp:Label><br />
								<asp:Label ID="txtMailTitle1" Text='' runat="server" Visible="true" Width="230px"></asp:Label>
								<asp:Panel runat="server" ID="pnlSelect" Height="580px" ScrollBars="Auto">
									<asp:GridView ID="grdSiteHtmlDoc" runat="server" AllowPaging="False" AutoGenerateColumns="False" DataSourceID="dsSiteHtmlDoc" SkinID="GridView" Width="230px"
										ShowHeader="False">
										<Columns>
											<asp:TemplateField>
												<ItemTemplate>
													<asp:LinkButton ID="lnkDoc" runat="server" CommandArgument='<%# Eval("HTML_DOC_SUB_SEQ") %>' Text='<%# string.Format("{0}",Eval("HTML_DOC_TITLE")) %>'
														OnCommand="lnkDoc_Command" Font-Bold="True" Font-Underline="True"></asp:LinkButton>
													<br />
													<asp:Literal ID="lblHtmlDoc1" runat="server" Text='<%# ReplaceForm(string.Format("<table class=\"userColor\" ><tr><td width=\"220px\">{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}</tr></td></table>",Eval("HTML_DOC1"), Eval("HTML_DOC2"), Eval("HTML_DOC3"), Eval("HTML_DOC4"), Eval("HTML_DOC5"), Eval("HTML_DOC6"),Eval("HTML_DOC7"), Eval("HTML_DOC8") ,Eval("HTML_DOC9"), Eval("HTML_DOC10"))) %>'></asp:Literal>
												</ItemTemplate>
												<ItemStyle HorizontalAlign="Left" />
											</asp:TemplateField>
										</Columns>
									</asp:GridView>
								</asp:Panel>
							</fieldset>
						</td>
						<td valign="top">
							<fieldset class="fieldset-inner">
								<legend>[メールテンプレート内容]</legend>
								<asp:Panel runat="server" ID="pnlDtl">
									<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seektopbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
									<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seektopbutton" OnClick="btnCancel_Click" CausesValidation="False" />
									<asp:Button runat="server" ID="btnDelete" Text="削除" OnClick="btnDelete_Click" ValidationGroup="Key" CssClass="delBtnStyle" />
									<br clear="all" />
									<table border="0" style="width: 580px" class="tableStyle f_clear">
										<tr>
											<td class="tdHeaderSmallStyle">
												ﾃﾝﾌﾟﾚｰﾄ種別
											</td>
											<td class="tdDataStyle">
												<asp:DropDownList ID="lstMailTemplateType" runat="server" DataSourceID="dsMailType" DataTextField="MAIL_TYPE_NM" DataValueField="MAIL_TYPE" Width="240px">
												</asp:DropDownList>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderSmallStyle">
												ﾃﾝﾌﾟﾚｰﾄ名称
											</td>
											<td class="tdDataStyle">
												$NO_TRANS_START;
												<asp:TextBox ID="txtTemplateNm" runat="server" Width="240px" MaxLength="40"></asp:TextBox>
												$NO_TRANS_END;
												<asp:RequiredFieldValidator ID="vdrTemplateNm" runat="server" ErrorMessage="テンプレート名称を入力して下さい。" ControlToValidate="txtTemplateNm" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderSmallStyle">
												ﾒｰﾙﾀｲﾄﾙ
											</td>
											<td class="tdDataStyle">
												$NO_TRANS_START;
												<asp:TextBox ID="txtMailTitle" runat="server" Width="240px" MaxLength="40"></asp:TextBox>
												$NO_TRANS_END;
												<asp:RequiredFieldValidator ID="vdrMailTitle" runat="server" ErrorMessage="メールタイトルを入力して下さい。" ControlToValidate="txtMailTitle" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderSmallStyle">
												ﾒｰﾙｱﾄﾞﾚｽ(from)
											</td>
											<td class="tdDataStyle">
												<asp:TextBox ID="txtMailAddrFrom" runat="server" Width="140px"></asp:TextBox>
												<asp:RegularExpressionValidator ID="vdeEmailAddr" runat="server" ErrorMessage="メールアドレスをを正しく入力して下さい。" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
													ControlToValidate="txtMailAddrFrom" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderStyle">
												送信先性別
											</td>
											<td class="tdDataStyle">
												<asp:DropDownList ID="lstSexCd" runat="server" DataSourceID="dsSexCd" DataTextField="CODE_NM" DataValueField="CODE" Width="100px">
												</asp:DropDownList>
											</td>
										</tr>
										<asp:PlaceHolder ID="plcWebFace" runat="server">
											<tr>
												<td class="tdHeaderStyle">
													カラーパターン
												</td>
												<td class="tdDataStyle">
													<asp:DropDownList ID="lstWebFaceSeq" runat="server" Width="206px" DataSourceID="dsWebFace" DataTextField="HTML_FACE_NAME" DataValueField="WEB_FACE_SEQ">
													</asp:DropDownList>
												</td>
											</tr>
										</asp:PlaceHolder>
										<asp:PlaceHolder ID="plcMobileMailFontSize" runat="server">
											<tr>
												<td class="tdHeaderStyle">
													HTMLﾒｰﾙﾌｫﾝﾄｻｲｽﾞ
												</td>
												<td class="tdDataStyle">
													<asp:TextBox ID="txtMobileMailFontSize" runat="server" Width="24px" MaxLength="2"></asp:TextBox>&nbsp;0以外の場合HTMLﾒｰﾙの全体に適用されるﾌｫﾝﾄﾀｸﾞが挿入されます。
													<asp:RequiredFieldValidator ID="vdrMobileMailFontSize" runat="server" ErrorMessage="HTMLﾒｰﾙﾌｫﾝﾄｻｲｽﾞを入力して下さい。" ControlToValidate="txtMobileMailFontSize"
														ValidationGroup="Detail">*</asp:RequiredFieldValidator>
												</td>
											</tr>
											<tr>
												<td class="tdHeaderStyle">
													HTMLﾒｰﾙﾊﾞｯｸｶﾗｰ
												</td>
												<td class="tdDataStyle">
													<asp:TextBox ID="txtMobileMailBackColor" runat="server" MaxLength="7" Width="70px"></asp:TextBox>
													<asp:RegularExpressionValidator ID="vdeMobileMailBackColor" runat="server" ErrorMessage="#で始まる6桁のカラーコードを入力して下さい。" ValidationExpression="#[a-fA-F0-9]{6}$"
														ControlToValidate="txtMobileMailBackColor" ValidationGroup="Detail">*</asp:RegularExpressionValidator>
												</td>
											</tr>
										</asp:PlaceHolder>
										<tr>
											<td class="tdHeaderStyle">
												HTMLﾀｲﾄﾙ
											</td>
											<td class="tdDataStyle">
												$NO_TRANS_START;
												<asp:TextBox ID="txtHtmlDocTitle" runat="server" Width="140px" MaxLength="30"></asp:TextBox>
												$NO_TRANS_END;
												<asp:RequiredFieldValidator ID="vdrHtmlDocTitle" runat="server" ErrorMessage="HTMLﾀｲﾄﾙを入力して下さい。" ControlToValidate="txtHtmlDocTitle" ValidationGroup="Detail">*</asp:RequiredFieldValidator>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderStyle">
												<%= DisplayWordUtil.Replace("出演者オリジナル作成")%>
											</td>
											<td class="tdDataStyle">
												<asp:CheckBox ID="chkCastCreateOrgFlag" runat="server" Text="許可する" />
											</td>
										</tr>
										<tr>
											<td class="tdHeaderStyle">
												添付種別
											</td>
											<td class="tdDataStyle">
												<asp:RadioButton ID="rdoAttachedNone" runat="server" GroupName="Attached" Text="添付なし" Checked="True">
												</asp:RadioButton>
												<asp:RadioButton ID="rdoAttachedPic" runat="server" GroupName="Attached" Text="写真">
												</asp:RadioButton>
												<asp:RadioButton ID="rdoAttachedMovie" runat="server" GroupName="Attached" Text="動画">
												</asp:RadioButton>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderStyle">
												添付方法
											</td>
											<td class="tdDataStyle">
												<asp:CheckBox ID="chkStock" runat="server" Text="ｽﾄｯｸ">
												</asp:CheckBox>
												<asp:CheckBox ID="chkMobile" runat="server" Text="携帯電話">
												</asp:CheckBox>
												<asp:CustomValidator ID="vdcMailAttachedMethod1" runat="server" ErrorMessage="*どちらかにﾁｪｯｸを入れて下さい。" OnServerValidate="vdcMailAttachedMethod1_ServerValidate"
													ValidationGroup="Detail" Display="Dynamic">*どちらかにﾁｪｯｸを入れて下さい。</asp:CustomValidator>
												<asp:CustomValidator ID="vdcMailAttachedMethod2" runat="server" ErrorMessage="*両方にチェックを入れないで下さい。" OnServerValidate="vdcMailAttachedMethod2_ServerValidate"
													ValidationGroup="Detail" Display="Dynamic">*両方にチェックを入れないで下さい。</asp:CustomValidator>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderStyle">
												テキストメール
											</td>
											<td class="tdDataStyle">
												<asp:CheckBox ID="chkTextMailFlag" runat="server">
												</asp:CheckBox>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderStyle">
												非表示区分
											</td>
											<td class="tdDataStyle">
												<asp:CheckBox ID="chkNonDispInMailBox" runat="server" Text="WEBｻｲﾄのﾒｰﾙBOXでは非表示にする" /><br />
												<asp:CheckBox ID="chkNonDispInDecomail" runat="server" Text="ﾃﾞｺﾒでは非表示にする" />
											</td>
										</tr>
										<tr>
											<td class="tdHeaderStyle">
												携帯ﾒｰﾙ強制送信
											</td>
											<td class="tdDataStyle">
												<asp:CheckBox ID="chkForceTxMobileMailFlag" runat="server">
												</asp:CheckBox>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderStyle">
												ﾒｰﾙ本文を省略する
											</td>
											<td class="tdDataStyle">
												<asp:CheckBox ID="chkMailDocShortenFlag" runat="server">
												</asp:CheckBox>
											</td>
										</tr>
										<tr>
											<td class="tdHeaderSmallStyle2" colspan="2">
												文章
											</td>
										</tr>
										<tr>
											<td class="tdDataStyle" colspan="2">
												<ajaxToolkit:TabContainer ID="tabDoc" runat="server">
													<ajaxToolkit:TabPanel ID="tpgHtml" runat="server" HeaderText="HTMLメール">
														<ContentTemplate>
															$NO_TRANS_START;
															<pin:pinEdit ID="txtHtmlDoc" runat="server" UserMode="HTML" DisplayStyle="Style5" DocumentView="True" HtmlMode="Body" Ruler="False" ReturnMode="BR" SpellMode="Inline"
																ToolbarConfiguration="ToolbarDesigner" Toolbar="T09SE10116012136267;T22232427282930313233343536;B53SE54SE55" FormatMode="Classic" BorderWidth="1px"
																Height="500px" Width="99%" DocumentWidth="230" Font-Bold="False" FontSizeType="px" RelativeBaseImageUrl="/" RelativeBaseLinkUrl="/" RelativeImageRoot="/">
															</pin:pinEdit>
															<asp:TextBox ID="txtHtmlDocText" runat="server" TextMode="MultiLine" Width="99%" Height="500px">
															</asp:TextBox>
															$NO_TRANS_END;
														</ContentTemplate>
													</ajaxToolkit:TabPanel>
													<ajaxToolkit:TabPanel ID="tpgText" runat="server" HeaderText="テキストメール">
														<ContentTemplate>
															$NO_TRANS_START;
															<asp:TextBox ID="txtTextDoc" runat="server" TextMode="MultiLine" Width="99%" Height="500px">
															</asp:TextBox>
															$NO_TRANS_END;
														</ContentTemplate>
													</ajaxToolkit:TabPanel>
												</ajaxToolkit:TabContainer>
											</td>
										</tr>
									</table>
									<asp:Button runat="server" ID="btnUpdate2" Text="更新" CssClass="seektopbutton" OnClick="btnUpdate_Click" ValidationGroup="Detail" />
									<asp:Button runat="server" ID="btnCancel2" Text="キャンセル" CssClass="seektopbutton" OnClick="btnCancel_Click" CausesValidation="False" />
									<asp:Button runat="server" ID="btnDelete2" Text="削除" CssClass="delBtnStyle" OnClick="btnDelete_Click" ValidationGroup="Key" />
								</asp:Panel>
							</fieldset>
						</td>
					</tr>
				</table>
			</fieldset>
		</asp:Panel>
		<fieldset class="f_clear">
			<legend>[メールテンプレート一覧]</legend>
			<table border="0" style="width: 600px" class="tableStyle">
				<tr>
					<td class="tdHeaderStyle">
						サイトコード
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px" AutoPostBack="True"
							OnSelectedIndexChanged="lstSeekSiteCd_SelectedIndexChanged">
						</asp:DropDownList>
					</td>
				</tr>
				<tr>
					<td class="tdHeaderStyle2">
						メールテンプレート種別
					</td>
					<td class="tdDataStyle">
						<asp:DropDownList ID="lstSeekMailTemplateType" runat="server" DataSourceID="dsMailType" DataTextField="MAIL_TYPE_NM" DataValueField="MAIL_TYPE" Width="240px">
						</asp:DropDownList>
					</td>
				</tr>
			</table>
			<asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
			<asp:Button runat="server" ID="btnRegist" Text="メールテンプレート追加" CssClass="seekbutton" CausesValidation="False" OnClick="btnRegist_Click" />
			<asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" CausesValidation="False" OnClick="btnCSV_Click" />
			<br />
			<br />
			<asp:Button runat="server" ID="btnBulkDelete" Text="一括削除" OnClientClick='return confirm("削除してもよろしいですか？");' CssClass="seektopbutton" OnClick="btnBulkDelete_Click" />
			<br /><br />
			<asp:GridView ID="grdTemplate" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="dsMailTemplate" AllowSorting="True" SkinID="GridViewColor">
				<Columns>
					<asp:TemplateField HeaderText="削除">
						<ItemTemplate>
							<asp:CheckBox ID="chkBulkDelete" runat="server" />
							<asp:HiddenField ID="hdnMailTemplateNo" runat="server" Value='<%# Eval("MAIL_TEMPLATE_NO") %>' />
						</ItemTemplate>
						<ItemStyle HorizontalAlign="center" />
					</asp:TemplateField>
					<asp:TemplateField HeaderText="テンプレート名称" SortExpression="TEMPLATE_NM">
						<ItemTemplate>
							<asp:LinkButton ID="lnkTemplate" runat="server" Text='<%# string.Format("{0} {1}",Eval("MAIL_TEMPLATE_NO"),Eval("TEMPLATE_NM")) %>' CommandArgument='<%# string.Format("{0}:{1}:{2}:{3}:{4}",Eval("SITE_CD"),Eval("MAIL_TEMPLATE_NO"),Eval("HTML_DOC_SEQ"),Eval("WEB_FACE_SEQ"),Eval("MAIL_TEMPLATE_TYPE"))  %>'
								OnCommand="lnkTemplate_Command" CausesValidation="False">
							</asp:LinkButton>
						</ItemTemplate>
						<ItemStyle HorizontalAlign="Left" />
					</asp:TemplateField>
					<asp:BoundField DataField="MAIL_TEMPLATE_TYPE_NM" HeaderText="メールテンプレート種別" SortExpression="MAIL_TEMPLATE_TYPE_NM">
						<ItemStyle HorizontalAlign="Left" />
					</asp:BoundField>
					<asp:BoundField DataField="DOC" HeaderText="HTML文章" SortExpression="DOC">
						<ItemStyle HorizontalAlign="Left" Wrap="True" />
					</asp:BoundField>
					<asp:BoundField DataField="SEX_NM" HeaderText="送信先性別" SortExpression="SEX_NM">
						<ItemStyle HorizontalAlign="Left" />
					</asp:BoundField>
                    <asp:TemplateField HeaderText="サイズ(KB)">
                        <ItemTemplate>
                            <asp:Label ID="lblSize" runat="server" Text='<%# GetMimeSizeMark(Eval("MIME_SIZE")) %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="right" />
                    </asp:TemplateField>
				</Columns>
				<PagerSettings Mode="NumericFirstLast" />
			</asp:GridView>
			<asp:Panel runat="server" ID="pnlCount">
				<a class="reccount">Record Count
					<%#GetRecCount() %>
				</a>
				<br />
				<a class="reccount">Current viewing page
					<%=grdTemplate.PageIndex + 1%>
					of
					<%=grdTemplate.PageCount%>
				</a>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsMailType" runat="server" SelectMethod="GetList" TypeName="MailType"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSiteHtmlDoc" runat="server" SelectMethod="GetListByDocSeq" TypeName="SiteHtmlDoc" OnSelecting="dsSiteHtmlDoc_Selecting" OnSelected="dsSiteHtmlDoc_Selected">
		<SelectParameters>
			<asp:Parameter Name="pHtmlDocSeq" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsMailTemplate" runat="server" SelectMethod="GetPageCollection" TypeName="MailTemplate" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsMailTemplate_Selected" OnSelecting="dsMailTemplate_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pMailTemplateType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSexCd" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="36" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsWebFace" runat="server" SelectMethod="GetList" TypeName="WebFace"></asp:ObjectDataSource>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtCopyMailTemplateNo" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtMobileMailFontSize" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender3" runat="server" TargetControlID="btnUpdate2" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender4" runat="server" TargetControlID="btnDelete2" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdrMailTitle" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrTemplateNm" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdrMailTemplateNo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdeMailTemplateNo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdrHtmlDocTitle" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdcMailAttachedMethod1" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vdcMailAttachedMethod2" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8" TargetControlID="vdrHtmlDocSubSeq" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9" TargetControlID="vdaHtmlDocSubSeq" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender13" TargetControlID="vdrMobileMailFontSize" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10" TargetControlID="vdeEmailAddr" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11" TargetControlID="vdrCopyMailTemplateNo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender12" TargetControlID="vdeCopyMailTemplateNo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender14" TargetControlID="vdeMobileMailBackColor" HighlightCssClass="validatorCallout" />
</asp:Content>
