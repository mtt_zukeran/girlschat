<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="MailInquiry.aspx.cs" Inherits="Mail_MailInquiry" Title="メール記録" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="iBridCommLib" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="メール記録"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <fieldset class="fieldset">
            <legend>[検索条件]</legend>
            <asp:Panel runat="server" ID="pnlKey">
                <table border="0" style="width: 900px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            サイト
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="180px">
                            </asp:DropDownList>
                        </td>
                        <td class="tdHeaderStyle">
                            メール種別
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstMailType" runat="server" DataSourceID="dsMailType" DataTextField="CODE_NM"
                                DataValueField="CODE" Width="240px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            送受信日
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtReportDayFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
                            <asp:TextBox ID="txtReportTimeFrom" runat="server" Width="15px" MaxLength="2"></asp:TextBox>
                            〜&nbsp;
                            <asp:TextBox ID="txtReportDayTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
                            <asp:TextBox ID="txtReportTimeTo" runat="server" Width="15px" MaxLength="2"></asp:TextBox>
                            <asp:RangeValidator ID="vdrReportTimeFrom" runat="server" ErrorMessage="送受信時Fromを正しく入力して下さい。"
                                ControlToValidate="txtReportTimeFrom" MaximumValue="23" MinimumValue="00" Type="Integer"
                                ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                            <asp:RangeValidator ID="vdrReportTimeTo" runat="server" ErrorMessage="送受信時Toを正しく入力して下さい。"
                                ControlToValidate="txtReportTimeTo" MaximumValue="23" MinimumValue="00" Type="Integer"
                                ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="vdrReportDayFrom" runat="server" ErrorMessage="送受信日Fromを入力して下さい。"
                                ControlToValidate="txtReportDayFrom" ValidationGroup="Key" Display="Dynamic">*</asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="vdeReportDayFrom" runat="server" ErrorMessage="送受信日Fromを正しく入力して下さい。"
                                ControlToValidate="txtReportDayFrom" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
                                Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                            <asp:RangeValidator ID="vdeReportDayTo" runat="server" ErrorMessage="送受信日Toを正しく入力して下さい。"
                                ControlToValidate="txtReportDayTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
                                Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                            <asp:CompareValidator ID="vdcReportDayFrom" runat="server" ErrorMessage="大小関係が正しくありません。"
                                ControlToCompare="txtReportDayFrom" ControlToValidate="txtReportDayTo" Operator="GreaterThanEqual"
                                ValidationGroup="Key" Display="Dynamic">*</asp:CompareValidator>
                        </td>
                        <td class="tdHeaderStyle">
                            Paging Off
                        </td>
                        <td class="tdDataStyle">
                            <asp:CheckBox ID="chkPagingOff" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            送信者ＩＤ
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtTxLoginId" runat="server" Width="70px"></asp:TextBox>
                        </td>
                        <td class="tdHeaderStyle">
                            送信者ｷｬﾗｸﾀｰＮｏ
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtTxUserCharNo" runat="server" Width="35px" MaxLength="2"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            受信者ＩＤ
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtRxLoginId" runat="server" Width="70px"></asp:TextBox>
                        </td>
                        <td class="tdHeaderStyle">
                            受信者ｷｬﾗｸﾀｰＮｏ
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtRxUserCharNo" runat="server" Width="35px" MaxLength="2"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle">
                            一括送信種別
                        </td>
                        <td class="tdDataStyle">
                            <asp:RadioButtonList ID="rdoWithBatchMail" runat="server" RepeatDirection="horizontal">
                                <asp:ListItem Text="通常送信のみ" Value="0" Selected="true"></asp:ListItem>
                                <asp:ListItem Text="一括送信のみ" Value="1"></asp:ListItem>
                                <asp:ListItem Text="全て" Value=""></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td class="tdHeaderStyle2">
                            添付種別
                        </td>
                        <td class="tdDataStyle">
                            <asp:CheckBoxList ID="chkAttachedType" runat="server" RepeatDirection="horizontal">
                                <asp:ListItem Text="画像添付" Value="1"></asp:ListItem>
                                <asp:ListItem Text="動画添付" Value="2"></asp:ListItem>
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            キーワード(空白区切り)
                        </td>
                        <td class="tdDataStyle" colspan="3">
                            <asp:TextBox ID="txtKeyword" runat="server" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
                <asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" ValidationGroup="Key" OnClick="btnCSV_Click" />
            </asp:Panel>
        </fieldset>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[メール記録]</legend>
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="430px">
                    <asp:GridView ID="grdMail" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        AllowSorting="True" SkinID="GridViewColor" OnSorting="grdMail_Sorting">
                        <Columns>
                            <asp:TemplateField HeaderText="作成日時" SortExpression="CREATE_DATE">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("CREATE_DATE")%>'></asp:Label><br />
                                    <asp:Label ID="lblMailTypeNm" runat="server" Text='<%# Eval("MAIL_TYPE_NM")%>'></asp:Label><br />
                                    <asp:LinkButton ID="lnkDel" runat="server" CommandArgument='<%# Eval("MAIL_SEQ") %>'
                                        Text="削除" OnCommand="lnkDelMail_Command"></asp:LinkButton><br />
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="送受信者">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblTxLoginId" runat="server" NavigateUrl='<%# GetViewUrl(Eval("TX_SEX_CD"),Eval("TX_SITE_CD"),Eval("TX_LOGIN_ID")) %>'
                                        Text='<%# Eval("TX_LOGIN_ID") %>' Enabled='<%#IsAreaManager(Eval("TX_SEX_CD"),Eval("TX_MANAGER_SEQ")) %>'></asp:HyperLink>
                                    <asp:HyperLink ID="lblRxLoginId" runat="server" NavigateUrl='<%# GetViewUrl(Eval("RX_SEX_CD"),Eval("RX_SITE_CD"),Eval("RX_LOGIN_ID")) %>'
                                        Text='<%# Eval("RX_LOGIN_ID") %>' Enabled='<%#IsAreaManager(Eval("RX_SEX_CD"),Eval("RX_MANAGER_SEQ")) %>'></asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="内容">
                                <ItemTemplate>
                                    <asp:Label ID="lblReadFlag" runat="server" Text='<%# GetReadNm(Eval("READ_FLAG")) %>'></asp:Label>
                                    <asp:Label ID="lblMailTitle" runat="server" Text='<%# GetMailTitle(Eval("MAIL_TITLE"),Eval("ORIGINAL_TITLE")) %>'></asp:Label><br />
                                    <asp:Label ID="lblDoc" runat="server" Text='<%# GetMailDoc(Eval("MAIL_DOC1"),Eval("MAIL_DOC2"),Eval("MAIL_DOC3"),Eval("MAIL_DOC4"),Eval("MAIL_DOC5"),Eval("ORIGINAL_DOC1"),Eval("ORIGINAL_DOC2"),Eval("ORIGINAL_DOC3"),Eval("ORIGINAL_DOC4"),Eval("ORIGINAL_DOC5") ) %>'
                                        Width="500px"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="種別">
                                <ItemTemplate>
                                    <asp:Panel ID="pnlOpenPicViewer" runat="server" Visible='<%# iBridUtil.GetStringValue(Eval("ATTACHED_OBJ_TYPE")).Equals(ViCommConst.ATTACH_PIC_INDEX.ToString()) %>'>
                                        <asp:HyperLink ID="lnkOpenPicViewer" runat="server" NavigateUrl="<%# GenerateOpenPicScript(Container.DataItem) %>">
										画像添付
                                        </asp:HyperLink>
                                        <br />
                                        <a href="<%# GenerateOpenPicScript(Container.DataItem) %>">
                                            <asp:Image runat="server" ID="imbPic" ImageUrl="<%# GenerateSmallPicPath(Container.DataItem) %>"
                                                Width="80px" />
                                        </a>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlOpenMovieViewer" runat="server" Visible='<%# iBridUtil.GetStringValue(Eval("ATTACHED_OBJ_TYPE")).Equals(ViCommConst.ATTACH_MOVIE_INDEX.ToString()) %>'>
                                        <asp:HyperLink ID="lnkOpenMovieViewer" runat="server" NavigateUrl="<%# GenerateOpenMovieScript(Container.DataItem) %>">
										動画添付
                                        </asp:HyperLink>
                                        <br />
                                        <a href="<%# GenerateOpenMovieScript(Container.DataItem) %>">
                                            <asp:Image runat="server" ID="imbMovie" ImageUrl="<%# GenerateSmallPicPathMovie(Container.DataItem) %>"
                                                Width="80px" />
                                        </a>
                                    </asp:Panel>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Top" HorizontalAlign="center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="削除">
                                <ItemTemplate>
                                    <asp:Label ID="lblDelMark" runat="server" Text='<%# GetDelMark(Eval("TX_DEL_FLAG"),Eval("RX_DEL_FLAG")) %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle VerticalAlign="Top" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
                &nbsp;
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%#GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%=grdMail.PageIndex + 1%>
                        of
                        <%=grdMail.PageCount%>
                    </a>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <br />
    </div>
    <asp:ObjectDataSource ID="dsMailLog" runat="server" SelectMethod="GetPageCollection"
        TypeName="MailLog" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelected="dsMail_Selected"
        OnSelecting="dsMail_Selecting">
        <SelectParameters>
            <asp:Parameter Name="pSiteCd" Type="String" />
            <asp:Parameter Name="pMailType" Type="String" />
            <asp:Parameter Name="pReportDayFrom" Type="String" />
            <asp:Parameter Name="pReportTimeFrom" Type="String" />
            <asp:Parameter Name="pReportDayTo" Type="String" />
            <asp:Parameter Name="pReportTimeTo" Type="String" />
            <asp:Parameter Name="pTxLoginId" Type="String" />
            <asp:Parameter Name="pTxUserCharNo" Type="String" />
            <asp:Parameter Name="pRxLoginId" Type="String" />
            <asp:Parameter Name="pRxUserCharNo" Type="String" />
            <asp:Parameter Name="pManagerSeq" Type="String" />
            <asp:Parameter Name="pWithBatchMail" Type="String" />
            <asp:Parameter Name="pAttachedType" Type="String" />
            <asp:Parameter Name="pKeyword" Type="String" />
            <asp:Parameter Name="pSortExpression" Type="String" />
            <asp:Parameter Name="pSortDirection" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsMailType" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
        <SelectParameters>
            <asp:Parameter DefaultValue="82" Name="pCodeType" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3"
        TargetControlID="vdrReportDayFrom" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4"
        TargetControlID="vdeReportDayFrom" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
        TargetControlID="vdeReportDayTo" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2"
        TargetControlID="vdrReportTimeFrom" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5"
        TargetControlID="vdrReportTimeTo" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6"
        TargetControlID="vdcReportDayFrom" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:MaskedEditExtender ID="mskReportDayFrom" runat="server" MaskType="Date"
        Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="True" TargetControlID="txtReportDayFrom">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:MaskedEditExtender ID="mskReportDayTo" runat="server" MaskType="Date"
        Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="True" TargetControlID="txtReportDayTo">
    </ajaxToolkit:MaskedEditExtender>
</asp:Content>
