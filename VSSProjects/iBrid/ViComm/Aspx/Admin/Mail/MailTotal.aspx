<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="MailTotal.aspx.cs" Inherits="Mail_MailTotal" Title="出演者別オープンメール集計" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="iBridCommLib" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="出演者別オープンメール集計"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <fieldset class="fieldset">
            <legend>[検索条件]</legend>
            <asp:Panel runat="server" ID="pnlKey">
                <table border="0" style="width: 650px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            サイト
                        </td>
                        <td class="tdDataStyle" style="width: 250px">
                            <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="180px">
                            </asp:DropDownList>
                        </td>
                        <td class="tdHeaderStyle">
                            メール件数下限
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtOffsetCount" runat="server" MaxLength="8" Width="60px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="vdrOffsetCount" runat="server" ErrorMessage="件数下限を入力して下さい。"
                                ControlToValidate="txtOffsetCount" ValidationGroup="Key" Display="Dynamic">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            送信日
                        </td>
                        <td class="tdDataStyle" style="width: 120px">
                            <asp:TextBox ID="txtReportDayFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
                            〜&nbsp;
                            <asp:TextBox ID="txtReportDayTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="vdrReportDayFrom" runat="server" ErrorMessage="送信日Fromを入力して下さい。"
                                ControlToValidate="txtReportDayFrom" ValidationGroup="Key" Display="Dynamic">*</asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="vdeReportDayFrom" runat="server" ErrorMessage="送信日Fromを正しく入力して下さい。"
                                ControlToValidate="txtReportDayFrom" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
                                Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                            <asp:RangeValidator ID="vdeReportDayTo" runat="server" ErrorMessage="送信日Toを正しく入力して下さい。"
                                ControlToValidate="txtReportDayTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
                                Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                            <asp:CompareValidator ID="vdcReportDay" runat="server" ErrorMessage="大小関係が正しくありません。"
                                ControlToCompare="txtReportDayFrom" ControlToValidate="txtReportDayTo" Operator="GreaterThanEqual"
                                ValidationGroup="Key" Display="Dynamic">*</asp:CompareValidator>
                        </td>
                        <td class="tdHeaderStyle2">
                        </td>
                        <td class="tdDataStyle">
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
            </asp:Panel>
        </fieldset>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[メール記録]</legend>
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Width="650px" Height="430px">
                    <asp:GridView ID="grdMailTotal" runat="server" AllowPaging="True" AutoGenerateColumns="False" SkinID="GridViewColor">
                        <Columns>
                            <asp:TemplateField HeaderText="ログインID">
                                <ItemTemplate>
                                    <asp:Label ID="lblCastLoginId" runat="server" Text='<%# Eval("CAST_LOGIN_ID")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ハンドル名">
                                <ItemTemplate>
                                    <asp:Label ID="lblCastHandleNm" runat="server" Text='<%# Eval("CAST_HANDLE_NM")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="オープンメール送信件数">
                                <ItemTemplate>
                                    <asp:Label ID="lblMailCount" runat="server" Text='<%# Eval("MAILCOUNT")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
                &nbsp;
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <br />
    </div>
    <asp:ObjectDataSource ID="dsMailTotal" runat="server" TypeName="MailTotal"
        SelectMethod="GetPageCollection" SelectCountMethod="GetPageCount" EnablePaging="True"
        OnSelected="dsMailTotal_Selected" OnSelecting="dsMailTotal_Selecting">
        <SelectParameters>
            <asp:Parameter Name="pSiteCd" Type="String" />
            <asp:Parameter Name="pReportDateFrom" Type="String" />
            <asp:Parameter Name="pReportDateTo" Type="String" />
            <asp:Parameter Name="pOffsetCount" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
        TargetControlID="vdrReportDayFrom" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2"
        TargetControlID="vdeReportDayFrom" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3"
        TargetControlID="vdeReportDayTo" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4"
        TargetControlID="vdcReportDay" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:MaskedEditExtender ID="mskReportDayFrom" runat="server" MaskType="Date"
        Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="True" TargetControlID="txtReportDayFrom">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:MaskedEditExtender ID="mskReportDayTo" runat="server" MaskType="Date"
        Mask="9999/99/99" UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="True" TargetControlID="txtReportDayTo">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtenderOffsetCount" runat="server"
        Enabled="true" FilterType="Numbers" TargetControlID="txtOffsetCount">
    </ajaxToolkit:FilteredTextBoxExtender>
    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtenderOffsetCount" runat="Server"
        HighlightCssClass="validatorCallout" TargetControlID="vdrOffsetCount">
    </ajaxToolkit:ValidatorCalloutExtender>
</asp:Content>
