﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;

public partial class Mail_PointMailOutcomeList : System.Web.UI.Page {
	private int comebackCount;
	private int comebackSettleCount;
	private int transferPoint;

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.InitPage();

			this.pnlInfo.Visible = true;
			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		this.pnlInfo.Visible = true;
		this.GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void vdcReportDay_ServerValidate(object source,ServerValidateEventArgs args) {
		if (this.IsValid) {
			if (lstReportFromYYYY.SelectedValue.Equals(string.Empty) &&
				lstReportFromMM.SelectedValue.Equals(string.Empty) &&
				lstReportFromDD.SelectedValue.Equals(string.Empty) &&
				lstReportToYYYY.SelectedValue.Equals(string.Empty) &&
				lstReportToMM.SelectedValue.Equals(string.Empty) &&
				lstReportToDD.SelectedValue.Equals(string.Empty)) {
				//全て未選択ならばOK
			} else {
				DateTime dtFrom;
				DateTime dtTo;
				if (!DateTime.TryParse(string.Format("{0}/{1}/{2}",lstReportFromYYYY.SelectedValue,lstReportFromMM.SelectedValue,lstReportFromDD.SelectedValue),out dtFrom)) {
					this.vdcReportDay.ErrorMessage = "送信日Fromが不正です。";
					args.IsValid = false;
					return;
				}
				if (!DateTime.TryParse(string.Format("{0}/{1}/{2}",lstReportToYYYY.SelectedValue,lstReportToMM.SelectedValue,lstReportToDD.SelectedValue),out dtTo)) {
					this.vdcReportDay.ErrorMessage = "送信日Toが不正です。";
					args.IsValid = false;
				}
			}
		}

		if (args.IsValid) {
			this.vdcReportDay.ErrorMessage = string.Empty;
		}
	}

	protected void grdPointMailOutcome_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			comebackCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"COME_BACK_COUNT"));
			comebackSettleCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"COME_BACK_SETTLE_COUNT"));
			transferPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"MAN_TRANSFER_POINT"));
		} else if (e.Row.RowType == DataControlRowType.Footer) {
			e.Row.Cells[0].Text = "計";
			e.Row.Cells[2].Text = "-";
			e.Row.Cells[3].Text = comebackCount.ToString();
			e.Row.Cells[4].Text = comebackSettleCount.ToString();
			e.Row.Cells[5].Text = "-";
			e.Row.Cells[6].Text = transferPoint.ToString();
		}
	}

	protected void dsPointMailOutcome_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		string sReportFromDay = string.Empty;
		string sReportToDay = string.Empty;

		if (!lstReportFromYYYY.SelectedValue.Equals(string.Empty)) {
			sReportFromDay = lstReportFromYYYY.SelectedValue + "/" + lstReportFromMM.SelectedValue + "/" + lstReportFromDD.SelectedValue;
		}
		if (!lstReportToYYYY.SelectedValue.Equals(string.Empty)) {
			sReportToDay = lstReportToYYYY.SelectedValue + "/" + lstReportToMM.SelectedValue + "/" + lstReportToDD.SelectedValue;
		}

		e.InputParameters["pReportDayFrom"] = sReportFromDay;
		e.InputParameters["pReportDayTo"] = sReportToDay;
		e.InputParameters["pRemarks"] = this.txtSeekRemarks.Text.Trim();
	}

	private void InitPage() {
		this.txtSeekRemarks.Text = string.Empty;
		this.lstSeekSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		if (!IsPostBack) {
			SysPrograms.SetupFromToDay(lstReportFromYYYY,lstReportFromMM,lstReportFromDD,lstReportToYYYY,lstReportToMM,lstReportToDD,false);
			lstReportFromYYYY.Items.Insert(0,new ListItem("----",""));
			lstReportFromMM.Items.Insert(0,new ListItem("---",""));
			lstReportFromDD.Items.Insert(0,new ListItem("---",""));
			lstReportToYYYY.Items.Insert(0,new ListItem("----",""));
			lstReportToMM.Items.Insert(0,new ListItem("---",""));
			lstReportToDD.Items.Insert(0,new ListItem("---",""));
		}
		lstReportFromYYYY.SelectedIndex = 0;
		lstReportFromMM.SelectedIndex = 0;
		lstReportFromDD.SelectedIndex = 0;
		lstReportToYYYY.SelectedIndex = 0;
		lstReportToMM.SelectedIndex = 0;
		lstReportToDD.SelectedIndex = 0;
	}

	private void GetList() {
		this.grdPointMailOutcome.DataSourceID = "dsPointMailOutcome";
		this.grdPointMailOutcome.DataBind();
	}

	protected string GetRemarks(object pRemarks) {
		return iBridUtil.GetStringValue(pRemarks).Replace(Environment.NewLine,"<br />");
	}
}
