﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 管理者ﾒｰﾙ送信履歴
--	Progaram ID		: AdminMailTxHistoryList.aspx
--
--  Creation Date	: 2011.02.15
--  Creater			: K.Itoh
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;

using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class Mail_TxInfoMailHistory:System.Web.UI.Page {
	private int recCount = 0;

	/// <summary>データ行の結合用</summary>
	private string sBufTxReservationDate = string.Empty;
	private string sBufRxSexCd = string.Empty;
	private string sBufMailTemplateNo = string.Empty;
	private int iRowIdx = 0;
	private int iRowSpan = 1;

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();

			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["sexcd"]))) {
				rdoSexCd.SelectedValue = iBridUtil.GetStringValue(Request.QueryString["sexcd"]);
			}
			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["sdate"]))) {
				DateTime oDT = Convert.ToDateTime(iBridUtil.GetStringValue(Request.QueryString["sdate"]));
				txtSendDayFrom.Text = oDT.ToString("yyyy/MM/dd");
				txtSendDayTo.Text = oDT.ToString("yyyy/MM/dd");
			}
			if (!string.IsNullOrEmpty(Request.QueryString.ToString())) {
				GetList();
			}
		}
	}
	private void InitPage() {
		ClearField();
	}

	/// <summary>
	/// 検索ボタンのクリックイベント
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (IsValid) {
			GetList();
		}
	}

	/// <summary>
	/// クリアボタンのクリックイベント
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void btnClear_Click(object sender,EventArgs e) {
		ClearField();
	}

	private void ClearField() {
		recCount = 0;
		grdData.PageIndex = 0;
		grdData.DataSourceID = "";
		pnlInfo.Visible = false;
	}
	private void GetList() {
		// データ行の結合用変数を初期化
		sBufTxReservationDate = string.Empty;
		sBufRxSexCd = string.Empty;
		sBufMailTemplateNo = string.Empty;
		iRowIdx = 0;
		iRowSpan = 1;

		pnlInfo.Visible = true;
		grdData.PageIndex = 0;
		grdData.DataSourceID = "dsData";
		grdData.DataBind();
		pnlCount.DataBind();
	}

	/// <summary>
	/// データ検索（パラメータ設定）
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void dsData_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		TxInfoMailHistory.SearchCondition oSearchCondition = new TxInfoMailHistory.SearchCondition();
		// サイトCD
		oSearchCondition.SiteCd = PwViCommConst.MAIN_SITE_CD;
		// 送信日時（From）
		if (!string.IsNullOrEmpty(txtSendDayFrom.Text) && !string.IsNullOrEmpty(txtSendTimeFrom.Text)) {
			oSearchCondition.SendDateFrom = string.Format("{0} {1}:00:00",txtSendDayFrom.Text,txtSendTimeFrom.Text.PadLeft(2,'0'));
		}
		else if (!string.IsNullOrEmpty(txtSendDayFrom.Text)) {
			oSearchCondition.SendDateFrom = string.Format("{0} 00:00:00",txtSendDayFrom.Text);
		}
		// 送信日時（To）
		if (!string.IsNullOrEmpty(txtSendDayTo.Text) && !string.IsNullOrEmpty(txtSendTimeTo.Text)) {
			oSearchCondition.SendDateTo = string.Format("{0} {1}:59:59",txtSendDayTo.Text,txtSendTimeTo.Text.PadLeft(2,'0'));
		}
		else if (!string.IsNullOrEmpty(txtSendDayTo.Text)) {
			oSearchCondition.SendDateTo = string.Format("{0} 23:59:59",txtSendDayTo.Text);
		}
		// 送信済みフラグ
		oSearchCondition.SentFlag = rdoSendFlag.SelectedValue;
		// 受信者性別
		oSearchCondition.SexCd = rdoSexCd.SelectedValue;
		// 送信方法
		oSearchCondition.ReservationFlag = rdoReservationFlag.SelectedValue;

		e.InputParameters[0] = oSearchCondition;
	}

	/// <summary>
	/// 検索完了（検索結果数設定）
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void dsData_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null && e.ReturnValue.GetType().Equals(recCount.GetType())) {
			recCount = int.Parse(e.ReturnValue.ToString());
		}
	}

	/// <summary>
	/// データ設定処理
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void grdData_RowDataBound(object sender,GridViewRowEventArgs e) {
		// データが存在しない場合
		if (grdData.Controls.Count <= 0) {
			return;
		}

		if (e.Row.RowType == DataControlRowType.DataRow) {
			string sSexCd = DataBinder.Eval(e.Row.DataItem,"RX_SEX_CD").ToString();
			string sMailTemplateNo = DataBinder.Eval(e.Row.DataItem,"MAIL_TEMPLATE_NO").ToString();
			string sTxReservationDate = DataBinder.Eval(e.Row.DataItem,"RESERVATION_SEND_DATE","{0:yyyy/MM/dd}");

			// 詳細列の表示文字を送信状態により設定
			PlaceHolder phDtl = (PlaceHolder)e.Row.Cells[e.Row.Cells.Count - 2].FindControl("phDtl");
			if (phDtl != null) {
				string sSentFlag = DataBinder.Eval(e.Row.DataItem,"SENT_FLAG").ToString();
				string sReservationFlag = DataBinder.Eval(e.Row.DataItem,"RESERVATION_FLAG").ToString();

				// 通常送信の場合
				if (sReservationFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
					Label lblDtl = new Label();
					lblDtl.Text = "-";
					phDtl.Controls.Add(lblDtl);
				}
				// 予約送信の場合
				else {
					// 未送信の場合
					if (sSentFlag.Equals(ViCommConst.FLAG_OFF_STR)) {
						string sSeq = DataBinder.Eval(e.Row.DataItem,"RESERVATION_MAIL_HISTORY_SEQ").ToString();
						string sRNo = DataBinder.Eval(e.Row.DataItem,"REVISION_NO").ToString();
						HyperLink lnkEdit = new HyperLink();
						lnkEdit.Text = "確認";
						if (sSexCd.Equals(ViCommConst.MAN)) {
							lnkEdit.NavigateUrl = string.Format("../Man/ManInquiry.aspx?rmhseq={0}&revno={1}",sSeq,sRNo);
						} else {
							lnkEdit.NavigateUrl = string.Format("../Cast/CastCharacterInquiry.aspx?rmhseq={0}&revno={1}",sSeq,sRNo);
						}
						phDtl.Controls.Add(lnkEdit);
					}
					// 送信済みの場合
					else {
						Label lblSent = new Label();
						lblSent.Text = "送信完了";
						phDtl.Controls.Add(lblSent);
					}
				}
			}

			// 上の行と結合しない場合
			if (!sBufTxReservationDate.Equals(sTxReservationDate)
				|| !sBufRxSexCd.Equals(sSexCd)
				|| !sBufMailTemplateNo.Equals(sMailTemplateNo)
			) {
				sBufTxReservationDate = sTxReservationDate;
				sBufRxSexCd = sSexCd;
				sBufMailTemplateNo = sMailTemplateNo;
				iRowIdx = e.Row.DataItemIndex;
				iRowSpan = 1;
			}
			// 上の行と結合する場合
			else {
				iRowSpan++;

				// 送信数～アクセス率を削除&結合
				// (前方から削除するとインデックス値がずれるため後方から削除する)
				e.Row.Cells.RemoveAt(10);
				e.Row.Cells.RemoveAt(9);
				e.Row.Cells.RemoveAt(8);
				e.Row.Cells.RemoveAt(7);
				grdData.Rows[iRowIdx].Cells[7].RowSpan = iRowSpan;
				grdData.Rows[iRowIdx].Cells[8].RowSpan = iRowSpan;
				grdData.Rows[iRowIdx].Cells[9].RowSpan = iRowSpan;
				grdData.Rows[iRowIdx].Cells[10].RowSpan = iRowSpan;
			}
		}
	}

	/// <summary>
	/// 検索結果数取得
	/// </summary>
	/// <returns></returns>
	protected int GetRecCount() {
		return recCount;
	}

	/// <summary>
	/// メールテンプレート画面へのリンクURL取得
	/// </summary>
	/// <param name="sSiteCd"></param>
	/// <param name="sMailTemplateNo"></param>
	/// <returns></returns>
	protected string GetUrlMailTemplate(object pSiteCd,object pMailTemplateNo) {
		return string.Format("../Mail/MailTemplateList.aspx?sitecd={0}&tno={1}",pSiteCd.ToString(),pMailTemplateNo.ToString());
	}

	/// <summary>
	/// メール送信サーバ名取得
	/// </summary>
	/// <param name="pMailServer"></param>
	/// <returns></returns>
	protected string GetMailServerNm(object pMailServer) {
		string sMailServer = pMailServer.ToString();
		if (string.IsNullOrEmpty(sMailServer)) {
			return string.Empty;
		}
		return sMailServer.Equals(ViCommConst.MailServer.BEAM) ? "BEAM" : "MULLER3";
	}

	/// <summary>
	/// 削除リンクのクリックイベント
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void lnkDelHistory_Command(object sender,CommandEventArgs e) {
		string[] aArgs = e.CommandArgument.ToString().Split(':');
		string sSeq = aArgs[0];
		string sRevisionNo = aArgs[1];
		TxInfoMailHistory oTxInfoMailHistory = new TxInfoMailHistory();
		TxInfoMailHistory.MainteData oMainteData = new TxInfoMailHistory.MainteData();
		oTxInfoMailHistory.Mainte(oMainteData,sRevisionNo,sSeq,ViCommConst.FLAG_ON_STR);
		GetList();
	}

	/// <summary>
	/// アクセス率を算出して返す
	/// </summary>
	/// <param name="pTxMailCount"></param>
	/// <param name="pUniqueAccessCount"></param>
	/// <returns></returns>
	protected string GetUniqueAccessRate(string pTxMailCount,string pUniqueAccessCount) {
		double dTxMailCount;
		double dUniqueAccessCount;
		string sValue = "-";

		if (double.TryParse(pTxMailCount,out dTxMailCount) && double.TryParse(pUniqueAccessCount,out dUniqueAccessCount) && dTxMailCount > 0) {
			double dValue = dUniqueAccessCount / dTxMailCount * 100;

			dValue = Math.Round(dValue,2);
			sValue = string.Format("{0}%",dValue.ToString());
		}

		return sValue;
	}
}
