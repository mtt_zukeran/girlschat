﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 男性メール配信スケジュール
--	Progaram ID		: MailScheduleMainte
--
--  Creation Date	: 2010.05.21
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using iBridCommLib;
using ViComm;

public partial class Mail_MailScheduleMainte:Page {
	private string RecCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
			tagNA_FLAG.DataBind();
		}
	}

	private void InitPage() {
		ClearField();
		pnlPattern.Visible = false;
		trCastLoginId.Visible = false;
		btnDelete.Visible = false;
		grdPattern.PageSize = 100;
		grdPattern.DataBind();

		tagCREDIT_MEASURED_RATE_TARGET.Visible = false;
		tagPOINT_AFFILIATE_TOTAL_COUNT.Visible = false;
		trOnlyMashup.Visible = false;
		{
			lblLastBlackDay.Visible = false;
			lblLastBlackDay2.Visible = false;
			tagLAST_BLACK_DAY.Visible = false;
		}
		{
			lblUrgeLevel.Visible = false;
			tagURGE_LEVEL.Visible = false;
		}

		DisplayWordUtil.ReplaceValidatorErrorMessage(this.Page.Validators);
	}

	private void ClearField() {
		for (int i = 0;i < pnlPattern.Controls.Count;i++) {
			if (pnlPattern.Controls[i] is TextBox) {
				((TextBox)pnlPattern.Controls[i]).Text = "";
			} else if (pnlPattern.Controls[i] is CheckBox) {
				((CheckBox)pnlPattern.Controls[i]).Checked = false;
			} else if (pnlPattern.Controls[i] is HiddenField) {
				((HiddenField)pnlPattern.Controls[i]).Value = "";
			} else if (pnlPattern.Controls[i] is Label) {
				if (((Label)pnlPattern.Controls[i]).ForeColor != Color.Red) {
					((Label)pnlPattern.Controls[i]).Text = "";
				}
			} else if (pnlPattern.Controls[i] is PlaceHolder) {
				for (int iPlh = 0;iPlh < ((PlaceHolder)pnlPattern.Controls[i]).Controls.Count;iPlh++) {
					if (((PlaceHolder)pnlPattern.Controls[i]).Controls[iPlh] is CheckBox) {
						((CheckBox)((PlaceHolder)pnlPattern.Controls[i]).Controls[iPlh]).Checked = false;
					} else if (((PlaceHolder)pnlPattern.Controls[i]).Controls[iPlh] is TextBox) {
						((TextBox)((PlaceHolder)pnlPattern.Controls[i]).Controls[iPlh]).Text = "";
					} else if (((PlaceHolder)pnlPattern.Controls[i]).Controls[iPlh] is Label) {
						((Label)((PlaceHolder)pnlPattern.Controls[i]).Controls[iPlh]).Text = "";
					}
				}
			}
		}
		tagPOINT_AFFILIATE_TOTAL_AMT_FROM.Text = "";
		tagPOINT_AFFILIATE_TOTAL_AMT_TO.Text = "";
		tagPOINT_AFFILIATE_TOTAL_COUNT_FROM.Text = "";
		tagPOINT_AFFILIATE_TOTAL_COUNT_TO.Text = "";
		tagBILL_AMT_FROM.Text = "";
		tagBILL_AMT_TO.Text = "";
		tagCAST_LOGIN_ID.Text = "";
		lblCastNm.Text = "";
		tagUSER_CHAR_NO.Text = "";
		tagNA_FLAG.SelectedIndex = -1;

		rdoMailEnable.Checked = true;
		rdoMailNoEnable.Checked = false;
		rdoMailOK.Checked = true;
		rdoMailNG.Checked = false;
		rdoRegistServicePointFlagAll.Checked = true;
		rdoRegistServicePointFlagOFF.Checked = false;
		rdoRegistServicePointFlagON.Checked = false;
		rdoMailServerBeam.Checked = true;
		rdoMailServerMuller3.Checked = false;

		// リッチーノランク
		if (((DropDownList)tagRICHINO_RANK_FROM) != null && ((DropDownList)tagRICHINO_RANK_FROM).Items.Count > 0) {
			tagRICHINO_RANK_FROM.SelectedIndex = 0;
		}
		if (((DropDownList)tagRICHINO_RANK_TO) != null && ((DropDownList)tagRICHINO_RANK_TO).Items.Count > 0) {
			tagRICHINO_RANK_TO.SelectedIndex = 0;
		}
		
		lstTaskHour.Items.Clear();
		for (int i = 0;i < 24;i++) {
			lstTaskHour.Items.Add(new ListItem(i.ToString("00"),i.ToString("00")));
		}
		lstTaskMin.Items.Clear();
		lstTaskMin.Items.Add(new ListItem("00","00"));
		lstTaskMin.Items.Add(new ListItem("15","15"));
		lstTaskMin.Items.Add(new ListItem("30","30"));
		lstTaskMin.Items.Add(new ListItem("45","45"));
		lblTaskWeek.Visible = false;
		RecCount = "0";
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		hdfPatternSeq.Value = "";
		tagSITE_CD.DataBind();
		if (Session["MENU_SITE"] != null) {
			tagSITE_CD.SelectedValue = Session["MENU_SITE"].ToString();
		} else {
			tagSITE_CD.SelectedIndex = 0;
		}
		SetupUserRank();
		SetupUrgeLevel();
		SetupMailTemplate();
		pnlPattern.Visible = true;
		btnDelete.Visible = false;
	}

	protected void grdPattern_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowIndex >= 0) {
			if (e.Row.Cells[2].Text.Equals("1")) {
				e.Row.Cells[2].Text = "有効";
				e.Row.BackColor = Color.FromArgb(0xFF,0xFF,0xFF);

				e.Row.Cells[8].BackColor = Color.FromArgb(0xCC,0xCC,0xFF);
				e.Row.Cells[9].BackColor = Color.FromArgb(0xFF,0xCC,0xCC);
			} else {
				e.Row.Cells[2].Text = "";
				e.Row.BackColor = Color.FromArgb(0xE0,0xE0,0xE0);
			}
			for (int iCol = grdPattern.Columns.Count - 8;iCol < grdPattern.Columns.Count - 1;iCol++) {
				if (e.Row.Cells[iCol].Text.Equals("1")) {
					e.Row.Cells[iCol].Text = "配信";
				} else {
					e.Row.Cells[iCol].Text = "";
				}
			}
		}
	}

	protected void lnkPatternNm_Command(object sender,CommandEventArgs e) {
		InitPage();
		hdfPatternSeq.Value = e.CommandArgument.ToString();
		btnDelete.Visible = true;
		GetData();
	}

	protected string GetRecCount() {
		return RecCount;
	}

	protected string CnvFlgData(object pPatternFlg) {
		if (pPatternFlg.ToString().Equals("1")) {
			return "有効";
		} else {
			return "";
		}
	}

	protected string CnvWeekData(object pWeekData) {
		if (pWeekData.ToString().Equals("1")) {
			return "配信";
		} else {
			return "";
		}
	}

	protected Color GetEnableColor(object pData,string pWeek) {
		if (pData.ToString().Equals("1")) {
			if (pWeek.ToString().Equals("Sat")) {
				return Color.FromArgb(0xCC,0xCC,0xFF);
			} else if (pWeek.ToString().Equals("Sun")) {
				return Color.FromArgb(0xFF,0xCC,0xCC);
			} else {
				return Color.FromArgb(0xFF,0xFF,0xFF);
			}
		} else {
			return Color.FromArgb(0xE0,0xE0,0xE0);
		}
	}

	protected void dsUrgeLevel_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = tagSITE_CD.SelectedValue;
	}

	protected void dsSchedulePattern_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = ViCommConst.MAN;
	}

	protected void tagSITE_CD_SelectedIndexChanged(object sender,EventArgs e) {
		SetupUserRank();
		SetupUrgeLevel();
		SetupMailTemplate();
		tagMAIL_TEMPLATE_NO_SelectedIndexChanged(sender,e);
	}

	/// <summary>
	/// セレクトボックス（リッチーノランク）のデータバインド後の処理
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void tagRICHINO_RANK_DataBound(object sender,EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList != null) {
			// 一番上の選択肢に空白を追加
			// (初期選択はInitPage()で初期化、GetData()で登録されてる選択肢を設定するため、ここでは行わない)
			oDropDownList.Items.Insert(0,new ListItem(string.Empty,string.Empty));
		}
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (sender == btnUpdate) {
			if (IsValid == false) {
				return;
			}
			if (!TaskWeekValidate()) {
				lblTaskWeek.Visible = true;
				return;
			}

			SetFromToData();
			UpdateData(0);
		} else {
			UpdateData(1);
		}
	}

	protected void vdcAdCd_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid) {
			using (Ad oAd = new Ad()) {
				string sAdNm = "";
				args.IsValid = oAd.IsExist(tagAD_CD.Text,ref sAdNm);
				lblAdNm.Text = sAdNm;
			}
		}
	}
	protected void vdcCastCharNo_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid) {
			using (CastCharacter oCastCharacter = new CastCharacter()) {
				args.IsValid = oCastCharacter.IsExistUserCharNoByLoginId(tagSITE_CD.SelectedValue,tagCAST_LOGIN_ID.Text,tagUSER_CHAR_NO.Text);
			}
		}
	}
	protected void vdcCastLoginId_ServerValidate(object source,ServerValidateEventArgs args) {
		if (args.IsValid) {
			using (MailTemplate oTemplate = new MailTemplate()) {
				if (oTemplate.GetOne(tagSITE_CD.SelectedValue,tagMAIL_TEMPLATE_NO.SelectedValue)) {
					if (oTemplate.mailTemplateType.Equals(ViCommConst.MAIL_TP_APPROACH_ADMIN_TO_USER) == false) {
						tagCAST_LOGIN_ID.Text = "";
						return;
					}
				}
			}

			using (Cast oCast = new Cast()) {
				args.IsValid = oCast.IsExistLoginId(tagCAST_LOGIN_ID.Text);
				if (args.IsValid) {
					lblCastNm.Text = oCast.castNm;
				} else {
					lblCastNm.Text = "";
				}
			}
		}
	}

	private bool TaskWeekValidate() {
		return chkWeek1.Checked || chkWeek2.Checked || chkWeek3.Checked || chkWeek4.Checked || chkWeek5.Checked || chkWeek6.Checked || chkWeek7.Checked;
	}

	protected void tagMAIL_TEMPLATE_NO_SelectedIndexChanged(object sender,EventArgs e) {
		trCastLoginId.Visible = (tagMAIL_TEMPLATE_NO.SelectedIndex >= int.Parse(hdfMailInfoCnt.Value));
		if ((tagMAIL_TEMPLATE_NO.SelectedIndex >= int.Parse(hdfMailInfoCnt.Value))) {
			if (hdfMaltiCharflg.Value.Equals("1")) {
				trMailAvaHour.Attributes["class"] = "tdHeaderSmallStyle";
				tdHeaderCastCharNo.Visible = true;
				tdDataCastCharNo.Visible = true;
			} else {
				trMailAvaHour.Attributes["class"] = "tdHeaderSmallStyle2";
				tdHeaderCastCharNo.Visible = false;
				tdDataCastCharNo.Visible = false;
				tagUSER_CHAR_NO.Text = ViCommConst.MAIN_CHAR_NO;
			}
		} else {
			trMailAvaHour.Attributes["class"] = "tdHeaderSmallStyle";
			tagCAST_LOGIN_ID.Text = "";
			lblCastNm.Text = "";
			tagUSER_CHAR_NO.Text = "";
		}
	}

	private void GetData() {
		tagSITE_CD.DataBind();
		if (Session["MENU_SITE"] != null) {
			tagSITE_CD.SelectedValue = Session["MENU_SITE"].ToString();
		} else {
			tagSITE_CD.SelectedIndex = 0;
		}

		ArrayList alID = new ArrayList();
		ArrayList alValue = new ArrayList();

		using (SchedulePattern oPattern = new SchedulePattern()) {
			using (DataSet ds = oPattern.GetOne(hdfPatternSeq.Value)) {
				foreach (DataRow dr in ds.Tables[0].Rows) {
					txtPatternName.Text = iBridUtil.GetStringValue(dr["PATTERN_NAME"]);
					if (iBridUtil.GetStringValue(dr["PATTERN_FLG"]).Equals("1")) {
						rdoMailEnable.Checked = true;
					} else {
						rdoMailNoEnable.Checked = true;
					}
					if (iBridUtil.GetStringValue(dr["TX_TIME"]).Length >= 8) {
						if (lstTaskHour.Items.IndexOf(new ListItem(iBridUtil.GetStringValue(dr["TX_TIME"]).Substring(0,2))) >= 0) {
							lstTaskHour.Text = iBridUtil.GetStringValue(dr["TX_TIME"]).Substring(0,2);
						}
						if (lstTaskMin.Items.IndexOf(new ListItem(iBridUtil.GetStringValue(dr["TX_TIME"]).Substring(3,2))) >= 0) {
							lstTaskMin.SelectedValue = iBridUtil.GetStringValue(dr["TX_TIME"]).Substring(3,2);
						}
					}
					for (int iCnt = 1;iCnt <= 7;iCnt++) {
						if (iBridUtil.GetStringValue(dr["TX_WEEK" + iCnt.ToString()]).Equals("1")) {
							CheckBox chkOk = (CheckBox)plcWeek.FindControl(string.Format("chkWeek{0}",iCnt)) as CheckBox;
							chkOk.Checked = true;
						}
					}
				}
			}
			using (DataSet ds = oPattern.GetListCnd(hdfPatternSeq.Value)) {
				foreach (DataRow dr in ds.Tables[0].Rows) {
					object ctlData = pnlPattern.FindControl("tag" + iBridUtil.GetStringValue(dr["PATTERN_SEARCH_NAME"]));
					if ((ctlData != null)) {
						if (ctlData is TextBox) {
							((TextBox)ctlData).Text = iBridUtil.GetStringValue(dr["PATTERN_SEARCH_VALUE"]);
						} else if (ctlData is DropDownList) {
							if (((DropDownList)ctlData).ID == "tagSITE_CD") {
								((DropDownList)ctlData).SelectedValue = iBridUtil.GetStringValue(dr["PATTERN_SEARCH_VALUE"]);
								SetupUserRank();
								SetupUrgeLevel();
								SetupMailTemplate();
							} else if (((DropDownList)ctlData).ID == "tagRICHINO_RANK_FROM" || ((DropDownList)ctlData).ID == "tagRICHINO_RANK_TO") {
								// リッチーノランクの初期設定
								string sValue = iBridUtil.GetStringValue(dr["PATTERN_SEARCH_VALUE"]);
								if (!string.IsNullOrEmpty(sValue)) {
									((DropDownList)ctlData).SelectedValue = sValue;
								}
							} else {
								alID.Add(((DropDownList)ctlData).ID);
								alValue.Add(iBridUtil.GetStringValue(dr["PATTERN_SEARCH_VALUE"]));
							}
						} else if (ctlData is CheckBox) {
							if (iBridUtil.GetStringValue(dr["PATTERN_SEARCH_VALUE"]).Equals("1")) {
								((CheckBox)ctlData).Checked = true;
							}
						} else if (ctlData is PlaceHolder) {
							int iCondData = 0;
							if (SysPrograms.IsNumber(iBridUtil.GetStringValue(dr["PATTERN_SEARCH_VALUE"]))) {
								iCondData = int.Parse(iBridUtil.GetStringValue(dr["PATTERN_SEARCH_VALUE"]));
							}
							for (int iPlh = 0;iPlh < ((PlaceHolder)ctlData).Controls.Count;iPlh++) {
								if (((PlaceHolder)ctlData).Controls[iPlh] is RadioButton) {
									if (CnvStrConditon(((PlaceHolder)ctlData).Controls[iPlh].ID) == iBridUtil.GetStringValue(dr["PATTERN_SEARCH_VALUE"])) {
										((RadioButton)((PlaceHolder)ctlData).Controls[iPlh]).Checked = true;
									}
								} else if (((PlaceHolder)ctlData).Controls[iPlh] is CheckBox) {
									if ((iCondData & CnvCondition(((PlaceHolder)ctlData).Controls[iPlh].ID)) != 0) {
										((CheckBox)((PlaceHolder)ctlData).Controls[iPlh]).Checked = true;
									}
								}
							}
						} else if (ctlData is CheckBoxList) {
							if (((CheckBoxList)ctlData).ID == "tagNA_FLAG") {
								foreach (string sValue in iBridUtil.GetStringValue(dr["PATTERN_SEARCH_VALUE"]).Split(',')) {
									foreach (ListItem oListItem in ((CheckBoxList)ctlData).Items) {
										if (oListItem.Value.Equals(sValue)) {
											oListItem.Selected = true;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		for (int i = 0;i < alID.Count;i++) {
			object ctlData = pnlPattern.FindControl(alID[i].ToString());

			if ((ctlData != null) && (ctlData is DropDownList)) {
				if (((DropDownList)ctlData).Items.FindByValue(alValue[i].ToString()) != null) {
					((DropDownList)ctlData).SelectedIndex = ((DropDownList)ctlData).Items.IndexOf(((DropDownList)ctlData).Items.FindByValue(alValue[i].ToString()));
				}
			}
		}

		trCastLoginId.Visible = (tagMAIL_TEMPLATE_NO.SelectedIndex >= int.Parse(hdfMailInfoCnt.Value));
		if ((tagMAIL_TEMPLATE_NO.SelectedIndex >= int.Parse(hdfMailInfoCnt.Value))) {
			if (hdfMaltiCharflg.Value.Equals("1")) {
				trMailAvaHour.Attributes["class"] = "tdHeaderSmallStyle";
				tdHeaderCastCharNo.Visible = true;
				tdDataCastCharNo.Visible = true;
			} else {
				trMailAvaHour.Attributes["class"] = "tdHeaderSmallStyle2";
				tdHeaderCastCharNo.Visible = false;
				tdDataCastCharNo.Visible = false;
				tagUSER_CHAR_NO.Text = ViCommConst.MAIN_CHAR_NO;
			}
		} else {
			trMailAvaHour.Attributes["class"] = "tdHeaderSmallStyle";
		}

		using (Ad oAd = new Ad()) {
			string sAdNm = "";
			oAd.IsExist(tagAD_CD.Text,ref sAdNm);
			lblAdNm.Text = sAdNm;
		}

		using (Cast oCast = new Cast()) {
			if (oCast.IsExistLoginId(tagCAST_LOGIN_ID.Text)) {
				lblCastNm.Text = oCast.castNm;
			} else {
				lblCastNm.Text = "";
			}
		}
		pnlPattern.Visible = true;
	}

	private void SetupUserRank() {
		DataSet ds;
		string sUserRank = "";
		string sUserRankNm = "";

		for (int i = 0;i < tagUSER_RANK.Controls.Count;i++) {
			if (tagUSER_RANK.Controls[i] is CheckBox) {
				((CheckBox)tagUSER_RANK.Controls[i]).Checked = false;
				((CheckBox)tagUSER_RANK.Controls[i]).Visible = false;
			}
		}

		using (UserRank oUserRank = new UserRank()) {
			ds = oUserRank.GetListIncDefault(tagSITE_CD.SelectedValue);
			foreach (DataRow dr in ds.Tables[0].Rows) {
				sUserRank = iBridUtil.GetStringValue(dr["USER_RANK"]);
				sUserRankNm = iBridUtil.GetStringValue(dr["USER_RANK_NM"]);

				if (!sUserRank.Equals(ViCommConst.DEFAUL_USER_RANK)) {
					CheckBox chkRank = (CheckBox)tagUSER_RANK.FindControl(string.Format("chkRank{0}",sUserRank)) as CheckBox;
					chkRank.Visible = true;
					chkRank.Text = sUserRankNm;
				}
			}
		}
	}

	private void SetupUrgeLevel() {
		tagURGE_LEVEL.DataBind();
		tagURGE_LEVEL.Items.Insert(0,new ListItem("",""));
	}

	private void SetupMailTemplate() {
		DataSet ds;
		tagMAIL_TEMPLATE_NO.Items.Clear();
		using (MailTemplate oMail = new MailTemplate()) {
			ds = oMail.GetListByTemplateType(tagSITE_CD.SelectedValue,ViCommConst.MAIL_TP_INFO,null);
			foreach (DataRow dr in ds.Tables[0].Rows) {
				tagMAIL_TEMPLATE_NO.Items.Add(new ListItem(iBridUtil.GetStringValue(dr["TEMPLATE_NM"]),iBridUtil.GetStringValue(dr["MAIL_TEMPLATE_NO"])));
			}
			hdfMailInfoCnt.Value = tagMAIL_TEMPLATE_NO.Items.Count.ToString();

			ds = oMail.GetListByTemplateType(tagSITE_CD.SelectedValue,ViCommConst.MAIL_TP_APPROACH_ADMIN_TO_USER,null);
			foreach (DataRow dr in ds.Tables[0].Rows) {
				tagMAIL_TEMPLATE_NO.Items.Add(new ListItem(iBridUtil.GetStringValue(dr["TEMPLATE_NM"]),iBridUtil.GetStringValue(dr["MAIL_TEMPLATE_NO"])));
			}
		}

		using (Site oSite = new Site()) {
			if (oSite.GetMultiCharFlag(tagSITE_CD.SelectedValue)) {
				hdfMaltiCharflg.Value = "1";
			} else {
				hdfMaltiCharflg.Value = "0";
			}
		}
	}

	private void SetFromToData() {

		for (int i = 0;i < pnlPattern.Controls.Count;i++) {
			if ((pnlPattern.Controls[i].ID == null) || (pnlPattern.Controls[i].ID.IndexOf("tag") != 0)) {
				continue;
			}
			if ((pnlPattern.Controls[i] is TextBox) && pnlPattern.Controls[i].ID.IndexOf("_FROM") > 0) {
				object ctlData = pnlPattern.FindControl(pnlPattern.Controls[i].ID.Replace("_FROM","_TO"));
				if ((ctlData != null) && (ctlData is TextBox)) {

					if ((!((TextBox)pnlPattern.Controls[i]).Text.Equals("")) || (!((TextBox)ctlData).Text.Equals(""))) {
						if (((TextBox)pnlPattern.Controls[i]).Text.Equals("")) {
							((TextBox)pnlPattern.Controls[i]).Text = ((TextBox)ctlData).Text;
						} else if (((TextBox)ctlData).Text.Equals("")) {
							((TextBox)ctlData).Text = ((TextBox)pnlPattern.Controls[i]).Text;
						}
					}
				}
			}
		}

		if ((tagPOINT_AFFILIATE_TOTAL_AMT_FROM.Text.Length > 0) || (tagPOINT_AFFILIATE_TOTAL_AMT_TO.Text.Length > 0)) {
			if (tagPOINT_AFFILIATE_TOTAL_AMT_FROM.Text.Length == 0) {
				tagPOINT_AFFILIATE_TOTAL_AMT_FROM.Text = tagPOINT_AFFILIATE_TOTAL_AMT_TO.Text;
			} else if (tagPOINT_AFFILIATE_TOTAL_AMT_TO.Text.Length == 0) {
				tagPOINT_AFFILIATE_TOTAL_AMT_TO.Text = tagPOINT_AFFILIATE_TOTAL_AMT_FROM.Text;
			}
		}
		if ((tagPOINT_AFFILIATE_TOTAL_COUNT_FROM.Text.Length > 0) || (tagPOINT_AFFILIATE_TOTAL_COUNT_TO.Text.Length > 0)) {
			if (tagPOINT_AFFILIATE_TOTAL_COUNT_FROM.Text.Length == 0) {
				tagPOINT_AFFILIATE_TOTAL_COUNT_FROM.Text = tagPOINT_AFFILIATE_TOTAL_COUNT_TO.Text;
			} else if (tagPOINT_AFFILIATE_TOTAL_COUNT_TO.Text.Length == 0) {
				tagPOINT_AFFILIATE_TOTAL_COUNT_TO.Text = tagPOINT_AFFILIATE_TOTAL_COUNT_FROM.Text;
			}
		}
		if ((tagBILL_AMT_FROM.Text.Length > 0) || (tagBILL_AMT_TO.Text.Length > 0)) {
			if (tagBILL_AMT_FROM.Text.Length == 0) {
				tagBILL_AMT_FROM.Text = tagBILL_AMT_TO.Text;
			} else if (tagBILL_AMT_TO.Text.Length == 0) {
				tagBILL_AMT_TO.Text = tagBILL_AMT_FROM.Text;
			}
		}
	}

	private void UpdateData(int pDelFlag) {

		ArrayList alSearchName = new ArrayList();
		ArrayList alSearchValue = new ArrayList();

		if (pDelFlag == 0) {
			// パターンを配列に格納
			for (int i = 0;i < pnlPattern.Controls.Count;i++) {
				if ((pnlPattern.Controls[i].ID == null) || (pnlPattern.Controls[i].ID.IndexOf("tag") != 0)) {
					continue;
				}

				if ((pnlPattern.Controls[i] is TextBox) && (((TextBox)pnlPattern.Controls[i]).Text.Length > 0)) {
					alSearchName.Add(pnlPattern.Controls[i].ID.Substring(3));
					alSearchValue.Add(((TextBox)pnlPattern.Controls[i]).Text);
				} else if ((pnlPattern.Controls[i] is DropDownList) && (((DropDownList)pnlPattern.Controls[i]).SelectedValue.Length > 0)) {
					// 範囲検索用のタグの場合、フィールド名を変更して設定する
					// （IDからtagを取り除いた値を使う）
					string sId = pnlPattern.Controls[i].ID;
					string sFirldName = ((DropDownList)pnlPattern.Controls[i]).DataValueField;
					if (sId.EndsWith("_FROM") || sId.EndsWith("_TO")) {
						sFirldName = sId.Replace("tag", "");
					}

					alSearchName.Add(sFirldName);
					alSearchValue.Add(((DropDownList)pnlPattern.Controls[i]).SelectedValue);
				} else if ((pnlPattern.Controls[i] is CheckBox) && (((CheckBox)pnlPattern.Controls[i]).Checked)) {
					alSearchName.Add(pnlPattern.Controls[i].ID.Substring(3));
					alSearchValue.Add("1");
				} else if (pnlPattern.Controls[i] is PlaceHolder) {
					int iData = 0;
					for (int iPlh = 0;iPlh < ((PlaceHolder)pnlPattern.Controls[i]).Controls.Count;iPlh++) {
						if (((PlaceHolder)pnlPattern.Controls[i]).Controls[iPlh] is RadioButton) {
							if ((((RadioButton)((PlaceHolder)pnlPattern.Controls[i]).Controls[iPlh]).Checked) &&
								 (CnvStrConditon(((RadioButton)((PlaceHolder)pnlPattern.Controls[i]).Controls[iPlh]).ID).Length > 0)) {
								alSearchName.Add(pnlPattern.Controls[i].ID.Substring(3));
								alSearchValue.Add(CnvStrConditon(((RadioButton)((PlaceHolder)pnlPattern.Controls[i]).Controls[iPlh]).ID));
							}
						} else if (((PlaceHolder)pnlPattern.Controls[i]).Controls[iPlh] is CheckBox) {
							if (((CheckBox)((PlaceHolder)pnlPattern.Controls[i]).Controls[iPlh]).Checked) {
								iData += CnvCondition(((PlaceHolder)pnlPattern.Controls[i]).Controls[iPlh].ID);
							}
						}
					}
					if (iData > 0) {
						alSearchName.Add(pnlPattern.Controls[i].ID.Substring(3));
						alSearchValue.Add(iData.ToString());
					}
				}
			}
			if (tagPOINT_AFFILIATE_TOTAL_AMT_FROM.Text.Length > 0) {
				alSearchName.Add("POINT_AFFILIATE_TOTAL_AMT_FROM");
				alSearchValue.Add(tagPOINT_AFFILIATE_TOTAL_AMT_FROM.Text);
				alSearchName.Add("POINT_AFFILIATE_TOTAL_AMT_TO");
				alSearchValue.Add(tagPOINT_AFFILIATE_TOTAL_AMT_TO.Text);
			}
			if (tagPOINT_AFFILIATE_TOTAL_COUNT_FROM.Text.Length > 0) {
				alSearchName.Add("POINT_AFFILIATE_TOTAL_COUNT_FROM");
				alSearchValue.Add(tagPOINT_AFFILIATE_TOTAL_COUNT_FROM.Text);
				alSearchName.Add("POINT_AFFILIATE_TOTAL_COUNT_TO");
				alSearchValue.Add(tagPOINT_AFFILIATE_TOTAL_COUNT_TO.Text);
			}
			if (tagBILL_AMT_FROM.Text.Length > 0) {
				alSearchName.Add("BILL_AMT_FROM");
				alSearchValue.Add(tagBILL_AMT_FROM.Text);
				alSearchName.Add("BILL_AMT_TO");
				alSearchValue.Add(tagBILL_AMT_TO.Text);
			}
			if (tagCAST_LOGIN_ID.Text.Length > 0) {
				alSearchName.Add("CAST_LOGIN_ID");
				alSearchValue.Add(tagCAST_LOGIN_ID.Text);
			}
			if (tagUSER_CHAR_NO.Text.Length > 0) {
				alSearchName.Add("USER_CHAR_NO");
				alSearchValue.Add(tagUSER_CHAR_NO.Text);
			}
			string sNaFlags = string.Empty;
			foreach (ListItem oListItem in tagNA_FLAG.Items) {
				if (!oListItem.Selected)
					continue;

				if (!sNaFlags.Equals(string.Empty)) {
					sNaFlags += ",";
				}
				sNaFlags += oListItem.Value;
			}
			if (!sNaFlags.Equals(string.Empty)) {
				alSearchName.Add("NA_FLAG");
				alSearchValue.Add(sNaFlags);
			}
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("SCHEDULE_PATTERN_MAINTE");
			db.ProcedureInParm("PPATTERN_SEQ",DbSession.DbType.NUMBER,hdfPatternSeq.Value);
			db.ProcedureInParm("PPATTERN_NAME",DbSession.DbType.VARCHAR2,txtPatternName.Text);
			db.ProcedureInParm("PPATTERN_FLG",DbSession.DbType.NUMBER,rdoMailEnable.Checked);
			db.ProcedureInParm("PTX_TIME",DbSession.DbType.VARCHAR2,string.Format("{0}:{1}:00",lstTaskHour.SelectedValue,lstTaskMin.SelectedValue));
			db.ProcedureInParm("PTX_WEEK1",DbSession.DbType.NUMBER,chkWeek1.Checked);
			db.ProcedureInParm("PTX_WEEK2",DbSession.DbType.NUMBER,chkWeek2.Checked);
			db.ProcedureInParm("PTX_WEEK3",DbSession.DbType.NUMBER,chkWeek3.Checked);
			db.ProcedureInParm("PTX_WEEK4",DbSession.DbType.NUMBER,chkWeek4.Checked);
			db.ProcedureInParm("PTX_WEEK5",DbSession.DbType.NUMBER,chkWeek5.Checked);
			db.ProcedureInParm("PTX_WEEK6",DbSession.DbType.NUMBER,chkWeek6.Checked);
			db.ProcedureInParm("PTX_WEEK7",DbSession.DbType.NUMBER,chkWeek7.Checked);
			db.ProcedureInArrayParm("PPATTERN_SEARCH_NAME",DbSession.DbType.VARCHAR2,alSearchName.Count,(string[])alSearchName.ToArray(typeof(string)));
			db.ProcedureInArrayParm("PPATTERN_SEARCH_VALUE",DbSession.DbType.VARCHAR2,alSearchValue.Count,(string[])alSearchValue.ToArray(typeof(string)));
			db.ProcedureInParm("PSEX_CD",DbSession.DbType.VARCHAR2,ViCommConst.MAN);
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
		}
		InitPage();
	}

	private int CnvCondition(string pID) {
		int iRet = 0;
		switch (pID) {
			case "chkRankA":
				iRet = ViCommConst.MASK_RANK_A;
				break;
			case "chkRankB":
				iRet = ViCommConst.MASK_RANK_B;
				break;
			case "chkRankC":
				iRet = ViCommConst.MASK_RANK_C;
				break;
			case "chkRankD":
				iRet = ViCommConst.MASK_RANK_D;
				break;
			case "chkRankE":
				iRet = ViCommConst.MASK_RANK_E;
				break;
			case "chkRankF":
				iRet = ViCommConst.MASK_RANK_F;
				break;
			case "chkRankG":
				iRet = ViCommConst.MASK_RANK_G;
				break;
			case "chkRankH":
				iRet = ViCommConst.MASK_RANK_H;
				break;
			case "chkManTelNoAuth":
				iRet = ViCommConst.MASK_MAN_TEL_NO_AUTH;
				break;
			case "chkManNew":
				iRet = ViCommConst.MASK_MAN_NEW;
				break;
			case "chkManNoReceipt":
				iRet = ViCommConst.MASK_MAN_NO_RECEIPT;
				break;
			case "chkManNonUsed":
				iRet = ViCommConst.MASK_MAN_NO_USED;
				break;
			case "chkManDelay":
				iRet = ViCommConst.MASK_MAN_DELAY;
				break;
			case "chkManResigned":
				iRet = ViCommConst.MASK_MAN_RESIGNED;
				break;
			case "chkManStop":
				iRet = ViCommConst.MASK_MAN_STOP;
				break;
			case "chkManBlack":
				iRet = ViCommConst.MASK_MAN_BLACK;
				break;
			case "chkManNormal":
				iRet = ViCommConst.MASK_MAN_NORMAL;
				break;
			case "chkOnline":
				iRet = ViCommConst.MASK_LOGINED;
				break;
			case "chkOffline":
				iRet = ViCommConst.MASK_OFFLINE;
				break;
			case "chkTalking":
				iRet = ViCommConst.MASK_TALKING;
				break;
			case "chkDocomo":
				iRet = ViCommConst.MASK_DOCOMO;
				break;
			case "chkAu":
				iRet = ViCommConst.MASK_KDDI;
				break;
			case "chkSoftbank":
				iRet = ViCommConst.MASK_SOFTBANK;
				break;
			case "chkAndroid":
				iRet = ViCommConst.MASK_ANDROID;
				break;
			case "chkIPhone":
				iRet = ViCommConst.MASK_IPHONE;
				break;
		}
		return iRet;
	}

	private string CnvStrConditon(string pID) {
		string sRet = "0";
		switch (pID) {
			case "rdoMailOK":
				sRet = "0";
				break;
			case "rdoMailNG":
				sRet = "1";
				break;
			case "rdoRegistServicePointFlagAll":
				sRet = string.Empty;
				break;
			case "rdoRegistServicePointFlagOFF":
				sRet = "0";
				break;
			case "rdoRegistServicePointFlagON":
				sRet = "1";
				break;
			case "rdoMailServerBeam":
				sRet = "1";
				break;
			case "rdoMailServerMuller3":
				sRet = "2";
				break;
		}
		return sRet;
	}
}
