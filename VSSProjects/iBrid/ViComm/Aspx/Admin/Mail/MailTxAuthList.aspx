<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MailTxAuthList.aspx.cs" Inherits="Mail_MailTxAuthList" Title="メール送信認証"
	ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="iBridCommLib" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="メール記録"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 400px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle2">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
							</asp:DropDownList>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[未認証ﾒｰﾙ一覧]</legend>
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="430px">
					<asp:GridView ID="grdMail" runat="server" AllowPaging="True" AutoGenerateColumns="False" AllowSorting="True" SkinID="GridViewColor" DataKeyNames="TX_SITE_CD,REQUEST_TX_MAIL_SEQ">
						<Columns>
							<asp:TemplateField HeaderText="作成日時">
								<ItemTemplate>
									<asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("CREATE_DATE")%>'></asp:Label>
								</ItemTemplate>
								<ItemStyle VerticalAlign="Top" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="送信者">
								<ItemTemplate>
									<asp:HyperLink ID="lblTxLoginId" runat="server" NavigateUrl='<%# GetViewUrl(Eval("TX_SEX_CD"),Eval("TX_SITE_CD"),Eval("TX_LOGIN_ID")) %>' Text='<%# Eval("TX_LOGIN_ID") %>'></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle VerticalAlign="Top" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="受信者">
								<ItemTemplate>
									<asp:HyperLink ID="lblRxLoginId" runat="server" NavigateUrl='<%# GetViewUrl(Eval("RX_SEX_CD"),Eval("RX_SITE_CD"),Eval("RX_LOGIN_ID")) %>' Text='<%# Eval("RX_LOGIN_ID") %>'></asp:HyperLink>
								</ItemTemplate>
								<ItemStyle VerticalAlign="Top" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="内容">
								<ItemTemplate>
									<asp:Label ID="lblMailTitle" runat="server" Text='<%# Eval("ORIGINAL_TITLE") %>'></asp:Label><br />
									<asp:Label ID="lblDoc" runat="server" Text='<%# GetMailDoc(Eval("ORIGINAL_DOC1"),Eval("ORIGINAL_DOC2"),Eval("ORIGINAL_DOC3"),Eval("ORIGINAL_DOC4"),Eval("ORIGINAL_DOC5") ) %>'
										Width="500px"></asp:Label>
								</ItemTemplate>
								<ItemStyle VerticalAlign="Top" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="種別">
								<ItemTemplate>
									<asp:Panel ID="pnlOpenPicViewer" runat="server" Visible='<%# iBridUtil.GetStringValue(Eval("ATTACHED_OBJ_TYPE")).Equals(ViCommConst.ATTACH_PIC_INDEX.ToString()) %>'>
										<asp:HyperLink ID="lnkOpenPicViewer" runat="server" NavigateUrl="<%# GenerateOpenPicScript(Container.DataItem) %>">
										画像添付
										</asp:HyperLink>
										<br />
										<a href="<%# GenerateOpenPicScript(Container.DataItem) %>">
											<asp:Image runat="server" ID="imbPic" ImageUrl="<%# GenerateSmallPicPath(Container.DataItem) %>" Width="80px" />
										</a>
									</asp:Panel>
									<asp:Panel ID="pnlOpenMovieViewer" runat="server" Visible='<%# iBridUtil.GetStringValue(Eval("ATTACHED_OBJ_TYPE")).Equals(ViCommConst.ATTACH_MOVIE_INDEX.ToString()) %>'>
										<asp:HyperLink ID="lnkOpenMovieViewer" runat="server" NavigateUrl="<%# GenerateOpenMovieScript(Container.DataItem) %>">
										動画添付
										</asp:HyperLink>
										<br />
										<a href="<%# GenerateOpenMovieScript(Container.DataItem) %>">
											<asp:Image runat="server" ID="imbMovie" ImageUrl="<%# GenerateSmallPicPathMovie(Container.DataItem) %>" Width="80px" />
										</a>
									</asp:Panel>
								</ItemTemplate>
								<ItemStyle VerticalAlign="Top" HorizontalAlign="center" />
							</asp:TemplateField>
							<asp:TemplateField HeaderText="認証">
								<ItemStyle HorizontalAlign="Center" />
								<ItemTemplate>
									<asp:Button ID="btnUpdate" runat="server" CommandArgument='<%# Container.DataItemIndex %>' OnClick="btnUpdate_OnClick" Text="認証" OnClientClick="return confirm('認証を行いますか？');">
									</asp:Button>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="削除">
								<ItemStyle HorizontalAlign="Center" />
								<ItemTemplate>
									<asp:Button ID="btnDelete" runat="server" CommandArgument='<%# Container.DataItemIndex %>' OnClick="btnDelete_OnClick" Text="削除" OnClientClick="return confirm('削除を行いますか？');">
									</asp:Button>
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
					</asp:GridView>
				</asp:Panel>
				&nbsp;
				<asp:Panel runat="server" ID="pnlCount">
					<a class="reccount">Record Count
						<%#GetRecCount() %>
					</a>
					<br />
					<a class="reccount">Current viewing page
						<%=grdMail.PageIndex + 1%>
						of
						<%=grdMail.PageCount%>
					</a>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsReqTxMail" runat="server" SelectMethod="GetPageCollection" TypeName="ReqTxMail" SelectCountMethod="GetPageCount" EnablePaging="True"
		OnSelected="dsReqTxMail_Selected" OnSelecting="dsReqTxMail_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
</asp:Content>
