﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メールテンプレートメンテナンス
--	Progaram ID		: MailTemplateList
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX
  2010/12/06  ttakahashi テキストメールフラグ対応


  2010/12/07  ttakahashi テキストメールフラグの取得方法を修正

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using iBridCommLib;
using ViComm;
using System.Text;

public partial class Mail_MailTemplateList:System.Web.UI.Page {
	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}
	private string MailTemplateNo {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MailTemplateNo"]);
		}
		set {
			this.ViewState["MailTemplateNo"] = value;
		}
	}
	private string MailTemplateType {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MailTemplateType"]);
		}
		set {
			this.ViewState["MailTemplateType"] = value;
		}
	}

	private bool DisablePinEdit {
		get {
			return (bool)this.ViewState["DisablePinEdit"];
		}
		set {
			this.ViewState["DisablePinEdit"] = value;
		}
	}

	private string HtmlDoc {
		get {
			if (this.DisablePinEdit) {
				return this.txtHtmlDocText.Text;
			} else {
				return this.txtHtmlDoc.Text;
			}
		}
		set {
			if (this.DisablePinEdit) {
				this.txtHtmlDocText.Text = value;
			} else {
				this.txtHtmlDoc.Text = value;
			}
		}
	}

	private string recCount = "";
	private Stream filter;

	protected void Page_Load(object sender,EventArgs e) {
		filter = Response.Filter;
		Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);
		txtHtmlDoc.EditorDirectory = string.Format("http://{0}:{1}/pinEdit/editor/",Request.UrlReferrer.Host,Request.UrlReferrer.Port);

		if (!IsPostBack) {
			FirstLoad();
			InitPage();

			// テンプレートNoを指定して遷移してきた場合
			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Request.QueryString["tno"]))) {
				txtMailTemplateNo.Text = iBridUtil.GetStringValue(Request.QueryString["tno"]);
				txtHtmlDocSubSeq.Text = "1";

				// 詳細表示
				GetData();
			}
		}
	}

	private void FirstLoad() {

		grdTemplate.PageSize = 999;
		string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		if (sSiteCd.Equals("")) {
			if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
				lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}
		} else {
			lstSeekSiteCd.SelectedValue = sSiteCd;
		}
		using (ManageCompany oManageCompany = new ManageCompany()) {
			this.DisablePinEdit = oManageCompany.IsAvailableService(ViCommConst.RELEASE_DISABLE_PINEDIT,2);
			this.txtHtmlDoc.Visible = !this.DisablePinEdit;
			this.txtHtmlDocText.Visible = this.DisablePinEdit;
		}

		lstSeekMailTemplateType.DataBind();
		lstSeekMailTemplateType.Items.Insert(0,new ListItem("",""));
		lstSeekMailTemplateType.DataSourceID = "";
		lstSeekMailTemplateType.SelectedIndex = 0;
		grdTemplate.DataSourceID = "";
		grdSiteHtmlDoc.DataSourceID = string.Empty;

		DataBind();

		grdTemplate.DataSourceID = "dsMailTemplate";
		grdSiteHtmlDoc.DataSourceID = string.Empty;
	}

	private void InitPage() {
		lstSiteCd.Enabled = true;
		lblHtmlDocSeq.Text = string.Empty;
		ClearField();
		pnlKey.Enabled = true;
		pnlMainte.Visible = false;
		pnlSelect.Visible = false;
		grdSiteHtmlDoc.DataSourceID = string.Empty;
		int iCompare = string.Compare(Session["AdminType"].ToString(),ViCommConst.RIGHT_SITE_OWNER);
		btnCSV.Visible = (iCompare >= 0);
	}

	private void ClearField() {
		lblMailTemplateNoMessage.Text = string.Empty;
		lblMailTemplateNoMessage.Visible = false;
		txtCopyMailTemplateNo.Text = string.Empty;
		txtTemplateNm.Text = string.Empty;
		txtMailTitle.Text = string.Empty;
		txtHtmlDocTitle.Text = string.Empty;
		this.HtmlDoc = string.Empty;
		txtTextDoc.Text = string.Empty;
		txtMobileMailFontSize.Text = string.Empty;
		txtMobileMailBackColor.Text = string.Empty;
		txtTemplateNm.Enabled = true;
		txtMailTitle.Enabled = true;
		txtMobileMailFontSize.Enabled = true;
		lstMailTemplateType.Enabled = true;
		lstWebFaceSeq.Enabled = true;
		rdoAttachedNone.Checked = true;
		rdoAttachedPic.Checked = false;
		rdoAttachedMovie.Checked = false;
		chkStock.Checked = false;
		chkMobile.Checked = false;
		chkForceTxMobileMailFlag.Checked = false;
		lstSexCd.Enabled = true;
		recCount = "0";
		chkTextMailFlag.Checked = false;
	}

	protected string GetRecCount() {
		return recCount;
	}

	private void GetList() {
		grdTemplate.PageIndex = 0;
		grdTemplate.DataBind();
		pnlCount.DataBind();
	}

	protected void btnSeek_Click(object sender,EventArgs e) {
		GetData();
	}

	protected void btnRegist_Click(object sender,EventArgs e) {
		InitPage();
		pnlMainte.Visible = true;
		pnlKey.Enabled = true;
		pnlDtl.Visible = false;
		lstSiteCd.SelectedIndex = lstSeekSiteCd.SelectedIndex;
		lstSiteCd.Enabled = false;
	}

	protected void btnUpdate_Click(object sender,EventArgs e) {
		if (IsValid) {
			UpdateData(0);
			SendTestMail(true);
			GetMimeSize();
			txtMailTitle1.Text = getTitle(lstSiteCd.SelectedValue,txtMailTemplateNo.Text,1);
		}
	}
	
	protected string subLblHtmlDocSeq;
	
	protected void btnDelete_Click(object sender,EventArgs e) {
		if (IsValid) {
			this.subLblHtmlDocSeq = lblHtmlDocSeq.Text;
			UpdateData(1);
		}
	}

	protected void btnCancel_Click(object sender,EventArgs e) {
		EndEdit();
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void lstSeekSiteCd_SelectedIndexChanged(object sender,EventArgs e) {
		GetList();
	}

	protected void btnSeekCancel_Click(object sender,EventArgs e) {
		InitPage();
	}

	private void GetData() {
		using (Site oSite = new Site()) {
			string sNaWebFace = string.Empty;
			oSite.GetValue(lstSiteCd.SelectedValue,"MAIL_DOC_NOT_USE_WEB_FACE_FLAG",ref sNaWebFace);
			if (sNaWebFace.Equals("1")) {
				plcWebFace.Visible = false;
				plcMobileMailFontSize.Visible = true;
			} else {
				plcWebFace.Visible = true;
				plcMobileMailFontSize.Visible = false;
			}
		}

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MAIL_TEMPLATE_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PMAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,txtMailTemplateNo.Text);
			db.ProcedureInParm("PHTML_DOC_SUB_SEQ",DbSession.DbType.NUMBER,int.Parse(txtHtmlDocSubSeq.Text));
			db.ProcedureOutParm("PMAIL_TEMPLATE_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PTEMPLATE_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMAIL_TITLE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PEMAIL_ADDR",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PWEB_FACE_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMOBILE_MAIL_FONT_SIZE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMOBILE_MAIL_BACK_COLOR",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_CREATE_ORG_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMAIL_ATTACHED_OBJ_TYPE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMAIL_ATTACHED_METHOD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PTEXT_MAIL_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PDEL_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PHTML_DOC_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHTML_DOC_TITLE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutArrayParm("PHTML_DOC",DbSession.DbType.VARCHAR2,SysConst.MAX_HTML_BLOCKS);
			db.ProcedureOutParm("PNON_DISP_IN_MAIL_BOX",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PNON_DISP_IN_DECOMAIL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSEX_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PFORCE_TX_MOBILE_MAIL_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMAIL_DOC_SHORTEN_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID_VIEW",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO_VIEW",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID_MANAGE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO_MANAGE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID_DOC",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO_DOC",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);

			db.ExecuteProcedure();

			ViewState["REVISION_NO_VIEW"] = db.GetStringValue("PREVISION_NO_VIEW");
			ViewState["ROWID_VIEW"] = db.GetStringValue("PROWID_VIEW");

			ViewState["REVISION_NO_MANAGE"] = db.GetStringValue("PREVISION_NO_MANAGE");
			ViewState["ROWID_MANAGE"] = db.GetStringValue("PROWID_MANAGE");

			ViewState["REVISION_NO_DOC"] = db.GetStringValue("PREVISION_NO_DOC");
			ViewState["ROWID_DOC"] = db.GetStringValue("PROWID_DOC");

			lblHtmlDocSeq.Text = db.GetStringValue("PHTML_DOC_SEQ");

			this.HtmlDoc = string.Empty;
			txtTextDoc.Text = Environment.NewLine;
			rdoAttachedNone.Checked = true;
			rdoAttachedPic.Checked = false;
			rdoAttachedMovie.Checked = false;
			lblMailTemplateNoMessage.Visible = false;
			txtCopyMailTemplateNo.Text = string.Empty;

			if (int.Parse(db.GetStringValue("PRECORD_COUNT")) > 0 && db.GetStringValue("PDEL_FLAG").Equals(ViCommConst.FLAG_OFF_STR)) {
				txtMailTitle.Text = db.GetStringValue("PMAIL_TITLE");
				lstMailTemplateType.SelectedValue = db.GetStringValue("PMAIL_TEMPLATE_TYPE");
				lstWebFaceSeq.SelectedValue = db.GetStringValue("PWEB_FACE_SEQ");
				lstSexCd.SelectedValue = db.GetStringValue("PSEX_CD");
				txtHtmlDocTitle.Text = db.GetStringValue("PHTML_DOC_TITLE");
				txtTemplateNm.Text = db.GetStringValue("PTEMPLATE_NM");
				txtMailAddrFrom.Text = db.GetStringValue("PEMAIL_ADDR");
				txtMobileMailFontSize.Text = db.GetStringValue("PMOBILE_MAIL_FONT_SIZE");
				txtMobileMailBackColor.Text = db.GetStringValue("PMOBILE_MAIL_BACK_COLOR");
				chkNonDispInMailBox.Checked = (db.GetIntValue("PNON_DISP_IN_MAIL_BOX") != 0);
				chkNonDispInDecomail.Checked = (db.GetIntValue("PNON_DISP_IN_DECOMAIL") != 0);
				chkForceTxMobileMailFlag.Checked = (db.GetIntValue("PFORCE_TX_MOBILE_MAIL_FLAG") != 0);
				chkMailDocShortenFlag.Checked = (db.GetIntValue("PMAIL_DOC_SHORTEN_FLAG") != 0);
				if (db.GetStringValue("PMAIL_ATTACHED_OBJ_TYPE").Equals(ViCommConst.ATTACH_PIC_INDEX.ToString())) {
					rdoAttachedPic.Checked = true;
					rdoAttachedNone.Checked = false;
				} else if (db.GetStringValue("PMAIL_ATTACHED_OBJ_TYPE").Equals(ViCommConst.ATTACH_MOVIE_INDEX.ToString())) {
					rdoAttachedMovie.Checked = true;
					rdoAttachedNone.Checked = false;
				}
				switch (db.GetStringValue("PMAIL_ATTACHED_METHOD")) {
					case "1":
						chkStock.Checked = true;
						chkMobile.Checked = false;
						break;
					case "2":
						chkStock.Checked = false;
						chkMobile.Checked = true;
						break;
					default:
						chkStock.Checked = false;
						chkMobile.Checked = false;
						break;
				}
				chkTextMailFlag.Checked = (db.GetIntValue("PTEXT_MAIL_FLAG") != 0);
				if ((db.GetIntValue("PTEXT_MAIL_FLAG") != 0)) {
					tabDoc.ActiveTabIndex = 1;
				} else {
					tabDoc.ActiveTabIndex = 0;
				}
				if (!txtHtmlDocSubSeq.Text.Equals("1")) {
					txtTemplateNm.Enabled = false;
					txtMailTitle.Enabled = false;
					txtMobileMailFontSize.Enabled = false;
					txtMobileMailBackColor.Enabled = false;
					lstMailTemplateType.Enabled = false;
					lstWebFaceSeq.Enabled = false;
					lstSexCd.Enabled = false;
					rdoAttachedNone.Enabled = false;
					rdoAttachedPic.Enabled = false;
					rdoAttachedMovie.Enabled = false;
					chkStock.Enabled = false;
					chkMobile.Enabled = false;
					chkTextMailFlag.Enabled = false;
					chkForceTxMobileMailFlag.Enabled = false;
					chkMailDocShortenFlag.Enabled = false;
				} else {
					txtTemplateNm.Enabled = true;
					txtMailTitle.Enabled = true;
					txtMobileMailFontSize.Enabled = true;
					txtMobileMailBackColor.Enabled = true;
					lstMailTemplateType.Enabled = true;
					lstWebFaceSeq.Enabled = true;
					lstSexCd.Enabled = true;
					rdoAttachedNone.Enabled = true;
					rdoAttachedPic.Enabled = true;
					rdoAttachedMovie.Enabled = true;
					chkStock.Enabled = true;
					chkMobile.Enabled = true;
					chkTextMailFlag.Enabled = true;
					chkForceTxMobileMailFlag.Enabled = true;
					chkMailDocShortenFlag.Enabled = true;
				}
				chkCastCreateOrgFlag.Checked = (db.GetIntValue("PCAST_CREATE_ORG_FLAG") != 0);

				for (int i = 0;i < SysConst.MAX_HTML_BLOCKS;i++) {
					if ((SysConst.MAX_HTML_BLOCKS / 2) > i) {
						this.HtmlDoc += db.GetArryStringValue("PHTML_DOC",i);
					} else {
						txtTextDoc.Text += db.GetArryStringValue("PHTML_DOC",i);
					}
				}

			} else {
				ClearField();
			}
		}

		pnlMainte.Visible = true;
		pnlDtl.Visible = true;
		lstSeekSiteCd.SelectedIndex = lstSiteCd.SelectedIndex;
		pnlKey.Enabled = false;
		pnlSelect.Visible = true;
		SetColor(lstWebFaceSeq.SelectedValue,txtMobileMailBackColor.Text);
		grdSiteHtmlDoc.DataSourceID = "dsSiteHtmlDoc";
		grdSiteHtmlDoc.DataBind();
		lblMailTemplateNoMessage.Text = string.Empty;
		lblMailTemplateNoMessage.Visible = false;
		txtHtmlDoc.DocumentBackgroundColor = txtMobileMailBackColor.Text;
	}

	private void UpdateData(int pDelFlag) {
		string[] sDoc;
		string[] sTextDoc;
		int iDocCount;
		int iTextDocCount;
		int iAttachedObjType = 0;
		string sAttachedMethod = string.Empty;
		int iTextMailFlag = 0;
		int iFontSize;

		if (rdoAttachedPic.Checked) {
			iAttachedObjType = ViCommConst.ATTACH_PIC_INDEX;
		} else if (rdoAttachedMovie.Checked) {
			iAttachedObjType = ViCommConst.ATTACH_MOVIE_INDEX;
		}

		if (chkStock.Checked) {
			sAttachedMethod = "1";
		}
		if (chkMobile.Checked) {
			sAttachedMethod = "2";
		}
		if (chkTextMailFlag.Checked) {
			iTextMailFlag = 1;
		}

		SysPrograms.SeparateHtml(HttpUtility.HtmlDecode(this.HtmlDoc),SysConst.MAX_HTML_BLOCKS / 2,out sDoc,out iDocCount);
		SysPrograms.SeparateHtml(txtTextDoc.Text,SysConst.MAX_HTML_BLOCKS / 2,out sTextDoc,out iTextDocCount);

		List<string> oDocList = new List<string>();
		oDocList.AddRange(sDoc);
		oDocList.AddRange(sTextDoc);

		int.TryParse(txtMobileMailFontSize.Text,out iFontSize);

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MAIL_TEMPLATE_MAINTE");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
			db.ProcedureInParm("PMAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,txtMailTemplateNo.Text);
			db.ProcedureInParm("PMAIL_TEMPLATE_TYPE",DbSession.DbType.VARCHAR2,lstMailTemplateType.SelectedValue);
			db.ProcedureInParm("PTEMPLATE_NM",DbSession.DbType.VARCHAR2,txtTemplateNm.Text);
			db.ProcedureInParm("PMAIL_TITLE",DbSession.DbType.VARCHAR2,txtMailTitle.Text);
			db.ProcedureInParm("PEMAIL_ADDR",DbSession.DbType.VARCHAR2,txtMailAddrFrom.Text);
			db.ProcedureInParm("PWEB_FACE_SEQ",DbSession.DbType.VARCHAR2,lstWebFaceSeq.SelectedValue);
			db.ProcedureInParm("PMOBILE_MAIL_FONT_SIZE",DbSession.DbType.VARCHAR2,iFontSize);
			db.ProcedureInParm("PMOBILE_MAIL_BACK_COLOR",DbSession.DbType.VARCHAR2,txtMobileMailBackColor.Text);
			db.ProcedureInParm("PCAST_CREATE_ORG_FLAG",DbSession.DbType.NUMBER,chkCastCreateOrgFlag.Checked);
			db.ProcedureInParm("PMAIL_ATTACHED_OBJ_TYPE",DbSession.DbType.NUMBER,iAttachedObjType);
			db.ProcedureInParm("PMAIL_ATTACHED_METHOD",DbSession.DbType.VARCHAR2,sAttachedMethod);
			db.ProcedureInParm("PTEXT_MAIL_FLAG",DbSession.DbType.NUMBER,iTextMailFlag);
			db.ProcedureInParm("PHTML_DOC_SUB_SEQ",DbSession.DbType.NUMBER,int.Parse(txtHtmlDocSubSeq.Text));
			db.ProcedureInParm("PHTML_DOC_TITLE",DbSession.DbType.VARCHAR2,txtHtmlDocTitle.Text);
			db.ProcedureInArrayParm("PHTML_DOC",DbSession.DbType.VARCHAR2,oDocList.Count,oDocList.ToArray());
			db.ProcedureInParm("PHTML_DOC_COUNT",DbSession.DbType.NUMBER,oDocList.Count);
			db.ProcedureInParm("PNON_DISP_IN_MAIL_BOX",DbSession.DbType.NUMBER,chkNonDispInMailBox.Checked);
			db.ProcedureInParm("PNON_DISP_IN_DECOMAIL",DbSession.DbType.NUMBER,chkNonDispInDecomail.Checked);
			db.ProcedureInParm("PSEX_CD",DbSession.DbType.VARCHAR2,lstSexCd.SelectedValue);
			db.ProcedureInParm("PFORCE_TX_MOBILE_MAIL_FLAG",DbSession.DbType.NUMBER,chkForceTxMobileMailFlag.Checked);
			db.ProcedureInParm("PMAIL_DOC_SHORTEN_FLAG",DbSession.DbType.NUMBER,chkMailDocShortenFlag.Checked);
			db.ProcedureInParm("PROWID_VIEW",DbSession.DbType.VARCHAR2,ViewState["ROWID_VIEW"].ToString());
			db.ProcedureInParm("PREVISION_NO_VIEW",DbSession.DbType.NUMBER,ViewState["REVISION_NO_VIEW"].ToString());
			db.ProcedureInParm("PROWID_MANAGE",DbSession.DbType.VARCHAR2,ViewState["ROWID_MANAGE"].ToString());
			db.ProcedureInParm("PREVISION_NO_MANAGE",DbSession.DbType.NUMBER,ViewState["REVISION_NO_MANAGE"].ToString());
			db.ProcedureInParm("PROWID_DOC",DbSession.DbType.VARCHAR2,ViewState["ROWID_DOC"].ToString());
			db.ProcedureInParm("PREVISION_NO_DOC",DbSession.DbType.NUMBER,ViewState["REVISION_NO_DOC"].ToString());
			db.ProcedureInParm("PDEL_FLAG",DbSession.DbType.NUMBER,pDelFlag);
			db.ProcedureOutParm("PHTML_DOC_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			if (pDelFlag == 0) {
				lblHtmlDocSeq.Text = db.GetStringValue("PHTML_DOC_SEQ");
			}else{
				lblHtmlDocSeq.Text = this.subLblHtmlDocSeq;
			}
		}

		EndEdit();
		
	}

	protected void lnkTemplate_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		lstSiteCd.SelectedValue = sKeys[0];
		txtMailTemplateNo.Text = sKeys[1];
		lblHtmlDocSeq.Text = sKeys[2];
		this.SiteCd = sKeys[0];
		this.MailTemplateNo = sKeys[1];
		this.MailTemplateType = sKeys[4];
		lstSiteCd.Enabled = false;
		pnlMainte.Visible = true;
		pnlSelect.Visible = true;
		pnlDtl.Visible = false;
		lblMailTemplateNoMessage.Visible = false;
		txtCopyMailTemplateNo.Text = string.Empty;
		SetColor(sKeys[3],txtMobileMailBackColor.Text);
		grdSiteHtmlDoc.DataSourceID = "dsSiteHtmlDoc";
		grdSiteHtmlDoc.DataBind();
		txtMailTitle1.Text = getTitle(lstSiteCd.SelectedValue,txtMailTemplateNo.Text,1);
	}

	protected string getTitle(string selectVal,string tempNo,int SubSeq) {

		using (DbSession db = new DbSession()) {
			db.PrepareProcedure("MAIL_TEMPLATE_GET");
			db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,selectVal);
			db.ProcedureInParm("PMAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,tempNo);
			db.ProcedureInParm("PHTML_DOC_SUB_SEQ",DbSession.DbType.NUMBER,SubSeq);
			db.ProcedureOutParm("PMAIL_TEMPLATE_TYPE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PTEMPLATE_NM",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMAIL_TITLE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PEMAIL_ADDR",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PWEB_FACE_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMOBILE_MAIL_FONT_SIZE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PMOBILE_MAIL_BACK_COLOR",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PCAST_CREATE_ORG_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMAIL_ATTACHED_OBJ_TYPE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMAIL_ATTACHED_METHOD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PTEXT_MAIL_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PDEL_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PHTML_DOC_SEQ",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PHTML_DOC_TITLE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutArrayParm("PHTML_DOC",DbSession.DbType.VARCHAR2,SysConst.MAX_HTML_BLOCKS);
			db.ProcedureOutParm("PNON_DISP_IN_MAIL_BOX",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PNON_DISP_IN_DECOMAIL",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PSEX_CD",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PFORCE_TX_MOBILE_MAIL_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PMAIIL_DOC_SHORTEN_FLAG",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID_VIEW",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO_VIEW",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID_MANAGE",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO_MANAGE",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PROWID_DOC",DbSession.DbType.VARCHAR2);
			db.ProcedureOutParm("PREVISION_NO_DOC",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PRECORD_COUNT",DbSession.DbType.NUMBER);
			db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
			db.ExecuteProcedure();
			return db.GetStringValue("PMAIL_TITLE");
			
		}
		
	}

	protected void lnkDoc_Command(object sender,CommandEventArgs e) {
		string[] sKeys = e.CommandArgument.ToString().Split(':');
		txtHtmlDocSubSeq.Text = sKeys[0];
		GetData();
	}

	protected void lnkEdit_Command(object sender,CommandEventArgs e) {

	}
	protected void dsSiteHtmlDoc_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lblHtmlDocSeq.Text;
	}

	protected void dsMailTemplate_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
		using (CastAttrType oAttrType = new CastAttrType()) {
			string sNm1,sNm2;
			oAttrType.GetAttrTypeName(lstSeekSiteCd.SelectedValue,out sNm1,out sNm2);
			lblAttrNm1.Text = DisplayWordUtil.Replace(sNm1);
			lblAttrNm2.Text = DisplayWordUtil.Replace(sNm2);
		}
	}

	protected void dsMailTemplate_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSeekSiteCd.SelectedValue;
		e.InputParameters[1] = lstSeekMailTemplateType.SelectedValue;
	}

	private void EndEdit() {
		ClearField();
		pnlDtl.Visible = false;
		pnlKey.Enabled = true;
		lstSiteCd.Enabled = false;
		SetColor(lstWebFaceSeq.SelectedValue,txtMobileMailBackColor.Text);
		grdSiteHtmlDoc.DataSourceID = "dsSiteHtmlDoc";
		grdSiteHtmlDoc.DataBind();
		grdTemplate.DataBind();
	}

	private void SetColor(string pWebFaceSeq,string pMobileMailBackColor) {
		string sForeColor;
		string sBackColor;
		string sLinkColor;

		using (WebFace oWebFace = new WebFace()) {
			oWebFace.GetOne(pWebFaceSeq);
			sForeColor = oWebFace.colorChar;
			sBackColor = oWebFace.colorBack;
			sLinkColor = oWebFace.colorLink;
		}
		using (Site oSite = new Site()) {
			string sNaWebFace = string.Empty;
			oSite.GetValue(lstSiteCd.SelectedValue,"MAIL_DOC_NOT_USE_WEB_FACE_FLAG",ref sNaWebFace);
			if (sNaWebFace.Equals(ViCommConst.FLAG_ON_STR)) {
				sBackColor = pMobileMailBackColor;
			}
		}

		ltlUserColor.Text = "<style type=\"text/css\">\r\n" +
							"<!--\r\n" +
							".userColor { background-color: " + sBackColor + ";}\r\n" +
							".userColor TD{ color: " + sForeColor + ";}\r\n" +
							".userColor TD A{color: " + sLinkColor + ";font-weight: bold;text-decoration: none;background-color: inherit;}\r\n" +
							"-->\r\n" +
							"</style>";
	}
	protected void btnCSV_Click(object sender,EventArgs e) {
		Response.Filter = filter;
		Response.AddHeader("Content-Disposition",string.Format("attachment;filename=MAIL_TEMPLATE_{0}.CSV",lstSeekSiteCd.SelectedValue));
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		using (MailTemplate oTemplate = new MailTemplate()) {
			DataSet ds = oTemplate.GetCsvData(lstSeekSiteCd.SelectedValue,lstSeekMailTemplateType.SelectedValue);
			SetCsvData(ds);
			Response.End();
		}
	}

	protected void btnTestSend_Click(object sender,EventArgs e) {
		SendTestMail(false);
	}

	protected void btnCopy_Click(object sender,EventArgs e) {
		if (IsValid) {
			string sResult = "0",sNo = txtCopyMailTemplateNo.Text;
			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("MAIL_TEMPLATE_COPY");
				db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
				db.ProcedureInParm("PMAIL_TEMPLATE_NO_FROM",DbSession.DbType.VARCHAR2,txtMailTemplateNo.Text);
				db.ProcedureInParm("PMAIL_TEMPLATE_NO_TO",DbSession.DbType.VARCHAR2,txtCopyMailTemplateNo.Text);
				db.ProcedureOutParm("PRESULT",DbSession.DbType.VARCHAR2);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				db.ExecuteProcedure();
				sResult = db.GetStringValue("PRESULT");
			}
			if (sResult == "1") {
				lblMailTemplateNoMessage.Text = "指定したﾒｰﾙﾃﾝﾌﾟﾚｰﾄNOは既に利用されています";
				lblMailTemplateNoMessage.Visible = true;
			} else {
				EndEdit();
				lblMailTemplateNoMessage.Text = sNo + "にコピーが完了しました";
				lblMailTemplateNoMessage.Visible = true;
			}
		}
	}

	public void SetCsvData(DataSet pDataSet) {

		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");
		string sDtl = "";
		DataRow dr = null;
		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];
			sDtl = "";
			sDtl += dr["SITE_CD"].ToString() + "\t";
			sDtl += dr["MAIL_TEMPLATE_NO"].ToString() + "\t";
			sDtl += dr["MAIL_TEMPLATE_TYPE"].ToString() + "\t";
			sDtl += dr["CAST_CREATE_ORG_SUB_SEQ"].ToString() + "\t";
			sDtl += dr["WEB_FACE_SEQ"].ToString() + "\t";
			sDtl += dr["MOBILE_MAIL_FONT_SIZE"].ToString() + "\t";
			sDtl += dr["MOBILE_MAIL_BACK_COLOR"].ToString() + "\t";
			sDtl += dr["MAIL_TITLE"].ToString() + "\t";
			sDtl += dr["EMAIL_ADDR"].ToString() + "\t";
			sDtl += dr["TEMPLATE_NM"].ToString() + "\t";
			sDtl += dr["HTML_DOC_SUB_SEQ"].ToString() + "\t";
			sDtl += dr["HTML_DOC_TITLE"].ToString() + "\t";
			sDtl += dr["MAIL_ATTACHED_OBJ_TYPE"].ToString() + "\t";
			sDtl += dr["NON_DISP_IN_MAIL_BOX"].ToString() + "\t";
			sDtl += dr["NON_DISP_IN_DECOMAIL"].ToString() + "\t";
			sDtl += dr["SEX_CD"].ToString() + "\t";
			sDtl += dr["MAIL_ATTACHED_METHOD"].ToString() + "\t";
			sDtl += dr["TEXT_MAIL_FLAG"].ToString() + "\t";
			sDtl += dr["FORCE_TX_MOBILE_MAIL_FLAG"].ToString() + "\t";
			sDtl += dr["HTML_DOC1"].ToString() + "\t";
			sDtl += dr["HTML_DOC2"].ToString() + "\t";
			sDtl += dr["HTML_DOC3"].ToString() + "\t";
			sDtl += dr["HTML_DOC4"].ToString() + "\t";
			sDtl += dr["HTML_DOC5"].ToString() + "\t";
			sDtl += dr["HTML_DOC6"].ToString() + "\t";
			sDtl += dr["HTML_DOC7"].ToString() + "\t";
			sDtl += dr["HTML_DOC8"].ToString() + "\t";
			sDtl += dr["HTML_DOC9"].ToString() + "\t";
			sDtl += dr["HTML_DOC10"].ToString() + "\t";
			// テキストメール部分 

			sDtl += ReplaceNewLine2Br(dr["HTML_DOC11"].ToString()) + "\t";
			sDtl += ReplaceNewLine2Br(dr["HTML_DOC12"].ToString()) + "\t";
			sDtl += ReplaceNewLine2Br(dr["HTML_DOC13"].ToString()) + "\t";
			sDtl += ReplaceNewLine2Br(dr["HTML_DOC14"].ToString()) + "\t";
			sDtl += ReplaceNewLine2Br(dr["HTML_DOC15"].ToString()) + "\t";
			sDtl += ReplaceNewLine2Br(dr["HTML_DOC16"].ToString()) + "\t";
			sDtl += ReplaceNewLine2Br(dr["HTML_DOC17"].ToString()) + "\t";
			sDtl += ReplaceNewLine2Br(dr["HTML_DOC18"].ToString()) + "\t";
			sDtl += ReplaceNewLine2Br(dr["HTML_DOC19"].ToString()) + "\t";
			sDtl += ReplaceNewLine2Br(dr["HTML_DOC20"].ToString()) + "\t";
			sDtl = sDtl.Replace("\r","");
			sDtl = sDtl.Replace("\n","");
			sDtl += "\r\n";
			Response.BinaryWrite(encoding.GetBytes(sDtl));
		}
	}

	private string ReplaceNewLine2Br(string sValue) {
		return sValue.Replace("\n","<br />");
	}
	protected string ReplaceForm(object pHtmlDoc) {
		string sUrl = "";
		using (Site oSite = new Site()) {
			if (oSite.GetOne(lstSiteCd.SelectedValue)) {
				sUrl = oSite.url;
			}
		}

		string sDoc = Regex.Replace(pHtmlDoc.ToString(),"%%12%%",sUrl);
		return Regex.Replace(sDoc,@"<\/?[fF][oO][rR][mM].*?>",string.Empty,RegexOptions.Compiled);
	}

	protected void vdcMailAttachedMethod1_ServerValidate(object source,ServerValidateEventArgs args) {
		if (rdoAttachedNone.Checked) {
			chkStock.Checked = false;
			chkMobile.Checked = false;
			return;
		} else if (!chkStock.Checked && !chkMobile.Checked) {
			args.IsValid = false;
			return;
		}
		return;
	}

	protected void vdcMailAttachedMethod2_ServerValidate(object source,ServerValidateEventArgs args) {
		if (rdoAttachedNone.Checked) {
			chkStock.Checked = false;
			chkMobile.Checked = false;
			return;
		} else if (chkStock.Checked && chkMobile.Checked) {
			args.IsValid = false;
			return;
		}
		return;
	}

	protected string GetMimeSizeMark(object pMimeSize) {
		double dByteSize = 0;
		if (double.TryParse(iBridUtil.GetStringValue(pMimeSize),out dByteSize)) {
			return dByteSize > 0 ? string.Format("{0:N2}",dByteSize / 1024) : " - ";
		}

		return " - ";
	}

	protected void dsSiteHtmlDoc_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		GetMimeSize();
	}

	private void GetMimeSize() {
		this.MailTemplateNo = txtMailTemplateNo.Text;
		using (MailTemplate oMailTemplate = new MailTemplate()) {
			this.lblMimeSize.Text = string.Format("MIMEｻｲｽﾞ:{0}KB",GetMimeSizeMark(oMailTemplate.GetMimeSize(this.SiteCd,this.MailTemplateNo)));
		}
	}

	private void SendTestMail(bool bSizeGetOnly) {

		string[] sManSeq = new string[999];
		string[] sManAddr = new string[999];

		string[] sWomanSeq = new string[999];
		string[] sWomanCharNo = new string[999];
		string[] sWomanAddr = new string[999];

		int iManCnt = 0,iWomanCnt = 0;

		using (TestMailAddr oAddr = new TestMailAddr()) {
			DataSet ds = oAddr.GetPageCollection(lstSiteCd.SelectedValue,0,999);
			foreach (DataRow dr in ds.Tables[0].Rows) {
				if (dr["SEX_CD"].ToString().Equals(ViCommConst.MAN)) {
					sManSeq[iManCnt] = dr["USER_SEQ"].ToString();
					sManAddr[iManCnt] = dr["TEST_EMAIL_ADDR"].ToString();
					iManCnt++;
				} else {
					sWomanSeq[iWomanCnt] = dr["USER_SEQ"].ToString();
					sWomanCharNo[iWomanCnt] = dr["USER_CHAR_NO"].ToString();
					sWomanAddr[iWomanCnt] = dr["TEST_EMAIL_ADDR"].ToString();
					iWomanCnt++;
				}
			}
		}

		bool isTextMail = false;
		string sSexCd = "";
		using (MailTemplate oTemplate = new MailTemplate()) {
			if (oTemplate.GetOne(lstSiteCd.SelectedValue,txtMailTemplateNo.Text)) {
				sSexCd = oTemplate.sexCd;
				isTextMail = oTemplate.IsTextMail;
			}
		}

		string sManLoginId = string.Empty;
		string sManCharNo = string.Empty;
		string sCastLoginId = string.Empty;
		string sCastCharNo = string.Empty;

		using (TestMailFrom oFrom = new TestMailFrom()) {
			if (oFrom.GetOne(lstSiteCd.SelectedValue)) {
				sManLoginId = oFrom.fromManLoginId;
				sManCharNo = oFrom.fromManCharNo;
				sCastLoginId = oFrom.fromCastLoginId;
				sCastCharNo = oFrom.fromCastCharNo;
			}
		}

		if (iManCnt > 0 && !sSexCd.Equals(ViCommConst.WOMAN)) {
			using (DbSession db = new DbSession()) {
				if (bSizeGetOnly) {
					iManCnt = 1;
				}
				if (ViCommConst.MAIL_TP_INFO.Equals(this.MailTemplateType)) {
					sCastLoginId = string.Empty;
					sCastCharNo = string.Empty;
				}
				db.PrepareProcedure("TX_ADMIN_TO_MAN_MAIL");
				db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
				db.ProcedureInParm("PMAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,txtMailTemplateNo.Text);
				db.ProcedureInParm("PCAST_LOGIN_ID",DbSession.DbType.VARCHAR2,sCastLoginId);
				db.ProcedureInParm("PCAST_CHAR_NO",DbSession.DbType.VARCHAR2,sCastCharNo);
				db.ProcedureInArrayParm("PMAN_USER_SEQ",DbSession.DbType.VARCHAR2,iManCnt,sManSeq);
				db.ProcedureInParm("PMAN_USER_COUNT",DbSession.DbType.NUMBER,iManCnt);
				db.ProcedureInParm("PMAIL_AVA_HOUR",DbSession.DbType.NUMBER,0);
				db.ProcedureInParm("PPOINT_TRANSFER_AVA_HOUR",DbSession.DbType.NUMBER,0);
				db.ProcedureInParm("PSERVICE_POINT",DbSession.DbType.NUMBER,0);
				db.ProcedureInParm("PORIGINAL_TITLE",DbSession.DbType.VARCHAR2,"");
				db.ProcedureInArrayParm("PORIGINAL_DOC",DbSession.DbType.VARCHAR2,0,null);
				db.ProcedureInParm("PORIGINAL_DOC_COUNT",DbSession.DbType.NUMBER,0);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				if (bSizeGetOnly) {
					db.ProcedureInParm("PTEST_SEND_FLAG",DbSession.DbType.NUMBER,2);
				} else {
					db.ProcedureInParm("PTEST_SEND_FLAG",DbSession.DbType.NUMBER,1);
				}
				db.ProcedureInParm("PMAIL_SEND_TYPE",DbSession.DbType.VARCHAR2,isTextMail ? ViCommConst.MailSendType.TEXT : ViCommConst.MailSendType.DECO_MAIL);
				db.ExecuteProcedure();
			}
		}
		if (iWomanCnt > 0 && !sSexCd.Equals(ViCommConst.MAN)) {
			if (bSizeGetOnly) {
				iWomanCnt = 1;
			}


			using (DbSession db = new DbSession()) {
				db.PrepareProcedure("TX_ADMIN_TO_CAST_MAIL");
				db.ProcedureInParm("PSITE_CD",DbSession.DbType.VARCHAR2,lstSiteCd.SelectedValue);
				db.ProcedureInParm("PMAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,txtMailTemplateNo.Text);
				db.ProcedureInArrayParm("PCAST_USER_SEQ",DbSession.DbType.VARCHAR2,iWomanCnt,sWomanSeq);
				db.ProcedureInParm("PCAST_USER_COUNT",DbSession.DbType.NUMBER,iWomanCnt);
				db.ProcedureInParm("PORIGINAL_TITLE",DbSession.DbType.VARCHAR2,"");
				db.ProcedureInArrayParm("PORIGINAL_DOC",DbSession.DbType.VARCHAR2,0,null);
				db.ProcedureInParm("PORIGINAL_DOC_COUNT",DbSession.DbType.NUMBER,0);
				db.ProcedureOutParm("PSTATUS",DbSession.DbType.VARCHAR2);
				if (bSizeGetOnly) {
					db.ProcedureInParm("PTEST_SEND_FLAG",DbSession.DbType.NUMBER,2);
				} else {
					db.ProcedureInParm("PTEST_SEND_FLAG",DbSession.DbType.NUMBER,1);
				}
				db.ProcedureInParm("PMAIL_SEND_TYPE",DbSession.DbType.VARCHAR2,isTextMail ? ViCommConst.MailSendType.TEXT : ViCommConst.MailSendType.DECO_MAIL);
				db.ExecuteProcedure();
			}

		}
	}
	
	protected void btnBulkDelete_Click(object sender,EventArgs e) {
		List<string> oMailTemplateNoList = new List<string>();
		
		for (int i = 0;i < this.grdTemplate.Rows.Count;i++) {
			GridViewRow row = this.grdTemplate.Rows[i];
			CheckBox oChk = (CheckBox)row.FindControl("chkBulkDelete");
			if (oChk.Checked) {
				HiddenField hdnMailTemplateNo = row.FindControl("hdnMailTemplateNo") as HiddenField;
				oMailTemplateNoList.Add(hdnMailTemplateNo.Value);
			}
		}
		
		MailtemplateDelete(this.lstSeekSiteCd.SelectedValue,oMailTemplateNoList.ToArray());
		
		DataBind();
	}

	protected void btnMailTemplateDelete_Click(object sender,EventArgs e) {
		string[] sMailTemplateNoArr = {txtMailTemplateNo.Text};
		MailtemplateDelete(this.lstSiteCd.SelectedValue,sMailTemplateNoArr);
		EndEdit();
	}
	
	private void MailtemplateDelete (string pSiteCd,string[] pMailTemplateNo) {
		if (pMailTemplateNo.Length > 0) {
			using (DbSession oDbSession = new DbSession()) {
				oDbSession.PrepareProcedure("MAIL_TEMPLATE_DELETE");
				oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,pSiteCd);
				oDbSession.ProcedureInArrayParm("pMAIL_TEMPLATE_NO",DbSession.DbType.VARCHAR2,pMailTemplateNo);
				oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
				oDbSession.ExecuteProcedure();
			}
		}
	}
}
