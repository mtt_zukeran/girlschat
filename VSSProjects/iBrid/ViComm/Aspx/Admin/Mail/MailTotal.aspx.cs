﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: メール記録
--	Progaram ID		: MailInquiry
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater		Update Explain
  2010/06/15  KazuakiItoh	一括送信メールのデフォルト非表示対応


-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Mail_MailTotal:System.Web.UI.Page
{
    private readonly string DEFAULT_OFFSET_COUNT = "0";

    private Int32 recCount = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack)
        {
			FisrtLoad();
			InitPage();
		}
	}

	private void FisrtLoad()
    {
		grdMailTotal.DataSourceID = "";
		DataBind();
		if (Session["SiteCd"].ToString().Equals(""))
        {
			lstSiteCd.Items.Insert(0,new ListItem("",""));
		}
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals(""))
        {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		lstSiteCd.DataSourceID = "";
	}

	private void InitPage()
    {
		pnlInfo.Visible = false;
		ClearField();
	}

	private void ClearField()
    {
        txtOffsetCount.Text = DEFAULT_OFFSET_COUNT;
		txtReportDayFrom.Text = DateTime.Now.ToString("yyyy/MM/dd");
		txtReportDayTo.Text = "";
	}

    protected void btnListSeek_Click(object sender, EventArgs e)
    {
        GetList();
        if (recCount > 0)
        {
            grdMailTotal.PageSize = recCount;
        }
    }

	protected void btnClear_Click(object sender,EventArgs e)
    {
		InitPage();
	}

	private void GetList()
    {
		if (txtReportDayTo.Text.Equals(""))
        {
			txtReportDayTo.Text = txtReportDayFrom.Text;
		}
		grdMailTotal.DataSourceID = "dsMailTotal";
		DataBind();
		pnlInfo.Visible = true;
	}

	protected void dsMailTotal_Selecting(object sender,ObjectDataSourceSelectingEventArgs e)
    {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = txtReportDayFrom.Text;
		e.InputParameters[2] = txtReportDayTo.Text;
        e.InputParameters[3] = Int32.Parse(txtOffsetCount.Text);
	}

	protected void dsMailTotal_Selected(object sender,ObjectDataSourceStatusEventArgs e)
    {
        if (e.ReturnValue != null)
        {
            Int32.TryParse(e.ReturnValue.ToString(), out recCount);
        }
    }

    protected string GetRecCount()
    {
        return recCount.ToString();
    }

}
