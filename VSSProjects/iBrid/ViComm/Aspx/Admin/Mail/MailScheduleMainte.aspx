﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MailScheduleMainte.aspx.cs" Inherits="Mail_MailScheduleMainte"
	Title="メール配信スケジュール" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="メール配信スケジュール"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<asp:Panel runat="server" ID="pnlPattern">
			<%--<a>『tag』+DBのColumn名としてIDを設定する。（Column名がDBに更新される）対象外はControl識別3文字をつける</a>--%>
			<fieldset class="fieldset">
				<legend>[パターン設定]</legend>
				<asp:HiddenField ID="hdfPatternSeq" runat="server" Visible="False" Value="">
				</asp:HiddenField>
				<table border="0" style="width: 900px" class="tableStyle" id="tbPattern">
					<tr>
						<td class="tdHeaderSmallStyle" style="width: 120px; height: 28px;">
							パターン名
						</td>
						<td class="tdDataStyle" style="width: 200px; height: 28px;">
							<asp:TextBox ID="txtPatternName" runat="server" MaxLength="60" Width="150px"></asp:TextBox>
							<asp:RequiredFieldValidator ID="vdrPatternName" runat="server" ErrorMessage="パターン名を入力して下さい。" ControlToValidate="txtPatternName" ValidationGroup="Key">*</asp:RequiredFieldValidator>
						</td>
						<td class="tdHeaderSmallStyle" style="width: 120px; height: 28px;">
							設定
						</td>
						<td class="tdDataStyle" style="width: 250px; height: 28px;">
							<asp:RadioButton ID="rdoMailEnable" runat="server" Checked="True" GroupName="MailEnable" Text="有効">
							</asp:RadioButton>
							<asp:RadioButton ID="rdoMailNoEnable" runat="server" GroupName="MailEnable" Text="無効">
							</asp:RadioButton>
						</td>
						<td class="tdHeaderSmallStyle2" style="width: 120px; height: 28px;">
							ﾒｰﾙｻｰﾊﾞ
						</td>
						<td class="tdDataStyle" style="width: 250px; height: 28px;">
							<asp:PlaceHolder ID="tagMAIL_SERVER" runat="server">
								<asp:RadioButton ID="rdoMailServerBeam" runat="server" GroupName="MailServer" Text="BEAM">
								</asp:RadioButton>
								<asp:RadioButton ID="rdoMailServerMuller3" runat="server" GroupName="MailServer" Text="MULLER3">
								</asp:RadioButton>
							</asp:PlaceHolder>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderSmallStyle" style="width: 120px; height: 28px;">
							サイト
						</td>
						<td class="tdDataStyle" style="width: 200px; height: 28px;">
							<asp:DropDownList ID="tagSITE_CD" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="170px" AutoPostBack="True"
								OnSelectedIndexChanged="tagSITE_CD_SelectedIndexChanged">
							</asp:DropDownList>
						</td>
						<td class="tdHeaderSmallStyle2" style="width: 120px; height: 28px;">
							会員ランク
						</td>
						<td class="tdDataStyle" style="width: 500px; height: 28px;" colspan="3">
							<asp:PlaceHolder ID="tagUSER_RANK" runat="server">
								<asp:CheckBox ID="chkRankA" runat="server" Text="" />
								<asp:CheckBox ID="chkRankB" runat="server" Text="" />
								<asp:CheckBox ID="chkRankC" runat="server" Text="" />
								<asp:CheckBox ID="chkRankD" runat="server" Text="" />
								<asp:CheckBox ID="chkRankE" runat="server" Text="" />
								<asp:CheckBox ID="chkRankF" runat="server" Text="" />
								<asp:CheckBox ID="chkRankG" runat="server" Text="" />
								<asp:CheckBox ID="chkRankH" runat="server" Text="" />
							</asp:PlaceHolder>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderSmallStyle" style="width: 120px">
							会員状態
						</td>
						<td class="tdDataStyle" style="width: 500px;" colspan="3">
							<asp:PlaceHolder ID="tagMAN_USER_STATUS" runat="server">
								<asp:CheckBox ID="chkManTelNoAuth" runat="server" Text="未認証" />
								<asp:CheckBox ID="chkManNew" runat="server" Text="新規" />
								<asp:CheckBox ID="chkManNoReceipt" runat="server" Text="未入金" />
								<asp:CheckBox ID="chkManNonUsed" runat="server" Text="未稼動" />
								<asp:CheckBox ID="chkManDelay" runat="server" Text="遅延" />
								<asp:CheckBox ID="chkManResigned" runat="server" Text="退会" />
								<asp:CheckBox ID="chkManStop" runat="server" Text="禁止" />
								<asp:CheckBox ID="chkManBlack" runat="server" Text="停止" />
								<asp:CheckBox ID="chkManNormal" runat="server" Text="通常" />
							</asp:PlaceHolder>
							<br />
							<asp:CheckBox ID="tagCREDIT_MEASURED_RATE_TARGET" runat="server" Text="ｸﾚｼﾞｯﾄ従量" />
						</td>
						<td class="tdHeaderSmallStyle" style="width: 120px;">
							電話番号
						</td>
						<td class="tdDataStyle" style="width: 180px;">
							<asp:TextBox ID="tagTEL" runat="server" MaxLength="11" Width="80px"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderSmallStyle" style="width: 120px; height: 28px;">
							ﾛｸﾞｲﾝＩＤ
						</td>
						<td class="tdDataStyle" style="width: 200px; height: 28px;">
							<asp:TextBox ID="tagLOGIN_ID" runat="server" MaxLength="8" Width="64px"></asp:TextBox>
						</td>
						<td class="tdHeaderSmallStyle" style="height: 28px; width: 100px;">
							残ポイント
						</td>
						<td class="tdDataStyle" style="width: 200px;">
							<asp:TextBox ID="tagBAL_POINT_FROM" runat="server" MaxLength="6" Width="48px"></asp:TextBox>&nbsp;～
							<asp:TextBox ID="tagBAL_POINT_TO" runat="server" MaxLength="6" Width="48px"></asp:TextBox>
							<asp:CompareValidator ID="vdcBalPointFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="tagBAL_POINT_FROM" ControlToValidate="tagBAL_POINT_TO"
								Operator="GreaterThanEqual" ValidationGroup="Key" Type="Integer">*</asp:CompareValidator>
						</td>
						<td class="tdHeaderSmallStyle" style="width: 120px; height: 28px;">
							ﾒｰﾙｱﾄﾞﾚｽ
						</td>
						<td class="tdDataStyle" style="width: 180px; height: 28px;">
							<asp:TextBox ID="tagEMAIL_ADDR" runat="server" Width="150px"></asp:TextBox>
							<asp:CheckBox ID="tagEXCLUDE_EMAIL_ADDR_FLAG" runat="server" Text="除外" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderSmallStyle" style="width: 120px; height: 28px;">
							入金累計額
						</td>
						<td class="tdDataStyle" style="width: 200px; height: 28px;">
							<asp:TextBox ID="tagTOTAL_RECEIPT_AMT_FROM" runat="server" MaxLength="9" Width="70px"></asp:TextBox>&nbsp;～
							<asp:TextBox ID="tagTOTAL_RECEIPT_AMT_TO" runat="server" MaxLength="9" Width="70px"></asp:TextBox>
							<asp:CompareValidator ID="vdcTotalReceiptAmtFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="tagTOTAL_RECEIPT_AMT_FROM" ControlToValidate="tagTOTAL_RECEIPT_AMT_TO"
								Operator="GreaterThanEqual" ValidationGroup="Key" Type="Integer">*</asp:CompareValidator>
						</td>
						<td class="tdHeaderSmallStyle" style="height: 28px; width: 100px;" id="trTotalReceiptCount" runat="server">
							入金回数
						</td>
						<td class="tdDataStyle" style="width: 200px; height: 28px;">
							<asp:TextBox ID="tagTOTAL_RECEIPT_COUNT_FROM" runat="server" MaxLength="4" Width="48px"></asp:TextBox>&nbsp;～
							<asp:TextBox ID="tagTOTAL_RECEIPT_COUNT_TO" runat="server" MaxLength="4" Width="48px"></asp:TextBox>
							<asp:CompareValidator ID="vdcTotalReceiptCountFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="tagTOTAL_RECEIPT_COUNT_FROM" ControlToValidate="tagTOTAL_RECEIPT_COUNT_TO"
								Operator="GreaterThanEqual" ValidationGroup="Key" Type="Integer">*</asp:CompareValidator>
						</td>
						<td class="tdHeaderSmallStyle" style="height: 28px; width: 120px;">
							ﾊﾝﾄﾞﾙﾈｰﾑ
						</td>
						<td class="tdDataStyle" style="width: 180px; height: 28px;">
							<asp:TextBox ID="tagHANDLE_NM" runat="server" Width="120px"></asp:TextBox>
						</td>
					</tr>
					<tr id="trOnlyMashup" runat="server">
						<td class="tdHeaderSmallStyle" style="width: 140px; height: 28px;">
							ﾎﾟｲﾝﾄｱﾌﾘ累計額
						</td>
						<td class="tdDataStyle" style="width: 200px; height: 28px;">
							<asp:TextBox ID="tagPOINT_AFFILIATE_TOTAL_AMT_FROM" runat="server" MaxLength="9" Width="70px"></asp:TextBox>&nbsp;～
							<asp:TextBox ID="tagPOINT_AFFILIATE_TOTAL_AMT_TO" runat="server" MaxLength="9" Width="70px"></asp:TextBox>
							<asp:CompareValidator ID="vdcMchaTotalReceiptAmtFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="tagPOINT_AFFILIATE_TOTAL_AMT_FROM"
								ControlToValidate="tagPOINT_AFFILIATE_TOTAL_AMT_TO" Operator="GreaterThanEqual" ValidationGroup="Key" Type="Integer">*</asp:CompareValidator>
						</td>
						<td class="tdHeaderSmallStyle2" style="height: 28px; width: 100px;">
							ﾎﾟｲﾝﾄｱﾌﾘ回数
						</td>
						<td class="tdDataStyle" style="width: 200px; height: 28px;">
							<asp:TextBox ID="tagPOINT_AFFILIATE_TOTAL_COUNT_FROM" runat="server" MaxLength="4" Width="48px"></asp:TextBox>&nbsp;～
							<asp:TextBox ID="tagPOINT_AFFILIATE_TOTAL_COUNT_TO" runat="server" MaxLength="4" Width="48px"></asp:TextBox>
							<asp:CompareValidator ID="vdcMchaTotalReceiptCountFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="tagPOINT_AFFILIATE_TOTAL_COUNT_FROM"
								ControlToValidate="tagPOINT_AFFILIATE_TOTAL_COUNT_TO" Operator="GreaterThanEqual" ValidationGroup="Key" Type="Integer">*</asp:CompareValidator>
						</td>
						<td class="tdHeaderSmallStyle" style="width: 120px; height: 28px;">
							請求額
						</td>
						<td class="tdDataStyle" style="width: 180px; height: 28px;">
							<asp:TextBox ID="tagBILL_AMT_FROM" runat="server" MaxLength="9" Width="60px"></asp:TextBox>&nbsp;～
							<asp:TextBox ID="tagBILL_AMT_TO" runat="server" MaxLength="9" Width="60px"></asp:TextBox>
							<asp:CompareValidator ID="vdcBillAmtFrom" runat="server" ErrorMessage="大小関係が正しくありません。" ControlToCompare="tagBILL_AMT_FROM" ControlToValidate="tagBILL_AMT_TO"
								Operator="GreaterThanEqual" ValidationGroup="Key" Type="Integer">*</asp:CompareValidator>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderSmallStyle" style="height: 28px; width: 100px;">
							ﾒｰﾙｱﾄﾞﾚｽ状態
						</td>
						<td class="tdDataStyle" style="width: 300px; height: 28px;">
							<asp:PlaceHolder ID="tagNON_EXIST_MAIL_ADDR_FLAG" runat="server">
								<asp:RadioButton ID="rdoMailOK" runat="server" GroupName="MailType" Text="正常">
								</asp:RadioButton>
								<asp:RadioButton ID="rdoMailNG" runat="server" GroupName="MailType" Text="ﾌｨﾙﾀﾘﾝｸﾞｴﾗｰ">
								</asp:RadioButton>
							</asp:PlaceHolder>
						</td>
						<td class="tdHeaderSmallStyle" style="height: 28px; width: 100px;">
							登録時<br />ｻｰﾋﾞｽPt
						</td>
						<td class="tdDataStyle" style="width: 300px; height: 28px;">
							<asp:PlaceHolder ID="tagREGIST_SERVICE_POINT_FLAG" runat="server">
								<asp:RadioButton ID="rdoRegistServicePointFlagAll" runat="server" GroupName="RegistServicePointFlag" Text="全て">
								</asp:RadioButton>
								<asp:RadioButton ID="rdoRegistServicePointFlagOFF" runat="server" GroupName="RegistServicePointFlag" Text="未付与">
								</asp:RadioButton>
								<asp:RadioButton ID="rdoRegistServicePointFlagON" runat="server" GroupName="RegistServicePointFlag" Text="付与済">
								</asp:RadioButton>
							</asp:PlaceHolder>
						</td>
						<td class="tdHeaderSmallStyle" style="width: 120px; height: 28px;">
							広告コード
						</td>
						<td class="tdDataStyle" style="width: 180px; height: 26px">
							<asp:TextBox ID="tagAD_CD" runat="server" MaxLength="32" Width="200px"></asp:TextBox>
							<asp:RegularExpressionValidator ID="vdeAdCd" runat="server" ErrorMessage="広告コードは32桁以下の英数字で入力して下さい。" ValidationExpression="[a-zA-Z0-9-\/_]{1,32}" ControlToValidate="tagAD_CD"
								ValidationGroup="Key" Display="Dynamic">*</asp:RegularExpressionValidator>
							<asp:CustomValidator ID="vdcAdCd" runat="server" ControlToValidate="tagAD_CD" ErrorMessage="広告コードが未登録です。" OnServerValidate="vdcAdCd_ServerValidate" ValidationGroup="Key"
								Display="Dynamic">*</asp:CustomValidator>
							<asp:Label ID="lblAdNm" runat="server"></asp:Label>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderSmallStyle" style="height: 28px; width: 100px;">
							ﾛｸﾞｲﾝ状態
						</td>
						<td class="tdDataStyle" style="width: 200px; height: 28px;">
							<asp:PlaceHolder ID="tagCHARACTER_ONLINE_STATUS" runat="server">
								<asp:CheckBox ID="chkOnline" runat="server" Text="ｵﾝﾗｲﾝ" />
								<asp:CheckBox ID="chkOffline" runat="server" Text="ｵﾌﾗｲﾝ" />
								<asp:CheckBox ID="chkTalking" runat="server" Text="ﾄｰｸ中" />
							</asp:PlaceHolder>
						</td>
						<td class="tdHeaderSmallStyle" style="width: 120px; height: 28px;">
							備考
						</td>
						<td class="tdDataStyle" style="width: 200px; height: 26px">
							<asp:TextBox ID="tagREMARKS1" runat="server" MaxLength="1024" Width="160px"></asp:TextBox>
						</td>
						<td class="tdHeaderSmallStyle" style="width: 120px; height: 28px;">
							ｷｬﾘｱ
						</td>
						<td class="tdDataStyle" style="width: 180px; height: 28px;">
							<asp:PlaceHolder ID="tagMOBILE_CARRIER_CD" runat="server">
								<asp:CheckBox ID="chkDocomo" runat="server" Text="ﾄﾞｺﾓ" />
								<asp:CheckBox ID="chkAu" runat="server" Text="au" />
								<asp:CheckBox ID="chkSoftbank" runat="server" Text="ｿﾌﾄﾊﾞﾝｸ" /><br />
								<asp:CheckBox ID="chkAndroid" runat="server" Text="Android" />
								<asp:CheckBox ID="chkIPhone" runat="server" Text="iPhone" />
							</asp:PlaceHolder>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderSmallStyle" style="height: 28px; width: 100px;">
							決済履歴
						</td>
						<td class="tdDataStyle" style="width: 180px; height: 28px;">
							<asp:CheckBox ID="tagPOINT_AFFILIATE_TOTAL_COUNT" runat="server" Text="ﾎﾟｲﾝﾄｱﾌﾘ決済履歴あり" />
							<asp:CheckBox ID="tagEXIST_CREDIT_DEAL_FLAG" runat="server" Text="ｸﾚｼﾞｯﾄ決済履歴あり" />
						</td>
						<td class="tdHeaderSmallStyle" style="height: 28px; width: 100px;">
							ｷｬﾗｸﾀ状態
						</td>
						<td class="tdDataStyle" style="width: 180px; height: 28px;">
							<asp:CheckBoxList ID="tagNA_FLAG" runat="server" DataSourceID="dsNaFlag" DataTextField="CODE_NM" DataValueField="CODE" RepeatDirection="Horizontal">
							</asp:CheckBoxList>
						</td>
						<td class="tdHeaderSmallStyle2" style="height: 28px; width: 100px;">
							ﾎﾟｲﾝﾄ未使用
						</td>
						<td class="tdDataStyle" style="width: 180px; height: 28px;">
							<asp:CheckBox ID="tagNOT_EXIST_FIRST_POINT_USED_DATE" runat="server" />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderSmallStyle" style="width: 150px">
							登録日<br />
							初回ﾛｸﾞｲﾝ日<br />
							最終ﾛｸﾞｲﾝ日<br />
							最終課金日<br />
						</td>
						<td class="tdDataStyle" style="width: 200px; height: 28px;">
							<asp:TextBox ID="tagREGIST_DATE" runat="server" MaxLength="3" Width="44px"></asp:TextBox>日経過
							<br />
							<asp:TextBox ID="tagFIRST_LOGIN_DATE" runat="server" MaxLength="3" Width="44px"></asp:TextBox>日経過
							<br />
							<asp:TextBox ID="tagLAST_LOGIN_DATE" runat="server" MaxLength="3" Width="44px"></asp:TextBox>日経過
							<br />
							<asp:TextBox ID="tagLAST_POINT_USED_DATE" runat="server" MaxLength="3" Width="44px"></asp:TextBox>日経過
						</td>
						<td class="tdHeaderSmallStyle2" style="width: 120px">
							初回入金日<br />
							最終入金日<br />
							<label id="lblLastBlackDay" runat="server">
								ﾌﾞﾗｯｸ登録日</label><br />
							<label id="lblUrgeLevel" runat="server">
								督促ﾚﾍﾞﾙ</label><br />
						</td>
						<td class="tdDataStyle" style="width: 500px; height: 28px;" colspan="3">
							<asp:TextBox ID="tagFIRST_RECEIPT_DAY" runat="server" MaxLength="3" Width="44px"></asp:TextBox>日経過
							<br />
							<asp:TextBox ID="tagLAST_RECEIPT_DATE" runat="server" MaxLength="3" Width="44px"></asp:TextBox>日経過
							<br />
							<asp:TextBox ID="tagLAST_BLACK_DAY" runat="server" MaxLength="3" Width="44px"></asp:TextBox><label id="lblLastBlackDay2" runat="server">日経過</label>
							<br />
							<asp:DropDownList ID="tagURGE_LEVEL" runat="server" DataSourceID="dsUrgeLevel" DataTextField="URGE_LEVEL" DataValueField="URGE_LEVEL" Width="40px">
							</asp:DropDownList><br />
						</td>
					</tr>
					<tr>
						<td class="tdHeaderSmallStyle" style="width: 120px; height: 28px;">
							ﾒｰﾙ種別
						</td>
						<td class="tdDataStyle" style=" height: 28px;" colspan="3">
							<asp:DropDownList ID="tagMAIL_TEMPLATE_NO" runat="server" Width="450px" OnSelectedIndexChanged="tagMAIL_TEMPLATE_NO_SelectedIndexChanged" AutoPostBack="True"
								DataValueField="MAIL_TEMPLATE_NO" DataTextField="TEMPLATE_NM">
							</asp:DropDownList>
							<asp:HiddenField ID="hdfMailInfoCnt" runat="server" Visible="False" Value="0">
							</asp:HiddenField>
						</td>
						<td class="tdHeaderSmallStyle" style="height: 28px; width: 100px;">
							ﾘｯﾁｰﾉﾗﾝｸ
						</td>
						<td class="tdDataStyle" style="width: 180px; height: 28px;">
							<asp:DropDownList ID="tagRICHINO_RANK_FROM" runat="server" Width="100px" DataSourceID="dsRichinoRank" OnDataBound="tagRICHINO_RANK_DataBound" DataValueField="RICHINO_RANK" DataTextField="RICHINO_RANK_NM"></asp:DropDownList>
							&nbsp;～&nbsp;
							<asp:DropDownList ID="tagRICHINO_RANK_TO" runat="server" Width="100px" DataSourceID="dsRichinoRank" OnDataBound="tagRICHINO_RANK_DataBound" DataValueField="RICHINO_RANK" DataTextField="RICHINO_RANK_NM"></asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderSmallStyle" style="width: 120px; height: 28px;">
							ｻｰﾋﾞｽP
						</td>
						<td class="tdDataStyle" style="width: 200px; height: 28px;">
							<asp:TextBox ID="tagSERVICE_POINT" runat="server" MaxLength="4" Width="30px"></asp:TextBox>Ｐ
							<asp:RequiredFieldValidator ID="vdrServicePoint" runat="server" ErrorMessage="サービスポイントを入力して下さい。" ControlToValidate="tagSERVICE_POINT" ValidationGroup="Key">*</asp:RequiredFieldValidator>
						</td>
						<td class="tdHeaderStyle" style="width: 100px; height: 28px;" id="trMailAvaHour" runat="server">
							ﾒｰﾙ有効時間
						</td>
						<td class="tdDataStyle" style="width: 200px; height: 28px;">
							<asp:TextBox ID="tagSERVICE_POINT_EFFECTIVE_DATE" runat="server" MaxLength="3" Width="30px"></asp:TextBox>時間
							<asp:RequiredFieldValidator ID="vdrMailAvaHour" runat="server" ErrorMessage="メール有効時間を入力して下さい。" ControlToValidate="tagSERVICE_POINT_EFFECTIVE_DATE" ValidationGroup="Key">*</asp:RequiredFieldValidator>
						</td>
						<td class="tdHeaderStyle2" style="width: 140px; height: 28px;">
							Ｐ振替後有効時間
						</td>
						<td class="tdDataStyle" style="width: 180px; height: 28px;">
							<asp:TextBox ID="tagPOINT_TRANSFER_AVA_HOUR" runat="server" MaxLength="3" Width="30px"></asp:TextBox>時間
							<asp:RequiredFieldValidator ID="vdrPointTransferAvaHour" runat="server" ErrorMessage="ポイント振替後有効時間を入力して下さい。" ControlToValidate="tagPOINT_TRANSFER_AVA_HOUR"
								ValidationGroup="Key">*</asp:RequiredFieldValidator>
						</td>
					</tr>
					<tr runat="server" id="trCastLoginId">
						<td class="tdHeaderSmallStyle" style="width: 120px; height: 28px;">
							<%= DisplayWordUtil.Replace("出演者ID") %>
						</td>
						<td class="tdDataStyle" style="width: 200px; height: 28px;">
							<asp:TextBox ID="tagCAST_LOGIN_ID" runat="server" MaxLength="8" Width="60px"></asp:TextBox>
							<asp:CustomValidator ID="vdcCastLoginId" runat="server" ControlToValidate="tagCAST_LOGIN_ID" ErrorMessage="出演者ログインＩＤが未登録です。" OnServerValidate="vdcCastLoginId_ServerValidate"
								ValidationGroup="Key" Display="Dynamic" ValidateEmptyText="True">*</asp:CustomValidator>
							<asp:Label ID="lblCastNm" runat="server" Text=""></asp:Label>
						</td>
						<td class="tdHeaderSmallStyle" runat="server" style="width: 100px; height: 28px;" id="tdHeaderCastCharNo">
							ｷｬﾗｸﾀｰNo
						</td>
						<td class="tdDataStyle" runat="server" style="width: 200px; height: 28px;" id="tdDataCastCharNo" colspan="3">
							<asp:TextBox ID="tagUSER_CHAR_NO" runat="server" MaxLength="2" Width="35px"></asp:TextBox>
							<asp:CustomValidator ID="vdcCastCharNo" runat="server" ControlToValidate="tagUSER_CHAR_NO" ErrorMessage="出演者キャラクターＮｏが未登録です。" ValidationGroup="Key" Display="Dynamic"
								ValidateEmptyText="True" OnServerValidate="vdcCastCharNo_ServerValidate">*</asp:CustomValidator>
							<asp:HiddenField ID="hdfMaltiCharflg" runat="server" Visible="False" Value="0">
							</asp:HiddenField>
						</td>
					</tr>
                    <tr>
                        <td class="tdHeaderSmallStyle2" style="width: 120px; height: 28px;">
                            配信時間
                        </td>
                        <td class="tdDataStyle" style="width: 200px; height: 28px;">
                            <asp:DropDownList ID="lstTaskHour" runat="server" Width="40px">
                            </asp:DropDownList>
                            <asp:DropDownList ID="lstTaskMin" runat="server" Width="40px">
                            </asp:DropDownList>
                        </td>
                        <td class="tdHeaderSmallStyle2" style="height: 28px; width: 100px;">
                            配信曜日
                        </td>
                        <td class="tdDataStyle" style="width: 500px; height: 28px;" colspan="3">
                            <asp:PlaceHolder ID="plcWeek" runat="server">
                                <asp:CheckBox ID="chkWeek1" runat="server" Text="月曜 " />
                                <asp:CheckBox ID="chkWeek2" runat="server" Text="火曜 " />
                                <asp:CheckBox ID="chkWeek3" runat="server" Text="水曜 " />
                                <asp:CheckBox ID="chkWeek4" runat="server" Text="木曜 " />
                                <asp:CheckBox ID="chkWeek5" runat="server" Text="金曜 " />
                                <asp:CheckBox ID="chkWeek6" runat="server" Text="土曜 " />
                                <asp:CheckBox ID="chkWeek7" runat="server" Text="日曜 " />
                            </asp:PlaceHolder>
                            <asp:Label ID="lblTaskWeek" runat="server" Text="曜日指定して下さい。" Font-Size="Small" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
				</table>
				<asp:Button runat="server" ID="btnUpdate" Text="更新" CssClass="seekbutton" ValidationGroup="Key" OnClick="btnUpdate_Click" />
				<asp:Button runat="server" ID="btnDelete" Text="削除" CssClass="seekbutton" CausesValidation="False" OnClick="btnUpdate_Click" />
				<asp:Button runat="server" ID="btnCancel" Text="キャンセル" CssClass="seekbutton" CausesValidation="False" OnClick="btnCancel_Click" />
			</fieldset>
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlList">
			<fieldset>
				<legend>[パターン一覧]</legend>
				<asp:GridView ID="grdPattern" runat="server" AllowPaging="True" DataSourceID="dsSchedulePattern" AllowSorting="True" SkinID="GridView" AutoGenerateColumns="False"
					Width="900px" OnRowDataBound="grdPattern_RowDataBound">
					<Columns>
						<asp:TemplateField HeaderText="パターン名">
							<ItemTemplate>
								<asp:LinkButton ID="lnkPatternNm" runat="server" Text='<%# Eval("PATTERN_NAME") %>' CommandArgument='<%# Eval("PATTERN_SEQ") %>' OnCommand="lnkPatternNm_Command"
									CausesValidation="False"></asp:LinkButton>
							</ItemTemplate>
							<HeaderTemplate>
								パターン名
							</HeaderTemplate>
							<HeaderStyle Width="250px" />
						</asp:TemplateField>
						<asp:BoundField DataField="TX_TIME" HeaderText="配信時刻" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" />
							<HeaderStyle Width="100px" />
						</asp:BoundField>
						<asp:BoundField DataField="PATTERN_FLG" HeaderText="設定" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" />
							<HeaderStyle Width="50px" />
						</asp:BoundField>
						<asp:BoundField DataField="TX_WEEK1" HeaderText="月" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" />
							<HeaderStyle Width="50px" />
						</asp:BoundField>
						<asp:BoundField DataField="TX_WEEK2" HeaderText="火" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" />
							<HeaderStyle Width="50px" />
						</asp:BoundField>
						<asp:BoundField DataField="TX_WEEK3" HeaderText="水" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" />
							<HeaderStyle Width="50px" />
						</asp:BoundField>
						<asp:BoundField DataField="TX_WEEK4" HeaderText="木" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" />
							<HeaderStyle Width="50px" />
						</asp:BoundField>
						<asp:BoundField DataField="TX_WEEK5" HeaderText="金" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" />
							<HeaderStyle Width="50px" />
						</asp:BoundField>
						<asp:BoundField DataField="TX_WEEK6" HeaderText="土" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" />
							<HeaderStyle Width="50px" />
						</asp:BoundField>
						<asp:BoundField DataField="TX_WEEK7" HeaderText="日" HtmlEncode="False">
							<ItemStyle HorizontalAlign="Center" />
							<HeaderStyle Width="50px" />
						</asp:BoundField>
						<asp:BoundField DataField="TX_MAIL_LAST_DATE" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HeaderText="最新配信日" HtmlEncode="False">
							<HeaderStyle Width="150px" />
						</asp:BoundField>
					</Columns>
				</asp:GridView>
				<a class="reccount">Record Count
					<%# GetRecCount() %>
				</a>
				<br>
				<a class="reccount">Current viewing page
					<%=grdPattern.PageIndex + 1%>
					of
					<%=grdPattern.PageCount%>
				</a>
				<asp:Button ID="btnRegist" runat="server" Text="パターン追加" OnClick="btnRegist_Click" />
			</fieldset>
		</asp:Panel>
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUrgeLevel" runat="server" SelectMethod="GetList" TypeName="UrgeLevel" OnSelecting="dsUrgeLevel_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSchedulePattern" runat="server" EnablePaging="True" SelectCountMethod="GetPageCount" SelectMethod="GetPageCollection" TypeName="SchedulePattern"
		OnSelecting="dsSchedulePattern_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSexCd" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsNaFlag" runat="server" SelectMethod="GetList" TypeName="CodeDtl">
		<SelectParameters>
			<asp:Parameter DefaultValue="98" Name="pCodeType" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsRichinoRank" runat="server" SelectMethod="GetList" TypeName="RichinoRank"></asp:ObjectDataSource>
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum1" runat="server" Enabled="true" FilterType="Numbers,Custom" ValidChars="-" TargetControlID="tagBAL_POINT_FROM" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum2" runat="server" Enabled="true" FilterType="Numbers,Custom" ValidChars="-" TargetControlID="tagBAL_POINT_TO" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum3" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="tagTOTAL_RECEIPT_AMT_FROM" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum4" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="tagTOTAL_RECEIPT_AMT_TO" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum5" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="tagTOTAL_RECEIPT_COUNT_FROM" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum6" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="tagTOTAL_RECEIPT_COUNT_TO" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum7" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="tagPOINT_AFFILIATE_TOTAL_AMT_FROM" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum8" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="tagPOINT_AFFILIATE_TOTAL_AMT_TO" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum9" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="tagPOINT_AFFILIATE_TOTAL_COUNT_FROM" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum10" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="tagPOINT_AFFILIATE_TOTAL_COUNT_TO" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum11" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="tagBILL_AMT_FROM" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum12" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="tagBILL_AMT_TO" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum13" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="tagREGIST_DATE" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum14" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="tagFIRST_LOGIN_DATE" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum15" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="tagLAST_LOGIN_DATE" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum16" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="tagLAST_POINT_USED_DATE" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum17" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="tagFIRST_RECEIPT_DAY" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum18" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="tagLAST_RECEIPT_DATE" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum19" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="tagLAST_BLACK_DAY" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum20" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="tagSERVICE_POINT" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum21" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="tagSERVICE_POINT_EFFECTIVE_DATE" />
	<ajaxToolkit:FilteredTextBoxExtender ID="FilterNum22" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="tagPOINT_TRANSFER_AVA_HOUR" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrPatternName" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdcBalPointFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="vdcTotalReceiptAmtFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="vdcTotalReceiptCountFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5" TargetControlID="vdcMchaTotalReceiptAmtFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="vdcMchaTotalReceiptCountFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7" TargetControlID="vdcBillAmtFrom" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8" TargetControlID="vdcAdCd" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9" TargetControlID="vdrServicePoint" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10" TargetControlID="vdrMailAvaHour" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11" TargetControlID="vdrPointTransferAvaHour" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender12" TargetControlID="vdcCastLoginId" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender14" TargetControlID="vdcCastCharNo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnUpdate" ConfirmText="更新を行ないますか？" ConfirmOnFormSubmit="true" />
	<ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="btnDelete" ConfirmText="削除を実行しますか？" ConfirmOnFormSubmit="true" />
</asp:Content>
