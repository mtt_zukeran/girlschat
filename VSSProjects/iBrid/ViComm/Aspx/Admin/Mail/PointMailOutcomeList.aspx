﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="PointMailOutcomeList.aspx.cs" Inherits="Mail_PointMailOutcomeList"
    Title="ポイントメール成果" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<%@ Import Namespace="System" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="ポイントメール成果"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[検索条件]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 640px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                サイトコード
                            </td>
                            <td class="tdDataStyle" colspan="3">
                                <asp:DropDownList ID="lstSeekSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="180px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                送信日
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstReportFromYYYY" runat="server" Width="60px">
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstReportFromMM" runat="server" Width="40px">
                                </asp:DropDownList>月
                                <asp:DropDownList ID="lstReportFromDD" runat="server" Width="40px">
                                </asp:DropDownList>日 &nbsp;～&nbsp;
                                <asp:DropDownList ID="lstReportToYYYY" runat="server" Width="60px">
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstReportToMM" runat="server" Width="40px">
                                </asp:DropDownList>月
                                <asp:DropDownList ID="lstReportToDD" runat="server" Width="40px">
                                </asp:DropDownList>日<br />
                                <asp:CustomValidator runat="server" ID="vdcReportDay" OnServerValidate="vdcReportDay_ServerValidate"
                                    ValidationGroup="Seek"></asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle2">
                                備考
                            </td>
                            <td class="tdDataStyle">
                                <asp:TextBox ID="txtSeekRemarks" runat="server" MaxLength="1000" Width="400px" />
                            </td>
                        </tr>
                    </table>
                    <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                        ValidationGroup="Seek" />
                    <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[ポイントメール成果一覧] </legend>
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="483px">
                    <asp:GridView ID="grdPointMailOutcome" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                        EnableViewState="true" DataSourceID="dsPointMailOutcome" AllowSorting="True"
                        ShowFooter="True" OnRowDataBound="grdPointMailOutcome_RowDataBound" SkinID="GridView">
                        <Columns>
                            <asp:BoundField HeaderText="送信日時" DataField="REQUEST_TX_DATE" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" />
                            <asp:TemplateField HeaderText="備考">
                                <ItemTemplate>
                                    <asp:Label ID="lblRemarks" runat="server" Text='<%# GetRemarks(Eval("REMARKS")) %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" Width="500px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="MAN_LOGIN_COUNT" HeaderText="総ログイン数">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="COME_BACK_COUNT" HeaderText="復活数">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="COME_BACK_SETTLE_COUNT" HeaderText="復活入金数">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="SERVICE_POINT" HeaderText="ｻｰﾋﾞｽﾎﾟｲﾝﾄ">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="MAN_TRANSFER_POINT" HeaderText="振替ｻｰﾋﾞｽﾎﾟｲﾝﾄ計">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                        </Columns>
                        <FooterStyle ForeColor="Black" BackColor="LightYellow" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
            <br />
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsPointMailOutcome" runat="server" SelectMethod="GetList"
        TypeName="PointMailOutcome" OnSelecting="dsPointMailOutcome_Selecting">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstSeekSiteCd" Name="pSiteCd" PropertyName="SelectedValue"
                Type="String" />
            <asp:Parameter Name="pReportDayFrom" Type="String" />
            <asp:Parameter Name="pReportDayTo" Type="String" />
            <asp:Parameter Name="pRemarks" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
</asp:Content>
