﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Progaram ID		: ProfAccessCountDailyList
--
--  Creation Date	: 2015.09.30
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Status_ProfAccessCountDailyList:System.Web.UI.Page {
	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SiteCd"]);
		}
		set {
			this.ViewState["SiteCd"] = value;
		}
	}

	private string Year {
		get {
			return iBridUtil.GetStringValue(this.ViewState["Year"]);
		}
		set {
			this.ViewState["Year"] = value;
		}
	}

	private string Month {
		get {
			return iBridUtil.GetStringValue(this.ViewState["Month"]);
		}
		set {
			this.ViewState["Month"] = value;
		}
	}

	private string Day {
		get {
			return iBridUtil.GetStringValue(this.ViewState["Day"]);
		}
		set {
			this.ViewState["Day"] = value;
		}
	}

	private string SortExpression {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortExpression"]);
		}
		set {
			this.ViewState["SortExpression"] = value;
		}
	}

	private string SortDirect {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SortDirect"]);
		}
		set {
			this.ViewState["SortDirect"] = value;
		}
	}

	string recCount;

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!IsPostBack) {
			this.SiteCd = this.Request.QueryString["sitecd"];
			this.Year = this.Request.QueryString["year"];
			this.Month = this.Request.QueryString["month"];
			this.Day = this.Request.QueryString["day"];

			SysPrograms.SetupDay(lstYear,lstMonth,lstDay,false);
			
			this.InitPage();

			if (!string.IsNullOrEmpty(this.SiteCd)) {
				this.lstSiteCd.SelectedValue = this.SiteCd;
			} else if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
				this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}

			if (!string.IsNullOrEmpty(this.Year)) {
				this.lstYear.SelectedValue = this.Year;
			}

			if (!string.IsNullOrEmpty(this.Month)) {
				this.lstMonth.SelectedValue = this.Month;
			}

			if (!string.IsNullOrEmpty(this.Day)) {
				this.lstDay.SelectedValue = this.Day;
			}

			this.GetList();
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		if (this.IsValid) {
			this.SortDirect = string.Empty;
			this.SortExpression = string.Empty;
			
			this.GetList();
		}
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		this.InitPage();
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		if (this.grdProfAccessCountDaily.Rows.Count == 0) {
			return;
		}

		DataTable oCsvData;
		using (ProfAccessCountDaily oProfAccessCountDaily = new ProfAccessCountDaily()) {
			ProfAccessCountDaily.SearchCondition oSearchCondition = new ProfAccessCountDaily.SearchCondition();
			oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
			oSearchCondition.Year = this.lstYear.SelectedValue;
			oSearchCondition.Month = this.lstMonth.SelectedValue;
			oSearchCondition.Day = this.lstDay.SelectedValue;
			
			DataSet oDataSet = oProfAccessCountDaily.GetPageCollection(oSearchCondition,0,SysConst.DB_MAX_ROWS);

			if (oDataSet.Tables[0].Rows.Count > 0) {
				oCsvData = oDataSet.Tables[0];
			} else {
				return;
			}
		}

		//ヘッダ作成
		string sHeader = "リファラ,ページ名称,アクセス数,アクセス数(ユニーク)";
		
		//CSVファイル名
		string sFileNm = string.Format("ProfAccessCountDailyList{0}{1}{2}.csv",lstYear.SelectedValue,lstMonth.SelectedValue,lstDay.SelectedValue);

		//CSV出力 
		Response.ContentType = "application/download";
		Response.AppendHeader("Content-Disposition","attachment;filename=" + sFileNm);
		Response.ContentEncoding = System.Text.Encoding.GetEncoding("Shift_JIS");

		Response.Write(sHeader + "\r\n");
		foreach (DataRow oCsvRow in oCsvData.Rows) {
			string sData =
				oCsvRow["REFERER_ASPX_NM"].ToString() + "," +
				oCsvRow["DISPLAY_NM"].ToString() + "," +
				oCsvRow["ACCESS_COUNT"].ToString() + "," +
				oCsvRow["UNIQUE_ACCESS_COUNT"].ToString();

			Response.Write(sData + "\r\n");
		}
		Response.End();
	}

	protected void dsProfAccessCountDaily_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		ProfAccessCountDaily.SearchCondition oSearchCondition = new ProfAccessCountDaily.SearchCondition();
		oSearchCondition.SiteCd = this.lstSiteCd.SelectedValue;
		oSearchCondition.Year = this.lstYear.SelectedValue;
		oSearchCondition.Month = this.lstMonth.SelectedValue;
		oSearchCondition.Day = this.lstDay.SelectedValue;
		oSearchCondition.SortDirection = this.SortDirect;
		oSearchCondition.SortExpression = this.SortExpression;

		e.InputParameters[0] = oSearchCondition;
	}

	protected void dsProfAccessCountDaily_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void grdProfAccessCountDaily_DataBound(object sender,EventArgs e) {
		
	}

	private void InitPage() {
		lstYear.SelectedValue = DateTime.Now.AddDays(-1).ToString("yyyy");
		lstMonth.SelectedValue = DateTime.Now.AddDays(-1).ToString("MM");
		lstDay.SelectedValue = DateTime.Now.AddDays(-1).ToString("dd");
	}

	private void GetList() {
		this.grdProfAccessCountDaily.PageSize = 100;
		this.grdProfAccessCountDaily.DataSourceID = "dsProfAccessCountDaily";
		this.grdProfAccessCountDaily.DataBind();
		this.pnlCount.DataBind();
		this.pnlInfo.Visible = true;
	}

	protected string GetRecCount() {
		return this.recCount;
	}

	protected void vdcReportDay_ServerValidate(object source,ServerValidateEventArgs args) {
		if (this.IsValid) {
			DateTime dtReportDay;
			if (!DateTime.TryParse(string.Format("{0}/{1}/{2}",this.lstYear.SelectedValue,this.lstMonth.SelectedValue,this.lstDay.SelectedValue),out dtReportDay)) {
				args.IsValid = false;
			}
		}
	}

	protected void grdProfAccessCountDaily_Sorting(object sender,GridViewSortEventArgs e) {
		if (this.SortExpression.Equals(e.SortExpression)) {
			if (this.SortDirect.Equals("ASC")) {
				this.SortDirect = "DESC";
			} else if (this.SortDirect.Equals("DESC")) {
				this.SortDirect = "ASC";
			}
		} else {
			this.SortDirect = "DESC";
		}
		this.SortExpression = e.SortExpression;

		e.Cancel = true;
		this.GetList();
	}

	protected void grdProfAccessCountDaily_Sorted(object sender,EventArgs e) {
	}
}
