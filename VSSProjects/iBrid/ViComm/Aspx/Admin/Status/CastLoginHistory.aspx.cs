﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者待機履歴
--	Progaram ID		: CastLoginHistory
--
--  Creation Date	: 2010.05.12
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using iBridCommLib;
using ViComm;

public partial class Status_CastLoginHistory:Page {
	private Stream filter;

	protected void Page_Load(object sender,EventArgs e) {

		filter = Response.Filter;
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);

		if (!IsPostBack) {
			FisrtLoad();
			InitPage();
		}
	}

	private void FisrtLoad() {
		grdLogin.PageSize = 100;
		lstSiteCd.DataBind();
		if (Session["SiteCd"].ToString().Equals("")) {
			lstSiteCd.Items.Insert(0,new ListItem("",""));
		}
		lstSiteCd.DataSourceID = "";
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void dsLoginCharacter_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = CreateDateString(txtReportDayFrom.Text,lstReportTimeFrom.Text);

		if (txtReportDayTo.Text.Equals("")) {
			txtReportDayTo.Text = txtReportDayFrom.Text;
		}
		e.InputParameters[2] = CreateDateString(txtReportDayTo.Text,lstReportTimeTo.SelectedValue);
		if (rdoDetail.Checked) {
			e.InputParameters[3] = 0;
		} else {
			e.InputParameters[3] = 1;
		}
		e.InputParameters[4] = txtLoginId.Text;
		e.InputParameters[5] = txtHandelNm.Text;
	}

	private void InitPage() {
		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);
		grdLogin.PageSize = 1;
		if (!IsPostBack) {
			SysPrograms.SetupTime(lstReportTimeFrom,lstReportTimeTo);
		}
		pnlInfo.Visible = false;
		ClearField();
	}

	private void ClearField() {
		txtReportDayFrom.Text = DateTime.Now.ToString("yyyy/MM/dd");
		txtReportDayTo.Text = DateTime.Now.ToString("yyyy/MM/dd");
		lstReportTimeFrom.SelectedIndex = 0;
		lstReportTimeTo.SelectedIndex = 0;
		txtLoginId.Text = "";
		txtHandelNm.Text = "";
	}

	private void GetList() {
		if (rdoDetail.Checked) {
			grdLogin.PageSize = 100;
			grdLogin.Columns[0].Visible = true;
			grdLogin.Columns[1].Visible = true;
			grdLogin.Columns[4].Visible = false;
			grdLogin.Columns[5].Visible = false;
			grdLogin.Sort("LOGIN_SEQ",SortDirection.Descending);
		} else {
			grdLogin.PageSize = 99999;
			grdLogin.Columns[0].Visible = false;
			grdLogin.Columns[1].Visible = false;
			grdLogin.Columns[4].Visible = true;
			grdLogin.Columns[5].Visible = true;
			grdLogin.Sort("LOGIN_ID",SortDirection.Ascending);
		}

		if (chkPagingOff.Checked) {
			grdLogin.PageSize = 99999;
			grdLogin.AllowSorting = false;
		} else {
			grdLogin.PageSize = 100;
			grdLogin.AllowSorting = true;
		}

		grdLogin.DataSourceID = "dsLoginCharacter";
		grdLogin.DataBind();
		pnlInfo.Visible = true;
	}

	private string CreateDateString(string pDate,string pHh) {
		return string.Format("{0} {1}",pDate,pHh);
	}

	protected void rdoList_CheckedChanged(object sender,EventArgs e) {
		GetList();
	}

	protected void grdLogin_Sorting(object sender,GridViewSortEventArgs e) {
//		e.SortDirection = SortDirection.Descending;
	}
}
