﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 未確認投稿確認
--	Progaram ID		: UnconfirmedContributionList
--
--  Creation Date	: 2016.08.29
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/
using System;
using System.Data;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class Status_UnconfirmedContributionList:System.Web.UI.Page {

	protected void Page_Load(object sender,EventArgs e) {
		this.ClearRow();
		this.CheckUnconfirmCount();
	}

	/// <summary>
	/// 件数表示初期化
	/// </summary>
	private void ClearRow() {
		pnlList.Visible = false;
		pnlCastBbsLogCount.Visible = false;
		pnlCastDiaryCount.Visible = false;
		pnlCastProfileMovieCount.Visible = false;
		pnlCastBbsPicCount.Visible = false;
		pnlCastBbsMovieCount.Visible = false;
		pnlCastStockPicCount.Visible = false;
		pnlCastStockMovieCount.Visible = false;
		pnlCastYakyukenCommentCount.Visible = false;
		pnlCastYakyukenPicCount.Visible = false;
		pnlManBbsLogCount.Visible = false;
		pnlManProfilePicCount.Visible = false;
		pnlManTweetCount.Visible = false;
		pnlManTweetPicCount.Visible = false;
		lblCastBbsLogCount.Text = "0";
		lblCastDiaryCount.Text = "0";
		lblCastProfileMovieCount.Text = "0";
		lblCastBbsPicCount.Text = "0";
		lblCastBbsMovieCount.Text = "0";
		lblCastStockPicCount.Text = "0";
		lblCastStockMovieCount.Text = "0";
		lblCastYakyukenCommentCount.Text = "0";
		lblCastYakyukenPicCount.Text = "0";
		lblManBbsLogCount.Text = "0";
		lblManProfilePicCount.Text = "0";
		lblManTweetCount.Text = "0";
		lblManTweetPicCount.Text = "0";
		// 【ｿｰｼｬﾙｹﾞｰﾑ】認証待ち男性用お宝画像
		pnlGameManTreasureCount.Visible = false;
		lblGameManTreasureCount.Text = "0";
		// 【ｿｰｼｬﾙｹﾞｰﾑ】認証待ちお宝動画
		pnlGameMovieCount.Visible = false;
		lblGameMovieCount.Text = "0";
		// 【出演者】野球拳ジャンケン画像
		pnlCastYakyukenJyankenPicCount.Visible = false;
		lblCastYakyukenJyankenPicCount.Text = "0";
		// 【出演者】公開待ちブログ記事
		pnlCastBlogArticleCount.Visible = false;
		lblCastBlogArticleCount.Text = "0";
		// 【会員】退会申請検索(友達紹介登録)
		pnlWithdrawalIntroduceManCount.Visible = false;
		lblWithdrawalIntroduceManCount.Text = "0";
	}
	
	private void CheckUnconfirmCount() {
		string sYesterday = DateTime.Today.AddDays(-1).ToString("yyyy/MM/dd");
		using (BbsLog oBbsLog = new BbsLog()) {
			int iCastBbsLogCount = oBbsLog.GetUnconfirmCount(PwViCommConst.MAIN_SITE_CD,ViCommConst.OPERATOR,sYesterday);
			int iManBbsLogCount = oBbsLog.GetUnconfirmCount(PwViCommConst.MAIN_SITE_CD,ViCommConst.MAN,sYesterday);

			if (iCastBbsLogCount > 0) {
				lblCastBbsLogCount.Text = iBridUtil.GetStringValue(iCastBbsLogCount);
				pnlCastBbsLogCount.Visible = true;
				pnlList.Visible = true;
			}

			if (iManBbsLogCount > 0) {
				lblManBbsLogCount.Text = iBridUtil.GetStringValue(iManBbsLogCount);
				pnlManBbsLogCount.Visible = true;
				pnlList.Visible = true;
			}
		}

		using (BlogArticle oBlogArticle = new BlogArticle()) {
			int iCastBlogArticleCount = oBlogArticle.GetUncofirmCount(PwViCommConst.MAIN_SITE_CD);

			// 【出演者】公開待ちブログ記事
			if (iCastBlogArticleCount > 0) {
				lblCastBlogArticleCount.Text = iBridUtil.GetStringValue(iCastBlogArticleCount);
				pnlCastBlogArticleCount.Visible = true;
				pnlList.Visible = true;
			}
		}
		
		using (CastDiary oCastDiary = new CastDiary()) {
			int iCastDiaryCount = oCastDiary.GetUnconfirmCount(PwViCommConst.MAIN_SITE_CD,sYesterday);
			if (iCastDiaryCount > 0) {
				lblCastDiaryCount.Text = iBridUtil.GetStringValue(iCastDiaryCount);
				pnlCastDiaryCount.Visible = true;
				pnlList.Visible = true;
			}
		}
		
		using (CastPic oCastPic = new CastPic()) {
			int iCastBbsPicCount = oCastPic.GetApproveManagePageCount(PwViCommConst.MAIN_SITE_CD,ViCommConst.FLAG_ON_STR,string.Empty,ViCommConst.ATTACHED_BBS.ToString(),string.Empty,string.Empty,string.Empty);
			int iCastStockPicCount = oCastPic.GetApproveManagePageCount(PwViCommConst.MAIN_SITE_CD,ViCommConst.FLAG_ON_STR,string.Empty,ViCommConst.ATTACHED_MAIL.ToString(),string.Empty,string.Empty,string.Empty);
			
			if (iCastBbsPicCount > 0) {
				lblCastBbsPicCount.Text = iBridUtil.GetStringValue(iCastBbsPicCount);
				pnlCastBbsPicCount.Visible = true;
				pnlList.Visible = true;	
			}
			
			if (iCastStockPicCount > 0) {
				lblCastStockPicCount.Text = iBridUtil.GetStringValue(iCastStockPicCount);
				pnlCastStockPicCount.Visible = true;
				pnlList.Visible = true;
			}
		}

		using (CastMovie oCastMovie = new CastMovie()) {
			int iCastProfileMovieCount = oCastMovie.GetApproveManagePageCount(PwViCommConst.MAIN_SITE_CD,ViCommConst.FLAG_ON_STR,string.Empty,ViCommConst.ATTACHED_PROFILE.ToString(),string.Empty,string.Empty,string.Empty);
			int iCastBbsMovieCount = oCastMovie.GetApproveManagePageCount(PwViCommConst.MAIN_SITE_CD,ViCommConst.FLAG_ON_STR,string.Empty,ViCommConst.ATTACHED_BBS.ToString(),string.Empty,string.Empty,string.Empty);
			int iCastStockMovieCount = oCastMovie.GetApproveManagePageCount(PwViCommConst.MAIN_SITE_CD,ViCommConst.FLAG_ON_STR,string.Empty,ViCommConst.ATTACHED_MAIL.ToString(),string.Empty,string.Empty,string.Empty);
			int iCastGameMovieCount = oCastMovie.GetApproveManagePageCount(PwViCommConst.MAIN_SITE_CD,ViCommConst.FLAG_ON_STR,string.Empty,ViCommConst.ATTACHED_SOCIAL_GAME.ToString(),string.Empty,string.Empty,string.Empty);

			if (iCastProfileMovieCount > 0) {
				lblCastProfileMovieCount.Text = iBridUtil.GetStringValue(iCastProfileMovieCount);
				pnlCastProfileMovieCount.Visible = true;
				pnlList.Visible = true;
			}
			
			if (iCastBbsMovieCount > 0) {
				lblCastBbsMovieCount.Text = iBridUtil.GetStringValue(iCastBbsMovieCount);
				pnlCastBbsMovieCount.Visible = true;
				pnlList.Visible = true;
			}

			if (iCastStockMovieCount > 0) {
				lblCastStockMovieCount.Text = iBridUtil.GetStringValue(iCastStockMovieCount);
				pnlCastStockMovieCount.Visible = true;
				pnlList.Visible = true;
			}

			// 【ｿｰｼｬﾙｹﾞｰﾑ】認証待ちお宝動画
			if (iCastGameMovieCount > 0) {
				lblGameMovieCount.Text = iBridUtil.GetStringValue(iCastGameMovieCount);
				pnlGameMovieCount.Visible = true;
				pnlList.Visible = true;
			}
		}
		
		using (YakyukenComment oYakyukenComment = new YakyukenComment()) {
			int iCastYakyukenCommentCount = oYakyukenComment.GetUnconfirmCount(PwViCommConst.MAIN_SITE_CD);
			
			if (iCastYakyukenCommentCount > 0) {
				lblCastYakyukenCommentCount.Text = iBridUtil.GetStringValue(iCastYakyukenCommentCount);
				pnlCastYakyukenCommentCount.Visible = true;
				pnlList.Visible = true;
			}
		}

		using (YakyukenPic oYakyukenPic = new YakyukenPic()) {
			int iCastYakyukenPicCount = oYakyukenPic.GetUnconfirmCount(PwViCommConst.MAIN_SITE_CD);
			
			if (iCastYakyukenPicCount > 0) {
				lblCastYakyukenPicCount.Text = iBridUtil.GetStringValue(iCastYakyukenPicCount);
				pnlCastYakyukenPicCount.Visible = true;
				pnlList.Visible = true;
			}
		}

		using (YakyukenJyankenPic oYakyukenJyankenPic = new YakyukenJyankenPic()) {
			int iCastYakyukenJyankenPicCount = oYakyukenJyankenPic.GetUncofirmCount(PwViCommConst.MAIN_SITE_CD);

			// 【出演者】野球拳ジャンケン画像
			if (iCastYakyukenJyankenPicCount > 0) {
				lblCastYakyukenJyankenPicCount.Text = iBridUtil.GetStringValue(iCastYakyukenJyankenPicCount);
				pnlCastYakyukenJyankenPicCount.Visible = true;
				pnlList.Visible = true;
			}
		}
		
		using (UserManCharacter oUserManCharacter = new UserManCharacter()) {
			int iManProfilePicCount = oUserManCharacter.GetProfilePicPageCount(PwViCommConst.MAIN_SITE_CD,ViCommConst.FLAG_ON,string.Empty,string.Empty,string.Empty);
			
			if (iManProfilePicCount > 0) {
				lblManProfilePicCount.Text = iBridUtil.GetStringValue(iManProfilePicCount);
				pnlManProfilePicCount.Visible = true;
				pnlList.Visible = true;
			}
		}
		
		using (ManTweet oManTweet = new ManTweet()) {
			int iManTweetCount = oManTweet.GetUnconfirmCount(PwViCommConst.MAIN_SITE_CD,sYesterday);
			int iManTweetPicCount = oManTweet.GetUnAuthPicCount(PwViCommConst.MAIN_SITE_CD);
			
			if (iManTweetCount > 0) {
				lblManTweetCount.Text = iBridUtil.GetStringValue(iManTweetCount);
				pnlManTweetCount.Visible =true;
				pnlList.Visible = true;
			}
			
			if (iManTweetPicCount > 0) {
				lblManTweetPicCount.Text = iBridUtil.GetStringValue(iManTweetPicCount);
				pnlManTweetPicCount.Visible = true;
				pnlList.Visible = true;
			}
		}

		using (ManTreasure oManTreasure = new ManTreasure()) {
			int iGameManTreasureCount = oManTreasure.GetPageCount(PwViCommConst.MAIN_SITE_CD,string.Empty,false,true,string.Empty,string.Empty,ViCommConst.FLAG_OFF_STR);

			// 【ｿｰｼｬﾙｹﾞｰﾑ】認証待ち男性用お宝画像
			if (iGameManTreasureCount > 0) {
				lblGameManTreasureCount.Text = iBridUtil.GetStringValue(iGameManTreasureCount);
				pnlGameManTreasureCount.Visible = true;
				pnlList.Visible = true;
			}
		}

		using (Withdrawal oWithdrawal = new Withdrawal()) {
			int iManWithdrawalIntroduce = oWithdrawal.GetPageCount(PwViCommConst.MAIN_SITE_CD,string.Empty,string.Empty,string.Empty,string.Empty,string.Empty,string.Empty,string.Empty,ViCommConst.MAN,ViCommConst.FLAG_ON_STR,ViCommConst.FLAG_ON_STR,string.Empty);

			// 【会員】退会申請検索(友達紹介登録)
			if (iManWithdrawalIntroduce > 0) {
				lblWithdrawalIntroduceManCount.Text = iBridUtil.GetStringValue(iManWithdrawalIntroduce);
				pnlWithdrawalIntroduceManCount.Visible = true;
				pnlList.Visible = true;
			}
		}
	}
}
