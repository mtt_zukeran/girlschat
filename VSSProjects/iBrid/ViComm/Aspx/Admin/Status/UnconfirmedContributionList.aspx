﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UnconfirmedContributionList.aspx.cs" Inherits="Status_UnconfirmedContributionList" Title="未確認投稿確認" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="未確認投稿確認"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" Runat="Server">
	 <div class="admincontent">
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[未確認投稿一覧]</legend>
				<asp:Button runat="server" ID="btnRefresh" Text="リフレッシュ" CssClass="seekbutton" CausesValidation="False" />
				<br /><br />
				<asp:Panel runat="server" ID="pnlList">
					<table class="DataWebControlStyle" cellspacing="0" cellpadding="20" rules="all" border="1" style="border-collapse:collapse;">
						<tr class="HeaderStyle">
							<th scope="col">
								コンテンツ名称
							</th>
							<th scope="col">
								未確認件数
							</th>
						</tr>
						<asp:Panel ID="pnlCastBbsLogCount" runat="server">
							<tr class="RowStyle">
								<td align="left">
									<a href="/Admin/Status/BbsInquiry.aspx?sex=3">【出演者】掲示板記録</a>
								</td>
								<td align="right">
									<asp:Label id="lblCastBbsLogCount" runat="server" />
								</td>
							</tr>
						</asp:Panel>
						<asp:Panel ID="pnlCastDiaryCount" runat="server">
							<tr class="AlternatingRowStyleColor">
								<td align="left">
									<a href="/Admin/Cast/CastDiaryInquiry.aspx?redirect=1&sitecd=A001">【出演者】日記記録</a>
								</td>
								<td align="right">
									<asp:Label id="lblCastDiaryCount" runat="server" />
								</td>
							</tr>
						</asp:Panel>
						<asp:Panel ID="pnlCastProfileMovieCount" runat="server">
							<tr class="RowStyle">
								<td align="left">
									<a href="/Admin/CastAdmin/ProfileMovieCheckList.aspx">【出演者】認証待ちプロフ動画</a>
								</td>
								<td align="right">
									<asp:Label id="lblCastProfileMovieCount" runat="server" />
								</td>
							</tr>
						</asp:Panel>
						<asp:Panel ID="pnlCastBbsPicCount" runat="server">
							<tr class="AlternatingRowStyleColor">
								<td align="left">
									<a href="/Admin/CastAdmin/BbsPicCheckList.aspx">【出演者】認証待ち掲示板画像</a>
								</td>
								<td align="right">
									<asp:Label id="lblCastBbsPicCount" runat="server" />
								</td>
							</tr>
						</asp:Panel>
						<asp:Panel ID="pnlCastBbsMovieCount" runat="server">
							<tr class="RowStyle">
								<td align="left">
									<a href="/Admin/CastAdmin/BbsMovieCheckList.aspx">【出演者】認証待ち掲示板動画</a>
								</td>
								<td align="right">
									<asp:Label id="lblCastBbsMovieCount" runat="server" />
								</td>
							</tr>
						</asp:Panel>
						<asp:Panel ID="pnlCastStockPicCount" runat="server">
							<tr class="AlternatingRowStyleColor">
								<td align="left">
									<a href="/Admin/CastAdmin/StockPicCheckList.aspx">【出演者】認証待ちストック画像</a>
								</td>
								<td align="right">
									<asp:Label id="lblCastStockPicCount" runat="server" />
								</td>
							</tr>
						</asp:Panel>
						<asp:Panel ID="pnlCastStockMovieCount" runat="server">
							<tr class="RowStyle">
								<td align="left">
									<a href="/Admin/CastAdmin/StockMovieCheckList.aspx">【出演者】認証待ちストック動画</a>
								</td>
								<td align="right">
									<asp:Label id="lblCastStockMovieCount" runat="server" />
								</td>
							</tr>
						</asp:Panel>
						<asp:Panel ID="pnlCastYakyukenCommentCount" runat="server">
							<tr class="AlternatingRowStyleColor">
								<td align="left">
									<a href="/Admin/Extension/Pwild/YakyukenCommentList.aspx">【出演者】野球拳女性勝利コメント一覧</a>
								</td>
								<td align="right">
									<asp:Label id="lblCastYakyukenCommentCount" runat="server" />
								</td>
							</tr>
						</asp:Panel>
						<asp:Panel ID="pnlCastBlogArticleCount" runat="server">
							<tr class="RowStyle">
								<td align="left">
									<a href="/Admin/Extension/BlogArticleList.aspx?uncheck=1">【出演者】公開待ちブログ記事</a>
								</td>
								<td align="right">
									<asp:Label id="lblCastBlogArticleCount" runat="server" />
								</td>
							</tr>
						</asp:Panel>
						<asp:Panel ID="pnlCastYakyukenPicCount" runat="server">
							<tr class="RowStyle">
								<td align="left">
									<a href="/Admin/Extension/Pwild/YakyukenPicList.aspx">【出演者】野球拳画像</a>
								</td>
								<td align="right">
									<asp:Label id="lblCastYakyukenPicCount" runat="server" />
								</td>
							</tr>
						</asp:Panel>
						<asp:Panel ID="pnlCastYakyukenJyankenPicCount" runat="server">
							<tr class="RowStyle">
								<td align="left">
									<a href="/Admin/Extension/Pwild/YakyukenJyankenPicList.aspx?unauth=1">【出演者】野球拳ジャンケン画像</a>
								</td>
								<td align="right">
									<asp:Label id="lblCastYakyukenJyankenPicCount" runat="server" />
								</td>
							</tr>
						</asp:Panel>
						<asp:Panel ID="pnlManBbsLogCount" runat="server">
							<tr class="AlternatingRowStyleColor">
								<td align="left">
									<a href="/Admin/Status/BbsInquiry.aspx?sex=1">【男性会員】掲示板記録</a>
								</td>
								<td align="right">
									<asp:Label id="lblManBbsLogCount" runat="server" />
								</td>
							</tr>
						</asp:Panel>
						<asp:Panel ID="pnlManProfilePicCount" runat="server">
							<tr class="RowStyle">
								<td align="left">
									<a href="/Admin/Man/ProfilePicList.aspx?unauth=1">【男性会員】認証待ち会員プロフ画像</a>
								</td>
								<td align="right">
									<asp:Label id="lblManProfilePicCount" runat="server" />
								</td>
							</tr>
						</asp:Panel>
						<asp:Panel ID="pnlManTweetCount" runat="server">
							<tr class="AlternatingRowStyleColor">
								<td align="left">
									<a href="/Admin/Extension/Pwild/ManTweetList.aspx">【男性会員】会員つぶやき検索</a>
								</td>
								<td align="right">
									<asp:Label id="lblManTweetCount" runat="server" />
								</td>
							</tr>
						</asp:Panel>
						<asp:Panel ID="pnlManTweetPicCount" runat="server">
							<tr class="RowStyle">
								<td align="left">
									<a href="/Admin/Extension/Pwild/ManTweetPicList.aspx?unauth=1">【男性会員】会員つぶやき認証待ち画像</a>
								</td>
								<td align="right">
									<asp:Label id="lblManTweetPicCount" runat="server" />
								</td>
							</tr>
						</asp:Panel>
						<asp:Panel ID="pnlGameManTreasureCount" runat="server">
							<tr class="RowStyle">
								<td align="left">
									<a href="/Admin/Extension/ManTreasureList.aspx?uncheck=1">【ｿｰｼｬﾙｹﾞｰﾑ】認証待ち男性用お宝画像</a>
								</td>
								<td align="right">
									<asp:Label id="lblGameManTreasureCount" runat="server" />
								</td>
							</tr>
						</asp:Panel>
						<asp:Panel ID="pnlGameMovieCount" runat="server">
							<tr class="RowStyle">
								<td align="left">
									<a href="/Admin/Extension/GameMovieCheckList.aspx">【ｿｰｼｬﾙｹﾞｰﾑ】認証待ちお宝動画</a>
								</td>
								<td align="right">
									<asp:Label id="lblGameMovieCount" runat="server" />
								</td>
							</tr>
						</asp:Panel>
						<asp:Panel ID="pnlWithdrawalIntroduceManCount" runat="server">
							<tr class="RowStyle">
								<td align="left">
									<a href="/Admin/Extension/AcceptWithdrawalList.aspx?sexcd=1&unconfirm=1">【男性会員】退会申請検索(友達紹介登録)</a>
								</td>
								<td align="right">
									<asp:Label id="lblWithdrawalIntroduceManCount" runat="server" />
								</td>
							</tr>
						</asp:Panel>
					</table>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
	 </div>
</asp:Content>

