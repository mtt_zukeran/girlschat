<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DailyTalkMovieDownload.aspx.cs" Inherits="Status_DailyTalkMovieDownload" Title="期間別ムービーアクセス集計" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="期間別ムービーアクセス集計"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							<asp:Label ID="lblAccessUnit" runat="server" Text="集計開始日"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstFromYYYY" runat="server" Width="60px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstFromMM" runat="server" Width="40px">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstFromDD" runat="server" Width="40px">
							</asp:DropDownList>日
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							<asp:Label ID="Label3" runat="server" Text="集計終了日"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstToYYYY" runat="server" Width="60px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstToMM" runat="server" Width="40px">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstToDD" runat="server" Width="40px">
							</asp:DropDownList>日
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
				<asp:Button runat="server" ID="btnCsv" Text="ＣＳＶ出力" CssClass="seekbutton" OnClick="btnCSV_Click" />
			</asp:Panel>
		</fieldset>
		<fieldset>
			<legend>[アクセス集計]</legend>
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="560px">
				<asp:GridView ID="grdDailyDownload" runat="server" AutoGenerateColumns="False" DataSourceID="dsAccessTalkMovie" AllowSorting="True" SkinID="GridViewColor" Width="600px">
					<Columns>
						<asp:BoundField DataField="LOGIN_ID" HeaderText="出演者ID" SortExpression="LOGIN_ID">
							<ItemStyle HorizontalAlign="Center" />
						</asp:BoundField>
						<asp:BoundField DataField="CAST_NM" HeaderText="出演者名" SortExpression="CAST_NM">
							<FooterStyle HorizontalAlign="Center" />
						</asp:BoundField>
						<asp:BoundField DataField="ACCESS_COUNT" HeaderText="アクセス数" SortExpression="ACCESS_COUNT">
							<ItemStyle HorizontalAlign="Right" />
						</asp:BoundField>
						<asp:BoundField DataField="TOTAL_USED_POINT" HeaderText="利用Pt" SortExpression="TOTAL_USED_POINT">
							<ItemStyle HorizontalAlign="Right" />
						</asp:BoundField>
						<asp:BoundField DataField="TOTAL_USED_SEC" HeaderText="利用秒数" SortExpression="TOTAL_USED_SEC">
							<ItemStyle HorizontalAlign="Right" />
						</asp:BoundField>
						<asp:TemplateField HeaderText="アクセス詳細">
							<ItemTemplate>
								<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# string.Format("CastTalkMovieDownload.aspx?sitecd={0}&loginid={1}&from={2}&to={3}",Eval("SITE_CD"),Eval("LOGIN_ID"),GetFromDay(),GetToDay()) %>'
									Text="詳細表示"></asp:HyperLink>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Center" />
						</asp:TemplateField>
					</Columns>
				</asp:GridView>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsAccessTalkMovie" runat="server" SelectMethod="GetList" TypeName="AccessTalkMovie">
		<SelectParameters>
			<asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstFromYYYY" Name="pFromYYYY" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstFromMM" Name="pFromMM" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstFromDD" Name="pFromDD" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstToYYYY" Name="pToYYYY" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstToMM" Name="pToMM" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstToDD" Name="pToDD" PropertyName="SelectedValue" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
</asp:Content>
