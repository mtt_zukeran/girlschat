﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者別ムービーアクセス集計
--	Progaram ID		: CastTalkMovieDownload
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Status_CastTalkMovieDownload:System.Web.UI.Page {


	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		lblUserSeq.Text = "";
		pnlInfo.Visible = false;
		ClearField();
		DataBind();

		if (!IsPostBack) {
			lstSiteCd.DataBind();
			lstSiteCd.DataSourceID = "";
			string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			string sLoginId = iBridUtil.GetStringValue(Request.QueryString["loginid"]);
			string sFrom = iBridUtil.GetStringValue(Request.QueryString["from"]);
			string sTo = iBridUtil.GetStringValue(Request.QueryString["to"]);
			if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
				lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}

			if ((!sSiteCd.Equals("")) && (!sLoginId.Equals("")) && (!sFrom.Equals("")) && (!sTo.Equals(""))) {
				lstSiteCd.SelectedValue = sSiteCd;
				txtLoginId.Text = sLoginId;
				lstFromYYYY.SelectedValue = sFrom.Substring(0,4);
				lstFromMM.SelectedValue = sFrom.Substring(4,2);
				lstFromDD.SelectedValue = sFrom.Substring(6,2);
				lstToYYYY.SelectedValue = sTo.Substring(0,4);
				lstToMM.SelectedValue = sTo.Substring(4,2);
				lstToDD.SelectedValue = sTo.Substring(6,2);
				Validate("Key");
				if (IsValid) {
					GetList();
				}
			}
		}
		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);
		this.vdcLoginId.ErrorMessage = DisplayWordUtil.Replace(this.vdcLoginId.ErrorMessage);
		this.vdrLoginId.ErrorMessage = DisplayWordUtil.Replace(this.vdrLoginId.ErrorMessage);
		DisplayWordUtil.ReplaceGridColumnHeader(this.grdDailyDownload);
		
	}

	private void ClearField() {
		SysPrograms.SetupFromToDay(lstFromYYYY,lstFromMM,lstFromDD,lstToYYYY,lstToMM,lstToDD,true);
		lstFromYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		lstToYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		lstFromMM.SelectedValue = DateTime.Now.ToString("MM");
		lstToMM.SelectedValue = DateTime.Now.ToString("MM");
		lstFromDD.SelectedValue = DateTime.Now.ToString("dd");
		lstToDD.SelectedValue = DateTime.Now.ToString("dd");
		txtLoginId.Text = "";
		lnkCastNm.Text = "";
		btnCsv.Visible = false;
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void vdcLoginId_ServerValidate(object source,ServerValidateEventArgs args) {
		using (Cast oCast = new Cast()) {
			args.IsValid = oCast.IsExistLoginId(txtLoginId.Text);
			if (args.IsValid) {
				lblUserSeq.Text = oCast.userSeq;
				lnkCastNm.Text = oCast.castNm;
				lnkCastNm.NavigateUrl = string.Format("~/Cast/CastView.aspx?loginid={0}&return=CastTalkMovieDownload.aspx",txtLoginId.Text);
			} else {
				lblUserSeq.Text = "";
				lnkCastNm.Text = "";
				lnkCastNm.NavigateUrl = "";
			}
		}
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		string fileName = ViCommConst.CSV_FILE_NM_ACCESS_MOVIE;

		Response.AddHeader("Content-Disposition","attachment;filename=" + fileName);
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		using (AccessTalkMovie oAccessTalkMovie = new AccessTalkMovie()) {
			DataSet ds = oAccessTalkMovie.GetListByCast(
								lstSiteCd.SelectedValue,
								lstFromYYYY.SelectedValue,
								lstFromMM.SelectedValue,
								lstFromDD.SelectedValue,
								lstToYYYY.SelectedValue,
								lstToMM.SelectedValue,
								lstToDD.SelectedValue,
								lblUserSeq.Text);

			string sCsvRec = GetCsvString(ds);

			Response.BinaryWrite(encoding.GetBytes(sCsvRec));
			Response.End();
		}
	}


	private string GetCsvString(DataSet pDataSet) {
		string sCsv = "";
		sCsv += "ＩＤ番号,";
		sCsv += "氏名,";
		sCsv += "アクセス数,";
		sCsv += "利用Pt,";
		sCsv += "利用秒数";
		sCsv += "\r\n";
		DataRow dr = null;
		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];
			sCsv += dr["LOGIN_ID"] + ",";
			sCsv += dr["MOVIE_TITLE"] + ",";
			sCsv += dr["ACCESS_COUNT"] + ",";
			sCsv += dr["TOTAL_USED_POINT"] + ",";
			sCsv += dr["TOTAL_USED_SEC"];
			sCsv += "\r\n";
		}
		return sCsv;
	}


	private void GetList() {
		btnCsv.Visible = false;
		grdDailyDownload.DataSourceID = "dsAccessTalkMovie";
		DataBind();
		int iCompare = string.Compare(Session["AdminType"].ToString(),ViCommConst.RIGHT_SITE_OWNER);
		btnCsv.Visible = (iCompare >= 0);
		pnlInfo.Visible = true;
	}
}
