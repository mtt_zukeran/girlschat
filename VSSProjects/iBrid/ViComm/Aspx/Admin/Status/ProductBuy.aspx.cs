﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 商品別購入履歴

--	Progaram ID		: ProductBuy
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Status_ProductBuy : System.Web.UI.Page {
	private string recCount = string.Empty;

	protected void Page_Load(object sender, EventArgs e) {
		if (!this.IsPostBack) {
			this.InitPage();
		}
	}

	protected void btnListSeek_Click(object sender, EventArgs e) {
		if (this.IsValid) {
			this.GetList();
		}
	}

	protected void btnClear_Click(object sender, EventArgs e) {
		this.InitPage();
	}

	//protected void btnCSV_Click(object sender, EventArgs e) {
	//    Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", ViCommConst.CSV_FILE_NM_PRODUCT_BUY_SUMMARY));
	//    Response.ContentType = "application/octet-stream-dummy";
	//    System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");
	//    Response.BinaryWrite(encoding.GetBytes(this.GetCsvString()));
	//    Response.End();
	//}

	protected void lst_DataBound(object sender, EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList != null) {
			oDropDownList.Items.Insert(0, new ListItem(string.Empty, string.Empty));
		}
	}

	protected void dsProductBuyHistory_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	protected void vdcProductId_ServerValidate(object source, ServerValidateEventArgs args) {
		using (Product oProduct = new Product()) {
			using (DataSet oDataSet = oProduct.GetOne(this.txtProductId.Text.TrimEnd())) {
				args.IsValid = oDataSet.Tables[0].Rows.Count > 0;
				if (args.IsValid) {
					DataRow oDataRow = oDataSet.Tables[0].Rows[0];
					this.lblProductSeq.Text = iBridUtil.GetStringValue(oDataRow["PRODUCT_SEQ"]);
					this.lnkProductNm.Text = iBridUtil.GetStringValue(oDataRow["PRODUCT_NM"]);
					this.lnkProductNm.NavigateUrl = string.Format("~/Product/ProductView.aspx?product_id={0}", this.txtProductId.Text);
				} else {
					this.lnkProductNm.Text = string.Empty;
					this.lblProductSeq.Text = string.Empty;
					this.lnkProductNm.NavigateUrl = string.Empty;
				}
			}
		}
	}

	private void InitPage() {
		this.pnlInfo.Visible = false;

		this.lblProductSeq.Text = string.Empty;
		this.recCount = "0";
		this.lstSiteCd.DataBind();
		this.lstProductType.DataBind();
		this.lstProductAgent.DataBind();
		this.ClearField();

		if (!this.IsPostBack) {
		//    if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals(string.Empty)) {
		//        this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		//    }
		//}

		//this.pnlInfo.Visible = false;
		//this.ClearField();
		////this.DataBind();

		//if (!IsPostBack) {
		//    this.lstSiteCd.DataBind();
		//    this.lstSiteCd.DataSourceID = string.Empty;
			string sSiteCd = iBridUtil.GetStringValue(Request.QueryString["site_cd"]);
			string sProductId = iBridUtil.GetStringValue(Request.QueryString["product_id"]);
			string sProductAgentCd = iBridUtil.GetStringValue(Request.QueryString["product_agent_cd"]);
			string sProductType = iBridUtil.GetStringValue(Request.QueryString["product_type"]);
			string sFrom = iBridUtil.GetStringValue(Request.QueryString["from"]);
			string sTo = iBridUtil.GetStringValue(Request.QueryString["to"]);

			if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals(string.Empty)) {
				this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}

			if ((!sSiteCd.Equals(string.Empty)) && (!sProductId.Equals(string.Empty)) && (!sFrom.Equals(string.Empty)) && (!sTo.Equals(string.Empty))) {
				this.lstSiteCd.SelectedValue = sSiteCd;
				this.lstProductAgent.SelectedValue = sProductAgentCd;
				this.lstProductType.SelectedValue = sProductType;
				this.txtProductId.Text = sProductId;
				this.lstFromYYYY.SelectedValue = sFrom.Substring(0, 4);
				this.lstFromMM.SelectedValue = sFrom.Substring(4, 2);
				this.lstFromDD.SelectedValue = sFrom.Substring(6, 2);
				this.lstToYYYY.SelectedValue = sTo.Substring(0, 4);
				this.lstToMM.SelectedValue = sTo.Substring(4, 2);
				this.lstToDD.SelectedValue = sTo.Substring(6, 2);
				this.Validate("Seek");
				if (this.IsValid) {
					this.GetList();
				}
			}
		}

		//this.Title = DisplayWordUtil.Replace(this.Title);
		//this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);
		//this.vdrProductId.ErrorMessage = DisplayWordUtil.Replace(this.vdrProductId.ErrorMessage);
		//this.vdcProductId.ErrorMessage = DisplayWordUtil.Replace(this.vdcProductId.ErrorMessage);
		//DisplayWordUtil.ReplaceGridColumnHeader(this.grdDailyDownload);
	}

	private void ClearField() {
		SysPrograms.SetupFromToDay(this.lstFromYYYY, this.lstFromMM, this.lstFromDD, this.lstToYYYY, this.lstToMM, this.lstToDD, false);
		this.lstFromYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstToYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstFromMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstToMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstFromDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstToDD.SelectedValue = DateTime.Now.ToString("dd");
	}

	private void GetList() {
		this.pnlInfo.Visible = true;
		this.grdProductBuy.PageSize = 1000;
		this.grdProductBuy.DataBind();
		this.pnlCount.DataBind();
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected string GenerateManViewUrl(object pSeed) {
		DataRowView oProductDataRowView = (DataRowView)pSeed;

		string sLoginId = iBridUtil.GetStringValue(oProductDataRowView["LOGIN_ID"]);

		UrlBuilder oUrlBuilder = new UrlBuilder("~/Man/ManView.aspx");
		oUrlBuilder.Parameters.Add("site", this.lstSiteCd.SelectedValue);
		oUrlBuilder.Parameters.Add("manloginid", sLoginId);
		return oUrlBuilder.ToString();
	}

}
