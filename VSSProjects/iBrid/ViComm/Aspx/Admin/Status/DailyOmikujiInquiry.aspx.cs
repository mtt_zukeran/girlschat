﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 日別おみくじ利用状況
--	Progaram ID		: DailyOmikujiInquiry
--
--  Creation Date	: 2011.10.02
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Status_DailyOmikujiInquiry : System.Web.UI.Page {

	public class colInfo {
		public string title;
		public string receiptType;
		public string countField;
		public string pointField;

		public colInfo(string pReceiptType, string pTitle, string pCountFiled, string pPointField) {
			title = pTitle;
			receiptType = pReceiptType;
			countField = pCountFiled;
			pointField = pPointField;
		}
	}
	public class OmikujiRowTemplate : System.Web.UI.ITemplate {
		private ListItemType templateType;
		private string bindField;

		public OmikujiRowTemplate(ListItemType pItemtype, string pBindField) {
			templateType = pItemtype;
			bindField = pBindField;
		}

		public void InstantiateIn(System.Web.UI.Control container) {
			PlaceHolder ph = new PlaceHolder();
			Label item1 = new Label();
			item1.ID = bindField;
			switch (templateType) {
				case ListItemType.Item:
					ph.Controls.Add(item1);
					ph.DataBinding += new EventHandler(Item_DataBinding);
					container.Controls.Add(ph);
					break;
			}
		}

		public void Item_DataBinding(object sender, EventArgs e) {
			PlaceHolder ph = (PlaceHolder)sender;
			GridViewRow container = (GridViewRow)ph.NamingContainer;
			((Label)ph.FindControl(bindField)).Text = ((DataRowView)container.DataItem)[this.bindField].ToString();
		}
	}

	private int NoReceiptOmikujiTotalCount;
	private int NoReceiptOmikujiTotalPoint;
	private int ReceiptOmikujiTotalCount;
	private int ReceiptOmikujiTotalPoint;
	private int AllOmikujiTotalCount;
	private int AllOmikujiTotalPoint;
	private List<colInfo> colList;
	private string recCount = "";
	private string SexCd {
		get { return iBridUtil.GetStringValue(this.ViewState["SexCd"]); }
		set { this.ViewState["SexCd"] = value; }
	}

    protected void Page_Load(object sender, EventArgs e) {
		if (!this.IsPostBack) {
			this.FirstLoad();
			this.InitPage();
		}
    }

	protected void FirstLoad() {
		this.SexCd = iBridUtil.GetStringValue(this.Request.QueryString["sexcd"]);

		lstSeekSiteCd.DataBind();
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		SysPrograms.SetupYear(lstYYYY);
		lstMM.SelectedValue = DateTime.Now.ToString("MM");
	}

	protected void InitPage() {
		this.ClearFirlds();
		this.GetList();
	}

	protected void ClearFirlds() { 
	}

	private void GetList() {
		NoReceiptOmikujiTotalCount = 0;
		NoReceiptOmikujiTotalPoint = 0;
		ReceiptOmikujiTotalCount = 0;
		ReceiptOmikujiTotalPoint = 0;
		AllOmikujiTotalCount = 0;
		AllOmikujiTotalPoint = 0;

		grdDailyOmikujiInquiry.PageIndex = 0;
		grdDailyOmikujiInquiry.DataSourceID = "dsDailyOmikujiInquiry";
		grdDailyOmikujiInquiry.DataBind();

		if (SexCd.Equals(ViCommConst.MAN)) {
			AddHeader();
		}
	}
	private void AddHeader() {

		GridViewRow row = new GridViewRow(-1, -1, DataControlRowType.Header, DataControlRowState.Normal);
		TableCell cell;
		cell = new TableCell();
		cell.ColumnSpan = 3;
		cell.RowSpan = 1;
		cell.Text = "";
		row.Cells.Add(cell);
		foreach (colInfo oInfo in colList) {
			cell = new TableCell();
			cell.ColumnSpan = 2;
			cell.Text = oInfo.title;
			cell.HorizontalAlign = HorizontalAlign.Center;
			row.Cells.Add(cell);
		}
		grdDailyOmikujiInquiry.Controls[0].Controls.AddAt(0, row);
	}


	private void JoinCells(GridView pGrd, int pCol) {
		int iRow = pGrd.Rows.Count;
		int iBaseidx = 0;

		while (iBaseidx < iRow) {
			int iNextIdx = iBaseidx + 1;
			TableCell celBase = pGrd.Rows[iBaseidx].Cells[pCol];

			while (iNextIdx < iRow) {

				TableCell celNext = pGrd.Rows[iNextIdx].Cells[pCol];

				if (GetText(celBase).Equals(GetText(celNext))) {
					if (celBase.RowSpan == 0) {
						celBase.RowSpan = 2;
					} else {
						celBase.RowSpan++;
					}
					pGrd.Rows[iNextIdx].Cells.Remove(celNext);
					iNextIdx++;
				} else {
					break;
				}
			}
			iBaseidx = iNextIdx;
		}
	}

	private string GetText(TableCell tc) {
		Label dblc =
		  (Label)tc.Controls[1];
		return dblc.Text;
	}

	protected Color GetBackColor(object pDayOfWeek) {
		if (pDayOfWeek.ToString().Equals("Sat")) {
			return Color.FromArgb(0xCCCCFF);
		} else {
			if (pDayOfWeek.ToString().Equals("Sun")) {
				return Color.FromArgb(0xFFCCCC);
			} else {
				return Color.Empty;
			}
		}
	}
    #region === Event Methods ===
    protected void btnListSeek_Click(object sender, EventArgs e) {
		GetList();
    }
	protected void dsDailyOmikujiInquiry_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
		
		e.InputParameters[2] = this.lstYYYY.SelectedValue;
		e.InputParameters[3] = this.lstMM.SelectedValue;
	}
    protected void dsDailyOmikujiInquiry_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
        if (e.ReturnValue != null) {
            this.recCount = e.ReturnValue.ToString();
        }
    }
	protected void grdDailyOmikujiInquiry_RowDataBound(object sender, GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			NoReceiptOmikujiTotalCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "NO_RCPT_OMIKUJI_TOTAL_COUNT"));
			NoReceiptOmikujiTotalPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "NO_RCPT_OMIKUJI_TOTAL_POINT"));
			ReceiptOmikujiTotalCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "RCPT_OMIKUJI_TOTAL_COUNT"));
			ReceiptOmikujiTotalPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "RCPT_OMIKUJI_TOTAL_POINT"));
			AllOmikujiTotalCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "ALL_OMIKUJI_TOTAL_COUNT")) ;
			AllOmikujiTotalPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "ALL_OMIKUJI_TOTAL_POINT")) ;
			e.Row.Cells[0].BackColor = GetBackColor(DataBinder.Eval(e.Row.DataItem, "REPORT_DAY_OF_WEEK"));
			e.Row.Cells[1].BackColor = GetBackColor(DataBinder.Eval(e.Row.DataItem, "REPORT_DAY_OF_WEEK"));
			e.Row.Cells[2].BackColor = Color.White;
		} else if (e.Row.RowType == DataControlRowType.Footer) {
			e.Row.Cells[0].Text = "合計";

			if (this.SexCd.Equals(ViCommConst.MAN)) {
				e.Row.Cells[3].Text = NoReceiptOmikujiTotalCount.ToString();
				e.Row.Cells[4].Text = NoReceiptOmikujiTotalPoint.ToString();
				e.Row.Cells[5].Text = ReceiptOmikujiTotalCount.ToString();
				e.Row.Cells[6].Text = ReceiptOmikujiTotalPoint.ToString();
				e.Row.Cells[7].Text = AllOmikujiTotalCount.ToString();
				e.Row.Cells[8].Text = AllOmikujiTotalPoint.ToString();
			} else {
				e.Row.Cells[3].Text = ReceiptOmikujiTotalCount.ToString();
				e.Row.Cells[4].Text = ReceiptOmikujiTotalPoint.ToString();
			}
		}
	}
	protected void grdDailyOmikujiInquiry_DataBound(object sender, EventArgs e) {
		JoinCells(grdDailyOmikujiInquiry, 0);
	}
	protected void grdDailyOmikujiInquiry_Init(object sender, EventArgs e) {
		if (SexCd.Equals(string.Empty)) { SexCd = iBridUtil.GetStringValue(this.Request.QueryString["sexcd"]); }
		colList = new List<colInfo>();
		if (this.SexCd.Equals(ViCommConst.MAN)) {
			colList.Add(new colInfo(ViCommConst.FLAG_ON_STR, "入金なし", "NO_RCPT_OMIKUJI_TOTAL_COUNT", "NO_RCPT_OMIKUJI_TOTAL_POINT"));
			colList.Add(new colInfo(ViCommConst.FLAG_OFF_STR, "入金あり", "RCPT_OMIKUJI_TOTAL_COUNT", "RCPT_OMIKUJI_TOTAL_POINT"));
			colList.Add(new colInfo(ViCommConst.FLAG_OFF_STR, "合計", "ALL_OMIKUJI_TOTAL_COUNT", "ALL_OMIKUJI_TOTAL_POINT"));
		} else {
			colList.Add(new colInfo(ViCommConst.FLAG_OFF_STR, "入金あり", "RCPT_OMIKUJI_TOTAL_COUNT", "RCPT_OMIKUJI_TOTAL_POINT"));
		}


		foreach (colInfo oInfo in colList) {
			TemplateField tmpCount = new TemplateField();
			TemplateField tmpPoint = new TemplateField();
			tmpCount.HeaderText = "件数";
			tmpCount.HeaderStyle.CssClass = "HeaderStyle";
			tmpCount.FooterStyle.HorizontalAlign = HorizontalAlign.Right;
			tmpCount.ItemStyle.Width = 30;
			tmpCount.ItemStyle.CssClass = "RowStylePad";
			tmpCount.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
			tmpCount.ItemTemplate = new OmikujiRowTemplate(ListItemType.Item, oInfo.countField);
			tmpCount.FooterStyle.HorizontalAlign = HorizontalAlign.Right;

			tmpPoint.HeaderText = "PT";
			tmpPoint.HeaderStyle.CssClass = "HeaderStyle";
			tmpPoint.FooterStyle.HorizontalAlign = HorizontalAlign.Right;
			tmpPoint.ItemStyle.Width = 60;
			tmpPoint.ItemStyle.CssClass = "RowStylePad";
			tmpPoint.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
			tmpPoint.ItemStyle.VerticalAlign = VerticalAlign.Middle;
			tmpPoint.ItemTemplate = new OmikujiRowTemplate(ListItemType.Item, oInfo.pointField);

			grdDailyOmikujiInquiry.Columns.Add(tmpCount);
			grdDailyOmikujiInquiry.Columns.Add(tmpPoint);
		}
		grdDailyOmikujiInquiry.Width = 120 + colList.Count * 100;
	}
    #endregion
}
