﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AccessMailCountByTxMailDayList.aspx.cs" Inherits="Status_AccessMailCountByTxMailDayList" Title="メール送信日別アクセス集計"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="メール送信日別アクセス集計"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
                        <td class="tdHeaderStyle">
                            サイトコード
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
						<td class="tdHeaderStyle">
							集計開始日
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstFromYYYY" runat="server" Width="56px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstFromMM" runat="server" Width="40px">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstFromDD" runat="server" Width="40px">
							</asp:DropDownList>日
							<asp:CustomValidator runat="server" ID="vdcFrom" ErrorMessage="集計開始日が不正です。" OnServerValidate="vdcFrom_ServerValidate" ValidationGroup="Key"></asp:CustomValidator>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							集計終了日
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstToYYYY" runat="server" Width="56px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstToMM" runat="server" Width="40px">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstToDD" runat="server" Width="40px">
							</asp:DropDownList>日
							<asp:CustomValidator ID="vdcTo" runat="server" ErrorMessage="集計終了日が不正です。" OnServerValidate="vdcTo_ServerValidate" ValidationGroup="Key"></asp:CustomValidator>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							ﾃﾝﾌﾟﾚｰﾄNO
						</td>
						<td class="tdDataStyle">
							<asp:TextBox ID="txtMailTemplateNo" runat="server" MaxLength="6"></asp:TextBox>
							<asp:RequiredFieldValidator ID="vdrMailTemplateNo" runat="server" ErrorMessage="ﾒｰﾙﾃﾝﾌﾟﾚｰﾄNOを入力して下さい。" ControlToValidate="txtMailTemplateNo" ValidationGroup="Key">*</asp:RequiredFieldValidator>
							<asp:RegularExpressionValidator ID="vdeMailTemplateNo" runat="server" ErrorMessage="ﾒｰﾙﾃﾝﾌﾟﾚｰﾄNOは数字で入力して下さい。" ValidationExpression="\d{4,6}" ControlToValidate="txtMailTemplateNo"
								ValidationGroup="Key">*</asp:RegularExpressionValidator>
							<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender33" TargetControlID="vdeMailTemplateNo" HighlightCssClass="validatorCallout" />
							<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" Enabled="true" FilterType="Numbers" TargetControlID="txtMailTemplateNo" />
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" ValidationGroup="Key" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
				<asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" CausesValidation="True" OnClick="btnCSV_Click" />
			</asp:Panel>
		</fieldset>
		<fieldset>
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="750px">
				<asp:GridView ID="grdAccessMailCountDaily" runat="server" AutoGenerateColumns="False" DataSourceID="dsAccessMailCountDaily" CellPadding="0" AllowSorting="True" ShowFooter="false"
					OnRowDataBound="grdAccessMailCountDaily_RowDataBound" SkinID="GridViewColorNoRowStyle">
					<Columns>
						<asp:TemplateField HeaderText="日付">
							<ItemStyle HorizontalAlign="Center" />
							<ItemTemplate>
								<asp:Label ID="Label1" runat="server" Text='<%# Eval("TX_MAIL_DAY") %>' BackColor='' Width="100px"></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="送信数">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:HyperLink ID="Label2" runat="server" NavigateUrl='<%# string.Format("AccessMailCountDailyList.aspx?site={0}&mailtemplateno={1}&sexcd={2}&txmailday={3}",Eval("SITE_CD"),Eval("MAIL_TEMPLATE_NO"),Eval("SEX_CD"),Eval("TX_MAIL_DAY")) %>'
                                        Text='<%# Eval("TX_MAIL_COUNT") %>'></asp:HyperLink>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ｱｸｾｽ総数">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("ACCESS_COUNT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ﾕﾆｰｸ数">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("UNIQUE_ACCESS_COUNT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ｱｸｾｽ率">
							<HeaderStyle Wrap="false" />
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# GetUniqueAccessRate(Eval("TX_MAIL_COUNT").ToString(),Eval("UNIQUE_ACCESS_COUNT").ToString()) %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
						</asp:TemplateField>
					</Columns>
					<FooterStyle ForeColor="Black" BackColor="LightYellow" />
				</asp:GridView>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsAccessMailCountDaily" runat="server" SelectMethod="GetAccessCountByTxMailDay" TypeName="AccessMailCountDaily"
		OnSelecting="dsAccessMailCountDaily_Selecting">
		<SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="vdrMailTemplateNo" HighlightCssClass="validatorCallout" />
	<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="vdeMailTemplateNo" HighlightCssClass="validatorCallout" />
</asp:Content>
