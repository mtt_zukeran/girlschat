﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DailyMailTxRxCount.aspx.cs" Inherits="Status_DailyMailTxRxCount"
	Title="期間別メールやりとり集計" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="期間別メールやりとり集計"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							<asp:Label ID="lblAccessUnit" runat="server" Text="表示月"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstYYYY" runat="server" Width="60px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstMM" runat="server" Width="40px">
							</asp:DropDownList>月
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							性別
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSexCd" runat="server" Width="80px">
							</asp:DropDownList>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
				<asp:Button runat="server" ID="btnCsv" Text="ＣＳＶ出力" CssClass="seekbutton" OnClick="btnCSV_Click" />
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[稼動集計]</legend>
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="Auto" Height="430px" Width="100%">
					<asp:GridView ID="grdDailyMailCount" runat="server" AutoGenerateColumns="False" DataSourceID="" ShowFooter="True" AllowSorting="True"
						SkinID="GridViewColor" OnRowDataBound="grdDailyMailCount_RowDataBound" Font-Size="X-Small" EnableViewState="false">
						<Columns>
							<asp:TemplateField HeaderText="">
								<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
								<ItemTemplate>
									<asp:Label ID="lblReportDate" runat="server" Text='<%# Eval("REPORT_DATE", "{0:yyyy/MM/dd}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Center" />
								<FooterTemplate>
									<asp:Label ID="lblTotalNm" runat="server" Text="合計"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾕﾆｰｸ人数">
								<ItemTemplate>
									<asp:Label ID="lblManTxUUCount" runat="server" Text='<%# Eval("MAN_TX_UNIQ_USER_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblManTxUUTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="件数">
								<ItemTemplate>
									<asp:Label ID="lblManTxNormalCount" runat="server" Text='<%# Eval("MAN_TX_NORMAL_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblManTxNormalTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="写真添付件数">
								<ItemTemplate>
									<asp:Label ID="lblManTxPicCount" runat="server" Text='<%# Eval("MAN_TX_PIC_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblManTxPicTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="動画添付件数">
								<ItemTemplate>
									<asp:Label ID="lblManTxMovieCount" runat="server" Text='<%# Eval("MAN_TX_MOVIE_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblManTxMovieTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾕﾆｰｸ人数">
								<ItemTemplate>
									<asp:Label ID="lblManTxReturnUUCount" runat="server" Text='<%# Eval("MAN_TX_RETURN_UNIQ_USER_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblManTxReturnUUTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="件数">
								<ItemTemplate>
									<asp:Label ID="lblManTxReturnNormalCount" runat="server" Text='<%# Eval("MAN_TX_RETURN_NORMAL_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblManTxReturnNormalTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="写真添付件数">
								<ItemTemplate>
									<asp:Label ID="lblManTxReturnPicCount" runat="server" Text='<%# Eval("MAN_TX_RETURN_PIC_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblManTxReturnPicTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="動画添付件数">
								<ItemTemplate>
									<asp:Label ID="lblManTxReturnMovieCount" runat="server" Text='<%# Eval("MAN_TX_RETURN_MOVIE_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblManTxReturnMovieTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾕﾆｰｸ人数">
								<ItemTemplate>
									<asp:Label ID="lblManTxTotalUUCount" runat="server" Text='<%# Eval("MAN_TX_TOTAL_UNIQ_USER_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" BackColor="#FFEBCD" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" BackColor="#FFEBCD" />
								<FooterTemplate>
									<asp:Label ID="lblManTxTotalUUTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="件数">
								<ItemTemplate>
									<asp:Label ID="lblManTxTotalNormalCount" runat="server" Text='<%# Eval("MAN_TX_TOTAL_NORMAL_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" BackColor="#FFEBCD" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" BackColor="#FFEBCD" />
								<FooterTemplate>
									<asp:Label ID="lblManTxTotalNormalTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="写真添付件数">
								<ItemTemplate>
									<asp:Label ID="lblManTxTotalPicCount" runat="server" Text='<%# Eval("MAN_TX_TOTAL_PIC_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" BackColor="#FFEBCD" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" BackColor="#FFEBCD" />
								<FooterTemplate>
									<asp:Label ID="lblManTxTotalPicTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="動画添付件数">
								<ItemTemplate>
									<asp:Label ID="lblManTxTotalMovieCount" runat="server" Text='<%# Eval("MAN_TX_TOTAL_MOVIE_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" BackColor="#FFEBCD" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" BackColor="#FFEBCD" />
								<FooterTemplate>
									<asp:Label ID="lblManTxTotalMovieTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾕﾆｰｸ人数">
								<ItemTemplate>
									<asp:Label ID="lblManRxUUCount" runat="server" Text='<%# Eval("MAN_RX_UNIQ_USER_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblManRxUUTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="一括送信件数">
								<ItemTemplate>
									<asp:Label ID="lblManRxBatchCount" runat="server" Text='<%# Eval("MAN_RX_BATCH_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblManRxBatchTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="個別送信件数">
								<ItemTemplate>
									<asp:Label ID="lblManRxNormalCount" runat="server" Text='<%# Eval("MAN_RX_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblManRxNormalTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="返信件数">
								<ItemTemplate>
									<asp:Label ID="lblManRxReturnCount" runat="server" Text='<%# Eval("MAN_RX_RETURN_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblManRxReturnTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="合計件数">
								<ItemTemplate>
									<asp:Label ID="lblManRxTotalCount" runat="server" Text='<%# Eval("MAN_RX_TOTAL_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblManRxTotalTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="">
								<ItemTemplate>
									<asp:Label ID="lblManReturnRate" runat="server" Text='<%# GetRate(Eval("MAN_RETURN_RATE")) %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblManTotalReturnRate" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾕﾆｰｸ人数">
								<ItemTemplate>
									<asp:Label ID="lblWomanTxUUCount" runat="server" Text='<%# Eval("WOMAN_TX_UNIQ_USER_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblWomanTxUUTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="一括送信件数">
								<ItemTemplate>
									<asp:Label ID="lblWomanTxBatchCount" runat="server" Text='<%# Eval("WOMAN_TX_BATCH_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblWomanTxBatchTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="個別送信件数">
								<ItemTemplate>
									<asp:Label ID="lblWomanTxNormalCount" runat="server" Text='<%# Eval("WOMAN_TX_NORMAL_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblWomanTxNormalTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="写真添付件数">
								<ItemTemplate>
									<asp:Label ID="lblWomanTxPicCount" runat="server" Text='<%# Eval("WOMAN_TX_PIC_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblWomanTxPicTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="動画添付件数">
								<ItemTemplate>
									<asp:Label ID="lblWomanTxMovieCount" runat="server" Text='<%# Eval("WOMAN_TX_MOVIE_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblWomanTxMovieTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾕﾆｰｸ人数">
								<ItemTemplate>
									<asp:Label ID="lblWomanTxReturnUUCount" runat="server" Text='<%# Eval("WOMAN_TX_RETURN_UNIQ_USER_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblWomanTxReturnUUTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="件数">
								<ItemTemplate>
									<asp:Label ID="lblWomanTxReturnNormalCount" runat="server" Text='<%# Eval("WOMAN_TX_RETURN_NORMAL_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblWomanTxReturnNormalTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="写真添付件数">
								<ItemTemplate>
									<asp:Label ID="lblWomanTxReturnPicCount" runat="server" Text='<%# Eval("WOMAN_TX_RETURN_PIC_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblWomanTxReturnPicTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="動画添付件数">
								<ItemTemplate>
									<asp:Label ID="lblWomanTxReturnMovieCount" runat="server" Text='<%# Eval("WOMAN_TX_RETURN_MOVIE_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblWomanTxReturnMovieTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾕﾆｰｸ人数">
								<ItemTemplate>
									<asp:Label ID="lblWomanTxTotalUUCount" runat="server" Text='<%# Eval("WOMAN_TX_TOTAL_UNIQ_USER_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" BackColor="#FFEBCD" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" BackColor="#FFEBCD" />
								<FooterTemplate>
									<asp:Label ID="lblWomanTxTotalUUTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="件数">
								<ItemTemplate>
									<asp:Label ID="lblWomanTxTotalNormalCount" runat="server" Text='<%# Eval("WOMAN_TX_TOTAL_NORMAL_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" BackColor="#FFEBCD" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" BackColor="#FFEBCD" />
								<FooterTemplate>
									<asp:Label ID="lblWomanTxTotalNormalTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="写真添付件数">
								<ItemTemplate>
									<asp:Label ID="lblWomanTxTotalPicCount" runat="server" Text='<%# Eval("WOMAN_TX_TOTAL_PIC_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" BackColor="#FFEBCD" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" BackColor="#FFEBCD" />
								<FooterTemplate>
									<asp:Label ID="lblWomanTxTotalPicTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="動画添付件数">
								<ItemTemplate>
									<asp:Label ID="lblWomanTxTotalMovieCount" runat="server" Text='<%# Eval("WOMAN_TX_TOTAL_MOVIE_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" BackColor="#FFEBCD" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" BackColor="#FFEBCD" />
								<FooterTemplate>
									<asp:Label ID="lblWomanTxTotalMovieTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾕﾆｰｸ人数">
								<ItemTemplate>
									<asp:Label ID="lblWomanRxUUCount" runat="server" Text='<%# Eval("WOMAN_RX_UNIQ_USER_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblWomanRxUUTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="送信件数">
								<ItemTemplate>
									<asp:Label ID="lblWomanRxCount" runat="server" Text='<%# Eval("WOMAN_RX_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblWomanRxTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="返信件数">
								<ItemTemplate>
									<asp:Label ID="lblWomanRxReturnCount" runat="server" Text='<%# Eval("WOMAN_RX_RETURN_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblWomanRxReturnTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="合計件数">
								<ItemTemplate>
									<asp:Label ID="lblWomanRxTotalCount" runat="server" Text='<%# Eval("WOMAN_RX_TOTAL_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblWomanRxTotalTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="">
								<ItemTemplate>
									<asp:Label ID="lblWomanReturnRate" runat="server" Text='<%# GetRate(Eval("WOMAN_RETURN_RATE")) %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblWomanTotalReturnRate" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
						</Columns>
						<FooterStyle ForeColor="Black" BackColor="LightYellow" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsDailyMailCount" runat="server" SelectMethod="GetList" TypeName="DailyMailTxRxCount">
		<SelectParameters>
			<asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstYYYY" Name="pYYYY" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstMM" Name="pMM" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstSexCd" Name="pSexCd" PropertyName="SelectedValue" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
</asp:Content>
