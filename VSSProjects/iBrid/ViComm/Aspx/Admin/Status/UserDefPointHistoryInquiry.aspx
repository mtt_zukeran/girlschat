﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="UserDefPointHistoryInquiry.aspx.cs" Inherits="Status_UserDefPointHistoryInquiry"
    Title="ポイント操作履歴" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="ポイント操作履歴"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <fieldset class="fieldset">
            <legend>[検索条件]</legend>
            <asp:Panel runat="server" ID="pnlKey">
                <table border="0" style="width: 740px" class="tableStyle">
                    <tr>
                        <td class="tdHeaderStyle">
                            サイト
                        </td>
                        <td class="tdDataStyle">
                            <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                DataValueField="SITE_CD" Width="180px">
                            </asp:DropDownList>
                        </td>
                        <td class="tdHeaderStyle" id="tdHeaderAddPointDate" runat="server">
                            追加日
                        </td>
                        <td class="tdDataStyle">
                            <asp:TextBox ID="txtDayFrom" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="vdrDayFrom" runat="server" ErrorMessage="追加日Fromを入力して下さい。"
                                ControlToValidate="txtDayFrom" ValidationGroup="Key" Display="Dynamic">*</asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="vdeDayFrom" runat="server" ErrorMessage="追加日Fromを正しく入力して下さい。"
                                ControlToValidate="txtDayFrom" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
                                Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                            ～&nbsp;
                            <asp:TextBox ID="txtDayTo" runat="server" MaxLength="10" Width="70px"></asp:TextBox>
                            <asp:RangeValidator ID="vdeDayTo" runat="server" ErrorMessage="追加日Toを正しく入力して下さい。"
                                ControlToValidate="txtDayTo" MaximumValue="2099/12/31" MinimumValue="1990/01/01"
                                Type="Date" ValidationGroup="Key" Display="Dynamic">*</asp:RangeValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdHeaderStyle2">
                            ユーザーID
                        </td>
                        <td class="tdDataStyle" id="tdDataLoginId" runat="server">
                            <asp:TextBox ID="txtLoginId" runat="server" Width="70px"></asp:TextBox>
                        </td>
                        <td class="tdHeaderStyle2" id="tdHeaderCastSite" runat="server">
                            キャストサイトを含む
                        </td>
                        <td class="tdDataStyle" id="tdDataCastSite" runat="server">
                            <asp:CheckBox ID="chkCastSite" runat="server" />
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
                <asp:Button runat="server" ID="btnCSV" Text="CSV出力" CssClass="seekbutton" CausesValidation="False"
                    OnClick="btnCSV_Click" />
            </asp:Panel>
        </fieldset>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[定義ポイント履歴]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count of
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%=grdUserDefPointHistory.PageIndex + 1%>
                        of
                        <%=grdUserDefPointHistory.PageCount%>
                    </a>
                </asp:Panel>
                &nbsp;
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="430px">
                    <asp:GridView ID="grdUserDefPointHistory" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        DataSourceID="dsSettleLog" AllowSorting="True" SkinID="GridViewColor" OnDataBound="grdUserDefPointHistory_DataBound">
                        <Columns>
                            <asp:BoundField DataField="ADD_POINT_DATE" HeaderText="追加日時" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}"
                                HtmlEncode="False">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="ユーザーID">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# Eval("SEX_CD").ToString().Equals("1") ? string.Format("../Man/ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("LOGIN_ID")) : string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}",Eval("LOGIN_ID"),"UserDefPointHistoryInquiry.aspx") %>'
                                        Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="HANDLE_NM" HeaderText="ハンドル名">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="USER_NM" HeaderText="氏名">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ADD_POINT_ID" HeaderText="ポイントID">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ADD_POINT" HeaderText="増減ポイント" DataFormatString="{0:N0}">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="REMARKS" HeaderText="備考">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="SEX_NM" HeaderText="性別">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
        </asp:Panel>
        <br />
    </div>
    <asp:ObjectDataSource ID="dsUserDefPointHistory" runat="server" SelectMethod="GetListPageCollection"
        TypeName="UserDefPointHistory" SelectCountMethod="GetListPageCount" EnablePaging="True"
        OnSelecting="dsUserDefPointHistory_Selecting" OnSelected="dsUserDefPointHistory_Selected">
        <SelectParameters>
            <asp:Parameter Name="pSiteCd" Type="String" />
            <asp:Parameter Name="pLoginId" Type="String" />
            <asp:Parameter Name="pDayFrom" Type="String" />
            <asp:Parameter Name="pDayTo" Type="String" />
            <asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" />
            <asp:Parameter Name="pContainsCastSite" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3"
        TargetControlID="vdrDayFrom" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4"
        TargetControlID="vdeDayFrom" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
        TargetControlID="vdeDayTo" HighlightCssClass="validatorCallout" />
    <ajaxToolkit:MaskedEditExtender ID="mskDayFrom" runat="server" MaskType="Date" Mask="9999/99/99"
        UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="True" TargetControlID="txtDayFrom">
    </ajaxToolkit:MaskedEditExtender>
    <ajaxToolkit:MaskedEditExtender ID="mskDayTo" runat="server" MaskType="Date" Mask="9999/99/99"
        UserDateFormat="YearMonthDay" ClearMaskOnLostFocus="True" TargetControlID="txtDayTo">
    </ajaxToolkit:MaskedEditExtender>
</asp:Content>
