<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AccessLog.aspx.cs" Inherits="Status_AccessLog" Title="アクセスログ"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="アクセスログ"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							<asp:Label ID="lblAccessUnit" runat="server" Text="報告年月"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstYYYY" runat="server" Width="54px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstMM" runat="server" Width="43px">
								<asp:ListItem Value="">--</asp:ListItem>
								<asp:ListItem Value="01">01</asp:ListItem>
								<asp:ListItem Value="02">02</asp:ListItem>
								<asp:ListItem Value="03">03</asp:ListItem>
								<asp:ListItem Value="04">04</asp:ListItem>
								<asp:ListItem Value="05">05</asp:ListItem>
								<asp:ListItem Value="06">06</asp:ListItem>
								<asp:ListItem Value="07">07</asp:ListItem>
								<asp:ListItem Value="08">08</asp:ListItem>
								<asp:ListItem Value="09">09</asp:ListItem>
								<asp:ListItem Value="10">10</asp:ListItem>
								<asp:ListItem Value="11">11</asp:ListItem>
								<asp:ListItem Value="12">12</asp:ListItem>
							</asp:DropDownList>
							<asp:Label ID="lblMonth" runat="server" Text="月"></asp:Label>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="False" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
			</asp:Panel>
		</fieldset>
		<fieldset>
			<legend>[アクセスログ]</legend>
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="560px">
				<asp:GridView ID="grdAccess" runat="server" AutoGenerateColumns="False" DataSourceID="dsAccessPage" CellPadding="0" ShowFooter="True" AllowSorting="True"
					OnRowDataBound="grdAccess_RowDataBound" SkinID="GridViewColorNoRowStyle">
					<Columns>
						<asp:TemplateField HeaderText="日付">
							<ItemStyle HorizontalAlign="Center" Width="40px" />
							<ItemTemplate>
								<asp:Label ID="Label1" runat="server" Text='<%# Eval("PRINT_DAY") %>' BackColor='<%# GetBackColor(Eval("ACCESS_DAY_OF_WEEK")) %>' Width="40px"></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="曜日" FooterText="合計">
							<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
							<ItemTemplate>
								<asp:Label ID="Label2" runat="server" Text='<%# Eval("ACCESS_DAY_OF_WEEK") %>' BackColor='<%# GetBackColor(Eval("ACCESS_DAY_OF_WEEK")) %>' Width="60px"></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="登録数">
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("REGIST_USER") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="認証数">
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("REGIST_REPORT_USER") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="退会数">
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("WITHDRAWAL_COUNT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="再登録数">
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("RE_REGIST_COUNT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="TOPﾍﾟｰｼﾞｱｸｾｽ数">
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("TOP_PAGE") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ﾛｸﾞｲﾝ数">
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("LOGIN_SUCCESS") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ﾛｸﾞｲﾝ数(ﾕﾆｰｸ)">
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("LOGIN_SUCCESS_UNIQUE") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ﾌﾟﾛﾌｱｸｾｽ数">
							<ItemTemplate>
								<asp:HyperLink ID="lnkCastPage" runat="server" NavigateUrl='<%# string.Format("ProfAccessCountDailyList.aspx?sitecd={0}&year={1}&month={2}&day={3}",Eval("SITE_CD"),lstYYYY.SelectedValue,lstMM.SelectedValue,Eval("LINK_DAY")) %>'
										Text='<%# Eval("CAST_PAGE") %>' Visible='<%# GetColumnVisible("day") %>'></asp:HyperLink>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("CAST_PAGE") %>' Visible='<%# GetColumnVisible("year") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ﾌﾟﾛﾌﾕﾆｰｸ数">
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("CAST_PAGE_UNIQUE") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="初回入金者">
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("FIRST_RECEIPT_COUNT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="入金者(決済)">
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("RECEIPT_COUNT_UNIQUE") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="入金者(ﾎﾟｲﾝﾄｱﾌﾘ)">
							<ItemTemplate>
								<asp:Label ID="Label3" runat="server" Text='<%# Eval("RECEIPT_POINT_AF_COUNT_UNIQUE") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" CssClass="NumPad" />
						</asp:TemplateField>
					</Columns>
					<FooterStyle ForeColor="Black" BackColor="LightYellow" />
				</asp:GridView>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsAccess" runat="server" SelectMethod="AccessInquiryDay" TypeName="AccessPage">
		<SelectParameters>
			<asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstYYYY" Name="pYYYY" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstMM" Name="pMM" PropertyName="SelectedValue" Type="String" />
			<asp:QueryStringParameter Name="pSexCd" QueryStringField="sexcd" Type="String" DefaultValue="" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
</asp:Content>
