﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DailyReceivePtCnt.aspx.cs" Inherits="Status_DailyReceivePtCnt" Title="期間別出演者獲得報酬集計" %>
<%@ Import namespace="ViComm" %>
<%@ Import Namespace="iBridCommLib" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="期間別出演者獲得報酬集計"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" Runat="Server">
	<div class="admincontent">
		<%-- ============================== --%>
		<%--  Search Condition              --%>
		<%-- ============================== --%>
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="180px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							<asp:Label ID="lblDispDate" runat="server" Text="表示月"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstYYYY" runat="server" Width="60px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstMM" runat="server" Width="40px">
							</asp:DropDownList>月
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" CausesValidation="True" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
				※1ヶ月の経過期間は30日/1年単位は360日です。
			</asp:Panel>
		</fieldset>

		<%-- ============================== --%>
		<%--  Data                          --%>
		<%-- ============================== --%>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset class="fieldset">
				<legend>[集計結果]</legend>
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="None">
					<asp:GridView ID="grdData" runat="server" ShowFooter="True" AllowSorting="True" AutoGenerateColumns="False" Width="3200px"
						DataSourceID="" OnRowDataBound="grdData_RowDataBound" SkinID="GridViewColor" Font-Size="X-Small" EnableViewState="false">
						<Columns>
							<asp:TemplateField HeaderText="経過">
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
								<ItemTemplate>
									<asp:Label ID="lblElapsedDaysDispTerm" runat="server" Text='<%# GetTerm(Eval("ELAPSED_DAYS_DISP_TERM").ToString()) %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Center" />
								<FooterTemplate>
									<asp:Label ID="lblTotalNm" runat="server" Text="合計"></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<%-- 各日付データ --%>
							<%-- 合計 --%>
							<asp:TemplateField HeaderText="人数">
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" BackColor="#FFEBCD" />
								<ItemTemplate>
									<asp:Label ID="lblITotalCount" runat="server" Text='<%# Eval("TOTAL_CNT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" BackColor="#FFEBCD" />
								<FooterTemplate>
									<asp:Label ID="lblFTotalCount" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="獲得pt">
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" BackColor="#FFEBCD" />
								<ItemTemplate>
									<asp:Label ID="lblITotalPoint" runat="server" Text='<%# Eval("TOTAL_PT") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" BackColor="#FFEBCD" />
								<FooterTemplate>
									<asp:Label ID="lblFTotalPoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
						</Columns>
						<PagerSettings Mode="NumericFirstLast" />
						<FooterStyle ForeColor="Black" BackColor="LightYellow" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
	</div>

	<%-- ============================== --%>
	<%--  Data Source                   --%>
	<%-- ============================== --%>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsDailyReceivePtCnt" runat="server" TypeName="DailyReceivePtCnt" SelectCountMethod="GetPageCount" SelectMethod="GetPageCollection" EnablePaging="True"
		OnSelecting="dsDailyReceivePtCnt_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSearchCondition" Type="Object" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
