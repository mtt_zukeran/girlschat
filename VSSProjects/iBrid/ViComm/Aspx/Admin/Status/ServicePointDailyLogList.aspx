﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ServicePointDailyLogList.aspx.cs" Inherits="Status_ServicePointDailyLogList" Title="サービスポイント集計"
    ValidateRequest="false" %>
<%@ Import Namespace="ViComm" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
    <asp:Label ID="lblPgmTitle" runat="server" Text="サービスポイント集計"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
    <div class="admincontent">
        <asp:Panel runat="server" ID="pnlKey">
            <fieldset class="fieldset">
                <legend>[設定]</legend>
                <asp:Panel ID="pnlSeekCondition" runat="server">
                    <table border="0" style="width: 800px" class="tableStyle">
                        <tr>
                            <td class="tdHeaderStyle">
                                サイトコード

                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM"
                                    DataValueField="SITE_CD" Width="170px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                開始日
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstFromYYYY" runat="server" Width="60px">
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstFromMM" runat="server" Width="40px">
                                </asp:DropDownList>月

                                <asp:DropDownList ID="lstFromDD" runat="server" Width="40px">
                                </asp:DropDownList>日
                            </td>
                        </tr>
                        <tr>
                            <td class="tdHeaderStyle">
                                終了日
                            </td>
                            <td class="tdDataStyle">
                                <asp:DropDownList ID="lstToYYYY" runat="server" Width="60px">
                                </asp:DropDownList>年
                                <asp:DropDownList ID="lstToMM" runat="server" Width="40px">
                                </asp:DropDownList>月

                                <asp:DropDownList ID="lstToDD" runat="server" Width="40px">
                                </asp:DropDownList>日

                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="btnListSeek" Text="一覧検索" CssClass="seekbutton" OnClick="btnListSeek_Click"
                    ValidationGroup="Key" CausesValidation="True" />
                <asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click"
                    CausesValidation="False" />
				<asp:Button runat="server" ID="btnCsv" Text="ＣＳＶ出力" CssClass="seekbutton" OnClick="btnCSV_Click" />
                <asp:CustomValidator ID="vdcFromTo" runat="server" ErrorMessage="" OnServerValidate="vdcFromTo_ServerValidate"
                    ValidationGroup="Key"></asp:CustomValidator>
            </fieldset>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInfo">
            <fieldset>
                <legend>[記事一覧]</legend>
                <asp:Panel runat="server" ID="pnlCount">
                    <a class="reccount">Record Count
                        <%# GetRecCount() %>
                    </a>
                    <br />
                    <a class="reccount">Current viewing page
                        <%= grdServicePointDailyLog.PageIndex + 1 %>
                        of
                        <%= grdServicePointDailyLog.PageCount %>
                    </a>
                </asp:Panel>
                &nbsp;<br />
                <asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="443px">
                    <asp:GridView ID="grdServicePointDailyLog" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        DataSourceID="dsServicePointDailyLog" SkinID="GridViewColor" AllowSorting="true" OnRowDataBound="grdServicePointDailyLog_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="年月日">
                                <ItemTemplate>
                                    <asp:Label ID="lblTweetDate" runat="server" Text='<%# Eval("REPORT_DAY") %>'></asp:Label><br />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="付与ﾎﾟｲﾝﾄ">
                                <ItemTemplate>
                                    <asp:Label ID="lblAddServicePoint" runat="server" Text='<%# Eval("ADD_SERVICE_POINT") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="失効ﾎﾟｲﾝﾄ">
                                <ItemTemplate>
                                    <asp:Label ID="lblLostServicePoint" runat="server" Text='<%# Eval("LOST_SERVICE_POINT") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="right" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerSettings Mode="NumericFirstLast" />
                    </asp:GridView>
                </asp:Panel>
            </fieldset>
            <br />
        </asp:Panel>
    </div>
    <asp:ObjectDataSource ID="dsServicePointDailyLog" runat="server" SelectMethod="GetPageCollection" ConvertNullToDBNull="true" SortParameterName=""
        TypeName="ServicePointDailyLog" SelectCountMethod="GetPageCount" EnablePaging="True" OnSelected="dsServicePointDailyLog_Selected"
        OnSelecting="dsServicePointDailyLog_Selecting">
        <SelectParameters>
            <asp:Parameter Name="pSearchCondition" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetNewVerList" TypeName="Site">
    </asp:ObjectDataSource>
</asp:Content>
