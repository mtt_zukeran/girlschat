﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;
using System.Text;

public partial class Status_DailyProductBuy : System.Web.UI.Page {
	private string recCount = string.Empty;

	protected void Page_Load(object sender, EventArgs e) {
		if (!IsPostBack) {
			this.InitPage();
			this.GetList();
		}
	}
	protected void btnListSeek_Click(object sender, EventArgs e) {
		if (this.IsValid) {
			this.GetList();
		}
	}
	protected void btnClear_Click(object sender, EventArgs e) {
		this.InitPage();
	}
	protected void btnCSV_Click(object sender, EventArgs e) {
		Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", ViCommConst.CSV_FILE_NM_PRODUCT_BUY_SUMMARY));
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");
		Response.BinaryWrite(encoding.GetBytes(this.GetCsvString()));
		Response.End();
	}
	protected void lst_DataBound(object sender, EventArgs e) {
		DropDownList oDropDownList = sender as DropDownList;
		if (oDropDownList != null) {
			oDropDownList.Items.Insert(0, new ListItem(string.Empty, string.Empty));
		}
	}
	protected void dsProductHistory_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			this.recCount = e.ReturnValue.ToString();
		}
	}

	private void InitPage() {
		this.pnlInfo.Visible = false;

		this.recCount = "0";
		this.lstSiteCd.DataBind();
		this.lstProductType.DataBind();
		this.lstProductAgent.DataBind();
		this.ClearField();

		if (!this.IsPostBack) {
			if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals(string.Empty)) {
				this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}
		}
	}

	private void ClearField() {
		SysPrograms.SetupFromToDay(this.lstFromYYYY, this.lstFromMM, this.lstFromDD, this.lstToYYYY, this.lstToMM, this.lstToDD, false);
		this.lstFromYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstToYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstFromMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstToMM.SelectedValue = DateTime.Now.ToString("MM");
		this.lstFromDD.SelectedValue = DateTime.Now.ToString("dd");
		this.lstToDD.SelectedValue = DateTime.Now.ToString("dd");
		this.txtProductMakerSummary.Text = string.Empty;
	}

	private void GetList() {
		this.pnlInfo.Visible = true;
		this.grdDailyProductBuy.PageSize = 1000;
		this.grdDailyProductBuy.DataBind();
		this.pnlCount.DataBind();
	}

	private string GetCsvString() {
		StringBuilder oCsvBuilder = new StringBuilder();
		oCsvBuilder.AppendLine("商品ID,商品名,製造元,購入数,ポイント計,ロイヤリティ計");

		using (ProductHistory oProductHistory = new ProductHistory()) {
			using (DataSet oDataSet = oProductHistory.GetList(
				this.lstSiteCd.SelectedValue,
				this.lstProductAgent.SelectedValue,
				this.lstProductType.SelectedValue,
				this.lstFromYYYY.SelectedValue,
				this.lstFromMM.SelectedValue,
				this.lstFromDD.SelectedValue,
				this.lstToYYYY.SelectedValue,
				this.lstToMM.SelectedValue,
				this.lstToDD.SelectedValue,
				this.txtProductMakerSummary.Text)) {

				foreach (DataRow oDataRow in oDataSet.Tables[0].Rows) {
					oCsvBuilder.AppendFormat("{0},{1},{2},{3},{4},{5}",oDataRow["PRODUCT_ID"],oDataRow["PRODUCT_NM"],oDataRow["PRODUCT_MAKER_SUMMARY"],oDataRow["BUY_COUNT"],oDataRow["TOTAL_POINT"],oDataRow["ROYALTY_POINT"]);
					oCsvBuilder.AppendLine();
				}
			}
		}

		return oCsvBuilder.ToString();
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected string GenerateProductMainteUrl(object pSeed) {
		DataRowView oProductDataRowView = (DataRowView)pSeed;

		string sSiteCd = iBridUtil.GetStringValue(oProductDataRowView["SITE_CD"]);
		string sProductAgentCd = iBridUtil.GetStringValue(oProductDataRowView["PRODUCT_AGENT_CD"]);
		string sProductSeq = iBridUtil.GetStringValue(oProductDataRowView["PRODUCT_SEQ"]);

		UrlBuilder oUrlBuilder = new UrlBuilder("~/Product/ProductMainte.aspx");
		oUrlBuilder.Parameters.Add("site_cd", sSiteCd);
		oUrlBuilder.Parameters.Add("product_agent_cd", sProductAgentCd);
		oUrlBuilder.Parameters.Add("product_seq", sProductSeq);
		return oUrlBuilder.ToString();
	}

	protected string GenerateProductBuyUrl(object pSeed) {
		DataRowView oProductDataRowView = (DataRowView)pSeed;

		//string sSiteCd = iBridUtil.GetStringValue(oProductDataRowView["SITE_CD"]);
		string sProductId = iBridUtil.GetStringValue(oProductDataRowView["PRODUCT_ID"]);

		UrlBuilder oUrlBuilder = new UrlBuilder("~/Status/ProductBuy.aspx");
		//oUrlBuilder.Parameters.Add("site_cd", sSiteCd);
		oUrlBuilder.Parameters.Add("site_cd", this.lstSiteCd.SelectedValue);
		oUrlBuilder.Parameters.Add("product_id", sProductId);
		oUrlBuilder.Parameters.Add("product_agent_cd", this.lstProductAgent.SelectedValue);
		oUrlBuilder.Parameters.Add("product_type", this.lstProductType.SelectedValue);
		oUrlBuilder.Parameters.Add("from", string.Concat(this.lstFromYYYY.SelectedValue, this.lstFromMM.SelectedValue, this.lstFromDD.SelectedValue));
		oUrlBuilder.Parameters.Add("to", string.Concat(this.lstToYYYY.SelectedValue, this.lstToMM.SelectedValue, this.lstToDD.SelectedValue));
		return oUrlBuilder.ToString();
	}
}
