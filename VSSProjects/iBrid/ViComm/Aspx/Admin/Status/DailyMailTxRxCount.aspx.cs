﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 期間別メールやりとり集計

--	Progaram ID		: DailyMailTxRxCount
--
--  Creation Date	: 2016.11.11
--  Creater			: M&TT Zukeran
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using iBridCommLib;
using ViComm;

public partial class Status_DailyMailTxRxCount:Page {

	// CSVファイル名
	private const string CSV_FILE_NM_DAILY_MAIL_COUNT = "DailyMailTxRxCount.csv";

	private const int RNUM_REPORT_DATE = 0;
	private const int RNUM_MAN_RETURN_RATE = 18;
	private const int RNUM_WOMAN_RETURN_RATE = 36;

	// 返信率の小数桁数
	private const int RETURN_RATE_DECIMAL = 1;

	private int[] iaTotalCount;
	private string[] saRowValueNm = { "lblReportDate","lblManTxUUCount","lblManTxNormalCount","lblManTxPicCount","lblManTxMovieCount","lblManTxReturnUUCount","lblManTxReturnNormalCount","lblManTxReturnPicCount","lblManTxReturnMovieCount","lblManTxTotalUUCount","lblManTxTotalNormalCount","lblManTxTotalPicCount","lblManTxTotalMovieCount","lblManRxUUCount","lblManRxBatchCount","lblManRxNormalCount","lblManRxReturnCount","lblManRxTotalCount","lblManReturnRate","lblWomanTxUUCount","lblWomanTxBatchCount","lblWomanTxNormalCount","lblWomanTxPicCount","lblWomanTxMovieCount","lblWomanTxReturnUUCount","lblWomanTxReturnNormalCount","lblWomanTxReturnPicCount","lblWomanTxReturnMovieCount","lblWomanTxTotalUUCount","lblWomanTxTotalNormalCount","lblWomanTxTotalPicCount","lblWomanTxTotalMovieCount","lblWomanRxUUCount","lblWomanRxCount","lblWomanRxReturnCount","lblWomanRxTotalCount","lblWomanReturnRate" };
	private string[] saTotalValueNm = { "lblTotalNm","lblManTxUUTotalCount","lblManTxNormalTotalCount","lblManTxPicTotalCount","lblManTxMovieTotalCount","lblManTxReturnUUTotalCount","lblManTxReturnNormalTotalCount","lblManTxReturnPicTotalCount","lblManTxReturnMovieTotalCount","lblManTxTotalUUTotalCount","lblManTxTotalNormalTotalCount","lblManTxTotalPicTotalCount","lblManTxTotalMovieTotalCount","lblManRxUUTotalCount","lblManRxBatchTotalCount","lblManRxNormalTotalCount","lblManRxReturnTotalCount","lblManRxTotalTotalCount","lblManTotalReturnRate","lblWomanTxUUTotalCount","lblWomanTxBatchTotalCount","lblWomanTxNormalTotalCount","lblWomanTxPicTotalCount","lblWomanTxMovieTotalCount","lblWomanTxReturnUUTotalCount","lblWomanTxReturnNormalTotalCount","lblWomanTxReturnPicTotalCount","lblWomanTxReturnMovieTotalCount","lblWomanTxTotalUUTotalCount","lblWomanTxTotalNormalTotalCount","lblWomanTxTotalPicTotalCount","lblWomanTxTotalMovieTotalCount","lblWomanRxUUTotalCount","lblWomanRxTotalCount","lblWomanRxReturnTotalCount","lblWomanRxTotalTotalCount","lblWomanTotalReturnRate" };

	// 期間合計ﾕﾆｰｸ人数
	private int iTxUUCount;
	private int iTxReturnUUCount;
	private int iTxTotalUUCount;
	private int iRxUUCount;

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		// 表示されてる検索結果の非表示
		grdDailyMailCount.DataSourceID = "";
		pnlInfo.Visible = false;

		// 検索結果の合計数を初期化
		ClearField();
		DataBind();

		if (!IsPostBack) {
			// 日付リストの選択肢を設定
			SysPrograms.SetupYear(lstYYYY);
			SysPrograms.SetMonthList(lstMM);

			// 性別の選択肢を設定
			lstSexCd.Items.Clear();
			lstSexCd.Items.Add(new ListItem("男性",ViCommConst.MAN));
			lstSexCd.Items.Add(new ListItem("女性",ViCommConst.WOMAN));

			// 検索の度に取得されないようにしておく
			lstSiteCd.DataSourceID = "";
		}

		// 日付リストの初期値設定
		lstYYYY.SelectedIndex = 0;
		lstMM.SelectedValue = DateTime.Now.ToString("MM");

		// 性別の初期値設定
		lstSexCd.SelectedValue = ViCommConst.MAN;
	}

	/// <summary>
	/// 表示初期化
	/// </summary>
	private void ClearField() {
		// 合計欄表示用
		iTxUUCount = 0;
		iTxReturnUUCount = 0;
		iTxTotalUUCount = 0;
		iRxUUCount = 0;
	}

	/// <summary>
	/// 選択されている検索条件での検索を実行
	/// </summary>
	private void GetList() {
		// 初期化
		ClearField();
		grdDailyMailCount.PageIndex = 0;
		pnlInfo.Visible = true;
		grdDailyMailCount.DataSourceID = "dsDailyMailCount";

		DataBind();
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		string fileName = CSV_FILE_NM_DAILY_MAIL_COUNT;

		Response.AddHeader("Content-Disposition","attachment;filename=" + fileName);
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		using (DailyMailTxRxCount oDailyMailTxRxCount = new DailyMailTxRxCount()) {
			DataSet ds = oDailyMailTxRxCount.GetList(
								lstSiteCd.SelectedValue,
								lstYYYY.SelectedValue,
								lstMM.SelectedValue,
								lstSexCd.SelectedValue);

			string sCsvRec = GetCsvString(ds);

			Response.BinaryWrite(encoding.GetBytes(sCsvRec));
			Response.End();
		}
	}

	/// <summary>
	/// CSVに出力する文字列を生成
	/// </summary>
	/// <param name="pDataSet"></param>
	/// <returns></returns>
	private string GetCsvString(DataSet pDataSet) {
		DataRow dr;
		StringBuilder oCsvBuilder = new StringBuilder();

		// ﾀｲﾄﾙ行
		oCsvBuilder.Append("日付,");
		oCsvBuilder.Append("男性送信_ﾕﾆｰｸ人数,");
		oCsvBuilder.Append("男性送信_添付なし件数,");
		oCsvBuilder.Append("男性送信_写真添付件数,");
		oCsvBuilder.Append("男性送信_動画添付件数,");
		oCsvBuilder.Append("男性返信_ﾕﾆｰｸ人数,");
		oCsvBuilder.Append("男性返信_添付なし件数,");
		oCsvBuilder.Append("男性返信_写真添付件数,");
		oCsvBuilder.Append("男性返信_動画添付件数,");
		oCsvBuilder.Append("男性送信・返信合計_ﾕﾆｰｸ人数,");
		oCsvBuilder.Append("男性送信・返信合計_添付なし件数,");
		oCsvBuilder.Append("男性送信・返信合計_写真添付件数,");
		oCsvBuilder.Append("男性送信・返信合計_動画添付件数,");
		oCsvBuilder.Append("男性受信_ﾕﾆｰｸ人数,");
		oCsvBuilder.Append("男性受信_一括送信件数,");
		oCsvBuilder.Append("男性受信_個別送信件数,");
		oCsvBuilder.Append("男性受信_返信件数,");
		oCsvBuilder.Append("男性受信_合計件数,");
		oCsvBuilder.Append("男性_返信率,");
		oCsvBuilder.Append("女性送信_ﾕﾆｰｸ人数,");
		oCsvBuilder.Append("女性送信_一括送信件数,");
		oCsvBuilder.Append("女性送信_添付なし個別送信件数,");
		oCsvBuilder.Append("女性送信_写真添付件数,");
		oCsvBuilder.Append("女性送信_動画添付件数,");
		oCsvBuilder.Append("女性返信_ﾕﾆｰｸ人数,");
		oCsvBuilder.Append("女性返信_添付なし件数,");
		oCsvBuilder.Append("女性返信_写真添付件数,");
		oCsvBuilder.Append("女性返信_動画添付件数,");
		oCsvBuilder.Append("女性送信・返信合計_ﾕﾆｰｸ人数,");
		oCsvBuilder.Append("女性送信・返信合計_添付なし件数,");
		oCsvBuilder.Append("女性送信・返信合計_写真添付件数,");
		oCsvBuilder.Append("女性送信・返信合計_動画添付件数,");
		oCsvBuilder.Append("女性受信_ﾕﾆｰｸ人数,");
		oCsvBuilder.Append("女性受信_送信件数,");
		oCsvBuilder.Append("女性受信_返信件数,");
		oCsvBuilder.Append("女性受信_合計件数,");
		oCsvBuilder.AppendLine("女性_返信率");

		// データ行
		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];
			oCsvBuilder.Append(string.Format("{0},",DateTime.Parse(dr["REPORT_DATE"].ToString()).ToString("yyyy/MM/dd")));
			oCsvBuilder.Append(string.Format("{0},",dr["MAN_TX_UNIQ_USER_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["MAN_TX_NORMAL_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["MAN_TX_PIC_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["MAN_TX_MOVIE_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["MAN_TX_RETURN_UNIQ_USER_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["MAN_TX_RETURN_NORMAL_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["MAN_TX_RETURN_PIC_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["MAN_TX_RETURN_MOVIE_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["MAN_TX_TOTAL_UNIQ_USER_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["MAN_TX_TOTAL_NORMAL_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["MAN_TX_TOTAL_PIC_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["MAN_TX_TOTAL_MOVIE_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["MAN_RX_UNIQ_USER_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["MAN_RX_BATCH_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["MAN_RX_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["MAN_RX_RETURN_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["MAN_RX_TOTAL_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0}%,",Math.Round(double.Parse(dr["MAN_RETURN_RATE"].ToString()),RETURN_RATE_DECIMAL)));
			oCsvBuilder.Append(string.Format("{0},",dr["WOMAN_TX_UNIQ_USER_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["WOMAN_TX_BATCH_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["WOMAN_TX_NORMAL_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["WOMAN_TX_PIC_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["WOMAN_TX_MOVIE_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["WOMAN_TX_RETURN_UNIQ_USER_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["WOMAN_TX_RETURN_NORMAL_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["WOMAN_TX_RETURN_PIC_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["WOMAN_TX_RETURN_MOVIE_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["WOMAN_TX_TOTAL_UNIQ_USER_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["WOMAN_TX_TOTAL_NORMAL_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["WOMAN_TX_TOTAL_PIC_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["WOMAN_TX_TOTAL_MOVIE_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["WOMAN_RX_UNIQ_USER_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["WOMAN_RX_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["WOMAN_RX_RETURN_CNT"].ToString()));
			oCsvBuilder.Append(string.Format("{0},",dr["WOMAN_RX_TOTAL_CNT"].ToString()));
			oCsvBuilder.AppendLine(string.Format("{0}%",Math.Round(double.Parse(dr["WOMAN_RETURN_RATE"].ToString()),RETURN_RATE_DECIMAL)));
		}
		return DisplayWordUtil.Replace(oCsvBuilder.ToString());
	}

	/// <summary>
	/// 集計結果のデータ設定処理
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	protected void grdDailyMailCount_RowDataBound(object sender,GridViewRowEventArgs e) {
		// データが存在しない場合
		if (grdDailyMailCount.Controls.Count <= 0) {
			return;
		}

		if (e.Row.RowType == DataControlRowType.Header) {
			// フッターの合計欄表示用
			iaTotalCount = new int[e.Row.Cells.Count];

			CustomHeader(e.Row);
		} else if (e.Row.RowType == DataControlRowType.DataRow) {
			CustomDataRow(e.Row);
		} else if (e.Row.RowType == DataControlRowType.Footer) {
			CustomFooter(e.Row);
		}
	}

	/// <summary>
	/// ヘッダー行のカスタマイズ
	/// </summary>
	private void CustomHeader(GridViewRow oHeaderRow) {
		if (oHeaderRow == null) {
			return;
		}
		GridViewRow oHeaderRow1 = new GridViewRow(-1,-1,DataControlRowType.Header,DataControlRowState.Normal);
		TableCell cell;

		// 1行目
		// 日付
		cell = new TableCell();
		cell.ColumnSpan = 1;
		cell.RowSpan = 2;
		cell.Text = "日付";
		oHeaderRow1.Cells.Add(cell);

		// メール送信
		cell = new TableCell();
		cell.ColumnSpan = ViCommConst.MAN.Equals(lstSexCd.SelectedValue) ? 4 : 5;
		cell.RowSpan = 1;
		cell.Text = "メール送信";
		oHeaderRow1.Cells.Add(cell);

		// メール返信
		cell = new TableCell();
		cell.ColumnSpan = 4;
		cell.RowSpan = 1;
		cell.Text = "メール返信";
		oHeaderRow1.Cells.Add(cell);

		// 送信・返信合計
		cell = new TableCell();
		cell.ColumnSpan = 4;
		cell.RowSpan = 1;
		cell.Text = "送信・返信合計";
		oHeaderRow1.Cells.Add(cell);

		// メール受信
		cell = new TableCell();
		cell.ColumnSpan = ViCommConst.MAN.Equals(lstSexCd.SelectedValue) ? 5 : 4;
		cell.RowSpan = 1;
		cell.Text = "メール受信";
		oHeaderRow1.Cells.Add(cell);

		// 返信率
		cell = new TableCell();
		cell.ColumnSpan = 1;
		cell.RowSpan = 2;
		cell.Text = "返信率";
		oHeaderRow1.Cells.Add(cell);

		// 2行目
		// 挿入するヘッダーで2行分の高さを持つ列に位置する箇所を削除する
		// (削除すると数がずれるため、後ろから処理する)
		for (int i = oHeaderRow.Cells.Count - 1;i >= 0;i--) {
			// 日付、返信率
			if (i == RNUM_REPORT_DATE || i == RNUM_MAN_RETURN_RATE || i == RNUM_WOMAN_RETURN_RATE) {
				oHeaderRow.Cells.RemoveAt(i);
			}
			// 男性が選択されてる場合、女性用の列を削除
			else if (ViCommConst.MAN.Equals(lstSexCd.SelectedValue) && i > RNUM_MAN_RETURN_RATE) {
				oHeaderRow.Cells.RemoveAt(i);
			}
			// 女性が選択されてる場合、男性用の列を削除
			else if (ViCommConst.WOMAN.Equals(lstSexCd.SelectedValue) && i <= RNUM_MAN_RETURN_RATE) {
				oHeaderRow.Cells.RemoveAt(i);
			}
		}

		// 元のヘッダー行の上に挿入
		grdDailyMailCount.Controls[0].Controls.AddAt(0,oHeaderRow1);
	}

	/// <summary>
	/// データ行のカスタマイズ
	/// </summary>
	/// <param name="oDataRow"></param>
	private void CustomDataRow(GridViewRow oDataRow) {
		Label lblRow;

		for (int i = 1;i < oDataRow.Cells.Count;i++) {
			// 日付、返信率
			if (i == RNUM_REPORT_DATE || i == RNUM_MAN_RETURN_RATE || i == RNUM_WOMAN_RETURN_RATE) {
				iaTotalCount[i] = 0;
				continue;
			}

			lblRow = (Label)oDataRow.Cells[i].FindControl(saRowValueNm[i]);
			iaTotalCount[i] += int.Parse(lblRow.Text);
		}

		// 選択されてる性別により逆の性別用の列を削除する
		// (削除すると数がずれるため、後ろから処理する)
		for (int i = oDataRow.Cells.Count - 1;i >= 0;i--) {
			// 日付
			if (i == RNUM_REPORT_DATE) {
			}
			// 男性が選択されてる場合、女性用の列を削除
			else if (ViCommConst.MAN.Equals(lstSexCd.SelectedValue) && i > RNUM_MAN_RETURN_RATE) {
				oDataRow.Cells.RemoveAt(i);
			}
			// 女性が選択されてる場合、男性用の列を削除
			else if (ViCommConst.WOMAN.Equals(lstSexCd.SelectedValue) && i <= RNUM_MAN_RETURN_RATE) {
				oDataRow.Cells.RemoveAt(i);
			}
		}

		// 表示期間内の合計ﾕﾆｰｸ人数
		if (ViCommConst.MAN.Equals(lstSexCd.SelectedValue)) {
			iTxUUCount = Convert.ToInt32(DataBinder.Eval(oDataRow.DataItem,"MAN_TERM_TX_UU_CNT"));
			iTxReturnUUCount = Convert.ToInt32(DataBinder.Eval(oDataRow.DataItem,"MAN_TERM_TX_RETURN_UU_CNT"));
			iTxTotalUUCount = Convert.ToInt32(DataBinder.Eval(oDataRow.DataItem,"MAN_TERM_TX_TOTAL_UU_CNT"));
			iRxUUCount = Convert.ToInt32(DataBinder.Eval(oDataRow.DataItem,"MAN_TERM_RX_UU_CNT"));
		} else {
			iTxUUCount = Convert.ToInt32(DataBinder.Eval(oDataRow.DataItem,"WOMAN_TERM_TX_UU_CNT"));
			iTxReturnUUCount = Convert.ToInt32(DataBinder.Eval(oDataRow.DataItem,"WOMAN_TERM_TX_RETURN_UU_CNT"));
			iTxTotalUUCount = Convert.ToInt32(DataBinder.Eval(oDataRow.DataItem,"WOMAN_TERM_TX_TOTAL_UU_CNT"));
			iRxUUCount = Convert.ToInt32(DataBinder.Eval(oDataRow.DataItem,"WOMAN_TERM_RX_UU_CNT"));
		}
	}

	/// <summary>
	/// フッター行のカスタマイズ
	/// </summary>
	/// <param name="oFooterRow"></param>
	private void CustomFooter(GridViewRow oFooterRow) {
		Label lblRow;
		int iTxTotalCount = 0;
		int iRxTotalCount = 0;
		double dReturnRate;

		for (int i = 1;i < oFooterRow.Cells.Count;i++) {
			// 日付、返信率
			if (i == RNUM_REPORT_DATE || i == RNUM_MAN_RETURN_RATE || i == RNUM_WOMAN_RETURN_RATE) {
				continue;
			}

			// 返信率の算出用の値を取得
			if (ViCommConst.MAN.Equals(lstSexCd.SelectedValue)) {
				if (saRowValueNm[i].Equals("lblManTxReturnNormalCount")
					|| saRowValueNm[i].Equals("lblManTxReturnPicCount")
					|| saRowValueNm[i].Equals("lblManTxReturnMovieCount")
				) {
					iTxTotalCount += iaTotalCount[i];
				} else if (saRowValueNm[i].Equals("lblManRxTotalCount")) {
					iRxTotalCount += iaTotalCount[i];
				}
			} else {
				if (saRowValueNm[i].Equals("lblWomanTxReturnNormalCount")
					|| saRowValueNm[i].Equals("lblWomanTxReturnPicCount")
					|| saRowValueNm[i].Equals("lblWomanTxReturnMovieCount")
				) {
					iTxTotalCount += iaTotalCount[i];
				} else if (saRowValueNm[i].Equals("lblWomanRxTotalCount")) {
					iRxTotalCount += iaTotalCount[i];
				}
			}

			// 合計値を設定
			lblRow = (Label)oFooterRow.Cells[i].FindControl(saTotalValueNm[i]);
			if (saRowValueNm[i].Equals("lblManTxUUCount")
				|| saRowValueNm[i].Equals("lblWomanTxUUCount")
			) {
				// メール送信ﾕﾆｰｸ人数
				lblRow.Text = iTxUUCount.ToString();
			} else if (saRowValueNm[i].Equals("lblManTxReturnUUCount")
				|| saRowValueNm[i].Equals("lblWomanTxReturnUUCount")
			) {
				// メール返信ﾕﾆｰｸ人数
				lblRow.Text = iTxReturnUUCount.ToString();
			} else if (saRowValueNm[i].Equals("lblManTxTotalUUCount")
				|| saRowValueNm[i].Equals("lblWomanTxTotalUUCount")
			) {
				// 送信・返信合計ﾕﾆｰｸ人数
				lblRow.Text = iTxTotalUUCount.ToString();
			} else if (saRowValueNm[i].Equals("lblManRxUUCount")
				|| saRowValueNm[i].Equals("lblWomanRxUUCount")
			) {
				// メール受信ﾕﾆｰｸ人数
				lblRow.Text = iRxUUCount.ToString();
			} else {
				lblRow.Text = iaTotalCount[i].ToString();
			}
		}

		// 合計の返信率を算出＆設定
		if (ViCommConst.MAN.Equals(lstSexCd.SelectedValue)) {
			lblRow = (Label)oFooterRow.Cells[RNUM_MAN_RETURN_RATE].FindControl(saTotalValueNm[RNUM_MAN_RETURN_RATE]);
		} else {
			lblRow = (Label)oFooterRow.Cells[RNUM_WOMAN_RETURN_RATE].FindControl(saTotalValueNm[RNUM_WOMAN_RETURN_RATE]);
		}
		if (iRxTotalCount > 0 && iTxTotalCount > 0) {
			dReturnRate = ((double)iTxTotalCount / iRxTotalCount) * 100;
			lblRow.Text = string.Format("{0}%",Math.Round(dReturnRate,RETURN_RATE_DECIMAL));
		} else {
			lblRow.Text = "0%";
		}

		// 選択されてる性別により逆の性別用の列を削除する
		// (削除すると数がずれるため、後ろから処理する)
		for (int i = oFooterRow.Cells.Count - 1;i >= 0;i--) {
			// 日付
			if (i == RNUM_REPORT_DATE) {
			}
			// 男性が選択されてる場合、女性用の列を削除
			else if (ViCommConst.MAN.Equals(lstSexCd.SelectedValue) && i > RNUM_MAN_RETURN_RATE) {
				oFooterRow.Cells.RemoveAt(i);
			}
			// 女性が選択されてる場合、男性用の列を削除
			else if (ViCommConst.WOMAN.Equals(lstSexCd.SelectedValue) && i <= RNUM_MAN_RETURN_RATE) {
				oFooterRow.Cells.RemoveAt(i);
			}
		}
	}

	/// <summary>
	/// パーセント表示（返信率表示用）
	/// </summary>
	/// <param name="pNum"></param>
	/// <returns></returns>
	protected string GetRate(object pNum) {
		return string.Format("{0}%",Math.Round(Convert.ToDouble(pNum),RETURN_RATE_DECIMAL));
	}
}
