﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ユーザー別稼動状況集計
--	Progaram ID		: UserPerformanceInquiry
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Status_UserPerformanceInquiry:System.Web.UI.Page {

	private string orderField = "";

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		ClearField();
		lstSiteCd.DataBind();
		if (!IsPostBack) {
			lstSiteCd.DataSourceID = "";
			if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
				lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}
		}
	}

	private void ClearField() {
		orderField = "";
		SysPrograms.SetupFromToDay(lstFromYYYY,lstFromMM,lstFromDD,lstToYYYY,lstToMM,lstToDD,true);
		lstFromYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		lstToYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		lstFromMM.SelectedValue = DateTime.Now.ToString("MM");
		lstToMM.SelectedValue = DateTime.Now.ToString("MM");
		lstFromDD.SelectedValue = DateTime.Now.ToString("dd");
		lstToDD.SelectedValue = DateTime.Now.ToString("dd");
		lstSexCd.SelectedIndex = 0;
		grdPerformance.Visible = false;
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		grdPerformance.Sort("RX_MAIL_COUNT",SortDirection.Descending);
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	private void GetList() {
		grdPerformance.Visible = true;
		grdPerformance.DataBind();
	}

	protected string GetFromDay() {
		return lstFromYYYY.SelectedValue + lstFromMM.SelectedValue + lstFromDD.SelectedValue;
	}
	protected string GetToDay() {
		return lstToYYYY.SelectedValue + lstToMM.SelectedValue + lstToDD.SelectedValue;
	}

	protected void dsSiteUserPerformance_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = lstFromYYYY.SelectedValue + "/" + lstFromMM.SelectedValue + "/" + lstFromDD.SelectedValue;
		e.InputParameters[2] = lstToYYYY.SelectedValue + "/" + lstToMM.SelectedValue + "/" + lstToDD.SelectedValue;
		e.InputParameters[3] = lstSexCd.SelectedValue;
		e.InputParameters[4] = orderField;
	}

	protected void grdPerformance_Sorting(object sender,GridViewSortEventArgs e) {
		e.SortDirection = SortDirection.Descending;
		orderField = "T1." + e.SortExpression + (e.SortDirection.Equals(SortDirection.Ascending) ? " ASC" : " DESC") + ",T1.SITE_CD,T1.USER_SEQ,T1.USER_CHAR_NO";
	}

	protected string GetViewUrl(object pSexCd,object pSiteCd,object pLoginId) {
		if (pSexCd.Equals(ViCommConst.MAN)) {
			return string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",pSiteCd.ToString(),pLoginId.ToString());
		} else {
			return string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}",pLoginId.ToString(),"UserPerformanceInquiry.aspx");
		}
	}

	protected void grdPerformance_DataBound(object sender, EventArgs e) {
		if (ViCommConst.MAN.Equals(this.lstSexCd.SelectedValue)) {
			this.grdPerformance.Columns[4].Visible = false;
		} else {
			this.grdPerformance.Columns[4].Visible = true;
		}
	}
}
