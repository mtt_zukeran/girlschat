﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 掲示板記録
--	Progaram ID		: BbsInquiry
--
--  Creation Date	: 2010.05.19
--  Creater			: Nakano
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using iBridCommLib;
using ViComm;

public partial class Status_BbsInquiry:Page {
	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		Response.Filter = new HtmlFilter(Response.Filter, ViCommConst.CARRIER_OTHERS);
		if (!IsPostBack) {
			FisrtLoad();
			InitPage();

			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(this.Request.QueryString["sex"]))) {
				if (iBridUtil.GetStringValue(this.Request.QueryString["sex"]).Equals(ViCommConst.MAN)) {
					this.rdoSexMan.Checked = true;
					this.rdoSexCast.Checked = false;
				} else {
					this.rdoSexMan.Checked = false;
					this.rdoSexCast.Checked = true;
				}
				this.GetList();
			}
		}
	}

	private void FisrtLoad() {
		grdBbs.PageSize = int.Parse(Session["PageSize"].ToString());
		grdBbs.DataSourceID = "";
		DataBind();
		if (Session["SiteCd"].ToString().Equals("")) {
			lstSiteCd.Items.Insert(0,new ListItem("",""));
		}
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
		lstSiteCd.DataSourceID = "";
		this.rdoSexMan.Text = DisplayWordUtil.Replace(this.rdoSexMan.Text);
		this.rdoSexCast.Text = DisplayWordUtil.Replace(this.rdoSexCast.Text);
	}

	private void InitPage() {
		pnlInfo.Visible = false;
		ClearField();
	}

	private void ClearField() {
		txtTxLoginId.Text = "";
		txtReportDayFrom.Text = DateTime.Now.ToString("yyyy/MM/dd");
		txtReportTimeFrom.Text = "";
		txtReportDayTo.Text = "";
		txtReportTimeTo.Text = "";
		rdoSexMan.Checked = true;
		rdoSexCast.Checked = false;
	}

	private void GetList() {
		if (txtReportDayTo.Text.Equals("")) {
			txtReportDayTo.Text = txtReportDayFrom.Text;
		}
		grdBbs.DataSourceID = "dsBbsLog";
		DataBind();
		pnlInfo.Visible = true;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		grdBbs.PageIndex = 0;
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void lnkDelBbs_Command(object sender,CommandEventArgs e) {
		string[] arguments = e.CommandArgument.ToString().Split(',');

		string sBbsSeq = arguments[0];
		string sexCd = arguments[1];
		int iDelFlag;
		int.TryParse(arguments[2],out iDelFlag);
		using (BbsLog oBbsLog = new BbsLog()) {
			oBbsLog.DeleteBbs(sBbsSeq,sexCd,iDelFlag);
		}
		DataBind();
	}

	protected void dsBbs_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;			// サイトコード 
		e.InputParameters[1] = GetSexCdInput();					// 性別 
		e.InputParameters[2] = txtReportDayFrom.Text;			// 書込日付FROM 
		e.InputParameters[3] = txtReportTimeFrom.Text;			// 書込日付TO 
		if (txtReportDayTo.Text.Equals("")) {
			txtReportDayTo.Text = txtReportDayFrom.Text;
		}
		e.InputParameters[4] = txtReportDayTo.Text;				// 書込時間FROM 
		e.InputParameters[5] = txtReportTimeTo.Text;			// 書込時間TO 
		e.InputParameters[6] = txtTxLoginId.Text.TrimEnd();		// 書込ログインID 
		e.InputParameters[7] = txtTxUserCharNo.Text;			// 書込キャラクター番号 
		e.InputParameters[8] = txtKeyword.Text.Trim();			// キーワード
	}

	private string GetSexCdInput() {
		if (rdoSexMan.Checked) {
			return ViCommConst.MAN;
		}
		if (rdoSexCast.Checked) {
			return ViCommConst.OPERATOR;
		}
		return "";
	}

	protected void dsBbs_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}

	protected string GetSexCd(object sexCd) {
		if (sexCd == null) {
			return "";
		}

		string sSexCd = sexCd.ToString();

		if (sSexCd == ViCommConst.MAN) {
			return DisplayWordUtil.Replace("男性");
		}
		if (sSexCd == ViCommConst.OPERATOR) {
			return DisplayWordUtil.Replace("出演者");
		}
		return "";
	}

	protected string GetViewUrl(object pSexCd,object pSiteCd,object pLoignId) {
		if (pSexCd.Equals(ViCommConst.MAN)) {
			return string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}",pSiteCd,pLoignId);
		} else {
			return string.Format("~/Cast/CastView.aspx?loginid={0}&return={1}",pLoignId,"BbsInquiry.aspx");
		}
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected string GetDelFlag(object pDelFlag,object pAdminDelFlag) {
		if (pAdminDelFlag.ToString().Equals(ViCommConst.FLAG_ON_STR)) {
			return "管理者削除";
		} else if (pDelFlag.ToString().Equals(ViCommConst.FLAG_ON_STR)) {
			return "ﾕｰｻﾞｰ削除";
		} else {
			return string.Empty;
		}
	}
	protected void grdBbs_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			if (DataBinder.Eval(e.Row.DataItem,"DEL_FLAG").ToString().Equals(ViCommConst.FLAG_ON_STR)) {
				e.Row.ForeColor = Color.Gray;
			} else if (DataBinder.Eval(e.Row.DataItem,"ADMIN_CHECK_FLAG").ToString().Equals(ViCommConst.FLAG_OFF_STR)) {
				e.Row.BackColor = Color.LavenderBlush;
			} else {
				e.Row.BackColor = Color.White;
			}
		}
	}

	protected void btnAdminCheck_Click(object sender,EventArgs e) {
		List<string> oBbsSeqList = new List<string>();

		for (int i = 0;i < this.grdBbs.Rows.Count;i++) {
			GridViewRow row = this.grdBbs.Rows[i];
			CheckBox oChk = (CheckBox)row.FindControl("chkAdminCheck");

			if (oChk.Checked) {
				HiddenField hdnBbsSeq = row.FindControl("hdnBbsSeq") as HiddenField;
				oBbsSeqList.Add(hdnBbsSeq.Value);
			}
		}

		if (oBbsSeqList.Count > 0) {
			using (DbSession oDbSession = new DbSession()) {
				oDbSession.PrepareProcedure("ADMIN_CHECK_BBS");
				oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.lstSiteCd.SelectedValue);
				oDbSession.ProcedureInArrayParm("pBBS_SEQ",DbSession.DbType.VARCHAR2,oBbsSeqList.ToArray());
				oDbSession.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,rdoSexMan.Checked ? ViCommConst.MAN : ViCommConst.OPERATOR);
				oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
				oDbSession.ExecuteProcedure();
			}

			lstSiteCd.DataSourceID = "";
			DataBind();
		}
	}

	protected void btnAdminCheckAll_Click(object sender,EventArgs e) {
		List<string> oBbsSeqList = new List<string>();

		for (int i = 0;i < this.grdBbs.Rows.Count;i++) {
			GridViewRow row = this.grdBbs.Rows[i];
			CheckBox oChk = (CheckBox)row.FindControl("chkAdminCheck");
			HiddenField hdnBbsSeq = row.FindControl("hdnBbsSeq") as HiddenField;
			oBbsSeqList.Add(hdnBbsSeq.Value);
		}

		if (oBbsSeqList.Count > 0) {
			using (DbSession oDbSession = new DbSession()) {
				oDbSession.PrepareProcedure("ADMIN_CHECK_BBS");
				oDbSession.ProcedureInParm("pSITE_CD",DbSession.DbType.VARCHAR2,this.lstSiteCd.SelectedValue);
				oDbSession.ProcedureInArrayParm("pBBS_SEQ",DbSession.DbType.VARCHAR2,oBbsSeqList.ToArray());
				oDbSession.ProcedureInParm("pSEX_CD",DbSession.DbType.VARCHAR2,rdoSexMan.Checked ? ViCommConst.MAN : ViCommConst.OPERATOR);
				oDbSession.ProcedureOutParm("pSTATUS",DbSession.DbType.VARCHAR2);
				oDbSession.ExecuteProcedure();
			}

			lstSiteCd.DataSourceID = "";
			DataBind();
		}
	}
}
