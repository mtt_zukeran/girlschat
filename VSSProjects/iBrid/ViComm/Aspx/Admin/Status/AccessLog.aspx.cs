﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: アクセスログ
--	Progaram ID		: AccessLog
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Status_AccessLog:System.Web.UI.Page {

	private int topPageCnt;
	private int userTopPageCnt;
	private int userTopPageCntUnique;
	private int loginSuccessCnt;
	private int loginSuccessCntUnique;
	private int castPageCnt;
	private int castPageCntUnique;
	private int registUserCnt;
	private int registUserReportCnt;
	private int reRegistCnt;
	private int withdrawalCnt;
	private int firstReceiptCnt;
	private int receiptCountUnique;
	private int receiptPointAfCountUnique;
	private string unit;

	private int colPrintDayIndex = 0;
	private int colAccessDayOfWeekIndex = 1;
	private int colRegistUserIndex = 2;
	private int colRegistUserReportIndex = 3;
	private int colWithdrawalIndex = 4;
	private int colReRegistIndex = 5;
	private int colTopPageIndex = 6;
	private int colLoginSuccessIndex = 7;
	private int colLoginSuccessUniqueIndex = 8;
	private int colCastPageIndex = 9;
	private int colCastPageUniqueIndex = 10;
	private int colFirstReceiptCountIndex = 11;
	private int colReceiptCountUniqueIndex = 12;
	private int colReceiptPointAfCountUniqueIndex = 13;

	protected void Page_Load(object sender,EventArgs e) {
		this.grdAccess.Columns[this.colLoginSuccessIndex].Visible = ManageCompany.IsAvailableUniqueAccessSummary();
		this.grdAccess.Columns[this.colLoginSuccessUniqueIndex].Visible = ManageCompany.IsAvailableUniqueAccessSummary();
		this.grdAccess.Columns[this.colCastPageUniqueIndex].Visible = ManageCompany.IsAvailableUniqueAccessSummary();

		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);
		DisplayWordUtil.ReplaceGridColumnHeader(this.grdAccess);


		grdAccess.DataSourceID = string.Empty;
		ClearField();
		DataBind();

		if (!IsPostBack) {
			unit = iBridUtil.GetStringValue(Request.QueryString["unit"]);
			ViewState["SEX_CD"] = iBridUtil.GetStringValue(Request.QueryString["sexcd"]);

			SysPrograms.SetupYear(lstYYYY);

			if (unit.Equals("year")) {
				lstMM.SelectedValue = string.Empty;
				lstMM.Visible = false;
				if (ViewState["SEX_CD"].ToString().Equals(ViCommConst.MAN)) {
					lblPgmTitle.Text = DisplayWordUtil.Replace("男性アクセスログ(月別)");
					lblAccessUnit.Text = DisplayWordUtil.Replace("男性アクセス年");
				} else {
					lblPgmTitle.Text = DisplayWordUtil.Replace("出演者アクセスログ(月別)");
					lblAccessUnit.Text = DisplayWordUtil.Replace("出演者アクセス年");
				}
				lblMonth.Visible = false;
			} else {
				if (ViewState["SEX_CD"].ToString().Equals(ViCommConst.MAN)) {
					lblPgmTitle.Text = DisplayWordUtil.Replace("男性アクセスログ(日別)");
					lblAccessUnit.Text = DisplayWordUtil.Replace("男性アクセス年月");
				} else {
					lblPgmTitle.Text = DisplayWordUtil.Replace("出演者アクセスログ(日別)");
					lblAccessUnit.Text = DisplayWordUtil.Replace("出演者アクセス年月");
				}
				lstMM.SelectedValue = DateTime.Now.ToString("MM");
			}
			if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals(string.Empty)) {
				lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}

			lstSiteCd.DataSourceID = string.Empty;
			GetList();
		}
	}

	private void ClearField() {
		topPageCnt = 0;
		userTopPageCnt = 0;
		userTopPageCntUnique = 0;
		loginSuccessCnt = 0;
		loginSuccessCntUnique = 0;
		castPageCnt = 0;
		castPageCntUnique = 0;
		registUserCnt = 0;
		registUserReportCnt = 0;
		reRegistCnt = 0;
		withdrawalCnt = 0;
		firstReceiptCnt = 0;
		receiptCountUnique = 0;
		receiptPointAfCountUnique = 0;
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void grdAccess_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			topPageCnt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TOP_PAGE"));
			userTopPageCnt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"USER_TOP_PAGE"));
			userTopPageCntUnique += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"USER_TOP_PAGE_UNIQUE"));
			loginSuccessCnt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"LOGIN_SUCCESS"));
			loginSuccessCntUnique += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"LOGIN_SUCCESS_UNIQUE"));
			castPageCnt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PAGE"));
			castPageCntUnique += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PAGE_UNIQUE"));
			registUserCnt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"REGIST_USER"));
			registUserReportCnt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"REGIST_REPORT_USER"));
			withdrawalCnt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"WITHDRAWAL_COUNT"));
			reRegistCnt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"RE_REGIST_COUNT"));
			firstReceiptCnt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"FIRST_RECEIPT_COUNT"));
			receiptCountUnique += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"RECEIPT_COUNT_UNIQUE"));
			receiptPointAfCountUnique += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"RECEIPT_POINT_AF_COUNT_UNIQUE"));

		} else if (e.Row.RowType == DataControlRowType.Footer) {
			e.Row.Cells[this.colTopPageIndex].Text = topPageCnt.ToString();
			e.Row.Cells[this.colLoginSuccessIndex].Text = loginSuccessCnt.ToString();
			e.Row.Cells[this.colLoginSuccessUniqueIndex].Text = loginSuccessCntUnique.ToString();
			e.Row.Cells[this.colCastPageIndex].Text = castPageCnt.ToString();
			e.Row.Cells[this.colCastPageUniqueIndex].Text = castPageCntUnique.ToString();
			e.Row.Cells[this.colRegistUserIndex].Text = registUserCnt.ToString();
			e.Row.Cells[this.colRegistUserReportIndex].Text = registUserReportCnt.ToString();
			e.Row.Cells[this.colWithdrawalIndex].Text = withdrawalCnt.ToString();
			e.Row.Cells[this.colReRegistIndex].Text = reRegistCnt.ToString();
			e.Row.Cells[this.colFirstReceiptCountIndex].Text = firstReceiptCnt.ToString();
			e.Row.Cells[this.colReceiptCountUniqueIndex].Text = receiptCountUnique.ToString();
			e.Row.Cells[this.colReceiptPointAfCountUniqueIndex].Text = receiptPointAfCountUnique.ToString();
			e.Row.Font.Bold = true;
		}
	}

	private void GetList() {
		topPageCnt = 0;
		castPageCnt = 0;

		grdAccess.Columns[this.colFirstReceiptCountIndex].Visible = false;
		grdAccess.Columns[this.colReceiptCountUniqueIndex].Visible = false;
		grdAccess.Columns[this.colReceiptPointAfCountUniqueIndex].Visible = false;

		if (lstMM.SelectedValue.Equals(string.Empty)) {
			grdAccess.Columns[this.colPrintDayIndex].Visible = false;
			grdAccess.Columns[this.colAccessDayOfWeekIndex].HeaderText = "年月";
			if (ViewState["SEX_CD"].ToString().Equals(ViCommConst.MAN)) {
				//lblPgmTitle.Text = "男性アクセスログ(月別)";
				grdAccess.Columns[this.colCastPageIndex].Visible &= true;
				grdAccess.Columns[this.colCastPageUniqueIndex].Visible &= true;
				grdAccess.Columns[this.colFirstReceiptCountIndex].Visible = true;
				grdAccess.Columns[this.colReceiptCountUniqueIndex].Visible = true;
				grdAccess.Columns[this.colReceiptPointAfCountUniqueIndex].Visible = true;
			} else {
				//lblPgmTitle.Text = "出演者アクセスログ(月別)";
				grdAccess.Columns[this.colCastPageIndex].Visible = false;
				grdAccess.Columns[this.colCastPageUniqueIndex].Visible = false;
			}
			lblMonth.Visible = false;
		} else {
			grdAccess.Columns[this.colPrintDayIndex].Visible &= true;
			grdAccess.Columns[this.colAccessDayOfWeekIndex].HeaderText = "曜日";
			if (ViewState["SEX_CD"].ToString().Equals(ViCommConst.MAN)) {
				//lblPgmTitle.Text = "男性アクセスログ(日別)";
				grdAccess.Columns[this.colCastPageIndex].Visible &= true;
				grdAccess.Columns[this.colCastPageUniqueIndex].Visible &= true;
				grdAccess.Columns[this.colFirstReceiptCountIndex].Visible = true;
				grdAccess.Columns[this.colReceiptCountUniqueIndex].Visible = true;
				grdAccess.Columns[this.colReceiptPointAfCountUniqueIndex].Visible = true;
			} else {
				//lblPgmTitle.Text = "出演者アクセスログ(日別)";
				grdAccess.Columns[this.colCastPageIndex].Visible = false;
				grdAccess.Columns[this.colCastPageUniqueIndex].Visible = false;
			}
		}

		grdAccess.PageIndex = 0;
		grdAccess.DataSourceID = "dsAccess";
		DataBind();
	}

	protected Color GetBackColor(object pDayOfWeek) {
		if (pDayOfWeek.ToString().Equals("Sat")) {
			return Color.FromArgb(0xCCCCFF);
		} else {
			if (pDayOfWeek.ToString().Equals("Sun")) {
				return Color.FromArgb(0xFFCCCC);
			} else {
				return Color.Empty;
			}
		}
	}
	
	protected bool GetColumnVisible(string pUnit) {
		return iBridUtil.GetStringValue(this.Request.QueryString["unit"]).Equals(pUnit);
	}


}
