﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="HourlyPerformance.aspx.cs" Inherits="Status_HourlyPerformance"
	Title="時間別稼動集計" ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="時間別稼動集計"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							<asp:Label ID="lblAccessUnit" runat="server" Text="表示日"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstYear" runat="server" Width="60px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstMonth" runat="server" Width="40px">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstDay" runat="server" Width="40px">
							</asp:DropDownList>日
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							<asp:Label ID="Label9" runat="server" Text="表示明細"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:RadioButton ID="rdoTel" runat="server" Checked="True" GroupName="DisplayType" Text="電話利用明細">
							</asp:RadioButton>
							<asp:RadioButton ID="rdoWeb" runat="server" GroupName="DisplayType" Text="WEB利用明細">
							</asp:RadioButton>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							<asp:Label ID="Label14" runat="server" Text="表示項目"></asp:Label>
						</td>
						<td class="tdDataStyle">
							上から順番に
							<asp:CheckBox ID="chkMin" runat="server" Text="分数" Checked="true">
							</asp:CheckBox>
							<asp:CheckBox ID="chkCount" runat="server" Text="件数" Checked="true">
							</asp:CheckBox>
							<asp:CheckBox ID="chkPt" runat="server" Text="稼動Pt" Checked="true">
							</asp:CheckBox>
							を表示する。
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
				<asp:Button runat="server" ID="btnCsv" Text="ＣＳＶ出力" CssClass="seekbutton" OnClick="btnCSV_Click" />
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[稼動集計]</legend>
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="Auto" Height="430px" Width="100%">
					<asp:GridView ID="grdDailyPerformace" runat="server" AutoGenerateColumns="False" DataSourceID="dsDailyPerformace" ShowFooter="True" AllowSorting="True"
						SkinID="GridViewColor" OnRowDataBound="grdDailyPerformace_RowDataBound" Font-Size="X-Small" OnRowCreated="grdDailyPerformace_RowCreated" EnableViewState="false">
						<Columns>
							<asp:TemplateField HeaderText="時" FooterText="合計">
								<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
								<ItemTemplate>
									<asp:Label ID="Label2" runat="server" Text='<%# Eval("REPORT_HOUR") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Center" />
								<FooterTemplate>
									<asp:Label ID="lblTotalMinNm" runat="server" Text="分数" Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label29" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblTotalCountNm" runat="server" Text="回数" Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblTotalPointNm" runat="server" Text="ポイント" Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="TV">
								<ItemTemplate>
									<asp:Label ID="Label4" runat="server" Text='<%# Eval("PRV_TV_TALK_MIN", "{0}") %>' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label30" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label11" runat="server" Text='<%# Eval("PRV_TV_TALK_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label12" runat="server" Text='<%# Eval("PRV_TV_TALK_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblPrvTvTalkMin" runat="server" Text='' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label17" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblPrvTvTalkCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label18" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblPrvTvTalkPoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="TVﾁｬｯﾄ">
								<ItemTemplate>
									<asp:Label ID="Label4" runat="server" Text='<%# Eval("PUB_TV_TALK_MIN", "{0}") %>' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label17" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("PUB_TV_TALK_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label5" runat="server" Text='<%# Eval("PUB_TV_TALK_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblPubTvTalkMin" runat="server" Text='' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label31" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblPubTvTalkCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label86" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblPubTvTalkPoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="音声">
								<ItemTemplate>
									<asp:Label ID="Label4" runat="server" Text='<%# Eval("PRV_VOICE_TALK_MIN", "{0}") %>' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("PRV_VOICE_TALK_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label6" runat="server" Text='<%# Eval("PRV_VOICE_TALK_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblPrvVoiceTalkMin" runat="server" Text='' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label32" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblPrvVoiceTalkCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label87" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblPrvVoiceTalkPoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="音声ﾁｬｯﾄ">
								<ItemTemplate>
									<asp:Label ID="Label4" runat="server" Text='<%# Eval("PUB_VOICE_TALK_MIN", "{0}") %>' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("PUB_VOICE_TALK_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label6" runat="server" Text='<%# Eval("PUB_VOICE_TALK_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblPubVoiceTalkMin" runat="server" Text='' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label33" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblPubVoiceTalkCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label88" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblPubVoiceTalkPoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="会話ﾓﾆﾀ">
								<ItemTemplate>
									<asp:Label ID="Label4" runat="server" Text='<%# Eval("VIEW_TALK_MIN", "{0}") %>' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("VIEW_TALK_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label7" runat="server" Text='<%# Eval("VIEW_TALK_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblViewTalkMin" runat="server" Text='' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label34" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblViewTalkCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label89" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblViewTalkPoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="音声ﾓﾆﾀ">
								<ItemTemplate>
									<asp:Label ID="Label4" runat="server" Text='<%# Eval("WIRETAP_MIN", "{0}") %>' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("WIRETAP_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label8" runat="server" Text='<%# Eval("WIRETAP_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblWiretapMin" runat="server" Text='' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label35" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblWiretapCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label90" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblWiretapPoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="部屋ﾓﾆﾀ">
								<ItemTemplate>
									<asp:Label ID="Label4" runat="server" Text='<%# Eval("VIEW_BROADCAST_MIN", "{0}") %>' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("VIEW_BROADCAST_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label8" runat="server" Text='<%# Eval("VIEW_BROADCAST_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblViewBroadcastMin" runat="server" Text='' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label36" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblViewBroadcastCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label91" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblViewBroadcastPoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾗｲﾌﾞ視聴">
								<ItemTemplate>
									<asp:Label ID="Label4" runat="server" Text='<%# Eval("LIVE_MIN", "{0}") %>' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("LIVE_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label8" runat="server" Text='<%# Eval("LIVE_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblLiveMin" runat="server" Text='' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label37" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblLiveCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label92" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblLivePoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="PF再生">
								<ItemTemplate>
									<asp:Label ID="Label4" runat="server" Text='<%# Eval("PLAY_PROFILE_MIN", "{0}") %>' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("PLAY_PROFILE_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label8" runat="server" Text='<%# Eval("PLAY_PROFILE_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblPlayProfileMin" runat="server" Text='' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label38" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblPlayProfileCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label93" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblPlayProfilePoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="PF録音">
								<ItemTemplate>
									<asp:Label ID="Label4" runat="server" Text='<%# Eval("REC_PROFILE_MIN", "{0}") %>' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("REC_PROFILE_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label8" runat="server" Text='<%# Eval("REC_PROFILE_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblRecProfileMin" runat="server" Text='' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label39" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblRecProfileCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label94" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblRecProfilePoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="逆PF会話">
								<ItemTemplate>
									<asp:Label ID="Label4" runat="server" Text='<%# Eval("GPF_TALK_VOICE_MIN", "{0}") %>' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("GPF_TALK_VOICE_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label8" runat="server" Text='<%# Eval("GPF_TALK_VOICE_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblGpfTalkVoiceMin" runat="server" Text='' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label40" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblGpfTalkVoiceCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label95" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblGpfTalkVoicePoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾌﾟﾗｲﾍﾞｰﾄ<br/>伝言再生">
								<ItemTemplate>
									<asp:Label ID="Label4" runat="server" Text='<%# Eval("PLAY_PV_MSG_MIN", "{0}") %>' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("PLAY_PV_MSG_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label8" runat="server" Text='<%# Eval("PLAY_PV_MSG_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblPlayPvMsgMin" runat="server" Text='' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label41" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblPlayPvMsgCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label96" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblPlayPvMsgPoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="待機TV">
								<ItemTemplate>
									<asp:Label ID="Label4" runat="server" Text='<%# Eval("CAST_PRV_TV_TALK_MIN", "{0}") %>' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("CAST_PRV_TV_TALK_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label8" runat="server" Text='<%# Eval("CAST_PRV_TV_TALK_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblCastPrvTvTalkMin" runat="server" Text='' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label42" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblCastPrvTvTalkCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label97" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblCastPrvTvTalkPoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="待機TV<br/>ﾁｬｯﾄ">
								<ItemTemplate>
									<asp:Label ID="Label4" runat="server" Text='<%# Eval("CAST_PUB_TV_TALK_MIN", "{0}") %>' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("CAST_PUB_TV_TALK_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label8" runat="server" Text='<%# Eval("CAST_PUB_TV_TALK_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblCastPubTvTalkMin" runat="server" Text='' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label43" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblCastPubTvTalkCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label98" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblCastPubTvTalkPoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="待機音声">
								<ItemTemplate>
									<asp:Label ID="Label4" runat="server" Text='<%# Eval("CAST_PRV_VOICE_TALK_MIN", "{0}") %>' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("CAST_PRV_VOICE_TALK_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label8" runat="server" Text='<%# Eval("CAST_PRV_VOICE_TALK_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblCastPrvVoiceTalkMin" runat="server" Text='' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label44" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblCastPrvVoiceTalkCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label99" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblCastPrvVoiceTalkPoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="待機音声<br/>ﾁｬｯﾄ">
								<ItemTemplate>
									<asp:Label ID="Label4" runat="server" Text='<%# Eval("CAST_PUB_VOICE_TALK_MIN", "{0}") %>' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("CAST_PUB_VOICE_TALK_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label8" runat="server" Text='<%# Eval("CAST_PUB_VOICE_TALK_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblCastPubVoiceTalkMin" runat="server" Text='' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label45" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblCastPubVoiceTalkCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label100" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblCastPubVoiceTalkPoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="電話計">
								<ItemTemplate>
									<asp:Label ID="Label4" runat="server" Text='<%# Eval("TALK_MIN", "{0}") %>' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("TALK_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("TALK_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" BackColor="#F5F5DC" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" BackColor="#F5F5DC" />
								<FooterTemplate>
									<asp:Label ID="lblTalkMin" runat="server" Text='' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label46" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblTalkCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label101" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblTalkPoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾒｰﾙ送信">
								<ItemTemplate>
									<asp:Label ID="Label4" runat="server" Text='' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("USER_MAIL_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("USER_MAIL_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblMLMin" runat="server" Text='' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label47" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblMLCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label102" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblMLPoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="写真添付<br/>ﾒｰﾙ送信">
								<ItemTemplate>
									<asp:Label ID="Label4" runat="server" Text='' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("USER_MAIL_PIC_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("USER_MAIL_PIC_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblMLPicMin" runat="server" Text='' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label48" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblMLPicCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label103" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblMLPicPoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="動画添付<br/>ﾒｰﾙ送信">
								<ItemTemplate>
									<asp:Label ID="Label4" runat="server" Text='' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("USER_MAIL_MOVIE_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("USER_MAIL_MOVIE_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblMLMovieMin" runat="server" Text='' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label49" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblMLMovieCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label104" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblMLMoviePoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾒｰﾙ添付<br/>写真閲覧">
								<ItemTemplate>
									<asp:Label ID="Label4" runat="server" Text='' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("OPEN_PIC_MAIL_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("OPEN_PIC_MAIL_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="Label50" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblOpenPicMailCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label105" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblOpenPicMailPoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾒｰﾙ添付<br/>動画閲覧">
								<ItemTemplate>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("OPEN_MOVIE_MAIL_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("OPEN_MOVIE_MAIL_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="Label51" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblOpenMovieMailCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label106" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblOpenMovieMailPoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="掲示板<br/>写真">
								<ItemTemplate>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("OPEN_PIC_BBS_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("OPEN_PIC_BBS_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="Label52" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblOpenPicBbsCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label107" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblOpenPicBbsPoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="掲示板<br/>動画">
								<ItemTemplate>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("OPEN_MOVIE_BBS_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("OPEN_MOVIE_BBS_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="Label53" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblOpenMovieBbsCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label108" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblOpenMovieBbsPoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="掲示板<br/>書込">
								<ItemTemplate>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("WRITE_BBS_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("WRITE_BBS_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="Label54" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblWriteBbsCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label109" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblWriteBbsPoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾀﾞｳﾝﾛｰﾄﾞ<br/>ﾑｰﾋﾞｰ">
								<ItemTemplate>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("DOWNLOAD_MOVIE_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("DOWNLOAD_MOVIE_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="Label55" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblDownloadMovieCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label110" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblDownloadMoviePoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="定時<br/>ｺｰﾙ">
								<ItemTemplate>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("TIME_CALL_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("TIME_CALL_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="Label56" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblTimeCallCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label111" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblTimeCallPoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾀﾞｳﾝﾛｰﾄﾞ<br/>着ﾎﾞｲｽ">
								<ItemTemplate>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("DOWNLOAD_VOICE_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("DOWNLOAD_VOICE_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="Label57" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblDownloadVoiceCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label112" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblDownloadVoicePoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ｿｰｼｬﾙ<br/>ｹﾞｰﾑ">
								<ItemTemplate>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("SOCIAL_GAME_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("SOCIAL_GAME_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblSocialGameCountBr" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblSocialGameCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="lblSocialGamePointBr" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblSocialGamePoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="野球拳<br />FC会費">
								<ItemTemplate>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("YAKYUKEN_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("YAKYUKEN_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblYakyukenCountBr" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblYakyukenCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="lblYakyukenPointBr" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblYakyukenPoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾌﾟﾚｾﾞﾝﾄ<br />ﾒｰﾙ">
								<ItemTemplate>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("PRESENT_MAIL_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("PRESENT_MAIL_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblPresentMailCountBr" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblPresentMailCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="lblPresentMailPointBr" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblPresentMailPoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="WEB計">
								<ItemTemplate>
									<asp:Label ID="Label4" runat="server" Text='' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("WEB_TOTAL_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("WEB_TOTAL_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" BackColor="#F5F5DC" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" BackColor="#F5F5DC" />
								<FooterTemplate>
									<asp:Label ID="Label58" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblWebTotalCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label113" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblWebTotalPoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="合計">
								<ItemTemplate>
									<asp:Label ID="Label4" runat="server" Text='' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label13" runat="server" Text='<%# CalcSum(Eval("TALK_COUNT"),Eval("WEB_TOTAL_COUNT")) %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# CalcSum(Eval("TALK_POINT"),Eval("WEB_TOTAL_POINT")) %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" BackColor="#FFEBCD" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" BackColor="#FFEBCD" />
								<FooterTemplate>
									<asp:Label ID="Label59" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblTotalCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label114" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblTotalPoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="FREE<br/>ﾀﾞｲｱﾙ計">
								<ItemTemplate>
									<asp:Label ID="Label4" runat="server" Text='<%# Eval("FREE_DIAL_MIN", "{0}") %>' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label84" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="Label1" runat="server" Text='<%# Eval("FREE_DIAL_COUNT", "{0}") %>' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label15" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="Label10" runat="server" Text='<%# Eval("FREE_DIAL_POINT", "{0}") %>' Visible='<%# CheckedPt() %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblFreeDialMin" runat="server" Text='' Visible='<%# CheckedMin() %>'></asp:Label>
									<asp:Label ID="Label60" runat="server" Text='<br />' Visible='<%# GetBrMin() %>'></asp:Label>
									<asp:Label ID="lblFreeDialCount" runat="server" Text='' Visible='<%# CheckedCount() %>'></asp:Label>
									<asp:Label ID="Label115" runat="server" Text='<br />' Visible='<%# GetBrCount() %>'></asp:Label>
									<asp:Label ID="lblFreeDialPoint" runat="server" Text='' Visible='<%# CheckedPt() %>'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
						</Columns>
						<FooterStyle ForeColor="Black" BackColor="LightYellow" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsDailyPerformace" runat="server" SelectMethod="GetListHourly" TypeName="DailyPerformance">
		<SelectParameters>
			<asp:ControlParameter ControlID="lstSiteCd" Name="pSiteCd" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstYear" Name="pYear" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstMonth" Name="pMonth" PropertyName="SelectedValue" Type="String" />
			<asp:ControlParameter ControlID="lstDay" Name="pDay" PropertyName="SelectedValue" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
</asp:Content>
