<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UserPerformanceInquiry.aspx.cs" Inherits="Status_UserPerformanceInquiry"
	Title="ユーザー別稼動状況集計" ValidateRequest="false" %>

<%@ Import Namespace="ViComm" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="ユーザー別稼動状況集計"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							<asp:Label ID="lblAccessUnit" runat="server" Text="集計開始日"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstFromYYYY" runat="server" Width="60px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstFromMM" runat="server" Width="40px">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstFromDD" runat="server" Width="40px">
							</asp:DropDownList>日
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							<asp:Label ID="Label3" runat="server" Text="集計終了日"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstToYYYY" runat="server" Width="60px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstToMM" runat="server" Width="40px">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstToDD" runat="server" Width="40px">
							</asp:DropDownList>日
						</td>
						<td class="tdHeaderStyle2">
							<asp:Label ID="Label1" runat="server" Text="性別"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:ListBox ID="lstSexCd" runat="server" Rows="1">
								<asp:ListItem Value="1">男性</asp:ListItem>
								<asp:ListItem Value="3">女性</asp:ListItem>
							</asp:ListBox>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
			</asp:Panel>
		</fieldset>
		<fieldset>
			<legend>[稼動集計集計]</legend>
			<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto">
				<asp:GridView ID="grdPerformance" runat="server" AutoGenerateColumns="False" DataSourceID="dsSiteUserPerformance" AllowSorting="True" SkinID="GridViewColor"
					AllowPaging="True" PageSize="100" OnSorting="grdPerformance_Sorting" OnDataBound="grdPerformance_DataBound">
					<Columns>
						<asp:TemplateField HeaderText="会員ID">
							<ItemTemplate>
								<asp:HyperLink ID="lblTxLoginId" runat="server" NavigateUrl='<%# GetViewUrl(Eval("SEX_CD"),Eval("SITE_CD"),Eval("LOGIN_ID")) %>' Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ﾊﾝﾄﾞﾙ名">
							<ItemStyle HorizontalAlign="Left" Width="200px" />
							<ItemTemplate>
								<asp:Label runat="server" Text='<%# ViCommPrograms.DefHandleName(Eval("HANDLE_NM")) %>' ID="lblHandleNm" />
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ﾒｰﾙ&lt;BR&gt;受信数" SortExpression="RX_MAIL_COUNT">
							<ItemTemplate>
								<asp:Label ID="Label7" runat="server" Text='<%# Eval("RX_MAIL_COUNT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" Width="60px" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="ﾒｰﾙ&lt;BR&gt;送信数" SortExpression="TX_MAIL_COUNT">
							<ItemTemplate>
								<asp:Label ID="Label8" runat="server" Text='<%# Eval("TX_MAIL_COUNT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" Width="60px" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="一括ﾒｰﾙ&lt;BR&gt;送信数" SortExpression="TX_BATCH_MAIL_COUNT">
							<ItemTemplate>
								<asp:Label ID="Label8" runat="server" Text='<%# Eval("TX_BATCH_MAIL_COUNT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" Width="60px" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="会話数" SortExpression="TALK_COUNT">
							<ItemTemplate>
								<asp:Label ID="Label7" runat="server" Text='<%# Eval("TALK_COUNT") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" Width="60px" />
						</asp:TemplateField>
						<asp:TemplateField HeaderText="会話分数" SortExpression="TALK_MIN">
							<ItemTemplate>
								<asp:Label ID="Label8" runat="server" Text='<%# Eval("TALK_MIN") %>'></asp:Label>
							</ItemTemplate>
							<ItemStyle HorizontalAlign="Right" Width="60px" />
						</asp:TemplateField>
					</Columns>
				</asp:GridView>
			</asp:Panel>
		</fieldset>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsSiteUserPerformance" runat="server" SelectMethod="GetPageCollection" TypeName="SiteUserPerformance" SelectCountMethod="GetPageCount"
		OnSelecting="dsSiteUserPerformance_Selecting" EnablePaging="True">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" Type="String" />
			<asp:Parameter Name="pReportDayFrom" Type="String" />
			<asp:Parameter Name="pReportDayTo" Type="String" />
			<asp:Parameter Name="pSexCd" Type="String" />
			<asp:Parameter Name="pOrder" Type="String" />
		</SelectParameters>
	</asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
</asp:Content>
