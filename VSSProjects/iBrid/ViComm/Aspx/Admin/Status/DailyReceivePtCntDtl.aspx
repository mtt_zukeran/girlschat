﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DailyReceivePtCntDtl.aspx.cs" Inherits="Status_DailyReceivePtCntDtl" Title="期間別出演者獲得報酬集計詳細" %>
<%@ Import namespace="ViComm" %>
<%@ Import Namespace="iBridCommLib" %>
<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" Runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="期間別出演者獲得報酬集計詳細"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" Runat="Server">
	<div class="admincontent">
		<asp:Label ID="lblDescription" runat="server" Text=""></asp:Label>
		<br />
		<asp:Button ID="btnHBack" runat="server" Text="戻る" CssClass="seekbutton" OnClientClick="history.back();return false;" />
		<br />
		<br />

		<%-- ============================== --%>
		<%--  Data                          --%>
		<%-- ============================== --%>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset class="fieldset">
				<legend>[集計結果詳細]</legend>
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="None">
					<asp:GridView ID="grdData" runat="server" ShowFooter="False" AllowSorting="True" AutoGenerateColumns="False"
						DataSourceID="dsDailyReceivePtCntDtl" OnRowDataBound="grdData_RowDataBound" SkinID="GridViewColor" Font-Size="X-Small" EnableViewState="false">
						<Columns>
							<asp:TemplateField HeaderText="出演者ID">
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
								<ItemTemplate>
                                    <asp:HyperLink ID="lnkCastLoginId" runat="server" Text='<%# Eval("CAST_LOGIN_ID") %>'
										NavigateUrl='<%# GetCastLink(Eval("CAST_USER_SEQ")) %>'></asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="会員ID">
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
								<ItemTemplate>
                                    <asp:HyperLink ID="lnkManLoginId" runat="server" Text='<%# Eval("MAN_LOGIN_ID") %>'
										NavigateUrl='<%# GetManLink(Eval("SITE_CD"),Eval("MAN_USER_SEQ")) %>'></asp:HyperLink>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="獲得pt">
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
								<ItemTemplate>
									<asp:Label ID="lblReceivePoint" runat="server" Text='<%# Eval("RECEIVE_POINT") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
		<asp:Button ID="btnFBack" runat="server" Text="戻る" CssClass="seekbutton" OnClientClick="history.back();return false;" />
		<br />
	</div>

	<%-- ============================== --%>
	<%--  Data Source                   --%>
	<%-- ============================== --%>
	<asp:ObjectDataSource ID="dsDailyReceivePtCntDtl" runat="server" TypeName="DailyReceivePtCnt" SelectCountMethod="GetPageCount" SelectMethod="GetPageCollectionDtl" EnablePaging="True"
		OnSelecting="dsDailyReceivePtCntDtl_Selecting">
		<SelectParameters>
			<asp:Parameter Name="pSearchCondition" Type="Object" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
