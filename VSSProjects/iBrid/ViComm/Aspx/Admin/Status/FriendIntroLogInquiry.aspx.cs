﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Progaram ID		: FriendIntroLogInquiry
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Status_FriendIntroLogInquiry:System.Web.UI.Page {

	private string recCount = "";

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			FisrtLoad();
			InitPage();
			if (!Request.QueryString.ToString().Equals("")) {
				GetList();
			}
		}
	}

	private void FisrtLoad() {
		grdFriendIntroLog.PageSize = int.Parse(Session["PageSize"].ToString());
		grdFriendIntroLog.DataSourceID = "";
		DataBind();
		lstSiteCd.DataSourceID = "";
	}

	private void InitPage() {
		pnlInfo.Visible = false;
		ClearField();
		if (!iBridUtil.GetStringValue(Request.QueryString["sitecd"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
		} else if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	private void ClearField() {
		txtLoginId.Text = "";
		txtCreateDayFrom.Text = DateTime.Now.ToString("yyyy/MM/dd");
		txtCreateDayTo.Text = DateTime.Now.ToString("yyyy/MM/dd");
	}

	protected string GetRecCount() {
		return recCount;
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		Response.Redirect(Request.Url.PathAndQuery);
	}

	protected string GetUserLink(object pSiteCd,object pLoginId,object pSexCd) {

		if (pSexCd.Equals(ViCommConst.OPERATOR)) {
			return string.Format("../Cast/CastView.aspx?loginid={0}&return=FriendIntroLogInquiry.aspx",pLoginId.ToString());
		} else {
			return string.Format("../Man/ManView.aspx?site={0}&manloginid={1}",pSiteCd.ToString(),pLoginId.ToString());
		}
	}

	protected string GetUserLinkByFriendIntroType(object pSiteCd,object pLoginId,object pSexCd,object pPartnerSiteCd,object pPartnerLoginId,object pPartnerSexCd,object pFriendIntroType) {
		if(pFriendIntroType.ToString().Equals(ViCommConst.FriendIntroType.UNDER_INTRODUCER)) {
			return GetUserLink(pPartnerSiteCd,pPartnerLoginId,pPartnerSexCd);
		}

		return GetUserLink(pSiteCd,pLoginId,pSexCd);
	}

	protected string GetUserLoginIdByFriendIntroType(object pLoginId,object pPartnerLoginId,object pFriendIntroType) {
	
		if(pFriendIntroType.ToString().Equals(ViCommConst.FriendIntroType.UNDER_INTRODUCER)) {
			return pPartnerLoginId.ToString();	
		}		
		return pLoginId.ToString();
	}
	
	private void GetList() {
		if (txtCreateDayTo.Text.Equals("")) {
			txtCreateDayTo.Text = txtCreateDayFrom.Text;
		}

		grdFriendIntroLog.PageSize = 50;
		grdFriendIntroLog.DataSourceID = "dsFriendIntroLog";
		DataBind();
		pnlInfo.Visible = true;
	}

	protected void dsFriendIntroLog_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = "";
		e.InputParameters[2] = "";
		e.InputParameters[3] = txtCreateDayFrom.Text;
		if (txtCreateDayTo.Text.Equals("")) {
			txtCreateDayTo.Text = txtCreateDayFrom.Text;
		}
		e.InputParameters[4] = txtCreateDayTo.Text;
		e.InputParameters[5] = "";
		e.InputParameters[6] = txtLoginId.Text;
	}

	protected void dsFriendIntroLog_Selected(object sender,ObjectDataSourceStatusEventArgs e) {
		if (e.ReturnValue != null) {
			recCount = e.ReturnValue.ToString();
		}
	}
}
