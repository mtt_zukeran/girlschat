﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: おみくじ履歴

--	Progaram ID		: OmikujiLog
--
--  Creation Date	: 2011.09.09
--  Creater			: iBrid
--
**************************************************************************/

// [ this.Update History ]
/*------------------------------------------------------------------------

  Date        this.Updater    this.Update Explain

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iBridCommLib;
using ViComm;
using System.Windows.Forms;


public partial class Status_OmikujiLog : System.Web.UI.Page
{
    protected static readonly string[] DayArray = new string[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" };
    protected static readonly string[] MonthArray = new string[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" };
    protected string[] YearArray;

    private string SexCd
    {
        get { return iBridUtil.GetStringValue(this.ViewState["SexCd"]); }
        set { this.ViewState["SexCd"] = value; }
    }

    private string recCount = "";

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            InitPage();
            FirstLoad();
            
        }
    }

    protected void InitPage() {
        this.ClearField();
    }
    private void FirstLoad() {
        this.SexCd = Request.QueryString["sexcd"];

        if (iBridUtil.GetStringValue(this.SexCd).Equals(ViCommConst.MAN)) {
            this.lblPgmTitle.Text = "男性" + lblPgmTitle.Text;
            this.Title = "男性" + this.Title;
        } else {
            this.lblPgmTitle.Text = "女性" + lblPgmTitle.Text;
            this.Title = "女性" + this.Title;
        }
        this.grdOmikujiLog.PageSize = 100;
        this.YearArray = new string[] { DateTime.Today.ToString("yyyy"), DateTime.Today.AddYears(-1).ToString("yyyy"), DateTime.Today.AddYears(-2).ToString("yyyy") };

        this.DataBind();
        if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
            this.lstSeekSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
        }
    }
    private void ClearField() {
        this.recCount = "0";
        this.pnlGrid.Visible = false;

		this.txtLoginId.Text = string.Empty;
        this.lstFromYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
        this.lstFromMM.SelectedValue = DateTime.Now.ToString("MM");
        this.lstFromDD.SelectedValue = DateTime.Now.ToString("dd");
        this.lstToYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
        this.lstToMM.SelectedValue = DateTime.Now.ToString("MM");
        this.lstToDD.SelectedValue = DateTime.Now.ToString("dd");

    }
    private void GetList() {
        this.grdOmikujiLog.PageIndex = 0;
        this.grdOmikujiLog.DataSourceID = "dsOmikujiLog";
        this.grdOmikujiLog.DataBind();
        this.pnlCount.DataBind();
        this.pnlGrid.Visible = true;
    }

    protected string GetRecCount() {
        return this.recCount;
    }
    #region === Event Methods ===
    protected void btnListSeek_Click(object sender, EventArgs e) {
        this.GetList();

    }
	protected void btnClear_Click(object sender,EventArgs e) {
        this.ClearField();
    }
    protected void dsOmikujiLog_Selected(object sender, ObjectDataSourceStatusEventArgs e) {
        if (e.ReturnValue != null) {
            this.recCount = e.ReturnValue.ToString();
        }
    }
    protected void dsOmikujiLog_Selecting(object sender, ObjectDataSourceSelectingEventArgs e) {
        e.InputParameters[0] = this.lstSeekSiteCd.SelectedValue;
        e.InputParameters[1] = this.SexCd;
		e.InputParameters[2] = this.txtLoginId.Text.Trim();
        e.InputParameters[3] = string.Format("{0}/{1}/{2}", this.lstFromYYYY.SelectedValue, this.lstFromMM.SelectedValue, this.lstFromDD.SelectedValue);
        e.InputParameters[4] = string.Format("{0}/{1}/{2}", this.lstToYYYY.SelectedValue, this.lstToMM.SelectedValue, this.lstToDD.SelectedValue);
    }
    protected void lnkLoginId_Command(object sender, CommandEventArgs e) {
        string[] sValues = e.CommandArgument.ToString().Split(',');
        if(this.SexCd.Equals(ViCommConst.MAN)){
            Server.Transfer(string.Format("~/Man/ManView.aspx?site={0}&manloginid={1}", sValues[0], sValues[1]));
        }else{
            Server.Transfer(string.Format("~/Cast/CastView.aspx?site={0}&loginid={1}", sValues[0], sValues[1]));
        }
    }
    #endregion
}
