﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: ポイント操作履歴(日別合計)
--	Progaram ID		: UserDefPointHistoryDailyList
--
--  Creation Date	: 2015.05.15
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.IO;
using iBridCommLib;
using ViComm;

public partial class Status_UserDefPointHistoryDailyList:System.Web.UI.Page {
	private string[] Week = { "日","月","火","水","木","金","土" };

	private double dAddPoint = 0;
	private Stream filter;

	private string SiteCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SITE_CD"]);
		}
		set {
			this.ViewState["SITE_CD"] = value;
		}
	}

	private string Year {
		get {
			return iBridUtil.GetStringValue(this.ViewState["YEAR"]);
		}
		set {
			this.ViewState["YEAR"] = value;
		}
	}

	private string Month {
		get {
			return iBridUtil.GetStringValue(this.ViewState["MONTH"]);
		}
		set {
			this.ViewState["MONTH"] = value;
		}
	}
	
	private string SexCd {
		get {
			return iBridUtil.GetStringValue(this.ViewState["SEX_CD"]);
		}
		set {
			this.ViewState["SEX_CD"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		this.Response.Filter = new HtmlFilter(Response.Filter,ViCommConst.CARRIER_OTHERS);

		if (!this.IsPostBack) {
			this.SiteCd = iBridUtil.GetStringValue(Request.QueryString["sitecd"]);
			this.Year = iBridUtil.GetStringValue(Request.QueryString["year"]);
			this.Month = iBridUtil.GetStringValue(Request.QueryString["month"]);
			this.SexCd = iBridUtil.GetStringValue(Request.QueryString["sexcd"]);

			if (string.IsNullOrEmpty(this.SexCd)) {
				this.SexCd = ViCommConst.MAN;
			}
			
			if (this.SexCd.Equals(ViCommConst.MAN)) {
				lblPgmTitle.Text = "男性会員" + lblPgmTitle.Text;
			} else {
				lblPgmTitle.Text = "出演者" + lblPgmTitle.Text;
			}

			this.CreateList();
			this.InitPage();

			pnlGrid.DataBind();
		}
	}

	private void InitPage() {
		this.lstYear.SelectedValue = DateTime.Now.ToString("yyyy");
		this.lstMonth.SelectedValue = DateTime.Now.ToString("MM");

		if (ViCommConst.MAN.Equals(this.SexCd)) {
			this.tdDataCastSite.Visible = false;
			this.tdHeaderCastSite.Visible = false;
			this.tdHeaderReportMonth.Attributes["class"] = "tdHeaderStyle2";
		} else {
			this.chkCastSite.Checked = true;
			this.tdDataCastSite.Visible = true;
			this.tdHeaderCastSite.Visible = true;
			this.tdHeaderReportMonth.Attributes["class"] = "tdHeaderStyle";
		}
	}

	private void ClearField() {
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		string sSiteCd = this.lstSiteCd.SelectedValue;
		string sYear = this.lstYear.SelectedValue;
		string sMonth = this.lstMonth.SelectedValue;
		string sSexNm = this.SexCd == ViCommConst.MAN ? "MAN" : "CAST";

		Response.Filter = filter;
		Response.AddHeader("Content-Disposition",string.Format("attachment;filename=USER_DEF_HISTORY_DAILY_{0}_{1}_{2}{3}.CSV",sSexNm,lstSiteCd.SelectedValue,sYear,sMonth));
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		DataSet ds;
		using (UserDefPointHistory oUserDefPointHistory = new UserDefPointHistory()) {
			ds = oUserDefPointHistory.GetDailyList(sSiteCd,sYear,sMonth,SexCd,this.chkCastSite.Checked ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR);
			SetCsvData(ds);
			Response.End();
		}
	}

	public void SetCsvData(DataSet pDataSet) {
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		string sHeaderString = "日付,曜日,増減ﾎﾟｲﾝﾄ\r\n";
		Response.BinaryWrite(encoding.GetBytes(sHeaderString));

		string sDtl = "";

		DataRow dr = null;
		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];

			sDtl = string.Format("{0},{1},{2}",dr["DAYS"].ToString(),GetDisplayDayOfWeek(dr["DAYS"].ToString()),dr["ADD_POINT"].ToString());
			sDtl += "\r\n";
			Response.BinaryWrite(encoding.GetBytes(sDtl));
		}
	}

	protected void grdUserDefPointHistory_RowDataBound(object sender,GridViewRowEventArgs e) {
		if (e.Row.RowType == DataControlRowType.DataRow) {
			DataRowView drv = (DataRowView)e.Row.DataItem;
			if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(drv["ADD_POINT"]))) {
				dAddPoint = dAddPoint + int.Parse(iBridUtil.GetStringValue(drv["ADD_POINT"]));
			}

			DateTime dtReportDay = DateTime.Parse(drv["DAYS"].ToString());
			int iDayOfWeek = (int)dtReportDay.DayOfWeek;

			if (iDayOfWeek == 0) {
				e.Row.BackColor = Color.FromArgb(0xFFCCCC);
			} else if (iDayOfWeek == 6) {
				e.Row.BackColor = Color.FromArgb(0xCCCCFF);
			}
		} else if (e.Row.RowType == DataControlRowType.Footer) {
			DateTime dtSelectedmonth = DateTime.Parse(string.Format("{0}/{1}/01",lstYear.SelectedValue,lstMonth.SelectedValue));
			DateTime dtNowMonth = DateTime.Parse(string.Format("{0}/{1}/01",DateTime.Now.ToString("yyyy"),DateTime.Now.ToString("MM")));

			int iDaysInMonth = DateTime.DaysInMonth(dtSelectedmonth.Year,dtSelectedmonth.Month);
			int iPastDays = 0;

			if (dtSelectedmonth == dtNowMonth) {
				iPastDays = DateTime.Now.Day - 1;
			} else {
				iPastDays = iDaysInMonth;
			}

			Label lblAddPointFooter = (Label)e.Row.FindControl("lblAddPointFooter");

			lblAddPointFooter.Text = dAddPoint.ToString();
		}
	}

	protected void dsUserDefPointHistory_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters["pSiteCd"] = this.lstSiteCd.SelectedValue;
		e.InputParameters["pYear"] = this.lstYear.SelectedValue;
		e.InputParameters["pMonth"] = this.lstMonth.SelectedValue;
		e.InputParameters["pContainsCastSite"] = this.chkCastSite.Checked ? ViCommConst.FLAG_ON_STR : ViCommConst.FLAG_OFF_STR;
	}

	private void GetList() {
		grdUserDefPointHistory.PageIndex = 0;
		grdUserDefPointHistory.DataSourceID = "dsUserDefPointHistory";
		DataBind();
	}

	protected Color GetBackColor(object pDayOfWeek) {
		if (pDayOfWeek.ToString().Equals("Sat")) {
			return Color.FromArgb(0xCCCCFF);
		} else {
			if (pDayOfWeek.ToString().Equals("Sun")) {
				return Color.FromArgb(0xFFCCCC);
			} else {
				return Color.Empty;
			}
		}
	}

	private void CreateList() {
		this.lstSiteCd.DataBind();
		if (!string.IsNullOrEmpty(iBridUtil.GetStringValue(Session["MENU_SITE"]))) {
			this.lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}

		int iStartYear = 2009;
		int iCurYear = DateTime.Now.Year;
		lstYear.Items.Clear();

		for (int i = 0;i + iStartYear <= iCurYear;i++) {

			lstYear.Items.Add(new ListItem(iBridUtil.GetStringValue(i + iStartYear)));
		}

		lstMonth.Items.Clear();
		for (int i = 1;i <= 12;i++) {
			lstMonth.Items.Add(new ListItem(string.Format("{0:D2}",i)));
		}
	}

	private bool IsCorrectDate() {
		return true;
	}

	protected string GetDisplayReportDay(string pReportDay) {
		DateTime dtReportDay = DateTime.Parse(pReportDay);
		return dtReportDay.Day.ToString();
	}

	protected string GetDisplayDayOfWeek(string pReportDay) {
		DateTime dtReportDay = DateTime.Parse(pReportDay);
		int iDayOfWeekNo = (int)dtReportDay.DayOfWeek;

		return Week[iDayOfWeekNo];
	}
}
