﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 期間別ムービーダウンロード集計
--	Progaram ID		: DailyTalkMovieDownload
--
--  Creation Date	: 2009.06.01
--  Creater			: iBrid
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using iBridCommLib;
using ViComm;

public partial class Status_DailyTalkMovieDownload:System.Web.UI.Page {


	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}
	}

	private void InitPage() {
		ClearField();
		DataBind();
		if (!IsPostBack) {
			lstSiteCd.DataSourceID = "";
			if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
				lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}
		}
		
		DisplayWordUtil.ReplaceGridColumnHeader(this.grdDailyDownload);
	}

	private void ClearField() {
		SysPrograms.SetupFromToDay(lstFromYYYY,lstFromMM,lstFromDD,lstToYYYY,lstToMM,lstToDD,true);
		lstFromYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		lstToYYYY.SelectedValue = DateTime.Now.ToString("yyyy");
		lstFromMM.SelectedValue = DateTime.Now.ToString("MM");
		lstToMM.SelectedValue = DateTime.Now.ToString("MM");
		lstFromDD.SelectedValue = DateTime.Now.ToString("dd");
		lstToDD.SelectedValue = DateTime.Now.ToString("dd");
		int iCompare = string.Compare(Session["AdminType"].ToString(),ViCommConst.RIGHT_SITE_OWNER);
		btnCsv.Visible = (iCompare >= 0);
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		string fileName = ViCommConst.CSV_FILE_NM_ACCESS_MOVIE;

		Response.AddHeader("Content-Disposition","attachment;filename=" + fileName);
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		using (AccessTalkMovie oAccessTalkMovie = new AccessTalkMovie()) {
			DataSet ds = oAccessTalkMovie.GetList(
								lstSiteCd.SelectedValue,
								lstFromYYYY.SelectedValue,
								lstFromMM.SelectedValue,
								lstFromDD.SelectedValue,
								lstToYYYY.SelectedValue,
								lstToMM.SelectedValue,
								lstToDD.SelectedValue);

			string sCsvRec = GetCsvString(ds);

			Response.BinaryWrite(encoding.GetBytes(sCsvRec));
			Response.End();
		}
	}


	private string GetCsvString(DataSet pDataSet) {
		string sCsv = "";
		sCsv += "ＩＤ番号,";
		sCsv += "氏名,";
		sCsv += "アクセス数,";
		sCsv += "利用Pt,";
		sCsv += "利用秒数";
		sCsv += "\r\n";
		DataRow dr = null;
		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];
			sCsv += dr["LOGIN_ID"] + ",";
			sCsv += dr["CAST_NM"] + ",";
			sCsv += dr["ACCESS_COUNT"] + ",";
			sCsv += dr["TOTAL_USED_POINT"] + ",";
			sCsv += dr["TOTAL_USED_SEC"];
			sCsv += "\r\n";
		}
		return sCsv;
	}


	private void GetList() {
		grdDailyDownload.DataSourceID = "dsAccessTalkMovie";
		DataBind();
	}


	protected string GetFromDay() {
		return lstFromYYYY.SelectedValue + lstFromMM.SelectedValue + lstFromDD.SelectedValue;
	}
	protected string GetToDay() {
		return lstToYYYY.SelectedValue + lstToMM.SelectedValue + lstToDD.SelectedValue;
	}
}
