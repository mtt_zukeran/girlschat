<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ManUsedPoint.aspx.cs" Inherits="Status_ManUsedPoint" Title="会員別使用ポイント集計"
	ValidateRequest="false" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="HolderContentTitle" runat="Server">
	<asp:Label ID="lblPgmTitle" runat="server" Text="会員別使用ポイント集計"></asp:Label>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="HolderContent" runat="Server">
	<div class="admincontent">
		<fieldset class="fieldset">
			<legend>[検索条件]</legend>
			<asp:Panel runat="server" ID="pnlKey">
				<table border="0" style="width: 640px" class="tableStyle">
					<tr>
						<td class="tdHeaderStyle">
							サイト
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstSiteCd" runat="server" DataSourceID="dsSite" DataTextField="SITE_NM" DataValueField="SITE_CD" Width="240px">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							集計開始日
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstFromYYYY" runat="server" Width="60px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstFromMM" runat="server" Width="40px">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstFromDD" runat="server" Width="40px">
							</asp:DropDownList>日
							<asp:DropDownList ID="lstFromHH" runat="server" Width="40px">
							</asp:DropDownList>時
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle">
							集計終了日
						</td>
						<td class="tdDataStyle">
							<asp:DropDownList ID="lstToYYYY" runat="server" Width="60px">
							</asp:DropDownList>年
							<asp:DropDownList ID="lstToMM" runat="server" Width="40px">
							</asp:DropDownList>月
							<asp:DropDownList ID="lstToDD" runat="server" Width="40px">
							</asp:DropDownList>日
							<asp:DropDownList ID="lstToHH" runat="server" Width="40px">
							</asp:DropDownList>時
						</td>
					</tr>
					<tr>
						<td class="tdHeaderStyle2">
							<asp:Label ID="Label9" runat="server" Text="表示明細"></asp:Label>
						</td>
						<td class="tdDataStyle">
							<asp:RadioButton ID="rdoTel" runat="server" Checked="true" GroupName="DisplayType" AutoPostBack="true" Text="電話利用明細">
							</asp:RadioButton>
							<asp:RadioButton ID="rdoWeb" runat="server" GroupName="DisplayType" AutoPostBack="true" Text="WEB利用明細">
							</asp:RadioButton>
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnListSeek" Text="検索" CssClass="seekbutton" OnClick="btnListSeek_Click" />
				<asp:Button runat="server" ID="btnClear" Text="クリア" CssClass="seekbutton" OnClick="btnClear_Click" />
				<asp:Button runat="server" ID="btnCsv" Text="ＣＳＶ出力" CssClass="seekbutton" OnClick="btnCSV_Click" />
				<asp:Button runat="server" ID="btnCSVDailyTotal" Text="日別使用ﾎﾟｲﾝﾄCSV出力" CssClass="seekbutton" OnClick="btnCSVDailyTotal_Click" />
			</asp:Panel>
		</fieldset>
		<asp:Panel runat="server" ID="pnlInfo">
			<fieldset>
				<legend>[稼動集計]</legend>
				合計人数&nbsp;<%#GetTotalCount()%>人
				<asp:Panel ID="pnlGrid" runat="server" ScrollBars="auto" Height="540px">
					<asp:GridView ID="grdUserManUsedPoint" runat="server" AutoGenerateColumns="False" DataSourceID="dsUserManUsedPoint" ShowFooter="True" AllowSorting="True"
						SkinID="GridViewColor" OnRowDataBound="grdUserManUsedPoint_RowDataBound" Font-Size="X-Small">
						<Columns>
							<asp:TemplateField HeaderText="ﾛｸﾞｲﾝID">
								<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
								<ItemTemplate>
									<asp:HyperLink ID="lnkLoginId" runat="server" NavigateUrl='<%# string.Format("../Man/ManView.aspx?site={0}&manloginid={1}",Eval("SITE_CD"),Eval("LOGIN_ID")) %>'
										Text='<%# Eval("LOGIN_ID") %>'></asp:HyperLink>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Center" />
								<FooterTemplate>
									<asp:Label ID="lblFooter" runat="server" Text='合計'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾕｰｻﾞｰ<br />ﾗﾝｸ">
								<ItemTemplate>
									<asp:Label ID="Label35" runat="server" Text='<%# Eval("USER_RANK_NM") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Center" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Left" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblUserRank" runat="server" Text='-'></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="TV">
								<ItemTemplate>
									<asp:Label ID="Label21" runat="server" Text='<%# Eval("PRV_TV_TALK_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblPrvTvTalkPoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="TVﾁｬｯﾄ">
								<ItemTemplate>
									<asp:Label ID="Label25" runat="server" Text='<%# Eval("PUB_TV_TALK_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblPubTvTalkPoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="音声">
								<ItemTemplate>
									<asp:Label ID="Label26" runat="server" Text='<%# Eval("PRV_VOICE_TALK_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblPrvVoiceTalkPoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="音声ﾁｬｯﾄ">
								<ItemTemplate>
									<asp:Label ID="Label26" runat="server" Text='<%# Eval("PUB_VOICE_TALK_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblPubVoiceTalkPoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="会話ﾓﾆﾀ">
								<ItemTemplate>
									<asp:Label ID="Label27" runat="server" Text='<%# Eval("VIEW_TALK_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblViewTalkPoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="音声ﾓﾆﾀ">
								<ItemTemplate>
									<asp:Label ID="Label27" runat="server" Text='<%# Eval("WIRETAPPING_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblWiretappingPoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="部屋ﾓﾆﾀ">
								<ItemTemplate>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("VIEW_ONLINE_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblViewOnlinePoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾗｲﾌﾞ視聴">
								<ItemTemplate>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("VIEW_LIVE_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblViewLivePoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾛﾝｸﾞ<br />ﾑｰﾋﾞｰ">
								<ItemTemplate>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("PLAY_MOVIE_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblPlayMoviePoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="PF再生">
								<ItemTemplate>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("PLAY_PF_VOICE_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblPlayPfVoicePoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="PF録音">
								<ItemTemplate>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("REC_PF_VOICE_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblRecPfVoicePoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="GP会話">
								<ItemTemplate>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("GPF_TALK_VOICE_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblGpfVoiceTalkPoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾌﾟﾗｲﾍﾞｰﾄ<br/>伝言再生">
								<ItemTemplate>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("PLAY_PV_MSG_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblPlayPvMsgPoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="男性待機<br/>TV">
								<ItemTemplate>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("CAST_PUB_TV_TALK_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblCastPubTvTalkPoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="男性待機<br/>TVﾁｬｯﾄ">
								<ItemTemplate>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("CAST_PRV_TV_TALK_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblCastPrvTvTalkPoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="男性待機<br/>音声">
								<ItemTemplate>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("CAST_PUB_VOICE_TALK_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblCastPubVoiceTalkPoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="男性待機<br/>音声ﾁｬｯﾄ">
								<ItemTemplate>
									<asp:Label ID="Label28" runat="server" Text='<%# Eval("CAST_PRV_VOICE_TALK_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblCastPrvVoiceTalkPoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="電話計">
								<ItemTemplate>
									<asp:Label ID="Label30" runat="server" Text='<%# Eval("TALK_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" BackColor="#F5F5DC" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" BackColor="#F5F5DC" />
								<FooterTemplate>
									<asp:Label ID="lblTalkPoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾒｰﾙ送信">
								<ItemTemplate>
									<asp:Label ID="Label31" runat="server" Text='<%# Eval("TX_MAIL_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblTxMailPoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="写真添付<br/>ﾒｰﾙ送信">
								<ItemTemplate>
									<asp:Label ID="Label31" runat="server" Text='<%# Eval("TX_MAIL_WITH_PIC_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblTxMailWithPicPoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="動画添付<br/>ﾒｰﾙ送信">
								<ItemTemplate>
									<asp:Label ID="Label31" runat="server" Text='<%# Eval("TX_MAIL_WITH_MOVIE_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblTxMailWithMoviePoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾒｰﾙ添付<br>写真閲覧">
								<ItemTemplate>
									<asp:Label ID="Label32" runat="server" Text='<%# Eval("MAIL_WITH_PIC_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblMailWithPicPoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾒｰﾙ添付<br>動画閲覧">
								<ItemTemplate>
									<asp:Label ID="Label33" runat="server" Text='<%# Eval("MAIL_WITH_MOVIE_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblMailWithMoviePoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="掲示板<br>写真閲覧">
								<ItemTemplate>
									<asp:Label ID="Label32" runat="server" Text='<%# Eval("BBS_PIC_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblBbsPicPoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="掲示板<br>動画閲覧">
								<ItemTemplate>
									<asp:Label ID="Label33" runat="server" Text='<%# Eval("BBS_MOVIE_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblBbsMoviePoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="掲示板<br>書込">
								<ItemTemplate>
									<asp:Label ID="Label33" runat="server" Text='<%# Eval("WRITE_BBS_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblWriteBbsPoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾀﾞｳﾝﾛｰﾄﾞ<br>動画">
								<ItemTemplate>
									<asp:Label ID="Label33" runat="server" Text='<%# Eval("DOWNLOAD_MOVIE_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblDownloadMoviePoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="定時<br>ｺｰﾙ">
								<ItemTemplate>
									<asp:Label ID="Label33" runat="server" Text='<%# Eval("TIME_CALL_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblTimeCallPoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾀﾞｳﾝﾛｰﾄﾞ<br>着ﾎﾞｲｽ">
								<ItemTemplate>
									<asp:Label ID="Label33" runat="server" Text='<%# Eval("DOWNLOAD_VOICE_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblDownloadVoicePoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ｿｰｼｬﾙ<br>ｹﾞｰﾑ">
								<ItemTemplate>
									<asp:Label ID="Label33" runat="server" Text='<%# Eval("SOCIAL_GAME_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblSocialGamePoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="野球拳<br />FC会費">
								<ItemTemplate>
									<asp:Label ID="Label33" runat="server" Text='<%# Eval("YAKYUKEN_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblYakyukenPoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="ﾌﾟﾚｾﾞﾝﾄ<br />ﾒｰﾙ">
								<ItemTemplate>
									<asp:Label ID="Label36" runat="server" Text='<%# Eval("PRESENT_MAIL_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" />
								<FooterTemplate>
									<asp:Label ID="lblPresentMailPoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="WEB計">
								<ItemTemplate>
									<asp:Label ID="Label34" runat="server" Text='<%# Eval("WEB_TOTAL_POINT", "{0}") %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" BackColor="#F5F5DC" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" BackColor="#F5F5DC" />
								<FooterTemplate>
									<asp:Label ID="lblWebTotalPoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="合計">
								<ItemTemplate>
									<asp:Label ID="Label35" runat="server" Text='<%#string.Format("{0}",CalcSum(Eval("TALK_POINT"),Eval("WEB_TOTAL_POINT"))) %>'></asp:Label>
								</ItemTemplate>
								<FooterStyle HorizontalAlign="Right" Width="50px" BackColor="#FFEBCD" />
								<HeaderStyle HorizontalAlign="Center" />
								<ItemStyle HorizontalAlign="Right" Width="50px" BackColor="#FFEBCD" />
								<FooterTemplate>
									<asp:Label ID="lblTotalPoint" runat="server" Text=''></asp:Label>
								</FooterTemplate>
							</asp:TemplateField>
						</Columns>
						<FooterStyle ForeColor="Black" BackColor="LightYellow" />
					</asp:GridView>
				</asp:Panel>
			</fieldset>
		</asp:Panel>
		<br />
	</div>
	<asp:ObjectDataSource ID="dsSite" runat="server" SelectMethod="GetList" TypeName="Site"></asp:ObjectDataSource>
	<asp:ObjectDataSource ID="dsUserManUsedPoint" runat="server" SelectMethod="GetPageCollection" TypeName="UserManUsedPoint" OnSelecting="grdUserManUsedPoint_Selecting"
		OnSelected="grdUserManUsedPoint_Selected">
		<SelectParameters>
			<asp:Parameter Name="pSiteCd" />
			<asp:Parameter Name="pFromYYYY" />
			<asp:Parameter Name="pFromMM" />
			<asp:Parameter Name="pFromDD" />
			<asp:Parameter Name="pFromHH" />
			<asp:Parameter Name="pToYYYY" />
			<asp:Parameter Name="pToMM" />
			<asp:Parameter Name="pToDD" />
			<asp:Parameter Name="pToHH" />
		</SelectParameters>
	</asp:ObjectDataSource>
</asp:Content>
