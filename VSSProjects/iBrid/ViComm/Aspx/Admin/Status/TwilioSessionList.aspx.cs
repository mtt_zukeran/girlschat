﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 音声通話セッション
--	Progaram ID		: TwilioSessionList
--
--  Creation Date	: 2014.12.18
--  Creater			: K.Miyazato
--
**************************************************************************/
using System;
using System.Data;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using iBridCommLib;
using ViComm;
using ViComm.Extension.Pwild;

public partial class Status_TwilioSessionList:System.Web.UI.Page {
	private string TwilioAccountSid {
		get {
			return iBridUtil.GetStringValue(this.ViewState["TWILIO_ACCOUNT_SID"]);
		}
		set {
			this.ViewState["TWILIO_ACCOUNT_SID"] = value;
		}
	}

	private string TwilioAuthToken {
		get {
			return iBridUtil.GetStringValue(this.ViewState["TWILIO_AUTH_TOKEN"]);
		}
		set {
			this.ViewState["TWILIO_AUTH_TOKEN"] = value;
		}
	}

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			using (Sys oSys = new Sys()) {
				if (oSys.GetOne()) {
					TwilioAccountSid = oSys.twilioAccountSid;
					TwilioAuthToken = oSys.twilioAuthToken;
				}
			}
		}
	}

	protected void dsTwilioSession_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = TwilioAccountSid;
		e.InputParameters[1] = TwilioAuthToken;
	}

	protected void btnRefresh_Click(object sender,EventArgs e) {
		grdTwilioSession.DataBind();
	}

	protected void lnkDisconnect_Command(object sender,CommandEventArgs e) {
		string sCallSid = e.CommandArgument.ToString();

		using (Twilio oTwilio = new Twilio()) {
			oTwilio.DisconnectTwilioCall(TwilioAccountSid,TwilioAuthToken,sCallSid);
		}

		grdTwilioSession.DataBind();
	}

	protected string GetFromTelUrl(object pFromTelNo,object pRequesterSexCd,object pUseManVoiceappFlag,object pUseCastVoiceappFlag) {
		string sUrl = string.Empty;

		if (pRequesterSexCd.ToString().Equals(ViCommConst.MAN)) {
			if (pUseManVoiceappFlag.ToString().Equals(ViCommConst.FLAG_ON_STR)) {
				sUrl = string.Format("~/Man/ManView.aspx?site=A001&userseq={0}",pFromTelNo.ToString().Replace("client:",""));
			} else {
				sUrl = string.Format("~/Man/ManView.aspx?site=A001&manloginid={0}",pFromTelNo.ToString());
			}
		} else if (pRequesterSexCd.ToString().Equals(ViCommConst.OPERATOR)) {
			if (pUseCastVoiceappFlag.ToString().Equals(ViCommConst.FLAG_ON_STR)) {
				sUrl = string.Format("~/Cast/CastView.aspx?userseq={0}",pFromTelNo.ToString().Replace("client:",""));
			} else {
				sUrl = string.Format("~/Cast/CastView.aspx?loginid={0}",pFromTelNo.ToString());
			}
		}

		return sUrl;
	}

	protected string GetToTelUrl(object pToTelNo,object pRequesterSexCd,object pUseManVoiceappFlag,object pUseCastVoiceappFlag) {
		string sUrl = string.Empty;

		if (pRequesterSexCd.ToString().Equals(ViCommConst.MAN)) {
			if (pUseCastVoiceappFlag.ToString().Equals(ViCommConst.FLAG_ON_STR)) {
				sUrl = string.Format("~/Cast/CastView.aspx?userseq={0}",pToTelNo.ToString().Replace("client:",""));
			} else {
				sUrl = string.Format("~/Cast/CastView.aspx?loginid={0}",pToTelNo.ToString());
			}
		} else if (pRequesterSexCd.ToString().Equals(ViCommConst.OPERATOR)) {
			if (pUseManVoiceappFlag.ToString().Equals(ViCommConst.FLAG_ON_STR)) {
				sUrl = string.Format("~/Man/ManView.aspx?site=A001&userseq={0}",pToTelNo.ToString().Replace("client:",""));
			} else {
				sUrl = string.Format("~/Man/ManView.aspx?site=A001&manloginid={0}",pToTelNo.ToString());
			}
		}

		return sUrl;
	}

	protected string GetTypeText(object pRequesterSexCd,object pUseFreeDialFlag,object pUseWhitePlanFlag) {
		string sText = string.Empty;

		if (pRequesterSexCd.ToString().Equals(ViCommConst.MAN)) {
			sText = "会員発信";
		} else if (pRequesterSexCd.ToString().Equals(ViCommConst.OPERATOR)) {
			sText = "出演者発信";
		}

		if (pUseFreeDialFlag.ToString().Equals(ViCommConst.FLAG_ON_STR)) {
			sText += "(Free)";
		}

		if (pUseWhitePlanFlag.ToString().Equals(ViCommConst.FLAG_ON_STR)) {
			sText += "(White)";
		}

		return sText;
	}

	protected string GetStatusText(object pStatus) {
		string sText = string.Empty;

		switch (pStatus.ToString()) {
			case PwViCommConst.TwilioCallStatus.QUEUED:
			case PwViCommConst.TwilioCallStatus.RINGING:
				sText = "呼出中";
				break;
			case PwViCommConst.TwilioCallStatus.IN_PROGRESS:
				sText = "通話中";
				break;
			case PwViCommConst.TwilioCallStatus.COMPLETED:
				sText = "正常終了";
				break;
			case PwViCommConst.TwilioCallStatus.BUSY:
				sText = "ビジー";
				break;
			case PwViCommConst.TwilioCallStatus.FAILED:
				sText = "接続失敗";
				break;
			case PwViCommConst.TwilioCallStatus.NO_ANSWER:
				sText = "応答なし";
				break;
			case PwViCommConst.TwilioCallStatus.CANCELED:
				sText = "キャンセル";
				break;
			default:
				sText = pStatus.ToString();
				break;
		}

		return sText;
	}

	protected bool GetDisconnectEnabled(object pStatus) {
		bool bVisible = false;

		switch (pStatus.ToString()) {
			case "queued":
			case "ringing":
			case "in-progress":
				bVisible = true;
				break;
		}

		return bVisible;
	}

	protected Color GetStatusColor(object pStatus) {
		Color foreColor = Color.Empty;

		switch (pStatus.ToString()) {
			case "queued":
			case "ringing":
			case "in-progress":
				foreColor = Color.Red;
				break;
		}

		return foreColor;
	}
}
