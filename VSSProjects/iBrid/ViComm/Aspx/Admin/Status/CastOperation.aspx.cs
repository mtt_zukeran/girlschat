﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 出演者稼動状況
--	Progaram ID		: CastOperation
--
--  Creation Date	: 2010.05.14
--  Creater			: Nakano
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using iBridCommLib;

public partial class Status_CastOperation:Page {
	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			FisrtLoad();
			InitPage();
		}
	}

	private void FisrtLoad() {
		grdCastOperation.PageSize = int.Parse(Session["PageSize"].ToString());
		grdCastOperation.DataSourceID = "";
		DataBind();
		if (Session["SiteCd"].ToString().Equals("")) {
			lstSiteCd.Items.Insert(0,new ListItem("",""));
		}
		lstSiteCd.DataSourceID = "";
		if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
			lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
		}
	}

	protected void dsCastOperation_Selecting(object sender,ObjectDataSourceSelectingEventArgs e) {
		e.InputParameters[0] = lstSiteCd.SelectedValue;
		e.InputParameters[1] = CreateDateString(txtRegistDayFrom.Text,lstRegistTimeFrom.Text);

		if (txtRegistDayTo.Text.Equals("")) {
			txtRegistDayTo.Text = txtRegistDayFrom.Text;
		}
		e.InputParameters[2] = CreateDateString(txtRegistDayTo.Text,lstRegistTimeTo.SelectedValue);
		e.InputParameters[3] = CreateDateString(txtLoginDayFrom.Text,lstLoginTimeFrom.Text);

		if (txtLoginDayTo.Text.Equals("")) {
			txtLoginDayTo.Text = txtLoginDayFrom.Text;
		}
		e.InputParameters[4] = CreateDateString(txtLoginDayTo.Text,lstLoginTimeTo.SelectedValue);
	}

	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	private void InitPage() {
		this.Title = DisplayWordUtil.Replace(this.Title);
		this.lblPgmTitle.Text = DisplayWordUtil.Replace(this.lblPgmTitle.Text);
		
		grdCastOperation.DataSourceID = "";
		DataBind();
		if (!IsPostBack) {
			SysPrograms.SetupTime(lstRegistTimeFrom,lstRegistTimeTo);
			SysPrograms.SetupTime(lstLoginTimeFrom,lstLoginTimeTo);
		}
		pnlInfo.Visible = false;
		ClearField();
	}

	private void ClearField() {
		txtRegistDayFrom.Text = DateTime.Now.ToString("yyyy/MM/dd");
		txtRegistDayTo.Text = DateTime.Now.ToString("yyyy/MM/dd");
		lstRegistTimeFrom.SelectedIndex = 0;
		lstRegistTimeTo.SelectedIndex = 0;
		txtLoginDayFrom.Text = DateTime.Now.ToString("yyyy/MM/dd");
		txtLoginDayTo.Text = DateTime.Now.ToString("yyyy/MM/dd");
		lstLoginTimeFrom.SelectedIndex = 0;
		lstLoginTimeTo.SelectedIndex = 0;
	}

	private void GetList() {
		grdCastOperation.DataSourceID = "dsCastOperation";
		DataBind();
		pnlInfo.Visible = true;
	}

	private string CreateDateString(string pDate,string pHh) {
		return string.Format("{0} {1}",pDate,pHh);
	}

	protected string CreateDateCountString(string pDate) {
		return string.IsNullOrEmpty(pDate) ? "" : string.Format("{0}日前",pDate);
	}
}
