﻿/*************************************************************************
--	System			: ViComm Site
--	Sub System Name	: Admin
--	Title			: 日別稼動集計
--	Progaram ID		: HourlyPerformance
--
--  Creation Date	: 2015.05.06
--  Creater			: M&TT Y.Ikemiya
--
**************************************************************************/

// [ Update History ]
/*------------------------------------------------------------------------

  Date        Updater    Update Explain
  yyyy/mm/dd  XXXXXXXXX

-------------------------------------------------------------------------*/
using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using iBridCommLib;
using ViComm;

public partial class Status_HourlyPerformance:Page {

	private int prvTvTalkCount;
	private int prvTvTalkMin;
	private int prvTvTalkPoint;
	private int pubTvTalkCount;
	private int pubTvTalkMin;
	private int pubTvTalkPoint;
	private int prvVoiceTalkCount;
	private int prvVoiceTalkMin;
	private int prvVoiceTalkPoint;
	private int pubVoiceTalkCount;
	private int pubVoiceTalkMin;
	private int pubVoiceTalkPoint;
	private int viewTalkCount;
	private int viewTalkMin;
	private int viewTalkPoint;
	private int viewBroadcastCount;
	private int viewBroadcastMin;
	private int viewBroadcastPoint;
	private int liveCount;
	private int liveMin;
	private int livePoint;
	//private int movieCount;
	//private int movieMin;
	//private int moviePoint;
	private int playProfileCount;
	private int playProfileMin;
	private int playProfilePoint;
	private int recProfileCount;
	private int recProfileMin;
	private int recProfilePoint;
	private int wiretapCount;
	private int wiretapMin;
	private int wiretapPoint;
	private int gpfTalkVoiceCount;
	private int gpfTalkVoiceMin;
	private int gpfTalkVoicePoint;
	private int playPvMsgCount;
	private int playPvMsgMin;
	private int playPvMsgPoint;
	private int castPrvTvTalkCount;
	private int castPrvTvTalkMin;
	private int castPrvTvTalkPoint;
	private int castPubTvTalkCount;
	private int castPubTvTalkMin;
	private int castPubTvTalkPoint;
	private int castPrvVoiceTalkCount;
	private int castPrvVoiceTalkMin;
	private int castPrvVoiceTalkPoint;
	private int castPubVoiceTalkCount;
	private int castPubVoiceTalkMin;
	private int castPubVoiceTalkPoint;

	private int talkCount;
	private int talkMin;
	private int talkPoint;
	private int freeDialCount;
	private int freeDialMin;
	private int freeDialPoint;

	//
	private int talkCountNf;
	private int talkMinNf;
	private int talkPointNf;
	private int talkCountFree;
	private int talkMinFree;
	private int talkPointFree;
	//

	private int mailCount;
	private int mailPoint;
	private int mailPicCount;
	private int mailPicPoint;
	private int mailMovieCount;
	private int mailMoviePoint;
	private int openPicMailCount;
	private int openPicMailPoint;
	private int openMovieMailCount;
	private int openMovieMailPoint;
	private int openPicBbsCount;
	private int openPicBbsPoint;
	private int openMovieBbsCount;
	private int openMovieBbsPoint;

	private int downloadMovieCount;
	private int downloadMoviePoint;
	private int timeCallCount;
	private int timeCallPoint;
	private int downloadVoiceCount;
	private int downloadVoicePoint;
	private int socialGameCount;
	private int socialGamePoint;
	private int yakyukenCount;
	private int yakyukenPoint;
	private int presentMailCount;
	private int presentMailPoint;

	private int writeBbsCount;
	private int writeBbsPoint;
	private int webTotalCount;
	private int webTotalPoint;

	protected void Page_Load(object sender,EventArgs e) {
		if (!IsPostBack) {
			InitPage();
		}

		DisplayWordUtil.ReplaceGridColumnHeader(this.grdDailyPerformace);
	}

	private void InitPage() {
		grdDailyPerformace.DataSourceID = "";
		pnlInfo.Visible = false;
		ClearField();
		DataBind();
		if (!IsPostBack) {
			SysPrograms.SetupDay(lstYear,lstMonth,lstDay,false);
			lstYear.SelectedIndex = 0;
			lstMonth.SelectedValue = DateTime.Now.ToString("MM");
			lstDay.SelectedValue = DateTime.Now.ToString("dd");

			if (!iBridUtil.GetStringValue(Session["MENU_SITE"]).Equals("")) {
				lstSiteCd.SelectedValue = iBridUtil.GetStringValue(Session["MENU_SITE"]);
			}

			lstSiteCd.DataSourceID = "";
		}
	}

	private void ClearField() {
		btnCsv.Visible = false;
		prvTvTalkCount = 0;
		prvTvTalkMin = 0;
		prvTvTalkPoint = 0;
		pubTvTalkCount = 0;
		pubTvTalkMin = 0;
		pubTvTalkPoint = 0;
		prvVoiceTalkCount = 0;
		prvVoiceTalkMin = 0;
		prvVoiceTalkPoint = 0;
		pubVoiceTalkCount = 0;
		pubVoiceTalkMin = 0;
		pubVoiceTalkPoint = 0;
		viewTalkCount = 0;
		viewTalkMin = 0;
		viewTalkPoint = 0;
		viewBroadcastCount = 0;
		viewBroadcastMin = 0;
		viewBroadcastPoint = 0;
		liveCount = 0;
		liveMin = 0;
		livePoint = 0;
		//movieCount = 0;
		//movieMin = 0;
		//moviePoint = 0;
		playProfileCount = 0;
		playProfileMin = 0;
		playProfilePoint = 0;
		recProfileCount = 0;
		recProfileMin = 0;
		recProfilePoint = 0;
		wiretapCount = 0;
		wiretapMin = 0;
		wiretapPoint = 0;
		gpfTalkVoiceCount = 0;
		gpfTalkVoiceMin = 0;
		gpfTalkVoicePoint = 0;
		playPvMsgCount = 0;
		playPvMsgMin = 0;
		playPvMsgPoint = 0;
		castPrvTvTalkCount = 0;
		castPrvTvTalkMin = 0;
		castPrvTvTalkPoint = 0;
		castPubTvTalkCount = 0;
		castPubTvTalkMin = 0;
		castPubTvTalkPoint = 0;
		castPrvVoiceTalkCount = 0;
		castPrvVoiceTalkMin = 0;
		castPrvVoiceTalkPoint = 0;
		castPubVoiceTalkCount = 0;
		castPubVoiceTalkMin = 0;
		castPubVoiceTalkPoint = 0;

		talkCount = 0;
		talkMin = 0;
		talkPoint = 0;
		freeDialCount = 0;
		freeDialMin = 0;
		freeDialPoint = 0;
		talkCountNf = 0;
		talkMinNf = 0;
		talkPointNf = 0;
		talkCountFree = 0;
		talkMinFree = 0;
		talkPointFree = 0;

		mailCount = 0;
		mailPoint = 0;
		mailPicCount = 0;
		mailPicPoint = 0;
		mailMovieCount = 0;
		mailMoviePoint = 0;
		openPicMailCount = 0;
		openPicMailPoint = 0;
		openMovieMailCount = 0;
		openMovieMailPoint = 0;
		openPicBbsCount = 0;
		openPicBbsPoint = 0;
		openMovieBbsCount = 0;
		openMovieBbsPoint = 0;
		writeBbsCount = 0;
		writeBbsPoint = 0;
		downloadMovieCount = 0;
		downloadMoviePoint = 0;
		timeCallCount = 0;
		timeCallPoint = 0;
		downloadVoiceCount = 0;
		downloadVoicePoint = 0;
		socialGameCount = 0;
		socialGamePoint = 0;
		yakyukenCount = 0;
		yakyukenPoint = 0;
		presentMailCount = 0;
		presentMailPoint = 0;
		webTotalCount = 0;
		webTotalPoint = 0;
	}


	protected void btnListSeek_Click(object sender,EventArgs e) {
		GetList();
	}

	protected void btnClear_Click(object sender,EventArgs e) {
		InitPage();
	}

	protected void btnCSV_Click(object sender,EventArgs e) {
		string fileName = string.Format("HOURLY_PERFORMANCE_{0}_{1}{2}{3}.csv",lstSiteCd.SelectedValue,lstYear.SelectedValue,lstMonth.SelectedValue,lstDay.SelectedValue);


		Response.AddHeader("Content-Disposition","attachment;filename=" + fileName);
		Response.ContentType = "application/octet-stream-dummy";
		System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("Shift-JIS");

		using (DailyPerformance oDailyPerformance = new DailyPerformance()) {
			DataSet ds = oDailyPerformance.GetListHourly(
								lstSiteCd.SelectedValue,
								lstYear.SelectedValue,
								lstMonth.SelectedValue,
								lstDay.SelectedValue);

			string sCsvRec = GetCsvString(ds);

			Response.BinaryWrite(encoding.GetBytes(sCsvRec));
			Response.End();
		}
	}


	protected void grdDailyPerformace_RowDataBound(object sender,GridViewRowEventArgs e) {

		if (e.Row.RowType == DataControlRowType.DataRow) {
			//今までの集計
			if (grdDailyPerformace.Visible) {
				prvTvTalkCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRV_TV_TALK_COUNT"));
				prvTvTalkMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRV_TV_TALK_MIN"));
				prvTvTalkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRV_TV_TALK_POINT"));
				pubTvTalkCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PUB_TV_TALK_COUNT"));
				pubTvTalkMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PUB_TV_TALK_MIN"));
				pubTvTalkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PUB_TV_TALK_POINT"));
				prvVoiceTalkCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRV_VOICE_TALK_COUNT"));
				prvVoiceTalkMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRV_VOICE_TALK_MIN"));
				prvVoiceTalkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRV_VOICE_TALK_POINT"));
				pubVoiceTalkCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PUB_VOICE_TALK_COUNT"));
				pubVoiceTalkMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PUB_VOICE_TALK_MIN"));
				pubVoiceTalkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PUB_VOICE_TALK_POINT"));
				viewTalkCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"VIEW_TALK_COUNT"));
				viewTalkMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"VIEW_TALK_MIN"));
				viewTalkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"VIEW_TALK_POINT"));
				viewBroadcastCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"VIEW_BROADCAST_COUNT"));
				viewBroadcastMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"VIEW_BROADCAST_MIN"));
				viewBroadcastPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"VIEW_BROADCAST_POINT"));
				liveCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"LIVE_COUNT"));
				liveMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"LIVE_MIN"));
				livePoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"LIVE_POINT"));
				//movieCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"MOVIE_COUNT"));
				//movieMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"MOVIE_MIN"));
				//moviePoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"MOVIE_POINT"));
				playProfileCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PLAY_PROFILE_COUNT"));
				playProfileMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PLAY_PROFILE_MIN"));
				playProfilePoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PLAY_PROFILE_POINT"));
				recProfileCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"REC_PROFILE_COUNT"));
				recProfileMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"REC_PROFILE_MIN"));
				recProfilePoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"REC_PROFILE_POINT"));
				wiretapCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"WIRETAP_COUNT"));
				wiretapMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"WIRETAP_MIN"));
				wiretapPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"WIRETAP_POINT"));
				gpfTalkVoiceCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"GPF_TALK_VOICE_COUNT"));
				gpfTalkVoiceMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"GPF_TALK_VOICE_MIN"));
				gpfTalkVoicePoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"GPF_TALK_VOICE_POINT"));
				playPvMsgCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PLAY_PV_MSG_COUNT"));
				playPvMsgMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PLAY_PV_MSG_MIN"));
				playPvMsgPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PLAY_PV_MSG_POINT"));
				castPrvTvTalkCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PRV_TV_TALK_COUNT"));
				castPrvTvTalkMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PRV_TV_TALK_MIN"));
				castPrvTvTalkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PRV_TV_TALK_POINT"));
				castPubTvTalkCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PUB_TV_TALK_COUNT"));
				castPubTvTalkMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PUB_TV_TALK_MIN"));
				castPubTvTalkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PUB_TV_TALK_POINT"));
				castPrvVoiceTalkCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PRV_VOICE_TALK_COUNT"));
				castPrvVoiceTalkMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PRV_VOICE_TALK_MIN"));
				castPrvVoiceTalkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PRV_VOICE_TALK_POINT"));
				castPubVoiceTalkCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PUB_VOICE_TALK_COUNT"));
				castPubVoiceTalkMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PUB_VOICE_TALK_MIN"));
				castPubVoiceTalkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"CAST_PUB_VOICE_TALK_POINT"));

				talkCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TALK_COUNT"));
				talkMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TALK_MIN"));
				talkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TALK_POINT"));
				freeDialCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"FREE_DIAL_COUNT"));
				freeDialMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"FREE_DIAL_MIN"));
				freeDialPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"FREE_DIAL_POINT"));
			} else {
				//FreeDial集計
				talkCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TALK_COUNT"));
				talkMin += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TALK_MIN"));
				talkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TALK_POINT"));
				talkPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"FREE_DIAL_POINT"));

				talkCountNf += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TALK_COUNT_NF"));
				talkMinNf += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TALK_MIN_NF"));
				talkPointNf += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TALK_POINT_NF"));

				talkCountFree += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"FREE_DIAL_COUNT"));
				talkMinFree += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"FREE_DIAL_MIN"));
				talkPointFree += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"FREE_DIAL_POINT2"));
			}

			mailCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"USER_MAIL_COUNT"));
			mailPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"USER_MAIL_POINT"));
			mailPicCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"USER_MAIL_PIC_COUNT"));
			mailPicPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"USER_MAIL_PIC_POINT"));
			mailMovieCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"USER_MAIL_MOVIE_COUNT"));
			mailMoviePoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"USER_MAIL_MOVIE_POINT"));
			openPicMailCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"OPEN_PIC_MAIL_COUNT"));
			openPicMailPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"OPEN_PIC_MAIL_POINT"));
			openMovieMailCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"OPEN_MOVIE_MAIL_COUNT"));
			openMovieMailPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"OPEN_MOVIE_MAIL_POINT"));
			openPicBbsCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"OPEN_PIC_BBS_COUNT"));
			openPicBbsPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"OPEN_PIC_BBS_POINT"));
			openMovieBbsCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"OPEN_MOVIE_BBS_COUNT"));
			openMovieBbsPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"OPEN_MOVIE_BBS_POINT"));
			writeBbsCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"WRITE_BBS_COUNT"));
			writeBbsPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"WRITE_BBS_POINT"));
			downloadMovieCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"DOWNLOAD_MOVIE_COUNT"));
			downloadMoviePoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"DOWNLOAD_MOVIE_POINT"));
			timeCallCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TIME_CALL_COUNT"));
			timeCallPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"TIME_CALL_POINT"));
			downloadVoiceCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"DOWNLOAD_VOICE_COUNT"));
			downloadVoicePoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"DOWNLOAD_VOICE_POINT"));
			socialGameCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"SOCIAL_GAME_COUNT"));
			socialGamePoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"SOCIAL_GAME_POINT"));
			yakyukenCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"YAKYUKEN_COUNT"));
			yakyukenPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"YAKYUKEN_POINT"));
			presentMailCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRESENT_MAIL_COUNT"));
			presentMailPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"PRESENT_MAIL_POINT"));
			webTotalCount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"WEB_TOTAL_COUNT"));
			webTotalPoint += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem,"WEB_TOTAL_POINT"));
		} else if (e.Row.RowType == DataControlRowType.Footer) {

			Label lblPrvTvTalkMin = (Label)e.Row.FindControl("lblPrvTvTalkMin");
			Label lblPrvTvTalkCount = (Label)e.Row.FindControl("lblPrvTvTalkCount");
			Label lblPrvTvTalkPoint = (Label)e.Row.FindControl("lblPrvTvTalkPoint");

			Label lblPubTvTalkMin = (Label)e.Row.FindControl("lblPubTvTalkMin");
			Label lblPubTvTalkCount = (Label)e.Row.FindControl("lblPubTvTalkCount");
			Label lblPubTvTalkPoint = (Label)e.Row.FindControl("lblPubTvTalkPoint");

			Label lblPrvVoiceTalkMin = (Label)e.Row.FindControl("lblPrvVoiceTalkMin");
			Label lblPrvVoiceTalkCount = (Label)e.Row.FindControl("lblPrvVoiceTalkCount");
			Label lblPrvVoiceTalkPoint = (Label)e.Row.FindControl("lblPrvVoiceTalkPoint");

			Label lblPubVoiceTalkMin = (Label)e.Row.FindControl("lblPubVoiceTalkMin");
			Label lblPubVoiceTalkCount = (Label)e.Row.FindControl("lblPubVoiceTalkCount");
			Label lblPubVoiceTalkPoint = (Label)e.Row.FindControl("lblPubVoiceTalkPoint");

			Label lblViewTalkMin = (Label)e.Row.FindControl("lblViewTalkMin");
			Label lblViewTalkCount = (Label)e.Row.FindControl("lblViewTalkCount");
			Label lblViewTalkPoint = (Label)e.Row.FindControl("lblViewTalkPoint");

			Label lblWiretapMin = (Label)e.Row.FindControl("lblWiretapMin");
			Label lblWiretapCount = (Label)e.Row.FindControl("lblWiretapCount");
			Label lblWiretapPoint = (Label)e.Row.FindControl("lblWiretapPoint");

			Label lblViewBroadcastMin = (Label)e.Row.FindControl("lblViewBroadcastMin");
			Label lblViewBroadcastCount = (Label)e.Row.FindControl("lblViewBroadcastCount");
			Label lblViewBroadcastPoint = (Label)e.Row.FindControl("lblViewBroadcastPoint");

			Label lblLiveMin = (Label)e.Row.FindControl("lblLiveMin");
			Label lblLiveCount = (Label)e.Row.FindControl("lblLiveCount");
			Label lblLivePoint = (Label)e.Row.FindControl("lblLivePoint");

			//Label lblMVMin = (Label)e.Row.FindControl("lblMVMin");
			//Label lblMVCount = (Label)e.Row.FindControl("lblMVCount");
			//Label lblMVPoint = (Label)e.Row.FindControl("lblMVPoint");

			Label lblPlayProfileMin = (Label)e.Row.FindControl("lblPlayProfileMin");
			Label lblPlayProfileCount = (Label)e.Row.FindControl("lblPlayProfileCount");
			Label lblPlayProfilePoint = (Label)e.Row.FindControl("lblPlayProfilePoint");

			Label lblRecProfileMin = (Label)e.Row.FindControl("lblRecProfileMin");
			Label lblRecProfileCount = (Label)e.Row.FindControl("lblRecProfileCount");
			Label lblRecProfilePoint = (Label)e.Row.FindControl("lblRecProfilePoint");

			Label lblGpfTalkVoiceMin = (Label)e.Row.FindControl("lblGpfTalkVoiceMin");
			Label lblGpfTalkVoiceCount = (Label)e.Row.FindControl("lblGpfTalkVoiceCount");
			Label lblGpfTalkVoicePoint = (Label)e.Row.FindControl("lblGpfTalkVoicePoint");

			Label lblPlayPvMsgMin = (Label)e.Row.FindControl("lblPlayPvMsgMin");
			Label lblPlayPvMsgCount = (Label)e.Row.FindControl("lblPlayPvMsgCount");
			Label lblPlayPvMsgPoint = (Label)e.Row.FindControl("lblPlayPvMsgPoint");

			Label lblCastPrvTvTalkMin = (Label)e.Row.FindControl("lblCastPrvTvTalkMin");
			Label lblCastPrvTvTalkCount = (Label)e.Row.FindControl("lblCastPrvTvTalkCount");
			Label lblCastPrvTvTalkPoint = (Label)e.Row.FindControl("lblCastPrvTvTalkPoint");

			Label lblCastPubTvTalkMin = (Label)e.Row.FindControl("lblCastPubTvTalkMin");
			Label lblCastPubTvTalkCount = (Label)e.Row.FindControl("lblCastPubTvTalkCount");
			Label lblCastPubTvTalkPoint = (Label)e.Row.FindControl("lblCastPubTvTalkPoint");

			Label lblCastPrvVoiceTalkMin = (Label)e.Row.FindControl("lblCastPrvVoiceTalkMin");
			Label lblCastPrvVoiceTalkCount = (Label)e.Row.FindControl("lblCastPrvVoiceTalkCount");
			Label lblCastPrvVoiceTalkPoint = (Label)e.Row.FindControl("lblCastPrvVoiceTalkPoint");

			Label lblCastPubVoiceTalkMin = (Label)e.Row.FindControl("lblCastPubVoiceTalkMin");
			Label lblCastPubVoiceTalkCount = (Label)e.Row.FindControl("lblCastPubVoiceTalkCount");
			Label lblCastPubVoiceTalkPoint = (Label)e.Row.FindControl("lblCastPubVoiceTalkPoint");


			if (grdDailyPerformace.Visible) {
				Label lblTalkMin = (Label)e.Row.FindControl("lblTalkMin");
				Label lblTalkCount = (Label)e.Row.FindControl("lblTalkCount");
				Label lblTalkPoint = (Label)e.Row.FindControl("lblTalkPoint");

				Label lblFreeDialMin = (Label)e.Row.FindControl("lblFreeDialMin");
				Label lblFreeDialCount = (Label)e.Row.FindControl("lblFreeDialCount");
				Label lblFreeDialPoint = (Label)e.Row.FindControl("lblFreeDialPoint");

				lblTalkMin.Text = talkMin.ToString();
				lblTalkCount.Text = talkCount.ToString();
				lblTalkPoint.Text = talkPoint.ToString();

				lblFreeDialMin.Text = freeDialMin.ToString();
				lblFreeDialCount.Text = freeDialCount.ToString();
				lblFreeDialPoint.Text = freeDialPoint.ToString();
			} else {
				Label lblTalkMin = (Label)e.Row.FindControl("lblTalkMin");
				Label lblTalkCount = (Label)e.Row.FindControl("lblTalkCount");
				Label lblTalkPoint = (Label)e.Row.FindControl("lblTalkPoint");

				Label lblTalkMinNf = (Label)e.Row.FindControl("lblTalkMinNf");
				Label lblTalkCountNf = (Label)e.Row.FindControl("lblTalkCountNf");
				Label lblTalkPointNf = (Label)e.Row.FindControl("lblTalkPointNf");

				Label lblTalkMinFree = (Label)e.Row.FindControl("lblTalkMinFree");
				Label lblTalkCountFree = (Label)e.Row.FindControl("lblTalkCountFree");
				Label lblTalkPointFree = (Label)e.Row.FindControl("lblTalkPointFree");

				lblTalkMin.Text = talkMin.ToString();
				lblTalkCount.Text = talkCount.ToString();
				lblTalkPoint.Text = talkPoint.ToString();

				lblTalkMinNf.Text = talkMinNf.ToString();
				lblTalkCountNf.Text = talkCountNf.ToString();
				lblTalkPointNf.Text = talkPointNf.ToString();

				lblTalkMinFree.Text = talkMinFree.ToString();
				lblTalkCountFree.Text = talkCountFree.ToString();
				lblTalkPointFree.Text = talkPointFree.ToString();
			}

			Label lblMLCount = (Label)e.Row.FindControl("lblMLCount");
			Label lblMLPoint = (Label)e.Row.FindControl("lblMLPoint");

			Label lblMLPicCount = (Label)e.Row.FindControl("lblMLPicCount");
			Label lblMLPicPoint = (Label)e.Row.FindControl("lblMLPicPoint");

			Label lblMLMovieCount = (Label)e.Row.FindControl("lblMLMovieCount");
			Label lblMLMoviePoint = (Label)e.Row.FindControl("lblMLMoviePoint");

			Label lblOpenPicMailCount = (Label)e.Row.FindControl("lblOpenPicMailCount");
			Label lblOpenPicMailPoint = (Label)e.Row.FindControl("lblOpenPicMailPoint");

			Label lblOpenMovieMailCount = (Label)e.Row.FindControl("lblOpenMovieMailCount");
			Label lblOpenMovieMailPoint = (Label)e.Row.FindControl("lblOpenMovieMailPoint");

			Label lblOpenPicBbsCount = (Label)e.Row.FindControl("lblOpenPicBbsCount");
			Label lblOpenPicBbsPoint = (Label)e.Row.FindControl("lblOpenPicBbsPoint");

			Label lblOpenMovieBbsCount = (Label)e.Row.FindControl("lblOpenMovieBbsCount");
			Label lblOpenMovieBbsPoint = (Label)e.Row.FindControl("lblOpenMovieBbsPoint");

			Label lblWriteBbsCount = (Label)e.Row.FindControl("lblWriteBbsCount");
			Label lblWriteBbsPoint = (Label)e.Row.FindControl("lblWriteBbsPoint");

			Label lblDownloadMovieCount = (Label)e.Row.FindControl("lblDownloadMovieCount");
			Label lblDownloadMoviePoint = (Label)e.Row.FindControl("lblDownloadMoviePoint");

			Label lblTimeCallCount = (Label)e.Row.FindControl("lblTimeCallCount");
			Label lblTimeCallPoint = (Label)e.Row.FindControl("lblTimeCallPoint");

			Label lblDownloadVoiceCount = (Label)e.Row.FindControl("lblDownloadVoiceCount");
			Label lblDownloadVoicePoint = (Label)e.Row.FindControl("lblDownloadVoicePoint");

			Label lblSocialGameCount = (Label)e.Row.FindControl("lblSocialGameCount");
			Label lblSocialGamePoint = (Label)e.Row.FindControl("lblSocialGamePoint");

			Label lblYakyukenCount = (Label)e.Row.FindControl("lblYakyukenCount");
			Label lblYakyukenPoint = (Label)e.Row.FindControl("lblYakyukenPoint");

			Label lblPresentMailCount = (Label)e.Row.FindControl("lblPresentMailCount");
			Label lblPresentMailPoint = (Label)e.Row.FindControl("lblPresentMailPoint");

			Label lblWebTotalCount = (Label)e.Row.FindControl("lblWebTotalCount");
			Label lblWebTotalPoint = (Label)e.Row.FindControl("lblWebTotalPoint");

			Label lblTotalCount = (Label)e.Row.FindControl("lblTotalCount");
			Label lblTotalPoint = (Label)e.Row.FindControl("lblTotalPoint");

			int iTotalCount = talkCount + webTotalCount;
			int iTotalPoint = talkPoint + webTotalPoint;

			lblPrvTvTalkMin.Text = prvTvTalkMin.ToString();
			lblPrvTvTalkCount.Text = prvTvTalkCount.ToString();
			lblPrvTvTalkPoint.Text = prvTvTalkPoint.ToString();

			lblPubTvTalkMin.Text = pubTvTalkMin.ToString();
			lblPubTvTalkCount.Text = pubTvTalkCount.ToString();
			lblPubTvTalkPoint.Text = pubTvTalkPoint.ToString();

			lblPrvVoiceTalkMin.Text = prvVoiceTalkMin.ToString();
			lblPrvVoiceTalkCount.Text = prvVoiceTalkCount.ToString();
			lblPrvVoiceTalkPoint.Text = prvVoiceTalkPoint.ToString();

			lblPubVoiceTalkMin.Text = pubVoiceTalkMin.ToString();
			lblPubVoiceTalkCount.Text = pubVoiceTalkCount.ToString();
			lblPubVoiceTalkPoint.Text = pubVoiceTalkPoint.ToString();

			lblViewTalkMin.Text = viewTalkMin.ToString();
			lblViewTalkCount.Text = viewTalkCount.ToString();
			lblViewTalkPoint.Text = viewTalkPoint.ToString();

			lblWiretapMin.Text = wiretapMin.ToString();
			lblWiretapCount.Text = wiretapCount.ToString();
			lblWiretapPoint.Text = wiretapPoint.ToString();

			lblViewBroadcastMin.Text = viewBroadcastMin.ToString();
			lblViewBroadcastCount.Text = viewBroadcastCount.ToString();
			lblViewBroadcastPoint.Text = viewBroadcastPoint.ToString();

			lblLiveMin.Text = liveMin.ToString();
			lblLiveCount.Text = liveCount.ToString();
			lblLivePoint.Text = livePoint.ToString();

			//lblMVMin.Text = movieMin.ToString();
			//lblMVCount.Text = movieCount.ToString();
			//lblMVPoint.Text = moviePoint.ToString();

			lblPlayProfileMin.Text = playProfileMin.ToString();
			lblPlayProfileCount.Text = playProfileCount.ToString();
			lblPlayProfilePoint.Text = playProfilePoint.ToString();

			lblRecProfileMin.Text = recProfileMin.ToString();
			lblRecProfileCount.Text = recProfileCount.ToString();
			lblRecProfilePoint.Text = recProfilePoint.ToString();

			lblGpfTalkVoiceMin.Text = gpfTalkVoiceMin.ToString();
			lblGpfTalkVoiceCount.Text = gpfTalkVoiceCount.ToString();
			lblGpfTalkVoicePoint.Text = gpfTalkVoicePoint.ToString();

			lblPlayPvMsgMin.Text = playPvMsgMin.ToString();
			lblPlayPvMsgCount.Text = playPvMsgCount.ToString();
			lblPlayPvMsgPoint.Text = playPvMsgPoint.ToString();

			lblCastPrvTvTalkMin.Text = castPrvTvTalkMin.ToString();
			lblCastPrvTvTalkCount.Text = castPrvTvTalkCount.ToString();
			lblCastPrvTvTalkPoint.Text = castPrvTvTalkPoint.ToString();

			lblCastPubTvTalkMin.Text = castPubTvTalkMin.ToString();
			lblCastPubTvTalkCount.Text = castPubTvTalkCount.ToString();
			lblCastPubTvTalkPoint.Text = castPubTvTalkPoint.ToString();

			lblCastPrvVoiceTalkMin.Text = castPrvVoiceTalkMin.ToString();
			lblCastPrvVoiceTalkCount.Text = castPrvVoiceTalkCount.ToString();
			lblCastPrvVoiceTalkPoint.Text = castPrvVoiceTalkPoint.ToString();

			lblCastPubVoiceTalkMin.Text = castPubVoiceTalkMin.ToString();
			lblCastPubVoiceTalkCount.Text = castPubVoiceTalkCount.ToString();
			lblCastPubVoiceTalkPoint.Text = castPubVoiceTalkPoint.ToString();

			lblMLCount.Text = mailCount.ToString();
			lblMLPoint.Text = mailPoint.ToString();

			lblMLPicCount.Text = mailPicCount.ToString();
			lblMLPicPoint.Text = mailPicPoint.ToString();
			lblMLMovieCount.Text = mailMovieCount.ToString();
			lblMLMoviePoint.Text = mailMoviePoint.ToString();

			lblOpenPicMailCount.Text = openPicMailCount.ToString();
			lblOpenPicMailPoint.Text = openPicMailPoint.ToString();

			lblOpenMovieMailCount.Text = openMovieMailCount.ToString();
			lblOpenMovieMailPoint.Text = openMovieMailPoint.ToString();

			lblOpenPicBbsCount.Text = openPicBbsCount.ToString();
			lblOpenPicBbsPoint.Text = openPicBbsPoint.ToString();

			lblOpenMovieBbsCount.Text = openMovieBbsCount.ToString();
			lblOpenMovieBbsPoint.Text = openMovieBbsPoint.ToString();

			lblWriteBbsCount.Text = writeBbsCount.ToString();
			lblWriteBbsPoint.Text = writeBbsPoint.ToString();

			lblDownloadMovieCount.Text = downloadMovieCount.ToString();
			lblDownloadMoviePoint.Text = downloadMoviePoint.ToString();

			lblTimeCallCount.Text = timeCallCount.ToString();
			lblTimeCallPoint.Text = timeCallPoint.ToString();

			lblDownloadVoiceCount.Text = downloadVoiceCount.ToString();
			lblDownloadVoicePoint.Text = downloadVoicePoint.ToString();

			lblSocialGameCount.Text = socialGameCount.ToString();
			lblSocialGamePoint.Text = socialGamePoint.ToString();

			lblYakyukenCount.Text = yakyukenCount.ToString();
			lblYakyukenPoint.Text = yakyukenPoint.ToString();

			lblPresentMailCount.Text = presentMailCount.ToString();
			lblPresentMailPoint.Text = presentMailPoint.ToString();

			lblWebTotalCount.Text = webTotalCount.ToString();
			lblWebTotalPoint.Text = webTotalPoint.ToString();

			lblTotalCount.Text = iTotalCount.ToString();
			lblTotalPoint.Text = iTotalPoint.ToString();
		}
	}

	private void GetList() {
		if (!chkCount.Checked && !chkMin.Checked && !chkPt.Checked) {
			chkCount.Checked = true;
			chkMin.Checked = true;
			chkPt.Checked = true;
		}
		ClearField();
		grdDailyPerformace.PageIndex = 0;
		pnlInfo.Visible = true;
		int iCompanyMask = int.Parse(Session["CompanyMask"].ToString());
		
		grdDailyPerformace.DataSourceID = "dsDailyPerformace";
		grdDailyPerformace.Visible = true;

		//VCS系の遊びの欄を非表示
		grdDailyPerformace.Columns[9].Visible = false;
		grdDailyPerformace.Columns[10].Visible = false;
		grdDailyPerformace.Columns[11].Visible = false;
		grdDailyPerformace.Columns[12].Visible = false;

		grdDailyPerformace.Columns[26].Visible = IsAvailableService(ViCommConst.RELEASE_SALE_MOVIE);
		grdDailyPerformace.Columns[27].Visible = IsAvailableService(ViCommConst.RELEASE_TIME_CALL);
		grdDailyPerformace.Columns[28].Visible = IsAvailableService(ViCommConst.RELEASE_SALE_VOICE);

		grdDailyPerformace.Columns[29].Visible = true;

		DataBind();
		int iCompare = string.Compare(Session["AdminType"].ToString(),ViCommConst.RIGHT_SITE_MANAGER);
		btnCsv.Visible = (iCompare >= 0);
	}

	public static bool IsAvailableService(ulong pService) {
		using (ManageCompany oInstance = new ManageCompany()) {
			return oInstance.IsAvailableService(pService);
		}
	}

	private string GetCsvString(DataSet pDataSet) {
		int iCompanyMask = int.Parse(Session["CompanyMask"].ToString());
		string sCsv = "";

		sCsv += "時,";
		sCsv += "TV着信,";
		sCsv += "TV分数,";
		sCsv += "TV消費Pt,";
		sCsv += "TVﾁｬｯﾄ着信,";
		sCsv += "TVﾁｬｯﾄ分数,";
		sCsv += "TVﾁｬｯﾄ消費Pt,";
		sCsv += "音声着信,";
		sCsv += "音声分数,";
		sCsv += "音声Pt,";
		sCsv += "音声ﾁｬｯﾄ着信,";
		sCsv += "音声ﾁｬｯﾄ分数,";
		sCsv += "音声ﾁｬｯﾄPt,";
		sCsv += "会話ﾓﾆﾀ着信,";
		sCsv += "会話ﾓﾆﾀ分数,";
		sCsv += "会話ﾓﾆﾀPt,";
		sCsv += "音声ﾓﾆﾀ着信,";
		sCsv += "音声ﾓﾆﾀ分数,";
		sCsv += "音声ﾓﾆﾀPt,";
		sCsv += "部屋ﾓﾆﾀ着信,";
		sCsv += "部屋ﾓﾆﾀ分数,";
		sCsv += "部屋ﾓﾆﾀPt,";
		sCsv += "ﾗｲﾌﾞ視聴着信,";
		sCsv += "ﾗｲﾌﾞ視聴分数,";
		sCsv += "ﾗｲﾌﾞ視聴Pt,";
		sCsv += "男性待機TV着信,";
		sCsv += "男性待機TV分数,";
		sCsv += "男性待機TV消費Pt,";
		sCsv += "男性待機TVﾁｬｯﾄ着信,";
		sCsv += "男性待機TVﾁｬｯﾄ分数,";
		sCsv += "男性待機TVﾁｬｯﾄ消費Pt,";
		sCsv += "男性待機音声着信,";
		sCsv += "男性待機音声分数,";
		sCsv += "男性待機音声Pt,";
		sCsv += "男性待機音声ﾁｬｯﾄ着信,";
		sCsv += "男性待機音声ﾁｬｯﾄ分数,";
		sCsv += "男性待機音声ﾁｬｯﾄPt,";
		sCsv += "TV電話着信計,";
		sCsv += "TV電話分数計,";
		sCsv += "TV電話Pt計,";
		sCsv += "FREEﾀﾞｲｱﾙ着信計,";
		sCsv += "FREEﾀﾞｲｱﾙ分数計,";
		sCsv += "FREEﾀﾞｲｱﾙPt計,";
		sCsv += "ﾒｰﾙ件数,";
		sCsv += "ﾒｰﾙPt,";
		sCsv += "写真添付ﾒｰﾙ件数,";
		sCsv += "写真添付ﾒｰﾙPt,";
		sCsv += "動画添付ﾒｰﾙ件数,";
		sCsv += "動画添付ﾒｰﾙPt,";
		sCsv += "ﾒｰﾙ添付写真閲覧件数,";
		sCsv += "ﾒｰﾙ添付写真閲覧Pt,";
		sCsv += "ﾒｰﾙ添付動画閲覧件数,";
		sCsv += "ﾒｰﾙ添付動画閲覧Pt,";
		sCsv += "BBS写真閲覧件数,";
		sCsv += "BBS写真閲覧Pt,";
		sCsv += "BBS動画閲覧件数,";
		sCsv += "BBS動画閲覧Pt,";
		sCsv += "ｿｰｼｬﾙｹﾞｰﾑ件数,";
		sCsv += "ｿｰｼｬﾙｹﾞｰﾑPt,";
		sCsv += "野球拳/FC会費件数,";
		sCsv += "野球拳/FC会費Pt,";
		sCsv += "ﾌﾟﾚｾﾞﾝﾄﾒｰﾙ件数,";
		sCsv += "ﾌﾟﾚｾﾞﾝﾄﾒｰﾙPt,";
		sCsv += "WEB利用回数計,";
		sCsv += "WEB利用Pt計,";
		sCsv += "合計回数,";
		sCsv += "合計Pt";
		sCsv += "\r\n";

		DataRow dr;
		int iTotalCount,iTotalPoint;

		for (int i = 0;i < pDataSet.Tables[0].Rows.Count;i++) {
			dr = pDataSet.Tables[0].Rows[i];

			iTotalCount = int.Parse(dr["TALK_COUNT"].ToString()) + int.Parse(dr["WEB_TOTAL_COUNT"].ToString());
			iTotalPoint = int.Parse(dr["TALK_POINT"].ToString()) + int.Parse(dr["WEB_TOTAL_POINT"].ToString());

			sCsv += dr["REPORT_HOUR"] + ",";
			sCsv += dr["PRV_TV_TALK_COUNT"] + ",";
			sCsv += dr["PRV_TV_TALK_MIN"] + ",";
			sCsv += dr["PRV_TV_TALK_POINT"] + ",";
			sCsv += dr["PUB_TV_TALK_COUNT"] + ",";
			sCsv += dr["PUB_TV_TALK_MIN"] + ",";
			sCsv += dr["PUB_TV_TALK_POINT"] + ",";
			sCsv += dr["PRV_VOICE_TALK_COUNT"] + ",";
			sCsv += dr["PRV_VOICE_TALK_MIN"] + ",";
			sCsv += dr["PRV_VOICE_TALK_POINT"] + ",";
			sCsv += dr["PUB_VOICE_TALK_COUNT"] + ",";
			sCsv += dr["PUB_VOICE_TALK_MIN"] + ",";
			sCsv += dr["PUB_VOICE_TALK_POINT"] + ",";
			sCsv += dr["VIEW_TALK_COUNT"] + ",";
			sCsv += dr["VIEW_TALK_MIN"] + ",";
			sCsv += dr["VIEW_TALK_POINT"] + ",";
			sCsv += dr["WIRETAP_COUNT"] + ",";
			sCsv += dr["WIRETAP_MIN"] + ",";
			sCsv += dr["WIRETAP_POINT"] + ",";
			sCsv += dr["VIEW_BROADCAST_COUNT"] + ",";
			sCsv += dr["VIEW_BROADCAST_MIN"] + ",";
			sCsv += dr["VIEW_BROADCAST_POINT"] + ",";
			sCsv += dr["LIVE_COUNT"] + ",";
			sCsv += dr["LIVE_MIN"] + ",";
			sCsv += dr["LIVE_POINT"] + ",";
			sCsv += dr["CAST_PRV_TV_TALK_COUNT"] + ",";
			sCsv += dr["CAST_PRV_TV_TALK_MIN"] + ",";
			sCsv += dr["CAST_PRV_TV_TALK_POINT"] + ",";
			sCsv += dr["CAST_PUB_TV_TALK_COUNT"] + ",";
			sCsv += dr["CAST_PUB_TV_TALK_MIN"] + ",";
			sCsv += dr["CAST_PUB_TV_TALK_POINT"] + ",";
			sCsv += dr["CAST_PRV_VOICE_TALK_COUNT"] + ",";
			sCsv += dr["CAST_PRV_VOICE_TALK_MIN"] + ",";
			sCsv += dr["CAST_PRV_VOICE_TALK_POINT"] + ",";
			sCsv += dr["CAST_PUB_VOICE_TALK_COUNT"] + ",";
			sCsv += dr["CAST_PUB_VOICE_TALK_MIN"] + ",";
			sCsv += dr["CAST_PUB_VOICE_TALK_POINT"] + ",";
			sCsv += dr["TALK_COUNT"] + ",";
			sCsv += dr["TALK_MIN"] + ",";
			sCsv += dr["TALK_POINT"] + ",";
			sCsv += dr["FREE_DIAL_COUNT"] + ",";
			sCsv += dr["FREE_DIAL_MIN"] + ",";
			sCsv += dr["FREE_DIAL_POINT"] + ",";
			sCsv += dr["USER_MAIL_COUNT"] + ",";
			sCsv += dr["USER_MAIL_POINT"] + ",";
			sCsv += dr["USER_MAIL_PIC_COUNT"] + ",";
			sCsv += dr["USER_MAIL_PIC_POINT"] + ",";
			sCsv += dr["USER_MAIL_MOVIE_COUNT"] + ",";
			sCsv += dr["USER_MAIL_MOVIE_POINT"] + ",";
			sCsv += dr["OPEN_PIC_MAIL_COUNT"] + ",";
			sCsv += dr["OPEN_PIC_MAIL_POINT"] + ",";
			sCsv += dr["OPEN_MOVIE_MAIL_COUNT"] + ",";
			sCsv += dr["OPEN_MOVIE_MAIL_POINT"] + ",";
			sCsv += dr["OPEN_PIC_BBS_COUNT"] + ",";
			sCsv += dr["OPEN_PIC_BBS_POINT"] + ",";
			sCsv += dr["OPEN_MOVIE_BBS_COUNT"] + ",";
			sCsv += dr["OPEN_MOVIE_BBS_POINT"] + ",";
			sCsv += dr["SOCIAL_GAME_COUNT"] + ",";
			sCsv += dr["SOCIAL_GAME_POINT"] + ",";
			sCsv += dr["YAKYUKEN_COUNT"] + ",";
			sCsv += dr["YAKYUKEN_POINT"] + ",";
			sCsv += dr["PRESENT_MAIL_COUNT"] + ",";
			sCsv += dr["PRESENT_MAIL_POINT"] + ",";
			sCsv += dr["WEB_TOTAL_COUNT"] + ",";
			sCsv += dr["WEB_TOTAL_POINT"] + ",";
			sCsv += iTotalCount.ToString() + ",";
			sCsv += iTotalPoint.ToString();
			sCsv += "\r\n";
		}
		return DisplayWordUtil.Replace(sCsv);
	}

	protected string CalcSum(object pSum1,object pSum2) {
		int iSum = int.Parse(pSum1.ToString()) + int.Parse(pSum2.ToString());
		return iSum.ToString();
	}

	protected string CalcSum(object pSum1,object pSum2,object pSum3) {
		int iSum = int.Parse(pSum1.ToString()) + int.Parse(pSum2.ToString()) + int.Parse(pSum3.ToString());
		return iSum.ToString();
	}
	//0:FREE以外 1:FREEのみ 2:合計



	protected string OutputByCheckBox(object pNum0,object pNum1,object pNum2) {
		return "";
	}

	protected void grdDailyPerformace_RowCreated(object sender,GridViewRowEventArgs e) {
		if ((e.Row.RowType == DataControlRowType.Header) ||
			(e.Row.RowType == DataControlRowType.DataRow) ||
			(e.Row.RowType == DataControlRowType.Footer)) {
			if (rdoTel.Checked) {
				e.Row.Cells[18].Visible = false;
				e.Row.Cells[19].Visible = false;
				e.Row.Cells[20].Visible = false;
				e.Row.Cells[21].Visible = false;
				e.Row.Cells[22].Visible = false;
				e.Row.Cells[23].Visible = false;
				e.Row.Cells[24].Visible = false;
				e.Row.Cells[25].Visible = false;
				e.Row.Cells[26].Visible = false;
				e.Row.Cells[27].Visible = false;
				e.Row.Cells[28].Visible = false;
				e.Row.Cells[29].Visible = false;
				e.Row.Cells[30].Visible = false;
				e.Row.Cells[31].Visible = false;
			} else if (rdoWeb.Checked) {
				e.Row.Cells[1].Visible = false;
				e.Row.Cells[2].Visible = false;
				e.Row.Cells[3].Visible = false;
				e.Row.Cells[4].Visible = false;
				e.Row.Cells[5].Visible = false;
				e.Row.Cells[6].Visible = false;
				e.Row.Cells[7].Visible = false;
				e.Row.Cells[8].Visible = false;
				e.Row.Cells[9].Visible = false;
				e.Row.Cells[10].Visible = false;
				e.Row.Cells[11].Visible = false;
				e.Row.Cells[12].Visible = false;
				e.Row.Cells[13].Visible = false;
				e.Row.Cells[14].Visible = false;
				e.Row.Cells[15].Visible = false;
				e.Row.Cells[16].Visible = false;
			} else {
				e.Row.Cells[20].Visible = false;
				e.Row.Cells[21].Visible = false;
				e.Row.Cells[22].Visible = false;
				e.Row.Cells[23].Visible = false;
				e.Row.Cells[24].Visible = false;
				e.Row.Cells[25].Visible = false;
				e.Row.Cells[26].Visible = false;
				e.Row.Cells[27].Visible = false;
				e.Row.Cells[28].Visible = false;
				e.Row.Cells[29].Visible = false;
				e.Row.Cells[30].Visible = false;
				e.Row.Cells[31].Visible = false;
				e.Row.Cells[32].Visible = false;
			}
		}
	}

	protected bool GetBrMin() {
		if (chkMin.Checked) {
			if (chkCount.Checked || chkPt.Checked) {
				return true;
			}
		}
		return false;
	}

	protected bool GetBrCount() {
		if (chkCount.Checked) {
			if (chkPt.Checked) {
				return true;
			}
		}
		return false;
	}

	protected bool CheckedMin() {
		return chkMin.Checked;
	}

	protected bool CheckedCount() {
		return chkCount.Checked;
	}

	protected bool CheckedPt() {
		return chkPt.Checked;
	}
}
